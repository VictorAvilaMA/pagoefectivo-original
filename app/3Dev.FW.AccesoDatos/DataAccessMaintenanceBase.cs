using System;
using System.Collections.Generic;
using _3Dev.FW.Entidades;


namespace _3Dev.FW.AccesoDatos
{
    [Serializable]
    public class DataAccessMaintenanceBase:DataAccessBase 
    {
        public virtual int InsertRecord(BusinessEntityBase be)
        {
            return 1;
        }
        //public virtual int DeleteERecord(
        public virtual int UpdateRecord(BusinessEntityBase be)
        {
            return 1;
        }
        public virtual BusinessEntityBase GetRecordByID(object id)
        {
            return null;
        }

        [Obsolete]
        public virtual int DeleteRecord(BusinessEntityBase be)
        {
            return 1;
        }
        public virtual int DeleteRecordByEntityId(object entityId)
        {
            return 1;
        }

        public virtual List<BusinessEntityBase> SearchByParameters(BusinessEntityBase be)
        {
            return null;
        }

        public virtual object GetObjectByParameters(string key, BusinessEntityBase be)
        {
            return null;
        }
        public virtual List<BusinessEntityBase> GetListByParameters(string key, BusinessEntityBase be)
        {
            return null;
        }
        public virtual BusinessEntityBase GetBusinessEntityByParameters(string key, BusinessEntityBase be)
        {
            return null;
        }
        public virtual int ExecProcedureByParameters(string key, BusinessEntityBase be)
        {
            return -1;
        }

        #region entity methods
        public virtual BusinessMessageBase GetEntityByID(object id)
        {
            throw new NotImplementedException();
        }
        public virtual BusinessMessageBase GetEntity(BusinessMessageBase request)
        {
            throw new NotImplementedException();
        }
        public virtual BusinessMessageBase DeleteEntity(BusinessMessageBase request)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
