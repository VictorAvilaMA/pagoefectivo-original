using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
//using IS_Security;
//using CustomCache;

namespace _3Dev.FW.AccesoDatos.Seguridad
{
    [Serializable]
    public class CustomMembershipProvider : MembershipProvider
    {
        //#region Constructors
        public CustomMembershipProvider()
        {


            //DataHelper.ProviderName = BaseAD.ProviderName;
        }
        //#endregion

        #region Global Variables
        //
        // Global connection string, generated password length, generic exception message, event log info.
        //
        //private int newPasswordLength = 8;
        private string eventSource = "InterseguroMembershipProvider";
        private string eventLog = "Application";
        private string exceptionMessage = "Una excepcion ha ocurrido, por favor revise el visor de eventos";
        //private string tableName = "UsuarioS10";
        private string connectionString = "";

        //
        // Used when determining encryption key values.
        //
        //private MachineKeySection machineKey=null;

        //
        // If false, exceptions are thrown to the caller. If true,
        // exceptions are written to the event log.
        //
        private bool pWriteExceptionsToEventLog;

        public bool WriteExceptionsToEventLog
        {
            get { return pWriteExceptionsToEventLog; }
            set { pWriteExceptionsToEventLog = value; }
        }

        #endregion

        #region MembershipProvider initialize
        //
        // System.Configuration.Provider.ProviderBase.Initialize Method
        //
        public override void Initialize(string name, NameValueCollection config)
        {
            //
            // Initialize values from web.config.
            //
            if (config == null)
                throw new ArgumentNullException("config");
            if (name == null || name.Length == 0)
                name = "InterseguroMembershipProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Interseguro Membership provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            // pApplicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            pMaxInvalidPasswordAttempts = Convert.ToInt32(this.GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            pPasswordAttemptWindow = Convert.ToInt32(this.GetConfigValue(config["passwordAttemptWindow"], "10"));
            pMinRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            pMinRequiredPasswordLength = Convert.ToInt32(this.GetConfigValue(config["minRequiredPasswordLength"], "7"));
            pPasswordStrengthRegularExpression = Convert.ToString(this.GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            pEnablePasswordReset = Convert.ToBoolean(this.GetConfigValue(config["enablePasswordReset"], "true"));
            pEnablePasswordRetrieval = Convert.ToBoolean(this.GetConfigValue(config["enablePasswordRetrieval"], "true"));
            pRequiresQuestionAndAnswer = Convert.ToBoolean(this.GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            pRequiresUniqueEmail = Convert.ToBoolean(this.GetConfigValue(config["requiresUniqueEmail"], "true"));
            pWriteExceptionsToEventLog = Convert.ToBoolean(this.GetConfigValue(config["writeExceptionsToEventLog"], "true"));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    pPasswordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    pPasswordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    pPasswordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Formato de password no soportado.");
            }

            //
            // Initialize OdbcConnection.
            //

            ConnectionStringSettings ConnectionStringSettings =
            ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

            if ((ConnectionStringSettings == null) || (ConnectionStringSettings.ConnectionString.Trim() == ""))
            {
                throw new ProviderException("Cadena de conexion no puede estar en blanco.");
            }

            connectionString = ConnectionStringSettings.ConnectionString;


            // Get encryption and decryption key information from the configuration.
            //Configuration cfg =
            //WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            //machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");
        }

        public DateTime GetConfigValue()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //
        // A helper function to retrieve config values from the configuration file.
        //
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }
        #endregion

        #region MembershipProvider properties
        //
        // System.Web.Security.MembershipProvider properties.
        //
        private string pApplicationName;
        private bool pEnablePasswordReset;
        private bool pEnablePasswordRetrieval;
        private bool pRequiresQuestionAndAnswer;
        private bool pRequiresUniqueEmail;
        private int pMaxInvalidPasswordAttempts;
        private int pPasswordAttemptWindow;
        private MembershipPasswordFormat pPasswordFormat;

        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        public override bool EnablePasswordReset
        {
            get { return pEnablePasswordReset; }
        }


        public override bool EnablePasswordRetrieval
        {
            get { return pEnablePasswordRetrieval; }
        }


        public override bool RequiresQuestionAndAnswer
        {
            get { return pRequiresQuestionAndAnswer; }
        }


        public override bool RequiresUniqueEmail
        {
            get { return pRequiresUniqueEmail; }
        }


        public override int MaxInvalidPasswordAttempts
        {
            get { return pMaxInvalidPasswordAttempts; }
        }


        public override int PasswordAttemptWindow
        {
            get { return pPasswordAttemptWindow; }
        }


        public override MembershipPasswordFormat PasswordFormat
        {
            get { return pPasswordFormat; }
        }

        private int pMinRequiredNonAlphanumericCharacters;

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return pMinRequiredNonAlphanumericCharacters; }
        }

        private int pMinRequiredPasswordLength;

        public override int MinRequiredPasswordLength
        {
            get { return pMinRequiredPasswordLength; }
        }

        private string pPasswordStrengthRegularExpression;

        public override string PasswordStrengthRegularExpression
        {
            get { return pPasswordStrengthRegularExpression; }
        }
        #endregion

        #region MembershipProvider methods
        //
        // System.Web.Security.MembershipProvider methods.
        //

        //
        // MembershipProvider.ChangePassword
        //
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            return false;
        }

        //
        // MembershipProvider.ChangePasswordQuestionAndAnswer
        // no implementada
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
        {

            return false;

        }

        //
        // MembershipProvider.CreateUser
        //
        public override MembershipUser CreateUser(string username, string password, string primNombre, string appPaterno, string appMaterno, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            return CreateUser(username, password, appPaterno, appMaterno, primNombre, out status);

        }
        // Sobrecarga para adaptar a tabla usuario S10
        private MembershipUser CreateUser(string codusuario, string pass, string appPaterno, string appMaterno, string Primnombre, out MembershipCreateStatus status)
        {

            throw new ProviderException("opcion no implementada.");
        }

        //
        // MembershipProvider.DeleteUser
        //
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new ProviderException("opcion no implementada.");

        }

        //
        // MembershipProvider.GetAllUsers
        // no implementada
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new ProviderException("opcion no implementada.");


        }

        //
        // MembershipProvider.GetNumberOfUsersOnline
        // no implementada
        public override int GetNumberOfUsersOnline()
        {
            throw new ProviderException("opcion no implementada.");


        }

        //
        // MembershipProvider.GetPassword
        // no implementada
        public virtual string GetPassword(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            string ret = string.Empty;
            using (IDataReader reader = oDataBase.ExecuteReader("[PagoEfectivo].prc_LeerPass", username))
            {
                if (reader.Read())
                    ret = reader[0].ToString();
            }
            return ret;


        }

        //
        // MembershipProvider.GetUser(string, bool)
        //
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new ProviderException("opcion no implementada.");

        }


        // MembershipProvider.GetUser(object, bool)

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new ProviderException("opcion no implementada.");


        }

        //
        // GetUserFromReader
        //    A helper function that takes the current row from the OdbcDataReader
        // and hydrates a MembershiUser from the values. Called by the 
        // MembershipUser.GetUser implementation.
        //
        private MembershipUser GetUserFromReader(DataRow reader)
        {
            throw new ProviderException("opcion no implementada.");

        }

        //
        // MembershipProvider.UnlockUser
        // no implementada
        public override bool UnlockUser(string username)
        {
            throw new ProviderException("opcion no implementada.");
        }

        //
        // MembershipProvider.GetUserNameByEmail
        // no implementada
        public override string GetUserNameByEmail(string email)
        {
            throw new ProviderException("opcion no implementada.");

        }

        //
        // MembershipProvider.ResetPassword
        // no implementada
        public override string ResetPassword(string username, string answer)
        {

            throw new ProviderException("opcion no implementada.");

        }

        //
        // MembershipProvider.UpdateUser
        //

        public void ActualizarUsuario(string telefonofijo, string celular, MembershipUser user)
        {
            throw new ProviderException("opcion no implementada.");
        }
        public override void UpdateUser(MembershipUser user)
        {
            throw new ProviderException("opcion no implementada.");
        }

        public virtual bool ValidateUserAndPass(string username, string password)
        {
            return ValidateUser(username, password);
        }

        //
        // MembershipProvider.ValidateUser
        //

        public override bool ValidateUser(string username, string password)
        {

            try
            {
                bool exists = ExistUser(username);
                int intentos = 0;
                if (!exists)
                    return false;
                else intentos = getNumberAttempts(username);

                string status = GetStatus(username);

                int maxIntentos = getMaximusAttepmts();
                if (intentos == maxIntentos)
                {
                    if(status != "92")
                    BlockUser(username);
                    return false;
                }
                string passdb = GetPassword(username);
                string passUnencode = UnEncodePassword(passdb);
                //string passUnencode = passdb;
                if ((status != "91"))
                {
                    if (status == "92" && getAvailableBlockUser(username))
                    {
                        UnBlockUser(username);
                        UpdateNumAttemps(username, 0);
                        intentos = 0;
                    }
                    else
                        return false;
                }

                if (passUnencode != password)
                {
                    UpdateNumAttemps(username, intentos + 1);
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "ValidateUser");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw ex;
                }
            }
            UpdateNumAttemps(username, 0);
            return true;

        }

        public virtual void UpdateNumAttemps(string username, int intentos)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarNumIntentosUsuario]", username, intentos);
            oDataBase.ExecuteNonQuery(db);
        }


        public virtual string GetStatus(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirUsuario]", username);

            DataTable dt = oDataBase.ExecuteDataSet(db).Tables[0];
            string Status;
            Status = dt.Rows[0]["IdEstado"].ToString();
            return Status;
        }

        public virtual bool ExistUser(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirUsuario]", username);
            DataTable dt = oDataBase.ExecuteDataSet(db).Tables[0];

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;

        }

        //
        // CheckPassword
        //   Compares password values based on the MembershipPasswordFormat.
        //

        public virtual int getNumberAttempts(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirNumeroIntentosUsuario]", username);
            DataTable Int = oDataBase.ExecuteDataSet(db).Tables[0];
            int Intentos = Convert.ToInt32(Int.Rows[0][0]);
            return Intentos;
        }
        public virtual int getMaximusAttepmts()
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirNumeroIntentosMaximo]");
            DataTable Int = oDataBase.ExecuteDataSet(db).Tables[0];
            int Intentos = Convert.ToInt32(Int.Rows[0][0]);
            return Intentos;
        }
        public virtual bool getAvailableBlockUser(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarDisponibilidadUsuario]");
            DataTable Int = oDataBase.ExecuteDataSet(db).Tables[0];
            Boolean Intentos = Convert.ToBoolean(Int.Rows[0][0]);
            return Intentos;
        }
        public virtual void BlockUser(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarEstadoUsuario]", username, 92);
            oDataBase.ExecuteNonQuery(db);
        }
        public virtual void UnBlockUser(string username)
        {
            Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarEstadoUsuario]", username, 91);
            oDataBase.ExecuteNonQuery(db);
        }

        public bool CheckPassword(string username, string password)
        {
            return false;
        }

        //
        // EncodePassword
        //   Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
        //
        public string EncodePassword(string plaintext)
        {
            string encodedPassword = "";
            encodedPassword =
            Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(plaintext)));
            return encodedPassword;

        }


        //UnEncodePassword
        //  Decrypts or leaves the password clear based on the PasswordFormat.

        public string UnEncodePassword(string ciphertext)
        {
            string unencodedPassword = Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(ciphertext)));
            return unencodedPassword;
        }

        //
        // HexToByte
        //   Converts a hexadecimal string to a byte array. Used to convert encryption
        // key values from the configuration.
        //
        //private byte[] HexToByte(string hexString)
        //{
        //    byte[] returnBytes = new byte[hexString.Length / 2];
        //    for (int i = 0; i < returnBytes.Length; i++)
        //        returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
        //    return returnBytes;
        //}

        //
        // MembershipProvider.FindUsersByName
        // no implementada
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new ProviderException("opcion no implementada.");

        }

        //
        // MembershipProvider.FindUsersByEmail
        // no implementada
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new ProviderException("opcion no implementada.");
        }

        //
        // WriteToEventLog
        //   A helper function that writes exception detail to the event log. Exceptions
        // are written to the event log as a security measure to avoid private database
        // details from being returned to the browser. If a method does not return a status
        // or boolean indicating the action succeeded or failed, a generic exception is also 
        // thrown by the caller.
        //
        private void WriteToEventLog(Exception e, string action)
        {
            // throw new ProviderException("opcion no implementada.");
            EventLog log = new EventLog();
            log.Source = eventSource;
            log.Log = eventLog;

            string message = "Una excepcion ocurrio al comunicarse con la fuente de datos.\n\n";
            message += "Accion: " + action + "\n\n";
            message += "Excepcion: " + e.ToString();

            log.WriteEntry(message);
        }


        //public string SendMailForgotPass(string usrname,int idpreg,string nomresp)
        //{
        //    Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
        //    DbCommand db = oDataBase.GetStoredProcCommand("uspLeerMailUsuario", usrname,idpreg,nomresp);
        //    string mail = oDataBase.ExecuteScalar(db).ToString();
        //    return mail;
        //}
        public override string GetPassword(string username, string answer)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string SendMailForgotPass(string usrname, int idpreg, string nomresp)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
    }
}
