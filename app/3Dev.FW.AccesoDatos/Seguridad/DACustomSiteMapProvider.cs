using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using _3Dev.FW.Entidades.Seguridad;
using Microsoft.Practices.EnterpriseLibrary.Data;



namespace _3Dev.FW.AccesoDatos.Seguridad
{
    [Serializable]
    public class DACustomSiteMapProvider
    {
        string conexion = "Seguridad";
        public virtual List<BESiteMap> BuildSiteMap(int idSistema)
        {
            List<BESiteMap> SiteMapList = new List<BESiteMap>();
            Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirSiteMap]", idSistema);
            using (IDataReader objIDataReader =
            oDataBase.ExecuteReader(db))
            {
                while (objIDataReader.Read())
                {
                    SiteMapList.Add(new BESiteMap(objIDataReader));
                }
            }
            return SiteMapList;
        }

        public DataTable GetPagesValidMenu(int IdSistema)
        {
            throw new Exception("The method or operation is not implemented.");


        }

        public DataTable GetControlOptionsByPage(string PageName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public bool UpdateRoleInSiteMap(int IdSiteMap, int IdRol)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public int GetIdSistemaByIDSiteMapNode(int idSiteMapNode)
        {
            try
            {
                Database objDatabase = DatabaseFactory.CreateDatabase(conexion);
                int idsistema = Convert.ToInt32(objDatabase.ExecuteScalar("uspGetIdSistemaForNode", idSiteMapNode));
                return idsistema;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return -1;
        }
        public virtual List<BESiteMap> GetPagesLikNameForSistema(string Name, int idSistema)
        {
            try
            {
                //List<BESiteMap> SiteMapList = new List<BESiteMap>();
                //Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                //DbCommand db = oDataBase.GetStoredProcCommand("prc_ConsultarMenu", Name, idSistema);

                //using (IDataReader objIDataReader =
                //oDataBase.ExecuteReader(db))
                //{
                //    while (objIDataReader.Read())
                //    {
                //        SiteMapList.Add(new BESiteMap(objIDataReader));
                //    }
                //}
                //return SiteMapList;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        //public int GetIdSistemaByIDSiteMapNode(int idSiteMapNode)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}
        public virtual List<BESiteMap> GetSiteMapsByIdRol(int idRol)
        {
            return null;
        }
        public virtual string GetCabezeraMenu(string urlPagina)
        {
            return null;
        }
    }
}
