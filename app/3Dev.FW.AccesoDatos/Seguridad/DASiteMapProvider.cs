using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using _3Dev.FW.Entidades.Seguridad;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace _3Dev.FW.AccesoDatos.Seguridad
{
    [Serializable]
    public class DASiteMapProvider
    {
        // string rootBD = "PKG_SEGURIDAD";
        string conexion = "Seguridad";
        public List<BESiteMap> BuildSiteMap(int idSistema)
        {
            try
            {
                List<BESiteMap> SiteMapList = new List<BESiteMap>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("SP_Get_SiteMap", idSistema);

                using (IDataReader objIDataReader =
                oDataBase.ExecuteReader(db))
                {
                    while (objIDataReader.Read())
                    {
                        SiteMapList.Add(new BESiteMap(objIDataReader));
                    }
                }
                return SiteMapList;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public DataTable GetPagesValidMenu(int IdSistema)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("SP_Get_PaginasValidasMenu", IdSistema);
                //oDataBase.AddInParameter(db, "pIdSistema", DbType.Int32, IdSistema);
                //oDataBase.AddOutParameter(db, "salida", DbType.String, 256);
                DataTable dt = new DataTable();
                dt = oDataBase.ExecuteDataSet(db).Tables[0];

                return dt;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public DataTable GetControlOptionsByPage(string PageName)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("SP_Get_ControlOptions_By_Page", PageName);
                //oDataBase.AddInParameter(db, "pPageName", DbType.String, PageName);
                //oDataBase.AddOutParameter(db, "salida", DbType.String, 256);
                DataTable dt = new DataTable();
                dt = oDataBase.ExecuteDataSet(db).Tables[0];

                return dt;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public bool UpdateRoleInSiteMap(int IdSiteMap, int IdSistema, string Roles)
        {
            try
            {
                Database objDatabase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand oDBRolUpd = objDatabase.GetStoredProcCommand("SP_UpdateRoleInSiteMap");
                objDatabase.AddInParameter(oDBRolUpd, "IdSiteMap", DbType.Int32, IdSiteMap);
                objDatabase.AddInParameter(oDBRolUpd, "IdSistema", DbType.Int32, IdSistema);
                objDatabase.AddInParameter(oDBRolUpd, "Roles", DbType.String, Roles);
                if (objDatabase.ExecuteNonQuery(oDBRolUpd) > 0)
                    return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public int GetIdSistemaByIDSiteMapNode(int idSiteMapNode)
        {
            try
            {
                Database objDatabase = DatabaseFactory.CreateDatabase(conexion);
                int idsistema = Convert.ToInt32(objDatabase.ExecuteScalar("uspGetIdSistemaForNode", idSiteMapNode));
                return idsistema;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return -1;
        }

        public BESiteMap GetSiteMapById(int IdSiteMap)
        {
            try
            {
                BESiteMap eSiteMap = new BESiteMap();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("SP_SiteMap_Select_By_Id");
                oDataBase.AddInParameter(db, "IdSiteMap", DbType.Int32, IdSiteMap);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader(db))
                {
                    if (objIDataReader.Read())
                    {
                        eSiteMap = new BESiteMap(objIDataReader);
                    }
                }
                return eSiteMap;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BESiteMap> GetSiteMapWithURLBySistema(int idSistema)
        {
            try
            {
                List<BESiteMap> SiteMapList = new List<BESiteMap>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("sp_SiteMap_Select_By_Sistema", idSistema);

                using (IDataReader objIDataReader =
                oDataBase.ExecuteReader(db))
                {
                    while (objIDataReader.Read())
                    {
                        SiteMapList.Add(new BESiteMap(objIDataReader));
                    }
                }
                return SiteMapList;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public bool AddControlToSitemap(int SiteMapId, int ControlId)
        {
            try
            {
                bool retorno = false;
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                try
                {
                    oDataBase.ExecuteNonQuery("SP_ControlxSiteMap_Insert", SiteMapId, ControlId);
                    retorno = true;
                }
                catch { }
                return retorno;
            }

            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public bool RemoveControlToSitemap(int SiteMapId, int ControlId)
        {
            try
            {
                bool retorno = false;
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                try
                {
                    oDataBase.ExecuteNonQuery("SP_ControlxSiteMap_Delete", SiteMapId, ControlId);
                    retorno = true;
                }
                catch { }
                return retorno;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        private string GetRolesForSiteMapID(int SiteMapId)
        {
            try
            {
                string retorno = "";
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("SP_SiteMapxRol_Select_By_SiteMap", SiteMapId);

                using (IDataReader objIDataReader = oDataBase.ExecuteReader(db))
                {
                    if (objIDataReader.Read())
                    {
                        retorno = objIDataReader["IdRol"].ToString();
                        while (objIDataReader.Read())
                        {
                            retorno += "," + objIDataReader["IdRol"].ToString();
                        }
                    }
                }
                return retorno;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BESiteMap> GetMenus(int idSistema, int idSiteMap)
        {
            try
            {
                List<BESiteMap> SiteMapList = new List<BESiteMap>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("sp_SiteMap_Select_By_Sistema_NoURL", idSistema, idSiteMap);

                using (IDataReader objIDataReader =
                oDataBase.ExecuteReader(db))
                {
                    while (objIDataReader.Read())
                    {
                        SiteMapList.Add(new BESiteMap(objIDataReader));
                    }
                }
                return SiteMapList;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BESiteMap> GetSiteMapBySistemaId(int idSistema)
        {
            try
            {
                List<BESiteMap> SiteMapList = new List<BESiteMap>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand db = oDataBase.GetStoredProcCommand("sp_SiteMap_Select_All_By_Sistema", idSistema);

                using (IDataReader objIDataReader =
                oDataBase.ExecuteReader(db))
                {
                    while (objIDataReader.Read())
                    {
                        SiteMapList.Add(new BESiteMap(objIDataReader));
                    }
                }
                return SiteMapList;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public int AddSitemap(BESiteMap beSiteMap)
        {
            try
            {
                ////bool retorno = false;
                ////Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                ////try
                ////{
                ////    oDataBase.ExecuteNonQuery("SP_SiteMap_Insert",
                ////        beSiteMap.IdParent,
                ////        beSiteMap.Title,
                ////        beSiteMap.Url,
                ////        beSiteMap.Descripcion,
                ////        beSiteMap.Display,
                ////        beSiteMap.Estado,
                ////        beSiteMap.IdSistema);
                ////    retorno = true;
                ////}
                ////catch { }
                ////return retorno;
                //Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                //DbCommand db = oDataBase.GetStoredProcCommand("SP_SiteMap_Insert", beSiteMap.IdParent,
                //        beSiteMap.Title,
                //        beSiteMap.Url,
                //        beSiteMap.Descripcion,
                //        beSiteMap.Display,
                //        beSiteMap.Estado,
                //        beSiteMap.IdSistema,
                //        beSiteMap.Secuencia);

                //using (IDataReader objIDataReader =
                //oDataBase.ExecuteReader(db))
                //{
                //    while (objIDataReader.Read())
                //    {
                //        return int.Parse(objIDataReader[0].ToString());
                //    }
                //}
                //return 0;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return -1;
        }
        public bool UpdateSitemap(BESiteMap beSiteMap)
        {
            try
            {
                //bool retorno = false;
                //Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                //try
                //{
                //    oDataBase.ExecuteNonQuery("SP_SiteMap_Update",
                //        beSiteMap.Id,
                //        beSiteMap.IdParent,
                //        beSiteMap.Title,
                //        beSiteMap.Url,
                //        beSiteMap.Descripcion,
                //        beSiteMap.Display,
                //        beSiteMap.Estado,
                //        beSiteMap.Secuencia);
                //    retorno = true;
                //}

                //catch (Exception ex) { Console.WriteLine(ex.Message); }
                //return retorno;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public bool UpdateSitemapRoles(int SiteMapId)
        {
            try
            {
                bool retorno = false;
                string Roles = GetRolesForSiteMapID(SiteMapId);
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                try
                {
                    oDataBase.ExecuteNonQuery("SP_SiteMap_Update_Roles", SiteMapId, Roles);
                    retorno = true;
                }

                catch (Exception ex) { Console.WriteLine(ex.Message); }
                return retorno;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public BESiteMap GetSiteMapBySistemaURL(string URL, int SistemaId, int ParentId)
        {
            try
            {
                BESiteMap eSiteMap = new BESiteMap();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_SiteMap_Select_By_SistemaURLParent", SistemaId, URL, ParentId))
                {
                    if (objIDataReader.Read())
                    {
                        eSiteMap = new BESiteMap(objIDataReader);
                    }
                }
                return eSiteMap;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public BESiteMap GetSiteMapByTitleParent(string Title, int ParentId)
        {
            try
            {
                BESiteMap eSiteMap = new BESiteMap();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_SiteMap_Select_By_TitleParent", Title, ParentId))
                {
                    if (objIDataReader.Read())
                    {
                        eSiteMap = new BESiteMap(objIDataReader);
                    }
                }
                return eSiteMap;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }

    }


}
