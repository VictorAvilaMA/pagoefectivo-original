using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.Security;
using _3Dev.FW.Entidades.Seguridad;
using Microsoft.Practices.EnterpriseLibrary.Data;




namespace _3Dev.FW.AccesoDatos.Seguridad
{



    // Remove CAS from sample: [AspNetHostingPermission(SecurityAction.LinkDemand, Level=AspNetHostingPermissionLevel.Minimal)]
    // Remove CAS from sample: [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level=AspNetHostingPermissionLevel.Minimal)]
    [Serializable]
    public class DASqlRoleProvider : RoleProvider
    {
        //string rootBD = "SEGURIDAD";
        string conexion = "Seguridad";
        private static string _AppName;
        //private int _SchemaVersionCheck=0;
        private string _sqlConnectionString;
        private int _CommandTimeout = 0;
        private const string _AppNameCIF = "AFP";
        ////////////////////////////////////////////////////////////
        // Public properties
        private int CommandTimeout
        {
            get { return _CommandTimeout; }
        }


        public override void Initialize(string name, NameValueCollection config)
        {
            // Remove CAS from sample: HttpRuntime.CheckAspNetHostingPermission (AspNetHostingPermissionLevel.Low, SR.Feature_not_supported_at_this_level);
            if (config == null)
                throw new ArgumentNullException("config");

            if (String.IsNullOrEmpty(name))
                name = "SqlRoleProvider";
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", SR.GetString(SR.RoleSqlProvider_description));
            }
            base.Initialize(name, config);

            //_SchemaVersionCheck = 0;

            //_CommandTimeout = SecUtility.GetIntValue(config, "commandTimeout", 30, true, 0);

            string temp = config["connectionStringName"];
            if (temp == null || temp.Length < 1)
                throw new ProviderException(SR.GetString(SR.Connection_name_not_specified));
            _sqlConnectionString = SqlConnectionHelper.GetConnectionString(temp, true, true);
            if (_sqlConnectionString == null || _sqlConnectionString.Length < 1)
            {
                throw new ProviderException(SR.GetString(SR.Connection_string_not_found, temp));
            }

            _AppName = config["applicationName"];
            // _AppNameCIF = "MyApplication";
            if (string.IsNullOrEmpty(_AppNameCIF))
                _AppName = SecUtility.GetDefaultAppName();

            if (_AppName.Length > 256)
            {
                throw new ProviderException(SR.GetString(SR.Provider_application_name_too_long));
            }

            config.Remove("connectionStringName");
            config.Remove("applicationName");
            // config.Remove("commandTimeout");
            if (config.Count > 0)
            {
                string attribUnrecognized = config.GetKey(0);
                if (!String.IsNullOrEmpty(attribUnrecognized))
                    throw new ProviderException(SR.GetString(SR.Provider_unrecognized_attribute, attribUnrecognized));
            }
        }

        private void CheckSchemaVersion(SqlConnection connection)
        {
            string[] features = { "Role Manager" };
            //string version = "1";

            //SecUtility.CheckSchemaVersion(this,
            //                               connection,
            //                               features,
            //                               version,
            //                               ref _SchemaVersionCheck);
        }
        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////


        public override bool IsUserInRole(string username, string roleName)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand dbUserAddToRol = oDataBase.GetStoredProcCommand("sp_IsUserInRole");
                oDataBase.AddInParameter(dbUserAddToRol, "UserName", DbType.String, username);
                oDataBase.AddInParameter(dbUserAddToRol, "RoleName", DbType.String, roleName);
                oDataBase.AddInParameter(dbUserAddToRol, "ApplicationName", DbType.String, _AppNameCIF);
                int users = int.Parse(oDataBase.ExecuteScalar(dbUserAddToRol).ToString());
                if (users > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }


        public override string[] GetRolesForUser(string username)
        {
            //  throw new Exception("The method or operation is not implemented.");
            //CustomMembershipProvider oCus = new CustomMembershipProvider();
            StringCollection RolesForUser = new StringCollection();
            Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
            DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirRolesPorUsuarios]", username);

            using (IDataReader oIDR =
             oDataBase.ExecuteReader(db))
            {
                while (oIDR.Read())
                {
                    RolesForUser.Add(oIDR.GetString(oIDR.GetOrdinal("DESCRI_ROL")));

                }
            }
            String[] strReturn = new String[RolesForUser.Count];
            RolesForUser.CopyTo(strReturn, 0);

            return strReturn;
        }
        protected  String[] ListaRolesAStrings(StringCollection strcol)
        {
            String[] strReturn = new String[strcol.Count];
             strcol.CopyTo(strReturn, 0);
             return strReturn;
        }

        //////////////////////////////////////////////////////////////////////

        public override void CreateRole(string roleName)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand dbRolIns = oDataBase.GetStoredProcCommand("SP_Rol_Create");
                oDataBase.AddInParameter(dbRolIns, "RoleName", DbType.String, roleName);
                oDataBase.ExecuteNonQuery(dbRolIns);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
    

        //public override bool RoleExists(string roleName)
        //{
        //    try
        //    {
        //        String result;
        //        Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
        //        List<BESistema> Sistemas = new List<BESistema>();
        //        DbCommand db = oDataBase.GetStoredProcCommand("sp_RolExists");

        //        result = oDataBase.ExecuteScalar(db).ToString();
        //        if (result != string.Empty)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
                
        //    }
        //    catch (Exception ex) { Console.WriteLine(ex.Message);}
        //    return false;
        //}

     
        //public override string ApplicationName
        //{
        //    get { return _AppName; }
        //    set
        //    {
        //        _AppName = value;

        //    }
        //}



                        
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            try
            {
    
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
     
        
        public override bool RoleExists(string roleName)
        {
            try
            {
                DataSet ds = new DataSet();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                //OracleDatabase oDataBase = (OracleDatabase)DatabaseFactory.CreateDatabase("ConexionOracle");
                List<BESistema> Sistemas = new List<BESistema>();
                DbCommand db = oDataBase.GetStoredProcCommand("PKG_SEGURIDAD.sp_RolExists");
                oDataBase.AddOutParameter(db, "salida", DbType.String, 256);
                oDataBase.AddInParameter(db, "pRolName", DbType.String, roleName);
                ds = oDataBase.ExecuteDataSet(db);
                if (ds.Tables[0].Rows.Count > 0) return true;
                else
                    return false;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                SecUtility.CheckArrayParameter(ref roleNames, true, true, true, 256, "roleNames");
                SecUtility.CheckArrayParameter(ref usernames, true, true, true, 256, "usernames");

                bool beginTranCalled = false;

                SqlConnectionHolder holder = null;
                try
                {
                    holder = SqlConnectionHelper.GetConnection(_sqlConnectionString, true);
                    CheckSchemaVersion(holder.Connection);
                    int numUsersRemaing = usernames.Length;
                    while (numUsersRemaing > 0)
                    {
                        int iter;
                        string allUsers = usernames[usernames.Length - numUsersRemaing];
                        numUsersRemaing--;
                        for (iter = usernames.Length - numUsersRemaing; iter < usernames.Length; iter++)
                        {
                            if (allUsers.Length + usernames[iter].Length + 1 >= 4000)
                                break;
                            allUsers += "," + usernames[iter];
                            numUsersRemaing--;
                        }

                        int numRolesRemaining = roleNames.Length;
                        while (numRolesRemaining > 0)
                        {
                            string allRoles = roleNames[roleNames.Length - numRolesRemaining];
                            numRolesRemaining--;
                            for (iter = roleNames.Length - numRolesRemaining; iter < roleNames.Length; iter++)
                            {
                                if (allRoles.Length + roleNames[iter].Length + 1 >= 4000)
                                    break;
                                allRoles += "," + roleNames[iter];
                                numRolesRemaining--;
                            }
                            //
                            // Note:  ADO.NET 2.0 introduced the TransactionScope class - in your own code you should use TransactionScope
                            //            rather than explicitly managing transactions with the TSQL BEGIN/COMMIT/ROLLBACK statements.
                            //
                            if (!beginTranCalled && (numUsersRemaing > 0 || numRolesRemaining > 0))
                            {
                                (new SqlCommand("BEGIN TRANSACTION", holder.Connection)).ExecuteNonQuery();
                                beginTranCalled = true;
                            }
                            AddUsersToRolesCore(holder.Connection, allUsers, allRoles);
                        }
                    }
                    if (beginTranCalled)
                    {
                        (new SqlCommand("COMMIT TRANSACTION", holder.Connection)).ExecuteNonQuery();
                        beginTranCalled = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    if (beginTranCalled)
                    {
                        try
                        {
                            (new SqlCommand("ROLLBACK TRANSACTION", holder.Connection)).ExecuteNonQuery();
                        }
                        catch
                        {
                        }
                        beginTranCalled = false;
                    }
                }
                finally
                {
                    if (holder != null)
                    {
                        holder.Close();
                        holder = null;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        private void AddUsersToRolesCore(SqlConnection conn, string usernames, string roleNames)
        {
            SqlCommand cmd = new SqlCommand("sp_AddUsersToRoles", conn);
            SqlDataReader reader = null;
            string s1 = String.Empty, s2 = String.Empty;
            try
            {
                SqlParameter p = new SqlParameter("@ReturnValue", SqlDbType.Int);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = CommandTimeout;
                p.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(p);
                cmd.Parameters.Add(CreateInputParam("@ApplicationName", SqlDbType.NVarChar, ApplicationName));
                cmd.Parameters.Add(CreateInputParam("@RoleNames", SqlDbType.NVarChar, roleNames));
                cmd.Parameters.Add(CreateInputParam("@UserNames", SqlDbType.NVarChar, usernames));
                cmd.Parameters.Add(CreateInputParam("@CurrentTimeUtc", SqlDbType.DateTime, DateTime.UtcNow));
                reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    if (reader.FieldCount > 0)
                        s1 = reader.GetString(0);
                    if (reader.FieldCount > 1)
                        s2 = reader.GetString(1);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            try
            {
                switch (GetReturnValue(cmd))
                {
                    case 0:
                        return;
                    case 1:
                        throw new ProviderException(SR.GetString(SR.Provider_this_user_not_found, s1));
                    case 2:
                        throw new ProviderException(SR.GetString(SR.Provider_role_not_found, s1));
                    case 3:
                        throw new ProviderException(SR.GetString(SR.Provider_this_user_already_in_role, s1, s2));
                }
                throw new ProviderException(SR.GetString(SR.Provider_unknown_failure));
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                SecUtility.CheckArrayParameter(ref roleNames, true, true, true, 256, "roleNames");
                SecUtility.CheckArrayParameter(ref usernames, true, true, true, 256, "usernames");

                bool beginTranCalled = false;
                SqlConnectionHolder holder = null;
                try
                {
                    holder = SqlConnectionHelper.GetConnection(_sqlConnectionString, true);
                    CheckSchemaVersion(holder.Connection);
                    int numUsersRemaing = usernames.Length;
                    while (numUsersRemaing > 0)
                    {
                        int iter;
                        string allUsers = usernames[usernames.Length - numUsersRemaing];
                        numUsersRemaing--;
                        for (iter = usernames.Length - numUsersRemaing; iter < usernames.Length; iter++)
                        {
                            if (allUsers.Length + usernames[iter].Length + 1 >= 4000)
                                break;
                            allUsers += "," + usernames[iter];
                            numUsersRemaing--;
                        }

                        int numRolesRemaining = roleNames.Length;
                        while (numRolesRemaining > 0)
                        {
                            string allRoles = roleNames[roleNames.Length - numRolesRemaining];
                            numRolesRemaining--;
                            for (iter = roleNames.Length - numRolesRemaining; iter < roleNames.Length; iter++)
                            {
                                if (allRoles.Length + roleNames[iter].Length + 1 >= 4000)
                                    break;
                                allRoles += "," + roleNames[iter];
                                numRolesRemaining--;
                            }
                            //
                            // Note:  ADO.NET 2.0 introduced the TransactionScope class - in your own code you should use TransactionScope
                            //            rather than explicitly managing transactions with the TSQL BEGIN/COMMIT/ROLLBACK statements.
                            //
                            if (!beginTranCalled && (numUsersRemaing > 0 || numRolesRemaining > 0))
                            {
                                (new SqlCommand("BEGIN TRANSACTION", holder.Connection)).ExecuteNonQuery();
                                beginTranCalled = true;
                            }
                            RemoveUsersFromRolesCore(holder.Connection, allUsers, allRoles);
                        }
                    }
                    if (beginTranCalled)
                    {
                        (new SqlCommand("COMMIT TRANSACTION", holder.Connection)).ExecuteNonQuery();
                        beginTranCalled = false;
                    }
                }
                catch (Exception ex)
                {
                    if (beginTranCalled)
                    {
                        (new SqlCommand("ROLLBACK TRANSACTION", holder.Connection)).ExecuteNonQuery();
                        beginTranCalled = false;
                    }
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (holder != null)
                    {
                        holder.Close();
                        holder = null;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        private void RemoveUsersFromRolesCore(SqlConnection conn, string usernames, string roleNames)
        {
            SqlCommand cmd = new SqlCommand("sp_RemoveUsersFromRoles", conn);
            SqlDataReader reader = null;
            SqlParameter p = new SqlParameter("@ReturnValue", SqlDbType.Int);
            string s1 = String.Empty, s2 = String.Empty;

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = CommandTimeout;

                p.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(p);
                cmd.Parameters.Add(CreateInputParam("@ApplicationName", SqlDbType.NVarChar, ApplicationName));
                cmd.Parameters.Add(CreateInputParam("@UserNames", SqlDbType.NVarChar, usernames));
                cmd.Parameters.Add(CreateInputParam("@RoleNames", SqlDbType.NVarChar, roleNames));

                reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    if (reader.FieldCount > 0)
                        s1 = reader.GetString(0);
                    if (reader.FieldCount > 1)
                        s2 = reader.GetString(1);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            try
            {
                switch (GetReturnValue(cmd))
                {
                    case 0:
                        return;
                    case 1:
                        throw new ProviderException(SR.GetString(SR.Provider_this_user_not_found, s1));
                    case 2:
                        throw new ProviderException(SR.GetString(SR.Provider_role_not_found, s2));
                    case 3:
                        throw new ProviderException(SR.GetString(SR.Provider_this_user_already_not_in_role, s1, s2));
                }
                throw new ProviderException(SR.GetString(SR.Provider_unknown_failure));
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        public override string[] GetUsersInRole(string roleName)
        {
            try
            {
                StringCollection UserRoles = new StringCollection();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader =
                 oDataBase.ExecuteReader("sp_GetUsersInRole", roleName, _AppNameCIF))
                {
                    while (objIDataReader.Read())
                    {
                        UserRoles.Add(objIDataReader.GetString(0));

                    }
                }
                string[] strReturn = new String[UserRoles.Count];
                UserRoles.CopyTo(strReturn, 0);
                return strReturn;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public override string[] GetAllRoles()
        {
            try
            {
                StringCollection Roles = new StringCollection();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader =
                 oDataBase.ExecuteReader("sp_GetAllRoles", _AppNameCIF))
                {
                    while (objIDataReader.Read())
                    {
                        Roles.Add(objIDataReader.GetString(0));

                    }
                }
                string[] strReturn = new String[Roles.Count];
                Roles.CopyTo(strReturn, 0);
                return strReturn;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public virtual List<BERol> GetAllRolesList()
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                //OracleDatabase oDataBase = (OracleDatabase)DatabaseFactory.CreateDatabase(conexion);
                //DbCommand db = oDataBase.GetStoredProcCommand(rootBD + ".sp_GetAllRoles");
                DbCommand db = oDataBase.GetStoredProcCommand("SP_Get_Roles_All");
                oDataBase.AddOutParameter(db, "salida", DbType.String, 256);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Get_Roles_All"))
                {
                    while (objIDataReader.Read())
                    {
                        Rol.Add(new BERol(objIDataReader));

                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BERol> GetAllRolesList(string username)
        {
            try
            {
                List<BERol> Roles = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader =
                 oDataBase.ExecuteReader("sp_GetAllRolesListForUser", username, _AppNameCIF))
                {
                    while (objIDataReader.Read())
                    {
                        BERol Rol = new BERol(objIDataReader);
                        Roles.Add(Rol);
                    }
                }
                return Roles;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            try
            {
                SecUtility.CheckParameter(ref roleName, true, true, true, 256, "roleName");
                SecUtility.CheckParameter(ref usernameToMatch, true, true, false, 256, "usernameToMatch");

                SqlConnectionHolder holder = null;

                try
                {
                    holder = SqlConnectionHelper.GetConnection(_sqlConnectionString, true);
                    CheckSchemaVersion(holder.Connection);

                    SqlCommand cmd = new SqlCommand("sp_FindUsersInRole", holder.Connection);
                    SqlDataReader reader = null;
                    SqlParameter p = new SqlParameter("@ReturnValue", SqlDbType.Int);
                    StringCollection sc = new StringCollection();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = CommandTimeout;

                    p.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(p);
                    cmd.Parameters.Add(CreateInputParam("@ApplicationName", SqlDbType.NVarChar, ApplicationName));
                    cmd.Parameters.Add(CreateInputParam("@RoleName", SqlDbType.NVarChar, roleName));
                    cmd.Parameters.Add(CreateInputParam("@UserNameToMatch", SqlDbType.NVarChar, usernameToMatch));
                    try
                    {
                        reader = cmd.ExecuteReader(CommandBehavior.SequentialAccess);
                        while (reader.Read())
                            sc.Add(reader.GetString(0));
                    }

                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                    try
                    {
                        if (sc.Count < 1)
                        {
                            switch (GetReturnValue(cmd))
                            {
                                case 0:
                                    return new string[0];

                                case 1:
                                    throw new ProviderException(SR.GetString(SR.Provider_role_not_found, roleName));

                                default:
                                    throw new ProviderException(SR.GetString(SR.Provider_unknown_failure));
                            }
                        }

                        string[] strReturn = new String[sc.Count];
                        sc.CopyTo(strReturn, 0);
                        return strReturn;
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }
                finally
                {
                    if (holder != null)
                    {
                        holder.Close();
                        holder = null;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public override string ApplicationName
        {
            get
            {
                return _AppName;
            }
            set
            {
                _AppName = value;
                if (_AppName.Length > 256)
                {
                    throw new ProviderException(SR.GetString(SR.Provider_application_name_too_long));
                }
            }
        }
        private SqlParameter CreateInputParam(string paramName, SqlDbType dbType, object objValue)
        {
            try
            {
                SqlParameter param = new SqlParameter(paramName, dbType);
                if (objValue == null)
                    objValue = String.Empty;
                param.Value = objValue;
                return param;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        private  int GetReturnValue(SqlCommand cmd)
        {
            try
            {
                foreach (SqlParameter param in cmd.Parameters)
                {
                    if (param.Direction == ParameterDirection.ReturnValue && param.Value != null && param.Value is int)
                        return (int)param.Value;
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return -1;
        }
        public virtual bool UpdateRoleName(int idRol, string RoleName, string codigo)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                int i = oDataBase.ExecuteNonQuery("SP_Rol_Update", idRol, RoleName);
                return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public virtual bool AddUserToRol(string UserName, string RoleName)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand dbUserAddToRol = oDataBase.GetStoredProcCommand("sp_AddUserToRol");
                oDataBase.AddInParameter(dbUserAddToRol, "UserName", DbType.String, UserName);
                oDataBase.AddInParameter(dbUserAddToRol, "RoleName", DbType.String, RoleName);
                oDataBase.AddInParameter(dbUserAddToRol, "ApplicationName", DbType.String, _AppNameCIF);
                if (oDataBase.ExecuteNonQuery(dbUserAddToRol) > 0)
                    return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public virtual bool RemoveUserFromRol(int UserId, int RolId)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand dbUserDel = oDataBase.GetStoredProcCommand("sp_Usuarioxrol_Delete");
                oDataBase.AddInParameter(dbUserDel, "UserId", DbType.String, UserId);
                oDataBase.AddInParameter(dbUserDel, "RolId", DbType.String, RolId);
                if (oDataBase.ExecuteNonQuery(dbUserDel) > 0)
                    return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public virtual bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand dbUserUpd = oDataBase.GetStoredProcCommand("sp_UpdateUserName");
                oDataBase.AddInParameter(dbUserUpd, "NewUserName", DbType.String, NewUserName);
                oDataBase.AddInParameter(dbUserUpd, "OldUserName", DbType.String, OldUserName);
                oDataBase.AddInParameter(dbUserUpd, "RoleName", DbType.String, RoleName);
                oDataBase.AddInParameter(dbUserUpd, "ApplicationName", DbType.String, _AppNameCIF);
                if (oDataBase.ExecuteNonQuery(dbUserUpd) > 0)
                    return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public virtual List<BERol> GetRolbyId(int id)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                //OracleDatabase oDataBase = (OracleDatabase)DatabaseFactory.CreateDatabase("ConexionOracle");
                List<BERol> rol = new List<BERol>();
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_By_Id", id))
                {
                    while (objIDataReader.Read())
                    {
                        rol.Add(new BERol(objIDataReader));
                    }
                }
                return rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BESistema> GetSistemasHabliesPorRol(int idRol)
        {
            try
            {
                List<BESistema> Sistemas = new List<BESistema>();
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                using (IDataReader objIDataReader =
                 oDataBase.ExecuteReader("SP_Sistemas_Select_All_For_Rol", idRol))
                {
                    while (objIDataReader.Read())
                    {
                        BESistema Sistema = new BESistema(objIDataReader);
                        Sistema.Pertenece = int.Parse(objIDataReader["Rol"].ToString());
                        Sistemas.Add(Sistema);
                    }
                }
                return Sistemas;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public virtual int GetRolIdByName(string Name)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Id_by_Name", Name))
                {
                    if (objIDataReader.Read())
                    {
                        return int.Parse(objIDataReader["IdRol"].ToString());
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return -1;
        }
        public virtual List<BERol> GetRolLikeName(string Name)
        {
            try
            {
                //List<BERol> Rol = new List<BERol>();
                //Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                //using (IDataReader objIDataReader = oDataBase.ExecuteReader("[PagoEfectivo].[prcRolForSistema]", Name))
                //{
                //    while (objIDataReader.Read())
                //    {
                //        Rol.Add(new BERol(objIDataReader));
                //    }
                //}
                //return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public virtual bool AddUserToRol(int UserId, int RolId)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                oDataBase.ExecuteNonQuery("SP_UsuarioxRol_Insert", UserId, RolId);
                return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public bool AddSistemaToRol(int SistemaId, int RolId)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_SistemaxRol_Insert", SistemaId, RolId))
                {
                    while (objIDataReader.Read())
                    {
                        new DASiteMapProvider().UpdateSitemapRoles(int.Parse(objIDataReader[0].ToString()));
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public bool RemoveSistemaFromRol(int SistemaId, int RolId)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("sp_Sistemaxrol_Delete", SistemaId, RolId))
                {
                    if (objIDataReader.Read())
                    {
                        new DASiteMapProvider().UpdateSitemapRoles(int.Parse(objIDataReader[0].ToString()));
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public List<BERol> GetRolLikeNameForSistema(string Name, int SistemaId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_Like_Name_For_Sistema", Name, SistemaId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        rol.Pertenece = int.Parse(objIDataReader["pertenece"].ToString());
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BERol> GetRolLikeNameForSistemaRelated(string Name, int SistemaId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_Like_Name_For_Sistema_Related", Name, SistemaId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        rol.Pertenece = int.Parse(objIDataReader["pertenece"].ToString());
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public virtual List<BERol> GetRolBySistema(int SistemaId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_By_Sistema", SistemaId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public virtual  List<BERol> GetRolForUserId(int UserId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_For_UserId", UserId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public virtual bool AddSiteMapToRol(int SiteMapId, int RolId)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");

                oDataBase.ExecuteNonQuery("SP_SiteMapxRol_Insert", SiteMapId, RolId);
                if (new DASiteMapProvider().UpdateSitemapRoles(SiteMapId))
                    return true;
                else
                {
                    RemoveSiteMapFromRol(SiteMapId, RolId);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public virtual bool RemoveSiteMapFromRol(int SiteMapId, int RolId)
        {
            try
            {
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                DbCommand dbSistemaDel = oDataBase.GetStoredProcCommand("sp_SiteMapxrol_Delete");
                oDataBase.AddInParameter(dbSistemaDel, "IdSitemap", DbType.String, SiteMapId);
                oDataBase.AddInParameter(dbSistemaDel, "IdRol", DbType.String, RolId);
                if (oDataBase.ExecuteNonQuery(dbSistemaDel) > 0)
                {
                    if (new DASiteMapProvider().UpdateSitemapRoles(SiteMapId))
                        return true;
                    else
                    {
                        AddSiteMapToRol(SiteMapId, RolId);
                        return false;
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return false;
        }
        public virtual List<BERol> GetRolLikeNameForSistemaSiteMap(string Name, int SistemaId, int SiteMapId)
        {
            try
            {
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BERol> GetRolLikeNameForSistemaSiteMapRelated(string Name, int SistemaId, int SiteMapId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_Like_Name_For_SistemaSiteMap_Related", Name, SistemaId, SiteMapId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        rol.Pertenece = int.Parse(objIDataReader["pertenece"].ToString());
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BERol> GetRolLikeNameForSiteMap(string Name, int SiteMapId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_Like_Name_For_SiteMap", Name, SiteMapId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        rol.Pertenece = int.Parse(objIDataReader["pertenece"].ToString());
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        public List<BERol> GetRolForSiteMap(int SiteMapId)
        {
            try
            {
                List<BERol> Rol = new List<BERol>();
                Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
                using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Rol_Select_Like_Name_For_SiteMap", Name, SiteMapId))
                {
                    while (objIDataReader.Read())
                    {
                        BERol rol = new BERol(objIDataReader);
                        rol.Pertenece = int.Parse(objIDataReader["pertenece"].ToString());
                        Rol.Add(rol);
                    }
                }
                return Rol;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }












    }
}