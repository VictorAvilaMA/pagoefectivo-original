using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
//
using _3Dev.FW.Entidades;
using _3Dev.FW.Entidades.Seguridad;
//using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;



namespace _3Dev.FW.AccesoDatos.Seguridad
{
    [Serializable]
    public class DAUsuario : IDisposable
    {

        public virtual int RegistrarUsuario(BEUsuario eUsuario)
        {
            return -1;
        }
        public virtual int ActualizarDatos(BEUsuario eUsuario)
        {
            return -1;
        }
        public virtual List<BEPregunta> GetPreguntas()
        {
            return null;

        }
        public virtual int EliminarUsuario(int idUsuario)
        {
            return -1;
        }
        public virtual int ModificarUsuario(BEUsuario eUsuario)
        {
            return -1;
        }
        public virtual List<BEUsuario> GetAllUsuarioList()
        {
            return null;
        }
        public virtual List<BEUsuario> GetUsuariosByFiltros(BEUsuario eUsuario)
        {
            return null;
        }
        public virtual int IsUsuarioInRol(int idUsuario)
        {
            return -1;
        }
        public virtual List<BERolesByUsuario> GetRolesByUsuario(int idUsuario)
        {
            return null;
        }
        public virtual List<BERol> GetRolesBySistema(int idSistema)
        {
            return null;
        }

        public virtual BEUsuario GetUsuarioById(int idUsuario)
        {
            return null;
        }
        public virtual BEUsuario SeleccionarUsuarioLista(List<BEUsuario> UsuarioList)
        {
            return null;
        }
        public virtual int AddUserToRol(int idUsuario, int idRol)
        {
            return -1;
        }
        public virtual int RemoveRolToUser(int id)
        {
            return -1;
        }

        public virtual List<BERol> GetRoles(Int32 userId, int idSistema)
        {
            return null;
        }
        public virtual  BEUserInfo GetUserInfoByUserName(string userName)
        {
            BEUserInfo eUsuario = null;
            try
            {
               
                Database oDataBase = DatabaseFactory.CreateDatabase("Seguridad");
                DbCommand db = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirUsuario]");
                oDataBase.AddInParameter(db, "email", DbType.String , userName );
                using (IDataReader objIDataReader =
                 oDataBase.ExecuteReader(db))
                {
                    while (objIDataReader.Read())
                    {
                        eUsuario = new BEUserInfo(objIDataReader);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return eUsuario;
        }

        public virtual  bool ConfirmUserValidation(string email, string guid)
        {

            try
            {
                Database objDatabase = DatabaseFactory.CreateDatabase("Seguridad");
                objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ConfirmarValidacionDeUsuario",email,guid);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
              
            }

        }

        public virtual List<BEUsuario> GetUsuarioLikeNameForRol(string Name, int RolId)
        {
            //try
            //{
            //    List<BEUsuario> Usuario = new List<BEUsuario>();
            //    Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
            //    using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Usuario_Select_Like_Name_For_Rol", Name, RolId))
            //    {
            //        while (objIDataReader.Read())
            //        {
            //            BEUsuario us = new BEUsuario(objIDataReader);
            //            us.Pertenece = int.Parse(objIDataReader["PERTENECE"].ToString());
            //            Usuario.Add(us);
            //        }
            //    }
            //    return Usuario;
            //}
            //catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }
        
        //MODIFICADO PARA PAGINACION FASE III
        public virtual List<BEUsuario> GetUsuarioLikeName(string Name, BusinessEntityBase entidad = null)
        {
            //try
            //{
            //    List<BEUsuario> Usuario = new List<BEUsuario>();
            //    Database oDataBase = DatabaseFactory.CreateDatabase(conexion);
            //    using (IDataReader objIDataReader = oDataBase.ExecuteReader("SP_Usuario_Select_Like_UserName", Name))
            //    {
            //        while (objIDataReader.Read())
            //        {
            //            Usuario.Add(new BEUsuario(objIDataReader));
            //        }
            //    }
            //    return Usuario;
            //}
            //catch (Exception ex) { Console.WriteLine(ex.Message); }
            return null;
        }

        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
    
    
}
