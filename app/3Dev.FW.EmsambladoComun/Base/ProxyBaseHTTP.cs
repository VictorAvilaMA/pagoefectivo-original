﻿//using System;
//using System.Linq;
//using System.Runtime.Serialization;
//using System.ServiceModel;
//using System.ServiceModel.Description;
//using System.ServiceModel.Discovery;

//namespace _3Dev.FW.EmsambladoComun
//{
//    public class ProxyBase<T> : IDisposable
//    {
//        public ChannelFactory<T> Conexion { get; set; }

//        public static DataContractResolver OdcResolver { get; set; }


//        public T DevolverContrato(DataContractResolver oDcResolver = null)
//        {

//            DiscoveryClient discovery = new DiscoveryClient(new UdpDiscoveryEndpoint());
//            FindCriteria criterio = new FindCriteria(typeof(T));
//            criterio.Duration = TimeSpan.FromSeconds(1);
//            try
//            {
//                FindResponse findresponse = discovery.Find(criterio);
//                Int32 port = 666;


//                var direccion = findresponse.Endpoints.First(p => p.Address.Uri.Scheme == "http").Address;
//                var bindTcp = new BasicHttpBinding();
//                bindTcp.OpenTimeout = new TimeSpan(1, 1, 20);
//                bindTcp.CloseTimeout = new TimeSpan(1, 1, 20);
//                bindTcp.SendTimeout = new TimeSpan(1, 1, 20);
//                //bindTcp.TransactionFlow = false;
//                bindTcp.TransferMode = TransferMode.Buffered;
//                //bindTcp.TransactionProtocol = TransactionProtocol.OleTransactions;
//                bindTcp.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
//                //bindTcp.ListenBacklog = 10;
//                bindTcp.MaxBufferPoolSize = 2147483646;
//                bindTcp.MaxBufferSize = 2147483646;
//                bindTcp.MaxReceivedMessageSize = 2147483646;
//                //bindTcp.MaxConnections = 10;
//                bindTcp.ReaderQuotas.MaxDepth = 32;
//                bindTcp.ReaderQuotas.MaxStringContentLength = 2147483647;
//                bindTcp.ReaderQuotas.MaxArrayLength = 2147483647;
//                bindTcp.ReaderQuotas.MaxBytesPerRead = 2147483647;
//                bindTcp.ReaderQuotas.MaxNameTableCharCount = 2147483647;// 16384;
//                //bindTcp.ReliableSession.Ordered = true;
//                //bindTcp.ReliableSession.InactivityTimeout = new TimeSpan(5, 59, 0);


//                Conexion = new ChannelFactory<T>(bindTcp, direccion);

//                if (oDcResolver != null)
//                    foreach (var operation in Conexion.Endpoint.Contract.Operations)
//                        operation.Behaviors.Find<DataContractSerializerOperationBehavior>().DataContractResolver = oDcResolver;

//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }

//            return Conexion.CreateChannel();
//        }

//        //Sub Dispose()
//        //    Conexion.Close()
//        //End Sub

//        public void Dispose()
//        {
//            Conexion.Close();
//        }

//    }
//}
