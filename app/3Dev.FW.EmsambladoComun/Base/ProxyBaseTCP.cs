﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using System.Configuration;
using _3Dev.FW.EmsambladoComun;
using System.ServiceModel.Channels;

namespace _3Dev.FW.EmsambladoComun
{
    public enum TiposServicios
    {
        Web,
        Portales
    }

    public class ProxyBase<T> : IDisposable
    {

        public ProxyBase(TiposServicios tipo = TiposServicios.Web)
        {
            switch (tipo)
            {
                case TiposServicios.Web: UrlServicio = ConfigurationManager.AppSettings["urlServiceBase"]; break;
                case TiposServicios.Portales: UrlServicio = ConfigurationManager.AppSettings["urlServicePortalBase"]; break;
            }
        }

        public string UrlServicio { get; set; }

        public ChannelFactory<T> Conexion { get; set; }

        public static DataContractResolver OdcResolver { get; set; }

        public T DevolverContrato(DataContractResolver oDcResolver = null)
        {
            try
            {
                Binding bind;
                switch (ConfigurationManager.AppSettings["bindingType"])
                {
                    case "netTcp":
                        bind = getNetTcpBinding();
                        break;
                    case "basicHttp":
                        bind = getBasicHttpBinding();
                        break;
                    default:
                        bind = getNetTcpBinding();
                        break;
                }


                Conexion = new ChannelFactory<T>(bind, GetUrlBase(typeof(T)));

                if (oDcResolver != null)
                    foreach (var operation in Conexion.Endpoint.Contract.Operations)
                    {
                        var dataContractBehavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
                        dataContractBehavior.DataContractResolver = oDcResolver;

                        if (dataContractBehavior != null)
                        {
                            dataContractBehavior.MaxItemsInObjectGraph = int.MaxValue;
                        }
                    }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Conexion.CreateChannel();
        }

        private string GetUrlBase(Type TypeInterface)
        {
            string result = null;
            result = UrlServicio;//ConfigurationManager.AppSettings["urlServiceBase"];
            if (TypeInterface.Name == "IEmail")
                result += "SPEEmail";
            else if (TypeInterface.Name == "IAgenciaBancaria")
                result += "AgenciaBancariaServer";
            else if (TypeInterface.Name == "IAgenciaRecaudadora")
                result += "AgenciaRecaudadoraServer";
            else if (TypeInterface.Name == "IBanco")
                result += "BancoServer";
            else if (TypeInterface.Name == "ICaja")
                result += "CajaServer";
            else if (TypeInterface.Name == "ICuenta")
                result += "CuentaServer";
            else if (TypeInterface.Name == "ICliente")
                result += "ClienteServer";
            else if (TypeInterface.Name == "IComercio")
                result += "ComercioServer";
            else if (TypeInterface.Name == "IComun")
                result += "ComunServer";
            else if (TypeInterface.Name == "IConciliacion")
                result += "ConciliacionServer";
            else if (TypeInterface.Name == "IFTPArchivo")
                result += "FTPArchivoServer";
            else if (TypeInterface.Name == "IEmpresaContratante")
                result += "EmpresaContratanteServer";
            else if (TypeInterface.Name == "IEstablecimiento")
                result += "EstablecimientoServer";
            else if (TypeInterface.Name == "IJefeProducto")
                result += "JefeProductoServer";
            else if (TypeInterface.Name == "IPasarela")
                result += "PasarelaServer";
            else if (TypeInterface.Name == "IPlantilla")
                result += "PlantillaServer";
            else if (TypeInterface.Name == "IPuntoVenta")
                result += "PuntoVentaServer";
            else if (TypeInterface.Name == "IServicioNotificacion")
                result += "ServicioNotificacionServer";
            else if (TypeInterface.Name == "IOperador")
                result += "OperadorServer";
            else if (TypeInterface.Name == "IOrdenPago")
                result += "OrdenPagoServer";
            else if (TypeInterface.Name == "IParametro")
                result += "ParametroServer";
            else if (TypeInterface.Name == "IRepresentante")
                result += "RepresentanteServer";
            else if (TypeInterface.Name == "IServicioComun")
                result += "ServicioComunServer";
            else if (TypeInterface.Name == "IServicio")
                result += "ServicioServer";
            else if (TypeInterface.Name == "IUbigeo")
                result += "UbigeoServer";
            else if (TypeInterface.Name == "ISeguridad")
                result += "SeguridadServer";
            else if (TypeInterface.Name == "IMemberShipProvider")
                result += "SPEMemberShipServer";
            else if (TypeInterface.Name == "IRoleProvider")
                result += "SPERoleProviderServer";
            else if (TypeInterface.Name == "ISiteMapProvider")
                result += "SPESiteMapServer";
            else if (TypeInterface.Name == "IUSuario")
                result += "SPEUsuarioServer";
            else if (TypeInterface.Name == "IDineroVirtual")
                result += "DineroVirtualServer";
            else if (TypeInterface.Name == "IServicioInstitucion")
                result += "ServicioInstitucionServer";
            else if (TypeInterface.Name == "IServicioMunicipalidadBarranco")
                result += "ServicioMunicipalidadBarrancoServer";

            result += ConfigurationManager.AppSettings["bindingType"] == "basicHttp" ? ".svc" : "";
            return result;
        }

        private NetTcpBinding getNetTcpBinding()
        {
            NetTcpBinding bind = new NetTcpBinding();
            bind.OpenTimeout = new TimeSpan(1, 1, 20);
            bind.CloseTimeout = new TimeSpan(1, 1, 20);
            bind.SendTimeout = new TimeSpan(1, 1, 20);
            bind.TransactionFlow = false;
            bind.TransferMode = TransferMode.Buffered;
            bind.TransactionProtocol = TransactionProtocol.OleTransactions;
            bind.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            bind.ListenBacklog = 10;
            bind.MaxBufferPoolSize = 2147483646;
            bind.MaxBufferSize = 2147483646;
            bind.MaxReceivedMessageSize = 2147483646;
            bind.MaxConnections = 10;
            bind.ReaderQuotas.MaxDepth = 2147483647;
            bind.ReaderQuotas.MaxStringContentLength = 2147483647;
            bind.ReaderQuotas.MaxArrayLength = 2147483647;
            bind.ReaderQuotas.MaxBytesPerRead = 2147483647;
            bind.ReaderQuotas.MaxNameTableCharCount = 2147483647;// 16384;
            bind.ReliableSession.Ordered = true;
            bind.ReliableSession.InactivityTimeout = new TimeSpan(5, 59, 0);
            bind.Security.Mode = SecurityMode.None;
            return bind;
        }

        private BasicHttpBinding getBasicHttpBinding()
        {
            BasicHttpBinding bind = new BasicHttpBinding();
            bind.OpenTimeout = new TimeSpan(1, 1, 20);
            bind.CloseTimeout = new TimeSpan(1, 1, 20);
            bind.SendTimeout = new TimeSpan(1, 1, 20);
            //bind.TransactionFlow = false;
            bind.TransferMode = TransferMode.Buffered;
            //bind.TransactionProtocol = TransactionProtocol.OleTransactions;
            bind.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            //bind.ListenBacklog = 10;
            bind.MaxBufferPoolSize = 2147483646;
            bind.MaxBufferSize = 2147483646;
            bind.MaxReceivedMessageSize = 2147483646;
            //bind.MaxConnections = 10;
            bind.ReaderQuotas.MaxDepth = 2147483647;
            bind.ReaderQuotas.MaxStringContentLength = 2147483647;
            bind.ReaderQuotas.MaxArrayLength = 2147483647;
            bind.ReaderQuotas.MaxBytesPerRead = 2147483647;
            bind.ReaderQuotas.MaxNameTableCharCount = 2147483647;// 16384;
            //bind.ReliableSession.Ordered = true;
            //bind.ReliableSession.InactivityTimeout = new TimeSpan(5, 59, 0);
            bind.Security.Mode = BasicHttpSecurityMode.None;
            return bind;
        }

        public void Dispose()
        {
            Conexion.Close();
        }

    }
}

