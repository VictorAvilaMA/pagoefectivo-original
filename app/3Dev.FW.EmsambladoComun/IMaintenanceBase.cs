using System.Collections.Generic;
using System.ServiceModel;
using _3Dev.FW.Entidades;

namespace _3Dev.FW.EmsambladoComun
{
    [ServiceContract]
    public interface IMaintenanceBase 
    {
        [OperationContract]
         int InsertRecord(BusinessEntityBase be);
        [OperationContract]
         int UpdateRecord(BusinessEntityBase be);
        [OperationContract]
         BusinessEntityBase GetRecordByID(object id);
        [OperationContract]
         int DeleteRecord(BusinessEntityBase be);
        [OperationContract]
        int DeleteRecordByEntityId(object entityId);
        [OperationContract]
        List<BusinessEntityBase> SearchByParameters(BusinessEntityBase be);
        [OperationContract]
        List<BusinessEntityBase> SearchByParametersOrderedBy(BusinessEntityBase be, string orderBy, bool isAccending);
        [OperationContract]
        object GetObjectByParameters(string key, BusinessEntityBase be);
        [OperationContract]
        List<BusinessEntityBase> GetListByParameters(string key, BusinessEntityBase be);
        [OperationContract]
        List<BusinessEntityBase> GetListByParametersOrderedBy(string key, BusinessEntityBase be, string orderBy, bool isAccending);
        [OperationContract]
        BusinessEntityBase GetBusinessEntityByParameters(string key, BusinessEntityBase be);
        [OperationContract]
        int ExecProcedureByParameters(string key, BusinessEntityBase be);

        #region entity methods
        [OperationContract]
        BusinessMessageBase GetEntityByID(object id);
        [OperationContract]
        BusinessMessageBase GetEntity(BusinessMessageBase request);
        [OperationContract]
        BusinessMessageBase DeleteEntity(BusinessMessageBase request);
        [OperationContract]
        BusinessMessageBase SaveEntity(BusinessMessageBase request);
        #endregion
    }
}
