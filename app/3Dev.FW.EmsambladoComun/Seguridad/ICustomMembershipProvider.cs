using System.ServiceModel;
using System.Web.Security;

namespace _3Dev.FW.EmsambladoComun.Seguridad
{
    [ServiceContract]
    public interface ICustomMembershipProvider
    {
    [OperationContract]
        void Initialize(string name, string config);
        [OperationContract]
        bool CheckPassword(string username, string password);
        [OperationContract]
        bool ValidateUser(string username, string password);
        [OperationContract]
        bool ChangePassword(string username, string oldPwd, string newPwd);
        [OperationContract]
        bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer);
        [OperationContract]
        void ActualizarUsuario(string telefonofijo, string celular, MembershipUser user);
        [OperationContract]
        MembershipUser CreateUser(string username, string password, string primNombre, string appPaterno, string appMaterno, bool isApproved, object providerUserKey, out MembershipCreateStatus status);
        [OperationContract]
        bool ExistUser(string username);
        [OperationContract]
        bool ValidateUserAndPass(string username, string password);
    }
}
