using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using _3Dev.FW.Entidades.Seguridad;

namespace _3Dev.FW.EmsambladoComun.Seguridad
{
    [ServiceContract]
    public interface ICustomSiteMapProvider
    {
        [OperationContract]
        List<BESiteMap> BuildSiteMap(int idSistema);
        //bool UpdateRoleInSiteMap(int IdSiteMap, int IdRol);
        //DataTable GetPagesValidMenu(int IdSistema);
        //DataTable GetControlOptionsByPage(string PageName);
        //int GetIdSistemaByIDSiteMapNode(int idSiteMapNode);

        //List<BESiteMap> BuildSiteMap(int idSistema);
        [OperationContract]
        bool UpdateRoleInSiteMap(int IdSiteMap, int IdSistema, string Roles);
        [OperationContract]
        DataTable GetPagesValidMenu(int IdSistema);
        [OperationContract]
        DataTable GetControlOptionsByPage(string PageName);
        [OperationContract]
        int GetIdSistemaByIDSiteMapNode(int idSiteMapNode);
        [OperationContract]
        List<BESiteMap> GetPagesLikNameForSistema(string Name, int idSistema);
        [OperationContract]
        BESiteMap GetSiteMapById(int IdSiteMap);
        [OperationContract]
        List<BESiteMap> GetSiteMapWithURLBySistema(int idSistema);
        [OperationContract]
        bool AddControlToSitemap(int SiteMapId, int ControlId);
        [OperationContract]
        bool RemoveControlToSitemap(int SiteMapId, int ControlId);
        [OperationContract]
        List<BESiteMap> GetMenus(int idSistema, int idSiteMap);
        [OperationContract]
        List<BESiteMap> GetSiteMapBySistemaId(int idSistema);
        [OperationContract]
        int AddSitemap(BESiteMap beSiteMap);
        [OperationContract]
        bool UpdateSitemap(BESiteMap beSiteMap);
        [OperationContract]
        bool UpdateSitemapRoles(int SiteMapId);
        [OperationContract]
        BESiteMap GetSiteMapBySistemaURL(string URL, int SistemaId, int ParentId);
        [OperationContract]
        BESiteMap GetSiteMapByTitleParent(string Title, int ParentId);
        [OperationContract]
        List<BESiteMap> GetSiteMapsByIdRol(int idRol);

        [OperationContract]
        string GetCabezeraMenu(string urlPagina);
    }
}
