using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using _3Dev.FW.Entidades.Seguridad;


namespace _3Dev.FW.EmsambladoComun.Seguridad
{
    [ServiceContract]
    public interface ISqlRoleProvider
    {
        [OperationContract]
        void Initialize(string name, string config);
        [OperationContract]
        bool IsUserInRole(string username, string roleName);
        [OperationContract]
        string[] GetRolesForUser(string username);
        [OperationContract]
        void CreateRole(string roleName);
        [OperationContract]        //void CreateRole(string roleName, string RoleCodigo, string NemoniRol);
        bool DeleteRole(string roleName, bool throwOnPopulatedRole);
        [OperationContract]
        bool RoleExists(string roleName);
        [OperationContract]
        void AddUsersToRoles(string[] usernames, string[] roleNames);
        [OperationContract]
        void RemoveUsersFromRoles(string[] usernames, string[] roleNames);
        [OperationContract]
        string[] GetUsersInRole(string roleName);
        [OperationContract]
        string[] GetAllRoles();
        [OperationContract(Name = "GetAllRolesList")]
        List<BERol> GetAllRolesList();
        [OperationContract(Name = "GetAllRolesList2")]
        List<BERol> GetAllRolesList(string username);
        [OperationContract]
        string[] FindUsersInRole(string roleName, string usernameToMatch);
        [DataMember]
        string ApplicationName{get; set;}
        [OperationContract(Name = "UpdateRoleName1")]
        bool UpdateRoleName(string NewRoleName, string OldRoleName, int PermisoHastaSeccion, string NombreSeccion);
        [OperationContract(Name = "AddUserToRol1")]
        bool AddUserToRol(string UserName, string RoleName);
        [OperationContract(Name = "RemoveUserFromRol1")]
        bool RemoveUserFromRol(string UserName, string RoleName);
        [OperationContract]
        bool UpdateUserName(string NewUserName, string OldUserName, string RoleName);
        [OperationContract]
        List<BERol> getRolById(int id);







  
        //void InitializeSqlRoleProvider(string name, string config);
     
    //    bool IsUserInRole(string username, string roleName);
         
   //     string[] GetRolesForUser(string username);
         
      // void CreateRole(string roleName);
         
        //void CreateRole2(string roleName, string RoleCodigo);//, string NemoniRol);
         
        //bool DeleteRole(string roleName, bool throwOnPopulatedRole);
         
        //bool RoleExists(string roleName);
         
        //void AddUsersToRoles(string[] usernames, string[] roleNames);
         
        //void RemoveUsersFromRoles(string[] usernames, string[] roleNames);
         
        //string[] GetUsersInRole(string roleName);
         
        //string[] GetAllRoles();
         
        //List<BERol> GetAllRolesList();
         
        //List<BERol> GetAllRolesList2(string username);
         
     //   string[] FindUsersInRole(string roleName, string usernameToMatch);
        string ApplicationNameSqlRoleProvider
        {
             
            get;
             
            set;
        }
         [OperationContract]
        bool UpdateRoleName(int idRol, string RoleName, string codigo);
         [OperationContract]
        bool AddUserToRolSqlRoleProvider(string UserName, string RoleName);
         [OperationContract]
        bool RemoveUserFromRol(int UserId, int RolId);
         
     //   bool UpdateUserName(string NewUserName, string OldUserName, string RoleName);
         
      //  List<BERol> getRolById(int id);
         [OperationContract]
        List<BESistema> GetSistemasHabliesPorRol(int idRol);
         [OperationContract]
        int GetRolIdByName(string Name);
         [OperationContract]
        List<BERol> GetRolLikeName(string Name);
         [OperationContract]
        bool AddUserToRol(int UserId, int RolId);
         [OperationContract]
        bool AddSistemaToRol(int SistemaId, int RolId);
         [OperationContract]
        bool RemoveSistemaFromRol(int SistemaId, int RolId);
         [OperationContract]
        List<BERol> GetRolLikeNameForSistema(string Name, int SistemaId);
         [OperationContract]
        List<BERol> GetRolLikeNameForSistemaRelated(string Name, int SistemaId);
         [OperationContract]
        List<BERol> GetRolBySistema(int SistemaId);
         [OperationContract]
        List<BERol> GetRolForUserId(int UserId);
         [OperationContract]
        bool AddSiteMapToRol(int SiteMapId, int RolId);
         [OperationContract]
        bool RemoveSiteMapFromRol(int SiteMapId, int RolId);
         [OperationContract]
        List<BERol> GetRolLikeNameForSistemaSiteMap(string Name, int SistemaId, int SiteMapId);
         [OperationContract]
        List<BERol> GetRolLikeNameForSistemaSiteMapRelated(string Name, int SistemaId, int SiteMapId);
         [OperationContract]
        List<BERol> GetRolLikeNameForSiteMap(string Name, int SiteMapId);
        [OperationContract]    
        List<BERol> GetRolForSiteMap(int SiteMapId);
    }
}
