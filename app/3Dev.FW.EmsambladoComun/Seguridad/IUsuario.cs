using System;
using System.Collections.Generic;
using System.ServiceModel;
using _3Dev.FW.Entidades.Seguridad;


namespace _3Dev.FW.EmsambladoComun.Seguridad
{
    [ServiceContract]
    public interface IUsuario
    {
        
        [OperationContract]
        int RegistrarUsuario(BEUsuario eUsuario);
        [OperationContract]
        List<BEUsuario> GetAllUsuarioList();
        [OperationContract]
        int ModificarUsuario(BEUsuario eUsuario);
        
        [OperationContract]
        int EliminarUsuario(int IdUsuario);
        [OperationContract]
        List<BEUsuario> GetUsuariosByFiltros(BEUsuario eUsuario);
        [OperationContract]
        int IsUsuarioInRol(int IdUsuario);
        [OperationContract]
        List<BERolesByUsuario> GetRolesByUsuario(int IdUsuario);
        [OperationContract]
        List<BERol> GetRolesBySistema(int IdSistema);
        [OperationContract]
        int AddUserToRol(int IdUsuario, int IdRol);
        [OperationContract]
        int RemoveRolToUser(int id);
        ////DataTable GetGuids(string userName);
        [OperationContract]
        List<BERol> GetRoles(Int32 userId, int idSistema);
        [OperationContract]
        List<BEPregunta> GetPreguntas();
        [OperationContract]
        bool SendMail(string usrname, int idpreg, string nomresp);
        //List<BETipoDocumento> GetTiposDocumentos();
        [OperationContract]
        BEUserInfo GetUserInfoByUserName(string userName);
        [OperationContract]
        bool ConfirmUserValidation(string email, string guid);
        [OperationContract]
        List<BEUsuario> GetUsuarioLikeNameForRol(string Name, int RolId);

        //MODIFICADO PARA PAGINACION FASE III
        [OperationContract]
        List<BEUsuario> GetUsuarioLikeName(string Name, _3Dev.FW.Entidades.BusinessEntityBase entidad);
        
        [OperationContract]
        BEUsuario GetUsuarioById(int IdUsuario);
        
    }
}
