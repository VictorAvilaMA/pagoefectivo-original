using System;

namespace _3Dev.FW.EmsambladoComun
{
    [Serializable]
    public class ServiciosConocidos
    {
        public const string SistemasServices = "Sistemas.rem";
        public const string CustomSiteMapProviderServices = "CustomSiteMapProviderServices.rem";
        public const string SqlRoleProviderServices = "SqlRoleProvider.rem";
        public const string UsuarioServices = "Usuarios.rem";  
        public const string DominioServices = "Dominios.rem";
        public const string ControlServices = "Control.rem";
        public const string ServiciosServices = "Servicio.rem";
        public const string CustomMembership = "customship";
    }
}
