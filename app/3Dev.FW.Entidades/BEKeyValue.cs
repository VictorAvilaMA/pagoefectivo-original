using System;
using System.Runtime.Serialization;

class MyClass<T>
{
    //public T MyProperty { get; set; }
}


namespace _3Dev.FW.Entidades
{
    [Serializable] [DataContract]
    public class BEKeyValue<TKey,TValue>
    {
        public BEKeyValue()
        {

        }
        public BEKeyValue(TKey key,TValue value)
        {
            this._key = key;
            this._value = value;
        }
        private TKey _key;
[DataMember]
        public TKey Key
        {
            get { return _key; }
            set { _key =value;}
        }

        private TValue _value;
[DataMember]
        public TValue Value
        {
            get { return _value; }
            set { _value = value; }
        }

    }
}
