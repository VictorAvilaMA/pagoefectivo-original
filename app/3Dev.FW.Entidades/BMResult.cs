using System;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades
{
    [Serializable()]
    [DataContract()]
    public enum BMResult
    {
        [EnumMember]
        Ok=1,
        [EnumMember]
        No=0,
        [EnumMember]
        Error=-1
    }
}
