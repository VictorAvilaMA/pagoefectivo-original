using System;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades
{
    [Serializable]
    [DataContract]
    public class BusinessEntityBase : IDisposable
    {
        [DataMember]
        public int PageNumber { get; set; }
        [DataMember]                                
        public int PageSize  { get; set; }
        [DataMember]
        public int TotalPageNumbers { get; set; }
        [DataMember]
        public bool TipoOrder { get; set; }
        [DataMember]
        public string PropOrder { get; set; }

        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
