using System;
using System.Collections.Generic;

namespace _3Dev.FW.Entidades
{
    [Serializable]
    public class BusinessMessageBase : BusinessEntityBase
    {
        private BMResult _BMresultState;
        public BMResult BMResultState
        {
            get { return _BMresultState; }
            set { _BMresultState = value; }
        }
        private string _state;
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        private string _message;
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        private BusinessEntityBase  _entity;
        public BusinessEntityBase Entity
        {
            get { return _entity; }
            set { _entity = value; }
        }

        private List<BusinessEntityBase> _entityList;
        public List<BusinessEntityBase> EntityList
        {
            get { return _entityList; }
            set { _entityList = value; }
        }

        private object _entityId;
        public object EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        private string _param1;
        public string Param1
        {
            get { return _param1; }
            set { _param1 = value; }
        }
        private string _param2;
        public string Param2
        {
            get { return _param2; }
            set { _param2 = value; }
        }

        private object _objparam1;
        public object ObjParam1
        {
            get { return _objparam1; }
            set { _objparam1 = value; }
        }
        private object _objparam2;
        public object ObjParam2
        {
            get { return _objparam2; }
            set { _objparam2 = value; }
        }

                
    }
}
