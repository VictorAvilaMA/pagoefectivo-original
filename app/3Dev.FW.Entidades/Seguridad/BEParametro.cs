using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad

{
    [Serializable] [DataContract]
    public class BEParametro : BusinessEntityBase
    {
        private int _idParametro;

[DataMember]
        public int idParametro
        {
            get { return _idParametro; }
            set { _idParametro = value; }
        }
        private string _descripParamerto;

[DataMember]
        public string descripParametro
        {
            get { return _descripParamerto; }
            set { _descripParamerto = value; }
        }
        private string _descripLarga;

[DataMember]
        public string descripLarga
        {
            get { return _descripLarga; }
            set { _descripLarga = value; }
        }
        private string _indActivo;

[DataMember]
        public string indActivo
        {
            get { return _indActivo; }
            set { _indActivo = value; }
        }
        private string _Nemonico;

[DataMember]
        public string Nemonico
        {
            get { return _Nemonico; }
            set { _Nemonico = value; }
        }
        private string _valorParametro;

[DataMember]
        public string valorParametro
        {
            get { return _valorParametro; }
            set { _valorParametro = value; }
        }
        private string _observacion;

[DataMember]
        public string observacion
        {
            get { return _observacion; }
            set { _observacion = value; }
        }
        private string _campo1;

[DataMember]
        public string campo1
        {
            get { return _campo1; }
            set { _campo1 = value; }
        }
        private string _campo2;

[DataMember]
        public string campo2
        {
            get { return _campo2; }
            set { _campo2 = value; }
        }
        private string _campo3;

[DataMember]
        public string campo3
        {
            get { return _campo3; }
            set { _campo3 = value; }
        }
        private string _campo4;

[DataMember]
        public string campo4
        {
            get { return _campo4; }
            set { _campo4 = value; }
        }
        private string _campo5;

[DataMember]
        public string campo5
        {
            get { return _campo5; }
            set { _campo5 = value; }

        }
        private string _campo6;
[DataMember]
        public string campo6
        {
            get { return _campo6; }
            set { _campo6 = value; }

        }
        private string _campo7;
[DataMember]
        public string campo7
        {
            get { return _campo7; }
            set { _campo7 = value; }
        }

        private string _campo8;
[DataMember]
        public string campo8
        {
            get { return _campo8; }
            set { _campo8 = value; }
        }
        private string _campo9;
[DataMember]
        public string campo9
        {
            get { return _campo9; }
            set { _campo9 = value; }

        }
        private string _campo10;
[DataMember]
        public string campo10
        {
            get { return _campo10; }
            set { _campo10 = value; }

        }
        private string _usrIng;
[DataMember]
        public string usrIng
        {
            get { return _usrIng; }
            set { _usrIng = value; }
        }
        private DateTime _fecIng;

[DataMember]
        public DateTime fecIng
        {
            get { return _fecIng; }
            set { _fecIng = value; }
        }

        private string _usrMod;

[DataMember]
        public string usrMod
        {
            get { return _usrMod; }
            set { _usrMod = value; }
        }
        private int _IdDominio;
[DataMember]
        public int IdDominio
        {
            get { return _IdDominio; }
            set { _IdDominio = value; }

        }



        public BEParametro()
        {

        }
        public BEParametro(IDataReader reader)
        {
            //this.
        }





    }
}

