using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BEPregunta : BusinessEntityBase
    {
        private int idpreguntasecreta;
[DataMember]
        public int Idpreguntasecreta
        {
            get { return idpreguntasecreta; }
            set { idpreguntasecreta = value; }
        }
        private string nompreguntasecreta;
[DataMember]
        public string Nompreguntasecreta
        {
            get { return nompreguntasecreta; }
            set { nompreguntasecreta = value; }
        }
        public BEPregunta(IDataReader reader)
        {
            this.Idpreguntasecreta = Convert.ToInt32(reader["idpreguntasecreta"]);
            this.Nompreguntasecreta = reader["nompreguntasecreta"].ToString();
        }
    }
}
