using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BERol : BusinessEntityBase
    {
        private int _idRol;
        private string _nemonico;
        private string _descripcion;
        //private List<BESistema> _Sistemas;
        //public List<BESistema> Sistemas
        //{
        //    get { return _Sistemas; }
        //    set { _Sistemas = value; }
        //}

[DataMember]
        public int IdRol
        {
            get { return _idRol; }
            set { _idRol = value; }
        }
[DataMember]
        public string Nemonico
        {
            get { return _nemonico; }
            set { _nemonico = value; }
        }
[DataMember]
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        public BERol()
        {
 
        }
[DataMember]
        public int _Pertenece;
[DataMember]
        public int Pertenece
        {
            get { return _Pertenece; }
            set { _Pertenece = value; }
        }
        public BERol(IDataReader reader)
        {
            this.IdRol = Convert.ToInt32(reader["IdRol"]);
            this.Descripcion = reader["Descripcion"].ToString();
            this.Nemonico = reader["Nemonico"].ToString();
        }
        

    }
}
