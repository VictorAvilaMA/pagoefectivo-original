using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BERolPagina : BusinessEntityBase
    {
        private int _idPagina;
[DataMember]
        public int IdPagina
        {
            get { return _idPagina; }
            set { _idPagina = value; }
        }
        private int _idRol;
[DataMember]
        public int IdRol
        {
            get { return _idRol; }
            set { _idRol = value; }
        }
        private string _url;
[DataMember]
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
        private string _TituloPagina;
[DataMember]
        public string TituloPagina
        {
            get { return _TituloPagina; }
            set { _TituloPagina = value; }
        }
        private string _Rol;
[DataMember]
        public string Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }
        public BERolPagina(IDataReader reader)
        {
            this.IdRol = Convert.ToInt32(reader["IdRol"]);
            this.IdPagina = Convert.ToInt32(reader["IdPagina"]);
            this.Url =reader["Url"].ToString();
            this.TituloPagina  = reader["Titulo"].ToString();
            this.Rol = reader["Rol"].ToString();
        }

    }
}
