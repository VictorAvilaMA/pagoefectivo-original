using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BERolesByUsuario : BusinessEntityBase
    {
        public BERolesByUsuario()
        {
        
        }

        public BERolesByUsuario(IDataReader reader)
        {
            this.IdUsuarioRol = int.Parse(reader["ID_USU_ROL"].ToString());
            this.CodigoSistema = reader["ID_SIS"].ToString();
            this.DescripcionSistema = reader["DESC_SIS"].ToString();
            this.CodigoRol = reader["id_rol"].ToString();
            this.DescripcionRol = reader["DESCRI_ROL"].ToString();

        }
            private int _IdUsuarioRol;
            private string _CodigoSistema;
            private string _DescripcionSistema;
            private string _CodigoRol;
            private string _DescripcionRol;

[DataMember]
            public string DescripcionRol
            {
                get { return _DescripcionRol; }
                set { _DescripcionRol = value; }
            }
[DataMember]
            public string CodigoRol
            {
                get { return _CodigoRol; }
                set { _CodigoRol = value; }
            }
[DataMember]
            public string DescripcionSistema
            {
                get { return _DescripcionSistema; }
                set { _DescripcionSistema = value; }
            }
[DataMember]
            public string CodigoSistema
            {
                get { return _CodigoSistema; }
                set { _CodigoSistema = value; }
            }
[DataMember]
            public int IdUsuarioRol
            {
                get { return _IdUsuarioRol; }
                set { _IdUsuarioRol = value; }
            }

       
    }
}
