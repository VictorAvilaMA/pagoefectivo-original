using System;
using System.Data;
using System.Runtime.Serialization;


namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BESistema : BusinessEntityBase
    {
        #region Propiedades

        //public string CodSistema { get; set; }

[DataMember]
        public string CodSistema
        {
            get { return CodSistema; }
            set { CodSistema = value; }

        }
        //public int IdSistema { get; set; }

[DataMember]
        public string DescripSistema
        {
            get { return DescripSistema; }
            set { DescripSistema = value; }
        }


[DataMember]
        public string EstadoSistema 
        {
            get { return EstadoSistema ;}
            set { EstadoSistema= value ;}
        }
[DataMember]
        public string Imagen
        {
            get { return Imagen; }
            set { Imagen= value;}
        }
[DataMember]
        public string Link 
        {
            get { return Link ;}
            set {Link= value ;}
        }
[DataMember]
        public Int32 Orden_Aparicion
        {
            get {return  Orden_Aparicion ;}
            set {Orden_Aparicion = value ;}
        }
[DataMember]
        public int Pertenece
        {
            get {return Pertenece  ;}
            set {Pertenece = value ;}
        }
        #endregion

[DataMember]
        public BESistemasByRoles BESistemasByRoles
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }
[DataMember]
        public BESiteMap BESiteMap
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }
        public BESistema()
        {

        }
        public BESistema(IDataReader reader)
        {

           // this.IdSistema = Convert.ToInt32(reader["ID_SIS"]);
            this.CodSistema = reader["COD_SIS"].ToString();
            this.DescripSistema = reader["DESC_SIS"].ToString();
            this.EstadoSistema = reader["EST_SIS"].ToString();
            this.Imagen = reader["IMAGEN"].ToString();
            this.Link = reader["LINK"].ToString();
            //this.Nemonico = reader["NEMON"].ToString();
            this.Orden_Aparicion = reader["ORD_APAR"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ORD_APAR"]);//Convert.ToInt32(reader["ORD_APAR"]);


        }

    }
}
