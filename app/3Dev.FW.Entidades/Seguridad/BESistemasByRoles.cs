using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BESistemasByRoles : BusinessEntityBase
    {
        #region Propiedades
[DataMember]
        public int idSistemasByRoles
        {
            get {return idSistemasByRoles  ;}
            set {idSistemasByRoles = value ;}
        }
[DataMember]
        public int idRol 
        {
            get {return idRol  ;}
            set {idRol = value ;}
        }
[DataMember]
        public int idSistema
        {
            get { return  idSistema;}
            set {idSistema = value ;}
        }
        #endregion
        public BESistemasByRoles()
        {


        }

        public BESistemasByRoles(IDataReader reader)
        {
            this.idSistemasByRoles = int.Parse(reader["ID_SIS_ROL"].ToString());
            this.idRol = Convert.ToInt32(reader["ID_ROL"]);
            this.idSistema = Convert.ToInt32(reader["ID_SIS"].ToString());

        }

    }
}
