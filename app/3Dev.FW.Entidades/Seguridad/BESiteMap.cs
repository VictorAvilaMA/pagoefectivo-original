using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable] [DataContract]
    public class BESiteMap : BusinessEntityBase
    {

        #region Propiedades
        private int _Id;

[DataMember]
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        private string _Title;

[DataMember]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        private string _Descripcion;

[DataMember]
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        private string _Url;

[DataMember]
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }
        private int _IdRol;

[DataMember]
        public int IdRol
        {
            get { return _IdRol; }
            set { _IdRol = value; }
        }
        private string _Rol;

[DataMember]
        public string Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }
        private int? _IdParent;

[DataMember]
        public int? IdParent
        {
            get { return _IdParent; }
            set { _IdParent = value; }
        }
        private string _Parent;

[DataMember]
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        private int _IdSistema;

[DataMember]
        public int IdSistema
        {
            get { return _IdSistema; }
            set { _IdSistema = value; }
        }
        private int _Display;
[DataMember]
        public int Display
        {
            get { return _Display; }
            set { _Display = value; }
        }

        private string _Estado;
[DataMember]
        public string Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }
        private int _Secuencia;
[DataMember]
        public int Secuencia
        {
            get { return _Secuencia; }
            set { _Secuencia = value; }
        }
        
        #endregion
        #region Constructores
        public BESiteMap()
        {

        }



        public BESiteMap(IDataReader oIDataReader)
        {
            this.Id = Convert.ToInt32(oIDataReader["Id"]);
            this.Title = oIDataReader.IsDBNull(1) ? null : Convert.ToString(oIDataReader["Title"]);
            this.Descripcion = Convert.ToString(oIDataReader["Descripcion"]);
            this.Url = Convert.ToString(oIDataReader["Url"]);
            this.Rol = oIDataReader.IsDBNull(4) ? null : Convert.ToString(oIDataReader["Roles"]);
            if (oIDataReader.IsDBNull(5))
                this.IdParent = null;
            else
                this.IdParent = Convert.ToInt32(oIDataReader["Parent"]);
            this.Parent = oIDataReader.IsDBNull(6) ? null : Convert.ToString(oIDataReader["ParentName"]);
            this.IdSistema = Convert.ToInt32(oIDataReader["IdSistema"]);
            this.Display = FW.Util.DataUtil.ObjectToInt(oIDataReader["Visible"]) ;
        }

        #endregion

        //#region Propiedades
        //public int _Id;
        //public int Id 
        //{
        //    get { return Id; }
        //    set { Id = value ;}
        //}
        //public string _Title;
        //public string Title
        //{
        //    get { return _Title; }
        //    set { _Title = value; }
        //}
        //public string _Descripcion;
        //public string Descripcion
        //{
        //    get { return _Descripcion; }
        //    set { _Descripcion = value; }
        //}
        //public string _Url;
        //public string Url
        //{
        //    get { return _Url; }
        //    set { _Url = value; }
        //}
        //public string _Roles;
        //public string Roles
        //{
        //    get { return _Roles; }
        //    set { _Roles = value; }
        //}
        //public int? _IdParent;
        //public int? IdParent
        //{
        //    get { return _IdParent; }
        //    set { _IdParent = value; }
        //}
        //public string _Parent;
        //public string Parent
        //{
        //    get { return _Parent; }
        //    set { _Parent = value; }
        //}
        //public int _IdSistema;
        //public int IdSistema
        //{
        //    get { return _IdSistema; }
        //    set { _IdSistema = value; }
        //}
        //public int _Display;
        //public int Display
        //{
        //    get { return _Display; }
        //    set { _Display = value; }
        //}
        //public string _Estado;
        //public string Estado
        //{
        //    get { return _Estado; }
        //    set { _Estado = value; }
        //}
        //public int _Secuencia;
        //public int Secuencia
        //{
        //    get { return _Secuencia; }
        //    set { _Secuencia = value; }
        //}
        //public int _Pertenece;
        //public int Pertenece
        //{
        //    get { return _Pertenece; }
        //    set { _Pertenece = value; }
        //}
        //#endregion

        //public BESiteMap BESiteMap1
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}
    
        //public BESiteMap()
        //{

        //}
        //public BESiteMap(IDataReader oIDataReader)
        //{
        //    this.Id = Convert.ToInt32(oIDataReader["Id"]);
        //    this.Title = oIDataReader.IsDBNull(1) ? null : Convert.ToString(oIDataReader["Title"]);
        //    this.Descripcion = Convert.ToString(oIDataReader["Descripcion"]);
        //    this.Url = Convert.ToString(oIDataReader["Url"]);
        //    this.Roles = oIDataReader.IsDBNull(4) ? null : Convert.ToString(oIDataReader["Roles"]);
        //    if (oIDataReader.IsDBNull(5))
        //        this.IdParent = null;
        //    else
        //        this.IdParent = Convert.ToInt32(oIDataReader["Parent"]);
        //    this.Parent = oIDataReader.IsDBNull(6) ? null : Convert.ToString(oIDataReader["ParentName"]);
        //    this.IdSistema = Convert.ToInt32(oIDataReader["IdSistema"]);
        //    this.Display = Convert.ToInt32(oIDataReader["Display"]);
        //    this.Estado = Convert.ToString(oIDataReader["Estado"]);
        //    this.Secuencia = Convert.ToInt32(oIDataReader["Secuencia"]);
        //}
    }
}
