using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable]
    [DataContract]
    public class BEUserInfo : BusinessEntityBase
    {
        public BEUserInfo()
        {

        }
        public BEUserInfo(IDataReader reader, int uno = 0)
        {
            this._idUsuario = FW.Util.DataUtil.ObjectToInt32(reader["IdUsuario"]);
            this._nombres = FW.Util.DataUtil.ObjectToString(reader["Nombres"]);
            this._apellidos = FW.Util.DataUtil.ObjectToString(reader["Apellidos"]);
            this._idTipoDocumento = FW.Util.DataUtil.ObjectToInt32(reader["IdTipoDocumento"]);
            this._numeroDocumento = FW.Util.DataUtil.ObjectToString(reader["NumeroDocumento"]);
            this._direccion = FW.Util.DataUtil.ObjectToString(reader["Direccion"]);
            this._telefono = FW.Util.DataUtil.ObjectToString(reader["Telefono"]);
            this._email = FW.Util.DataUtil.ObjectToString(reader["Email"].ToString());
            this._idPais = FW.Util.DataUtil.ObjectToInt32(reader["IdPais"]);
            this._idDepartamento = FW.Util.DataUtil.ObjectToInt32(reader["IdDepartamento"]);
            this._idCiudad = FW.Util.DataUtil.ObjectToInt32(reader["IdCiudad"]);
            this._alias = FW.Util.DataUtil.ObjectToString(reader["Alias"]);
            this._rol = FW.Util.DataUtil.ObjectToString(reader["Rol"]);
            this._idEstado = FW.Util.DataUtil.ObjectToInt32(reader["IdEstado"]);
            if (uno == 1)
            {
                if ((reader["Imagen"]) is DBNull)
                {
                    this._imagenAvatar = null;
                }
                else
                {
                    this._imagenAvatar = ((byte[])reader["Imagen"]);
                }
                this._flagRegistrarPc = FW.Util.DataUtil.ObjectToBool(reader["HabilitarRegistroPC"]);
                this.HabilitarMonedero = FW.Util.DataUtil.ObjectToBool(reader["HabilitarMonedero"]);
            }
            this.TieneInfoCompleta = FW.Util.DataUtil.ObjectToBool(reader["TieneInfoCompleta"]);
        }

        private bool _HabilitarMonedero;
        [DataMember]
        public bool HabilitarMonedero
        {
            get { return _HabilitarMonedero; }
            set { _HabilitarMonedero = value; }
        }
        

        private int _idUsuario;
        [DataMember]
        public int IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        private string _alias;
        [DataMember]
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        private string _rol;
        [DataMember]
        public string Rol
        {
            get { return _rol; }
            set { _rol = value; }
        }
        private string[] _roles;
        [DataMember]
        public string[] Roles
        {
            get { return _roles; }
            set { _roles = value; }
        }


        private string _nombres;
        [DataMember]
        public string Nombres
        {
            get { return _nombres; }
            set { _nombres = value; }
        }
        public string NombreCompleto
        {
            get { return _nombres + " " + _apellidos; }
        }

        private string _apellidos;
        [DataMember]
        public string Apellidos
        {
            get { return _apellidos; }
            set { _apellidos = value; }
        }

        private int _idTipoDocumento;

        [DataMember]
        public int IdTipoDocumento
        {
            get { return _idTipoDocumento; }
            set { _idTipoDocumento = value; }
        }
        private string _numeroDocumento;

        [DataMember]
        public string NumeroDocumento
        {
            get { return _numeroDocumento; }
            set { _numeroDocumento = value; }
        }

        private string _direccion;
        [DataMember]
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        private string _telefono;
        [DataMember]
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        private string _email;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private int _idEstado;

        [DataMember]
        public int IdEstado
        {
            get { return _idEstado; }
            set { _idEstado = value; }
        }

        private int _idPais;
        [DataMember]
        public int IdPais
        {
            get { return _idPais; }
            set { _idPais = value; }
        }

        private int _idDepartamento;
        [DataMember]
        public int IdDepartamento
        {
            get { return _idDepartamento; }
            set { _idDepartamento = value; }
        }

        private int _idCiudad;
        [DataMember]
        public int IdCiudad
        {
            get { return _idCiudad; }
            set { _idCiudad = value; }
        }
        private List<BERolPagina> _PaginasAcceso;
        [DataMember]
        public List<BERolPagina> PaginasAcceso
        {
            get { return _PaginasAcceso; }
            set { _PaginasAcceso = value; }
        }

        private List<BEKeyValue<string, object>> _sessionUser;
        [DataMember]
        public List<BEKeyValue<string, object>> SessionUser
        {
            get { return _sessionUser; }
            set { _sessionUser = value; }
        }

        [DataMember]
        public string EsJefeProducto{get;set;}

        private byte[] _imagenAvatar;
        [DataMember]
        public byte[] ImagenAvatar
        {
            get { return _imagenAvatar; }
            set { _imagenAvatar = value; }
        }

        private bool _flagRegistrarPc;
        [DataMember]
        public bool FlagRegistrarPc
        {
            get { return _flagRegistrarPc; }
            set { _flagRegistrarPc = value; }
        }



        private bool _TieneInfoCompleta;
        /// <summary>
        /// Retorna true si tiene datos
        /// </summary>
        [DataMember]
        public bool TieneInfoCompleta
        {
            get { return _TieneInfoCompleta; }
            set { _TieneInfoCompleta  = value; }
        }

    }


}
