using System;
using System.Data;
using System.Runtime.Serialization;

namespace _3Dev.FW.Entidades.Seguridad
{
    [Serializable]
    [DataContract]
    public class BEUsuario : BusinessEntityBase
    {
        public BEUsuario()
        {

        }
        public void ActualizarCampos(IDataReader reader, int flag = 0)
        {
            this.IdUsuario = FW.Util.DataUtil.ObjectToInt32(reader["IdUsuario"]);
            this.Nombres = FW.Util.DataUtil.ObjectToString(reader["Nombres"]);
            this.Apellidos = FW.Util.DataUtil.ObjectToString(reader["Apellidos"]);
            this.NombresApellidos = FW.Util.DataUtil.ObjectToString(reader["Nombres"]) + " " + FW.Util.DataUtil.ObjectToString(reader["Apellidos"]);
            this.IdTipoDocumento = FW.Util.DataUtil.ObjectToInt32(reader["IdTipoDocumento"]);
            this.NumeroDocumento = FW.Util.DataUtil.ObjectToString(reader["NumeroDocumento"]);
            this.Direccion = FW.Util.DataUtil.ObjectToString(reader["Direccion"]);
            this.Telefono = FW.Util.DataUtil.ObjectToString(reader["Telefono"]);
            this.Email = FW.Util.DataUtil.ObjectToString(reader["Email"]);
            this.IdEstado = FW.Util.DataUtil.ObjectToInt32(reader["IdEstado"]);
            this.IdUsuarioCreacion = FW.Util.DataUtil.ObjectToInt32(reader["IdUsuarioCreacion"]);
            this.FechaCreacion = FW.Util.DataUtil.ObjectToDateTime(reader["FechaCreacion"]);
            this.IdUsuarioActualizacion = FW.Util.DataUtil.ObjectToInt32(reader["IdUsuarioActualizacion"]);
            this.FechaActualizacion = FW.Util.DataUtil.ObjectToDateTime(reader["FechaActualizacion"]);
            this.IdPais = FW.Util.DataUtil.ObjectToInt32(reader["IdPais"]);
            this.IdDepartamento = FW.Util.DataUtil.ObjectToInt32(reader["IdDepartamento"]);
            this.IdCiudad = FW.Util.DataUtil.ObjectToInt32(reader["IdCiudad"]);
            this.DescripcionEstado = FW.Util.DataUtil.ObjectToString(reader["DescripcionEstado"]);

            this.IdGenero = FW.Util.DataUtil.ObjectToInt32(reader["IdGenero"]);
            this.FechaNacimiento = FW.Util.DataUtil.ObjectToDateTime(reader["FechaNacimiento"]);

            if (flag == 1)
            {
                if ((reader["Imagen"]) is DBNull)
                    this.ImagenAvatar = null;
                else
                    this.ImagenAvatar = ((byte[])reader["Imagen"]);
            }



            //this.TipoDoc = FW.Util.DataUtil.ObjectToString(reader["TipoDoc"]);

        }

        public BEUsuario(IDataReader reader)
        {
            ActualizarCampos(reader);
        }

        public BEUsuario(IDataReader reader, int paginacion)
        {
            ActualizarCampos(reader);
            this.TotalPageNumbers = FW.Util.DataUtil.ObjectToInt32(reader["TOTAL_RESULT"]);
        }

        public BEUsuario(IDataReader reader, int uno, int dos)
        {
            ActualizarCampos(reader, 1);
            this.FlagRegistrarPc = FW.Util.DataUtil.ObjectToBool(reader["HabilitarRegistroPC"]);
        }

        public BEUsuario(IDataReader reader, string Consulta)
        {

            if (Consulta == "ValidarUsuarioEmail")
            {


                this.IdUsuario = Convert.ToInt32(reader["IdUsuario"].ToString());
                this.Nombres = Convert.ToString(reader["Nombres"]);
                this.Apellidos = Convert.ToString(reader["Apellidos"]);
                this.Email = Convert.ToString(reader["Email"].ToString());
                this.Password = reader["Password"].ToString();
                this.IdEstado = Convert.ToInt32(reader["IdEstado"].ToString());
                this.DescripcionEstado = reader["DescripcionEstado"].ToString();
            }
        }

        public BEUsuario(IDataReader reader, int val1, string val2)
        {
            this.Email = FW.Util.DataUtil.ObjectToString(reader["Email"].ToString());
            this.Apellidos = FW.Util.DataUtil.ObjectToString(reader["Apellidos"]);
            this.Nombres = FW.Util.DataUtil.ObjectToString(reader["Nombres"]);
            this.NombresApellidos = FW.Util.DataUtil.ObjectToString(reader["Apellidos"]) + ", " + FW.Util.DataUtil.ObjectToString(reader["Nombres"]);
            this.TipoDoc = FW.Util.DataUtil.ObjectToString(reader["TipoDocumento"]);
            this.NumeroDocumento = FW.Util.DataUtil.ObjectToString(reader["NumeroDocumento"]);
            this.DescripcionGenero = FW.Util.DataUtil.ObjectToString(reader["Genero"]);
        }


        private string _nuevoPassword;


        [DataMember]
        public string NuevoPassword
        {
            get { return _nuevoPassword; }
            set { _nuevoPassword = value; }
        }


        private string _descripcionEstado;


        [DataMember]
        public string DescripcionEstado
        {
            get { return _descripcionEstado; }
            set { _descripcionEstado = value; }
        }


        private string _descripcionGenero;


        [DataMember]
        public string DescripcionGenero
        {
            get { return _descripcionGenero; }
            set { _descripcionGenero = value; }
        }


        private int _idUsuario;

        [DataMember]
        public int IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        private string _nombres;

        [DataMember]
        public string Nombres
        {
            get { return _nombres; }
            set { _nombres = value; }
        }

        private string _apellidos;

        [DataMember]
        public string Apellidos
        {
            get { return _apellidos; }
            set { _apellidos = value; }
        }

        private string _nombresApellidos;

        [DataMember]
        public string NombresApellidos
        {
            get { return _nombresApellidos; }
            set { _nombresApellidos = value; }
        }

        private int _idTipoDocumento;

        [DataMember]
        public int IdTipoDocumento
        {
            get { return _idTipoDocumento; }
            set { _idTipoDocumento = value; }
        }
        private string _numeroDocumento;

        [DataMember]
        public string NumeroDocumento
        {
            get { return _numeroDocumento; }
            set { _numeroDocumento = value; }
        }

        private string _direccion;

        [DataMember]
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        private string _telefono;

        [DataMember]
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        private string _email;

        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private int _idEstado;

        [DataMember]
        public int IdEstado
        {
            get { return _idEstado; }
            set { _idEstado = value; }
        }

        private DateTime _fechaCreacion;

        [DataMember]
        public DateTime FechaCreacion
        {
            get { return _fechaCreacion; }
            set { _fechaCreacion = value; }
        }

        private DateTime _fechaActualizacion;

        [DataMember]
        public DateTime FechaActualizacion
        {
            get { return _fechaActualizacion; }
            set { _fechaActualizacion = value; }
        }

        private int _idIsuarioCreacion;

        [DataMember]
        public int IdUsuarioCreacion
        {
            get { return _idIsuarioCreacion; }
            set { _idIsuarioCreacion = value; }
        }


        private int _idUsuarioActualizacion;

        [DataMember]
        public int IdUsuarioActualizacion
        {
            get { return _idUsuarioActualizacion; }
            set { _idUsuarioActualizacion = value; }
        }

        private string _password;

        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private int _idPais;

        [DataMember]
        public int IdPais
        {
            get { return _idPais; }
            set { _idPais = value; }
        }

        private int _idDepartamento;

        [DataMember]
        public int IdDepartamento
        {
            get { return _idDepartamento; }
            set { _idDepartamento = value; }
        }

        private int _idCiudad;

        [DataMember]
        public int IdCiudad
        {
            get { return _idCiudad; }
            set { _idCiudad = value; }
        }

        private int _idGenero;

        [DataMember]
        public int IdGenero
        {
            get { return _idGenero; }
            set { _idGenero = value; }
        }

        private DateTime _fechaNacimiento;

        [DataMember]
        public DateTime FechaNacimiento
        {
            get { return _fechaNacimiento; }
            set { _fechaNacimiento = value; }
        }

        private string _tipoDoc;

        [DataMember]
        public string TipoDoc
        {
            get { return _tipoDoc; }
            set { _tipoDoc = value; }
        }

        private byte[] _imagenAvatar;

        [DataMember]
        public byte[] ImagenAvatar
        {
            get { return _imagenAvatar; }
            set { _imagenAvatar = value; }
        }

        private bool _flagRegistrarPc;

        [DataMember]
        public bool FlagRegistrarPc
        {
            get { return _flagRegistrarPc; }
            set { _flagRegistrarPc = value; }
        }

    }


}
