using System;
using System.Runtime.Serialization;

namespace _3Dev.FW.Excepciones
{
    
    [Serializable()]
    public class FWBusinessException : Exception
    {
        protected object _objRemoto;

        public FWBusinessException()
        { 
        }        

        public FWBusinessException(string message,object oRemoto)
            : base(message)
        {
            this._objRemoto = oRemoto;
        }

        protected FWBusinessException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {            
            _objRemoto = info.GetValue("_objRemoto", typeof(object));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {            
            info.AddValue("_objRemoto", _objRemoto, typeof(object));
            base.GetObjectData(info, context);
        }        
        
        public object  ObjRemoto
        {
            get {
                return _objRemoto;
            }
            set {
                _objRemoto = value;
            }

        }

    }
}
