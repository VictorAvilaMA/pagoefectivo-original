using System;

namespace _3Dev.FW.Excepciones
{
    [Serializable()]
    public class FWException:System.Exception
    {

        public FWException()
        { }

        public FWException(string message)
            : base(message)
        {
        }

        protected string customMessage;
        protected string errorCode;

        public string CustomMessage
        {
            get { return customMessage; }
            set { customMessage = value; }
        }
        public string ErrorCode
        {
            get { return errorCode; }
            set { errorCode = value; }
        }

    }

}
