using System;

namespace _3Dev.FW.Negocio
{
    public class BLBase : IDisposable
    {

        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
