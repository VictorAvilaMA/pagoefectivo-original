using System;
using System.Collections.Specialized;
using System.Web.Security;
using _3Dev.FW.AccesoDatos.Seguridad;

namespace _3Dev.FW.Negocio.Seguridad
{
    [Serializable]
    public class BLCustomMembershipProvider
    {
        //private CustomMembershipProvider dataAccessProviderObject = null;

        //private void DoConstructorDataAccessObject()
        //{
        //    if (dataAccessProviderObject == null)
        //        dataAccessProviderObject = GetConstructorDataAccessObject();
        //}
        public virtual CustomMembershipProvider GetConstructorDataAccessObject()
        {
            return null;
        }
        public virtual CustomMembershipProvider DataAccessProviderObject
        {
            get
            {
                //DoConstructorDataAccessObject();
                return GetConstructorDataAccessObject();
            }
        }


        public void Initialize(string name, string config)
        {

            NameValueCollection slconfig = new NameValueCollection();
            string[] pares = config.Split(';');
            for (int i = 0; i <= pares.Length - 2; i++)
            {
                string[] valores = pares[i].Split(':');
                slconfig[valores[0].ToString()] = valores[1].ToString();
            }

            DataAccessProviderObject.Initialize(name, slconfig);
        }
        public bool ValidateUser(string username, string password)
        {
            return DataAccessProviderObject.ValidateUser(username, password);
        }
        public virtual bool ValidateUserAndPass(string username, string password)
        {
            return DataAccessProviderObject.ValidateUserAndPass(username, password);
        }
        public bool CheckPassword(string username, string password)
        {
            return DataAccessProviderObject.CheckPassword(username, password);
        }
        public bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            return DataAccessProviderObject.ChangePassword(username, oldPwd, newPwd);
        }
        public bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
        {
            return DataAccessProviderObject.ChangePasswordQuestionAndAnswer(username, password, newPwdQuestion, newPwdAnswer);
        }
        public void ActualizarUsuario(string telefonofijo, string celular, MembershipUser user)
        {
            DataAccessProviderObject.ActualizarUsuario(telefonofijo, celular, user);
        }
        public MembershipUser CreateUser(string username, string password, string primNombre, string appPaterno, string appMaterno, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            return DataAccessProviderObject.CreateUser(username, password, primNombre, appPaterno, appMaterno, isApproved, providerUserKey, out status);
        }
        public virtual bool ExistUser(string username)
        {
            return DataAccessProviderObject.ExistUser(username);
        }
    }
}
