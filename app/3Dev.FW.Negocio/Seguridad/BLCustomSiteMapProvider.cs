using System;
using System.Collections.Generic;
using System.Data;
using _3Dev.FW.AccesoDatos.Seguridad;
using _3Dev.FW.Entidades.Seguridad;


namespace _3Dev.FW.Negocio.Seguridad
{
    [Serializable]
    public class BLCustomSiteMapProvider
    {
        //private DACustomSiteMapProvider dataAccessProviderObject = null;

        //private void DoConstructorDataAccessObject()
        //{
        //    if (dataAccessProviderObject == null)
        //        dataAccessProviderObject = GetConstructorDataAccessObject();
        //}
        public virtual DACustomSiteMapProvider GetConstructorDataAccessObject()
        {
            return null;
        }
        public virtual DACustomSiteMapProvider DataAccessProviderObject
        {
            get
            {
                //DoConstructorDataAccessObject();
                return GetConstructorDataAccessObject();
            }
        }

        public List<BESiteMap> BuildSiteMap(int idSistema)
        {
            return DataAccessProviderObject.BuildSiteMap(idSistema);
        }
        public bool UpdateRoleInSiteMap(int IdSiteMap, int IdRol)
        {
            return DataAccessProviderObject.UpdateRoleInSiteMap(IdSiteMap, IdRol);
        }

        public DataTable GetPagesValidMenu(int IdSistema)
        {
            return DataAccessProviderObject.GetPagesValidMenu(IdSistema);
        }
        public DataTable GetControlOptionsByPage(string PageName)
        {
            return DataAccessProviderObject.GetControlOptionsByPage(PageName);
        }
        public int GetIdSistemaByIDSiteMapNode(int idSiteMapNode)
        {
            return DataAccessProviderObject.GetIdSistemaByIDSiteMapNode(idSiteMapNode);
        }
        public List<BESiteMap> GetPagesLikNameForSistema(string Name, int idSistema)
        {
            return DataAccessProviderObject.GetPagesLikNameForSistema(Name, idSistema);
        }
        public virtual List<BESiteMap> GetSiteMapsByIdRol(int idRol)
        {
            return DataAccessProviderObject.GetSiteMapsByIdRol(idRol);
        }
        public string GetCabezeraMenu(string urlPagina)
        {
            return DataAccessProviderObject.GetCabezeraMenu(urlPagina);
        }
    }
}
