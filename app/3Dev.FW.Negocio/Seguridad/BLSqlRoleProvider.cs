using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using _3Dev.FW.AccesoDatos.Seguridad;
using _3Dev.FW.Entidades.Seguridad;

namespace _3Dev.FW.Negocio.Seguridad
{
    [Serializable]
    public class BLSqlRoleProvider
    {
        //private DASqlRoleProvider dataAccessProviderObject = null;

        //private void DoConstructorDataAccessObject()
        //{
        //    if (dataAccessProviderObject == null)
        //        dataAccessProviderObject = GetConstructorDataAccessObject();
        //}
        public virtual DASqlRoleProvider GetConstructorDataAccessObject()
        {
            return null;
        }
        public virtual DASqlRoleProvider DataAccessProviderObject
        {
            get
            {
                //DoConstructorDataAccessObject();
                return GetConstructorDataAccessObject();
            }
        }
        public void Initialize(string name, string config)
        {
            NameValueCollection slconfig = new NameValueCollection();
            string[] pares = config.Split(';');
            for (int i = 0; i <= pares.Length - 2; i++)
            {
                string[] valores = pares[i].Split(':');
                slconfig[valores[0].ToString()] = valores[1].ToString();
            }
            //DataAccessProviderObject.Initialize(name, slconfig);

            //new AFP.AccesoDatos.Seguridad.DASqlRoleProvider().Initialize(name, config);




            DataAccessProviderObject.Initialize(name, slconfig);
        }

        public bool IsUserInRole(string username, string roleName)
        {
            return DataAccessProviderObject.IsUserInRole(username, roleName);
        }

        public string[] GetRolesForUser(string username)
        {
            return DataAccessProviderObject.GetRolesForUser(username);
        }

        public void CreateRole(string roleName)
        {
            DataAccessProviderObject.CreateRole(roleName);
        }
        public void CreateRole(string roleName, string RolCodigo)//, string nemoRol)
        {
            //DataAccessProviderObject.CreateRole(roleName, RolCodigo);//, nemoRol);
        }

        public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return DataAccessProviderObject.DeleteRole(roleName, throwOnPopulatedRole);
        }

        public bool RoleExists(string roleName)
        {
            return DataAccessProviderObject.RoleExists(roleName);
        }

        public void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            DataAccessProviderObject.AddUsersToRoles(usernames, roleNames);
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            DataAccessProviderObject.RemoveUsersFromRoles(usernames, roleNames);
        }

        public string[] GetUsersInRole(string roleName)
        {
          return   DataAccessProviderObject.GetUsersInRole(roleName);
        }

        public string[] GetAllRoles()
        {
            return DataAccessProviderObject.GetAllRoles();
        }

        public virtual List<BERol> GetAllRolesList()
        {
            return DataAccessProviderObject.GetAllRolesList();
        }
        public List<BERol> GetAllRolesList(string username)
        {
            return DataAccessProviderObject.GetAllRolesList(username);
        }

        public string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return DataAccessProviderObject.FindUsersInRole(roleName, usernameToMatch);
        }

        public string ApplicationName
        {
            get { return DataAccessProviderObject.ApplicationName; }
            set { DataAccessProviderObject.ApplicationName = value; }
        }
        public bool UpdateRoleName(int idRol, string RoleName, string codigo)
        {
            return DataAccessProviderObject.UpdateRoleName(idRol, RoleName, codigo);
        }
        public bool AddUserToRol(string UserName, string RoleName)
        {
            return DataAccessProviderObject.AddUserToRol(UserName, RoleName);
        }
        public bool AddUserToRol(int UserId, int RolId)
        {
            return DataAccessProviderObject.AddUserToRol(UserId, RolId);
        }
        public bool RemoveUserFromRol(int UserId, int RolId)
        {
            return DataAccessProviderObject.RemoveUserFromRol(UserId, RolId);
        }
        public bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
        {
            return DataAccessProviderObject.UpdateUserName(NewUserName, OldUserName, RoleName);
        }

        public List<BERol> getRolById(int id)
        {
            return DataAccessProviderObject.GetRolbyId(id);
        }
        public List<BESistema> GetSistemasHabliesPorRol(int idRol)
        {
            return DataAccessProviderObject.GetSistemasHabliesPorRol(idRol);
        }
        public int GetRolIdByName(string Name)
        {
            return DataAccessProviderObject.GetRolIdByName(Name);
        }
        public List<BERol> GetRolLikeName(string Name)
        {
            return DataAccessProviderObject.GetRolLikeName(Name);
        }
        public bool AddSistemaToRol(int SistemaId, int RolId)
        {
            return DataAccessProviderObject.AddSistemaToRol(SistemaId, RolId);
        }
        public bool RemoveSistemaFromRol(int SistemaId, int RolId)
        {
            return DataAccessProviderObject.RemoveSistemaFromRol(SistemaId, RolId);
        }
        public List<BERol> GetRolLikeNameForSistema(string Name, int SistemaId)
        {
            return DataAccessProviderObject.GetRolLikeNameForSistema(Name, SistemaId);
        }
        public List<BERol> GetRolLikeNameForSistemaRelated(string Name, int SistemaId)
        {
            return DataAccessProviderObject.GetRolLikeNameForSistemaRelated(Name, SistemaId);
        }
        public List<BERol> GetRolBySistema(int SistemaId)
        {
            return DataAccessProviderObject.GetRolBySistema(SistemaId);
        }
        public List<BERol> GetRolForUserId(int UserId)
        {
            return DataAccessProviderObject.GetRolForUserId(UserId);
        }
        public bool AddSiteMapToRol(int SiteMapId, int RolId)
        {
            return DataAccessProviderObject.AddSiteMapToRol(SiteMapId, RolId);
        }
        public bool RemoveSiteMapFromRol(int SiteMapId, int RolId)
        {
            return DataAccessProviderObject.RemoveSiteMapFromRol(SiteMapId, RolId);
        }
        public List<BERol> GetRolLikeNameForSistemaSiteMap(string Name, int SistemaId, int SiteMapId)
        {
            return DataAccessProviderObject.GetRolLikeNameForSistemaSiteMap(Name, SistemaId, SiteMapId);
        }
        public List<BERol> GetRolLikeNameForSistemaSiteMapRelated(string Name, int SistemaId, int SiteMapId)
        {
            return DataAccessProviderObject.GetRolLikeNameForSistemaSiteMapRelated(Name, SistemaId, SiteMapId);
        }
        public List<BERol> GetRolLikeNameForSiteMap(string Name, int SiteMapId)
        {
            return DataAccessProviderObject.GetRolLikeNameForSiteMap(Name, SiteMapId);
        }
        public List<BERol> GetRolForSiteMap(int SiteMapId)
        {
            return DataAccessProviderObject.GetRolForSiteMap(SiteMapId);
        }

        //public void Initialize(string name, NameValueCollection config)
        //{
        //    DataAccessProviderObject.Initialize(name, config);
        //    //new AFP.AccesoDatos.Seguridad.DASqlRoleProvider().Initialize(name, config);
        //}     

        //public bool IsUserInRole(string username, string roleName)
        //{
        //    return DataAccessProviderObject.IsUserInRole(username, roleName);
        //}       

        //public  string[] GetRolesForUser(string username)
        //{
        //    return DataAccessProviderObject.GetRolesForUser(username);
        //}
       
        //public  void CreateRole(string roleName)
        //{
        //    DataAccessProviderObject.CreateRole(roleName);
        //}


        //public  bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        //{
        //    return DataAccessProviderObject.DeleteRole(roleName, throwOnPopulatedRole);
        //}     

        //public  bool RoleExists(string roleName)
        //{
        //    return DataAccessProviderObject.RoleExists(roleName);
        //}     

        //public  void AddUsersToRoles(string[] usernames, string[] roleNames)
        //{
        //    DataAccessProviderObject.AddUsersToRoles(usernames, roleNames);
        //}      

        //public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        //{
        //    DataAccessProviderObject.RemoveUsersFromRoles(usernames, roleNames);
        //}

        //public  string[] GetUsersInRole(string roleName)
        //{
        //    return DataAccessProviderObject.GetUsersInRole(roleName);
        //}

        //public  string[] GetAllRoles()
        //{
        //    return DataAccessProviderObject.GetAllRoles();
        //}

        //public List<BERol> GetAllRolesList()
        //{
        //    return DataAccessProviderObject.GetAllRolesList();
        //}
        //public List<BERol> GetAllRolesList(string username)
        //{
        //    return DataAccessProviderObject.GetAllRolesList(username);
        //}
      
        //public  string[] FindUsersInRole(string roleName, string usernameToMatch)
        //{
        //    return DataAccessProviderObject.FindUsersInRole(roleName, usernameToMatch);
        //}       

        //public  string ApplicationName
        //{
        //    get { return DataAccessProviderObject.ApplicationName; }
        //    set { DataAccessProviderObject.ApplicationName = value; }
        //}
        //public bool UpdateRoleName(string NewRoleName, string OldRoleName, int PermisoHastaSeccion, string NombreSeccion)
        //{
        //    return DataAccessProviderObject.UpdateRoleName(NewRoleName, OldRoleName, PermisoHastaSeccion, NombreSeccion);
        //}
        //public bool AddUserToRol(string UserName, string RoleName)
        //{
        //    return DataAccessProviderObject.AddUserToRol(UserName, RoleName);
        //}
        //public bool RemoveUserFromRol(string UserName, string RoleName)
        //{
        //    return DataAccessProviderObject.RemoveUserFromRol(UserName, RoleName);
        //}
        //public bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
        //{
        //    return DataAccessProviderObject.UpdateUserName(NewUserName, OldUserName, RoleName);
        //}

        //public List<BERol> getRolById(int id)
        //{
        //    return DataAccessProviderObject.GetRolIdByName(id);
        //}
    }
}
