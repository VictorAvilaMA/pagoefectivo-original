using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using _3Dev.FW.AccesoDatos.Seguridad;
using _3Dev.FW.Entidades.Seguridad;

namespace _3Dev.FW.Negocio.Seguridad
{
    [Serializable]
    public class BLUsuario:BLMaintenanceBase
    {
        //private DAUsuario dataAccessProviderObject = null;

        //private void DoConstructorDataAccessObject()
        //{
        //    if (dataAccessProviderObject == null)
        //        dataAccessProviderObject = GetConstructorDataAccessObject();
        //}
        public virtual DAUsuario GetConstructorDataAccessObject()
        {
            return null;
        }
        public virtual DAUsuario DataAccessProviderObject
        {
            get
            {
               // DoConstructorDataAccessObject();
                return GetConstructorDataAccessObject();
            }
        }
        public virtual int RegistrarUsuario(BEUsuario Usuario)
        {
            return DataAccessProviderObject.RegistrarUsuario(Usuario);
        }
        public List<BEUsuario> GetAllUsuarioList()
        {
            return DataAccessProviderObject.GetAllUsuarioList();
        }
        public int ModificarUsuario(BEUsuario eUsuario)
        {
            return DataAccessProviderObject.ModificarUsuario(eUsuario);
        }
        public int EliminarUsuario(int IdUsuario)
        {

            return DataAccessProviderObject.EliminarUsuario(IdUsuario);
        }
        public BEUsuario GetUsuarioById(int IdUsuario)
        {
            return DataAccessProviderObject.GetUsuarioById(IdUsuario);
        }
        public List<BEUsuario> GetUsuariosByFiltros(BEUsuario eUsuario)
        {
            return DataAccessProviderObject.GetUsuariosByFiltros(eUsuario);
        }
        public int IsUsuarioInRol(int IdUsuario)
        {
            return DataAccessProviderObject.IsUsuarioInRol(IdUsuario);
        }
        public List<BERolesByUsuario> GetRolesByUsuario(int IdUsuario)
        {
            return DataAccessProviderObject.GetRolesByUsuario(IdUsuario);
        }
        public List<BERol> GetRolesBySistema(int IdSistema)
        {
            return DataAccessProviderObject.GetRolesBySistema(IdSistema);
        }
        public int AddUserToRol(int IdUsuario, int IdRol)
        {
            return DataAccessProviderObject.AddUserToRol(IdUsuario, IdRol);
        }
        public int RemoveRolToUser(int id)
        {
            return DataAccessProviderObject.RemoveRolToUser(id);
        }
        //public DataTable GetGuids(string userName)
        //{
        //    return new DAUsuario().GetGuids(userName);
        //}
        public List<BERol> GetRoles(Int32 userId, int idSistema)
        {
            return DataAccessProviderObject.GetRoles(userId, idSistema);
        }
        public List<BEPregunta> GetPreguntas()
        {
            return DataAccessProviderObject.GetPreguntas();
        }

        public bool SendMail(string usrname, int idpreg, string nomresp)
        {
            CustomMembershipProvider ocustom = new CustomMembershipProvider();
            string mail=ocustom.SendMailForgotPass(usrname, idpreg, nomresp);
            if (mail != string.Empty)
                try
                {
                    MailMessage msg = new MailMessage();
                    msg.To.Add(mail);
                    msg.From = new MailAddress(ConfigurationManager.AppSettings.Get("From"));
                    msg.Subject = "Seguridad, envio de contraseña";
                    msg.Body = ocustom.GetPassword(usrname,null);
                    msg.Priority = MailPriority.High;
                    msg.IsBodyHtml = true;
                    msg.BodyEncoding = Encoding.Unicode;
                    string SmtpServer = ConfigurationManager.AppSettings.Get("ServerSmtp");
                    SmtpClient cliente = new SmtpClient();
                    cliente.Host = SmtpServer;
                    cliente.Send(msg);
                    return true;

                }
                catch (OverflowException ov)
                {
                    throw ov;
                }
                catch (SmtpException sm)
                {
                    throw sm;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            else
                return false;

        }
        private string ArmarCorreo(string usrname)
        {

            //string pass=new CustomMembershipProvider().GetPassword(usrname, null);
            //StringBuilder sb = new StringBuilder();
            //sb.Append("<html>Envio de contraseña<br>");
            //sb.Append("Codigo de usuario: " + usrname + "<br>");
            //sb.Append("contraseña de usuario: " + pass + "<br>");
            //sb.Append("<br>");
            //sb.Append("Interseguro Seguros de Vida y Jubilación<br></html>");
            //return sb.ToString();
            return "";
        }

        //public List<BETipoDocumento> GetTiposDocumentos()
        //{
        //    return new DAUsuario().GetTiposDocumentos();
        //}
        public BEUserInfo GetUserInfoByUserName(string userName)
        {
            return DataAccessProviderObject.GetUserInfoByUserName(userName);
        }
        public bool ConfirmUserValidation(string email, string guid)
        {
            return DataAccessProviderObject.ConfirmUserValidation(email, guid);
        }
        public List<BEUsuario> GetUsuarioLikeNameForRol(string Name, int RolId)
        {
            return DataAccessProviderObject.GetUsuarioLikeNameForRol(Name, RolId);
        }
        //MODIFICADO PARA PAGINACION FASE III
        public List<BEUsuario> GetUsuarioLikeName(string Name,_3Dev.FW.Entidades.BusinessEntityBase entidad )
        {
            return DataAccessProviderObject.GetUsuarioLikeName(Name,entidad);
        }
    }
}
