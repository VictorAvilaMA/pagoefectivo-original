using System;
using System.Collections.Generic;
using _3Dev.FW.Entidades;
using _3Dev.FW.Negocio;
namespace _3Dev.FW.ServidorRemoting
{
    public class BFServerMaintenaceBase : BFServerBase, _3Dev.FW.EmsambladoComun.IMaintenanceBase
    {
        private BLMaintenanceBase businessLogicObject = null;

        private void DoConstructorBusinessLogicObject()
        {
            if (businessLogicObject == null)
            businessLogicObject = GetConstructorBusinessLogicObject();
        }
        public virtual BLMaintenanceBase GetConstructorBusinessLogicObject()
        {
            return new BLMaintenanceBase();
            //throw new NotImplementedException("GetConstructorDataAccessObject isn't Implemented");
        }
        public virtual BLMaintenanceBase BusinessLogicObject
        {
            get
            {
                DoConstructorBusinessLogicObject();
                return businessLogicObject;
            }
        }

        public virtual int InsertRecord(BusinessEntityBase be)
        {
            return BusinessLogicObject.InsertRecord(be);
        }
        public virtual int UpdateRecord(BusinessEntityBase be)
        {
            return BusinessLogicObject.UpdateRecord(be);
        }
        public virtual BusinessEntityBase GetRecordByID(object id)
        {
            return BusinessLogicObject.GetRecordByID(id);
        }
        [Obsolete]
        public virtual int DeleteRecord(BusinessEntityBase be)
        {
            return BusinessLogicObject.DeleteRecord(be);
        }
        public virtual int DeleteRecordByEntityId(object entityId)
        {
            return BusinessLogicObject.DeleteRecordByEntityId(entityId);
        }
        public virtual List<BusinessEntityBase> SearchByParameters(BusinessEntityBase be)
        {
            return BusinessLogicObject.SearchByParameters(be);
        }
        public virtual List<BusinessEntityBase> SearchByParametersOrderedBy(BusinessEntityBase be, string orderedBy, bool isAccending)
        {
            return BusinessLogicObject.SearchByParametersOrderedBy(be, orderedBy, isAccending);
        }
        public virtual object GetObjectByParameters(string key, BusinessEntityBase be)
        {
            return BusinessLogicObject.GetObjectByParameters(key, be);
        }
        public virtual List<BusinessEntityBase> GetListByParameters(string key, BusinessEntityBase be)
        {
            return BusinessLogicObject.GetListByParameters(key, be);
        }
        public virtual List<BusinessEntityBase> GetListByParametersOrderedBy(string key, BusinessEntityBase be, string orderedBy, bool isAccending)
        {
            return BusinessLogicObject.GetListByParametersOrderedBy(key, be, orderedBy, isAccending);
        }
        public virtual BusinessEntityBase GetBusinessEntityByParameters(string key, BusinessEntityBase be)
        {
            return BusinessLogicObject.GetBusinessEntityByParameters(key, be);
        }

        public virtual int ExecProcedureByParameters(string key, BusinessEntityBase be)
        {
            return BusinessLogicObject.ExecProcedureByParameters(key, be);
        }

        #region entity methods
        public virtual BusinessMessageBase GetEntityByID(object id)
        {
            return BusinessLogicObject.GetEntityByID(id);
        }
        public virtual BusinessMessageBase GetEntity(BusinessMessageBase request)
        {
            return BusinessLogicObject.GetEntity(request);
        }
        public virtual BusinessMessageBase DeleteEntity(BusinessMessageBase request)
        {
            return BusinessLogicObject.DeleteEntity(request);
        }
        public virtual BusinessMessageBase SaveEntity(BusinessMessageBase request)
        {
            return BusinessLogicObject.SaveEntity(request);
        }
        #endregion

        public override void DoDispose()
        {
            if (BusinessLogicObject != null)
                BusinessLogicObject.Dispose();
            base.DoDispose();
        }
    }
}
