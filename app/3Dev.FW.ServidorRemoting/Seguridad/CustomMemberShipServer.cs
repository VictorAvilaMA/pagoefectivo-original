using System;
using _3Dev.FW.Negocio.Seguridad;
using System.ServiceModel.Activation;


namespace _3Dev.FW.ServidorRemoting.Seguridad
{

    //[AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Required)> _    
    [AspNetCompatibilityRequirements( RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed  ) ]
    public class CustomMemberShipServer : MarshalByRefObject, _3Dev.FW.EmsambladoComun.Seguridad.ICustomMembershipProvider
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }
        //private BLCustomMembershipProvider businessLogicProviderObject = null;

        //private void DoConstructorBusinessLogicObject()
        //{
        //    if (businessLogicProviderObject == null)
        //        businessLogicProviderObject = GetConstructorBusinessProviderObject();
        //}
        public virtual BLCustomMembershipProvider GetConstructorBusinessProviderObject()
        {
            return null;
        }
        public virtual BLCustomMembershipProvider BusinessLogicProviderObject
        {
            get
            {
                //DoConstructorBusinessLogicObject();
                return GetConstructorBusinessProviderObject();
            }
        }



        #region ICustomMembershipProvider Members

        public void Initialize(string name, string config)
        {
            BusinessLogicProviderObject.Initialize(name, config);
        }

        public bool ValidateUser(string username, string password)
        {
            return BusinessLogicProviderObject.ValidateUser(username, password);
        }
        public virtual bool ValidateUserAndPass(string username, string password)
        {
            return BusinessLogicProviderObject.ValidateUserAndPass(username, password);
        }
      
        public bool CheckPassword(string username, string password)
        {
            return BusinessLogicProviderObject.CheckPassword(username, password);
        }



        public bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            return BusinessLogicProviderObject.ChangePassword(username, oldPwd, newPwd);
        }

        public bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
        {
            return BusinessLogicProviderObject.ChangePasswordQuestionAndAnswer(username, password, newPwdQuestion, newPwdAnswer);
        }

       

   


        public void ActualizarUsuario(string telefonofijo, string celular, System.Web.Security.MembershipUser user)
        {
            BusinessLogicProviderObject.ActualizarUsuario(telefonofijo, celular, user);
        }

   

        public System.Web.Security.MembershipUser CreateUser(string username, string password, string primNombre, string appPaterno, string appMaterno, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            return BusinessLogicProviderObject.CreateUser(username, password, primNombre, appPaterno, appMaterno, isApproved, providerUserKey, out status);
        }

        public virtual bool ExistUser(string username)
        {
            return BusinessLogicProviderObject.ExistUser(username);
        }
        #endregion
    }
}
