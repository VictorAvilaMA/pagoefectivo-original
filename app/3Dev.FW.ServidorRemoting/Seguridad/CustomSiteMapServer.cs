using System;
using System.Collections.Generic;
using System.Data;
using _3Dev.FW.EmsambladoComun.Seguridad;
using _3Dev.FW.Entidades.Seguridad;
using _3Dev.FW.Negocio.Seguridad;
using System.ServiceModel.Activation;

namespace _3Dev.FW.ServidorRemoting.Seguridad
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CustomSiteMapServer : MarshalByRefObject, ICustomSiteMapProvider
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }
        //private BLCustomSiteMapProvider businessLogicProviderObject = null;

        //private void DoConstructorBusinessLogicObject()
        //{
        //    if (businessLogicProviderObject == null)
        //        businessLogicProviderObject = GetConstructorBusinessProviderObject();
        //}
        public virtual BLCustomSiteMapProvider GetConstructorBusinessProviderObject()
        {
            return null;
        }
        public virtual BLCustomSiteMapProvider BusinessLogicProviderObject
        {
            get
            {
              //  DoConstructorBusinessLogicObject();
                return GetConstructorBusinessProviderObject();
            }
        }
        public List<BESiteMap> BuildSiteMap(int idSistema)
        {
            return BusinessLogicProviderObject.BuildSiteMap(idSistema);
        }
        public bool UpdateRoleInSiteMap(int IdSiteMap, int IdRol)
        {
            return BusinessLogicProviderObject.UpdateRoleInSiteMap(IdSiteMap, IdRol);
        }
        public DataTable GetPagesValidMenu(int IdSistema)
        {
            return BusinessLogicProviderObject.GetPagesValidMenu(IdSistema);
        }
        public DataTable GetControlOptionsByPage(string PageName)
        {
            return BusinessLogicProviderObject.GetControlOptionsByPage(PageName);
        }

        public int GetIdSistemaByIDSiteMapNode(int idSiteMapNode)
        {
            return BusinessLogicProviderObject.GetIdSistemaByIDSiteMapNode(idSiteMapNode);
        }

        public string GetCabezeraMenu(string urlPagina)
        { 
            return BusinessLogicProviderObject.GetCabezeraMenu(urlPagina);
        }
        #region ICustomSiteMapProvider Members
        
        public bool UpdateRoleInSiteMap(int IdSiteMap, int IdSistema, string Roles)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<BESiteMap> GetPagesLikNameForSistema(string Name, int idSistema)
        {
            return BusinessLogicProviderObject.GetPagesLikNameForSistema(Name, idSistema);
        }

        public BESiteMap GetSiteMapById(int IdSiteMap)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<BESiteMap> GetSiteMapWithURLBySistema(int idSistema)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool AddControlToSitemap(int SiteMapId, int ControlId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool RemoveControlToSitemap(int SiteMapId, int ControlId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<BESiteMap> GetMenus(int idSistema, int idSiteMap)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<BESiteMap> GetSiteMapBySistemaId(int idSistema)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int AddSitemap(BESiteMap beSiteMap)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool UpdateSitemap(BESiteMap beSiteMap)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool UpdateSitemapRoles(int SiteMapId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public BESiteMap GetSiteMapBySistemaURL(string URL, int SistemaId, int ParentId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public BESiteMap GetSiteMapByTitleParent(string Title, int ParentId)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<BESiteMap> GetSiteMapsByIdRol(int idRol)
        {
            return BusinessLogicProviderObject.GetSiteMapsByIdRol(idRol);
        }
        #endregion
    }
}
