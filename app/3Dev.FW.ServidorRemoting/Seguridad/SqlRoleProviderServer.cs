using System;
using System.Collections.Generic;
using _3Dev.FW.EmsambladoComun.Seguridad;
using _3Dev.FW.Entidades.Seguridad;
using _3Dev.FW.Negocio.Seguridad;
using System.ServiceModel.Activation;

namespace _3Dev.FW.ServidorRemoting.Seguridad
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
     public class SqlRoleProviderServer:MarshalByRefObject ,   ISqlRoleProvider 
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }
         //private BLSqlRoleProvider businessLogicProviderObject = null;

        //private void DoConstructorBusinessLogicObject()
        //{
        //    if (businessLogicProviderObject == null)
        //        businessLogicProviderObject = GetConstructorBusinessProviderObject();
        //}
         public virtual BLSqlRoleProvider GetConstructorBusinessProviderObject()
        {
            return null;
        }
         public virtual BLSqlRoleProvider BusinessLogicProviderObject
        {
            get
            {
                //DoConstructorBusinessLogicObject();
                return GetConstructorBusinessProviderObject();
            }
        }
        #region "..."
        //public void Initialize(string name, NameValueCollection config)
        //{
        //    BusinessLogicProviderObject.Initialize(name, config);
        //}

        //public bool IsUserInRole(string username, string roleName)
        //{
        //    return BusinessLogicProviderObject.IsUserInRole(username, roleName);
        //}

        //public string[] GetRolesForUser(string username)
        //{
        //    return BusinessLogicProviderObject.GetRolesForUser(username);
        //}

        //public void CreateRole(string roleName)
        //{
        //    BusinessLogicProviderObject.CreateRole(roleName);
        //}
        //public void CreateRole(string roleName, string RolCodigo, string NemoRol)
        //{
        //    BusinessLogicProviderObject.CreateRole(roleName, RolCodigo, NemoRol);
        //}
        //public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        //{
        //    return BusinessLogicProviderObject.DeleteRole(roleName, throwOnPopulatedRole);
        //}

        //public bool RoleExists(string roleName)
        //{
        //    return BusinessLogicProviderObject.RoleExists(roleName);
        //}

        //public void AddUsersToRoles(string[] usernames, string[] roleNames)
        //{
        //    BusinessLogicProviderObject.AddUsersToRoles(usernames, roleNames);
        //}

        //public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        //{
        //    BusinessLogicProviderObject.RemoveUsersFromRoles(usernames, roleNames);
        //}

        //public string[] GetUsersInRole(string roleName)
        //{
        //    return BusinessLogicProviderObject.GetUsersInRole(roleName);
        //}

        //public string[] GetAllRoles()
        //{
        //    return BusinessLogicProviderObject.GetAllRoles();
        //}

        // public List<BERol> GetAllRolesList()
        // {
        //     return BusinessLogicProviderObject.GetAllRolesList();
        // }
        // public List<BERol> GetAllRolesList(string username)
        // {
        //     return BusinessLogicProviderObject.GetAllRolesList(username);
        // }
        //public string[] FindUsersInRole(string roleName, string usernameToMatch)
        //{
        //    return BusinessLogicProviderObject.FindUsersInRole(roleName, usernameToMatch);
        //}

        //public string ApplicationName
        //{
        //    get { return BusinessLogicProviderObject.ApplicationName; }
        //    set { BusinessLogicProviderObject.ApplicationName = value; }
        //}
        // public bool UpdateRoleName(string NewRoleName, string OldRoleName, int PermisoHastaSeccion, string NombreSeccion)
        // {

        //     return BusinessLogicProviderObject.UpdateRoleName(NewRoleName, OldRoleName, PermisoHastaSeccion, NombreSeccion);
        // }
        // public bool AddUserToRol(string UserName, string RoleName)
        // {
        //     return BusinessLogicProviderObject.AddUserToRol(UserName, RoleName);
        // }
        // public bool RemoveUserFromRol(string UserName, string RoleName)
        // {
        //     return BusinessLogicProviderObject.RemoveUserFromRol(UserName, RoleName);
        // }
        // public bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
        // {
        //     return BusinessLogicProviderObject.UpdateUserName(NewUserName, OldUserName, RoleName);
        // }


        // List<BERol> ISqlRoleProvider.getRolById(int id)
        // {
        //     return BusinessLogicProviderObject.getRolById(id);
        // }
        #endregion


        #region ISqlRoleProvider Members

         public void Initialize(string name, string config)
        {
            BusinessLogicProviderObject.Initialize(name, config);
            //throw new Exception("The method or operation is not implemented.");
        }

        public bool IsUserInRole(string username, string roleName)
        {
        return    BusinessLogicProviderObject.IsUserInRole(username, roleName);
        }

        public string[] GetRolesForUser(string username)
        {
            return BusinessLogicProviderObject.GetRolesForUser(username);
        }

        public void CreateRole(string roleName)
        {
             BusinessLogicProviderObject.CreateRole(roleName);
        }

        public void CreateRole(string roleName, string RoleCodigo )
        {
            BusinessLogicProviderObject.CreateRole(roleName, RoleCodigo);
        }

        public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            return BusinessLogicProviderObject.DeleteRole(roleName, throwOnPopulatedRole);
        }

        public bool RoleExists(string roleName)
        {
           return  BusinessLogicProviderObject.RoleExists(roleName);
        }

        public void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            BusinessLogicProviderObject.AddUsersToRoles(usernames, roleNames);
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            BusinessLogicProviderObject.RemoveUsersFromRoles(usernames, roleNames);
        }

        public string[] GetUsersInRole(string roleName)
        {
            return BusinessLogicProviderObject.GetUsersInRole(roleName);
        }

        public string[] GetAllRoles()
        {
            return BusinessLogicProviderObject.GetAllRoles();
        }

        public List<BERol> GetAllRolesList()
        {
            return BusinessLogicProviderObject.GetAllRolesList();
        }

        public List<BERol> GetAllRolesList(string username)
        {
            return BusinessLogicProviderObject.GetAllRolesList(username);
        }

        public string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            return BusinessLogicProviderObject.FindUsersInRole(roleName, usernameToMatch);
        }

        public string ApplicationName
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool UpdateRoleName(string NewRoleName, string OldRoleName, int PermisoHastaSeccion, string NombreSeccion)
        {
            //return BusinessLogicProviderObject.UpdateRoleName(NewRoleName, OldRoleName, PermisoHastaSeccion, NombreSeccion);
            return false ;
        }

        public bool AddUserToRol(string UserName, string RoleName)
        {
            return BusinessLogicProviderObject.AddUserToRol(UserName, RoleName);
        }

        //public bool RemoveUserFromRol(string UserName, string RoleName)
        //{
        //    return BusinessLogicProviderObject.RemoveUserFromRol(UserName, RoleName);
        //}

        public bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
        {
            return BusinessLogicProviderObject.UpdateUserName(NewUserName, OldUserName, RoleName);
        }

        public List<BERol> getRolById(int id)
        {
            return BusinessLogicProviderObject.getRolById(id);
        }

        //public void InitializeSqlRoleProvider(string name, string config)
        //{
        //    return BusinessLogicProviderObject.InitializeSqlRoleProvider(name, config);
        //}

        //public void CreateRole2(string roleName, string RoleCodigo)
        //{
        //    return BusinessLogicProviderObject.CreateRole2(roleName, RoleCodigo);
        //}

        //public List<BERol> GetAllRolesList2(string username)
        //{
        //    return BusinessLogicProviderObject.GetAllRolesList2(username);
        //}

        public string ApplicationNameSqlRoleProvider
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool UpdateRoleName(int idRol, string RoleName, string codigo)
        {
            return BusinessLogicProviderObject.UpdateRoleName(idRol, RoleName, codigo);
        }

        //public bool AddUserToRolSqlRoleProvider(string UserName, string RoleName)
        //{
        //    return BusinessLogicProviderObject.AddUserToRolSqlRoleProvider(UserName, RoleName);
        //}

        public bool RemoveUserFromRol(int UserId, int RolId)
        {
            return BusinessLogicProviderObject.RemoveUserFromRol(UserId, RolId);
        }

        public List<BESistema> GetSistemasHabliesPorRol(int idRol)
        {
            return BusinessLogicProviderObject.GetSistemasHabliesPorRol(idRol);
        }

        public int GetRolIdByName(string Name)
        {
            return BusinessLogicProviderObject.GetRolIdByName(Name);
        }

        public List<BERol> GetRolLikeName(string Name)
        {
            return BusinessLogicProviderObject.GetRolLikeName(Name);
        }

        public bool AddUserToRol(int UserId, int RolId)
        {
            return BusinessLogicProviderObject.AddUserToRol(UserId, RolId);
        }

        public bool AddSistemaToRol(int SistemaId, int RolId)
        {
            return BusinessLogicProviderObject.AddSistemaToRol(SistemaId, RolId);
        }

        public bool RemoveSistemaFromRol(int SistemaId, int RolId)
        {
            return BusinessLogicProviderObject.RemoveSistemaFromRol(SistemaId, RolId);
        }

        public List<BERol> GetRolLikeNameForSistema(string Name, int SistemaId)
        {
            return BusinessLogicProviderObject.GetRolLikeNameForSistema(Name, SistemaId);
        }

        public List<BERol> GetRolLikeNameForSistemaRelated(string Name, int SistemaId)
        {
            return BusinessLogicProviderObject.GetRolLikeNameForSistemaRelated(Name, SistemaId);
        }

        public List<BERol> GetRolBySistema(int SistemaId)
        {
            return BusinessLogicProviderObject.GetRolBySistema(SistemaId);
        }

        public List<BERol> GetRolForUserId(int UserId)
        {
            return BusinessLogicProviderObject.GetRolForUserId(UserId);
        }

        public bool AddSiteMapToRol(int SiteMapId, int RolId)
        {
            return BusinessLogicProviderObject.AddSiteMapToRol(SiteMapId, RolId);
        }

        public bool RemoveSiteMapFromRol(int SiteMapId, int RolId)
        {
            return BusinessLogicProviderObject.RemoveSiteMapFromRol(SiteMapId, RolId);
        }

        public List<BERol> GetRolLikeNameForSistemaSiteMap(string Name, int SistemaId, int SiteMapId)
        {
            return BusinessLogicProviderObject.GetRolLikeNameForSistemaSiteMap(Name, SistemaId, SiteMapId);
        }

        public List<BERol> GetRolLikeNameForSistemaSiteMapRelated(string Name, int SistemaId, int SiteMapId)
        {
            return BusinessLogicProviderObject.GetRolLikeNameForSistemaSiteMapRelated(Name, SistemaId, SiteMapId);
        }

        public List<BERol> GetRolLikeNameForSiteMap(string Name, int SiteMapId)
        {
            return BusinessLogicProviderObject.GetRolLikeNameForSiteMap(Name, SiteMapId);
        }

        public List<BERol> GetRolForSiteMap(int SiteMapId)
        {
            return BusinessLogicProviderObject.GetRolForSiteMap(SiteMapId);
        }

        #endregion

        #region ISqlRoleProvider Members


        public bool RemoveUserFromRol(string UserName, string RoleName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool AddUserToRolSqlRoleProvider(string UserName, string RoleName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
