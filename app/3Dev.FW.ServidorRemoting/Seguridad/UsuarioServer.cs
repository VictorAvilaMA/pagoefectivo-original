using System.Collections.Generic;
using _3Dev.FW.EmsambladoComun.Seguridad;
using _3Dev.FW.Entidades.Seguridad;
using _3Dev.FW.Negocio.Seguridad;
using System.ServiceModel.Activation;


namespace _3Dev.FW.ServidorRemoting.Seguridad
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UsuarioServer : BFServerBase,IUsuario
    {
        //public override object InitializeLifetimeService()
        //{
        //    return null;
        //}
        private BLUsuario businessLogicProviderObject = null;

        private void DoConstructorBusinessLogicObject()
        {
            if (businessLogicProviderObject == null)
                businessLogicProviderObject = GetConstructorBusinessProviderObject();
        }
        public virtual BLUsuario GetConstructorBusinessProviderObject()
        {
            return null;
        }
        public virtual BLUsuario BusinessLogicProviderObject
        {
            get
            {
                //DoConstructorBusinessLogicObject();
                return GetConstructorBusinessProviderObject();
            }
        }

        public int RegistrarUsuario(BEUsuario Usuario)
        {
            return BusinessLogicProviderObject.RegistrarUsuario(Usuario);
        }
        public List<BEUsuario> GetAllUsuarioList()
        {
            return BusinessLogicProviderObject.GetAllUsuarioList();
        }
        public int ModificarUsuario(BEUsuario eUsuario)
        {
            return BusinessLogicProviderObject.ModificarUsuario(eUsuario);
        }
        public BEUsuario GetUsuarioById(int IdUsuario)
        {
            return BusinessLogicProviderObject.GetUsuarioById(IdUsuario);
        }
        //public BEUsuario GetUsuarioById(int IdUsuario)
        //{
        //    return new BLUsuario().GetUsuarioById(IdUsuario);
        //}
        public int EliminarUsuario(int IdUsuario)
        {
            return BusinessLogicProviderObject.EliminarUsuario(IdUsuario);
        }
        public List<BEUsuario> GetUsuariosByFiltros(BEUsuario eUsuario)
        {
            return BusinessLogicProviderObject.GetUsuariosByFiltros(eUsuario);
        }
        public int IsUsuarioInRol(int IdUsuario)
        {
            return BusinessLogicProviderObject.IsUsuarioInRol(IdUsuario);
        }
        public List<BERolesByUsuario> GetRolesByUsuario(int IdUsuario)
        {
            return BusinessLogicProviderObject.GetRolesByUsuario(IdUsuario);
        }
        public List<BERol> GetRolesBySistema(int IdSistema)
        {
            return BusinessLogicProviderObject.GetRolesBySistema(IdSistema);
            
        }
        public int AddUserToRol(int IdUsuario, int IdRol)
        {
            return BusinessLogicProviderObject.AddUserToRol(IdUsuario, IdRol);
        }
        public int RemoveRolToUser(int Id)
        {
            return BusinessLogicProviderObject.RemoveRolToUser(Id);
        }



        public List<BERol> GetRoles(int userId, int idSistema)
        {
            return BusinessLogicProviderObject.GetRoles(userId, idSistema);
        }



        public List<BEPregunta> GetPreguntas()
        {
            return BusinessLogicProviderObject.GetPreguntas();
        }


        #region IUsuario Members


        public bool SendMail(string usrname, int idpreg, string nomresp)
        {
            return BusinessLogicProviderObject.SendMail(usrname, idpreg, nomresp);
        }

        public BEUserInfo GetUserInfoByUserName(string userName)
        {
            return BusinessLogicProviderObject.GetUserInfoByUserName(userName);
        }

        public bool ConfirmUserValidation(string email, string guid)
        {
            return BusinessLogicProviderObject.ConfirmUserValidation(email, guid);
        }
        #endregion
        public List<BEUsuario> GetUsuarioLikeNameForRol(string Name, int RolId)
        {
            return BusinessLogicProviderObject.GetUsuarioLikeNameForRol(Name, RolId);
        }
        //MODIFICADO PARA PAGINACION FASE III
        public List<BEUsuario> GetUsuarioLikeName(string Name, _3Dev.FW.Entidades.BusinessEntityBase entidad)
        {
            return BusinessLogicProviderObject.GetUsuarioLikeName(Name,entidad );
        }

    }
}
