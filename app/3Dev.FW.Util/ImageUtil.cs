﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _3Dev.FW.Util
{
    public class ImageUtil
    {
        public static void Save(Bitmap imageBitmap, string fullImagePath, ImageFormat format)
        {
            if (File.Exists(fullImagePath))
                File.Delete(fullImagePath);
            imageBitmap.Save(fullImagePath, format);
        }
        public static void SaveAsBmp(Bitmap imageBitmap, string fullImagePath)
        {
            Save(imageBitmap, fullImagePath, ImageFormat.Bmp);
        }
        public static void SaveAsJpeg(Bitmap imageBitmap, string fullImagePath)
        {
            Save(imageBitmap, fullImagePath, ImageFormat.Jpeg);
        }
        public static void SaveAsPng(Bitmap imageBitmap, string fullImagePath)
        {
            Save(imageBitmap, fullImagePath, ImageFormat.Png);
        }
        public static void SaveAsGif(Bitmap imageBitmap, string fullImagePath)
        {
            Save(imageBitmap, fullImagePath, ImageFormat.Gif);
        }
        public static void Save(Bitmap imageBitmap, string imagePath, string filename, ImageFormat format)
        {
            Save(imageBitmap, Path.Combine(imagePath, filename), format);
        }
        public static void SaveAsBmp(Bitmap imageBitmap, string imagePath, string filename)
        {
            SaveAsJpeg(imageBitmap, Path.Combine(imagePath, filename));
        }
        public static void SaveAsJpeg(Bitmap imageBitmap, string imagePath, string filename)
        {
            SaveAsBmp(imageBitmap, Path.Combine(imagePath, filename));
        }
        public static void SaveAsPng(Bitmap imageBitmap, string imagePath, string filename)
        {
            SaveAsPng(imageBitmap, Path.Combine(imagePath, filename));
        }
        public static void SaveAsGif(Bitmap imageBitmap, string imagePath, string filename)
        {
            SaveAsGif(imageBitmap, Path.Combine(imagePath, filename));
        }

        public static void Save(Stream imageStream, string fullImagePath, ImageFormat format)
        {
            using (Bitmap ImageBitMap = new Bitmap(imageStream))
            {
                Save(ImageBitMap, fullImagePath, format);
            }
        }
        public static void SaveAsBmp(Stream imageStream, string fullImagePath)
        {
            Save(imageStream, fullImagePath, ImageFormat.Bmp);
        }
        public static void SaveAsJpeg(Stream imageStream, string fullImagePath)
        {
            Save(imageStream, fullImagePath, ImageFormat.Jpeg);
        }
        public static void SaveAsPng(Stream imageStream, string fullImagePath)
        {
            Save(imageStream, fullImagePath, ImageFormat.Png);
        }
        public static void SaveAsGif(Stream imageStream, string fullImagePath)
        {
            Save(imageStream, fullImagePath, ImageFormat.Gif);
        }
        public static void Save(Stream imageStream, string imagePath, string filename, ImageFormat format)
        {
            Save(imageStream, Path.Combine(imagePath, filename), format);
        }
        public static void SaveAsBmp(Stream imageStream, string imagePath, string filename)
        {
            SaveAsBmp(imageStream, Path.Combine(imagePath, filename));
        }
        public static void SaveAsJpeg(Stream imageStream, string imagePath, string filename)
        {
            SaveAsJpeg(imageStream, Path.Combine(imagePath, filename));
        }
        public static void SaveAsPng(Stream imageStream, string imagePath, string filename)
        {
            SaveAsPng(imageStream, Path.Combine(imagePath, filename));
        }
        public static void SaveAsGif(Stream imageStream, string imagePath, string filename)
        {
            SaveAsGif(imageStream, Path.Combine(imagePath, filename));
        }

        public static void Save(Byte[] imageBytes, string fullImagePath, ImageFormat format)
        {
            using (Stream ImageStream = new MemoryStream(imageBytes))
            {
                Save(ImageStream, fullImagePath, fullImagePath, format);
            }
        }
        public static void SaveAsBmp(Byte[] imageBytes, string fullImagePath)
        {
            Save(imageBytes, fullImagePath, ImageFormat.Bmp);
        }
        public static void SaveAsJpeg(Byte[] imageBytes, string fullImagePath)
        {
            Save(imageBytes, fullImagePath, ImageFormat.Jpeg);
        }
        public static void SaveAsPng(Byte[] imageBytes, string fullImagePath)
        {
            Save(imageBytes, fullImagePath, ImageFormat.Png);
        }
        public static void SaveAsGif(Byte[] imageBytes, string fullImagePath)
        {
            Save(imageBytes, fullImagePath, ImageFormat.Gif);
        }
        public static void Save(Byte[] imageBytes, string imagePath, string filename, ImageFormat format)
        {
            Save(imageBytes, Path.Combine(imagePath, filename), format);
        }
        public static void SaveAsBmp(Byte[] imageBytes, string imagePath, string filename)
        {
            SaveAsBmp(imageBytes, Path.Combine(imagePath, filename));
        }
        public static void SaveAsJpeg(Byte[] imageBytes, string imagePath, string filename)
        {
            SaveAsJpeg(imageBytes, Path.Combine(imagePath, filename));
        }
        public static void SaveAsPng(Byte[] imageBytes, string imagePath, string filename)
        {
            SaveAsPng(imageBytes, Path.Combine(imagePath, filename));
        }
        public static void SaveAsGif(Byte[] imageBytes, string imagePath, string filename)
        {
            SaveAsGif(imageBytes, Path.Combine(imagePath, filename));
        }

        public static Bitmap Resize(Bitmap imageBitmap, int width, int height, Color backgroundColor)
        {
            Image Canvas = new Bitmap(width, height);
            using (Graphics Graphic = Graphics.FromImage(Canvas))
            {
                Graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                Graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                Graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                Graphic.Clear(backgroundColor);
                int X = 0, Y = 0;
                float Percent = 0,
                    PercentWidth = (float)width / (float)imageBitmap.Width,
                    PercentHeight = (float)height / (float)imageBitmap.Height;
                if (PercentHeight < PercentWidth)
                {
                    Percent = PercentHeight;
                    X = Convert.ToInt16((width - (imageBitmap.Width * Percent)) / 2);
                }
                else
                {
                    Percent = PercentWidth;
                    Y = Convert.ToInt16((height - (imageBitmap.Height * Percent)) / 2);
                }
                int FinalWidth = (int)(imageBitmap.Width * Percent);
                int FinalHeight = (int)(imageBitmap.Height * Percent);
                Graphic.DrawImage(imageBitmap,
                    new Rectangle(X, Y, FinalWidth, FinalHeight),
                    new Rectangle(0, 0, imageBitmap.Width, imageBitmap.Height),
                    GraphicsUnit.Pixel);
            }
            return new Bitmap(Canvas);
        }
        public static Bitmap Resize(Bitmap imageBitmap, int width, int height)
        {
            return Resize(imageBitmap, width, height, Color.White);
        }

        public static Bitmap Resize(Stream imageStream, int width, int height, Color backgroundColor)
        {
            using (Bitmap ImageBitMap = new Bitmap(imageStream))
            {
                return Resize(ImageBitMap, width, height, backgroundColor);
            }
        }
        public static Bitmap Resize(Stream imageStream, int width, int height)
        {
            return Resize(imageStream, width, height, Color.White);
        }

        public static Bitmap Resize(Byte[] imageBytes, int width, int height, Color backgroundColor)
        {
            using (Stream ImageStream = new MemoryStream(imageBytes))
            {
                return Resize(ImageStream, width, height, backgroundColor);
            }
        }
        public static Bitmap Resize(Byte[] imageBytes, int width, int height)
        {
            return Resize(imageBytes, width, height, Color.White);
        }

        public static void ResizeAndSave(Stream imageStream, string fullImagePath, int width, int height, ImageFormat format)
        {
            using (Bitmap ImageBitmap = Resize(imageStream, width, height))
            {
                Save(ImageBitmap, fullImagePath, format);
            }
        }
        public static void ResizeAndSaveAsBmp(Stream imageStream, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageStream, fullImagePath, width, height, ImageFormat.Bmp);
        }
        public static void ResizeAndSaveAsJpeg(Stream imageStream, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageStream, fullImagePath, width, height, ImageFormat.Jpeg);
        }
        public static void ResizeAndSaveAsPng(Stream imageStream, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageStream, fullImagePath, width, height, ImageFormat.Png);
        }
        public static void ResizeAndSaveAsGif(Stream imageStream, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageStream, fullImagePath, width, height, ImageFormat.Gif);
        }

        public static void ResizeAndSave(Byte[] imageBytes, string fullImagePath, int width, int height, ImageFormat format)
        {
            using (Stream ImageStream = new MemoryStream(imageBytes))
            {
                ResizeAndSave(ImageStream, fullImagePath, width, height, format);
            }
        }
        public static void ResizeAndSaveAsBmp(Byte[] imageBytes, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageBytes, fullImagePath, width, height, ImageFormat.Bmp);
        }
        public static void ResizeAndSaveAsJpeg(Byte[] imageBytes, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageBytes, fullImagePath, width, height, ImageFormat.Jpeg);
        }
        public static void ResizeAndSaveAsPng(Byte[] imageBytes, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageBytes, fullImagePath, width, height, ImageFormat.Png);
        }
        public static void ResizeAndSaveAsGif(Byte[] imageBytes, string fullImagePath, int width, int height)
        {
            ResizeAndSave(imageBytes, fullImagePath, width, height, ImageFormat.Gif);
        }
        public static void ResizeAndSave(Byte[] imageBytes, string imagePath, string filename, int width, int height, ImageFormat format)
        {
            ResizeAndSave(imageBytes, Path.Combine(imagePath, filename), width, height, format);
        }
        public static void ResizeAndSaveAsBmp(Byte[] imageBytes, string imagePath, string filename, int width, int height)
        {
            ResizeAndSave(imageBytes, imagePath, filename, width, height, ImageFormat.Bmp);
        }
        public static void ResizeAndSaveAsJpeg(Byte[] imageBytes, string imagePath, string filename, int width, int height)
        {
            ResizeAndSave(imageBytes, imagePath, filename, width, height, ImageFormat.Jpeg);
        }
        public static void ResizeAndSaveAsPng(Byte[] imageBytes, string imagePath, string filename, int width, int height)
        {
            ResizeAndSave(imageBytes, imagePath, filename, width, height, ImageFormat.Png);
        }
        public static void ResizeAndSaveAsGif(Byte[] imageBytes, string imagePath, string filename, int width, int height)
        {
            ResizeAndSave(imageBytes, imagePath, filename, width, height, ImageFormat.Gif);
        }

        public static bool IsSquared(Bitmap imageBitmap)
        {
            return (imageBitmap.Width == imageBitmap.Height);
        }
        public static bool IsSquared(Stream imageStream)
        {
            using (Bitmap ImageBitmap = new Bitmap(imageStream))
            {
                return IsSquared(ImageBitmap);
            }
        }
        public static bool IsSquared(Byte[] imageBytes)
        {
            using (Stream ImageStream = new MemoryStream(imageBytes))
            {
                return IsSquared(ImageStream);
            }
        }

        public static bool HasSize(Bitmap imageBitmap, int width, int height)
        {
            return (imageBitmap.Width == width && imageBitmap.Height == height);
        }
        public static bool HasSize(Stream imageStream, int width, int height)
        {
            using (Bitmap ImageBitMap = new Bitmap(imageStream))
            {
                return HasSize(ImageBitMap, width, height);
            }
        }
        public static bool HasSize(Byte[] imageBytes, int width, int height)
        {
            using (Stream ImageStream = new MemoryStream(imageBytes))
            {
                return HasSize(ImageStream, width, height);
            }
        }
    }
}
