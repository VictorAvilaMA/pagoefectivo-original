using System;
using System.Collections.Generic;
using System.Text;
using CaptchaDotNet2.Security.Cryptography;
using System.Web.UI.WebControls;

namespace _3Dev.FW.Web.Capcha
{
    public delegate void CaptchaEventHandler();

    public class UCCapchaBase : System.Web.UI.UserControl
    {
        private string color = "#ffffff";
        protected string style;
        private event CaptchaEventHandler success;
        private event CaptchaEventHandler failure;

        public virtual TextBox TxtCaptcha
        {
            get {
                return null;
            }
        }
        public virtual Label LblCMessage
        {
            get
            {
                return null;
            }
        }
        public virtual Image ImgCaptcha
        {
            get
            {
                return null;
            }
        }

        public string Message
        {
            // We don't set message in page_load, because it prevents us from changing message in failure event
            set { LblCMessage.Text = value; }
            get { return LblCMessage.Text; }
        }
        public string BackgroundColor
        {
            set { color = value.Trim("#".ToCharArray()); }
            get { return color; }
        }
        public string Style
        {
            set { style = value; }
            get { return style; }
        }
        public event CaptchaEventHandler Success
        {
            add { success += value; }
            remove { success += null; }
        }
        public event CaptchaEventHandler Failure
        {
            add { failure += value; }
            remove { failure += null; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetCaptcha();
                TxtCaptcha.Text = "";
            }
        }
        private void SetCaptcha()
        {
            // Set image
            string s = RandomText.Generate();

            // Encrypt
            string ens = Encryptor.Encrypt(s, "srgerg$%^bg", Convert.FromBase64String("srfjuoxp"));

            // Save to session
            Session["captcha"] = s.ToLower();

            // Set url
            ImgCaptcha.ImageUrl = "~/UC/Captcha.ashx?w=305&h=92&c=" + ens + "&bc=" + color;
        }
        public bool DoSuccess()
        {
            if (Session["captcha"] != null && TxtCaptcha.Text.ToLower() == Session["captcha"].ToString())
            {
                if (success != null)
                {
                    success();
                }
                return true;
            }
            else
            {
                TxtCaptcha.Text = "";
                SetCaptcha();

                if (failure != null)
                {
                    failure();
                }
                return false;
            }

        }
    }
}
