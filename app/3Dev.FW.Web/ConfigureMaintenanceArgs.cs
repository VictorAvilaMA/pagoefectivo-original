
namespace _3Dev.FW.Web
{
    public class ConfigureMaintenanceArgs
    {

        public ConfigureMaintenanceArgs()
        { 
        }

        private string maintenancesearchpagename = "";
        public string MaintenanceSearchPageName
        {
            get { return maintenancesearchpagename; }
            set { maintenancesearchpagename = value; }
        }

        private string urlPageCancelForEdit = "";
        public string URLPageCancelForEdit
        {
            get { return urlPageCancelForEdit; }
            set { urlPageCancelForEdit = value; }
        }
        private string urlPageCancelForSave = "";
        public string URLPageCancelForSave
        {
            get { return urlPageCancelForSave; }
            set { urlPageCancelForSave = value; }
        }

        private string urlPageCancelForInsert = "";
        public string URLPageCancelForInsert
        {
            get { return urlPageCancelForInsert; }
            set { urlPageCancelForInsert = value; }
        }

        private string maintenancekey = "";
        public string MaintenanceKey
        {
            get { return maintenancekey; }
            set { maintenancekey = value; }
        }

        private bool useEntitiesMethod = false;
        public bool UseEntitiesMethod
        {
            get { return useEntitiesMethod; }
            set { useEntitiesMethod = value; }
        }

        private string pageTitle = "";
        public string PageTitle
        {
            get { return pageTitle; }
            set { pageTitle = value; }
        }
    }
}
