
namespace _3Dev.FW.Web
{
   public class ConfigureMaintenanceSearchArgs
    {
        private string maintenancepagename = "";
        private string maintenancekey = "";
        private bool executeSearchOnFirstLoad = false;
        private bool executeSearchOnDelete = true  ;
       private bool useEntityMethods = false;

        public string MaintenancePageName
        {
            get
            {
                return maintenancepagename;
            }
            set
            {
                maintenancepagename = value;
            }
        }

        public string MaintenanceKey
        {
            get { return maintenancekey; }
            set { maintenancekey = value; }
        }
       public bool ExecuteSearchOnFirstLoad
       {
           get { return executeSearchOnFirstLoad; }
           set { executeSearchOnFirstLoad = value; }
       }
       public bool ExecuteSearchOnDelete
       {
           get { return executeSearchOnDelete; }
           set { executeSearchOnDelete = value; }
       }
       public bool UseEntityMethods
       {
           get { return useEntityMethods; }
           set { useEntityMethods = value; }
       }
    }
}
