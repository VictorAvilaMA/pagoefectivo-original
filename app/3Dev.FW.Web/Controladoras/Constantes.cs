



/// <summary>
	/// Constantes
	/// </summary>
    /// 

namespace _3Dev.FW.Web
{
	public class Constantes
    {
        #region Tablas
        static public readonly string TABLACONDICIONFLUJOTRABAJO = "06";
        static public readonly string TABLASITUACIONFLUJOTRABAJO = "07";
        static public readonly string TABLAESTADOFLUJOTRABAJO = "08";

        static public readonly string TABLAPRIORIDAD = "01";
        static public readonly string TABLAESTADOTAREA = "05";
        static public readonly string TABLAUNIDADTIEMPO = "20";

        static public readonly string TABLASEXO = "03";
        static public readonly string TABLANATURALJURIDICA = "10";

        #endregion

        static public readonly string TEXTOSSELECCIONAR = "-Seleccionar-";
		static public readonly string VALORSELECCIONAR = "%";

		static public readonly string TEXTOTODOS = "-Todos-";

		static public readonly string VALORTODOS = "%";

		static public readonly string SIGNOCOMA = ",";
		static public readonly string SIGNOIGUAL = "=";
		static public readonly string SIMBOLOPUNTO = ".";
		static public readonly string SEPARADOR = " ¦ ";
		static public readonly string SIGNOMAS = "+";
		static public readonly string SIGNOAMPERSON = "&";
		static public readonly string SIGNOX = "X";
		static public readonly string SEPARADORFECHA = "/";
		static public readonly string VALORCERO = "0";
		static public readonly string VALORUNO = "1";
        static public readonly string VALORDOS = "2";
        static public readonly string VALORCUATRO = "4";
        static public readonly string VALORCINCO = "5";
        static public readonly string VALORSEIS = "6";

		static public readonly string TEXTOTOTAL = "TOTAL";

		static public readonly string FORMATODECIMAL2 = "##0.00";
		static public readonly string FORMATODECIMAL3 = "##0.000";
		static public readonly string FORMATODECIMAL4 = "###,##0.00";
		static public readonly string FORMATOFECHA = "dddd MMM dd";
		static public readonly string FORMATOFECHA2 ="yyyyMMdd";
		static public readonly string FORMATOFECHA3 ="dd/MM/yyyy";
		static public readonly string FORMATOHORA = "HH:mm:ss";
		static public readonly string FORMATOAÑO = "yyyy";
		
		static public readonly string PREFIJOLOGAPLICATIVO = "APL";
		static public readonly string PREFIJOLOGTRANSACCIONAL = "LOG";
		
		static public readonly string EXTENCIONARCHIVOTXT = ".txt";
		static public readonly string CODIGOERRORGENERICO = "ERR00000";
		static public readonly string PREFIJOCODIGOERRORTAD = "TAD";
		static public readonly string PREFIJOCODIGOERRORNTAD = "NTA";
		static public readonly string PREFIJOCODIGOERRORLOG = "LOG";
		static public readonly string PREFIJOCODIGOERRORIU = "IUS";
		static public readonly string CODIGOERRORGENERICOTAD = "TAD00000";
		static public readonly string CODIGOERRORGENERICONTAD = "NTA00000";
		static public readonly string CEROS = "000000000";
		static public readonly string CODIGOERRORDIVICIONENTRECERO = "00002";
		static public readonly string CODIGOERRORTIPOCAMBIOSOLESDOLARES = "00003";
		static public readonly string ERRORTECNICOTIPOCAMBIOSOLESDOLARES = "No se encontró el tipo de cambio actual de soles a Dólares.";
		static public readonly string KEYCASSEMBLYAD = "assembly";
		static public readonly string CARPETACLASESNTAD = "NoTransaccionales";
		static public readonly string CARPETACLASESTAD = "Transaccionales";
		static public readonly string KEYMODOPAGINA = "Modo";
		static public readonly string KEYQPK = "ID";
		static public readonly string KEYQPK2 = "ID2";
		static public readonly string KEYQPKR = "IDR";
        static public readonly string VALORCAMPOID = "ID";

        //Entidades de Negocio
        static public readonly string KEYCASSEMBLYBE = "assemblyBE";
        static public readonly string KEYCASSEMBLYSEGBE = "assemblySegBE";        

		static public readonly string RUTAUPLOAD = "RutaUpload";
		static public readonly string RUTAUPLOADSERVER = "RutaUploadServer";
		
		
		static public readonly string NOMBREDATATABLEIMPRIMIR = "dtImprimir";
		
		static public readonly string KEYSDTIMPRIMIR = "dtImprimir";
		static public readonly string KEYSDTIMPRIMIRDETALLE = "dtImprimirDetalle";
		static public readonly string KEYSCOLUMNAORDENAR = "columnaOrdenar";
		static public readonly string KEYSINDICEPAGINA = "indicePagina";
		static public readonly string KEYSTITULOIMPRIMIR = "tituloImprimir";
		static public readonly string KEYSNOMBREARCHIVOEXPORTAR = "archivoExportar";
		static public readonly int INDICEPAGINADEFAULT = 0;
		static public readonly string KEYSSORT = "Sort";
		static public readonly string KEYSDIRECCIONSORT = "DireccionSort";
		static public readonly string SCRIPTCERRARVENTANA = "<script>javascript:self.close();</script>";
																		     
		//Key querystring pagina error
		static public readonly string KEYQMENSAJEPAGINAERRROR= "MensajePaginaError";
        
		//Configuracion de catalogos
		static public readonly string KEYPID = "IdGenerado";
		static public readonly string KEYPESCENARIO = "Escenario";
		static public readonly string KEYPVALORKEY = "ValorKey";
		static public readonly string KEYPVALORCODIGO = "ValorCodigo";
		static public readonly string KEYPVALORDESCRIPCION = "ValorDescripcion";
		static public readonly string KEYPCAMPOKEY = "CampoKey";
		static public readonly string KEYPCAMPOCODIGO = "CampoCodigo";
		static public readonly string KEYPCAMPODESCRIPCION = "CampoDescripcion";
		static public readonly string KEYPNOMBRECLASE = "NombreClase";
		static public readonly string KEYPCAMPOTITULO = "Titulo";

        static public readonly string CODIGOCATALOGO = "CodigoCatalogo";
		static public readonly string TIPOGRID = "TipoGrid";
		static public readonly string MULTIPLESELECCION = "MultipleSeleccion";
		static public readonly string ITEMSSELECCIONADOS = "ItemsSeleccionados";
		static public readonly string TIPOENVIO = "TipoEnvio";

        // - - - -  Constantes de Generador Catalogo - - - - - - - - - - - - - 
        static public readonly string TIPOCATALOGO = "TipoCatalogo";
        static public readonly string IDMENUOPCION = "IdMenuOpcion";
        static public readonly string TIPOADICIONARCATALOGO = "TipoAdicionar";
        static public readonly string LONGITUDDINAMICA = "{dynamic}";
        static public readonly string CATALOGONEWGUID = "{newguid}";
        static public readonly string CATALOGOPARAMETROS = "Parametros";
        static public readonly string CATALOGONIVEL = "CatalogoNivel";

        static public readonly string CAMPODESCRIPCION = "Descripcion";
        static public readonly string IDOPCION = "IdOpcion";
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        // - - - - - - - - - - Constantes de Calendario  - - - - - - - - - -
        static public readonly string CALENDARIOCULTURE = "CalendarCulture";
        static public readonly string CALENDARIOHORAINICIODIA = "HoraInicioDia";
        static public readonly string CALENDARIOHORAFINDIA = "HoraFinDia";
        static public readonly string CALENDARIOHORAINICIODIAEXTENDIDO = "HoraInicioDiaExtendido";
        static public readonly string CALENDARIOHORAFINDIAEXTENDIDO = "HoraFinDiaExtendido";
        static public readonly string CALENDARIOVISTADIARIA = "VistaDiaria";
        static public readonly string CALENDARIOVISTASEMANAL = "VistaSemanal";
        static public readonly string CALENDARIOVISTAMENSUAL = "VistaMensual";

        static public readonly string FORMATOFECHADIAMESAÑO = "FormatoFechaDiaMesAño";
        static public readonly string FORMATOFECHAMESAÑO = "FormatoFechaMesAño";
        static public readonly string FORMATOFECHAAÑO = "FormatoFechaAño";
        // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        static public readonly string PROYECTO = "1901";
        static public readonly string UNIDADNEGOCIO = "1902";
        static public readonly string UNIDADOPERATIVA = "1903";

        // - - - -  Constantes de Organizador - - - - - - - - - - - - - - - 
        static public readonly string KEYIDORGANIZADOR = "IdOrganizador";
        static public readonly string KEYCLASEORGANIZADOR = "ClaseOrganizador";

		//Selecion
		static public readonly string TABLA = "dtSeleccionado";
        static public readonly string ESTADO = "Estado";

        // Filas por pagina
        static public readonly string FILASPORPAGINA = "FilasPorPagina";

		
	
}
}