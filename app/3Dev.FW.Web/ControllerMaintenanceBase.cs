using System;
using System.Collections.Generic;
using _3Dev.FW.Entidades;

namespace _3Dev.FW.Web
{
    public class ControllerMaintenanceBase : ControllerBase
    {
        public virtual _3Dev.FW.EmsambladoComun.IMaintenanceBase   RemoteServicesInterfaceObject
        {
            get {
                throw new NotImplementedException("GetConstructorDataAccessObject isn't Implemented");
            }
        }
        public virtual int InsertRecord(BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.InsertRecord(be);
        }
        public virtual int UpdateRecord(BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.UpdateRecord(be);
        }
        public virtual BusinessEntityBase GetRecordByID(object id)
        {
            return RemoteServicesInterfaceObject.GetRecordByID(id);
        }
        public virtual int DeleteRecord(BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.DeleteRecord(be);
        }
        public virtual int DeleteRecordByEntityId(object entityId)
        {
            return RemoteServicesInterfaceObject.DeleteRecordByEntityId(entityId);
        }

        public virtual List<BusinessEntityBase> SearchByParameters(BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.SearchByParameters(be);
        }


        public virtual List<BusinessEntityBase> SearchByParametersOrderedBy(BusinessEntityBase be, string orderBy, bool isAccending)
        {
            return RemoteServicesInterfaceObject.SearchByParametersOrderedBy(be,orderBy,isAccending);
        }

        public virtual object GetObjectByParameters(string key, BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.GetObjectByParameters(key, be);
        }

        public virtual List<BusinessEntityBase> GetListByParameters(string key, BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.GetListByParameters(key,be);
        }
        public virtual List<BusinessEntityBase> GetListByParametersOrderedBy(string key, BusinessEntityBase be, string orderBy, bool isAccending)
        {
            return RemoteServicesInterfaceObject.GetListByParametersOrderedBy( key,be, orderBy, isAccending);
        }
        public virtual BusinessEntityBase GetBusinessEntityByParameters(string key, BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.GetBusinessEntityByParameters(key,be);
        }
        public virtual int ExecProcedureByParameters(string key, BusinessEntityBase be)
        {
            return RemoteServicesInterfaceObject.ExecProcedureByParameters(key, be);
        }

        #region entity methods
        public virtual BusinessMessageBase GetEntityByID(object id)
        {
            return RemoteServicesInterfaceObject.GetEntityByID(id);
        }
        public virtual BusinessMessageBase GetEntity(BusinessMessageBase request)
        {
            return RemoteServicesInterfaceObject.GetEntity(request);
        }
        public virtual BusinessMessageBase DeleteEntity(BusinessMessageBase request)
        {
            return RemoteServicesInterfaceObject.DeleteEntity(request);
        }
        public virtual BusinessMessageBase SaveEntity(BusinessMessageBase request)
        {
            return RemoteServicesInterfaceObject.SaveEntity(request);
        }

        #endregion
    }
}
