using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using _3Dev.FW.Entidades;
using _3Dev.FW.Web.Log;

namespace _3Dev.FW.Web
{
    public class MaintenancePageBase<T> : PageBase, IPageParameter where T : ControllerMaintenanceBase, new()
    {



        private ControllerMaintenanceBase controllerObject = null;
        public BusinessEntityBase businessEntityObject = null;
        public BusinessMessageBase saveEntityRequest = null;
        public BusinessMessageBase saveEntityResponse = null;
        public BusinessMessageBase loadEntityResponse = null;
        protected ConfigureMaintenanceArgs configMain;
        protected PageManager pagemanager;
        protected int valueRegister = -1;
        protected int valueUpdated = -1;
        private object maintenanceKeyValue = null;
        public string DefaultErrorMessage = "Ha ocurrido un error, cont�ctese con el Administrador.";
        public string DefaultNoMessage = "No se ha podido realizar la operaci�n.";
        public string DefaultSuccessfullMessage = "La operaci�n se realiz� correctamente.";
        public string CssClassError = "MensajeValidacion";
        public string CssClassSuccessfullMessage = "MensajeTransaccion";
        public string CssClassWarningMessage = "MensajeTransaccion";
        public MaintenancePageBase()
        {
        }
        public void SetState(PageManagerStatus modelState)
        {
            PageManager.Status = modelState;
        }
        public void RegisterEditControlsMaintenance()
        {
        }
        private void DoConstructorControllerObject()
        {
            if (controllerObject == null)
                controllerObject = new T();
        }
        public ControllerMaintenanceBase ControllerObject
        {
            get
            {
                DoConstructorControllerObject();
                return controllerObject;
            }
        }

        #region .. properties ..

        public object MaintenanceKeyValue
        {
            get
            {
                if (maintenanceKeyValue == null)
                {
                    maintenanceKeyValue = ((IPageParameter)PreviousPage).MaintenanceKeyValue;
                }
                return maintenanceKeyValue;
            }
            set
            {
                maintenanceKeyValue = value;
            }
        }
        protected virtual Button BtnInsert
        {
            get
            {
                return null;
            }
        }
        protected virtual Button BtnUpdate
        {
            get
            {
                return null;
            }
        }
        protected virtual Button BtnCancel
        {
            get
            {
                return null;
            }
        }
        protected virtual ImageButton BtnImgInsert
        {
            get
            {
                return null;
            }
        }
        protected virtual ImageButton BtnImgUpdate
        {
            get
            {
                return null;
            }
        }
        protected virtual ImageButton BtnImgCancel
        {
            get
            {
                return null;
            }
        }
        protected virtual Label LblMessageMaintenance
        {
            get
            {
                return null;
            }
        }
        #endregion
        #region register methods..
        public virtual void OnInsert()
        {
            try
            {
                if (AllowToInsert())
                {
                    OnBeforeInsert();
                    OnMainInsert();
                    OnAfterInsert();
                }
            }
            catch (Exception ex)
            {
                //Logger.LogException(ex);
                ThrowErrorMessage(DefaultErrorMessage);
            }
        }
        public virtual void OnBeforeInsert()
        {

        }
        public virtual void OnAfterInsert()
        {

        }
        public virtual bool AllowToInsert()
        {
            return true;
        }
        public virtual int DoInsertRecord(BusinessEntityBase be)
        {
            return ControllerObject.InsertRecord(be);
        }
        public virtual void OnMainInsert()
        {
            businessEntityObject = CreateBusinessEntityForRegister(businessEntityObject);
            valueRegister = DoInsertRecord(businessEntityObject);
            EnabledButtonControls();
            ThrowSuccessfullyMessage(DefaultSuccessfullMessage);

        }
        public virtual BusinessEntityBase CreateBusinessEntityForRegister(BusinessEntityBase be)
        {
            return be;
        }

        #endregion

        #region updates methods..
        public virtual void OnUpdate()
        {
            try
            {
                if (AllowToUpdate())
                {
                    OnBeforeUpdate();
                    OnMainUpdate();
                    OnAfterUpdate();
                }
            }
            catch (Exception ex)
            {
                //Logger.LogException(ex);
                ThrowErrorMessage(DefaultErrorMessage);
            }
        }
        public virtual void OnBeforeUpdate()
        {

        }
        public virtual void OnAfterUpdate()
        {

        }
        public virtual bool AllowToUpdate()
        {
            return true;
        }
        public virtual int DoUpdateRecord(BusinessEntityBase be)
        {
            return ControllerObject.UpdateRecord(businessEntityObject);
        }
        public virtual void OnMainUpdate()
        {
            businessEntityObject = CreateBusinessEntityForUpdate(businessEntityObject);
            valueUpdated = DoUpdateRecord(businessEntityObject);
            EnabledButtonControls();
            ThrowSuccessfullyMessage(DefaultSuccessfullMessage);
        }
        public virtual BusinessEntityBase CreateBusinessEntityForUpdate(BusinessEntityBase be)
        {
            return be;
        }
        #endregion

        #region cancel methods..
        public virtual void OnCancel()
        {
            if (AllowToCancel())
            {
                OnBeforeCancel();
                OnMainCancel();
                OnAfterCancel();
            }
        }
        public virtual void OnBeforeCancel()
        {

        }
        public virtual void OnAfterCancel()
        {
        }
        public virtual bool AllowToCancel()
        {
            return true;
        }
        public virtual void OnMainCancel()
        {
            if (PageManager.Status == PageManagerStatus.IsInserting)
            {
                string urlredirect = "";
                if (configMain.UseEntitiesMethod) urlredirect = configMain.URLPageCancelForSave; else urlredirect = configMain.URLPageCancelForInsert;
                if (urlredirect != "")
                    NavigateTo(urlredirect);
            }
            else if (PageManager.Status == PageManagerStatus.IsEditing)
            {
                string urlredirect = "";
                if (configMain.UseEntitiesMethod) urlredirect = configMain.URLPageCancelForSave; else urlredirect = configMain.URLPageCancelForEdit;
                if (urlredirect != "")
                    NavigateTo(urlredirect);
            }
            else if (PageManager.Status == PageManagerStatus.IsSaving)
            {
                if (configMain.URLPageCancelForSave != "")
                    NavigateTo(configMain.URLPageCancelForSave);
            }
        }
        #endregion
        protected virtual void InitializeMaintenance()
        {
            this.Page.PreLoad += new EventHandler(MaintenancePage_PreLoad);
            this.Page.Load += new EventHandler(MaintenancePage_Load);
            if (this.BtnInsert != null) this.BtnInsert.Click += new EventHandler(BtnInsert_Click);
            if (this.BtnUpdate != null) this.BtnUpdate.Click += new EventHandler(BtnUpdate_Click);
            if (this.BtnCancel != null) this.BtnCancel.Click += new EventHandler(BtnCancel_Click);
            if (this.BtnImgInsert != null) this.BtnImgInsert.Click += new ImageClickEventHandler(BtnImgInsert_Click);
            if (this.BtnImgUpdate != null) this.BtnImgUpdate.Click += new ImageClickEventHandler(BtnImgUpdate_Click);
            if (this.BtnImgCancel != null) this.BtnImgCancel.Click += new ImageClickEventHandler(BtnImgCancel_Click);
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeMaintenance();
            configMain = new ConfigureMaintenanceArgs();
            ConfigureMaintenance(configMain);
            RegisterEditControlsMaintenance();
        }
        public void BtnInsert_Click(object sender, EventArgs e)
        {
            if (configMain.UseEntitiesMethod)
                OnSaveEntity();
            else
                OnInsert();
        }
        public void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (configMain.UseEntitiesMethod)
                OnSaveEntity();
            else
                OnUpdate();
        }
        public void BtnCancel_Click(object sender, EventArgs e)
        {
            OnCancel();
        }

        public void BtnImgInsert_Click(object sender, ImageClickEventArgs e)
        {
            if (configMain.UseEntitiesMethod)
                OnSaveEntity();
            else
                OnInsert();
        }
        public void BtnImgUpdate_Click(object sender, ImageClickEventArgs e)
        {
            if (configMain.UseEntitiesMethod)
                OnSaveEntity();
            else
                OnUpdate();
        }
        public void BtnImgCancel_Click(object sender, ImageClickEventArgs e)
        {
            OnCancel();
        }
        public void ThrowErrorMessage(string msg)
        {
            if (LblMessageMaintenance != null)
            {
                LblMessageMaintenance.CssClass = CssClassError;
                LblMessageMaintenance.Text = msg;
            }
        }
        public void ThrowSuccessfullyMessage(string msg)
        {
            if (LblMessageMaintenance != null)
            {
                LblMessageMaintenance.CssClass = CssClassSuccessfullMessage;
                LblMessageMaintenance.Text = msg;
            }
        }
        public void ThrowWarningMessage(string msg)
        {
            if (LblMessageMaintenance != null)
            {
                LblMessageMaintenance.CssClass = CssClassWarningMessage;
                LblMessageMaintenance.Text = msg;
            }
        }
        protected virtual void ConfigureMaintenance(ConfigureMaintenanceArgs e)
        {
        }
        protected virtual void MaintenancePage_PreLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((maintenanceKeyValue != null) || (((PreviousPage != null) && (!IsCrossPagePostBack) && (MaintenanceKeyValue != null))))
                {
                    this.PageManager.Status = PageManagerStatus.IsEditing;
                }
                else
                {
                    this.PageManager.Status = PageManagerStatus.IsInserting;
                }
            }
        }
        protected virtual void MaintenancePage_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (PageManager.IsEditing)
                {
                    EnabledButtonControls();
                    OnLoadInformation();
                }
                else
                {
                    EnabledButtonControls();
                }
            }
        }

        public virtual void OnLoadInformation()
        {
            if (AllowToLoadInformation())
            {
                OnBeforeLoadInformation();
                OnMainLoadInformation();
                OnAfterLoadInformation();
            }
        }
        public virtual bool AllowToLoadInformation()
        {
            return true;
        }
        public virtual void OnBeforeLoadInformation()
        {

        }
        public virtual void OnAfterLoadInformation()
        {

        }
        public virtual void OnMainLoadInformation()
        {
            try
            {
                if (configMain.UseEntitiesMethod)
                {
                    loadEntityResponse = ControllerObject.GetEntityByID(MaintenanceKeyValue);
                    AssignEntityMessageResponseValuesInLoadingInfo(loadEntityResponse);
                }
                else
                {
                    businessEntityObject = ControllerObject.GetRecordByID(MaintenanceKeyValue);
                    AssignBusinessEntityValuesInLoadingInfo(businessEntityObject);
                }
            }
            catch (Exception ex)
            {
                //Logger.LogException(ex);
                ThrowErrorMessage(DefaultErrorMessage);
            }
        }
        public virtual void AssignBusinessEntityValuesInLoadingInfo(BusinessEntityBase be)
        {
        }

        public virtual void AssignEntityMessageResponseValuesInLoadingInfo(BusinessMessageBase response)
        {
        }

        public PageManager PageManager
        {
            get
            {
                pagemanager = (PageManager)ViewState[configMain.MaintenanceKey + "_PageManager"];
                if (pagemanager == null)
                {
                    pagemanager = new PageManager();
                    ViewState[configMain.MaintenanceKey + "_PageManager"] = pagemanager;
                }
                return pagemanager;
            }
            //set
            //{
            //    ViewState[configMain.MaintenanceKey + "_PageState"] = value;
            //}
        }
        public virtual void GoToMaintenanceSearchPage()
        {
            NavigateTo(configMain.MaintenanceSearchPageName);
        }
        public virtual void EnabledButtonControls()
        {
            switch (PageManager.Status)
            {
                case PageManagerStatus.IsEditing:
                    if (BtnCancel != null) BtnCancel.Visible = true;
                    if (BtnUpdate != null) BtnUpdate.Visible = true;
                    if (BtnInsert != null) BtnInsert.Visible = false;
                    if (BtnImgCancel != null) BtnImgCancel.Visible = true;
                    if (BtnImgUpdate != null) BtnImgUpdate.Visible = true;
                    if (BtnImgInsert != null) BtnImgInsert.Visible = false;

                    break;
                case PageManagerStatus.IsInserting:
                    if (BtnCancel != null) BtnCancel.Visible = true;
                    if (BtnUpdate != null) BtnUpdate.Visible = false;
                    if (BtnInsert != null) BtnInsert.Visible = true;
                    if (BtnImgCancel != null) BtnImgCancel.Visible = true;
                    if (BtnImgUpdate != null) BtnImgUpdate.Visible = false;
                    if (BtnImgInsert != null) BtnImgInsert.Visible = true;
                    break;
                case PageManagerStatus.IsSaving:
                    if (BtnCancel != null) BtnCancel.Visible = true;
                    if (BtnUpdate != null) BtnUpdate.Visible = false;
                    if (BtnInsert != null) BtnInsert.Visible = false;
                    if (BtnImgCancel != null) BtnImgCancel.Visible = true;
                    if (BtnImgUpdate != null) BtnImgUpdate.Visible = false;
                    if (BtnImgInsert != null) BtnImgInsert.Visible = false;
                    break;
            }
        }


        #region save methods..
        public virtual void OnSaveEntity()
        {
            try
            {
                if (AllowToSaveEntity())
                {
                    OnBeforeSaveEntity();
                    OnMainSaveEntity();
                    OnAfterSaveEntity(saveEntityRequest, saveEntityResponse);
                }
            }
            catch (Exception ex)
            {
                //Logger.LogException(ex);
                ThrowErrorMessage(DefaultErrorMessage);
            }
        }
        public virtual void OnBeforeSaveEntity()
        {

        }
        public virtual void OnAfterSaveEntity(BusinessMessageBase request, BusinessMessageBase response)
        {
            if (response.BMResultState == BMResult.Ok)
            {
                this.PageManager.Status = PageManagerStatus.IsSaving;
                EnabledButtonControls();
                EnabledMaintenanceControls(false);
                BtnCancel.Text = "Regresar";
                BtnCancel.OnClientClick = "";
                ThrowSuccessfullyMessage(response.Message);
            }
            else if (response.BMResultState == BMResult.No)
            {
                ThrowErrorMessage(string.Format("{0}:{1}", DefaultNoMessage, response.Message));
            }
            else if (response.BMResultState == BMResult.Error)
            {
                ThrowErrorMessage(string.Format("{0}:{1}", DefaultErrorMessage, response.Message));
            }
        }

        public virtual bool AllowToSaveEntity()
        {
            return true;
        }
        public virtual BusinessMessageBase DoSaveEntity(BusinessMessageBase request)
        {
            return ControllerObject.SaveEntity(request);
        }
        public virtual void OnMainSaveEntity()
        {
            saveEntityRequest = new BusinessMessageBase();
            saveEntityRequest = CreateMessageRequestForSaveEntity(saveEntityRequest);
            saveEntityResponse = DoSaveEntity(saveEntityRequest);
        }
        public virtual BusinessMessageBase CreateMessageRequestForSaveEntity(BusinessMessageBase request)
        {
            return request;
        }
        public virtual void EnabledMaintenanceControls(bool enabled)
        {
        }
        #endregion

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (configMain != null) this.Title = configMain.PageTitle;
        }


    }
}
