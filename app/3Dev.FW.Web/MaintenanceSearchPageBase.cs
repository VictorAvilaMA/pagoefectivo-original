using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using _3Dev.FW.Entidades;


namespace _3Dev.FW.Web
{
    public class MaintenanceSearchPageBase<T> : PageBase, IPageParameter where T : ControllerMaintenanceBase, new()
    {

        const string CONS_IMGBTNDELETE = "imgbtndelete";
        private T controllerObject = null;
        private BusinessEntityBase businessEntityObject = null;
        protected ConfigureMaintenanceSearchArgs configMain;
        private object maintenanceKeyValue = null;
        public string MessageNoRecordsWereFound = "No se encontraron registros en la b�squeda.";
        public string CssClassError = "MensajeValidacion";
        public string CssClassSuccessfullMessage = "MensajeTransaccion";
        public string CssClassWarningMessage = "MensajeTransaccion";
        public string DefaultSuccessfullMessage = "La operaci�n se realiz� correctamente.";

        public List<_3Dev.FW.Entidades.BusinessEntityBase> ResultList;

        public MaintenanceSearchPageBase()
        {
            //SortDir = "desc";
        }
        public object MaintenanceKeyValue
        {
            get
            {
                return maintenanceKeyValue;
            }
            set
            {
                maintenanceKeyValue = value;
            }
        }
        protected virtual Button BtnSearch
        {
            get
            {
                return null;
            }
        }
        protected virtual Button BtnClear
        {
            get
            {
                return null;
            }
        }
        protected virtual Button BtnNew
        {
            get
            {
                return null;
            }
        }
        protected virtual ImageButton BtnImgSearch
        {
            get
            {
                return null;
            }
        }
        protected virtual ImageButton BtnImgClear
        {
            get
            {
                return null;
            }
        }
        protected virtual ImageButton BtnImgNew
        {
            get
            {
                return null;
            }
        }
        protected virtual GridView GridViewResult
        {
            get
            {
                return null;
            }
        }
        protected virtual Label LblMessageMaintenance
        {
            get
            {
                return null;
            }
        }


        //add <PeruCom FaseIII>
        #region propiedades paginado
        protected virtual bool FlagPager
        {
            get
            {
                return false;
            }
        }


        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] == null)
                    ViewState["PageNumber"] = 1;
                return (int)ViewState["PageNumber"];
            }
            set { ViewState["PageNumber"] = value; }
        }
        #endregion


        protected virtual void ConfigureMaintenanceSearch(ConfigureMaintenanceSearchArgs e)
        {

        }
        private void DoConstructorControllerObject()
        {
            if (controllerObject == null)
                controllerObject = new T();
        }


        public ControllerMaintenanceBase ControllerObject
        {
            get
            {
                DoConstructorControllerObject();
                return controllerObject;
            }
        }

        public virtual void OnSearch()
        {
            if (AllowToSearch())
            {
                OnBeforeSearch();
                OnMainSearch();
                OnAfterSearch();
            }
        }
        public virtual void OnBeforeSearch()
        {
        }
        public virtual void OnAfterSearch()
        {
        }
        public virtual bool AllowToSearch()
        {
            return true;
        }

        #region delete object
        BusinessMessageBase messageResponseOnDelete = null;
        public virtual void OnDelete(object entityId)
        {
            BusinessMessageBase request = new BusinessMessageBase();
            request.EntityId = entityId;
            if (AllowToDelete(request))
            {
                OnBeforeDelete(request);
                messageResponseOnDelete = OnMainDelete(request);
                OnAfterDelete(request, messageResponseOnDelete);
            }
        }
        public virtual void OnBeforeDelete(BusinessMessageBase request)
        {
        }
        public virtual void OnAfterDelete(BusinessMessageBase request, BusinessMessageBase response)
        {
        }
        public virtual bool AllowToDelete(BusinessMessageBase request)
        {
            return true;
        }

        public virtual BusinessMessageBase OnMainDelete(BusinessMessageBase request)
        {
            BusinessMessageBase response = null;
            try
            {
                response = ControllerObject.DeleteEntity(request);
                if (response.BMResultState == BMResult.Ok)
                {
                    ThrowSuccessfullyMessage(response.Message);
                    if (configMain.ExecuteSearchOnDelete)
                        OnSearch();
                }
                if (response.BMResultState == BMResult.No)
                {
                    ThrowErrorMessage(response.Message);
                }
            }
            catch (Exception ex)
            {
                ThrowErrorMessage(ex.Message);
            }
            return response;
        }
        #endregion
        //#region delete entity
        //public virtual void OnDeleteEntity(BusinessMessageBase request)
        //{
        //    if (AllowToDelete(entityId))
        //    {
        //        OnBeforeDelete(entityId);
        //        OnMainDelete(entityId);
        //        OnAfterDelete(entityId);
        //    }
        //}
        //public virtual void OnBeforeDelete(object entityId)
        //{
        //}
        //public virtual void OnAfterDelete(object entityId)
        //{
        //}
        //public virtual bool AllowToDelete(object entityId)
        //{
        //    return true;
        //}
        //public virtual void OnMainDelete(object entityId)
        //{
        //    try
        //    {
        //        ControllerObject.DeleteRecordByEntityId(entityId);
        //        ThrowSuccessfullyMessage(DeleteSuccessfullMessage);
        //        if (configMain.ExecuteSearchOnDelete)
        //            OnSearch();
        //    }
        //    catch (Exception ex)
        //    {
        //        ThrowErrorMessage(ex.Message);
        //    }
        //}
        //#endregion

        public virtual List<BusinessEntityBase> GetMethodSearchByParameters(BusinessEntityBase be)
        {
            if ((GridViewResult != null) && (GridViewResult.AllowSorting))
                return ControllerObject.SearchByParametersOrderedBy(businessEntityObject, SortExpression, SortDir == SortDirection.Ascending);
            else
                return ControllerObject.SearchByParameters(businessEntityObject);
        }
        public virtual void OnMainSearch()
        {
            try
            {
                businessEntityObject = CreateBusinessEntityForSearch();
                //add <PeruCom FaseIII>
                if (FlagPager)
                {
                    businessEntityObject.PageNumber = PageNumber;
                    businessEntityObject.PageSize = GridViewResult.PageSize;
                    businessEntityObject.TipoOrder = SortDir == SortDirection.Ascending ? true : false;
                    businessEntityObject.PropOrder = SortExpression;
                    GridViewResult.PageIndex = PageNumber - 1;

                    ResultList = GetMethodSearchByParameters(businessEntityObject);

                    ObjectDataSource oObjectDataSource = new ObjectDataSource();
                    oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID;
                    oObjectDataSource.EnablePaging = GridViewResult.AllowPaging;
                    oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter";
                    oObjectDataSource.SelectMethod = "GetDataGrilla";
                    oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros";
                    oObjectDataSource.StartRowIndexParameterName = "filaInicio";
                    oObjectDataSource.MaximumRowsParameterName = "maxFilas";
                    oObjectDataSource.EnableViewState = false;
                    oObjectDataSource.ObjectCreating += new ObjectDataSourceObjectEventHandler(oObjectDataSource_ObjectCreating);
                    GridViewResult.DataSource = oObjectDataSource;

                }
                else
                {
                    ResultList = GetMethodSearchByParameters(businessEntityObject);
                    GridViewResult.DataSource = ResultList;
                }


                GridViewResult.DataBind();
                if (GridViewResult.Rows.Count == 0) ThrowWarningMessage(MessageNoRecordsWereFound);
                else ThrowWarningMessage("");
            }
            catch (Exception ex)
            {
                ThrowErrorMessage(ex.Message);
            }
        }

        private void oObjectDataSource_ObjectCreating(Object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = new TotalRegistrosTableAdapter(ResultList, ResultList == null ? 0 : (ResultList.Count == 0 ? 0 : ResultList.First().TotalPageNumbers));
        }

        public virtual BusinessEntityBase CreateBusinessEntityForSearch()
        {
            return null;
        }
        public void ThrowErrorMessage(string msg)
        {
            if (LblMessageMaintenance != null)
            {
                LblMessageMaintenance.CssClass = CssClassError;
                LblMessageMaintenance.Text = msg;
            }
        }
        public void ThrowSuccessfullyMessage(string msg)
        {
            if (LblMessageMaintenance != null)
            {
                LblMessageMaintenance.CssClass = CssClassSuccessfullMessage;
                LblMessageMaintenance.Text = msg;
            }
        }
        public void ThrowWarningMessage(string msg)
        {
            if (LblMessageMaintenance != null)
            {
                LblMessageMaintenance.CssClass = CssClassWarningMessage;
                LblMessageMaintenance.Text = msg;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeMaintenanceSearch();
            configMain = new ConfigureMaintenanceSearchArgs();
            ConfigureMaintenanceSearch(configMain);
        }
        protected virtual void InitializeMaintenanceSearch()
        {
            this.Page.Load += new EventHandler(MaintenanceSearchPage_Load);
            if (this.GridViewResult != null) this.GridViewResult.RowEditing += new GridViewEditEventHandler(GridViewResult_RowEditing);
            if (this.GridViewResult != null) this.GridViewResult.RowDeleting += new GridViewDeleteEventHandler(GridViewResult_RowDeleting);
            if (this.GridViewResult != null) this.GridViewResult.PageIndexChanging += new GridViewPageEventHandler(GridViewResult_PageIndexChanging);
            if (this.GridViewResult != null) this.GridViewResult.Sorting += new System.Web.UI.WebControls.GridViewSortEventHandler(this.GridViewResult_SortCommand);
            if (this.GridViewResult != null) this.GridViewResult.RowDataBound += new GridViewRowEventHandler(GridViewResult_RowDataBound);

            if (this.BtnSearch != null) this.BtnSearch.Click += new EventHandler(BtnSearch_Click);
            if (this.BtnClear != null) this.BtnClear.Click += new EventHandler(BtnClear_Click);
            if (this.BtnNew != null) this.BtnNew.Click += new EventHandler(BtnNew_Click);
            if (this.BtnImgSearch != null) this.BtnImgSearch.Click += new ImageClickEventHandler(BtnImgSearch_Click);
            if (this.BtnImgClear != null) this.BtnImgClear.Click += new ImageClickEventHandler(BtnImgClear_Click);
            if (this.BtnImgNew != null) this.BtnImgNew.Click += new ImageClickEventHandler(BtnImgNew_Click);
        }
        protected virtual void MaintenanceSearchPage_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (configMain.ExecuteSearchOnFirstLoad)
                    OnSearch();
            }

        }
        public virtual void OnNew()
        {
            Response.Redirect(configMain.MaintenancePageName);
        }
        public virtual void OnGoToMaintenanceEditPage(object sender, GridViewEditEventArgs e)
        {
            object key = null;
            GridViewRow gvr = null;
            gvr = GridViewResult.Rows[e.NewEditIndex];
            AssignParametersToLoadMaintenanceEdit(out key, gvr);
            MaintenanceKeyValue = key;
            //GoToMaintenancePage();

        }
        public void GridViewResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewResult.PageIndex = e.NewPageIndex;
            //add <PeruCom FaseIII>
            PageNumber = e.NewPageIndex + 1;

            OnSearch();
        }
        public void GridViewResult_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //if(e.CommandName=="Edit")
            //   OnGoToMaintenanceEditPage(sender, e);
            OnGoToMaintenanceEditPage(sender, e);
        }
        public void GridViewResult_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!AllowToDelete(null))
            {
                if (!(GridViewResult.Rows.Count > 0))
                {
                    GridViewResult.DataSource = null;
                }
                GridViewResult.DataBind();
                return;
            }
            object entityId = GridViewResult.DataKeys[e.RowIndex].Values[0];
            OnDelete(entityId);
        }
        public void GridViewResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ImageButton imagebutton = (ImageButton)e.Row.FindControl(CONS_IMGBTNDELETE);
            if (imagebutton != null)
            {
                imagebutton.Click += new ImageClickEventHandler(BtnImgDelete_Click);
                imagebutton.CommandName = "Delete";
                imagebutton.OnClientClick = "return DeleteMe();";
            }
            if (e.Row != null)
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    if(e.Row.Cells[0] != null)
                        e.Row.Cells[0].Text = "Page " + (GridViewResult.PageIndex + 1) + " of " + GridViewResult.PageCount;
                }
            }
            
        }
        public void BtnImgDelete_Click(object sender, ImageClickEventArgs e)
        {
        }

        public void BtnNew_Click(object sender, EventArgs e)
        {
            OnNew();
        }
        public void BtnSearch_Click(object sender, EventArgs e)
        {
            //add <PeruCom FaseIII>
            PageNumber = 1;
            OnSearch();
        }
        public void BtnClear_Click(object sender, EventArgs e)
        {
            OnClear();
        }
        public void BtnImgSearch_Click(object sender, ImageClickEventArgs e)
        {
            OnSearch();
        }
        public void BtnImgClear_Click(object sender, ImageClickEventArgs e)
        {
            OnClear();
        }
        public void BtnImgNew_Click(object sender, ImageClickEventArgs e)
        {
            OnNew();
        }
        public virtual void OnClear()
        {
            GridViewResult.DataSource = null;
            GridViewResult.DataBind();
        }
        public virtual void AssignParametersToLoadMaintenanceEdit(out object key, GridViewRow e)
        {
            key = null;
        }
        public virtual void GoToMaintenancePage()
        {
            //Response.Redirect(configMain.MaintenancePageName);
            Server.Transfer(configMain.MaintenancePageName);
        }
        public void EnabledMaintenanceButtonControls()
        {

        }
        public SortDirection SortDir
        {
            get
            {
                if (ViewState["SortDir"] == null)
                    ViewState["SortDir"] = SortDirection.Ascending;
                return (SortDirection)ViewState["SortDir"];
            }
            set { ViewState["SortDir"] = value; }
        }
        public string SortExpression
        {
            get
            {
                if (ViewState["SortExpression"] == null)
                    ViewState["SortExpression"] = "";
                return (String)ViewState["SortExpression"];
            }
            set { ViewState["SortExpression"] = value; }
        }

        public virtual void GridViewResult_SortCommand(object source, System.Web.UI.WebControls.GridViewSortEventArgs e)
        {
            if (SortDir.Equals(SortDirection.Ascending))
                SortDir = SortDirection.Descending;
            else SortDir = SortDirection.Ascending;
            SortExpression = e.SortExpression;
            OnSearch();
            //string sortend = e.SortExpression + " " + SortDir;
            //LoadBySortCommand(e.SortExpression, SortDir == "asc");
        }
        //public virtual void LoadBySortCommand(string pSortExpression,bool isAccending)
        //{
        //   businessEntityObject = CreateBusinessEntityForSearch();
        //   List<BusinessEntityBase> list= GetMethodSearchByParameters(businessEntityObject);
        //   DoDefineSortExpression(list, pSortExpression);
        //   if (!isAccending) list.Reverse();
        //   GridViewResult.DataSource =list;
        //   GridViewResult.DataBind();
        //}
        //public virtual void DoDefineSortExpression(List<BusinessEntityBase> list, string pSortExpression)
        //{

        //}
    }
}
