using System;
using System.Web.Security;

/// <summary>
/// Summary description for CustomMemberShip
/// </summary>
/// 
namespace _3Dev.FW.Web
{
public class CustomMemberShip : MembershipProvider
{
    public CustomMemberShip()
    {
    
    }
    
    //private CustomMembershipProviderClient providerClientObject = null;
    //private void DoConstructorProviderClient()
    //{
    //    if (providerClientObject == null)
    //        providerClientObject = GetConstructorProviderClient();
    //}
    public virtual CustomMembershipProviderClient GetConstructorProviderClient()
    {
        return null;
    }
    public CustomMembershipProviderClient ProviderClientObject
    {
        get
        {
//            DoConstructorProviderClient();
            return GetConstructorProviderClient();
        }
    }

    public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
    {
        base.Initialize(name, config);
        string sconfig = "";
        for (int i = 0; i <= config.Count - 1; i++)
        {
            sconfig += config.GetKey(i).Trim() + ":";
            sconfig += config[i].Trim() + ";";
        }
        ProviderClientObject.Initialize(name, sconfig);
    }

    public override string ApplicationName
    {
        get
        {
            throw new Exception("The method or operation is not implemented.");
        }
        set
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    {
        return ProviderClientObject.ChangePassword(username, oldPassword, newPassword);
    }
    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    {
        return ProviderClientObject.ChangePasswordQuestionAndAnswer(username, password, newPasswordQuestion, newPasswordAnswer);
    }
    public virtual bool ValidateUserAndPass(string username, string password)
    {
        return ProviderClientObject.ValidateUserAndPass(username, password);
    }
        public override bool ValidateUser(string username, string password)
    {
        return ProviderClientObject.ValidateUser(username, password);
    }
    public override MembershipUser CreateUser(string username, string password, string primNombre, string appPaterno, string appMaterno, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
        return ProviderClientObject.CreateUser(username, password, primNombre, appPaterno, appMaterno, isApproved, providerUserKey, out status);
    }
    public virtual bool ExistUser(string username)
    {
        return ProviderClientObject.ExistUser(username);
    }
    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
        throw new Exception("The method or operation is not implemented.");
    }
    public override bool EnablePasswordReset
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }
    public override bool EnablePasswordRetrieval
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }
    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }
    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override int GetNumberOfUsersOnline()
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override string GetPassword(string username, string answer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override string GetUserNameByEmail(string email)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override int MaxInvalidPasswordAttempts
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override int MinRequiredNonAlphanumericCharacters
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override int MinRequiredPasswordLength
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override int PasswordAttemptWindow
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override MembershipPasswordFormat PasswordFormat
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override string PasswordStrengthRegularExpression
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override bool RequiresQuestionAndAnswer
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override bool RequiresUniqueEmail
    {
        get { throw new Exception("The method or operation is not implemented."); }
    }

    public override string ResetPassword(string username, string answer)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override bool UnlockUser(string userName)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public override void UpdateUser(MembershipUser user)
    {
        throw new Exception("The method or operation is not implemented.");
    }

}
}