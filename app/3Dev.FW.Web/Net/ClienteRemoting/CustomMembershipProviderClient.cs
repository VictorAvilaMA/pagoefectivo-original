using System.Web.Security;
using _3Dev.FW.EmsambladoComun.Seguridad;


/// <summary>
/// Summary description for ControlClient
/// </summary>
/// 
namespace _3Dev.FW.Web
{
public class CustomMembershipProviderClient:ICustomMembershipProvider
{
    public CustomMembershipProviderClient()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public virtual _3Dev.FW.EmsambladoComun.Seguridad.ICustomMembershipProvider RemoteServicesInterfaceObject
    {
        get
        {
            return null;
        }
    }

    public void Initialize(string name, string config)
    {
         RemoteServicesInterfaceObject.Initialize(name, config);
//        RemoteServices.Instance.ICustomMembershipProvider.Initialize(name, config);
    }

    public bool ValidateUser(string username, string password)
    {
        return RemoteServicesInterfaceObject.ValidateUser(username, password);
        //return RemoteServices.Instance.ICustomMembershipProvider.ValidateUser(username, password);
    }

    public virtual bool ValidateUserAndPass(string username, string password)
    {
        return RemoteServicesInterfaceObject.ValidateUserAndPass(username, password);
    }


    #region ICustomMembershipProvider Members

    public bool CheckPassword(string username, string password)
    {
        return RemoteServicesInterfaceObject.CheckPassword(username, password);
        //return RemoteServices.Instance.ICustomMembershipProvider.CheckPassword(username,password);
    }

    #endregion

    #region ICustomMembershipProvider Members

    public bool ChangePassword(string username, string oldPwd, string newPwd)
    {
        return RemoteServicesInterfaceObject.ChangePassword(username, oldPwd, newPwd);
        //return RemoteServices.Instance.ICustomMembershipProvider.ChangePassword(username,oldPwd,newPwd);
    }

    public bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
    {
        return RemoteServicesInterfaceObject.ChangePasswordQuestionAndAnswer(username, password, newPwdQuestion, newPwdAnswer);
        //return RemoteServices.Instance.ICustomMembershipProvider.ChangePasswordQuestionAndAnswer(username,password,newPwdQuestion,newPwdAnswer);
    }

    #endregion

    #region ICustomMembershipProvider Members

    public void ActualizarUsuario(string telefonofijo, string celular, MembershipUser user)
    {
        RemoteServicesInterfaceObject.ActualizarUsuario(telefonofijo, celular, user);
        //RemoteServices.Instance.ICustomMembershipProvider.ActualizarUsuario(telefonofijo,celular,user);
    }

    #endregion

    #region ICustomMembershipProvider Members


    public MembershipUser CreateUser(string username, string password, string primNombre, string appPaterno, string appMaterno, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    {
        return RemoteServicesInterfaceObject.CreateUser(username, password, primNombre, appPaterno, appMaterno, isApproved, providerUserKey, out status);
        //return RemoteServices.Instance.ICustomMembershipProvider.CreateUser(username,password,primNombre,appPaterno,appMaterno,isApproved,providerUserKey,out status);
    }
    public virtual bool ExistUser(string username)
    {
        return RemoteServicesInterfaceObject.ExistUser(username);
    }


    #endregion
}
}