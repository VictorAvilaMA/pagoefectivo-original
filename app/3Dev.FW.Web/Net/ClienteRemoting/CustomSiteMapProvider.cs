using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Web;
using _3Dev.FW.Entidades.Seguridad;



namespace _3Dev.FW.Web
{
    public class CustomSiteMapProvider : StaticSiteMapProvider
    {
        private const string _errmsg1 = "Missing node ID";
        private const string _errmsg2 = "Duplicate node ID";
        private const string _errmsg3 = "Missing parent ID";
        private const string _errmsg4 = "Invalid parent ID";
        private const string _errmsg5 = "Empty or missing connectionStringName";
        private const string _errmsg6 = "Missing connection string";
        private const string _errmsg7 = "Empty connection string";
        protected Dictionary<int, SiteMapNode> _nodes = new Dictionary<int, SiteMapNode>(16);
        protected SiteMapNode _root;
        protected readonly object _lock = new object();
        /// <summary>
        /// EL Atributo flag es para poder refrescar el sitemap en caso se realice un
        /// modificacion sobre la tabla
        /// </summary>
        public static bool flag = false;
        //private CustomSiteMapProviderClient providerClientObject = null;
        //private void DoConstructorProviderClient()
        //{
        //    if (providerClientObject == null)
        //        providerClientObject = GetConstructorProviderClient();
        //}
        public virtual CustomSiteMapProviderClient GetConstructorProviderClient()
        {
            return null;
        }
        public CustomSiteMapProviderClient ProviderClientObject
        {
            get
            {
                //DoConstructorProviderClient();
                return GetConstructorProviderClient();
            }
        }
        protected int _IdSistema;
        public static int Id;
        public static bool seleccionado;
        private bool _Seleccionado;
        private int _IdRol;
        public static int IdR;
        public bool Seleccionado
        {
            get { return _Seleccionado; }
            set
            {
                _Seleccionado = value;
                seleccionado = value;
            }
        }

        public int IdSistema
        {
            get
            {

                return _IdSistema;
            }
            set
            {
                _IdSistema = value;
                Id = value;
            }
        }
        public int IdRol
        {
            get
            {

                return _IdRol;
            }
            set
            {
                _IdRol = value;
                IdR = value;
            }
        }
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);


        }
        public virtual bool AllowToAddSiteMapNode(SiteMapNode node)
        {
            return true;
        }
        public virtual void OnBeforeToBuildSiteMap()
        {

        }
        protected void addNodeToCache(SiteMapNode node)
        {
            HttpContext.Current.Cache.Insert(getCacheKey(), node, null);
        }
        public override SiteMapNode BuildSiteMap()
        {
            OnBeforeToBuildSiteMap();
            if (HttpContext.Current.Cache[getCacheKey()] != null)
            {
                SiteMapNode mySn = (SiteMapNode)HttpContext.Current.Cache[getCacheKey()];
                return mySn;
            }

            if (seleccionado == true)
            {
                _root = null;
                seleccionado = false;
            }
            if (_root != null && flag)
                return _root;


            _root = null;
            _nodes.Clear();
            Clear();
            List<BESiteMap> SiteMapList = new List<BESiteMap>();
            SiteMapList = ProviderClientObject.BuildSiteMap(Id);


            for (int i = 0; i < SiteMapList.Count; i++)
            {
                if (i == 0)
                {
                    _root = CreateSiteMapNodeFromBESiteMap(SiteMapList[i]);
                    AddNode(_root, null);
                    continue;
                }
                //if (AllowToAddSiteMapNode(SiteMapList[i]))
                //{
                SiteMapNode node = CreateSiteMapNodeFromBESiteMap(SiteMapList[i]);
                AddNode(node, GetParentNodeFromBESiteMap(SiteMapList[i]));
                //}
                //User abc;
                ///////////////////////////////
                //System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            }
            flag = true;
            addNodeToCache(_root);
            int a = _root.ChildNodes.Count;
            return _root;
        }
        protected override SiteMapNode GetRootNodeCore()
        {
            BuildSiteMap();
            return _root;

        }
        public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (context == null)
                throw new ArgumentNullException("context");

            if (!this.SecurityTrimmingEnabled)
                return true;


            if ((node.Roles != null) && (node.Roles.Count > 0))
            {
                foreach (string role in node.Roles)
                {
                    bool isAst = !string.Equals(role, "*", StringComparison.InvariantCultureIgnoreCase);//False
                    //or
                    bool isContextUserNull = (context.User == null);//false
                    bool IsInrole = !context.User.IsInRole(role);//and false

                    bool allow = !AllowToAddSiteMapNode(node);//and False

                    if (node.ToString().Contains("Comi"))
                    {
                        Console.Write("asd");  
                    }

                    if (!string.Equals(role, "*", StringComparison.InvariantCultureIgnoreCase) &&
                       ((context.User == null) || !context.User.IsInRole(role)))
                    {
                        continue;
                    }
                    if (!AllowToAddSiteMapNode(node))
                        continue;

                    return true;
                }
            }
            return false;
        }
        private SiteMapNode CreateSiteMapNodeFromBESiteMap(BESiteMap oBESiteMap)
        {
            //string[] roles = null;
            //if (!String.IsNullOrEmpty(oBESiteMap.Roles))
            //    roles = oBESiteMap.Roles.Split(new char[] { ',', ';' }, 512);

            //SiteMapNode node = new SiteMapNode(this, oBESiteMap.Id.ToString(), oBESiteMap.Url, oBESiteMap.Title,
            //                       oBESiteMap.Descripcion, roles, null, null, null);

            //_nodes.Add(oBESiteMap.Id, node);

            //return node;
            string[] roles = null;
            if (!String.IsNullOrEmpty(oBESiteMap.Rol))
                roles = oBESiteMap.Rol.Split(new char[] { ',', ';' }, 512);

            SiteMapNode node = new SiteMapNode(this, oBESiteMap.Id.ToString(), oBESiteMap.Url, oBESiteMap.Title,
                                   oBESiteMap.Descripcion, roles, null, null, null);

            _nodes.Add(oBESiteMap.Id, node);

            return node;
        }
        private SiteMapNode GetParentNodeFromBESiteMap(BESiteMap oBESiteMap)
        {

            // Get the parent ID from the DataReader


            object pid = oBESiteMap.IdParent;
            // Make sure the parent ID is valid

            if (!_nodes.ContainsKey(Convert.ToInt32(pid)))
                throw new ProviderException(_errmsg4);

            // Return the parent SiteMapNode
            return _nodes[Convert.ToInt32(pid)];


        }
        public void OnSiteMapChanged()
        {
            lock (_lock)
            {
                Clear();
                _root = null;
                _nodes.Clear();
                //Purge any nodes we have in our cache and force a refresh from the db
                purgeNodesFromCache();
            }
        }
        protected virtual void purgeNodesFromCache()
        {
            List<string> itemsToRemove = new List<string>();
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                if (enumerator.Key.ToString().StartsWith(getCacheKey()))
                {
                    itemsToRemove.Add(enumerator.Key.ToString());
                }
            }

            foreach (string itemToRemove in itemsToRemove)
            {
                HttpContext.Current.Cache.Remove(itemToRemove);
            }
        }
        protected string getCacheKey()
        {
            return "SitemapDB";
        }
        //public bool UpdateRoleInSiteMap(int IdSiteMap, int Roles)
        //{
        //    return new CustomSiteMapProviderClient().UpdateRoleInSiteMap(IdSiteMap, Roles);
        //}

    }
}
