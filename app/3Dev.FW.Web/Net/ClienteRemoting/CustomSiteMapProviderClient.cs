using System;
using System.Collections.Generic;
using System.Data;
using _3Dev.FW.EmsambladoComun.Seguridad;
using _3Dev.FW.Entidades.Seguridad;

namespace _3Dev.FW.Web
{
    public class CustomSiteMapProviderClient : _3Dev.FW.EmsambladoComun.Seguridad.ICustomSiteMapProvider
    {
        public CustomSiteMapProviderClient()
        {
        }
        public virtual _3Dev.FW.EmsambladoComun.Seguridad.ICustomSiteMapProvider RemoteServicesInterfaceObject
        {
            get
            {
                return null;
            }
        }
        //public List<BESiteMap> BuildSiteMap(int idSistema)
        //{
        //    return RemoteServicesInterfaceObject.BuildSiteMap(idSistema);
        //}

        //string conexionSiteMap = ConfigurationManager.AppSettings["BindSiteMapProvider"].ToString();
        //public CustomSiteMapProviderClient()
        //{
        //}
        //public BESiteMap[] BuildSiteMap(int idSistema)
        //{
        //    return  RemoteServicesInterfaceObject.BuildSiteMap(idSistema);
        //}
        public List<BESiteMap> BuildSiteMap(int idSistema)
        {
            return RemoteServicesInterfaceObject.BuildSiteMap(idSistema);
        }
        public bool UpdateRoleInSiteMap(int IdSiteMap, int IdSistema, string Roles)
        {
            return RemoteServicesInterfaceObject.UpdateRoleInSiteMap(IdSiteMap, IdSistema, Roles);
        }
        public DataTable GetPagesValidMenu(int IdSistema)
        {
            return RemoteServicesInterfaceObject.GetPagesValidMenu(IdSistema);
        }
        public DataTable GetControlOptionsByPage(string PageName)
        {
            return RemoteServicesInterfaceObject.GetControlOptionsByPage(PageName);
        }
        public int GetIdSistemaByIDSiteMapNode(int idSiteMapNode)
        {
            return RemoteServicesInterfaceObject.GetIdSistemaByIDSiteMapNode(idSiteMapNode);
        }
        public List<BESiteMap> GetPagesLikNameForSistema(string Name, int idSistema)
        {
            return RemoteServicesInterfaceObject.GetPagesLikNameForSistema(Name, idSistema);
        }
        public BESiteMap GetSiteMapById(int IdSiteMap)
        {
            return RemoteServicesInterfaceObject.GetSiteMapById(IdSiteMap);
        }
        public List<BESiteMap> GetSiteMapWithURLBySistema(int idSistema)
        {
            return RemoteServicesInterfaceObject.GetSiteMapWithURLBySistema(idSistema);
        }
        public bool AddControlToSitemap(int SiteMapId, int ControlId)
        {
            return RemoteServicesInterfaceObject.AddControlToSitemap(SiteMapId, ControlId);
        }
        public bool RemoveControlToSitemap(int SiteMapId, int ControlId)
        {
            return RemoteServicesInterfaceObject.RemoveControlToSitemap(SiteMapId, ControlId);
        }
        public List<BESiteMap> GetMenus(int idSistema, int idSiteMap)
        {
            return RemoteServicesInterfaceObject.GetMenus(idSistema, idSiteMap);
        }
        public List<BESiteMap> GetSiteMapBySistemaId(int idSistema)
        {
            return RemoteServicesInterfaceObject.GetSiteMapBySistemaId(idSistema);
        }
        public int AddSitemap(BESiteMap beSiteMap)
        {
            return RemoteServicesInterfaceObject.AddSitemap(beSiteMap);
        }
        public bool UpdateSitemap(BESiteMap beSiteMap)
        {
            return RemoteServicesInterfaceObject.UpdateSitemap(beSiteMap);
        }
        public bool UpdateSitemapRoles(int SiteMapId)
        {
            return RemoteServicesInterfaceObject.UpdateSitemapRoles(SiteMapId);
        }
        public BESiteMap GetSiteMapBySistemaURL(string URL, int SistemaId, int ParentId)
        {
            return RemoteServicesInterfaceObject.GetSiteMapBySistemaURL(URL, SistemaId, ParentId);
        }
        public BESiteMap GetSiteMapByTitleParent(string Title, int ParentId)
        {
            return RemoteServicesInterfaceObject.GetSiteMapByTitleParent(Title, ParentId);
        }
        public virtual List<BESiteMap> GetSiteMapsByIdRol(int idRol)
        {
            return RemoteServicesInterfaceObject.GetSiteMapsByIdRol(idRol);
        }

        public string GetCabezeraMenu(string urlPagina)
        {
            return RemoteServicesInterfaceObject.GetCabezeraMenu(urlPagina);
        }

        #region ICustomSiteMapProvider Members


        List<BESiteMap> ICustomSiteMapProvider.GetPagesLikNameForSistema(string Name, int idSistema)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        List<BESiteMap> ICustomSiteMapProvider.GetSiteMapWithURLBySistema(int idSistema)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        List<BESiteMap> ICustomSiteMapProvider.GetMenus(int idSistema, int idSiteMap)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        List<BESiteMap> ICustomSiteMapProvider.GetSiteMapBySistemaId(int idSistema)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
