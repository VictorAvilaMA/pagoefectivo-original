using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Security;
using _3Dev.FW.Entidades.Seguridad;

/// <summary>
/// Summary description for SqlRoleProvider
/// </summary>
/// 
namespace _3Dev.FW.Web
{
public class SqlRoleProvider:RoleProvider 

{
    //private SqlRoleProviderClient providerClientObject = null;
    //private void DoConstructorProviderClient()
    //{
    //    if (providerClientObject == null)
    //        providerClientObject = GetConstructorProviderClient();
    //}
    public virtual SqlRoleProviderClient GetConstructorProviderClient()
    {
        return null;
    }
    public SqlRoleProviderClient ProviderClientObject
    {
        get
        {
            //DoConstructorProviderClient();
            return GetConstructorProviderClient();
        }
    }

    public override void Initialize(string name, NameValueCollection config)
    {
        base.Initialize(name, config);
        
        string sconfig = "";
        for (int i = 0; i <= config.Count - 1; i++)
        {
            sconfig += config.GetKey(i).Trim() + ":";
            sconfig += config[i].Trim() + ";";
        }
        ProviderClientObject.Initialize(name, sconfig);        
    }

    public override bool IsUserInRole(string username, string roleName)
    {
        return ProviderClientObject.IsUserInRole(username, roleName);
    }
    
    public override string[] GetRolesForUser(string username)
    {
        return ProviderClientObject.GetRolesForUser(username);
    }
    

    public override void CreateRole(string roleName)
    {
        ProviderClientObject.CreateRole(roleName);
    }


    //public void CreateRole(string roleName, string roleCodigo)
    //{
    //    ProviderClientObject.CreateRole(roleName, roleCodigo);
    //}
    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
        return ProviderClientObject.DeleteRole(roleName, throwOnPopulatedRole);
    }

    public override bool RoleExists(string roleName)
    {
        return ProviderClientObject.RoleExists(roleName);
    }

    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
        ProviderClientObject.AddUsersToRoles(usernames, roleNames);
    }

    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
        ProviderClientObject.RemoveUsersFromRoles(usernames, roleNames);
    }

    public override string[] GetUsersInRole(string roleName)
    {
        return ProviderClientObject.GetUsersInRole(roleName);
    }

    public override string[] GetAllRoles()
    {
        return ProviderClientObject.GetAllRoles();
    }
    public List<BERol> GetAllRolesList()
    {
        return ProviderClientObject.GetAllRolesList();
    }

    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
        return ProviderClientObject.FindUsersInRole(roleName, usernameToMatch);
    }

    public override string ApplicationName
    {
        get { return ProviderClientObject.ApplicationName; }
        set { ProviderClientObject.ApplicationName = value; }
    }
    public bool UpdateRoleName(string NewRoleName, string OldRoleName, int PermisoHastaSeccion, string NombreSeccion)
    {
        return ProviderClientObject.UpdateRoleName(NewRoleName, OldRoleName, PermisoHastaSeccion, NombreSeccion);
    }
    public bool AddUserToRol(string UserName, string RoleName)
    {
        return ProviderClientObject.AddUserToRol(UserName, RoleName);
    }
    public bool RemoveUserFromRol(string UserName, string RoleName)
    {
        return ProviderClientObject.RemoveUserFromRol(UserName, RoleName);
    }

    public bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
    {
        return ProviderClientObject.UpdateUserName(NewUserName, OldUserName, RoleName);
    }

    public List<BERol> getRolByID(int id)
    {
        return ProviderClientObject.getRolById(id);
    }
    public List<BERol> GetRolLikeName(string Name)
    {
        return ProviderClientObject.GetRolLikeName(Name);

        
    }
}
}