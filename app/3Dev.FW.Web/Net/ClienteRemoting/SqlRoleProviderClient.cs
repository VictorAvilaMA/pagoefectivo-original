using System;
using System.Collections.Generic;
using _3Dev.FW.Entidades.Seguridad;


namespace _3Dev.FW.Web
{
public class SqlRoleProviderClient : _3Dev.FW.EmsambladoComun.Seguridad.ISqlRoleProvider
{   

    public virtual _3Dev.FW.EmsambladoComun.Seguridad.ISqlRoleProvider RemoteServicesInterfaceObject
    {
        get
        {
            return null;
        }
    }
    public void Initialize(string name, string config)
    {
        RemoteServicesInterfaceObject.Initialize(name, config);       
     
    }
    public bool IsUserInRole(string username, string roleName)
    {
        return RemoteServicesInterfaceObject.IsUserInRole(username, roleName);

    }

    public string[] GetRolesForUser(string username)
    {
        return RemoteServicesInterfaceObject.GetRolesForUser(username);

    }

    public void CreateRole(string roleName)
    {
        RemoteServicesInterfaceObject.CreateRole(roleName);

    }
    //public void CreateRole(string roleName, string roleCodigo)
    //{
    //    RemoteServicesInterfaceObject.CreateRole(roleName, roleCodigo);

    //}
    public bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
        return RemoteServicesInterfaceObject.DeleteRole(roleName, throwOnPopulatedRole);
    }

    public bool RoleExists(string roleName)
    {
        return RemoteServicesInterfaceObject.RoleExists(roleName);
    }

    public void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
        RemoteServicesInterfaceObject.AddUsersToRoles(usernames, roleNames);
    }

    public void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
        RemoteServicesInterfaceObject.RemoveUsersFromRoles(usernames, roleNames);
    }

    public string[] GetUsersInRole(string roleName)
    {
        return RemoteServicesInterfaceObject.GetUsersInRole(roleName);
    }

    public string[] GetAllRoles()
    {
        return RemoteServicesInterfaceObject.GetAllRoles();
    }
    public List<BERol> GetAllRolesList()
    {
        return RemoteServicesInterfaceObject.GetAllRolesList();
    }
    public List<BERol> GetAllRolesList(string  username)
    {
        return RemoteServicesInterfaceObject.GetAllRolesList(username);
    }
    

    public string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
        return RemoteServicesInterfaceObject.FindUsersInRole(roleName, usernameToMatch);
    }

    public string ApplicationName
    {
        get { return RemoteServicesInterfaceObject.ApplicationName; }
        set
        {
            RemoteServicesInterfaceObject.ApplicationName = value;
        }

    }
    public bool UpdateRoleName(string NewRoleName, string OldRoleName, int PermisoHastaSeccion, string NombreSeccion)
    {
        return RemoteServicesInterfaceObject.UpdateRoleName(NewRoleName, OldRoleName, PermisoHastaSeccion, NombreSeccion);
    }
    public bool AddUserToRol(string UserName, string RoleName)
    {
        return RemoteServicesInterfaceObject.AddUserToRol(UserName, RoleName);
    }
    public bool RemoveUserFromRol(string UserName, string RoleName)
    {
        return RemoteServicesInterfaceObject.RemoveUserFromRol(UserName, RoleName);
    }
    public bool UpdateUserName(string NewUserName, string OldUserName, string RoleName)
    {
        return RemoteServicesInterfaceObject.UpdateUserName(NewUserName, OldUserName, RoleName);
    }

    
    public List<BERol> getRolById(int id)
    {
        return RemoteServicesInterfaceObject.getRolById(id);

    }

    #region ISqlRoleProvider Members


    public void InitializeSqlRoleProvider(string name, string config)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public void CreateRole2(string roleName, string RoleCodigo)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetAllRolesList2(string username)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public string ApplicationNameSqlRoleProvider
    {
        get
        {
            throw new Exception("The method or operation is not implemented.");
        }
        set
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    public bool UpdateRoleName(int idRol, string RoleName, string codigo)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public bool AddUserToRolSqlRoleProvider(string UserName, string RoleName)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public bool RemoveUserFromRol(int UserId, int RolId)
    {
        
        return RemoteServicesInterfaceObject.RemoveUserFromRol(UserId, RolId);
    }

    public List<BESistema> GetSistemasHabliesPorRol(int idRol)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public int GetRolIdByName(string Name)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetRolLikeName(string Name)
    {
        return RemoteServicesInterfaceObject.GetRolLikeName(Name);
    }

    public bool AddUserToRol(int UserId, int RolId)
    {
        
        return RemoteServicesInterfaceObject.AddUserToRol(UserId,RolId);

    }

    public bool AddSistemaToRol(int SistemaId, int RolId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public bool RemoveSistemaFromRol(int SistemaId, int RolId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetRolLikeNameForSistema(string Name, int SistemaId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetRolLikeNameForSistemaRelated(string Name, int SistemaId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetRolBySistema(int SistemaId)
    {
        return RemoteServicesInterfaceObject.GetRolBySistema(SistemaId);
    }

    public List<BERol> GetRolForUserId(int UserId)
    {
        return RemoteServicesInterfaceObject.GetRolForUserId(UserId );
    }

    public bool AddSiteMapToRol(int SiteMapId, int RolId)
    {
        return RemoteServicesInterfaceObject.AddSiteMapToRol(SiteMapId, RolId);
    }

    public bool RemoveSiteMapFromRol(int SiteMapId, int RolId)
    {
        return RemoteServicesInterfaceObject.RemoveSiteMapFromRol(SiteMapId, RolId);
    }

    public List<BERol> GetRolLikeNameForSistemaSiteMap(string Name, int SistemaId, int SiteMapId)
    {
        return RemoteServicesInterfaceObject.GetRolLikeNameForSistemaSiteMap(Name, SistemaId, SiteMapId);
     }

    public List<BERol> GetRolLikeNameForSistemaSiteMapRelated(string Name, int SistemaId, int SiteMapId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetRolLikeNameForSiteMap(string Name, int SiteMapId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    public List<BERol> GetRolForSiteMap(int SiteMapId)
    {
        throw new Exception("The method or operation is not implemented.");
    }

    #endregion
}
}