using System.Collections.Generic;
using _3Dev.FW.Entidades.Seguridad;




/// <summary>
/// Summary description for UsuarioClient
/// </summary>
/// 
namespace _3Dev.FW.Web
{
public class UsuarioClient 
{
    public UsuarioClient()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public virtual _3Dev.FW.EmsambladoComun.Seguridad.IUsuario RemoteServicesInterfaceObject
    {
        get
        {
            return null;
        }
    }

    /// <summary>
    /// Registrar Usuario
    /// </summary>
    /// <param name="eUsuario"></param>
    /// <returns></returns>
    public int RegistrarUsuario(BEUsuario eUsuario)
    {
        return RemoteServicesInterfaceObject.RegistrarUsuario(eUsuario);
    }
    /// <summary>
    /// Devuelve todos los Usuario
    /// </summary>
    /// <returns></returns>
    public List<BEUsuario> GetAllUsuarioList()
    {
        return RemoteServicesInterfaceObject.GetAllUsuarioList();
    }
    public int ModificarUsuario(BEUsuario eUsuario)
    {
        return RemoteServicesInterfaceObject.ModificarUsuario(eUsuario);
    }
    public BEUsuario GetUsuarioById(int IdUsuario)
    {
        return RemoteServicesInterfaceObject.GetUsuarioById(IdUsuario);
    }
    public int EliminarUsuario(int IdUsuario)
    {
        return RemoteServicesInterfaceObject.EliminarUsuario(IdUsuario);
    }
    public List<BEUsuario> GetUsuariosByFiltros(BEUsuario eUsuario)
    {
        return RemoteServicesInterfaceObject.GetUsuariosByFiltros(eUsuario);
    }
    public int IsUsuarioInRol(int IdUsuario)
    {
        return RemoteServicesInterfaceObject.IsUsuarioInRol(IdUsuario);
    }
    public List<BERolesByUsuario> GetRolesByUsuario(int IdUsuario)
    {
        return RemoteServicesInterfaceObject.GetRolesByUsuario(IdUsuario);
    }
    public List<BERol> GetRolesBySistema(int IdSistema)
    {
        return RemoteServicesInterfaceObject.GetRolesBySistema(IdSistema);
    }
    public int RemoveRolToUser(int id)
    {
        return RemoteServicesInterfaceObject.RemoveRolToUser(id);
    }
    public int AddUserToRol(int IdUsuario, int IdRol)
    {
        return RemoteServicesInterfaceObject.AddUserToRol(IdUsuario, IdRol);
    }
    public List<BERol> GetRoles(int userId,int idSistema)
    {
        return RemoteServicesInterfaceObject.GetRoles(userId, idSistema);
    }
    public List<BEPregunta> GetPreguntas()
    {
        return RemoteServicesInterfaceObject.GetPreguntas();
    }
    public bool SendMail(string usrname, int idpreg, string nomresp)
    {
        return RemoteServicesInterfaceObject.SendMail(usrname, idpreg, nomresp);
    }
    public BEUserInfo GetUserInfoByUserName(string userName)
    {
        return RemoteServicesInterfaceObject.GetUserInfoByUserName(userName);
    }
    public List<BEUsuario> GetUsuarioLikeNameForRol(string Name, int RolId)
    {
        return RemoteServicesInterfaceObject.GetUsuarioLikeNameForRol(Name, RolId);
    }

    //MODIFICADO PARA PAGINACION FASE III
    public List<BEUsuario> GetUsuarioLikeName(string Name, _3Dev.FW.Entidades.BusinessEntityBase entidad)
    {
        return RemoteServicesInterfaceObject.GetUsuarioLikeName(Name,entidad);
    }
}
}