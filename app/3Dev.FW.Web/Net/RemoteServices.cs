using System;
using System.Collections;
using System.Configuration;
using System.Runtime.Remoting.Channels;
using Belikov.GenuineChannels.TransportContext;
using Belikov.GenuineChannels.GenuineTcp;
using Belikov.GenuineChannels.Security.ZeroProofAuthorization;
using Belikov.GenuineChannels.DotNetRemotingLayer;
//using AFP.EmsambladoComun.ServiciosConocidos;

/// <summary>
/// Summary description for RemoteServices
/// </summary>
/// 

namespace _3Dev.FW.Web
{
public class RemoteServices
{
	#region atributos
	private string _serverUrl;
	private static RemoteServices _instance;
	private bool _isRemotingConfigured = false;
	private ITransportContext _iTransportContext;
	private string _parsedServerUri = "";
	private GenuineTcpChannel _genuineTcpChannel;
	private KeyProvider_ZpaClient _keyProvider_ZpaClient;
	private object _lockForInternalMembers = new object();
	#endregion

	public RemoteServices()
	{
	}

	public string ServerUrl
	{
		get
		{
			return this._serverUrl;
		}
	}

	public static RemoteServices Instance
	{
		get
		{
			return RemoteServices._instance;
		}
		set
		{
			RemoteServices._instance = value;
		}
	}
	public bool IsRemotingConfigured
	{
		get
		{
			lock (this._lockForInternalMembers)
				return this._isRemotingConfigured;
		}
	}
	public ITransportContext ITransportContext
	{
		get
		{
			return this._iTransportContext;
		}
	}
	public GenuineTcpChannel IGenuineTcpChannel
	{
		get
		{
			return this._genuineTcpChannel;
		}
	}
		
	//public System.Runtime.Remoting.Channels.Tcp.TcpChannel TcpChannel
	//{
	//    get { return _TcpChannel; }
	//}
	public HostInformation ServerHostInformation
	{
		get
		{
			return this.ITransportContext.KnownHosts[this._parsedServerUri];
		}
	}

	/// <summary>
	/// Zero Proof Authorization Security Key Provider.
	/// </summary>
	public KeyProvider_ZpaClient KeyProvider_ZpaClient
	{
		get
		{
			return this._keyProvider_ZpaClient;
		}
		set
		{
			this._keyProvider_ZpaClient = value;
		}
	}
	public virtual void DoSetupNetworkEnviroment()
	{ 
	
	}

	public bool SetupNetworkEnvironment()
	{
		lock (_lockForInternalMembers)
		{

			if (!this.IsRemotingConfigured)
			{
				// create the channel
				Hashtable properties = new Hashtable();

				//properties["InvocationTimeout"] = ConfigurationManager.AppSettings.Get("InvocationTimeout");

				#region "..."
				//this._TcpChannel = new System.Runtime.Remoting.Channels.Tcp.TcpChannel(properties, null, null);
				//ChannelServices.RegisterChannel(this._TcpChannel, false);
				//
				#endregion
				properties["InvocationTimeout"] = ConfigurationManager.AppSettings.Get("InvocationTimeout");
				this._genuineTcpChannel = new GenuineTcpChannel(properties, null, null);
				ChannelServices.RegisterChannel(this._genuineTcpChannel, false);

				// create and register the Zero Proof Authorization Security Key Provider
				this._keyProvider_ZpaClient = new KeyProvider_ZpaClient(ZpaFeatureFlags.None, null, null);

				// subscribe to channel events
				GenuineGlobalEventProvider.GenuineChannelsGlobalEvent += new GenuineChannelsGlobalEventHandler(this.ProcessChannelEvent);

				this._isRemotingConfigured = true;
			}

			for (; ; )
			{

				string serverUrl = null;
				bool isGtcp = true;
				if (isGtcp)
					this._iTransportContext = this._genuineTcpChannel.ITransportContext;

				if (serverUrl == null)
					this._serverUrl = ConfigurationManager.AppSettings.Get("tcpDefaultURL");



				//this._ICustomSiteMapProvider = (ICustomSiteMapProvider)Activator.GetObject(typeof(ICustomSiteMapProvider), this.ServerUrl + "/" + _3Dev.FW.EmsambladoComun.ServiciosConocidos.CustomSiteMapProviderServices);
				//this._ISqlRoleProvider = (ISqlRoleProvider)Activator.GetObject(typeof(_3Dev.FW.EmsambladoComun.Seguridad.ISqlRoleProvider), this.ServerUrl + "/" + ServiciosConocidos.SqlRoleProviderServices);
				//this._IUsuario = (IUsuario)Activator.GetObject(typeof(IUsuario), this.ServerUrl + "/" + ServiciosConocidos.UsuarioServices);
				//this._ICustomMembershipProvider = (ICustomMembershipProvider)Activator.GetObject(typeof(ICustomMembershipProvider), this.ServerUrl + "/" + ServiciosConocidos.CustomMembership);
				DoSetupNetworkEnviroment();
				try
				{

				}

				catch (Exception ex)
				{
					ex.Message.ToString();
					continue;
				}

				break;
			}

		}
		return true;
	}

	private bool _connectionEstablished;

	public bool ConnectionEstablished
	{
		get
		{
			return this._connectionEstablished;
		}
		set
		{
			if (this._connectionEstablished == value)
				return;

			this._connectionEstablished = value;

		}
	}

	private bool _connectionIsReestablished;

	public bool ConnectionIsReestablished
	{
		get
		{
			return this._connectionIsReestablished;
		}
		set
		{
			if (this._connectionIsReestablished == value)
				return;

			this._connectionIsReestablished = value;

		}
	}

	public void ProcessChannelEvent(object sender, GenuineEventArgs e)
	{


		switch (e.EventType)
		{
			case GenuineEventType.GeneralConnectionEstablished:
				this.ConnectionEstablished = true;
				this.ConnectionIsReestablished = false;
				break;

			case GenuineEventType.GeneralConnectionReestablishing:
				this.ConnectionIsReestablished = true;
				break;

			case GenuineEventType.GeneralConnectionClosed:
				this.ConnectionEstablished = false;
				this.ConnectionIsReestablished = false;
				break;

			case GenuineEventType.GeneralNewSessionDetected:
			case GenuineEventType.GeneralServerRestartDetected:
				// the server has been restarted, it is necessary to reload all content
				break;
		}
	}
}

}