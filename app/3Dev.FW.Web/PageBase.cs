using System;
using System.Web.UI;
using _3Dev.FW.Entidades.Seguridad;

namespace _3Dev.FW.Web
{
    public class PageBase : System.Web.UI.Page
    {
        public object  VariableTransicion
        {
            get {return  Session["VariableTransicion"]; }
            set { Session["VariableTransicion"] = value; }
        }


        public void NavigateTo(string url)
        {
            Response.Redirect(url);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!Page.IsPostBack)
                OnFirstLoadPage();
        }

        public virtual void OnFirstLoadPage()
        {

        }

        public static BEUserInfo UserInfo
        {
            get
            {
                return Security.UserInfo;
            }
        }

    }
}
