using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace _3Dev.FW.Web
{
    [Serializable]
    public class PageManager
    {
        private static PageManagerStatus status;
        public PageManagerStatus Status
        {
            get { return status; }
            set { status = value; }
        }


        public static bool IsEditing
        {
            get { return status == PageManagerStatus.IsEditing; }
        }
        public static bool IsInserting
        {
            get { return status == PageManagerStatus.IsInserting; }
        }
        public bool IsSaving
        {
            get { return status == PageManagerStatus.IsSaving; }
        }
        public bool IsInactive
        {
            get { return status == PageManagerStatus.IsInactive; }
        }

        public static HttpResponse SetExcel(HttpResponse Response)
        {
            Response.Charset = "ISO 8859-1";
            Response.ContentEncoding = Encoding.Default;
            return Response;
        }
    }

}
