using System;

namespace _3Dev.FW.Web
{
    [Serializable]
    public enum PageManagerStatus
    {
        IsEditing,
        IsInserting,
        IsSaving,
        IsInactive
    }
}
