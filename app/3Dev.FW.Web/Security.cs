using System.Web;
using _3Dev.FW.Entidades.Seguridad;

namespace _3Dev.FW.Web
{
    public class Security
    {
        public static BEUserInfo UserInfo
        {
            get
            {
                if (HttpContext.Current.Session[_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo] ==null)
                {
                    HttpContext.Current.Response.Redirect(System.Web.Security.FormsAuthentication.LoginUrl);
                }
                
                return (BEUserInfo)HttpContext.Current.Session[_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo];
            }
        }
        public static void ClearUserInfo()
        {
            
            HttpContext.Current.Session.Remove(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo);
        }
        public static void LoadUserInfo(BEUserInfo be)
        {                                                     
            HttpContext.Current.Session[_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo] = be;
        }
        public static bool ValidatePageAccess(string urlpage)
        {
            _urlpage = urlpage;
          BERolPagina rolpagina= UserInfo.PaginasAcceso.Find(PredicateUrlPagina);
          return rolpagina != null;
        }
        private static string _urlpage;
        private static bool PredicateUrlPagina(BERolPagina  oBERolPagina)
        {
            return ( oBERolPagina.Url.ToUpper() == _urlpage.ToUpper());
        }
    }
}