﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _3Dev.FW.Web
{
    public class TotalRegistrosTableAdapter
    {
        private object dataGrilla;
        private Int32 totalVirtual;

        public TotalRegistrosTableAdapter(object data, Int32 totVirtual)
        {
            this.dataGrilla = data;
            this.totalVirtual = totVirtual;
        }

        public object GetDataGrilla() { return dataGrilla; }

        public Int32 ObtenerTotalRegistros() { return totalVirtual; }

        public object GetDataGrilla(int filaInicio, int maxFilas) { return dataGrilla; }
    }
}
