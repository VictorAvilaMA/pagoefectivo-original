using System;
using System.Web.UI.WebControls;

namespace _3Dev.FW.Web
{
   public class WebMessage
    {

       const string defaultcssClassError = "MensajeValidacion";
       const string defaultcssClassSuccessfullMessage = "MensajeTransaccion";
       const string defaultcssClassWarningMessage = "MensajeTransaccion";
       const string defaultErrorMessage = "Ha ocurrido un error, cont�ctese con el Administrador.";
       const string defaultSuccessfullMessage = "La operaci�n se realiz� correctamente.";

       private Label lblMessage;
       private string errorMessage = "Ha ocurrido un error, cont�ctese con el Administrador.";
       private string successfullMessage = "La operaci�n se realiz� correctamente.";
       private string cssClassError = "";
       private string cssClassSuccessfullMessage = "";
       private string cssClassWarningMessage = "";

       public WebMessage()
       {
       }

       public WebMessage(Label lblMessage)
       {
          this.Initialize(lblMessage);
       }

       public void Initialize(Label lblMessage)
       {
           this.lblMessage = lblMessage;
           Initialize(lblMessage, defaultErrorMessage, defaultSuccessfullMessage);
       }
       public void Initialize(Label lblMessage, string errorMessage, string successfullMessage)
       {
           Initialize(lblMessage, defaultErrorMessage, defaultSuccessfullMessage, defaultcssClassError, defaultcssClassSuccessfullMessage, defaultcssClassWarningMessage);
       }
       public void Initialize(Label lblMessage, string defaultErrorMessage, string defaultSuccessfullMessage, string cssClassError, string cssClassSuccessfullMessage, string cssClassWarningMessage)
       {
           this.errorMessage = defaultErrorMessage;
           this.successfullMessage = defaultSuccessfullMessage;
           this.cssClassError = cssClassError;
           this.cssClassSuccessfullMessage = cssClassSuccessfullMessage;
           this.cssClassWarningMessage = cssClassWarningMessage;
           this.lblMessage = lblMessage;
           lblMessage.Text = "";
       }

       
       protected virtual Label LblMessage
       {
           get
           {
               return lblMessage;
           }
       }


       public void Clear()
       {
           lblMessage.Text = "";
       }
       public void ShowErrorMessage(Exception e, bool showWithMsgDefault)
       {
           ShowErrorMessage(e.Message,showWithMsgDefault );
       }
       public void ShowErrorMessage(Exception e)
       {
           ShowErrorMessage(e.Message);
       }
       public void ShowErrorMessage(string msg)
       {
           ShowErrorMessage(msg, false);
       }
       public void ShowErrorMessage(string msg,bool showWithMsgDefault)
       {
           if (lblMessage != null)
           {
               lblMessage.CssClass = cssClassError;
               if (showWithMsgDefault)
                   lblMessage.Text =string.Format("{0}:{1}", defaultErrorMessage, msg);
               else
               lblMessage.Text = msg;
           }
       }
       public void ShowSuccessfullyMessage(string msg)
       {
           ShowSuccessfullyMessage(msg, false);
       }
       public void ShowSuccessfullyMessage(string msg, bool showWithMsgDefault)
       {
           if (lblMessage != null)
           {
               lblMessage.CssClass = cssClassSuccessfullMessage;
               if (showWithMsgDefault)
                   lblMessage.Text = string.Format("{0}:{1}", defaultErrorMessage, msg);
               else
                   lblMessage.Text = msg;
           }
       }
       public void ShowWarningMessage(string msg)
       {
           if (lblMessage != null)
           {
               lblMessage.CssClass = cssClassWarningMessage;
               lblMessage.Text = msg;
           }
       }

   
   
    }
}
