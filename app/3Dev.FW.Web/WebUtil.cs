using System;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;

namespace _3Dev.FW.Web
{
    public class WebUtil
    {

        //       Public Sub DropDownListBinding(ByVal drop As DropDownList, ByVal dataSource As Object, ByVal dataTextField As String, ByVal dataValueField As String)

        //    If Not drop Is Nothing Then
        //        drop.DataSource = dataSource
        //        drop.DataTextField = dataTextField
        //        drop.DataValueField = dataValueField
        //        drop.DataBind()
        //    End If


        //End Sub
        public static void DropDownlistBinding(DropDownList drop, object datasource, string dataTextField, string dataValueField)
        {
            DropDownlistBinding(drop, datasource, dataTextField, dataValueField, "");
        }
        public static void DropDownlistBinding(DropDownList drop, object datasource, string dataTextField, string dataValueField, string firstItem)
        {
            if (drop != null)
            {
                drop.Items.Clear();
                drop.SelectedIndex = -1;
                drop.SelectedValue = null;
                drop.DataSource = datasource;
                drop.DataTextField = dataTextField;
                drop.DataValueField = dataValueField;
                drop.DataBind();
                if (firstItem != "")
                {
                    drop.Items.Insert(0, new ListItem(firstItem, ""));
                    drop.SelectedIndex = 0;
                }
            }
        }

        public static string GetAbsoluteUriWithOutQuery(Uri uri)
        {
            if (uri.Query.Length == 0) return uri.AbsoluteUri;
            else return uri.AbsoluteUri.Replace(uri.Query, "");
        }

        const string styledisable = "color: #000000;background-color: gainsboro;";
        public static void DisableTextBox(TextBox control)
        {
            if (control.Style.Value == null) control.Style.Value = "";
            control.Style.Value = control.Style.Value.Replace(styledisable, "");
            control.Style.Value = control.Style.Value + styledisable;
            control.Style.Value = control.Style.Value;
            control.ReadOnly = true;
        }
        public static void EnableTextBox(TextBox control)
        {
            if (control.Style.Value == null) control.Style.Value = "";
            control.Style.Value = control.Style.Value.Replace(styledisable, "");
            control.ReadOnly = false;
        }
        public static void EnableTextBox(TextBox control, bool enabled)
        {
            if (enabled) EnableTextBox(control);
            else DisableTextBox(control);
        }
        public static ValidateImageResult ValidateImage(FileUpload imageFile)
        {
            if ((!(imageFile.PostedFile == null)) && (imageFile.PostedFile.ContentLength > 0))
            {
                if (imageFile.HasFile)
                {
                    //'Get the extension of the uploaded file.
                    string extension = System.IO.Path.GetExtension(imageFile.FileName);
                    //' Get the size in bytes of the file to upload.
                    int fileSize = imageFile.PostedFile.ContentLength;
                    if ((extension.ToUpper() == ".GIF") ||
                         (extension.ToUpper() == ".JPG") ||
                         (extension.ToUpper() == ".JPEG") ||
                         (extension.ToUpper() == ".PNG"))
                    {
                        if (fileSize < Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"]))
                        {
                            //fisUpload.Visible = false;
                            return ValidateImageResult.LoadedOk;
                        }
                        else
                        {
                            //if(lblUpdateLoad!=null)
                            //     lblUpdateLoad.Text = "  ** Nota: La imagen no fue cargada porque excede el tama�o limite de 2MB.";
                            //fisUpload.Visible = true;
                            return ValidateImageResult.LoadedErrorWeightLimitExceeded;
                        }
                    }
                    else
                    {
                        //if (lblUpdateLoad != null)
                        //    lblUpdateLoad.Text = "  ** Nota: La imagen no fue cargada porque no tiene una extensi�n v�lida.";
                        //fisUpload.Visible = true;
                        return ValidateImageResult.LoadedErrorExtensionInvalid;
                    }
                }
                else
                {
                    //fisUpload.Visible = false;
                    return ValidateImageResult.LoadedErrorHasNoFile;
                }
            }
            else
            {
                // fisUpload.Visible = false;
                return ValidateImageResult.LoadedOk;
            }
        }

        public static byte[] GetImage(FileUpload imageFile)
        {
            if ((!(imageFile.PostedFile == null)) && (imageFile.PostedFile.ContentLength > 0))
            {
                if (imageFile.HasFile)
                {
                    //'Get the extension of the uploaded file.
                    string extension = System.IO.Path.GetExtension(imageFile.FileName);
                    //' Get the size in bytes of the file to upload.
                    int fileSize = imageFile.PostedFile.ContentLength;

                    if ((extension.ToUpper() == ".GIF") ||
                         (extension.ToUpper() == ".JPG") ||
                         (extension.ToUpper() == ".JPEG") ||
                         (extension.ToUpper() == ".PNG"))
                    {
                        if (fileSize < Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"]))
                        {
                            HttpPostedFile imgFile = imageFile.PostedFile;
                            byte[] bytesImage = new byte[imageFile.PostedFile.ContentLength];// Dim bytesImage(ImagenFile.PostedFile.ContentLength) As Byte

                            imgFile.InputStream.Read(bytesImage, 0, imageFile.PostedFile.ContentLength);
                            return bytesImage;
                        }
                    }
                }
            }
            return null;
        }

        //<add PeruCom FaseIII>
        public static byte[] GetKeyPublic(FileUpload keyFile)
        {
            if ((!(keyFile.PostedFile == null)) && (keyFile.PostedFile.ContentLength > 0))
            {
                if (keyFile.HasFile)
                {
                    //'Get the extension of the uploaded file.
                    string extension = System.IO.Path.GetExtension(keyFile.FileName);
                    //' Get the size in bytes of the file to upload.
                    int fileSize = keyFile.PostedFile.ContentLength;

                    if ((extension.ToUpper() == ".P1Z"))
                    {
                        if (fileSize < Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"]))
                        {
                            HttpPostedFile imgFile = keyFile.PostedFile;
                            byte[] bytesImage = new byte[keyFile.PostedFile.ContentLength];// Dim bytesImage(ImagenFile.PostedFile.ContentLength) As Byte

                            imgFile.InputStream.Read(bytesImage, 0, keyFile.PostedFile.ContentLength);
                            return bytesImage;
                        }
                    }
                }
            }
            return null;
        }
        public enum ValidateImageResult
        {
            LoadedOk = 1,
            LoadedErrorWeightLimitExceeded = 2,
            LoadedErrorExtensionInvalid = 3,
            LoadedErrorHasNoFile
        }

        #region Construccion Menu Dinamico
        public string ObtenerMenu(SiteMapNode oSiteMapNode)
        {
            string Cadena = string.Empty;
            int Count = 0;
            if (!oSiteMapNode.HasChildNodes)
                return Cadena;

            Cadena += "<ul id=\"menu_principal\">";
            foreach (SiteMapNode item in oSiteMapNode.ChildNodes)
            {
                Count = Count + 1;
                Cadena = string.Concat(Cadena, ObtenerNodo(item, 1, Count, string.Empty));
            }
            Cadena += "</ul>";

            return Cadena;
        }

        public string ObtenerMenu(SiteMapNode oSiteMapNode, List<_3Dev.FW.Entidades.Seguridad.BERolPagina> PaginasPermitidas)
        {
            string Cadena = string.Empty;
            int Count = 0;
            if (!oSiteMapNode.HasChildNodes)
                return Cadena;

            Cadena += "<ul id=\"menu_principal\">";
            foreach (SiteMapNode item in oSiteMapNode.ChildNodes)
            {
                Count = Count + 1;
                Cadena = string.Concat(Cadena, ObtenerNodo(item, 1, Count, string.Empty, PaginasPermitidas));
            }
            Cadena += "</ul>";

            return Cadena;
        }

        public string ObtenerNodo(SiteMapNode oSiteMapNode, int Nivel, int numChild, string IdNodo)
        {
            string Cadena = string.Empty;
            string strNuevoId;
            int Count = 0;
            string nuevoURL = "";
            string urlCapturada = string.Empty;

            if (oSiteMapNode.Title.Length > 27)
                oSiteMapNode.Title = oSiteMapNode.Title.Substring(0, 20) + "...";

            strNuevoId = ((!string.IsNullOrEmpty(IdNodo)) ? string.Concat(IdNodo, ".", numChild.ToString().PadLeft(2, '0')) : numChild.ToString().PadLeft(2, '0'));

            if (!oSiteMapNode.HasChildNodes)
            {
                nuevoURL = oSiteMapNode.Url;
                //nuevoURL += nuevoURL.Contains("?") ? "&" : "?";
                // ----------------------------------------------
                // CARPETAS DEFINIDAS DENTRO DE LA APLICACI�N WEB
                // ----------------------------------------------
                string strAppPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                string strUrlReffer = String.Empty;
                if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                {
                    strUrlReffer = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
                    nuevoURL = strUrlReffer.Substring(0, strUrlReffer.ToLower().IndexOf(strAppPath.ToLower())) + strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                }
                else
                {
                    nuevoURL = strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                }

                //nuevoURL += "JER=" + Identificador + Nivel + "_" + strNuevoId;

                switch (Nivel)
                {
                    case 1:
                        Cadena += "<li><img src=\"" + System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/atras.png\" alt=\"\" class=\"element_atr\" /><p class=\"contenedor_menu\">";
                        Cadena += "<a href=\"" + nuevoURL + "\">" + oSiteMapNode.Title + "</a></p></li>";
                        break;
                    case 2:
                        //Cadena += "<a href=\"" + nuevoURL + "\"><span class=\"lista_it\">- </span> " + " " + oSiteMapNode.Title + "</a>";
                        Cadena += "<li class=\"has_submen\"><p class=\"noactiva\"><span class=\"lista_it\">>></span> " + "<a href=\"" + nuevoURL + "\"> " + " " + oSiteMapNode.Title + "</a>" + "</p></li>";
                        break;
                    default:
                        Cadena += "<li><a href=\"" + nuevoURL + "\"><span class=\"lista_it\">- </span> " + " " + oSiteMapNode.Title + "</a></li>";
                        break;
                }
            }
            else
            {

                string nuevoTitle = " " + oSiteMapNode.Title;
                if (!String.IsNullOrWhiteSpace(oSiteMapNode.Url))
                {
                    nuevoURL = oSiteMapNode.Url;
                    string strAppPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                    string strUrlReffer = String.Empty;
                    if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                    {
                        strUrlReffer = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
                        nuevoURL = strUrlReffer.Substring(0, strUrlReffer.ToLower().IndexOf(strAppPath.ToLower())) + strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                    }
                    else
                    {
                        nuevoURL =  strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                    }
                }
                switch (Nivel)
                {
                    case 1:
                        if (String.IsNullOrEmpty(nuevoURL))
                            nuevoTitle = "<a class=\"cambio\" href=\"" + nuevoURL + "\" onclick=\"return false;\"> " + oSiteMapNode.Title + "</a>";
                        else
                            nuevoTitle = "<a href=\"" + nuevoURL + "\"> " + oSiteMapNode.Title + "</a>";
                        Cadena += "<li><img src=\"" + System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/atras.png\" alt=\"\" class=\"element_atr\" style=\"display:hidden\" ></img><p class=\"contenedor_menu\">" + nuevoTitle + "<span class=\"cambio\">Mostrar</span></p>";
                        Cadena += "<ul class=\"submenu\">";
                        break;
                    default:
                        if (String.IsNullOrEmpty(nuevoURL))
                            nuevoTitle = "<a class=\"cambioHijo\" href=\"" + nuevoURL + "\" onclick=\"return false;\"> " + oSiteMapNode.Title + "</a>";
                        else
                            nuevoTitle = "<a href=\"" + nuevoURL + "\"> " + oSiteMapNode.Title + "</a>";
                        Cadena += "<li class=\"has_submen\"><p class=\"noactiva\"><span class=\"lista_it\">>></span> " + nuevoTitle + "</p>";
                        Cadena += "<ul class=\"subsubmenu\">";
                        break;
                }
                foreach (SiteMapNode item in oSiteMapNode.ChildNodes)
                {
                    Count = Count + 1;
                    Cadena = string.Concat(Cadena, ObtenerNodo(item, Nivel + 1, Count, strNuevoId));
                }
                Cadena = string.Concat(Cadena, "</ul>");
                Cadena = string.Concat(Cadena, "</li>");
            }

            return Cadena;

        }
        public string ObtenerNodo(SiteMapNode oSiteMapNode, int Nivel, int numChild, string IdNodo, List<_3Dev.FW.Entidades.Seguridad.BERolPagina> PaginasPermitidas)
        {
            string Cadena = string.Empty;
            string strNuevoId;
            int Count = 0;
            string nuevoURL = "";
            string urlCapturada = string.Empty;

            if (oSiteMapNode.Title.Length > 27)
                oSiteMapNode.Title = oSiteMapNode.Title.Substring(0, 20) + "...";

            strNuevoId = ((!string.IsNullOrEmpty(IdNodo)) ? string.Concat(IdNodo, ".", numChild.ToString().PadLeft(2, '0')) : numChild.ToString().PadLeft(2, '0'));

            if (!oSiteMapNode.HasChildNodes)
            {
                nuevoURL = oSiteMapNode.Url;
                //nuevoURL += nuevoURL.Contains("?") ? "&" : "?";
                // ----------------------------------------------
                // CARPETAS DEFINIDAS DENTRO DE LA APLICACI�N WEB
                // ----------------------------------------------
                string strAppPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                string strUrlReffer = String.Empty;
                if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                {
                    strUrlReffer = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
                    nuevoURL = strUrlReffer.Substring(0, strUrlReffer.ToLower().IndexOf(strAppPath.ToLower())) + strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                }
                else
                {
                    nuevoURL = strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                }

                //nuevoURL += "JER=" + Identificador + Nivel + "_" + strNuevoId;

                switch (Nivel)
                {
                    case 1:
                        Cadena += "<li><img src=\"" + System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/atras.png\" alt=\"\" class=\"element_atr\" /><p class=\"contenedor_menu\">";
                        Cadena += "<a href=\"" + nuevoURL + "\">" + oSiteMapNode.Title + "</a></p></li>";
                        break;
                    case 2:
                        //Cadena += "<a href=\"" + nuevoURL + "\"><span class=\"lista_it\">- </span> " + " " + oSiteMapNode.Title + "</a>";
                        Cadena += "<li class=\"has_submen\"><p class=\"noactiva\"><span class=\"lista_it\">>></span> " + "<a href=\"" + nuevoURL + "\"> " + " " + oSiteMapNode.Title + "</a>" + "</p></li>";
                        break;
                    default:
                        Cadena += "<li><a href=\"" + nuevoURL + "\"><span class=\"lista_it\">- </span> " + " " + oSiteMapNode.Title + "</a></li>";
                        break;
                }
            }
            else
            {

                string nuevoTitle = " " + oSiteMapNode.Title;
                if (!String.IsNullOrWhiteSpace(oSiteMapNode.Url))
                {
                    nuevoURL = oSiteMapNode.Url;
                    string strAppPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                    string strUrlReffer = String.Empty;
                    if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
                    {
                        strUrlReffer = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
                        nuevoURL = strUrlReffer.Substring(0, strUrlReffer.ToLower().IndexOf(strAppPath.ToLower())) + strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                    }
                    else
                    {
                        nuevoURL =  strAppPath + "/" + (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("urlBase")) ? "" : (ConfigurationManager.AppSettings.Get("urlBase") + "/")) + nuevoURL.Replace("~/", "");
                    }
                }
                switch (Nivel)
                {
                    case 1:
                        if (String.IsNullOrEmpty(nuevoURL))
                            nuevoTitle = "<a class=\"cambio\" href=\"" + nuevoURL + "\" onclick=\"return false;\"> " + oSiteMapNode.Title + "</a>";
                        else
                            nuevoTitle = "<a href=\"" + nuevoURL + "\"> " + oSiteMapNode.Title + "</a>";
                        Cadena += "<li><img src=\"" + System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/atras.png\" alt=\"\" class=\"element_atr\" style=\"display:hidden\" ></img><p class=\"contenedor_menu\">" + nuevoTitle + "<span class=\"cambio\">Mostrar</span></p>";
                        Cadena += "<ul class=\"submenu\">";
                        break;
                    default:
                        if (String.IsNullOrEmpty(nuevoURL))
                            nuevoTitle = "<a class=\"cambioHijo\" href=\"" + nuevoURL + "\" onclick=\"return false;\"> " + oSiteMapNode.Title + "</a>";
                        else
                            nuevoTitle = "<a href=\"" + nuevoURL + "\"> " + oSiteMapNode.Title + "</a>";
                        Cadena += "<li class=\"has_submen\"><p class=\"noactiva\"><span class=\"lista_it\">>></span> " + nuevoTitle + "</p>";
                        Cadena += "<ul class=\"subsubmenu\">";
                        break;
                }
                foreach (SiteMapNode item in oSiteMapNode.ChildNodes)
                {
                    Count = Count + 1;
                    foreach (var pageone in PaginasPermitidas)
                    {
                        if (pageone.IdPagina.ToString().Trim()==item.Key.Trim())
                        {
                            Cadena = string.Concat(Cadena, ObtenerNodo(item, Nivel + 1, Count, strNuevoId));
                        }
                    }
                }
                Cadena = string.Concat(Cadena, "</ul>");
                Cadena = string.Concat(Cadena, "</li>");
            }

            return Cadena;

        }
        private string getStyle(string name)
        {
            string result = name.Replace('�', 'a').Replace('�', 'e').Replace('�', 'i').Replace('�', 'o').Replace('�', 'u').Replace('�', 'n');
            return result;
        }

        public string ObtenerMenuCabezera(string cadenaString)
        {
            string cadenaHTML = String.Empty;
            string[] niveles = cadenaString.Split('|');
            for (int i = 0; i < niveles.Length; i++)
            {
                if (!String.IsNullOrEmpty(niveles[i].Trim()))
                {
                    if (i == niveles.Length - 1)
                        cadenaHTML += "<label>" + niveles[i].Trim() + "</label>";
                    else
                        cadenaHTML += "<label>" + niveles[i].Trim() + "</label><span> / </span>";
                }

            }
            return cadenaHTML;
        }

        #endregion
    }
}
