Imports Microsoft.VisualBasic

Public Class CntrlSuscripciones
    Public Function ObtenerCuentasPendientes(ByVal idEnte As String) As ProxySuscripciones.ObtenerCuentasPendientesResultListaCuentaPendiente()
        Dim Proxy As New ProxySuscripciones.Service
        Return Proxy.ObtenerCuentasPendientes(idEnte)
    End Function

    Public Function ObtenerCorrelativo(ByVal strUrlRefer As String) As Integer
        Dim Proxy As New ProxyCorrelativo.Service
        Return Proxy.ConsultarNumCorrelativo(strUrlRefer)
    End Function
End Class
