Imports System.Data
Imports System.Collections.Generic
Imports System.IO
Imports System.Drawing.Text
Imports System.Xml
Partial Class FormSuscriptores
    Inherits System.Web.UI.Page

    Dim PredicateEvaluaMoneda As New Predicate(Of ProxySuscripciones.ObtenerCuentasPendientesResultListaCuentaPendiente)(AddressOf EvaluaMoneda)
    Dim monedatemp As String = ""
    Private Function EvaluaMoneda(ByVal obj As ProxySuscripciones.ObtenerCuentasPendientesResultListaCuentaPendiente) As Boolean
        Return monedatemp = obj.Moneda
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Button1.PostBackUrl = System.Configuration.ConfigurationManager.AppSettings("urlPagoEfectivo")

            Dim pNumDoc, pCliente, pCodCli, pDir As Object
            Dim pcodSoles, pcodDolares As String
            'pTipoDocSus = System.Configuration.ConfigurationManager.AppSettings("TipoDocSuscriptor")
            pNumDoc = System.Configuration.ConfigurationManager.AppSettings("num_documento")
            pCliente = System.Configuration.ConfigurationManager.AppSettings("des_cliente")
            pCodCli = System.Configuration.ConfigurationManager.AppSettings("cod_cliente")
            pDir = System.Configuration.ConfigurationManager.AppSettings("des_direccion")

            pcodSoles = System.Configuration.ConfigurationManager.AppSettings("codsoles")
            pcodDolares = System.Configuration.ConfigurationManager.AppSettings("coddolares")

            'If Not Page.PreviousPage Is Nothing Then
            cod_cliente.Text = Request.Form(pCodCli)
            cliente.Text = Request.Form(pCliente)

            dir.Text = Request.Form(pDir)
            numdoc.Text = Request.Form(pNumDoc)
            If (cod_cliente.Text <> "") Then
                Dim cntrlSuscripciones As CntrlSuscripciones = New CntrlSuscripciones()
                Dim resultado As ProxySuscripciones.ObtenerCuentasPendientesResultListaCuentaPendiente() = cntrlSuscripciones.ObtenerCuentasPendientes(cod_cliente.Text)
                monedatemp = pcodSoles
                gvResultadoS.DataSource = System.Array.FindAll(Of ProxySuscripciones.ObtenerCuentasPendientesResultListaCuentaPendiente)(resultado, PredicateEvaluaMoneda)
                gvResultadoS.DataBind()
                monedatemp = pcodDolares
                gvResultadoD.DataSource = System.Array.FindAll(Of ProxySuscripciones.ObtenerCuentasPendientesResultListaCuentaPendiente)(resultado, PredicateEvaluaMoneda)
                gvResultadoD.DataBind()
                msj.Visible = False
                msj.Text = ""
                pGrillas.Visible = True
            Else
                msj.Text = "No existen los datos del cliente para hacer la busqueda."
                msj.Visible = True
                pGrillas.Visible = False
            End If

        End If




    End Sub

    Public Shared Function EncriptarStr(ByVal str As String) As String
        Dim libx As New clsLibreriax()
        Return libx.Encrypt(Str, libx.fEncriptaKey)
    End Function


    Protected Sub soles_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles soles.CheckedChanged
        Dim ch As New CheckBox
        asignarMInicial()
        dolares.Checked = False
        gvResultadoD.Enabled = False

        gvResultadoS.Enabled = True
        msj.Visible = False
    End Sub

    Protected Sub dolares_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dolares.CheckedChanged
        Dim ch As New CheckBox
        asignarMInicial()
        soles.Checked = False

        gvResultadoD.Enabled = True

        gvResultadoS.Enabled = False
        msj.Visible = False
    End Sub

    Public Sub asignarMInicial()
        If (dolares.Checked = True) Then
            moneda.Text = System.Configuration.ConfigurationManager.AppSettings("dolar")
            moneda2.Text = System.Configuration.ConfigurationManager.AppSettings("dolar")
        ElseIf (soles.Checked = True) Then
            moneda.Text = System.Configuration.ConfigurationManager.AppSettings("soles")
            moneda2.Text = System.Configuration.ConfigurationManager.AppSettings("soles")
        End If

        MontoTotal.Text = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings("montoInicial")).ToString("#,#0.00")
        MontoTotal2.Text = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings("montoInicial")).ToString("#,#0.00")
    End Sub

    Protected Sub chTodos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim grilla As New GridView
        Dim total As Decimal
        Dim gvr As GridViewRow
        Dim i As Integer
        msj.Visible = False
        If (gvResultadoS.Enabled = True) Then
            grilla = gvResultadoS
        Else
            grilla = gvResultadoD
        End If

        Dim chktodos As CheckBox = CType(sender, CheckBox)
        For Each gvr In grilla.Rows
            CType(gvr.FindControl("ch1"), CheckBox).Checked = chktodos.Checked
            total += Convert.ToDecimal(CType(gvr.FindControl("lblSaldo"), Label).Text)
            i = i + 1
            'total += Convert.ToDecimal(grilla.Rows(i).Cells(4).Text)
            If (chktodos.Checked = False) Then
                total = 0

            End If
        Next

        MontoTotal.Text = total.ToString("#,#0.00")
        MontoTotal2.Text = total.ToString("#,#0.00")
    End Sub
    Protected Sub ch1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim grilla As New GridView
        Dim i As Integer
        Dim total As Decimal
        msj.Visible = False
        If (soles.Checked = True) Then
            grilla = gvResultadoS
        Else
            grilla = gvResultadoD
        End If

        Dim ch As CheckBox

        For i = 0 To grilla.Rows.Count - 1
            ch = grilla.Rows(i).Cells(0).FindControl("ch1")

            If ch.Checked = True Then
                total += Convert.ToDecimal(CType(grilla.Rows(i).FindControl("lblSaldo"), Label).Text)
                ' total += Convert.ToDecimal(grilla.Rows(i).Cells(4).Text)

            End If
        Next

        MontoTotal.Text = total.ToString("#,#0.00")
        MontoTotal2.Text = total.ToString("#,#0.00")

    End Sub
    Protected Sub pagar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles pagar.Click

        validarCuenta()

    End Sub
    Public Sub validarCuenta()
        If (Convert.ToDecimal(MontoTotal.Text) <= 0) Then
            msj.Visible = True
            msj.Text = "El monto debe ser mayor que 0"
        Else
            pagarCuenta()
        End If


    End Sub


    Public Sub pagarCuenta()
        Dim Util As New UtilOrdenPagoXML
        Dim moneda As String
        Dim grilla As New GridView
        Dim ch As New CheckBox
        Dim i As Integer
        Dim valor As Boolean
        Dim total As Decimal
        Dim CodCli As String
        CodCli = cod_cliente.Text
        If (soles.Checked = True) Then
            moneda = "1"
        Else
            moneda = "2"
        End If
        If (gvResultadoS.Enabled = True) Then
            grilla = gvResultadoS
        Else
            grilla = gvResultadoD
        End If
        For i = 0 To grilla.Rows.Count - 1
            ch = grilla.Rows(i).Cells(0).FindControl("ch1")
            valor = ch.Checked
            If valor = True Then
                total += Convert.ToDecimal(CType(grilla.Rows(i).FindControl("lblSaldo"), Label).Text)

            End If

        Next


        Util.AgregarCabecera("", "", "", moneda, "", total.ToString(), "", "", "", "", "", cod_cliente.Text, "", cliente.Text, "", "", "", "", "")
        For i = 0 To grilla.Rows.Count - 1
            ch = grilla.Rows(i).Cells(0).FindControl("ch1")
            valor = ch.Checked
            If valor = True Then
                Dim strSerie, strCorrelativo As String
                strSerie = Convert.ToString(CType(grilla.Rows(i).Cells(2).FindControl("lblSerie"), Label).Text)
                strCorrelativo = Convert.ToString(CType(grilla.Rows(i).Cells(2).FindControl("lblCorrelativo"), Label).Text)

                Util.AgregarDetalle(Convert.ToString(CType(grilla.Rows(i).Cells(5).FindControl("lblNroInterno"), Label).Text), grilla.Rows(i).Cells(1).Text.ToString(), "", Convert.ToString(CType(grilla.Rows(i).Cells(5).FindControl("lblSaldo"), Label).Text), grilla.Rows(i).Cells(3).Text.ToString(), Convert.ToString(CType(grilla.Rows(i).Cells(2).FindControl("lblNroDocumento"), Label).Text), CodCli)

            End If

        Next
        TextBox6.Text = EncriptarStr(Util.ObtenerXMLString())

        Dim script As String = "  <script language='javascript' type ='text/javascript' >  Enviar(); </script> "
        'ClientScript.RegisterClientScriptBlock(GetType(Page), "stenviar", script)
        ClientScript.RegisterStartupScript(GetType(Page), "stenviar", script)
    End Sub

    Protected Sub pagar2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles pagar2.Click

        validarCuenta()

    End Sub


End Class
