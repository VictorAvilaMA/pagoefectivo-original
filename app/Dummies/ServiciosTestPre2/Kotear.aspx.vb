
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub btnPagoEfectivo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPagoEfectivo.Click
        '
        If (chkUtilizaEncriptacion.Checked) Then

            Dim objclsLibreria As New clsLibreriax
            Dim subparam As New StringBuilder
            Dim sbsubparam As New StringBuilder
            If (chkMerchantID.Checked) Then sbsubparam.Append(txtMerchantID.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtMerchantIDValor.Text) : sbsubparam.Append("|")
            If (chkOrderID.Checked) Then sbsubparam.Append(txtOrderID.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtOrderIDValor.Text) : sbsubparam.Append("|")
            If (chkUrlOk.Checked) Then sbsubparam.Append(txtUrlOk.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUrlOkValor.Text) : sbsubparam.Append("|")
            If (chkUrlError.Checked) Then sbsubparam.Append(txtUrlError.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUrlErrorValor.Text) : sbsubparam.Append("|")
            If (chkMailCom.Checked) Then sbsubparam.Append(txtMailCom.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtMailComValor.Text) : sbsubparam.Append("|")
            If (chkOrdenDesc.Checked) Then sbsubparam.Append(txtOrdenDesc.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtOrdenDescValor.Text) : sbsubparam.Append("|")

            If (chkUsuarioID.Checked) Then sbsubparam.Append(txtUsuarioID.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioIDValor.Text) : sbsubparam.Append("|")
            If (chkDataAdicional.Checked) Then sbsubparam.Append(txtDataAdicional.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtDataAdicionalValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioNombre.Checked) Then sbsubparam.Append(txtUsuarioNombre.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioNombreValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioApellidos.Checked) Then sbsubparam.Append(txtUsuarioApellidos.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioApellidosValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioLocalidad.Checked) Then sbsubparam.Append(txtUsuarioLocalidad.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioLocalidadValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioDomicilio.Checked) Then sbsubparam.Append(txtUsuarioDomicilio.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioDomicilioValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioProvincia.Checked) Then sbsubparam.Append(txtUsuarioProvincia.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioProvinciaValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioPais.Checked) Then sbsubparam.Append(txtUsuarioPais.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioPaisValor.Text) : sbsubparam.Append("|")
            If (chkMoneda.Checked) Then sbsubparam.Append(txtMoneda.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtMonedaValor.Text) : sbsubparam.Append("|")
            If (chkMonto.Checked) Then sbsubparam.Append(txtMonto.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtMontoValor.Text) : sbsubparam.Append("|")
            If (chkUsuarioAlias.Checked) Then sbsubparam.Append(txtUsuarioAlias.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioAliasValor.Text) ': sbsubparam.Append("|")

            '
            subparam.Append(txtServer.Text)
            subparam.Append(txtMerchantID.Text) : subparam.Append("=") : subparam.Append(txtMerchantIDValor.Text) : subparam.Append("&")
            subparam.Append(txtDatosEnc.Text) : subparam.Append("=") : subparam.Append(objclsLibreria.Encrypt(sbsubparam.ToString, objclsLibreria.fEncriptaKey))
            '
            Response.Redirect(subparam.ToString, False)
            '

        Else

            Dim subparam As New StringBuilder
            Dim sbsubparam As New StringBuilder
            If (chkMerchantID.Checked) Then sbsubparam.Append(txtMerchantID.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtMerchantIDValor.Text) : sbsubparam.Append("&")
            If (chkMonto.Checked) Then sbsubparam.Append(txtMonto.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtMontoValor.Text) : sbsubparam.Append("&")
            If (chkUsuarioID.Checked) Then sbsubparam.Append(txtUsuarioID.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioIDValor.Text) : sbsubparam.Append("&")
            If (chkUsuarioAlias.Checked) Then sbsubparam.Append(txtUsuarioAlias.Text) : sbsubparam.Append("=") : sbsubparam.Append(txtUsuarioAliasValor.Text)
            subparam.Append(txtServer.Text)
            'subparam.Append("?")
            subparam.Append(sbsubparam.ToString())
            '
            Response.Redirect(subparam.ToString(), False)

        End If
    End Sub

End Class
