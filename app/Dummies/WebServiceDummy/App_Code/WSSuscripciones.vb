Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class WSSuscripciones
     Inherits System.Web.Services.WebService

    <WebMethod()> _
Public Function WSObtenerCuentasPendientes(ByVal idente As String) As CuentaPendResponse
        Dim response As New CuentaPendResponse
        response.ListaCuentaPendiente = New CuentaPendResponseListaCuentaPendiente() {}


        System.Array.Resize(response.ListaCuentaPendiente, response.ListaCuentaPendiente.Length + 1)
        Dim entidad As CuentaPendResponseListaCuentaPendiente = New CuentaPendResponseListaCuentaPendiente()
        entidad.Correlativo = "1"
        entidad.FechaVencimiento = "25/10/2009"
        entidad.Moneda = "1"
        entidad.Monto = "100.43"
        entidad.NroInterno = "2323423"
        entidad.Saldo = "55.43"
        entidad.Serie = "1"
        entidad.TipoDoc = "FAC"
        entidad.NumDocumento = entidad.Serie + "-" + entidad.Correlativo
        response.ListaCuentaPendiente(0) = entidad


        System.Array.Resize(response.ListaCuentaPendiente, response.ListaCuentaPendiente.Length + 1)
        Dim entidad2 As CuentaPendResponseListaCuentaPendiente = New CuentaPendResponseListaCuentaPendiente()
        entidad2.Correlativo = "2"
        entidad2.FechaVencimiento = "25/10/2009"
        entidad2.Moneda = "1"
        entidad2.Monto = "100.43"
        entidad2.NroInterno = "2323423"
        entidad2.Saldo = "55.43"
        entidad2.Serie = "1"
        entidad2.TipoDoc = "FAC"
        entidad2.NumDocumento = entidad.Serie + "-" + entidad.Correlativo
        response.ListaCuentaPendiente(1) = entidad2


        System.Array.Resize(response.ListaCuentaPendiente, response.ListaCuentaPendiente.Length + 1)
        Dim entidad3 As CuentaPendResponseListaCuentaPendiente = New CuentaPendResponseListaCuentaPendiente()
        entidad3.Correlativo = "2"
        entidad3.FechaVencimiento = "25/10/2009"
        entidad3.Moneda = "1"
        entidad3.Monto = "130.43"
        entidad3.NroInterno = "2323423"
        entidad3.Saldo = "55.43"
        entidad3.Serie = "1"
        entidad3.TipoDoc = "FAC"
        entidad3.NumDocumento = entidad.Serie + "-" + entidad.Correlativo
        response.ListaCuentaPendiente(2) = entidad3
        Return response

    End Function

End Class
