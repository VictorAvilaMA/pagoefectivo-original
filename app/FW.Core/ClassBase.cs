using System;
using System.Collections.Generic;
using System.Text;

namespace FW.Core
{
    public class ClassBase : IDisposable
    {
        #region IDisposable Members

        private bool disposedValue = false;
        public virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
