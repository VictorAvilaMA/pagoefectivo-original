<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default.aspx.cs" Inherits="Demo._Default" %>

<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Demo</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server" />--%>
        <asp:ScriptManager ID="scripManager1" runat="server" ></asp:ScriptManager>
        This page shows that the ModalUpdateProgress works for all async postbacks on the
        page.<br />
        <br />
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            <asp:ListItem Text="Option A"></asp:ListItem>
            <asp:ListItem Text="Option B"></asp:ListItem>
            <asp:ListItem Text="Option C"></asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList2" runat="server">
            <asp:ListItem Text="Option A"></asp:ListItem>
            <asp:ListItem Text="Option B"></asp:ListItem>
            <asp:ListItem Text="Option C"></asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="Button1" runat="server" Text="Button1" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="Button2" OnClick="Button2_Click" />
        <asp:LinkButton ID="Button3" runat="server" Text="Button3" OnClick="Button3_Click" />
        <asp:ModalUpdateProgress ID="ModalUpdateProgress1" runat="server" DisplayAfter="0"
            BackgroundCssClass="modalProgressGreyBackground">
            <ProgressTemplate>
                <div class="modalPopup">
                    Loading
                    <img src="indicator.gif" align="middle" />
                </div>
            </ProgressTemplate>
        </asp:ModalUpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h1 style="border: solid 1 black;">
                    Panel #1:
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button1" />
                <asp:AsyncPostBackTrigger ControlID="Button2" />
                <asp:AsyncPostBackTrigger ControlID="Button3" />
                <asp:AsyncPostBackTrigger ControlID="DropDownList1" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
