<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" Codebehind="MasterPage1.aspx.cs"
    Inherits="Demo.MasterPage1" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    This page shows how to associate ModalUpdateProgress to individual UpdatePanels
    on the page.<br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h1 style="border: solid 1 black;">
                Panel #1:
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
            <asp:Button ID="Button1" runat="server" Text="Button1" OnClick="Button1_Click" />
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem Text="Option A"></asp:ListItem>
                <asp:ListItem Text="Option B"></asp:ListItem>
                <asp:ListItem Text="Option C"></asp:ListItem>
            </asp:DropDownList>
            <asp:ModalUpdateProgress ID="ModalUpdateProgress1" runat="server" DisplayAfter="0"
                AssociatedUpdatePanelID="UpdatePanel1" BackgroundCssClass="modalProgressGreyBackground">
                <ProgressTemplate>
                    <div class="modalPopup">
                        Loading
                        <img src="indicator.gif" align="middle" />
                    </div>
                </ProgressTemplate>
            </asp:ModalUpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <h1 style="border: solid 1 black;">
                Panel #2:
                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
            <asp:Button ID="Button2" runat="server" Text="Button2" OnClick="Button2_Click" />
            <asp:ModalUpdateProgress ID="ModalUpdateProgress2" runat="server" DisplayAfter="300"
                CancelControlID="Cancel" AssociatedUpdatePanelID="UpdatePanel2" BackgroundCssClass="modalProgressRedBackground">
                <ProgressTemplate>
                    <div class="modalPopup">
                        Loading
                        <img src="indicator.gif" align="middle" />
                        <p>
                            <asp:Button ID="Cancel" runat="server" Text="Cancel" /></p>
                    </div>
                </ProgressTemplate>
            </asp:ModalUpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
