﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Amazon;
using Amazon.CloudWatchLogs;
using Amazon.CloudWatchLogs.Model;
using NLog.Common;
using NLog.Config;

namespace NLog.Targets.AWSCloudWatchLog
{
    [Target("CloudWatchLogs")]
    public sealed class CloudWatchLogsTarget : TargetWithLayout
    {

        private IAmazonCloudWatchLogs _client;
        private static ConcurrentDictionary<string, string> _tokens = new ConcurrentDictionary<string, string>();

        [RequiredParameter]
        public string AWSAccessKeyId { get; set; }

        [RequiredParameter]
        public string AWSSecretKey { get; set; }

        [RequiredParameter]
        public string AWSRegion { get; set; }

        [RequiredParameter]
        public string LogGroupName { get; set; }

        [RequiredParameter]
        public string LogStreamName { get; set; }

        
        protected override void InitializeTarget()
        {
            base.InitializeTarget();

            InternalLogger.Debug("Initialising AWSCloudWatch '{0}' target", AWSRegion);

            try
            {

                if (string.IsNullOrEmpty(AWSAccessKeyId) || string.IsNullOrEmpty(AWSSecretKey))
                {
                    InternalLogger.Info("AWS Access Keys are not specified. Use Application Setting or EC2 Instance profile for keys.");
                    _client = new AmazonCloudWatchLogsClient(RegionEndpoint.GetBySystemName(AWSRegion));
                }
                else
                {
                    _client = new AmazonCloudWatchLogsClient(AWSAccessKeyId, AWSSecretKey, RegionEndpoint.GetBySystemName(AWSRegion));
                }

            }
            catch (Exception e)
            {
                InternalLogger.Fatal("Amazon CloudWatch client failed to be configured and won't send any messages. Error is\n{0}\n{1}", e.Message, e.StackTrace);
            }

            InternalLogger.Debug("Initialised AWSCloudWatch '{0}' target", AWSRegion);
        }


        protected override void Write(LogEventInfo logEvent) 
        {
            InternalLogger.Debug("Sending log to AWSCloudWatch '{0}' {1}.{2}", AWSRegion, LogGroupName, LogStreamName);
            try
            {

                try
                {

                    var log = new InputLogEvent { Message = Layout.Render(logEvent), Timestamp = logEvent.TimeStamp };
                    var logEvents = new List<InputLogEvent> { log };

                    var request = new PutLogEventsRequest
                    {
                        LogGroupName = LogGroupName,
                        LogStreamName = LogStreamName,
                        SequenceToken = _tokens.GetOrAdd(GetTokenKey(), Init),
                        LogEvents = logEvents.OrderBy(e => e.Timestamp).ToList()
                    };

                    var response = _client.PutLogEvents(request);

                    _tokens.AddOrUpdate(GetTokenKey(), response.NextSequenceToken, (k, ov) => response.NextSequenceToken);

                    InternalLogger.Debug("Sent log to AWSCloudWatch, response received. HttpStatusCode: {0}, RequestId: {1}",
                       response.HttpStatusCode, response.ResponseMetadata.RequestId);

                }
                catch (AmazonCloudWatchLogsException e)
                {
                    InternalLogger.Fatal("RequestId: {0}, ErrorType: {1}, Status: {2}\nFailed to send log with\n{3}\n{4}",
                        e.RequestId, e.ErrorType, e.StatusCode,
                        e.Message, e.StackTrace);
                }
            }
            catch (Exception e)
            {
                InternalLogger.Fatal("Failed to write log to Amazon CloudWatch with\n{0}\n{1}",
                       e.Message, e.StackTrace);
            }
        }

        /// <summary>
        /// Gets token key for the current log group and stream.
        /// </summary>
        /// <returns>The token key string.</returns>
        private string GetTokenKey()
        {
            return string.Format("{0}:{1}", LogGroupName, LogStreamName);
        }

        /// <summary>
        /// Initializes the CloudWatch Logs group and stream and returns next sequence token.
        /// </summary>
        /// <returns>Next sequence token.</returns>
        private string Init(string key)
        {

            var logGroupsResponse = _client.DescribeLogGroups(new DescribeLogGroupsRequest { LogGroupNamePrefix = LogGroupName });
            string nextToken = null;

            var logGroups = logGroupsResponse.LogGroups;
            var group = logGroups.Find(lg => lg.LogGroupName == LogGroupName);
            if (group == null)
            {
                _client.CreateLogGroup(new CreateLogGroupRequest { LogGroupName = LogGroupName });
            }

            var describer = new DescribeLogStreamsRequest(LogGroupName = LogGroupName);
            var streams = new List<LogStream>(_client.DescribeLogStreams(describer).LogStreams).ToList();
            var stream = streams.Find(ls => ls.LogStreamName == LogStreamName);

            if (stream == null)
            {
                _client.CreateLogStream(new CreateLogStreamRequest
                {
                    LogStreamName = LogStreamName,
                    LogGroupName = LogGroupName
                });
            }
            else
            {
                nextToken = stream.UploadSequenceToken;
            }

            return nextToken;
        }

    }
}
