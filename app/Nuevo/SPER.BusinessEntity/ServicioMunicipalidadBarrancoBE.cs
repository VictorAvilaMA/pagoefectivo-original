﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPER.BusinessEntity
{
    public class ServicioMunicipalidadBarrancoBE : IDisposable
    {
        //Cabecera
        public string MerchantID {get; set;}
        public string CodigoActual { get; set; }
        public string CodigoContribuyenteComp { get; set; }
        public string NombreContribuyente { get; set; }
        public string ApellidoPaternoContribuyente { get; set; }
        public string ApellidoMaternoContribuyente { get; set; }
        public string EmailContribuyente { get; set; }

        //Detalle
        public string IdOrden { get; set; }
        public string SerieNroDocumento { get; set; }
        public string DescripcionOrden { get; set; }
        public string ConceptoDocPago { get; set; }
        public string MonedaDocPago { get; set; }
        public decimal ImporteDocPago { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public decimal ImporteMora { get; set; }
        public int NumeroCargaDiaria { get; set; }
        public int Tiempo { get; set; }
        public Int64 IdOrdenPago { get; set; }
        public DateTime FechaCarga { get; set; }
        public decimal Total { get; set; }

        public string strFechaVencimiento { get; set; }
        public string strImporteMora { get; set; }

        //Respuesta
        public string ClaveAPI { get; set; }
        public int IdEstado { get; set; }
        private int TipoError { get; set; }

        //Descarga de archivo
        public int IdServicio { get; set; }
        public string NombreServicio { get; set; }
        public decimal Tamano { get; set; }
        public string NombreArchivo { get; set; }
        public string NombreArchivoTXT { get; set; }
        public string NombreArchivoXLS { get; set; }
        public int Orden { get; set; }

        //Detalle archivo
        public int NumeroOrdenPago { get; set; }
        public DateTime FechaCancelacion { get; set; }

        //Detalle carga
        public int IdError { get; set; }
        public int CantCabArchivo { get; set; }
        public int CantDetArchivo { get; set; }
        public int CantCabProc { get; set; }
        public int CantDetProc { get; set; }

        public string CadenaErrores { get; set; }


        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
