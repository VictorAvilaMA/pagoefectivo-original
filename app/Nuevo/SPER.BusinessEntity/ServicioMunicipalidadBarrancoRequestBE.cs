﻿using System;

namespace SPER.BusinessEntity
{
    public class ServicioMunicipalidadBarrancoRequestBE : IDisposable
    {
        public byte[] Bytes { get; set; }
        public string TipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public int IdServicio { get; set; }
        public string MerchantID { get; set; }
        public DateTime FechaEmision { get; set; }
        public int NumeroCargaDiaria { get; set; }
        public string CodUsuario { get; set; }
        public int IdOrdenPago { get; set; }
        public string NroDocumento { get; set; }
        public DateTime FechaCargaInicial { get; set; }
        public DateTime FechaCargaFinal { get; set; }
        public int IdUsuarioActualizacion { get; set; }
        public int IdOrigenEliminacion { get; set; }
        public string GCParametro { get; set; }
        public string DesParametro { get; set; }


        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}