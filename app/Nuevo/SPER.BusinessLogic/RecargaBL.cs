﻿using System;
using System.Collections.Generic;

using SPER.BusinessEntity;
using SPER.DataAccess;
using SPER.Utility;
using SPE.Entidades;
namespace SPER.BusinessLogic
{
    public class RecargaBL :IDisposable
    {
        public BERespuesta ValidarRecarga(BERecarga oRecargaBE, string CadenaConexionPESaldos, string DNIInvalido1, string DNIInvalido2)
        {
            BERespuesta oRespuestaBE = new BERespuesta();
            RecargaDA oRecargaDA = new RecargaDA();
            List<BEMensaje> oListMensaje = oRecargaDA.MensajesValidacionRecarga(CadenaConexionPESaldos);

            if (Utiles.IsValid(oRecargaBE.xCIPRecarga))
            {
                if (oRecargaBE.xCIPRecarga.Length > 10)
                {
                    int LonguitudDNI = 8;
                    int LonguitudCodigoRecarga = 2;
                    int LonguitudExacta = 10;
                    int LonguitudVariable = oRecargaBE.xCIPRecarga.Length;
                    int Longuitud = LonguitudVariable - LonguitudExacta;

                    string NumeroDocumento = (String.IsNullOrEmpty(oRecargaBE.xCIPRecarga) ? "" : Utiles.ForLeft(oRecargaBE.xCIPRecarga, LonguitudDNI));

                    //if (NumeroDocumento == "00000000" || NumeroDocumento == "11111111")
                    if (NumeroDocumento == DNIInvalido1 || NumeroDocumento == DNIInvalido2)
                    {
                        oRespuestaBE.Resultado = false;
                        oRespuestaBE.Codigo = "05";
                        //oRespuestaBE.Mensaje = "DNI inválido";
                        oRespuestaBE.Mensaje = oListMensaje.Find(x => x.Codigo == oRespuestaBE.Codigo).Descripcion;
                    }
                    else
                    {

                        string CodigoRecarga = (String.IsNullOrEmpty(oRecargaBE.xCIPRecarga) ? "" : Utiles.ForMid(oRecargaBE.xCIPRecarga, LonguitudDNI, LonguitudCodigoRecarga));
                        string Digito11 = (String.IsNullOrEmpty(oRecargaBE.xCIPRecarga) ? "" : Utiles.ForMid(oRecargaBE.xCIPRecarga, LonguitudExacta, 1));
                        decimal Monto = (String.IsNullOrEmpty(oRecargaBE.xCIPRecarga) ? 0 : Convert.ToDecimal(Utiles.ForMid(oRecargaBE.xCIPRecarga, LonguitudExacta, Longuitud)));

                        oRecargaBE.xCIPRecarga = oRecargaBE.xCIPRecarga;
                        oRecargaBE.xDocumento = NumeroDocumento;
                        oRecargaBE.xCodigoRecarga = CodigoRecarga;
                        oRecargaBE.xDigito11 = Digito11;
                        oRecargaBE.mMonto = Monto;
                        oRespuestaBE = oRecargaDA.ValidarRecarga(oRecargaBE, CadenaConexionPESaldos);
                    }
                }
                else
                {
                    oRespuestaBE.Resultado = false;
                    oRespuestaBE.Codigo = "07";
                    //oRespuestaBE.Mensaje = "Código de recarga inválido";
                    oRespuestaBE.Mensaje = oListMensaje.Find(x => x.Codigo == oRespuestaBE.Codigo).Descripcion;
                }
            }
            else
            {
                oRespuestaBE.Resultado = false;
                oRespuestaBE.Codigo = "07";
                //oRespuestaBE.Mensaje = "Código de recarga inválido";
                oRespuestaBE.Mensaje = oListMensaje.Find(x => x.Codigo == oRespuestaBE.Codigo).Descripcion;
            }
            return oRespuestaBE;
        }

        public BERespuestaSaldos ConsultarRecarga(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().ConsultarRecarga(_recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldos RealizarRecarga(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().RealizarRecarga( _recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldos ExtornarRecarga(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().ExtornarRecarga( _recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldosBanbif ConsultarRecargaBanBif(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().ConsultarRecargaBanBif(_recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldosBanbif RealizarRecargaBanBif(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().RealizarRecargaBanBif(_recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldosBanbif ExtornarRecargaBanBif(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().ExtornarRecargaBanBif(_recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldosBBVA ConsultarRecargaBBVA(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().ConsultarRecargaBBVA(_recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldosBBVA RealizarRecargaBBVA(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().RealizarRecargaBBVA(_recargaBE, CadenaConexionPESaldos);
        }

        public BERespuestaSaldosBBVA ExtornarRecargaBBVA(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            return new RecargaDA().ExtornarRecargaBBVA(_recargaBE, CadenaConexionPESaldos);
        }

        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
