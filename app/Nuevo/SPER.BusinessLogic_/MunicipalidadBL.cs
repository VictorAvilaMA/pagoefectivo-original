﻿using System;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

using SPER.BusinessEntity;
using SPER.DataAccess;
using SPER.Utility;

namespace SPER.BusinessLogic
{
    public class MunicipalidadBL : IDisposable
    {
        
        public ServicioMunicipalidadBarrancoBE ProcesarArchivoServicioMunicipalidadBarranco(ref ServicioMunicipalidadBarrancoRequestBE oServicioMunicipalidadBarrancoRequest, string EmailUsuarioInst, int Vigente, string cadenaConexion, string ValTimeOut, int BatchSize)
        {
            ServicioMunicipalidadBarrancoBE oServicioMunicipalidadBarrancoBE = new ServicioMunicipalidadBarrancoBE();

            //int idErrorActualizarTiempoCarga = 0;
            int idErrorRegistrarCabecera = 0;
            int idErrorRegistrarDetalle = 0;

            int intCantCabArchivo = 0;
            int intCantDetArchivo = 0;
            int intCantCabProc = 0;
            int intCantDetProc = 0;
            int idError = 0;

            DataTable dtCabecera = new DataTable();
            DataTable dtDetalle = new DataTable();

            IniciarCabecera(dtCabecera);
            IniciarDetalle(dtDetalle);

            try
            {
                String Texto = Utiles.BytesToString(oServicioMunicipalidadBarrancoRequest.Bytes);

                //String Texto = System.Text.Encoding.UTF8.GetString(oServicioMunicipalidadBarrancoRequest.Bytes);
        
                int intCantidadCargaDiaria = oServicioMunicipalidadBarrancoRequest.NumeroCargaDiaria;
                //cabecera
                string strMerchantID = string.Empty;
                string strCodigoActual = string.Empty;
                string strCodigoContribuyenteComp = string.Empty;
                string strNombreContribuyente = string.Empty;
                string strApellidoPaternoContribuyente = string.Empty;
                string strApellidoMaternoContribuyente = string.Empty;
                string strEmailContribuyente = string.Empty;

                //detalle            
                int intIdOrden = 0;
                string strSerieNroDocumento = string.Empty;
                string strDescripcionOrden = string.Empty;
                int intMonedaDocPago = 0;
                decimal decImporteDocPago = 0;
                string strFechaEmision = null;
                string strFechaVencimiento = null;
                string decImporteMora = string.Empty;
                int intNumeroCargaDiaria = 0;
                int intTiempo = 0;
                int intIdOrdenPago = 0;
                string strCadenaErrores = string.Empty;
                int intLineaError = 1;
                DateTime dtNow = System.DateTime.Now;

                //Actualizar la tabla con tiempo = 2

                using (StringReader str = new StringReader(Texto))
                {
                    do
                    {
                        string Linea = str.ReadLine();
                        string[] cadena = Linea.Split(new Char[] { '|' });
                        intLineaError += 1;
                        if (Linea.StartsWith("CC") | Linea.StartsWith("DD"))
                        {
                            if (Linea.StartsWith("CC"))
                            {
                                if (cadena.Length == 8)
                                {
                                    strMerchantID = cadena[1].Trim();
                                    strCodigoActual = cadena[2];
                                    strCodigoContribuyenteComp = cadena[3].Trim();
                                    strNombreContribuyente = cadena[4].Trim();
                                    strApellidoPaternoContribuyente = cadena[5].Trim();
                                    strApellidoMaternoContribuyente = cadena[6].Trim();

                                    if (string.IsNullOrEmpty(cadena[7].Trim()))
                                    {
                                        strEmailContribuyente = string.Empty;
                                    }
                                    else
                                    {
                                        strEmailContribuyente = Utiles.ValidateEmail(cadena[7].ToLower().Replace("ñ", "n"), EmailUsuarioInst);
                                    }

                                    CargarCabecera(dtCabecera, strMerchantID, strCodigoActual, strCodigoContribuyenteComp, strNombreContribuyente, strApellidoPaternoContribuyente, strApellidoMaternoContribuyente, strEmailContribuyente);

                                    intCantCabArchivo += 1;

                                    if (idErrorRegistrarCabecera == 0)
                                    {
                                        intCantCabProc += 1;
                                    }
                                    else
                                    {
                                        strCadenaErrores += "o Linea: " + intLineaError.ToString() + "</br>";
                                    }
                                }
                                else
                                {
                                    strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString() + " Cabecera Incorrecta</br>";
                                    ActualizarServMunicipalidadBarranco(oServicioMunicipalidadBarrancoBE, 0, 0, 0, 0, 0, strCadenaErrores);
                                    return oServicioMunicipalidadBarrancoBE;
                                }
                            }
                            else
                            {
                                if (Linea.StartsWith("DD"))
                                {
                                    if (cadena.Length == 12)
                                    {
                                        if (cadena[1] == strMerchantID & strCodigoActual == cadena[2])
                                        {
                                            intIdOrden = int.Parse(cadena[4]);
                                            strSerieNroDocumento = cadena[5];
                                            strDescripcionOrden = cadena[6];
                                            intMonedaDocPago = int.Parse(cadena[7]);
                                            decImporteDocPago = decimal.Parse(cadena[8]);

                                            if (string.IsNullOrEmpty(cadena[9].Trim()))
                                            {
                                                strFechaEmision = string.Empty;
                                            }
                                            else
                                            {
                                                DateTime fechavalida;
                                                if (DateTime.TryParse(cadena[9], out fechavalida))
                                                    strFechaEmision = cadena[9].Split(new Char[] { '/' })[2] + cadena[9].Split(new Char[] { '/' })[1] + cadena[9].Split(new Char[] { '/' })[0];
                                                else
                                                {
                                                    strFechaEmision = string.Empty;
                                                }
                                            }

                                            if (string.IsNullOrEmpty(cadena[10].Trim()))
                                            {
                                                strFechaVencimiento = string.Empty;
                                            }
                                            else
                                            {
                                                DateTime fechavalida;
                                                if (DateTime.TryParse(cadena[10], out fechavalida))
                                                    strFechaVencimiento = cadena[10].Split(new Char[] { '/' })[2] + cadena[10].Split(new Char[] { '/' })[1] + cadena[10].Split(new Char[] { '/' })[0];
                                                else
                                                {
                                                    strFechaVencimiento = string.Empty;
                                                }
                                            }
                                            
                                            if (string.IsNullOrEmpty(cadena[11].Trim()))
                                            {
                                                decImporteMora = string.Empty;
                                            }
                                            else
                                            {
                                                decImporteMora = cadena[11];
                                            }

                                            intNumeroCargaDiaria = intCantidadCargaDiaria;
                                            intTiempo = Vigente;
                                            intIdOrdenPago = 0;

                                            CargarDetalle(dtDetalle, strMerchantID, strCodigoActual, intIdOrden, strSerieNroDocumento, strDescripcionOrden, intMonedaDocPago, decImporteDocPago, strFechaEmision, strFechaVencimiento, decImporteMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, strCodigoContribuyenteComp);

                                            intCantDetArchivo += 1;

                                            if (idErrorRegistrarDetalle == 0)
                                            {
                                                intCantDetProc += 1;
                                            }
                                            else
                                            {
                                                strCadenaErrores += "<li>Linea: " + intLineaError.ToString() + "</li>";
                                            }
                                        }
                                        else
                                        {
                                            strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString() + " El Detalle no pertenece a la Cabecera</br>";
                                            ActualizarServMunicipalidadBarranco(oServicioMunicipalidadBarrancoBE, 0, 0, 0, 0, 0, strCadenaErrores);
                                            return oServicioMunicipalidadBarrancoBE;
                                        }
                                    }
                                    else
                                    {
                                        strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString() + " Detalle Incorrecto</br>";
                                        ActualizarServMunicipalidadBarranco(oServicioMunicipalidadBarrancoBE, 0, 0, 0, 0, 0, strCadenaErrores);
                                        return oServicioMunicipalidadBarrancoBE;
                                    }
                                }
                            }
                        }
                        else
                        {
                            idError = 1;
                            strCadenaErrores = "Formato Inválido </br>";
                            ActualizarServMunicipalidadBarranco(oServicioMunicipalidadBarrancoBE, 0, 0, 0, 0, 0, strCadenaErrores);
                            return oServicioMunicipalidadBarrancoBE;
                            //el formato del archivo no es el correcto
                        }
                    } while (str.Peek() != -1);
                    str.Close();
                }

                if (strCadenaErrores.Length == 0)
                {
                    MunicipalidadBarrancoDA oMunicipalidadBarrancoDA = new MunicipalidadBarrancoDA();
                    idErrorRegistrarDetalle = oMunicipalidadBarrancoDA.RegistrarLoteMunicipalidadBarranco(strMerchantID, dtCabecera, dtDetalle, cadenaConexion, ValTimeOut, BatchSize);
                }

                ActualizarServMunicipalidadBarranco(oServicioMunicipalidadBarrancoBE, intCantCabArchivo, intCantDetArchivo, intCantCabProc, intCantDetProc, idError, strCadenaErrores);
                return oServicioMunicipalidadBarrancoBE;
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                idError = 2;
                string strCadenaErrores = "Formato Inválido </br>";
                ActualizarServMunicipalidadBarranco(oServicioMunicipalidadBarrancoBE, 0, 0, 0, 0, 0, strCadenaErrores);
                return oServicioMunicipalidadBarrancoBE;
                throw ex;
            }
        }

        public DataTable IniciarCabecera(DataTable dt)
        {
            dt.Columns.Add("MerchantID");
            dt.Columns.Add("CodigoActual");
            dt.Columns.Add("CodigoContribuyenteComp");
            dt.Columns.Add("NombreContribuyente");
            dt.Columns.Add("ApellidoPaternoContribuyente");
            dt.Columns.Add("ApellidoMaternoContribuyente");
            dt.Columns.Add("EmailContribuyente");
            return dt;
        }

        public void CargarCabecera(DataTable dt, string strMerchantID, string strCodigoActual, string strCodigoContribuyenteComp, string strNombreContribuyente, string strApellidoPaternoContribuyente, string strApellidoMaternoContribuyente, string strEmailContribuyente)
        {
            dt.Rows.Add(strMerchantID, strCodigoActual, strCodigoContribuyenteComp, strNombreContribuyente, strApellidoPaternoContribuyente, strApellidoMaternoContribuyente, strEmailContribuyente);
        }

        public DataTable IniciarDetalle(DataTable dt)
        {
            dt.Columns.Add("MerchantID");
            dt.Columns.Add("CodigoActual");
            dt.Columns.Add("IdOrden");
            dt.Columns.Add("SerieNroDocumento");
            dt.Columns.Add("DescripcionOrden");
            dt.Columns.Add("ConceptoDocPago");
            dt.Columns.Add("MonedaDocPago");
            dt.Columns.Add("ImporteDocPago");
            dt.Columns.Add("FechaEmision");
            dt.Columns.Add("FechaVencimiento");
            dt.Columns.Add("ImporteMora");
            dt.Columns.Add("NumeroCargaDiaria");
            dt.Columns.Add("Tiempo");
            dt.Columns.Add("IdOrdenPago");
            dt.Columns.Add("CodigoContribuyenteComp");
            return dt;
        }

        public void CargarDetalle(DataTable dt, string strMerchantID, string strCodigoActual, int intIdOrden, string strSerieNroDocumento, string strDescripcionOrden, int intMonedaDocPago, decimal decImporteDocPago, string dtFechaEmision, string dtFechaVencimiento,
        
        string decImporteMora, int intNumeroCargaDiaria, int intTiempo, int intIdOrdenPago, string strCodigoContribuyenteComp)
        {
            DataRow row = dt.NewRow();
            row["MerchantID"] = strMerchantID;
            row["CodigoActual"] = strCodigoActual;
            row["CodigoContribuyenteComp"] = strCodigoContribuyenteComp;
            row["IdOrden"] = intIdOrden;
            row["SerieNroDocumento"] = strSerieNroDocumento;
            row["DescripcionOrden"] = strDescripcionOrden;
            row["ConceptoDocPago"] = strDescripcionOrden;
            row["MonedaDocPago"] = intMonedaDocPago;
            row["ImporteDocPago"] = decImporteDocPago;

            if (dtFechaEmision == string.Empty)
                row["FechaEmision"] = DBNull.Value;
            else
                row["FechaEmision"] = Convert.ToDateTime(dtFechaEmision.Substring(6, 2) + "/" + dtFechaEmision.Substring(4, 2) + "/" + dtFechaEmision.Substring(0, 4));

            if (dtFechaVencimiento == string.Empty)
                row["FechaVencimiento"] = DBNull.Value;
            else
                row["FechaVencimiento"] = Convert.ToDateTime(dtFechaVencimiento.Substring(6, 2) + "/" + dtFechaVencimiento.Substring(4, 2) + "/" + dtFechaVencimiento.Substring(0, 4));

            if (string.IsNullOrEmpty(decImporteMora))
                row["ImporteMora"] = DBNull.Value;
            else
                row["ImporteMora"] = Convert.ToDecimal(decImporteMora);

            row["NumeroCargaDiaria"] = intNumeroCargaDiaria;
            row["Tiempo"] = intTiempo;
            row["IdOrdenPago"] = intIdOrdenPago;
            //row["FechaCarga"] = DateTime.Now;
            dt.Rows.Add(row);
            //dt.Rows.Add(strMerchantID, strCodigoActual, intIdOrden, strSerieNroDocumento, strDescripcionOrden, intMonedaDocPago, decImporteDocPago,
            //dtFechaEmision, dtFechaVencimiento, decImporteMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, strCodigoContribuyenteComp)
        }

        public void ActualizarServMunicipalidadBarranco(ServicioMunicipalidadBarrancoBE oBEMunicipalidadBarranco, int intCantCabArchivo, int intCantDetArchivo, int intCantCabProc, int intCantDetProc, int idError, string strCadenaErrores)
        {
            oBEMunicipalidadBarranco.CantCabArchivo = intCantCabArchivo;
            oBEMunicipalidadBarranco.CantDetArchivo = intCantDetArchivo;
            oBEMunicipalidadBarranco.CantCabProc = intCantCabProc;
            oBEMunicipalidadBarranco.CantDetProc = intCantDetProc;
            oBEMunicipalidadBarranco.IdError = idError;
            oBEMunicipalidadBarranco.CadenaErrores = strCadenaErrores;
        }

        public int ActualizarDocumentosCIPMunicipalidadBarranco(ServicioMunicipalidadBarrancoRequestBE request, string UsuarioActMunicipalidadBarranco, string cadenaConexion, string ValTimeOut)
        {
            int idError = 0;
            string Texto = Utiles.BytesToString(request.Bytes);
            Hashtable htTabla = new Hashtable();
            int intCont = 0;
            string[] cadena = null;
            try
            {
                using (StringReader str = new StringReader(Texto))
                {
                    do
                    {
                        string Linea = str.ReadLine();
                        cadena = Regex.Split(Linea, "|");
                        if (Linea.StartsWith("DD"))
                        {
                            try{htTabla.Add(cadena[5], intCont);}
                            catch{}
                        }
                    } while (str.Peek() != -1);
                    str.Close();
                }

                ServicioMunicipalidadBarrancoRequestBE Req = request;
                Req.MerchantID = cadena[1];
                Req.FechaEmision = DateTime.Today;

                ArrayList ArrayCIPS = new ArrayList();
                List<ServicioMunicipalidadBarrancoBE> listServMunicipalidadBarranco = new List<ServicioMunicipalidadBarrancoBE>();
                listServMunicipalidadBarranco = ConsultarMunicipalidadBarrancoCIP(request, cadenaConexion, ValTimeOut);
                if (listServMunicipalidadBarranco.Count > 0)
                {
                    int intConteo = 0;
                    do
                    {
                        if ((listServMunicipalidadBarranco[intConteo].SerieNroDocumento) == null)
                        {
                            if (!ArrayCIPS.Contains(listServMunicipalidadBarranco[intConteo].IdOrdenPago))
                            {
                                ArrayCIPS.Add(listServMunicipalidadBarranco[intConteo].IdOrdenPago);
                            }
                        }
                        intConteo += 1;
                    } while ((intConteo <= listServMunicipalidadBarranco.Count - 1));

                    if (ArrayCIPS.Count > 0)
                    {
                        MunicipalidadBarrancoDA oDAServicioMunicipalidadBarranco = new MunicipalidadBarrancoDA();
                        //DAOrdenPago oDAOrdenPago = new DAOrdenPago();
                        ServicioMunicipalidadBarrancoRequestBE oBEServicioMunicipalidadBarrancoRequest = new ServicioMunicipalidadBarrancoRequestBE();
                        oBEServicioMunicipalidadBarrancoRequest.IdUsuarioActualizacion = Convert.ToInt32(UsuarioActMunicipalidadBarranco);
                        oBEServicioMunicipalidadBarrancoRequest.IdOrigenEliminacion = 0;

                        int ValDoc = 0;
                        foreach (int ValDoc_loopVariable in ArrayCIPS)
                        {
                            ValDoc = ValDoc_loopVariable;
                            oBEServicioMunicipalidadBarrancoRequest.IdOrdenPago = Convert.ToInt32(ValDoc.ToString());
                            //Dim intCIP As Integer = oDAOrdenPago.EliminarCIP(oBEOrdenPago)
                            int intCIP = oDAServicioMunicipalidadBarranco.EliminarCIPMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest, cadenaConexion, ValTimeOut);
                        }
                    }
                }
                return idError;
            }
            catch(Exception ex)
            {
                //Logger.Write(ex);
                return idError;
                throw ex;
            }
        }

        public List<ServicioMunicipalidadBarrancoBE> ConsultarMunicipalidadBarrancoCIP(ServicioMunicipalidadBarrancoRequestBE oServicioMunicipalidadBarrancoRequestBE, string cadenaConexion, string ValTimeOut)
        {
            try
            {
                MunicipalidadBarrancoDA oMunicipalidadBarrancoDA = new MunicipalidadBarrancoDA();
                return oMunicipalidadBarrancoDA.ConsultarMunicipalidadBarrancoCIP(oServicioMunicipalidadBarrancoRequestBE, cadenaConexion, ValTimeOut);
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                throw ex;
            }
        }

        public List<ServicioMunicipalidadBarrancoBE> ConsultarDetalleArchivoDescargaMunicipalidadBarranco(ServicioMunicipalidadBarrancoRequestBE oServicioMunicipalidadBarrancoRequestBE, string cadenaConexion, string ValTimeOut)
        {
            try
            {
                MunicipalidadBarrancoDA oMunicipalidadBarrancoDA = new MunicipalidadBarrancoDA();
                return oMunicipalidadBarrancoDA.ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oServicioMunicipalidadBarrancoRequestBE, cadenaConexion, ValTimeOut);
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                throw ex;
            }
        }


        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
