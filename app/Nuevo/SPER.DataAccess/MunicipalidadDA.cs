﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using SPER.BusinessEntity;

namespace SPER.DataAccess
{
    public class MunicipalidadBarrancoDA :IDisposable
    {
        public int RegistrarLoteMunicipalidadBarranco(string strMerchantID, DataTable dtCabecera, DataTable dtDetalle, string cadenaConexion, string ValTimeOut, int BatchSize)
        {
            int Resultado = 0;
            SqlTransaction Tx = null;
            try
            {
                using (SqlConnection Cn = new SqlConnection(cadenaConexion))
                {
                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    Tx = Cn.BeginTransaction();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandText = "[PagoEfectivo].[prc_RegistrarLote_MunicipalidadBarranco]";
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandTimeout = Convert.ToInt32(ValTimeOut);
                        Cmd.Parameters.Add("@MerchantID", SqlDbType.VarChar).Value = strMerchantID;
                        Cmd.Parameters.Add("@Cabecera", SqlDbType.Structured).Value = dtCabecera;
                        //Cmd.Parameters.Add("@Detalle", SqlDbType.Structured).Value = dtDetalle;
                        Cmd.Transaction = Tx;
                        Resultado = (int)Cmd.ExecuteScalar();
                    }
                    using (SqlBulkCopy Bc = new SqlBulkCopy(Cn, SqlBulkCopyOptions.Default, Tx))
                    {
                        foreach (DataColumn col in dtDetalle.Columns)
                        {
                            Bc.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                        }
                        Bc.DestinationTableName = "PagoEfectivo.ServicioMunicipalidadBarrancoDetalle";
                        Bc.BatchSize = BatchSize;
                        Bc.BulkCopyTimeout = Convert.ToInt32(ValTimeOut);
                        Bc.WriteToServer(dtDetalle);
                    }
                    Tx.Commit();
                    if (Cn.State == ConnectionState.Open) Cn.Close();
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                Tx.Rollback();
                throw ex;
            }
            finally{
                Tx.Dispose();
            }
            return Resultado;
        }

        public int EliminarCIPMunicipalidadBarranco(ServicioMunicipalidadBarrancoRequestBE request, string cadenaConexion, string ValTimeOut)
        {
            int IdError = 0;
            try
            {
                using (SqlConnection Cn = new SqlConnection(cadenaConexion))
                {
                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandText = "[PagoEfectivo].[prc_EliminarCIPMunicipalidadBarranco]";
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandTimeout = Convert.ToInt32(ValTimeOut);
                        Cmd.Parameters.Add("@IdOrdenPago", SqlDbType.BigInt).Value = request.IdOrdenPago;
                        Cmd.Parameters.Add("@IdUsuarioActualizacion", SqlDbType.Int).Value = request.IdUsuarioActualizacion;
                        Cmd.Parameters.Add("@IdOrigenEliminacion", SqlDbType.Int).Value = request.IdOrigenEliminacion;
                    }
                    if (Cn.State == ConnectionState.Open) Cn.Close();
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                throw ex;
            }
            return IdError;
        }

        public List<ServicioMunicipalidadBarrancoBE> ConsultarMunicipalidadBarrancoCIP(ServicioMunicipalidadBarrancoRequestBE request, string cadenaConexion, string ValTimeOut)
        {
            List<ServicioMunicipalidadBarrancoBE> lista = new List<ServicioMunicipalidadBarrancoBE>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(cadenaConexion))
                {
                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandText = "[PagoEfectivo].[prc_ConsultarMunicipalidadBarrancoCIP]";
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandTimeout = Convert.ToInt32(ValTimeOut);
                        Cmd.Parameters.Add("@MerchantID", SqlDbType.VarChar).Value = request.MerchantID;
                        Cmd.Parameters.Add("@FechaCarga", SqlDbType.DateTime).Value = request.FechaEmision;
                        SqlDataReader Dr = Cmd.ExecuteReader(CommandBehavior.Default);
                        if (Dr.HasRows)
                        {
                            while (Dr.Read())
                            {
                                ServicioMunicipalidadBarrancoBE item = new ServicioMunicipalidadBarrancoBE();
                                item.SerieNroDocumento = Convert.ToString(Dr["SerieNroDocumento"]);
                                item.IdOrdenPago = Convert.ToInt64(Dr["IdOrdenPago"]);
                                lista.Add(item);
                            }
                        }
                    }
                    if (Cn.State == ConnectionState.Open) Cn.Close();
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                throw ex;
            }
            return lista;
        }

        public int DeterminarIdParametroMunicipalidadBarranco(ServicioMunicipalidadBarrancoRequestBE request, string cadenaConexion, int ValTimeOut)
        {
            int Resultado = 0;
            try
            {
                using (SqlConnection Cn = new SqlConnection(cadenaConexion))
                {
                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandText = "[PagoEfectivo].[prc_DeterminarIdParametro]";
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandTimeout = Convert.ToInt32(ValTimeOut);
                        Cmd.Parameters.Add("@GCParametro", SqlDbType.VarChar).Value = request.GCParametro;
                        Cmd.Parameters.Add("@DesParametro", SqlDbType.VarChar).Value = request.DesParametro;
                        Resultado = (int)Cmd.ExecuteScalar();
                    }
                    if (Cn.State == ConnectionState.Open) Cn.Close();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return Resultado;
        }

        public List<ServicioMunicipalidadBarrancoBE> ConsultarDetalleArchivoDescargaMunicipalidadBarranco(ServicioMunicipalidadBarrancoRequestBE oServicioMunicipalidadBarrancoRequestBE, string cadenaConexion, string ValTimeOut)
        {
            List<ServicioMunicipalidadBarrancoBE> lista = new List<ServicioMunicipalidadBarrancoBE>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(cadenaConexion))
                {
                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandText = "[PagoEfectivo].[prc_ConsultarDetalleArchivoDescarga_MunicipalidadBarranco]";
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandTimeout = Convert.ToInt32(ValTimeOut);
                        Cmd.Parameters.Add("@p_MerchantId", SqlDbType.VarChar).Value = oServicioMunicipalidadBarrancoRequestBE.MerchantID;
                        Cmd.Parameters.Add("@p_FechaCarga", SqlDbType.DateTime).Value = oServicioMunicipalidadBarrancoRequestBE.FechaCargaInicial;
                        SqlDataReader Dr = Cmd.ExecuteReader(CommandBehavior.Default);
                        if (Dr.HasRows)
                        {
                            while (Dr.Read())
                            {
                                ServicioMunicipalidadBarrancoBE item = new ServicioMunicipalidadBarrancoBE();
                                item.MerchantID = Convert.ToString(Dr["MerchantID"]);
                                item.CodigoActual = Convert.ToString(Dr["CodigoActual"]);
                                item.CodigoContribuyenteComp = Convert.ToString(Dr["CodigoContribuyenteComp"]);
                                item.IdOrden = Convert.ToString(Dr["IdOrden"]);
                                item.SerieNroDocumento = Convert.ToString(Dr["SerieNroDocumento"]);
                                item.DescripcionOrden = Convert.ToString(Dr["DescripcionOrden"]);
                                item.MonedaDocPago = Convert.ToString(Dr["MonedaDocPago"]);
                                item.ImporteDocPago = Convert.ToInt64(Dr["ImporteDocPago"]);
                                item.FechaEmision = Convert.ToDateTime(Dr["FechaEmision"]);
                                item.strFechaVencimiento = Convert.ToString(Dr["FechaVencimiento"]);
                                item.strImporteMora = Convert.ToString(Dr["ImporteMora"]);
                                item.MonedaDocPago  = Convert.ToString(Dr["MonedaDocPago"]);
                                item.Total  = Convert.ToDecimal(Dr["Total"]);
                                item.FechaCancelacion = Convert.ToDateTime(Dr["FechaCancelacion"]);
                                item.NumeroOrdenPago = Convert.ToInt32(Dr["NumeroOrdenPago"]);
                                lista.Add(item);
                            }
                        }
                    }
                    if (Cn.State == ConnectionState.Open) Cn.Close();
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                throw ex;
            }
            return lista;

        }


        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}