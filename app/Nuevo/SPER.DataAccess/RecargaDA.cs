﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SPER.BusinessEntity;
using SPE.Entidades;

namespace SPER.DataAccess
{
    public class RecargaDA : IDisposable
    {
        public List<BEMensaje> MensajesValidacionRecarga(string CadenaConexionPESaldos)
        {
            List<BEMensaje> lista = new List<BEMensaje>();
            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandText = "[dbo].[spl_lstValidarRecarga]";
                        Cmd.CommandType = CommandType.StoredProcedure;
                        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.Default);
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                BEMensaje item = new BEMensaje();
                                item.Codigo = Convert.ToString(reader.GetString(0));
                                item.Descripcion = Convert.ToString(reader.GetString(1));
                                lista.Add(item);
                            }
                        }
                    }
                    if (Cn.State == ConnectionState.Open) Cn.Close();
                }
            }
            catch (Exception ex)
            {
                //Logger.Write(ex);
                throw ex;
            }
            return lista;
        }

        public BERespuesta ValidarRecarga(BERecarga oRecargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;

            try
            {
                Monto = oRecargaBE.mMonto;

                CIP = oRecargaBE.xDocumento + oRecargaBE.xCodigoRecarga + oRecargaBE.mMonto.ToString();

                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[7];

                    Params[0] = new SqlParameter("@NumeroDocumento", SqlDbType.VarChar, 8);
                    Params[0].Value = oRecargaBE.xDocumento;

                    Params[1] = new SqlParameter("@CodigoRecarga", SqlDbType.VarChar, 2);
                    Params[1].Value = oRecargaBE.xCodigoRecarga;

                    Params[2] = new SqlParameter("@Digito11", SqlDbType.VarChar, 1);
                    Params[2].Value = oRecargaBE.xDigito11;

                    Params[3] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[3].Value = oRecargaBE.mMonto;

                    Params[4] = new SqlParameter("@Resultado", SqlDbType.Bit);
                    Params[4].Direction = ParameterDirection.Output;

                    Params[5] = new SqlParameter("@Codigo", SqlDbType.VarChar, 2);
                    Params[5].Direction = ParameterDirection.Output;

                    Params[6] = new SqlParameter("@Mensaje", SqlDbType.VarChar, 500);
                    Params[6].Direction = ParameterDirection.Output;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.spl_outValidarRecarga";
                        Cmd.Parameters.AddRange(Params);
                        Cmd.ExecuteNonQuery();

                        Resultado = (bool)Params[4].Value;
                        Codigo = (string)Params[5].Value;
                        Mensaje = (string)Params[6].Value;
                        Error = true;
                    }
                }
                return new BERespuesta { Resultado = Resultado, Codigo = Codigo, Mensaje = Mensaje, Monto = Monto, CIP = CIP, _Error = Error };
            }
            catch (Exception ex)
            {
                Codigo = "004";
                Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Monto = 0;
                CIP = "";
                return new BERespuesta { Resultado = Resultado, Codigo = Codigo, Mensaje = Mensaje, Monto = Monto, CIP = CIP, _Error = Error };
                throw ex;
            }
        }

        public BERespuestaSaldos ConsultarRecarga(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;
            BERespuestaSaldos Respuesta = new BERespuestaSaldos();

            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[7];

                    Params[0] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[0].Value = _recargaBE.xCodBancoPE;

                    Params[1] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[1].Value = _recargaBE.xCodigoServicioBanco;

                    Params[2] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidad;

                    Params[3] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[3].Value = _recargaBE.xCIPRecarga;

                    Params[4] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xDocumento;

                    Params[5] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[5].Value = _recargaBE.iMoneda;

                    Params[6] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[6].Value = _recargaBE.mMonto;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBCP_Consultar";
                        Cmd.Parameters.AddRange(Params);

                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "003";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                Respuesta._Error = true;
                return Respuesta;
            }
        }

        public BERespuestaSaldos RealizarRecarga(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;

            BERespuestaSaldos Respuesta = new BERespuestaSaldos();

            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[7];

                    Params[0] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[0].Value = _recargaBE.xCodBancoPE;

                    Params[1] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[1].Value = _recargaBE.xCodigoServicioBanco;

                    Params[2] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidad;

                    Params[3] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[3].Value = _recargaBE.xCIPRecarga;

                    Params[4] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xDocumento;

                    Params[5] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[5].Value = _recargaBE.iMoneda;

                    Params[6] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[6].Value = _recargaBE.mMonto;

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBCP_Recargar";
                        Cmd.Parameters.AddRange(Params);
                        //Cmd.ExecuteNonQuery();


                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "004";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                return Respuesta;
                throw ex;
            }
        }

        public BERespuestaSaldos ExtornarRecarga(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;


            BERespuestaSaldos Respuesta = new BERespuestaSaldos();

            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[8];

                    Params[0] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[0].Value = _recargaBE.xCodBancoPE;

                    Params[1] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[1].Value = _recargaBE.xCodigoServicioBanco;

                    Params[2] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidad;
                    //Del numero a extornar
                    Params[3] = new SqlParameter("@NroOperacionBancariaRecarga", SqlDbType.VarChar, 20);
                    Params[3].Value = _recargaBE.xNumOperacionEntidadRecarga;

                    Params[4] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xCIPRecarga;

                    Params[5] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[5].Value = _recargaBE.xDocumento;

                    Params[6] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[6].Value = _recargaBE.iMoneda;

                    Params[7] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[7].Value = _recargaBE.mMonto;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBCP_Extornar";
                        Cmd.Parameters.AddRange(Params);
                        //Cmd.ExecuteNonQuery();

                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "005";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                return Respuesta;
            }
        }

        public BERespuestaSaldosBanbif ConsultarRecargaBanBif(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;
            BERespuestaSaldosBanbif Respuesta = new BERespuestaSaldosBanbif();
           
            Respuesta.PagoConcepto = "Consulta Recarga BanBif";
            Respuesta.EmpresaDescripcion = "Pago Efectivo";
            Respuesta.FechaVencimiento = DateTime.Now;
            Respuesta.FechaEmision = DateTime.Now;
            Respuesta.CIP= "6060606000200";
            Respuesta.Monto = Convert.ToDecimal("22.00");
            Respuesta.IdEstado = 01;
            Respuesta.CipListoParaExpirar = "0";
            Respuesta.CodMonedaBanco = "02";
            Respuesta.DescripcionCliente = "Brady Lopez";
            Respuesta.CodigoServicio = "001"; 
            Respuesta.DescripcionServicio = "Recarga";
            Respuesta.IdCliente = "60606060";
            Respuesta.UsuarioNombre = "Brady";

            Respuesta.Codigo = "001";
            Respuesta.Mensaje = "Consulta Exitosa BanBif"; 
            return Respuesta;
            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[7];

                    Params[0] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[0].Value = _recargaBE.xCodBancoPE;

                    Params[1] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[1].Value = _recargaBE.xCodigoServicioBanco;

                    Params[2] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidad;

                    Params[3] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[3].Value = _recargaBE.xCIPRecarga;

                    Params[4] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xDocumento;

                    Params[5] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[5].Value = _recargaBE.iMoneda;

                    Params[6] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[6].Value = _recargaBE.mMonto;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBANBIF_Consultar";
                        Cmd.Parameters.AddRange(Params);

                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                //Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaEmision = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                //Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                //Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                //Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                //Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "003";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                Respuesta._Error = true;
                return Respuesta;
            }
        }

        public BERespuestaSaldosBanbif RealizarRecargaBanBif(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;

            BERespuestaSaldosBanbif Respuesta = new BERespuestaSaldosBanbif();

            Respuesta.PagoConcepto = "Consulta Recarga BanBif";
            Respuesta.EmpresaDescripcion = "Pago Efectivo";
            Respuesta.FechaVencimiento = DateTime.Now;
            Respuesta.FechaEmision = DateTime.Now;
            Respuesta.CIP = "6060606000200";
            Respuesta.Monto = Convert.ToDecimal("22.00");
            Respuesta.IdEstado = 01;
            Respuesta.CipListoParaExpirar = "0";
            Respuesta.CodMonedaBanco = "02";
            Respuesta.DescripcionCliente = "Brady Lopez";
            Respuesta.CodigoServicio = "001";
            Respuesta.DescripcionServicio = "Recarga";
            Respuesta.IdCliente = "60606060";
            Respuesta.UsuarioNombre = "Brady";

            Respuesta.Codigo = "001";
            Respuesta.Mensaje = "Recarga Exitosa BanBif";
            return Respuesta;
            
            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[7];

                    Params[0] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[0].Value = _recargaBE.xCodBancoPE;

                    Params[1] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[1].Value = _recargaBE.xCodigoServicioBanco;

                    Params[2] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidad;

                    Params[3] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[3].Value = _recargaBE.xCIPRecarga;

                    Params[4] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xDocumento;

                    Params[5] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[5].Value = _recargaBE.iMoneda;

                    Params[6] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[6].Value = _recargaBE.mMonto;

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBANBIF_Recargar";
                        Cmd.Parameters.AddRange(Params);
                        //Cmd.ExecuteNonQuery();


                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                //Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                //Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                //Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                //Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                //Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "004";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                return Respuesta;
                throw ex;
            }
        }

        public BERespuestaSaldosBanbif ExtornarRecargaBanBif(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;


            BERespuestaSaldosBanbif Respuesta = new BERespuestaSaldosBanbif();

            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[8];

                    Params[0] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[0].Value = _recargaBE.xCodBancoPE;

                    Params[1] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[1].Value = _recargaBE.xCodigoServicioBanco;

                    Params[2] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidad;
                    //Del numero a extornar
                    Params[3] = new SqlParameter("@NroOperacionBancariaRecarga", SqlDbType.VarChar, 20);
                    Params[3].Value = _recargaBE.xNumOperacionEntidadRecarga;

                    Params[4] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xCIPRecarga;

                    Params[5] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[5].Value = _recargaBE.xDocumento;

                    Params[6] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[6].Value = _recargaBE.iMoneda;

                    Params[7] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[7].Value = _recargaBE.mMonto;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBANBIF_Extornar";
                        Cmd.Parameters.AddRange(Params);
                        //Cmd.ExecuteNonQuery();

                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                //Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                //Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                //Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                //Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                //Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "005";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                return Respuesta;
            }
        }
        
        public BERespuestaSaldosBBVA ConsultarRecargaBBVA(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;
            BERespuestaSaldosBBVA Respuesta = new BERespuestaSaldosBBVA();
            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[7];
                    Params[0] = new SqlParameter("@xCodigoOperacion", SqlDbType.VarChar, 4);
                    Params[0].Value = _recargaBE.xCodigoOperacion;

                    Params[1] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[1].Value = _recargaBE.xNumOperacionEntidad;

                    Params[2] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[2].Value = _recargaBE.xCodBancoPE;
                    //CodigoConvenio
                    Params[3] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[3].Value = _recargaBE.xCodigoServicioBanco;
                    //CodigoCliente
                    Params[4] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xDocumento;
                    //NumeroReferenciaDeuda
                    Params[5] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[5].Value = _recargaBE.xCIPRecarga;
                    //FechadeOperacion
                    Params[6] = new SqlParameter("@FechaOperacion", SqlDbType.VarChar, 2);
                    Params[6].Value = _recargaBE.dFechaOperacion;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBANBIF_Consultar";
                        Cmd.Parameters.AddRange(Params);

                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Respuesta.CodCliente = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                Respuesta.CIP = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                Respuesta.CodMonedaBanco = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.Monto = reader.IsDBNull(4) ? 0 : reader.GetDecimal(4);
                                Respuesta.Codigo = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                Respuesta.Mensaje = reader.IsDBNull(6) ? "" : reader.GetString(6);
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "003";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                Respuesta._Error = true;
                return Respuesta;
            }
        }

        public BERespuestaSaldosBBVA RealizarRecargaBBVA(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;

            BERespuestaSaldosBBVA Respuesta = new BERespuestaSaldosBBVA();

            //Respuesta.PagoConcepto = "Recarga Recarga BanBif";
            ////Respuesta.DescripcionCliente = "Pago Efectivo";
            //Respuesta.FechaVencimiento = DateTime.Now;
            //Respuesta.FechaEmision = DateTime.Now;
            //Respuesta.CIP = "6060606000200";
            //Respuesta.Monto = Convert.ToDecimal("22.00");

            ////Respuesta.CipListoParaExpirar = "0";
            //Respuesta.CodMonedaBanco = "02";
            //Respuesta.DescripcionCliente = "Brady Lopez";

            //Respuesta.Codigo = "001";
            //Respuesta.Mensaje = "Recarga Exitosa BanBif";
            //return Respuesta;

            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[8];
                    Params[0] = new SqlParameter("@xCodigoOperacion", SqlDbType.VarChar,4);
                    Params[0].Value = _recargaBE.xCodigoOperacion;

                    Params[1] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[1].Value = _recargaBE.xNumOperacionEntidad;

                    Params[2] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[2].Value = _recargaBE.xCodBancoPE;
                    //CodigoConvenio
                    Params[3] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[3].Value = _recargaBE.xCodigoServicioBanco;
                    //CodigoCliente
                    Params[4] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[4].Value = _recargaBE.xDocumento;
                    //NumeroReferenciaDeuda
                    Params[5] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[5].Value = _recargaBE.xCIPRecarga;
                    //Cod Moneda
                    Params[6] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[6].Value = _recargaBE.iMoneda;
                    //ImporteTotalPagado
                    Params[7] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[7].Value = _recargaBE.mMonto;

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBANBIF_Recargar";
                        Cmd.Parameters.AddRange(Params);
                        //Cmd.ExecuteNonQuery();


                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                //Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                //Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaEmision = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                //Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                //Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                //Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                //Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "004";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                return Respuesta;
                throw ex;
            }
        }

        public BERespuestaSaldosBBVA ExtornarRecargaBBVA(BERecarga _recargaBE, string CadenaConexionPESaldos)
        {
            bool Error = false;
            bool Resultado = false;
            string Codigo = string.Empty;
            string Mensaje = string.Empty;
            Decimal Monto = 0;
            string CIP = string.Empty;


            BERespuestaSaldosBBVA Respuesta = new BERespuestaSaldosBBVA();

            try
            {
                using (SqlConnection Cn = new SqlConnection(CadenaConexionPESaldos))
                {
                    SqlParameter[] Params = new SqlParameter[8];
                    Params[0] = new SqlParameter("@xCodigoOperacion", SqlDbType.VarChar, 4);
                    Params[0].Value = _recargaBE.xCodigoOperacion;
                    //No operacion de extorno
                    Params[1] = new SqlParameter("@NroOperacionBancaria", SqlDbType.VarChar, 20);
                    Params[1].Value = _recargaBE.xNumOperacionEntidad;
                    //Del numero a extornar
                    Params[2] = new SqlParameter("@NroOperacionBancariaRecarga", SqlDbType.VarChar, 20);
                    Params[2].Value = _recargaBE.xNumOperacionEntidadRecarga;
                    //Codigo de Banco
                    Params[3] = new SqlParameter("@Banco", SqlDbType.VarChar, 2);
                    Params[3].Value = _recargaBE.xCodBancoPE;
                    //CodigoConvenio
                    Params[4] = new SqlParameter("@BancoCodigoServicio", SqlDbType.VarChar, 10);
                    Params[4].Value = _recargaBE.xCodigoServicioBanco;
                    //CodigoCliente
                    Params[5] = new SqlParameter("@NroDocumentoIdentidad", SqlDbType.VarChar, 20);
                    Params[5].Value = _recargaBE.xDocumento;
                    //NumeroReferenciaDeuda
                    Params[6] = new SqlParameter("@CIPRecarga", SqlDbType.VarChar, 20);
                    Params[6].Value = _recargaBE.xCIPRecarga;
                    //Cod Moneda
                    Params[7] = new SqlParameter("@Moneda", SqlDbType.VarChar, 10);
                    Params[7].Value = _recargaBE.iMoneda;
                    //ImporteTotalPagado
                    Params[8] = new SqlParameter("@Monto", SqlDbType.Decimal);
                    Params[8].Value = _recargaBE.mMonto;

                    //SqlHelper.ExecuteScalar(CadenaConexionPESaldos, CommandType.StoredProcedure, "spl_outValidarRecarga", parametros);

                    if (Cn.State == ConnectionState.Closed) Cn.Open();
                    using (SqlCommand Cmd = new SqlCommand())
                    {
                        Cmd.Connection = Cn;
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.CommandText = "dbo.sp_MovimientoRecargaBANBIF_Extornar";
                        Cmd.Parameters.AddRange(Params);
                        //Cmd.ExecuteNonQuery();

                        SqlDataReader reader = Cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Respuesta.ClienteNombre = reader.IsDBNull(0) ? "" : reader.GetString(0);
                                //Respuesta.ClienteEstado = reader.IsDBNull(1) ? "" : reader.GetString(1);
                                //Respuesta.EmpresaDescripcion = reader.IsDBNull(2) ? "" : reader.GetString(2);
                                Respuesta.PagoConcepto = reader.IsDBNull(3) ? "" : reader.GetString(3);
                                Respuesta.FechaEmision = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.FechaVencimiento = reader.IsDBNull(4) ? DateTime.Now : reader.GetDateTime(4);
                                Respuesta.CIP = reader.IsDBNull(5) ? "" : reader.GetString(5);
                                //Respuesta.Moneda = reader.IsDBNull(6) ? "" : reader.GetString(6);
                                Respuesta.Monto = reader.IsDBNull(7) ? 0 : reader.GetDecimal(7);
                                //Respuesta.CodigoServicioBanco = reader.IsDBNull(8) ? "" : reader.GetString(8);
                                //Respuesta.NumeroOperacion = reader.IsDBNull(9) ? "" : reader.GetString(9);
                                //Respuesta.FechaDeOperacion = reader.IsDBNull(10) ? DateTime.Now : reader.GetDateTime(10);
                                Respuesta.Codigo = reader.IsDBNull(11) ? "" : reader.GetString(11);
                                //Resultado = (bool)Params[4].Value,
                                //Codigo = (string)Params[5].Value,
                                Respuesta.Mensaje = reader.IsDBNull(12) ? "" : reader.GetString(12);
                                //_Error = false
                            }
                        }
                    }
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                Respuesta.Codigo = "005";
                Respuesta.Mensaje = "Sistema No Disponible";
                //Mensaje = ex.Message;
                Respuesta.Monto = 0;
                Respuesta.CIP = "";
                return Respuesta;
            }
        }

        #region IDisposable Members

        private bool disposedValue = false;
        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //' TODO: free managed resources when explicitly called
                    DoDispose();
                }
            }
            //' TODO: free shared unmanaged resources
            this.disposedValue = true;
        }

        public virtual void DoDispose()
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
