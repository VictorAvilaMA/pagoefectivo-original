﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace SPER.Utility
{
    public class Utiles
    {
        public static string BytesToString(byte[] byt)
        {
            return Encoding.Default.GetString(byt);
        }

        public static string ValidateEmail(string email, string EmailUsuarioInst)
        {
            if (!string.IsNullOrEmpty(email.Trim()))
            {
                Regex emailRegex = new Regex("^(?<user>[^@]+)@(?<host>.+)$");
                Match emailMatch = emailRegex.Match(email);
                if (emailMatch.Success)
                {
                    return email;
                }
            }
            return EmailUsuarioInst;
        }

        public static String ForLeft(String cadena, Int32 lenght)
        {
            cadena = cadena.Trim();
            String subcadena = cadena.Substring(0, lenght);
            return subcadena;
        }

        public static String ForMid(String cadena, Int32 starindex, Int32 lenght)
        {
            cadena = cadena.Trim();
            String subcadena = cadena.Substring(starindex, lenght);
            return subcadena;
        }

        public static bool IsValid(String str)
        {
            decimal salida = 0;
            bool IsNumber = decimal.TryParse(str, out salida);
            return IsNumber;
        }

    }
}
