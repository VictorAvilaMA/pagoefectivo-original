﻿using System.ServiceModel.Activation;
using System.ServiceModel;
using System;
using System.Linq;
using System.ServiceModel.Description;
using SPE.EmsambladoComun;

public class CustomServiceHostFactory : ServiceHostFactory
{
 	protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
	{
		ServiceHost host = new ServiceHost(serviceType, baseAddresses );

		foreach (var operation in host.Description.Endpoints.First().Contract.Operations)
			operation.Behaviors.Find<DataContractSerializerOperationBehavior>().DataContractResolver = new EntityBaseContractResolver();

         
        return host;
	}



}



