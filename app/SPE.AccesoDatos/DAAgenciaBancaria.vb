Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports _3Dev.FW.Entidades
Imports System.Data.Common
Imports System.Configuration
Imports SPE.Logging

Public Class DAAgenciaBancaria
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    Private ReadOnly _dlogger As ILogger = New Logger()

    'REGISTRAR AGENCIA BANCARIA
    Public Function RegistrarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
        If (New DAComun().VerificarCodigoAgenciaBancaria(obeAgenciaBancaria.Codigo)) > 0 Then 'SE ENCONTRO EL CODIGO
            Return SPE.EmsambladoComun.ParametrosSistema.ExisteAgenciaBancaria.Existe
        End If

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDataBase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarAgenciaBancaria]", obeAgenciaBancaria.IdAgenciaBancaria, _
            obeAgenciaBancaria.IdBanco, _
            obeAgenciaBancaria.Codigo, _
            obeAgenciaBancaria.Descripcion, _
            obeAgenciaBancaria.Direccion, _
            obeAgenciaBancaria.IdEstado, _
            obeAgenciaBancaria.IdUsuarioCreacion, _
            obeAgenciaBancaria.IdUsuarioActualizacion, _
            obeAgenciaBancaria.IdCiudad))

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    'ACTUALIZAR AGENCIA BANCARIA
    Public Function ActualizarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
        Dim IdAgenciaEncontrada As String = ConsultarAgenciaBancariaxCodigo(obeAgenciaBancaria.Codigo).IdAgenciaBancaria

        If (IdAgenciaEncontrada <> obeAgenciaBancaria.IdAgenciaBancaria) And (IdAgenciaEncontrada <> 0) Then 'SE ENCONTRO EL CODIGO
            Return SPE.EmsambladoComun.ParametrosSistema.ExisteAgenciaBancaria.Existe
        End If

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarAgenciaBancaria", _
            obeAgenciaBancaria.IdAgenciaBancaria, _
            obeAgenciaBancaria.IdBanco, _
            obeAgenciaBancaria.Codigo, _
            obeAgenciaBancaria.Descripcion, _
            obeAgenciaBancaria.Direccion, _
            obeAgenciaBancaria.IdEstado, _
            obeAgenciaBancaria.IdUsuarioActualizacion, _
            obeAgenciaBancaria.IdCiudad))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'CONSULTAR AGENCIA BANCARIA
    Public Function ConsultarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As List(Of BusinessEntityBase)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listAgenciaBancaria As New List(Of BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenciaBancaria", _
            obeAgenciaBancaria.Descripcion, obeAgenciaBancaria.Codigo, obeAgenciaBancaria.IdEstado, obeAgenciaBancaria.PageNumber, obeAgenciaBancaria.PageSize, _
            obeAgenciaBancaria.PropOrder, obeAgenciaBancaria.TipoOrder)

                While reader.Read
                    obeAgenciaBancaria = New BEAgenciaBancaria(reader, "busqueda")
                    obeAgenciaBancaria.TotalPageNumbers = reader("TOTAL").ToString()
                    listAgenciaBancaria.Add(obeAgenciaBancaria)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listAgenciaBancaria
    End Function

    'CONSULTAR AGENCIA BANCARIA POR CODIGO
    Public Function ConsultarAgenciaBancariaxCodigo(ByVal codAgenciaBancaria As String) As BEAgenciaBancaria
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgenciaBancaria As New BEAgenciaBancaria

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenciaBancariaPorCodigo", codAgenciaBancaria)

                While reader.Read
                    obeAgenciaBancaria = New BEAgenciaBancaria(reader, "codigo")
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeAgenciaBancaria
    End Function

    'CONSULTAR AGENCIA BANCARIA POR ID
    Function ConsultarAgenciaBancariaxId(ByVal idAgenciaBancaria As Integer) As BEAgenciaBancaria
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oBEAgenciaBancaria As New BEAgenciaBancaria

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenciaBancariaxId", idAgenciaBancaria)

                While reader.Read
                    oBEAgenciaBancaria = New BEAgenciaBancaria(reader, "busqueda")
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return oBEAgenciaBancaria
    End Function

    Public Function RegistrarCancelacionOrdenPagoAgenciaBancaria(ByVal obeMovimiento As BEMovimiento) As BEOperacionBancaria
        Return RegistrarCancelacionOrdenPagoAgenciaBancariaComun(obeMovimiento, Nothing)
    End Function

    'REGISTRAR CANCELACION DE Código de Identificación de Pago AGENCIA BANCARIA
    Public Function RegistrarCancelacionOrdenPagoAgenciaBancariaComun(ByVal obeMovimiento As BEMovimiento, ByVal FechaCancelacion As Nullable(Of DateTime)) As BEOperacionBancaria

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Dim objOperacionBancaria As BEOperacionBancaria = Nothing

        Try

            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarCancelacionOrdenPagoAgenciaBancaria]", _
                obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion, obeMovimiento.CodigoAgenciaBancaria, _
                obeMovimiento.CodMonedaBanco, obeMovimiento.Monto, obeMovimiento.CodigoServicio, obeMovimiento.CodigoBanco, FechaCancelacion, obeMovimiento.CodigoCanal)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                'result = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarCancelacionOrdenPagoAgenciaBancaria]", obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion, obeMovimiento.CodigoAgenciaBancaria, obeMovimiento.IdMoneda, obeMovimiento.Monto)
                While reader.Read
                    objOperacionBancaria = New BEOperacionBancaria(reader, "CancelacionOP")
                End While
            End Using
        Catch ex As Exception
            Dim datos = String.Format("NumeroOrdenPago:{0},NumeroOperacion:{1},CodigoAgenciaBancaria:{2},CodMonedaBanco:{3},Monto:{4},CodigoServicio:{5},CodigoBanco:{6},FechaCancelacion:{7},CodigoCanal:{8}",
                                      obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion, obeMovimiento.CodigoAgenciaBancaria,
                                      obeMovimiento.CodMonedaBanco, obeMovimiento.Monto, obeMovimiento.CodigoServicio,
                                      obeMovimiento.CodigoBanco, FechaCancelacion, obeMovimiento.CodigoCanal)
            _dlogger.Error(ex, ex.Message + "::" + datos)
            Throw (ex)
        End Try
        Return objOperacionBancaria
    End Function

    'CONSULTAR Código de Identificación de Pago PARA AGENCIAS BANCARIAS
    Public Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Dim objOrdenPago As BEOrdenPago = Nothing
        objOrdenPago = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarOrdenPagoAgenciaBancaria]", _
                obeOrdenPago.NumeroOrdenPago, obeOrdenPago.MonedaAgencia, obeOrdenPago.CodigoServicio, obeOrdenPago.CodigoBanco)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)

                While reader.Read
                    objOrdenPago = New BEOrdenPago(reader, "ConsultarOrdenPagoAgenciaBancaria")
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return objOrdenPago
    End Function


    ''' <summary>
    ''' EXTORNAR OPERACION DE PAGO (Registra el movimiento y actualiza el estado de el Código de Identificación de Pago)
    ''' </summary>
    ''' <param name="obeMovimiento"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExtornarOperacionPago(ByVal obeMovimiento As BEMovimiento) As BEMovimiento
        '
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim beMovimiento As BEMovimiento = Nothing
        Try
            _dlogger.Info(String.Format("ExtornarOperacionPago - NumeroOrdenPago:{0},NumeroOperacion:{1},IdTipoMovimiento:{2}", obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion, obeMovimiento.IdTipoMovimiento))
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ExtornarOperacionPagoAB]", obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion, obeMovimiento.IdTipoMovimiento)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read
                    beMovimiento = New BEMovimiento(reader)
                End While

            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try

        '
        Return beMovimiento
        '
    End Function

    Public Function ExtornarOperacionAnulacion(ByVal oBEMovimiento As BEMovimiento) As BEMovimiento
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim result As BEMovimiento = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ExtornarOperacionAnulacionAB]", _
                oBEMovimiento.NumeroOrdenPago, oBEMovimiento.NumeroOperacion)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                If reader.Read Then
                    result = New BEMovimiento(reader)
                End If
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return result
    End Function

    'ANULAR OPERACION DE PAGO (Registra el movimiento y actualiza el estado de el Código de Identificación de Pago)
    Public Function AnularOperacionPago(ByVal obeMovimiento As BEMovimiento) As BEMovimiento
        '
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim beMovimiento As BEMovimiento = Nothing
        Try
            'Resultado = CInt(objDataBase.ExecuteNonQuery("[PagoEfectivo].[prc_AnularOperacionPagoAB]", _
            'obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion))
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_AnularOperacionPagoAB]", _
                obeMovimiento.NumeroOrdenPago, obeMovimiento.NumeroOperacion, obeMovimiento.IdTipoOrigenCancelacion)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read
                    beMovimiento = New BEMovimiento(reader)
                End While

            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return beMovimiento
    End Function

    Public Function RegistrarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objConciliacion As BEConciliacion = Nothing
        Dim IdConciliacion As Integer
        Try 'wjra 18/11/2010
            IdConciliacion = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarConciliacionBancaria]", obeConciliacion.NombreArchivo, obeConciliacion.IdUsuarioCreacion)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return IdConciliacion
    End Function

    'Registrar Operacion Bancaria
    Public Function RegistrarOperacionBancaria(ByVal obeOperacionBancaria As BEOperacionBancaria, ByVal idUsuarioCreacion As Integer, ByVal idOrigenRegistro As Integer) As BEOperacionBancaria
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objOperacionBancaria As BEOperacionBancaria = Nothing
        Try
            'Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RegistrarOperacionBancaria]", obeOperacionBancaria.IdConciliacion, obeOperacionBancaria.OperacionBancaria, idUsuarioCreacion, idOrigenRegistro)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RegistrarDetalleConciliacion]", obeOperacionBancaria.IdConciliacion, obeOperacionBancaria.OperacionBancaria, idUsuarioCreacion, idOrigenRegistro)
                While reader.Read
                    objOperacionBancaria = New BEOperacionBancaria(reader, "RegistroOperacionBancaria")
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return objOperacionBancaria
    End Function

    Public Function ConsultarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objListaOperacionBancaria As New List(Of BEOperacionBancaria)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarConciliacionBancaria]", obeConciliacion.IdConciliacion, obeConciliacion.OperacionBancaria, obeConciliacion.NumeroOrdenPago, obeConciliacion.IdEStado, obeConciliacion.FechaInicio, obeConciliacion.FechaFin)

                While reader.Read
                    objListaOperacionBancaria.Add(New BEOperacionBancaria(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objListaOperacionBancaria
    End Function

    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOperaciones As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        'Dim obeOrdenPago As SPE.Entidades.BEOrdenPago = CType(be, SPE.Entidades.BEOrdenPago)
        Dim obeConciliacion As SPE.Entidades.BEConciliacion = CType(be, SPE.Entidades.BEConciliacion)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarConciliacionBancaria]", obeConciliacion.IdConciliacion, obeConciliacion.OperacionBancaria, obeConciliacion.NumeroOrdenPago, obeConciliacion.IdEStado, obeConciliacion.FechaInicio, obeConciliacion.FechaFin)
                If Not reader Is Nothing Then
                    While reader.Read()
                        ListOperaciones.Add(New SPE.Entidades.BEOperacionBancaria(reader))
                    End While
                    Return ListOperaciones
                Else
                    Return Nothing
                End If
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarOperacionesNoConciliadas(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objListaOperacionBancaria As New List(Of BEOperacionBancaria)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarOperacionesNoConciliadas]")

                While reader.Read
                    objListaOperacionBancaria.Add(New BEOperacionBancaria(reader, "NoConciliadas"))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return objListaOperacionBancaria
    End Function

#Region "Métodos Bancos"
    Public Function ConsultarCIPDesdeBanco(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objOrdenPago As BEOrdenPago = Nothing
        objOrdenPago = Nothing
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarCIPDesdeBanco]", _
                obeOrdenPago.NumeroOrdenPago, obeOrdenPago.CodigoBanco, obeOrdenPago.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    objOrdenPago = New BEOrdenPago(reader, "ConsultarCIPDesdeBanco")
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return objOrdenPago
    End Function

    Public Function PagarCIPDesdeBancoComun(ByVal oBEMovimiento As BEMovimiento, ByVal FechaCancelacion As Nullable(Of DateTime)) As BEOperacionBancaria
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objOperacionBancaria As BEOperacionBancaria = Nothing
        Try
            Dim datos = String.Format("PagarCIPDesdeBancoComun - NumeroOrdenPago:{0},NumeroOperacion:{1},CodigoBanco:{2},IdAperturaOrigen:{3},CodMonedaBanco:{4},Monto:{5},CodigoMedioPago:{6},IdTipoOrigenCancelacion:{7},FechaCancelacion:{8},CodigoCanal:{9}", oBEMovimiento.NumeroOrdenPago,
                                      oBEMovimiento.NumeroOperacion, oBEMovimiento.CodigoBanco, oBEMovimiento.IdAperturaOrigen, oBEMovimiento.CodMonedaBanco, oBEMovimiento.Monto,
                                      oBEMovimiento.CodigoMedioPago, oBEMovimiento.IdTipoOrigenCancelacion, FechaCancelacion, oBEMovimiento.CodigoCanal)
            _dlogger.Info(datos)

            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_PagarCIPDesdeBanco]", _
                oBEMovimiento.NumeroOrdenPago, oBEMovimiento.NumeroOperacion, oBEMovimiento.CodigoBanco, _
                oBEMovimiento.IdAperturaOrigen, oBEMovimiento.CodMonedaBanco, oBEMovimiento.Monto, _
                oBEMovimiento.CodigoMedioPago, oBEMovimiento.IdTipoOrigenCancelacion, FechaCancelacion, oBEMovimiento.CodigoCanal)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                If reader.Read Then
                    objOperacionBancaria = New BEOperacionBancaria(reader, "PagarCIPDesdeBanco")
                End If
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return objOperacionBancaria
    End Function

    Public Function PagarCIPDesdeBancoComun_Conciliacion_Diaria(ByVal oBEMovimiento As BEMovimiento, ByVal FechaCancelacion As Nullable(Of DateTime)) As BEOperacionBancaria
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objOperacionBancaria As BEOperacionBancaria = Nothing
        Try
            Dim datos = String.Format("PagarCIPDesdeBancoComun - NumeroOrdenPago:{0},NumeroOperacion:{1},CodigoBanco:{2},IdAperturaOrigen:{3},CodMonedaBanco:{4},Monto:{5},CodigoMedioPago:{6},IdTipoOrigenCancelacion:{7},FechaCancelacion:{8},CodigoCanal:{9}", oBEMovimiento.NumeroOrdenPago,
                                      oBEMovimiento.NumeroOperacion, oBEMovimiento.CodigoBanco, oBEMovimiento.IdAperturaOrigen, oBEMovimiento.CodMonedaBanco, oBEMovimiento.Monto,
                                      oBEMovimiento.CodigoMedioPago, oBEMovimiento.IdTipoOrigenCancelacion, FechaCancelacion, oBEMovimiento.CodigoCanal)
            _dlogger.Info(datos)

            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_PagarCIPDesdeBanco_Conciliacion_Diaria]", _
                oBEMovimiento.NumeroOrdenPago, oBEMovimiento.NumeroOperacion, oBEMovimiento.CodigoBanco, _
                oBEMovimiento.IdAperturaOrigen, oBEMovimiento.CodMonedaBanco, oBEMovimiento.Monto, _
                oBEMovimiento.CodigoMedioPago, oBEMovimiento.IdTipoOrigenCancelacion, FechaCancelacion, oBEMovimiento.CodigoCanal)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                If reader.Read Then
                    objOperacionBancaria = New BEOperacionBancaria(reader, "PagarCIPDesdeBanco")
                End If
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return objOperacionBancaria
    End Function

    Public Function PagarCIPDesdeBanco(ByVal oBEMovimiento As BEMovimiento) As BEOperacionBancaria
        Return PagarCIPDesdeBancoComun(oBEMovimiento, Nothing)
    End Function

    Public Function RegistrarAgenciaBancariaDesdePago(ByVal CodigoBanco As String, ByVal CodigoAgenciaBancaria As String) As Hashtable
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Hashtable = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarAgenciaBancariaDesdePago]", _
                CodigoBanco, CodigoAgenciaBancaria)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                If reader.Read Then
                    Resultado = New Hashtable
                    Resultado("IdAperturaOrigen") = Convert.ToInt32(reader("IdAperturaOrigen"))
                    Resultado("RegistroAgenciaBancaria") = Convert.ToBoolean(reader("RegistroAgenciaBancaria"))
                End If
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, String.Format("RegistrarAgenciaBancariaDesdePago - CodigoBanco:{0}, CodigoAgenciaBancaria:{1}, Mensaje:{2}", CodigoBanco, CodigoAgenciaBancaria, ex.Message))
            Throw ex
        End Try
        Return Resultado
    End Function

#End Region


End Class
