Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil

Public Class DAAgenciaRecaudadora
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase



    'REGISTRAR AGENCIA RECAUDADORA
    Public Function RegistrarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarAgenciaRecaudadora", _
            obeAgenciaRecaudadora.IdTipoAgenciaRecaudadora, _
            obeAgenciaRecaudadora.NombreComercial, _
            obeAgenciaRecaudadora.Ruc, _
            obeAgenciaRecaudadora.Contacto, _
            obeAgenciaRecaudadora.Telefono, _
            obeAgenciaRecaudadora.Fax, _
            obeAgenciaRecaudadora.Direccion, _
            obeAgenciaRecaudadora.Email, _
            obeAgenciaRecaudadora.IdCiudad, _
            obeAgenciaRecaudadora.IdUsuarioCreacion))
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        ' 
    End Function

    'ACTUALIZAR AGENCIA RECAUDADORA
    Public Function ActualizarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarAgenciaRecaudadora ", _
            obeAgenciaRecaudadora.IdAgenciaRecaudadora, _
            obeAgenciaRecaudadora.IdTipoAgenciaRecaudadora, _
            obeAgenciaRecaudadora.NombreComercial, _
            obeAgenciaRecaudadora.Ruc, _
            obeAgenciaRecaudadora.Contacto, _
            obeAgenciaRecaudadora.Telefono, _
            obeAgenciaRecaudadora.Fax, _
            obeAgenciaRecaudadora.Direccion, _
            obeAgenciaRecaudadora.Email, _
            obeAgenciaRecaudadora.IdCiudad, _
            obeAgenciaRecaudadora.IdEstado, _
            obeAgenciaRecaudadora.IdUsuarioActualizacion))
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    'CONSULTAR AGENCIA RECAUDADORA POR IDAGENCIA
    Public Function ConsultarAgenciaRecaudadoraPorIdAgencia(ByVal IdAgencia As Integer) As BEAgenciaRecaudadora
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenciaRecaudadoraPorIdAgencia", _
                    IdAgencia)

                While reader.Read
                    '
                    obeAgenciaRecaudadora = New BEAgenciaRecaudadora(reader)
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return obeAgenciaRecaudadora
        '
    End Function

    'CONSULTAR AGENCIA RECAUDADORA POR IDUSUARIO
    Public Function ConsultarAgenciaRecaudadoraPorIdUsuario(ByVal IdUsuario As Integer) As BEAgenciaRecaudadora
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenciaRecaudadoraPorIdUsuario", _
                    IdUsuario)

                While reader.Read
                    '
                    obeAgenciaRecaudadora = New BEAgenciaRecaudadora(reader, "Consulta")
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return obeAgenciaRecaudadora
        '
    End Function

    'CONSULTAR AGENCIA RECAUDADORA
    Public Function ConsultarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As List(Of BEAgenciaRecaudadora)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listAgenciaRecaudadoras As New List(Of BEAgenciaRecaudadora)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenciaRecaudadora", _
            obeAgenciaRecaudadora.NombreComercial, _
            obeAgenciaRecaudadora.Contacto, _
            obeAgenciaRecaudadora.IdEstado)

                While reader.Read
                    '
                    listAgenciaRecaudadoras.Add(New BEAgenciaRecaudadora(reader, "busq", ""))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return listAgenciaRecaudadoras
        '
    End Function

    'REGISTRAR AGENTE
    Public Function RegistrarAgenteRecaudador(ByVal obeAgente As BEAgente) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarAgente", _
            obeAgente.IdCiudad, obeAgente.Nombres, obeAgente.Apellidos, obeAgente.IdTipoDocumento, obeAgente.NumeroDocumento, _
            obeAgente.Direccion, obeAgente.Telefono, obeAgente.Email, obeAgente.Password, _
            obeAgente.IdGenero, obeAgente.FechaNacimiento, _
            obeAgente.IdAgenciaRecaudadora, _
            obeAgente.IdTipoAgente, _
            obeAgente.CodigoRegistro, _
            obeAgente.IdEstado, _
            obeAgente.IdUsuarioCreacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    'ACTUALIZAR AGENTE
    Public Function ActualizarAgente(ByVal obeAgente As BEAgente) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarAgente", _
            obeAgente.IdAgente, obeAgente.IdCiudad, obeAgente.Nombres, obeAgente.Apellidos, _
            obeAgente.IdTipoDocumento, obeAgente.NumeroDocumento, obeAgente.Direccion, _
            obeAgente.Telefono, obeAgente.Password, obeAgente.IdAgenciaRecaudadora, _
            obeAgente.IdTipoAgente, obeAgente.IdGenero, obeAgente.FechaNacimiento, _
            obeAgente.IdEstado, obeAgente.IdUsuarioActualizacion _
            ))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    'CONSULTAR AGENTE
    Public Function ConsultarAgente(ByVal obeAgente As BEAgente) As List(Of BEAgente)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listAgentes As New List(Of BEAgente)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgente", _
                        obeAgente.IdTipoAgente, _
                        obeAgente.DescAgenciaRecaudadora, _
                        obeAgente.Nombres, _
                        obeAgente.Apellidos, _
                        obeAgente.Telefono, _
                        obeAgente.IdEstado)

                While reader.Read
                    '
                    listAgentes.Add(New BEAgente(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return listAgentes
        '
    End Function


    'CONSULTAR AGENCIA RECAUDADORA POR IDAGENCIA
    Public Function ConsultarAgentePorIdAgenteOrIdUsuario(ByVal opcion As Integer, ByVal IdAgenteUsuario As Integer) As BEAgente
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgente As New BEAgente

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgentePorIdAgente", _
                    opcion, _
                    IdAgenteUsuario)

                While reader.Read
                    '
                    obeAgente = New BEAgente(reader)
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return obeAgente
        '
    End Function


    'CONSULTAR CAJA POR IDAGENCIA
    Public Function ConsultarCaja(ByVal obeCaja As BECaja) As List(Of BECaja)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCajas As New List(Of BECaja)

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarCajaPorIdAgencia", _
                    obeCaja.IdAgenciaRecaudadora, _
                    obeCaja.IdUsoCaja, _
                    obeCaja.IdEstado)

                While reader.Read
                    '
                    listCajas.Add(New BECaja(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return listCajas
        '
    End Function

    'REGISTRAR AGENTE CAJA
    Public Function RegistrarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarAgenteCaja", _
            obeAgenteCaja.IdCaja, _
            obeAgenteCaja.IdAgente, _
            obeAgenteCaja.FechaApertura, _
            obeAgenteCaja.EmailSupervisorApertura, _
            obeAgenteCaja.IdEstado, _
            obeAgenteCaja.IdUsuarioCreacion, _
            obeAgenteCaja.NroOPRecibidas))
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    'REGISTRO DE MOVIMIENTOS
    Public Function RegistrarMovimiento(ByVal obeMovimiento As BEMovimiento) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarMovimiento", _
            obeMovimiento.IdOrdenPago, _
            obeMovimiento.IdAgenteCaja, _
            obeMovimiento.IdMoneda, _
            obeMovimiento.IdTipoMovimiento, _
            obeMovimiento.IdMedioPago, _
            obeMovimiento.Monto, _
            obeMovimiento.FechaMovimiento, _
            obeMovimiento.IdUsuarioCreacion))
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    'CONSULTAR AGENTE CAJA
    Public Function ConsultarAgenteCajaPorIdUsuarioIdEstado(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listAgenteCaja As New List(Of BEAgenteCaja)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarAgenteCajaPorIdUsuarioIdEstado", _
                    obeAgenteCaja.IdUsuarioCreacion, _
                    obeAgenteCaja.IdEstado)

                While reader.Read
                    '
                    listAgenteCaja.Add(New BEAgenteCaja(reader, "ConsultaParaAperturarCaja"))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return listAgenteCaja
    End Function

    'CONSULTA CAJA ACTIVA POR AGENTE
    Public Function ConsultaCajaActiva(ByVal IdAgente As Integer) As BEAgenteCaja

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgenteCaja As New BEAgenteCaja

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCajaActiva]", _
                    IdAgente)

                While reader.Read
                    '
                    obeAgenteCaja = New BEAgenteCaja(reader, "LiquidarCaja")
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return obeAgenteCaja

    End Function


    'CONSULTA AGENTE CAJA AVANZADA
    Public Function ConsultarCajaAvanzada(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listAgenteCaja As New List(Of BEAgenteCaja)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarCajaAvanzada", _
                    obeAgenteCaja.IdAgenciaRecaudadora, _
                    obeAgenteCaja.IdUsuarioCreacion, _
                    obeAgenteCaja.NombreAgente, _
                    obeAgenteCaja.NombreEstado, _
                    obeAgenteCaja.FechaCreacion, _
                    obeAgenteCaja.FechaActualizacion, _
                    obeAgenteCaja.IdEstado)
                While reader.Read
                    '
                    listAgenteCaja.Add(New BEAgenteCaja(reader, "ConsultarCajaAvanzada"))
                    '
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return listAgenteCaja

    End Function


    'CONSULTA MONTOS POR TIPO TARJETA , MONEDA Y AGENTE_CAJA
    Public Function ConsultaMontosPorAgenteCaja(ByVal IdAgenteCaja As Integer) As List(Of BEMovimiento)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListMontosCaja As New List(Of BEMovimiento)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarMontoPorAgenteCaja", _
                    IdAgenteCaja)

                While reader.Read
                    '
                    ListMontosCaja.Add(New BEMovimiento(reader, "LiquidarCaja"))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return ListMontosCaja

    End Function

    'LIQUIDAR CAJA DE AGENTE
    Public Function LiquidarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_LiquidarAgenteCaja]", _
            obeAgenteCaja.IdAgenteCaja))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado

    End Function

    'CONSULTAR CAJAS CERRADAS PARA LIQUIDAR EN BLOQUE
    Public Function ConsultarCajasCerradas(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListCajasCerrardas As New List(Of BEAgenteCaja)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCajasCerradas]", _
                    obeAgenteCaja.IdUsuarioCreacion)

                While reader.Read

                    ListCajasCerrardas.Add(New BEAgenteCaja(reader, "LiquidarCaja"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return ListCajasCerrardas

    End Function

    'LIQUIDAR CAJA DE AGENTES EN BLOQUE
    Public Function LiquidarCajaEnBLoque(ByVal obeAgenteCaja As BEAgenteCaja) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_LiquidarCajaEnBloque", _
                        obeAgenteCaja.IdAgenteCaja, _
                        obeAgenteCaja.IdUsuarioActualizacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado

    End Function

    Public Function ConsultarAgentesConCajaPendientesdeCierre(ByVal IdTipoFechaValuta As Integer, ByVal IdAgencia As Integer) As List(Of BEAgente)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listAgentes As New List(Of BEAgente)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarAgentesConSinCajaPendientesCierre]", _
                    IdTipoFechaValuta, _
                 IdAgencia)
                While reader.Read
                    '
                    listAgentes.Add(New BEAgente(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return listAgentes
        '
    End Function

    Public Function ConsultarCajaActivaPorIdAgente(ByVal IdAgente As Integer) As BEAgenteCaja

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgenteCaja As New BEAgenteCaja

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCajaPorAgente]", _
                    IdAgente)

                While reader.Read
                    '
                    obeAgenteCaja = New BEAgenteCaja(reader)
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return obeAgenteCaja

    End Function

    Public Function RegistrarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_OtorgarFechaValuta", _
            obeFechaValuta.IdTipoFechaValuta, _
            obeFechaValuta.Fecha, _
            obeFechaValuta.IdAgente, _
            obeFechaValuta.IdCaja, _
            obeFechaValuta.IdAgenteCaja, _
            obeFechaValuta.IdUsuarioCreacion))
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    Public Function ActualizarEstadoFechaValuta(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarEstadoFechaValuta]", obeAgenteCaja.IdAgenteCaja, obeAgenteCaja.IdUsuarioActualizacion, obeAgenteCaja.EstadoFechaValuta))

            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
    End Function

    Public Function ActualizarEstadoFechaValutaPorIdAgente(ByVal obeFechaValuta As BEFechaValuta) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarFechaValutaPorIdAgente", _
            obeFechaValuta.IdTipoFechaValuta, _
            obeFechaValuta.IdAgente, _
            obeFechaValuta.IdAgenteCaja, _
            obeFechaValuta.IdEstado, _
            obeFechaValuta.IdUsuarioActualizacion))
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
    End Function

    Public Function ConsultarFechaValutaAgenteCaja(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim objFechaValuta As New BEFechaValuta

        '
        Try
            '
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarFechaValuta]", obeFechaValuta.IdAgenteCaja)

                While reader.Read
                    '
                    objFechaValuta = New BEFechaValuta(reader)
                    '
                End While
            End Using
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return objFechaValuta
    End Function

    Public Function ConsultarFechaValutaPorAgente(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim objFechaValuta As New BEFechaValuta

        '
        Try
            '
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_TraerFechaValutaPorIdAgente", _
            obeFechaValuta.IdTipoFechaValuta, _
            obeFechaValuta.IdAgente, _
            obeFechaValuta.IdEstado)

                While reader.Read
                    '
                    objFechaValuta = New BEFechaValuta(reader)
                    '
                End While
            End Using
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return objFechaValuta
    End Function

    Public Function AnularAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_AnularAgenteCaja]", obeAgenteCaja.IdAgenteCaja, obeAgenteCaja.IdUsuarioActualizacion, obeAgenteCaja.EmailSupervisorTermino))

            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    'OBTENER AGENTES POR AGENCIA
    Public Function ConsultarAgentesPorAgencia(ByVal IdSupervisor As Integer) As List(Of BEAgenteCaja)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListAgentes As New List(Of BEAgenteCaja)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ObtenerAgentesPorAgencia]", _
                    IdSupervisor)

                While reader.Read

                    ListAgentes.Add(New BEAgenteCaja(reader, "LiquidarCaja"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return ListAgentes

    End Function

    'CONSULTA CAJA ACTIVA POR AGENTE
    Public Function ConsultarCajaActivaPorAgente(ByVal IdAgente As Integer) As BEAgenteCaja

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgenteCaja As New BEAgenteCaja

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCajaActivaPorAgente]", _
                    IdAgente)

                While reader.Read
                    '
                    obeAgenteCaja = New BEAgenteCaja(reader, "LiquidarCaja")
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return obeAgenteCaja

    End Function

    'REGISTRAR SOBRANTE O FALTANTE
    Public Function RegistrarSobranteFaltante(ByVal obeMovimiento As BEMovimiento) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarSobranteFaltante]", _
                          obeMovimiento.IdAgenteCaja, obeMovimiento.IdMoneda, obeMovimiento.IdTipoMovimiento, _
                          obeMovimiento.IdMedioPago, obeMovimiento.Monto, obeMovimiento.IdUsuarioCreacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado

    End Function

    'CONSULTAR EL DETALLE DE LAS CAJAS POR AGENTE
    Public Function ConsultarDetalleDeCaja(ByVal IdAgenteCaja As Integer) As List(Of BEMovimiento)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListDetalle As New List(Of BEMovimiento)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleCajaAgente", _
                    IdAgenteCaja)

                While reader.Read

                    ListDetalle.Add(New BEMovimiento(reader, "DetalleCaja"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return ListDetalle

    End Function

    'CONSULTA DE TODAS LAS CAJAS CERRADAS
    Public Function ConsultarResumenCajasCerradas(ByVal parListAgenteCaja As String) As List(Of BEMovimiento)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListDetalle As New List(Of BEMovimiento)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarResumenCajasCerradas", _
                    parListAgenteCaja)

                While reader.Read

                    ListDetalle.Add(New BEMovimiento(reader, "ResumenCajaCerradas"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return ListDetalle
        '
    End Function

    'ACTUALIZAMOS DE LOS MOVIMIENTOS DE LAS CAJAS LIQUIDADA
    Public Function ActualizarMovimientoPorCajaLiquidada(ByVal obeMovimiento As BEMovimiento) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarMovimientoPorCajaLiquidada", _
            obeMovimiento.IdAgenteCaja, _
            obeMovimiento.IdOrdenPago, _
            obeMovimiento.IdMoneda, _
            obeMovimiento.IdUsuarioActualizacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    'CONSULTA CAJAS LIQUIDADAS, METODO PARA EL REPORTE
    Public Function ConsultarCajasLiquidadas(ByVal IdUsuario As Integer, ByVal NroOrdenPago As String, ByVal FechaIncio As DateTime, ByVal FechaFin As DateTime) As List(Of BEAgenteCaja)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListAgenteCaja As New List(Of BEAgenteCaja)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCajasLiquidadas]", _
                    IdUsuario, NroOrdenPago, FechaIncio, FechaFin)

                While reader.Read

                    ListAgenteCaja.Add(New BEAgenteCaja(reader, "ConsultaCajasLiquidadas"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return ListAgenteCaja

    End Function

    'CONSULTA DEL DETALLE DE LOS MOVIMIENTOS DE UNA CAJA
    Public Function ConsultarDetalleMovimientosCaja(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEMovimientoConsulta)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListDetalleMovimiento As New List(Of BEMovimientoConsulta)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleMovimientosCaja", _
                    obeAgenteCaja.IdAgenteCaja)

                While reader.Read

                    ListDetalleMovimiento.Add(New BEMovimientoConsulta(reader, "prc_ConsultarDetalleMovimientosCaja"))

                End While

            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        '
        Return ListDetalleMovimiento
        '
    End Function


    Public Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As BEMovimiento
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As New BEMovimiento
        '
        Try
            '
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarMovimientoporOrdenPago]", _
                    obeMovimiento.NumeroOrdenPago, obeMovimiento.IdTipoMovimiento)

                While reader.Read
                    '
                    resultado = New BEMovimiento(reader)
                    '
                End While
            End Using
            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function

    Public Overrides Function DeleteEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As New _3Dev.FW.Entidades.BusinessMessageBase()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = ObjectToInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarAgenciaRecaudadora]", ObjectToInt(request.EntityId)))
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok
        Catch ex As Exception
            'Logger.Write(ex)
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Error
            Throw ex
        End Try
        Return response
    End Function

    Public Function CantidadAgentesPorIdAgenciaRecaudadora(ByVal idAgenciaRecaudadora As Integer) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CantidadAgentesPorIdAgenciaRecaudadora]", idAgenciaRecaudadora))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    '    Private disposedValue As Boolean = False        ' To detect redundant calls

    '    ' IDisposable
    '    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
    '        If Not Me.disposedValue Then
    '            If disposing Then
    '                ' TODO: free managed resources when explicitly called
    '            End If

    '            ' TODO: free shared unmanaged resources
    '        End If
    '        Me.disposedValue = True
    '    End Sub

    '#Region " IDisposable Support "
    '    ' This code added by Visual Basic to correctly implement the disposable pattern.
    '    Public Sub Dispose() Implements IDisposable.Dispose
    '        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '        Dispose(True)
    '        GC.SuppressFinalize(Me)
    '    End Sub
    '#End Region

End Class

