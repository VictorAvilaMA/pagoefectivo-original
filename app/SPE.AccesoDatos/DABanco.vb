Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Data.SqlClient
Imports System.Configuration

Public Class DABanco

    'REGISTRAR BANCO
    Function RegistrarBanco(ByVal oBEBanco As BEBanco) As Integer
        '
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDataBase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarBanco]", oBEBanco.IdBanco, _
                        oBEBanco.Codigo, oBEBanco.Descripcion, oBEBanco.IdEstado, oBEBanco.IdUsuarioCreacion))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        '
        Return Resultado
        '
    End Function

    'CONSULTAR AGENCIA BANCARIA
    Function ConsultarBanco(ByVal oBEBanco As BEBanco) As List(Of BEBanco)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listBanco As New List(Of BEBanco)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarBanco", _
            If(oBEBanco.Descripcion Is Nothing, "", oBEBanco.Descripcion), If(oBEBanco.Codigo Is Nothing, "", oBEBanco.Codigo), oBEBanco.IdEstado)

                While reader.Read
                    listBanco.Add(New BEBanco(reader))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return listBanco
    End Function

    '<add Peru.Com FaseIII>
    Function ConsultarCuentaBanco(ByVal oBECuentaBanco As BECuentaBanco) As List(Of BECuentaBanco)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of BECuentaBanco)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarCuentaBanco", _
            oBECuentaBanco.IdBanco, oBECuentaBanco.IdMoneda)
                While reader.Read
                    listCuentaBanco.Add(New BECuentaBanco(reader))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function

    '<upd proc.Tesoreria>
    Public Function RegistrarDeposito(ByVal beDeposito As BEDeposito, ByVal detalleDeposito As List(Of BEOrdenPago)) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim dt As New DataTable
                dt.Columns.Add("IdOrden")
                dt.Columns.Add("Comision")
                For Each item As BEOrdenPago In detalleDeposito
                    If item.FlagSeleccionado Then
                        dt.Rows.Add(item.IdOrdenPago, item.Comision)
                    End If
                Next
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_RegistrarDeposito]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@FechaDeposito", SqlDbType.DateTime).Value = beDeposito.FechaDeposito
                oSqlCommand.Parameters.Add("@CodigoOperacion", SqlDbType.VarChar).Value = beDeposito.CodigoOperacion
                oSqlCommand.Parameters.Add("@Comentarios", SqlDbType.VarChar).Value = beDeposito.Comentarios
                oSqlCommand.Parameters.Add("@MontoDepositado", SqlDbType.Decimal).Value = beDeposito.MontoDepositado
                oSqlCommand.Parameters.Add("@IdUsuarioRegistro", SqlDbType.Int).Value = beDeposito.IdUsuarioCreacion
                oSqlCommand.Parameters.Add("@IdFactura", SqlDbType.Int).Value = IIf(beDeposito.IdFactura = 0, DBNull.Value, beDeposito.IdFactura)
                oSqlCommand.Parameters.Add("@NroTransaccion", SqlDbType.VarChar).Value = beDeposito.NroTransaccion
                oSqlCommand.Parameters.Add("@IdConciliacionArchivo", SqlDbType.Int).Value = beDeposito.IdConciliacionArchivo
                oSqlCommand.Parameters.Add("@ploteOperaciones", SqlDbType.Structured).Value = dt
                oSqlConnection.Open()
                Resultado = oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ConsultarDepositoRegistroFactura(ByVal beDeposito As BEDeposito) As List(Of BEDeposito)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of BEDeposito)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDepositoToFactura", _
                                                                    beDeposito.NroTransaccion, _
                                                                    beDeposito.IdBanco, _
                                                                    beDeposito.CodigoOperacion, _
                                                                    beDeposito.FechaDesde, _
                                                                    beDeposito.FechaHasta)
                'beDeposito.PageNumber, 
                'beDeposito.PageSize, _
                'beDeposito.PropOrder, _
                'beDeposito.TipoOrder

                While reader.Read
                    listCuentaBanco.Add(New BEDeposito(reader, "consultaRegistroFactura"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function
    Public Function RegistrarFactura(ByVal beFactura As BEFactura, ByVal detalleDeposito As List(Of BEDeposito)) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim dt As New DataTable
                dt.Columns.Add("IdParam")
                dt.Columns.Add("ValueParam")
                For Each item As BEDeposito In detalleDeposito
                    dt.Rows.Add(item.IdDeposito, item.IdDeposito)
                Next
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_RegistrarFactura]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@NroFactura", SqlDbType.VarChar).Value = beFactura.NroFactura
                oSqlCommand.Parameters.Add("@MontoFactura", SqlDbType.Decimal).Value = beFactura.MontoFactura
                oSqlCommand.Parameters.Add("@FechaEmision", SqlDbType.DateTime).Value = beFactura.FechaEmision
                oSqlCommand.Parameters.Add("@Observacion", SqlDbType.VarChar).Value = beFactura.Observacion
                oSqlCommand.Parameters.Add("@IdUsuarioRegistro", SqlDbType.Int).Value = beFactura.IdUsuarioCreacion
                oSqlCommand.Parameters.Add("@IdsDepositos", SqlDbType.Structured).Value = dt
                oSqlConnection.Open()
                Resultado = oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Function ConsultarDeposito(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDeposito", _
                                                                    beDeposito.NroTransaccion, _
                                                                    beDeposito.IdBanco, _
                                                                    beDeposito.CodigoOperacion, _
                                                                    beDeposito.CIP, _
                                                                    beDeposito.FechaDesde, _
                                                                    beDeposito.FechaHasta, _
                                                                    beDeposito.PageNumber, _
                                                                    beDeposito.PageSize, _
                                                                    beDeposito.PropOrder, _
                                                                    beDeposito.TipoOrder)

                While reader.Read
                    listCuentaBanco.Add(New BEDeposito(reader, "consultaRegistro"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function
    Public Function ConsultarDepositoDetalleByIdDeposito(ByVal request As BEDeposito) As List(Of BEOrdenPago)
        Dim response As New List(Of BEOrdenPago)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarDepositoDetalleByIdDeposito]", request.IdDeposito)
                While reader.Read
                    response.Add(New BEOrdenPago(reader, "DepositoDetalleByIdDeposito"))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function
    Public Function ConsultarDepositoByIdFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDepositoById", _
                                                                    beDeposito.IdFactura)

                While reader.Read
                    listCuentaBanco.Add(New BEDeposito(reader, "consultaByIdFactura"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function
    Public Function ConsultarFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarFactura", _
                                                                    beDeposito.NroTransaccion, _
                                                                    beDeposito.IdBanco, _
                                                                    beDeposito.CodigoOperacion, _
                                                                    beDeposito.CIP, _
                                                                    beDeposito.FechaDesde, _
                                                                    beDeposito.FechaHasta, _
                                                                    beDeposito.PageNumber, _
                                                                    beDeposito.PageSize, _
                                                                    beDeposito.PropOrder, _
                                                                    beDeposito.TipoOrder)
                While reader.Read
                    listCuentaBanco.Add(New BEFactura(reader, "consultaFactura"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function
    Public Function ConsultarDepositoReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarDepositoReporte]", _
                                                                    beDeposito.NroTransaccion, _
                                                                    beDeposito.IdBanco, _
                                                                    beDeposito.CodigoOperacion, _
                                                                    beDeposito.CIP, _
                                                                    beDeposito.FechaDesde, _
                                                                    beDeposito.FechaHasta)
                While reader.Read
                    listCuentaBanco.Add(New BEDetDeposito(reader, "consultaDepositoReporte"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function
    Public Function ConsultarFacturaReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCuentaBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarFacturaReporte]", _
                                                                    beDeposito.NroTransaccion, _
                                                                    beDeposito.IdBanco, _
                                                                    beDeposito.CodigoOperacion, _
                                                                    beDeposito.CIP, _
                                                                    beDeposito.FechaDesde, _
                                                                    beDeposito.FechaHasta)
                While reader.Read
                    listCuentaBanco.Add(New BEDetDeposito(reader, "consultaFacturaReporte"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listCuentaBanco
    End Function
End Class

