Imports System.Data
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Public Class DACaja

    'REGISTRAR CAJA
    Public Function RegistrarCaja(ByVal obeCaja As BECaja) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarCaja]", _
            obeCaja.IdAgenciaRecaudadora, obeCaja.Nombre, obeCaja.IdEstado, obeCaja.IdUsuarioCreacion, _
            obeCaja.IdUsoCaja))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado

    End Function


    'ACTUALIZAR CAJA
    Public Function ActualizarCaja(ByVal obeCaja As BECaja) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarCaja]", _
            obeCaja.IdCaja, obeCaja.Nombre, obeCaja.IdEstado, obeCaja.IdUsuarioActualizacion, _
            obeCaja.IdUsoCaja))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado

    End Function

    'CONSULTAR CAJA POR AGENCIA
    Public Function ConsultarCajaPorAgencia(ByVal IdAgenciaRecaudadora As Integer) As List(Of BECaja)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListCaja As New List(Of BECaja)

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarCaja", IdAgenciaRecaudadora)

                While reader.Read()

                    ListCaja.Add(New BECaja(reader))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try

        Return ListCaja
    End Function

    'OBTENER AGENCIA POR USUARIO SUPERVISOR
    Public Function ObtenerAgenciaPorUsuario(ByVal IdUsuario As Integer) As BEAgenciaRecaudadora
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeAgencia As New BEAgenciaRecaudadora


        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ObtenerAgenciaRecaudadoraPorUsuario]", IdUsuario)

                While reader.Read
                    obeAgencia = New BEAgenciaRecaudadora(reader)
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try

        Return obeAgencia
    End Function

    Public Function OtorgarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarFechaValuta]")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return 0
    End Function

End Class
