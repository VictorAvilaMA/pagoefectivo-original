Imports System
Imports System.Security
Imports System.Web
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades.Seguridad

Imports SPE.Entidades
Imports System.Configuration


Public Class DACliente
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    ''' <summary>
    ''' Metodo para registrar un cliente
    ''' </summary>
    ''' <param name="be">Entidad que contiene los datos del cliente</param>
    ''' <returns>Valor que indica el registro o no del cliente</returns>
    ''' <remarks></remarks>
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer
        Dim obeCliente As BECliente = CType(be, BECliente)

        'Actualización MIRGUECE 08/01/2010 Requerimiento redmine #56' 
        If _3Dev.FW.Util.DataUtil.ObjectToInt(obeCliente.IdOrigenRegistro) = 0 Then
            obeCliente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarCliente]", _
            IntToDBNull(obeCliente.IdCiudad), obeCliente.Nombres, obeCliente.Apellidos, _
            obeCliente.IdTipoDocumento, obeCliente.NumeroDocumento, obeCliente.Direccion, obeCliente.Telefono, _
            obeCliente.Email, obeCliente.Password, obeCliente.AliasCliente, obeCliente.EmailAlternativo, _
            obeCliente.IdOrigenRegistro, obeCliente.IdGenero, DateTimeToDBNull(obeCliente.FechaNacimiento), _
            obeCliente.CodigoRegistro, obeCliente.IdEstado, obeCliente.IdUsuarioCreacion)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            resultado = CInt(objDataBase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        obeCliente.IdUsuario = resultado
        Return resultado
    End Function

    Public Function RegistrarClienteDesdeUsuario(ByVal obeCliente As BECliente) As BECliente
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarClienteDesdeUsuario]", _
            obeCliente.IdUsuario, obeCliente.AliasCliente, obeCliente.EmailAlternativo, 1)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            resultado = CInt(objDataBase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        obeCliente.IdCliente = resultado
        Return obeCliente
    End Function

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeCliente As SPE.Entidades.BECliente = CType(be, SPE.Entidades.BECliente)
        Try

            'Actualización MIRGUECE 08/01/2010 Requerimiento redmine #56' 
            Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarCliente]", obeCliente.IdCliente, _
            obeCliente.IdCiudad, obeCliente.Nombres, obeCliente.Apellidos, obeCliente.IdTipoDocumento, obeCliente.NumeroDocumento, _
            obeCliente.Direccion, obeCliente.Telefono, obeCliente.IdGenero, obeCliente.FechaNacimiento, _
            obeCliente.AliasCliente, obeCliente.EmailAlternativo, obeCliente.IdEstado, obeCliente.IdUsuarioActualizacion, _
            obeCliente.ImagenAvatar, obeCliente.FlagRegistrarPc))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Overrides Function GetObjectByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Object
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Object = 0
        Dim obeCliente As BECliente = CType(be, BECliente)
        Dim oDbCommand As DbCommand
        Dim oDbCommand1 As DbCommand
        Try
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ValidarEmail]", obeCliente.Email)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            If CObj(objDatabase.ExecuteScalar(oDbCommand)) > 0 Then
                resultado = 1
            End If
            oDbCommand1 = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ValidarTipoyNumeroDoc]", obeCliente.IdTipoDocumento, obeCliente.NumeroDocumento)
            oDbCommand1.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            If resultado = 0 Then
                If CObj(objDatabase.ExecuteScalar(oDbCommand1)) > 0 Then
                    resultado = 2
                End If
            End If

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado

    End Function
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListCliente As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCliente As SPE.Entidades.BECliente = CType(be, SPE.Entidades.BECliente)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarClientePG]", obeCliente.Email, obeCliente.Nombres, obeCliente.Apellidos, obeCliente.IdEstado, obeCliente.PageNumber, obeCliente.PageSize, obeCliente.PropOrder, obeCliente.TipoOrder)
                While reader.Read()
                    ListCliente.Add(New SPE.Entidades.BECliente().MakePG(reader))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return ListCliente
    End Function

    Public Function ValidarEmailUsuarioPorEstado(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As SPE.Entidades.BEUsuarioBase
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim objUsuario As New SPE.Entidades.BEUsuarioBase
        Dim resultado As Integer = 0

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ValidarEmailUsuarioPorEstado]", obeUsuario.Email)
                While reader.Read
                    objUsuario = New SPE.Entidades.BEUsuarioBase(reader, "ValidarUsuarioEmail")
                End While
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return objUsuario
    End Function

    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeCliente As _3Dev.FW.Entidades.BusinessEntityBase = Nothing

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarClientePorEmail]", id)
                While reader.Read
                    obeCliente = New SPE.Entidades.BECliente(reader, 1)
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return obeCliente

    End Function

    Public Function ActualizarEstadoUsuario(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarEstadoUsuario]", obeUsuario.Email, obeUsuario.IdEstado)

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function



    Public Function RegistrarPreOrdenRegistroUsuario(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim resultadoDetalle As Integer = 0
        Dim i As Integer = 0

        Try
            'Generando la Pre Código de Identificación de Pago

            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarPreOrdenPago", _
            obeOrdenPago.MerchantID, _
            obeOrdenPago.DatosEnc, _
            obeOrdenPago.ParUtil, _
            obeOrdenPago.Url, _
            obeOrdenPago.IdUsuarioCreacion, _
            obeOrdenPago.XmlString, _
            obeOrdenPago.UrlServicio))
            '

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try

        Return resultado

    End Function
    Public Function ValidaConfirmacionDeUsuario(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeClienteRetorno As New BECliente
        Dim obeOrdenPago As New BEOrdenPago
        obeOrdenPago = Nothing
        obeClienteRetorno.OrdenPago = Nothing
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Object = 0
        Dim obeCliente As BECliente = CType(be, BECliente)

        Try

            resultado = CObj(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ConfirmarValidacionDeUsuario]", obeCliente.Email, obeCliente.CodigoRegistro))

            obeClienteRetorno.IdCliente = resultado

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPreOrdenPago]", resultado)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultaPreOrdenPago")
                End While

            End Using

            obeClienteRetorno.OrdenPago = obeOrdenPago

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        Finally

        End Try

        Return obeClienteRetorno

    End Function


    Public Overrides Function GetBusinessEntityByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Select Case key
            Case "ValidaConfirmacionDeUsuario"
                Return ValidaConfirmacionDeUsuario(be)

        End Select
        Return Nothing
    End Function

    Public Function CambiarContraseñaUsuario(ByVal obeUsuario As BEUsuario) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try

            resultado = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CambiarContraseniaUsuario]", obeUsuario.IdUsuario, obeUsuario.Password, obeUsuario.NuevoPassword, obeUsuario.IdUsuarioActualizacion)

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function GenerarPreordenPagoSuscriptores(ByVal xmlstring As String, ByVal urlservicio As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try

            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarPreOrdenPagoSuscriptor]", xmlstring, urlservicio))

            '
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function ObtenerUsuarioAnonimo() As BECliente
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeCliente As _3Dev.FW.Entidades.BusinessEntityBase = Nothing

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarUsuarioAnonimo]")

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    obeCliente = New SPE.Entidades.BECliente(reader)
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return obeCliente
    End Function


    Public Function ConsultarClientePorEmail(ByVal email As Object) As BECliente

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeCliente As _3Dev.FW.Entidades.BusinessEntityBase = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarClientePorEmail]", email)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    obeCliente = New SPE.Entidades.BECliente(reader)
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return obeCliente

    End Function






    Public Function ConsultarClientePorId(ByVal IdCliente As Long) As BECliente
        Dim obeCliente As BECliente = Nothing
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarClientePorId]", IdCliente)
                If reader.Read Then
                    obeCliente = New SPE.Entidades.BECliente(reader)
                    obeCliente.IdCiudad = DataUtil.ObjectToInt32(reader("IdCiudad"))
                    obeCliente.Nombres = DataUtil.ObjectToString(reader("Nombres"))
                    obeCliente.Apellidos = DataUtil.ObjectToString(reader("Apellidos"))
                    obeCliente.IdTipoDocumento = DataUtil.ObjectToInt32(reader("IdTipoDocumento"))
                    obeCliente.NumeroDocumento = DataUtil.ObjectToString(reader("NumeroDocumento"))
                    obeCliente.Direccion = DataUtil.ObjectToString(reader("Direccion"))
                    obeCliente.Telefono = DataUtil.ObjectToString(reader("Telefono"))
                    obeCliente.Email = DataUtil.ObjectToString(reader("Email"))
                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeCliente
    End Function

    Public Function ObtenerClientexIdUsuario(ByVal obeCliente As BECliente) As BECliente

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarClientexIDUsuario]", obeCliente.IdUsuario)
                While reader.Read
                    obeCliente = New SPE.Entidades.BECliente()
                    obeCliente.IdCliente = ObjectToInt32(reader("IdCliente"))
                    obeCliente.Nombres = ObjectToString(reader("Nombres"))
                    obeCliente.Apellidos = ObjectToString(reader("Apellidos"))
                    obeCliente.IdTipoDocumento = ObjectToInt32(reader("IdTipoDocumento"))
                    obeCliente.Email = ObjectToString(reader("Email"))
                    obeCliente.NumeroDocumento = ObjectToString(reader("NumeroDocumento"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try
        Return obeCliente

    End Function

    Public Function ValidarTipoyNumeroDocPorEmail(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        ' Dim objUsuario As New SPE.Entidades.BEUsuarioBase
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarTipoyNumeroDocPorEmail]", obeUsuario.IdTipoDocumento, obeUsuario.NumeroDocumento, obeUsuario.Email))
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
End Class
