Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades

Public Class DAComercio
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase


    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeComercio As BEComercio = CType(be, BEComercio)
        Try

            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarComercio", _
            obeComercio.codComercio, obeComercio.razonSocial, obeComercio.ruc, _
            obeComercio.contacto, obeComercio.telefono, _
            obeComercio.direccion, obeComercio.idCiudad, _
            obeComercio.idEstado, obeComercio.IdUsuarioCreacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function

    'CONSULTAR EMPRESA CONTRATANTE
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListComercio As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeComercio As BEComercio = CType(be, BEComercio)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarComercio", _
            obeComercio.codComercio, obeComercio.razonSocial, obeComercio.ruc, obeComercio.contacto, obeComercio.idEstado)

                While reader.Read()

                    ListComercio.Add(New BEComercio(reader, "busq"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListComercio

    End Function
    '************************************************************************************************** 
    ' M�todo          : UpdateRecord
    ' Descripci�n     : Actualizacion de un Establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 21/02/2009
    ' Parametros_In   : Instancia de la Entidad BEEstablecimiento cargada
    ' Parametros_Out  : Resultado de la transaccion
    '*************************************************************************************************

    Public Function GetComercioWhitAO() As List(Of BEComercio)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListComercio As New List(Of BEComercio)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarTodosLosComercio")

                While reader.Read()

                    ListComercio.Add(New BEComercio(reader, "busqWithAO"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListComercio
    End Function


    '************************************************************************************************** 
    ' M�todo          : UpdateRecord
    ' Descripci�n     : Actualizacion de un Establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 14/02/2009
    ' Parametros_In   : Instancia de la Entidad BEEstablecimiento cargada
    ' Parametros_Out  : Resultado de la transaccion
    '*************************************************************************************************
    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeComercio As BEComercio = CType(be, BEComercio)
        Try
            resultado = objDatabase.ExecuteScalar("PagoEfectivo.prc_ActualizarComercio", _
            obeComercio.codComercio, _
            obeComercio.idComercio, obeComercio.razonSocial, _
            obeComercio.ruc, obeComercio.contacto, obeComercio.telefono, _
             obeComercio.direccion, obeComercio.idCiudad, obeComercio.idEstado, obeComercio.IdUsuarioActualizacion)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function
    Public Overrides Function GetListByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Select Case key
            Case ""
                Return ObtenerComercios(be)
            Case "EmpresasLibresParaServicio"
                Return ObtenerEmpresasLibresParaServicios(be)
        End Select
        Return Nothing

    End Function


    Public Function ObtenerComercios(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListComercio As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeComercio As SPE.Entidades.BEComercio = CType(be, SPE.Entidades.BEComercio)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ObtenerComercios", _
            obeComercio.razonSocial)
                While reader.Read()
                    ListComercio.Add(New SPE.Entidades.BEComercio(reader, "busq"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListComercio

    End Function
    Public Function ObtenerEmpresasLibresParaServicios(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListEmpresa As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeEmpresa As SPE.Entidades.BEEmpresaContratante = CType(be, SPE.Entidades.BEEmpresaContratante)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.[prc_ObtenerEmpresasLibresParaRegServicio]", _
            obeEmpresa.RazonSocial)
                While reader.Read()

                    ListEmpresa.Add(New SPE.Entidades.BEEmpresaContratante(reader))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListEmpresa

    End Function

    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeComercio As New BEComercio
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarComercioPorIdComercio]", CInt(id))
                If reader.Read() Then

                    obeComercio = New BEComercio(reader, "busqByID")

                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeComercio
    End Function

End Class
