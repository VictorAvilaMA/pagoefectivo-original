Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports System.Configuration
Imports SPE.Adapters
Imports MongoDB.Driver
Imports MongoDB.Bson
Imports SPE.Logging

Public Class DAComun
    Inherits _3Dev.FW.AccesoDatos.DataAccessBase
    Private ReadOnly _dlogger As ILogger = New Logger()
    '
    'CONSULTAR MONEDA
    Public Function ConsultarMoneda() As List(Of BEMoneda)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMoneda As New List(Of BEMoneda)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarMoneda")

                While reader.Read
                    listMoneda.Add(New BEMoneda(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listMoneda
        '
    End Function
    Public Function ConsultarMonedaPorId(ByVal parametro As Object) As BEMoneda
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeMoneda As BEMoneda = Nothing


        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarMonedaPorId]", parametro)

                While reader.Read
                    obeMoneda = New BEMoneda(reader)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try


        Return obeMoneda
        '
    End Function

    'CONSULTAR MEDIO PAGO
    Public Function ConsultarMedioPago() As List(Of BEMedioPago)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMedioPago As New List(Of BEMedioPago)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarMedioPago")

                While reader.Read
                    '
                    listMedioPago.Add(New BEMedioPago(reader))
                    '
                End While
            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listMedioPago
        '
    End Function
    '<upd Proc.Tesoreria>
    Public Function ConsultarMedioPagoBancoByBanco(be As BEBanco) As List(Of BEMedioPagoBanco)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMedioPagoBanco As New List(Of BEMedioPagoBanco)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarMedioDePagoByBanco]", be.IdBanco)

                While reader.Read
                    listMedioPagoBanco.Add(New BEMedioPagoBanco(reader))
                End While
            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listMedioPagoBanco
        '
    End Function


    'CONSULTAR MONEDA
    Public Function ConsultarTarjeta() As List(Of BETarjeta)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listTarjeta As New List(Of BETarjeta)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarTarjeta")

                While reader.Read
                    '
                    listTarjeta.Add(New BETarjeta(reader))
                    '
                End While
            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listTarjeta
        '
    End Function

    'REGISTRO DE MOVIMIENTOS
    Public Function RegistrarDetalleTarjeta(ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            '
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarDetalleTarjeta", _
            obeDetalleTarjeta.IdTarjeta, _
            obeDetalleTarjeta.Numero, _
            obeDetalleTarjeta.FechaVencimiento, _
            obeDetalleTarjeta.Lote, _
            obeDetalleTarjeta.Bin, _
            obeDetalleTarjeta.IdMovimiento))
            '
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return resultado
        '
    End Function
    '
    'CONSULTAR MOVIMIENTO
    Public Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As List(Of BEMovimiento)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMovimiento As New List(Of BEMovimiento)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarMovimiento", _
            obeMovimiento.IdOrdenPago, _
            obeMovimiento.IdAgenteCaja, _
            obeMovimiento.IdMoneda, _
            obeMovimiento.IdTipoMovimiento, _
            obeMovimiento.IdMedioPago)

                While reader.Read
                    '
                    listMovimiento.Add(New BEMovimiento(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listMovimiento
        '
    End Function
    '
    'VALIDAR EMAIL
    Public Function ValidarEmail(ByVal value As String) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarEmail]", value))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function ConsultarFechaHoraActual() As DateTime
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As DateTime

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarFechaYHora]")

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            resultado = objDatabase.ExecuteScalar(oDbCommand)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function



    'VERIFICAR SI EXISTE CODIGO AGENCIA BANCARIA
    Public Function VerificarCodigoAgenciaBancaria(ByVal value As String) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarCodigoAgenciaBancaria]", value))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado

    End Function

    Public Function ConsultarTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMovimiento As New List(Of BEMovimiento)
        Dim listTipoOrigenCancelacion As New List(Of BETipoOrigenCancelacion)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarTipoOrigenCancelacion]")

                While reader.Read
                    '
                    listTipoOrigenCancelacion.Add(New BETipoOrigenCancelacion(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listTipoOrigenCancelacion
        '
    End Function

    Public Function ConsultarPasarelaMedioPago() As List(Of BEPasarelaMedioPago)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMovimiento As New List(Of BEMovimiento)
        Dim listPasarelaMedioPago As New List(Of BEPasarelaMedioPago)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPasarelaMedioPago]")

                While reader.Read
                    '
                    listPasarelaMedioPago.Add(New BEPasarelaMedioPago(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listPasarelaMedioPago
        '
    End Function

    Public Function ConsultaNumCorrelativo(ByVal url As String) As Integer
        Dim numcorrelativo As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase("SPESQLPagoEfectivo")
            Dim dbCmd As DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultaServicioNumCorrelativo]")
            objDatabase.AddInParameter(dbCmd, "pstrUrl", DbType.String, url)
            objDatabase.AddOutParameter(dbCmd, "correlativo", DbType.Int32, 11)
            objDatabase.ExecuteNonQuery(dbCmd)
            numcorrelativo = Convert.ToInt32(objDatabase.GetParameterValue(dbCmd, "correlativo"))
            dbCmd.Dispose()
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return numcorrelativo
    End Function

    Public Function ConsultarMonedaBanco() As List(Of BEMonedaBanco)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim lista As New List(Of BEMonedaBanco)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarMonedaBanco]")
                While reader.Read
                    lista.Add(New BEMonedaBanco(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return lista
    End Function


#Region "Log"

    Public Function RegistrarLog(ByVal obelog As BELog) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarLog", obelog.IdTipo, obelog.Origen, _
                            obelog.Descripcion, obelog.Descripcion2, obelog.Parametro1, obelog.Parametro2, obelog.Parametro3, _
                            obelog.Parametro4, obelog.Parametro5, obelog.Parametro6, obelog.Parametro7, obelog.Parametro8), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        obelog.IdLog = resultado
        Return resultado
    End Function

    Public Function ConsultarLog(ByVal obelog As BELog) As List(Of BELog)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim lista As New List(Of BELog)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarLog", obelog.IdTipo, obelog.Origen)
                While reader.Read
                    lista.Add(New BELog(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return lista
    End Function

#End Region

#Region "PasarelaLog"
    ''' <summary>
    ''' registrar en pasarela log
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RegistrarPasLog(ByVal oPasLog As BELog)
        Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        With oPasLog
            Try
                resultado = CInt(oDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarLog", _
                                    .IdTipo, .Origen, .Descripcion, .Descripcion2))
            Catch ex As Exception
                _dlogger.Error(ex, ex.Message)
                Throw ex
            End Try
            oPasLog.IdLog = resultado
        End With
        Return resultado

    End Function


#End Region

#Region "Log BCP"
    Public Function RegistrarLogBCP(ByVal obelog As BELog) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarLogBCP", obelog.IdTipo, obelog.Origen, _
                            obelog.Descripcion, obelog.Descripcion2, obelog.Parametro1, obelog.Parametro2, obelog.Parametro3, _
                            obelog.Parametro4, obelog.Parametro5, obelog.Parametro6, obelog.Parametro7, obelog.Parametro8), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        obelog.IdLog = resultado
        Return resultado
    End Function




    Public Function RegistrarLogBCPConsReq(ByVal request As BEBCPRequest) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As String = ""
        Dim tipoRegistro As String = System.Configuration.ConfigurationManager.AppSettings("flagLogMongoDBSQL")
        Try
            'pjmfb
            Select Case tipoRegistro

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDB
                    Dim id = ObjectId.GenerateNewId()
                    Dim oRequest As BEBCPRequest = request
                    oRequest.Ip = ""
                    oRequest.id = id
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPRequest)(oRequest, "LogBCPConsReq", True)
                    resultado = id.ToString()

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.SQL
                    Dim oDbCommand As DbCommand
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPConsReq", _
                            request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
                            request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
                            request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
                            request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand)).ToString()

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL
                    'mongo
                    Dim id = ObjectId.GenerateNewId()
                    Dim oRequest As BEBCPRequest = request
                    oRequest.Ip = ""
                    oRequest.id = id
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPRequest)(oRequest, "LogBCPConsReq", True)
                    'sql
                    Dim oDbCommand As DbCommand
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPConsReq", _
                            request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
                            request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
                            request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
                            request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = id.ToString() + "-" + ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand)).ToString()
            End Select


            ' End With

            'Dim oDbCommand As DbCommand
            'oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPConsReq", _
            '        request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
            '        request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
            '        request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
            '        request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
            'oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            ''End M
            ''With 
            'resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            '' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBCPConsRes(ByVal response As BEBCPResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim tipoRegistro As String = System.Configuration.ConfigurationManager.AppSettings("flagLogMongoDBSQL")
        Dim valor As String
        Dim oDbCommand As DbCommand
        Dim oResponse As BEBCPResponse = response
        Try
            ' End With

            Select Case tipoRegistro
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDB
                    valor = MongoDBAdapter.CollectionSeleccionarCodigo("LogBCPConsReq", "_id", response.IdRequest, "Codigo")
                    oResponse.CIP = valor
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPResponse)(oResponse, "LogBCPConsRes", False)
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.SQL
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPConsRes", _
                         response.IdRequest, response.Codigo, response.Descripcion1, response.Descripcion2, response.Estado, response.DescEstado)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL
                    'mongo
                    Dim strIdRequestM = response.IdRequest.Split("-")(0).ToString()
                    Dim strIdRequestS = response.IdRequest.Split("-")(1).ToString()
                    valor = MongoDBAdapter.CollectionSeleccionarCodigo("LogBCPConsReq", "_id", strIdRequestM, "Codigo")
                    oResponse.IdRequest = strIdRequestM
                    oResponse.CIP = valor
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPResponse)(oResponse, "LogBCPConsRes", False)
                    'sql
                    oResponse.IdRequest = strIdRequestS
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPConsRes", _
                    oResponse.IdRequest, response.Codigo, response.Descripcion1, response.Descripcion2, response.Estado, response.DescEstado)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End Select



        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBCPCancReq(ByVal request As BEBCPRequest) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As String = ""
        Dim tipoRegistro As String = System.Configuration.ConfigurationManager.AppSettings("flagLogMongoDBSQL")
        Dim oDbCommand As DbCommand
        Try

            Select Case tipoRegistro

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDB
                    Dim id = ObjectId.GenerateNewId()
                    Dim oRequest As BEBCPRequest = request
                    oRequest.Ip = ""
                    oRequest.id = id
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPRequest)(oRequest, "LogBCPCancReq", True)
                    resultado = id.ToString()

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.SQL

                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPCancReq", _
                            request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
                            request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
                            request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
                            request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand)).ToString()


                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL
                    'mongo
                    Dim id = ObjectId.GenerateNewId()
                    Dim oRequest As BEBCPRequest = request
                    oRequest.Ip = ""
                    oRequest.id = id
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPRequest)(oRequest, "LogBCPCancReq", True)
                    'sql
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPCancReq", _
                            request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
                            request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
                            request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
                            request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = id.ToString() + "-" + ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand)).ToString()
            End Select

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBCPCancRes(ByVal response As BEBCPResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim tipoRegistro As String = System.Configuration.ConfigurationManager.AppSettings("flagLogMongoDBSQL")
        Dim valor As String
        Dim oDbCommand As DbCommand
        Dim oResponse As BEBCPResponse = response
        Try
            Select Case tipoRegistro
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDB
                    valor = MongoDBAdapter.CollectionSeleccionarCodigo("LogBCPCancReq", "_id", response.IdRequest, "Codigo")
                    oResponse.CIP = valor
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPResponse)(oResponse, "LogBCPCancRes", False)
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.SQL
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPCancRes", _
                             response.IdRequest, response.Codigo, response.Descripcion1, response.Descripcion2, response.Estado, response.DescEstado)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL
                    'mongo
                    Dim strIdRequestM = response.IdRequest.Split("-")(0).ToString()
                    Dim strIdRequestS = response.IdRequest.Split("-")(1).ToString()
                    valor = MongoDBAdapter.CollectionSeleccionarCodigo("LogBCPCancReq", "_id", strIdRequestM, "Codigo")
                    oResponse.IdRequest = strIdRequestM
                    oResponse.CIP = valor
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPResponse)(oResponse, "LogBCPCancRes", False)
                    'sql
                    oResponse.IdRequest = strIdRequestS
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPCancRes", _
                            response.IdRequest, response.Codigo, response.Descripcion1, response.Descripcion2, response.Estado, response.DescEstado)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End Select
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBCPAnulReq(ByVal request As BEBCPRequest) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As String = ""
        Dim tipoRegistro As String = System.Configuration.ConfigurationManager.AppSettings("flagLogMongoDBSQL")
        Dim oDbCommand As DbCommand
        Try

            Select Case tipoRegistro

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDB
                    Dim id = ObjectId.GenerateNewId()
                    Dim oRequest As BEBCPRequest = request
                    oRequest.Ip = ""
                    oRequest.id = id
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPRequest)(oRequest, "LogBCPAnulReq", True)
                    resultado = id.ToString()

                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.SQL
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPAnulReq", _
                            request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
                            request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
                            request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
                            request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand)).ToString()


                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL
                    'mongo
                    Dim id = ObjectId.GenerateNewId()
                    Dim oRequest As BEBCPRequest = request
                    oRequest.Ip = ""
                    oRequest.id = id
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPRequest)(oRequest, "LogBCPAnulReq", True)
                    'sql
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPAnulReq", _
                            request.Codigo, request.CodServicio, request.Moneda, request.NroOperacion, request.NroOperacionAnulada, request.Agencia, _
                            request.NroDocs, request.NDoc1, request.Mdoc1, request.NDoc2, request.Mdoc2, request.NDoc3, request.Mdoc3, request.NDoc4, request.Mdoc4, _
                            request.NDoc5, request.Mdoc5, request.NDoc6, request.Mdoc6, request.NDoc7, request.Mdoc7, request.NDoc8, request.Mdoc8, request.NDoc9, request.Mdoc9, _
                            request.NDoc10, request.Mdoc10, request.Monto, request.CodOperacion)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = id.ToString() + "-" + ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand)).ToString()
            End Select


        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBCPAnulRes(ByVal response As BEBCPResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim tipoRegistro As String = System.Configuration.ConfigurationManager.AppSettings("flagLogMongoDBSQL")
        Dim valor As String
        Dim oDbCommand As DbCommand
        Dim oResponse As BEBCPResponse = response
        Try
            Select Case tipoRegistro
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDB
                    valor = MongoDBAdapter.CollectionSeleccionarCodigo("LogBCPAnulReq", "_id", response.IdRequest, "Codigo")
                    oResponse.CIP = valor
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPResponse)(oResponse, "LogBCPAnulRes", False)
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.SQL
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPAnulRes",
                             response.IdRequest, response.Codigo, response.Descripcion1, response.Descripcion2, response.Estado, response.DescEstado)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
                Case SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL
                    'mongo
                    Dim strIdRequestM = response.IdRequest.Split("-")(0).ToString()
                    Dim strIdRequestS = response.IdRequest.Split("-")(1).ToString()
                    valor = MongoDBAdapter.CollectionSeleccionarCodigo("LogBCPAnulReq", "_id", strIdRequestM, "Codigo")
                    oResponse.IdRequest = strIdRequestM
                    oResponse.CIP = valor
                    MongoDBAdapter.CollectionInsertarGenerico(Of BEBCPResponse)(oResponse, "LogBCPAnulRes", False)
                    'sql
                    oResponse.IdRequest = strIdRequestS
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBCPAnulRes",
                             response.IdRequest, response.Codigo, response.Descripcion1, response.Descripcion2, response.Estado, response.DescEstado)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End Select
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log BBVA"
    'REGISTRAR LOG BBVA
    Public Function RegistrarLogBBVA(ByVal obelog As BELog) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarLogBBVA", obelog.IdTipo, obelog.Origen, _
                            obelog.Descripcion, obelog.Descripcion2, obelog.Parametro1, obelog.Parametro2, obelog.Parametro3, _
                            obelog.Parametro4, obelog.Parametro5, obelog.Parametro6, obelog.Parametro7, obelog.Parametro8), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        obelog.IdLog = resultado
        Return resultado
    End Function

    'CONSULTAR
    Public Function RegistrarLogBBVAConsReq(ByVal obelog As BEBBVAConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAConsReq", obelog.codigoOperacion, _
                            obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, obelog.tipocliente, obelog.codigocliente, _
                            obelog.numeroReferenciaDeuda, obelog.referenciaDeudaAdicional, obelog.canalOperacion, obelog.codigoOficina, _
                            obelog.fechaOperacion, obelog.horaOperacion, obelog.datosEmpresa)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = CType(objDatabase.ExecuteScalar(oDbCommand), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBBVAConsRes(ByVal idLogBBVAConsReq As Int64, ByVal obelog As BEBBVAConsultarResponse, _
                                                ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim numeroreferenciadocumento As String = ""
        Dim importedeudadocumento As String = ""
        Dim importedeudaminimadocumento As String = ""
        Dim fechavencimientodocumento As String = ""
        Dim fechaemisiondocumento As String = ""
        Dim descripciondocumento As String = ""
        Dim numerodocumento As String = ""
        Dim indicadorrestriccpago As String = ""
        Dim indicadorsituaciondocumento As String = ""
        Dim cantidadsubconceptos As String = ""

        If (obelog.detalle.Count > 0) Then
            Dim obedetalle As BEBBVAConsultarDetalle = obelog.detalle(0)
            numeroreferenciadocumento = obedetalle.numeroReferenciaDocumento
            importedeudadocumento = obedetalle.importeDeudaDocumento
            importedeudaminimadocumento = obedetalle.importeDeudaMinimaDocumento
            fechavencimientodocumento = obedetalle.fechaVencimientoDocumento
            fechaemisiondocumento = obedetalle.fechaEmisionDocumento
            descripciondocumento = obedetalle.descripcionDocumento
            numerodocumento = obedetalle.numeroDocumento
            indicadorrestriccpago = obedetalle.indicadorRestriccPago
            indicadorsituaciondocumento = obedetalle.indicadorSituacionDocumento
            cantidadsubconceptos = obedetalle.cantidadSubconceptos
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAConsRes", idLogBBVAConsReq, obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, _
            obelog.tipoCliente, obelog.codigoCliente, obelog.datoCliente, obelog.numeroReferenciaDeuda, obelog.cantidadDocumentos, obelog.codigoMoneda, obelog.numeroOperacionEmpresa, _
            obelog.referenciaDeudaAdicional, obelog.codigoResultado, obelog.mensajeResultado, obelog.datosEmpresa, numeroreferenciadocumento, _
            importedeudadocumento, importedeudaminimadocumento, fechavencimientodocumento, fechaemisiondocumento, _
            descripciondocumento, numerodocumento, indicadorrestriccpago, indicadorsituaciondocumento, cantidadsubconceptos, _
            oLog.Origen, oLog.Descripcion, oLog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'PAGAR
    Public Function RegistrarLogBBVAPagarReq(ByVal obelog As BEBBVAPagarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        'Dim datosEmpresa As String = ""
        Dim numeroreferenciadocumento As String = ""
        Dim descripciondocumento As String = ""
        Dim numerooperacionbanco As String = ""
        Dim importedeudapagada As String = ""
        Dim referenciapagoadicional As String = ""

        If (obelog.detalle.Count > 0) Then
            Dim obedetalle As BEBBVAPagarDetalle = obelog.detalle(0)
            numeroreferenciadocumento = obedetalle.numeroReferenciaDocumento
            descripciondocumento = obedetalle.descripcionDocumento
            numerooperacionbanco = obedetalle.numeroOperacionBanco
            importedeudapagada = obedetalle.importeDeudaPagada
            referenciapagoadicional = obedetalle.referenciaPagoAdicional
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAPagarReq", obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, obelog.cuentaRecaudadora, obelog.tipoCliente, obelog.codigoCliente, obelog.numeroReferenciaDeuda, _
                        obelog.cantidadDocumentos, obelog.formaPago, obelog.codigoMoneda, obelog.importeTotalPagado, obelog.referenciaDeudaAdicional, obelog.canalOperacion, obelog.codigoOficina, obelog.fechaOperacion, _
                        obelog.horaOperacion, obelog.datosEmpresa, numeroreferenciadocumento, descripciondocumento, numerooperacionbanco, importedeudapagada, referenciapagoadicional)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBBVAPagarRes(ByVal idlogbbvapagarreq As Int64, ByVal obelog As BEBBVAPagarResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAPagarRes", idlogbbvapagarreq, obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, obelog.tipoCliente, obelog.codigoCliente, _
            obelog.numeroReferenciaDeuda, obelog.numeroOperacionEmpresa, obelog.referenciaDeudaAdicional, obelog.codigoResultado, obelog.mensajeResultado, obelog.datosEmpresa, _
            oLog.Origen, oLog.Descripcion, oLog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteNonQuery(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function


    'ANULAR
    Public Function RegistrarLogBBVAExtReq(ByVal obelog As BEBBVAAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim numeroreferenciadocumento As String = ""
        Dim descripciondocumento As String = ""
        Dim numerooperacionbanco As String = ""
        Dim importedeudapagada As String = ""
        Dim referenciapagoadicional As String = ""

        If (obelog.detalle.Count > 0) Then
            Dim obedetalle As BEBBVAAnularDetalle = obelog.detalle(0)
            numeroreferenciadocumento = obedetalle.numeroReferenciaDocumento
            descripciondocumento = obedetalle.descripcionDocumento
            numerooperacionbanco = obedetalle.numeroOperacionBanco
            importedeudapagada = obedetalle.importeDeudaPagada
            referenciapagoadicional = obedetalle.referenciaPagoAdicional
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAExtReq", obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, obelog.cuentaRecaudadora, obelog.tipoCliente, obelog.codigoCliente, obelog.numeroReferenciaDeuda, _
                   obelog.cantidadDocumentos, obelog.formaPago, obelog.codigoMoneda, obelog.importeTotalPagado, obelog.numeroOperacionOriginal, obelog.fechaOperacionOriginal, obelog.referenciaDeudaAdicional, obelog.canalOperacion, _
                   obelog.codigoOficina, obelog.fechaOperacion, obelog.horaOperacion, obelog.datosEmpresa, numeroreferenciadocumento, descripciondocumento, numerooperacionbanco, importedeudapagada, referenciapagoadicional)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBBVAExtRes(ByVal idlogbbvaextreq As Int64, ByVal obelog As BEBBVAAnularResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAExtRes", idlogbbvaextreq, obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, _
                         obelog.tipoCliente, obelog.codigoCliente, obelog.numeroReferenciaDeuda, obelog.numeroOperacionEmpresa, _
                        obelog.referenciaDeudaAdicional, obelog.codigoResultado, obelog.mensajeResultado, obelog.datosEmpresa, _
                        oLog.Origen, oLog.Descripcion, oLog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'EXTORNO AUTOMATICO
    Public Function RegistrarLogBBVAExtAutoReq(ByVal obelog As BEBBVAExtornarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim numeroreferenciadocumento As String = ""
        Dim descripciondocumento As String = ""
        Dim numerooperacionbanco As String = ""
        Dim importedeudapagada As String = ""
        Dim referenciapagoadicional As String = ""

        If (obelog.detalle.Count > 0) Then
            Dim obedetalle As BEBBVAExtornarDetalle = obelog.detalle(0)
            numeroreferenciadocumento = obedetalle.numeroReferenciaDocumento
            descripciondocumento = obedetalle.descripcionDocumento
            numerooperacionbanco = obedetalle.numeroOperacionBanco
            importedeudapagada = obedetalle.importeDeudaPagada
            referenciapagoadicional = obedetalle.referenciaPagoAdicional
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAExtAutoReq", obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, obelog.cuentaRecaudadora, obelog.tipoCliente, obelog.codigoCliente, _
            obelog.numeroReferenciaDeuda, obelog.cantidadDocumentos, obelog.formaPago, obelog.codigoMoneda, obelog.importeTotalPagado, obelog.numeroOperacionOriginal, obelog.fechaOperacionOriginal, _
            obelog.referenciaDeudaAdicional, obelog.canalOperacion, obelog.codigoOficina, obelog.fechaOperacion, obelog.horaOperacion, obelog.datosEmpresa, numeroreferenciadocumento, _
            descripciondocumento, numerooperacionbanco, importedeudapagada, referenciapagoadicional)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBBVAExtAutoRes(ByVal idlogbbvaextautoreq As Int64, ByVal obelog As BEBBVAExtornarResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBBVAExtAutoRes", idlogbbvaextautoreq, obelog.codigoOperacion, obelog.numeroOperacion, obelog.codigoBanco, obelog.codigoConvenio, obelog.tipoCliente, _
                        obelog.codigoCliente, obelog.numeroReferenciaDeuda, obelog.numeroOperacionEmpresa, obelog.referenciaDeudaAdicional, _
                        obelog.codigoResultado, obelog.mensajeResultado, obelog.datosEmpresa, _
                        oLog.Origen, oLog.Descripcion, oLog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log FullCarga"

    Public Function RegistrarLogFCConsReq(ByVal obelog As BEFCConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCConsReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionFullCarga, obelog.CIP, obelog.CodServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogFCConsRes(ByVal idLogFCConsReq As Int64, ByVal response As BEFCConsultarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCConsRes", response.CIP, _
                response.CodResultado, response.MensajeResultado, response.Monto, response.CodMoneda, response.ConceptoPago, _
                response.FechaVencimiento, response.CodServicio, idLogFCConsReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogFCCancReq(ByVal obelog As BEFCCancelarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCCancReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionFullCarga, obelog.CodMedioPago, obelog.CIP, obelog.Monto, obelog.CodMoneda, obelog.CodServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogFCCancRes(ByVal idLogFCCancReq As Int64, ByVal response As BEFCCancelarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCCancRes", response.CodResultado, _
                response.MensajeResultado, response.CodMovimiento, response.CodServicio, idLogFCCancReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogFCAnulReq(ByVal obelog As BEFCAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCAnulReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionFullCargaPago, obelog.NroOperacionFullCarga, obelog.CodMedioPago, _
                obelog.CIP, obelog.Monto, obelog.CodMoneda, obelog.CodServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogFCAnulRes(ByVal idLogFCAnulReq As Int64, ByVal response As BEFCAnularResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCAnulRes", response.CodResultado, _
                response.MensajeResultado, response.CodMovimiento, response.CodServicio, idLogFCAnulReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log LIFE!"

    Public Function RegistrarLogLFConsReq(ByVal obelog As BELFConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogLFConsReq", obelog.CIP, obelog.CodServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogLFConsRes(ByVal idLogLFConsReq As Int64, ByVal response As BELFConsultarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogLFConsRes", response.CIP, _
                response.CodResultado, response.MensajeResultado, response.Monto, response.CodMoneda, response.Dni, _
                response.FechaVencimiento, response.CodServicio, idLogLFConsReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogLFCancReq(ByVal obelog As BELFCancelarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogLFCancReq", obelog.CIP, obelog.Monto, obelog.CodMoneda, obelog.CodServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogLFCancRes(ByVal idLogLFCancReq As Int64, ByVal response As BELFCancelarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogLFCancRes", response.CodResultado, _
                response.MensajeResultado, response.CodMovimiento, response.CodServicio, idLogLFCancReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogLFAnulReq(ByVal obelog As BELFAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCAnulReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionFullCargaPago, obelog.NroOperacionFullCarga, obelog.CodMedioPago, _
                obelog.CIP, obelog.Monto, obelog.CodMoneda, obelog.CodServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogLFAnulRes(ByVal idLogLFAnulReq As Int64, ByVal response As BELFAnularResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogFCAnulRes", response.CodResultado, _
                response.MensajeResultado, response.CodMovimiento, response.CodServicio, idLogLFAnulReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log Scotiabank"

    Public Function RegistrarLogSBKConsReq(ByVal request As BESBKConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With request
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKConsReq", .IP, .Trama, .MessageTypeIdentification, _
                    .PrimaryBitMap, .SecondaryBitMap, .NumeroTarjeta, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, .ModoIngresoCampos, _
                    .Canal, .BinAdquiriente, .ForwardInstitutionCode, .RetrievalReferenceNumber, .TerminalID, .Comercio, .CardAcceptorLocation, _
                    .TransactionCurrencyCode, .DatosReservados, .LongitudRequerimiento, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, _
                    .CodigoPlazaRecaudador, .CodigoAgenciaRecaudador, .TipoDatoConsulta, .DatoConsulta, .CodigoCiudad, .CodigoServicio, .NumeroDocumento, _
                    .NumeroOperacion, .Filler, .TamanoMaximoBloque, .PosicionUltimoDocumento, .PunteroBaseDatos)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKConsRes(ByVal idLogSBKConsReq As Int64, ByVal response As BESBKConsultarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With response
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKConsRes", idLogSBKConsReq, .ToString(), _
                    .MessageTypeIdentification, .PrimaryBitMap, .SecondaryBitMap, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, _
                    .BinAdquiriente, .RetrievalReferenceNumber, .AuthorizationIDResponse, .ResponseCode, .TerminalID, .TransactionCurrencyCode, _
                    .DatosReservados, .CabResLongitudCampo, .CabResCodigoFormato, .CabResBinProcesador, .CabResBinAcreedor, .CabResCodigoProductoServicio, _
                    .CabResAgencia, .CabResTipoIdentificacion, .CabResNumeroIdentificacion, .CabResNombreDeudor, .CabResNumeroServiciosDevueltos, _
                    .CabResNumeroOperacionCobranza, .CabResIndicadorSiHayDocumentos, .CabResTamanoMaximoBloque, .CabResPosicionUltimoDocumento, _
                    .CabResPunteroBaseDatos, .CabResOrigenRespuesta, .CabResCodigoRespuesta, .CabResFiller, .DetResCabSerCodigoProductoServicio, _
                    .DetResCabSerMoneda, .DetResCabSerEstadoDeudor, .DetResCabSerMensaje1Deudor, .DetResCabSerMensaje2Deudor, .DetResCabSerIndicadorCronologia, _
                    .DetResCabSerIndicadorPagosVencidos, .DetResCabSerRestriccionPago, .DetResCabSerDocumentosServicio, .DetResCabSerFiller, _
                    .DetResDetSerTipoServicio, .DetResDetSerNumeroDocumento, .DetResDetSerReferenciaDeuda, .DetResDetSerFechaVencimiento, _
                    .DetResDetSerImporteMinimo, .DetResDetSerImporteTotal, oBELog.Descripcion, oBELog.Descripcion2)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKCancReq(ByVal request As BESBKCancelarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With request
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKCancReq", .IP, .Trama, .MessageTypeIdentification, _
                    .PrimaryBitMap, .SecondaryBitMap, .NumeroTarjeta, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, .ModoIngresoDatos, _
                    .Canal, .BinAdquiriente, .ForwardInstitutionCode, .RetrievalReferenceNumber, .TerminalID, .Comercio, .CardAcceptorLocation, _
                    .TransactionCurrencyCode, .DatosReservados, .LongitudTrama, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, _
                    .CodigoPlazaRecaudador, .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .NumeroProductoServicioPagado, _
                    .NumeroTotalDocumentosPagados, .Filler, .MedioPago, .ImportePagadoEfectivo, .ImportePagoConCuenta, .NumeroCheque1, _
                    .BancoGirador1, .ImporteCheque1, .PlazaCheque1, .NumeroCheque2, .BancoGirador2, .ImporteCheque2, .PlazaCheque2, .NumeroCheque3, _
                    .BancoGirador3, .ImporteCheque3, .PlazaCheque3, .MonedaPago, .TipoCambioAplicado, .PagoTotalRealizado, .Filler2, .CodigoServicioPagado, _
                    .EstadoDeudor, .ImporteTotalProductoServicio, .NumeroCuentaAbono, .NumeroReferenciaAbono, .NumeroDocumentosPagados, .Filler3, _
                    .TipoDocumentoPago, .NumeroDocumentoPago, .PeriodoCotizacion, .TipoDocumentoIDDeudor, .NumeroDocumentoIDDeudor, .ImporteOriginalDeuda, _
                    .ImportePagadoDcumento, .CodigoConcepto1, .ImporteConcepto1, .CodigoConcepto2, .ImporteConcepto2, .CodigoConcepto3, .ImporteConcepto3, _
                    .CodigoConcepto4, .ImporteConcepto4, .CodigoConcepto5, .ImporteConcepto5, .ReferenciaDeuda, .Filler4)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKCancRes(ByVal idLogSBKCancReq As Int64, ByVal response As BESBKCancelarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With response
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKCancRes", idLogSBKCancReq, .ToString(), _
                    .MessageTypeIdentification, .PrimaryBitMap, .SecondaryBitMap, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, _
                    .IdentificacionEmpresa, .RetrievalReferenceNumber, .AuthorizationIDResponse, .ResponseCode, .TerminalID, .TransactionCurrencyCode, _
                    .DatosReservados, .TamanoBloque, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, .CodigoPlazaRecaudador, _
                    .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .NumeroOperacionCobranza, .NumeroOperacionAcreedor, _
                    .NumeroProductoServicioPagado, .NumeroTotalDocumentosPagados, .Filler, .OrigenRespuesta, .CodigoRespuestaExtendida, _
                    .DescripcionRespuestaAplicativa, .NombreDeudor, .RUCDeudor, .RUCAcreedor, .CodigoZonaDeudor, .Filler2, .CodigoProductoServicio2, _
                    .DescripcionProductoServicio, .ImporteTotalProductoServicio, .Mensaje1, .Mensaje2, .NumeroDocumentos, .Filler3, .TipoServicio, _
                    .DescripcionDocumento, .NumeroDocumento, .PeriodoCotizacion, .TipoDocumentoIdentidad, .NumeroDocumentoIdentidad, .FechaEmision, _
                    .FechaVencimiento, .ImportePagado, .CodigoConcepto1, .ImporteConcepto1, .CodigoConcepto2, .ImporteConcepto2, .CodigoConcepto3, _
                    .ImporteConcepto3, .CodigoConcepto4, .ImporteConcepto4, .CodigoConcepto5, .ImporteConcepto5, .IndicadorFacturacion, .NumeroFactura, _
                    .ReferenciaDeuda, .Filler4, oBELog.Descripcion, oBELog.Descripcion2)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKAnulReq(ByVal request As BESBKAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            With request
                resultado = ObjectToInt64(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarLogSBKAnulReq", .IP, .Trama, .MessageTypeIdentification, _
                    .PrimaryBitMap, .SecondaryBitMap, .NumeroTarjeta, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, .ModoIngresoDatos, _
                    .Canal, .BinAdquiriente, .ForwardInstitutionCode, .RetrievalReferenceNumber, .TerminalID, .Comercio, .CardAcceptorLocation, _
                    .TransactionCurrencyCode, .OriDatEleMessageTypeIdentification, .OriDatEleTrace, .OriDatEleFechaHoraTransaccion, .OriDatEleBinAdquiriente, _
                    .OriDatEleForwardInstitutionCode, .DatosReservados, .LongitudDato, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, _
                    .CodigoPlazaRecaudador, .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .Filler, .TipoServicio, .NumeroDocumento, _
                    .Disponible, .NumeroTransaccionesCobrOri, .NumeroOperacionesOriginalAcreedor))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKAnulRes(ByVal idLogSBKAnulReq As Int64, ByVal response As BESBKAnularResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            With response
                resultado = ObjectToInt64(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarLogSBKAnulRes", idLogSBKAnulReq, .ToString, _
                    .MessageTypeIdentification, .PrimaryBitMap, .SecondaryBitMap, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, _
                    .IdentificacionEmpresa, .RetrievalReferenceNumber, .AuthorizationIDResponse, .ResponseCode, .TerminalID, .TransactionCurrencyCode, _
                    .DatosReservados, .LongitudTrama, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, .CodigoPlazaRecaudador, _
                    .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .NombreCliente, .RUCDeudor, .RUCAcreedor, .NumeroTransaccionesCobrOri, _
                    .NumeroOperacionesOriginalAcreedor, .Filler, .OrigenRespuesta, .CodigoRespuestaExtendida, .DescripcionRespuestaAplicativa, _
                    .CodigoProductoServicio2, .DescripcionProductoServicio, .ImporteProductoServicio, .Mensaje1Marketing, .Mensaje2Marketing, _
                    .NumeroDocumentos, .Filler2, .TipoDocumentoServicio, .DescripcionDocumento, .NumeroDocumento, .PeriodoCotizacion, .TipoDocumentoIdentidad, _
                    .NumeroDocumentoIdentidad, .FechaEmision, .FechaVencimiento, .ImporteAnuladoDocumento, .CodigoConcepto1, .ImporteConcepto1, _
                    .CodigoConcepto2, .ImporteConcepto2, .CodigoConcepto3, .ImporteConcepto3, .CodigoConcepto4, .ImporteConcepto4, .CodigoConcepto5, _
                    .ImporteConcepto5, .IndicadorComprobante, .NumeroFacturaAnulada, .ReferenciaDeuda, .Filler3, oBELog.Descripcion, oBELog.Descripcion2))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKExtCancReq(ByVal request As BESBKExtornarCancelacionRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With request
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKExtCancReq", .IP, .Trama, .MessageTypeIdentification, _
                .PrimaryBitMap, .SecondaryBitMap, .NumeroTarjeta, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, .ModoIngresoDatos, _
                .Canal, .BinAdquiriente, .ForwardInstitutionCode, .RetrievalReferenceNumber, .ResponseCode, .TerminalID, .Comercio, .CardAcceptorLocation, _
                .TransactionCurrencyCode, .OriDatEleMessageTypeIdentification, .OriDatEleTrace, .OriDatEleFechaTransaction, .OriDatEleBinAdquiriente, _
                .OriDatEleForwardInstitutionCode, .DatosReservados, .LongitudTrama, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, _
                .CodigoPlazaRecaudador, .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .NumeroProductoServicioPagado, .NumeroTotalDocumentosPagados, _
                .Filler, .MedioPago, .ImportePagadoEfectivo, .ImportePagadoConCuenta, .NumeroCheque1, .BancoGirador1, .ImporteCheque1, .PlazaCheque1, .NumeroCheque2, _
                .BancoGirador2, .ImporteCheque2, .PlazaCheque2, .NumeroCheque3, .BancoGirador3, .ImporteCheque3, .PlazaCheque3, .MonedaPago, _
                .TipoCambioAplicado, .PagoTotalRealizado, .Filler2, .CodigoServicioPagado, .EstadoDeudor, .ImporteTotalProductoServicio, .NumeroCuentaAbono, _
                .NumeroReferenciaAbono, .NumeroDocumentosPagados, .Filler3, .TipoDocumentoPago, .NumeroDocumentoPago, .PeriodoCotizacion, .TipoDocumentoIDDeudor, _
                .NumeroDocumentoIDDeudor, .ImporteOriginalDeuda, .ImportePagadoDocumento, .CodigoConcepto1, .ImporteConcepto1, .CodigoConcepto2, _
                .ImporteConcepto2, .CodigoConcepto3, .ImporteConcepto3, .CodigoConcepto4, .ImporteConcepto4, .CodigoConcepto5, .ImporteConcepto5, _
                .ReferenciaDeuda, .Filler4)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKExtCancRes(ByVal idLogSBKExtCancReq As Int64, ByVal response As BESBKExtornarCancelacionResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With response
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKExtCancRes", idLogSBKExtCancReq, .ToString, _
                    .MessageTypeIdentification, .PrimaryBitMap, .SecondaryBitMap, .CodigoProceso, .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, _
                    .IdentificacionEmpresa, .RetrievalReferenceNumber, .AuthorizationIDResponse, .ResponseCode, .TerminalID, .TransactionCurrencyCode, _
                    .DatosReservados, .TamanoBloque, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, .CodigoPlazaRecaudador, _
                    .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .NumeroOperacionCobranza, .NumeroOperacionAcreedor, _
                    .NumeroProductoServicioPagado, .NumeroTotalDocumentosPagados, .Filler, .OrigenRespuesta, .CodigoRespuestaExtendida, _
                    .DescripcionRespuestaAplicativa, .NombreDeudor, .RUCDeudor, .RUCAcreedor, .CodigoZonaDeudor, .Filler2, .CodigoProductoServicio2, _
                    .DescripcionProductoServicio, .ImporteTotalProductoServicio, .Mensaje1, .Mensaje2, .NumeroDocumentos, .Filler3, .TipoServicio, _
                    .DescripcionDocumento, .NumeroDocumento, .PeriodoCotizacion, .TipoDocumentoIdentidad, .NumeroDocumentoIdentidad, .FechaEmision, _
                    .FechaVencimiento, .ImportePagado, .CodigoConcepto1, .ImporteConcepto1, .CodigoConcepto2, .ImporteConcepto2, .CodigoConcepto3, _
                    .ImporteConcepto3, .CodigoConcepto4, .ImporteConcepto4, .CodigoConcepto5, .ImporteConcepto5, .IndicadorFacturacion, .NumeroFactura, _
                    .ReferenciaDeuda, .Filler4, oBELog.Descripcion, oBELog.Descripcion2)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKExtAnulReq(ByVal request As BESBKExtornarAnulacionRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With request
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKExtAnulReq", .IP, .Trama, _
                    .MessageTypeIdentification, .PrimaryBitMap, .SecondaryBitMap, .NumeroTarjeta, .CodigoProceso, .Monto, _
              .FechaHoraTransaccion, .Trace, .FechaCaptura, .ModoIngresoDatos, .Canal, .BinAdquiriente, .ForwardInstitutionCode, _
              .RetrievalReferenceNumber, .ResponseCode, .TerminalID, .Comercio, .CardAcceptorLocation, .TransactionCurrencyCode, _
              .OriDatEleMessageTypeIdentification, .OriDatEleTrace, .OriDatEleFechaHoraTransaccion, .OriDatEleBinAdquiriente, _
              .OriDatEleForwardInstitutionCode, .DatosReservados, .LongitudDato, .CodigoFormato, .BinProcesador, .CodigoAcreedor, _
              .CodigoProductoServicio, .CodigoPlazaRecaudador, .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, _
              .Filler, .TipoServicio, .NumeroDocumento, .Disponible, .NumeroTransaccionesCobrOri, .NumeroOperacionesOriginalAcreedor)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogSBKExtAnulRes(ByVal idLogSBKExtAnulReq As Int64, ByVal response As BESBKExtornarAnulacionResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            With response
                oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogSBKExtAnulRes", idLogSBKExtAnulReq, _
                    .ToString, .MessageTypeIdentification, .PrimaryBitMap, .SecondaryBitMap, .CodigoProceso, _
                    .Monto, .FechaHoraTransaccion, .Trace, .FechaCaptura, .IdentificacionEmpresa, .RetrievalReferenceNumber, _
                    .AuthorizationIDResponse, .ResponseCode, .TerminalID, .TransactionCurrencyCode, .DatosReservados, _
                    .LongitudTrama, .CodigoFormato, .BinProcesador, .CodigoAcreedor, .CodigoProductoServicio, .CodigoPlazaRecaudador, _
                    .CodigoAgenciaRecaudador, .TipoDatoPago, .DatoPago, .CodigoCiudad, .NombreCliente, .RUCDeudor, .RUCAcreedor, _
                    .NumeroTransaccionesCobrOri, .NumeroOperacionesOriginalAcreedor, .Filler, .OrigenRespuesta, .CodigoRespuestaExtendida, _
                    .DescripcionRespuestaAplicativa, .CodigoProductoServicio2, .DescripcionProductoServicio, .ImporteProductoServicio, _
                    .Mensaje1Marketing, .Mensaje2Marketing, .NumeroDocumentos, .Filler2, .TipoDocumentoServicio, .DescripcionDocumento, _
                    .NumeroDocumento, .PeriodoCotizacion, .TipoDocumentoIdentidad, .NumeroDocumentoIdentidad, .FechaEmision, .FechaVencimiento, _
                    .ImporteAnuladoDocumento, .CodigoConcepto1, .ImporteConcepto1, .CodigoConcepto2, .ImporteConcepto2, _
                    .CodigoConcepto3, .ImporteConcepto3, .CodigoConcepto4, .ImporteConcepto4, .CodigoConcepto5, .ImporteConcepto5, _
                    .IndicadorComprobante, .NumeroFacturaAnulada, .ReferenciaDeuda, .Filler3, oBELog.Descripcion, oBELog.Descripcion2)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log Interbank"

    Public Function RegistrarLogIBKConsReq(ByVal request As BEIBKConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKConsReq", request.IP, request.Trama, request.MessageTypeIdentification, _
                    request.PrimaryBitMap, request.SecondaryBitMap, request.PrimaryAccountNumber, request.ProcessingCode, request.AmountTransaction, request.Trace, request.TimeLocalTransaction, _
                    request.DateLocalTransaction, request.POSEntryMode, request.POSConditionCode, request.AcquirerInstitutionIDCode, request.ForwardInstitutionIDCode, request.RetrievalReferenceNumber, _
                    request.CardAcceptorTerminalID, request.CardAcceptorIDCode, request.CardAcceptorNameLocation, request.TransactionCurrencyCode, request.LongitudCampo, request.CIP, request.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKConsRes(ByVal idLogIBKConsReq As Int64, ByVal response As BEIBKConsultarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKConsRes", idLogIBKConsReq, response.ToString(), _
                    response.MessageTypeIdentification, response.PrimaryBitMap, response.SecondaryBitMap, response.PrimaryAccountNumber, response.ProcessingCode, response.AmountTransaction, _
                    response.Trace, response.TimeLocalTransaction, response.DateLocalTransaction, response.POSEntryMode, response.POSConditionCode, response.AcquirerInstitutionIDCode, _
                    response.ForwardInstitutionIDCode, response.RetrievalReferenceNumber, response.ApprovalCode, response.ResponseCode, response.CardAcceptorTerminalID, _
                    response.TransactionCurrencyCode, response.LongitudCampo, response.CIP, response.CodigoServicio, response.CodigoRetorno, response.DescripcionCodigoRetorno, response.NombreRazonSocial, _
                    oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKCancReq(ByVal request As BEIBKCancelarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKCancReq", request.IP, request.Trama, request.MessageTypeIdentification, _
                    request.PrimaryBitMap, request.SecondaryBitMap, request.PrimaryAccountNumber, request.ProcessingCode, request.AmountTransaction, request.Trace, request.TimeLocalTransaction, request.DateLocalTransaction, _
                    request.POSEntryMode, request.POSConditionCode, request.AcquirerInstitutionIDCode, request.ForwardInstitutionIDCode, request.RetrievalReferenceNumber, request.CardAcceptorTerminalID, _
                    request.CardAcceptorIDCode, request.CardAcceptorNameLocation, request.TransactionCurrencyCode, request.LongitudCampo, request.CIP, request.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            ' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKCancRes(ByVal idLogIBKCancReq As Int64, ByVal response As BEIBKCancelarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKCancRes", idLogIBKCancReq, response.ToString(), _
                    response.MessageTypeIdentification, response.PrimaryBitMap, response.SecondaryBitMap, response.PrimaryAccountNumber, response.ProcessingCode, response.AmountTransaction, _
                    response.Trace, response.TimeLocalTransaction, response.DateLocalTransaction, response.POSEntryMode, response.POSConditionCode, response.AcquirerInstitutionIDCode, _
                    response.ForwardInstitutionIDCode, response.RetrievalReferenceNumber, response.ApprovalCode, response.ResponseCode, response.CardAcceptorTerminalID, _
                    response.TransactionCurrencyCode, response.LongitudCampo, response.CIP, response.CodigoServicio, response.CodigoRetorno, response.DescripcionCodigoRetorno, _
                    oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            ' With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            ' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKAnulReq(ByVal request As BEIBKAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKAnulReq", request.IP, request.Trama, request.MessageTypeIdentification, _
                    request.PrimaryBitMap, request.SecondaryBitMap, request.PrimaryAccountNumber, request.ProcessingCode, request.AmountTransaction, request.Trace, request.TimeLocalTransaction, _
                    request.DateLocalTransaction, request.POSEntryMode, request.POSConditionCode, request.AcquirerInstitutionIDCode, request.ForwardInstitutionIDCode, request.RetrievalReferenceNumber, _
                    request.ApprovalCode, request.CardAcceptorTerminalID, request.CardAcceptorIDCode, request.CardAcceptorNameLocation, request.TransactionCurrencyCode, request.LongitudCampo, _
                    request.CIP, request.CodigoServicio, request.NumeroOperacionPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            ' With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            ' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKAnulRes(ByVal idLogIBKAnulReq As Int64, ByVal response As BEIBKAnularResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKAnulRes", idLogIBKAnulReq, response.ToString, _
                    response.MessageTypeIdentification, response.PrimaryBitMap, response.SecondaryBitMap, response.PrimaryAccountNumber, response.ProcessingCode, response.AmountTransaction, _
                    response.Trace, response.TimeLocalTransaction, response.DateLocalTransaction, response.POSEntryMode, response.POSConditionCode, response.AcquirerInstitutionIDCode, _
                    response.ForwardInstitutionIDCode, response.RetrievalReferenceNumber, response.ApprovalCode, response.ResponseCode, response.CardAcceptorTerminalID, response.TransactionCurrencyCode, _
                    response.LongitudCampo, response.CIP, response.CodigoServicio, response.CodigoRetorno, response.DescripcionCodigoRetorno, oBELog.Descripcion, _
                    oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKExtCancReq(ByVal request As BEIBKExtornarCancelacionRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKExtCancReq", request.IP, request.Trama, request.MessageTypeIdentification, _
                request.PrimaryBitMap, request.SecondaryBitMap, request.PrimaryAccountNumber, request.ProcessingCode, request.AmountTransaction, request.Trace, request.TimeLocalTransaction, request.DateLocalTransaction, _
                request.POSEntryMode, request.POSConditionCode, request.AcquirerInstitutionIDCode, request.ForwardInstitutionIDCode, request.RetrievalReferenceNumber, request.CardAcceptorTerminalID, _
                request.CardAcceptorIDCode, request.CardAcceptorNameLocation, request.TransactionCurrencyCode, request.LongitudCampo, request.CIP, request.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKExtCancRes(ByVal idLogIBKExtCancReq As Int64, ByVal response As BEIBKExtornarCancelacionResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKExtCancRes", idLogIBKExtCancReq, response.ToString, _
                    response.MessageTypeIdentification, response.PrimaryBitMap, response.SecondaryBitMap, response.PrimaryAccountNumber, response.ProcessingCode, response.AmountTransaction, _
                    response.Trace, response.TimeLocalTransaction, response.DateLocalTransaction, response.POSEntryMode, response.POSConditionCode, response.AcquirerInstitutionIDCode, _
                    response.ForwardInstitutionIDCode, response.RetrievalReferenceNumber, response.ApprovalCode, response.ResponseCode, response.CardAcceptorTerminalID, _
                    response.TransactionCurrencyCode, response.LongitudCampo, response.CIP, response.CodigoServicio, response.CodigoRetorno, response.DescripcionCodigoRetorno, _
                    oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            ' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKExtAnulReq(ByVal request As BEIBKExtornarAnulacionRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKExtAnulReq", request.IP, request.Trama, _
                    request.MessageTypeIdentification, request.PrimaryBitMap, request.SecondaryBitMap, request.PrimaryAccountNumber, request.ProcessingCode, _
                    request.AmountTransaction, request.Trace, request.TimeLocalTransaction, request.DateLocalTransaction, request.POSEntryMode, request.POSConditionCode, _
                    request.AcquirerInstitutionIDCode, request.ForwardInstitutionIDCode, request.RetrievalReferenceNumber, request.ApprovalCode, _
                    request.CardAcceptorTerminalID, request.CardAcceptorIDCode, request.CardAcceptorNameLocation, request.TransactionCurrencyCode, _
                    request.LongitudCampo, request.CIP, request.CodigoServicio, request.NumeroOperacionPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogIBKExtAnulRes(ByVal idLogIBKExtAnulReq As Int64, ByVal response As BEIBKExtornarAnulacionResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogIBKExtAnulRes", idLogIBKExtAnulReq, _
                    response.ToString, response.MessageTypeIdentification, response.PrimaryBitMap, response.SecondaryBitMap, response.PrimaryAccountNumber, response.ProcessingCode, _
                    response.AmountTransaction, response.Trace, response.TimeLocalTransaction, response.DateLocalTransaction, response.POSEntryMode, response.POSConditionCode, _
                    response.AcquirerInstitutionIDCode, response.ForwardInstitutionIDCode, response.RetrievalReferenceNumber, response.ApprovalCode, response.ResponseCode, _
                    response.CardAcceptorTerminalID, response.TransactionCurrencyCode, response.LongitudCampo, response.CIP, response.CodigoServicio, response.CodigoRetorno, _
                    response.DescripcionCodigoRetorno, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With 
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log Western Union"
    Public Function RegistrarLogWUConsReq(ByVal request As BEWUConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUConsReq", _
                   request.HdReq.algoritmo, request.HdReq.cajero, request.HdReq.codTrx, request.HdReq.fechaHora, request.HdReq.idMensaje, request.HdReq.marca, _
                   request.HdReq.nroSecuencia, request.HdReq.plataforma, request.HdReq.puesto, request.HdReq.supervisor, request.HdReq.terminal, _
                   request.HdReq.version, request.HdReq.versionAutorizador, request.Utility, request.Cod_barra, request.Codigo_cliente)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            With request
                resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogWUConsRes(ByVal idLogWUConsReq As Int64, ByVal response As BEWUConsultarResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUConsRes", idLogWUConsReq, response.ObjBEBK.header.algoritmo, _
                    response.ObjBEBK.header.cajero, response.ObjBEBK.header.codError, response.ObjBEBK.header.codSeveridad, response.ObjBEBK.header.fechaHora, response.ObjBEBK.header.idMensaje, response.ObjBEBK.header.marca, _
                    response.ObjBEBK.header.nroSecuencia, response.ObjBEBK.header.terminal, response.ObjBEBK.header.version, response.ObjBEBK.cob_cliente_nomb, response.ObjBEBK.count, response.ObjBEBK.seleccion_con_prioridad, _
                    response.ObeLog.Descripcion, response.ObeLog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            ' With response.ObjBEBK
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            ' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogWUConsResDet(ByVal idLogWUConsRes As Int64, ByVal response As BEWUConsultarResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try

            If response.ObjBEBK IsNot Nothing Then
                For Each item As ArrayFieldsQuerie In response.ObjBEBK.fields
                    'M
                    Dim oDbCommand As DbCommand
                    oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUConsResDet", idLogWUConsRes, item.cob_cod_barra, _
                            item.cob_prior_gpo, item.cob_prior_nro, item.cob_texto_fe, item.cob_estado, item.cob_cobro_tipo, item.cob_comp_imp, _
                            item.numero_de_orden, response.BarCodeObject.Moneda, response.BarCodeObject.CodigoCliente.Trim.PadLeft(15, "0").Substring(1, 14), response.BarCodeObject.Importe)
                    oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    'End M
                    'With item
                    objDatabase.ExecuteScalar(oDbCommand)
                    'End With
                Next
            End If
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogWUCancReq(ByVal request As BEWUCancelarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUCancReq",
                    request.HdReq.algoritmo, request.HdReq.cajero, request.HdReq.codTrx, request.HdReq.fechaHora, request.HdReq.idMensaje, request.HdReq.marca, _
                    request.HdReq.nroSecuencia, request.HdReq.plataforma, request.HdReq.puesto, request.HdReq.supervisor, request.HdReq.terminal, _
                    request.HdReq.version, request.HdReq.versionAutorizador, request.Utility, request.BarCode, request.MedioPago, request.CreditCard, request.Amount, _
                    request.Check.accountID, request.Check.amount, request.Check.bankBranch, request.Check.bankID, request.Check.bankSquare, request.Check.checkNumber, _
                    request.Check.expiration, request.BarCodeObject.CodigoCliente.Trim.PadLeft(15, "0").Substring(1, 14))
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            ' With request
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            ' End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogWUCancRes(ByVal idLogWUCancReq As Int64, ByVal response As BEWUCancelarResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUCancRes", idLogWUCancReq, response.ObjBEBK.header.algoritmo, _
                    response.ObjBEBK.header.cajero, response.ObjBEBK.header.codError, response.ObjBEBK.header.codSeveridad, response.ObjBEBK.header.fechaHora, response.ObjBEBK.header.idMensaje, response.ObjBEBK.header.marca, response.ObjBEBK.header.nroSecuencia, _
                    response.ObjBEBK.header.terminal, response.ObjBEBK.header.version, response.ObjBEBK.msg, response.ObeLog.Descripcion, response.ObeLog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With response.ObjBEBK
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogWUAnulReq(ByVal request As BEWUAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUAnulReq", _
                    request.HdReq.algoritmo, request.HdReq.cajero, request.HdReq.codTrx, request.HdReq.fechaHora, request.HdReq.idMensaje, request.HdReq.marca, _
                    request.HdReq.nroSecuencia, request.HdReq.plataforma, request.HdReq.puesto, request.HdReq.supervisor, request.HdReq.terminal, _
                    request.HdReq.version, request.HdReq.versionAutorizador, request.TerminalOriginal, request.CajeroOriginal, request.FechaHoraOriginal, _
                    request.NroSecuenciaOriginal, request.TipoReversa, request.Utility, request.Amount)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With request
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogWUAnulRes(ByVal idLogWUAnulReq As Int64, ByVal response As BEWUAnularResponse) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogWUAnulRes", idLogWUAnulReq, response.ObjBEBK.header.algoritmo, _
                    response.ObjBEBK.header.cajero, response.ObjBEBK.header.codError, response.ObjBEBK.header.codSeveridad, response.ObjBEBK.header.fechaHora, response.ObjBEBK.header.idMensaje, response.ObjBEBK.header.marca, response.ObjBEBK.header.nroSecuencia, _
                    response.ObjBEBK.header.terminal, response.ObjBEBK.header.version, response.ObjBEBK.estado, response.ObjBEBK.operador, response.ObjBEBK.ticket, response.ObeLog.Descripcion, response.ObeLog.Descripcion2, response.CIP)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            'With response.ObjBEBK
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            'End With
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function
#End Region

#Region "Log BanBif"

    'CONSULTAR
    Public Function RegistrarLogBanBifConsReq(ByVal obelog As BEBanBifConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifConsReq", obelog.RqUID, _
                            obelog.Login, obelog.SegRol, obelog.OpnNro, obelog.OrgCod, _
                            obelog.ClientCod, obelog.SvcId, obelog.Spcod, obelog.PagId, _
                            obelog.Clasificacion)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = CType(objDatabase.ExecuteScalar(oDbCommand), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBanBifConsRes(ByVal idLogBanBifConsReq As Int64, ByVal obelog As BEBanBifConsultarResponse, _
                                                ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim numeroreferencia As String = ""
        Dim codigoServicio As String = ""
        Dim DescripcionRecibo As String = ""
        Dim fechaVencimientoDocumento As String = ""
        Dim fechaemisiondocumento As String = ""
        Dim CodigoMoneda As String = ""
        Dim ImporteTotal As Decimal = 0
        Dim CantidadImportes As Int16 = 1

        If Not (obelog.detalle Is Nothing) Then


            If (obelog.detalle.Count > 0) Then
                Dim obedetalle As BEBanBifConsultarDetalle = obelog.detalle(0)
                numeroreferencia = obedetalle.Recibo
                codigoServicio = obedetalle.SvcId
                DescripcionRecibo = obedetalle.Descripcion
                fechaVencimientoDocumento = obedetalle.FecVct
                fechaemisiondocumento = obedetalle.FecEmision
                CodigoMoneda = obedetalle.MonCod
                ImporteTotal = obedetalle.Total
                CantidadImportes = obedetalle.Ctd

            End If
        End If
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifConsRes", idLogBanBifConsReq, obelog.StatusCod, obelog.Severidad, obelog.ServStatusCod, obelog.StatusDes, _
            obelog.RqUID, obelog.Login, obelog.SegRol, obelog.Spcod, obelog.PagId, obelog.NomCompleto, _
            obelog.Clasificacion, obelog.Ctd, obelog.OpnNro, numeroreferencia, codigoServicio, _
            DescripcionRecibo, fechaVencimientoDocumento, fechaemisiondocumento, _
            CodigoMoneda, ImporteTotal, numeroreferencia, ImporteTotal)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'PAGAR
    Public Function RegistrarLogBanBifPagarReq(ByVal obelog As BEBanBifPagarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifPagarReq", obelog.RqUID, obelog.Login, obelog.SegRol, obelog.ExtornoTp, obelog.OpnNro, obelog.OrgCod, obelog.ClientCod, _
                        obelog.Ctd, obelog.SvcId, obelog.Spcod, obelog.PagId, obelog.Recibo, obelog.Clasificacion, obelog.MedioAbonoCod, obelog.Moneda, obelog.Mto)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBanBifPagarRes(ByVal idlogBanBifpagarreq As Int64, ByVal obelog As BEBanBifPagarResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifPagoRes", idlogBanBifpagarreq, obelog.StatusCod, obelog.Severidad, obelog.ServStatusCod, obelog.StatusDes, obelog.ExtornoTp, obelog.RqUID, _
            obelog.Login, obelog.SegRol, obelog.OpnNro, obelog.Ctd, obelog.SvcId, obelog.Spcod, obelog.PagId, obelog.Recibo, obelog.Clasificacion, obelog.Moneda, obelog.Mto)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteNonQuery(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'ANULAR
    Public Function RegistrarLogBanBifExtornarReq(ByVal obelog As BEBanBifExtornoRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifExtornarReq", obelog.RqUID, obelog.Login, obelog.SegRol, obelog.ExtornoTp, obelog.OpnNro, obelog.OrgCod, obelog.ClientCod, _
                        obelog.Ctd, obelog.SvcId, obelog.Spcod, obelog.PagId, obelog.Recibo, obelog.Clasificacion, obelog.MedioAbonoCod, obelog.Moneda, obelog.Mto)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBanBifExtornarRes(ByVal idlogBanBifpagarreq As Int64, ByVal obelog As BEBanBifExtornoResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifPagarRes", idlogBanBifpagarreq, obelog.StatusCod, obelog.Severidad, obelog.ServStatusCod, obelog.StatusDes, obelog.ExtornoTp, obelog.RqUID, _
            obelog.Login, obelog.SegRol, obelog.OpnNro, obelog.Ctd, obelog.SvcId, obelog.Spcod, obelog.PagId, obelog.Recibo, obelog.Clasificacion, obelog.Moneda, obelog.Mto)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteNonQuery(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'EXTORNO AUTOMATICO
    Public Function RegistrarLogBanBifExtornarAutomaticoReq(ByVal obelog As BEBanBifExtornoRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifExtornarReq", obelog.RqUID, obelog.Login, obelog.SegRol, obelog.ExtornoTp, obelog.OpnNro, obelog.OrgCod, obelog.ClientCod, _
                        obelog.Ctd, obelog.SvcId, obelog.Spcod, obelog.PagId, obelog.Recibo, obelog.Clasificacion, obelog.MedioAbonoCod, obelog.Moneda, obelog.Mto)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogBanBifExtornarAutomaticoRes(ByVal idlogBanBifpagarreq As Int64, ByVal obelog As BEBanBifExtornoResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogBanBifExtornoRes", idlogBanBifpagarreq, obelog.StatusCod, obelog.Severidad, obelog.ServStatusCod, obelog.StatusDes, obelog.ExtornoTp, obelog.RqUID, _
            obelog.Login, obelog.SegRol, obelog.OpnNro, obelog.Ctd, obelog.SvcId, obelog.Spcod, obelog.PagId, obelog.Recibo, obelog.Clasificacion, obelog.Moneda, obelog.Mto)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteNonQuery(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log MoMo"

    Public Function RegistrarLogMoMoConsReq(ByVal obelog As BEMoMoConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogMoMoConsReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionMoMo, obelog.CIP, obelog.CodServicio, obelog.CodCliente, obelog.CelularCliente, obelog.CodCanal)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogMoMoConsRes(ByVal idLogMoMoConsReq As Int64, ByVal response As BEMoMoConsultarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogMoMoConsRes", response.CIP, _
                response.CodResultado, response.MensajeResultado, response.Monto, response.CodMoneda, response.ConceptoPago, _
                response.FechaVencimiento, response.CodServicio, idLogMoMoConsReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogMoMoCancReq(ByVal obelog As BEMoMoCancelarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogMoMoCancReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionMoMo, obelog.CodMedioPago, obelog.CIP, obelog.Monto, obelog.CodMoneda, obelog.CodServicio, obelog.CodCliente, obelog.CelularCliente, obelog.CodCanal)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogMoMoCancRes(ByVal idLogMoMoCancReq As Int64, ByVal response As BEMoMoCancelarResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogMoMoCancRes", response.CodResultado, _
                response.MensajeResultado, response.CodMovimiento, response.CodServicio, idLogMoMoCancReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogMoMoAnulReq(ByVal obelog As BEMoMoAnularRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogMoMoAnulReq", obelog.CodPuntoVenta, _
                obelog.NroSerieTerminal, obelog.NroOperacionMoMoPago, obelog.NroOperacionMoMo, obelog.CodMedioPago, _
                obelog.CIP, obelog.Monto, obelog.CodMoneda, obelog.CodServicio, obelog.CodCliente, obelog.CelularCliente, obelog.CodCanal)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogMoMoAnulRes(ByVal idLogMoMoAnulReq As Int64, ByVal response As BEMoMoAnularResponse, _
        ByVal oBELog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogMoMoAnulRes", response.CodResultado, _
                response.MensajeResultado, response.CodMovimiento, response.CodServicio, idLogMoMoAnulReq, oBELog.Descripcion, oBELog.Descripcion2)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log Kasnet"
    'REGISTRAR LOG Kasnet
    Public Function RegistrarLogKasnet(ByVal obelog As BELog) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarLogKasnet", obelog.IdTipo, obelog.Origen, _
                            obelog.Descripcion, obelog.Descripcion2, obelog.Parametro1, obelog.Parametro2, obelog.Parametro3, _
                            obelog.Parametro4, obelog.Parametro5, obelog.Parametro6, obelog.Parametro7, obelog.Parametro8), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        obelog.IdLog = resultado
        Return resultado
    End Function

    'CONSULTAR
    Public Function RegistrarLogKasnetConsReq(ByVal obelog As BEKasnetConsultarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogKASNETConsReq", obelog.CodPuntoVenta, _
                            obelog.NroTerminal, obelog.NroOperacionConsulta, obelog.CodOrdenPago, obelog.CodConvenio, obelog.CodCanal.ToString, _
                            obelog.CodUbigeo, obelog.FecConsulta)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = CType(objDatabase.ExecuteScalar(oDbCommand), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogKasnetConsRes(ByVal idLogKasnetConsReq As Int64, ByVal obelog As BEKasnetConsultarResponse, _
                                                ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim numerorecibo As String = ""
        Dim totalpagar As String = ""
        Dim montopagar As String = ""
        Dim morapagar As String = ""
        Dim codigomoneda As String = ""
        Dim montotipocambio As String = ""
        Dim montoconvertido As String = ""
        Dim fechaemision As String = ""
        Dim fechavencimiento As String = ""
        Dim conceptopago As String = ""
        Dim codigoconvenio As String = ""

        If (obelog.DetDocumentos.Count > 0) Then
            Dim obedetalle As BEKasnetConsultarDetalle = obelog.DetDocumentos(0)
            numerorecibo = obedetalle.NroRecibo
            totalpagar = obedetalle.TotalPagar.ToString
            montopagar = obedetalle.MontoPagar.ToString
            morapagar = obedetalle.MoraPagar.ToString
            codigomoneda = obedetalle.CodMoneda.ToString
            montotipocambio = obedetalle.MontoTipoCambio.ToString
            montoconvertido = obedetalle.MontoConvertido.ToString
            fechaemision = obedetalle.FecEmision
            fechavencimiento = obedetalle.FecVencimiento
            conceptopago = obedetalle.ConceptoPago
            codigoconvenio = obedetalle.CodConvenio
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogKASNETConsRes", idLogKasnetConsReq, obelog.CodRespuesta.ToString, obelog.CodMensaje, obelog.Mensaje, obelog.FecRespuesta, _
            obelog.UsuarioPagador, obelog.CodPuntoVenta, obelog.NroTerminal, obelog.NroOperacionConsulta, obelog.CodOrdenPago, obelog.CantDocumentos.ToString, _
            numerorecibo, totalpagar, montopagar, morapagar, codigomoneda, montotipocambio, montoconvertido, fechaemision, fechavencimiento, conceptopago, codigoconvenio)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'PAGAR
    Public Function RegistrarLogKasnetPagarReq(ByVal obelog As BEKasnetPagarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        'Dim datosEmpresa As String = ""
        Dim numerorecibo As String = ""
        Dim totalpagar As String = ""
        Dim montopagar As String = ""
        Dim morapagar As String = ""
        Dim codigomoneda As String = ""
        Dim montotipocambio As String = ""
        Dim montoconvertido As String = ""
        Dim fechaemision As String = ""
        Dim fechavencimiento As String = ""
        Dim conceptopago As String = ""

        If (obelog.DetDocumentos.Count > 0) Then
            Dim obedetalle As BEKasnetPagarDetalle = obelog.DetDocumentos(0)
            numerorecibo = obedetalle.NroRecibo
            totalpagar = obedetalle.TotalPagar.ToString
            montopagar = obedetalle.MontoPagar.ToString
            morapagar = obedetalle.MoraPagar.ToString
            codigomoneda = obedetalle.CodMoneda.ToString
            montotipocambio = obedetalle.MontoTipoCambio.ToString
            montoconvertido = obedetalle.MontoConvertido.ToString
            fechaemision = obedetalle.FecEmision
            fechavencimiento = obedetalle.FecVencimiento
            conceptopago = obedetalle.ConceptoPago
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogKASNETPagarReq", obelog.CodPuntoVenta, obelog.NroTerminal, obelog.NroOperacionPago, obelog.CodOrdenPago, obelog.FecPago, obelog.CodConvenio, obelog.CodCanal.ToString, obelog.CodUbigeo, _
                        obelog.CodMedioPago.ToString, obelog.CantDocumentos.ToString, numerorecibo, totalpagar, montopagar, morapagar, codigomoneda, montotipocambio, montoconvertido, fechaemision, fechavencimiento, conceptopago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogKasnetPagarRes(ByVal idlogkasnetpagarreq As Int64, ByVal obelog As BEKasnetPagarResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogKASNETPagarRes", idlogkasnetpagarreq, obelog.CodRespuesta.ToString, obelog.CodMensaje, obelog.Mensaje, obelog.FecRespuesta, obelog.CodOperacionEmpresa, obelog.CodPuntoVenta, _
            obelog.NroTerminal, obelog.NroOperacionPago, obelog.CodOrdenPago, obelog.CantDocumentos.ToString)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteNonQuery(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function




    'EXTORNO AUTOMATICO
    Public Function RegistrarLogKasnetExtReq(ByVal obelog As BEKasnetExtornarRequest) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim numerorecibo As String = ""
        Dim totalpagar As String = ""
        Dim montopagar As String = ""
        Dim morapagar As String = ""
        Dim montotipocambio As String = ""
        Dim montoconvertido As String = ""
        Dim fechaemision As String = ""
        Dim fechavencimiento As String = ""

        If (obelog.DetDocumentos.Count > 0) Then
            Dim obedetalle As BEKasnetExtornarDetalle = obelog.DetDocumentos(0)
            numerorecibo = obedetalle.NroRecibo
            totalpagar = obedetalle.TotalPagar.ToString
            montopagar = obedetalle.MontoPagar.ToString
            morapagar = obedetalle.MoraPagar.ToString
            montotipocambio = obedetalle.MontoTipoCambio.ToString
            montoconvertido = obedetalle.MontoConvertido.ToString
            fechaemision = obedetalle.FecEmision
            fechavencimiento = obedetalle.FecVencimiento
        End If

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogKASNETExtReq", obelog.CodPuntoVenta, obelog.NroTerminal, obelog.NroOperacionPago, obelog.NroOperacionExtorno, obelog.FecPago, obelog.FecExtorno, obelog.TipoExtorno.ToString, obelog.CodOrdenPago, _
            obelog.CodConvenio, obelog.CodMoneda.ToString, obelog.CodCanal.ToString, obelog.CodMedioPago.ToString, obelog.CantDocumentos.ToString, numerorecibo, totalpagar, montopagar, morapagar, montotipocambio, montoconvertido, fechaemision, fechavencimiento)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))

            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function RegistrarLogKasnetExtRes(ByVal idlogkasnetextreq As Int64, ByVal obelog As BEKasnetExtornarResponse, ByVal oLog As BELog) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarLogKASNETExtRes", idlogkasnetextreq, obelog.CodRespuesta.ToString, obelog.CodMensaje, obelog.Mensaje, obelog.FecRespuesta, obelog.CodOperacionEmpresa, _
                        obelog.CodPuntoVenta, obelog.NroTerminal, obelog.NroOperacionExtorno, obelog.CodOrdenPago, obelog.CantDocumentos.ToString)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = ObjectToInt64(objDatabase.ExecuteScalar(oDbCommand))
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

#Region "Log Navegacion"
    Public Function RegistrarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarLogNavegacion", obelognavegacion.IdUsuario, obelognavegacion.UrlPage, obelognavegacion.Accion, obelognavegacion.PCName), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        obelognavegacion.IdLogNavegacion = resultado
        Return resultado
    End Function
    'ByVal idLogFCCancReq As Int64

    Public Function ConsultarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As List(Of BELogNavegacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim lista As New List(Of BELogNavegacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarLogNavegacionXClientePg", _
            obelognavegacion.IdUsuario, obelognavegacion.FechaNavegacionDesde, obelognavegacion.FechaNavegacionHasta, obelognavegacion.PageNumber, obelognavegacion.PageSize, obelognavegacion.PropOrder, _
            obelognavegacion.TipoOrder)
                While reader.Read
                    lista.Add(New BELogNavegacion(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return lista
    End Function

#End Region

#Region "Seguridad por Token"

    Public Function RegistrarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As BEParametrosEmailMonedero
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As New BEParametrosEmailMonedero
        Try
            Dim db As DbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarEquipoRegistradoXusuario", obeequiporegistradoxusuario.Usuario, obeequiporegistradoxusuario.Token, obeequiporegistradoxusuario.PcName)

            Using objIDataReader As IDataReader = objDatabase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    resultado.NombreCliente = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("NombreCliente"))
                    resultado.NombrePC = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("NombrePC"))
                    resultado.FechaRegistro = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objIDataReader("FechaRegistro"))
                    resultado.EmailCliente = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("EmailCliente"))
                    resultado.Monto = 1
                End If
            End Using
            Return resultado
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ValidarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim lista As New List(Of BEEquipoRegistradoXusuario)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ValidarEquipoRegistradoXusuario", obeequiporegistradoxusuario.Usuario, obeequiporegistradoxusuario.Token)
                While reader.Read
                    lista.Add(New BEEquipoRegistradoXusuario(reader, 1))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return lista
    End Function

    Public Function ConsultarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim lista As New List(Of BEEquipoRegistradoXusuario)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarEquipoRegistradoXusuario", _
                                                                    obeequiporegistradoxusuario.Usuario, _
                                                                    obeequiporegistradoxusuario.PageNumber, obeequiporegistradoxusuario.PageSize, _
                                                                    obeequiporegistradoxusuario.PropOrder, obeequiporegistradoxusuario.TipoOrder)
                While reader.Read
                    lista.Add(New BEEquipoRegistradoXusuario(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return lista
    End Function

    Public Function EliminarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_EliminarEquipoRegistrado", obeequiporegistradoxusuario.IdEquipoRegistrado), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        obeequiporegistradoxusuario.IdEquipoRegistrado = resultado
        Return resultado
    End Function

#End Region

    '<add Peru.Com FaseIII>
#Region "Log Redirect"
    Public Function RegistrarLogRedirect(ByVal oBELogRedirect As BELogRedirect) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CType(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogRedirect]", _
                                                        oBELogRedirect.IdTipo, oBELogRedirect.UrlOrigen, _
                                                        oBELogRedirect.UrlDestino, oBELogRedirect.Descripcion, _
                                                        oBELogRedirect.Parametro1, oBELogRedirect.Parametro2, _
                                                        oBELogRedirect.Parametro3, oBELogRedirect.Parametro4, _
                                                        oBELogRedirect.Parametro5, oBELogRedirect.Parametro6, _
                                                        oBELogRedirect.Parametro7, oBELogRedirect.Parametro8), Integer)
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        oBELogRedirect.IdLogRedirect = resultado
        Return resultado
    End Function
#End Region

    Public Function RegistrarTokenCambioCorreo(ByVal be As BEUsuarioBase) As BEUsuarioBase
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


        Dim obeUsuario As BEUsuarioBase = Nothing
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[GenerarTokenContraseñaOlvidada]", _
                                                                    be.Email, be.TokenGUIDPassword)
                While reader.Read()
                    obeUsuario = New BEUsuarioBase()
                    obeUsuario.Nombres = ObjectToString(reader("Nombres"))
                    obeUsuario.Email = ObjectToString(reader("Email"))
                    obeUsuario.FechaCreacionTokenGUID = ObjectToDateTime(reader("FechaExpiracionTokenClave"))
                    obeUsuario.TokenGUIDPassword = be.TokenGUIDPassword
                End While
            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeUsuario
    End Function
    Public Function ActualizarContraseniaOlvidada(ByVal be As BEUsuarioBase) As BEUsuarioBase
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


        Dim obeUsuario As BEUsuarioBase = Nothing
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[ActualizarContraseniaOlvidada]", _
                                                                    be.TokenGUIDPassword, be.NuevoPassword)

                While reader.Read()
                    obeUsuario = New BEUsuarioBase()
                    obeUsuario.Nombres = ObjectToString(reader("Nombres"))
                    obeUsuario.Email = ObjectToString(reader("Email"))
                    obeUsuario.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
                    obeUsuario.IdEstado = ObjectToInt(reader("Mensaje"))
                    obeUsuario.TokenGUIDPassword = be.TokenGUIDPassword
                End While

            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeUsuario
    End Function

    Public Function SuscriptorRegistrar(ByVal oBESuscriptor As SPE.Entidades.BESuscriptor) As String
        Dim CorreoRetorno As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        CorreoRetorno = CStr(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarSuscriptor]", oBESuscriptor.Correo))
        Return CorreoRetorno
    End Function

    Public Function ConsultarPlantillaTipoVariacion() As List(Of BEParametro)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listaBEParametro As New List(Of BEParametro)
        Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaTipoVariacion]")
            While reader.Read
                listaBEParametro.Add(New BEParametro(reader))
            End While
        End Using
        Return listaBEParametro
    End Function

    Private Function resultado() As Long
        Throw New NotImplementedException
    End Function

    Public Function HabilitarNuevaVersionExtornoInterno(ByVal codBanco As String, ByVal NumeroOrdenPago As Integer) As Integer
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            _dlogger.Info(String.Format("HabilitarNuevaVersionExtornoInterno - NumeroOrdenPago:{0},codBanco:{1}", NumeroOrdenPago, codBanco))
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerParametrosExtornoInterno]", NumeroOrdenPago, codBanco))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function
    'FixSaga20140425
    Public Function HabilitarActualizacionOrdenPago(ByVal IdServicio As String) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[HabilitarActualizacionOrdenPago]", IdServicio))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'FixSaga20140425
    Public Function ObtenerOrdenSolicitudPorCodTransaccion(ByVal IdServicio As String, ByVal CodTransaccion As String) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[ObtenerOrdenSolicitudPorCodTransaccion]", IdServicio, CodTransaccion))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

#Region "Liquidacion"

    Public Function RecuperarCipsNoConciliados(ByVal idMoneda As Integer, ByVal Fecha As DateTime) As List(Of BELiquidacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListNC As New List(Of BELiquidacion)

        Try
            'Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarTransaccionesaLiquidarPG]", obeTransferencia.idEmpresaContratante, obeTransferencia.IdMoneda, obeTransferencia.IdEstado, obeTransferencia.FechaInicio, obeTransferencia.FechaFin, obeTransferencia.PageNumber, obeTransferencia.PageSize, obeTransferencia.PropOrder, obeTransferencia.TipoOrder)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RecuperarCipsNoConciliados]", idMoneda, Fecha)
                While reader.Read()
                    ListNC.Add(New SPE.Entidades.BELiquidacion(reader, "ConsLiqNoConc"))
                End While
            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListNC
    End Function



    Public Function RecuperarDepositosBCP(ByVal idMoneda As Integer, ByVal empresas As String) As List(Of BELiquidacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListNC As New List(Of BELiquidacion)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RecuperarDepositosBCP]", idMoneda, empresas)
                While reader.Read()
                    ListNC.Add(New SPE.Entidades.BELiquidacion(reader, "DepositosBCP"))
                End While
            End Using

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListNC
    End Function

    Public Function RegistrarPeriodo(ByVal oBEPeriodo As BEPeriodoLiquidacion) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarPeriodo]", oBEPeriodo.Descripcion, oBEPeriodo.IdEstado, _
            oBEPeriodo.DiasLiquidacion, oBEPeriodo.FechasFin, oBEPeriodo.FechaCreacion))

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function


    Public Function ActualizarPeriodo(ByVal oBEPeriodo As BEPeriodoLiquidacion) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarPeriodo]", oBEPeriodo.IdPeriodo, oBEPeriodo.Descripcion, oBEPeriodo.IdEstado, _
            oBEPeriodo.DiasLiquidacion, oBEPeriodo.FechasFin))

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function


    Public Function ConsultarIdDescripcionPeriodo() As List(Of BEPeriodoLiquidacion)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listParametros As New List(Of BEPeriodoLiquidacion)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarIdDescripcionPeriodo")

                While reader.Read
                    listParametros.Add(New BEPeriodoLiquidacion(reader, "parametros"))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listParametros
        '
    End Function

    Public Function RecuperarDatosPeriodoLiquidacion(ByVal be As BEPeriodoLiquidacion) As List(Of BEPeriodoLiquidacion)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listDatosPeriodo As New List(Of BEPeriodoLiquidacion)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_RecuperarDatosPeriodoLiquidacion", be.IdPeriodo, be.IdEstado)

                While reader.Read
                    listDatosPeriodo.Add(New BEPeriodoLiquidacion(reader, "ListaDetalle"))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listDatosPeriodo
        '
    End Function



    Public Function DeletePeriodo(ByVal oBEPeriodo As BEPeriodoLiquidacion) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_EliminarPeriodoLiquidacion]", oBEPeriodo.IdPeriodo))

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

#End Region
End Class
