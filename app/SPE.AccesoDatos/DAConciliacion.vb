Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SPE.Entidades
Imports System.Configuration
Imports System.Data.Common
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class DAConciliacion
    Implements IDisposable

    ''' <summary>
    ''' Registra cabecera en Tabla PagoEfectivo.Conciliacion
    ''' </summary>
    ''' <param name="oBEConciliacionRequest">
    ''' Instancia de objeto BEConciliacionRequest
    ''' - FechaConciliacion
    ''' - IdUsuarioCreacion
    ''' </param>
    ''' <returns>Id generado al registrar Conciliacion</returns>
    ''' <remarks></remarks>
    Public Function RegistrarConciliacion(ByVal oBEConciliacionRequest As BEConciliacionRequest) As Integer
        Dim IdConciliacion As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdConciliacion = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarConciliacion]", _
                oBEConciliacionRequest.FechaConciliacion, oBEConciliacionRequest.IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdConciliacion
    End Function

    ''' <summary>
    ''' Registrar cabecera en Tabla PagoEfectivo.ConciliacionEntidad
    ''' </summary>
    ''' <param name="oBEConciliacionEntidad">
    ''' Objeto BEConciliacionEntidad
    ''' - IdConciliacion
    ''' - CodigoBanco
    ''' - IdUsuarioCreacion
    ''' </param>
    ''' <returns>Id generado al registrar ConciliacionEntidad</returns>
    ''' <remarks></remarks>
    Public Function RegistrarConciliacionEntidad(ByVal oBEConciliacionEntidad As BEConciliacionEntidad) As Integer
        Dim IdConciliacionEntidad As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdConciliacionEntidad = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarConciliacionEntidad]", _
                oBEConciliacionEntidad.IdConciliacion, oBEConciliacionEntidad.CodigoBanco, oBEConciliacionEntidad.IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdConciliacionEntidad
    End Function

    ''' <summary>
    ''' Registrar cabecera en Tabla PagoEfectivo.ConciliacionArchivo.
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">
    ''' Instancia de objeto BEConciliacionArchivo:
    ''' - CodigoBanco
    ''' - NombreArchivo
    ''' - Fecha
    ''' - IdTipoConciliacion
    ''' - IdOrigenConciliacion
    ''' - IdUsuarioCreacion
    ''' </param>
    ''' <returns>Id generado al registrar ConciliacionArchivo.</returns>
    ''' <remarks>Retorna -1 cuando el nombre de archivo ya existe.</remarks>
    Public Function RegistrarConciliacionArchivo(ByVal oBEConciliacionArchivo As BEConciliacionArchivo) As Integer
        Dim IdConciliacionArchivo As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdConciliacionArchivo = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarConciliacionArchivo]", _
                oBEConciliacionArchivo.CodigoBanco, oBEConciliacionArchivo.NombreArchivo, _
                oBEConciliacionArchivo.Fecha, oBEConciliacionArchivo.IdTipoConciliacion, _
                oBEConciliacionArchivo.IdOrigenConciliacion, oBEConciliacionArchivo.IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdConciliacionArchivo
    End Function

    ''' <summary>
    ''' Registra en detalle de la conciliación BCP
    ''' </summary>
    ''' <param name="IdConciliacionArchivo">Id de tabla ConciliacionArchivo</param>
    ''' <param name="Linea">Cadena que representa una línea de detalle del archivo enviado por BCP</param>
    ''' <param name="IdUsuarioCreacion">Id del Usuario de creación del registro</param>
    ''' <remarks></remarks>
    Public Function RegistrarDetConciliacionBCP(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, _
        ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionBCP As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionBCP = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionBCP]", _
                IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionBCP
    End Function

    ''' <summary>
    ''' Registra en detalle de la conciliación BBVA
    ''' </summary>
    ''' <param name="IdConciliacionArchivo">Id de tabla ConciliacionArchivo</param>
    ''' <param name="Linea">Cadena que representa una línea de detalle del archivo enviado por BBVA</param>
    ''' <param name="IdUsuarioCreacion">Id del Usuario de creación del registro</param>
    ''' <remarks></remarks>
    Public Function RegistrarDetConciliacionBBVA(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, _
        ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionBBVA As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionBBVA = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionBBVA]", _
                IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionBBVA
    End Function

    ''' <summary>
    ''' Registra en detalle de la conciliación WesternUnion
    ''' </summary>
    ''' <param name="IdConciliacionArchivo">Id de tabla ConciliacionArchivo</param>
    ''' <param name="Linea1"></param>
    ''' <param name="Linea2"></param>
    ''' <param name="Linea3"></param>
    ''' <param name="IdUsuarioCreacion">Id del Usuario de creación del registro</param>
    ''' <remarks></remarks>
    Public Function RegistrarDetConciliacionWU(ByVal IdConciliacionArchivo As Int64, ByVal Linea1 As String, ByVal Linea2 As String, ByVal Linea3 As String, ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionWU As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionWU = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionWU]", IdConciliacionArchivo, Linea1, Linea2, Linea3, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionWU
    End Function

    ''' <summary>
    ''' Registra en detalle de la conciliación Interbank
    ''' </summary>
    ''' <param name="IdConciliacionArchivo">Id de tabla ConciliacionArchivo</param>
    ''' <param name="Linea">Cadena que representa una línea de detalle del archivo enviado por IBK</param>
    ''' <param name="IdUsuarioCreacion">Id del Usuario de creación del registro</param>
    ''' <remarks></remarks>
    Public Function RegistrarDetConciliacionIBK(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionWU As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionWU = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionIBK]", IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionWU
    End Function

    ''' <summary>
    ''' Procesa el detalle del archivo enviado por el BCP
    ''' </summary>
    ''' <param name="IdConciliacionArchivoBCP">Id de la tabla ConciliacionArchivo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ProcesarDetConciliacionBCP(ByVal IdConciliacionArchivoBCP As Int64) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionBCP]", IdConciliacionArchivoBCP)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut") '600

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "BCP", 1))
                    End While
                    reader.Close()
                End Using
            End Using

        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    ''' <summary>
    ''' Procesa el detalle del archivo enviado por el BBVA
    ''' </summary>
    ''' <param name="IdConciliacionArchivoBBVA">Id de la tabla ConciliacionArchivo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ProcesarDetConciliacionBBVA(ByVal IdConciliacionArchivoBBVA As Int64, ByVal CodigoMoneda As String) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionBBVA]", IdConciliacionArchivoBBVA, CodigoMoneda)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut") '600

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "BBVA", 1))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    ''' <summary>
    ''' Procesa el detalle del archivo enviado por WesternUnion
    ''' </summary>
    ''' <param name="IdConciliacionArchivoWU">Id de la tabla ConciliacionArchivo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ProcesarDetConciliacionWU(ByVal IdConciliacionArchivoWU As Int64) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionWU]", IdConciliacionArchivoWU)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut")

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "WU", 1))
                    End While
                    reader.Close()
                End Using
            End Using

        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    ''' <summary>
    ''' Procesa el detalle del archivo enviado por Interbank
    ''' </summary>
    ''' <param name="IdConciliacionArchivoIBK">Id de la tabla ConciliacionArchivo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ProcesarDetConciliacionIBK(ByVal IdConciliacionArchivoIBK As Int64) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionIBK]", IdConciliacionArchivoIBK)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut")

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "IBK", 1))
                    End While
                    reader.Close()
                End Using
            End Using

        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    ''' <summary>
    ''' Actualiza el estado de conciliación de las Tablas DetConciliacionBCP y Orden Pago
    ''' </summary>
    ''' <param name="IdDetConciliacionBCP">Id en tabla DetConciliacionBCP</param>
    ''' <param name="IdEstadoConciliacion">Id de estado de la conciliación</param>
    ''' <remarks></remarks>
    Public Sub ActualizarDetConciliacionBCP(ByVal IdDetConciliacionBCP As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionBCP]", IdDetConciliacionBCP, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza el estado de conciliación de las Tablas DetConciliacionBBVA y Orden Pago
    ''' </summary>
    ''' <param name="IdDetConciliacionBBVA">Id en tabla DetConciliacionBBVA</param>
    ''' <param name="IdEstadoConciliacion">Id de estado de la conciliación</param>
    ''' <remarks></remarks>
    Public Sub ActualizarDetConciliacionBBVA(ByVal IdDetConciliacionBBVA As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionBBVA]", IdDetConciliacionBBVA, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza el estado de conciliación de las Tablas DetConciliacionWU y Orden Pago
    ''' </summary>
    ''' <param name="IdDetConciliacionWU">Id en tabla DetConciliacionWU</param>
    ''' <param name="IdEstadoConciliacion">Id de estado de la conciliación</param>
    ''' <remarks></remarks>
    Public Sub ActualizarDetConciliacionWU(ByVal IdDetConciliacionWU As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionWU]", IdDetConciliacionWU, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza el estado de conciliación de las Tablas DetConciliacionIBK y Orden Pago
    ''' </summary>
    ''' <param name="IdDetConciliacionIBK">Id en tabla DetConciliacionIBK</param>
    ''' <param name="IdEstadoConciliacion">Id de estado de la conciliación</param>
    ''' <remarks></remarks>
    Public Sub ActualizarDetConciliacionIBK(ByVal IdDetConciliacionIBK As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionIBK]", IdDetConciliacionIBK, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza el estado, observación y cantidad de CIPs procesados y conciliados de la tabla ConciliacionArchivo
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">
    ''' Instancia de objeto BEConciliacionArchivo
    ''' - IdConciliacionArchivo
    ''' - IdUsuarioActualizacion
    ''' - ListaOperacionesConciliadas
    ''' - ListaOperacionesNoConciliadas
    ''' </param>
    ''' <remarks></remarks>
    Public Sub ActualizarEstadoConciliacionArchivo(ByVal oBEConciliacionArchivo As BEConciliacionArchivo)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarEstadoConciliacionArchivo]", _
                oBEConciliacionArchivo.IdConciliacionArchivo, oBEConciliacionArchivo.IdUsuarioActualizacion, _
                (oBEConciliacionArchivo.ListaOperacionesConciliadas.FindAll(Function(x) Not x.CIP.Substring(0, 2).Equals("99")).Count + oBEConciliacionArchivo.ListaOperacionesNoConciliadas.FindAll(Function(x) Not x.CIP.Substring(0, 2).Equals("99")).Count), _
                oBEConciliacionArchivo.ListaOperacionesConciliadas.FindAll(Function(x) Not x.CIP.Substring(0, 2).Equals("99")).Count,
            (oBEConciliacionArchivo.ListaOperacionesConciliadas.FindAll(Function(x) x.CIP.Substring(0, 2).Equals("99")).Count + oBEConciliacionArchivo.ListaOperacionesNoConciliadas.FindAll(Function(x) x.CIP.Substring(0, 2).Equals("99")).Count), _
            oBEConciliacionArchivo.ListaOperacionesConciliadas.FindAll(Function(x) x.CIP.Substring(0, 2).Equals("99")).Count)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza la cantidad de OP y Recargas que tiene el Archivo de conciliación
    ''' </summary>
    ''' <param name="ops">
    ''' Cantidad de OPs en el archivo
    ''' </param>
    ''' ''' <param name="recs">
    ''' Cantidad de Recargas en el archivo
    ''' </param>
    ''' <remarks></remarks>
    Public Sub ActualizarCantidadDetallesConciliacionArchivo(ByVal IdConciliacionArchivo As Long, ByVal ops As Integer, ByVal recs As Integer)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarCantidadDetallesConciliacionArchivo]", IdConciliacionArchivo, ops, recs)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza el estado, observación y cantidad de CIPs procesados y conciliados de la tabla ConciliacionEntidad
    ''' </summary>
    ''' <param name="oBEConciliacionEntidad">
    ''' Objeto BEConciliacionEntidad
    ''' - IdConciliacionEntidad
    ''' - IdUsuarioActualizacion
    ''' </param>
    ''' <remarks></remarks>
    Public Sub ActualizarEstadoConciliacionEntidad(ByVal oBEConciliacionEntidad As BEConciliacionEntidad)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarEstadoConciliacionEntidad]", _
                oBEConciliacionEntidad.IdConciliacionEntidad, oBEConciliacionEntidad.IdUsuarioActualizacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Actualiza el estado, observación y cantidad de CIPs procesados y conciliados de la tabla Conciliacion
    ''' </summary>
    ''' <param name="oBEConciliacionRequest">
    ''' Objeto BEConciliacion
    ''' - IdConciliacion
    ''' - IdUsuarioActualizacion
    ''' </param>
    ''' <remarks></remarks>
    Public Sub ActualizarEstadoConciliacion(ByVal oBEConciliacionRequest As BEConciliacionRequest)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarEstadoConciliacion]", _
                oBEConciliacionRequest.IdConciliacion, oBEConciliacionRequest.IdUsuarioActualizacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ConsultarUltimasConciliaciones(ByVal request As BEConciliacionRequest) As List(Of BEConciliacionResponse)
        Dim response As New List(Of BEConciliacionResponse)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)

            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarUltimasConciliaciones]", _
                                                                    request.IdEstado, request.FechaInicio, request.FechaFin)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    response.Add(New BEConciliacionResponse(reader, "UltimasConciliaciones"))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function

    Public Function ConsultarConciliacionEntidad(ByVal request As BEConciliacionEntidad) As List(Of BEConciliacionEntidad)
        Dim response As New List(Of BEConciliacionEntidad)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarConciliacionEntidad]", _
                                                                    request.IdConciliacion, request.IdEstado, request.CodigoBanco, request.FechaInicio, request.FechaFin)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    response.Add(New BEConciliacionEntidad(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function

    Public Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionArchivo)
        Dim response As New List(Of BEConciliacionArchivo)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarConciliacionArchivo]", _
                                                                    request.IdEstado, request.CodigoBanco, request.NombreArchivo, request.IdTipoConciliacion, _
                request.IdOrigenConciliacion, request.FechaInicio, request.FechaFin)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    response.Add(New BEConciliacionArchivo(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function

    Public Function ConsultarDetalleConciliacion(ByVal request As BEConciliacionDetalle) As List(Of BEConciliacionDetalle)
        Dim response As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarConciliacionDetalle]", _
                                                                    request.IdConciliacionArchivo, request.IdEstado, request.CodigoBanco, request.CIP, request.NombreArchivo, _
                 request.FechaInicio, request.FechaFin)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    response.Add(New BEConciliacionDetalle(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function

    Public Sub RecalcularCIPsConciliacion(ByVal IdUsuario As Integer)
        Try
            'Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            'objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RecalcularConciliacion]", IdUsuario)
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_RecalcularConciliacion]", IdUsuario)
                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut")
                Dim reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Function ConsultarTareasConciliacion(ByVal idTipoConciliacion As Integer) As List(Of BETareaConciliacion)
    '    Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
    '    Dim response As New List(Of BETareaConciliacion)
    '    Try
    '        Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarTareaConciliacion]", idTipoConciliacion)
    '            While reader.Read
    '                response.Add(New BETareaConciliacion(reader))
    '            End While
    '        End Using
    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        Throw ex
    '    End Try
    '    Return response
    'End Function

    'Public Function ConsultarOperacionesNoConciliadas(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria)
    '    Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
    '    Dim objListaOperacionBancaria As New List(Of BEOperacionBancaria)
    '    Try
    '        Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarOperacionesNoConciliadas]")
    '            While reader.Read
    '                objListaOperacionBancaria.Add(New BEOperacionBancaria(reader, "NoConciliadas"))
    '            End While
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return objListaOperacionBancaria
    'End Function

    Public Function RegistrarDetConciliacionFC(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, _
        ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionFC As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionFC = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionFC]", _
                IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionFC
    End Function

    Public Function ProcesarDetConciliacionFC(ByVal IdDetConciliacionFC As Int64, ByVal CodigoMoneda As String) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionFC]", IdDetConciliacionFC, CodigoMoneda)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut") '600

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "FC", 1))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Sub ActualizarDetConciliacionFC(ByVal IdDetConciliacionFC As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionFC]", IdDetConciliacionFC, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function RegistrarDetConciliacionSBK(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, _
        ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionSBK As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionSBK = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionSBK]", _
                IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionSBK
    End Function

    Public Function ProcesarDetConciliacionSBK(ByVal IdConciliacionArchivoSBK As Int64) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionSBK]", IdConciliacionArchivoSBK)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut") '600

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "SBK", 1))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Sub ActualizarDetConciliacionSBK(ByVal IdDetConciliacionSBK As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionSBK]", IdDetConciliacionSBK, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'BANBIF
    Public Function RegistrarDetConciliacionBIF(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionWU As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionWU = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionBIF]", IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionWU
    End Function
    Public Function ProcesarDetConciliacionBIF(ByVal IdDetConciliacionBIF As Int64) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionBIF]", IdDetConciliacionBIF)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut") '600

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "BIF", 1))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Sub ActualizarDetConciliacionBIF(ByVal IdDetConciliacionBIF As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionBIF]", IdDetConciliacionBIF, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Kasnet

    Public Function RegistrarDetConciliacionKasnet(ByVal IdConciliacionArchivo As Int64, ByVal Linea As String, ByVal IdUsuarioCreacion As Integer)
        Dim IdDetConciliacionWU As Int64
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdDetConciliacionWU = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetConciliacionKasnet]", IdConciliacionArchivo, Linea, IdUsuarioCreacion)
        Catch ex As Exception
            Throw ex
        End Try
        Return IdDetConciliacionWU
    End Function
    Public Function ProcesarDetConciliacionKasnet(ByVal IdDetConciliacionKasnet As Int64) As List(Of BEConciliacionDetalle)
        Dim Resultado As New List(Of BEConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ProcesarDetConciliacionKasnet]", IdDetConciliacionKasnet)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut") '600

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        Resultado.Add(New BEConciliacionDetalle(reader, "Kasnet", 1))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Sub ActualizarDetConciliacionKasnet(ByVal IdDetConciliacionKasnet As Int64, ByVal IdEstadoConciliacion As Int32)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarDetConciliacionKasnet]", IdDetConciliacionKasnet, IdEstadoConciliacion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ConsultarConciliacionAjuste(ByVal request As BEConciliacionAjuste) As List(Of BEConciliacionAjuste)
        Dim response As New List(Of BEConciliacionAjuste)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarConciliacionAjuste]", _
                 request.CIP, request.CodigoServicio, request.CodigoBanco, request.NombreArchivo, _
                 request.FechaInicio, request.FechaFin)
                While reader.Read
                    response.Add(New BEConciliacionAjuste(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function


    Public Function ConsultarConciliacionDiaria(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionDiaria)
        Dim response As New List(Of BEConciliacionDiaria)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
            Using oDbCommand As DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarConciliacionDiaria]", _
                     request.CodigoBanco, request.FechaInicio)
                oDbCommand.CommandTimeout = ConfigurationManager.AppSettings("TimeoutSQLReplica")
                Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                    While reader.Read
                        response.Add(New BEConciliacionDiaria(reader))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function


    Public Function ConsultarPreConciliacionDetalle(ByVal request As BEConciliacionArchivo) As List(Of BEPreConciliacionDetalle)
        Dim response As New List(Of BEPreConciliacionDetalle)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)

            Dim oDbCommand As DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsDetPreConciliacion]", request.CodigoBanco)
            oDbCommand.CommandTimeout = ConfigurationManager.AppSettings("TimeoutSQLReplica")

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    response.Add(New BEPreConciliacionDetalle(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function





    Public Function ConsultarConciliacionDetalleAjuste(ByVal CodigoBanco As String, ByVal IdDetConciliacion As Integer) As BEConciliacionDetalleAjuste
        Dim response As New BEConciliacionDetalleAjuste
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarConciliacionDetalleAjuste]", _
                 CodigoBanco, IdDetConciliacion)
                If reader.Read Then
                    response = New BEConciliacionDetalleAjuste(reader)
                End If
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function

    '<upd Proc.Tesoreria>
    Public Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago)
        Dim response As New List(Of BEOrdenPago)
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarOrdenesConcArch]", _
                 request.IdConciliacionArchivo, request.CodigoServicio, request.CodigoMedioPago)
                While reader.Read
                    response.Add(New BEOrdenPago(reader, "OpsConArch"))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return response
    End Function

#Region "Pre-Conciliacion"

    Public Function RegistrarPreConciliacionArchivo(ByRef request As BEPreConciliacionRequest) As Integer
        Dim IdConciliacionArchivo As Int64
        Try
            Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdConciliacionArchivo = oDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarPreconciliacion", _
                request.NombreArchivoPreCon, request.CantidadCodPago, request.IdMoneda, request.CodigoBanco)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdConciliacionArchivo
    End Function

    Public Sub ActualizarEstadoArchivoPreConciliacion(ByVal request As BEPreConciliacionRequest)
        Try
            Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            oDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarArchivoPreConciliacion", request.Archivo.IdConciliacionArchivo, _
                                      request.ListaOperacionesConciliadas.Count + request.ListaOperacionesNoConciliadas.Count, _
                                      request.ListaOperacionesConciliadas.Count)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub

    Public Sub RegistrarDetPreConciliacionBCP(ByVal IdPreConciliacionArchivo As Int64, ListaDataPreConciliacion As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion))
        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)

                Dim DtListaDataPreConciliacion As New DataTable
                DtListaDataPreConciliacion.Columns.Add("IdPreconciliacion")
                DtListaDataPreConciliacion.Columns.Add("CodPago")
                DtListaDataPreConciliacion.Columns.Add("Monto")
                DtListaDataPreConciliacion.Columns.Add("OficinaSucursal")
                DtListaDataPreConciliacion.Columns.Add("NroOperacion")
                DtListaDataPreConciliacion.Columns.Add("IdEstado")
                DtListaDataPreConciliacion.Columns.Add("Observacion")
                Try
                    For Each item As BEDataPreConciliacion In ListaDataPreConciliacion.ToArray()
                        'If item.Campo2.ToString.StartsWith("EFECTIVO") Or item.Campo2.ToString.StartsWith("EXTORNO") Then
                        '    If item.Campo2.ToString.StartsWith("EFECTIVO") Then
                        '        DtListaDataPreConciliacion.Rows.Add(IdPreConciliacionArchivo, item.Campo2.ToString.Remove(0, 8), CDec(item.Campo3), item.Campo4.ToString,
                        '                                        item.Campo5, 0, "")
                        '    Else
                        '        DtListaDataPreConciliacion.Rows.Add(IdPreConciliacionArchivo, "EXTORNO", CDec(item.Campo3), item.Campo4.ToString, item.Campo5, 0, "")
                        '    End If
                        'End If
                        DtListaDataPreConciliacion.Rows.Add(IdPreConciliacionArchivo, item.Campo1.ToString, CDec(item.Campo2), item.Campo3.ToString,
                                                                item.Campo4, item.Campo5, item.Campo6)
                    Next
                Catch ex As Exception
                    Throw New Exception("El archivo no tiene el formato correcto o no pertenece al banco establecido.")
                End Try
                If DtListaDataPreConciliacion.Rows.Count() = 0 Then
                    Throw New Exception("El archivo no tiene el formato correcto o no pertenece al banco establecido.")
                End If
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_RegistrarDetPreConciliacionBCP]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                'oSqlCommand.Parameters.Add("@IdPreConciliacionArchivo", SqlDbType.Int).Value = IdPreConciliacionArchivo
                oSqlCommand.Parameters.Add("@ListaDataPreConciliacion", SqlDbType.Structured).Value = DtListaDataPreConciliacion
                oSqlConnection.Open()
                oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub

    Public Sub RegistrarDetPreConciliacionBBVA(ByVal IdPreConciliacionArchivo As Int64, ListaDataPreConciliacion As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion))
        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)

                Dim DtListaDataPreConciliacion As New DataTable
                DtListaDataPreConciliacion.Columns.Add("IdPreconciliacion")
                DtListaDataPreConciliacion.Columns.Add("CodPago")
                DtListaDataPreConciliacion.Columns.Add("Monto")
                DtListaDataPreConciliacion.Columns.Add("OficinaSucursal")
                DtListaDataPreConciliacion.Columns.Add("NroOperacion")
                DtListaDataPreConciliacion.Columns.Add("IdEstado")
                DtListaDataPreConciliacion.Columns.Add("Observacion")
                Try
                    For Each item As BEDataPreConciliacion In ListaDataPreConciliacion.ToArray()
                        If item.Campo4.ToString.StartsWith("*0") Or item.Campo4.ToString.StartsWith("0") Or item.Campo4.ToString.StartsWith("1") Then
                            Dim CantCar As Integer = item.Campo4.ToString.Length()
                            Dim CodigoPago As String
                            If item.Campo4.ToString.StartsWith("*0") Then
                                Dim tempo1 As String = item.Campo4.ToString.Remove(0, 1).Replace(".", "")
                                CodigoPago = tempo1.Substring(0, 14)
                                DtListaDataPreConciliacion.Rows.Add(IdPreConciliacionArchivo, CodigoPago, CDec(item.Campo6), _
                                                                    item.Campo2.ToString, item.Campo5, 0, "")
                            ElseIf item.Campo4.ToString.StartsWith("0") Then
                                CodigoPago = item.Campo4.ToString.Substring(0, 14).Replace(".", "")
                                DtListaDataPreConciliacion.Rows.Add(IdPreConciliacionArchivo, CodigoPago, CDec(item.Campo6), _
                                                                    item.Campo2.ToString, item.Campo5, 0, "")
                            ElseIf item.Campo4.ToString.StartsWith("1") Then
                                Dim pad As Char
                                Dim tempo2 As String = item.Campo4.ToString().Substring(0, 8).Replace(".", "")
                                pad = "0"
                                CodigoPago = tempo2.PadLeft(14, pad)
                                DtListaDataPreConciliacion.Rows.Add(IdPreConciliacionArchivo, CodigoPago, CDec(item.Campo6), _
                                                                    item.Campo2.ToString, item.Campo5, 0, "")
                            End If
                        End If
                    Next
                Catch ex As Exception
                    Throw New Exception("El archivo no tiene el formato correcto o no pertenece al banco establecido.")
                End Try
                If DtListaDataPreConciliacion.Rows.Count() = 0 Then
                    Throw New Exception("El archivo no tiene el formato correcto o no pertenece al banco establecido.")
                End If
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_RegistrarDetPreConciliacionBCP]" 'Tiene la misma estructura del BCP.
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@ListaDataPreConciliacion", SqlDbType.Structured).Value = DtListaDataPreConciliacion
                oSqlConnection.Open()
                oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub

    Public Function ProcesarDetArchivoPreConciliacion(ByRef request As BEPreConciliacionRequest) As List(Of BEConciliacionDetalle)
        Dim ListaBEConciliacionDetalle As New List(Of BEConciliacionDetalle)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ProcDetPreConciliacion", _
                                                                    request.Archivo.IdConciliacionArchivo)
                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("ValTimeOut")
                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    While reader.Read
                        ListaBEConciliacionDetalle.Add(New BEConciliacionDetalle(reader, 1, 1))
                    End While
                    reader.Close()
                End Using
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListaBEConciliacionDetalle
    End Function

#End Region


    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class