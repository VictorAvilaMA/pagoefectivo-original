Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class DACuenta
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    Public Function ConsultarMovimientoCuenta(ByVal be As BEMovimientoCuentaBanco) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obe As BEMovimientoCuenta = Nothing
        Dim Lista As New System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarMovimientoCuenta", be.IdMoneda, be.IdTipoMovimiento, be.FechaMovimientoDesde, be.FechaMovimientoHasta, be.PageNumber, be.PageSize, be.PropOrder, be.TipoOrder)
            While reader.Read
                Lista.Add(New BEMovimientoCuentaBanco(reader, "ConsultaTrazabilidad"))
            End While

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Lista

    End Function
    Public Function ConsultarDetMovCuentaPorIdMovCuentaBanco(ByVal be As BEDetMovimientoCuenta) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obe As BEMovimientoCuenta = Nothing
        Dim Lista As New System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarMovimientoCuentaPorId", be.IdMovimientoCuentaBanco)
            While reader.Read
                Lista.Add(New BEDetMovimientoCuenta(reader, "PorID"))
            End While

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Lista

    End Function
End Class
