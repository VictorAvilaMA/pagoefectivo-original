﻿Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Configuration




Public Class DADineroVirtual

#Region "Victor"

    Function RegistrarCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As New BECuentaDineroVirtual

        Try
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarCuentaDineroVirtual]", _
                                                        obecuentadinerovirtual.AliasCuenta, _
                                                        obecuentadinerovirtual.IdMoneda, _
                                                        obecuentadinerovirtual.IdUsuario)

            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    'Resultado = New BECuentaDineroVirtual
                    Resultado.ParametrosEmailMonedero = New BEParametrosEmailMonedero
                    Resultado.ParametrosEmailMonedero.NombreCliente = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("NombreCliente"))
                    Resultado.ParametrosEmailMonedero.ResultadoInt = _3Dev.FW.Util.DataUtil.ObjectToInt32(objIDataReader("Resultado"))
                    Resultado.ParametrosEmailMonedero.FechaExpiracion = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objIDataReader("FechaExpiracion"))
                    Resultado.ParametrosEmailMonedero.EmailCliente = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("EmailCliente"))
                    Resultado.ParametrosEmailMonedero.FechaSolicitud = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objIDataReader("FechaSolicitud"))
                    Resultado.ParametrosEmailMonedero.DescripcionMoneda = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("DescripcionMoneda"))
                    Resultado.ParametrosEmailMonedero.AliasCuenta = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("AliasCuenta"))
                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
            Resultado.ParametrosEmailMonedero.ResultadoInt = -1
        End Try
        Return Resultado
    End Function

    Function ActualizarEstadoCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer

        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarEstadoCuentaDineroVirtual]", _
                                                         obecuentadinerovirtual.IdCuentaDineroVirtual,
                                                         obecuentadinerovirtual.IdEstado,
                                                         obecuentadinerovirtual.ObservacionBloqueo, obecuentadinerovirtual.IdUsuario))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
            Resultado = -1
        End Try
        Return Resultado
    End Function

    Function ConsultarMovimientoXIdCuenta(ByVal entidad As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)
        Dim Resultado As New List(Of BEMovimientoCuenta)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarMovimientoXIdCuenta]", entidad.IdCuentaVirtual _
                                                                    , entidad.IdTipoMovimiento, entidad.FechaInicio, entidad.FechaFin _
                                                                    , entidad.PageNumber, entidad.PageSize, entidad.PropOrder, entidad.TipoOrder)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BEMovimientoCuenta(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Function ConsultarMovimientoXIdCuentaExportar(ByVal entidad As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)
        Dim Resultado As New List(Of BEMovimientoCuenta)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarMovimientoXIdCuentaExportar]", entidad.IdCuentaVirtual _
                                                                    , entidad.IdTipoMovimiento, entidad.FechaInicio, entidad.FechaFin)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BEMovimientoCuenta(objIDataReader, 1))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Function ActualizarMontoRecargaDineroVirtual(ByVal oBEMontoRecargaMonedero As BEMontoRecargaMonedero) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer

        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarMontoRecargaDineroVirtual]", _
                                                         oBEMontoRecargaMonedero.IdMoneda, _
                                                         oBEMontoRecargaMonedero.IdUsuario, _
                                                         oBEMontoRecargaMonedero.Monto))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
            Resultado = -1
        End Try
        Return Resultado
    End Function

    Function ConsultarMonedaXIdCliente(ByVal entidad As BEMontoRecargaMonedero) As List(Of BEMontoRecargaMonedero)
        Dim Resultado As New List(Of BEMontoRecargaMonedero)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarMonedaXIdCliente]", entidad.IdUsuario)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BEMontoRecargaMonedero(objIDataReader, 1))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Function ConsultarClienteXIdClienteDV(ByVal entidad As BECliente) As BECliente
        Dim Resultado As New List(Of BECliente)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarClienteXIdClienteDV]", entidad.IdCliente)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BECliente(objIDataReader, 1, "1"))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado(0)
    End Function

    Function ActualizarEstadoHabilitarMonederoClienteDV(ByVal oBECliente As BECliente) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer

        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarEstadoHabilitarMonedero]", _
                                                            oBECliente.IdCliente,
                                                            oBECliente.HabilitarMonedero,
                                                            oBECliente.Observacion))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
            Resultado = -1
        End Try
        Return Resultado
    End Function

    Public Function ConsultarMonedaMonederoVirtual() As List(Of BEMoneda)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listMoneda As New List(Of BEMoneda)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarMonedaMonederoVirtual")



                While reader.Read
                    '
                    listMoneda.Add(New BEMoneda(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return listMoneda
        '
    End Function

#End Region

#Region "Joe"

    Function ActualizarCuentaDineroVirtual(ByVal entidad As BECuentaDineroVirtual) As Boolean
        Dim Resultado As New List(Of BECuentaDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarCuentaDineroByID]", entidad.IdCuentaDineroVirtual, entidad.Observacion, entidad.IdUsuarioRespuesta, entidad.IdEstado)
            objDataBase.ExecuteNonQuery(db)
            Return True
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Function ConsultarCuentaDineroVirtualUsuarioByCuentaId(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual
        Dim Resultado As New List(Of BECuentaDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarCuentaDineroByID]", entidad.IdCuentaDineroVirtual)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BECuentaDineroVirtual().prc_ConsultarCuentaDineroVirtual(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado(0)
    End Function


    Function ConsultarCuentaDineroVirtualUsuario(ByVal entidad As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)
        Dim Resultado As New List(Of BECuentaDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarCuentaDineroVirtual]", entidad.IdEstado, entidad.IdMoneda, entidad.IdUsuario)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BECuentaDineroVirtual().prc_ConsultarCuentaDineroVirtual(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ConsultarSolicitudRetiroMonedero(ByVal entidad As BESolicitudRetiroDineroVirtual) As List(Of BESolicitudRetiroDineroVirtual)
        Dim Resultado As New List(Of BESolicitudRetiroDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            '  @IdCliente varchar(10),
            '  @Email varchar(50),
            '  @Desde datetime,
            '  @Hasta datetime,
            '  @Numero varchar(20),--numero de cuenta 
            '  @IdEstado varchar(20),

            '   @PAGE_NUMBER INT,
            '@PAGE_SIZE INT,
            '@PROP_ORDER VARCHAR(200),
            '@TIPO_ORDER BIT
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].prc_ConsultarSolicitudRetiroMonedero", entidad.IdCliente, entidad.ClienteNombre, entidad.Email, entidad.FechaInicio, entidad.FechaFin, entidad.Numero, entidad.IdEstado, entidad.PageNumber, entidad.PageSize, entidad.PropOrder, entidad.TipoOrder)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BESolicitudRetiroDineroVirtual().prc_ConsultarSolicitudRetiro(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ConsultarSolicitudRetiroMonederoById(ByVal entidad As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual
        Dim Resultado As New List(Of BESolicitudRetiroDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].prc_ConsultarSolicitudRetiroMonederoById", entidad.IdSolicitud)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BESolicitudRetiroDineroVirtual().prc_ConsultarSolicitudRetiro(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        If Resultado.Count > 0 Then
            Return Resultado(0)
        Else
            Return Nothing
        End If
    End Function

    Function ConsultarCuentaDineroVirtualUsuarioByNumero(ByVal cuenta As BECuentaDineroVirtual) As BECuentaDineroVirtual
        Dim Resultado As BECuentaDineroVirtual = Nothing
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].prc_ConsultarCuentaDineroByNumero", cuenta.Numero)
            db.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = New BECuentaDineroVirtual().prc_ConsultarCuentaDineroVirtual(objIDataReader)
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function


    Function ExtornarCuenta(ByVal cuenta As BECuentaDineroVirtual, ByVal oBEMovimiento As BEMovimiento) As Long

        Dim Resultado As Long

        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_GenerarExtornoDineroVirtual]", cuenta.IdCuentaDineroVirtual, _
                                                                   cuenta.IdUsuario, cuenta.IdMoneda, oBEMovimiento.CodigoServicio, cuenta.Numero, _
                                                                   oBEMovimiento.NumeroOperacion, oBEMovimiento.IdAperturaOrigen, oBEMovimiento.CodigoBanco, _
                                                                   oBEMovimiento.NumeroOperacionPago, oBEMovimiento.Monto)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader.GetInt64(0))
                End While
            End Using
            Return Resultado
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function



    Public Function RecargarCuenta(ByVal cuenta As BECuentaDineroVirtual, ByVal codigoServicio As String, ByVal oBEMovimiento As BEMovimiento) As Long
        Dim Resultado As Long

        Try

            '@CodigoServicio bigint,
            '@EquivalenciaBancaria varchar(15)

            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_GenerarRecargaDineroVirtual]", cuenta.IdCuentaDineroVirtual, _
                                                                   cuenta.MontoRecarga, cuenta.IdUsuario, cuenta.IdMoneda, oBEMovimiento.CodigoServicio, cuenta.Numero, _
                                                                   oBEMovimiento.NumeroOperacion, oBEMovimiento.IdAperturaOrigen, oBEMovimiento.CodigoBanco)
            db.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = objIDataReader.GetInt64(0)
                End While
            End Using
            Return Resultado
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    Public Function ActualizarSolicitudRetiroDinero(ByVal entidad As BESolicitudRetiroDineroVirtual) As Boolean
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            '@IdSolicitud VARCHAR(20),
            '@IdUsuario int,
            '@IdEstadoSolicitudRetiroDinero int,
            '@ObservacionAprobacion varchar(200),
            '@ObservacionLiquidacion varchar(200),
            '@RutaDocAdjFtp varchar(100),
            '@NombreDocAdjFtp varchar(100),
            '@NombreInicialDocAdj varchar(100)

            objDataBase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarSolicitudRetiroDinero]", _
                                                            entidad.IdSolicitud, _
                                                         entidad.IdUsuario, _
                                                         entidad.IdEstado, _
                                                         entidad.ObservacionAdministrador, _
                                                        entidad.ObservacionTesoreria, _
                                                        entidad.RutaDocAdjFtp, _
                                                        entidad.NombreDocAdjFtp, _
                                                                entidad.NombreInicialDocAdj, entidad.NumeroCuentaComercioId
                                                                )
            Return True
        Catch ex As Exception
            'Logger.Write(ex)
            Return False
        End Try

    End Function


#End Region

    Function ObtenerParametroMonederoByID(ByVal IdParametro) As BEParametroMonedero
        Dim Resultado As New List(Of BECuentaDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.usp_ObtenerParametroMonederoByID", IdParametro)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Return New BEParametroMonedero(objIDataReader)
                End While
            End Using
            Return Nothing
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function


#Region "IMPLEMENTADO POR MXRT¡N"
    Function ConsultarCuentaDineroVirtualAdministrador(ByVal entidad As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)
        Dim Resultado As New List(Of BECuentaDineroVirtual)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarCuentasAdmin]",
                        If(String.IsNullOrEmpty(entidad.ClienteNombre), Nothing, entidad.ClienteNombre),
                        If(String.IsNullOrEmpty(entidad.Email), Nothing, entidad.Email),
                        If(entidad.FechaSolicitud = DateTime.MinValue, Nothing, entidad.FechaSolicitud),
                        If(entidad.FechaAprobacion = DateTime.MinValue, Nothing, entidad.FechaAprobacion),
                        entidad.IdMoneda,
                        entidad.IdEstado,
                        entidad.PageNumber,
                        entidad.PageSize,
                        entidad.PropOrder,
                        entidad.TipoOrder)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado.Add(New BECuentaDineroVirtual(objIDataReader))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ObtenerOrdenPagoCompletoPorNroOrdenPago(ByVal nroOrdenPago As Long)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOrdenPago As BEOrdenPago = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenPagoPorNroOrdenPago", nroOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "Completo")
                    obeOrdenPago.IdEstadoConciliacion = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("IdEstadoConciliacion"))

                    obeOrdenPago.ListaTipoOrigenCancelacion = New DAServicio().ConsultarTiposOrigenesCancelacionPorIdServicio(obeOrdenPago.IdServicio)

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function

    Public Function RegistrarTokenMonedero(ByVal token As BETokenDineroVirtual) As BETokenDineroVirtual
        Dim Resultado As BETokenDineroVirtual
        Resultado = Nothing
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarTokenMonedero]", token.IdCuenta, token.IdTipoOperacion)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = New BETokenDineroVirtual
                    Resultado.ParametrosEmailMonedero = New BEParametrosEmailMonedero
                    Resultado.IdTokenDineroVirtual = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("IdToken"))
                    Resultado.Token = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Token"))
                    Resultado.FechaCaducidad = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("TiempoExpiracion"))
                    Resultado.ParametrosEmailMonedero.NombreCliente = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("NombreCliente"))
                    Resultado.ParametrosEmailMonedero.TipoOperacion = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("TipoOperacion"))
                    'Resultado.ParametrosEmailMonedero.TiempoExpiracion = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("TiempoExpiracion"))
                    Resultado.ParametrosEmailMonedero.EmailCliente = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Email"))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function RegistrarPagoCIP(ByVal beMovimiento As BEMovimientoCuenta) As BEMovimientoCuenta
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarPagoCIPDesdeMonedero]",
                beMovimiento.IdCuentaVirtual,
                beMovimiento.IdTipoMovimiento,
                beMovimiento.Observacion,
                beMovimiento.IdTokenDineroVirtual,
                beMovimiento.CodigoReferencia,
                beMovimiento.ConceptoPago,
                beMovimiento.IdUsuario,
                beMovimiento.NroToken)

            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    Dim oBeMovCuenta As New BEMovimientoCuenta
                    oBeMovCuenta.Observacion = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
                    oBeMovCuenta.IdMovimientoAsociado = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("IdMovimientoOP"))
                    oBeMovCuenta.FechaMovimiento = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objIDataReader("FechaMovimiento"))
                    oBeMovCuenta.NumeroOperacion = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("IdMovimientoDV"))

                    If objIDataReader("NumeroCta") = String.Empty Then
                        oBeMovCuenta.IdCuentaVirtual = 0
                    Else : oBeMovCuenta.IdCuentaVirtual = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("NumeroCta"))
                    End If
                    Return oBeMovCuenta
                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Nothing
    End Function

    Public Function RegistrarPagoCIPPortal(ByVal beMovimiento As BEMovimientoCuenta) As BEMovimientoCuenta
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarPagoCIPPortalDesdeMonedero]",
                beMovimiento.IdCuentaVirtual,
                beMovimiento.IdMovimientoAsociado,
                beMovimiento.IdUsuario,
                beMovimiento.IdTokenDineroVirtual,
                beMovimiento.NroToken)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    Dim oBeMovCuenta As New BEMovimientoCuenta
                    oBeMovCuenta.Observacion = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
                    oBeMovCuenta.NombrePortal = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Empresa")) 'Razon Social de la Empresa
                    oBeMovCuenta.FechaMovimiento = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objIDataReader("FechaMovimiento"))
                    oBeMovCuenta.NumeroOperacion = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("IdMovimientoDV"))

                    If objIDataReader("NumeroCta") = String.Empty Then
                        oBeMovCuenta.IdCuentaVirtual = 0
                    Else : oBeMovCuenta.IdCuentaVirtual = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("NumeroCta"))
                    End If
                    Return oBeMovCuenta
                    'Return _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Nothing
    End Function

    'Public Function RegistrarPagoCIPPortal(ByVal beMovimiento As BEMovimientoCuenta) As String
    '    Try
    '        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

    '        Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarPagoCIPPortalDesdeMonedero]",
    '            beMovimiento.IdCuentaVirtual,
    '            beMovimiento.IdMovimientoAsociado,
    '            beMovimiento.IdUsuario,
    '            beMovimiento.IdTokenDineroVirtual,
    '            beMovimiento.NroToken)
    '        Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
    '            If objIDataReader.Read() Then
    '                Return _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
    '            End If
    '        End Using

    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        Throw ex
    '    End Try
    '    Return Nothing
    'End Function


    Public Function RegistrarSolicitudRetiroDinero(ByVal beSolicitudRetiroDinero As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarSolicitudRetiroMonedero]",
                beSolicitudRetiroDinero.IdCuenta,
                beSolicitudRetiroDinero.MontoSolicitado,
                beSolicitudRetiroDinero.IdBanco,
                beSolicitudRetiroDinero.CuentaBancariaDestino,
                beSolicitudRetiroDinero.TitularCuentaBancariaDestino,
                beSolicitudRetiroDinero.IdToken,
                beSolicitudRetiroDinero.NroToken,
                beSolicitudRetiroDinero.IdUsuario)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    Dim objSol As New BESolicitudRetiroDineroVirtual
                    objSol.ObservacionAdministrador = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
                    objSol.IdSolicitud = _3Dev.FW.Util.DataUtil.ObjectToInt64(objIDataReader("IdSolicitud"))
                    Return objSol
                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Nothing
    End Function

    Function ConsultarCuentaDineroVirtualPorNroCuenta(ByVal NroCuenta As String) As BECuentaDineroVirtual
        Dim Resultado As New BECuentaDineroVirtual
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarCuentaDineroVirtualXNroCuenta]", NroCuenta)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    Resultado = New BECuentaDineroVirtual(objIDataReader, 0)
                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function RegistrarTransferenciaDinero(ByVal beMovimiento As BEMovimientoCuenta) As BEParametrosEmailMonedero
        Try
            Dim entidad As New BEParametrosEmailMonedero
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarTransferencia]",
                beMovimiento.IdCuentaVirtual,
                beMovimiento.IdMovimientoAsociado,
                beMovimiento.Monto,
                beMovimiento.IdTokenDineroVirtual,
                beMovimiento.IdUsuario,
                beMovimiento.NroToken)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    If Not _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje")) = String.Empty Then
                        entidad.AliasCuenta = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
                    Else
                        entidad.CuentaOrigen = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("CuentaOrigen"))
                        entidad.CuentaDestino = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("CuentaDestino"))
                        entidad.TitularCuentaOrigen = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("TitularCuentaOrigen"))
                        entidad.TitularCuentaDestino = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("TitularCuentaDestino"))
                        entidad.Monto = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Monto"))
                        entidad.MontoComision = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("MontoComision"))
                        entidad.PTo = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("PTo"))
                        entidad.PFrom = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("PFrom"))
                        entidad.AliasCuenta = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("Mensaje"))
                        entidad.FechaRegistro = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objIDataReader("FechaTransaccion"))
                        entidad.SimboloMoneda = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("SimboloMoneda"))
                        entidad.NumeroOrdenPago = _3Dev.FW.Util.DataUtil.ObjectToString(objIDataReader("IdMovimiento"))
                        entidad.ObservacionTransferencia = _3Dev.FW.Util.DataUtil.ObjectToString(beMovimiento.ObservacionTransferencia)
                    End If
                    Return entidad
                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Nothing
    End Function

    Function ConsultarReglaMovimientoSospechoso() As List(Of BEReglaMovimientoSospechoso)
        Dim Lista As New List(Of BEReglaMovimientoSospechoso)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarReglaMovimientoSospechoso]")
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Lista.Add(New BEReglaMovimientoSospechoso(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Lista
    End Function

    Function ConsultarRegistroEjecucion(ByVal IdRegla As Integer, ByVal Anio As Integer, ByVal Mes As Integer) As List(Of BERegistroEjecucionRegla)
        Dim Lista As New List(Of BERegistroEjecucionRegla)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarRegistroEjecucionRegla]", IdRegla, Anio, Mes)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Lista.Add(New BERegistroEjecucionRegla(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Lista
    End Function

    Function ConsultarResultadoEjecucionRegla(ByVal entidad As BEResultadoEjecucionRegla) As List(Of BEResultadoEjecucionRegla)
        Dim Lista As New List(Of BEResultadoEjecucionRegla)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarResultadoEjecucionRegla]", entidad.IdRegistro, entidad.PageNumber, entidad.PageSize, entidad.PropOrder, entidad.TipoOrder)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Lista.Add(New BEResultadoEjecucionRegla(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Lista
    End Function

    Function ConsultarDetalleResultadoEjecucion(ByVal entidad As BEDetalleResultadoEjecucion) As List(Of BEDetalleResultadoEjecucion)
        Dim Lista As New List(Of BEDetalleResultadoEjecucion)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarDetalleResultadoEjecucion]", entidad.IdResultado, entidad.PageNumber, entidad.PageSize, entidad.PropOrder, entidad.TipoOrder)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Lista.Add(New BEDetalleResultadoEjecucion(objIDataReader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Lista
    End Function

    Public Function ObtenerSolicitudRetiroEmail(ByVal entidad As BESolicitudRetiroDineroVirtual) As BEParametrosEmailMonedero
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeEmail As BEParametrosEmailMonedero = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ObtenerSolicitudRetiroEmail", entidad.IdCuenta)

                While reader.Read()
                    obeEmail = New BEParametrosEmailMonedero(reader, "SolicitudRetiroEmail")
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeEmail
    End Function

    Public Function ObtenerSolicitudPagoPortal(ByVal entidad As BEMovimientoCuenta) As BEParametrosEmailMonedero
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeEmail As BEParametrosEmailMonedero = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ObtenerSolicitudPagoPortal", entidad.IdMovimientoAsociado)

                While reader.Read()
                    obeEmail = New BEParametrosEmailMonedero(reader, "SolicitudPagoPortal")
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeEmail
    End Function

    Public Function ObtenerTokenMonedero(ByVal IdTokenMonedero As Long) As BETokenDineroVirtual
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As BETokenDineroVirtual = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ObtenerTokenMonedero]", IdTokenMonedero)

                If reader.Read() Then

                    Resultado = New BETokenDineroVirtual
                    Resultado.ParametrosEmailMonedero = New BEParametrosEmailMonedero
                    Resultado.IdTokenDineroVirtual = _3Dev.FW.Util.DataUtil.ObjectToInt64(reader("IdTokenMonedero"))
                    Resultado.IdCuenta = _3Dev.FW.Util.DataUtil.ObjectToInt64(reader("IdCuenta"))
                    Resultado.IdTipoOperacion = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("IdTipoOperacion"))
                    Resultado.Token = _3Dev.FW.Util.DataUtil.ObjectToString(reader("Token"))
                    Resultado.FechaCreacion = _3Dev.FW.Util.DataUtil.ObjectToDateTime(reader("FechaCreacion"))
                    Resultado.FechaCaducidad = _3Dev.FW.Util.DataUtil.ObjectToDateTime(reader("FechaCaducidad"))
                    Resultado.Activo = _3Dev.FW.Util.DataUtil.ObjectToBool(reader("Activo"))
                    Resultado.Utilizado = _3Dev.FW.Util.DataUtil.ObjectToBool(reader("Utilizado"))

                    Resultado.ParametrosEmailMonedero.NombreCliente = _3Dev.FW.Util.DataUtil.ObjectToString(reader("NOMBRE_CLIENTE"))
                    Resultado.ParametrosEmailMonedero.EmailCliente = _3Dev.FW.Util.DataUtil.ObjectToString(reader("EMAIL_CLIENTE"))
                    Resultado.ParametrosEmailMonedero.TipoOperacion = _3Dev.FW.Util.DataUtil.ObjectToString(reader("TipoOperacion"))
                    Resultado.ParametrosEmailMonedero.TiempoExpiracion = _3Dev.FW.Util.DataUtil.ObjectToString(reader("TiempoExpiracion"))

                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function


#End Region

    Public Function ValidarRegistrarCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obResultado As BECuentaDineroVirtual = Nothing

        Try

            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ValidarRegistrarCuentaDineroVirtual]", obecuentadinerovirtual.IdUsuario)

                While reader.Read
                    obResultado = New BECuentaDineroVirtual()
                    obResultado.IdEstado = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("Resultado"))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obResultado

    End Function


    Function OperacionCuenta(ByVal idUSuario As Long, ByVal Operacion As String) As Boolean
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_OperacionPrimeraCuenta]", idUSuario, Operacion)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read
                    Return objIDataReader.GetBoolean(0)
                End While
            End Using
            Throw New Exception("Custom message: No Data")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    Function ConsultaEstadoUsuarioByIdUsuario(IdUsuario As Integer) As Integer
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultaUsuarioEstadoByIdCuenta]", IdUsuario)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read
                    Return objIDataReader.GetInt32(0)
                End While
            End Using
            Throw New Exception("Custom message: No Data")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function



    Function ConsultaUsuarioByIdCuenta(IdCuenta As Integer) As Integer
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultaUsuarioByIdCuenta]", IdCuenta)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read
                    Return objIDataReader.GetInt32(0)
                End While
            End Using
            Throw New Exception("Custom message: No Data")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Function ConsultarCuentaDineroByNumeroWS(ByVal cuenta As BECuentaDineroVirtual, ByVal ServicioMonedero As String, ByVal CodBan As String) As BECuentaDineroVirtual
        Dim Resultado As BECuentaDineroVirtual = Nothing
        Try
            'M
            'End M
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarCuentaDineroByNumeroWS]", cuenta.Numero, ServicioMonedero, CodBan)
            db.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = New BECuentaDineroVirtual().prc_ConsultarCuentaDineroVirtual(objIDataReader)
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function


#Region "Unibanca"

    Public Function ConsultarSolicitudRecargaComun(ByVal NroCuenta As String) As BERecarga
        Dim Resultado As New BERecarga
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionPESaldos)


            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[dbo].[spl_regMontoRecargas]", NroCuenta)
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                If objIDataReader.Read() Then
                    Resultado = New BERecarga(objIDataReader, "ConsultarSolicitudRecarga")
                End If
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function RecargarCuentaUnibancaComun(ByVal cuenta As BERecarga) As Long
        Dim Resultado As Long

        Try

            '@CodigoServicio bigint,
            '@EquivalenciaBancaria varchar(15)

            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionPESaldos)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[dbo].[spl_addRecargas]", cuenta.gUsuario, _
                                                                   cuenta.xCIPRecarga, cuenta.xCodBancoPE, cuenta.xNumOperacionEntidad, cuenta.iMoneda, cuenta.mMonto)
            db.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = objIDataReader.GetInt64(0)
                End While
            End Using
            Return Resultado
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function AnularRecargaUnibancaComun(ByVal cuenta As BERecarga) As Long
        Dim Resultado As Long

        Try

            '@CodigoServicio bigint,
            '@EquivalenciaBancaria varchar(15)

            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionPESaldos)
            Dim db As DbCommand = objDataBase.GetStoredProcCommand("[dbo].[spl_modRecargasXExtorno]", cuenta.gUsuario, _
                                                                   cuenta.xCIPRecarga, cuenta.xCodBancoPE, cuenta.xNumOperacionEntidad, cuenta.xNumExtornoEntidad, cuenta.iMoneda, cuenta.mMonto)
            db.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using objIDataReader As IDataReader = objDataBase.ExecuteReader(db)
                While objIDataReader.Read()
                    Resultado = objIDataReader.GetInt64(0)
                End While
            End Using
            Return Resultado
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function
#End Region


End Class
