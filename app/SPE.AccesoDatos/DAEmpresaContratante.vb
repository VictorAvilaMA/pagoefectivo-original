Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common

Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades.BusinessMessageBase
Imports _3Dev.FW.Entidades

Imports SPE.Entidades
Imports System.Configuration
Imports SPE.Logging

Public Class DAEmpresaContratante
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    Private ReadOnly _logger As ILogger = New Logger()
    'REGISTRAR EMPRESA CONTRATANTE
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeEmpresa As BEEmpresaContratante = CType(be, BEEmpresaContratante)

        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarEmpresaContratante]", obeEmpresa.Codigo, obeEmpresa.RazonSocial, _
            obeEmpresa.RUC, obeEmpresa.Contacto, obeEmpresa.Telefono, obeEmpresa.Fax, obeEmpresa.Direccion, _
            obeEmpresa.SitioWeb, obeEmpresa.Email, obeEmpresa.IdCiudad, obeEmpresa.IdEstado, obeEmpresa.IdUsuarioCreacion, _
            obeEmpresa.ImagenLogo, _3Dev.FW.Util.DataUtil.BoolToString(obeEmpresa.OcultarEmpresa), obeEmpresa.HoraCorte, obeEmpresa.IdPeriodoLiquidacion))
            obeEmpresa.IdEmpresaContratante = Resultado
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado

    End Function

    'ACTUALIZAR EMPRESA CONTRATANTE
    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeEmpresa As BEEmpresaContratante = CType(be, BEEmpresaContratante)

        Try

            Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizaREmpresaContratante]", obeEmpresa.IdEmpresaContratante, obeEmpresa.Codigo, _
            obeEmpresa.RazonSocial, obeEmpresa.RUC, obeEmpresa.Contacto, obeEmpresa.Telefono, obeEmpresa.Fax, obeEmpresa.Direccion, _
            obeEmpresa.SitioWeb, obeEmpresa.Email, obeEmpresa.IdCiudad, obeEmpresa.IdUsuarioActualizacion, obeEmpresa.IdEstado, _
            obeEmpresa.ImagenLogo, _3Dev.FW.Util.DataUtil.BoolToString(obeEmpresa.OcultarEmpresa), obeEmpresa.HoraCorte, obeEmpresa.IdPeriodoLiquidacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try

        Return Resultado

    End Function

    'CONSULTAR EMPRESA CONTRATANTE
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListEmpresa As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeEmpresa As BEEmpresaContratante = CType(be, BEEmpresaContratante)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarEmpresaContratante", _
            obeEmpresa.Codigo, obeEmpresa.RazonSocial, obeEmpresa.RUC, obeEmpresa.Contacto, obeEmpresa.IdEstado, obeEmpresa.Email, obeEmpresa.IdPeriodoLiquidacion)

                While reader.Read()
                    ListEmpresa.Add(New BEEmpresaContratante(reader, "busq"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListEmpresa

    End Function

    ''' <summary>
    '''  Consultar Empresa Contratante Por id Empresa
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeEmpresa As New BEEmpresaContratante

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarEmpresaContratantePorIdEmpresa]", CInt(id))
                If reader.Read Then

                    obeEmpresa = New BEEmpresaContratante(reader, "porID")

                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return obeEmpresa

    End Function
    ''' <summary>
    ''' Consultar Empresa Contratante Por id Empresa
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function GetEntityByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As New BusinessMessageBase()
        response.Entity = GetRecordByID(id)
        Return response
    End Function

    Public Function OcultarEmpresa(ByVal idservicio As Integer) As BEOcultarEmpresa
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOcultaEmpresa As New BEOcultarEmpresa

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarEmpresaContratanteOcultarImagen]", CInt(idservicio))
                If reader.Read Then

                    obeOcultaEmpresa = New BEOcultarEmpresa(reader)

                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return obeOcultaEmpresa
    End Function
    Public Overrides Function DeleteEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As New _3Dev.FW.Entidades.BusinessMessageBase()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = ObjectToInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarEmpresaContratante]", ObjectToInt(request.EntityId)))
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok
        Catch ex As Exception
            'Logger.Write(ex)
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Error
            Throw ex
        End Try
        Return response
    End Function

    Public Function ConsultarCantidadEmpresaContratantePorCodigo(ByVal codigo As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ExisteCodigoEmpresaContratante]", codigo))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    'Transferencia 
    Public Function RegistrarTransaccionesaLiquidar(ByVal be As BETransferencia) As Integer

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim FechaDeposito As Object
        If be.FechaDeposito = Date.MinValue Then
            FechaDeposito = Nothing
        Else
            FechaDeposito = be.FechaDeposito
        End If
        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarTransaccionesaLiquidar]", be.idEmpresaContratante, be.IdMoneda _
                                                       , be.FechaInicio, be.FechaFin, be.IdUsuarioCreacion, be.TotalOperaciones, be.TotalPago, be.TotalComision, _
                                                       be.Estado, be.DescripcionBanco, FechaDeposito, be.NumeroOperacion, be.NumeroCuenta, be.Observacion, be.ArchivoAdjunto))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado

    End Function

    Public Function ConsultarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
        Dim ListTransferencia As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeTransferencia As SPE.Entidades.BETransferencia = CType(be, SPE.Entidades.BETransferencia)
        Try
            'Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarTransaccionesaLiquidarPG]", obeTransferencia.idEmpresaContratante, obeTransferencia.IdMoneda, obeTransferencia.IdEstado, obeTransferencia.FechaInicio, obeTransferencia.FechaFin, obeTransferencia.PageNumber, obeTransferencia.PageSize, obeTransferencia.PropOrder, obeTransferencia.TipoOrder)
            Using oDbCommand As DbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarTransaccionesaLiquidar]", obeTransferencia.idEmpresaContratante, obeTransferencia.IdMoneda, obeTransferencia.IdEstado, obeTransferencia.FechaInicio, obeTransferencia.FechaFin, obeTransferencia.PageNumber, obeTransferencia.PageSize, obeTransferencia.PropOrder, obeTransferencia.TipoOrder)
                oDbCommand.CommandTimeout = ConfigurationManager.AppSettings("TimeoutSQLReplica")
                Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                    While reader.Read()
                        ListTransferencia.Add(New SPE.Entidades.BETransferencia(reader, "ConsTrans"))
                    End While
                End Using
            End Using
        Catch ex As Exception
            _logger.Error(ex, String.Format("Message:{0}, idEmpresaContratante:{1}", ex.Message, obeTransferencia.idEmpresaContratante))
            Throw ex
        End Try
        Return ListTransferencia
    End Function

    Public Function ObtenerFechaInicioTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As New DateTime
        Resultado = Nothing
        Try
            Resultado = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerFechaInicioTransaccionesaLiquidar]", be.idEmpresaContratante, be.IdMoneda))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado

    End Function



    Public Function ObtenerFechaInicioTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As New DateTime
        Resultado = Nothing
        Try
            Resultado = _3Dev.FW.Util.DataUtil.ObjectToDateTime(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerFechaInicioTransaccionesaLiquidarLote]"))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado

    End Function



    Public Function ObtenerFechaFinTransaccionesaLiquidar(ByVal be As BETransferencia) As BEEmpresaPeriodo
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As BEEmpresaPeriodo = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ObtenerFechaFinTransaccionesaLiquidar]", be.idEmpresaContratante)
                While reader.Read()
                    Resultado = New BEEmpresaPeriodo(reader, "porIDEmpresa")
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function




    Public Function ObtenerFechaFinTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As BEPeriodoLiquidacion
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As BEPeriodoLiquidacion = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ObtenerFechaFinTransaccionesaLiquidarLote]", be.IdPeriodo)
                While reader.Read()
                    Resultado = New BEPeriodoLiquidacion(reader, "PeriodoLiquidacion")
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ActualizarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeTransferencia As BETransferencia = CType(be, BETransferencia)
        Dim FechaDeposito As Object
        If obeTransferencia.FechaDeposito = Date.MinValue Then
            FechaDeposito = Nothing
        Else
            FechaDeposito = obeTransferencia.FechaDeposito
        End If
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarTransaccionesaLiquidar]", obeTransferencia.idTransfenciaEmpresas, obeTransferencia.IdUsuarioActualizacion, _
            obeTransferencia.DescripcionBanco, FechaDeposito, obeTransferencia.NumeroOperacion, obeTransferencia.NumeroCuenta, obeTransferencia.Observacion, _
            obeTransferencia.ArchivoAdjunto, obeTransferencia.IdEstado))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try

        Return Resultado

    End Function
    Public Function ObtenerTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BETransferencia
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeResultado As BETransferencia = Nothing
        Dim obeTransferencia As BETransferencia = CType(be, BETransferencia)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ObtenerTransaccionesaLiquidarxId", obeTransferencia.idTransfenciaEmpresas)

                While reader.Read()
                    obeResultado = New BETransferencia(reader, "ObtxId")
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeResultado
    End Function

    Public Function ValidarPendientesaConciliar(ByVal be As BETransferencia) As Integer

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            'Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ValidarPendientesaConciliar]", be.idEmpresaContratante, be.IdMoneda _
            '                                          , be.FechaInicio, be.FechaFin))

            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ValidarPendientesaConciliar]", be.idEmpresaContratante, be.IdMoneda _
                                                      , be.FechaInicio, be.FechaFin)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("ValTimeOut"))
            Resultado = CInt(objDataBase.ExecuteScalar(oDbCommand))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function

#Region "Configuración de cuenta y banco de empresas"
    Public Function InsertarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeCuentaEmpresa As BECuentaEmpresa = CType(be, BECuentaEmpresa)
        Try
            Resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarCuentaEmpresa]", _
                                                         obeCuentaEmpresa.IdEmpresaContratante, _
                                                         obeCuentaEmpresa.IdMoneda, _
                                                         obeCuentaEmpresa.Banco, _
                                                         obeCuentaEmpresa.NroCuenta, _
                                                         obeCuentaEmpresa.IdEstado, _
                                                         obeCuentaEmpresa.IdUsuarioCreacion, _
                                                         obeCuentaEmpresa.IdTipoCuenta))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Function ActualizarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeCuentaEmpresa As BECuentaEmpresa = CType(be, BECuentaEmpresa)
        Try
            Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarCuentaEmpresa]", _
                                                         obeCuentaEmpresa.IdCuentaEmpresa, _
                                                         obeCuentaEmpresa.IdEmpresaContratante, _
                                                         obeCuentaEmpresa.IdMoneda, _
                                                         obeCuentaEmpresa.Banco, _
                                                         obeCuentaEmpresa.NroCuenta, _
                                                         obeCuentaEmpresa.IdEstado, _
                                                         obeCuentaEmpresa.IdUsuarioActualizacion, _
                                                         obeCuentaEmpresa.IdTipoCuenta))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Function ConsultarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of BusinessEntityBase)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListCuentaEmpresa As New List(Of BusinessEntityBase)
        Dim obeCuentaEmpresa As BECuentaEmpresa = CType(be, BECuentaEmpresa)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCuentaEmpresa]", _
                                                                    obeCuentaEmpresa.IdEmpresaContratante, _
                                                                    IIf(obeCuentaEmpresa.NombreEmpresaContratante = Nothing, "", obeCuentaEmpresa.NombreEmpresaContratante), _
                                                                    obeCuentaEmpresa.IdMoneda, _
                                                                    IIf(obeCuentaEmpresa.Banco = Nothing, "", obeCuentaEmpresa.Banco), _
                                                                    IIf(obeCuentaEmpresa.NroCuenta = Nothing, "", obeCuentaEmpresa.NroCuenta), _
                                                                    obeCuentaEmpresa.IdEstado, _
                                                                    obeCuentaEmpresa.PageNumber, _
                                                                    obeCuentaEmpresa.PageSize, _
                                                                    IIf(obeCuentaEmpresa.PropOrder = Nothing, "", obeCuentaEmpresa.PropOrder), _
                                                                    obeCuentaEmpresa.TipoOrder)
                While reader.Read()
                    ListCuentaEmpresa.Add(New BECuentaEmpresa(reader, "busq"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListCuentaEmpresa
    End Function
    Public Function ConsultarCuentaEmpresaPorId(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeCuentaEmpresa As New BECuentaEmpresa
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCuentaEmpresaPorId]", CInt(id))
                If reader.Read Then
                    obeCuentaEmpresa = New BECuentaEmpresa(reader, "porID")
                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeCuentaEmpresa
    End Function
#End Region


#Region "LiquidacionLotes"



    Public Function RecuperarEmpresasLiquidacion(ByVal FechaDe As Date, ByVal FechaAl As Date, ByVal idPeriodoLiquidacion As Integer, ByVal idMoneda As Integer) As System.Collections.Generic.List(Of BEEmpresaContratante)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListaEmpresas As New List(Of BEEmpresaContratante)

        Try
            'Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarTransaccionesaLiquidarPG]", obeTransferencia.idEmpresaContratante, obeTransferencia.IdMoneda, obeTransferencia.IdEstado, obeTransferencia.FechaInicio, obeTransferencia.FechaFin, obeTransferencia.PageNumber, obeTransferencia.PageSize, obeTransferencia.PropOrder, obeTransferencia.TipoOrder)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prcRecuperarEmpresasLiquidacion]", FechaDe, FechaAl, idPeriodoLiquidacion, idMoneda)
                While reader.Read()
                    ListaEmpresas.Add(New SPE.Entidades.BEEmpresaContratante(reader, "LiqLot"))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListaEmpresas
    End Function


#End Region
End Class
