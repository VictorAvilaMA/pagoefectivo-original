Imports System.Data.Common
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades


Public Class DAEstablecimiento
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    '************************************************************************************************** 
    ' M�todo          : InsertRecord
    ' Descripci�n     : Registro de un Establecimiento
    ' Autor           : Christian Villanueva Nicho
    ' Fecha/Hora      : 28/12/2008
    ' Parametros_In   : Instancia de la Entidad BEEstablecimiento cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeEstablecimiento As BEEstablecimiento = CType(be, BEEstablecimiento)
        Try

            resultado = objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarEstablecimiento", _
            obeEstablecimiento.SerieTerminal, obeEstablecimiento.Descripcion, obeEstablecimiento.IdEstado, _
            obeEstablecimiento.IdOperador, obeEstablecimiento.IdUsuarioCreacion, obeEstablecimiento.idComercio)

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : UpdateRecord
    ' Descripci�n     : Actualizacion de un Establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 13/02/2009
    ' Parametros_In   : Instancia de la Entidad BEEstablecimiento cargada
    ' Parametros_Out  : Resultado de la transaccion
    '*************************************************************************************************
    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeEstablecimiento As BEEstablecimiento = CType(be, BEEstablecimiento)
        Try
            resultado = objDatabase.ExecuteScalar("PagoEfectivo.prc_ActualizarEstablecimiento", _
            obeEstablecimiento.IdEstablecimiento, obeEstablecimiento.SerieTerminal, _
            obeEstablecimiento.IdOperador, obeEstablecimiento.IdUsuarioActualizacion, obeEstablecimiento.IdEstado, obeEstablecimiento.Descripcion, obeEstablecimiento.idComercio)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : SearchByParameters
    ' Descripci�n     : consulta de Establecimiento
    ' Autor           : Christian Villanueva Nicho
    ' Fecha/Hora      : 28/12/2008
    ' Parametros_In   : Instancia de la Entidad BEEstablecimiento cargada
    ' Parametros_Out  : Lista de Entidades del tipo BEEstablecimiento
    ''*************************************************************************************************
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListEstablecimiento As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeEstablecimiento As BEEstablecimiento = CType(be, BEEstablecimiento)
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarEstablecimientos", _
            obeEstablecimiento.SerieTerminal, obeEstablecimiento.Descripcion, _
             obeEstablecimiento.IdOperador, obeEstablecimiento.IdEstado, _
            obeEstablecimiento.DescripcionComercio)
                While reader.Read()
                    ListEstablecimiento.Add(New BEEstablecimiento(reader, "busq"))
                End While
            End Using

        Catch ex As Exception

            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListEstablecimiento
    End Function

    '************************************************************************************************** 
    ' M�todo          : GetRecordByID
    ' Descripci�n     : consulta de Establecimiento por Id Establecimiento
    ' Autor           : Enrique Rojas 
    ' Fecha/Hora      : 13/02/2009
    ' Parametros_In   : Id del Comercio
    ' Parametros_Out  : Lista de Entidades del tipo BEEstablecimiento
    ''*************************************************************************************************
    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeEstablecimiento As New BEEstablecimiento
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarEstablecimientoPorIdEstablecimiento]", CInt(id))
                If reader.Read() Then
                    obeEstablecimiento = New BEEstablecimiento(reader, "busq")
                End If
            End Using

        Catch ex As Exception

            'Logger.Write(ex)
            Throw ex

        End Try

        Return obeEstablecimiento

    End Function

    Public Function ObtenerOperadores() As List(Of BEOperador)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOperadores As New List(Of BEOperador)
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ObtenerOperadores")

                While reader.Read()
                    ListOperadores.Add(New BEOperador(reader, "ObtenerOperadores"))
                End While

            End Using

        Catch ex As Exception

            'Logger.Write(ex)
            Throw ex

        End Try

        Return ListOperadores
    End Function

    Public Function ConsultarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = ""
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoPOS", obeParametroPOS.NroEstablecimiento, _
            obeParametroPOS.NroOrdenPago, obeParametroPOS.codComercio)
                While reader.Read()
                    Respuesta = Respuesta + reader(0).ToString()
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Respuesta
    End Function

    Public Function CancelarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As BEOPPOSResultado
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        'Dim Respuesta As String = ""
        Dim obeOPPOSResultado As BEOPPOSResultado = Nothing
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_CancelarOrdenPago]", obeParametroPOS.codComercio, obeParametroPOS.NroEstablecimiento, _
            obeParametroPOS.NroOperacion, obeParametroPOS.NroOrdenPago, _
            obeParametroPOS.Monto, obeParametroPOS.IdMoneda, obeParametroPOS.IdMedioPago)
                While reader.Read()
                    'Respuesta = Respuesta + reader(0).ToString()
                    obeOPPOSResultado = New BEOPPOSResultado(reader)
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeOPPOSResultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : Anular C�digo de Identificaci�n de Pago
    ' Descripci�n     : Anula la cancelacion de una C�digo de Identificaci�n de Pago
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 19/02/2009
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function AnularOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As BEOPPOSResultado
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        'Dim Respuesta As String = ""
        Dim obeOPPOSResultado As BEOPPOSResultado = Nothing
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_AnularOperacionPOS]", obeParametroPOS.codComercio, obeParametroPOS.NroEstablecimiento, obeParametroPOS.NroOperacion, _
                                          obeParametroPOS.NroOperacionAnular, obeParametroPOS.IdMedioPago, obeParametroPOS.NroOrdenPago)
                While reader.Read()
                    'Respuesta = Respuesta + reader(0).ToString()
                    obeOPPOSResultado = New BEOPPOSResultado(reader)
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeOPPOSResultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja de un Establecimiento
    ' Descripci�n     : Aperturar la Caja de un establecimiento
    ' Autor           : Karen Figueroa
    ' Fecha/Hora      : 28/12/2008
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function AperturarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = String.Empty
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_AperturarCajaPOS]", obeParametroPOS.NroOperacion, _
            obeParametroPOS.NroEstablecimiento, obeParametroPOS.IdMoneda, obeParametroPOS.Monto)
                While reader.Read()
                    Respuesta = Respuesta + reader(0).ToString()
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Respuesta
    End Function

    '************************************************************************************************** 
    ' M�todo          : Cerrar Caja de un Establecimiento
    ' Descripci�n     : Cerrar la Caja de un establecimiento
    ' Autor           : Karen Figueroa
    ' Fecha/Hora      : 28/12/2008
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function CerrarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = String.Empty
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_CierreCajaPOS]", obeParametroPOS.NroOperacion, _
            obeParametroPOS.NroEstablecimiento, obeParametroPOS.MontoEfectivoSoles, obeParametroPOS.MontoEfectivoDolares, obeParametroPOS.MontoTarjetaSoles, _
            obeParametroPOS.MontoTarjetaDolares)
                While reader.Read()
                    Respuesta = Respuesta + reader(0).ToString()
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Respuesta
    End Function



    '************************************************************************************************** 
    ' M�todo          : Cerrar Caja de un Establecimiento
    ' Descripci�n     : Cerrar la Caja de un establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 17/02/2009
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function CerrarAperturarCaja() As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As Integer = 0
        Try
            'Using reader As IDataReader = objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_CierreCajasPOS]")
            Respuesta = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CierreAperturaCajasPOS]")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Respuesta
    End Function



    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja de un Establecimiento
    ' Descripci�n     : Aperturar la Caja de un establecimiento
    ' Autor           : Karen Figueroa
    ' Fecha/Hora      : 28/12/2008
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function AperturarCaja() As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = String.Empty
        Try
            Respuesta = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_AperturarCajasPOS]")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Respuesta
    End Function


    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja de un Establecimiento
    ' Descripci�n     : Aperturar la Caja de un establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function GetLiquidacionPendientexIdEstablecimiento(ByVal obeEstablecimiento As BEEstablecimiento) As List(Of BEEstablecimiento)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = String.Empty
        Dim ListEstablecimiento As New List(Of BEEstablecimiento)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPendienteLiquidarPOSXIdEstablecimiento]", obeEstablecimiento.IdEstablecimiento)
                While reader.Read()
                    ListEstablecimiento.Add(New BEEstablecimiento(reader, "busqPendientes"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListEstablecimiento
    End Function
    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja de un Establecimiento
    ' Descripci�n     : Aperturar la Caja de un establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : Instancia de la Entidad BEParametros cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function GetPendientesCancelacion(ByVal obeComercio As BEComercio) As List(Of BEEstablecimiento)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = String.Empty
        Dim ListEstablecimiento As New List(Of BEEstablecimiento)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPendienteCancelacionAperturaOrigen]", obeComercio.idComercio)
                While reader.Read()
                    ListEstablecimiento.Add(New BEEstablecimiento(reader, "busqPendientesEstablecimiento"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListEstablecimiento
    End Function

    '************************************************************************************************** 
    ' M�todo          : LiquidarBloque
    ' Descripci�n     : LiquidarBloque
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : BEAperturaOrigen
    ' Parametros_Out  : Resultado de la transaccion
    ''*************************************************************************************************

    Public Function LiquidarBloque(ByVal cadena As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Respuesta As String = String.Empty
        Dim ListEstablecimiento As New List(Of BEEstablecimiento)
        Dim result As Integer
        Try
            result = objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_LiquidarCajaBloquePOS]", cadena)


        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function

    Public Function ConsultarCajasLiquidadas(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEMovimiento)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListBEMovimiento As New List(Of BEMovimiento)
        Try
            Using datareader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCajasLiquidadasPOS]", obeOrdenPago.NumeroOrdenPago, _
            obeOrdenPago.FechaCreacion, obeOrdenPago.FechaActualizacion)
                ', Convert.ToInt32(obeOrdenPago.DataAdicional))

                While datareader.Read()

                    ListBEMovimiento.Add(New BEMovimiento(datareader, "ReqReporte"))

                End While


            End Using


        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListBEMovimiento
    End Function




End Class
