Imports System.Data.Common
Imports System.Data
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades

Public Class DAFTPArchivo
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    'ACTUALIZAR ARCHIVOS FTP
    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeFTPArchivo As BEFTPArchivo = CType(be, SPE.Entidades.BEFTPArchivo)

        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarFTPArchivo", _
            obeFTPArchivo.IdFtpArchivo, obeFTPArchivo.IdEstado))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function

    'CONSULTAR ARCHIVOS FTP GENERADOS
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListArchivos As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeArchivo As BEFTPArchivo = CType(be, BEFTPArchivo)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarFTPArchivo", _
            obeArchivo.IdServicio, obeArchivo.NombreArchivo, obeArchivo.FechaGenerado, _
            obeArchivo.FechaHasta, obeArchivo.IdTipoArchivo, obeArchivo.IdEstado)

                While reader.Read()
                    ListArchivos.Add(New BEFTPArchivo(reader))
                End While

            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListArchivos

    End Function

    Public Function ObtenerFTPArchivoPorId(ByVal id As Object) As BEFTPArchivo
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultFTPArchivos As BEFTPArchivo = Nothing

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarFTPArchivoPorId]", id)

                While reader.Read
                    objresultFTPArchivos = New BEFTPArchivo(reader)
                    objresultFTPArchivos.DetalleArchivoFTP = ConsultarDetalleArchivoPorIdFTPArchivo(id)

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return objresultFTPArchivos

    End Function


    Public Function ConsultarDetalleArchivoPorIdFTPArchivo(ByVal id As Int16) As List(Of BEDetalleArchivoFTP)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultDetalleFTPArchivos As New List(Of BEDetalleArchivoFTP)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarFTPArchivoDetallePorIdFTPArchivo]", id)

                While reader.Read
                    '
                    objresultDetalleFTPArchivos.Add(New BEDetalleArchivoFTP(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return objresultDetalleFTPArchivos
    End Function
    Public Function ConsultarServiciosParaNotificacionArchivoFTP() As List(Of BEServicio)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As New List(Of BEServicio)
        'Dim obeservicio As BEServicio

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServiciosParaNotificarFTP]")
                While reader.Read
                    objresultServicios.Add(New BEServicio(reader, "mantenimiento"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return objresultServicios
    End Function

    Public Function RegistrarArchivosOrdenPagoParaNotificar() As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarArchivosOrdenPagoParaNotificarFTP]"))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function

    Public Function ConsultarFTPArchivoCabecera(ByVal idServicio As Integer) As List(Of BEFTPArchivo)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresults As New List(Of BEFTPArchivo)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarFTPArchivoCabecera]", idServicio)
                While reader.Read
                    objresults.Add(New BEFTPArchivo(reader))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return objresults
    End Function
    Public Function ActualizarFTPArchivoEstado(ByVal idftpArchivo As Integer, ByVal idEstado As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarEstadoFTPArchivoCabecera]", idftpArchivo, idEstado))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function
End Class
