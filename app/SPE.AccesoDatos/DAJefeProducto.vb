Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class DAJefeProducto
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    'REGISTRAR JEFE DE PRODUCTO
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarJefeProducto]", obeJefeProducto.Nombres, _
            obeJefeProducto.Apellidos, obeJefeProducto.IdTipoDocumento, obeJefeProducto.NumeroDocumento, obeJefeProducto.Direccion, _
            obeJefeProducto.Telefono, obeJefeProducto.Email, obeJefeProducto.Password, _
            obeJefeProducto.IdGenero, obeJefeProducto.FechaNacimiento, _
            obeJefeProducto.IdCiudad, obeJefeProducto.CodigoRegistro, _
            obeJefeProducto.IdEstado, obeJefeProducto.IdUsuarioCreacion))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado

    End Function

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)

        Try
            Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarJefeProducto]", obeJefeProducto.IdJefeProducto, _
            obeJefeProducto.IdCiudad, obeJefeProducto.Nombres, obeJefeProducto.Apellidos, obeJefeProducto.IdTipoDocumento, _
            obeJefeProducto.NumeroDocumento, obeJefeProducto.Direccion, obeJefeProducto.Telefono, _
            obeJefeProducto.IdGenero, obeJefeProducto.FechaNacimiento, _
            obeJefeProducto.IdEstado, obeJefeProducto.IdUsuarioActualizacion))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado

    End Function

    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeJefeProducto As New SPE.Entidades.BEJefeProducto

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarJefeProductoPorIdUsuario]", CInt(id))
                If reader.Read Then
                    obeJefeProducto = New SPE.Entidades.BEJefeProducto(reader)
                    obeJefeProducto.ListaTipoOrigenCancelacion = ObtenerTipoOrigenCancelacion(CInt(id))
                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeJefeProducto

    End Function

    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListJefeProducto As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarJefeProducto", obeJefeProducto.Nombres, _
            obeJefeProducto.Apellidos, obeJefeProducto.IdEstado, obeJefeProducto.Email, obeJefeProducto.IdTipoDocumento, obeJefeProducto.NumeroDocumento)
                While reader.Read
                    ListJefeProducto.Add(New SPE.Entidades.BEJefeProducto(reader, "consulta"))
                End While

            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListJefeProducto

    End Function

    Public Overrides Function GetObjectByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Object

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)

        Try

            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarEmail]", obeJefeProducto.Email))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return Resultado
    End Function

    Public Sub RegistraJefeProductoTipoOrigenCancelacion(ByVal idJefeProducto As Integer, ByVal idTipoOrigenCancelacion As Integer, _
    ByVal IsSelect As Boolean)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarJefeProductoTipoOrigen]", idJefeProducto, idTipoOrigenCancelacion, IsSelect)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub

    Public Function ObtenerTipoOrigenCancelacion(ByVal IdJefeProducto) As List(Of SPE.Entidades.BETipoOrigenCancelacion)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOrigenCancelacion As New List(Of SPE.Entidades.BETipoOrigenCancelacion)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarJefeProductoTipoOrigenPorIdUsuario", IdJefeProducto)
                While reader.Read()
                    ListOrigenCancelacion.Add(New SPE.Entidades.BETipoOrigenCancelacion(reader))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListOrigenCancelacion

    End Function
    Public Function ObtenerListaEmailsJefesProducto() As String()

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listaEmails As New List(Of String)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarJefeProductoEmails")
                While reader.Read()
                    listaEmails.Add(Convert.ToString(reader("Email")))
                End While
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listaEmails.ToArray
    End Function

End Class
