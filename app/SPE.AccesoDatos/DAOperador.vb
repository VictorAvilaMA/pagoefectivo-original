Imports System.Data.Common
Imports System.Data
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades


Public Class DAOperador
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    ''************************************************************************************************** 
    '' M�todo          : RegistrarOperador
    '' Descripci�n     : Registro de un Operador
    '' Autor           : Karen Figueroa
    '' Fecha/Hora      : 26/12/2008
    '' Parametros_In   : Instancia de la Entidad BEOperador Cargada
    '' Parametros_Out  : resultado de la transaccion
    ' ''**************************************************************************************************
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeOperador As BEOperador = CType(be, SPE.Entidades.BEOperador)
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarOperador]", _
            obeOperador.Codigo, obeOperador.Descripcion, obeOperador.IdEstado, obeOperador.IdUsuarioCreacion))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function


    ''************************************************************************************************** 
    '' M�todo          : ActualizarOperador
    '' Descripci�n     : Actualiza un registro de un Operador
    '' Autor           : Karen Figueroa
    '' Fecha/Hora      : 26/11/2008
    '' Parametros_In   : Instancia de la Entidad BEOperador Cargada
    '' Parametros_Out  : resultado de la transaccion
    ' ''**************************************************************************************************

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeOperador As BEOperador = CType(be, SPE.Entidades.BEOperador)


        Try

            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarOperador]", _
            obeOperador.IdOperador, obeOperador.Codigo, obeOperador.Descripcion, obeOperador.IdEstado, obeOperador.IdUsuarioActualizacion))


        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function


    ''************************************************************************************************** 
    '' M�todo          : ConsultarOperador
    '' Descripci�n     :Conseguir un Operador Por IdOperador
    '' Autor           : Karen Figueroa
    '' Fecha/Hora      : 26/12/2008
    '' Parametros_In   : IdOperador
    '' Parametros_Out  : Lista de entidades BEOperador
    ' ''**************************************************************************************************

    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOperador As New SPE.Entidades.BEOperador

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarOperadorPorOperadorId]", CInt(id))
                If reader.Read Then

                    obeOperador = New SPE.Entidades.BEOperador(reader)

                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return obeOperador
    End Function

    ''************************************************************************************************** 
    '' M�todo          : ConsultarOperador
    '' Descripci�n     : Consulta de Operador
    '' Autor           : Karen Figueroa
    '' Fecha/Hora      : 26/12/2008
    '' Parametros_In   : IdOperador
    '' Parametros_Out  : Lista de entidades BEOperador
    ' ''**************************************************************************************************

    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOperador As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeOperador As BEOperador = CType(be, SPE.Entidades.BEOperador)
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarOperador", obeOperador.Codigo, obeOperador.Descripcion, obeOperador.IdEstado)
                While reader.Read()
                    ListOperador.Add(New BEOperador(reader))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex

        End Try

        Return ListOperador
    End Function


End Class
