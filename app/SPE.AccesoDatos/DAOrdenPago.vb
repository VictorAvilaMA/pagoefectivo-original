Imports System.Data.Common
Imports System.Data
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
'Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports _3Dev.FW.Util
Imports System.Configuration
Imports System.Data.SqlClient
Imports _3Dev.FW.Entidades

Imports SPE.Logging

Public Class DAOrdenPago
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    Private ReadOnly _logger As ILogger = New Logger()

    Public Function ConsultarOrdenPagoPorOrdenIdComercio(ByVal OrdenIdComercio As String, ByVal IdServicio As Integer) As BEOPXServClienCons
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim oBEOPXServClienCons As BEOPXServClienCons = Nothing
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarOrdenPagoxOrdenIdComercioIdServicio]", OrdenIdComercio, IdServicio)
                While reader.Read()
                    oBEOPXServClienCons = New BEOPXServClienCons(reader)
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return oBEOPXServClienCons
    End Function

    Public Function ConsultarOrdenPagoPorNumeroOrdenPago(ByVal NumeroOrdenPago As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim oBEOrdenPago As BEOrdenPago = Nothing
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_LeerOrdenPagoxNumeroOrdenPago", NumeroOrdenPago)
                While reader.Read()
                    oBEOrdenPago = New BEOrdenPago(reader, "ConsultarOrdenPagoxNumeroOrdenPago")
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return oBEOrdenPago.IdOrdenPago
    End Function

    Public Function ExpirarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ExpirarOrdenPago", _
            obeOrdenPago.IdOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            resultado = CInt(objDatabase.ExecuteScalar(oDbCommand)) 'Pablo
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return resultado
    End Function

    Public Function EliminarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim idUsuAct As Object = obeOrdenPago.IdUsuarioActualizacion
        If (obeOrdenPago.IdOrigenEliminacion = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.OrigenEliminacion.EliminadoDeWS) Then
            idUsuAct = DBNull.Value
        End If
        Try
            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_EliminarOrdenPago", _
            obeOrdenPago.IdOrdenPago, obeOrdenPago.IdOrigenEliminacion, idUsuAct))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function ActualizarFechaExpiraCIP(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim idUsuAct As Object = obeOrdenPago.IdUsuarioActualizacion
        Try
            resultado = Convert.ToInt64(objDatabase.ExecuteScalar("PagoEfectivo.prc_ActualizarFechaExpiraCIP", _
            obeOrdenPago.IdOrdenPago, obeOrdenPago.FechaAExpirar, idUsuAct, obeOrdenPago.ClaveAPI))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    'VERIFICAR EXPIRACION C�digo de Identificaci�n de Pago
    Public Function ConsultarOrdenesPagoListaParaExpiradas() As List(Of BEOrdenPago)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOrdenPago As New List(Of BEOrdenPago)
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenesPagoListaParaExpiradas")
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read()
                    ListOrdenPago.Add(New BEOrdenPago(reader, "InfoExpiradas"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListOrdenPago
    End Function

    'GENERAR C�digo de Identificaci�n de Pago
    Public Function GenerarOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim resultadoDetalle As Integer = 0
        Dim i As Integer

        Try
            'Generando el C�digo de Identificaci�n de Pago
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_GenerarOrdenPago", _
            obeOrdenPago.IdEnteGenerador, _
            obeOrdenPago.IdUsuarioCreacion, _
            obeOrdenPago.IdServicio, _
            obeOrdenPago.IdMoneda, _
            obeOrdenPago.FechaEmision, _
            obeOrdenPago.Total, _
            obeOrdenPago.MerchantID, _
            obeOrdenPago.OrderIdComercio, _
            obeOrdenPago.UrlOk, _
            obeOrdenPago.UrlError, _
            obeOrdenPago.MailComercio, _
            obeOrdenPago.IdUsuarioCreacion, _
            obeOrdenPago.UsuarioID, _
            obeOrdenPago.DataAdicional, _
            obeOrdenPago.UsuarioNombre, _
            obeOrdenPago.UsuarioApellidos, _
            obeOrdenPago.UsuarioDomicilio, _
            obeOrdenPago.UsuarioLocalidad, _
            obeOrdenPago.UsuarioProvincia, _
            obeOrdenPago.UsuarioPais, _
            obeOrdenPago.UsuarioAlias, _
            obeOrdenPago.UsuarioEmail, _
            obeOrdenPago.TramaSolicitud, _
            obeOrdenPago.IdTramaTipo, _
            obeOrdenPago.TiempoExpiracion, _
            obeOrdenPago.ConceptoPago
            )
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M

            resultado = CInt(objDatabase.ExecuteScalar(oDbCommand))
            '
            If resultado > 0 Then
                'Registrado el detalle
                For i = 0 To ListaDetalleOrdePago.Count - 1
                    resultadoDetalle = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarDetalleOrdenPagoXML", _
                    resultado, _
                    ListaDetalleOrdePago(i).ConceptoPago, _
                    ListaDetalleOrdePago(i).Importe, _
                    ListaDetalleOrdePago(i).tipoOrigen, _
                    ListaDetalleOrdePago(i).codOrigen, _
                    ListaDetalleOrdePago(i).campo1, _
                    ListaDetalleOrdePago(i).campo2, _
                    ListaDetalleOrdePago(i).campo3))
                Next

            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return resultado
    End Function
    'GENERAR C�digo de Identificaci�n de Pago
    Public Function GenerarOrdenPagoAgenciaRecaudadora(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim odaParametro As New DAParametro
        Dim resultado As Integer = 0

        Try
            'Generando el C�digo de Identificaci�n de Pago para la Agencia Recaudadora
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_GenerarOrdenPagoAgenciaRecaudadora]", _
            obeOrdenPago.IdEnteGenerador, _
            obeOrdenPago.IdEnte, _
            obeOrdenPago.IdEstado, _
            obeOrdenPago.IdServicio, _
            obeOrdenPago.IdMoneda, _
            obeOrdenPago.FechaEmision, _
            obeOrdenPago.Total, _
            obeOrdenPago.IdUsuarioCreacion _
            ))
            If resultado > 0 Then
                'Registrado el detalle
                objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarDetalleOrdenPago", _
                    resultado, _
                    odaParametro.ConsultarParametroPorCodigoGrupo("COSE")(0).Descripcion, _
                    obeOrdenPago.Total)
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return resultado
    End Function

    'GENERAR PRE C�digo de Identificaci�n de Pago
    Public Function GenerarPreOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim resultadoDetalle As Integer = 0
        Dim i As Integer = 0

        Try
            'Generando la Pre C�digo de Identificaci�n de Pago

            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarPreOrdenPago", _
            obeOrdenPago.MerchantID, _
            obeOrdenPago.DatosEnc, _
            obeOrdenPago.ParUtil))
            '
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

        Return resultado
    End Function

    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
        Dim ListOrdenPago As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeOrdenPago As SPE.Entidades.BEOrdenPago = CType(be, SPE.Entidades.BEOrdenPago)

        Dim listaParam As New List(Of Object)
        listaParam.Add(obeOrdenPago.NumeroOrdenPago)
        listaParam.Add(obeOrdenPago.IdEmpresa)
        listaParam.Add(obeOrdenPago.IdServicio)
        listaParam.Add(obeOrdenPago.IdCliente)
        listaParam.Add(obeOrdenPago.UsuarioNombre)
        listaParam.Add(obeOrdenPago.IdTipoOrigenCancelacion)
        listaParam.Add(obeOrdenPago.IdEstado)
        listaParam.Add(obeOrdenPago.FechaCreacion)
        listaParam.Add(obeOrdenPago.FechaActualizacion)
        listaParam.Add(obeOrdenPago.UsuarioEmail)
        listaParam.Add(obeOrdenPago.OrderIdComercio)
        listaParam.Add(obeOrdenPago.IdUsuarioCreacion) 'id usuario Representante
        If (be.PageSize > 0) Then
            listaParam.Add(be.PageNumber)
            listaParam.Add(be.PageSize)
            listaParam.Add(be.PropOrder)
            listaParam.Add(be.TipoOrder)
        Else
            be.PageSize = Integer.MaxValue - 1
            listaParam.Add(1)
            listaParam.Add(be.PageSize)
            listaParam.Add(be.PropOrder)
            listaParam.Add(be.TipoOrder)
        End If

        Try
            Using dbCommand As Common.DbCommand = objDatabase.GetStoredProcCommand(If(be.PageSize = 0, "[PagoEfectivo].[prc_ConsultarHistorialOrdenPagoAvanzado]", "[PagoEfectivo].[prc_ConsultarHistorialOrdenPagoAvanzadoPG]"), listaParam.ToArray)

                dbCommand.CommandTimeout = ConfigurationManager.AppSettings("TimeoutSQLReplica")

                Using reader As IDataReader = objDatabase.ExecuteReader(dbCommand)
                    If Not reader Is Nothing Then
                        While reader.Read()
                            ListOrdenPago.Add(New SPE.Entidades.BEOrdenPago(reader, If(be.PageSize = 0, "Historial", "HistorialPG")))
                        End While
                    Else
                        ListOrdenPago = Nothing
                    End If
                    reader.Close()
                End Using
            End Using

            Return ListOrdenPago
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function

    Public Function ConsultarOrdenPagoPendientes(ByVal obeOrdenPago As SPE.Entidades.BEOrdenPago) As List(Of BEOrdenPago)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOrdenPago As New List(Of SPE.Entidades.BEOrdenPago)
        'Dim obeOrdenPago As SPE.Entidades.BEOrdenPago
        Dim listaParametros As New List(Of Object)
        listaParametros.Add(obeOrdenPago.IdCliente.ToString())
        If (obeOrdenPago.PageSize > 0) Then
            listaParametros.Add(obeOrdenPago.PageNumber)
            listaParametros.Add(obeOrdenPago.PageSize)
            listaParametros.Add(obeOrdenPago.PropOrder)
            listaParametros.Add(obeOrdenPago.TipoOrder)
        Else
            obeOrdenPago.PageSize = Integer.MaxValue - 1
            listaParametros.Add(1)
            listaParametros.Add(obeOrdenPago.PageSize)
            listaParametros.Add(obeOrdenPago.PropOrder)
            listaParametros.Add(obeOrdenPago.TipoOrder)
        End If

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader(If(obeOrdenPago.PageSize = 0, "[PagoEfectivo].[prc_ConsultarOrdenPagoPendientes]", "[PagoEfectivo].[prc_ConsultarOrdenPagoPendientesPG]"), listaParametros.ToArray())
                While reader.Read()
                    ListOrdenPago.Add(New SPE.Entidades.BEOrdenPago(reader, If(obeOrdenPago.PageSize = 0, "Pendientes", "PendientesPG")))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListOrdenPago
    End Function

    'CONSULTAR C�digo de Identificaci�n de Pago RECEPCIONAR
    Public Function ConsultarOrdenPagoRecepcionar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoRecepcionar", _
                obeOrdenPago.NumeroOrdenPago, _
                obeOrdenPago.IdEstado)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultaRecepcionarOrdenPago")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeOrdenPago

    End Function

    'CONSULTAR C�digo de Identificaci�n de Pago NOTIFICAR
    Public Function ConsultarOrdenPagoNotificar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoNotificar", _
                obeOrdenPago.IdOrdenPago)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultaNotificarOrdenPago")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeOrdenPago

    End Function

    'CONSULTAR ORDEN
    Public Function ConsultarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listOrdenPago As New List(Of BEOrdenPago)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoAvanzadaPG", _
            obeOrdenPago.NumeroOrdenPago, _
            obeOrdenPago.DescripcionCliente, _
            obeOrdenPago.DescripcionEmpresa, _
            obeOrdenPago.DescripcionServicio, _
            obeOrdenPago.IdEstado, _
            obeOrdenPago.IdUsuarioCreacion.ToString(), _
            obeOrdenPago.FechaCreacion, _
            obeOrdenPago.FechaActualizacion, obeOrdenPago.PageNumber, obeOrdenPago.PageSize, obeOrdenPago.PropOrder, obeOrdenPago.TipoOrder)

                While reader.Read()
                    listOrdenPago.Add(New BEOrdenPago(reader, "ConsultarOrdenPagoAvanzada"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return listOrdenPago

    End Function

    'ACTUALIZAR C�digo de Identificaci�n de Pago
    Public Function ActualizarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarOrdenPago", _
            obeOrdenPago.IdOrdenPago, _
            obeOrdenPago.IdEstado, _
            obeOrdenPago.IdUsuarioActualizacion))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return resultado
    End Function

    'ACTUALIZAR C�digo de Identificaci�n de Pago
    Public Function ActualizarOrdenPagoRecepcionar(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ActualizarOrdenPagoRecepcionar", _
            obeOrdenPago.IdOrdenPago, _
            obeOrdenPago.IdEstado, _
            obeOrdenPago.IdUsuarioActualizacion, _
            obeOrdenPago.FechaCancelacion))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return resultado
    End Function

    Public Function ObtenerOrdenPagoCompletoPorIdOrdenPago(ByVal idOrdenPago As Integer)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOrdenPago As BEOrdenPago = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoPorId", idOrdenPago)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "Completo")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function

    Public Function ObtenerOrdenPagoCompletoPorNroOrdenPago(ByVal nroOrdenPago As Integer)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOrdenPago As BEOrdenPago = Nothing
        Try
            _logger.Info(String.Format("ObtenerOrdenPagoCompletoPorNroOrdenPago - NumeroOrdenPago:{0}", nroOrdenPago))

            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenPagoPorNroOrdenPago", nroOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "Completo")
                    obeOrdenPago.VersionOrdenPago = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("VersionOrdenPago"))
                    obeOrdenPago.oBEServicio = New BEServicio()
                    obeOrdenPago.oBEServicio.FlgEmailCCAdmin = _3Dev.FW.Util.DataUtil.ObjectToBool(reader("FlgEmailCCAdmin"))
                    obeOrdenPago.oBEServicio.FlgEmailGenCIP = _3Dev.FW.Util.DataUtil.ObjectToBool(reader("FlgEmailGenCIP"))
                    obeOrdenPago.oBEServicio.FlgEmailGenUsuario = _3Dev.FW.Util.DataUtil.ObjectToBool(reader("FlgEmailGenUsuario"))
                    obeOrdenPago.oBEServicio.FlgEmailPagoCIP = _3Dev.FW.Util.DataUtil.ObjectToBool(reader("FlgEmailPagoCIP"))
                    obeOrdenPago.Token = _3Dev.FW.Util.DataUtil.ObjectToString(reader("Token"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message + "::" + nroOrdenPago)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function

    Public Function ConsultarOrdenPagoPorId(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oDbCommand As DbCommand

        Try
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenPagoPorId", obeOrdenPago.IdOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    ' obeOrdenPago = New BEOrdenPago(reader, "ConceptoPago")
                    obeOrdenPago = New BEOrdenPago(reader, "Completo")
                    If Not DataUtil.ObjectToString(reader("VersionOrdenPago")) = String.Empty Then
                        obeOrdenPago.VersionOrdenPago = DataUtil.ObjectToString(reader("VersionOrdenPago"))
                    End If

                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeOrdenPago

    End Function

    Public Function ConsultarDetalleOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As BEDetalleOrdenPago

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeDetalleOP As BEDetalleOrdenPago = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleOrdenPago", obeOrdenPago.IdOrdenPago.ToString)
                While reader.Read()
                    obeDetalleOP = New BEDetalleOrdenPago(reader)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeDetalleOP
    End Function

    Public Function ConsultarPagosServicios(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
        Dim ListPagoServicio As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeOrdenPago As SPE.Entidades.BEPagoServicio = CType(be, SPE.Entidades.BEPagoServicio)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarPagoServiciosxP]", _
                                                                    obeOrdenPago.IdTipoOrigenCancelacion, _
                                                                    obeOrdenPago.IdServicio, _
                                                                    obeOrdenPago.DescripcionAgencia, _
                                                                    obeOrdenPago.DescripcionCliente, _
                                                                    obeOrdenPago.NumeroOrdenPago, _
                                                                    obeOrdenPago.FechaInicio, _
                                                                    obeOrdenPago.FechaCancelacion, _
                                                                    DBNull.Value, DBNull.Value, _
                                                                    obeOrdenPago.UsuarioNombre, _
                                                                    obeOrdenPago.IdMoneda, _
                                                                    obeOrdenPago.OrderIdComercio, _
                                                                    obeOrdenPago.IdUsuarioCreacion, _
                                                                    obeOrdenPago.IdEmpresa, _
                                                                    obeOrdenPago.PageNumber, _
                                                                    obeOrdenPago.PageSize, _
                                                                    obeOrdenPago.PropOrder, _
                                                                    obeOrdenPago.TipoOrder)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read()
                    ListPagoServicio.Add(New SPE.Entidades.BEPagoServicio(reader, "ConsPagServPag"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListPagoServicio
    End Function


    '--consultar formulario nuevo

    Public Function ConsultarPagosServiciosFormulario(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
        Dim ListPagoServicio As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeOrdenPago As SPE.Entidades.BEPagoServicio = CType(be, SPE.Entidades.BEPagoServicio)
        Try
          
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarPagoServiciosxPFormulario]", _
                                                                    obeOrdenPago.IdTipoOrigenCancelacion, _
                                                                    obeOrdenPago.IdServicio, _
                                                                    obeOrdenPago.DescripcionAgencia, _
                                                                    obeOrdenPago.DescripcionCliente, _
                                                                    obeOrdenPago.NumeroOrdenPago, _
                                                                    obeOrdenPago.FechaInicio, _
                                                                    obeOrdenPago.FechaCancelacion, _
                                                                    DBNull.Value, DBNull.Value, _
                                                                    obeOrdenPago.UsuarioNombre, _
                                                                    obeOrdenPago.IdMoneda, _
                                                                    obeOrdenPago.OrderIdComercio, _
                                                                    obeOrdenPago.IdUsuarioCreacion, _
                                                                    obeOrdenPago.IdEmpresa, _
                                                                    obeOrdenPago.PageNumber, _
                                                                    obeOrdenPago.PageSize, _
                                                                    obeOrdenPago.PropOrder, _
                                                                    obeOrdenPago.TipoOrder)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read()
                    ListPagoServicio.Add(New SPE.Entidades.BEPagoServicio(reader, "ConsPagServPagFormulario"))
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListPagoServicio
    End Function



    ''nuevo formlario consultar activo
    Public Function ConsutarFormularioActivo(ByVal IdServicio As Integer) As Boolean

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Boolean = False

        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarFormularioActivo]", IdServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    retorna = reader(0)
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return retorna

    End Function

    ''' <summary>
    ''' retornar mensaje de validacion de la orden de pago
    ''' </summary>
    ''' <param name="obeOrdenPago"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaAnulacionDesdeBanco(ByVal obeOrdenPago As BEOrdenPago, _
                                                    ByVal CodOpeBanco As String) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Dim oDbCommand As DbCommand
            With obeOrdenPago

                oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ValidarCIPParaAnularDesdeBanco", _
                            .NumeroOrdenPago, .CodigoOrigenCancelacion, _
                            .CodigoBanco, CodOpeBanco, _
                            .Total, .codMonedaBanco, .CodigoServicio)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                Return CType(objDataBase.ExecuteScalar(oDbCommand), Integer)

            End With

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

        'Return intRetorna

    End Function

    ''' <summary>
    ''' Validar Orden de Pago para Anular desde FullCarga
    ''' </summary>
    ''' <param name="oBEOrdenPago">
    ''' Propiedades con valor obligatorio:
    ''' - NumeroOrdenPago
    ''' - CodigoBanco
    ''' - Total
    ''' - CodMonedaBanco
    ''' - CodigoServicio
    ''' </param>
    ''' <param name="CodPuntoVenta"></param>
    ''' <param name="NroSerieTerminal"></param>
    ''' <param name="NroOperacionPago"></param>
    ''' <returns>
    ''' > 0: IdOrdenPago
    '''   0: CIP no existe o ya est� conciliado
    '''  -1: CIP no cancelado
    '''  -2: Monto no coincide
    '''  -3: Moneda no coincide
    '''  -4: Banco no coincide
    '''  -5: El movimiento no fue encontrado
    '''  -6: Punto de venta no coincide
    '''  -7: Terminal no coincide
    '''  -8: CIP expirado
    '''  -9: N�mero de operaci�n de pago no coincide
    ''' </returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaAnulacionDesdeFullCarga(ByVal oBEOrdenPago As BEOrdenPago, _
        ByVal CodPuntoVenta As String, ByVal NroSerieTerminal As String, ByVal NroOperacionPago As String) As Integer
        Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = oDatabase.GetStoredProcCommand("PagoEfectivo.prc_ValidarCIPParaAnularDesdeFullCarga", _
                        oBEOrdenPago.NumeroOrdenPago, CodPuntoVenta, NroSerieTerminal, oBEOrdenPago.CodigoBanco, _
                        NroOperacionPago, oBEOrdenPago.Total, oBEOrdenPago.codMonedaBanco, oBEOrdenPago.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Return Convert.ToInt32(oDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Validar Orden de Pago para Anular desde MoMo
    ''' </summary>
    ''' <param name="oBEOrdenPago">
    ''' Propiedades con valor obligatorio:
    ''' - NumeroOrdenPago
    ''' - CodigoBanco
    ''' - Total
    ''' - CodMonedaBanco
    ''' - CodigoServicio
    ''' </param>
    ''' <param name="CodPuntoVenta"></param>
    ''' <param name="NroSerieTerminal"></param>
    ''' <param name="NroOperacionPago"></param>
    ''' <returns>
    ''' > 0: IdOrdenPago
    '''   0: CIP no existe o ya est� conciliado
    '''  -1: CIP no cancelado
    '''  -2: Monto no coincide
    '''  -3: Moneda no coincide
    '''  -4: Banco no coincide
    '''  -5: El movimiento no fue encontrado
    '''  -6: Punto de venta no coincide
    '''  -7: Terminal no coincide
    '''  -8: CIP expirado
    '''  -9: N�mero de operaci�n de pago no coincide
    ''' </returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaAnulacionDesdeMoMo(ByVal oBEOrdenPago As BEOrdenPago, _
        ByVal CodPuntoVenta As String, ByVal NroSerieTerminal As String, ByVal NroOperacionPago As String) As Integer
        Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = oDatabase.GetStoredProcCommand("PagoEfectivo.prc_ValidarCIPParaAnularDesdeMoMo", _
                        oBEOrdenPago.NumeroOrdenPago, CodPuntoVenta, NroSerieTerminal, oBEOrdenPago.CodigoBanco, _
                        NroOperacionPago, oBEOrdenPago.Total, oBEOrdenPago.codMonedaBanco, oBEOrdenPago.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Return Convert.ToInt32(oDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Validar Orden de Pago para Anular desde Kasnet
    ''' </summary>
    ''' <param name="oBEOrdenPago">
    ''' Propiedades con valor obligatorio:
    ''' - NumeroOrdenPago
    ''' - CodigoBanco
    ''' - Total
    ''' - CodMonedaBanco
    ''' - CodigoServicio
    ''' </param>
    ''' <param name="CodPuntoVenta"></param>
    ''' <param name="NroSerieTerminal"></param>
    ''' <param name="NroOperacionPago"></param>
    ''' <returns>
    ''' > 0: IdOrdenPago
    '''   0: CIP no existe o ya est� conciliado
    '''  -1: CIP no cancelado
    '''  -2: Monto no coincide
    '''  -3: Moneda no coincide
    '''  -4: Banco no coincide
    '''  -5: El movimiento no fue encontrado
    '''  -6: Punto de venta no coincide
    '''  -7: Terminal no coincide
    '''  -8: CIP expirado
    '''  -9: N�mero de operaci�n de pago no coincide
    ''' </returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaExtornarDesdeKasnet(ByVal oBEOrdenPago As BEOrdenPago, _
        ByVal CodPuntoVenta As String, ByVal NroSerieTerminal As String, ByVal NroOperacionPago As String) As Integer
        Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = oDatabase.GetStoredProcCommand("PagoEfectivo.prc_ValidarCIPParaExtornarDesdeKasnet", _
                        oBEOrdenPago.NumeroOrdenPago, CodPuntoVenta, NroSerieTerminal, oBEOrdenPago.CodigoBanco, _
                        NroOperacionPago, oBEOrdenPago.Total, oBEOrdenPago.codMonedaBanco, oBEOrdenPago.CodigoServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Return Convert.ToInt32(oDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' retornar mensaje de validacion de la orden de pago para extorno automatico
    ''' </summary>
    ''' <param name="obeOrdenPago"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaExtornoDesdeBanco(ByVal obeOrdenPago As BEOrdenPago, _
                                                    ByVal CodOpeBanco As String) As Integer

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Try
            With obeOrdenPago

                'M
                Dim oDbCommand As DbCommand
                oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ValidarCIPParaExtornoDesdeBanco", _
                           .NumeroOrdenPago, .CodigoOrigenCancelacion, _
                            .CodigoBanco, CodOpeBanco, _
                            .Total, .codMonedaBanco, .CodigoServicio)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                'End M
                Return CType(objDataBase.ExecuteScalar(oDbCommand), Integer)

            End With

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

    End Function

    Public Function ActualizarIntencionExtorno(ByVal IdOrdenPago As Integer) As Integer

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ActualizarIntencionExtorno", IdOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Return CType(objDataBase.ExecuteScalar(oDbCommand), Integer)

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

    End Function

    ''' <summary>
    ''' retornar mensaje de validacion de la orden de pago para extorno automatico
    ''' </summary>
    ''' <param name="IdOrdenPago"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerCantidadIntentoExtorno(ByVal IdOrdenPago As String) As List(Of BEIntentoExtorno)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListOrdenPago As New List(Of BEIntentoExtorno)
        Try
            _logger.Info(String.Format("ObtenerCantidadIntentoExtorno - IdOrdenPago: {0}", IdOrdenPago))

            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ObtenerCantidadIntentoExtorno", IdOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read()
                    ListOrdenPago.Add(New BEIntentoExtorno(reader, "Consultar"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListOrdenPago
    End Function

    Public Function RestaurarEstadoPago(ByVal IdOrdenPago As String, ByVal IdEstado As Integer) As Integer

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Try
            _logger.Info(String.Format("RestaurarEstadoPago - IdOrdenPago:{0},IdEstado:{1}", IdOrdenPago, IdEstado))

            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_RestaurarEstadoPago", IdOrdenPago, IdEstado)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Return CType(objDataBase.ExecuteScalar(oDbCommand), Integer)
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

    End Function


    ''' <summary>
    ''' Retorna c�digo de mensaje de validaci�n para extorno de anulaci�n de CIPs desde bancos
    ''' </summary>
    ''' <param name="oBEOrdenPago">
    ''' - NumeroOrdenPago
    ''' - CodigoOrigenCancelacion
    ''' - CodigoBanco
    ''' - NumeroOperacionAnulacion
    ''' - Total
    ''' - CodMonedaBanco
    ''' - CodigoServicio
    ''' </param>
    ''' <param name="NumeroOperacionAnulacion">N�mero de operaci�n de anulaci�n</param>
    ''' <returns>C�digo de mensaje de validaci�n para extorno de anulaci�n</returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaExtornoAnulacionDesdeBanco(ByVal oBEOrdenPago As BEOrdenPago, _
        ByVal NumeroOperacionAnulacion As String) As Integer
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Dim oDbCommand As DbCommand
            With oBEOrdenPago
                oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ValidarCIPParaExtornoAnulacionDesdeBanco", .NumeroOrdenPago, _
                .CodigoOrigenCancelacion, .CodigoBanco, NumeroOperacionAnulacion, .Total, .codMonedaBanco, .CodigoServicio)
                oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                Return CType(objDataBase.ExecuteScalar(oDbCommand), Integer)
            End With
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function



    Public Function ValidarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As String = ""

        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ValidarOrdenPagoExAgenciaBancaria", _
            obeOrdenPago.NumeroOrdenPago, obeOrdenPago.CodigoOrigenCancelacion, obeOrdenPago.CodigoServicio, obeOrdenPago.CodigoBanco)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    retorna = reader(0)
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return retorna

    End Function

    Public Function ConsultarOrdenPagoPorIdPOS(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoPorId", obeOrdenPago.IdOrdenPago.ToString)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultaNotificacionMailPOS")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeOrdenPago

    End Function

    Public Function WSConsultarCIPConciliados(ByVal FechaDesde As DateTime, ByVal FechaHasta As DateTime, _
        ByVal Tipo As String, ByVal IdServicio As Integer) As List(Of BEWSConsCIPConciliado)
        Dim Resultado As New List(Of BEWSConsCIPConciliado)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarCIPsConciliadosPorIdServicio", _
                IdServicio, FechaDesde, FechaHasta, Tipo)
                While reader.Read()
                    Resultado.Add(New BEWSConsCIPConciliado(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function WSConsultarCIPDetalle(ByVal request As BEWSConsCIPsConciliadosRequest, ByVal IdServicio As Integer) As List(Of BEDetalleOrdenPago)
        Dim Resultado As New List(Of BEDetalleOrdenPago)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarCIPsConciliadosDetalle", _
                IdServicio, request.FechaDesde, request.FechaHasta, request.Tipo)
                While reader.Read()
                    Dim oBEDetalleOrdenPago As New BEDetalleOrdenPago
                    With oBEDetalleOrdenPago
                        .IdOrdenPago = Convert.ToString(reader("IdOrdenPago"))
                        .IdDetalleOrdenPago = Convert.ToString(reader("IdDetalleOrdenPago"))
                        .ConceptoPago = Convert.ToString(reader("ConceptoPago"))
                        .Importe = Convert.ToString(reader("Importe"))
                        .tipoOrigen = Convert.ToString(reader("Tipo_Origen"))
                        .codOrigen = Convert.ToString(reader("Cod_Origen"))
                        .campo1 = Convert.ToString(reader("Campo1"))
                        .campo2 = Convert.ToString(reader("Campo2"))
                        .campo3 = Convert.ToString(reader("Campo3"))
                    End With
                    Resultado.Add(oBEDetalleOrdenPago)
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    'Consultar Informacion de CIPs por IdOrdenPago para uno o varias Ordenes concatenadas por coma
    Public Function WSConsultarCIP(ByVal CIPs As String, ByVal IdServicio As Integer) As List(Of BEWSConsCIP)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As New List(Of BEWSConsCIP)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarCIPsPorIdOrdenPago", _
                CIPs, IdServicio)
                While reader.Read()
                    retorna.Add(New BEWSConsCIP(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna
    End Function

    'Consultar Informacion de CIPs por IdOrdenPago para uno o varias Ordenes concatenadas por coma
    Public Function WSConsultarCIPMod1(ByVal CIPs As String, ByVal IdServicio As Integer) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As String = String.Empty
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarSolicitudesXMLPorOrdenesPagos]", CIPs, IdServicio)
                While reader.Read()
                    retorna = reader(0)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna
    End Function

    'Consultar Informacion de CIPs por IdOrdenPago para uno o varias Ordenes concatenadas por coma v2
    Public Function WSConsultarCIPv2(ByVal CIPs As String, ByVal IdServicio As Integer) As List(Of BEWSConsCIPv2)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As New List(Of BEWSConsCIPv2)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarCIPsPorIdOrdenPago", _
                CIPs, IdServicio)
                While reader.Read()
                    retorna.Add(New BEWSConsCIPv2(reader))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna
    End Function

    'Nuevos metodos : 19/11/2010 - Walter Rojas Aguilar
    Public Function ConsultarOPsByServicio(ByVal oBEPagoServicio As BEPagoServicio) As List(Of BEPagoServicio)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListPagoServicio As New List(Of BEPagoServicio)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPagoServicios]", Nothing, oBEPagoServicio.IdServicio, Nothing, Nothing, Nothing, oBEPagoServicio.FechaInicio, oBEPagoServicio.FechaFin, oBEPagoServicio.Tipo, Nothing)
                While reader.Read()
                    ListPagoServicio.Add(New SPE.Entidades.BEPagoServicio(reader, "ConsOPsByServ"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListPagoServicio
    End Function


    Public Function ObtenerOrdenPagCabeceraPorNumeroOrden(ByVal numeroOrden As String) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOrdenPago As BEOrdenPago = Nothing
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarCabeceraOP", numeroOrden)

                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultarCabeceraPorNumeroOrdenPago")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function


    Public Function ConsultarDetalleOrdenPagoPorIdOrdenPago(ByVal idOrdenPago As Integer) As List(Of BEDetalleOrdenPago)
        Dim Resultado As New List(Of BEDetalleOrdenPago)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleOP", _
                idOrdenPago)
                While reader.Read()
                    Dim oBEDetalleOrdenPago As New BEDetalleOrdenPago
                    With oBEDetalleOrdenPago
                        .IdOrdenPago = Convert.ToString(reader("IdOrdenPago"))
                        .IdDetalleOrdenPago = Convert.ToString(reader("IdDetalleOrdenPago"))
                        .ConceptoPago = Convert.ToString(reader("ConceptoPago"))
                        .Importe = Convert.ToString(reader("Importe"))
                        .tipoOrigen = Convert.ToString(reader("Tipo_Origen"))
                        .codOrigen = Convert.ToString(reader("Cod_Origen"))
                        '.campo1 = Convert.ToString(reader("Campo1"))
                        '.campo2 = Convert.ToString(reader("Campo2"))
                        '.campo3 = Convert.ToString(reader("Campo3"))
                    End With
                    Resultado.Add(oBEDetalleOrdenPago)
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Function ConsultarHistoricoMovimientosporIdOrden(ByVal idOrdenPago As Integer) As List(Of BEMovimiento)
        Dim Resultado As New List(Of BEMovimiento)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarHistorialMovimientosOP", _
                idOrdenPago)
                While reader.Read()
                    Dim oBEMovimiento As New BEMovimiento
                    With oBEMovimiento
                        .IdOrdenPago = Convert.ToString(reader("IdOrdenPago"))
                        .IdMovimiento = Convert.ToString(reader("IdMovimiento"))
                        .Moneda = Convert.ToString(reader("moneda"))
                        .DescripcionTipoMovimiento = Convert.ToString(reader("tipomov"))
                        .MedioPago = Convert.ToString(reader("mediopago"))
                        .Monto = Convert.ToString(reader("Monto"))
                        .FechaMovimiento = Convert.ToDateTime(reader("FechaMovimiento"))
                        .DescripcionOrigenCanc = Convert.ToString(reader("origenCanc"))
                        .DescripcionAgencia = Convert.ToString(reader("AgenciaBancaria"))
                        .NumeroOperacion = Convert.ToString(reader("NumeroOperacion"))
                    End With
                    Resultado.Add(oBEMovimiento)
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    '<add Peru.Com FaseIII>
#Region "Integracion Servicios v2"
    Public Function GenerarSolicitudPago(ByVal obeSolicitudPago As BESolicitudPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim resultadoDetalle As Integer = 0
        Dim i As Integer

        Try
            'Generando el C�digo de Identificaci�n de Pago

            obeSolicitudPago.Token = System.Guid.NewGuid.ToString()

            resultado = CInt(objDatabase.ExecuteScalar("PagoEfectivo.prc_GenerarSolicitudPago", _
            obeSolicitudPago.IdCliente, _
            obeSolicitudPago.Idestado, _
            obeSolicitudPago.IdServicio, _
            obeSolicitudPago.IdMoneda, _
            obeSolicitudPago.MetodosPago, _
            obeSolicitudPago.Total, _
            obeSolicitudPago.CodServicio, _
            obeSolicitudPago.CodTransaccion, _
            obeSolicitudPago.MailComercio, _
            obeSolicitudPago.IdUsuarioCreacion, _
            obeSolicitudPago.FechaExpiracion, _
            obeSolicitudPago.DataAdicional, _
            obeSolicitudPago.UsuarioId, _
            obeSolicitudPago.UsuarioNombre, _
            obeSolicitudPago.UsuarioApellidos, _
            obeSolicitudPago.UsuarioLocalidad, _
            obeSolicitudPago.UsuarioProvincia, _
            obeSolicitudPago.UsuarioPais, _
            obeSolicitudPago.UsuarioAlias, _
            obeSolicitudPago.UsuarioEmail, _
            obeSolicitudPago.UsuarioTipoDoc, _
            obeSolicitudPago.UsuarioNumDoc, _
            obeSolicitudPago.TramaSolicitud, _
            obeSolicitudPago.TiempoExpiracion, _
            obeSolicitudPago.ConceptoPago, _
            obeSolicitudPago.Token
            ))


            Dim ListaDetalleSolicitudPago As List(Of BEDetalleSolicitudPago) = obeSolicitudPago.DetallesSolicitudPago
            Dim ListaParametroEmail As List(Of BEParametroEmail) = obeSolicitudPago.ParametrosEmail
            Dim ListaParametroURL As List(Of BEParametroURL) = obeSolicitudPago.ParametrosURL
            If resultado > 0 Then
                'Registrado el detalle
                For i = 0 To ListaDetalleSolicitudPago.Count - 1
                    resultadoDetalle = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarDetalleSolicitudPago", _
                    resultado, _
                    ListaDetalleSolicitudPago(i).ConceptoPago, _
                    ListaDetalleSolicitudPago(i).Importe, _
                    ListaDetalleSolicitudPago(i).tipoOrigen, _
                    ListaDetalleSolicitudPago(i).codOrigen, _
                    ListaDetalleSolicitudPago(i).campo1, _
                    ListaDetalleSolicitudPago(i).campo2, _
                    ListaDetalleSolicitudPago(i).campo3))
                Next

                For i = 0 To ListaParametroEmail.Count - 1
                    resultadoDetalle = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarParametroEmailSolicitudPago", _
                    resultado, _
                    ListaParametroEmail(i).Nombre, _
                    ListaParametroEmail(i).Valor))
                Next

                For i = 0 To ListaParametroURL.Count - 1
                    resultadoDetalle = CInt(objDatabase.ExecuteNonQuery("PagoEfectivo.prc_RegistrarParametroURLSolicitudPago", _
                    resultado, _
                    ListaParametroURL(i).Nombre, _
                    ListaParametroURL(i).Valor))
                Next
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

        Return resultado
    End Function
    Public Function WSConsultarSolicitudPago(ByVal obeSolicitudPago As BESolicitudPago) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As String = String.Empty
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarSolicitudXMLPorIdSolicitudPago]", obeSolicitudPago.IdSolicitudPago)
                While reader.Read()
                    retorna = reader(0)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna
    End Function
    Public Function WSConsultarSolicitudPagoPorIDSolicitudYIDServicio(ByVal obeSolicitudPago As BESolicitudPago) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As String = String.Empty
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarSolicitudXMLPorIdSolicitudPagoYIdServicio]", obeSolicitudPago.IdSolicitudPago, obeSolicitudPago.IdServicio)
                While reader.Read()
                    retorna = reader(0)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna

    End Function
    Public Function WSConsultarSolicitudPagoPorIdOrdenPago(ByVal oBEOrdenPago As BEOrdenPago) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As String = String.Empty
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarSolicitudXMLPorOrdenPago]", oBEOrdenPago.IdOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read()
                    retorna = reader(0)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna

    End Function
    Public Function ConsultarSolicitudPagoPorId(ByVal obeSolicitudPago As BESolicitudPago) As BESolicitudPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarSolicitudPorIdSolicitudPago", obeSolicitudPago.IdSolicitudPago)

                While reader.Read()
                    obeSolicitudPago = New BESolicitudPago(reader, "PorID")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeSolicitudPago

    End Function
    Public Function ConsultarSolicitudPagoPorOrdenIdComercio(ByVal IdOrdenComercio As String, ByVal IdServicio As Int64) As Boolean
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Return CBool(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ConsultaSolicitudPagoPorOrdenIdComercio]", IdOrdenComercio, IdServicio))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return False
    End Function

    Public Function ConsultarSolicitudPagoPorToken(ByVal obeSolicitudPago As BESolicitudPago) As BESolicitudPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarSolicitudPorToken", obeSolicitudPago.Token)
                obeSolicitudPago = Nothing
                While reader.Read()
                    obeSolicitudPago = New BESolicitudPago(reader, "PorID")
                End While
            End Using
            If obeSolicitudPago IsNot Nothing Then
                Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarSolicitudDetallePorId", obeSolicitudPago.IdSolicitudPago)
                    obeSolicitudPago.DetallesSolicitudPago = New List(Of BEDetalleSolicitudPago)
                    While reader.Read()
                        obeSolicitudPago.DetallesSolicitudPago.Add(New BEDetalleSolicitudPago(reader))
                    End While
                End Using
                'Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarSolicitudParamsURLPorId", obeSolicitudPago.IdSolicitudPago)
                '    obeSolicitudPago.ParametrosURL = New List(Of BEParametroURL)
                '    While reader.Read()
                '        obeSolicitudPago.ParametrosURL.Add(New BEParametroURL(reader))
                '    End While
                'End Using
                Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarSolicitudParamsEmailPorId", obeSolicitudPago.IdSolicitudPago)
                    obeSolicitudPago.ParametrosEmail = New List(Of BEParametroEmail)
                    While reader.Read()
                        obeSolicitudPago.ParametrosEmail.Add(New BEParametroEmail(reader))
                    End While
                End Using
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeSolicitudPago

    End Function

    Public Function GenerarOrdenPagoFromSolicitud(ByVal obeSolicitud As BESolicitudPago, ByVal obeOrdenpago As BEOrdenPago) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Int64 = 0
        Dim resultadoDetalle As Integer = 0
        Dim i As Integer

        Try
            'Generando el C�digo de Identificaci�n de Pago
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_GenerarOrdenPagoFromSolicitud", _
            obeOrdenpago.IdEnteGenerador, _
            obeSolicitud.IdUsuarioCreacion, _
            obeSolicitud.IdServicio, _
            obeSolicitud.IdMoneda, _
            obeOrdenpago.FechaEmision, _
            obeSolicitud.Total, _
            obeSolicitud.CodServicio, _
            obeSolicitud.CodTransaccion, _
            obeOrdenpago.UrlOk, _
            obeOrdenpago.UrlError, _
            obeSolicitud.MailComercio, _
            obeSolicitud.IdUsuarioCreacion, _
            obeSolicitud.UsuarioId, _
            obeSolicitud.DataAdicional, _
            obeSolicitud.UsuarioNombre, _
            obeSolicitud.UsuarioApellidos, _
            obeOrdenpago.UsuarioDomicilio, _
            obeSolicitud.UsuarioLocalidad, _
            obeSolicitud.UsuarioProvincia, _
            obeSolicitud.UsuarioPais, _
            obeSolicitud.UsuarioAlias, _
            obeSolicitud.UsuarioEmail, _
            obeSolicitud.TramaSolicitud, _
            obeOrdenpago.IdTramaTipo, _
            obeOrdenpago.TiempoExpiracion, _
            obeSolicitud.ConceptoPago, _
            obeSolicitud.UsuarioTipoDoc, _
            obeSolicitud.UsuarioNumDoc, _
            obeSolicitud.IdSolicitudPago _
            )

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            resultado = Convert.ToInt64(objDatabase.ExecuteScalar(oDbCommand))
            '
            If resultado > 0 Then
                'Registrado el detalle
                For i = 0 To obeSolicitud.DetallesSolicitudPago.Count - 1
                    Dim oDbCommand1 As DbCommand
                    oDbCommand1 = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_RegistrarDetalleOrdenPagoXML", _
                    resultado, _
                    obeSolicitud.DetallesSolicitudPago(i).ConceptoPago, _
                    obeSolicitud.DetallesSolicitudPago(i).Importe, _
                    obeSolicitud.DetallesSolicitudPago(i).tipoOrigen, _
                    obeSolicitud.DetallesSolicitudPago(i).codOrigen, _
                    obeSolicitud.DetallesSolicitudPago(i).campo1, _
                    obeSolicitud.DetallesSolicitudPago(i).campo2, _
                    obeSolicitud.DetallesSolicitudPago(i).campo3)

                    oDbCommand1.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
                    resultadoDetalle = CInt(objDatabase.ExecuteNonQuery(oDbCommand1))
                Next

            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function


    Public Function ConsultarSolicitudesPagoListaParaExpiradas() As List(Of BESolicitudPago)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listBESolicitudPago As New List(Of BESolicitudPago)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarSolicitudPagoListaParaExpiradas]")
                While reader.Read()
                    listBESolicitudPago.Add(New BESolicitudPago(reader, "InfoExpiradas"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listBESolicitudPago
    End Function

    Public Function ExpirarSolicitudPago(ByVal oBESolicitudPago As BESolicitudPago) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        '
        Try
            resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ExpirarSolicitudPago]", oBESolicitudPago.IdSolicitudPago))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return resultado
    End Function
    Public Function WSConsultarCIPDetalle(ByVal CIPs As String, ByVal IdServicio As Integer) As List(Of BEDetalleOrdenPago)
        Dim Resultado As New List(Of BEDetalleOrdenPago)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleCIPsPorIdOrdenPago", _
                 CIPs, IdServicio)
                While reader.Read()
                    Dim oBEDetalleOrdenPago As New BEDetalleOrdenPago
                    With oBEDetalleOrdenPago
                        .IdOrdenPago = Convert.ToString(reader("IdOrdenPago"))
                        .IdDetalleOrdenPago = Convert.ToString(reader("IdDetalleOrdenPago"))
                        .ConceptoPago = Convert.ToString(reader("ConceptoPago"))
                        .Importe = Convert.ToString(reader("Importe"))
                        .tipoOrigen = Convert.ToString(reader("Tipo_Origen"))
                        .codOrigen = Convert.ToString(reader("Cod_Origen"))
                        .campo1 = Convert.ToString(reader("Campo1"))
                        .campo2 = Convert.ToString(reader("Campo2"))
                        .campo3 = Convert.ToString(reader("Campo3"))
                    End With
                    Resultado.Add(oBEDetalleOrdenPago)
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function
    'Nuevos metodos : 19/11/2010 - Walter Rojas Aguilar
    Public Function ConsultarOPsByCIPs(ByVal oBEPagoServicio As BEPagoServicio) As List(Of BEPagoServicio)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListPagoServicio As New List(Of BEPagoServicio)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPagoServicios]", Nothing, oBEPagoServicio.IdServicio, Nothing, Nothing, Nothing, oBEPagoServicio.FechaInicio, oBEPagoServicio.FechaFin, Nothing, oBEPagoServicio.CIPS)
                While reader.Read()
                    ListPagoServicio.Add(New SPE.Entidades.BEPagoServicio(reader, "ConsOPsByServ"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListPagoServicio
    End Function
    Public Function ExisteCIPsPorIdServicio(ByVal idServicio As Integer) As Boolean

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Boolean = False
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ExisteCIPsPorIdServicio]", idServicio)) > 0
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function
#End Region

#Region "Nueva Modalidad 1"
    Public Function GenerarSolicitudPagoYOrden(ByVal obeSolicitudPago As BESolicitudPago) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Int64 = 0
        Dim resultadoDetalle As Integer = 0

        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim dtDetallePago As New DataTable
                Dim dtParamEmail As New DataTable
                Dim dtParamUrl As New DataTable

                dtDetallePago.Columns.Add("ConceptoPago")
                dtDetallePago.Columns.Add("Importe")
                dtDetallePago.Columns.Add("TipoOrigen")
                dtDetallePago.Columns.Add("CodOrigen")
                dtDetallePago.Columns.Add("Campo1")
                dtDetallePago.Columns.Add("Campo2")
                dtDetallePago.Columns.Add("Campo3")

                dtParamEmail.Columns.Add("Name")
                dtParamEmail.Columns.Add("Value")

                dtParamUrl.Columns.Add("Name")
                dtParamUrl.Columns.Add("Value")

                For Each item As BEDetalleSolicitudPago In obeSolicitudPago.DetallesSolicitudPago.ToArray()
                    dtDetallePago.Rows.Add(item.ConceptoPago, item.Importe, item.tipoOrigen, item.codOrigen, item.campo1, item.campo2, item.campo3)
                Next
                For Each item As BEParametroEmail In obeSolicitudPago.ParametrosEmail.ToArray()
                    dtParamEmail.Rows.Add(item.Nombre, item.Valor)
                Next
                For Each item As BEParametroURL In obeSolicitudPago.ParametrosURL.ToArray()
                    dtParamUrl.Rows.Add(item.Nombre, item.Valor)
                Next

                obeSolicitudPago.Token = System.Guid.NewGuid.ToString()
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_GenerarSolicitudPagoYOrdenPago]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@IdCliente", SqlDbType.Int).Value = obeSolicitudPago.IdCliente
                oSqlCommand.Parameters.Add("@IdEstado", SqlDbType.Int).Value = obeSolicitudPago.Idestado
                oSqlCommand.Parameters.Add("@IdServicio", SqlDbType.Int).Value = obeSolicitudPago.IdServicio
                oSqlCommand.Parameters.Add("@IdMoneda", SqlDbType.Int).Value = obeSolicitudPago.IdMoneda
                oSqlCommand.Parameters.Add("@MetodosPago", SqlDbType.VarChar).Value = obeSolicitudPago.MetodosPago
                oSqlCommand.Parameters.Add("@Total", SqlDbType.Decimal).Value = obeSolicitudPago.Total
                oSqlCommand.Parameters.Add("@CodServicio", SqlDbType.VarChar).Value = obeSolicitudPago.CodServicio
                oSqlCommand.Parameters.Add("@CodTransaccion", SqlDbType.VarChar).Value = obeSolicitudPago.CodTransaccion
                oSqlCommand.Parameters.Add("@MailComercio", SqlDbType.VarChar).Value = obeSolicitudPago.MailComercio
                oSqlCommand.Parameters.Add("@IdUsuarioCreacion", SqlDbType.Int).Value = obeSolicitudPago.IdUsuarioCreacion
                oSqlCommand.Parameters.Add("@FechaExpiracion", SqlDbType.DateTime).Value = obeSolicitudPago.FechaExpiracion
                oSqlCommand.Parameters.Add("@DataAdicional", SqlDbType.VarChar).Value = obeSolicitudPago.DataAdicional
                oSqlCommand.Parameters.Add("@UsuarioId", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioId
                oSqlCommand.Parameters.Add("@UsuarioNombre", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioNombre
                oSqlCommand.Parameters.Add("@UsuarioApellidos", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioApellidos
                oSqlCommand.Parameters.Add("@UsuarioLocalidad", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioLocalidad
                oSqlCommand.Parameters.Add("@UsuarioProvinicia", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioProvincia
                oSqlCommand.Parameters.Add("@UsuarioPais", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioPais
                oSqlCommand.Parameters.Add("@UsuarioAlias", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioAlias
                oSqlCommand.Parameters.Add("@UsuarioEmail", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioEmail
                oSqlCommand.Parameters.Add("@UsuarioTipoDoc", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioTipoDoc
                oSqlCommand.Parameters.Add("@UsuarioNumDoc", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioNumDoc
                oSqlCommand.Parameters.Add("@TramaSolicitud", SqlDbType.VarChar).Value = obeSolicitudPago.TramaSolicitud
                oSqlCommand.Parameters.Add("@TiempoExpiracion", SqlDbType.Decimal).Value = obeSolicitudPago.TiempoExpiracion
                oSqlCommand.Parameters.Add("@ConceptoPago", SqlDbType.VarChar).Value = obeSolicitudPago.ConceptoPago
                oSqlCommand.Parameters.Add("@Token", SqlDbType.VarChar).Value = obeSolicitudPago.Token
                oSqlCommand.Parameters.Add("@DetSolicitudpago", SqlDbType.Structured).Value = dtDetallePago
                oSqlCommand.Parameters.Add("@ParamEmailSolicitud", SqlDbType.Structured).Value = dtParamEmail
                oSqlCommand.Parameters.Add("@ParamUrlSolicitud", SqlDbType.Structured).Value = dtParamUrl


                'datos adicioanles------------------------------------------------------------------
                oSqlCommand.Parameters.Add("@Telefono", SqlDbType.VarChar).Value = obeSolicitudPago.Telefono
                oSqlCommand.Parameters.Add("@Anexo", SqlDbType.VarChar).Value = obeSolicitudPago.Anexo
                oSqlCommand.Parameters.Add("@Celular", SqlDbType.VarChar).Value = obeSolicitudPago.Celular
                oSqlCommand.Parameters.Add("@DatoAdicional", SqlDbType.VarChar).Value = obeSolicitudPago.DatoAdicional
                oSqlCommand.Parameters.Add("@CanalPago", SqlDbType.VarChar).Value = obeSolicitudPago.CanalPago
                oSqlCommand.Parameters.Add("@TipoComprobante", SqlDbType.VarChar).Value = obeSolicitudPago.TipoComprobante
                oSqlCommand.Parameters.Add("@Ruc", SqlDbType.VarChar).Value = obeSolicitudPago.Ruc
                oSqlCommand.Parameters.Add("@RazonSocial", SqlDbType.VarChar).Value = obeSolicitudPago.RazonSocial
                oSqlCommand.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = obeSolicitudPago.Direccion            
                oSqlCommand.Parameters.Add("@CodigoFormulario", SqlDbType.VarChar).Value = obeSolicitudPago.CodigoFormulario
                oSqlConnection.Open()
                resultado = oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    'FixSaga20140425
    Public Function ActualizarSolicitudPagoYOrden(ByVal idSolicitudPago As Integer, ByVal obeSolicitudPago As BESolicitudPago) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Int64 = 0
        Dim resultadoDetalle As Integer = 0

        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim dtDetallePago As New DataTable
                Dim dtParamEmail As New DataTable
                Dim dtParamUrl As New DataTable

                dtDetallePago.Columns.Add("ConceptoPago")
                dtDetallePago.Columns.Add("Importe")
                dtDetallePago.Columns.Add("TipoOrigen")
                dtDetallePago.Columns.Add("CodOrigen")
                dtDetallePago.Columns.Add("Campo1")
                dtDetallePago.Columns.Add("Campo2")
                dtDetallePago.Columns.Add("Campo3")

                dtParamEmail.Columns.Add("Name")
                dtParamEmail.Columns.Add("Value")

                dtParamUrl.Columns.Add("Name")
                dtParamUrl.Columns.Add("Value")

                For Each item As BEDetalleSolicitudPago In obeSolicitudPago.DetallesSolicitudPago.ToArray()
                    dtDetallePago.Rows.Add(item.ConceptoPago, item.Importe, item.tipoOrigen, item.codOrigen, item.campo1, item.campo2, item.campo3)
                Next
                For Each item As BEParametroEmail In obeSolicitudPago.ParametrosEmail.ToArray()
                    dtParamEmail.Rows.Add(item.Nombre, item.Valor)
                Next
                For Each item As BEParametroURL In obeSolicitudPago.ParametrosURL.ToArray()
                    dtParamUrl.Rows.Add(item.Nombre, item.Valor)
                Next

                obeSolicitudPago.Token = System.Guid.NewGuid.ToString()
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_ActualizarSolicitudPagoYOrdenPago]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@IdSolicitud", SqlDbType.Int).Value = idSolicitudPago
                'oSqlCommand.Parameters.Add("@IdCliente", SqlDbType.Int).Value = obeSolicitudPago.IdCliente
                'oSqlCommand.Parameters.Add("@IdEstado", SqlDbType.Int).Value = obeSolicitudPago.Idestado
                'oSqlCommand.Parameters.Add("@IdServicio", SqlDbType.Int).Value = obeSolicitudPago.IdServicio
                'oSqlCommand.Parameters.Add("@IdMoneda", SqlDbType.Int).Value = obeSolicitudPago.IdMoneda
                'oSqlCommand.Parameters.Add("@MetodosPago", SqlDbType.VarChar).Value = obeSolicitudPago.MetodosPago
                oSqlCommand.Parameters.Add("@Total", SqlDbType.Decimal).Value = obeSolicitudPago.Total
                'oSqlCommand.Parameters.Add("@CodServicio", SqlDbType.VarChar).Value = obeSolicitudPago.CodServicio
                oSqlCommand.Parameters.Add("@CodTransaccion", SqlDbType.VarChar).Value = obeSolicitudPago.CodTransaccion
                'oSqlCommand.Parameters.Add("@MailComercio", SqlDbType.VarChar).Value = obeSolicitudPago.MailComercio
                'oSqlCommand.Parameters.Add("@IdUsuarioCreacion", SqlDbType.Int).Value = obeSolicitudPago.IdUsuarioCreacion
                oSqlCommand.Parameters.Add("@FechaExpiracion", SqlDbType.DateTime).Value = obeSolicitudPago.FechaExpiracion
                'oSqlCommand.Parameters.Add("@DataAdicional", SqlDbType.VarChar).Value = obeSolicitudPago.DataAdicional
                'oSqlCommand.Parameters.Add("@UsuarioId", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioId
                'oSqlCommand.Parameters.Add("@UsuarioNombre", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioNombre
                'oSqlCommand.Parameters.Add("@UsuarioApellidos", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioApellidos
                'oSqlCommand.Parameters.Add("@UsuarioLocalidad", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioLocalidad
                'oSqlCommand.Parameters.Add("@UsuarioProvinicia", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioProvincia
                'oSqlCommand.Parameters.Add("@UsuarioPais", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioPais
                'oSqlCommand.Parameters.Add("@UsuarioAlias", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioAlias
                'oSqlCommand.Parameters.Add("@UsuarioEmail", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioEmail
                'oSqlCommand.Parameters.Add("@UsuarioTipoDoc", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioTipoDoc
                'oSqlCommand.Parameters.Add("@UsuarioNumDoc", SqlDbType.VarChar).Value = obeSolicitudPago.UsuarioNumDoc
                oSqlCommand.Parameters.Add("@TramaSolicitud", SqlDbType.VarChar).Value = obeSolicitudPago.TramaSolicitud
                oSqlCommand.Parameters.Add("@TiempoExpiracion", SqlDbType.Decimal).Value = obeSolicitudPago.TiempoExpiracion
                oSqlCommand.Parameters.Add("@ConceptoPago", SqlDbType.VarChar).Value = obeSolicitudPago.ConceptoPago
                'oSqlCommand.Parameters.Add("@Token", SqlDbType.VarChar).Value = obeSolicitudPago.Token
                oSqlCommand.Parameters.Add("@DetSolicitudpago", SqlDbType.Structured).Value = dtDetallePago
                oSqlCommand.Parameters.Add("@ParamEmailSolicitud", SqlDbType.Structured).Value = dtParamEmail
                'oSqlCommand.Parameters.Add("@ParamUrlSolicitud", SqlDbType.Structured).Value = dtParamUrl
                oSqlConnection.Open()
                resultado = oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function
#End Region

    Public Function COnsultarOrdenPagoPorNumeroMovimientoYCodigoBanco(ByVal numeroOperacion As String, ByVal codigoBanco As String) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOrdenPago As BEOrdenPago = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenPagoPorNumeroMovimientoYCodigoBanco", numeroOperacion, codigoBanco)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultarPorNumeroPagoYCodigoBanco")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function

    Public Function COnsultarOrdenPagoPorNumeroMovimientoYCodigoBancoEnBatch(ByVal numeroOperacion As String, ByVal codigoBanco As String, ByVal numeroOrdenPago As Int64) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeOrdenPago As BEOrdenPago = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenPagoPorNumeroMovimientoYCodigoBancoEnBatch", numeroOperacion, codigoBanco, numeroOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read()
                    obeOrdenPago = New BEOrdenPago(reader, "ConsultarPorNumeroPagoYCodigoBanco")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function

#Region "Devoluciones"
    Public Function ObtenerEmailsAdminCip() As String()
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim EmailsRetornar As New List(Of String)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarEmailsAdminCips]")
                While reader.Read()
                    EmailsRetornar.Add(Convert.ToString(reader("Email")))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
        End Try
        Return EmailsRetornar.ToArray()
    End Function
    Public Function RegistrarDevolucion(ByVal oBEDevolucion As BEDevolucion) As BEDevolucion
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oBEDevolucionRetorno As New BEDevolucion()
        Try
            'Registrar Devolucion
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_RegistrarDevolucion]", _
                                                                    oBEDevolucion.NroCIP, _
                                                                    oBEDevolucion.ClienteNombres, _
                                                                    oBEDevolucion.ClienteApellidos, _
                                                                    oBEDevolucion.ClienteNroDNI, _
                                                                    oBEDevolucion.ClienteDireccion, _
                                                                    oBEDevolucion.Observaciones, _
                                                                    oBEDevolucion.NombreArchivoDevolucion, _
                                                                    oBEDevolucion.EmailRepresentante, _
                                                                    oBEDevolucion.Monto, _
                                                                    oBEDevolucion.IdUsuarioCreacion)
                While reader.Read()
                    oBEDevolucionRetorno = New BEDevolucion(reader, 1)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
        End Try
        Return oBEDevolucionRetorno
    End Function
    Public Function ConsultarDevoluciones(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Dim listBEDevolucion As List(Of BusinessEntityBase) = New List(Of BusinessEntityBase)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarDevoluciones]", _
                                                                    oBEDevolucion.IdDevolucion, _
                                                                    oBEDevolucion.NroCIP, _
                                                                    oBEDevolucion.ClienteNombres, _
                                                                    oBEDevolucion.ClienteNroDNI, _
                                                                    oBEDevolucion.IdServicio, _
                                                                    oBEDevolucion.IdEmpresaContratante, _
                                                                    oBEDevolucion.IdEstadoCliente, _
                                                                    oBEDevolucion.IdEstadoAdmin, _
                                                                    oBEDevolucion.IdUsuarioCreacion, _
                                                                    oBEDevolucion.FechaInicio, _
                                                                    oBEDevolucion.FechaFinal, _
                                                                    oBEDevolucion.PageNumber, _
                                                                    oBEDevolucion.PageSize, _
                                                                    oBEDevolucion.PropOrder, _
                                                                    oBEDevolucion.TipoOrder)
                While reader.Read()
                    listBEDevolucion.Add(New BEDevolucion(reader, "ConsultaGeneral"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return listBEDevolucion
        End Try
        Return listBEDevolucion
    End Function

    Public Function ActualizarDevolucion(ByVal oBEDevolucion As BEDevolucion) As Int64
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Int64 = 0
        Try
            Int64.TryParse(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarDevolucion]", _
                                                                    oBEDevolucion.IdDevolucion, _
                                                                    oBEDevolucion.IdEstadoAdmin, _
                                                                    oBEDevolucion.IdUsuarioActualizacion, _
                                                                    oBEDevolucion.FlagEnProceso), retorna)
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return -1
        End Try
        Return retorna
    End Function

    Public Function ConsultarCipDevolucion(ByVal NroCip As Int64, ByVal IdUsuario As Int32) As BEOrdenPago
        Dim oBEOrdenPago As New BEOrdenPago()
        oBEOrdenPago = Nothing
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCipDevolucion]", _
                                                                    NroCip, _
                                                                    IdUsuario)
                While reader.Read()
                    oBEOrdenPago = (New BEOrdenPago(reader, 1, 1))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return oBEOrdenPago
        End Try
        Return oBEOrdenPago
    End Function

    Public Function ConsultarDevolucionesExcel(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Dim listBEDevolucion As List(Of BusinessEntityBase) = New List(Of BusinessEntityBase)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarDevolucionesExcel]", _
                                                                    oBEDevolucion.IdDevolucion, _
                                                                    oBEDevolucion.NroCIP, _
                                                                    oBEDevolucion.ClienteNombres, _
                                                                    oBEDevolucion.ClienteNroDNI, _
                                                                    oBEDevolucion.IdServicio, _
                                                                    oBEDevolucion.IdEmpresaContratante, _
                                                                    oBEDevolucion.IdEstadoCliente, _
                                                                    oBEDevolucion.IdEstadoAdmin, _
                                                                    oBEDevolucion.IdUsuarioCreacion, _
                                                                    oBEDevolucion.FechaInicio, _
                                                                    oBEDevolucion.FechaFinal)
                While reader.Read()
                    listBEDevolucion.Add(New BEDevolucion(reader, "ConsultaGeneralExcel"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return listBEDevolucion
        End Try
        Return listBEDevolucion
    End Function
#End Region
    Public Function ActualizarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Try
            retorna = Int64.Parse(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarCIP]", _
                                                                    oBEOrdenPago.NumeroOrdenPago, _
                                                                    oBEOrdenPago.FechaAExpirar, _
                                                                    oBEOrdenPago.IdUsuarioActualizacion))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return -1
        End Try
        Return retorna
    End Function
    Public Function EliminarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Try
            retorna = Int64.Parse(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_EliminarCIP]", _
                                                                    oBEOrdenPago.NumeroOrdenPago, _
                                                                    oBEOrdenPago.IdUsuarioActualizacion, _
                                                                    oBEOrdenPago.IdOrigenEliminacion))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return -1
        End Try
        Return retorna
    End Function
    Public Function ConsultarOrdenPagoPorIdOrdenYIdRepresentante(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim Result As BEOrdenPago = Nothing
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenPagoPorIdOrdenYIdRepresentante", _
                                                                    obeOrdenPago.NumeroOrdenPago, _
                                                                    obeOrdenPago.IdUsuarioActualizacion)
                While reader.Read()
                    Result = New BEOrdenPago(reader, "MinInfoCIP")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return Result
    End Function

#Region "SagaInicio"
    Public Function WSConsultarSolicitudPagoPorCodServCodTran(ByVal obeSolicitudPago As BESolicitudPago) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As String = String.Empty
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("[PagoEfectivo].[prc_ConsultarSolicitudXMLPorCodTransaccionYCodServicio]", obeSolicitudPago.CodServicio, obeSolicitudPago.CodTransaccion)
                While reader.Read()
                    retorna = reader(0)
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return retorna

    End Function
#End Region

#Region "ToGenerado"
    Public Function ConsultarOrdenPagoPorIdSoloGenYExp(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oDbCommand As DbCommand

        Try
            oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarOrdenPagoPorIdSoloGenYExp", obeOrdenPago.IdOrdenPago)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    ' obeOrdenPago = New BEOrdenPago(reader, "ConceptoPago")
                    obeOrdenPago = New BEOrdenPago(reader, "Completo")
                    If Not DataUtil.ObjectToString(reader("VersionOrdenPago")) = String.Empty Then
                        obeOrdenPago.VersionOrdenPago = DataUtil.ObjectToString(reader("VersionOrdenPago"))
                    End If

                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return obeOrdenPago

    End Function

    Public Function ActualizarCIPDeExpAGen(ByVal oBEOrdenPago As BEOrdenPago) As Int64
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Try
            retorna = Int64.Parse(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarCIPDeExpAGen]", _
                                                                    oBEOrdenPago.NumeroOrdenPago, _
                                                                    oBEOrdenPago.FechaAExpirar, _
                                                                    oBEOrdenPago.IdUsuarioActualizacion, _
                                                                    oBEOrdenPago.MotivoDeExpAGen))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return -1
        End Try
        Return retorna
    End Function

    Public Function ExtornarCIP(ByVal nroCIP As String, ByVal tipoExtorno As Integer) As Int64
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim retorna As Integer = 0
        Try
            If (tipoExtorno = 1) Then
                retorna = Int64.Parse(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ExtornoEliminacion]", nroCIP))
            Else
                retorna = Int64.Parse(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_ExtornoPendiente]", nroCIP))
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Return -1
        End Try
        Return retorna
    End Function

#End Region

#Region "Messaging"
    Public Function RegisterCellPhoneNumber(ByVal oBEMobileMessaging As BEMobileMessaging) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim result As Integer = 0

        Try
            'Registro de celular

            result = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegisterCellPhoneNumber]", _
                                                                    oBEMobileMessaging.PaymentOrderId, _
                                                                    oBEMobileMessaging.CountryCode, _
                                                                    oBEMobileMessaging.PhoneNumber))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

        Return result
    End Function

    Public Function ValidateCipExpiration(ByVal PaymentOrderId As Int64) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim result As String = ""

        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ValidateCipExpiration]", _
                                                                       PaymentOrderId)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    result = reader(0)
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return result

    End Function

    Public Function GetMessageSms(ByVal smsTemplateId As Integer) As String
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim result As String = ""

        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_GetSmsTemplate]", _
                                                          SmsTemplateId)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)

                While reader.Read()
                    result = reader(0)
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return result

    End Function
#End Region

End Class
