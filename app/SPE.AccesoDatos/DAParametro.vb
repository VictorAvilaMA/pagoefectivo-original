Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class DAParametro
    Implements IDisposable

    '
    'CONSULTAR PARAMETRO POR ESTADO MANTENIMIENTO
    Public Function ConsultarParametroPorCodigoGrupo(ByVal CodigoGrupo As String) As List(Of BEParametro)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listParametros As New List(Of BEParametro)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarParametroPorCodigoGrupo", _
            CodigoGrupo)

                While reader.Read
                    '
                    listParametros.Add(New BEParametro(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return listParametros
        '
    End Function

    Public Function ListarVersionServicio() As List(Of BEParametro)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listParametros As New List(Of BEParametro)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarVersionServicio")

                While reader.Read
                    '
                    listParametros.Add(New BEParametro(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return listParametros
        '
    End Function
    

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
