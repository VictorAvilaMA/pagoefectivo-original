Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades

Public Class DAPasarela
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    Implements IDisposable

#Region "Mantenimiento de la Tabla Pasarela"

    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Dim objbeservicio As BEServicio = CType(be, BEServicio)
        Try
            IdServicio = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarPasarela]", objbeservicio.Codigo, objbeservicio.IdEmpresaContratante, _
            objbeservicio.Nombre, objbeservicio.IdEstado, objbeservicio.TiempoExpiracion, objbeservicio.LogoImagen, objbeservicio.IdUsuarioCreacion, objbeservicio.Url, _
            objbeservicio.IdTipoNotificacion, objbeservicio.UrlFTP, objbeservicio.Usuario, objbeservicio.Password, _
             objbeservicio.ClaveAPI, objbeservicio.ClaveSecreta, objbeservicio.UsaUsuariosAnonimos, objbeservicio.IdTipoIntegracion))
            '*************API*********** 
            objbeservicio.IdServicio = IdServicio
        Catch ex As Exception
            Logger.Write(ex)
            Throw ex
        End Try
        Return IdServicio

    End Function

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Dim objbeservicio As BEServicio = CType(be, BEServicio)
        Try
            '****************API***********************
            IdServicio = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarPasarela]", objbeservicio.Codigo, objbeservicio.IdServicio, _
            objbeservicio.IdEmpresaContratante, objbeservicio.Nombre, objbeservicio.IdEstado, objbeservicio.TiempoExpiracion, _
            objbeservicio.LogoImagen, objbeservicio.IdUsuarioActualizacion, objbeservicio.Url, _
            objbeservicio.IdTipoNotificacion, objbeservicio.UrlFTP, objbeservicio.Usuario, objbeservicio.Password, _
            objbeservicio.ClaveAPI, objbeservicio.ClaveSecreta, objbeservicio.UsaUsuariosAnonimos, objbeservicio.IdTipoIntegracion))

        Catch ex As Exception
            Logger.Write(ex)
            Throw ex
        End Try

        Return IdServicio
    End Function

#End Region

#Region "Configuración de Pasarela"

    Public Function ConsultarContratoXMLPasarela() As String

        Dim xmlTramaPasRuta As String
        Dim contenido As String

        Try
            xmlTramaPasRuta = ConsultarRutaXMLContratoPasarela()

            Using fs As System.IO.StreamReader = System.IO.File.OpenText(xmlTramaPasRuta)
                contenido = fs.ReadToEnd()
            End Using

            Return contenido
        Catch ex As Exception
            Logger.Write(ex.Message)
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' obtener la ruta del archivo XML
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarRutaXMLContratoPasarela() As String
        Return System.Configuration.ConfigurationManager.AppSettings("xmlTramaPasRuta")
    End Function

#End Region

End Class
