Imports System.Data.Common
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports SPE.Logging

Public Class DAPlantilla
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    Private ReadOnly _dlogger As ILogger = New Logger()

    Public Sub New()

    End Sub

    Public Function ConsultarPlantillaPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Dim obePlantilla As BEPlantilla = Nothing
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaPorTipoYVariacion]", idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
                While reader.Read
                    obePlantilla = New BEPlantilla(reader)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obePlantilla
    End Function
    Public Function ConsultarPlantillaPorTipoYVariacionExiste(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Dim obePlantilla As BEPlantilla = Nothing
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaPorTipoYVariacionSiExiste]", idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
                While reader.Read
                    obePlantilla = New BEPlantilla(reader)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obePlantilla
    End Function

    Public Function ConsultarPlantillaParametroPorIdPlantilla(ByVal idPlantilla As Integer) As List(Of BEPlantillaParametro)
        Dim obeListaPlantillaParametro As New List(Of BEPlantillaParametro)()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaParametroPorIdPlantilla]", idPlantilla)
                While reader.Read
                    obeListaPlantillaParametro.Add(New BEPlantillaParametro(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeListaPlantillaParametro
    End Function

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdPlantilla As Integer = 0
        Dim objbePlantilla As BEPlantilla = CType(be, BEPlantilla)
        Try
            IdPlantilla = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarPlantilla]", objbePlantilla.IdPlantilla, _
            objbePlantilla.PlantillaHtml, objbePlantilla.IdUsuarioActualizacion))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return IdPlantilla
    End Function


    Public Function CantidadPlantillasPorIdServicio(ByVal idServicio As Integer) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CantidadPlantillasPorIdServicio]", idServicio))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    '<add Peru.Com FaseIII>
    Public Function ConsultarPlantillaTipoPorMoneda(ByVal idMoneda As Int32) As List(Of BEPlantillaTipo)

        Dim listaBEPlantillaTipo As New List(Of BEPlantillaTipo)()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaTipoPorMoneda]", idMoneda)
                While reader.Read
                    listaBEPlantillaTipo.Add(New BEPlantillaTipo(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listaBEPlantillaTipo
    End Function

    '<add Peru.Com FaseIII>
    Public Function ConsultarPlantillaTipoPorMonedaYServicio(ByVal idMoneda As Int32, ByVal idServicio As Int32) As List(Of BEPlantillaTipo)

        Dim listaBEPlantillaTipo As New List(Of BEPlantillaTipo)()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaTipoPorMonedaYServicio]", idMoneda, idServicio)
                While reader.Read
                    listaBEPlantillaTipo.Add(New BEPlantillaTipo(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listaBEPlantillaTipo
    End Function


    Public Function GenerarPlantillaDesdePlantillaBase(ByVal oBEPlantillaRequest As BEPlantilla) As BEPlantilla
        Dim obePlantilla As BEPlantilla = Nothing
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            objDatabase.ExecuteScalar("[PagoEfectivo].[prc_GenerarPlantillaDesdePlantillaBase]", oBEPlantillaRequest.IdPlantillaTipo, oBEPlantillaRequest.IdServicio, oBEPlantillaRequest.IdUsuarioCreacion, oBEPlantillaRequest.CodigoPlantillaTipoVariacion)
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaPorTipoYVariacion]", oBEPlantillaRequest.IdServicio, oBEPlantillaRequest.IdPlantillaTipo, oBEPlantillaRequest.CodigoPlantillaTipoVariacion)
                While reader.Read
                    obePlantilla = New BEPlantilla(reader)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obePlantilla
    End Function
    Public Function ConsultarParametrosFuentes() As List(Of BEPlantillaParametro)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listParametros As New List(Of BEPlantillaParametro)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarParametrosFuente]")

                While reader.Read
                    listParametros.Add(New BEPlantillaParametro(reader, "ConsultaParametrosFuente"))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return listParametros
        '
    End Function
    Public Function RegistrarPlantillaParametros(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarPlantillaParametro]", _
                            oBEPlantillaParametro.IdPlantilla, _
                            If(oBEPlantillaParametro.IdPlantillaFuenteParametro = 0, DBNull.Value, oBEPlantillaParametro.IdPlantillaFuenteParametro), _
                            oBEPlantillaParametro.IdTipoParametro, _
                            oBEPlantillaParametro.Etiqueta, _
                            oBEPlantillaParametro.Valor))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function ActualizarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarPlantillaParametro]", _
                            oBEPlantillaParametro.IdPlantillaParametro, _
                            oBEPlantillaParametro.IdPlantilla, _
                            If(oBEPlantillaParametro.IdPlantillaFuenteParametro = 0, DBNull.Value, oBEPlantillaParametro.IdPlantillaFuenteParametro), _
                            oBEPlantillaParametro.IdTipoParametro, _
                            If(oBEPlantillaParametro.Etiqueta = "", DBNull.Value, oBEPlantillaParametro.Etiqueta), _
                            If(oBEPlantillaParametro.Valor = "", DBNull.Value, oBEPlantillaParametro.Valor)))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function EliminarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_EliminarPlantillaParametro]", _
                            oBEPlantillaParametro.IdPlantillaParametro))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function ActualizarPlantilla(ByVal be As BEPlantilla) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim result As Integer = 0
        Dim objbePlantilla As BEPlantilla = CType(be, BEPlantilla)
        Try
            objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarPlantilla]", objbePlantilla.IdPlantilla, objbePlantilla.PlantillaHtml, objbePlantilla.IdUsuarioActualizacion)
            result = 1
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return result
    End Function

#Region "IMPLEMENTACIÓN PARA MONEDERO ELECTRÓNICO"
    Public Function ConsultarPlantillaEmailPorId(ByVal idPlantilla As Integer) As BEPlantilla
        Dim obePlantilla As BEPlantilla = Nothing
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaEmailPorTipo]", idPlantilla)
                While reader.Read
                    obePlantilla = New BEPlantilla(reader, 0)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obePlantilla
    End Function

    Public Function ConsultarPlantillaParametroEmailPorIdPlantilla(ByVal idPlantilla As Integer) As List(Of BEPlantillaParametro)
        Dim obeListaPlantillaParametro As New List(Of BEPlantillaParametro)()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillaParametroEmailPorIdPlantilla]", idPlantilla)
                While reader.Read
                    obeListaPlantillaParametro.Add(New BEPlantillaParametro(reader, "ConsultaParametroEmail"))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeListaPlantillaParametro
    End Function
#End Region

    '************************************************************************************************** 
    ' Método          : ConsultarParametrosFuentes
    ' Descripción     : Consulta las plantillas de los correos correspondientes a un servicio para la pasarela de Pagos
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 29/05/2012
    ' Parametros_In   : idServicio , idMoneda
    ' Parametros_Out  : Lista de Plantillas
    ''**************************************************************************************************
    Public Function ConsultarPLantillasXServicioGenPago(ByVal idServicio As Integer, ByVal idMoneda As Integer) As List(Of BEPlantilla)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listBEPlantilla As New List(Of BEPlantilla)
        Dim obePlantilla As BEPlantilla = Nothing
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPLantillasXServicioGenPago]", idServicio, idMoneda)
                While reader.Read
                    obePlantilla = New BEPlantilla(reader, "PlantillaGenPago")
                    obePlantilla.ListaParametros = ConsultarPlantillaParametroPorIdPlantilla(obePlantilla.IdPlantilla)
                    listBEPlantilla.Add(obePlantilla)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listBEPlantilla
    End Function

    Function ConsultarPlantillasXServicioQueEsPE(ByVal idServicio As Integer, ByVal idMoneda As Integer) As List(Of BEPlantilla)
        Dim oDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Plantillas As New List(Of BEPlantilla)
        Try
            Using reader As IDataReader = oDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarPlantillasXServicioQueEsPE]", idServicio, idMoneda)
                While reader.Read
                    Dim obePlantilla As New BEPlantilla(reader, "PlantillaQueEsPE")
                    obePlantilla.ListaParametros = ConsultarPlantillaParametroPorIdPlantilla(obePlantilla.IdPlantilla)
                    Plantillas.Add(obePlantilla)
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Plantillas
    End Function

    Public Function ObtenerSeccionesPlantillaPorServicio(ByVal idServicio As Integer, ByVal idPlantillaFormato As Integer) As List(Of BEPlantillaSeccion)
        Dim obeSeccionPlantilla As New List(Of BEPlantillaSeccion)()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[ObtenerSeccionesPlantillaPorServicio]", idServicio, idPlantillaFormato)
                While reader.Read
                    obeSeccionPlantilla.Add(New BEPlantillaSeccion(reader))
                End While
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeSeccionPlantilla
    End Function

    Public Function ActualizarServicioSeccion(ByVal lstPlantillaSeccion As List(Of BEPlantillaSeccion), ByVal idUsuario As Integer) As Integer
        Dim obeSeccionPlantilla As New List(Of BEPlantillaSeccion)()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try

            For i As Integer = 0 To lstPlantillaSeccion.Count - 1
                Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[ActualizarServicioSeccion]", lstPlantillaSeccion.Item(i).IdServicio,
                                                                                                                                   lstPlantillaSeccion.Item(i).IdPlantillaFormato,
                                                                                                                                   lstPlantillaSeccion.Item(i).IdSeccion,
                                                                                                                                   lstPlantillaSeccion.Item(i).Contenido,
                                                                                                                                   idUsuario)
                End Using
            Next

        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return 1
    End Function

End Class
