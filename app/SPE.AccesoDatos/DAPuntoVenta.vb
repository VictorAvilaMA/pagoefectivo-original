Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SPE.Entidades
Imports System.Configuration
Imports System.Data.Common
Imports SPE.Logging

Public Class DAPuntoVenta
    Implements IDisposable
    Private ReadOnly _dlogger As ILogger = New Logger()

    Public Function RegistrarPuntoVentaYTerminalDesdePago(ByVal CodPuntoVenta As String, ByVal NroSerieTerminal As String) As Hashtable
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Hashtable = Nothing
        Dim IdAperturaOrigen As Integer = 0
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarPuntoVentaYTerminalDesdePago]", _
                CodPuntoVenta, NroSerieTerminal)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                If reader.Read Then
                    Resultado = New Hashtable()
                    Resultado("IdAperturaOrigen") = Convert.ToInt32(reader("IdAperturaOrigen"))
                    Resultado("IdPuntoVenta") = Convert.ToString(reader("IdPuntoVenta"))
                    Resultado("RegistroPuntoVenta") = Convert.ToBoolean(reader("RegistroPuntoVenta"))
                    Resultado("IdTerminal") = Convert.ToString(reader("IdTerminal"))
                    Resultado("RegistroTerminal") = Convert.ToBoolean(reader("RegistroTerminal"))
                End If
            End Using
        Catch ex As Exception
            _dlogger.Error(ex, String.Format("RegistrarPuntoVentaYTerminalDesdePago - CodPuntoVenta:{0},NroSerieTerminal:{1},Mensaje:{2}", CodPuntoVenta, NroSerieTerminal, ex.Message))
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ObtenerAperturaOrigenLife(ByVal idTipoOrigenCancelacion As Integer) As Int64
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ObtenerAperturaOrigenLife", idTipoOrigenCancelacion)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            resultado = Convert.ToInt64(objDatabase.ExecuteScalar(oDbCommand))
        Catch ex As Exception
            _dlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return resultado
    End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
