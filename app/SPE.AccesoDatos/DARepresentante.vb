Imports System.Data
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports System.Data.SqlClient
Imports System.Configuration

Public Class DARepresentante
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    'REGISTRAR REPRESENTANTE
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Dim obeRepresentante As SPE.Entidades.BERepresentante = CType(be, SPE.Entidades.BERepresentante)

        Try

            'Resultado = objDatabase.ExecuteScalar("PagoEfectivo.prc_RegistrarRepresentante", obeRepresentante.IdCiudad, _
            'obeRepresentante.Nombres, obeRepresentante.Apellidos, obeRepresentante.IdTipoDocumento, obeRepresentante.NumeroDocumento, _
            'obeRepresentante.Direccion, obeRepresentante.Telefono, obeRepresentante.Email, obeRepresentante.Password, _
            'obeRepresentante.IdEmpresaContratante, obeRepresentante.IdGenero, obeRepresentante.FechaNacimiento, _
            'obeRepresentante.CodigoRegistro, obeRepresentante.IdEstado, obeRepresentante.IdUsuarioCreacion)

            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim dt As New DataTable
                dt.Columns.Add("IdParam")
                dt.Columns.Add("ValueParam")
                Dim indice As Integer = 0
                For Each item As String In obeRepresentante.EmIdsEmpresas
                    dt.Rows.Add(indice, item)
                    indice += 1
                Next
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_RegistrarRepresentante]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@pintIdCiudad", SqlDbType.Int).Value = obeRepresentante.IdCiudad
                oSqlCommand.Parameters.Add("@pstrNombres", SqlDbType.VarChar).Value = obeRepresentante.Nombres
                oSqlCommand.Parameters.Add("@pstrApellidos", SqlDbType.VarChar).Value = obeRepresentante.Apellidos
                oSqlCommand.Parameters.Add("@pintIdTipoDocumento", SqlDbType.Char).Value = obeRepresentante.IdTipoDocumento
                oSqlCommand.Parameters.Add("@pstrNroDocumento", SqlDbType.VarChar).Value = obeRepresentante.NumeroDocumento
                oSqlCommand.Parameters.Add("@pstrDireccion", SqlDbType.VarChar).Value = obeRepresentante.Direccion
                oSqlCommand.Parameters.Add("@pstrTelefono", SqlDbType.VarChar).Value = obeRepresentante.Telefono
                oSqlCommand.Parameters.Add("@pstrEmail", SqlDbType.VarChar).Value = obeRepresentante.Email
                oSqlCommand.Parameters.Add("@pstrPassword", SqlDbType.VarChar).Value = obeRepresentante.Password
                oSqlCommand.Parameters.Add("@pintIdGenero", SqlDbType.Int).Value = obeRepresentante.IdGenero
                oSqlCommand.Parameters.Add("@pdateFechaNacimiento", SqlDbType.DateTime).Value = obeRepresentante.FechaNacimiento
                oSqlCommand.Parameters.Add("@pstrCodigoRegistro", SqlDbType.VarChar).Value = obeRepresentante.CodigoRegistro
                oSqlCommand.Parameters.Add("@pintIdEstado", SqlDbType.Int).Value = obeRepresentante.IdEstado
                oSqlCommand.Parameters.Add("@pintIdUsuarioCreacion", SqlDbType.Int).Value = obeRepresentante.IdUsuarioCreacion
                oSqlCommand.Parameters.Add("@ploteIdsEmpresas", SqlDbType.Structured).Value = dt
                oSqlConnection.Open()
                Resultado = oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return Resultado
    End Function

    'ACTULIZAR REPRESENTANTE
    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeRepresentante As SPE.Entidades.BERepresentante = CType(be, SPE.Entidades.BERepresentante)

        Try

            'Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarRepresentante]", obeRepresentante.IdRepresentante, _
            'obeRepresentante.IdCiudad, obeRepresentante.Nombres, obeRepresentante.Apellidos, _
            'obeRepresentante.IdTipoDocumento, obeRepresentante.NumeroDocumento, obeRepresentante.Direccion, obeRepresentante.Telefono, _
            'obeRepresentante.Password, obeRepresentante.IdEmpresaContratante, _
            'obeRepresentante.IdGenero, obeRepresentante.FechaNacimiento, _
            'obeRepresentante.IdEstado, _
            'obeRepresentante.IdUsuarioActualizacion))

            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim dt As New DataTable
                dt.Columns.Add("IdParam")
                dt.Columns.Add("ValueParam")
                Dim indice As Integer = 0
                For Each item As String In obeRepresentante.EmIdsEmpresas
                    dt.Rows.Add(indice, item)
                    indice += 1
                Next
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_ActualizarRepresentante]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@pintIdRepresentante", SqlDbType.Int).Value = obeRepresentante.IdRepresentante
                oSqlCommand.Parameters.Add("@pintIdCiudad", SqlDbType.Int).Value = obeRepresentante.IdCiudad
                oSqlCommand.Parameters.Add("@pstrNombres", SqlDbType.VarChar).Value = obeRepresentante.Nombres
                oSqlCommand.Parameters.Add("@pstrApellidos", SqlDbType.VarChar).Value = obeRepresentante.Apellidos
                oSqlCommand.Parameters.Add("@pintIdTipoDocumento", SqlDbType.Char).Value = obeRepresentante.IdTipoDocumento
                oSqlCommand.Parameters.Add("@pstrNroDocumento", SqlDbType.VarChar).Value = obeRepresentante.NumeroDocumento
                oSqlCommand.Parameters.Add("@pstrDireccion", SqlDbType.VarChar).Value = obeRepresentante.Direccion
                oSqlCommand.Parameters.Add("@pstrTelefono", SqlDbType.VarChar).Value = obeRepresentante.Telefono
                oSqlCommand.Parameters.Add("@pstrPassword", SqlDbType.VarChar).Value = obeRepresentante.Password
                oSqlCommand.Parameters.Add("@pintIdGenero", SqlDbType.Int).Value = obeRepresentante.IdGenero
                oSqlCommand.Parameters.Add("@pdateFechaNacimiento", SqlDbType.DateTime).Value = obeRepresentante.FechaNacimiento
                oSqlCommand.Parameters.Add("@pintIdEstado", SqlDbType.Int).Value = obeRepresentante.IdEstado
                oSqlCommand.Parameters.Add("@pintIdUsuarioActualizacion", SqlDbType.Int).Value = obeRepresentante.IdUsuarioActualizacion
                oSqlCommand.Parameters.Add("@ploteIdsEmpresas", SqlDbType.Structured).Value = dt
                oSqlConnection.Open()
                oSqlCommand.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return Resultado
    End Function


    'VALIDA SI EXISTE EL EMAIL
    Public Overrides Function GetObjectByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Object

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeRepresentante As SPE.Entidades.BERepresentante = CType(be, SPE.Entidades.BERepresentante)

        Try

            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarEmail]", obeRepresentante.Email))

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return Resultado
    End Function

    'CONSULTAR REPRESENTANTE
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListRepresentante As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeRepresentante As SPE.Entidades.BERepresentante = CType(be, SPE.Entidades.BERepresentante)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarRepresentante", obeRepresentante.Nombres, _
                                        obeRepresentante.Apellidos, obeRepresentante.IdEstado)
                While reader.Read()

                    ListRepresentante.Add(New SPE.Entidades.BERepresentante(reader, "Busq"))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListRepresentante

    End Function

    'CONSULTAR REPRESENTANTE POR ID
    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeRepresentante As New SPE.Entidades.BERepresentante

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarRepresentantePorIdRepresentante]", CInt(id))
                If reader.Read Then

                    obeRepresentante = New SPE.Entidades.BERepresentante(reader)

                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return obeRepresentante

    End Function
    Public Function ObtenerEmpresasLibres(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListEmpresa As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeEmpresa As SPE.Entidades.BEEmpresaContratante = CType(be, SPE.Entidades.BEEmpresaContratante)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ObtenerEmpresasLibres", _
            obeEmpresa.RazonSocial)
                While reader.Read()

                    ListEmpresa.Add(New SPE.Entidades.BEEmpresaContratante(reader))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListEmpresa

    End Function
    Public Function ObtenerEmpresasLibresParaServicios(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListEmpresa As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeEmpresa As SPE.Entidades.BEEmpresaContratante = CType(be, SPE.Entidades.BEEmpresaContratante)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.[prc_ObtenerEmpresasLibresParaRegServicio]", _
            obeEmpresa.RazonSocial)
                While reader.Read()

                    ListEmpresa.Add(New SPE.Entidades.BEEmpresaContratante(reader))

                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return ListEmpresa

    End Function

    Public Overrides Function GetListByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Select Case key
            Case ""
                Return ObtenerEmpresasLibres(be)
            Case "EmpresasLibresParaServicio"
                Return ObtenerEmpresasLibresParaServicios(be)
        End Select
        Return Nothing

    End Function

    Public Function CantidadRepresentantesPorIdEmpresaContratante(ByVal idEmpresaContratante As Integer) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CantidadRepresentantesPorIdEmpresa]", idEmpresaContratante))
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Function ObtenerEmpresaporIDRepresentante(ByVal objRepresentante As BERepresentante) As BERepresentante

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeRepresentante As New SPE.Entidades.BERepresentante

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarEmpresaxIDUsuario]", objRepresentante.IdUsuario)
                If reader.Read Then

                    obeRepresentante = New SPE.Entidades.BERepresentante(reader, "Obt")

                End If
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return obeRepresentante

    End Function
    'Agregado requerimiento #24567 redmine
    'Jonathan Bastidas - Jonathan.bastidas@ec.pe
    'Consulta empresas por usuario (solo para representantes)
    Public Function ConsultarEmpresasPorIdUsuario(ByVal be As BEUsuarioBase) As List(Of BEEmpresaContratante)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListEmpresa As New List(Of BEEmpresaContratante)
        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarEmpresaContratantePorUsuario", be.IdUsuario)
                While reader.Read()
                    ListEmpresa.Add(New BEEmpresaContratante(reader, "busq"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return ListEmpresa
    End Function
End Class
