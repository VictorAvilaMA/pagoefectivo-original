Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common

Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades
Imports System.Data.Common
Imports System.Configuration
Imports SPE.Logging


Public Class DAServicio
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    Implements IDisposable

    Private ReadOnly _logger As ILogger = New Logger()

    Public Function ConsultarServicioFormularioUrl(ByVal IdServicio As Integer, ByVal URL As String) As SPE.Entidades.BEServicio

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing
        Dim NombreUrl As String = ""

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicioFormularioURL]", IdServicio, URL)

                While reader.Read
                    obeServicio = New BEServicio(reader, "ServicioFormulario")
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)

        End Try
        Return obeServicio
    End Function



    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Dim objbeservicio As BEServicio = CType(be, BEServicio)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarServicio]", objbeservicio.Codigo, objbeservicio.IdEmpresaContratante, _
            objbeservicio.Nombre, objbeservicio.IdEstado, objbeservicio.TiempoExpiracion, objbeservicio.LogoImagen, objbeservicio.IdUsuarioCreacion, objbeservicio.Url, _
            objbeservicio.IdTipoNotificacion, objbeservicio.UrlFTP, objbeservicio.Usuario, objbeservicio.Password, _
                objbeservicio.ClaveAPI, objbeservicio.ClaveSecreta, objbeservicio.UsaUsuariosAnonimos, objbeservicio.IdTipoIntegracion, objbeservicio.UrlNotificacion, _
                objbeservicio.UrlRedireccionamiento, objbeservicio.CodigoServicioVersion, objbeservicio.ProximoAfiliado, objbeservicio.VisibleEnPortada, _
                objbeservicio.RutaClavePublica, objbeservicio.RutaClavePrivate, objbeservicio.ExtornoUnico, objbeservicio.FlgEmailCCAdmin, objbeservicio.FlgEmailGenUsuario, _
                objbeservicio.FlgEmailGenCIP, objbeservicio.FlgEmailPagoCIP, objbeservicio.FlgEmailExpCIP, objbeservicio.TiempoEmailExpiracion, objbeservicio.FormularioActivo, _
            objbeservicio.FormularioNombreUrl, objbeservicio.FormularioItemsMaximo, objbeservicio.FormularioCantidad, objbeservicio.FormularioPlaceHolderDatosAdicionales, _
            objbeservicio.FormularioMensajePersonalizado, objbeservicio.FormularioUrlTerminos, objbeservicio.FormularioRutaLogo, _
             objbeservicio.FormularioRutaLogoResponsive, objbeservicio.FormularioRutaFondoHeader, objbeservicio.FormularioRutaFondoHeaderResponsive, objbeservicio.FormularioCorreo, objbeservicio.EstadoDireccion,
             objbeservicio.IdCanalVenta, objbeservicio.IdWebService, objbeservicio.Ip, objbeservicio.CantidadMaximaPlanillaMes,
             objbeservicio.CantidadCobrosPlanilla, objbeservicio.MontoMinimoCobro, objbeservicio.MontoMaximoCobro,
             objbeservicio.MontoMinimoPlanilla, objbeservicio.MontoMaximoPlanilla, objbeservicio.VigenciaMinimaDias, objbeservicio.VigenciaMinimaHoras,
             objbeservicio.VigenciaMaximaDias, objbeservicio.VigenciaMaximaHoras, objbeservicio.DiaMaximoInicioCobranza,
             objbeservicio.EnvioEmailOperador, objbeservicio.EmailOperador, objbeservicio.NombreLogo, objbeservicio.HorasAvisoExpiracion, objbeservicio.EnvioGenerarCip, objbeservicio.TipoPlantilla, objbeservicio.LogoServicio)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            IdServicio = CInt(objDatabase.ExecuteScalar(oDbCommand))
            '*************API*********** 
            objbeservicio.IdServicio = IdServicio
        Catch ex As Exception
            _logger.Error(ex, ex.Message)

        End Try
        Return IdServicio

    End Function

    Public Sub RegistrarServicioTipoOrigenCancelacion(ByVal IdServicio As Integer, ByVal IdTipoOrigenCancelacion As Integer, ByVal IdEstado As Integer, ByVal IdUsuario As Integer)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarServicioTipoOrigenCancelacion]", IdServicio, IdTipoOrigenCancelacion, IdEstado)
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Sub

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Dim objbeservicio As BEServicio = CType(be, BEServicio)
        Try
            '****************API***********************
            IdServicio = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarServicio]", objbeservicio.Codigo, objbeservicio.IdServicio, _
            objbeservicio.IdEmpresaContratante, objbeservicio.Nombre, objbeservicio.IdEstado, objbeservicio.TiempoExpiracion, _
            objbeservicio.LogoImagen, objbeservicio.IdUsuarioActualizacion, objbeservicio.Url, _
            objbeservicio.IdTipoNotificacion, objbeservicio.UrlFTP, objbeservicio.Usuario, objbeservicio.Password, _
               objbeservicio.ClaveAPI, objbeservicio.ClaveSecreta, objbeservicio.UsaUsuariosAnonimos, objbeservicio.IdTipoIntegracion, _
               objbeservicio.UrlNotificacion, objbeservicio.UrlRedireccionamiento, objbeservicio.RutaClavePublica, objbeservicio.RutaClavePrivate, _
               objbeservicio.CodigoServicioVersion, objbeservicio.ProximoAfiliado, objbeservicio.VisibleEnPortada, objbeservicio.ExtornoUnico, _
               objbeservicio.FlgEmailCCAdmin, objbeservicio.FlgEmailGenUsuario, objbeservicio.FlgEmailGenCIP, objbeservicio.FlgEmailPagoCIP, _
            objbeservicio.FlgEmailExpCIP, objbeservicio.TiempoEmailExpiracion, objbeservicio.FormularioActivo, _
            objbeservicio.FormularioNombreUrl, objbeservicio.FormularioItemsMaximo, objbeservicio.FormularioCantidad, objbeservicio.FormularioPlaceHolderDatosAdicionales, _
            objbeservicio.FormularioMensajePersonalizado, objbeservicio.FormularioUrlTerminos, objbeservicio.FormularioRutaLogo, _
             objbeservicio.FormularioRutaLogoResponsive, objbeservicio.FormularioRutaFondoHeader, objbeservicio.FormularioRutaFondoHeaderResponsive, objbeservicio.FormularioCorreo, objbeservicio.EstadoDireccion,
             objbeservicio.IdCanalVenta, objbeservicio.IdWebService, objbeservicio.Ip, objbeservicio.CantidadMaximaPlanillaMes,
             objbeservicio.CantidadCobrosPlanilla, objbeservicio.MontoMinimoCobro, objbeservicio.MontoMaximoCobro,
             objbeservicio.MontoMinimoPlanilla, objbeservicio.MontoMaximoPlanilla, objbeservicio.VigenciaMinimaDias, objbeservicio.VigenciaMinimaHoras,
             objbeservicio.VigenciaMaximaDias, objbeservicio.VigenciaMaximaHoras, objbeservicio.DiaMaximoInicioCobranza,
             objbeservicio.EnvioEmailOperador, objbeservicio.EmailOperador, objbeservicio.NombreLogo, objbeservicio.HorasAvisoExpiracion, objbeservicio.EnvioGenerarCip, objbeservicio.TipoPlantilla, objbeservicio.LogoServicio))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)

        End Try

        Return IdServicio
    End Function

    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0

        Dim obeservicio As BEServicio = CType(be, BEServicio)
        Dim listaServicios As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)


        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicio]", obeservicio.Codigo, obeservicio.NombreEmpresaContrante, obeservicio.Nombre, obeservicio.IdEstado, obeservicio.IdEmpresaContratante)
                While reader.Read
                    listaServicios.Add(New BEServicio(reader, "busq"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        Finally

        End Try
        Return listaServicios


    End Function

    Public Function ConsultarServicioPorUrl(ByVal parametro As Object) As SPE.Entidades.BEServicio

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicioPorUrl]", parametro)

                While reader.Read
                    obeServicio = New BEServicio(reader, "busqPorUrl")
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeServicio
    End Function

    Public Function ConsultarServicioCabeceraPorId(ByVal idServicio As Integer) As BEServicio
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            '*************API*************
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarServicioPorId]", idServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "mantenimiento")
                End While

            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objresultServicios


    End Function

    Public Overrides Function GetRecordByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            _logger.Info(String.Format("GetRecordByID - IdServicio: {0}", id))
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarServicioPorId]", id)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "MantenimientoPorId")
                End While

            End Using
            If (objresultServicios IsNot Nothing) Then
                _logger.Info(String.Format("GetRecordByID - IdServicio: {0}, FlgNotificaMov: {1}, UrlNotificacion:{2}", id, objresultServicios.FlgNotificaMovimiento, objresultServicios.UrlNotificacion))
                objresultServicios.ListaTipoOrigenCancelacion = ConsultarTiposOrigenesCancelacionPorIdServicio(id)
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
        End Try

        Return objresultServicios

    End Function
    Public Overrides Function GetEntityByID(ByVal id As Object) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As New BusinessMessageBase()
        response.Entity = GetRecordByID(id)
        Return response
    End Function

    Public Function ConsultarTiposOrigenesCancelacionPorIdServicio(ByVal IdServicio As Integer) As List(Of BETipoOrigenCancelacion)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListTipoOrigenesCancelacion As New List(Of BETipoOrigenCancelacion)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarTiposOrigenesCancelacionPorIdServicio]", IdServicio)

            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'M Fin
            Dim reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
            While reader.Read
                ListTipoOrigenesCancelacion.Add(New BETipoOrigenCancelacion(reader, "Carga"))
            End While
            reader.Close()
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return ListTipoOrigenesCancelacion
        '
    End Function
    'CONSULTA PAGO DE SERVICIOS, METODO PARA EL REPORTE
    Public Function ConsultarPagoServicios(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
        Dim ListOrdenPago As New List(Of BEOrdenPago)
        Dim sp As String = ConsultarPagoServiciosSP(obeOrdenPago)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand(sp, _
                    obeOrdenPago.IdOrdenPago, _
                    obeOrdenPago.IdsServicios, _
                    obeOrdenPago.IdMoneda, _
                    obeOrdenPago.IdEstado, _
                    obeOrdenPago.FechaCreacion, _
                    obeOrdenPago.FechaActualizacion, _
                    obeOrdenPago.IdTipoOrigenCancelacion)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))
            Dim reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)

            While reader.Read
                ListOrdenPago.Add(New BEOrdenPago(reader, "ConsultaPagoServicios"))
            End While

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        '
        Return ListOrdenPago
        '
    End Function

    Public Function ConsultarPagoServiciosNoRepresentante(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexionReplica)
        Dim ListaOrdenPago As New List(Of BEOrdenPago)
        Dim sp As String = ConsultarPagoServiciosSP(obeOrdenPago)
        Try
            Dim reader As IDataReader
            Dim oDbCommand As DbCommand

            If obeOrdenPago.IdOrdenPago = "0" Then
                oDbCommand = objDatabase.GetStoredProcCommand(sp + "NoRepresentante", _
                        obeOrdenPago.IdEmpresa, _
                        obeOrdenPago.IdsServicios, _
                        obeOrdenPago.IdMoneda, _
                        obeOrdenPago.IdEstado, _
                        obeOrdenPago.FechaCreacion, _
                        obeOrdenPago.FechaActualizacion, _
                        obeOrdenPago.IdTipoOrigenCancelacion)
            Else
                oDbCommand = objDatabase.GetStoredProcCommand(sp, _
                        obeOrdenPago.IdOrdenPago, _
                        obeOrdenPago.IdsServicios, _
                        obeOrdenPago.IdMoneda, _
                        obeOrdenPago.IdEstado, _
                        obeOrdenPago.FechaCreacion, _
                        obeOrdenPago.FechaActualizacion, _
                        obeOrdenPago.IdTipoOrigenCancelacion)
            End If
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQLReplica"))
            reader = objDatabase.ExecuteReader(oDbCommand)

            While reader.Read
                ListaOrdenPago.Add(New BEOrdenPago(reader, "ConsultaPagoServicios"))
            End While

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListaOrdenPago
    End Function

    Public Function ConsultarPagoServiciosSP(ByVal obeOrdenPago As BEOrdenPago) As String
        If (obeOrdenPago.DescripcionEstado = SPE.EmsambladoComun.ParametrosSistema.TipoConsultaPG.NroOrdenes) Then
            Return "PagoEfectivo.prc_ConsultaPagoServiciosNroOperaciones"
        Else
            Return "PagoEfectivo.prc_ConsultaPagoServicios"
        End If
    End Function

    'CONSULTA DE 
    Public Function ConsultarServiciosPorIdUsuario(ByVal obeServicio As BEServicio) As List(Of BEServicio)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListServicio As New List(Of BEServicio)
        Try
            Dim reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarServiciosPorIdUsuario", obeServicio.IdUsuarioCreacion)
            While reader.Read
                ListServicio.Add(New BEServicio(reader))
            End While
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListServicio
    End Function

#Region "Configuraci�n de Servicios"
    Public Function ConsultarContratoXMLDeServicios(ByVal codigoAcceso As String) As String
        Try
            If (codigoAcceso <> System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRutaCodAcceso")) Then
                Throw New Exception("No se pudo guardar, el C�digo de Acceso no es el Correcto.")
            End If
            Dim xmlTramaServRuta As String = ConsultarRutaXMLContratoServicios()
            Dim contenido As String = ""
            Using fs As System.IO.StreamReader = System.IO.File.OpenText(xmlTramaServRuta)
                contenido = fs.ReadToEnd()
            End Using
            Return contenido
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex)
            Throw ex
        End Try
    End Function
    Public Function ConsultarRutaXMLContratoServicios() As String
        Return System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRuta")
    End Function

    Public Function GuardarContratoXMLDeServicios(ByVal codigoAcceso As String, ByVal contratoXml As String) As String
        Try
            If (codigoAcceso <> System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRutaCodAcceso")) Then
                Throw New Exception("No se pudo guardar, el C�digo de Acceso no es el Correcto.")
            End If
            Dim xmlTramaServRuta As String = ConsultarRutaXMLContratoServicios()
            System.IO.File.WriteAllText(xmlTramaServRuta, contratoXml)
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex)
            Throw ex
        End Try
        Return ConsultarContratoXMLDeServicios(codigoAcceso)
    End Function
#End Region

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cAPI"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarServicioPorAPI(ByVal cAPI As String) As BEServicio
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarServiciosPorAPI", cAPI)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Dim reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
            While reader.Read
                obeServicio = New BEServicio(reader, "ConsultarServicioPorAPI")
            End While

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeServicio
    End Function

    ''' <summary>
    ''' dj.- consultar servicio por codigo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarServicioPorCodigo(ByVal Codigo As String) As BEServicio

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing

        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarServiciosPorCodigo", Codigo)
            While reader.Read
                'obeServicio = New BEServicio(reader, "codigo")
                '<upd Peru.Com FaseIII>
                'motivo: no se encontro constructor con la opcion codigo, por eso se procedio a homogenizar la contruccion con mantenimiento
                obeServicio = New BEServicio(reader, "ConsultarServicioPorAPI")
            End While

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeServicio

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cAPI"></param>
    ''' <param name="cClave"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidarServicioPorAPIYClave(ByVal cAPI As String, ByVal cClave As String) As Boolean
        Try
            Using obeServicio As BEServicio = ConsultarServicioPorAPI(cAPI)
                If IsNothing(obeServicio) Then
                    Return False
                Else
                    Return obeServicio.ClaveSecreta = cClave
                End If
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function CantidadServiciosPorIdEmpresaContratante(ByVal idEmpresaContratante As Integer) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CantidadServiciosPorIdEmpresa]", idEmpresaContratante))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function CantidadTipoOrigenCancelaPorIdServicio(ByVal idServicio As Integer) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_CantidadTipoOrigenCancelaPorIdServicio]", idServicio))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Overrides Function DeleteEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As New _3Dev.FW.Entidades.BusinessMessageBase()
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = ObjectToInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarServicio]", ObjectToInt(request.EntityId)))
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Error
            Throw ex
        End Try
        Return response
    End Function


    Public Function ConsultarCantidadServiciosPorCodigo(ByVal codigo As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ExisteCodigoServicio]", codigo))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ConsultarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim obeservicio As BEServicio = CType(be, BEServicio)
        Dim listaServicios As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicioxEmpresaContratante]", obeservicio.Codigo, obeservicio.NombreEmpresaContrante, obeservicio.Nombre, obeservicio.IdEstado, obeservicio.TipoComision, obeservicio.IdMoneda, obeservicio.PageNumber, obeservicio.PageSize, obeservicio.PropOrder, obeservicio.TipoOrder)
                While reader.Read
                    listaServicios.Add(New BEServicio(reader, "conServicioXEmpresa"))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        Finally

        End Try
        Return listaServicios


    End Function
    Public Function ActualizaServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Dim objbeservicio As BEServicio = CType(be, BEServicio)
        Try

            IdServicio = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarServicioComision]", objbeservicio.IdServicio, objbeservicio.IdMoneda, objbeservicio.IdUsuarioActualizacion, objbeservicio.TipoComision, objbeservicio.MontoComision, objbeservicio.PorcentajeComision, objbeservicio.IdEstadoComision, objbeservicio.MontoLimite, objbeservicio.MontoLimitePorcentaje))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return IdServicio
    End Function

    Public Function ValidarMetodoBloqueadoXServicio(ByVal IdServicio As Integer, ByVal TipoMetodo As Integer) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ConsultarMetodoBloqueadoXServicio]", IdServicio, TipoMetodo))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    '<add Peru.Com FaseIII>
    Public Function UpdateServicioContratoXML(ByVal objbeservicio As BEServicio) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Try
            Dim obj As Object = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarServicioContratoXML]", objbeservicio.IdServicio, objbeservicio.ContratoXML, objbeservicio.IdUsuarioActualizacion)
            IdServicio = CInt(obj)

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return IdServicio
    End Function

    Public Function GetServicioContratoXML(ByVal codigoApi As String) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ContratoXML As String = ""
        Try
            Dim obj As Object = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerServicioContratoXML]", codigoApi)
            ContratoXML = CStr(obj)

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return ContratoXML
    End Function
    Public Function ConsultarTransaccionesxServicio(ByVal objbeservicio As BEServicio, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing


        Dim oDbCommand As DbCommand
        oDbCommand = objDataBase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarTransaccionesxServicio", objbeservicio.IdEmpresaContratante, objbeservicio.IdMoneda, objFechaAl, objFechaDel)

        oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("ValTimeOut"))
        Try

            ''Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarTransaccionesxServicio", objbeservicio.IdEmpresaContratante, objbeservicio.IdMoneda, objFechaAl, objFechaDel)
            Using reader As IDataReader = objDataBase.ExecuteReader(oDbCommand)
                While reader.Read
                    obeServicio = New BEServicio(reader, "consultaTransaccion")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeServicio

    End Function

    Public Function ConsultarTransaccionesxServicioLote(ByVal objbeservicio As BEServicio, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing

        Try

            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarTransaccionesxServicioLote", objbeservicio.IdEmpresaContratante, objbeservicio.IdMoneda, objFechaAl, objFechaDel)
                While reader.Read
                    obeServicio = New BEServicio(reader, "consultaTransaccion")
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeServicio

    End Function

    '<add Peru.Com FaseIII >
    '************************************************************************************************** 
    ' M�todo                : RegistrarAsociacionCuentaServicio
    ' Descripci�n           : Registrar una Asociacion de Servicio con Cuenta Bancaria
    ' Autor                 : Jonathan Bastidas
    ' Fecha Creacion        : xx/xx/2011
    ' Fecha Actualizaci�n   : 30/05/2012 - Jonathan Bastidas
    ' Parametros_In         : BEServicioBanco
    ' Parametros_Out        : Id de la relacion ServicioBanco
    ''**************************************************************************************************
    Public Function RegistrarAsociacionCuentaServicio(ByVal be As BEServicioBanco) As Long

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Long = 0
        Try
            Resultado = CLng(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarAsociacionCuentaServicio]", _
                                                       be.idServicio, be.IdBanco, _
                                                       be.IdMoneda, be.CodigoServicio, _
                                                       be.IdEstado, be.idCuentaBanco, _
                                                       be.IdUsuarioCreacion, _
                                                       be.FlagVisibleGenPago))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado

    End Function
    '************************************************************************************************** 
    ' M�todo                : ActualizarAsociacionCuentaServicio
    ' Descripci�n           : Actualizar una asociacion de Servicio con Cuenta Bancaria
    ' Autor                 : Jonathan Bastidas
    ' Fecha Creacion        : xx/xx/2011
    ' Fecha Actualizaci�n   : 30/05/2012 - Jonathan Bastidas
    ' Parametros_In         : BEServicioBanco
    ' Parametros_Out        : Id de la relacion ServicioBanco
    ''**************************************************************************************************
    Public Function ActualizarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeCB As BEServicioBanco = CType(be, BEServicioBanco)

        Try

            Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarAsociacionCuentaServicio]", _
                                                         obeCB.idServicio, _
                                                         obeCB.IdBanco, _
                                                         obeCB.IdMoneda, _
                                                         obeCB.CodigoServicio, _
                                                         obeCB.IdEstado, _
                                                         obeCB.idCuentaBanco, _
                                                         obeCB.IdUsuarioActualizacion, _
                                                         obeCB.FlagVisibleGenPago))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

        Return Resultado

    End Function
    Public Function ConsultarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListBEServicioBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCB As SPE.Entidades.BEServicioBanco = CType(be, SPE.Entidades.BEServicioBanco)

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarAsociacionCuentaServicioPG]", _
                                                                    obeCB.idEmpresaContratante, obeCB.idServicio, obeCB.IdBanco, _
                                                                    obeCB.IdMoneda, obeCB.IdEstado, obeCB.PageNumber, obeCB.PageSize, obeCB.PropOrder, obeCB.TipoOrder)
                While reader.Read()
                    ListBEServicioBanco.Add(New SPE.Entidades.BEServicioBanco(reader, "Consultar"))
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListBEServicioBanco
    End Function

    '<upd Proc.Tesoreria>
    Public Function ConsultarCodigosServiciosPorBanco(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListBEServicioBanco As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCB As SPE.Entidades.BEBanco = CType(be, SPE.Entidades.BEBanco)

        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarCodigoServicioByBanco]", _
                                                                    obeCB.IdBanco)
                While reader.Read()
                    Dim oBEServicioBanco As SPE.Entidades.BEServicioBanco = New SPE.Entidades.BEServicioBanco
                    oBEServicioBanco.CodigoServicio = reader("Codigo").ToString()
                    ListBEServicioBanco.Add(oBEServicioBanco)
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListBEServicioBanco
    End Function

    '************************************************************************************************** 
    ' M�todo                : ObtenerAsociacionCuentaServicio
    ' Descripci�n           : Obtiene un BEServicioBanco apartir de un IdServicioBanco
    ' Autor                 : Jonathan Bastidas
    ' Fecha Creacion        : xx/xx/2011
    ' Fecha Actualizaci�n   : 30/05/2012 - Jonathan Bastidas
    ' Parametros_In         : BEServicioBanco - IdServicioBanco
    ' Parametros_Out        : BEServicioBanco
    ''**************************************************************************************************
    Public Function ObtenerAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BEServicioBanco
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)

        Dim obeCB As SPE.Entidades.BEServicioBanco = CType(be, SPE.Entidades.BEServicioBanco)
        Dim obeServicio As BEServicioBanco = Nothing
        Try

            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ObtenerAsociacionCuentaServicio]", _
                                                                    obeCB.IdServicioBanco)
                While reader.Read()
                    obeServicio = New BEServicioBanco(reader, "Obtener")
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeServicio
    End Function

    Public Function EliminarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Boolean
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Boolean = False
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarAsociacionCuentaServicio]", ObjectToInt(DirectCast(be, BEServicioBanco).IdServicioBanco))
            Resultado = True
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function
    Public Function ObtenerServicioComisionporID(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeSC As SPE.Entidades.BEServicio = CType(be, SPE.Entidades.BEServicio)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicioComisionPorId]", obeSC.IdServicio, obeSC.IdMoneda)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "MantenimientoSCPorId")
                End While
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objresultServicios

    End Function

    Public Function ConsultarDetalleTransaccionesxServicio(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BETransferencia = Nothing
        Dim ListTransferencia As New List(Of BETransferencia)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleTransaccionesxServicio", objbeservicio.idEmpresaContratante, objbeservicio.IdMoneda, objbeservicio.FechaFin, objbeservicio.FechaInicio, objbeservicio.idTransfenciaEmpresas)
            While reader.Read
                ListTransferencia.Add(New BETransferencia(reader, "consultaDetalleTransaccion"))
            End While
            reader.Close()
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListTransferencia

    End Function

    Public Function ConsultarDetalleTransaccionesxServicioSaga(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaSaga)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BETransferencia = Nothing
        Dim ListTransferencia As New List(Of BETransferenciaSaga)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleTransaccionesxServicioSaga", objbeservicio.idEmpresaContratante, objbeservicio.IdMoneda, objbeservicio.FechaFin, objbeservicio.FechaInicio, objbeservicio.idTransfenciaEmpresas)
            While reader.Read
                ListTransferencia.Add(New BETransferenciaSaga(reader, "ConsSaga"))
            End While
            reader.Close()
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListTransferencia

    End Function

    Public Function ConsultarDetalleTransaccionesxServicioRipley(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaRipley)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BETransferencia = Nothing
        Dim ListTransferencia As New List(Of BETransferenciaRipley)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleTransaccionesxServicioRipley", objbeservicio.idEmpresaContratante, objbeservicio.IdMoneda, objbeservicio.FechaFin, objbeservicio.FechaInicio, objbeservicio.idTransfenciaEmpresas)
            While reader.Read
                ListTransferencia.Add(New BETransferenciaRipley(reader, "ConsRipley"))
            End While
            reader.Close()
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListTransferencia

    End Function

    Public Function ConsultarDetalleTransaccionesPendientes(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)

        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim ListTransferencia As New List(Of BETransferencia)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleTransaccionesPendientes", _
                                                                  objbeservicio.idEmpresaContratante, _
                                                                  objbeservicio.IdMoneda, _
                                                                  objbeservicio.FechaInicio, _
                                                                  objbeservicio.FechaFin, _
                                                                  objbeservicio.FlagConciliado)
            While reader.Read
                ListTransferencia.Add(New BETransferencia(reader, "consultaDetalleTransaccionP"))
            End While
            reader.Close()
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListTransferencia

    End Function
    Public Function RegistrarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim IdServicio As Integer = 0
        Dim objbeservicio As BEServicio = CType(be, BEServicio)
        Try
            IdServicio = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarServicioComision]", objbeservicio.IdServicio, objbeservicio.IdMoneda, objbeservicio.IdUsuarioCreacion, objbeservicio.TipoComision, objbeservicio.MontoComision, objbeservicio.PorcentajeComision, objbeservicio.MontoLimite, objbeservicio.MontoLimitePorcentaje))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return IdServicio
    End Function
    Public Function ConsultarServicioPorIdEmpresa(ByVal be As BEServicio) As List(Of BEServicio)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim obeServicio As BEServicio = Nothing
        Dim ListS As New List(Of BEServicio)
        Try

            Dim reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.[prc_ServiciosPorIdEmpresa]", be.IdServicio)
            While reader.Read
                ListS.Add(New BEServicio(reader, "ServiciosPorIdEmpresa"))
            End While
            reader.Close()
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return ListS
    End Function

    Function ConsultarPorAPIYClave(ByVal CAPI As String, ByVal CClave As String) As BEServicio
        Try
            Using obeServicio As BEServicio = ConsultarServicioPorAPI(CAPI)
                If obeServicio Is Nothing Then
                    Return Nothing
                End If
                If obeServicio.ClaveSecreta <> CClave Then
                    Return Nothing
                End If
                Return obeServicio
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarComerciosAfiliados(ByVal visibleEnPortada As Boolean, ByVal proximoAfiliado As Boolean) As List(Of BEServicio)
        Dim Servicios As New List(Of BEServicio)
        Try
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarComerciosAfiliados", visibleEnPortada, proximoAfiliado)
                While reader.Read()
                    Servicios.Add(New BEServicio(reader, "mantenimiento"))
                End While
                reader.Close()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Servicios
    End Function
    Public Function ObtenerServicioPorIdWS(ByVal id As Integer) As BEServicio
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicioPorId]", id)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "MantenimientoPorId")
                End While

            End Using
            If (objresultServicios IsNot Nothing) Then
                objresultServicios.ListaTipoOrigenCancelacion = ConsultarTiposOrigenesCancelacionPorIdServicio(id)
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objresultServicios

    End Function

    Public Function ConsultarServicioPorClasificacion(ByVal be As BEServicio) As List(Of BEServicio)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim Servicios As New List(Of BEServicio)
        Dim objresultServicios As BEServicio = Nothing

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ConsultarServicioPorClasificacion]", be.Clasificacion)
                While reader.Read()
                    Servicios.Add(New BEServicio(reader, "ServiciosPorIdEmpresa"))
                End While
                reader.Close()
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return Servicios

    End Function

    Public Function ObtenerServicioInstitucionPorId(ByVal id As Integer) As BEServicio
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ObtenerServicioInstitucionPorId]", id)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "ServicioInstitucion")
                End While

            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objresultServicios

    End Function

    Public Function RecuperarDatosServicioDesconectado(ByVal id As Integer) As BEServicio
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RecuperarDatosServicioDesconectado]", id)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "ServicioProductos")
                End While

            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objresultServicios

    End Function


    Public Function RecuperarDatosProductoServicio(ByVal be As BEServicioProductoRequest) As List(Of BEServicioProducto)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim Servicios As New List(Of BEServicioProducto)

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RecuperarDatosProductoServicio]", be.idServicio)
                While reader.Read()
                    Servicios.Add(New BEServicioProducto(reader, "ListaProducto"))
                End While
                reader.Close()
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return Servicios

    End Function

    Public Function RecuperarDetalleProductoServicio(ByVal be As BEServicioProductoRequest) As BEServicioProducto
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim Servicios As New BEServicioProducto

        Try
            '*************API*************
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_RecuperarDetalleProductoServicio]", be.idProducto)
                While reader.Read()
                    Servicios = New BEServicioProducto(reader, "ConsultaProducto")
                End While
                reader.Close()
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return Servicios

    End Function




    '************************************************************************************************** 
    ' M�todo                : ActualizarAsociacionCuentaServicio
    ' Descripci�n           : Actualizar una asociacion de Servicio con Cuenta Bancaria
    ' Autor                 : Jonathan Bastidas
    ' Fecha Creacion        : xx/xx/2011
    ' Fecha Actualizaci�n   : 30/05/2012 - Jonathan Bastidas
    ' Parametros_In         : BEServicioBanco
    ' Parametros_Out        : Id de la relacion ServicioBanco
    ''**************************************************************************************************
    Public Function RegistrarOrderIdComercio(ByVal codigo As String) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Try

            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarOrderIdComercio]", codigo))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex

        End Try

        Return Resultado

    End Function

    Public Function ObtenerServicioMunicipalidadBarrancoPorId(ByVal id As Integer) As BEServicio
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Dim objresultServicios As BEServicio = Nothing

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].[prc_ObtenerServicioMunicipalidadBarrancoPorId]", id)

                While reader.Read
                    objresultServicios = New BEServicio(reader, "ServicioMunicipalidadBarranco")
                End While

            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return objresultServicios

    End Function

End Class
