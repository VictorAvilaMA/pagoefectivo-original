Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class DAServicioComun
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

    Public Function ConsultarOPsConciliadas() As System.Data.DataSet
        Dim dsResult As DataSet
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            dsResult = objDatabase.ExecuteDataSet("[PagoEfectivo].[prc_ConsultarOPConciliadasSREC]")
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return dsResult
    End Function

    Public Function ActualizarOPAMigradoRecaudacion(ByVal lista As String) As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarOPAMigradoRecaudacion]", lista)
            Return 1
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
            Return 0
        End Try
    End Function

End Class
