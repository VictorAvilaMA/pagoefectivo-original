﻿Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SPE.Entidades
Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Logging


Public Class DAServicioInstitucion
    Implements IDisposable

    Public Function RegistrarCabecera(ByVal strMerchantID As String, ByVal strCodigoUsuario As String, ByVal strCodigoUsuarioComp As String, ByVal strNombreUsuario As String, ByVal strApepaterno As String, ByVal strApeMaterno As String, ByVal strEmail As String) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarCabecera]", strMerchantID, strCodigoUsuario, strCodigoUsuarioComp, strNombreUsuario, strApepaterno, strApeMaterno, strEmail)
        Catch ex As Exception
            'Logger.Write(ex)
            Return 1
        End Try
        Return IdError
    End Function



    '<upd proc.Tesoreria>
    Public Function RegistrarLote(ByVal strMerchantID As String, ByVal dtCabecera As DataTable, ByVal dtDetalle As DataTable) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Using oSqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings(SPE.EmsambladoComun.ParametrosSistema.StrConexion).ConnectionString)
                Dim oSqlCommand As New SqlCommand()
                oSqlCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("ValTimeOut"))
                oSqlCommand.Connection = oSqlConnection
                oSqlCommand.CommandText = "[PagoEfectivo].[prc_RegistrarLote]"
                oSqlCommand.CommandType = CommandType.StoredProcedure
                oSqlCommand.Parameters.Add("@MerchantID", SqlDbType.VarChar).Value = strMerchantID
                oSqlCommand.Parameters.Add("@Cabecera", SqlDbType.Structured).Value = dtCabecera
                oSqlCommand.Parameters.Add("@Detalle", SqlDbType.Structured).Value = dtDetalle
                oSqlConnection.Open()
                Resultado = oSqlCommand.ExecuteScalar()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return Resultado
    End Function


    Public Function ValidarCarga(ByVal strMerchantID As String, ByVal dtFechaEmision As DateTime) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarCarga]", strMerchantID, dtFechaEmision)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdError
    End Function

    Public Function RegistrarDetalle(ByVal strMerchantID As String, ByVal strCodUsuario As String, ByVal strCodUsuarioComp As String, ByVal strNroDocumento As String,
                                ByVal intIdOrden As Integer, ByVal strDescripcionOrden As String, ByVal intMoneda As Integer,
                                ByVal decImporte As Decimal, ByVal dtFechaEmision As DateTime, ByVal dtFechaVencimiento As DateTime,
                                ByVal decMora As Decimal, ByVal intNumeroCargaDiaria As Integer, ByVal intTiempo As Integer, ByVal intIdOrdenPago As Integer, ByVal dtFechaCarga As DateTime) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarDetalle]", strMerchantID, strCodUsuario, strCodUsuarioComp, strNroDocumento, intIdOrden, strDescripcionOrden, intMoneda, decImporte, dtFechaEmision, dtFechaVencimiento, decMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, dtFechaCarga)
        Catch ex As Exception
            'Logger.Write(ex)
            Return 1
        End Try
        Return IdError
    End Function


    Public Function ConsultarDetallePagoServicioInstitucion(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServInst As New List(Of BEServicioInstitucion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetallePagoServicioInstitucion", _
            oBEServicioInstitucionRequest.IdServicio, oBEServicioInstitucionRequest.CodUsuario)

                While reader.Read()
                    listServInst.Add(New BEServicioInstitucion(reader, "ConsultaPago"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return listServInst

    End Function



    Public Function ConsultarInstitucionCIP(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServInst As New List(Of BEServicioInstitucion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarInstitucionCIP", _
            oBEServicioInstitucionRequest.MerchantID, oBEServicioInstitucionRequest.FechaEmision)

                While reader.Read()
                    listServInst.Add(New BEServicioInstitucion(reader, "AnularCIP"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return listServInst

    End Function


    Public Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarDocumentoInstitucion]", request.IdOrdenPago, request.NroDocumento)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdError
    End Function

    Public Function ActualizarTiempoCarga(ByVal strMerchantId As String) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarTiempoCarga]", strMerchantId)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdError
    End Function

    Public Function ConsultarArchivosDescarga(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServInst As New List(Of BEServicioInstitucion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarArchivosDescarga", _
            oBEServicioInstitucionRequest.IdServicio, oBEServicioInstitucionRequest.FechaCargaInicial, oBEServicioInstitucionRequest.FechaCargaFinal)
                While reader.Read()
                    listServInst.Add(New BEServicioInstitucion(reader, "ConsultarDescarga"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listServInst

    End Function


    Public Function ConsultarDetalleArchivoDescarga(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServInst As New List(Of BEServicioInstitucion)

        Try
            Using reader As IDataReader = objDataBase.ExecuteReader("PagoEfectivo.prc_ConsultarDetalleArchivoDescarga", _
            oBEServicioInstitucionRequest.MerchantID, oBEServicioInstitucionRequest.FechaCargaInicial)
                While reader.Read()
                    listServInst.Add(New BEServicioInstitucion(reader, "ConsultarDetalle"))
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return listServInst

    End Function



    Public Function EliminarCIPInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_EliminarCIPInstitucion]", request.IdOrdenPago, request.IdUsuarioActualizacion, request.IdOrigenEliminacion)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdError
    End Function


    Public Function ConsultarCIPDocumento(ByVal request As BEServicioInstitucionRequest) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ConsultarCIPDocumento]", request.IdServicio, request.NroDocumento)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdError
    End Function


    Public Function DeterminarIdParametro(ByVal request As BEServicioInstitucionRequest) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_DeterminarIdParametro]", request.GCParametro, request.DesParametro)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return IdError
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class




