Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports System.Data.Common
Imports System.Configuration
Imports SPE.Logging

Public Class DAServicioNotificacion
    Inherits _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
    Private ReadOnly _logger As ILogger = New Logger()

    '<add Peru.Com FaseIII> - Se modifico el procedimiento con la finalidad de que acepte notificaciones de solicitud
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeSN As SPE.Entidades.BEServicioNotificacion = CType(be, SPE.Entidades.BEServicioNotificacion)

        Try
            _logger.Info(String.Format("InsertRecord - IdServicioNotificacion:{0}, IdOrdenPago:{1}, IdTipoServNotificacion:{2}, IdMovimiento:{3}, UrlRespuesta:{4}, IdEstado:{5}, IdUsuarioCreacion:{6}, IdOrigenRegistro:{7}, IdSolicitudPago:{8}, IdGrupoNotificacion:{9}", obeSN.IdServicioNotificacion,
                                       obeSN.IdOrdenPago, obeSN.IdTipoServNotificacion, obeSN.IdMovimiento, obeSN.UrlRespuesta, obeSN.IdEstado, obeSN.IdUsuarioCreacion, obeSN.IdOrigenRegistro, obeSN.IdSolicitudPago, obeSN.IdGrupoNotificacion))

            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarServicioNotificacion]",
                                                        obeSN.IdServicioNotificacion, _
                                                        obeSN.IdOrdenPago, _
                                                        obeSN.IdTipoServNotificacion, _
                                                        obeSN.IdMovimiento, _
                                                        obeSN.UrlRespuesta, _
                                                        obeSN.IdEstado, _
                                                        obeSN.IdUsuarioCreacion, _
                                                        obeSN.IdOrigenRegistro, _
                                                        obeSN.IdSolicitudPago, _
                                                        obeSN.IdGrupoNotificacion)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            Resultado = CInt(objDatabase.ExecuteScalar(oDbCommand)) 'wjra (el chu�o) Agregue el campo IdOrigenRegistro a pedido de cmiranda

            obeSN.IdServicioNotificacion = Resultado
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado

    End Function

    Public Function ActualizarEstadoNotificacion(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        Dim obeSN As SPE.Entidades.BEServicioNotificacion = CType(be, SPE.Entidades.BEServicioNotificacion)

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarEstadoServicioNotificacion]", obeSN.IdServicioNotificacion, _
           obeSN.IdEstado, obeSN.Observacion))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ActualizarEstadoNotificacionMaviso(ByVal idstrServicioNotificacion As String, ByVal idEstado As String, ByVal observacion As String) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0
        'Dim obeSN As SPE.Entidades.BEServicioNotificacion = CType(be, SPE.Entidades.BEServicioNotificacion)

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarEstadoServicioNotificacionMasivo]", idstrServicioNotificacion, _
           idEstado, observacion))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function ConsultarNotificacionesPendientes() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarServicioNotificacionPendiente")
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarNotificacionesPendientesSaga() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarServicioNotificacionPendienteSaga")
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarServicioNotificacionPorIdEstadoSaga(ByVal idEstado As Integer) As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarServicioNotificacionPorIdEstadoSaga", idEstado)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarNotificacionesPendientesSodimac() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarServicioNotificacionPendienteSodimac")
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarServicioNotificacionReenvioSodimac() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarServicioNotificacionReenvioSodimac")
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))

            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarServicioNotificacionPorIdEstadoSodimac(ByVal idEstado As Integer) As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarServicioNotificacionPorIdEstadoSodimac", idEstado)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While


            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarNotificacionesPendientesRipley() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("PagoEfectivo.prc_ConsultarServicioNotificacionPendienteRipley")
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarServicioNotificacionPorIdEstadoRipley(ByVal idEstado As Integer) As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarServicioNotificacionPorIdEstadoRipley", idEstado)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While


            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function ConsultarServicioNotificacionPorIdEstado(ByVal idEstado As Integer) As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarServicioNotificacionPorIdEstado", idEstado)
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    'SagaInicio
    Public Function ConsultarDatosParaServicioSaga(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oBEServicioNotificacion As New BEServicioNotificacion
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDatosParaServicioSaga", IdOrdenPago)
                While reader.Read
                    oBEServicioNotificacion = (New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Saga))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return oBEServicioNotificacion
    End Function

    Public Function ConsultarOrdenesPagoSaga() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenesPagoSaga")
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Saga))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    ''' <summary>
    ''' Angello Peña
    ''' Utilizado para conectarse con replica por que se demora mas de 20 seg en devolver resultados
    ''' </summary>
    ''' <returns>listado de notificaciones</returns>
    ''' <remarks>Solo para replica, ejecutar el script prc_ConsultarOrdenesPagoSaga_GenFile</remarks>
    'Public Function ConsultarOrdenesPagoSaga() As List(Of BEServicioNotificacion)
    '    Dim cnx As String = System.Configuration.ConfigurationManager.AppSettings(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
    '    Dim cn As New System.Data.SqlClient.SqlConnection(cnx)
    '    Dim cmd As New System.Data.SqlClient.SqlCommand("PagoEfectivo.prc_ConsultarOrdenesPagoSaga_GenFile", cn)

    '    cmd.CommandTimeout = 120
    '    cn.Open()

    '    Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
    '    Try
    '        Using reader As IDataReader = cmd.ExecuteReader()
    '            While reader.Read
    '                listServicioNotificacion.Add(New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Saga))
    '            End While
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return listServicioNotificacion
    'End Function

    Public Function RegistrarLogConsumoWSSagaRequest(ByVal obeLogWSSagaRequest As BELogWebServiceSagaRequest) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogServicioNotificacionSagaRequest]", _
                                                       obeLogWSSagaRequest.OrdenPago, obeLogWSSagaRequest.OrderIdSaga, obeLogWSSagaRequest.FechaPago, obeLogWSSagaRequest.FechaContable, _
                                                       obeLogWSSagaRequest.MontoCancelado, obeLogWSSagaRequest.CajaSaga, obeLogWSSagaRequest.WebServiceSagaURL, obeLogWSSagaRequest.TramaXML))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function


    Public Function RegistrarLogConsumoWSSagaResponse(ByVal obeLogWSSagaResponse As BELogWebServiceSagaResponse) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogServicioNotificacionSagaResponse]", _
                                                       obeLogWSSagaResponse.LogIDRequest, obeLogWSSagaResponse.OrdenPago, obeLogWSSagaResponse.OrderIdSaga, obeLogWSSagaResponse.CodigoRespuesta, _
                                                       obeLogWSSagaResponse.GlosaRespuesta, obeLogWSSagaResponse.MensajeError, obeLogWSSagaResponse.Xml, obeLogWSSagaResponse.WebServiceSagaURL))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    'SagaFin

    'SodimacInicio

    Public Function ActualizarEstadoNotificacionReenvioSodimac(ByVal idServicioNotificacion As Long, ByVal idEstado As Integer) As Integer
        Dim IdError As Integer
        Try
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            IdError = objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ActualizarEstadoNotificacionReenvioSodimac]", idServicioNotificacion, idEstado)
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return IdError
    End Function

    Public Function ConsultarDatosParaServicioSodimac(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oBEServicioNotificacion As New BEServicioNotificacion
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDatosParaServicioSodimac", IdOrdenPago)
                While reader.Read
                    oBEServicioNotificacion = (New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Sodimac))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return oBEServicioNotificacion
    End Function

    Public Function ConsultarOrdenesPagoSodimac() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenesPagoSodimac")
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Sodimac))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function RegistrarLogConsumoWSSodimacRequest(ByVal obeLogWSSodimacRequest As BELogWebServiceSodimacRequest) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogServicioNotificacionSodimacRequest]", _
                                                       obeLogWSSodimacRequest.OrdenPago, obeLogWSSodimacRequest.OrderIdSodimac, obeLogWSSodimacRequest.FechaPago, obeLogWSSodimacRequest.FechaContable, _
                                                       obeLogWSSodimacRequest.MontoCancelado, obeLogWSSodimacRequest.CajaSodimac, obeLogWSSodimacRequest.WebServiceSodimacURL, obeLogWSSodimacRequest.TramaXML))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    Public Function RegistrarLogConsumoWSSodimacResponse(ByVal obeLogWSSodimacResponse As BELogWebServiceSodimacResponse) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogServicioNotificacionSodimacResponse]", _
                                                       obeLogWSSodimacResponse.LogIDRequest, obeLogWSSodimacResponse.OrdenPago, obeLogWSSodimacResponse.OrderIdSodimac, obeLogWSSodimacResponse.CodigoRespuesta, _
                                                       obeLogWSSodimacResponse.GlosaRespuesta, obeLogWSSodimacResponse.MensajeError, obeLogWSSodimacResponse.Xml, obeLogWSSodimacResponse.WebServiceSodimacURL))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function

    'SodimacFin

    'RipleyInicio
    Public Function ConsultarDatosParaServicioRipley(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim oBEServicioNotificacion As New BEServicioNotificacion
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDatosParaServicioRipley", IdOrdenPago)
                While reader.Read
                    oBEServicioNotificacion = (New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Saga))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return oBEServicioNotificacion
    End Function

    Public Function ConsultarOrdenesPagoRipley() As List(Of BEServicioNotificacion)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listServicioNotificacion As New List(Of BEServicioNotificacion)
        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarOrdenesPagoRipley")
                While reader.Read
                    listServicioNotificacion.Add(New BEServicioNotificacion(reader, 1, 1, BEServicioNotificacion.Servicio.Ripley))
                End While
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return listServicioNotificacion
    End Function

    Public Function RegistrarLogConsumoWSRipleyRequest(ByVal obeLogWSRipleyRequest As BELogWebServiceRipleyRequest) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogServicioNotificacionRipleyRequest]", _
                                                       obeLogWSRipleyRequest.OrdenPago, obeLogWSRipleyRequest.OrderIdRipley, obeLogWSRipleyRequest.FechaPago, obeLogWSRipleyRequest.FechaContable, _
                                                       obeLogWSRipleyRequest.MontoCancelado, obeLogWSRipleyRequest.CajaRipley, obeLogWSRipleyRequest.WebServiceRipleyURL, obeLogWSRipleyRequest.TramaXML))
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function


    Public Function RegistrarLogConsumoWSRipleyResponse(ByVal obeLogWSRipleyResponse As BELogWebServiceRipleyResponse) As Integer
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim Resultado As Integer = 0

        Try
            Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarLogServicioNotificacionRipleyResponse]", _
                                                       obeLogWSRipleyResponse.LogIDRequest, obeLogWSRipleyResponse.OrdenPago, obeLogWSRipleyResponse.OrderIdRipley, obeLogWSRipleyResponse.CodigoRespuesta, _
                                                       obeLogWSRipleyResponse.GlosaRespuesta, obeLogWSRipleyResponse.MensajeError, obeLogWSRipleyResponse.Xml, obeLogWSRipleyResponse.WebServiceRipleyURL))

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return Resultado
    End Function
    'RipleyFin

#Region "Mongo"
    'Public Function InsertLogNotificacionBD(ByVal obeSN As BEMongoServicioNotificacion) As Integer

    '    Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
    '    Dim Resultado As Integer = 0
    '    'Dim obeSN As SPE.Entidades.BEServicioNotificacion = CType(be, SPE.Entidades.BEServicioNotificacion)

    '    Try
    '        'M
    '        Dim oDbCommand As DbCommand
    '        oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_RegistrarLogServicioNotificacion]",
    '                                                    obeSN.IdServicioNotificacion, _
    '                                                    obeSN.IdOrdenPago, _
    '                                                    obeSN.IdTipoServNotificacion, _
    '                                                    obeSN.IdMovimiento, _
    '                                                    obeSN.UrlRespuesta, _
    '                                                    obeSN.IdEstado, _
    '                                                    obeSN.IdUsuarioCreacion, _
    '                                                    obeSN.IdOrigenRegistro)
    '        oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
    '        'End M
    '        Resultado = CInt(objDatabase.ExecuteScalar(oDbCommand))

    '        obeSN.IdServicioNotificacion = Resultado
    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        Throw ex
    '    End Try
    '    Return Resultado

    'End Function
#End Region

End Class
