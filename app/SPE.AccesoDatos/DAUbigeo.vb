
Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports SPE.Entidades
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Public Class DAUbigeo

    'CONSULTA DE PAIS
    Public Function ConsultarPais() As List(Of BEPais)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listPais As New List(Of BEPais)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarPais")

                While reader.Read
                    '
                    listPais.Add(New BEPais(reader))
                    '
                End While
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        '
        Return listPais
        '
    End Function


    'CONSULTA DE DEPARTAMENTO
    Public Function ConsultarDepartamentoPorIdPais(ByVal IdPais As Integer) As List(Of BEDepartamento)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listDepartamento As New List(Of BEDepartamento)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarDepartamentoPorIdPais", _
             IdPais)

                While reader.Read
                    '
                    listDepartamento.Add(New BEDepartamento(reader))
                    '
                End While
            End Using
        Catch ex As Exception

        End Try
        '
        Return listDepartamento
        '
    End Function


    'CONSULTA DE CIUDAD
    Public Function ConsultarCiudadPorIdDepartamento(ByVal IdDepartamento As Integer) As List(Of BECiudad)
        '
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim listCiudad As New List(Of BECiudad)

        Try
            Using reader As IDataReader = objDatabase.ExecuteReader("PagoEfectivo.prc_ConsultarCiudadPorIdDepartamento", _
            IdDepartamento)

                While reader.Read
                    '
                    listCiudad.Add(New BECiudad(reader))
                    '
                End While
            End Using
        Catch ex As Exception

        End Try
        '
        Return listCiudad
        '
    End Function

End Class
