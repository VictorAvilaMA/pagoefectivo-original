Imports System.Collections.Generic
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Data.Common
Namespace Seguridad

    Public Class DAMemberShipProvider
        Inherits _3Dev.FW.AccesoDatos.Seguridad.CustomMembershipProvider

        Public Overrides Function GetPassword(ByVal username As String) As String
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim ret As String = ""
            Try
                Using reader As IDataReader = objDatabase.ExecuteReader("[PagoEfectivo].prc_LeerPass", username)
                    While reader.Read()
                        ret = reader(0).ToString()
                    End While
                End Using
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return ret
        End Function

        Public Overrides Sub UpdateNumAttemps(ByVal username As String, ByVal Intentos As Integer)
            Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarNumIntentosUsuario]", username, Intentos)
            oDataBase.ExecuteNonQuery(db)
        End Sub
        Public Overrides Function GetStatus(ByVal username As String) As String
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirUsuario]", username)
                Dim dt As DataTable = oDataBase.ExecuteDataSet(db).Tables(0)
                Dim Status As String = dt.Rows(0)("IdEstado").ToString()
                Return Status

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

        End Function
        Public Overrides Function ExistUser(ByVal username As String) As Boolean
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirUsuario]", username)
                Dim dt As DataTable = oDataBase.ExecuteDataSet(db).Tables(0)
                If (dt.Rows.Count > 0) Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

        End Function

        Public Overrides Function getNumberAttempts(ByVal username As String) As Integer

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirNumeroIntentosUsuario]", username)
                Dim Int As DataTable = oDataBase.ExecuteDataSet(db).Tables(0)
                Dim Intentos As Integer = Convert.ToInt32(Int.Rows(0)(0))
                Return Intentos

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function
        Public Overrides Function getMaximusAttepmts() As Integer

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirNumeroIntentosMaximo]")
                Dim Int As DataTable = oDataBase.ExecuteDataSet(db).Tables(0)
                Dim Intentos As Integer = Convert.ToInt32(Int.Rows(0)(0))
                Return Intentos

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function
        Public Overrides Function getAvailableBlockUser(ByVal username As String) As Boolean

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarDisponibilidadUsuario]", username)
                Dim Int As DataTable = oDataBase.ExecuteDataSet(db).Tables(0)
                Dim result As Boolean = Convert.ToBoolean(Int.Rows(0)(0))
                Return result
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function
        Public Overrides Sub BlockUser(ByVal username As String)
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarEstadoUsuario]", username, 92)
                oDataBase.ExecuteNonQuery(db)


            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Sub
        Public Overrides Sub UnBlockUser(ByVal username As String)
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ActualizarEstadoUsuario]", username, 91)
                oDataBase.ExecuteNonQuery(db)


            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Sub


    End Class


End Namespace
