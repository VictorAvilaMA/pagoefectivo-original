
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Data.Common

Namespace Seguridad

    Public Class DARoleProvider
        Inherits _3Dev.FW.AccesoDatos.Seguridad.DASqlRoleProvider
        Public Overrides Function GetRolesForUser(ByVal username As String) As String()
            Try
                Dim RolesForUser As StringCollection = New StringCollection()
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirRolesPorUsuarios]", username)

                Using oIDR As IDataReader = oDataBase.ExecuteReader(db)
                    While oIDR.Read()
                        RolesForUser.Add(oIDR.GetString(oIDR.GetOrdinal("DESCRI_ROL")))
                    End While
                End Using

 
                Return ListaRolesAStrings(RolesForUser)

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function

        Public Overrides Function RoleExists(ByVal roleName As String) As Boolean
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_RolExiste]", roleName)
                Dim dt As DataTable = oDataBase.ExecuteDataSet(db).Tables(0)
                If (dt.Rows.Count > 0) Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function

        Public Overrides Sub CreateRole(ByVal roleName As String)
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim resultado As Integer
            Try
                resultado = CInt(objDataBase.ExecuteScalar("[PagoEfectivo].[prc_RegistrarRol]", roleName))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Sub
        Public Overrides Function GetAllRoles() As String()
            Try
                Dim RolesForUser As StringCollection = New StringCollection()
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarRoles]")

                Using oIDR As IDataReader = oDataBase.ExecuteReader(db)
                    While oIDR.Read()
                        RolesForUser.Add(oIDR.GetString(oIDR.GetOrdinal("Description")))
                    End While
                End Using


                Return ListaRolesAStrings(RolesForUser)

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function
        Public Overrides Function GetRolForUserId(ByVal UserId As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BERol)
            Try
                Dim list As New List(Of BERol)()
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarRolesPorIdUsuario]", UserId)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        list.Add(New BERol(objIDataReader))
                    End While
                End Using
                Return list
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function

        Public Overrides Function RemoveUserFromRol(ByVal UserId As Integer, ByVal RolId As Integer) As Boolean
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            Try
                Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarUsuarioXRol]", _
                UserId, _
                RolId))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return Resultado
        End Function
        Public Overrides Function AddUserToRol(ByVal UserId As Integer, ByVal RolId As Integer) As Boolean
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            Try
                Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_AgregarUsuarioXRol]", _
                UserId, _
                RolId))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return Resultado
        End Function
        Public Overrides Function GetRolBySistema(ByVal SistemaId As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BERol)
            Dim list As New List(Of BERol)()

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarRoles]")

                Using oIDR As IDataReader = oDataBase.ExecuteReader(db)
                    While oIDR.Read()
                        list.Add(New BERol(oIDR))
                    End While
                End Using
                Return list
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function


        Public Overrides Function GetRolLikeNameForSistemaSiteMap(ByVal Name As String, ByVal SistemaId As Integer, ByVal SiteMapId As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BERol)

            Dim Roles As List(Of BERol) = New List(Of BERol)
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prcRolForSistema]", Name, SistemaId, SiteMapId)
                Dim rol As New BERol

                Using oIDR As IDataReader = oDataBase.ExecuteReader(db)

                    While oIDR.Read()

                        rol.Pertenece = Convert.ToInt32(oIDR("pertenece").ToString())
                        Roles.Add(rol)
                    End While

                end using
                Return Roles


            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

        End Function


        Public Overrides Function GetRolLikeName(ByVal Name As String) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BERol)

            Dim list As New List(Of BERol)()

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarRoles]")

                Using oIDR As IDataReader = oDataBase.ExecuteReader(db)
                    While oIDR.Read()
                        list.Add(New BERol(oIDR))
                    End While
                End Using
                Return list
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

        End Function
        Public Overrides Function DeleteRole(ByVal roleName As String, ByVal throwOnPopulatedRole As Boolean) As Boolean
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            Try
                Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarRol]", _
                roleName))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return Resultado > 0
        End Function
        Public Overrides Function AddSiteMapToRol(ByVal SiteMapId As Integer, ByVal RolId As Integer) As Boolean
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            Try
                Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_RegistrarRolPagina]", SiteMapId, RolId))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return True
        End Function
        Public Overrides Function RemoveSiteMapFromRol(ByVal SiteMapId As Integer, ByVal RolId As Integer) As Boolean
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            Try
                Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_EliminarRolPagina]", SiteMapId, RolId))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return True
        End Function
    End Class


  



End Namespace
