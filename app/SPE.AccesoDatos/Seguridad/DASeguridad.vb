Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Configuration
Imports System.Data.Common

Public Class DASeguridad

    Public Sub New()

    End Sub

    Public Function ValidarServicioClaveAPI(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Integer = 0
        Try
            resultado = _3Dev.FW.Util.DataUtil.ObjectToInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarClaveAPI]", claveAPI, claveSecreta))
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado > 0
    End Function

    '<add Peru.Com FaseIII>
#Region "ModuloSeguridadEncriptacion"
    Public Function SaveKeyPrivate(ByVal codEntidad As Int32, ByVal keyPrivate As String)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Boolean = False
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarServicioKeyPrivate]", codEntidad, keyPrivate)
            resultado = True
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
    'Public Function GetKeyPrivate(ByVal codEntidad As String) As Byte()
    '    Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
    '    Dim resultado As Byte()
    '    Try
    '        resultado = DirectCast(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerPrivateKey]", codEntidad), Byte())
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return resultado
    'End Function
    Public Function SaveKeyPublic(ByVal idServicio As Integer, ByVal keyPublic As String)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Boolean = False
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarServicioKeyPublic]", idServicio, keyPublic)
            resultado = True
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function GetKeyPublic(ByVal idServicio As Integer) As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim RutaClavePublica As String = String.Empty
        Try
            'M
            Dim oDbCommand As DbCommand
            oDbCommand = objDatabase.GetStoredProcCommand("[PagoEfectivo].[prc_ObtenerPublicKey]", idServicio)
            oDbCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutSQL"))
            'End M
            Using reader As IDataReader = objDatabase.ExecuteReader(oDbCommand)
                While reader.Read()
                    If reader("RutaClaPub") IsNot DBNull.Value Then
                        RutaClavePublica = reader("RutaClaPub").ToString()
                    End If
                End While
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return RutaClavePublica
    End Function

    Public Function SaveKeyPublicSPE(ByVal keyPublic As String)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Boolean = False
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarKeyPublicSPE]", keyPublic)
            resultado = True
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function SaveKeyPrivateSPE(ByVal keyPrivate As String)
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As Boolean = False
        Try
            objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarKeyPrivateSPE]", keyPrivate)
            resultado = True
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function GetKeyPublicSPE() As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As String
        Try
            resultado = DirectCast(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerPublicKeySPE]"), String)
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function GetKeyPrivateSPE() As String
        Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
        Dim resultado As String
        Try
            resultado = DirectCast(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerPrivateKeySPE]"), String)
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function

#End Region

End Class

