Imports System.Collections.Generic
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Data.Common


Namespace Seguridad

    Public Class DASiteMapProvider
        Inherits _3Dev.FW.AccesoDatos.Seguridad.DACustomSiteMapProvider

        Public Overrides Function BuildSiteMap(ByVal idSistema As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BESiteMap)
            Try
                Dim SiteMapList As New List(Of BESiteMap)
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarMenu]", idSistema)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        SiteMapList.Add(New BESiteMap(objIDataReader))
                    End While
                End Using
                Return SiteMapList

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function



        Public Overrides Function GetPagesLikNameForSistema(ByVal Name As String, ByVal idSistema As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BESiteMap)


            Try
                Dim SiteMapList As New List(Of BESiteMap)
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarPaginasXIdSistemaXNombre]", idSistema, Name)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        SiteMapList.Add(New BESiteMap(objIDataReader))
                    End While
                End Using
                Return SiteMapList

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

        End Function

        Public Overrides Function GetSiteMapsByIdRol(ByVal idRol As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BESiteMap)
            Try
                Dim SiteMapList As New List(Of BESiteMap)
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarPaginasPorIdRol]", idRol)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        SiteMapList.Add(New BESiteMap(objIDataReader))
                    End While
                End Using
                Return SiteMapList

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function
        Public Overrides Function GetCabezeraMenu(ByVal urlPagina As String) As String
            Try
                Dim SiteMapList As New List(Of BESiteMap)
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim oCadena As Object
                oCadena = oDataBase.ExecuteScalar("[PagoEfectivo].[prc_ObtenerCabezeraMenu]", urlPagina)
                Return If(oCadena.Equals(DBNull.Value), String.Empty, CStr(oCadena))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return ""
        End Function
    End Class

End Namespace
