Imports System.Collections.Generic
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports System.Data.Common
Imports System.Configuration


Namespace Seguridad

    Public Class DAUsuario
        Inherits _3Dev.FW.AccesoDatos.Seguridad.DAUsuario
        Public Overrides Function GetUserInfoByUserName(ByVal userName As String) As _3Dev.FW.Entidades.Seguridad.BEUserInfo
            'Dim eUsuario As SPE.Entidades.BEUserInfo = Nothing
            Dim eUsuario As _3Dev.FW.Entidades.Seguridad.BEUserInfo = Nothing
            Dim listaPaginas As New List(Of _3Dev.FW.Entidades.Seguridad.BERolPagina)
            Dim ListaCorrecta As New List(Of _3Dev.FW.Entidades.Seguridad.BERolPagina)
            Dim UsActMon As String
            Dim ListaUsActMon() As String
            Dim ValEstMon As Boolean = False
            Dim ListaRoles() As String
            Dim ContieneRolCliente As Boolean = False

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConseguirUsuario]")
                oDataBase.AddInParameter(db, "email", DbType.String, userName)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        eUsuario = New _3Dev.FW.Entidades.Seguridad.BEUserInfo(objIDataReader, 1)
                    End While
                End Using
                Dim roleProvider As New DARoleProvider()
                If Not eUsuario Is Nothing Then

                    'Set Valores para usuarios a los que no le afecta el estado de Monedero Electronico
                    UsActMon = ConfigurationManager.AppSettings("UsuariosActivosMonedero").ToString()
                    ListaUsActMon = Split(UsActMon, ",")

                    For Each a As String In ListaUsActMon
                        If a = eUsuario.Email Then
                            ValEstMon = True
                            Exit For
                        End If
                    Next


                    eUsuario.Roles = roleProvider.GetRolesForUser(userName)

                    ListaRoles = eUsuario.Roles
                    For Each t As String In ListaRoles
                        If t = "Cliente" Then
                            ContieneRolCliente = True
                            Exit For
                        End If
                    Next

                    eUsuario.EsJefeProducto = Array.IndexOf(eUsuario.Roles, SPE.Entidades.BEUserInfo.RolJefeProducto) >= 0
                    'eUsuario.EsJefeProducto = Array.IndexOf(eUsuario.Roles, SPE.Entidades.BEUserInfo.RolJefeProducto) >= 0
                    listaPaginas = DamePaginasAccesables(userName)
                    If eUsuario.FlagRegistrarPc Then
                        ListaCorrecta = listaPaginas
                    Else
                        For Each a As _3Dev.FW.Entidades.Seguridad.BERolPagina In listaPaginas
                            If a.IdPagina <> SPE.EmsambladoComun.ParametrosSistema.IdPaginaAdminDisp Then

                                ListaCorrecta.Add(a)

                            End If
                        Next
                    End If
                    Dim ListaCorrecta2 As New List(Of BERolPagina)


                    If eUsuario.Rol.Trim() <> "Cliente" And eUsuario.Rol.Trim() <> "Representante" Then
                        eUsuario.HabilitarMonedero = True
                    End If

                    If ConfigurationManager.AppSettings("ActivarMonedero") Or ValEstMon Then
                        If eUsuario.HabilitarMonedero Then
                            For Each x As BERolPagina In ListaCorrecta
                                ListaCorrecta2.Add(x)
                            Next
                        Else
                            'If eUsuario.Rol = "Cliente" Then
                            eUsuario.HabilitarMonedero = False
                            If ContieneRolCliente Then
                                For Each a As _3Dev.FW.Entidades.Seguridad.BERolPagina In ListaCorrecta
                                    Select Case a.IdPagina
                                        Case SPE.EmsambladoComun.ParametrosSistema.DV_Consultar_Cuentas_de_Dinero,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Consulta_Detalle_Cuenta,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Pagar_CIP,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Consultar_Movimientos,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Ver_solicitudes_de_retiro_de_dinero,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Ver_detalle_de_las_Solicitudes_de_retiro_de_dinero,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Solicitar_Retiro_de_Dinero,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Establecer_Monto_de_Recarga,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Transferencia_de_Dinero,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Movimientos_Sospechosos,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Solicitar_Cuenta,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Det_Sol_Crear_Cta,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Sol_Ret_Din,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Det_Cta,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Cta_Cli,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Mov_CTa,
                                            SPE.EmsambladoComun.ParametrosSistema.DV_MonederoElectronico
                                        Case Else
                                            ListaCorrecta2.Add(a)
                                    End Select
                                Next
                            Else
                                For Each x As BERolPagina In ListaCorrecta
                                    ListaCorrecta2.Add(x)
                                Next
                            End If
                        End If
                    Else
                        'Aqui la Logica
                        eUsuario.HabilitarMonedero = False
                        'If eUsuario.Rol = "Cliente" Then
                        If ContieneRolCliente Then
                            For Each a As _3Dev.FW.Entidades.Seguridad.BERolPagina In ListaCorrecta
                                Select Case a.IdPagina
                                    Case SPE.EmsambladoComun.ParametrosSistema.DV_Consultar_Cuentas_de_Dinero,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Consulta_Detalle_Cuenta,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Pagar_CIP,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Consultar_Movimientos,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Ver_solicitudes_de_retiro_de_dinero,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Ver_detalle_de_las_Solicitudes_de_retiro_de_dinero,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Solicitar_Retiro_de_Dinero,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Establecer_Monto_de_Recarga,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Transferencia_de_Dinero,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Movimientos_Sospechosos,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Mis_Cuentas,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Solicitar_Cuenta,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Det_Sol_Crear_Cta,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Sol_Ret_Din,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Det_Cta,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Cta_Cli,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_Rpt_Mov_CTa,
                                        SPE.EmsambladoComun.ParametrosSistema.DV_MonederoElectronico
                                    Case Else
                                        ListaCorrecta2.Add(a)
                                End Select
                            Next
                        Else
                            For Each x As BERolPagina In ListaCorrecta
                                ListaCorrecta2.Add(x)
                            Next
                        End If
                    End If

                    eUsuario.PaginasAcceso = ListaCorrecta2

                    eUsuario.SessionUser = New List(Of _3Dev.FW.Entidades.BEKeyValue(Of String, Object))()
                End If
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

            Return eUsuario
        End Function
        Public Function DamePaginasAccesables(ByVal userName As String) As List(Of _3Dev.FW.Entidades.Seguridad.BERolPagina)
            Try
                Dim list As New List(Of BERolPagina)
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarPaginasPorEmailUsuario]", userName)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        list.Add(New BERolPagina(objIDataReader))
                    End While
                End Using
                Return list

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function

        Public Overrides Function ConfirmUserValidation(ByVal email As String, ByVal guid As String) As Boolean
            Try
                Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                objDatabase.ExecuteNonQuery("PagoEfectivo.prc_ConfirmarValidacionDeUsuario", email, guid)
                Return True

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try

        End Function
        ''MODIFICADO PARA PAGINACION FASE III
        Public Overrides Function GetUsuarioLikeName(ByVal Name As String, Optional ByVal entidad As _3Dev.FW.Entidades.BusinessEntityBase = Nothing) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BEUsuario)
            Dim eUsuario As SPE.Entidades.BEUserInfo = Nothing
            Dim listUsuario As List(Of BEUsuario) = New List(Of BEUsuario)()

            Dim listaParametros As New List(Of Object)
            listaParametros.Add(Name)
            If (entidad IsNot Nothing) Then
                listaParametros.Add(entidad.PageNumber)
                listaParametros.Add(entidad.PageSize)
                listaParametros.Add(entidad.PropOrder)
                listaParametros.Add(entidad.TipoOrder)
            End If

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand(If(entidad Is Nothing, "[PagoEfectivo].[prc_ConsultarUsuario]", "[PagoEfectivo].[prc_ConsultarUsuarioPG]"), listaParametros.ToArray())
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        listUsuario.Add(New _3Dev.FW.Entidades.Seguridad.BEUsuario(objIDataReader, 1))
                    End While
                End Using
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return listUsuario
        End Function
        Public Overrides Function GetUsuarioLikeNameForRol(ByVal Name As String, ByVal RolId As Integer) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.Seguridad.BEUsuario)
            Try
                Dim list As New List(Of BEUsuario)()
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarUsuarioPorLikeEmailyRol]", Name, RolId)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        list.Add(New BEUsuario(objIDataReader))
                    End While
                End Using
                Return list

            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
        End Function

        Public Overrides Function RegistrarUsuario(ByVal eUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
            Dim objDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim resultado As Integer
            Dim obeUsuario As SPE.Entidades.BEUsuarioBase = CType(eUsuario, SPE.Entidades.BEUsuarioBase)

            Try
                resultado = CInt(objDataBase.ExecuteScalar("PagoEfectivo.prc_RegistrarUsuario", _
                DBNull.Value, _
                obeUsuario.IdCiudad, _
                obeUsuario.Nombres, _
                obeUsuario.Apellidos, _
                obeUsuario.IdTipoDocumento, _
                obeUsuario.NumeroDocumento, _
                obeUsuario.Direccion, _
                obeUsuario.Telefono, _
                obeUsuario.Email, _
                obeUsuario.Password, _
                obeUsuario.IdOrigenRegistro, _
                obeUsuario.IdGenero, _
                obeUsuario.FechaNacimiento, _
                obeUsuario.CodigoRegistro, _
                obeUsuario.IdEstado, _
                obeUsuario.IdUsuarioCreacion))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            eUsuario.IdUsuario = resultado
            Return resultado
        End Function

        Public Overrides Function ModificarUsuario(ByVal eUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            'Dim pass As Object = Nothing
            Dim obeUsuario As SPE.Entidades.BEUsuarioBase = CType(eUsuario, SPE.Entidades.BEUsuarioBase)
            'If Not (eUsuario.Password = "") Then
            '    pass = SeguridadComun.EncryptarCodigo(eUsuario.Password)
            'End If

            Try
                Resultado = CInt(objDatabase.ExecuteNonQuery("[PagoEfectivo].[prc_ActualizarUsuarioPorIdUsuario]", obeUsuario.IdUsuario, _
                obeUsuario.IdCiudad, _
                obeUsuario.Nombres, _
                obeUsuario.Apellidos, _
                obeUsuario.IdTipoDocumento, _
                obeUsuario.NumeroDocumento, _
                obeUsuario.Direccion, _
                obeUsuario.Telefono, _
                obeUsuario.IdGenero, _
                obeUsuario.FechaNacimiento, _
                obeUsuario.IdEstado, _
                obeUsuario.IdUsuarioActualizacion, Nothing, Nothing))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return Resultado
        End Function

        Public Overrides Function GetUsuarioById(ByVal idUsuario As Integer) As _3Dev.FW.Entidades.Seguridad.BEUsuario
            Dim eUsuario As SPE.Entidades.BEUsuarioBase = Nothing
            'Dim eUsuario As New _3Dev.FW.Entidades.Seguridad.BEUsuario
            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarUsuarioPorId]")
                oDataBase.AddInParameter(db, "idusuario", DbType.Int32, idUsuario)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        eUsuario = New SPE.Entidades.BEUsuarioBase(objIDataReader)
                        'eUsuario = New _3Dev.FW.Entidades.Seguridad.BEUsuario(objIDataReader)
                    End While
                End Using
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return eUsuario

        End Function

        'VALIDA SI EXISTE EL EMAIL
        Public Function ValidarEmailUsuario(ByVal be As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Boolean
            Dim objDatabase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
            Dim Resultado As Integer = 0
            Dim obeUsuario As SPE.Entidades.BEUsuarioBase = CType(be, SPE.Entidades.BEUsuarioBase)
            Try
                Resultado = CInt(objDatabase.ExecuteScalar("[PagoEfectivo].[prc_ValidarEmail]", obeUsuario.Email))
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return Resultado > 0
        End Function

        Public Function ConsultarUsuarioPorEmail(ByVal email As String) As BEUsuario
            Dim eUsuario As BEUsuario = Nothing

            Try
                Dim oDataBase As Database = DatabaseFactory.CreateDatabase(SPE.EmsambladoComun.ParametrosSistema.StrConexion)
                Dim db As DbCommand = oDataBase.GetStoredProcCommand("[PagoEfectivo].[prc_ConsultarUsuarioPorEmail]")
                oDataBase.AddInParameter(db, "pstrEmail", DbType.String, email)
                Using objIDataReader As IDataReader = oDataBase.ExecuteReader(db)
                    While objIDataReader.Read()
                        eUsuario = New BEUsuario(objIDataReader)
                    End While
                End Using
            Catch ex As Exception
                'Logger.Write(ex)
                Throw ex
            End Try
            Return eUsuario
        End Function

    End Class




End Namespace
