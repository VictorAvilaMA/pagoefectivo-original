﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Configuration;
using SPE.Entidades;
using MongoDB.Driver.Builders;

namespace SPE.Adapters
{
    public static class MongoDBAdapter
    {
        public static MongoDatabase EstablecerConexion()
        {
            //Enlace al Servidor
            MongoServer servidor = MongoServer.Create(ConfigurationManager.AppSettings["connMongoDB"]);
            //Especificar la Base de datos
            MongoDatabase bd = servidor.GetDatabase(ConfigurationManager.AppSettings["LogsPagoEfectivo"]);
            return bd;
        }

        public static Int32 CollectionInsertarGenerico<T>(T oT, String oCollection, bool save)
        {
            Int32 IdReturn = 0;
            try
            {
                MongoDatabase mongoBD = MongoDBAdapter.EstablecerConexion();
                MongoCollection<T> coleccion = mongoBD.GetCollection<T>(oCollection);
                //coleccion.Insert(oT);
                if (save)
                    coleccion.Save(oT);
                else
                    coleccion.Insert(oT);
                IdReturn = 1;
                return IdReturn;//((T)oT).id;
            }
            catch (Exception)
            {
                return IdReturn;
            }

        }





        public static String CollectionInsertarBCPRequest(BEBCPRequest oBEBCPRequest, String oCollection)
        {
            var id = ObjectId.GenerateNewId();
            try
            {
                BEBCPRequest oBEBCPRequest2 = oBEBCPRequest;
                oBEBCPRequest2.id = id;
                MongoDatabase mongoBD = MongoDBAdapter.EstablecerConexion();
                MongoCollection coleccion = mongoBD.GetCollection(oCollection);
                coleccion.Save(oBEBCPRequest2);
                return id.ToString();//((T)oT).id;
            }
            catch (Exception)
            {
                return "0";
            }

        }


        public static String CollectionSeleccionarCodigo(String oCollection, String campo, string valor, string retorno)
        {

            string strRetorno = "";
            try
            {

                MongoDatabase mongoBD = MongoDBAdapter.EstablecerConexion();
                MongoCollection<BsonDocument> coleccion = mongoBD.GetCollection<BsonDocument>(oCollection);
                BsonDocument book = new BsonDocument();
                var query = new QueryDocument("_id", new BsonObjectId(valor));

                foreach (BsonDocument item in coleccion.Find(query))
                {
                    BsonElement ret = item.GetElement(retorno);
                    strRetorno = ret.Value.ToString();
                }

            }
            catch (Exception)
            {
            }

            //MongoCollection<BsonDocument> books;
            //var query = Query.EQ("author", "Kurt Vonnegut");
            //foreach (BsonDocument book in books.Find(query))
            //{
            //    strRetorno = book[posicion].ToString();
            //}


            return strRetorno;

        }






    }
}
