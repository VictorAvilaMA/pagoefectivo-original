﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace SPE.Api.Crypto
{
    public class Encripter
    {
        public static string EncryptText(string plainText, byte[] publicKey)
        {
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePublicaContraparte = publicKey;
            String[] textEncript = encripter.Cifrar(plainText);
            return textEncript[0] + "|" + textEncript[1];
        }
        public static string DecryptText(string encryptText, byte[] privateKey)
        {
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePrivada = privateKey;
            String[] textEncript = encryptText.Split('|');
            return encripter.DesCifrar(textEncript[0], textEncript[1]);
        }
        public static string Signer(string plainText, byte[] privateKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePrivada = privateKey;
            return encripter.Firmar(plainText);
        }
        public static bool SignerVal(string plainText, string signerText, byte[] publicKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePublicaContraparte = publicKey;
            return encripter.ValidarFirma(plainText, signerText);
        }

        public static string EncryptText(string plainText, string pathPublicKey)
        {
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePublicaContraparte = ByteUtil.FileToByteArray(pathPublicKey);
            String[] textEncript = encripter.Cifrar(plainText);
            return textEncript[0] + "|" + textEncript[1];
        }
        public static string DecryptText(string encryptText, string pathPrivateKey)
        {
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePrivada = ByteUtil.FileToByteArray(pathPrivateKey);
            String[] textEncript = encryptText.Split('|');
            return encripter.DesCifrar(textEncript[0], textEncript[1]);
        }
        public static string Signer(string plainText, string pathPrivateKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePrivada = ByteUtil.FileToByteArray(pathPrivateKey);
            return encripter.Firmar(plainText);
        }
        public static bool SignerVal(string plainText, string signerText, string pathPublicKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Api.Encripter encripter = new SPE.Api.Encripter();
            encripter.clavePublicaContraparte = ByteUtil.FileToByteArray(pathPublicKey);
            return encripter.ValidarFirma(plainText, signerText);
        }
        public static void GenerateAndSaveKeys(string pathPublicKey, string pathPrivateKey)
        {
            String randomContainerName = Guid.NewGuid().ToString();
            Byte[] keyPrivate = new SPE.Api.Encripter().GenerarKeyPriv(randomContainerName);
            Byte[] keyPublic = new SPE.Api.Encripter().GenerarKeyPub(randomContainerName);
            ByteUtil.ByteArrayToFile(pathPrivateKey, keyPrivate);
            ByteUtil.ByteArrayToFile(pathPublicKey, keyPublic);
        }
    }
}
