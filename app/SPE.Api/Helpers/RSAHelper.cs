using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using System.Configuration;

namespace SPE.Api
{
    class RSAHelper
    {
        #region attributes

        const int longitudClave = 2048;
        const string algoritmoSHA = "SHA512";
        public Byte[] clavePublica{ get; set; }
        public Byte[] clavePrivada{ get; set; }
        public Byte[] clavePublicaContraparte { get; set; }

        #endregion

        #region metodos

        private byte[] GetHashRSA(string str, string hashName)
        {
            byte[] tmpSource = ASCIIEncoding.ASCII.GetBytes(str);
            switch (hashName.ToLower(CultureInfo.InvariantCulture))
            {
                case "md5":
                    // md5withRSAEncryption
                    return new MD5CryptoServiceProvider().ComputeHash(tmpSource);
                case "sha1":
                    // sha1withRSAEncryption
                    return new SHA1Managed().ComputeHash(tmpSource);
                case "sha256":
                    // sha256WithRSAEncryption
                    return new SHA256Managed().ComputeHash(tmpSource);
                case "sha384":
                    // sha384WithRSAEncryption
                    return new SHA384Managed().ComputeHash(tmpSource);
                case "sha512":
                    // sha512WithRSAEncryption
                    return new SHA512Managed().ComputeHash(tmpSource);
                default:
                    throw new NotSupportedException("Unknown hash algorithm " + hashName);
            }
        }
        private void Generar_Kpriv(string containerName)
        {
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = containerName;
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048, cp);
        }
        private RSAParameters ObtenerRSAParameters(int tipoKey)
        {
            BEDer oBEDer = new BEDer();
            RSAParameters rsaParameters = new RSAParameters();

            switch (tipoKey)
            { 
                case TipoKey.privada:
                    oBEDer = (BEDer)ByteUtil.ByteArrayToObject(clavePrivada);
                     rsaParameters.D = oBEDer.D;
                    rsaParameters.DP = oBEDer.DP;
                    rsaParameters.DQ = oBEDer.DQ;
                    rsaParameters.InverseQ = oBEDer.InverseQ;
                    rsaParameters.P = oBEDer.P;
                    rsaParameters.Q = oBEDer.Q;
                    break;
                case TipoKey.publica:
                    oBEDer = (BEDer)ByteUtil.ByteArrayToObject(clavePublica);
                    break;
                case TipoKey.publicaContraparte:
                    oBEDer = (BEDer)ByteUtil.ByteArrayToObject(clavePublicaContraparte);
                    break;
            }
            rsaParameters.Modulus = oBEDer.Modulus;
            rsaParameters.Exponent = new byte[] { 1, 0, 1 };
           

            return rsaParameters;
        }

        public Byte[] GenerarKPriv(string containerName)
        {
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = containerName;
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048, cp);
            RSAParameters RSAParam = rsa.ExportParameters(true);//FALSE PARA LA PUBLICA, TRUE PRIVADA 
            BEDer secuencia = new BEDer();
            secuencia.Modulus = RSAParam.Modulus;
            secuencia.Exponent = 65537;
            secuencia.D = RSAParam.D;
            secuencia.DP = RSAParam.DP;
            secuencia.DQ = RSAParam.DQ;
            secuencia.InverseQ = RSAParam.InverseQ;
            secuencia.P = RSAParam.P;
            secuencia.Q = RSAParam.Q;

            return ByteUtil.ObjectToArrayByte(secuencia);
        }
        public Byte[] GenerarKPub(string containerName)
        {
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = containerName;
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048, cp);
            RSAParameters RSAParam = rsa.ExportParameters(false);//FALSE PARA LA PUBLICA , TRUE PRIVADA 
            BEDer secuencia = new BEDer();
            secuencia.Modulus = RSAParam.Modulus;
            secuencia.Exponent = 65537;

            return ByteUtil.ObjectToArrayByte(secuencia);
        }

        public string CifrarCadena(byte[] parByte)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(longitudClave);
            rsa.ImportParameters(ObtenerRSAParameters(TipoKey.publicaContraparte));
            byte[] bCypher = rsa.Encrypt(parByte, false);
            return ByteUtil.ByteArrayToStringHex(bCypher);
        }
        public string DesCifrarCadena(string toDecrypt)
        {
            //Si bPlain tiene m�s bytes del n�mero permitido por el tama�o del xxxxxxxx
            //generado, dar� un error en tiempo de ejecucion
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(longitudClave);
            rsa.ImportParameters(ObtenerRSAParameters(TipoKey.privada));
            byte[] bCypher = rsa.Decrypt(ByteUtil.StringHexToByteArray(toDecrypt), false);
            return ByteUtil.ByteArrayToStringHex(bCypher);
        }
        public string SignMessage(string str)
        {
            byte[] hashValue = ASCIIEncoding.ASCII.GetBytes(str);
            SHA512Managed oSHA512Managed = new SHA512Managed();
            byte[] hashedData;
            hashedData = oSHA512Managed.ComputeHash(hashValue);
            byte[] signedHashValue;
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(longitudClave);
            rsa.ImportParameters(ObtenerRSAParameters(TipoKey.privada));
            signedHashValue = rsa.SignHash(hashedData, CryptoConfig.MapNameToOID(algoritmoSHA));
            return ByteUtil.ByteArrayToStringHex(signedHashValue);
        }
        public bool ValidSignedMessage(string str, string strSignedHash)
        {
            byte[] hashValue = ASCIIEncoding.ASCII.GetBytes(str);
            byte[] SignedHashValue = ByteUtil.StringHexToByteArray(strSignedHash);
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(longitudClave);
            rsa.ImportParameters(ObtenerRSAParameters(TipoKey.publicaContraparte));

            SHA512Managed oSHA512Managed = new SHA512Managed();
            byte[] hashedData;
            hashedData = oSHA512Managed.ComputeHash(hashValue);
            bool estado = rsa.VerifyHash(hashedData, CryptoConfig.MapNameToOID("SHA512"), SignedHashValue);
            return estado;
        }

        #endregion
        
    }

    public class TipoKey
    {
        public const int publica = 1;
        public const int privada = 2;
        public const int publicaContraparte = 3;
    }
}
