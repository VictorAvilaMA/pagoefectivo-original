using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using System.Configuration;

namespace SPE.Api
{
    public class TripleDesHelper
    {
        TripleDESCryptoServiceProvider tdes;
        private void GetTripleDES()
        {
            if (tdes == null)
            {
                tdes = new TripleDESCryptoServiceProvider();
                tdes.BlockSize = 64;
                //tdes.IV => Autogenerado
                tdes.KeySize = 192; //Longitud Triple (clave de 64 bits - Des * 3)
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.Zeros; //Nota: el byte 0 no corresponde a caracter alguno y aparece como un espacio en blanco
            }
        }
        private byte[] GenerateKeyTripleDes()
        {
            GetTripleDES();

            //Generate Key
            tdes.GenerateKey();

            return tdes.Key;
        }

        public String[] CifrarMensaje3DES(string toEncrypt)
        {
            GetTripleDES();
            String[] arrayResult = new String[2];
            byte[] keyArray = GenerateKeyTripleDes();
            tdes.Key = keyArray;
            int len = (8 - (toEncrypt.Length % 8)) + toEncrypt.Length;
            toEncrypt = String.Format("{0,-" + len + "}", toEncrypt);
            byte[] toEncryptArray = ASCIIEncoding.ASCII.GetBytes(toEncrypt);
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            tdes.Clear();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            arrayResult[0] = BitConverter.ToString(resultArray).Replace("-", "");
            arrayResult[1] = ByteUtil.ByteArrayToStringHex(keyArray);
            return arrayResult;
        }
        public string DesCifrarMensaje3DES(string toDecrypt, string key)
        {
            GetTripleDES();
            byte[] keyArray = ByteUtil.StringHexToByteArray(key);
            tdes.Key = keyArray;
            byte[] toEncryptArray = ByteUtil.StringHexToByteArray(toDecrypt);
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            tdes.Clear();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return ASCIIEncoding.ASCII.GetString(resultArray);
        }
    }
}
