using System;
using System.Collections.Generic;
using System.Text;
using org.bn.attributes;
using org.bn.attributes.constraints;
using org.bn.coders;
using org.bn.types;

namespace SPE.Api
{

    //RSAParam.D byte[]
    //RSAParam.DP byte[]
    //RSAParam.DQ byte[]
    //RSAParam.InverseQ byte[]
    //RSAParam.P byte[]
    //RSAParam.Q byte[]

    [Serializable]
    [ASN1Sequence(Name = "BEDer", IsSet = false)]
    class BEDer
    {
        private byte[] modulus_;

        [ASN1Element(Name = "modulus", IsOptional = false, HasTag = true, HasDefaultValue = false, Tag = 2, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] Modulus
        {
            get { return modulus_; }
            set { modulus_ = value; }
        }

        private int exponent_;
        [ASN1Integer(Name = "")]
        [ASN1Element(Name = "exponent", IsOptional = false, HasTag = false, HasDefaultValue = false)]
        public int Exponent
        {
            get { return exponent_; }
            set { exponent_ = value; }
        }

        private byte[] D_;

        [ASN1Element(Name = "D", IsOptional = true, HasTag = true, HasDefaultValue = false, Tag = 3, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] D
        {
            get { return D_; }
            set { D_ = value; }
        }

        private byte[] DP_;

        [ASN1Element(Name = "DP", IsOptional = true, HasTag = true, HasDefaultValue = false, Tag = 4, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] DP
        {
            get { return DP_; }
            set { DP_ = value; }
        }

        private byte[] DQ_;

        [ASN1Element(Name = "DQ", IsOptional = true, HasTag = true, HasDefaultValue = false, Tag = 5, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] DQ
        {
            get { return DQ_; }
            set { DQ_ = value; }
        }

        private byte[] InverseQ_;

        [ASN1Element(Name = "InverseQ", IsOptional = true, HasTag = true, HasDefaultValue = false, Tag = 6, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] InverseQ
        {
            get { return InverseQ_; }
            set { InverseQ_ = value; }
        }

        private byte[] P_;

        [ASN1Element(Name = "P", IsOptional = true, HasTag = true, HasDefaultValue = false, Tag = 7, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] P
        {
            get { return P_; }
            set { P_ = value; }
        }

        private byte[] Q_;

        [ASN1Element(Name = "Q", IsOptional = true, HasTag = true, HasDefaultValue = false, Tag = 8, IsImplicitTag = true, TagClass = TagClasses.Universal)]
        public byte[] Q
        {
            get { return Q_; }
            set { Q_ = value; }
        }
    }
}
