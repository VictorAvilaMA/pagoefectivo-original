﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SPE.Api.Proxys;
using System.Net;

namespace SPE.Api
{
    public class PagoEfectivo
    {
        public static string PublicPathContraparte;
        public static string PrivatePath;

        #region ConsultarSolicitudPago
        public static BEWSConsultarSolicitudResponse ConsultarSolicitudPago(BEWSConsultarSolicitudRequest request)
        {
            BEWSConsultarSolicitudResponse response = new BEWSConsultarSolicitudResponse();
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Xml = request.Xml.Trim();
                request.CClave = proxy.Signer(request.Xml, ByteUtil.FileToByteArray(PrivatePath));
                request.Xml = proxy.EncryptText(request.Xml, ByteUtil.FileToByteArray(PublicPathContraparte));
                using (var proxyCIP = new WSCIP())
                {
                    response = proxyCIP.ConsultarSolicitudPago(request);
                    if (response != null)
                        if (!String.IsNullOrEmpty(response.Xml))
                            response.Xml = proxy.DecryptText(response.Xml, ByteUtil.FileToByteArray(PrivatePath));
                }
            }
            return response;

        }
        public static BEWSConsultarSolicitudResponse ConsultarSolicitudPago(string CClave, string cServ, string Xml)
        {
            BEWSConsultarSolicitudRequest request = new BEWSConsultarSolicitudRequest();
            request.CClave = CClave;
            request.cServ = cServ;
            request.Xml = Xml;
            return ConsultarSolicitudPago(request);
        }
        #endregion

        #region SolicitarPago
        public static BEWSSolicitarResponse SolicitarPago(BEWSSolicitarRequest request)
        {
            request.Xml = request.Xml.Trim();
            BEWSSolicitarResponse response = new BEWSSolicitarResponse();

            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.CClave = proxy.Signer(request.Xml, ByteUtil.FileToByteArray(PrivatePath));
                request.Xml = proxy.EncryptText(request.Xml, ByteUtil.FileToByteArray(PublicPathContraparte));
                using (var proxyCIP = new WSCIP())
                {
                    response = proxyCIP.SolicitarPago(request);
                    if (response != null)
                        if (!String.IsNullOrEmpty(response.Xml))
                            response.Xml = proxy.DecryptText(response.Xml, ByteUtil.FileToByteArray(PrivatePath));
                }
            }
            return response;
        }
        public static BEWSSolicitarResponse SolicitarPago(string cServ, string CClave, string xml)
        {
            BEWSSolicitarRequest request = new BEWSSolicitarRequest();
            request.cServ = cServ;
            request.CClave = CClave;
            request.Xml = xml;
            return SolicitarPago(request);
        }
        #endregion
        #region ConsultarCIP
        public static BEWSConsultarCIPResponse ConsultarCIP(BEWSConsultarCIPRequest request)
        {
            BEWSConsultarCIPResponse response = new BEWSConsultarCIPResponse();

            using (var proxyCIP = new WSCIP())
            {
                proxyCIP.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxyCIP.Proxy = WebProxy.GetDefaultProxy();
                proxyCIP.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxyCIP.Url = System.Configuration.ConfigurationManager.AppSettings["SPE_WebServiceTest_WSGeneral_WSCIP"];
                response = proxyCIP.ConsultarCIP(request);
            }
            return response;
        }
        #endregion

        #region EliminarCIP
        public static BEWSElimCIPResponse EliminarCIP(BEWSElimCIPRequest request)
        {
            BEWSElimCIPResponse response = new BEWSElimCIPResponse();
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Url = System.Configuration.ConfigurationManager.AppSettings["SPE_WebServiceTest_WSCrypto"];
                //request.InfoRequest = request.InfoRequest.Trim();
                //request.CClave = proxy.Signer(request.InfoRequest, ByteUtil.FileToByteArray(PrivatePath));
                //request.InfoRequest = proxy.EncryptText(request.InfoRequest, ByteUtil.FileToByteArray(PublicPathContraparte));
                using (var proxyCIP = new WSCIP())
                {
                    proxyCIP.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    proxyCIP.Proxy = WebProxy.GetDefaultProxy();
                    proxyCIP.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    proxyCIP.Url = System.Configuration.ConfigurationManager.AppSettings["SPE_WebServiceTest_WSGeneral_WSCIP"];
                    response = proxyCIP.EliminarCIP(request);
                    if (response != null)
                        if (!String.IsNullOrEmpty(response.InfoResponse))
                            response.InfoResponse = proxy.DecryptText(response.InfoResponse, ByteUtil.FileToByteArray(PrivatePath));
                }
            }
            return response;
        }
        #endregion

        #region Metodos Nueva Modalidad1
        public static BEWSGenCIPResponseMod1 GenerarCIPMod1(BEWSGenCIPRequestMod1 request)
        {
            BEWSGenCIPResponseMod1 response = new BEWSGenCIPResponseMod1();
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.Xml = request.Xml.Trim();
                request.Firma = proxy.Signer(request.Xml, ByteUtil.FileToByteArray(PrivatePath));
                request.Xml = proxy.EncryptText(request.Xml, ByteUtil.FileToByteArray(PublicPathContraparte));
                using (var proxyCIP = new Service())
                {
                    response = proxyCIP.GenerarCIPMod1(request);
                    if (response != null)
                        if (!String.IsNullOrEmpty(response.Xml))
                            response.Xml = proxy.DecryptText(response.Xml, ByteUtil.FileToByteArray(PrivatePath));
                }
            }
            return response;
        }
        public static BEWSConsultarCIPResponseMod1 ConsultarCIPsMod1(BEWSConsultarCIPRequestMod1 request)
        {
            BEWSConsultarCIPResponseMod1 response = new BEWSConsultarCIPResponseMod1();
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.CIPS = request.CIPS.Trim();
                request.Firma = proxy.Signer(request.CIPS, ByteUtil.FileToByteArray(PrivatePath));
                request.CIPS = proxy.EncryptText(request.CIPS, ByteUtil.FileToByteArray(PublicPathContraparte));
                using (var proxyCIP = new Service())
                {
                    response = proxyCIP.ConsultarCIPMod1(request);
                    if (response != null)
                        if (!String.IsNullOrEmpty(response.XML))
                            response.XML = proxy.DecryptText(response.XML, ByteUtil.FileToByteArray(PrivatePath));
                }
            }
            return response;
        }
        public static BEWSElimCIPResponseMod1 EliminarCIPMod1(BEWSElimCIPRequestMod1 request)
        {
            BEWSElimCIPResponseMod1 response = new BEWSElimCIPResponseMod1();
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.CIP = request.CIP.Trim();
                request.Firma = proxy.Signer(request.CIP, ByteUtil.FileToByteArray(PrivatePath));
                request.CIP = proxy.EncryptText(request.CIP, ByteUtil.FileToByteArray(PublicPathContraparte));
                //request.codServ=""
                using (var proxyCIP = new Service())
                {
                    response = proxyCIP.EliminarCIPMod1(request);
                }
            }
            return response;
        }
        public static BEWSActualizaCIPResponseMod1 ActualizarCIPMod1(BEWSActualizaCIPRequestMod1 request)
        {
            BEWSActualizaCIPResponseMod1 response = new BEWSActualizaCIPResponseMod1();
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                request.CIP = request.CIP.Trim();
                request.Firma = proxy.Signer(request.CIP, ByteUtil.FileToByteArray(PrivatePath));
                request.CIP = proxy.EncryptText(request.CIP, ByteUtil.FileToByteArray(PublicPathContraparte));
                using (var proxyCIP = new Service())
                {
                    response = proxyCIP.ActualizarCIPMod1(request);
                    //if (response != null)
                    //    if (!String.IsNullOrEmpty(response.XML))
                    //        response.XML = proxy.DecryptText(response.XML, ByteUtil.FileToByteArray(PrivatePath));
                }
            }
            return response;
        }
        #endregion

        public static String EnciptarTexto(String TextoPlano)
        {
            String response;
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                //try
                //{
                response = proxy.EncryptText(TextoPlano, ByteUtil.FileToByteArray(PublicPathContraparte));
                //}
                //catch (Exception e)
                //{

                //    response = e.ToString();
                //}

            }
            return response;
        }
        public static String DesenciptarTexto(String TextoEncriptado)
        {
            String response;
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                response = proxy.DecryptText(TextoEncriptado, ByteUtil.FileToByteArray(PrivatePath));
            }
            return response;
        }
        public static String Firmar(String TextoPlano)
        {
            String response;
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                response = proxy.Signer(TextoPlano, ByteUtil.FileToByteArray(PrivatePath));
            }
            return response;
        }
        public static bool ValidarFirma(String TextoPlano, String TextoFirmado)
        {
            bool response;
            using (var proxy = new WSCrypto())
            {
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                proxy.Proxy = WebProxy.GetDefaultProxy();
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                response = proxy.SignerVal(TextoPlano, TextoFirmado, ByteUtil.FileToByteArray(PublicPathContraparte));
            }
            return response;
        }
    }
}