﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by wsdl, Version=4.0.30319.1.
// 
namespace ProxyWSIBK {
    using System.Diagnostics;
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System.Web.Services;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="ServiceSoap", Namespace="http://tempuri.org/")]
    public partial class Service : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback ejecutarTransaccionInterbankOperationCompleted;
        
        /// <remarks/>
        public Service() {
            this.Url = System.Configuration.ConfigurationManager.AppSettings["SPE_Proxy_IBK"];
        }
        
        /// <remarks/>
        public event ejecutarTransaccionInterbankCompletedEventHandler ejecutarTransaccionInterbankCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string ejecutarTransaccionInterbank(string Input) {
            object[] results = this.Invoke("ejecutarTransaccionInterbank", new object[] {
                        Input});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginejecutarTransaccionInterbank(string Input, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("ejecutarTransaccionInterbank", new object[] {
                        Input}, callback, asyncState);
        }
        
        /// <remarks/>
        public string EndejecutarTransaccionInterbank(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void ejecutarTransaccionInterbankAsync(string Input) {
            this.ejecutarTransaccionInterbankAsync(Input, null);
        }
        
        /// <remarks/>
        public void ejecutarTransaccionInterbankAsync(string Input, object userState) {
            if ((this.ejecutarTransaccionInterbankOperationCompleted == null)) {
                this.ejecutarTransaccionInterbankOperationCompleted = new System.Threading.SendOrPostCallback(this.OnejecutarTransaccionInterbankOperationCompleted);
            }
            this.InvokeAsync("ejecutarTransaccionInterbank", new object[] {
                        Input}, this.ejecutarTransaccionInterbankOperationCompleted, userState);
        }
        
        private void OnejecutarTransaccionInterbankOperationCompleted(object arg) {
            if ((this.ejecutarTransaccionInterbankCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ejecutarTransaccionInterbankCompleted(this, new ejecutarTransaccionInterbankCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
    public delegate void ejecutarTransaccionInterbankCompletedEventHandler(object sender, ejecutarTransaccionInterbankCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ejecutarTransaccionInterbankCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal ejecutarTransaccionInterbankCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}
