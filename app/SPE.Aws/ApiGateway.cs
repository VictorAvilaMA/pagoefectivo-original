﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using SPE.Aws.Contracts;
using SPE.Aws.Helpers;

namespace SPE.Aws
{
    public class ApiGateway
    {
        private readonly string _awsAccessKey;
        private readonly string _awsSecretKey;
        private readonly string _region;

        public ApiGateway(string awsAccessKey, string awsSecretKey, string region)
        {
            _awsAccessKey = awsAccessKey;
            _awsSecretKey = awsSecretKey;
            _region = region;
        }

        public ResponseMessage PostMessage<T>(T message, string endPointUrl)
        {
            ResponseMessage response;
            var stringContent = new StringContent(JsonConvert.SerializeObject(message), Encoding.UTF8, "application/json");

            var request = AwsSigningHelper.GenerateAwsSignedPostRequest(
                endpointUrl: endPointUrl,
                content: stringContent,
                awsRegion: _region,
                accessKeyId: _awsAccessKey,
                secretAccessKey: _awsSecretKey
            );

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                HttpResponseMessage result;
                using (var httpRequest = client.SendAsync(request, HttpCompletionOption.ResponseContentRead))
                {
                    result = httpRequest.Result;
                }
                var resultAsStringAsync = result.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<ResponseMessage>(resultAsStringAsync.Result);
            }

            return response;
        }
    }
}