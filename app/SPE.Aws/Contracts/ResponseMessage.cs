﻿using Newtonsoft.Json;

namespace SPE.Aws.Contracts
{
    public class ResponseMessage
    {
        [JsonProperty(PropertyName = "code", Required = Required.Always)]
        public int Code { get; set; }

        [JsonProperty(PropertyName = "message", Required = Required.Always)]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "data", Required = Required.Default)]
        public dynamic Data { get; set; }
    }
}