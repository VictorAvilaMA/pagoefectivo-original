﻿using System.Collections;
using System.Collections.Generic;

namespace SPE.Aws.Core
{
    /// <summary>Representa una colección de elementos de solo lectura a los que se puede tener acceso por un índice. </summary>
    /// <typeparam name="T">Tipo de elementos en la lista de solo lectura. Este parámetro de tipo es covariante. Es decir, puede usar el tipo especificado o cualquier tipo que sea más derivado. Para obtener más información sobre la covarianza y la contravarianza, consulte Covarianza y contravarianza en genéricos.</typeparam>
    internal interface IReadOnlyList<out T> : IEnumerable<T>, IEnumerable
    {
        /// <summary>Obtiene el elemento en el índice especificado en la lista de solo lectura.</summary>
        /// <returns>Elemento en el índice especificado en la lista de solo lectura.</returns>
        /// <param name="index">Índice de base cero del elemento que se va a obtener. </param>
        T this[int index] { get; }

        int Count { get; }
    }
}