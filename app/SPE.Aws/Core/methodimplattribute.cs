﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace SPE.Aws.Core
{
    [Flags]
    [ComVisible(true)]
    [Serializable]
    internal enum MethodImplOptions
    {
        Unmanaged = 4,
        NoInlining = 8,
        ForwardRef = 16,
        Synchronized = 32,
        NoOptimization = 64,
        PreserveSig = 128,
        [ComVisible(false)]
        AggressiveInlining = 256,
        InternalCall = 4096,
    }


    [Serializable]
    [System.Runtime.InteropServices.ComVisible(true)]
    internal enum MethodCodeType
    {
        IL = System.Reflection.MethodImplAttributes.IL,
        Native = System.Reflection.MethodImplAttributes.Native,
        /// <internalonly/>
        OPTIL = System.Reflection.MethodImplAttributes.OPTIL,
        Runtime = System.Reflection.MethodImplAttributes.Runtime
    }

    // Custom attribute to specify additional method properties.
    [Serializable]
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor, Inherited = false)]
    [System.Runtime.InteropServices.ComVisible(true)]
    internal sealed class MethodImplAttribute : Attribute
    {
        internal MethodImplOptions Val;
        public MethodCodeType MethodCodeType;

        internal MethodImplAttribute(MethodImplAttributes methodImplAttributes)
        {
            MethodImplOptions all =
                MethodImplOptions.Unmanaged | MethodImplOptions.ForwardRef | MethodImplOptions.PreserveSig |
                MethodImplOptions.InternalCall | MethodImplOptions.Synchronized |
                MethodImplOptions.NoInlining | MethodImplOptions.AggressiveInlining |
                MethodImplOptions.NoOptimization;
            Val = ((MethodImplOptions)methodImplAttributes) & all;
        }

        public MethodImplAttribute(MethodImplOptions methodImplOptions)
        {
            Val = methodImplOptions;
        }

        public MethodImplAttribute(short value)
        {
            Val = (MethodImplOptions)value;
        }

        public MethodImplAttribute()
        {
        }

        public MethodImplOptions Value { get { return Val; } }
    }
}
