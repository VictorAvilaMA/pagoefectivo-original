--- [ParametroGrupo]
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('ESMB','Tipo de Mensaje de retorno al banco')
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('ESCO','Estado Conciliacion')
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('ESBA','Estado Banco')
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('ESAB','Estados Agencia Bancaria')
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('TARG','Tipo de Archivos FTP')
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('ESAG','Estado de Archivos FTP')
INSERT INTO [PagoEfectivo].[ParametroGrupo]([Codigo],[Descripcion])VALUES('TNOT','Tipos de notificaciones a empresas contratantes')




--- [Parametro]
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('161','ESMB','Consulta')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('162','ESMB','Consulta')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('163','ESMB','Consulta')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('171','ESCO','Conciliado')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('172','ESCO','No Conciliado')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('173','ESCO','Conciliado Modificado')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('181','ESBA','Activo')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('182','ESBA','Inactivo')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('191','ESAB','Activo')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('192','ESAB','Inactivo')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('193','ESAB','Modificar')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('221','TARG','Pago')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('222','TARG','Expiracion')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('231','ESAG','Generado')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('232','ESAG','No Generado')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('241','TNOT','Archivo FTP')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES('242','TNOT','Request')
INSERT INTO [PagoEfectivo].[Parametro]([Id],[GrupoCodigo],[Descripcion])VALUES	('133','EGOP','POS')





 --select 'insert into PagoEfectivo.Banco(Codigo,Descripcion,IdEstado,IdUsuarioCreacion,FechaCreacion) values(''' +   Codigo +  ''',''' + Descripcion + ''','''+ convert(varchar,IdEstado) + ''',''' + convert(varchar,IdUsuarioCreacion) + ''',''' + convert(varchar,FechaCreacion,101) + ''');' from PagoEfectivo.Banco;


--BANCO
insert into PagoEfectivo.Banco(Codigo,Descripcion,IdEstado,IdUsuarioCreacion,FechaCreacion) values('02','Banco de Cr�dito BCP','181','1',GETDATE());

--- TipoOrigenCancelacion

insert into PagoEfectivo.TipoOrigenCancelacion(Descripcion) values('Agencia Bancaria');
insert into PagoEfectivo.TipoOrigenCancelacion(Descripcion) values('Agencia Recaudadora');
insert into PagoEfectivo.TipoOrigenCancelacion(Descripcion) values('Establecimiento');


--- Actualizar datos de moneda

UPDATE PagoEfectivo.Moneda
SET EquivalenciaBancaria = 0
WHERE IdMoneda = 1

UPDATE PagoEfectivo.Moneda
SET EquivalenciaBancaria = 1
WHERE IdMoneda = 2

--- Ingresar registros en ServicioTipoOrigenCancelacion

INSERT INTO PagoEfectivo.ServicioTipoOrigenCancelacion
(IdServicio,IdTipoOrigenCancelacion)
VALUES(4,1) 