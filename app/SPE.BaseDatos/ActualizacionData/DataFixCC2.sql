
INSERT INTO PagoEfectivo.Sistema
	(idSistema,CodSistema,DescripSistema,Estado)
VALUES 
	(1,'SPE','Pago Efectivo','1');
go

--- Roles
INSERT INTO PagoEfectivo.Rol([IdRol],[Description],[Nemonico]) VALUES(6,'Jefe de Producto','Jefe de Producto')
GO

INSERT INTO PagoEfectivo.SistemaXRol
	(idSistema,idRol)
	(SELECT (SELECT IdSistema FROM PagoEfectivo.Sistema WHERE CodSistema='SPE'),IdRol 
	 FROM PagoEfectivo.Rol)

GO
DELETE FROM  PagoEfectivo.Pagina 
--exec [PagoEfectivo].[prc_ConsultarMenu] 1
--Cliente
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (1,0,'Default','Default',1,'Default.aspx',1,0);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (2,1,'Principal - Cliente','Principal - Cliente',1,'~/CLI/PgPrl.aspx',1,1);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (9,1,'&nbsp;&nbsp;&nbsp;Actualizar Datos','Actualizar Datos',1,'~/CLI/ADAcCl.aspx',1,2);
go

INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (10,1,'&nbsp;&nbsp;&nbsp;Historial de Pagos','Historial de Pagos',1,'~/CLI/COHiOrPa.aspx',1,3);
go


--Administrador
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (3,1,'Principal - Administrador','Principal - Administrador',1,'~/ADM/PgPrl.aspx',1,1);
go

INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (11,1,'&nbsp;&nbsp;&nbsp;Registrar Cliente','Registrar Cliente',1, '~/ADM/ADClint.aspx',1,5);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (15,1,'&nbsp;&nbsp;&nbsp;Consultar Cliente','Consultar Cliente',1,'~/ADM/COClint.aspx',1,6);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (38,1,'&nbsp;&nbsp;&nbsp;Actualizar Cliente','Actualizar Cliente',1,'~/CLI/ADAcCl.aspx',1,7); 
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (12,1,'&nbsp;&nbsp;&nbsp;Registrar Agente','Registrar Agente',1,'~/ADM/ADAgen.aspx',1,8);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (31,1,'&nbsp;&nbsp;&nbsp;Consultar Agente','Consultar Agente',1,'~/ADM/COAgen.aspx',1,9);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (13,1,'&nbsp;&nbsp;&nbsp;Registrar Agencia Recaudadora','Registrar Agencia Recaudadora',1,'~/ADM/ADAgRe.aspx',1,10);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (16,1,'&nbsp;&nbsp;&nbsp;Consultar Agencia Recaudadora','Consultar Agencia Recaudadora',1, '~/ADM/COAgRe.aspx',1,11);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (14,1,'&nbsp;&nbsp;&nbsp;Registrar Empresa Contratante','Registrar Empresa Contratante',1,'~/ADM/ADEmCo.aspx',1,12);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (17,1,'&nbsp;&nbsp;&nbsp;Consultar Empresa Contratante','Consultar Empresa Contratante',1,'~/ADM/COEmCo.aspx',1,13);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (20,1,'&nbsp;&nbsp;&nbsp;Registrar Representante','Registrar Representante',1,'~/ADM/ADRepr.aspx',1,14);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (21,1,'&nbsp;&nbsp;&nbsp;Consultar Representante','Consultar Representante',1,'~/ADM/CORepr.aspx',1,15);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (22,1,'&nbsp;&nbsp;&nbsp;Registrar Servicio','Registrar Servicio',1, '~/ADM/ADServ.aspx',1,16);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (23,1,'&nbsp;&nbsp;&nbsp;Consultar Servicio','Consultar Servicio',1,'~/ADM/COServ.aspx',1,17);
go

INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (34,1,'&nbsp;&nbsp;&nbsp;Desbloquear Usuario','Desbloquear Usuario',1,'~/ADM/PRDesUs.aspx',1,18);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (47,1,'&nbsp;&nbsp;&nbsp;Consultar Archivos Generados','Consultar Archivos Generados',1,'~/ADM/COArGe.aspx',1,19);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (52,1,'&nbsp;&nbsp;&nbsp;Actualizar Archivos Generados','Actulizar Archivos Generados',1,'~/ADM/ADArGe.aspx',0,20);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (51,1,'&nbsp;&nbsp;&nbsp;Registrar Jefe de Producto','Registrar Jefe de Producto',1,'~/ADM/ADJePro.aspx',1,21);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (50,1,'&nbsp;&nbsp;&nbsp;Consultar Jefe de Producto','Consultar Jefe de Producto',1,'~/ADM/COJePro.aspx',1,1);
go

INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (32,1,'&nbsp;&nbsp;&nbsp;Generar Código de Identificación de Pago','Generar Código de Identificación de Pago',1, '~/CLI/PRGeOrPa.aspx',1,23);
go

--Agente
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (4,1,'Principal - Agente','Principal - Agente',1,'~/ARE/PgPrl.aspx',1,1);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (25,1,'&nbsp;&nbsp;&nbsp;Aperturar Caja','Aperturar Caja',1,'~/ARE/PRApCa.aspx',1,31);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (26,1,'&nbsp;&nbsp;&nbsp;Anular Caja','Anular caja',1,'~/ARE/PRAnCa.aspx',1,32);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (27,1,'&nbsp;&nbsp;&nbsp;Liquidar Caja','Liquidar Caja',1,'~/ARE/PRLiCa.aspx',1,33);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (29,1,'&nbsp;&nbsp;&nbsp;Consultar Caja','Consultar Caja',1,'~/ARE/COCaLq.aspx',1,34);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (28,1,'&nbsp;&nbsp;&nbsp;Consultar Código de Identificación de Pago','Consultar Código de Identificación de Pago',1,'~/ARE/COOrPa.aspx',1,35);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (30,1,'&nbsp;&nbsp;&nbsp;Recepcionar Código de Identificación de Pago','Recepcionar Código de Identificación de Pago',1,'~/ARE/PRReOrPa.aspx',1,36);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (36,1,'&nbsp;&nbsp;&nbsp;Anular Código de Identificación de Pago','Anular Código de Identificación de Pago',1,'~/ARE/PRAnOrPa.aspx',1,37);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (33,1,'&nbsp;&nbsp;&nbsp;Administración de Cajas','Administración de Cajas',1, '~/ARE/ADCaja.aspx',1,38);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (39,1,'&nbsp;&nbsp;&nbsp;Consultar Cajas Activas','Consultar Cajas Activas',1,'~/ARE/COCaAc.aspx',1,39);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (37,1,'&nbsp;&nbsp;&nbsp;Registrar Sobrante / Faltante','Registrar Sobrante / Faltante',1,'~/ARE/PRReSoFa.aspx',1,40);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (35,1,'&nbsp;&nbsp;&nbsp;Liquidar Caja en Bloque','Liquidar Caja en Bloque',1, '~/ARE/PRLiCaBl.aspx',1,41);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (18,1,'&nbsp;&nbsp;&nbsp;Otorgar Fecha Valuta','Default',1, '~/ARE/PROtFv.aspx',1,42);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (40,1,'&nbsp;&nbsp;&nbsp;Reporte de Cajas Liquidadas','Reporte de Cajas Liquidadas',1, '~/ARE/PRLiCaBlReporte.aspx',1,43);
go


--EmpresaContrante
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (5,1,'Principal - Empresa','Principal - Empresa',1,'~/ECO/PgPrl.aspx',1,1);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (24,1,'&nbsp;&nbsp;&nbsp;Consultar Pago Servicios','Consultar Pago Servicios',1,'~/ECO/COPaSer.aspx',1,45);
go


--JefeProducto
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (6,1,'Principal - Jefe de Producto','Principal - Jefe de Producto',1,'~/JPR/PgPrl.aspx',1,1);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (41,1,'&nbsp;&nbsp;&nbsp;Registrar Agencia Bancaria','Registrar Agencia Bancaria',1, '~/JPR/ADAgBa.aspx',1,48);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (42,1,'&nbsp;&nbsp;&nbsp;Consulta de Agencia Bancaria','Consulta de Agencia Bancaria',1,'~/JPR/COAgBa.aspx',1,49);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (43,1,'&nbsp;&nbsp;&nbsp;Registrar Operador','Registrar Operador',1,'~/JPR/ADOper.aspx',1,50);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (44,1,'&nbsp;&nbsp;&nbsp;Consultar Operador','Consultar Operador',1,'~/JPR/COOper.aspx',1,51);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (45,1,'&nbsp;&nbsp;&nbsp;Registrar Establecimiento','Registrar Establecimiento',1,'~/JPR/ADEstab.aspx',1,52);	
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (46,1,'&nbsp;&nbsp;&nbsp;Consultar Establecimiento','Consultar Establecimiento',1,'~/JPR/COEstab.aspx',1,53);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (48,1,'&nbsp;&nbsp;&nbsp;Consultar Conciliaciones','Consultar Conciliaciones',1,'~/JPR/COConc.aspx',1,54);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (49,1,'&nbsp;&nbsp;&nbsp;Conciliación Bancaria','Conciliación Bancaria',1,'~/JPR/PRConOpeBan.aspx',1,55);
go


--Todos
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (7,1,'Contactar Administrador','Contactar Administrador',1,'~/ADM/PRCoAdm.aspx',1,99);
go
INSERT INTO PagoEfectivo.Pagina(IdPagina,Parent,Titulo,Descripcion,Estado,URL,Visible,NroOrden)
VALUES (8,1,'Cambiar Contraseña','Cambiar Contraseña',1,'~/CLI/ADCaCo.aspx',1,100);
go





BEGIN

DECLARE @idSistema INT;
DECLARE @idRol INT;

set @idSistema=(SELECT IdSistema FROM PAGOEFECTIVO.Sistema WHERE CodSistema='SPE');

set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Administrador');

DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,3,1);
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,15,2);
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,11,3);
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,31,4);
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,12,5);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,23,6);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,22,7);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,21,8);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,20,9);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,16,10);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,13,11);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,17,12);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,14,13);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,34,14);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,15);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,9,16);				
	
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,7,17);				

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,50,18);				

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,51,19);				

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,52,20);		

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,47,21);

set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Supervisor');

DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,2,2);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,9,3);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,10,4);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,5);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,7,6);

set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Representante');

DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,5,1);
----------------------
----------------------INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
----------------------VALUES (@idsistema,@idRol,5,2);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,24,3);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,7,4);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,5);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,9,6);


set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Supervisor');
DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,4,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,33,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,29,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,39,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,28,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,25,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,26,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,27,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,30,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,36,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,18,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,37,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,35,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,40,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,7,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,9,1);


set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Cajero');
DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,4,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,25,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,26,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,27,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,28,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,29,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,30,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,36,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,1);

set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Jefe de Producto');
DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,6,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,7,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,9,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,44,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,43,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,46,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,45,1);
----------------INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
----------------VALUES (@idsistema,@idRol,46,1);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,41,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,42,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,48,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,49,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,13,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,16,1);
--INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
--VALUES (@idsistema,@idRol,20,1);
--INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
--VALUES (@idsistema,@idRol,21,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,12,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,31,1);

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,12,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,31,1);


set @idRol=(SELECT IdRol FROM PagoEfectivo.Rol WHERE Description='Cliente');
DELETE FROM PagoEfectivo.SistemaXRolXPagina WHERE idSistema=@idSistema AND idRol=@idRol;

INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,1,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,2,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,9,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,10,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,8,1);
INSERT INTO PagoEfectivo.SistemaXRolXPagina(idSistema,idRol,idPagina,NroOrden)
VALUES (@idsistema,@idRol,7,1)
END

GO

DELETE FROM PagoEfectivo.SistemaXRolXPagina 
WHERE IdRol<>4 AND IdPagina=9


GO

--- Agencias
DELETE FROM PagoEfectivo.TipoOrigenCancelacionPagina 
WHERE IdtipoOrigenCancelacion=1;
go

INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,1);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,6);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,41);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,42);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,48);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,49);
go

INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,7);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,8);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (1,9);
GO

---- Establecimiento
DELETE FROM PagoEfectivo.TipoOrigenCancelacionPagina 
WHERE IdtipoOrigenCancelacion=3;
go

INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,1);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,6);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,43);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,44);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,45);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,46);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,7);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,8);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (3,9);
GO
---- Agencia Recaudadora

DELETE FROM PagoEfectivo.TipoOrigenCancelacionPagina 
WHERE IdtipoOrigenCancelacion=2;
go


INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,1);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,6);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,13);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,16);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,20);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,21);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,7);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,8);
go
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,9);
GO
INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,12);
GO

INSERT INTO PagoEfectivo.TipoOrigenCancelacionPagina
	(IdTipoOrigenCancelacion,IdPagina)
VALUES (2,31);
GO

delete PagoEfectivo.Parametro
where id = 24
GO

UPDATE	PagoEfectivo.Movimiento
SET		IdTipoOrigenCancelacion = 2
WHERE	IdTipoMovimiento in (34,35)
go
