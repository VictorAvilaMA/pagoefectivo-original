

CREATE TABLE PagoEfectivo.Comercio
	(

IdComercio INT IDENTITY NOT NULL,
RazonSocial VARCHAR (200) NOT NULL,
RUC CHAR(11) NOT NULL,
Contacto VARCHAR(200), 
Telefono VARCHAR (15),
Direccion VARCHAR(200),
IdCiudad INT NOT NULL,
IdEstado INT NOT NULL,
IdUsuarioCreacion INT NOT NULL,
FechaCreacion DATETIME NOT NULL,
IdUsuarioActualizacion INT ,
FechaActualizacion DATETIME,
Codigo VARCHAR (50)
CONSTRAINT PK_Comerio PRIMARY KEY (IdComercio),
)

GO

ALTER TABLE [PagoEfectivo].[Comercio]  WITH CHECK ADD  CONSTRAINT [FK_Comercio_Ciudad] FOREIGN KEY([IdCiudad])
REFERENCES [PagoEfectivo].[Ciudad] ([IdCiudad])

GO


ALTER TABLE  PagoEfectivo.Establecimiento 
DROP COLUMN RazonSocial;
ALTER TABLE  PagoEfectivo.Establecimiento 
DROP COLUMN	Contacto;
ALTER TABLE  PagoEfectivo.Establecimiento 
DROP COLUMN	RUC;
ALTER TABLE  PagoEfectivo.Establecimiento 
DROP COLUMN	Telefono;
ALTER TABLE  PagoEfectivo.Establecimiento 
DROP COLUMN	Direccion;
ALTER TABLE  PagoEfectivo.Establecimiento 
DROP COLUMN IdCiudad;
GO

ALTER TABLE PagoEfectivo.Establecimiento
ADD Descripcion VARCHAR(200) NULL;
GO

ALTER TABLE PagoEfectivo.Establecimiento
ADD IdComercio INT NOT NULL;
GO	


ALTER TABLE [PagoEfectivo].[Establecimiento]  WITH CHECK ADD  CONSTRAINT [FK_Establecimiento_Comercio] FOREIGN KEY([IdComercio])
REFERENCES [PagoEfectivo].[Comercio] ([IdComercio])
GO





CREATE TABLE PagoEfectivo.AperturaOrigenError
   (
	[id] int IDENTITY NOT NULL,
	[FechaOcurrencia] datetime NULL,
	[idEstablecimiento] int NULL,
	[idAperturaCaja] int NULL,
	[MensajeError] nvarchar(50)  NULL,
	[TipoError] nvarchar( 50)  NULL
    );
    
    GO
    
ALTER TABLE PagoEfectivo.AperturaOrigen 
ADD FechaLiquidacion DATETIME  NULL;
GO


