USE BD_PAGOEFECTIVO_TEST3
CREATE TABLE PagoEfectivo.TipoOrigenCancelacion
	(
	IdTipoOrigenCancelacion INT IDENTITY NOT NULL,
	Descripcion VARCHAR(50) NOT NULL,
	CONSTRAINT PK_TipoOrigenCancelacion PRIMARY KEY (IdTipoOrigenCancelacion)
	)
GO



CREATE TABLE PagoEfectivo.ServicioTipoOrigenCancelacion
	(
	IdServicio INT  NOT NULL,
	IdTipoOrigenCancelacion INT NOT NULL,
	FOREIGN KEY (IdServicio) REFERENCES PagoEfectivo.Servicio (IdServicio),
	FOREIGN KEY (IdTipoOrigenCancelacion) REFERENCES PagoEfectivo.TipoOrigenCancelacion (IdTipoOrigenCancelacion)
	)
GO


CREATE TABLE PagoEfectivo.ConciliacionBancaria
	(
	IdConciliacion INT IDENTITY NOT NULL ,
	FechaConciliacion DATETIME NOT NULL,
	NombreArchivo VARCHAR(100) NOT NULL,
	IdEstado INT NULL,
	CONSTRAINT PK_ConciliacionBancaria PRIMARY KEY (IdConciliacion)
	)
GO

CREATE TABLE PagoEfectivo.OperacionBancaria
	(
	idOperacion INT IDENTITY NOT NULL,
	IdConciliacion INT NOT NULL,
	TipoRegistro CHAR(2) NULL,
	CodigoSucursal CHAR(3) NULL,
	CodigoMoneda CHAR(1) NULL,
	NumeroCuenta CHAR(7) NULL,
	CodigoDepositante CHAR(14) NULL,
	InformacionRetorno CHAR(50) NULL,
	FechaPago CHAR(8) NULL,
	FechaVencimiento CHAR(8) NULL,
	Monto CHAR(15) NULL,
	Mora CHAR(15) NULL,
	MontoTotal CHAR(15) NULL,
	CodigoOficina CHAR(6) NULL,
	NumeroOperacion CHAR(6) NULL,
	Referencia CHAR(22) NULL,
	IdentificacionTerminal CHAR(4) NULL,
	MedioAtencion CHAR(12) NULL,
	HoraAtencion CHAR(6) NULL,
	NumeroCheque CHAR(10) NULL,
	CodigoBanco CHAR(2) NULL,
	CargoFijoPagado CHAR(10) NULL,
	Filler CHAR(4) NULL,
	IdEstado INT NULL,
	Observacion VARCHAR(100),
	CONSTRAINT PK_OperacionBancaria PRIMARY KEY (idOperacion),
	FOREIGN KEY (IdConciliacion) REFERENCES PagoEfectivo.ConciliacionBancaria (IdConciliacion)
	)
GO

CREATE TABLE PagoEfectivo.Banco
	(
	IdBanco INT IDENTITY NOT NULL,
	Codigo VARCHAR(50) NOT NULL,
	Descripcion VARCHAR(100) NOT NULL,
	IdEstado int NULL,
	IdUsuarioCreacion int NOT NULL,
	IdUsuarioActualizacion int NULL,
	FechaCreacion datetime NOT NULL,
	FechaActualizacion datetime NULL,
	CONSTRAINT PK_Banco PRIMARY KEY (IdBanco)
	)
GO


CREATE TABLE PagoEfectivo.AgenciaBancaria
	(
	IdAgenciaBancaria INT IDENTITY NOT NULL,
	IdBanco INT NULL,
	Codigo VARCHAR(50) NOT NULL,
	Descripcion VARCHAR(250) NULL,
	Direccion VARCHAR(250) NULL,
	IdEstado INT NOT NULL,
	IdUsuarioCreacion INT NULL,
	FechaCreacion DATETIME NOT NULL,
	IdUsuarioActualizacion INT NULL,
	FechaActualizacion DATETIME NULL,
	IdCiudad	INT	NULL,
	CONSTRAINT PK_AgenciaBancaria_1 PRIMARY KEY (IdAgenciaBancaria),
	FOREIGN KEY (IdBanco) REFERENCES PagoEfectivo.Banco (IdBanco)
	)
GO

ALTER TABLE [PagoEfectivo].[AgenciaBancaria]  WITH CHECK ADD  CONSTRAINT [FK_AgenciaBancaria_Ciudad] FOREIGN KEY([IdCiudad])
REFERENCES [PagoEfectivo].[Ciudad] ([IdCiudad])
GO
ALTER TABLE [PagoEfectivo].[AgenciaBancaria] CHECK CONSTRAINT [FK_AgenciaBancaria_Ciudad]


GO

CREATE TABLE PagoEfectivo.Operador
	(
	IdOperador INT IDENTITY NOT NULL,
	Codigo VARCHAR(50) NOT NULL,
	Descripcion VARCHAR(50) NOT NULL,
	IdEstado INT NOT NULL,
	IdUsuarioCreacion INT NULL,
	FechaCreacion DATETIME NOT NULL,
	IdUsuarioActualizacion INT NULL,
	FechaActualizacion DATETIME NULL,
	CONSTRAINT PK_Operador PRIMARY KEY (IdOperador)
	)
GO


CREATE TABLE PagoEfectivo.Establecimiento
	(
	IdEstablecimiento INT IDENTITY NOT NULL,
	SerieTerminal VARCHAR(50) NOT NULL,
	RazonSocial VARCHAR(200)  NULL,
	RUC CHAR(11)  NULL,
	Contacto VARCHAR(200) NULL,
	Telefono VARCHAR(15) NULL,
	Direccion VARCHAR(200) NULL,
	IdOperador INT NOT NULL,
	IdCiudad INT NOT NULL,
	IdEstado INT NOT NULL,
	IdUsuarioCreacion INT NULL,
	FechaCreacion DATETIME NOT NULL,
	IdUsuarioActualizacion INT NULL,
	FechaActualizacion DATETIME NULL,
	CONSTRAINT PK_Establecimiento_1 PRIMARY KEY (IdEstablecimiento),
	FOREIGN KEY (IdOperador) REFERENCES PagoEfectivo.Operador (IdOperador)
	)
GO


CREATE TABLE PagoEfectivo.AperturaOrigen
	(
	IdAperturaOrigen INT IDENTITY NOT NULL,
	IdTipoOrigenCancelacion INT NULL,
	IdEstablecimiento INT NULL,
	IdAgenciaBancaria INT NULL,
	FechaApertura DATETIME NULL,
	FechaCierre DATETIME NULL,
	IdEstado INT NOT NULL,
	CONSTRAINT PK_OrigenCancelacion PRIMARY KEY (IdAperturaOrigen),
	FOREIGN KEY (IdTipoOrigenCancelacion) REFERENCES PagoEfectivo.TipoOrigenCancelacion (IdTipoOrigenCancelacion),
	FOREIGN KEY (IdAgenciaBancaria) REFERENCES PagoEfectivo.AgenciaBancaria (IdAgenciaBancaria),
	FOREIGN KEY (IdEstablecimiento) REFERENCES PagoEfectivo.Establecimiento (IdEstablecimiento)
	)
GO

ALTER TABLE PagoEfectivo.Movimiento 
ADD  IdAperturaOrigen INT NULL;
go
ALTER TABLE PagoEfectivo.Movimiento
ADD  NumeroOperacion VARCHAR(50) NULL;
go
ALTER TABLE PagoEfectivo.Movimiento
ADD  IdTipoOrigenCancelacion INT NULL;
go
---
ALTER TABLE PagoEfectivo.Movimiento 
DROP FK_Movimiento_AgenteCaja;
go
ALTER TABLE PagoEfectivo.Movimiento
ADD IdAgenteCajaTemp INT NULL;
go
UPDATE PagoEfectivo.Movimiento
SET IdAgenteCajaTemp=IdAgenteCaja;
go
ALTER TABLE  PagoEfectivo.Movimiento
DROP COLUMN IdAgenteCaja;

ALTER TABLE PagoEfectivo.Movimiento
ADD IdAgenteCaja INT NULL;
go
ALTER TABLE PagoEfectivo.Movimiento
ADD FOREIGN KEY (IdAgenteCaja) REFERENCES PagoEfectivo.AgenteCaja (IdAgenteCaja);
go
ALTER TABLE PagoEfectivo.Movimiento
ADD FOREIGN KEY (IdTipoOrigenCancelacion) REFERENCES PagoEfectivo.TipoOrigenCancelacion (IdTipoOrigenCancelacion);
go
UPDATE PagoEfectivo.Movimiento
SET IdAgenteCaja=IdAgenteCajaTemp;
go
ALTER TABLE PagoEfectivo.Movimiento
DROP COLUMN IdAgenteCajaTemp;
go
--
ALTER TABLE PagoEfectivo.Moneda
ADD EquivalenciaBancaria CHAR(1);
go

ALTER TABLE PagoEfectivo.Movimiento
ADD FOREIGN KEY (IdAperturaOrigen) REFERENCES PagoEfectivo.AperturaOrigen(IdAperturaOrigen);
GO

---
--- PONER UsuarioCreacion Como Null

ALTER TABLE PagoEfectivo.Movimiento
ADD IdUsuarioCreacionTemp INT NULL;
go
UPDATE PagoEfectivo.Movimiento
SET IdUsuarioCreacionTemp=IdUsuarioCreacion;
go
ALTER TABLE  PagoEfectivo.Movimiento
DROP COLUMN IdUsuarioCreacion;

ALTER TABLE PagoEfectivo.Movimiento
ADD IdUsuarioCreacion INT NULL;
go

UPDATE PagoEfectivo.Movimiento
SET IdUsuarioCreacion=IdUsuarioCreacionTemp;
go
ALTER TABLE PagoEfectivo.Movimiento
DROP COLUMN IdUsuarioCreacionTemp;
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD FechaConciliacion DATETIME NULL;
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD IdEstadoConciliacion INT NULL;
GO


ALTER TABLE PagoEfectivo.Servicio ADD 
	IdTipoNotificacion	INT NULL,
	UrlFTP				VARCHAR(100) NULL,
	Usuario				VARCHAR(100) NULL,
	Password			VARCHAR(100) NULL 
go	
