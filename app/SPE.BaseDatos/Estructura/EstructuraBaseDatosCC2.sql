go
CREATE TABLE PagoEfectivo.JefeProducto
	(
	IdJefeProducto INT IDENTITY NOT NULL,
	IdUsuario INT NOT NULL,
	CONSTRAINT PK_JefeProducto PRIMARY KEY (IdJefeProducto),
	FOREIGN KEY (IdUsuario) REFERENCES PagoEfectivo.Usuario (IdUsuario)
	)
GO



CREATE TABLE PagoEfectivo.JefeProductoTipoOrigen
	(
	IdJefeProducto INT NOT NULL,
	IdTipoOrigenCancelacion INT NOT NULL,
	CONSTRAINT PK_JefeProductoTipoOrigen PRIMARY KEY (IdJefeProducto,IdTipoOrigenCancelacion),
	FOREIGN KEY (IdJefeProducto) REFERENCES PagoEfectivo.JefeProducto (IdJefeProducto),
	FOREIGN KEY (IdTipoOrigenCancelacion) REFERENCES PagoEfectivo.TipoOrigenCancelacion (IdTipoOrigenCancelacion)
	)
	
GO



CREATE TABLE PagoEfectivo.TipoOrigenCancelacionPagina
	(
	IdTipoOrigenCancelacion INT NOT NULL,
	IdPagina INT NOT NULL,
	CONSTRAINT PK_TipoOrigenCancelacionPagina PRIMARY KEY (IdTipoOrigenCancelacion,IdPagina),
	FOREIGN KEY (IdPagina) REFERENCES PagoEfectivo.Pagina (IdPagina),
	FOREIGN KEY (IdTipoOrigenCancelacion) REFERENCES PagoEfectivo.TipoOrigenCancelacion (IdTipoOrigenCancelacion)
	)

GO

CREATE TABLE PagoEfectivo.FTPArchivo
	(
	IdFtpArchivo INT IDENTITY NOT NULL,
	IdServicio INT NOT NULL,
	NombreArchivo VARCHAR(100) NOT NULL,
	FechaGenerado DATETIME NOT NULL,
	IdTipoArchivo INT NOT NULL,
	IdEstado INT NOT NULL
	CONSTRAINT PK_FTPArchivo PRIMARY KEY (IdFtpArchivo)
	)
GO

CREATE TABLE PagoEfectivo.FTPArchivoDetalle
	(
	IdFtpArchivoDetalle INT IDENTITY NOT NULL,
	IdFtpArchivo INT NOT NULL,
	idUsuario varchar(50) NULL,
    OrdenIdComercio varchar(64) NULL,
	tipoDePago VARCHAR(10) NULL,
	fechaPago DATETIME NULL,
	IdMoneda INT NULL,
	Monto DECIMAL(18,2) NULL,
	CONSTRAINT PK_FTPArchivoDetalle PRIMARY KEY (IdFtpArchivoDetalle),
	FOREIGN KEY (IdFtpArchivo) REFERENCES PagoEfectivo.FTPArchivo (IdFtpArchivo)
	)
GO




ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioID VARCHAR(50);
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD DataAdicional VARCHAR(200);
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioNombre VARCHAR(50);
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioApellidos VARCHAR(100);
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioDomicilio VARCHAR(100);
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioLocalidad VARCHAR(100);
go

ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioProvincia VARCHAR(100);
go


ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioPais VARCHAR(100);
go

UPDATE PagoEfectivo.OrdenPago
SET IdServicio=(SELECT Descripcion FROM PagoEfectivo.Parametro WHERE Id=141)
WHERE IdOrdenPago=11 OR idordenpago=12;

go

ALTER TABLE PagoEfectivo.OrdenPago
ADD FOREIGN KEY (IdServicio) REFERENCES PagoEfectivo.Servicio (IdServicio);

go



DROP TABLE PagoEfectivo.SistemaxRolxPagina;
go
ALTER TABLE PagoEfectivo.Pagina
DROP COLUMN Description;
GO
ALTER TABLE PagoEfectivo.Pagina
DROP COLUMN Estado;
GO
ALTER TABLE PagoEfectivo.Pagina
DROP COLUMN URL;
GO
ALTER TABLE PagoEfectivo.Pagina
DROP COLUMN Display;

go

ALTER TABLE PagoEfectivo.Pagina
ADD Titulo VARCHAR(50) NULL
GO

ALTER TABLE PagoEfectivo.Pagina
ADD Descripcion VARCHAR(250) NOT NULL;
GO
ALTER TABLE PagoEfectivo.Pagina
ADD Estado INT NOT NULL;
GO
ALTER TABLE PagoEfectivo.Pagina
ADD URL VARCHAR(250) NOT NULL;
GO
ALTER TABLE PagoEfectivo.Pagina
ADD Visible INT;
GO
ALTER TABLE PagoEfectivo.Pagina
ADD NroOrden INT NOT NULL;

go

alter table pagoefectivo.pagina
drop constraint FK__Pagina__Parent__34C8D9D1
go

CREATE TABLE PagoEfectivo.Sistema
	(
	idSistema INT NOT NULL,
	CodSistema VARCHAR(10) NOT NULL,
	DescripSistema VARCHAR(255) NOT NULL,
	Estado VARCHAR(1) NOT NULL,
	CONSTRAINT PK_Sistema PRIMARY KEY (idSistema)
	)
GO

CREATE TABLE PagoEfectivo.SistemaXRol
	(
	idSistema INT NOT NULL,
	idRol INT NOT NULL,
	CONSTRAINT PK_SistemaXRol PRIMARY KEY (idSistema,idRol),
	FOREIGN KEY (idRol) REFERENCES PagoEfectivo.Rol (IdRol),
	FOREIGN KEY (idSistema) REFERENCES PagoEfectivo.Sistema (idSistema)
	)	
GO

CREATE TABLE PagoEfectivo.SistemaXRolXPagina
	(
	idSistema INT NOT NULL,
	idRol INT NOT NULL,
	idPagina INT NOT NULL,
	NroOrden INT NOT NULL,
	CONSTRAINT PK_SistemaXRolXPagina PRIMARY KEY (idSistema,idRol,idPagina),
	FOREIGN KEY (idSistema,idRol) REFERENCES PagoEfectivo.SistemaXRol (idSistema,idRol),
	FOREIGN KEY (idPagina) REFERENCES PagoEfectivo.Pagina (IdPagina)
	)
GO


----
--- Para la notificacion
--ALTER TABLE PagoEfectivo.OrdenPago 
--ADD Notificado INT;

--ALTER TABLE PagoEfectivo.OrdenPago 
--ADD FechaNotificacion DATETIME;

ALTER TABLE PagoEfectivo.FTPArchivoDetalle 
ADD IdOrdenPago BIGINT
go

ALTER TABLE PagoEfectivo.FTPArchivoDetalle
ADD FOREIGN KEY (IdOrdenPago) REFERENCES PagoEfectivo.OrdenPago (IdOrdenPago);
go
CREATE INDEX IX_FTPArchivo_IdTipoArchivo
 ON PagoEfectivo.FTPArchivo(IdTipoArchivo);
 go
 
CREATE INDEX IX_FTPArchivo_IdEstado
 ON PagoEfectivo.FTPArchivo(IdEstado);

go


ALTER TABLE PagoEfectivo.OrdenPago
ADD UsuarioAlias VARCHAR(20) 

GO