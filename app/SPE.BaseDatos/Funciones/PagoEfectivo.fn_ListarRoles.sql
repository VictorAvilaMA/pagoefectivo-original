CREATE FUNCTION PagoEfectivo.fn_ListarRoles(@idPagina INT) 
	RETURNS VARCHAR(500)
	AS
	BEGIN
	  DECLARE @vResultado VARCHAR(100);
	  SET @vResultado='';
	  
	  DECLARE @vTabla TABLE(ID INT IDENTITY NOT NULL, 
                             Descripcion VARCHAR(255)
                           );
      DECLARE @vCantidad INT;
      DECLARE @vIndice INT;
                             
      INSERT INTO @vTabla(Descripcion)
      (SELECT DISTINCT R.Description  
	   FROM PagoEfectivo.SistemaXRolXPagina SRP
       JOIN PagoEfectivo.Rol R ON SRP.idRol =R.IdRol
       WHERE SRP.idPagina=@idPagina );
       
       
       SET @vIndice=1;
       SET @vCantidad=(SELECT count(*)  FROM @vTabla );
       
       
       WHILE(@vIndice<=@vCantidad)
       BEGIN
       
       DECLARE @vDescripcion VARCHAR(255);
       SELECT @vDescripcion=Descripcion FROM @vTabla
       WHERE id=@vIndice;
       
       SET @vResultado=@vResultado+@vDescripcion+',';
     
	  set	@vIndice=@vIndice+1;       
       END

      DECLARE @tam INT;
      SET @tam=len(@VResultado);

	 IF(@tam=0)
	 BEGIN
   	   set @VResultado= '';
   	 END 
   	 ELSE 
   	 BEGIN
   	  SET  @VResultado=substring(@VResultado,1,@tam-1);
	  
	  END 
	  RETURN @VResultado;
	END