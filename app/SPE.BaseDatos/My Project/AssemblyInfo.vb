﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Data.Sql

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SPE.BaseDatos")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("CMG")>
<Assembly: AssemblyProduct("SPE.BaseDatos")>
<Assembly: AssemblyCopyright("Copyright © CMG 2009")>
<Assembly: AssemblyTrademark("")>
<Assembly: CLSCompliant(True)>
<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("98600f66-170c-4896-aa3a-bad7537e9f03")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 
