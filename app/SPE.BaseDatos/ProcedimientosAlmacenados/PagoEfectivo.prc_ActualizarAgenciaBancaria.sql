/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Anular operacion de cancelacion (extorno)				*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 23/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ActualizarAgenciaBancaria] --'',0
	-- Add the parameters for the stored procedure here
	@pintIdAgenciaBancaria		int,
	@pintIdBanco				int,
	@pstrCodigo					varchar(50),
	@pstrDescripcion			varchar(100),
	@pstrDireccion				varchar(100),
	@pintIdEstado				int,
	@pintIdUsuarioActualizacion	int,
	@pintIdCiudad				int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		UPDATE	PagoEfectivo.AgenciaBancaria
		SET		IdBanco = @pintIdBanco,
				Codigo = @pstrCodigo,
				Descripcion = @pstrDescripcion,
				Direccion = @pstrDireccion,
				IdEstado = @pintIdEstado,
				IdUsuarioActualizacion = @pintIdUsuarioActualizacion,
				FechaActualizacion = GetDate(),
				IdCiudad = @pintIdCiudad
		WHERE	IdAgenciaBancaria = @pintIdAgenciaBancaria
	SET NOCOUNT OFF;

END
