
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Actualizacion de Establecimientos */    
/*             */
/* Creado x       : cvillanueva - Christian Villanueva                                */    
/* Fecha		  : 22/12/2008                                           */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ActualizarEstablecimiento]
	
	@pintIdEstablecimiento			INT,			
	@pstrSerieTerminal				VARCHAR(50),
	@pstrRazonSocial				VARCHAR(200),
	@pstrRUC						CHAR(11),
	@pstrContacto					VARCHAR(200),
	@pstrTelefono					VARCHAR(15),
	@pstrDireccion					VARCHAR(200),
	@pintIdOperador					INT,
	@pintIdCiudad					INT,
	@pintIdEstado					INT,
	@pintIdUsuarioActualizacion		INT
AS
BEGIN
	
		SET NOCOUNT ON;

		UPDATE 	PagoEfectivo.Establecimiento
		SET RazonSocial = @pstrRazonSocial
			,RUC = @pstrRUC
			,SerieTerminal = @pstrSerieTerminal
			,Contacto = @pstrContacto
			,Telefono = @pstrTelefono
			,Direccion = @pstrDireccion
			,IdOperador = @pintIdOperador
			,IdCiudad = @pintIdCiudad
			,IdEstado = @pintIdEstado
			,IdUsuarioActualizacion = @pintIdUsuarioActualizacion
			,FechaActualizacion = GETDATE()

		WHERE 	IdEstablecimiento = @pintIdEstablecimiento

		SET NOCOUNT OFF;

END