/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                           */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Actualizacion del Estado del FTPArchivo*/    
/*              */
/* Creado x       : Cesar Miranda                                 */    
/* Fecha    : 13/01/2009                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 13/01/2009                                             */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ActualizarEstadoFTPArchivoCabecera]
(
@IdFTPArchivo INT,
@IdEstado INT
)
AS
BEGIN

  UPDATE PagoEfectivo.FTPArchivo 
  SET IdEstado=@IdEstado
  WHERE IdFtpArchivo=@IdFTPArchivo;


END




GO