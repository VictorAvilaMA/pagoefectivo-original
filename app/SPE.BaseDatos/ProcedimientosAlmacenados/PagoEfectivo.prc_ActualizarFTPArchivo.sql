/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Actualizar estado archivo FTP							*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 08/01/2009												*/    
/* Validado x     :															*/    
/* Fec.Validacion :															*/    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ActualizarFTPArchivo] --1,'','1/7/2009','1/7/2009',221,0
	-- Add the parameters for the stored procedure here
	@pintIdFtpArchivo	int,
	@pintIdEstado		int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		UPDATE	PagoEfectivo.FTPArchivo
		SET		IdEstado = @pintIdEstado
		WHERE	IdFtpArchivo = @pintIdFtpArchivo

	SET NOCOUNT OFF;

END

