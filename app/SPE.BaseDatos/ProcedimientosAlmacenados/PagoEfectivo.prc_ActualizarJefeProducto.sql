/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la actualizacion de un Jefe de Producto. */    
/*             */
/* Creado x       : cvillanueva - Christian Villanueva                                */    
/* Fecha    : 06/01/2009                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ActualizarJefeProducto]
	
	@pintIdUsuario				INT,
	@pintIdCiudad				INT,
	@pstrNombres				VARCHAR(100),
	@pstrApellidos				VARCHAR(100),	
	@pintIdTipoDocumento		INT,
	@pstrNroDocumento			VARCHAR(50),
	@pstrDireccion				VARCHAR(100),
	@pstrTelefono				VARCHAR(15),
	@pintIdEstado				INT,
	@pintIdUsuarioActualizacion	INT

AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE	US 
	SET		US.IdCiudad = @pintIdCiudad,
			US.Nombres = @pstrNombres,
			US.Apellidos = @pstrApellidos,
			US.IdTipoDocumento = @pintIdTipoDocumento,
			US.NumeroDocumento = @pstrNroDocumento,
			US.Direccion = @pstrDireccion,
			US.Telefono = @pstrTelefono,
			US.IdEstado = @pintIdEstado,
			US.IdUsuarioActualizacion = @pintIdUsuarioActualizacion,
			US.FechaActualizacion = GETDATE()
	FROM	PagoEfectivo.Usuario US
	WHERE	US.IdUsuario = @pintIdUsuario

	SET NOCOUNT OFF;
END
