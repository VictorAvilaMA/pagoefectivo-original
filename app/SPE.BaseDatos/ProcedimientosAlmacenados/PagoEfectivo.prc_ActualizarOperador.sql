/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                           */    
/* Sistema        : Pago Efectivo                                           */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Actualizar de un Operador                      */    
/*                */
/* Creado x       : kfigueroa@3devnet.com - Karen Figueroa                  */    
/* Fecha          : 26/12/2008                                              */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/


CREATE PROCEDURE [PagoEfectivo].[prc_ActualizarOperador]
	@pintIdOperador				INT,
	@pstrCodigo				    VARCHAR(200),
	@pstrDescripcion			VARCHAR(200),
	@pintIdEstado				INT,
	@pintIdUsuarioActualizacion	INT

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE PagoEfectivo.Operador
	SET Descripcion = @pstrDescripcion
        ,Codigo = @pstrCodigo
		,IdEstado = @pintIdEstado
		,FechaActualizacion = GETDATE()
		,IdUsuarioActualizacion = @pintIdUsuarioActualizacion

	WHERE IdOperador = @pintIdOperador;
	
	SET NOCOUNT OFF;

END
