/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Anular operacion de cancelacion (extorno)				*/    
/*																			*/
/* Creado x       : kfigueroa@3devnet.com - Karen Figueroa                  */    
/* Fecha		  : 23/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_AnularOperacionPOS] --'',0
    @pstrNroOperacion       VARCHAR(50),
	@pstrCodigoPOS          VARCHAR(50),  /* Codigo Establecimiento*/
    @pstrOrdenPago		    CHAR(14)     /* C�digo de C�digo de Identificaci�n de Pago*/

        ---------------------
AS
 BEGIN
      SET NOCOUNT ON;
        DECLARE @intMensaje				      INT
               ,@strMensaje				      VARCHAR(100)
	           ,@strOrdenPago			      VARCHAR(14) 
	           ,@intIdEstadoOP			      INT
	           ,@intIdAperturaOrigen		  INT
               ,@intIdOrdenPago			      INT
			   ,@intOrigenCancelacion         INT
			   ,@strCodigoPos                 VARCHAR(10)
			   ,@intIdAperturaOrigenInicio	  INT	
			
			   SET @intMensaje = 0
			   SET @strOrdenPago = RIGHT('00000000000000'+ RTRIM(CONVERT(VARCHAR(14),@pstrOrdenPago)),14)

			   
			   --VERIFICAMOS QUE EL ESTABLECIMIENTO ESTE REGISTRADO
			   IF NOT EXISTS (SELECT  E.IdEstablecimiento
			                  FROM     PagoEfectivo.AperturaOrigen AO 
									   INNER JOIN PagoEfectivo.Establecimiento E ON AO.IdEstablecimiento = E.IdEstablecimiento    
			                  WHERE	   E.SerieTerminal = @pstrCodigoPOS) --AND AO.idestado = 81)
                               
			 	  BEGIN 
					   SELECT DATOS_TX.RESPUESTA
						      ,DATOS_TX.MENSAJE
					   FROM (SELECT 105 RESPUESTA
							       ,'El establecimiento no se encuentra registrado.' MENSAJE) 
					   DATOS_TX
					   FOR XML AUTO, ELEMENTS
					   RETURN
				  END
			 
				IF NOT EXISTS (SELECT Es.IdEstablecimiento 
							   FROM PagoEfectivo.Establecimiento Es
									INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
									AND DATEADD(dd, 0, DATEDIFF(dd, 0,Ao.FechaApertura)) = DATEADD(dd, 0, DATEDIFF(dd, 0,GETDATE()))
									INNER JOIN PagoEfectivo.TipoOrigenCancelacion Toc ON Ao.IdTipoOrigenCancelacion = Toc.IdTipoOrigenCancelacion
									AND Toc.IdTipoOrigenCancelacion = 3
									AND Ao.IdEstado = 81 -- Estado de Apertura
								WHERE Es.SerieTerminal = @pstrCodigoPOS)
					BEGIN
	              		SELECT DATOS_TX.RESPUESTA
									,DATOS_TX.MENSAJE
						FROM (SELECT 108 RESPUESTA
									       ,'No tiene caja activa el d�a de hoy.' MENSAJE) 
						DATOS_TX
						FOR XML AUTO, ELEMENTS
				 		RETURN
					END

					/* OBTENEMOS DATOS DE LA C�digo de Identificaci�n de Pago */ 
					SELECT @intIdEstadoOP= OP.IdEstado
						   ,@intIdOrdenPago = M.IdOrdenPago
						   ,@strCodigoPos = E.SerieTerminal
						   ,@intOrigenCancelacion = AO.IdTipoOrigenCancelacion
						   ,@intIdAperturaOrigenInicio = M.idAperturaOrigen
					FROM   PagoEfectivo.Establecimiento E 
                           INNER JOIN PagoEfectivo.AperturaOrigen AO ON E.IdEstablecimiento = AO.IdEstablecimiento 
                           INNER JOIN PagoEfectivo.Movimiento M 
                           INNER JOIN PagoEfectivo.OrdenPago OP ON M.IdOrdenPago = OP.IdOrdenPago ON AO.IdAperturaOrigen = M.IdAperturaOrigen
					WHERE  OP.NumeroOrdenPago =  @strOrdenPago


					SELECT  @intIdAperturaOrigen = AO.IdAperturaOrigen
					FROM    PagoEfectivo.AperturaOrigen AO 
							INNER JOIN PagoEfectivo.Establecimiento E ON AO.IdEstablecimiento = E.IdEstablecimiento    
	                WHERE	E.SerieTerminal = @pstrCodigoPOS AND AO.idestado = 81  --Aperturado

               
						 IF (@intIdOrdenPago IS NULL) 
							BEGIN
							SET @intMensaje = 102 
							SET @strMensaje = 'El C�digo de Identificaci�n de Pago no existe.'
							END
				         
						 ELSE IF  (@intIdEstadoOP = 22) --OP GENERADA
							BEGIN
							SET @intMensaje = 109
							SET @strMensaje ='Lo sentimos, el C�digo de Identificaci�n de Pago no est� cancelada.'
							END
						
						ELSE IF (@intIdEstadoOP = 21)  --OP EXPIRADA
							BEGIN
							SET @intMensaje = 104
							SET @strMensaje ='El C�digo de Identificaci�n de Pago ha expirado.'
							END

						ELSE IF (@intOrigenCancelacion <> 3)  -- 3 = Origen Establecimiento
							BEGIN
							SET @intMensaje = 106
							SET @strMensaje ='El establecimiento no est� autorizado para realizar la operaci�n.'
							END 
					

						ELSE IF (@intIdAperturaOrigen <> @intIdAperturaOrigenInicio)  
							BEGIN
							SET @intMensaje = 122
							SET @strMensaje ='El C�digo de Identificaci�n de Pago no puede ser anulada.'
							END 


				IF (@intMensaje != 0)
					BEGIN
						SELECT DATOS_TX.RESPUESTA
								   ,DATOS_TX.MENSAJE
								   ,CASE WHEN DATOS_TX.RESPUESTA != 106 THEN DATOS_TX.COD_OP
									END COD_OP
						FROM (SELECT @intMensaje RESPUESTA
									,@strMensaje MENSAJE
									,@strOrdenPago COD_OP)
							DATOS_TX
						FOR XML AUTO, ELEMENTS
					END
               ELSE 
				 	BEGIN
				
					   --SE REALIZA LA ANULACION DE LA C�digo de Identificaci�n de Pago
					   INSERT INTO PagoEfectivo.Movimiento (
										   IdOrdenPago
										  ,IdMoneda
										  ,IdTipoMovimiento
										  ,IdMedioPago
										  ,Monto
										  ,FechaMovimiento
										  ,FechaCreacion
										  ,IdAperturaOrigen
										  ,NumeroOperacion
										  ,IdAgenteCaja
										  ,IdTipoOrigenCancelacion)
										   SELECT	Top 1 P.IdOrdenPago
													,P.IdMoneda
													,35				--35= Estado Anulado
													,P.IdMedioPago
													,Monto*-1
													,GETDATE()
													,GETDATE()
													,@intIdAperturaOrigen
													,@pstrNroOperacion 
													,P.IdAgenteCaja 
													,3 --ORIGEN: ESTABLECIMIENTO
										   FROM		PagoEfectivo.Movimiento P 
										   WHERE	P.IdOrdenPago= @intIdOrdenPago 
							               
		
						UPDATE	PagoEfectivo.OrdenPago 
						SET		IdEstado = 22, /* 22= Generada Estado de el C�digo de Identificaci�n de Pago */
								FechaCancelacion = NULL,
								FechaAnulada = Getdate()
						WHERE	NumeroOrdenPago =  @strOrdenPago


						SELECT DATOS_TX.RESPUESTA
						     ,DATOS_TX.MENSAJE
							 ,DATOS_TX.COD_OP
					    FROM (SELECT 107 RESPUESTA
							 ,'La anulaci�n se realiz� correctamente.' MENSAJE
							,@strOrdenPago COD_OP)
						   DATOS_TX
						FOR XML AUTO, ELEMENTS

                    END
								
  	   SET NOCOUNT OFF;
   END