/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 		*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_AnularOperacionPagoAB] --'00000000000019','11111'
	-- Add the parameters for the stored procedure here
	@pstrNumeroOrdenPago	VARCHAR(14),
	@pstrNumeroOperacion	VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE	@vintEstadoOrdenPago INT,
			@vintTipoMovimiento INT,
			@vintTipoMovimientoReg INT,
			@vstrNuemroOrdenP VARCHAR(14)

	SET		@vstrNuemroOrdenP = RIGHT('00000000000000' + @pstrNumeroOrdenPago,14)
	SET		@vintEstadoOrdenPago = 22 --(SELECT Id FROM PagoEfectivo.Parametro WHERE	GrupoCodigo = 'ESOP' and Descripcion = 'Generada')
	SET		@vintTipoMovimiento = 34 --(SELECT Id FROM PagoEfectivo.Parametro WHERE	GrupoCodigo = 'TMOV' and Descripcion = 'Recepcionar OP')
	SET		@vintTipoMovimientoReg = 35 --(SELECT Id FROM PagoEfectivo.Parametro WHERE	GrupoCodigo = 'TMOV' and Descripcion = 'Anular OP')


	INSERT	INTO	PagoEfectivo.Movimiento
	(IdOrdenPago, IdMoneda, IdTipoMovimiento, IdMedioPago, Monto, FechaMovimiento, FechaCreacion,
	FechaActualizacion, IdUsuarioActualizacion, IdAperturaOrigen, NumeroOperacion, IdAgenteCaja, IdUsuarioCreacion,
	IdTipoOrigenCancelacion)
	SELECT	TOP 1 M.IdOrdenPago,
			M.IdMoneda, 
			@vintTipoMovimientoReg,
			IdMedioPago,
			Monto * -1,
			GetDate(),
			GetDate(),
			NULL,
			NULL,
			IdAperturaOrigen,
			@pstrNumeroOperacion,
			IdAgenteCaja,
			NULL,
			1		--ORIGEN :AGENCIA BANCARIA
	FROM	PagoEfectivo.Movimiento M INNER JOIN PagoEfectivo.OrdenPago OP ON
			M.IdOrdenPago = OP.IdOrdenPago
	WHERE	OP.NumeroOrdenPago = @vstrNuemroOrdenP AND
			IdTipoMovimiento = @vintTipoMovimiento;


	UPDATE	PagoEfectivo.OrdenPago
	SET		IdEstado				= @vintEstadoOrdenPago,
			FechaActualizacion		= GETDATE(),
			IdUsuarioActualizacion	= NULL,
			FechaAnulada			= GETDATE(),
			FechaCancelacion		= NULL

	WHERE	NumeroOrdenPago	= @vstrNuemroOrdenP;
	


	SET NOCOUNT OFF;

END

