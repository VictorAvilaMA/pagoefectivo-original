
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Aperturar Caja Establecimiento				            */    
/*																			*/
/* Creado x       : kfigueroa@3devnet.com - Karen Figueroa                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_AperturarCajaPOS] --'9999',1,500
	@pstrNumeroOperacion	VARCHAR(50),
	@pstrCodigoPOS          VARCHAR(50),  /* Codigo Establecimiento*/
    @pintIdMoneda		    INT,     /* C�digo de C�digo de Identificaci�n de Pago*/
    @pintMonto              decimal(18,2)

AS
  BEGIN
      SET NOCOUNT ON;
      DECLARE @intMensaje				      INT
             ,@strMensaje				      VARCHAR(100)
             ,@intIdAperturaOrigen		      INT
             ,@intIdEstadoAperturaOrigen      INT
             ,@strCodigoPos                   VARCHAR(10)
			
			 SET @intMensaje = 0

			 IF NOT EXISTS (SELECT  E.IdEstablecimiento
			                FROM    PagoEfectivo.Establecimiento E 
						    WHERE	E.SerieTerminal = @pstrCodigoPOS )
                               
			  BEGIN 
				   SELECT @intMensaje = 105;
				   SELECT DATOS_TX.RESPUESTA
					      ,DATOS_TX.MENSAJE
				   FROM (SELECT 105 RESPUESTA
						       ,'El establecimiento no se encuentra registrado.' MENSAJE) 
				   DATOS_TX
				   FOR XML AUTO, ELEMENTS
				   RETURN
              END
		
        
 		     ELSE IF EXISTS (SELECT Es.IdEstablecimiento 
					        FROM PagoEfectivo.Establecimiento Es
								  INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
								  INNER JOIN PagoEfectivo.TipoOrigenCancelacion Toc ON Ao.IdTipoOrigenCancelacion = Toc.IdTipoOrigenCancelacion
								  AND Toc.IdTipoOrigenCancelacion = 3	AND Ao.IdEstado = 81 -- Estado de Apertura
							WHERE Es.SerieTerminal =@pstrCodigoPOS)
								
			  BEGIN

						DECLARE @fecha DATETIME
						SELECT @fecha = Ao.FechaApertura 
						FROM PagoEfectivo.Establecimiento Es
							  INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
							  INNER JOIN PagoEfectivo.TipoOrigenCancelacion Toc ON Ao.IdTipoOrigenCancelacion = Toc.IdTipoOrigenCancelacion
							  AND Toc.IdTipoOrigenCancelacion = 3	AND Ao.IdEstado = 81 -- Estado de Apertura
						WHERE Es.SerieTerminal =@pstrCodigoPOS
						
						IF DATEADD(dd, 0, DATEDIFF(dd, 0,@fecha)) = DATEADD(dd, 0, DATEDIFF(dd, 0,GETDATE()))
							BEGIN
								SELECT @intMensaje = 114;
	              				SELECT DATOS_TX.RESPUESTA
									  ,DATOS_TX.MENSAJE
								FROM (SELECT 114 RESPUESTA
									  ,'Ya posee una caja activa.' MENSAJE) 
								DATOS_TX
								FOR XML AUTO, ELEMENTS
				 				RETURN
							END
						ELSE
							BEGIN
								SELECT @intMensaje = 120;
	              				SELECT DATOS_TX.RESPUESTA
									  ,DATOS_TX.MENSAJE
								FROM (SELECT 120 RESPUESTA
									  ,'Tiene una caja pendiente de cierre.' MENSAJE) 
								DATOS_TX
								FOR XML AUTO, ELEMENTS
				 				RETURN
							END					

							
			  END


			ELSE IF NOT EXISTS (SELECT MO.IdMoneda FROM PagoEfectivo.Moneda MO WHERE MO.IdMoneda= @pintIdMoneda)
					BEGIN
							SELECT @intMensaje = 115;
	              		    SELECT DATOS_TX.RESPUESTA
								  ,DATOS_TX.MENSAJE
							FROM (SELECT 115 RESPUESTA
								  ,'El codigo de moneda ingresado no es correcto' MENSAJE) 
							DATOS_TX
							FOR XML AUTO, ELEMENTS
				 			RETURN

					END
          

             ELSE IF    (@pintMonto < 0.00)
             	BEGIN
							SELECT @intMensaje = 116;
	              		    SELECT DATOS_TX.RESPUESTA
								  ,DATOS_TX.MENSAJE
							FROM (SELECT 116 RESPUESTA
								  ,'El monto ingresado no es correcto' MENSAJE) 
							DATOS_TX
							FOR XML AUTO, ELEMENTS
	 		 			    RETURN
				END
            
			DECLARE @IdAperturaOrigenGenerado INT
			DECLARE @IdEstablecimiento INT
		    SET @IdEstablecimiento = (SELECT E.IdEstablecimiento 
									  FROM PagoEfectivo.Establecimiento E
									  WHERE E.SerieTerminal =@pstrCodigoPos)

		     IF (@intMensaje != 0)
			  	    BEGIN
					  	   SELECT DATOS_TX.RESPUESTA
								   ,DATOS_TX.MENSAJE
								   ,DATOS_TX.COD_POS
						   FROM (SELECT @intMensaje RESPUESTA
									,@strMensaje MENSAJE
									,@pstrCodigoPOS COD_POS)
							DATOS_TX
					 	    FOR XML AUTO, ELEMENTS
					END
             ELSE 
           	 	  BEGIN
					IF (@pintMonto > 0.00)
						 BEGIN
							INSERT INTO [PagoEfectivo].[AperturaOrigen]
									([IdTipoOrigenCancelacion]
									,[IdEstablecimiento]
									,[FechaApertura]
									,[IdEstado])
							VALUES
									(3 
									,@IdEstablecimiento
									,getdate()
									,81) /* 81 = Estado Aperturado*/
							
							SET	@IdAperturaOrigenGenerado = @@identity

							INSERT INTO PagoEfectivo.Movimiento
										   (
									 [IdMoneda]
									 ,[IdTipoMovimiento]
									 ,[IdMedioPago]
									 ,[FechaMovimiento]
									 ,[Monto]
									 ,[FechaCreacion]
									 ,[IdAperturaOrigen]
									 ,NumeroOperacion
									 ,IdTipoOrigenCancelacion)
							VALUES
									(
									@pintIdMoneda
									,33 /* APERTURA */ 
									,2
									,GETDATE()
									,@pintMonto
									,GETDATE()
									,@IdAperturaOrigenGenerado
									,@pstrNumeroOperacion
									,3 /*ORIGEN: ESTABLECIMIENTO*/)
				       END
                             
			        ELSE IF (@pintMonto = 0.00)
				      BEGIN
						--SELECT @IdAperturaEstablecimiento = (SELECT IdAperturaOrigen FROM PagoEfectivo.AperturaOrigen WHERE IdEstablecimiento = @IdEstablecimiento)
							INSERT INTO [PagoEfectivo].[AperturaOrigen]
									([IdTipoOrigenCancelacion]
									,[IdEstablecimiento]
									,[FechaApertura]
									,[IdEstado])
							VALUES
									(3 
									,@IdEstablecimiento
									,GETDATE()
									,81) /* 81 = Estado Aperturado*/
   					END  
			  END

                
		   SELECT DATOS_TX.RESPUESTA
				     ,DATOS_TX.MENSAJE
					 ,DATOS_TX.COD_POS
		   FROM (SELECT 113 RESPUESTA
				     ,'La caja ha sido aperturada.' MENSAJE
					 ,@pstrCodigoPOS COD_POS)
		   DATOS_TX
		   FOR XML AUTO, ELEMENTS

   							
  SET NOCOUNT OFF;
END