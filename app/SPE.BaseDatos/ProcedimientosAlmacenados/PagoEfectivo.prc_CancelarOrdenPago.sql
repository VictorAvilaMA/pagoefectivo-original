/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Cancelar C�digo de Identificaci�n de Pago para POS */    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva          */    
/* Fecha		  : 26/12/2008                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_CancelarOrdenPago] 
	@pstrNroEstablecimiento	VARCHAR(50),
	@pstrNumeroOperacion	VARCHAR(50),
	@pstrNumeroOP			CHAR(14),
	@pdecMonto				DECIMAL(18,2),
	@pintMoneda				INT,
	@pintMedioPago			INT
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @intMensaje				INT,
			@strMensaje				VARCHAR(100),
			@intMoneda				INT,
			@decMonto				DECIMAL(18,2),
			@intIdEstado			INT,
			@intIdAperturaOrigen	INT,
			@intIdOrdenPago			INT,
			@strNroOrdenPago	CHAR(14)
	SET @intMensaje = 0
	SET @strNroOrdenPago = RIGHT('00000000000000'+ RTRIM(CONVERT(VARCHAR(14),@pstrNumeroOP)),14)


	--VERIFICA SI EXISTE EL ESTABLECIMIENTO
	IF NOT EXISTS (SELECT  E.IdEstablecimiento
			                  FROM     PagoEfectivo.AperturaOrigen AO 
									   INNER JOIN PagoEfectivo.Establecimiento E ON AO.IdEstablecimiento = E.IdEstablecimiento    
			                  WHERE	   E.SerieTerminal = @pstrNroEstablecimiento)
                               
			 	  BEGIN 
					   SELECT DATOS_TX.RESPUESTA
						      ,DATOS_TX.MENSAJE
					   FROM (SELECT 105 RESPUESTA
							       ,'El establecimiento no se encuentra registrado.' MENSAJE) 
					   DATOS_TX
					   FOR XML AUTO, ELEMENTS
					   RETURN
				  END

	/** VERIFICAR SI LA CAJA ESTA ABIERTA PARA EL ESTABLECIMIENTO **/
	IF NOT EXISTS(SELECT Es.IdEstablecimiento FROM PagoEfectivo.Establecimiento Es
					INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
								AND DATEADD(dd, 0, DATEDIFF(dd, 0,Ao.FechaApertura)) = DATEADD(dd, 0, DATEDIFF(dd, 0,GETDATE()))
					INNER JOIN PagoEfectivo.TipoOrigenCancelacion Toc ON Ao.IdTipoOrigenCancelacion = Toc.IdTipoOrigenCancelacion
								AND Toc.IdTipoOrigenCancelacion = 3
								AND Ao.IdEstado = 81 -- Estado de Apertura
					WHERE Es.SerieTerminal = @pstrNroEstablecimiento)
		BEGIN
		SELECT DATOS_TX.RESPUESTA
				,DATOS_TX.MENSAJE
		FROM (SELECT 108 RESPUESTA
					,'No tiene caja activa el d�a de hoy.' MENSAJE) DATOS_TX
		FOR XML AUTO, ELEMENTS
		RETURN
		END
	
	IF EXISTS (SELECT IdOrdenPago	FROM PagoEfectivo.OrdenPago
				WHERE NumeroOrdenPago = @strNroOrdenPago)
		BEGIN
		SELECT   @intIdOrdenPago = OP.IdOrdenPago
				,@intMoneda = OP.IdMoneda
				,@decMonto = OP.Total
				,@intIdEstado = OP.IdEstado
		FROM PagoEfectivo.OrdenPago OP
		INNER JOIN PagoEfectivo.Servicio SE ON OP.IdServicio = SE.IdServicio
		INNER JOIN PagoEfectivo.ServicioTipoOrigenCancelacion SEOC ON SE.IdServicio = SEOC.IdServicio
		INNER JOIN PagoEfectivo.TipoOrigenCancelacion TIOC ON SEOC.IdTipoOrigenCancelacion = TIOC.IdTipoOrigenCancelacion
		AND TIOC.IdTipoOrigenCancelacion=3
		WHERE OP.NumeroOrdenPago = @strNroOrdenPago 

		SELECT @intIdAperturaOrigen = Ao.IdAperturaOrigen
		FROM PagoEfectivo.Establecimiento Es
		INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
		AND Ao.IdEstado = 81 -- Estado de Apertura
		WHERE Es.SerieTerminal = @pstrNroEstablecimiento
		
		IF (@intIdOrdenPago is null)
				BEGIN
				SET @intMensaje = 106
				SET @strMensaje ='El establecimiento no esta autorizado para realizar la operaci�n.'
				END
		ELSE IF (@decMonto != @pdecMonto)
				BEGIN
				SET @intMensaje = 111 
				SET @strMensaje = 'El monto ingresado no es el correcto.'
				END
		ELSE IF(@intMoneda ! = @pintMoneda)
				BEGIN
				SET @intMensaje = 112
				SET @strMensaje ='La moneda no coincide.'
				END
		ELSE IF  (@intIdEstado = 23) --OP CANCELADA
				BEGIN
				SET @intMensaje = 103
				SET @strMensaje ='El C�digo de Identificaci�n de Pago est� cancelada.'
				END
		ELSE IF (@intIdEstado = 21)  --OP EXPIRADA
				BEGIN
				SET @intMensaje = 104
				SET @strMensaje ='El C�digo de Identificaci�n de Pago ha expirado.'
				END
		IF (@intMensaje != 0)
			BEGIN
			SELECT DATOS_TX.RESPUESTA
					,DATOS_TX.MENSAJE
					,CASE WHEN DATOS_TX.RESPUESTA != 106 THEN DATOS_TX.COD_OP
					 END COD_OP
			FROM (SELECT @intMensaje RESPUESTA
						,@strMensaje MENSAJE
						,@strNroOrdenPago COD_OP)DATOS_TX
			FOR XML AUTO, ELEMENTS

			END
		ELSE IF (@intIdEstado = 22) --OP GENERADA LISTA PARA SER CANCELADA
			BEGIN 
			/* CAMBIAR EL ESTADO DE LA C�digo de Identificaci�n de Pago A CANCELADA (23)*/
			UPDATE PagoEfectivo.OrdenPago 
			SET IdEstado = 23
				,FechaCancelacion = GETDATE()
				,FechaActualizacion = GETDATE()
			WHERE NumeroOrdenPago = @strNroOrdenPago
			/* INGRESAMOS UN REGISTRO EN MOVIMIENTO */
			INSERT PagoEfectivo.Movimiento
					(IdOrdenPago
					,IdMoneda
					,IdTipoMovimiento
					,IdMedioPago
					,Monto
					,FechaMovimiento
					,FechaCreacion
					,NumeroOperacion
					,IdAperturaOrigen
					,IdTipoOrigenCancelacion)
			VALUES	(@intIdOrdenPago
					,@intMoneda
					,34			--RECEPCIONAR OP
					,@pintMedioPago
					,@decMonto
					,GETDATE()
					,GETDATE()
					,@pstrNumeroOperacion
					,@intIdAperturaOrigen
					,3 /*ORIGEN: ESTABLECIMIENTO*/)

			/* RESPUESTA DEL SISTEMA */

			SELECT DATOS_TX.RESPUESTA
					,DATOS_TX.MENSAJE
					,DATOS_TX.COD_OP
					,DATOS_TX.MONEDA
					,DATOS_TX.MONTO
			FROM (SELECT 110 RESPUESTA
					,'El pago se realiz� correctamente.' MENSAJE
					,@strNroOrdenPago COD_OP
					,dsmbmnd MONEDA
					,@decMonto MONTO
			FROM PagoEfectivo.Moneda
			WHERE IdMoneda = @intMoneda) DATOS_TX
			FOR XML AUTO, ELEMENTS
			END
		END
		
	ELSE
		BEGIN
		SELECT DATOS_TX.RESPUESTA
					,DATOS_TX.MENSAJE
					,DATOS_TX.COD_OP
		FROM 
			(SELECT	102 RESPUESTA
					,'El C�digo de Identificaci�n de Pago no existe.' MENSAJE
					,@strNroOrdenPago COD_OP) DATOS_TX
		FOR XML AUTO, ELEMENTS
		END
	
	SET NOCOUNT OFF;
END