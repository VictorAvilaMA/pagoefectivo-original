/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Cierre Caja Establecimiento				                */    
/* Creado x       : kfigueroa@3devnet.com - Karen Figueroa                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     :							                                */    
/* Fec.Validacion :			                                                */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_CierreCajaPOS] 
	@pstrNroOperacion       VARCHAR(50),
    @pstrCodigoPOS          VARCHAR(50),  
    @pdecEfectivoSoles      DECIMAL(18,2), 
    @pdecEfectivoDolares    DECIMAL(18,2), 
    @pdecTarjetaSoles       DECIMAL(18,2), 
    @pdecTarjetaDolares     DECIMAL(18,2) 

AS
	BEGIN
		SET NOCOUNT ON;
		DECLARE	@intMensaje				INT
				,@strMensaje			VARCHAR(100)
				,@intIdAperturaOrigen   INT
				,@strCodigoPos          VARCHAR(10)
				,@decEfectivoSoles		DECIMAL(18,2) 
				,@decEfectivoDolares    DECIMAL(18,2) 
				,@decTarjetaSoles       DECIMAL(18,2) 
				,@decTarjetaDolares     DECIMAL(18,2) 
				,@IdEstablecimiento		INT

			
		SET @intMensaje = 0
		SET @intIdAperturaOrigen = (SELECT	IdAperturaOrigen 
									FROM	PagoEfectivo.AperturaOrigen Ao 
											INNER JOIN PagoEfectivo.Establecimiento E 
											ON Ao.IdEstablecimiento = E.IdEstablecimiento
											AND Ao.IdEstado = 81 --Estado de Apertura
											AND Ao.IdTipoOrigenCancelacion = 3 --Establecimiento
									WHERE  E.SerieTerminal=  @pstrCodigoPOS)
		SET @IdEstablecimiento =   (SELECT IdEstablecimiento 
									FROM   PagoEfectivo.Establecimiento
									WHERE  SerieTerminal = @pstrCodigoPOS)

		IF NOT EXISTS  (SELECT  E.IdEstablecimiento
						FROM PagoEfectivo.Establecimiento E 
						WHERE	E.SerieTerminal = @pstrCodigoPOS )
			BEGIN 
				SELECT	@intMensaje = 105;
				SELECT	DATOS_TX.RESPUESTA
						,DATOS_TX.MENSAJE
				FROM	(SELECT 105 RESPUESTA
						,'El establecimiento no se encuentra registrado.' MENSAJE) DATOS_TX
				FOR XML AUTO, ELEMENTS
				RETURN
			END
	
		ELSE IF NOT EXISTS (SELECT	Es.IdEstablecimiento 
							FROM	PagoEfectivo.Establecimiento Es
									INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
									INNER JOIN PagoEfectivo.TipoOrigenCancelacion Toc ON Ao.IdTipoOrigenCancelacion = Toc.IdTipoOrigenCancelacion
							WHERE	Es.SerieTerminal = @pstrCodigoPOS 
									AND Toc.IdTipoOrigenCancelacion = 3
									AND Ao.IdEstado = 81) -- Estado Aperturado

			BEGIN
				SELECT	@intMensaje = 114;
				SELECT	DATOS_TX.RESPUESTA
						,DATOS_TX.MENSAJE
				FROM	(SELECT 114 RESPUESTA
						,'No tiene caja activa el dia de hoy.' MENSAJE) DATOS_TX
				FOR XML AUTO, ELEMENTS
				RETURN
			END


		ELSE IF EXISTS (SELECT	Es.IdEstablecimiento 
						FROM	PagoEfectivo.Establecimiento Es
								INNER JOIN PagoEfectivo.AperturaOrigen Ao ON Es.IdEstablecimiento = Ao.IdEstablecimiento
								INNER JOIN PagoEfectivo.TipoOrigenCancelacion Toc ON Ao.IdTipoOrigenCancelacion = Toc.IdTipoOrigenCancelacion
						WHERE	Es.SerieTerminal =  @pstrCodigoPOS 
								AND Ao.IdEstado = 81 /* 81 = Estado Apertura*/
								AND Toc.IdTipoOrigenCancelacion = 3)
			BEGIN





				--SE VALIDA PARA EFECTIVO SOLES
				-------------------------------
				SET @decEfectivoSoles = ISNULL((SELECT  SUM(M.Monto) AS Monto 
												FROM	PagoEfectivo.Movimiento M 
												WHERE	M.IdAperturaOrigen = @intIdAperturaOrigen AND 
														M.IdMoneda = 1 AND                 /* 1= Soles */
														M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
														M.IdMedioPago = 2 AND              /* 2  = Efectivo*/ 
														M.IdTipoMovimiento <> 32        /* 32 = Cierre */
										   GROUP BY		M.IdMoneda, 
														M.IdMedioPago),-1)

												
				IF (@decEfectivoSoles > 0 OR @pdecEfectivoSoles >0)
					BEGIN	            
				            
						IF @pdecEfectivoSoles = @decEfectivoSoles
		 					BEGIN
								 
								INSERT PagoEfectivo.Movimiento
									(
						 			   [IdMoneda]
									  ,[IdTipoMovimiento]
									  ,[IdMedioPago]
									  ,[FechaMovimiento]
									  ,[Monto]
									  ,[FechaCreacion]
									  ,[IdAperturaOrigen]
									  ,NumeroOperacion
									  ,IdTipoOrigenCancelacion)
								VALUES
									(
									   1    /*Soles*/
									   ,32	/*32 = Cierre */ 
									   ,2     /* 2 = Efectivo */
									   ,GETDATE()
									   ,@pdecEfectivoSoles
									   ,GETDATE()
									   ,@intIdAperturaOrigen
									   ,@pstrNroOperacion
									   ,3 /* 3= Origen Establecimiento */);

								UPDATE PagoEfectivo.OrdenPago
								SET		FechaConciliacion=getdate(),
										IdEstadoConciliacion=171  --- Estado Conciliado
								WHERE	IdEstado = 23 AND --Cancelada
										IdOrdenPago IN (SELECT  M.IdOrdenPago
														FROM	PagoEfectivo.Movimiento M 
														WHERE	M.IdAperturaOrigen = @intIdAperturaOrigen AND 
																M.IdMoneda = 1 AND                 /* 1= Soles */
																M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
																M.IdMedioPago = 2 AND              /* 2  = Efectivo*/ 
																M.IdTipoMovimiento <> 32        /* 32 = Cierre */
														)
								
							END	
						ELSE
							BEGIN
								SELECT	@intMensaje = 118;
	        					SELECT	DATOS_TX.RESPUESTA,
										DATOS_TX.MENSAJE
								FROM	(SELECT 118 RESPUESTA
										,'El medio y monto de pago no coinciden.' MENSAJE) DATOS_TX
								FOR XML AUTO, ELEMENTS
								RETURN
							END
					END	--Fin Begin IF
				




				--SE VALIDA PARA EFECTIVO DOLARES
				---------------------------------
				SET @decEfectivoDolares =	ISNULL((SELECT  SUM(M.Monto) AS Monto 
											FROM PagoEfectivo.Movimiento M
											WHERE  M.IdAperturaOrigen = @intIdAperturaOrigen AND 
												    M.IdMoneda = 2 AND                 /* 2= Dolares */
													M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
													M.IdMedioPago = 2 AND              /* 2  = Efectivo*/ 
													M.IdTipoMovimiento <> 32           /* 32 = Cierre */
										   GROUP BY M.IdMoneda, 
													M.IdMedioPago ),-1)
				
				
				IF (@decEfectivoDolares > 0 OR @pdecEfectivoDolares > 0)
					BEGIN
												     
						IF @pdecEfectivoDolares = @decEfectivoDolares 
		 					BEGIN
 															   
								INSERT INTO PagoEfectivo.Movimiento
									(
									[IdMoneda]
									,[IdTipoMovimiento]
									,[IdMedioPago]
									,[FechaMovimiento]
									,[Monto]
									,[FechaCreacion]
									,[IdAperturaOrigen]
									,NumeroOperacion
									,IdTipoOrigenCancelacion)
								VALUES
									(
									2     /* 2 = Dolares*/
									,32	/*32 = Cierre */ 
									,2     /* 2 = Efectivo */
									,GETDATE()
									,@pdecEfectivoDolares
									,GETDATE()
									,@intIdAperturaOrigen
									,@pstrNroOperacion
									,3 /* 3= Origen Establecimiento */);


								UPDATE	PagoEfectivo.OrdenPago
								SET		FechaConciliacion=getdate(),
										IdEstadoConciliacion=171  --- Estado Conciliado
								WHERE	IdEstado = 23 AND --Cancelada
										IdOrdenPago IN (SELECT  M.IdOrdenPago
														FROM	PagoEfectivo.Movimiento M 
														WHERE	M.IdAperturaOrigen = @intIdAperturaOrigen AND 
																M.IdMoneda = 2 AND                 /* 2= Dolares */
																M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
																M.IdMedioPago = 2 AND              /* 2  = Efectivo*/ 
																M.IdTipoMovimiento <> 32 
													   )     
							END	
						ELSE
							BEGIN
								SELECT	@intMensaje = 118;
	        					SELECT	DATOS_TX.RESPUESTA,
										DATOS_TX.MENSAJE
								FROM	(SELECT 118 RESPUESTA
										,'El medio y monto de pago no coinciden.' MENSAJE) DATOS_TX
								FOR XML AUTO, ELEMENTS
								RETURN
							END			
					END
			





				--SE VALIDA PARA TARJETA SOLES
				------------------------------
				SET @decTarjetaSoles =	ISNULL((SELECT  SUM(M.Monto) AS Monto 
										FROM	PagoEfectivo.Movimiento M 
										WHERE	M.IdAperturaOrigen = @intIdAperturaOrigen AND 
												M.IdMoneda = 1 AND                 /* 1  = Soles */
												M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
												M.IdMedioPago = 3 AND              /* 3  = Tarjeta */ 
												M.IdTipoMovimiento <> 32     /* 32 = Cierre */
										GROUP BY M.IdMoneda, 
												M.IdMedioPago
												),0)

				IF (@decTarjetaSoles > 0 OR @pdecTarjetaSoles >0)
 					BEGIN
						
				            
						IF @pdecTarjetaSoles = @decTarjetaSoles 
		 					BEGIN

								INSERT INTO PagoEfectivo.Movimiento
								(
								 [IdMoneda]
								,[IdTipoMovimiento]
								,[IdMedioPago]
								,[FechaMovimiento]
								,[Monto]
								,[FechaCreacion]
								,[IdAperturaOrigen]
								,NumeroOperacion
								,IdTipoOrigenCancelacion)

								VALUES
								(
								  1     /* 1= Soles*/
								 ,32	/*32 = Cierre */ 
								 ,3     /* 3 = Tarjeta */
								 ,GETDATE()
								 ,@pdecTarjetaSoles
								 ,GETDATE()
								 ,@intIdAperturaOrigen
								 ,@pstrNroOperacion
								 ,3 /* 3= Origen Establecimiento */);

								UPDATE	PagoEfectivo.OrdenPago
								SET		FechaConciliacion=getdate(),
										IdEstadoConciliacion=171  --- Estado Conciliado
								WHERE	IdEstado = 23 AND --Cancelada
										IdOrdenPago IN (SELECT  M.IdOrdenPago
														FROM	PagoEfectivo.Movimiento M 
														WHERE	M.IdAperturaOrigen = @intIdAperturaOrigen AND 
																M.IdMoneda = 1 AND                 /* 1  = Soles */
																M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
																M.IdMedioPago = 3 AND              /* 3  = Tarjeta */		
																M.IdTipoMovimiento <> 32     /* 32 = Cierre */
														)
							
							END	
						ELSE
							BEGIN
								SELECT	@intMensaje = 118;
	        					SELECT	DATOS_TX.RESPUESTA,
										DATOS_TX.MENSAJE
								FROM	(SELECT 118 RESPUESTA
										,'El medio y monto de pago no coinciden.' MENSAJE) DATOS_TX
								FOR XML AUTO, ELEMENTS
								RETURN
							END			
		
					END





				--SE VALIDA PARA TARJETA DOLARES
				------------------------------
				SET @decTarjetaDolares = ISNULL((SELECT  SUM(M.Monto) AS Monto 
										  FROM		PagoEfectivo.Movimiento M 
										  WHERE		M.IdAperturaOrigen = @intIdAperturaOrigen AND 
													M.IdMoneda = 2 AND                 /* 2= Dolares */
													M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
													M.IdMedioPago = 3 AND              /* 3  = Tarjeta */ 
													M.IdTipoMovimiento <> 32         /* 32 = Cierre */
											GROUP BY	M.IdMoneda, 
												   M.IdMedioPago),0)

				IF (@decTarjetaDolares > 0 OR @pdecTarjetaDolares > 0)
					BEGIN
						
						IF @pdecTarjetaDolares = @decTarjetaDolares 
		 					BEGIN
					
								INSERT INTO PagoEfectivo.Movimiento
									(
									[IdMoneda]
									,[IdTipoMovimiento]
									,[IdMedioPago]
									,[FechaMovimiento]
									,[Monto]
									,[FechaCreacion]
									,[IdAperturaOrigen]
									,NumeroOperacion
									,IdTipoOrigenCancelacion)
								VALUES
									(
									  2     /* 2= Dolares*/
									 ,32	/*32 = Cierre */ 
									 ,3     /* 3 = Tarjeta */
									 ,GETDATE()
									 ,@pdecTarjetaDolares
									 ,GETDATE()
									 ,@intIdAperturaOrigen
									 ,@pstrNroOperacion
									 ,3 /* 3= Origen Establecimiento */);

								UPDATE	PagoEfectivo.OrdenPago
								SET		FechaConciliacion=getdate(),
										IdEstadoConciliacion=171  --- Estado Conciliado
								WHERE	IdEstado = 23 AND --Cancelada
										IdOrdenPago IN (SELECT	M.IdOrdenPago
														FROM	PagoEfectivo.Movimiento M 
														WHERE	M.IdAperturaOrigen = @intIdAperturaOrigen AND 
																M.IdMoneda = 2 AND                 /* 2= Dolares */
																M.IdTipoOrigenCancelacion = 3 AND  /* 3  = Establecimiento */ 
																M.IdMedioPago = 3 AND              /* 3  = Tarjeta */ 
																M.IdTipoMovimiento <> 32         /* 32 = Cierre */
														)
							END				
						ELSE
							BEGIN
								SELECT	@intMensaje = 118;
	        					SELECT	DATOS_TX.RESPUESTA,
										DATOS_TX.MENSAJE
								FROM	(SELECT 118 RESPUESTA
										,'El medio y monto de pago no coinciden.' MENSAJE) DATOS_TX
								FOR XML AUTO, ELEMENTS
								RETURN
							END		
					END


			END


		IF (@intMensaje = 0)
			BEGIN
				UPDATE	PagoEfectivo.AperturaOrigen 
				SET		FechaCierre=GETDATE(),
						IdEstado=82   /*82= Estado Cierre*/
				WHERE	IdAperturaOrigen =@intIdAperturaOrigen

				SELECT	DATOS_TX.RESPUESTA
						,DATOS_TX.MENSAJE
						,DATOS_TX.COD_POS
				FROM (SELECT 117 RESPUESTA
					,'El cierre se ha realizado correctamente.' MENSAJE
					,@pstrCodigoPOS COD_POS)
					DATOS_TX
					FOR XML AUTO, ELEMENTS
			END
		
	SET NOCOUNT OFF;
 END





