/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de Agencias Bancarias							*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 22/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarAgenciaBancaria] --'','999901956',0
	-- Add the parameters for the stored procedure here
	@pstrDescripcion	VARCHAR(100),
	@pstrCodigo			VARCHAR(50),
	@pintIdEstado			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		
	SELECT	AB.IdAgenciaBancaria,
			AB.IdBanco,
			AB.Codigo,
			AB.Descripcion,
			AB.Direccion,
			AB.IdEstado,
			AB.IdUsuarioCreacion,
			AB.FechaCreacion,
			ISNULL(AB.IdUsuarioActualizacion,0) IdUsuarioActualizacion,
			ISNULL(AB.FechaActualizacion,'') FechaActualizacion,
			ISNULL(AB.IdCiudad,0) IdCiudad,
			0 IdDepartamento, --CI.cdptest IdDepartamento,
			0 IdPais --CI.cpai000 IdPais
		    ,ES.Descripcion AS  DescripcionEstado

	FROM	PagoEfectivo.AgenciaBancaria AB
			--INNER JOIN PagoEfectivo.Ciudad CI ON Ab.IdCiudad = CI.IdCiudad
			INNER JOIN PagoEfectivo.Parametro ES ON AB.IdEstado = ES.Id		

	WHERE	(AB.Descripcion LIKE @pstrDescripcion + '%' OR '' = @pstrDescripcion)
			AND (AB.Codigo LIKE @pstrCodigo + '%' OR '' = @pstrCodigo)
			AND (AB.IdEstado = @pintIdEstado OR 0 = @pintIdEstado)
	ORDER BY AB.Descripcion ASC

	SET NOCOUNT OFF;

END

