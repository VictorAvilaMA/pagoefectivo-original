/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de Agencias Bancarias x Codigo					*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 26/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarAgenciaBancariaPorCodigo] --'999901956'
	-- Add the parameters for the stored procedure here
	@pstrCodigo	VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		
	SELECT	AB.IdAgenciaBancaria,
			AB.IdBanco,
			AB.Codigo,
			AB.Descripcion,
			AB.Direccion,
			AB.IdEstado,
			ISNULL(AB.IdUsuarioCreacion,0) IdUsuarioCreacion,
			AB.FechaCreacion,
			ISNULL(AB.IdUsuarioActualizacion,0) IdUsuarioActualizacion,
			AB.FechaActualizacion,
			ISNULL(AB.IdCiudad,0) IdCiudad
			
	FROM	PagoEfectivo.AgenciaBancaria AB
	WHERE	Codigo = @pstrCodigo
	ORDER BY AB.Descripcion ASC

	SET NOCOUNT OFF;

END

