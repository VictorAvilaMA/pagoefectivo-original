/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de Agencias Bancarias x Id						*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     :	cmiranda@3devnet.com - Cesar Miranda														*/    
/* Fec.Validacion :	08/01/2008														*/    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarAgenciaBancariaxId] --13
	-- Add the parameters for the stored procedure here
	@pintIdAgenciaBancaria	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@vintIdCiudad INT

			SELECT	AB.IdAgenciaBancaria,
					AB.IdBanco,
					AB.Codigo,
					AB.Descripcion,
					AB.Direccion,
					AB.IdEstado,
					AB.IdUsuarioCreacion,
					AB.FechaCreacion,
					ISNULL(AB.IdUsuarioActualizacion,0) IdUsuarioActualizacion,
					ISNULL(AB.FechaActualizacion,'') FechaActualizacion,
					ISNULL(AB.IdCiudad,0) IdCiudad ,
					ISNULL(CI.cdptest,0) IdDepartamento,
					ISNULL(CI.cpai000,0) IdPais,
					ES.Descripcion AS  DescripcionEstado

			FROM	PagoEfectivo.AgenciaBancaria AB
					LEFT JOIN PagoEfectivo.Ciudad CI ON Ab.IdCiudad = CI.IdCiudad
					INNER JOIN PagoEfectivo.Parametro ES ON AB.IdEstado = ES.Id		
			WHERE	IdAgenciaBancaria = @pintIdAgenciaBancaria
			ORDER BY AB.Descripcion ASC

	SET NOCOUNT OFF;

END
