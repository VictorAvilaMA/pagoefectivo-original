/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de bancos							*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarBanco] --'',0
	-- Add the parameters for the stored procedure here
	@pstrDescripcion	VARCHAR(100),
	@pstrCodigo			VARCHAR(50),
	@pintIdEstado			INT
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT	B.IdBanco,
			B.Codigo,
			B.Descripcion,
			B.idEstado,
			B.idUsuarioCreacion,
			B.IdUsuarioActualizacion,
			ES.Descripcion	
		
	FROM	PagoEfectivo.Banco B INNER JOIN PagoEfectivo.Parametro ES ON B.IdEstado = ES.Id		

	WHERE	(B.Descripcion LIKE @pstrDescripcion + '%' OR '' = @pstrDescripcion)
			 AND (B.IdEstado = @pintIdEstado OR 0 = @pintIdEstado)
	ORDER BY B.Descripcion ASC

	SET NOCOUNT OFF;

END

