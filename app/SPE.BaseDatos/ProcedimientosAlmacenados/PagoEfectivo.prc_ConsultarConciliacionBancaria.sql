/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar Conciliacion Bancaria */    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan                                */    
/* Fecha		  : 12/01/2009                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 12/01/2009                                             */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarConciliacionBancaria] 
@pintIdConciliacion		INT,
@pstrNumeroOperacion	VARCHAR(14),
@pstrNumeroOrdenPago	VARCHAR(14),
@pintIdEstado			INT,
@pdatDel				DATETIME,
@pdatAl					DATETIME
AS

SELECT 
OB.NumeroOperacion,
OP.NumeroOrdenPago,
OP.FechaConciliacion,
P.Descripcion,
ISNULL(OB.Observacion,'') Observacion

FROM PagoEfectivo.OrdenPago OP
INNER JOIN PagoEfectivo.OperacionBancaria OB
ON CodigoDepositante=NumeroOrdenPago
INNER JOIN PagoEfectivo.Parametro P
ON OP.IdEstadoConciliacion = P.Id
WHERE (FechaConciliacion IS NOT NULL) 
AND IdEstadoConciliacion IS NOT NULL
AND (OP.FechaConciliacion >= CONVERT(DATETIME,@pdatDel,110)) AND (OP.FechaConciliacion < DATEADD(DD,1,CONVERT(DATETIME,@pdatAl,110)))
AND (OB.NumeroOperacion = RIGHT('000000'+RTRIM(@pstrNumeroOperacion),6) OR RIGHT('000000'+RTRIM(@pstrNumeroOperacion),6)='000000')
AND (OP.NumeroOrdenPago=RIGHT('00000000000000'+RTRIM(@pstrNumeroOrdenPago),14) OR RIGHT('00000000000000'+RTRIM(@pstrNumeroOrdenPago),14)='00000000000000')
AND (OP.IdEstadoConciliacion=@pintIdEstado OR @pintIdEstado=0)
AND (OB.IdConciliacion=ISNULL(@pintIdConciliacion,0) OR ISNULL(@pintIdConciliacion,0)=0)



