
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la consulta de Establecimiento por IdEstablecimiento */    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha		  : 28/12/2008                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarEstablecimientoPorIdEstablecimiento] 
	
	@pintIdEstablecimiento			INT

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT Es.IdEstablecimiento IdEstablecimiento
			,Es.SerieTerminal SerieTerminal
			,Es.RazonSocial RazonSocial
			,Es.RUC	RUC 
			,Es.Contacto Contacto
			,Es.Telefono Telefono
			,Es.Direccion Direccion
			,Es.IdOperador IdOperador
			,Ci.IdCiudad IdCiudad
			,Ci.dciudis Ciudad
			,Ci.cdptest IdDepartamento
			,Ci.cpai000	IdPais
			,Es.IdEstado IdEstado
			,Pa.Descripcion DescripcionEstado
	FROM PagoEfectivo.Establecimiento Es
	INNER JOIN PagoEfectivo.Ciudad Ci ON Es.IdCiudad = Ci.IdCiudad
	INNER JOIN PagoEfectivo.Parametro Pa ON Es.IdEstado = Pa.Id
	WHERE Es.IdEstablecimiento = @pintIdEstablecimiento

	SET NOCOUNT OFF;

END
