/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la consulta de Establecimientos */    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha		  : 04/11/2008                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarEstablecimientos] 
	@pstrNroEstablecimiento				VARCHAR(50),
	@pstrRazonSocial					VARCHAR(200),
	@pstrRUC							CHAR(11),
	@pstrContacto						VARCHAR(200),
	@pintIdOperador						INT,
	@pintIdEstado						INT

AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SELECT Es.IdEstablecimiento IdEstablecimiento
			,Es.SerieTerminal SerieTerminal
			,Es.RazonSocial RazonSocial
			,Es.RUC	RUC 
			,Es.Contacto Contacto
			,Es.Telefono Telefono
			,Es.Direccion Direccion
			,Es.IdOperador IdOperador
			,Ci.IdCiudad IdCiudad
			,Ci.dciudis Ciudad
			,Ci.cdptest IdDepartamento
			,Ci.cpai000	IdPais
			,Es.IdEstado IdEstado
			,Pa.Descripcion DescripcionEstado
	FROM PagoEfectivo.Establecimiento Es
	INNER JOIN PagoEfectivo.Ciudad Ci ON Es.IdCiudad = Ci.IdCiudad
	INNER JOIN PagoEfectivo.Parametro Pa ON Es.IdEstado = Pa.Id
	WHERE (Es.SerieTerminal LIKE @pstrNroEstablecimiento + '%' OR '' = @pstrNroEstablecimiento)
		AND	(Es.Contacto LIKE @pstrContacto + '%' OR '' = @pstrContacto)
		AND (Es.RazonSocial LIKE @pstrRazonSocial + '%' OR ''= @pstrRazonSocial)
		AND (Es.RUC LIKE @pstrRUC + '%' OR ''= @pstrRUC)
		AND(Es.IdOperador = @pintIdOperador OR 0 = @pintIdOperador)
		AND (Es.IdEstado = @pintIdEstado  OR 0 = @pintIdEstado)

	SET NOCOUNT OFF;

END
