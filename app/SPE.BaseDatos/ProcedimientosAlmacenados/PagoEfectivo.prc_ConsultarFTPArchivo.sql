/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar archivo FTP									*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 06/01/2009												*/    
/* Validado x     :															*/    
/* Fec.Validacion :															*/    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarFTPArchivo] --1,'','1/7/2009','1/7/2009',221,0
	-- Add the parameters for the stored procedure here
	@pintIdServicio		int,
	@pstrNombreArchivo	varchar(50),
	@pdtmFechaDe		datetime,
	@pdtmFechaA			datetime,
	@pintIdTipoArchivo	int,
	@pintIdEstado		int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		SELECT	AR.IdFtpArchivo
				,S.Nombre Servicio
				,S.IdServicio
				,AR.IdTipoArchivo
				,AR.NombreArchivo
				,AR.FechaGenerado
				,P.Descripcion TipoArchivo
				,AR.IdEstado
				,PA.Descripcion DescripcionEstado

		FROM	PagoEfectivo.FTPArchivo AR INNER JOIN PagoEfectivo.Servicio S ON
				AR.IdServicio = S.IdServicio INNER JOIN PagoEfectivo.Parametro P ON 
				AR.IdTipoArchivo = P.Id INNER JOIN (SELECT Id, Descripcion FROM PagoEfectivo.Parametro
				WHERE GrupoCodigo = 'ESAG') PA ON AR.IdEstado = PA.Id

		WHERE	(AR.IdServicio = @pintIdServicio OR 0 = @pintIdServicio)AND
				(AR.NombreArchivo LIKE @pstrNombreArchivo + '%' OR '' = @pstrNombreArchivo) AND

				AR.FechaGenerado >= CONVERT(DATETIME,@pdtmFechaDe,110) AND 
				AR.FechaGenerado < DATEADD(dd,1,CONVERT(DATETIME,@pdtmFechaA,110)) AND

				(AR.IdTipoArchivo = @pintIdTipoArchivo OR 0 = @pintIdTipoArchivo) AND
				(AR.IdEstado = @pintIdEstado OR 0 = @pintIdEstado)


	SET NOCOUNT OFF;

END
