/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                           */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar FTP Archivo Cabecera*/    
/*              */
/* Creado x       : Cesar Miranda                                 */    
/* Fecha    : 13/01/2009                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 13/01/2009                                             */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarFTPArchivoCabecera]
(

@IdServicio INT
)
AS
BEGIN
 SET NOCOUNT ON;

  SELECT	AR.IdFtpArchivo
				,S.Nombre Servicio
				,S.IdServicio
				,AR.IdTipoArchivo
				,AR.NombreArchivo
				,AR.FechaGenerado
				,P.Descripcion TipoArchivo
				,AR.IdEstado
				,PA.Descripcion DescripcionEstado
  FROM	PagoEfectivo.FTPArchivo AR 
  INNER JOIN PagoEfectivo.Servicio S ON AR.IdServicio = S.IdServicio 
  INNER JOIN PagoEfectivo.Parametro P ON AR.IdTipoArchivo = P.Id 
  INNER JOIN (SELECT Id, Descripcion FROM PagoEfectivo.Parametro WHERE GrupoCodigo = 'ESAG') PA ON AR.IdEstado = PA.Id
 WHERE  AR.IdEstado=232 AND AR.IdServicio=@IdServicio
  --- Consulta los archivos no generados y del tipo seleccionado vIdTipoArchivo=@IdTipoArchivo 



 SET NOCOUNT OFF;
END



GO