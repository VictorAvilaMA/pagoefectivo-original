/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar detalle archivo FTP por idFTPArchivo			*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 08/01/2009												*/    
/* Validado x     :															*/    
/* Fec.Validacion :															*/    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarFTPArchivoDetallePorIdFTPArchivo] --2
	-- Add the parameters for the stored procedure here
	@pintIdFtpArchivo	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT	DA.IdFtpArchivoDetalle
				,DA.IdFtpArchivo
				,DA.idUsuario
				,DA.OrdenIdComercio
				,DA.tipoDePago
				,DA.fechaPago
				,DA.IdMoneda
				,MO.dsmbmnd Moneda
				,Monto
				
		FROM	PagoEfectivo.FTPArchivoDetalle DA INNER JOIN PagoEfectivo.Moneda MO ON
				DA.IdMoneda = MO.IdMoneda
		WHERE	IdFtpArchivo = @pintIdFtpArchivo

	SET NOCOUNT OFF;

END

