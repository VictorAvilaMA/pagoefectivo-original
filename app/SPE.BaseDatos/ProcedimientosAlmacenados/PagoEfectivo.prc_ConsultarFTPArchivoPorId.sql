/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar archivo FTP por Id							*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 06/01/2009												*/    
/* Validado x     :															*/    
/* Fec.Validacion :															*/    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarFTPArchivoPorId] --2
	-- Add the parameters for the stored procedure here
	@pintIdFtpArchivo	int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT	AR.IdFtpArchivo
				,S.Nombre Servicio
				,S.IdServicio
				,AR.IdTipoArchivo
				,AR.NombreArchivo
				,AR.FechaGenerado
				,P.Descripcion TipoArchivo
				,AR.IdEstado
				,PA.Descripcion DescripcionEstado

		FROM	PagoEfectivo.FTPArchivo AR INNER JOIN PagoEfectivo.Servicio S ON
				AR.IdServicio = S.IdServicio INNER JOIN PagoEfectivo.Parametro P ON 
				AR.IdTipoArchivo = P.Id INNER JOIN (SELECT Id, Descripcion FROM PagoEfectivo.Parametro
				WHERE GrupoCodigo = 'ESAG') PA ON AR.IdEstado = PA.Id

		WHERE	IdFtpArchivo = @pintIdFtpArchivo

	SET NOCOUNT OFF;

END

