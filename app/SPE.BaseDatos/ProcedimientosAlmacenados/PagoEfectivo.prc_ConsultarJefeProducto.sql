/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 		Consultar de Jefe de Producto*/    
/*             */
/* Creado x       :cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha		  :  06/01/2009                                      */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarJefeProducto] 
	@pstrNombres	VARCHAR(100),
	@pstrApellidos	VARCHAR(100),
	@pintIdEstado	INT
AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT ISNULL(JP.IdJefeProducto,0) IdJefeProducto
		,ISNULL(US.IdUsuario,0) IdUsuario
		,US.Nombres
		,US.Apellidos
		,US.IdTipoDocumento
		,US.NumeroDocumento
		,US.Direccion
		,US.Telefono
		,US.Email
		,US.Telefono
		,US.Direccion
		,ISNULL(US.IdCiudad,0) IdCiudad
		,ISNULL(CI.cdptest,0) IdDepartamento
		,ISNULL(CI.cpai000,0) IdPais
		,ISNULL(US.IdEstado,0) IdEstado
		,PA.Descripcion Estado
	FROM PagoEfectivo.JefeProducto JP
	INNER JOIN PagoEfectivo.Usuario US ON JP.IdUsuario = US.IdUsuario
	INNER JOIN PagoEfectivo.Ciudad CI ON US.IdCiudad = CI.IdCiudad
	INNER JOIN PagoEfectivo.Parametro PA ON US.IdEstado = PA.Id
	WHERE (US.Nombres LIKE @pstrNombreS + '%' OR ''= @pstrNombreS)
		AND (US.Apellidos LIKE @pstrApellidos + '%' OR '' = @pstrApellidos)
		AND (US.IdEstado = @pintIdEstado OR 0 = @pintIdEstado)
	
	SET NOCOUNT OFF;
END
