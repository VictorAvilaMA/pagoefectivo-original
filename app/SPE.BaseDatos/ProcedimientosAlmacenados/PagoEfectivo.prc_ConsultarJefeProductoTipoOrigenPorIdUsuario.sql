/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar de JefeProductoTipoOrigenrigen por IdJefeProducto*/    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha		  : 06/01/2009                                      */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarJefeProductoTipoOrigenPorIdUsuario]
	
	@pintIdUsuario		INT

AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT TOC.IdTipoOrigenCancelacion
			,TOC.Descripcion
	FROM PagoEfectivo.JefeProductoTipoOrigen JTO
	INNER JOIN PagoEfectivo.JefeProducto JP ON JTO.IdJefeProducto = JP.IdJefeProducto
	INNER JOIN PagoEfectivo.Usuario US ON JP.IdUsuario = US.IdUsuario 
				AND US.IdUsuario = @pintIdUsuario
	INNER JOIN PagoEfectivo.TipoOrigenCancelacion TOC ON JTO.IdTipoOrigenCancelacion = TOC.IdTipoOrigenCancelacion
	
	SET NOCOUNT OFF;
END
