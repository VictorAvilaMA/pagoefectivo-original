/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta los elementos del menu (Site Map) */    
/*             */
/* Creado x       :  cmiranda@3devnet.com -  Cesar Miranda                                */    
/* Fecha    : 10/01/2009                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 10/01/2009                                             */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarMenu] 
(
@idSistema int
)
AS
BEGIN
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


	SELECT DISTINCT P.IdPagina AS id,
       P.Titulo AS Title,
       P.Descripcion AS Descripcion,
       P.URL,
       PagoEfectivo.fn_ListarRoles(P.IdPagina) AS Roles,
       P.Parent,
       P.NroOrden,
       (SELECT Titulo FROM PagoEfectivo.Pagina
        WHERE IdPagina =P.Parent ) AS ParentName,
        SRP.IdSistema
	FROM PagoEfectivo.Pagina P 
	JOIN PagoEfectivo.SistemaXRolXPagina SRP ON P.IdPagina =SRP.idPagina
	WHERE P.Visible =1 AND P.Estado =1 and SRP.IdSistema=@idSistema
	ORDER BY P.NroOrden;

	SET NOCOUNT OFF;

END
