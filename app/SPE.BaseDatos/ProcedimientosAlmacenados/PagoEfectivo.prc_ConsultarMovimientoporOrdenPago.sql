/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de Movimiento por Código de Identificación de Pago cancelada		*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarMovimientoporOrdenPago] --'14',34
	-- Add the parameters for the stored procedure here
	@pstrNumeroOrdenPago	VARCHAR(14),
	@pintIdTipoMovimiento	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @vstrNuemroOrdenP varchar(14)
	SET		@vstrNuemroOrdenP = RIGHT('00000000000000' + @pstrNumeroOrdenPago,14)
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		
	SELECT	M.IdMovimiento,
			M.IdOrdenPago,
			ISNULL(M.IdAgenteCaja,0),
			M.IdMoneda,
			M.IdTipoMovimiento,
			M.IdMedioPago,
			M.Monto,
			M.FechaMovimiento,
			M.FechaCreacion,
			M.IdUsuarioCreacion,
			ISNULL(M.FechaActualizacion,GetDate()) FechaActualizacion,
			ISNULL(M.IdUsuarioActualizacion,0) IdUsuarioActualizacion,
			ISNULL(M.NumeroOperacion,'') NumeroOperacion,
			ISNULL(M.IdAperturaOrigen,0) IdAperturaOrigen

	FROM	PagoEfectivo.Movimiento M INNER JOIN PagoEfectivo.OrdenPago OP ON
			M.IdOrdenPago = OP.IdOrdenPago
	WHERE	OP.NumeroOrdenPago = @vstrNuemroOrdenP AND
			IdTipoMovimiento = @pintIdTipoMovimiento

	SET NOCOUNT OFF;

END

