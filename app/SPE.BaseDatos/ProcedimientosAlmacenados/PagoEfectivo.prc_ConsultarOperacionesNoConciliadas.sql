/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo Fase II                                   */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar operaciones no conciliadas*/    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan Quispe                                */    
/* Fecha		  : 10/01/2009                                    */    
/* Validado x     :                                 */    
/* Fec.Validacion :                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarOperacionesNoConciliadas]
AS
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT 
AB.Codigo,
M.NumeroOperacion,
OP.NumeroOrdenPago,
CONVERT(DATETIME,OP.FechaCancelacion) FechaPago
FROM  PagoEfectivo.OrdenPago OP
inner join PagoEfectivo.Servicio S ON OP.IdServicio=S.IdServicio
INNER JOIN PagoEfectivo.ServicioTipoOrigenCancelacion STOC ON STOC.IdServicio=S.IdServicio
INNER JOIN PagoEfectivo.Movimiento M ON M.IdOrdenPago = OP.IdOrdenPago
INNER JOIN PagoEfectivo.AperturaOrigen AO ON AO.IdAperturaOrigen = m.IdAperturaOrigen
INNER JOIN PagoEfectivo.AgenciaBancaria AB ON AO.IdAgenciaBancaria = AB.IdAgenciaBancaria
INNER JOIN PagoEfectivo.TipoOrigenCancelacion TOC ON TOC.IdTipoOrigenCancelacion = STOC.IdTipoOrigenCancelacion
where OP.IdEstado = 23
AND IdEstadoConciliacion IS NULL
and M.IdAperturaOrigen IS NOT NULL
and M.NumeroOperacion is not null
AND TOC.IdTipoOrigenCancelacion = 1


SET NOCOUNT OFF;


