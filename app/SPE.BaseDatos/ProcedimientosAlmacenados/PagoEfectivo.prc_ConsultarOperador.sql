/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                           */    
/* Sistema        : Pago Efectivo                                           */    
/* Modulo         : WEB                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de un Operador                      */    
/*                */
/* Creado x       : cmiranda@3devnet.com - Cesar Miranda                  */    
/* Fecha          : 28/12/2008                                              */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda              */    
/* Fec.Validacion : 28/12/2008                                              */    
/*--------------------------------------------------------------------------*/


CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarOperador]
(	@pstrCodigo		VARCHAR(50),
	@pstrDescripcion		VARCHAR(50),
	@pintEstado		INT
)
AS
    
  BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
  	  SELECT OP.IdOperador
			,OP.Codigo
			,OP.Descripcion
			,OP.IdEstado
			,P.Descripcion AS DescripcionEstado
	  FROM PagoEfectivo.Operador OP 
	  LEFT JOIN PagoEfectivo.Parametro P ON (P.Id=OP.IdEstado AND P.GrupoCodigo='ESMA')
	  WHERE 
   	      (@pstrCodigo='' OR OP.Codigo like @pstrCodigo + '%') AND
   	      (@pstrDescripcion='' OR OP.Descripcion LIKE @pstrDescripcion + '%') AND
  	      (@pintEstado=0 OR OP.IdEstado = @pintEstado );

	SET NOCOUNT OFF;
	
END

