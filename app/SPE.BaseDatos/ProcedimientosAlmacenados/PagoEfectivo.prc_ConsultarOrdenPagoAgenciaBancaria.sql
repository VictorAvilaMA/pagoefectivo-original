/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo Fase II                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de Código de Identificación de Pago Agencias Bancarias*/    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan                                */    
/* Fecha		  :	27/12/2008                                            */    
/* Validado x     :													 */    
/* Fec.Validacion :				                                          */    
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarOrdenPagoAgenciaBancaria] 
	@pstrNumeroOrdenPago	VARCHAR(14),
	@pintIdMoneda			INT
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @IdEstado INT
	SELECT @IdEstado = (22)
	DECLARE @IdTipoOC INT 
	DECLARE @IdEstadoMantenimiento INT
	SELECT @IdEstadoMantenimiento = (41)
	DECLARE @IdMoneda INT

	SELECT @IdTipoOC = (1)
	SELECT  DOP.ConceptoPago
			,EC.RazonSocial as DescripcionEmpresa
			,S.TiempoExpiracion
			,OP.FechaEmision
			,ISNULL(dateadd(hour,S.TiempoExpiracion,OP.FechaEmision),GETDATE()) AS FechaVencimiento
			,OP.NumeroOrdenPago as NumeroOrdenPago
			,OP.Total AS Total
			
			
	FROM PagoEfectivo.OrdenPago OP
	INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
	INNER JOIN PagoEfectivo.ServicioTipoOrigenCancelacion STOC ON STOC.IdServicio = S.IdServicio
	INNER JOIN PagoEfectivo.EmpresaContratante EC ON EC.IdEmpresaContratante = S.IdEmpresaContratante
	INNER JOIN PagoEfectivo.Cliente C ON C.IdCliente = OP.IdCliente
	INNER JOIN PagoEfectivo.DetalleOrdenPago DOP ON DOP.IdOrdenPago = OP.IdOrdenPago
	WHERE 
	(OP.NumeroOrdenPago=right('00000000000000' + rtrim(@pstrNumeroOrdenPago),14))
	AND OP.IdEstado = @IdEstado
	AND STOC.IdTipoOrigenCancelacion = @IdTipoOC
	AND OP.IdMoneda = (select IdMoneda from PagoEfectivo.Moneda where EquivalenciaBancaria = @pintIdMoneda)
	ORDER BY OP.NumeroOrdenPago desc


	SET NOCOUNT OFF;


END
