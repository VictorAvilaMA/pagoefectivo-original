/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registro del Establecimiento */    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva          */    
/* Fecha		  : 22/12/2008                                            */    
/* Validado x     :                                 */    
/* Fec.Validacion :                                            */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarOrdenPagoPOS] 
	@pstrNroEstablecimiento VARCHAR(50),	
	@pstrNumeroOP			CHAR(14)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @strNroOrdenPago	CHAR(14)
	SET @strNroOrdenPago = RIGHT('00000000000000'+ RTRIM(CONVERT(VARCHAR(14),@pstrNumeroOP)),14)
	
	IF NOT EXISTS (SELECT Es.IdEstablecimiento FROM PagoEfectivo.Establecimiento Es
					WHERE Es.SerieTerminal = @pstrNroEstablecimiento)
	BEGIN
		SELECT DATOS_TX.RESPUESTA
					,DATOS_TX.MENSAJE
			FROM 
				(SELECT	105 RESPUESTA
						,'El establecimiento no est� registrado.' MENSAJE) DATOS_TX
			FOR XML AUTO, ELEMENTS
		RETURN
	END

	
	IF EXISTS(SELECT NumeroOrdenPago 
				FROM PagoEfectivo.OrdenPago
				WHERE NumeroOrdenPago = @strNroOrdenPago)
		BEGIN
			
			IF NOT EXISTS (SELECT OP.IdOrdenPago FROM PagoEfectivo.OrdenPago OP
					INNER JOIN PagoEfectivo.Servicio SE ON OP.IdServicio = SE.IdServicio
					INNER JOIN PagoEfectivo.ServicioTipoOrigenCancelacion SEOC ON SE.IdServicio = SEOC.IdServicio
					INNER JOIN PagoEfectivo.TipoOrigenCancelacion TIOC ON SEOC.IdTipoOrigenCancelacion = TIOC.IdTipoOrigenCancelacion
					AND TIOC.IdTipoOrigenCancelacion =3 --Establecimiento
					WHERE OP.NumeroOrdenPago = @strNroOrdenPago)
			BEGIN 
					SELECT DATOS_TX.RESPUESTA
								,DATOS_TX.MENSAJE
					FROM 
							(SELECT	106 RESPUESTA
									,'El establecimiento no esta autorizado para realizar la operaci�n.' MENSAJE) DATOS_TX
					FOR XML AUTO, ELEMENTS
					RETURN
			END

			SELECT DATOS_TX.RESPUESTA
					,DATOS_TX.MENSAJE
					,DATOS_TX.COD_OP
					,DATOS_TX.MONEDA
					,DATOS_TX.MONTO
			FROM 
					(SELECT DATOS_TX.IdOrdenPago
							,CASE WHEN DATOS_TX.IdEstado = 21 THEN 104 
								  WHEN DATOS_TX.IdEstado = 22 THEN 101
								  WHEN DATOS_TX.IdEstado = 23 THEN 103
							END RESPUESTA
							,CASE WHEN DATOS_TX.IdEstado = 21 THEN 'El C�digo de Identificaci�n de Pago ha expirado.'
								  WHEN DATOS_TX.IdEstado = 22 THEN 'Existe el C�digo de Identificaci�n de Pago' 
								  WHEN DATOS_TX.IdEstado = 23 THEN 'El C�digo de Identificaci�n de Pago esta cancelada'
							END MENSAJE
							,DATOS_TX.NumeroOrdenPago COD_OP
							,CASE WHEN DATOS_TX.IdEstado != 21 THEN M.dsmbmnd   END MONEDA
							,CASE WHEN DATOS_TX.IdEstado != 21 THEN  DATOS_TX.Total  END MONTO
					FROM	PagoEfectivo.OrdenPago DATOS_TX 
							INNER JOIN PagoEfectivo.Parametro P ON DATOS_TX.IdEstado = P.Id 
							INNER JOIN PagoEfectivo.Moneda M ON DATOS_TX.IdMoneda = M.IdMoneda
							
					WHERE	DATOS_TX.NumeroOrdenPago = @strNroOrdenPago)DATOS_TX
			FOR XML AUTO, ELEMENTS
			
		END
	ELSE 
		BEGIN
			
			SELECT DATOS_TX.RESPUESTA
					,DATOS_TX.MENSAJE
					,DATOS_TX.COD_OP
			FROM 
				(SELECT	102 RESPUESTA
						,'No existe el C�digo de Identificaci�n de Pago' MENSAJE
						,@strNroOrdenPago COD_OP) DATOS_TX
			FOR XML AUTO, ELEMENTS
			
		END
		
	SET NOCOUNT OFF;
END
