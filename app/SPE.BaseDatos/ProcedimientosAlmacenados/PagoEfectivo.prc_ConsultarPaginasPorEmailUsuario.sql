/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta los elementos del menu por usuario */    
/*             */
/* Creado x       :  cmiranda@3devnet.com -  Cesar Miranda                                */    
/* Fecha    : 10/01/2009                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 10/01/2009                                             */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarPaginasPorEmailUsuario] --'clvillanueva@3devnet.com'
(
@email varchar(100)
)
AS	
BEGIN
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


SELECT P.IdPagina, P.Titulo , P.URL, R.Description AS Rol,UR.IdRol ,UR.IdUsuario 
FROM PagoEfectivo.Usuarioxrol UR
JOIN PagoEfectivo.Rol R ON UR.IdRol =R.IdRol 
JOIN PagoEfectivo.SistemaXRolXPagina SRP ON UR.IdRol=SRP.idRol 
JOIN PagoEfectivo.Pagina P ON SRP.idPagina =P.IdPagina 
JOIN PagoEfectivo.Usuario U ON UR.IdUsuario=U.IdUsuario
	WHERE U.email=@email AND 
	     ( (R.IdRol=6 AND(SELECT count(*) FROM PagoEfectivo.TipoOrigenCancelacionPagina TOCP
                          JOIN PagoEfectivo.JefeProductoTipoOrigen JPTO ON TOCP.IdTipoOrigenCancelacion =JPTO.IdTipoOrigenCancelacion
                          JOIN PagoEfectivo.JefeProducto JP ON JPTO.IdJefeProducto =JP.IdJefeProducto
                          JOIN PagoEfectivo.Usuario U ON JP.IdUsuario=U.IdUsuario 
                          WHERE U.Email =@email AND TOCP.IdPagina=SRP.IdPagina)>0
            ) OR R.IdRol<>6 );                
         

	SET NOCOUNT OFF;

END
GO