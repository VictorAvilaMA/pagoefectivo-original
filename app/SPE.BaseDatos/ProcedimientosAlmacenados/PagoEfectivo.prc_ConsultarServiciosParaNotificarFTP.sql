GO

/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la consulta de Servicios que notifican mediante el tipo de notificacion FTP */    
/* Creado x       : cmiranda - Cesar Miranda                               */    
/* Fecha    : 10/01/2009                                           */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 10/01/2009                                           */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarServiciosParaNotificarFTP]

AS
BEGIN
 SET NOCOUNT ON;

	SELECT 
		S.IdServicio,
		S.IdEmpresaContratante,
		EP.RazonSocial  AS NombreEmpresaContrante,
		S.Nombre,
		S.Url,
		S.IdEstado,
		S.TiempoExpiracion,
		ISNULL(S.LogoImagen,'') LogoImagen,
		S.FechaCreacion,
		S.IdUsuarioCreacion
		,ISNULL(S.IdUsuarioActualizacion,0) AS IdUsuarioActualizacion
		,ISNULL(S.FechaActualizacion,'') AS FechaActualizacion
		,S.Url
		,S.IdTipoNotificacion
		,ISNULL(S.UrlFTP,'') UrlFTP
		,ISNULL(S.Usuario,'') Usuario
		,ISNULL(S.Password,'') Password
		FROM PagoEfectivo.Servicio S
	JOIN PagoEfectivo.EmpresaContratante EP ON (S.IdEmpresaContratante=EP.IdEmpresaContratante)	
	WHERE S.IdEstado =41 AND S.IdTipoNotificacion=241 AND
	       isnull(urlftp,'')<>'' AND 
           isnull(usuario,'')<>'' AND 
           isnull(password,'')<>'' 
	 -- Activo 
    
 SET NOCOUNT OFF;
END