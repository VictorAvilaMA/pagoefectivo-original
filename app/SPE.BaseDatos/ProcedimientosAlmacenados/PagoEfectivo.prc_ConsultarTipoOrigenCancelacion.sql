/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : realiza la consulta de tipo de origen de cancelacion */    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan Quispe                                */    
/* Fecha    : 28/12/2008                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarTipoOrigenCancelacion] 
	
AS
BEGIN
	
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    
	SELECT IdTipoOrigenCancelacion,Descripcion
	FROM PagoEfectivo.TipoOrigenCancelacion

	SET NOCOUNT OFF;
END
