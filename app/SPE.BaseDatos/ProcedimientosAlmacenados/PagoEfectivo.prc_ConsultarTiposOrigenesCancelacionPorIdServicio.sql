/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar Tipos de Origenes de Cancelación por Servicio */    
/*             */
/* Creado x       : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fecha			: 30/12/2008                                           */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
create PROCEDURE [PagoEfectivo].[prc_ConsultarTiposOrigenesCancelacionPorIdServicio] --7

@IdServicio INT

AS
BEGIN

	SET NOCOUNT ON;
		select IdTipoOrigenCancelacion,IdServicio from PagoEfectivo.ServicioTipoOrigenCancelacion 
		where IdServicio = @IdServicio
	SET NOCOUNT OFF;
END
