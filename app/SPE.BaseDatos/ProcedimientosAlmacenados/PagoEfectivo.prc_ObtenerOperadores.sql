/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Obtiene todos los operadores */    
/*             */
/* Creado x       : cvillanueva - Christian Villanueva                                */    
/* Fecha		  : 28/12/2008                                           */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE PagoEfectivo.prc_ObtenerOperadores
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT IdOperador
			,Descripcion
	FROM PagoEfectivo.Operador
	WHERE IdEstado = 41		--ACTIVO

	SET NOCOUNT OFF;
END
