/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Anular operacion de cancelacion (extorno)				*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 23/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarAgenciaBancaria] --0,1,'0540','00','',1,1,1,1
	-- Add the parameters for the stored procedure here
	@pintIdAgenciaBancaria		int,
	@pintIdBanco				int,
	@pstrCodigo					varchar(50),
	@pstrDescripcion			varchar(250),
	@pstrDireccion				varchar(250),
	@pintIdEstado				int,
	@pintIdUsuarioCreacion		int,
	@pintIdUsuarioActualizacion	int,
	@pintIdCiudad				int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DECLARE @IdAgenciaBancaria INT,
				@IdTipoOrigenCancelacion	INT,
				@IdEstadoAperturaOrigen		INT

		--SELECT  @IdEstadoGenerarAB = (select id from PagoEfectivo.Parametro where grupoCodigo='ESAB' AND Descripcion='Activo')	
		SELECT  @IdEstadoAperturaOrigen = (select id from PagoEfectivo.Parametro where grupoCodigo='ESAC' AND Descripcion='Aperturado')	
		SELECT  @IdTipoOrigenCancelacion = (select IdTipoOrigenCancelacion from PagoEfectivo.TipoOrigenCancelacion where Descripcion ='Agencia Bancaria')	


		INSERT	INTO	PagoEfectivo.AgenciaBancaria
		(IdBanco, Codigo, Descripcion, Direccion, IdEstado, IdUsuarioCreacion, FechaCreacion,
		IdUsuarioActualizacion, FechaActualizacion, IdCiudad)
		VALUES	(	@pintIdBanco,
					@pstrCodigo,
					@pstrDescripcion,
					@pstrDireccion,
					@pintIdEstado,
					CASE @pintIdUsuarioCreacion WHEN 0 THEN NULL ELSE @pintIdUsuarioCreacion END,
					GetDate(),
					NULL,
					--CASE @pintIdUsuarioActualizacion WHEN 0 THEN NULL ELSE @pintIdUsuarioActualizacion END,
					NULL,
					CASE @pintIdCiudad WHEN 0 THEN NULL ELSE @pintIdCiudad END)

		SELECT	@IdAgenciaBancaria = @@identity
		
		INSERT	INTO	PagoEfectivo.AperturaOrigen
		(IdTipoOrigenCancelacion, IdEstablecimiento, IdAgenciaBancaria, FechaApertura, 
		FechaCierre, IdEstado)
		VALUES	(	@IdTipoOrigenCancelacion,
					NULL,
					@IdAgenciaBancaria,
					GetDate(),
					NULL,
					@IdEstadoAperturaOrigen
				)
	SET NOCOUNT OFF;

END
