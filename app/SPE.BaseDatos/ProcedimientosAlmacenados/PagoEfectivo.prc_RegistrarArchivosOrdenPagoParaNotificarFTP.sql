/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                           */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registrar Archivos Orden PAGO Para Notificar FTP*/    
/*              */
/* Creado x       : Cesar Miranda                                 */    
/* Fecha    : 13/01/2009                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 13/01/2009                                             */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarArchivosOrdenPagoParaNotificarFTP]

AS
BEGIN
 SET NOCOUNT ON;

	  DECLARE @vtblServicios TABLE(ID INT IDENTITY NOT NULL, 
                                   IdServicio INT,
                                   Nombre VARCHAR(100)
                                  );
                           
      DECLARE @vCantidad INT;
      DECLARE @vIndice INT;

	INSERT INTO @vtblServicios(IdServicio,Nombre)
    (SELECT S.IdServicio,S.Nombre 
     FROM PagoEfectivo.Servicio S
     WHERE S.IdEstado =41 AND S.IdTipoNotificacion=241 AND 
           isnull(urlftp,'')<>'' AND 
           isnull(usuario,'')<>'' AND 
           isnull(password,'')<>'' 
    );           -- Activo y Tipo Archivo FTP
    
     SET @vIndice=1;
     SET @vCantidad=(SELECT count(*)  FROM @vtblServicios);

	 WHILE(@vIndice<=@vCantidad)
     BEGIN
      DECLARE @vIdServicio INT;
      DECLARE @vNombre VARCHAR(255);
      
      SELECT @vNombre=Nombre ,
             @vIdServicio=IdServicio
      FROM @vtblServicios
      WHERE id=@vIndice;
       --- Para Cada Servicio
       
   --  SELECT 'indice - ' + cast( @vIndice AS VARCHAR(10));
       EXEC [PagoEfectivo].[prc_RegistrarArchivosXServicioOPPagadasYExpiradas] @vIdServicio;

    
	  SET @vIndice=@vIndice+1;       

       
     END
     
     

 SET NOCOUNT OFF;
END





GO
