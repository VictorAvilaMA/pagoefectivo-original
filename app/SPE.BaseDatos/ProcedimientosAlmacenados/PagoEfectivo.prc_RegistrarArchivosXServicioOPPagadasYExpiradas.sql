/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                           */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registrar Archivos Por Servicio Pagadas y Expiradas*/    
/*              */
/* Creado x       : Cesar Miranda                                 */    
/* Fecha    : 13/01/2009                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 13/01/2009                                             */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarArchivosXServicioOPPagadasYExpiradas]
(
@IdServicio INT
)
AS
BEGIN

--DECLARE @IdServicio INT
 --sET @idServicio=9;
 DECLARE @NombreArchivo VARCHAR(100);
 DECLARE @IdTipoArchivo INT;
 DECLARE @idEstado INT ;
 DECLARE @idftparchivo INT;
 DECLARE @FechaGenerado DATETIME;
  
 -- SELECT 'IdServicio-' +cast( @IdServicio AS VARCHAR(10)) ;
 
 
 ---- PAGADAS
 DECLARE @CantPagadas INT;
 SET @CantPagadas=(SELECT COUNT(*)
    FROM PagoEfectivo.OrdenPago OP
    WHERE ( (OP.IdServicio =@idServicio) AND 
            (OP.IdEstado=23) AND
            (SELECT COUNT(*) 
             FROM PagoEfectivo.FTPArchivoDetalle FTPAD
             JOIN PagoEfectivo.FTPArchivo FTP ON FTPAD.IdFtpArchivo =FTP.IdFtpArchivo 
             WHERE FTPAD.IdOrdenPago =OP.idOrdenPago  AND FTP.IdTipoArchivo=221)=0 
          ));
 
 IF(@CantPagadas >0)
        
 BEGIN
 

 SET @FechaGenerado=getdate();
 SET @NombreArchivo='pagos_'+[PagoEfectivo].[fn_DameFechaFormato](DATEPART(yy,@FechaGenerado)) +
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(month,@FechaGenerado))+
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(dd,@FechaGenerado))+
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(HH,@FechaGenerado))+
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(mi,@FechaGenerado))+
                     '.txt';
                     
 --SELECT  'PagoNombreArchivo-' + cast( @NombreArchivo AS VARCHAR(10));
 
  INSERT INTO PagoEfectivo.FTPArchivo
  (IdServicio,NombreArchivo,FechaGenerado,IdTipoArchivo,IdEstado)
  VALUES
  (@idservicio,@nombrearchivo,@FechaGenerado,221,232); -- Cabecera Archivo Pagado No Generado;
  
  
  SET @idftparchivo=@@IDENTITY;
    
  --- Códigos de Identificación de Pago Canceladas y no esten registradas en la tabla FTPArchivoDetalle
  INSERT INTO PagoEfectivo.FTPArchivoDetalle(IdFtpArchivo,idUsuario,OrdenIdComercio,
    tipoDePago,fechaPago,IdMoneda,Monto,IdOrdenPago)
   (SELECT @idftparchivo,OP.UsuarioID,OP.OrderIdComercio,'BK',OP.FechaCancelacion,
    OP.IdMoneda,OP.Total,OP.IdOrdenPago
    FROM PagoEfectivo.OrdenPago OP
    WHERE ( (OP.IdServicio =@IdServicio) AND 
            (OP.IdEstado=23) AND
            (SELECT COUNT(*) 
             FROM PagoEfectivo.FTPArchivoDetalle FTPAD
             JOIN PagoEfectivo.FTPArchivo FTP ON FTPAD.IdFtpArchivo =FTP.IdFtpArchivo 
             WHERE FTPAD.IdOrdenPago =OP.idOrdenPago AND  FTP.IdTipoArchivo=221)=0 
          )
   );
   
  -- SELECT  'idFTPArchivo-' + cast( @idftparchivo AS VARCHAR(10));
   
 END;
   --------------------------   EXPIRADAS
   
   DECLARE @CantExp INT;
   SET @CantExp=(SELECT COUNT(*)
       FROM PagoEfectivo.OrdenPago OP
       WHERE OP.IdServicio=@idServicio and OP.IdEstado=21 AND
             (SELECT COUNT(*) 
             FROM PagoEfectivo.FTPArchivoDetalle FTPAD 
             JOIN PagoEfectivo.FTPArchivo FTP ON FTPAD.IdFtpArchivo=FTP.IdFtpArchivo 
             WHERE IdOrdenPago=OP.IDOrdenPago AND FTP.IdTipoArchivo=222)=0 );
   
  IF(@CantExp>0)
  BEGIN
  
   SET @FechaGenerado=getdate();
   SET @NombreArchivo='habil_'+[PagoEfectivo].[fn_DameFechaFormato](DATEPART(yy,@FechaGenerado)) +
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(month,@FechaGenerado) )+
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(dd,@FechaGenerado))+
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(HH,@FechaGenerado))+
                     [PagoEfectivo].[fn_DameFechaFormato](DATEPART(mi,@FechaGenerado))+
                     '.txt';
   
 --SELECT  'habilNombreArchivo-' + cast( @NombreArchivo AS VARCHAR(10)); 
  INSERT INTO PagoEfectivo.FTPArchivo
  (IdServicio,NombreArchivo,FechaGenerado,IdTipoArchivo,IdEstado)
  VALUES
  (@idservicio,@nombrearchivo,getdate(),222,232); -- Cabecera Archivo Expirado y No Generado;
  
  
  SET @idftparchivo=@@IDENTITY;
    
  --- Códigos de Identificación de Pago Expiradas y no esten registradas en la tabla FTPArchivoDetalle
  INSERT INTO PagoEfectivo.FTPArchivoDetalle(IdFtpArchivo,idUsuario,OrdenIdComercio,
    tipoDePago,fechaPago,IdMoneda,Monto,IdOrdenPago)
   (SELECT @idftparchivo,OP.UsuarioID,OP.OrderIdComercio,'BK',OP.FechaCancelacion,
    OP.IdMoneda,OP.Total,OP.IdOrdenPago
    FROM PagoEfectivo.OrdenPago OP
    WHERE ( (OP.IdServicio =@IdServicio) AND 
            (OP.IdEstado=21) AND
            (SELECT COUNT(*) FROM PagoEfectivo.FTPArchivoDetalle FTPAD 
              JOIN PagoEfectivo.FTPArchivo FTP ON FTPAD.IdFtpArchivo=FTP.IdFtpArchivo 
              WHERE IdOrdenPago=OP.IDOrdenPago AND FTP.IdTipoArchivo=222 )=0 
            )
   );
           
   --SELECT  'idFTPArchivo-' + cast(@idftparchivo AS VARCHAR(10));
  END

END





GO

