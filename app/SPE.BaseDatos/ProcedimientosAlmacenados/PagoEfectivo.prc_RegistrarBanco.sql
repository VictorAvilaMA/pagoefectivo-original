/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registrar banco											*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarBanco] --'',0
	-- Add the parameters for the stored procedure here
	@pintIdBanco				int,
	@pstrCodigo					varchar(50),
	@pstrDescripcion			varchar(100),
	@pintidEstado				int,
	@pintidUsuarioCreacion		int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DECLARE @IdEstadoGenerarB INT
		SELECT  @IdEstadoGenerarB = (select id from PagoEfectivo.Parametro where grupoCodigo='ESBA' AND Descripcion='Activo')	


		INSERT	INTO	PagoEfectivo.Banco
		(Codigo, Descripcion, IdEstado, IdUsuarioCreacion, IdUsuarioActualizacion, FechaCreacion, FechaActualizacion)
		VALUES	(	@pstrCodigo,
					@pstrDescripcion,
					@IdEstadoGenerarB,
					@pintidUsuarioCreacion,
					NULL,
					GetDate(),
					NULL)
	SET NOCOUNT OFF;

END
