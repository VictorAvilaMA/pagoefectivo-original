set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo - Fase 2                                   */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registro de Cancelacion de Código de Identificación de Pago por Agencia Bancaria*/    
/*             */
/* Creado x       :	erojas@3devnet.com - Enrique Rojas                              */    
/* Fecha		  :  16/02/2009                                      */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion :  16/02/2009                                           */    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_RegistrarCancelacionOrdenPagoAgenciaBancaria] 
(
	@pstrNumeroOrdenPago			VARCHAR(14),
	@pstrNumeroOperacion			varchar(50),
	@pstrCodigoAgenciaBancaria		varchar(50),
	@pintIdMoneda					INT,
	@pdecMonto						decimal(18,2)
)
AS
BEGIN

SET NOCOUNT ON

--OBTENIENDO VALORES
DECLARE @NuevoCodigoAgenciaBancaria VARCHAR(50),
		@IdTipoOrigenCancelacion	INT,
		@IdEstadoActivo				INT,
		@IdEstadoOPGenerada			INT,
		@IdOrdenPago				INT,

		@IdMoneda					INT,
		@IdTipoMovimiento			INT,
		@Total						INT,
		@IdAperturaOrigen			INT,
		@IdAgenciaBancaria			INT,
		@IdMovimiento				INT,
		@IdEstadoOPCancelada		INT,
		@IdMedioPago				INT


	SET @NuevoCodigoAgenciaBancaria	=''
	SET @IdTipoOrigenCancelacion	=1
	SET @IdEstadoActivo			= 41
	SET @IdEstadoOPGenerada		= 22	
    SET @IdOrdenPago = ISNULL((SELECT OP.IdOrdenPago 
							    FROM PagoEfectivo.OrdenPago OP
							    INNER JOIN PagoEfectivo.Servicio S ON S.IdServicio = OP.IdServicio
							    INNER JOIN PagoEfectivo.ServicioTipoOrigenCancelacion STOC ON STOC.IdServicio = S.IdServicio
							   WHERE OP.NumeroOrdenPago = right('00000000000000' + rtrim(@pstrNumeroOrdenPago),14) 
							         AND OP.IdEstado=@IdEstadoOPGenerada
							         AND OP.Total = @pdecMonto
							         AND OP.IdMoneda = (select IdMoneda from PagoEfectivo.Moneda where EquivalenciaBancaria = @pintIdMoneda)
							         AND STOC.IdTipoOrigenCancelacion = @IdTipoOrigenCancelacion),0) -- Se obtiene el IdOrdenPago a cancelar
							
 --   SET @pstrCodigoAgenciaBancaria = RIGHT('000000'+RTRIM(@pstrCodigoAgenciaBancaria),6)
IF (@IdOrdenPago > 0 )
	BEGIN

			SET @IdMoneda = (SELECT IdMoneda	FROM PagoEfectivo.OrdenPago where IdOrdenPago = @IdOrdenPago)

			SET @IdTipoMovimiento = (34)
			SET @Total = (select Total from PagoEfectivo.OrdenPago where NumeroOrdenPago = @IdOrdenPago)
			SET @IdAperturaOrigen =ISNULL((SELECT AO.IdAperturaOrigen 
									       FROM PagoEfectivo.AgenciaBancaria AB
									       INNER JOIN PagoEfectivo.AperturaOrigen AO ON AO.IdAgenciaBancaria = AB.IdAgenciaBancaria
									       WHERE AB.Codigo=@pstrCodigoAgenciaBancaria
									     ),0)
		IF ( @IdAperturaOrigen = 0)
		BEGIN
			-- Si no existe la apertura origen se debe de crear

			INSERT INTO PagoEfectivo.AgenciaBancaria
            (Codigo,IdEstado,FechaCreacion,IdBanco)
            VALUES(@pstrCodigoAgenciaBancaria,191,getdate(),1)

			SET @IdAgenciaBancaria = @@Identity
			INSERT INTO PagoEfectivo.AperturaOrigen 
            (IdTipoOrigenCancelacion,IdAgenciaBancaria,FechaApertura,IdEstado)
            VALUES (1,@IdAgenciaBancaria,GETDATE(),121) -- Se apertura 

			SET @IdAperturaOrigen = @@Identity
			SET @NuevoCodigoAgenciaBancaria = @pstrCodigoAgenciaBancaria
		END

		SET @IdEstadoOPCancelada = 23	-- Estado OP Cancelada
		SET @IdMedioPago=2   -- Estado Id Medio Pago

		--REGISTRANDO EL MOVIMIENTO
		INSERT INTO PagoEfectivo.Movimiento 
		(IdOrdenPago,IdMoneda,IdTipoMovimiento,IdMedioPago,Monto,
		 FechaMovimiento,FechaCreacion,IdAperturaOrigen,NumeroOperacion,IdTipoOrigenCancelacion
        )VALUES 
        (@IdOrdenPago,@IdMoneda,@IdTipoMovimiento,@IdMedioPago,@Total,
         GETDATE(),GETDATE(),@IdAperturaOrigen,@pstrNumeroOperacion,1
        )
		
		SET @IdMovimiento = @@IDENTITY

		UPDATE	PagoEfectivo.OrdenPago
		 SET	IdEstado=@IdEstadoOPCancelada ,
				FechaCancelacion = getdate(),
				FechaActualizacion=GETDATE()
		WHERE	IdOrdenPago = @IdOrdenPago
        ---- Actualiza El Código de Identificación de Pago A cancelada

		--Resultado con exito
				SELECT	@IdOrdenPago as IdOrdenPago,
						@NuevoCodigoAgenciaBancaria as CodigoOficina,
						OP.NumeroOrdenPago,
						isnull(EC.OcultarEmpresa,0) as OcultarEmpresa,
						EC.RazonSocial as DescripcionEmpresa,
						M.dsmbmnd AS Moneda,
						S.Nombre as DescripcionServicio,
						ISNULL(OP.Total,0) AS Total,
						DOP.ConceptoPago,
						U.Email
				FROM	[PagoEfectivo].[OrdenPago] OP
						INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
						INNER JOIN PagoEfectivo.EmpresaContratante EC ON S.IdEmpresaContratante = EC.IdEmpresaContratante
						INNER JOIN PagoEfectivo.DetalleOrdenPago DOP ON OP.IdOrdenPago = DOP.IdOrdenPago
						INNER JOIN PagoEfectivo.Moneda M ON M.IdMoneda = OP.IdMoneda
						INNER JOIN PagoEfectivo.Cliente C ON C.IdCliente = OP.IdCliente
						INNER JOIN PagoEfectivo.Usuario U ON U.IdUsuario = C.IdUsuario
				WHERE	OP.IdOrdenPago = @IdOrdenPago
	END 
ELSE
	BEGIN
		--Resultado sin exito
		SELECT @IdOrdenPago as IdOrdenPago,
				@NuevoCodigoAgenciaBancaria as CodigoOficina,
				'' AS NumeroOrdenPago,
				'' AS OcultarEmpresa,
				'' AS DescripcionEmpresa,
				'' AS Moneda,
				'' AS DescripcionServicio,
				0 AS Total,
				'' AS ConceptoPago,
				'' AS Email
		
	END
SET NOCOUNT OFF
END
