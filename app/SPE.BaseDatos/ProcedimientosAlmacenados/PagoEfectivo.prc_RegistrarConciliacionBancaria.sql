
/****** Object:  StoredProcedure [PagoEfectivo].[prc_RegistrarConciliacionBancaria]    Script Date: 12/31/2008 10:19:08 ******/

/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo Fase II                                   */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 		Insertar cabecera de conciliacion*/    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan Quispe                                */    
/* Fecha		  : 27/10/2008                                     */    
/* Validado x     :                                 */    
/* Fec.Validacion :                                          */    
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarConciliacionBancaria] 
(
	@pstrNombreArchivo VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @IdConciliacion INT
	SET @IdConciliacion=ISNULL((SELECT IdConciliacion FROM PagoEfectivo.ConciliacionBancaria WHERE NombreArchivo=@pstrNombreArchivo),0)
	IF (@IdConciliacion) = 0
		BEGIN 
			INSERT INTO [PagoEfectivo].[ConciliacionBancaria]
				   ([FechaConciliacion]
				   ,[NombreArchivo]
				   ,[IdEstado])
			 VALUES
				   (GETDATE()
				   ,@pstrNombreArchivo
				   ,171)
			SET  @IdConciliacion = @@Identity
			SELECT  @IdConciliacion
		END
	ELSE
		SELECT 0

	SET NOCOUNT OFF;
END


