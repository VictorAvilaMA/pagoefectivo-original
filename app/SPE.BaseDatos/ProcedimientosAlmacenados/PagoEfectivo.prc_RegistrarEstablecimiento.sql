/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registro del Establecimiento */    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva          */    
/* Fecha		  : 22/12/2008                                            */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarEstablecimiento]
	
	@pstrSerieTerminal			VARCHAR(50),
	@pstrRazonSocial			VARCHAR(200),
	@pstrRUC					CHAR(11),
	@pstrContacto				VARCHAR(200),
	@pstrTelefono				VARCHAR(15),
	@pstrDireccion				VARCHAR(200),
	@pintIdOperador				INT,
	@pintIdCiudad				INT,
	@pintIdEstado				INT,
	@pintIdUsuarioCreacion		INT		
AS
BEGIN
	

	SET NOCOUNT ON;

	INSERT 	PagoEfectivo.Establecimiento
			(SerieTerminal
			,RazonSocial
			,RUC
			,Contacto
			,Telefono
			,Direccion
			,IdOperador
			,IdCiudad
			,IdEstado
			,IdUsuarioCreacion
			,FechaCreacion)
	VALUES	(@pstrSerieTerminal
			,@pstrRazonSocial
			,@pstrRUC
			,@pstrContacto
			,@pstrTelefono
			,@pstrDireccion
			,@pintIdOperador
			,@pintIdCiudad
			,@pintIdEstado
			,@pintIdUsuarioCreacion
			,GETDATE())

	SET NOCOUNT OFF;

END
