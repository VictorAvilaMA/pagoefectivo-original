USE [BD_PAGOEFECTIVO_TEST3]
GO
/****** Object:  StoredProcedure [PagoEfectivo].[prc_RegistrarJefeProducto]    Script Date: 01/15/2009 14:32:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 		Refistro de Jefe de Producto*/    
/*             */
/* Creado x       :cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha    :  06/01/2009                                      */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarJefeProducto]
	
	@pstrNombres					VARCHAR(50),
	@pstrApellidos					VARCHAR(50),
	@pintIdTipoDocumento				CHAR(8),
	@pstrNroDocumento				VARCHAR(50),
	@pstrDireccion					VARCHAR(100),
	@pstrTelefono					VARCHAR(15),
	@pstrEmail						VARCHAR(100),
	@pstrPassword					VARCHAR(100),
	@pintIdEstado					INT,
	@pintIdUsuarioCreacion			INT,
	@pintIdCiudad					INT,
	@pstrCodigoRegistro				VARCHAR(100)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @intIdUsuario INT
    
	EXEC PagoEfectivo.prc_RegistrarUsuario
	@intIdUsuario OUTPUT,
	@pintIdCiudad,
	@pstrNombres,
	@pstrApellidos,
	@pintIdTipoDocumento,
	@pstrNroDocumento,
	@pstrDireccion,
	@pstrTelefono,
	@pstrEmail,
	@pstrPassword,
	93,
	@pintIdUsuarioCreacion

	INSERT PagoEfectivo.JefeProducto (IdUsuario)
	VALUES(@intIdUsuario)

	-- CODIGO DE CONFIRMACION DEL USUARIO CUANDO SE ENVIE EL EMAIL
	INSERT PagoEfectivo.RegistroValidacion
		(IdUsuario
		,CodigoRegistro
		,FechaRegistro
		,IdEstado)
	VALUES(@intIdUsuario
		,@pstrCodigoRegistro
		,GETDATE()
		,71);
		

	INSERT	PagoEfectivo.Usuarioxrol(IdUsuario,IdRol)
	VALUES	(@intIdUsuario,6);	

	SELECT @intIdUsuario
	
	SET NOCOUNT OFF;
END


