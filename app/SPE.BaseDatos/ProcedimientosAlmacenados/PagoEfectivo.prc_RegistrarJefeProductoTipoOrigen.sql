/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 		Insertar Servicio*/    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha		  : 06/01/2009                                     */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarJefeProductoTipoOrigen]
	@pintIdUsuario					INT,
	@pintIdTipoOrigenCancelacion	INT,
	@pbitIsSelect					BIT
AS
BEGIN
	DECLARE @pintIdJefeProducto INT

	SET NOCOUNT ON;
	SELECT @pintIdJefeProducto = JP.IdJefeProducto
	FROM PagoEfectivo.JefeProducto JP 
	INNER JOIN PagoEfectivo.Usuario US ON JP.IdUsuario = US.IdUsuario AND US.IdUsuario = @pintIdUsuario 
	
	IF NOT EXISTS (SELECT IdTipoOrigenCancelacion FROM PagoEfectivo.JefeProductoTipoOrigen
					WHERE IdJefeProducto = @pintIdJefeProducto AND IdTipoOrigenCancelacion = @pintIdTipoOrigenCancelacion)
	BEGIN
		IF @pbitIsSelect = 1
			BEGIN
				INSERT PagoEfectivo.JefeProductoTipoOrigen (IdJefeProducto,IdTipoOrigenCancelacion) 
				VALUES (@pintIdJefeProducto,@pintIdTipoOrigenCancelacion)
			END
	END
	ELSE
	BEGIN 
		IF @pbitIsSelect = 0
			BEGIN
				DELETE FROM PagoEfectivo.JefeProductoTipoOrigen 
				WHERE IdJefeProducto = @pintIdJefeProducto
					  AND IdTipoOrigenCancelacion = @pintIdTipoOrigenCancelacion
			END
	END
	SET NOCOUNT OFF;
END

