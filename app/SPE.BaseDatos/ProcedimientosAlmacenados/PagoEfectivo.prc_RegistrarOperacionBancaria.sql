
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo Fase II                                   */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registrar operaciones bancarias */    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan                          */    
/* Fecha		  : 08/01/2009                                     */    
/* Validado x     :                                 */    
/* Fec.Validacion :                                          */    
/*--------------------------------------------------------------------------*/
															    
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarOperacionBancaria] 
(
	@pintIdConciliacion INT,
	@pstrOperacionBancaria VARCHAR(200)
)
AS
BEGIN

	SET NOCOUNT ON
           
           DECLARE @TipoRegistro char(2)
           DECLARE @CodigoSucursal char(3)
           DECLARE @CodigoMoneda char(1)
           DECLARE @NumeroCuenta char(7)
           DECLARE @CodigoDepositante char(14)
           DECLARE @InformacionRetorno char(30)
           DECLARE @FechaPago char(8)
           DECLARE @FechaVencimiento char(8)
           DECLARE @Monto char(15)
           DECLARE @Mora char(15)
           DECLARE @MontoTotal char(15)
           DECLARE @CodigoOficina char(6)
           DECLARE @NumeroOperacion char(6)
           DECLARE @Referencia char(22)
           DECLARE @IdentificacionTerminal char(4)
           DECLARE @MedioAtencion char(12)
           DECLARE @HoraAtencion char(6)
           DECLARE @NumeroCheque char(10)
           DECLARE @CodigoBanco char(2)
           DECLARE @CargoFijoPagado char(10)
           DECLARE @Filler char(4)
           DECLARE @IdEstado int
		   DECLARE @IdOperacion INT
-------
DECLARE @IdEstadoOP INT
DECLARE @IdEstadoCB INT
DECLARE @FlatForEmail INT
			
--Asignaciones de variables de operacion

           SET @TipoRegistro = SUBSTRING(@pstrOperacionBancaria,1,2)--char(2)
           SET @CodigoSucursal = SUBSTRING(@pstrOperacionBancaria,3,5) --char(3)
           SET @CodigoMoneda = SUBSTRING(@pstrOperacionBancaria,6,6)--char(1)
           SET @NumeroCuenta = SUBSTRING(@pstrOperacionBancaria,7,13)--char(7)
           SET @CodigoDepositante = SUBSTRING(@pstrOperacionBancaria,14,27)--char(14)
           SET @InformacionRetorno = SUBSTRING(@pstrOperacionBancaria,28,57)--char(50)
           SET @FechaPago = SUBSTRING(@pstrOperacionBancaria,58,65)--char(8)
           SET @FechaVencimiento = SUBSTRING(@pstrOperacionBancaria,66,73)--char(8)
           SET @Monto = SUBSTRING(@pstrOperacionBancaria,74,88)--char(15)
           SET @Mora = SUBSTRING(@pstrOperacionBancaria,89,103)--char(15)
           SET @MontoTotal = SUBSTRING(@pstrOperacionBancaria,104,118)--char(15)
           SET @CodigoOficina = SUBSTRING(@pstrOperacionBancaria,119,124)--char(6)
           SET @NumeroOperacion = SUBSTRING(@pstrOperacionBancaria,125,130)--char(6)
           SET @Referencia = SUBSTRING(@pstrOperacionBancaria,131,152)--char(22)
           SET @IdentificacionTerminal = SUBSTRING(@pstrOperacionBancaria,153,156)--char(4)
           SET @MedioAtencion = SUBSTRING(@pstrOperacionBancaria,157,168)--char(12)
           SET @HoraAtencion = SUBSTRING(@pstrOperacionBancaria,169,174)--char(6)
           SET @NumeroCheque = SUBSTRING(@pstrOperacionBancaria,175,184)--char(10)
           SET @CodigoBanco = SUBSTRING(@pstrOperacionBancaria,185,186)--char(2)
           SET @CargoFijoPagado = SUBSTRING(@pstrOperacionBancaria,187,196)--char(10)
           SET @Filler = SUBSTRING(@pstrOperacionBancaria,197,200)--char(4)
--           SELECT @IdEstado 

INSERT INTO [PagoEfectivo].[OperacionBancaria]
           ([IdConciliacion]
           ,[TipoRegistro]
           ,[CodigoSucursal]
           ,[CodigoMoneda]
           ,[NumeroCuenta]
           ,[CodigoDepositante]
           ,[InformacionRetorno]
           ,[FechaPago]
           ,[FechaVencimiento]
           ,[Monto]
           ,[Mora]
           ,[MontoTotal]
           ,[CodigoOficina]
           ,[NumeroOperacion]
           ,[Referencia]
           ,[IdentificacionTerminal]
           ,[MedioAtencion]
           ,[HoraAtencion]
           ,[NumeroCheque]
           ,[CodigoBanco]
           ,[CargoFijoPagado]
           ,[Filler]
           ,[IdEstado])
     VALUES
           (@pintIdConciliacion
           ,@TipoRegistro
           ,@CodigoSucursal
           ,@CodigoMoneda
           ,@NumeroCuenta
           ,@CodigoDepositante
           ,@InformacionRetorno
           ,@FechaPago
           ,@FechaVencimiento
           ,@Monto
           ,@Mora
           ,@MontoTotal
           ,@CodigoOficina
           ,@NumeroOperacion
           ,@Referencia
           ,@IdentificacionTerminal
           ,@MedioAtencion
           ,@HoraAtencion
           ,@NumeroCheque
           ,@CodigoBanco
           ,@CargoFijoPagado
           ,@Filler
           ,NULL)
	SET @IdOperacion = @@Identity
	
--AGENCIA BANCARIA
DECLARE @ObservacionCB VARCHAR(100)

DECLARE @IdMonedaOP INT
DECLARE @MontoOP DECIMAL(18,2)
DECLARE @IdOrdenPago INT
DECLARE @AgenciaBancariaCreada VARCHAR(50)
SET @AgenciaBancariaCreada=''
SET @FlatForEmail=0


SET @IdOrdenPago=ISNULL((SELECT IdOrdenPago FROM PagoEfectivo.OrdenPago WHERE NumeroOrdenPago=@CodigoDepositante AND (IdEstadoConciliacion IS NULL OR  IdEstadoConciliacion=172) ),0)
SET @IdMonedaOP=ISNULL((SELECT IdMoneda FROM PagoEfectivo.OrdenPago WHERE NumeroOrdenPago=@CodigoDepositante AND IdMoneda=(select IdMoneda from PagoEfectivo.Moneda where EquivalenciaBancaria=@CodigoMoneda)),0)
SET @MontoOP=ISNULL((SELECT Total FROM PagoEfectivo.OrdenPago WHERE NumeroOrdenPago=@CodigoDepositante AND Total=(CONVERT(DECIMAL(18,2),LEFT(@MontoTotal,13))+0.01*CONVERT(INT,RIGHT(@MontoTotal,2)))),0)
--Evaluando los valores de la OP

IF @IdOrdenPago<>0
	BEGIN
		IF @IdMonedaOP=0 OR @MontoOP=0
			BEGIN
				IF @IdMonedaOP=0
					SET @ObservacionCB=' El tipo de moneda no coinciden. '
				ELSE IF @MontoOP=0
					SET @ObservacionCB=' Los montos no coinciden. '
				SET @IdEstadoCB=172

			END
		ELSE
			BEGIN
				-- Buscando el IdApertura
				DECLARE @IdAperturaOrigen INT
				SET @IdAperturaOrigen = (ISNULL((SELECT AO.IdAperturaOrigen 
											FROM PagoEfectivo.AgenciaBancaria AB
											INNER JOIN PagoEfectivo.AperturaOrigen AO 
											ON AO.IdAgenciaBancaria = AB.IdAgenciaBancaria
											WHERE AB.Codigo=@CodigoOficina),0)) 
				IF @IdAperturaOrigen = 0
					BEGIN
						DECLARE @IdAgenciaBancaria INT
						INSERT INTO PagoEfectivo.AgenciaBancaria (Codigo,IdEstado,FechaCreacion)VALUES(@CodigoOficina,191,getdate())
						SET @IdAgenciaBancaria = @@Identity
						INSERT INTO PagoEfectivo.AperturaOrigen (IdTipoOrigenCancelacion,IdAgenciaBancaria,FechaApertura,IdEstado)VALUES (1,@IdAgenciaBancaria,GETDATE(),121)
						SET @IdAperturaOrigen = @@Identity
						SET @AgenciaBancariaCreada = @CodigoOficina
					END
				SET @IdEstadoOP=(SELECT IdEstado FROM PagoEfectivo.OrdenPago
								WHERE (IdEstadoConciliacion IS NULL OR IdEstadoConciliacion=172) 
								AND 
								IdOrdenPago=@IdOrdenPago
								AND IdMoneda=@IdMonedaOP
								AND Total=@MontoOP)

				IF @IdEstadoOP = 23
					BEGIN
						SET @IdEstadoCB = 171 --Conciliado
					END
				IF (@IdEstadoOP = 21) OR @IdEstadoOP = 22 OR @IdEstadoOP = 24 --Conciliado con modificacion
					BEGIN
						SET @IdEstadoCB = 173
						DECLARE @Fecha DATETIME
						SET @Fecha = CONVERT(DATETIME,@FechaPago)
						INSERT INTO PagoEfectivo.Movimiento (IdOrdenPago,IdMoneda,IdTipoMovimiento,IdMedioPago,Monto,FechaMovimiento,FechaCreacion,IdAperturaOrigen,NumeroOperacion)
																				VALUES (@IdOrdenPago,@IdMonedaOP,34,2,@MontoOP,GETDATE(),GETDATE(),@IdAperturaOrigen,@NumeroOperacion)
						EXECUTE [PagoEfectivo].[prc_ActualizarOrdenPagoRecepcionar] @IdOrdenPago,23,0,@Fecha
						SET @FlatForEmail=1
--
					END
			END		

			--Actualizando en Código de Identificación de Pago
			UPDATE PagoEfectivo.OrdenPago
			SET FechaConciliacion=GETDATE(),IdEstadoConciliacion=@IdEstadoCB
			WHERE IdOrdenPago=@IdOrdenPago

			--Actualizando en operacion bancaria
			UPDATE PagoEfectivo.OperacionBancaria
			SET Observacion=@ObservacionCB,IdEstado=@IdEstadoCB
			WHERE IdOperacion=@IdOperacion
			AND IdConciliacion=@pintIdConciliacion
			IF @FlatForEmail=1
				SELECT @FlatForEmail as Resultado,
				@AgenciaBancariaCreada as AgenciaBancaria,
				OP.NumeroOrdenPago,
				EC.OcultarEmpresa,
				EC.RazonSocial,
				M.dsmbmnd AS Moneda,
				S.Nombre as Servicio,
				ISNULL(OP.Total,0) AS Total,
				DOP.ConceptoPago,
				U.Email
				FROM [PagoEfectivo].[OrdenPago] OP
				INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
				INNER JOIN PagoEfectivo.EmpresaContratante EC ON S.IdEmpresaContratante = EC.IdEmpresaContratante
				INNER JOIN PagoEfectivo.DetalleOrdenPago DOP ON OP.IdOrdenPago = DOP.IdOrdenPago
				INNER JOIN PagoEfectivo.Moneda M ON M.IdMoneda = OP.IdMoneda
				INNER JOIN PagoEfectivo.Cliente C ON C.IdCliente = OP.IdCliente
				INNER JOIN PagoEfectivo.Usuario U ON U.IdUsuario = C.IdUsuario
				WHERE OP.IdOrdenPago = @IdOrdenPago
			ELSE
				SELECT @FlatForEmail as Resultado,
				@AgenciaBancariaCreada as AgenciaBancaria,
				'' AS NumeroOrdenPago,
				'' AS OcultarEmpresa,
				'' AS RazonSocial,
				'' AS Moneda,
				'' AS Servicio,
				0 AS Total,
				'' AS ConceptoPago,
				'' AS Email

	END
	ELSE
				SELECT 0 as Resultado,
				@AgenciaBancariaCreada as AgenciaBancaria,
				'' AS NumeroOrdenPago,
				'' AS OcultarEmpresa,
				'' AS RazonSocial,
				'' AS Moneda,
				'' AS Servicio,
				0 AS Total,
				'' AS ConceptoPago,
				'' AS Email
	    SELECT 0 as Resultado,@AgenciaBancariaCreada as AgenciaBancaria
	SET NOCOUNT OFF;
END
		
		