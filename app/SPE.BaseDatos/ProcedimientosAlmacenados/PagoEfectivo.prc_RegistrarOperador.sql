
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                           */    
/* Sistema        : Pago Efectivo                                           */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 	Registro de Operadores                              */    
/*             */
/* Creado x       :kfigueroa@3devnet.com - Karen Figueroa                   */    
/* Fecha          :26/12/2008                                               */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/

-- [PagoEfectivo].[prc_RegistrarOperador]  '048001', 'pRUEBAS', 41, 1
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarOperador] 
	@pstrCodigo				        VARCHAR(50),
	@pstrDescripcion				VARCHAR(50),
	@pintIdEstado					INT,
	@pintIdUsuarioCreacion			INT

AS
BEGIN
	
SET NOCOUNT ON;
    DECLARE @MENSAJE INT	
    
	SET @MENSAJE= 0    
   
    IF NOT EXISTS (SELECT Codigo FROM PagoEfectivo.Operador WHERE Codigo = @pstrCodigo ) 
	   BEGIN
			 INSERT INTO PagoEfectivo.Operador
				(Codigo
				,Descripcion
				,IdEstado
				,FechaCreacion
				,IdUsuarioCreacion)
			 VALUES(@pstrCodigo	
				,@pstrDescripcion
				,@pintIdEstado
				,GETDATE()
				,@pintIdUsuarioCreacion);
		    SELECT @MENSAJE
	  END
    
    
   ELSE
     BEGIN
		SET	@MENSAJE = 1
        SELECT @MENSAJE
     END

SET NOCOUNT OFF;

END
