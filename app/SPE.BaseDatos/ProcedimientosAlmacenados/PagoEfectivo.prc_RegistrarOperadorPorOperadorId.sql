
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                           */    
/* Sistema        : Pago Efectivo                                           */    
/* Modulo         : WEB                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de un Operador                      */    
/*                */
/* Creado x       : kfigueroa@3devnet.com - Karen Figueroa                  */    
/* Fecha          : 26/12/2008                                              */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/


CREATE PROCEDURE [PagoEfectivo].[prc_ConsultarOperadorPorOperadorId]
	@pintIdOperador		INT
AS
    
  BEGIN
	SET NOCOUNT ON;

		SELECT 
			IdOperador,
			Codigo,
			Descripcion,
			IdEstado,
			IdUsuarioCreacion,
			FechaCreacion,
			IdUsuarioActualizacion,
			FechaActualizacion,
			'' AS DescripcionEstado
		FROM PagoEfectivo.Operador
	  WHERE IdOperador = @pintIdOperador
	SET NOCOUNT OFF;
	
END