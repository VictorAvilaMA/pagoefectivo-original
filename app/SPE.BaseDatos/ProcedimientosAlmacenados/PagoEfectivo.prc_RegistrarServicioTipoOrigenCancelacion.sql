
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 		Insertar Servicio*/    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan                                */    
/* Fecha		  : 29/12/2008                                     */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_RegistrarServicioTipoOrigenCancelacion] 
(
	@pintIdServicio INT ,
	@pintIdTipoOrigenCancelacion INT ,
	@pintIdEstado INT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @IdEstadoActivo INT
	SELECT @IdEstadoActivo = (41)
	DECLARE @IdEstadoInactivo INT
	SELECT @IdEstadoInactivo = (42)
	--Verificamos si existe el IdServicioTipoOrigenCancelacion
	DECLARE @numero INT
	SELECT @numero = isnull((SELECT COUNT(*) FROM PagoEfectivo.ServicioTipoOrigenCancelacion WHERE IdServicio = @pintIdServicio and IdTipoOrigenCancelacion=@pintIdTipoOrigenCancelacion),0)
	
	IF @numero=0
		BEGIN 
			if @pintIdEstado = @IdEstadoActivo
				BEGIN
					--Si estado es activo
					INSERT INTO PagoEfectivo.ServicioTipoOrigenCancelacion
						(IdServicio,IdTipoOrigenCancelacion)
					VALUES 
						(@pintIdServicio,@pintIdTipoOrigenCancelacion);
				END
		END
	ELSE
		BEGIN 
			--Si estado es inactivo
			if @pintIdEstado = @IdEstadoInactivo
				BEGIN
					DELETE PagoEfectivo.ServicioTipoOrigenCancelacion 
					WHERE IdServicio = @pintIdServicio AND  IdTipoOrigenCancelacion = @pintIdTipoOrigenCancelacion
				END
			END 
	SET NOCOUNT OFF;
END
