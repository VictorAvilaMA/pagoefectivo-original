/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Verificar si esxiste el codigo de la agencia bancaria	*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 27/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/
CREATE PROCEDURE [PagoEfectivo].[prc_ValidarCodigoAgenciaBancaria]  --'191014'
@pstrCodigo varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	COUNT(IdAgenciaBancaria)
	FROM	PagoEfectivo.AgenciaBancaria
	WHERE	Codigo = @pstrCodigo

	SET NOCOUNT OFF;
END;
