/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de Código de Identificación de Pago								*/    
/*																			*/
/* Creado x       : cclaros@3devnet.com - Christian Claros                  */    
/* Fecha		  : 24/12/2008												*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 08/01/2009                                          */
/*--------------------------------------------------------------------------*/

CREATE PROCEDURE [PagoEfectivo].[prc_ValidarOrdenPagoExAgenciaBancaria] --'8', '000011'
	-- Add the parameters for the stored procedure here
	@pstrNumeroOP		varchar(14),
	@pstrCodAgenciaBanc varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE		@vstrRetorna as varchar(20),
				@vintNumeroReg as int,
				@vstrNumOrdenP as varchar(14)
	
	SET	@vstrNumOrdenP = Right('00000000000000' + LTRIM(RTRIM(@pstrNumeroOP)),14)
	
			
	--Primero validamos que El Código de Identificación de Pago exista
	SELECT	@vintNumeroReg = COUNT(IdOrdenPago)
	FROM	PagoEfectivo.OrdenPago OP
	WHERE	OP.NumeroOrdenPago = @vstrNumOrdenP AND 
			IdEstado IN (22,23) --Generada, Cancelada

	IF	@vintNumeroReg <> 0 
		BEGIN
			SET	@vstrRetorna = 'EX'
			SET	@vintNumeroReg = 0

			--Verificamos que El Código de Identificación de Pago este cancelada
			SELECT	@vintNumeroReg = COUNT(IdOrdenPago)
			FROM	PagoEfectivo.OrdenPago OP
			WHERE	OP.NumeroOrdenPago = @vstrNumOrdenP AND
					IdEstado = 23	--Cancelada

			IF @vintNumeroReg <> 0
				BEGIN
					SET	@vstrRetorna = @vstrRetorna + '-EX'
					SET	@vintNumeroReg = 0
					
						--Verificamos que la Agencia Bancaria en que se cancelo se la misma en la que se extorna
						SELECT	@vintNumeroReg = COUNT(IdMovimiento)
						FROM	PagoEfectivo.Movimiento MO INNER JOIN PagoEfectivo.AperturaOrigen OC ON
								MO.IdAperturaOrigen = OC.IdAperturaOrigen INNER JOIN PagoEfectivo.AgenciaBancaria AB ON
								OC.IdAgenciaBancaria = AB.IdAgenciaBancaria
						WHERE	AB.Codigo = @pstrCodAgenciaBanc
						
					IF @vintNumeroReg <> 0
						BEGIN
						SET @vstrRetorna = @vstrRetorna + '-EX';
						SET	@vintNumeroReg = 0

						--Verificamos que la fecha de cancelacion sea igual al dia del extorno
						SELECT	@vintNumeroReg = COUNT(IdMovimiento)
						FROM	PagoEfectivo.Movimiento MO INNER JOIN PagoEfectivo.OrdenPago OP ON
								MO.IdOrdenPago = OP.IdOrdenPago
						WHERE	OP.NumeroOrdenPago = @vstrNumOrdenP AND
								CONVERT(VARCHAR(10),FechaMovimiento, 105) = CONVERT(VARCHAR(10),GetDate(), 105) 

						IF @vintNumeroReg <> 0
							BEGIN
								SET @vstrRetorna = @vstrRetorna + '-EX';
							END
						ELSE
							SET @vstrRetorna = @vstrRetorna + '-NE';
								
						END

					ELSE
						SET @vstrRetorna = @vstrRetorna + '-NE-NE';

				END
			ELSE
				SET @vstrRetorna = @vstrRetorna + '-NE-NE-NE';

		END
	ELSE
			SET	@vstrRetorna = 'NE-NE-NE-NE';

	SELECT @vstrRetorna
	SET NOCOUNT OFF;	
END


/*
SELECT *FROM PagoEfectivo.Parametro
WHERE	GrupoCodigo = 'ESOP'
*/
