/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la actualizacion de un servicio */    
/*             */
/* Creado x       : cmiranda - Cesar Miranda                                */    
/* Fecha    : 27/10/2008                                         */    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 27/10/2008                                           */    
/*--------------------------------------------------------------------------*/


ALTER PROCEDURE [PagoEfectivo].[prc_ActualizarServicio]
(
  	@IdServicio INT ,
	@IdEmpresaContratante INT ,
	@Nombre VARCHAR(100) ,
	@IdEstado INT ,
	@TiempoExpiracion DECIMAL(18,2) ,
	@LogoImagen IMAGE ,
	@idusuarioactualizacion INT,
	@Url VARCHAR (250),
	@IdTipoNotificacion		INT,
	@UrlFTP					VARCHAR(100),
	@Usuario				VARCHAR(100),
	@Password				VARCHAR(100)
)
AS
BEGIN


	SET NOCOUNT ON;

	IF(@LogoImagen is NULL)
	BEGIN
	 SET @LogoImagen = (SELECT LogoImagen FROM PagoEfectivo.Servicio WHERE IdServicio=@IdServicio);
	 
	END;
	UPDATE PagoEfectivo.Servicio
	SET 
		IdEmpresaContratante = @idempresacontratante,
		Nombre = @nombre,
		IdEstado = @idestado,
		TiempoExpiracion = @tiempoexpiracion,
		LogoImagen = @logoimagen,
		FechaActualizacion = getdate(),
		IdUsuarioActualizacion = @idusuarioactualizacion,
		Url=@Url,
		IdTipoNotificacion= @IdTipoNotificacion,
		UrlFTP = @UrlFTP,
		Usuario =@Usuario,
		Password = @Password
	WHERE IdServicio = @IdServicio;
	SELECT @IdServicio
	SET NOCOUNT OFF;
END

