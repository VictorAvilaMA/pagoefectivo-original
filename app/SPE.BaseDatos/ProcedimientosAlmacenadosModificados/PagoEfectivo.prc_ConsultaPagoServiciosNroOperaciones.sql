/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta de Pagos de Servicios de una Empresa Contratante */    
/*																			*/
/* Creado x       : cmiranda - Cesar Miranda								*/    
/* Fecha    : 19/12/2008													*/    
/* Validado x     : cmiranda - Cesar Miranda                                */    
/* Fec.Validacion : 19/12/2008												*/    
/*--------------------------------------------------------------------------*/


ALTER PROCEDURE [PagoEfectivo].[prc_ConsultaPagoServiciosNroOperaciones] --14,1,1,23,'01-01-2008','10-10-2009',2
	-- Add the parameters for the stored procedure here
	@pintIdUsuario				INT,
	@pintIdServicio				INT,
	@pintIdMoneda				INT,
	@pintIdEstado				INT,
	@pdteFechaCancDe			DATETIME,
	@pdteFechaCancA				DATETIME,
	@pintIdTipoOrigenCancelacion INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Insert statements for procedure here
	SELECT	SE.IdServicio,SE.Nombre DescripcionServicio,MO.IdMoneda,MO.dsmbmnd SimboloMoneda,OP.IdEstado,PA.Descripcion DescripcionEstado,CONVERT(VARCHAR(20),OP.FechaCancelacion,106) Fecha,count(*) Total

	FROM	PagoEfectivo.Servicio SE
			INNER JOIN PagoEfectivo.EmpresaContratante EC ON SE.IdEmpresaContratante = EC.IdEmpresaContratante
			INNER JOIN PagoEfectivo.Representante RE ON EC.IdEmpresaContratante = RE.IdEmpresaContratante
			INNER JOIN PagoEfectivo.OrdenPago OP ON SE.IdServicio = OP.IdServicio
			INNER JOIN PagoEfectivo.Parametro PA ON OP.IdEstado = PA.Id
			INNER JOIN PagoEfectivo.Moneda MO ON OP.IdMoneda = MO.IdMoneda

			INNER JOIN (select max(idMovimiento) idMovimiento,idordenPago,IdTipoMovimiento, IdTipoOrigenCancelacion from pagoefectivo.movimiento
						where idtipomovimiento = 34 --RECEPCIONAR OP
						group by idordenPago, IdTipoOrigenCancelacion, IdTipoMovimiento) MOV ON OP.IdOrdenPago = MOV.IdOrdenPago

	WHERE	RE.IdUsuario = @pintIdUsuario AND
			OP.IdServicio = @pintIdServicio AND
			OP.IdMoneda = @pintIdMoneda AND
			OP.IdEstado = @pintIdEstado AND
			OP.FechaCancelacion >= CONVERT(DATETIME,@pdteFechaCancDe,110) AND 
			OP.FechaCancelacion < DATEADD(dd,1,CONVERT(DATETIME,@pdteFechaCancA,110)) AND

			(MOV.IdTipoOrigenCancelacion = @pintIdTipoOrigenCancelacion OR 0 = @pintIdTipoOrigenCancelacion)

	GROUP BY SE.IdServicio,SE.Nombre,MO.IdMoneda,MO.dsmbmnd,OP.IdEstado,PA.Descripcion,CONVERT(VARCHAR(20),OP.FechaCancelacion,106),OP.Total
	SET NOCOUNT OFF;
END

