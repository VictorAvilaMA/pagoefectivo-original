--*********************************
--ATUALIZACION DE PROCEDURES FASE I
--*********************************

/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consulta las Cajas Liquidadas por Supervisor*/    
/*             */
/* Creado x       : cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha    : 11/11/2008                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 11/11/2008                                            */		
/*--------------------------------------------------------------------------*/

ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarCajasLiquidadas]
	@pintIdUsuario			INT,
	@pstrNumeroOP			VARCHAR(14),
	@pdteFechaInicio		DATETIME,
	@pdateFechaFin			DATETIME

AS
BEGIN
	
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT CA.Nombre  AS NombreCaja
			,AC.IdAgenteCaja
			,AR.NombreComercial NombreAgencia
			,US.Apellidos +', '+ US.Nombres AS Agente
			,CONVERT(VARCHAR(11),AC.FechaApertura,106) FechaAgrupar
			,AC.FechaApertura
			,AC.FechaCierre
			,ME.ddocgrl MedioPago
			,MON.dsmbmnd SimboloMoneda
			,MON.dmnd000 Moneda
			,SUM(MO.Monto) AS Monto
			,CONVERT(DECIMAL(20),OP.NumeroOrdenPago) NumeroOrdenPago
	FROM PagoEfectivo.Usuario US
	INNER JOIN PagoEfectivo.Agente AG ON  US.IdUsuario = AG.IdUsuario
	INNER JOIN PagoEfectivo.AgenciaRecaudadora AR ON AG.IdAgenciaRecaudadora = AR.IdAgenciaRecaudadora
	AND AR.IdAgenciaRecaudadora = (SELECT AR.IdAgenciaRecaudadora AS IdAgenciaRecaudadora
				FROM PagoEfectivo.AgenciaRecaudadora AR
				INNER JOIN PagoEfectivo.Agente AG ON AR.IdAgenciaRecaudadora = AG.IdAgenciaRecaudadora
				INNER JOIN PagoEfectivo.Usuario US ON AG.IdUsuario = US.IdUsuario
				WHERE US.IdUsuario = @pintIdUsuario)
	INNER JOIN PagoEfectivo.AgenteCaja AC ON AG.IdAgente = AC.IdAgente
	INNER JOIN PagoEfectivo.Caja CA ON AC .IdCaja = CA.IdCaja 
	LEFT JOIN  PagoEfectivo.Movimiento MO ON AC.IdAgenteCaja = MO.IdAgenteCaja
	INNER JOIN PagoEfectivo.OrdenPago OP ON MO.IdOrdenPago = OP.IdOrdenPago
	INNER JOIN PagoEfectivo.MedioPago ME ON MO.IdMedioPago	= ME.IdMedioPago	
	INNER JOIN PagoEfectivo.Moneda MON ON MO.IdMoneda = MON.IdMoneda
	WHERE	AC.IdEstado = 83 -- SOLO CAJAS LIQUIDADAS 
			AND MO.IdTipoMovimiento = 32 -- TOTAL DE LIQUIDACION
			AND AC.FechaActualizacion >= CONVERT(DATETIME,@pdteFechaInicio,110) AND AC.FechaActualizacion < DATEADD(dd,1,CONVERT(DATETIME,@pdateFechaFin,110))
			AND (OP.NumeroOrdenPago = RIGHT('0000000000000' + CONVERT(VARCHAR(14),@pstrNumeroOP),14) OR '' = @pstrNumeroOP)
	GROUP BY CA.Nombre
			,AC.IdAgenteCaja
			,AR.NombreComercial
			,US.Apellidos
			,US.Nombres
			,AC.FechaApertura
			,AC.FechaCierre
			,ME.ddocgrl
			,MON.dmnd000	
			,MON.dsmbmnd
			,OP.NumeroOrdenPago

	SET NOCOUNT OFF;

END
GO