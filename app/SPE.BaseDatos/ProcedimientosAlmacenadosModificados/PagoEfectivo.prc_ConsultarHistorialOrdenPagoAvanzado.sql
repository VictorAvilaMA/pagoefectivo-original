
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la consulta del Historial de Código de Identificación de Pago Avanzado */    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan                                */    
/* Fecha    : 04/11/2008                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 04/11/2008                                             */    
/*--------------------------------------------------------------------------*/


ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarHistorialOrdenPagoAvanzado]  
	-- Add the parameters for the stored procedure here
	@pstrNumeroOrdenPago	VARCHAR(14),
	@pintIdUsuario			INT,
	@IdTipoOrigenCancelacion INT,
	@pintIdEstado			INT,
	@pdteFechaInicioE		DATETIME,
	@pdteFechaFinE			DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@dteFechaInicioE	VARCHAR(10)
	DECLARE	@dteFechaFinE		VARCHAR(10)

	SET	@dteFechaInicioE = CONVERT(VARCHAR(10),@pdteFechaInicioE,110)
	SET	@dteFechaFinE = CONVERT(VARCHAR(10),@pdteFechaFinE,110)
--	DECLARE @M NVARCHAR(MAX)
DECLARE  @N NVARCHAR(MAX)
SET @N=N''
IF (@pintIdEstado= 22 OR @pintIdEstado= 0 )
BEGIN
	SET @N=@N+'SELECT	OP.IdOrdenPago,
			OP.IdCliente,
			OP.IdEstado,
			OP.IdServicio,
			OP.IdMoneda,
			OP.NumeroOrdenPago,
			OP.FechaEmision,
			ISNULL(OP.FechaCancelacion,'''') FechaCancelacion,
			ISNULL(OP.FechaActualizacion,'''') FechaActualizacion,
			M.dsmbmnd SimboloMoneda,
			OP.Total,
			OP.OrderIdComercio,
			OP.UrlOK,
			OP.UrlError,
			OP.MailComercio,
			OP.FechaCreacion,
			OP.IdUsuarioCreacion,
			ISNULL(OP.IdUsuarioActualizacion,0) IdUsuarioActualizacion,
			US.Nombres + '' '' + US.Apellidos DescripcionCliente,
			S.Nombre DescripcionServicio,
			PA.Descripcion DescripcionEstado,
			S.Nombre DescripcionServicio,
			OP.IdEstado,
			EC.RazonSocial DescripcionEmpresa,
			'''' AS DescripcionTipoOrigenCancelacion,
			OP.FechaEmision AS FechaGeneral  

	FROM	PagoEfectivo.OrdenPago OP
			INNER JOIN PagoEfectivo.Moneda M ON M.IdMoneda = OP.IdMoneda
			INNER JOIN PagoEfectivo.Cliente C ON C.IdCliente = OP.IdCliente
			INNER JOIN PagoEfectivo.Usuario US ON US.IdUsuario = C.IdUsuario
			INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
			INNER JOIN PagoEfectivo.EmpresaContratante EC ON EC.IdEmpresaContratante = S.IdEmpresaContratante 
		
			INNER JOIN PagoEfectivo.Parametro PA ON OP.IdEstado = PA.Id ' +
			' WHERE	(OP.NumeroOrdenPago = RIGHT(''0000000000000'' + CONVERT(VARCHAR(14),''' +  @pstrNumeroOrdenPago + '''),14) OR '''' = ''' + @pstrNumeroOrdenPago + ''') ' + 
			' AND (US.IdUsuario = '+convert(varchar(2),@pintIdUsuario)+') 
			AND (OP.IdEstado = 22 ) '+
			' AND OP.FechaEmision >= CONVERT(DATETIME, ''' + @dteFechaInicioE + ''',110) AND OP.FechaEmision < dateadd(dd,1,CONVERT(DATETIME, ''' + @dteFechaFinE + ''',110))'  +
			' UNION '
END
	
	SET @N =@N+' SELECT	OP.IdOrdenPago,
			OP.IdCliente,
			OP.IdEstado,
			OP.IdServicio,
			OP.IdMoneda,
			OP.NumeroOrdenPago,
			OP.FechaEmision,
			ISNULL(OP.FechaCancelacion,'''') FechaCancelacion,
			ISNULL(OP.FechaActualizacion,'''') FechaActualizacion,
			M.dsmbmnd SimboloMoneda,
			OP.Total,
			OP.OrderIdComercio,
			OP.UrlOK,
			OP.UrlError,
			OP.MailComercio,
			OP.FechaCreacion,
			OP.IdUsuarioCreacion,
			ISNULL(OP.IdUsuarioActualizacion,0) IdUsuarioActualizacion,
			US.Nombres + '' '' + US.Apellidos DescripcionCliente,
			S.Nombre DescripcionServicio,
			PA.Descripcion DescripcionEstado,
			S.Nombre DescripcionServicio,
			OP.IdEstado,
			EC.RazonSocial DescripcionEmpresa,
			TOC.Descripcion DescripcionTipoOrigenCancelacion,
            case OP.IdEstado 
               when 21 then OP.FechaExpirada 
               when 22 then  OP.FechaEmision  
               when 23 then OP.FechaCancelacion  
               when 24 then OP.FechaAnulada 
            END FechaGeneral  
	FROM	PagoEfectivo.OrdenPago OP
			INNER JOIN PagoEfectivo.Moneda M ON M.IdMoneda = OP.IdMoneda
			INNER JOIN PagoEfectivo.Cliente C ON C.IdCliente = OP.IdCliente
			INNER JOIN PagoEfectivo.Usuario US ON US.IdUsuario = C.IdUsuario
			INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
			INNER JOIN PagoEfectivo.EmpresaContratante EC ON EC.IdEmpresaContratante = S.IdEmpresaContratante
			INNER JOIN PagoEfectivo.Movimiento MO ON MO.IdMovimiento = (SELECT TOP 1 IdMovimiento 
																FROM PagoEfectivo.Movimiento
																WHERE IdOrdenPago = OP.IdOrdenPago AND IdTipoMovimiento = 34
																ORDER BY IdMovimiento  DESC)
			INNER JOIN PagoEfectivo.TipoOrigenCancelacion TOC ON TOC.IdTipoOrigenCancelacion = MO.IdTipoOrigenCancelacion
			INNER JOIN PagoEfectivo.Parametro PA ON OP.IdEstado = PA.Id ' + 
	'WHERE	(OP.NumeroOrdenPago = RIGHT(''0000000000000'' + CONVERT(VARCHAR(14),''' +  @pstrNumeroOrdenPago + '''),14) OR '''' = ''' + @pstrNumeroOrdenPago + ''') ' + 
				' AND (US.IdUsuario =' + convert(varchar(10),@pintIdUsuario) + ')' + 
				' AND (TOC.IdTipoOrigenCancelacion = ' + CONVERT(VARCHAR(2),@IdTipoOrigenCancelacion) + ' OR 0 = ''' + CONVERT(VARCHAR(2),@IdTipoOrigenCancelacion) + ''')' +
				' AND (OP.IdEstado = ' + CONVERT(VARCHAR(2),@pintIdEstado) + ' OR 0 = ''' + CONVERT(VARCHAR(2),@pintIdEstado) + ''')' + 				
				' AND OP.IdEstado <> 22 '
				
	--***********************************
	--VALIDAMO LOS ESTADOS DE LA CONSULTA
	--***********************************

	IF		@pintIdEstado = 0 --TODOS
			BEGIN
			SET @N =	@N + ' AND ( (OP.FechaEmision >= CONVERT(DATETIME, ''' + @dteFechaInicioE + ''',110) AND OP.FechaEmision < dateadd(dd,1,CONVERT(DATETIME, ''' + @dteFechaFinE + ''',110)) )' + 
						' OR (OP.FechaCancelacion >= CONVERT(DATETIME, ''' + @dteFechaInicioE + ''',110) AND OP.FechaCancelacion < dateadd(dd,1,CONVERT(DATETIME, ''' + @dteFechaFinE + ''',110)) )' + 
						' OR (OP.FechaActualizacion >= CONVERT(DATETIME, ''' + @dteFechaInicioE + ''',110) AND OP.FechaActualizacion < dateadd(dd,1,CONVERT(DATETIME, ''' + @dteFechaFinE + ''',110)) ) )' 
			END
	ELSE IF		@pintIdEstado = 23 --CANCELADO
			BEGIN
			SET @N =	@N + ' AND OP.FechaCancelacion >= CONVERT(DATETIME, ''' + @dteFechaInicioE + ''',110) AND OP.FechaCancelacion < dateadd(dd,1,CONVERT(DATETIME, ''' + @dteFechaFinE + ''',110))' 
			END
	ELSE	--21 (EXPIRADO) , 24 (ANULADO)
			BEGIN
			SET @N =	@N + ' AND OP.FechaActualizacion >= CONVERT(DATETIME, ''' + @dteFechaInicioE + ''',110) AND OP.FechaActualizacion < dateadd(dd,1,CONVERT(DATETIME, ''' + @dteFechaFinE + ''',110))' 
			END

	

	EXEC sp_executesql @N
	
	SET NOCOUNT OFF;
END