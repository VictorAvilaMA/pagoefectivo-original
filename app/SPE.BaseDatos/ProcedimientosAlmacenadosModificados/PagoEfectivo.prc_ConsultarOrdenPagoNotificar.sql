/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de Código de Identificación de Pago para la Notificacion*/    
/*             */
/* Creado x       : kgranados@3devnet.com - Kleber Granados Diego                                */    
/* Fecha    : 04/11/2008                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 04/11/2008                                             */    
/*--------------------------------------------------------------------------*/

ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarOrdenPagoNotificar] 
	-- Add the parameters for the stored procedure here
	@pintIdOrdenPago	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	OP.MerchantID,
			OP.OrderIdComercio,
			OP.UrlOK,
			OP.UrlError,
			OP.MailComercio,
			DOP.ConceptoPago,
			OP.IdMoneda,
			OP.Total,
			S.Url UrlServicio,
			S.IdTipoNotificacion 
	FROM	PagoEfectivo.OrdenPago OP
			INNER JOIN PagoEfectivo.Moneda M ON OP.IdMoneda = M.IdMoneda
			INNER JOIN PagoEfectivo.Parametro PA ON OP.IdEstado = PA.Id
			INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
			INNER JOIN PagoEfectivo.EmpresaContratante EC ON EC.IdEmpresaContratante = S.IdEmpresaContratante
			INNER JOIN PagoEfectivo.Cliente C ON OP.IdCliente = C.IdCliente
			INNER JOIN PagoEfectivo.Usuario US ON US.IdUsuario = C.IdUsuario
			INNER JOIN PagoEfectivo.DetalleOrdenPago DOP ON OP.IdOrdenPago = DOP.IdOrdenPago
	WHERE	OP.IdOrdenPago = @pintIdOrdenPago

	SET NOCOUNT OFF;
END
