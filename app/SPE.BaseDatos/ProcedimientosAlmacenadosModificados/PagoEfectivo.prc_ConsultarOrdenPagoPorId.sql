

/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de Código de Identificación de Pago por Id*/    
/*             */
/* Creado x       : apaitan@3devnet.com - Alex Paitan                                */    
/* Fecha    : 02/12/2008                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 02/12/2008                                             */    
/*--------------------------------------------------------------------------*/

ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarOrdenPagoPorId] 
( 
@pintIdOrdenPago			INT
)
AS
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @pintIdCliente INT
	
	
	SELECT  OP.IdOrdenPago as IdOrdenPago
			,D.ConceptoPago AS ConceptoPago
			,OP.IdCliente as IdCliente
			,(U.Nombres + ' ' + U.Apellidos) AS DescripcionCliente
			,OP.IdEstado as IdEstado
			,OP.IdServicio as IdServicio
			,OP.IdMoneda AS IdMoneda
			,M.dsmbmnd AS SimboloMoneda
			,OP.NumeroOrdenPago as NumeroOrdenPago
			,OP.FechaEmision AS FechaEmision
			,OP.FechaCancelacion AS FechaCancelacion
			,OP.Total AS Total
			,OP.OrderIdComercio AS OrderIdComercio
			,OP.UrlOK AS UrlOK
			,OP.UrlError AS UrlError
			,OP.MailComercio AS MailComercio
			,OP.FechaCreacion AS FechaCreacion
			,OP.IdUsuarioCreacion AS IdUsuarioCreacion
			,GETDATE() AS FechaActualizacion
			,1 AS IdUsuarioActualizacion
			,EC.IdEmpresaContratante AS IdEmpresa
			,EC.RazonSocial as DescripcionEmpresa
			,S.Nombre as DescripcionServicio 
			,P.Descripcion DescripcionEstado
			,EC.OcultarEmpresa 
			,U.Email MailCliente
			 ,isnull(S.LogoImagen,'') AS ImagenServicio,
			  isnull(EC.ImagenLogo,'') AS ImagenEmpresa			
	FROM PagoEfectivo.OrdenPago OP
	INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
	INNER JOIN PagoEfectivo.EmpresaContratante EC ON EC.IdEmpresaContratante = S.IdEmpresaContratante
	INNER JOIN PagoEfectivo.Cliente C ON C.IdCliente = OP.IdCliente
	INNER JOIN PagoEfectivo.Usuario U ON C.IdUsuario = U.IdUsuario
	INNER JOIN PagoEfectivo.DetalleOrdenPago D ON D.IdOrdenPago = OP.IdOrdenPago
	INNER JOIN PagoEfectivo.Moneda M ON M.IdMoneda=OP.IdMoneda
	INNER JOIN PagoEfectivo.Parametro  P ON OP.IdEstado = P.Id AND P.GrupoCodigo='ESOP'
	
	WHERE (OP.IdOrdenPago=@pintIdOrdenPago)
	ORDER BY NumeroOrdenPago DESC;

	SET NOCOUNT OFF;

END


