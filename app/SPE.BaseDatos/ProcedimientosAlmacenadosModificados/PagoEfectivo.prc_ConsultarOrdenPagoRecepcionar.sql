go
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de Código de Identificación de Pago para la Recepcion*/    
/*             */
/* Creado x       : kgranados@3devnet.com - Kleber Granados Diego                                */    
/* Fecha    : 04/11/2008                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 04/11/2008                                             */    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarOrdenPagoRecepcionar] 
	-- Add the parameters for the stored procedure here
	@pstrNumeroOrdenPago	VARCHAR(14),
	@pintIdEstado			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    -- Insert statements for procedure here
	SELECT	OP.IdOrdenPago,
			S.IdServicio,
			EC.IdEmpresaContratante,
			OP.IdMoneda,
			OP.NumeroOrdenPago,
			DOP.ConceptoPago,
			OP.FechaEmision,
			M.dsmbmnd SimboloMoneda,
			OP.Total,
			S.Nombre DescripcionServicio,
			PA.Descripcion DescripcionEstado,
			EC.RazonSocial DescripcionEmpresa,
			US.Nombres + ' ' + US.Apellidos DescripcionCliente,
			US.Email EmailCliente,
			ISNULL(S.LogoImagen,'') LogoServicio,
			ISNULL(EC.ImagenLogo,'') LogoEmpresa,
			OP.MerchantID,
			OP.OrderIdComercio,
			OP.UrlOK,
			OP.UrlError,
			OP.MailComercio,
			S.Url UrlServicio,
			isnull((SELECT IdTipoOrigenCancelacion
			   FROM PagoEfectivo.ServicioTipoOrigenCancelacion 
			   WHERE IdServicio=OP.IdServicio AND IdTipoOrigenCancelacion=2),'0') AS OCAgenciaRecadudadora
	FROM	PagoEfectivo.OrdenPago OP
			INNER JOIN PagoEfectivo.Moneda M ON OP.IdMoneda = M.IdMoneda
			INNER JOIN PagoEfectivo.Parametro PA ON OP.IdEstado = PA.Id
			INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio
			INNER JOIN PagoEfectivo.EmpresaContratante EC ON EC.IdEmpresaContratante = S.IdEmpresaContratante
			INNER JOIN PagoEfectivo.Cliente C ON OP.IdCliente = C.IdCliente
			INNER JOIN PagoEfectivo.Usuario US ON US.IdUsuario = C.IdUsuario
			INNER JOIN PagoEfectivo.DetalleOrdenPago DOP ON OP.IdOrdenPago = DOP.IdOrdenPago
	WHERE	NumeroOrdenPago = RIGHT('0000000000000' + CONVERT(VARCHAR(14),@pstrNumeroOrdenPago),14) AND '' != @pstrNumeroOrdenPago
			AND OP.IdEstado = @pintIdEstado
			AND OP.IdEnteGenerador = 131 /*CLIENTE*/

	SET NOCOUNT OFF;
END
