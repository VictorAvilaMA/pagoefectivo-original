/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Realiza la Consulta de Pago de Servicios */    
/*             */
/* Creado x       : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fecha    : 04/11/2008                                            */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 04/11/2008                                             */    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarPagoServicios]
(
@pintIdTipoOrigenCancelacion INT,
@pintIdServicio INT,
@pstrAgencia VARCHAR(100),
@pstrCliente VARCHAR(100),
@pstrNroOrdenPago VARCHAR(14),
@pdtFechaInicio DATETIME ,
@pdtFechaFin DATETIME 

)
AS
BEGIN
SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	DECLARE @IdTipoMovimiento	INT,
			@TipoOrigenAB		INT,
			@TipoOrigenAR		INT,
			@TipoOrigenPOS		INT,
			@IdEstadoOP			INT,
			@IdEstadoMant		INT
	
	SET @IdTipoMovimiento= 34 -- Recepcionar OP
	SET @TipoOrigenAB = 1
	SET @TipoOrigenAR = 2
	SET @TipoOrigenPOS = 3
	SET @IdEstadoOP = 23
	SET @IdEstadoMant = 41


	SELECT 
		 OP.IdOrdenPago
		,OP.IdCliente
		,OP.IdEstado
		,OP.IdServicio
		,OP.IdMoneda
		,OP.NumeroOrdenPago
		,OP.FechaEmision
		,OP.FechaCancelacion
		,OP.Total
		,S.Nombre AS Servicio
		,U.Email
		,TOC.Descripcion AS DescripcionTipoOrigenCancelacion
		,M.dsmbmnd AS SimboloMoneda
		,CASE WHEN TOC.IdTipoOrigenCancelacion = @TipoOrigenAR THEN AR.NombreComercial
		  WHEN TOC.IdTipoOrigenCancelacion = @TipoOrigenAB THEN EAB.Descripcion
		  WHEN TOC.IdTipoOrigenCancelacion = @TipoOrigenPOS THEN EAB.RazonSocial
		 ELSE TOC.Descripcion 
		 END Agencia
	FROM PagoEfectivo.OrdenPago OP
	INNER JOIN PagoEfectivo.Servicio S ON OP.IdServicio = S.IdServicio 
	INNER JOIN PagoEfectivo.Cliente C ON OP.idCliente= C.IdCliente 
	INNER JOIN PagoEfectivo.Usuario U ON C.idUsuario=U.IdUsuario 
	INNER JOIN PagoEfectivo.Moneda M ON OP.idMoneda=M.IdMoneda 
	INNER JOIN PagoEfectivo.Movimiento MO ON MO.IdMovimiento = (SELECT TOP 1 IdMovimiento 
																FROM PagoEfectivo.Movimiento
																WHERE IdOrdenPago = OP.IdOrdenPago AND IdTipoMovimiento = @IdTipoMovimiento
																ORDER BY IdMovimiento  DESC) 
		  AND MO.IdTipoMovimiento = @IdTipoMovimiento
	INNER JOIN PagoEfectivo.TipoOrigenCancelacion TOC ON MO.IdTipoOrigenCancelacion = TOC.IdTipoOrigenCancelacion
	LEFT JOIN PagoEfectivo.AgenciaRecaudadora AR ON  AR.IdAgenciaRecaudadora =
				(SELECT TOP 1 AGR.IdAgenciaRecaudadora
				FROM PagoEfectivo.AgenciaRecaudadora AGR
				INNER JOIN PagoEfectivo.Caja C ON AGR.IdAgenciaRecaudadora = C.IdAgenciaRecaudadora
				INNER JOIN PagoEfectivo.AgenteCaja AC ON C.IdCaja =AC.IdCaja 
				INNER JOIN PagoEfectivo.Movimiento M ON AC.IdAgenteCaja = M.IdAgenteCaja
				WHERE  M.IdMovimiento = MO.IdMovimiento)
	LEFT JOIN  PagoEfectivo.AperturaOrigen AOR ON MO.IdAperturaOrigen = AOR.IdAperturaOrigen
	LEFT JOIN (SELECT ES.RazonSocial,AB.Descripcion,AO.IdAperturaOrigen
				FROM PagoEfectivo.AperturaOrigen AO  
				LEFT JOIN PagoEfectivo.Establecimiento ES ON AO.IdEstablecimiento = ES.IdEstablecimiento
				LEFT JOIN PagoEfectivo.AgenciaBancaria AB ON AO.IdAgenciaBancaria = AB.IdAgenciaBancaria
				) EAB
			ON AOR.IdAperturaOrigen = EAB.IdAperturaOrigen
	WHERE OP.IdEstado = @IdEstadoOP 
		  AND(@pintIdServicio=0 OR S.IdServicio=@pintIdServicio) 
		  AND (@pstrCliente='' OR U.Email LIKE @pstrCliente+'%' ) 
		  AND (@pstrNroOrdenPago ='' OR OP.NumeroOrdenPago = RIGHT('0000000000000' + CONVERT(VARCHAR(14),@pstrNroOrdenPago),14) )
		  AND (OP.FechaCancelacion >= CONVERT(DATETIME,@pdtFechaInicio,110) AND OP.FechaCancelacion < DATEADD(DD,1,CONVERT(DATETIME,@pdtFechaFin,110)))
		  AND (TOC.IdTipoOrigenCancelacion = @pintIdTipoOrigenCancelacion OR 0=@pintIdTipoOrigenCancelacion)
	      AND (@pstrAgencia ='' OR AR.NombreComercial LIKE CASE WHEN TOC.IdTipoOrigenCancelacion = @TipoOrigenAR THEN @pstrAgencia + '%'
									  ELSE ''
									  END)
		  AND (@pstrAgencia='' OR EAB.Descripcion LIKE CASE WHEN TOC.IdTipoOrigenCancelacion = @TipoOrigenAB THEN @pstrAgencia + '%'
									  ELSE ''
									  END )
		  AND (@pstrAgencia='' OR EAB.RazonSocial LIKE CASE WHEN TOC.IdTipoOrigenCancelacion = @TipoOrigenPOS THEN @pstrAgencia + '%'
									  ELSE ''
									  END )
    SET NOCOUNT OFF;  
END
