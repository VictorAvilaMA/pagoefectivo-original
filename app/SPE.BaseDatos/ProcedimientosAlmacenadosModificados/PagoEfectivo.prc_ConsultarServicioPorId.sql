/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio					            */    
/* Proyecto       : Pago Efectivo			                                */    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Consultar Servicio										*/    
/*																			*/
/* Creado x       : cmiranda@3devnet.com - Cesar Miranda                    */    
/* Fecha    : 27/10/2008													*/    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                    */    
/* Fec.Validacion : 27/10/2008												*/    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_ConsultarServicioPorId] --1

@IdServicio INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		S.IdServicio,
		S.IdEmpresaContratante,
		EP.RazonSocial  AS NombreEmpresaContrante,
		S.Nombre,
		S.Url,
		S.IdEstado,
		S.TiempoExpiracion,
		ISNULL(S.LogoImagen,'') LogoImagen,
		S.FechaCreacion,
		S.IdUsuarioCreacion
		,ISNULL(S.IdUsuarioActualizacion,0) AS IdUsuarioActualizacion
		,ISNULL(S.FechaActualizacion,'') AS FechaActualizacion
		,S.Url
		,ISNULL(S.IdTipoNotificacion,0) IdTipoNotificacion
		,ISNULL(S.UrlFTP,'') UrlFTP
		,ISNULL(S.Usuario,'') Usuario
		,ISNULL(S.Password,'') Password
		FROM PagoEfectivo.Servicio S
	JOIN PagoEfectivo.EmpresaContratante EP ON (S.IdEmpresaContratante=EP.IdEmpresaContratante)	
	WHERE S.IdServicio =@IdServicio

	SET NOCOUNT OFF;
END
