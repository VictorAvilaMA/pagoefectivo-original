set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Generacion de Código de Identificación de Pago */    
/*             */
/* Creado x       : apaitan@3devnet.com -Alex Paitan Quispe                                */    
/* Fecha    : 05/11/2008                                          */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion : 05/11/2008                                          */    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE  [PagoEfectivo].[prc_GenerarOrdenPago] 
	-- Add the parameters for the stored procedure here
(
	@pintIdEnteGenerador		INT,
	@pintUsuario				INT, 
	@pintIdServicio				INT, 
	@pintIdMoneda				INT, 
	@pdteFechaEmision			DATETIME, 
	@pdecTotal					DECIMAL(18,2),
	@strMerchantID				VARCHAR(20), 
	@pstrOrderIdComercio		VARCHAR(64),
	@pstrUrlOK					VARCHAR(100), 
	@pstrUrlError				VARCHAR(100),
	@pstrMailComercio			VARCHAR(100),
	@pintIdUsuarioCreacion		INT,
	@pstrUsuarioID				VARCHAR(50),
	@pstrDataAdicional			VARCHAR(200),
	@pstrUsuarioNombre			VARCHAR(50),
	@pstrUsuarioApellidos		VARCHAR(100),
	@pstrUsuarioDomicilio		VARCHAR(100),
	@pstrUsuarioLocalidad		VARCHAR(100),
	@pstrUsuarioProvincia		VARCHAR(100),
	@pstrUsuarioPais			VARCHAR(100),
	@pstrUsuarioAlias			VARCHAR(20)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @pintIdOrdenPago		INT,
			@pstrNumeroOrdenPago	VARCHAR(14),
			@pintIdCliente			INT,
			@EstadoOP				INT,
			@Existe					INT

	SET	@EstadoOP =22  --- estado Código de Identificación de Pago generada 
	IF not(ISNULL(@pstrOrderIdComercio,'')='')
	BEGIN
 	 SET @Existe = (select top 1  IdOrdenPago  from PagoEfectivo.OrdenPago where OrderIdComercio= @pstrOrderIdComercio)
	END
	ELSE
	BEGIN
     SET @Existe =NULL; 	
	END

	IF @Existe IS NULL 
		BEGIN 

			SET @pintIdCliente = (select TOP 1 IdCliente from PagoEfectivo.Cliente where IdUsuario = @pintUsuario)

			-- Insert statements for procedure here
			INSERT INTO PagoEfectivo.OrdenPago 
			(	IdEnteGenerador
				,IdCliente
				,IdServicio
				,IdEstado
				,IdMoneda
				,FechaEmision
				,Total
				,MerchantID
				,OrderIdComercio
				,UrlOK
				,UrlError
				,MailComercio
				,FechaCreacion
				,IdUsuarioCreacion
				,UsuarioID
				,DataAdicional
				,UsuarioNombre
				,UsuarioApellidos
				,UsuarioDomicilio
				,UsuarioLocalidad
				,UsuarioProvincia
				,UsuarioPais
				,UsuarioAlias
			)
			VALUES 
			(	@pintIdEnteGenerador
				,@pintIdCliente
				,@pintIdServicio
				,@EstadoOP
				,@pintIdMoneda
				,@pdteFechaEmision
				,@pdecTotal
				,@strMerchantID
				,@pstrOrderIdComercio
				,@pstrUrlOK
				,@pstrUrlError
				,@pstrMailComercio
				,GETDATE()
				,@pintIdCliente
				,@pstrUsuarioID 
				,@pstrDataAdicional 
				,@pstrUsuarioNombre 
				,@pstrUsuarioApellidos 
				,@pstrUsuarioDomicilio 
				,@pstrUsuarioLocalidad 
				,@pstrUsuarioProvincia 
				,@pstrUsuarioPais 
				,@pstrUsuarioAlias
			)

			SET	@pintIdOrdenPago = @@IDENTITY
			SET @pstrNumeroOrdenPago = RIGHT('00000000000000' + convert(varchar(14),@pintIdOrdenPago),14) 
			
			UPDATE  PagoEfectivo.OrdenPago
			SET		NumeroOrdenPago = @pstrNumeroOrdenPago
			WHERE	IdOrdenPago		= @pintIdOrdenPago

			SELECT @pintIdOrdenPago
		END 
	ELSE
		BEGIN
			SELECT 0 AS IdOrdenPago
		END 

	SET NOCOUNT OFF;
END










