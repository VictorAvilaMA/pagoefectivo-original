
/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo                                    */    
/* Sistema        : Pago Efectivo                            */    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : 	Registro de Empres Contratante*/    
/*             */
/* Creado x       :cvillanueva@3devnet.com - Christian Villanueva                                */    
/* Fecha    :  05/11/2008                                      */    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                                */    
/* Fec.Validacion :  05/11/2008                                         */    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_RegistrarEmpresaContratante]
	
	@pstrRazonSocial			VARCHAR(100), 
	@pstrRUC					CHAR(11), 
	@pstrContacto				VARCHAR(200), 
	@pstrTelefono				VARCHAR(15), 
	@pstrFax					VARCHAR(15),	 
	@pstrDireccion				VARCHAR(100), 
	@pstrSitioWeb				VARCHAR(100),
	@pstrEmail					VARCHAR(50), 
	@IdCiudad					INT,
	@pintEstado					INT,
	@pintIdUsuarioCreacion		INT,
	@pimgImagenLogo				IMAGE,
    @pstrOcultarEmpresa         VARCHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT PagoEfectivo.EmpresaContratante
	(RazonSocial
	,RUC
	,Contacto
	,Telefono
	,Fax
	,Direccion
	,SitioWeb
	,Email
	,IdCiudad
	,ImagenLogo
	,IdEstado
	,FechaCreacion
	,IdUsuarioCreacion
	,OcultarEmpresa)
	VALUES(@pstrRazonSocial 
	,@pstrRUC
	,@pstrContacto
	,@pstrTelefono 
	,@pstrFax
	,@pstrDireccion
	,@pstrSitioWeb
	,@pstrEmail
	,@IdCiudad
	,@pimgImagenLogo
	,@pintEstado
	,GETDATE()
	,@pintIdUsuarioCreacion
	,@pstrOcultarEmpresa)
	
		SET NOCOUNT OFF

END