/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Registro de Movimientos de una Caja por Agente	*/    
/*																			*/
/* Creado x       : kgranados@3devnet.com - Kleber Granados Diego            */    
/* Fecha		  : 23/10/2008													*/    
/* Validado x     : cmiranda@3devnet.com - Cesar Miranda                    */    
/* Fec.Validacion : 23/10/2008												*/    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_RegistrarMovimiento] 
	-- Add the parameters for the stored procedure here
	@pintIdOrdenPago				BIGINT,
	@pintIdAgenteCaja				INT,
	@pintIdMoneda					INT,
	@pintIdTipoMovimiento			INT,
	@pintIdMedioPago				INT,
	@pdecMonto						DECIMAL(18,2),
	@pdteFechaMovimiento			DATETIME,
	@pintIdUsuarioCreacion			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @IdTipoMovSaldoInicial INT,
			@IdTipoMovAnularOP INT,
			@IdTipoOrigenCancelacion INT

	--Saldo Inicial
	SET @IdTipoMovSaldoInicial = 33 --(select id from PagoEfectivo.Parametro where grupoCodigo='TMOV' AND Descripcion='Saldo Inicial')	
	--Anular OP'
	SET @IdTipoMovAnularOP = 35 --(select id from PagoEfectivo.Parametro where grupoCodigo='TMOV' AND Descripcion='Anular OP')	
	--Agencia Recaudadora
	SET @IdTipoOrigenCancelacion = 2


	IF 	@pintIdTipoMovimiento = @IdTipoMovSaldoInicial --SALDO INICIAL
		BEGIN
		SET		@pintIdOrdenPago = NULL
		END

	IF 	@pintIdTipoMovimiento = @IdTipoMovAnularOP --ANULACION DE UNA Código de Identificación de Pago
		BEGIN

		DECLARE @IdEstadoGenerarOP INT
		--'Generada'
		SELECT  @IdEstadoGenerarOP = 22--(select id from PagoEfectivo.Parametro where grupoCodigo='ESOP' AND Descripcion='Generada')	

		UPDATE	PagoEfectivo.OrdenPago
		SET		IdEstado = @IdEstadoGenerarOP,
				FechaAnulada = GETDATE(),
				FechaActualizacion = GETDATE(),
				IdUsuarioActualizacion = @pintIdUsuarioCreacion
		WHERE	IdOrdenPago = @pintIdOrdenPago
		END
	
    -- Insert statements for procedure here
	INSERT INTO PagoEfectivo.Movimiento
	(IdOrdenPago, IdAgenteCaja, IdMoneda, IdTipoMovimiento,	IdMedioPago, Monto, FechaMovimiento,
	 FechaCreacion, IdUsuarioCreacion, FechaActualizacion, IdUsuarioActualizacion, IdTipoOrigenCancelacion)

	VALUES (@pintIdOrdenPago,
	@pintIdAgenteCaja,
	@pintIdMoneda,
	@pintIdTipoMovimiento,
	@pintIdMedioPago,
	@pdecMonto,
	@pdteFechaMovimiento,	
	GETDATE(),
	@pintIdUsuarioCreacion,
	NULL,
	NULL,
	CASE @pintIdTipoMovimiento WHEN 34 THEN @IdTipoOrigenCancelacion WHEN 35 THEN @IdTipoOrigenCancelacion ELSE NULL END) --Solo cuando es recepcionar OP y Anular OP

	SELECT MAX(IdMovimiento) FROM	PagoEfectivo.Movimiento
    
    SET NOCOUNT OFF;

END
