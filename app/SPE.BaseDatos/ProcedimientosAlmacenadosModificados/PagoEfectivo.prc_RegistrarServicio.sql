/****************************************************************************/    
/* Empresa        : Empresa Editora el Comercio                             */    
/* Proyecto       : Pago Efectivo											*/    
/* Sistema        : Pago Efectivo											*/    
/* Modulo         : Web                                                     */    
/*--------------------------------------------------------------------------*/    
/* Objetivo       : Insertar Servicio										*/    
/*																			*/
/* Creado x       :cmiranda@3devnet.com - Cesar Miranda                     */    
/* Fecha		  :  27/10/2008												*/    
/* Validado x	  : cmiranda@3devnet.com - Cesar Miranda                    */    
/* Fec.Validacion :  27/10/2008												*/    
/*--------------------------------------------------------------------------*/
ALTER PROCEDURE [PagoEfectivo].[prc_RegistrarServicio] 
(
	@IdEmpresaContratante	INT ,
	@Nombre					VARCHAR(100),
	@IdEstado				INT,
	@TiempoExpiracion		DECIMAL(18,2),
	@LogoImagen				IMAGE,
	@IdUsuarioCreacion		INT,
	@Url					VARCHAR (250),
	@IdTipoNotificacion		INT,
	@UrlFTP					VARCHAR(100),
	@Usuario				VARCHAR(100),
	@Password				VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @IdServicio INT
	INSERT INTO PagoEfectivo.Servicio
			(IdEmpresaContratante,Nombre,Url,IdEstado,TiempoExpiracion,LogoImagen, FechaCreacion,
			 IdUsuarioCreacion, IdTipoNotificacion, UrlFTP, Usuario, Password)
	VALUES 
		(@idempresacontratante,
		@nombre,
		@Url,
		@idestado,
		@tiempoexpiracion,
		@logoimagen,
		getdate(),
		@idusuariocreacion,
		@IdTipoNotificacion,
		@UrlFTP,
		@Usuario,
		@Password)

	SELECT  @IdServicio = @@Identity
	SELECT  @IdServicio

	SET NOCOUNT OFF;
END

