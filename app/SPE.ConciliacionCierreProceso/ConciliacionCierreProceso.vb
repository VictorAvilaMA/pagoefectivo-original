Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Module ConciliacionCierreProceso
    Private oCConciliacion As CConciliacionCierre = New CConciliacionCierre
    Sub Main()
        Try
            DoStartRemotingServices()
            ProcesarConciliacion()
            '  Console.ReadLine()
        Catch ex As Exception
            Logger.Write(ex)
        End Try
    End Sub
    Private Sub DoStartRemotingServices()
        Try
            SPE.ConciliacionCierreProceso.RemoteServices.Instance = New SPE.ConciliacionCierreProceso.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try
        If Not EstablishConnection() Then
            Return
        End If
    End Sub
    Function EstablishConnection() As Boolean
        Return SPE.ConciliacionCierreProceso.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function

    Public Sub ProcesarConciliacion()
        Try
            oCConciliacion.ProcesarConciliacion()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Module
