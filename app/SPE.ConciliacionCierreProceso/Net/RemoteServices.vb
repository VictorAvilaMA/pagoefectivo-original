Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun
Public Class RemoteServices
    Inherits _3Dev.FW.Web.RemoteServices

    Private _IConciliacion As SPE.EmsambladoComun.IConciliacion
    Public Property IConciliacion() As SPE.EmsambladoComun.IConciliacion
        Get
            Return _IConciliacion
        End Get
        Set(ByVal value As SPE.EmsambladoComun.IConciliacion)
            _IConciliacion = value
        End Set
    End Property
    Public Overrides Sub DoSetupNetworkEnviroment()
        MyBase.DoSetupNetworkEnviroment()
        Me.IConciliacion = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IConciliacion), Me.ServerUrl + "/" + NombreServiciosConocidos.IConciliacion), SPE.EmsambladoComun.IConciliacion)
    End Sub
End Class