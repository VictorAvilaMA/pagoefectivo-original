Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Module ConciliacionProceso
    Private oCConciliacion As CConciliacion = New CConciliacion
    Sub Main()
        Try
            DoStartRemotingServices()
            ProcesarConciliacion()
            '  Console.ReadLine()
        Catch ex As Exception
            Logger.Write(ex)
        End Try
    End Sub
    Private Sub DoStartRemotingServices()
        Try
            SPE.ConciliacionProceso.RemoteServices.Instance = New SPE.ConciliacionProceso.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try
        If Not EstablishConnection() Then
            Return
        End If
    End Sub
    Function EstablishConnection() As Boolean
        Return SPE.ConciliacionProceso.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function

    Public Sub ProcesarConciliacion()
        Try
            oCConciliacion.ProcesarConciliacion()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Module
