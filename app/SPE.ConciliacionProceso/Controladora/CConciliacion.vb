Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports System.IO
Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Public Class CConciliacion
    Public Function ProcesarConciliacion() As Integer
        Dim request As New BEConciliacionRequest
        Dim oConArchivo As New BEConciliacionArchivo
        Dim lstConcArchivo As New List(Of BEConciliacionArchivo)
        Dim oConEntidad As New BEConciliacionEntidad
        Dim lstConcEntidad As New List(Of BEConciliacionEntidad)
        Dim response As New BEConciliacionResponse
        Try
            oConArchivo.IdTipoConciliacion = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.TiposConciliacion.Diaria
            oConArchivo.IdOrigenConciliacion = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.OrigenConciliacion.Automatizada
            lstConcArchivo.Add(oConArchivo)
            oConEntidad.ListaConcilacionesArchivo = lstConcArchivo
            lstConcEntidad.Add(oConEntidad)
            request.ListaConciliacionesEntidad = lstConcEntidad
            request.FechaConciliacion = DateTime.MinValue
            response = CType(SPE.ConciliacionProceso.RemoteServices.Instance, SPE.ConciliacionProceso.RemoteServices).IConciliacion.ProcesarConciliacion(request)
            Return response.IdConciliacion
        Catch ex As Exception
            Logger.Write(ex)
        End Try

    End Function
End Class
