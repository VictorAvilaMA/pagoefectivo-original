﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Discovery;
using _3Dev.FW.Entidades;
using SPE.EmsambladoComun;
using SPE.Entidades;

namespace SPE.ConsoleServer.Client
{
    class Program
    {
        static void Main(string[] args)
        {

            DiscoveryClient discovery = new DiscoveryClient(new UdpDiscoveryEndpoint());
            //FindCriteria criterio = new FindCriteria(typeof(SPE.EmsambladoComun.Seguridad.IUSuario));
            //FindCriteria criterio = new FindCriteria(typeof(SPE.EmsambladoComun.IAgenciaRecaudadora));
            FindCriteria criterio = new FindCriteria(typeof(SPE.EmsambladoComun.IEmpresaContratante));
            criterio.Duration = TimeSpan.FromSeconds(2);
            FindResponse findresponse = discovery.Find(criterio);

            if (findresponse.Endpoints.Count > 0)
            {
                var direccion = findresponse.Endpoints.First(p => p.Address.Uri.Scheme == "net.tcp").Address;
                
                //readerQuotas.MaxArrayLength = 25 * 1024;

                //new NetTcpBinding().ReaderQuotas.MaxDepth = 10;

                NetTcpBinding bindTcp = new NetTcpBinding
                {
                    OpenTimeout = new TimeSpan(1, 1, 20),
                    CloseTimeout = new TimeSpan(1, 1, 20),
                    SendTimeout = new TimeSpan(1, 1, 20),
                    TransactionFlow = false,
                    TransferMode = TransferMode.Buffered,
                    TransactionProtocol = TransactionProtocol.OleTransactions,
                    HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                    ListenBacklog = 10,
                    MaxBufferPoolSize = 2147483646,
                    MaxBufferSize = 2147483646,
                    MaxConnections = 10,
                    MaxReceivedMessageSize = 2147483646
                };

                bindTcp.ReaderQuotas.MaxDepth = 32;
                bindTcp.ReaderQuotas.MaxStringContentLength = 2147483647;
                bindTcp.ReaderQuotas.MaxArrayLength = 2147483647;
                bindTcp.ReaderQuotas.MaxBytesPerRead = 2147483647;
                bindTcp.ReaderQuotas.MaxNameTableCharCount = 16384; 

                var factory = new ChannelFactory<SPE.EmsambladoComun.IEmpresaContratante>(bindTcp, direccion);


                foreach (var operation in factory.Endpoint.Contract.Operations )
                {
                    operation.Behaviors.Find<DataContractSerializerOperationBehavior>().DataContractResolver = new EntityBaseContractResolver();
                }


                //var factory = new ChannelFactory<SPE.EmsambladoComun.IAgenciaRecaudadora>(new NetTcpBinding(), direccion);
                var proxy = factory.CreateChannel();


                BusinessEntityBase oEmpresaContrante = new BEEmpresaContratante();

                var itemList = proxy.SearchByParametersOrderedBy(oEmpresaContrante, "", true);
                //var item = proxy.ConsultarCajaActivaPorIdAgente(1);
                //var item = proxy.ConsultarAgentePorIdAgenteOrIdUsuario(1,1);

                Console.WriteLine("\n\n\n\nNº e Elementos de Lista: "+itemList.Count);
                foreach (var item in itemList)
                {
                    BEEmpresaContratante oEmpresa = (BEEmpresaContratante)item;
                    Console.WriteLine(oEmpresa.RazonSocial +" - "+oEmpresa.RUC);
                }

                
                //var resultado = ;

                ((ICommunicationObject)proxy).Close();
            }
            Console.ReadLine();


        }
    }
}
