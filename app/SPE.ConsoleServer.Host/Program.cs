﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using SPE.EmsambladoComun;
using SPE.ServidorRemoting;
using SPE.ServidorRemoting.Seguridad;

namespace SPE.ConsoleServer.Host
{

    class Program
    {
        static void Main(string[] args)
        {

            List<Type> Servicios = new List<Type>();
            
            Servicios.Add(typeof(EmailServer));
            Servicios.Add(typeof(SPEUsuarioServer));
            Servicios.Add(typeof(AgenciaBancariaServer));
            Servicios.Add(typeof(AgenciaRecaudadoraServer));
            Servicios.Add(typeof(BancoServer));
            Servicios.Add(typeof(CajaServer));
            Servicios.Add(typeof(ClienteServer));
            Servicios.Add(typeof(ComercioServer));
            Servicios.Add(typeof(ComunServer));
            Servicios.Add(typeof(ConciliacionServer));
            Servicios.Add(typeof(FTPArchivoServer));
            Servicios.Add(typeof(EmpresaContratanteServer));
            Servicios.Add(typeof(EstablecimientoServer));
            Servicios.Add(typeof(JefeProductoServer));
            Servicios.Add(typeof(PasarelaServer));
            Servicios.Add(typeof(PlantillaServer));
            Servicios.Add(typeof(PuntoVentaServer));
            Servicios.Add(typeof(ServicioNotificacionServer));
            Servicios.Add(typeof(OperadorServer));
            Servicios.Add(typeof(OrdenPagoServer));
            Servicios.Add(typeof(ParametroServer));
            Servicios.Add(typeof(RepresentanteServer));
            Servicios.Add(typeof(ServicioComunServer));
            Servicios.Add(typeof(ServicioServer));
            Servicios.Add(typeof(UbigeoServer));
            Servicios.Add(typeof(SeguridadServer));
            Servicios.Add(typeof(SPEMemberShipServer));
            Servicios.Add(typeof(SPERoleProviderServer));
            Servicios.Add(typeof(SPESiteMapServer));
            Servicios.Add(typeof(DineroVirtualServer));
            Servicios.Add(typeof(CuentaServer));
            Servicios.Add(typeof(ServicioInstitucionServer));
            Servicios.Add(typeof(ServicioMunicipalidadBarrancoServer));

            #region whitThread
            for (int i = 0; i < Servicios.Count; i++)
            {
                Type tipo = Servicios[i];
                Thread hilo = new Thread(delegate()
                   {
                       try
                       {

                           ServiceHost myServiceHostComunServer = null;
                           if (myServiceHostComunServer != null) { myServiceHostComunServer.Close(); }
                           myServiceHostComunServer = new ServiceHost(tipo);

                           //if (tipo == typeof(IEmail))
                           //{
                               
                           //}
                           //else
                           //{
                               foreach (var operation in myServiceHostComunServer.Description.Endpoints.First().Contract.Operations)
                                   operation.Behaviors.Find<DataContractSerializerOperationBehavior>().DataContractResolver = new EntityBaseContractResolver();

                           //}



                           myServiceHostComunServer.Faulted += new EventHandler(myServiceHostComunServer_Faulted);
                           myServiceHostComunServer.UnknownMessageReceived += new EventHandler<UnknownMessageReceivedEventArgs>(myServiceHostComunServer_UnknownMessageReceived);
                           myServiceHostComunServer.Opening += new EventHandler(myServiceHostComunServer_Opening);
                           myServiceHostComunServer.Opened += new EventHandler(myServiceHostComunServer_Opened);
                           myServiceHostComunServer.Closing += new EventHandler(myServiceHostComunServer_Closing);
                           myServiceHostComunServer.Closed += new EventHandler(myServiceHostComunServer_Closed);
                           myServiceHostComunServer.Open();


                           Console.WriteLine("Servicio " + tipo.Name + " OK");
                           Console.ReadLine();

                       }
                       catch (Exception ex)
                       {
                           Console.WriteLine("Servicio " + tipo.Name + " se achoro..." + ex.Message);
                           Console.ReadLine();
                       }
                   });
                hilo.Start();
                Thread.Sleep(10);
            }
            #endregion

            Console.ReadLine();
            Console.ReadLine();
        }

        static void myServiceHostComunServer_Closed(object sender, EventArgs e)
        {
            //Console.WriteLine("El Servicio Closed" + e.ToString());
        }

        static void myServiceHostComunServer_Closing(object sender, EventArgs e)
        {
            //Console.WriteLine("El Servicio Closing" + e.ToString());
        }

        static void myServiceHostComunServer_Opened(object sender, EventArgs e)
        {
            //Console.WriteLine("El Servicio Opened" + e.ToString());
        }

        static void myServiceHostComunServer_Opening(object sender, EventArgs e)
        {
            //Console.WriteLine("El Servicio Openining" + e.ToString());
        }

        static void myServiceHostComunServer_UnknownMessageReceived(object sender, UnknownMessageReceivedEventArgs e)
        {
            //Console.WriteLine(e.Message.ToString());
        }

        static void myServiceHostComunServer_Faulted(object sender, EventArgs e)
        {
            //Console.WriteLine("El Servicio Fallo" + e.ToString());
        }
    }
}
