<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1. Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

</style>
</head>
<body style="color: #6b6b6b; font-family: verdana; font-size: 12px;">
<div id="container" style="position:relative; min-height: 20px; overflow: hidden; width: 644px; padding-right: 125px; background: url(https://pagoefectivo.pe/images/handemail.jpg) no-repeat left bottom; padding-bottom: 120px; border: 1px solid #f8f7f6;">
	<table style="border:none; width: 644px; padding: 0; margin: 0;  background: url(https://pagoefectivo.pe/images/fondocorreo.jpg) no-repeat left top; padding-bottom: 15px;">
		<tbody>
			<tr>
				<td align="center" style="margin:0; padding:0;"><a href="https://pagoefectivo.pe" style="border:none;"><img src="https://pagoefectivo.pe/images/pagologo.jpg" alt="" /></a></td>
			</tr>
			<tr>
				<td align="center" style="margin:0; padding:0;"><h1 style="font-size:16px; line-height:24px;"> Creaci&oacute;n de [PuntoVentaTerminal] !</h1></td>				
			</tr>
			<tr>
				<td style="margin:0; padding:0;">
				
		<ul style="width: 595px; margin: 0px auto 10px; font-size: 12px; padding: 0; list-style: none;">
			<p style="display: block; position: relative;	min-height: 20px;	width: 600px;	overflow: hidden;	margin: 10px auto;"><b>[PuntoVenta]</b></li>			
			<li style="padding-left:10px;">Se ha creado un Terminal con n&uacute;mero de serie: [NroSerieTerminal] durante el proceso de: [Proceso]<br/>
			Por favor se debe completar los datos del Punto de venta y/o Terminal creado.</li>
		</ul>
				</td>
			</tr>
			<tr>
				<td style="margin:0; padding:0;">
					<ul style="width: 595px; min-height: 70px; margin: 0px auto 10px; padding: 20px 0; font-size: 12px; background: #fff; border: 1px solid #cdcdcd; box-shadow: inset 0px 0px 5px #cfcfcf;">
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> Fecha de creaci&oacute;n de la Agencia Bancaria: [FechaCreacion]</li>
																				
					</ul>
				</td>
			</tr>
			
			
		</body>
	</table>
	</div>
</body>

</html>