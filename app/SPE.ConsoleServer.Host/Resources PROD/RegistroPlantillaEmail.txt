            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1. Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <style type="text/css">
        html, body
        {
            color: #6b6b6b;
            font-family: verdana;
            font-size: 12px;
        }
        tr, td, h1, p
        {
            margin: 0;
            padding: 0;
        }
        h1
        {
            font-size: 16px;
            line-height: 24px;
        }
        h1 span
        {
            text-transform: uppercase;
        }
        p
        {
            display: block;
            position: relative;
            min-height: 20px;
            width: 600px;
            overflow: hidden;
            margin: 10px auto;
        }
        a.azul
        {
            display: block;
            width: 320px;
            height: 32px;
            line-height: 32px;
            overflow: hidden;
            margin: 20px auto;
            text-align: center;
            background: url(https://pagoefectivo.pe/images/botonazul.png) no-repeat left top;
            color: #fff;
            font-size: 19px;
        }
        p strong
        {
            font-weight: bold;
        }
        ul.no_separ
        {
            width: 595px;
            margin: 0px auto 10px;
            font-size: 12px;
            padding: 0;
            list-style: none;
        }
        ul.no_separ li
        {
            padding-left: 10px;
        }
        ul.no_separ li span
        {
            display: block;
            position: relative;
            float: left;
            margin-left: -10px;
        }
        a.linker
        {
            display: block;
            position: relative;
            width: 595px;
            margin: 0px auto 10px;
            font-size: 12px;
            text-indent: 10px;
            text-decoration: underline;
            color: #00aeef;
        }
        ul.lista
        {
            width: 595px;
            min-height: 70px;
            margin: 0px auto 10px;
            padding: 20px 0;
            font-size: 12px;
            background: #fff;
            border: 1px solid #cdcdcd;
            box-shadow: inset 0px 0px 5px #cfcfcf;
        }
        ul.lista li
        {
            display: block;
            width: 543px;
            font-size: 12px;
            margin-bottom: 10px;
            text-indent: 40px;
        }
        ul.lista li a
        {
            text-decoration: underline;
            color: #00aeef;
        }
        table tbody
        {
            margin: 0;
            padding: 0;
        }
        * + html table#main-tb
        {
            width: 768px !important;
        }
        .ReadMsgBody
        {
            width: 100%;
        }
        .ExternalClass
        {
            width: 100%;
        }
        table
        {
            background-repeat: no-repeat !important;
        }
    </style>
</head>
<body style="color: #6b6b6b; font-family: verdana; font-size: 12px; margin: 0; padding: 0;">
    <table name="main-tb" id="main-tb" border="0" cellpadding="0" cellspacing="0" background="https://pagoefectivo.pe/images/handemail.jpg"
        style="background: url(https://pagoefectivo.pe/images/handemail.jpg) no-repeat left top;
        position: relative; height: 573px; width: 644px; padding-right: 125px; padding-bottom: 120px;">
        <tr>
            <td align="left" valign="top">
                <table border="0" align="left" cellpadding="0" cellspacing="0" background="https://pagoefectivo.pe/images/fondocorreo.jpg"
                    style="background: url(https://pagoefectivo.pe/images/fondocorreo.jpg) no-repeat left top;
                    border: none; width: 644px; padding: 0; margin: 0; padding-bottom: 15px;">
                    <tbody>
                        <tr>
                            <td align="center" style="margin: 0; padding: 0;">
                                <a href="https://pagoefectivo.pe" style="border: none;">
                                    <img src="https://pagoefectivo.pe/images/pagologo.jpg" alt="" border="0" /></a>
                            </td>
                        </tr>
                        <tr>                            
							<td align="center" style="margin:0; padding:0;">
								<h1 style="font-size:16px; line-height:24px;">
								Hola <b>[Nombre]</b></h1></td>				
                        </tr>
                        <tr>
                           <td style="margin:0; padding:0;"><p style="display: block; position: relative;	min-height: 20px;	width: 600px;	overflow: hidden;	margin: 10px auto;">Para completar tu registro en PAGO EFECTIVO, s&oacute;lo falta que hagas click en el link de abajo y estar&aacute;s habilitado para poder realizar pagos en nuestro sitio.</p></td>
                        </tr>
						<tr>
                           <td style="margin:0; padding:0;"><a	href="https://pagoefectivo.pe/ValidaRegistro.aspx?Email=[cuenta]&Valida=[codigoregistro]" style="display: block;	width: 320px;	height: 32px;	line-height: 32px;	overflow:hidden;	margin: 20px auto;	text-align: center;	background: url(https://pagoefectivo.pe/images/botonazul.png) no-repeat left top;	color: #fff;	font-size: 19px;">&iexcl;Confirma tu registro ahora!</a></td>
                        </tr>
                        <tr>
                            <td style="margin:0; padding:0;">
				
		<ul style="width: 595px; margin: 0px auto 10px; font-size: 12px; padding: 0; list-style: none;">
			<p style="display: block; position: relative;	min-height: 20px;	width: 600px;	overflow: hidden;	margin: 10px auto;"><b>Importante</b></li>			
			<li style="padding-left:10px;">Al hacer click en el link de arriba estaremos verificando que tu e-mail este funcionando.</li>
			<li style="padding-left:10px;">En caso de error al hacer click en el link &quot;Confirma tu registro ahora &quot; , copia y pega en tu navegador el link que te damos a continuaci&oacute;n:</li>
		</ul>
		<a href="https://pagoefectivo.pe/ValidaRegistro.aspx" style="display: block; position: relative; width: 595px; margin: 0px auto 10px; font-size: 12px; text-indent: 10px; text-decoration: underline; color: #00aeef;">https://pagoefectivo.pe/ValidaRegistro.aspx</a>
		<ul style="width: 595px; margin: 0px auto 10px; font-size: 12px; padding: 0; list-style: none;">
			<li style="padding-left:10px;">Una vez que haya aparecido la nueva pantalla, porfavor ingresa la siguiente informaci&oacute;n:</li>
		</ul>
				</td>
			</tr>
			<tr>
				<td style="margin:0; padding:0;">
					<ul style="width: 595px; min-height: 70px; margin: 0px auto 10px; padding: 20px 0; font-size: 12px; background: #fff; border: 1px solid #cdcdcd; box-shadow: inset 0px 0px 5px #cfcfcf;">
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> 1. Tu cuenta: [cuenta] </li>
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> 2. Tu clave de registro:  [contrasenia]</li>							
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> 3. Tu c&oacute;digo de registro:  [codigoregistro]</li>							
																				
					</ul>
				</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
