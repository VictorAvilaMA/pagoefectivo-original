<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1. Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

</style>
</head>
<body style="color: #6b6b6b; font-family: verdana; font-size: 12px;">
<div id="container" style="position:relative; min-height: 20px; overflow: hidden; width: 644px; padding-right: 125px; background: url(http://pre.pagoefectivo.pe/images/handemail.jpg) no-repeat left bottom; padding-bottom: 120px; border: 1px solid #f8f7f6;">
	<table style="border:none; width: 644px; padding: 0; margin: 0;  background: url(http://pre.pagoefectivo.pe/images/fondocorreo.jpg) no-repeat left top; padding-bottom: 15px;">
		<tbody>
			<tr>
				<td align="center" style="margin:0; padding:0;"><a href="http://pre.pagoefectivo.pe" style="border:none;"><img src="http://pre.pagoefectivo.pe/images/pagologo.jpg" alt="" /></a></td>
			</tr>
			<tr>
				<td align="center" style="margin:0; padding:0;"><h1 style="font-size:16px; line-height:24px;">Hola <b>[Nombre]</b></h1></td>				
			</tr>
			<tr>
				<td style="margin:0; padding:0;"><p style="display: block; position: relative;	min-height: 20px;	width: 600px;	overflow: hidden;	margin: 10px auto;">Para completar tu registro en PAGO EFECTIVO, s&oacute;lo falta que hagas click en el link de abajo y estar&aacute;s habilitado para poder realizar pagos en nuestro sitio.</p></td>
			</tr>
			<tr>
				<td style="margin:0; padding:0;"><a	href="http://pre.pagoefectivo.pe/ValidaRegistro.aspx?Email=[cuenta]&Valida=[codigoregistro]" style="display: block;	width: 320px;	height: 32px;	line-height: 32px;	overflow:hidden;	margin: 20px auto;	text-align: center;	background: url(http://pre.pagoefectivo.pe/images/botonazul.png) no-repeat left top;	color: #fff;	font-size: 19px;">&iexcl;Confirma tu registro ahora!</a></td>
			</tr>
			<tr>
			<tr>
				<td style="margin:0; padding:0;">
				
		<ul style="width: 595px; margin: 0px auto 10px; font-size: 12px; padding: 0; list-style: none;">
			<p style="display: block; position: relative;	min-height: 20px;	width: 600px;	overflow: hidden;	margin: 10px auto;"><b>Importante</b></li>			
			<li style="padding-left:10px;">Al hacer click en el link de arriba estaremos verificando que tu e-mail este funcionando.</li>
			<li style="padding-left:10px;">En caso de error al hacer click en el link &quot;Confirma tu registro ahora &quot; , copia y pega en tu navegador el link que te damos a continuaci&oacute;n:</li>
		</ul>
		<a href="http://pre.pagoefectivo.pe/ValidaRegistro.aspx" style="display: block; position: relative; width: 595px; margin: 0px auto 10px; font-size: 12px; text-indent: 10px; text-decoration: underline; color: #00aeef;">http://pre.pagoefectivo.pe/ValidaRegistro.aspx</a>
		<ul style="width: 595px; margin: 0px auto 10px; font-size: 12px; padding: 0; list-style: none;">
			<li style="padding-left:10px;">Una vez que haya aparecido la nueva pantalla, porfavor ingresa la siguiente informaci&oacute;n:</li>
		</ul>
				</td>
			</tr>
			<tr>
				<td style="margin:0; padding:0;">
					<ul style="width: 595px; min-height: 70px; margin: 0px auto 10px; padding: 20px 0; font-size: 12px; background: #fff; border: 1px solid #cdcdcd; box-shadow: inset 0px 0px 5px #cfcfcf;">
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> 1. Tu cuenta: [cuenta] </li>
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> 2. Tu clave de registro:  [contrasenia]</li>							
							<li style="display: block; width: 543px; font-size: 12px; margin-bottom: 10px; text-indent: 40px;"> 3. Tu c&oacute;digo de registro:  [codigoregistro]</li>							
																				
					</ul>
				</td>
			</tr>
			
			
		</body>
	</table>
	</div>
</body>

</html>