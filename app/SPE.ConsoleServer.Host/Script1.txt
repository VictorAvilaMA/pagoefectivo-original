﻿		<service name="SPE.ServidorRemoting.ComunServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IComun" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ComunServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.EmailServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IEmail" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/EmailServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.AgenciaBancariaServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IAgenciaBancaria" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/AgenciaBancariaServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.AgenciaRecaudadoraServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IAgenciaRecaudadora" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/AgenciaRecaudadoraServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.BancoServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IBanco" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/BancoServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.CajaServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ICaja" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/CajaServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ClienteServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ICliente" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ClienteServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ComercioServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IComercio" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ComercioServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ComunServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IComun" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ComunServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ConciliacionServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IConciliacion" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ConciliacionServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.FTPArchivoServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IFTPArchivo" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/FTPArchivoServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.EmpresaContratanteServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IEmpresaContratante" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/EmpresaContratanteServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.EstablecimientoServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IEstablecimiento" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/EstablecimientoServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.JefeProductoServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IJefeProducto" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/JefeProductoServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.PasarelaServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IPasarela" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/PasarelaServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.PlantillaServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IPlantilla" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/PlantillaServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.PuntoVentaServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IPuntoVenta" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/PuntoVentaServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ServicioNotificacionServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IServicioNotificacion" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ServicioNotificacionServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.OperadorServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IOperador" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/OperadorServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.OrdenPagoServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IOrdenPago" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/OrdenPagoServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ParametroServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IParametro" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ParametroServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.RepresentanteServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IRepresentante" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/RepresentanteServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ServicioComunServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IServicioComun" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ServicioComunServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.ServicioServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IServicio" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/ServicioServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.UbigeoServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.IUbigeo" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/UbigeoServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.SeguridadServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ISeguridad" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/SeguridadServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.SPEMemberShipServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ISPEMemberShip" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/SPEMemberShipServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.SPERoleProviderServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ISPERoleProvider" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/SPERoleProviderServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.SPESiteMapServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ISPESiteMap" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/SPESiteMapServer" /></baseAddresses></host>
      </service>
		
			<service name="SPE.ServidorRemoting.SPEUsuarioServer" behaviorConfiguration="NewBehavior0">
        <endpoint address="" binding="netTcpBinding" contract="SPE.EmsambladoComun.ISPEUsuario" >
          <identity><dns value="localhost" /></identity>
        </endpoint>

        <endpoint kind="udpDiscoveryEndpoint" />
        <host><baseAddresses><add baseAddress="net.tcp://localhost:667/SPEUsuarioServer" /></baseAddresses></host>
      </service>
		
	