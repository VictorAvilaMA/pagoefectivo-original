﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SPE.Criptography
{
    public class Encripter
    {
        #region Attributes
        private RSAHelper RSAHelper = new RSAHelper();
        private TripleDesHelper TripleDesHelper = new TripleDesHelper();
        public Byte[] clavePublicaContraparte 
        { 
            set
            {
                RSAHelper.clavePublicaContraparte= value;
            }
        }
        public Byte[] clavePrivada
        {
            set
            {
                RSAHelper.clavePrivada = value;
            }
        }
        #endregion
        #region Metodos Sin enviar rutaClavePublicaPrivada
        public Byte[] GenerarKeyPub(string codEntidad)
        {
            return RSAHelper.GenerarKPub(codEntidad);
        }
        public Byte[] GenerarKeyPriv(string codEntidad)
        {
            return RSAHelper.GenerarKPriv(codEntidad);
        }
        public String[] Encriptar(string textoAEncriptar)
        {
            return TripleDesHelper.CifrarMensaje3DES(textoAEncriptar);
        }
        public String DesEncriptar(string textoEncriptado, string key)
        {
            return TripleDesHelper.DesCifrarMensaje3DES(textoEncriptado, key);
        }
        public String Firmar(string textoAFirmar)
        {
            return RSAHelper.SignMessage(textoAFirmar);
        }
        public bool ValidarFirma(string textoAFirmar,string textoFirmado)
        {
            return RSAHelper.ValidSignedMessage(textoAFirmar, textoFirmado);
        }
        public String[] Cifrar(string mensajeACifrar)
        {
            //HtmlEncode para el uso de caracteres extranios
            mensajeACifrar = HttpUtility.HtmlEncode(mensajeACifrar);
            String[] ArrayResult = new String[2];
            String[] MensajeEncriptado= new String[2];
            MensajeEncriptado = Encriptar(mensajeACifrar);
            ArrayResult[0] = MensajeEncriptado[0];
            ArrayResult[1] = RSAHelper.CifrarCadena(ByteUtil.StringHexToByteArray(MensajeEncriptado[1]));
            return ArrayResult;
        }
        public String DesCifrar(string mensajeEncriptado, string keyCifrado)
        {
            String key = RSAHelper.DesCifrarCadena(keyCifrado);
            String MensajeDesCifrado = DesEncriptar(mensajeEncriptado, key);
            //HtmlDecode para el uso de caracteres extranios
            MensajeDesCifrado = HttpUtility.HtmlDecode(MensajeDesCifrado);
            return MensajeDesCifrado;
        }
        #endregion
        #region Metodos enviando rutaClavePublicaPrivada
        public static string EncryptText(string plainText, string pathPublicKey)
        {
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePublicaContraparte = ByteUtil.FileToByteArray(pathPublicKey);
            String[] textEncript = encripter.Cifrar(plainText);
            return textEncript[0] + "|" + textEncript[1];
        }
        public static string DecryptText(string encryptText, string pathPrivateKey)
        {
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePrivada = ByteUtil.FileToByteArray(pathPrivateKey);
            String[] textEncript = encryptText.Split('|');
            string aaaa = encripter.DesCifrar(textEncript[0], textEncript[1]);
            return aaaa;
        }
        public static string Signer(string plainText, string pathPrivateKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePrivada = ByteUtil.FileToByteArray(pathPrivateKey);
            return encripter.Firmar(plainText);
        }
        public static bool SignerVal(string plainText, string signerText, string pathPublicKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePublicaContraparte = ByteUtil.FileToByteArray(pathPublicKey);
            return encripter.ValidarFirma(plainText, signerText);
        }
        #endregion
    }
}
