Public Class CIPComun
    Implements IDisposable


    Public Sub New()

    End Sub

    Public Function GenerarCodigoBarra(ByVal cip As String, ByVal capi As String, ByVal cclave As String) As String
        Dim str As String = String.Format("cip={0}|capi={1}|cclave={2}", cip, capi, cclave)
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Dim querystring As String = clsEncripta.Encrypt(str, clsEncripta.fEncriptaKey)
        Return querystring
    End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
