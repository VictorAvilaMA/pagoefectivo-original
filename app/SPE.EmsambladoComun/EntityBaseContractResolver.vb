﻿Imports System.Xml
Imports System.Runtime.Serialization
Imports System.Linq

Public Class EntityBaseContractResolver
    Inherits DataContractResolver

    Const ResolverNamespace As String = "SPE.Entidades"
    Public Overrides Function ResolveName(typeName As String, typeNamespace As String, declaredType As Type, knownTypeResolver As DataContractResolver) As Type
        If typeNamespace = ResolverNamespace Then
            Dim result As Type = Nothing
            result = GetTypeEntity(typeName)
            If result IsNot Nothing Then
                Return result
            End If
        End If


        Return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, Nothing)
    End Function

    Public Overrides Function TryResolveType(type As Type, declaredType As Type, knownTypeResolver As DataContractResolver, ByRef typeName As XmlDictionaryString, ByRef typeNamespace As XmlDictionaryString) As Boolean
   
            If type.Assembly.GetName().Name = ResolverNamespace Then
                Dim dic As New XmlDictionary()
                typeName = dic.Add(type.Name)
                typeNamespace = dic.Add(type.[Namespace])
                Return True
            Else
                Return knownTypeResolver.TryResolveType(type, declaredType, Nothing, typeName, typeNamespace)
            End If

    End Function

    Private Shared Function GetTypeEntity(ByVal TypeName As String) As Type

        Try
            Dim xxx = System.Reflection.Assembly.GetAssembly(GetType(SPE.Entidades.BEAuditoria)).GetExportedTypes().ToList().Find(Function(x) x.Name.Equals(TypeName))
            Return xxx
        Catch ex As Exception
            Return Nothing
        End Try



        'Select Case TypeName
        '    Case "BEEmpresaContratante"
        '        Return GetType(SPE.Entidades.BEEmpresaContratante)
        'End Select


    End Function
End Class
