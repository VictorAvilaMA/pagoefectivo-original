Imports Microsoft.VisualBasic
Imports System.Xml
Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Util


Public Class GeneradorXmlOP
    Implements IDisposable



    Sub New()

    End Sub

    Public Function ObtenerOrdenPagoDeXMLString(ByVal xml As String) As BEOrdenPago
        Dim _xmldoc As New XmlDocument
        Dim objOrdenPago As BEOrdenPago = New BEOrdenPago
        Dim objDetalleOrden As BEDetalleOrdenPago
        objOrdenPago.DetallesOrdenPago = New List(Of BEDetalleOrdenPago)


        _xmldoc.LoadXml(xml)
        Dim ordenPago As System.Xml.XmlNode = _xmldoc.SelectSingleNode("ordenPago")

        Dim IdOrdenPago As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdOrdenPago")
        objOrdenPago.IdOrdenPago = DataUtil.StringToInt(IdOrdenPago.InnerText)

        '********API************'
        Dim claveAPI As System.Xml.XmlNode = ordenPago.SelectSingleNode("ClaveAPI")
        If Not (claveAPI Is Nothing) Then
            objOrdenPago.ClaveAPI = DataUtil.StringToInt(claveAPI.InnerText)
        Else
            objOrdenPago.ClaveAPI = ""
        End If


        Dim claveSecreta As System.Xml.XmlNode = ordenPago.SelectSingleNode("ClaveSecreta")
        If Not (claveSecreta Is Nothing) Then
            objOrdenPago.ClaveSecreta = DataUtil.StringToInt(claveSecreta.InnerText)
        Else
            objOrdenPago.ClaveSecreta = ""
        End If

        '***********************'

        Dim IdEstado As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdEstado")
        If (IdEstado IsNot Nothing) Then
            objOrdenPago.IdEstado = DataUtil.StringToInt(IdEstado.InnerText)
        End If

        Dim IdServicio As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdServicio")
        If (IdServicio IsNot Nothing) Then
            objOrdenPago.IdServicio = DataUtil.StringToInt(IdServicio.InnerText)
        End If

        Dim IdMoneda As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdMoneda")
        If (IdMoneda IsNot Nothing) Then
            objOrdenPago.IdMoneda = DataUtil.StringToInt(IdMoneda.InnerText)
        End If

        Dim NumeroOrdenPago As System.Xml.XmlNode = ordenPago.SelectSingleNode("NumeroOrdenPago")
        If (NumeroOrdenPago IsNot Nothing) Then
            objOrdenPago.NumeroOrdenPago = NumeroOrdenPago.InnerText.ToString()
        End If


        Dim Total As System.Xml.XmlNode = ordenPago.SelectSingleNode("Total")
        If (Total IsNot Nothing) Then
            objOrdenPago.Total = Convert.ToDecimal(Total.InnerText)
        End If

        Dim MerchantId As System.Xml.XmlNode = ordenPago.SelectSingleNode("MerchantId")
        If (MerchantId IsNot Nothing) Then
            objOrdenPago.MerchantID = MerchantId.InnerText
        End If

        Dim OrdenIdComercio As System.Xml.XmlNode = ordenPago.SelectSingleNode("OrdenIdComercio")
        If (OrdenIdComercio IsNot Nothing) Then
            objOrdenPago.OrderIdComercio = OrdenIdComercio.InnerText
        End If

        Dim UrldOk As System.Xml.XmlNode = ordenPago.SelectSingleNode("UrldOk")
        If (UrldOk IsNot Nothing) Then
            objOrdenPago.UrlOk = UrldOk.InnerText
        End If

        Dim UrlError As System.Xml.XmlNode = ordenPago.SelectSingleNode("UrlError")
        If (UrlError IsNot Nothing) Then
            objOrdenPago.UrlError = UrlError.InnerText
        End If

        Dim MailComercio As System.Xml.XmlNode = ordenPago.SelectSingleNode("MailComercio")
        If (MailComercio IsNot Nothing) Then
            objOrdenPago.MailComercio = UrlError.InnerText
        End If


        Dim UsuarioId As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioId")
        If (UsuarioId IsNot Nothing) Then
            objOrdenPago.UsuarioID = UsuarioId.InnerText
        End If

        Dim DataAdicional As System.Xml.XmlNode = ordenPago.SelectSingleNode("DataAdicional")
        If (DataAdicional IsNot Nothing) Then
            objOrdenPago.DataAdicional = UsuarioId.InnerText
        End If

        Dim UsuarioNombre As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioNombre")
        If (UsuarioNombre IsNot Nothing) Then
            objOrdenPago.UsuarioNombre = UsuarioNombre.InnerText
        End If

        Dim UsuarioApellidos As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioApellidos")
        If (UsuarioApellidos IsNot Nothing) Then
            objOrdenPago.UsuarioApellidos = UsuarioApellidos.InnerText
        End If

        Dim UsuarioLocalidad As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioLocalidad")
        If (UsuarioLocalidad IsNot Nothing) Then
            objOrdenPago.UsuarioLocalidad = UsuarioLocalidad.InnerText
        End If

        Dim UsuarioProvincia As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioProvincia")
        If (UsuarioProvincia IsNot Nothing) Then
            objOrdenPago.UsuarioProvincia = UsuarioProvincia.InnerText
        End If

        Dim UsuarioPais As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioPais")
        If (UsuarioPais IsNot Nothing) Then
            objOrdenPago.UsuarioPais = UsuarioPais.InnerText
        End If

        Dim UsuarioAlias As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioAlias")
        If (UsuarioAlias IsNot Nothing) Then
            objOrdenPago.UsuarioAlias = ordenPago.InnerText
        End If

        Dim UsuarioEmail As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioEmail")
        If (UsuarioEmail IsNot Nothing) Then
            objOrdenPago.UsuarioEmail = ordenPago.InnerText
        End If



        Dim Detalles As System.Xml.XmlNode = ordenPago.SelectSingleNode("Detalles")
        Dim list As XmlNodeList

        list = Detalles.SelectNodes("Detalle")

        For Each Node As XmlNode In list

            objDetalleOrden = New BEDetalleOrdenPago()
            Dim Cod_Origen As System.Xml.XmlNode = Node.SelectSingleNode("Cod_Origen")
            If (Cod_Origen IsNot Nothing) Then
                objDetalleOrden.codOrigen = Cod_Origen.InnerText
            End If

            Dim TipoOrigen As System.Xml.XmlNode = Node.SelectSingleNode("TipoOrigen")
            If (TipoOrigen IsNot Nothing) Then
                objDetalleOrden.tipoOrigen = TipoOrigen.InnerText
            End If

            Dim ConceptoPago As System.Xml.XmlNode = Node.SelectSingleNode("ConceptoPago")
            If (ConceptoPago IsNot Nothing) Then
                objDetalleOrden.ConceptoPago = ConceptoPago.InnerText
            End If

            Dim Importe As System.Xml.XmlNode = Node.SelectSingleNode("Importe")
            If (Importe IsNot Nothing) Then
                objDetalleOrden.Importe = Convert.ToDecimal(Importe.InnerText)
            End If

            Dim Campo1 As System.Xml.XmlNode = Node.SelectSingleNode("Campo1")
            If (Campo1 IsNot Nothing) Then
                objDetalleOrden.campo1 = Campo1.InnerText
            End If

            Dim Campo2 As System.Xml.XmlNode = Node.SelectSingleNode("Campo2")
            If (Campo2 IsNot Nothing) Then
                objDetalleOrden.campo2 = Campo2.InnerText
            End If

            Dim Campo3 As System.Xml.XmlNode = Node.SelectSingleNode("Campo3")
            If (Campo3 IsNot Nothing) Then
                objDetalleOrden.campo3 = Campo3.InnerText
            End If

            objOrdenPago.DetallesOrdenPago.Add(objDetalleOrden)
        Next


        Return objOrdenPago


    End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
