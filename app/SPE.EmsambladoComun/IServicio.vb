Imports System.ServiceModel
Imports System
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

<ServiceContract()>
Public Interface IServicio
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase


    <OperationContract()>
    Function ConsultarServicioFormularioUrl(ByVal IdServicio As Integer, ByVal URL As String) As SPE.Entidades.BEServicio

    <OperationContract()>
    Function ConsultarServicioPorUrl(ByVal parametro As Object) As SPE.Entidades.BEServicio
    <OperationContract()>
    Function ConsultarPagoServicios(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ConsultarPagoServiciosNoRepresentante(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ConsultarServiciosPorIdUsuario(ByVal obeServicio As BEServicio) As List(Of BEServicio)



#Region "ServicioInstitucion"
    <OperationContract()>
    Function ProcesarArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As BEServicioInstitucion
    <OperationContract()>
    Function ValidarCarga(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function MostrarResumenArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarDetallePagoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarInstitucionCIP(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function ConsultarArchivosDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarDetalleArchivoDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarCIPDocumento(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function DeterminarIdParametro(ByVal request As BEServicioInstitucionRequest) As Integer
#End Region

#Region "ServicioMunicipalidadBarranco"

    <OperationContract()>
    Function ObtenerServicioMunicipalidadBarrancoPorId(ByVal id As Integer) As BEServicio
    <OperationContract()>
    Function ProcesarArchivoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As BEServicioMunicipalidadBarranco
    <OperationContract()>
    Function ValidarCargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function MostrarResumenArchivoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarDetallePagoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarMunicipalidadBarrancoCIP(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ActualizarDocumentosCIPMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function ActualizarDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function ConsultarArchivosDescargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarDetalleArchivoDescargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarCIPDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function DeterminarIdParametroMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
#End Region


#Region "Servicio Desconectado"
    <OperationContract()>
    Function RecuperarDatosProductoServicio(ByVal obeServicio As BEServicioProductoRequest) As List(Of BEServicioProducto)
    <OperationContract()>
    Function RecuperarDatosServicioDesconectado(ByVal intID As Integer) As BEServicio
    <OperationContract()>
    Function RecuperarDetalleProductoServicio(ByVal obeServicio As BEServicioProductoRequest) As BEServicioProducto
#End Region


#Region "Configuracion de Servicios"
    <OperationContract()>
    Function ConsultarContratoXMLDeServicios(ByVal codigoAcceso As String) As String
    <OperationContract()>
    Function GuardarContratoXMLDeServicios(ByVal codigoAcceso As String, ByVal contratoXml As String) As String
#End Region

    <OperationContract()>
    Function ValidarServicioPorAPIYClave(ByVal cAPI As String, ByVal cClave As String) As Boolean
    <OperationContract()>
    Function ConsultarServicioPorAPI(ByVal cAPI As String) As BEServicio
    <OperationContract()>
    Function ConsultarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ActualizaServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    <OperationContract()>
    Function ConsultarTransaccionesxServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio
    <OperationContract()>
    Function ConsultarTransaccionesxServicioLote(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio
    '<add Peru.com FaseIII>
    <OperationContract()>
    Function UpdateServicioContratoXML(ByVal oBEServicio As BEServicio) As Boolean
    <OperationContract()>
    Function RegistrarAsociacionCuentaServicio(ByVal be As BEServicioBanco) As Long
    <OperationContract()>
    Function ActualizarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    <OperationContract()>
    Function ConsultarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    '<upd Proc.Tesoreria>
    <OperationContract()>
    Function ConsultarCodigosServiciosPorBanco(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ObtenerAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BEServicioBanco
    <OperationContract()>
    Function EliminarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Boolean
    <OperationContract()>
    Function ObtenerServicioComisionporID(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
    <OperationContract()>
    Function ConsultarDetalleTransaccionesxServicio(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)
    <OperationContract()>
    Function ConsultarDetalleTransaccionesxServicioSaga(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaSaga)
    <OperationContract()>
    Function ConsultarDetalleTransaccionesxServicioRipley(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaRipley)
    <OperationContract()>
    Function ConsultarDetalleTransaccionesPendientes(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)
    <OperationContract()>
    Function RegistrarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    <OperationContract()>
    Function ConsultarServicioPorIdEmpresa(ByVal objbeservicio As BEServicio) As List(Of BEServicio)
    <OperationContract()>
    Function ConsultarPorAPIYClave(ByVal CAPI As String, ByVal CClave As String) As BEServicio
    <OperationContract()>
    Function ConsultarComerciosAfiliados(ByVal visibleEnPortada As Boolean, ByVal proximoAfiliado As Boolean) As List(Of BEServicio)
    <OperationContract()>
    Function ObtenerServicioPorIdWS(ByVal id As Integer) As BEServicio
    <OperationContract()>
    Function ConsultarServicioPorClasificacion(ByVal objbeservicio As BEServicio) As List(Of BEServicio)
    <OperationContract()>
    Function ObtenerServicioInstitucionPorId(ByVal id As Integer) As BEServicio
    <OperationContract()>
    Function RegistrarOrderIdComercio(ByVal codigo As String) As Integer



End Interface
