Imports System

Public Class NombreServiciosConocidos
    '
    Public Const IAgenciaRecaudadora As String = "IAgenciaRecaudadora.rem"
    Public Const ICliente As String = "ICliente.rem"
    Public Const IServicio As String = "IServicio.rem"
    Public Const IEmpresaContratante As String = "IEmpresaContratante.rem"
    Public Const IParametro As String = "IParametro.rem"
    Public Const IUbigeo As String = "IUbigeo.rem"
    Public Const IRepresentante As String = "IRepresentante.rem"

    Public Const IComun As String = "IComun.rem"
    Public Const IOrdenPago As String = "IOrdenPago.rem"
    Public Const ICaja As String = "ICaja.rem"
    Public Const IEmail As String = "IEmail.rem"
    'FASE II
    Public Const IAgenciaBancaria As String = "IAgenciaBancaria.rem"
    Public Const IOperador As String = "IOperador.rem"
    Public Const IEstablecimiento As String = "IEstablecimiento.rem"
    Public Const IComercio As String = "IComercio.rem"
    '
    Public Const IBanco As String = "IBanco.rem"
    Public Const IJefeProducto As String = "IJefePeroducto.rem"
    Public Const IFTPArchivo As String = "IFTPArchivo.rem"
    Public Const IServicioComun As String = "IServicioComun.rem"

    Public Const ISeguridad As String = "ISeguridad.rem"
    Public Const IServicioNotificacion As String = "IServicioNotificacion.rem"
    Public Const IPlantilla As String = "IPlantilla.rem"
    'wjra 14/01/2011
    Public Const IConciliacion As String = "IConciliacion.rem"
    Public Const IPasarela As String = "IPasarela.rem"

    'FullCarga 31/05/2011
    Public Const IPuntoVenta As String = "IPuntoVenta.rem"

    Public Const IUsuario As String = "IUsuario.rem"

    'Pmonzon ServicioInstitucion
    Public Const IServicioInsitucion As String = "IServicioInstitucion.rem"

End Class
