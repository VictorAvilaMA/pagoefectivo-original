Public Class ParametrosSistema

    Public Const RolAdministrador As String = "Administrador"
    Public Const RolCliente As String = "Cliente"
    Public Const RolCajero As String = "Cajero"
    Public Const RolSupervisor As String = "Supervisor"
    Public Const RolRepresentante As String = "Representante"
    Public Const RolJefeProducto As String = "Jefe de Producto"
    Public Const RolTesoreria As String = "Tesoreria"
    Public Const RolAdminMonederoElectronico As String = "Administrador Monedero Electronico"
    Public Const GrupoEstadoMantenimiento As String = "ESMA"
    Public Const GrupoEstadoOrdenPago As String = "ESOP"
    Public Const GrupoTipoServicioOP As String = "TSCO"
    Public Const GrupoGenero As String = "UGEN"
    Public Const GrupoTipoDocumento As String = "TDOC"
    Public Const GrupoEstadoDevolucion As String = "EDEV"
    Public Const MensajeDeErrorCliente As String = "Ha ocurrido un error. Por favor, cont�ctese con el Administrador."
    Public Const MensajeDeCorrectoCliente = "La operaci�n se realiz� correctamente."
    Public Const GrupoEstadoUsuario As String = "ESUS"
    Public Const StrConexion As String = "SPESQLPagoEfectivo"
    Public Const StrConexionReplica As String = "SPESQLPagoEfectivoReplica"
    Public Const StrConexionPESaldos As String = "SPESQLPagoEfectivoSaldos"
    Public Const StrConexionLogShipping As String = "SPESQLPagoEfectivoLogShipping"
    Public Const ItemDefaultCombo As String = "::: Seleccionar :::"
    Public Const ItemDefaultComboSmall As String = ":Seleccionar:"
    Public Const MinCaracteresContrasenia As String = "MinTamPass"
    Public Const BackColorAnulado As String = "silver"
    Public Const GrupoTipoConsultaPG As String = "TPPG"
    Public Const MensajeExisteOperador As String = "Ya existe operador con este c�digo."
    Public Const MensajeExisteAgenciaBancaria As String = "Este c�digo ya existe."
    Public Const NombreAgenciaBancariaDefault As String = "Sin nombre"
    Public Const IdSistemaPagoEfectivo As Integer = 1
    Public Const ProcesoConciliacion As String = " Conciliaci�n de operaciones bancarias."
    Public Const ProcesoCancelacionOP As String = " Cancelaci�n de orden pago por agencia bancaria"
    Public Const ProcesoCancelacionFC As String = " Cancelaci�n de orden pago por Full Carga"
    Public Const ProcesoCancelacionMoMo As String = " Cancelaci�n de orden pago por MoMo"
    Public Const MensajeErrorWebServicePOS As String = "No se pudo realizar la operaci�n."
    Public Const Consultar As String = "CO"
    Public Const Anular As String = "EX"
    Public Const Cancelar As String = "CA"
    Public Const Registrar As String = "Registrado"
    Public Const GrupoActNotifURLError As String = "NEAC"
    Public Const GrupoMonedaBanco As String = "GrupoMonedaBanco"
    Public Const DireccionFTP As String = ""
    Public Const GrupoClasificacionServicio As String = "CSER"


#Region "CONSTANTES A�ADIDAS PARA DINERO VIRTUAL 14/11/2011"
    Public Const EstadoSolicitudDineroVirtual As String = "CUVI"

    Public Enum EnvioTipoEmail
        PagarCip = 1                        'CU04
        SolicitudCreacionCuenta = 2         'CU03
        EnviarToken = 3                     'CU04,05,06
        TransferenciaDineroOrigen = 5       'CU05
        TransferenciaDineroDestino = 4      'CU05
        SolicitudRetiroAdmin = 6            'CU06
        SolicitudRetiroCliente = 7          'CU06
        EquipoRegistrado = 8                'CU07
        CuentaAprobadaCliente = 9           'CU12
        CuentaRechazadaCliente = 10         'CU12
        AprobacionRetiroCliente = 11        'CU14
        AprobacionRetiroTesoreria = 12      'CU14
        RechazadaRetiroCliente = 13         'CU14
        CuentaLiquidacionAprobada = 14      'CU17
        CuentaLiquidacionRechazada = 15     'CU17
        CuentaHabilitadaCliente = 17        'CU15
        CuentaInhabilitadaCliente = 16      'CU15
        CuentaRecarga = 18                  'CU18
        CuentaExtorno = 19                  'CU18
        UsuarioBloqueado = 20
        UsuarioDesbloqueado = 21
        PagarSolicitudPortal = 22
        CuentaAprobadaClientePrimeraVez = 23
        CambiarContrasenia = 24
        CambiarContraseniaConfirmacion = 25
    End Enum

    Public Const IdPaginaAdminDisp As Integer = 102

    Public Const DV_Consultar_Cuentas_de_Dinero As Integer = 107
    Public Const DV_Mis_Cuentas As Integer = 108
    Public Const DV_Consulta_Detalle_Cuenta As Integer = 109
    Public Const DV_Pagar_CIP As Integer = 112
    Public Const DV_Consultar_Movimientos As Integer = 113
    Public Const DV_Ver_solicitudes_de_retiro_de_dinero As Integer = 115
    Public Const DV_Ver_detalle_de_las_Solicitudes_de_retiro_de_dinero As Integer = 116
    Public Const DV_Solicitar_Retiro_de_Dinero As Integer = 117
    Public Const DV_Establecer_Monto_de_Recarga As Integer = 118
    Public Const DV_Transferencia_de_Dinero As Integer = 119
    Public Const DV_Movimientos_Sospechosos As Integer = 130


    Public Const DV_MonederoElectronico As Integer = 105
    Public Const DV_Solicitar_Cuenta As Integer = 106

    Public Const DV_Rpt_Det_Sol_Crear_Cta As Integer = 126
    Public Const DV_Rpt_Sol_Ret_Din As Integer = 127
    Public Const DV_Rpt_Det_Cta As Integer = 128
    Public Const DV_Rpt_Cta_Cli As Integer = 124
    Public Const DV_Rpt_Mov_CTa As Integer = 114

#End Region

    Public Enum EstadoUsuario
        Activo = 91
        Bloqueado = 92
        Pendiente = 93
        Inactivo = 94
    End Enum


    Public Enum EstadoAgenteCaja
        Aperturado = 81
        Cerrado = 82
        Liquidado = 83
        Anulado = 84
    End Enum

    Public Enum EstadoMantenimiento
        Activo = 41
        Inactivo = 42
    End Enum
    Public Enum EstadoBanco
        Activo = 181
        Inactivo = 182
    End Enum

    Public Enum TipoMovimiento
        Cierre = 32
        SaldoInicial = 33
        RecepcionarOP = 34
        AnularOP = 35
        Sobrante = 36
        Faltante = 37
        Extorno = 144
        ExtornoAutomatico = 31
    End Enum

    Public Enum MedioPago
        Efectivo = 2
        Tarjeta = 3
    End Enum

    Public Enum TipoAgente
        Cajero = 51
        Supervisor = 52
    End Enum

    Public Enum EstadoOrdenPago
        Expirado = 21
        Generada = 22
        Cancelada = 23
        Anulada = 24
        Eliminado = 25
        EnProcesoPago = 26
    End Enum

    Public Enum ValidaFechaExpiraOrdenPago
        Inv�lido = -2
        Excedido = -3
        claveAPI = -4
    End Enum

    Public Enum EstadoUsoCaja
        Activo = 101
        Inactivo = 102
    End Enum

    Public Enum ExisteEmail
        Existe = 1000
        NoExiste = -1000
    End Enum

    'Add Jonathan Bastidas - jonathan.bastidas@ec.pe
    Public Enum Representante
        ExisteRepresentante = -1
        ExisteUsuario = 0
    End Enum

    Public Enum EstadoRegistroValidacion
        Pendiente = 71
        Completado = 72
    End Enum

    Public Enum EstadoFechaValuta
        DisponibleVencido = 110
        Disponible = 111
        EnUso = 112
        Inactivo = 113
        NoExiste = -1
    End Enum

    Public Enum EstadoValidacionCaja
        Aperturado = 1
        NoAperturado = 2
        AperturadoConFechaActual = 11
        AperturadoConFechaValuta = 12
        AperturadoPendienteCierrePorFechaActual = 13
        AperturadoPendienteCierrePorFechaValuta = 14
    End Enum

    Public Enum EstadoPuedeLiquidar
        Si = 1
        No = 0
    End Enum

    Public Enum TipoFechaValuta
        Apertura = 121
        Cierre = 122
    End Enum

    Public Enum EnteGeneradorOP
        Cliente = 131
        AgenciaRecaudadora = 132
    End Enum

    Public Enum TipoNotificacion
        Recepcion = 1
        Anulacion = 2
        Expiracion = 3
    End Enum

    Public Enum ProcesoNotificacion
        ValidarRequest = 1
        ProcesarRequest = 2
    End Enum

    Public Enum TipoConsultaPG
        NroOrdenes = 211
        Montos = 212
    End Enum


    Public Enum TipoOrigenCancelacion
        AgenciaBancaria = 1
        AgenciaRecaudadora = 2
        POS = 3
        FullCarga = 4
        Monedero = 5
        MoMo = 6
    End Enum

    Public Enum ExisteAgenciaBancaria
        Existe = 1000
        NoExiste = -1000
    End Enum

    Public Enum EstadoAgenciaBancaria
        Activo = 191
        Inactivo = 192
        Modificar = 193
    End Enum

    'Public Enum ServicioTipoNotificacion
    '    ArchivoFTP = 241
    '    Request = 242
    'End Enum

    Public Enum ClasificacionServicio
        'pre
        'ComElect = 734
        'VentasTlf = 735
        'Institucional = 736
        'dev
        'ComElect = 697
        'VentasTlf = 698
        'Institucional = 699
        'prod
        ComElect = 696
        VentasTlf = 697
        Institucional = 698

    End Enum

    Public Class PlantillaFormato
        Public Class Tipo
            Public Const Personalizado = 53
            Public Const Express = 54
            Public Const Estandar = 55
        End Class
    End Class

    Public Class Servicio
        Public Class TipoIntegracion
            Public Const Request As Integer = 270
            Public Const Post As Integer = 271
            Public Const WebService As Integer = 272
        End Class
        Public Class TipoNotificacion
            Public Const ArchivoFTP = 241
            Public Const Request = 242
            Public Const NoNotifica = 243
        End Class
        Public Class VersionIntegracion
            Public Const Version1 = "1.0"
            Public Const Version2 = "2.0"
        End Class
    End Class

    Public Class ServicioNotificacion
        Public Class Tipo
            Public Const UrlOk As Integer = 291
            Public Const UrlError As Integer = 290
            Public Const UrlNotificacion As Integer = 292
            Public Const UrlNotificacionSaga As Integer = 293
            'Public Const UrlNotificacionRipley As Integer = 294
            Public Const UrlNotificacionSodimac As Integer = 294
        End Class
        Public Class Estado
            Public Const ErrorRegistro As Integer = 280
            Public Const Pendiente As Integer = 281
            Public Const Notificado As Integer = 282
            Public Const Inactivo As Integer = 283
            Public Const ErrorNotificado As Integer = 284
            Public Const ErrorIntentoNotificado As Integer = 285
        End Class
        Public Class IdOrigenRegistro
            Public Const ProcesoConciliacion As Integer = 501
            Public Const Web As Integer = 502
            Public Const ServicioWeb As Integer = 503
            Public Const ProcesoExpiraci�n As Integer = 504
            Public Const ProcesoExpiraci�nOfertop As Integer = 505
        End Class
    End Class

    Public Enum ArchivoFTPTipoArchivo
        Pago = 221
        Expiracion = 222
    End Enum

    Public Enum ArchivoFTPEstado
        Generado = 231
        NoGenerado = 232
    End Enum

    Public Enum CodigoMensajeErrorWSPOs
        respuestaError = 121
    End Enum

    Public Enum CodigoMensajePOS
        ExisteOP = 101
        NoexisteOP = 102
        PagoRealizado = 110
        CierreRealizado = 117
    End Enum

    Public Class Cliente
        Public Enum OrigenRegistro
            RegistroCompleto = 260
            RegistroBasico = 261
            RegistroBasicoWS = 262
        End Enum

    End Class
    Public Enum EstadoPeriodoPagoServicio
        Registrado = 560
        Liquidado = 561
    End Enum
    Public Enum TipoPeriodo
        Diaria = 681
        Semanal = 682
        Mensual = 683
    End Enum

    Public Enum VigenciaArchivoServicioInstitucion
        Vigente = 1
        Caducado = 0
    End Enum


#Region "Enumeracion MONEDERO ELECTR�NICO"
    Public Class CuentaVirtualEstado
        Public Const Solicitado As Integer = 601
        Public Const Rechazado As Integer = 602
        Public Const Habilitado As Integer = 603
        Public Const Inhabilitado As Integer = 604
    End Class

    Public Class TipoMovimientoMonedero
        Public Const Recarga As Integer = 611
        Public Const TransferenciaDeOtraCta As Integer = 612
        Public Const TransferenciaAOtroCta As Integer = 613
        Public Const PagoCIP As Integer = 614
        Public Const PagoCIPDesdePortal As Integer = 615
        Public Const RetiroDinero As Integer = 616
        Public Const Comision As Integer = 618
    End Class

    Public Class EstadoSolicitudRetiroDinero
        Public Const Solicitado As Integer = 621
        Public Const Aprobado As Integer = 622
        Public Const Rechazado As Integer = 623
        Public Const Liquidado As Integer = 624
        Public Const Anulado As Integer = 625
    End Class

    Public Class TipoComisionME
        Public Const Fijo As Integer = 631
        Public Const Porcentual As Integer = 632
    End Class

    Public Class TipoComisionServicio
        Public Const Porcentual As Integer = 550
        Public Const Fijo As Integer = 551
        Public Const Mixto As Integer = 552
    End Class
#End Region

    Public Class UrlsApp
        'Public Const UrlCondicionesUso As String = "~/condiciones.aspx"
        'Public Const UrlPoliticaPrivacidad As String = "~/politicas.aspx"
        Public Const UrlCondicionesUso As String = "#"
        Public Const UrlPoliticaPrivacidad As String = "#"
        Public Const UrlConoceMasDePE As String = "UrlConoceMasDePE"
        Public Const UrlConoceCentrosCobro As String = "UrlConoceCentrosCobro"
        Public Const UrlContactateAgentePE As String = "UrlContactateAgentePE"
        Public Const UrlTerminosYCondicionesUso As String = "UrlTerminosYCondicionesUso"
    End Class

    Public Class OrdenPago
        Public Class ObtCIPInfoByUrLServEstado
            Public Const SolicitudSatisfactoria As String = "1"
            Public Const FormatoContratoXMLIncorrecto As String = "-1"
            Public Const ServicioNoExiste As String = "-2"
            Public Const MontoTotalOPMenorQueCero As String = "-3"
            Public Const CodigoMonedaIncorrecto As String = "-4"
        End Class
        Public Class GenerarCIPEstado
            Public Const CIPGeneracionOk As String = "1"
            Public Const CIPGeneracionOKMsq As String = "El c�digo de identificaci�n de pago (CIP) se ha generado satisfactoriamente."

            'Public Const CIPGeneracionAnterior As String = "2"
            'Public Const CIPGeneracionAnteriorMsq As String = "El C�digo de Identificaci�n de Pago no se gener� debido a que ya ha sido generado anteriormente."
            Public Const CIPGenAnterior As String = "-1"
            Public Const CIPGenAnteriorMsq As String = "El c�digo de identificaci�n de pago (CIP) ya se ha generado anteriormente."

            Public Const CIPGenAnteriorOtroUsuario As String = "-2"
            Public Const CIPGenAnteriorOtroUsuarioMsq As String = "El c�digo de identificaci�n de pago (CIP) para esta operaci�n ya se ha generado anteriormente por otro usuario."
        End Class

        Public Class OrigenEliminacion
            Public Const EliminadoDeWeb As Integer = 421
            Public Const EliminadoDeWS As Integer = 420
        End Class

        Public Class VersionOrdenPago
            Public Const Version1 As Integer = 1
            Public Const Version2 As Integer = 2
        End Class
    End Class

    '<add Peru.Com FaseIII>
    Public Class SolicitudPago
        Public Class Tipo
            Public Const Pendiente As Integer = 591
            Public Const GeneradaCIP As Integer = 592
            Public Const Pagada As Integer = 593
            Public Const Expirada As Integer = 594
            Public Const ExpiradaCIP As Integer = 595
        End Class
        Public Class MetodoPago
            Public Const Bancos As Integer = 1
            Public Const CuentaVirtual As Integer = 2
            Public Const Visa As Integer = 3
            Public Const MasterCard As Integer = 4
        End Class
    End Class


    'wjra 12/01/2010 
    Public Class Conciliacion
        Public Class ResultadosConciliacion
            Public Const ConciliacionOk As String = "1"
            Public Const ConciliacionOKMsg As String = "La conciliaci�n se realiz� satisfactoriamente."
            Public Const ConciliacionNo As String = "0"
            Public Const ConciliacionNoMsg As String = "La conciliaci�n no se realiz� satisfactoriamente."
            Public Const ConciliacionError As String = "-1"
            Public Const ConciliacionErrorMsg As String = "Ha ocurrido un error en el proceso de conciliaci�n."
            Public Const ConciliacionErrorOperacionBancaria As Integer = -999
            Public Const ConciliacionErrorOperacionBancariaMsg As String = "Uno de los campos principales de esta operacion es nulo u erroneo."
            Public Const PreConciliacionErrorMsg As String = "Ha ocurrido un error en el proceso de pre-conciliaci�n."
        End Class
        Public Class CodigoBancos
            Public Const BCP As String = "02"
            Public Const BBVA As String = "03"
            Public Const Interbank As String = "04"
            Public Const Scotiabank As String = "05"
            Public Const WesterUnion As String = "06"
            Public Const FullCarga As String = "100"
            Public Const BanBif As String = "07"
            Public Const MoMo As String = "101"
            Public Const Kasnet As String = "08"
            Public Const Life As String = "09"
        End Class
        Public Class TiposConciliacion
            Public Const Diaria As Integer = 510
            Public Const AlCierre As Integer = 511
        End Class
        Public Class OrigenConciliacion
            Public Const Manual As Integer = 526
            Public Const Automatizada As Integer = 527
        End Class
        Public Class EstadosConciliacion
            Public Const Conciliado As Integer = 171
            Public Const NoConciliado As Integer = 172
            Public Const ConciliadoModificado As Integer = 173
            Public Const ConciliacionObservado As Integer = 174
        End Class
        'Public Class IdTiposEstructuraArchivos
        '    Public Const BCP As Integer = 520   '520 Estructura Archivo del BCP
        '    Public Const BBVA As Integer = 521  '521	TEAB	Estructura Archivo del BBVA
        '    Public Const Interbank As Integer = 522   '522	TEAB	Estructura Archivo del Interbank
        '    Public Const Scotiabank As Integer = 523   '523	TEAB	Estructura Archivo del Scotiabank
        'End Class
        'Public Class EstructuraArchivoBCP
        '    Public Const Header As String = "CC"
        '    Public Const Cuerpo As String = "DD"
        '    Public Const Pie As String = "FF"
        'End Class
        'Public Class EstructuraArchivoBBVA
        '    Public Const Header As String = "CC"
        '    Public Const Cuerpo As String = "DD"
        '    Public Const Pie As String = "FF"
        'End Class
        'Public Class EstructuraArchivoIBK
        '    Public Const Header As String = "CC"
        '    Public Const Cuerpo As String = "DD"
        '    Public Const Pie As String = "FF"
        'End Class
        'Public Class EstructuraArchivoSBK
        '    Public Const Header As String = "CC"
        '    Public Const Cuerpo As String = "DD"
        '    Public Const Pie As String = "FF"
        'End Class
    End Class
    '****************************************************
    Public Class Log
        Public Class Tipo
            Public Const idTipoServiciosWebAGBA As Integer = 301
            Public Const idTipoServiciosWebVisa As Integer = 302
            Public Const idTipoServiciosWebGenCIP As Integer = 303
            Public Const idTipoServiciosWebElimCIP As Integer = 304
            Public Const idTipoServiciosWebBBVA As Integer = 305
            Public Const idTipoServiciosWebSBK As Integer = 306
            Public Const idTipoServiciosWebIBK As Integer = 307
            Public Const idTipoServiciosWebWU As Integer = 308
            Public Const idTipoServiciosWebBanBif As Integer = 309
            Public Const idTipoPasarelSolicitud As Integer = 530
            Public Const idTipoServiciosWebKasnet As Integer = 310
        End Class

        Public Class MetodosBBVA
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const pagar_request As String = "pagar_request"
            Public Const pagar_response As String = "pagar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
            Public Const extornar_request As String = "extornar_request"
            Public Const extornar_response As String = "extornar_response"
            Public Const log_bbva As String = "log_bbva"
        End Class

        Public Class MetodosFullCarga
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const cancelar_request As String = "cancelar_request"
            Public Const cancelar_response As String = "cancelar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
        End Class

        Public Class MetodosLife
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const cancelar_request As String = "cancelar_request"
            Public Const cancelar_response As String = "cancelar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
        End Class

        Public Class MetodosMoMo
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const cancelar_request As String = "cancelar_request"
            Public Const cancelar_response As String = "cancelar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
        End Class
        Public Class MetodosScotiabank
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const cancelar_request As String = "cancelar_request"
            Public Const cancelar_response As String = "cancelar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
            Public Const cancelarauto_request As String = "cancelarauto_request"
            Public Const cancelarauto_response As String = "cancelarauto_response"
            Public Const anularauto_request As String = "anularauto_request"
            Public Const anularauto_response As String = "anularauto_response"
        End Class

        Public Class MetodosInterbank
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const cancelar_request As String = "cancelar_request"
            Public Const cancelar_response As String = "cancelar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
            Public Const cancelarauto_request As String = "cancelarauto_request"
            Public Const cancelarauto_response As String = "cancelarauto_response"
            Public Const anularauto_request As String = "anularauto_request"
            Public Const anularauto_response As String = "anularauto_response"
        End Class

        Public Class MetodosWesternUnion
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const cancelar_request As String = "cancelar_request"
            Public Const cancelar_response As String = "cancelar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
        End Class
        Public Class MetodosBanBif
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const pagar_request As String = "pagar_request"
            Public Const pagar_response As String = "pagar_response"
            Public Const anular_request As String = "anular_request"
            Public Const anular_response As String = "anular_response"
            Public Const extornar_request As String = "extornar_request"
            Public Const extornar_response As String = "extornar_response"
            Public Const log_banbif As String = "log_banbif"
        End Class
        Public Class MetodosKasnet
            Public Const consultar_request As String = "consultar_request"
            Public Const consultar_response As String = "consultar_response"
            Public Const pagar_request As String = "pagar_request"
            Public Const pagar_response As String = "pagar_response"
            Public Const extornar_request As String = "extornar_request"
            Public Const extornar_response As String = "extornar_response"
        End Class
    End Class

    Public Class LogRedirect
        Public Class Tipo
            Public Const redirectEntrante As Integer = 581
            Public Const redirectSaliente As Integer = 582
        End Class
    End Class

    Public Class Moneda
        Public Class Tipo
            Public Const soles As Integer = 1
            Public Const dolares As Integer = 2
        End Class
    End Class

    Public Class Plantilla
        Public Class Tipo
            Public Const EmailGenCIP As String = "EmailGenCIP"
            Public Const EmailCanCIP As String = "EmailCanCIP"
            Public Const EmailValUsu As String = "EmailValUsu"
            Public Const VencPago As String = "VencPago"
            Public Const InfoConfirm As String = "InfoConfirm"
            Public Const InfoConfirmIF As String = "InfoConfirmIF"
            Public Const InfoPago02 As String = "InfoPago02"
            Public Const InfoPago03 As String = "InfoPago03"
            Public Const InfoPago100 As String = "InfoPago100"
            Public Const InfoPagoGeneral As String = "InfoPagoGeneral"
            Public Const InfoPagoGeneralIF As String = "InfoPagoGeneralIF"
            Public Const InfoImpresion As String = "InfoImpresion"
            Public Const QueEsPE As String = "QueEsPE"

#Region "PlantillasActualizacionMasiva"
            Public Const GenCip As String = "GenCip"
            Public Const ConfGen As String = "ConfGen"
            Public Const SecImpr As String = "SecImpr"
            Public Const ConfPago As String = "ConfPago"
#End Region

        End Class
        Public Class TipoVariacion
            Public Const Standar As String = "es"
        End Class
        Public Class Parametro
            Public Class Tipo
                Public Const Dinamico As Integer = 410
                Public Const Estatico As Integer = 411
            End Class
        End Class
        Public Const PreFijoSeccion As String = "Seccion"
    End Class

    Public Class WSBancoMensaje
        Public Class BCP
            Public Enum Mensaje
                Consulta_Realizada = 1
                Cliente_no_presenta_deuda = 2
                Cliente_no_encontrado = 3
                Pago_Realizado = 1
                Pago_no_realizado = 4
                Extorno_Realizado = 1
                Extorno_no_Realizado = 5
                ' ''Dinero Virtual
                'Cuenta_no_encontrada = 3
            End Enum
            Public Class DetalleMensaje
                Public Const CodigoServicioInvalida As String = "No se encontro un c�digo de Servicio asociado al error"
                Public Const FechaExtornoInvalida As String = "La fecha de extorno no coincide con la fecha de Pago"
                Public Const TiempoExtornoInvalida As String = "Se excedi� el tiempo m�ximo de extorno."
                Public Const AgenciaBancariaInvalida As String = "La Agencia bancaria no es la misma donde se cancelo"
                Public Const OrdenPagoNoCancelada As String = "La Orden de Pago no se encuentra cancelada"
                Public Const NoSeEncontroOrdenPago As String = "No se encontr� la orden de Pago"
            End Class
        End Class
        Public Class BBVA
            Public Class Mensaje
                Public Const TransaccionRealizadaExito As String = "0001"
                Public Const NoSePudoRealizarLaTransaccion As String = "3002"
                Public Const EstadoNumeroReferenciaInvalida As String = "3013"
                Public Const TramaEnviadaEmpresaInvalida As String = "3012"
                Public Const NumeroReferenciaNoExiste As String = "0101"
                Public Const NoTieneDeudasPendientes As String = "3009"
                Public Const NumeroReferenciaEstadoPagado As String = "0106"
                Public Const NUmeroReferenciaEstadoExpirado As String = "0102"

                'EXTORNO SIN EXITO � TIEMPO MAXIMO DE EXTORNO
                Public Const NoSePudoRealizarRegistroExtorno As String = "3004"

                Public Const NoExistePagoParaExtornar As String = "3014"

                'Public Const Pago_Realizado = "1"
                'Public Const Pago_no_realizado = "2"
                'Public Const Extorno_Realizado = "3"
                'Public Const Extorno_no_Realizado = "4"
                'Public Const Cuenta_encontrada = "5"
                'Public Const Cuenta_no_encontrada = "6"
            End Class
        End Class
        Public Class FC
            Public Class Mensaje
                Public Const TransaccionRealizadaExito As String = "01"
                Public Const NoSePudoRealizarTransaccion As String = "02"
                Public Const TramaEnviadaInvalida As String = "03"
                Public Const CodigoPagoNoExiste As String = "04"
                Public Const CodigoConEstadoPagado As String = "05"
                Public Const CodigoConEstadoExpirado As String = "06"
                Public Const NoSePudoRealizarExtorno As String = "07"
                Public Const CodigoExtornarNoExiste As String = "08"
                Public Const CodigoExtornarExpirado As String = "09"
                Public Const MontoOMonedaNoCoincide As String = "10"
                'Public Const CuentaNoExiste As String = "11"
                'Public Const CuentaNoHabilitada As String = "12"
                'Public Const MonedaNoCoincide As String = "13"
                'Public Const CuentaExiste As String = "14"
                'Public Const Pago_Realizado = "15"
                'Public Const Pago_no_realizado = "16"
                'Public Const Extorno_Realizado = "17"
                'Public Const Extorno_no_Realizado = "18"
                'TIEMPO MAXIMO DE EXPIRACION
                Public Const NoSePermiteExtornar As String = "37"
            End Class
        End Class
        Public Class MoMo
            Public Class Mensaje
                Public Const TransaccionRealizadaExito As String = "01"
                Public Const NoSePudoRealizarTransaccion As String = "02"
                Public Const TramaEnviadaInvalida As String = "03"
                Public Const CodigoPagoNoExiste As String = "04"
                Public Const CodigoConEstadoPagado As String = "05"
                Public Const CodigoConEstadoExpirado As String = "06"
                Public Const NoSePudoRealizarExtorno As String = "07"
                Public Const CodigoExtornarNoExiste As String = "08"
                Public Const CodigoExtornarExpirado As String = "09"
                Public Const MontoOMonedaNoCoincide As String = "10"
                'Public Const CuentaNoExiste As String = "11"
                'Public Const CuentaNoHabilitada As String = "12"
                'Public Const MonedaNoCoincide As String = "13"
                'Public Const CuentaExiste As String = "14"
                'Public Const Pago_Realizado = "15"
                'Public Const Pago_no_realizado = "16"
                'Public Const Extorno_Realizado = "17"
                'Public Const Extorno_no_Realizado = "18"
            End Class
        End Class

        Public Class kasnet
            Public Class Mensaje
                Public Const TransaccionRealizadaExito As String = "01"
                Public Const NoSePudoRealizarTransaccion As String = "02"
                Public Const TramaEnviadaInvalida As String = "03"
                Public Const CodigoPagoNoExiste As String = "04"
                Public Const CodigoConEstadoPagado As String = "05"
                Public Const CodigoConEstadoExpirado As String = "06"
                Public Const CodigoConEstadoEliminado As String = "07"
                Public Const CodigoExtornarNoExiste As String = "08"
                Public Const CodigoExtornarExpirado As String = "09"
                Public Const MontoOMonedaNoCoincide As String = "10"
                'TIEMPO MAXIMO DE EXPIRACION
                Public Const NoSePermiteExtornar As String = "37"
                'Public Const CuentaNoExiste As String = "11"
                'Public Const CuentaNoHabilitada As String = "12"
                'Public Const MonedaNoCoincide As String = "13"
                'Public Const CuentaExiste As String = "14"
                'Public Const Pago_Realizado = "15"
                'Public Const Pago_no_realizado = "16"
                'Public Const Extorno_Realizado = "17"
                'Public Const Extorno_no_Realizado = "18"
            End Class
        End Class
        Public Class BanBif
            Public Class TipoRespuesta
                Public Const Informacion = "CP0000"
                Public Const Errores = "CP0001"
                Public Const Advertencia_Otros = "CP0002"
            End Class
            Public Class CodigosRespuesta
                Public Const Proceso_Conforme = "000001"
                Public Const Sin_Deuda_Pendiente = "000002"
                Public Const Deuda_No_Valida = "000003"
                Public Const Error_Proceso_Transaccion = "000004"
                Public Const No_Procede_Extorno_Entidad = "000005"
                Public Const No_Procede_Pago_Entidad = "000006"
                Public Const Deuda_Con_Restricciones = "000007"
                Public Const Limite_Pago_Superado = "000008"
                Public Const Monto_Pagar_errado = "000009"
                'Propios de PE
                Public Const Codigo_No_Existe = "000010"
                Public Const Trama_Invalida = "000011"

                'TIEMPO MAXIMO DE EXPIRACION
                Public Const NoSePermiteExtornar = "000037"
            End Class
            Public Class TipoOperacion
                Public Const Consulta = "C"
                Public Const Pago = "P"
                Public Const ExtornoAutomatico = "A"
                Public Const Extorno = "E"
            End Class
        End Class
    End Class
    Public Class Pasarela

        Public Class Estado
            Public Const Pendiente As Integer = 540
            Public Const Errado As Integer = 541
            Public Const Aceptado As Integer = 542
            Public Const Rechazado As Integer = 543
        End Class

    End Class
    Public Class Scotiabank
        Public Class CodigosRespuesta
            Public Const RespuestaOk = "00"
            Public Const NoActionToken = "21"
            Public Const CutOffIsInProgress = "90"
            Public Const IssuerOrSwitchIsInoperative = "91"
            Public Const FinancialInstCanNotBeFoundViolLaw = "92"
            Public Const TransCanNotBeCompletes = "93"
            Public Const DuplicateTrasmision = "94"
            Public Const ReconcileError = "95"
            Public Const SystemMalFunction = "96"
            Public Const CommunicationError = "99"
        End Class
        Public Class CodigosRespuestaExtendidos
            Public Const TransaccionExitosa = "000"
            Public Const TransaccionFallida = "001"
            Public Const CIPNoExiste = "002"
            Public Const CIPEliminado = "003"
            Public Const CIPCancelado = "004"
            Public Const CIPExpirado = "005"
            Public Const CodigoMonedaNoCoincide = "006"
            Public Const MontoNoCoincide = "007"
            Public Const NotificacionFallidaRegistroAgencia = "008"
            Public Const NotificacionFallidaUsuario = "009"
            Public Const NotificacionFallidaPortal = "010"
            Public Const MontoOMonedaNoCoincide = "011"
            Public Const CodigoServicioNoCoincide = "012"
            Public Const CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide = "013"
            Public Const MovimientoPagoNoExiste = "014"
            Public Const FechaPagoNoActual = "015"
            Public Const CIPNoExisteONoCancelado = "016"
            Public Const CIPNoExisteONoCanceladoOMonedaNoCoincide = "017"
            Public Const CIPNoExisteONoGeneradoOMonedaNoCoincide = "018"
            Public Const MovimientoAnulacionNoExiste = "019"
            Public Const CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide = "020"
            Public Const FechaAnulacionNoActual = "021"
            Public Const TramaInvalida = "022"
            'TIEMPO MAXIMO DE EXPIRACION
            'Modificado
            Public Const NoSePermiteExtornar = "023"
        End Class
        Public Class IdentificacionTipoMensaje
            Public Const Solicitud = "0200"
            Public Const Respuesta = "0210"
            Public Const SolicitudAutomatica = "0400"
            Public Const RespuestaAutomatica = "0410"
        End Class
        Public Class CodigoProceso
            Public Const Consulta = "355000"
            Public Const Pago = "945000"
            Public Const Anulacion = "965000"
        End Class
    End Class
    Public Class Interbank
        Public Class CodigosRespuesta
            Public Const TransaccionAprobadaPorAutorizador = "00"
            Public Const CodigoProcesoNoSoportado = "01"
            Public Const FechaHoraFueraRangoServicio = "02"
            Public Const CanalNoSoportado = "03"
            Public Const BinAdquirienteNoCorrespondeAutorizador = "04"
            Public Const BinAdministradorNoCorresponde = "05"
            Public Const FormatoMensajeInvalido = "06"
            Public Const DocumentoReferenciaNoExiste = "07"
            Public Const DocumentoReferenciaVencido = "08"
            Public Const DocumentoReferenciaAnulado = "09"
            Public Const DocumentoReferenciaBloqueado = "10"
            Public Const DocumentoReferenciaExcedioTiempoParaAnulacion = "11"
            Public Const FormaPagoNoSoportada = "12"
            Public Const ImportePagoNoCorresponde = "13"
            Public Const SistemaAutorizadorOcupadoReintenteLuego = "14"
            Public Const SistemaAutorizadorFueraServicio = "15"
            Public Const TransaccionSospechosaFraude = "16"
            Public Const DocumentoReferenciaYaCancelado = "17"
            Public Const ProblemaAutenticacionConexion = "18"
            Public Const TransaccionDesaprobadaPorAutorizador = "19"
            Public Const ErrorGeneralSistema = "99"
        End Class
        Public Class CodigosRespuestaExtendidos
            Public Const RespuestaOk = "00"
            Public Const TransaccionFallida = "01"
            Public Const CIPNoExiste = "02"
            Public Const CIPEliminado = "03"
            Public Const CIPCancelado = "04"
            Public Const CIPExpirado = "05"
            Public Const CodigoMonedaNoCoincide = "06"
            Public Const MontoNoCoincide = "07"
            Public Const NotificacionFallidaRegistroAgencia = "08"
            Public Const NotificacionFallidaUsuario = "09"
            Public Const NotificacionFallidaPortal = "10"
            Public Const MontoOMonedaNoCoincide = "11"
            Public Const CodigoServicioNoCoincide = "12"
            Public Const CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide = "13"
            Public Const MovimientoPagoNoExiste = "14"
            Public Const FechaPagoNoActual = "15"
            Public Const CIPNoExisteONoCancelado = "16"
            Public Const CIPNoExisteONoCanceladoOMonedaNoCoincide = "17"
            Public Const CIPNoExisteONoGeneradoOMonedaNoCoincide = "18"
            Public Const MovimientoAnulacionNoExiste = "19"
            Public Const CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide = "20"
            Public Const FechaAnulacionNoActual = "21"
            Public Const TramaInvalida = "22"
            'TIEMPO MAXIMO DE EXPIRACION
            Public Const NoSePermiteExtornar = "37"
        End Class
        Public Class IdentificacionTipoMensaje
            Public Const Solicitud = "0200"
            Public Const Respuesta = "0210"
            Public Const SolicitudAutomatica = "0400"
            Public Const RespuestaAutomatica = "0410"
        End Class
        Public Class CodigoProceso
            Public Const Consulta = "31"
            Public Const Pago = "21"
            Public Const Anulacion = "22"
        End Class
    End Class
    Public Class WesternUnion
        Public Class CodError
            Public Const TransaccionAceptada As Int32 = 0               'consulta/cancelacion/anulacion
            Public Const ErrorBD As Int32 = 1                           'consulta/cancelacion/anulacion
            Public Const ClaveDuplicada As Int32 = 2                    'cancelacion
            Public Const TransaccionYaReversada As Int32 = 3            'anulacion
            Public Const NoExisteDirecta As Int32 = 4                   'anulacion
            Public Const OperacionInvalida As Int32 = 5                 'consulta/cancelacion/anulacion
            Public Const NoExisteRegistro As Int32 = 6                  'anulacion
            Public Const ClienteNoExiste As Int32 = 7                   'consulta/cancelacion/anulacion
            Public Const ErrorValidacionCodigoCliente As Int32 = 8      'consulta/cancelacion
            Public Const ParametrosIncorrectos As Int32 = 9             'consulta/cancelacion/anulacion
            Public Const ErrorInternoEntidad As Int32 = 10              'consulta/cancelacion/anulacion
            Public Const UsuarioNoHabilitadoParaOperar As Int32 = 11    'consulta/cancelacion/anulacion
            Public Const TransaccionFueraDeHorario As Int32 = 13        'consulta/cancelacion/anulacion
            'TIEMPO MAXIMO DE EXPIRACION
            Public Const NoSePermiteExtornar As Int32 = 37
        End Class
        Public Class CodSeveridad
            Public Const zero As Int32 = 0
            Public Const uno As Int32 = 1
            Public Shared Function resolverSeveridad(ByVal codError) As Int32 'Segun especificaci�n WSDL v1.1
                Select Case codError
                    Case 1, 2, 5, 6, 7, 8, 9, 10, 11, 13
                        Return uno
                    Case Else
                        Return zero
                End Select
            End Function
        End Class
        Public Class CodErrorExtendido
            Public Const RespuestaOk = "00"
            Public Const TransaccionFallida = "01"
            Public Const CIPNoExiste = "02"
            Public Const CIPEliminado = "03"
            Public Const CIPCancelado = "04"
            Public Const CIPExpirado = "05"
            Public Const CodigoMonedaNoCoincide = "06"
            Public Const MontoNoCoincide = "07"
            Public Const NotificacionFallidaRegistroAgencia = "08"
            Public Const NotificacionFallidaUsuario = "09"
            Public Const NotificacionFallidaPortal = "10"
            Public Const MontoOMonedaNoCoincide = "11"
            Public Const CodigoServicioNoCoincide = "12"
            Public Const CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide = "13"
            Public Const MovimientoPagoNoExiste = "14"
            Public Const FechaPagoNoActual = "15"
            Public Const CIPNoExisteONoCancelado = "16"
            Public Const CIPNoExisteONoCanceladoOMonedaNoCoincide = "17"
            Public Const CIPNoExisteONoGeneradoOMonedaNoCoincide = "18"
            Public Const MovimientoAnulacionNoExiste = "19"
            Public Const CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide = "20"
            Public Const FechaAnulacionNoActual = "21"
            Public Const TramaInvalida = "22"
            Public Const CodigoPaisErroneo = "23"
            Public Const CodigoEntidadErroneo = "24"
            'TIEMPO MAXIMO DE EXPIRACION
            Public Const NoSePermiteExtornar = "37"
        End Class
        Public Class IdentificacionTipoMensaje
            Public Const Consulta = "C"
            Public Const Directa = "D"
            Public Const Reversa = "R"
        End Class
        Public Class TipoPago
            Public Const Total = "T" 'pago total
            Public Const Parcial = "P" 'pago parcial
            Public Const Abierto = "A" 'monto abierto
        End Class
        Public Class CodMoneda
            Public Const Soles = "0"
            Public Const Dolares = "1"
        End Class
        Public Class CodMedioPago
            Public Const Efectivo = "0"
            Public Const Cheque = "1"
        End Class
        Public Class CodBarra
            Public Const CodEntidad = "20175"
            Public Const CodPais = "775"
        End Class
        Public Class CobroEstado
            Public Const Disponible = "D"
            Public Const Cobrada = "C"
            Public Const Reversada = "R"
            Public Const Anulada = "A"
            Public Const Procesada = "P"
            Public Const NoCobrable = "N"
        End Class
    End Class


    Public Class MensajeSolicitud
        Public Const EstadoOk As String = "1"
        Public Const EstadoNoValido As String = "0"
        Public Const EstadoException As String = "-1"

        'Generar Solicitud
        Public Const ErrorCodigoServicio As String = "La API o Codigo no est� asignada a ningun servicio."
        Public Const ErrorClavePublica As String = "Hubo un error al encriptar, verifique que este actualizada la clave publica del servicio en PagoEfectivo"
        Public Const ErrorNoTieneKPublica As String = "Pago Efectivo no tiene la clave publica del Servicio"
        Public Const ErrorFormatoXML As String = "El formato del xml es incorrecto."
        Public Const ErrorCCLaveFirma As String = "Firma Invalida, el CCLave es incorrecto."
        Public Const ErrorCodigoMoneda As String = "No se puedo Generar el C�digo de Identificaci�n de Pago. El c�digo de moneda recibido no es el correcto."
        Public Const ErrorGeneracionCliente As String = "No se pudo Generar el C�digo de Identificaci�n de Pago."
        Public Const ErrorGeneracion As String = "No se ha podido generar el C�digo de Identificaci�n de Pago"
        Public Const ErrorGeneracionMonto As String = "No se ha podido generar el C�digo de Identificaci�n de Pago. El monto total del CIP no concuerda con la suma del importe de los detalles"
        Public Shared Function GeneracionCIPOK(ByVal NumeroOrdenPago As String) As String
            Return String.Format("Se ha Generado el CIP: {0} .", NumeroOrdenPago)
        End Function
        Public Shared Function GeneracionOK(ByVal NumeroOrdenPago As String) As String
            Return String.Format("Se ha Generado la Solicitud de Pago: {0} .", NumeroOrdenPago)
        End Function
        Public Shared Function ErrorSolicitudYaGenerada(ByVal CodigoTransaccion As String) As String
            Return String.Format("El C�digo de Identificaci�n de Transaccion ya se ha generado anteriormente para esta operaci�n. Codigo Transaccion: {0} ", CodigoTransaccion)
        End Function
        Public Shared Function ErrorNoManejado(ByVal Exception As String) As String
            Return String.Format("Ha ocurrido un Error no Manejado:{0}", Exception)
        End Function

        'Consulta Solicitud
        Public Const SolicitudEncontrada As String = "Se encontr� la solicitud de Pago"
        Public Shared Function SolicitudNoEncontrada(ByVal NroSolicitud As String) As String
            Return String.Format("No se encontro la Solicitud: {0}", NroSolicitud)
        End Function
    End Class
    Public Class MensajeGeneracionCIP
        Public Const EstadoOk As String = "1"
        Public Const EstadoNoValido As String = "0"
        Public Const EstadoException As String = "-1"

        'Generar Solicitud
        Public Const ErrorCodigoServicio As String = "La API o Codigo no est� asignada a ningun servicio."
        Public Const ErrorClavePublica As String = "Hubo un error al encriptar, verifique que este actualizada la clave publica del servicio en PagoEfectivo"
        Public Const ErrorNoTieneKPublica As String = "Pago Efectivo no tiene la clave publica del Servicio"
        Public Const ErrorFormatoXML As String = "El formato del xml es incorrecto."
        Public Const ErrorCCLaveFirma As String = "Firma Invalida, el CCLave es incorrecto."
        Public Const ErrorCodigoMoneda As String = "No se puedo Generar el C�digo de Identificaci�n de Pago. El c�digo de moneda recibido no es el correcto."
        Public Const ErrorGeneracionCliente As String = "No se pudo Generar el C�digo de Identificaci�n de Pago."
        Public Const ErrorGeneracion As String = "No se ha podido generar el C�digo de Identificaci�n de Pago"
        Public Const ErrorGeneracionMonto As String = "No se ha podido generar el C�digo de Identificaci�n de Pago. El monto total del CIP no concuerda con la suma del importe de los detalles"
        Public Shared Function GeneracionOK(ByVal NumeroOrdenPago As String) As String
            Return String.Format("Se ha Generado la Solicitud de Pago: {0} .", NumeroOrdenPago)
        End Function
        Public Shared Function ErrorSolicitudYaGenerada(ByVal CodigoTransaccion As String) As String
            Return String.Format("El C�digo de Identificaci�n de Transaccion ya se ha generado anteriormente para esta operaci�n. Codigo Transaccion: {0} ", CodigoTransaccion)
        End Function
        Public Shared Function ErrorNoManejado(ByVal Exception As String) As String
            Return String.Format("Ha ocurrido un Error no Manejado:{0}", Exception)
        End Function

        'Consulta Solicitud
        Public Const SolicitudEncontrada As String = "Se encontr� la solicitud de Pago"
        Public Shared Function SolicitudNoEncontrada(ByVal NroSolicitud As String) As String
            Return String.Format("No se encontro la Solicitud: {0}", NroSolicitud)
        End Function
    End Class
    Public Class EstadosTX
        Public Const Ok As String = "OK"
        Public Const Pendiente As String = "P"
    End Class
    Public Class Empresa
        Public Class Codigo
            Public Const Comercio As String = "EEC"
        End Class
    End Class
    Public Class MedioPagoSBK
        Public Const MediosVirtuales As Int32 = 1
        Public Const Agencias As Int32 = 2
        Public Const MediosVirtualesDesc As String = "Medios virtuales"
        Public Const AgenciasDesc As String = "Agencias"
    End Class
    Public Enum EstadoDevolucion
        Solicitado = 691
        EnProceso = 692
        ChequeGenerado = 693
        Emitido = 694
        Expirado = 695
    End Enum
    Public Class EstadoDevolucionDescripcion
        Public Const Solicitado As String = "Solicitado"
        Public Const EnProceso As String = "En Proceso"
        Public Const ChequeGenerado As String = "Cheque Generado"
        Public Const Emitido As String = "Emitido"
        Public Const Expirado As String = "Expirado"
    End Class

    Public Class TipoRegistroLog
        Public Const MongoDB As String = "1"
        Public Const SQL As String = "2"
        Public Const MongoDBSQL As String = "3"
    End Class

   

End Class
