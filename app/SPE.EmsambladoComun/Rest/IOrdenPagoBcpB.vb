﻿Imports System.ServiceModel
Imports System.ServiceModel.Web
Imports SPE.Entidades
Imports _3Dev.FW.Entidades
    
<ServiceContract()>
Public Interface IOrdenPagoBcpB

    '<OperationContract()>
    '<WebGet(UriTemplate:="/ConsultarOrdenPagoPorNumeroOrdenPago?NumeroOrdenPago={NumeroOrdenPago}&fecha={fecha}")>
    'Function ConsultarOrdenPagoPorNumeroOrdenPago(ByVal NumeroOrdenPago As String, ByVal fecha As Integer) As Integer

    <OperationContract()>
    <WebGet(UriTemplate:="/ConsultarHistoricoMovimientosporIdOrden/{idOrdenPago}", _
            BodyStyle:=WebMessageBodyStyle.Bare, _
            ResponseFormat:=System.ServiceModel.Web.WebMessageFormat.Xml)>
    Function ConsultarHistoricoMovimientosporIdOrden(ByVal idOrdenPago As String) As List(Of BEMovimiento)

    <OperationContract()>
    <WebInvoke(Method:="POST", UriTemplate:="ConsultarOrdenPagoPorId", ResponseFormat:=ServiceModel.Web.WebMessageFormat.Xml)>
    Function ConsultarOrdenPagoPorId(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago

End Interface
