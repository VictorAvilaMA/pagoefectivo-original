Imports System.ServiceModel

<ServiceContract()>
Public Interface ISeguridad

	<OperationContract()>
    Function ValidarServicioClaveAPI(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean

    '<add Peru.Com FaseIII>
    <OperationContract()>
    Function GenerateKeyPrivatePublicSPE() As Boolean
    <OperationContract()>
    Function GetPublicKey(ByVal codServicio As String) As String
    <OperationContract()>
    Function SaveKeyPublic(ByVal codEntidad As Int32, ByVal keyPublic As String) As Boolean
    <OperationContract()>
    Function GetPublicKeySPE() As String
    <OperationContract()>
    Function GetPrivateKeySPE() As String

    '<OperationContract()>
    'Function CifrarMensaje(ByVal mensajeACifrar As String, ByVal codEntidad As String) As String()
    '<OperationContract()>
    'Function DesCifrarMensaje(ByVal mensajeADescifrar As String, ByVal keyCifrado As String, ByVal codEntidad As String) As String


End Interface
