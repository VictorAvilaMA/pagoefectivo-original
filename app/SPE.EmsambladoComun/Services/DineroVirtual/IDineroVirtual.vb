﻿Imports System.ServiceModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IDineroVirtual
#Region "CUENTAS Dinero Virtual"
    <OperationContract()>
    Function CuentaDineroVirtualLeer(ByVal oBeCuentaDineroVirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual

    <OperationContract()>
    Function CuentaDineroVirtualListar(ByVal oBeCuentaDineroVirtual As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)

    <OperationContract()>
    Function CuentaDineroVirtualInsertar(ByVal oBeCuentaDineroVirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual

    <OperationContract()>
    Sub CuentaDineroVirtualActualizar(ByVal oBeCuentaDineroVirtual As BECuentaDineroVirtual)

    <OperationContract()>
    Sub CuentaDineroVirtualCambiarEstado(ByVal oBeCuentaDineroVirtual As BECuentaDineroVirtual)

#End Region

#Region "MOVIMIENTOS Dinero Virtual"
    <OperationContract()>
    Function MovimientoDineroVirtualLeer(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As BEMovimientoCuenta

    <OperationContract()>
    Function MovimientoDineroVirtualListar(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)

    <OperationContract()>
    Function MovimientoDineroVirtualInsertar(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As BEMovimientoCuenta

#End Region

#Region "SOLICITUDES DE RETIRO Dinero Virtual"
    <OperationContract()>
    Function SolicitudRetiroDineroVirtualLeer(ByVal oBESolicitudRetiroDineroVirtual As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual

    <OperationContract()>
    Function SolicitudRetiroDineroVirtualListar(ByVal oBESolicitudRetiroDineroVirtual As BESolicitudRetiroDineroVirtual) As List(Of BESolicitudRetiroDineroVirtual)

    <OperationContract()>
    Function SolicitudRetiroDineroVirtualInsertar(ByVal oBESolicitudRetiroDineroVirtual As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual

    <OperationContract()>
    Sub SolicitudRetiroDineroVirtualActualizar(ByVal oBESolicitudRetiroDineroVirtual As BESolicitudRetiroDineroVirtual)

    <OperationContract()>
    Sub SolicitudRetiroDineroVirtualCambiarEstado(ByVal oBESolicitudRetiroDineroVirtual As BESolicitudRetiroDineroVirtual)
#End Region

#Region "TOKEN Dinero Virtual"
    <OperationContract()>
    Function TokenDineroVirtualLeer(ByVal oBETokenDineroVirtual As BETokenDineroVirtual) As BETokenDineroVirtual

    <OperationContract()>
    Function TokenDineroVirtualListar(ByVal oBETokenDineroVirtual As BETokenDineroVirtual) As List(Of BETokenDineroVirtual)

    <OperationContract()>
    Function TokenDineroVirtualInsertar(ByVal oBETokenDineroVirtual As BETokenDineroVirtual) As BETokenDineroVirtual

#End Region

End Interface
