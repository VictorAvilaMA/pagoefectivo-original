Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades
Imports _3Dev.FW.Entidades

':P
<ServiceContract()>
Public Interface IAgenciaBancaria
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase
    <OperationContract()>
    Function RegistrarCancelacionOrdenPagoAgenciaBancaria(ByVal obeMovimiento As BEMovimiento) As Integer

    'Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As SPE.Entidades.webServiceXResponse
    <OperationContract()>
    Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String()

    <OperationContract()>
    Function RegistrarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer

    <OperationContract()>
    Function ActualizarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer

    <OperationContract()>
    Function ConsultarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As List(Of BusinessEntityBase)

    <OperationContract()>
    Function ConsultarAgenciaBancariaxCodigo(ByVal codAgenciaBancaria As String) As BEAgenciaBancaria

    <OperationContract()>
    Function ConsultarAgenciaBancariaxId(ByVal idAgenciaBancaria As Integer) As BEAgenciaBancaria

    <OperationContract()>
    Function AnularOperacionPago(ByVal obeMovimiento As BEMovimiento) As Integer
    '
    <OperationContract()>
    Function GenerarEstructuraXML(ByVal array() As String) As SPE.Entidades.webServiceXResponse

    <OperationContract()>
    Function RegistrarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As BEConciliacion

    <OperationContract()>
    Function ConsultarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria)

#Region "Integracion BCP"
    <OperationContract()>
    Function ConsultarBCP(ByVal obeOrdenPago As BEOrdenPago) As BEBCPResponse
    <OperationContract()>
    Function PagarBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse

    <OperationContract()>
        <FaultContract(GetType(_3Dev.FW.Excepciones.FWBusinessException))>
    Function AnularBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse

#End Region

#Region "Integraci?n BBVA"
    <OperationContract()>
    Function ConsultarBBVA(ByVal request As BEBBVAConsultarRequest) As BEBBVAResponse(Of BEBBVAConsultarResponse)

    <OperationContract()>
    Function PagarBBVA(ByVal request As BEBBVAPagarRequest) As BEBBVAResponse(Of BEBBVAPagarResponse)

    <OperationContract()>
    Function AnularBBVA(ByVal request As BEBBVAAnularRequest) As BEBBVAResponse(Of BEBBVAAnularResponse)

    <OperationContract()>
    Function ExtornarBBVA(ByVal request As BEBBVAExtornarRequest) As BEBBVAResponse(Of BEBBVAExtornarResponse)
#End Region

#Region "Integración Scotiabank"
    <OperationContract()>
    Function ConsultarSBK(ByVal request As BESBKConsultarRequest) As BEBKResponse(Of BESBKConsultarResponse)
    <OperationContract()>
    Function CancelarSBK(ByVal request As BESBKCancelarRequest) As BEBKResponse(Of BESBKCancelarResponse)
    <OperationContract()>
    Function AnularSBK(ByVal request As BESBKAnularRequest) As BEBKResponse(Of BESBKAnularResponse)
    <OperationContract()>
    Function ExtornarCancelacionSBK(ByVal request As BESBKExtornarCancelacionRequest) As BEBKResponse(Of BESBKExtornarCancelacionResponse)
    <OperationContract()>
    Function ExtornarAnulacionSBK(ByVal request As BESBKExtornarAnulacionRequest) As BEBKResponse(Of BESBKExtornarAnulacionResponse)
#End Region

#Region "Integración Interbank"
    <OperationContract()>
    Function ConsultarIBK(ByVal request As BEIBKConsultarRequest) As BEBKResponse(Of BEIBKConsultarResponse)
    <OperationContract()>
    Function CancelarIBK(ByVal request As BEIBKCancelarRequest) As BEBKResponse(Of BEIBKCancelarResponse)
    <OperationContract()>
    Function AnularIBK(ByVal request As BEIBKAnularRequest) As BEBKResponse(Of BEIBKAnularResponse)
    <OperationContract()>
    Function ExtornarCancelacionIBK(ByVal request As BEIBKExtornarCancelacionRequest) As BEBKResponse(Of BEIBKExtornarCancelacionResponse)
    <OperationContract()>
    Function ExtornarAnulacionIBK(ByVal request As BEIBKExtornarAnulacionRequest) As BEBKResponse(Of BEIBKExtornarAnulacionResponse)
#End Region

#Region "Integración Western Union"
    <OperationContract()>
    Function ConsultarWU(ByVal request As BEWUConsultarRequest) As BEWUConsultarResponse
    <OperationContract()>
    Function CancelarWU(ByVal request As BEWUCancelarRequest) As BEWUCancelarResponse
    <OperationContract()>
    Function AnularWU(ByVal request As BEWUAnularRequest) As BEWUAnularResponse
#End Region


#Region "Integraci?n BanBif"
    <OperationContract()>
    Function ConsultarBanBif(ByVal request As BEBanBifConsultarRequest) As BEBanBifResponse(Of BEBanBifConsultarResponse)

    <OperationContract()>
    Function PagarBanBif(ByVal request As BEBanBifPagarRequest) As BEBanBifResponse(Of BEBanBifPagarResponse)

    <OperationContract()>
    Function ExtornarBanBif(ByVal request As BEBanBifExtornoRequest) As BEBanBifResponse(Of BEBanBifExtornoResponse)

    '<OperationContract()>
    'Function AnularBanBif(ByVal request As BEBBVAAnularRequest) As BEBBVAResponse(Of BEBBVAAnularResponse)

#End Region

#Region "Integraci?n Kasnet"
    <OperationContract()>
    Function ConsultarKasnet(ByVal request As BEKasnetConsultarRequest) As BEKasnetResponse(Of BEKasnetConsultarResponse)

    <OperationContract()>
    Function PagarKasnet(ByVal request As BEKasnetPagarRequest) As BEKasnetResponse(Of BEKasnetPagarResponse)

    <OperationContract()>
    Function ExtornarKasnet(ByVal request As BEKasnetExtornarRequest) As BEKasnetResponse(Of BEKasnetExtornarResponse)
#End Region



End Interface
