Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades


<ServiceContract()>
Public Interface IAgenciaRecaudadora
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase
    <OperationContract()>
    Function RegistrarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer
    <OperationContract()>
    Function ActualizarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer
    <OperationContract()>
    Function ConsultarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As List(Of BEAgenciaRecaudadora)
    <OperationContract()>
    Function ConsultarAgenciaRecaudadoraPorIdAgencia(ByVal IdAgencia As Integer) As BEAgenciaRecaudadora
    <OperationContract()>
    Function ConsultarAgenciaRecaudadoraPorIdUsuario(ByVal IdUsuario As Integer) As BEAgenciaRecaudadora
    <OperationContract()>
    Function RegistrarAgente(ByVal obeAgente As BEAgente) As Integer
    <OperationContract()>
    Function ActualizarAgente(ByVal obeAgente As BEAgente) As Integer
    <OperationContract()>
    Function ConsultarAgente(ByVal obeAgente As BEAgente) As List(Of BEAgente)
    <OperationContract()>
    Function ConsultarAgentePorIdAgenteOrIdUsuario(ByVal opcion As Integer, ByVal IdAgencia As Integer) As BEAgente
    <OperationContract()>
    Function ConsultarCaja(ByVal obeCaja As BECaja) As List(Of BECaja)
    <OperationContract()>
    Function RegistrarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
    <OperationContract()>
    Function RegistrarMovimiento(ByVal obeMovimiento As BEMovimiento) As Integer
    <OperationContract()>
    Function ConsultarAgenteCajaPorIdUsuarioIdEstado(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)


    <OperationContract()>
    Function ConsultaCajaActiva(ByVal IdAgente As Integer) As BEAgenteCaja
    <OperationContract()>
    Function ConsultaMontosPorAgenteCaja(ByVal IdAgenteCaja As Integer, ByRef ListCierre As Collection(Of BEMovimiento)) As Collection(Of BEMovimiento)
    <OperationContract()>
    Function LiquidarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
    <OperationContract()>
    Function ConsultarAgentesConCajaPendientesdeCierre(ByVal IdTipoFechaValuta As Integer, ByVal IdAgencia As Integer) As List(Of BEAgente)
    <OperationContract()>
    Function ConsultarCajaActivaPorIdAgente(ByVal IdAgente As Integer) As BEAgenteCaja
    <OperationContract()>
    Function RegistrarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer
    <OperationContract()>
    Function ActualizarEstadoFechaValuta(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
    <OperationContract()>
    Function ActualizarEstadoFechaValutaPorIdAgente(ByVal obeFechaValuta As BEFechaValuta) As Integer
    <OperationContract()>
    Function ConsultarFechaValutaAgenteCaja(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta
    <OperationContract()>
    Function ConsultarFechaValutaPorAgente(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta
    <OperationContract()>
    Function AnularAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
    <OperationContract()>
    Function ConsultarCajasCerradas(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)
    <OperationContract()>
    Function LiquidarCajaEnBloque(ByVal obeOrdenPago As BEOrdenPago, ByVal ListAgenteCaja As List(Of BEAgenteCaja)) As Integer
    <OperationContract()>
    Function ConsultarCajaAvanzada(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)
    <OperationContract()>
    Function ConsultarAgentesPorAgencia(ByVal IdSupervisor As Integer) As List(Of BEAgenteCaja)
    <OperationContract()>
    Function ConsultarCajaActivaPorAgente(ByVal IdAgente As Integer) As BEAgenteCaja
    <OperationContract()>
    Function RegistrarSobranteFaltante(ByVal obeMovimiento As BEMovimiento) As Integer
    <OperationContract()>
    Function ConsultarDetalleDeCaja(ByVal IdAgenteCaja As Integer) As List(Of BEMovimiento)
    <OperationContract()>
    Function ConsultarCajasLiquidadas(ByVal IdUsuario As Integer, ByVal NroOrdenPago As String, ByVal FechaInicio As DateTime, ByVal FechaFin As DateTime) As List(Of BEAgenteCaja)
    <OperationContract()>
    Function ConsultarDetalleMovimientosCaja(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEMovimientoConsulta)
    <OperationContract()>
    Function ValidarEstadoCaja(ByVal Form As String, ByVal IdUsuario As Integer) As String()()
    <OperationContract()>
    Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As BEMovimiento
    '
    <OperationContract()>
    Function ProbarConexion() As String
End Interface
