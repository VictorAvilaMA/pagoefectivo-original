Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IBanco

    <OperationContract()>
    Function RegistrarBanco(ByVal oBEBanco As BEBanco) As Integer
    <OperationContract()>
    Function ConsultarBanco(ByVal oBEBanco As BEBanco) As List(Of BEBanco)
    <OperationContract()>
    Function ConsultarCuentaBanco(ByVal oBECuentaBanco As BECuentaBanco) As List(Of BECuentaBanco)
    'Function ConsultarBancoPorId(ByVal IdBanco As Integer) As List(Of BEBanco)

    '<upd proc.Tesoreria>
    <OperationContract()>
    Function RegistrarDeposito(ByVal beDeposito As BEDeposito, ByVal detalleDeposito As List(Of BEOrdenPago)) As Integer
    <OperationContract()>
    Function ConsultarDepositoRegistroFactura(ByVal beDeposito As BEDeposito) As List(Of BEDeposito)
    <OperationContract()>
    Function RegistrarFactura(ByVal beFactura As BEFactura, ByVal detalleDeposito As List(Of BEDeposito)) As Integer
    <OperationContract()>
    Function ConsultarDeposito(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarDepositoByIdFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarDepositoDetalleByIdDeposito(ByVal request As BEDeposito) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ConsultarFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarDepositoReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarFacturaReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionArchivo)
End Interface
