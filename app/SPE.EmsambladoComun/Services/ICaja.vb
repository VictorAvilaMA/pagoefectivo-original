Imports System.ServiceModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface ICaja

	<OperationContract()>
    Function RegistrarCaja(ByVal obeCaja As BECaja)
	<OperationContract()>
    Function ActualizarCaja(ByVal obeCaja As BECaja)
	<OperationContract()>
    Function ConsultarCaja(ByVal IdAgenciaRecaudadora As Integer) As List(Of BECaja)
	<OperationContract()>
    Function ObtenerAgenciaPorUsuario(ByVal IdUsuario As Integer) As BEAgenciaRecaudadora
	<OperationContract()>
    Function OtorgarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer

End Interface
