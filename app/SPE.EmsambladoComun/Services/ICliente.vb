Imports System.ServiceModel
Imports System
Imports SPE.Entidades
Imports _3Dev.FW.Entidades.Seguridad

<ServiceContract()>
Public Interface ICliente
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase
    'Function RegistrarCliente(ByVal obeCliente As BECliente) As Integer
    <OperationContract()>
    Function ActualizarEstadoUsuario(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
    <OperationContract()>
    Function CambiarContraseņaUsuario(ByVal obeUsuario As BEUsuario) As Integer
    <OperationContract()>
    Function ValidarEmailUsuarioPorEstado(ByVal obeUsuario As BEUsuario) As BEUsuario
    <OperationContract()>
    Function GenerarPreordenPagoSuscriptores(ByVal xmlstring As String, ByVal urlservicio As String) As Integer
    <OperationContract()>
    Function ObtenerClientexIdUsuario(ByVal obeCliente As BECliente) As BECliente
    <OperationContract()>
    Function ValidarTipoyNumeroDocPorEmail(ByVal obeUsuario As BEUsuario) As Integer

End Interface
