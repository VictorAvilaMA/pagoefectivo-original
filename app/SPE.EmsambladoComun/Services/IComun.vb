Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IComun

	<OperationContract()>
    Function ConsultarMoneda() As List(Of BEMoneda)
	<OperationContract()>
    Function ConsultarMonedaPorId(ByVal parametro As Object) As BEMoneda
    <OperationContract()>
    Function ConsultarMedioPago() As List(Of BEMedioPago)
    '<upd Proc.Tesoreria>
    <OperationContract()>
    Function ConsultarMedioPagoBancoByBanco(be As BEBanco) As List(Of BEMedioPagoBanco)
	<OperationContract()>
    Function ConsultarTarjeta() As List(Of BETarjeta)
	<OperationContract()>
    Function RegistrarDetalleTarjeta(ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
	<OperationContract()>
    Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As List(Of BEMovimiento)
	<OperationContract()>
    Function ConsultarFechaHoraActual() As DateTime
	<OperationContract()>
    Function ConsultarTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
	<OperationContract()>
    Function ConsultarPasarelaMedioPago() As List(Of BEPasarelaMedioPago)
	<OperationContract()>
    Function ConsultaNumCorrelativo(ByVal url As String) As Integer
	<OperationContract()>
    Function RegistrarLog(ByVal obelog As BELog) As Integer
	<OperationContract()>
    Function RegistarPasLog(ByVal oPasLog As BELog) As Integer
	<OperationContract()>
    Function ConsultarLog(ByVal obelog As BELog) As List(Of BELog)
    Sub InicializarTodoCache()
    Sub InicializarElementoCache(ByVal nombre As String)
    <OperationContract()>
    Function RegistrarLogBBVA(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeBBVA As Object) As Int64

    Function RegistrarLogBCP(ByVal obelog As BELog) As Int64
    <OperationContract()>
    Function RegistrarLogBCPResquest(ByVal codOperacion As String, ByVal obeBCP As BEBCPRequest) As String
    <OperationContract()>
    Function RegistrarLogBCPResponse(ByVal codOperacion As String, ByVal obeBCP As BEBCPResponse) As Int64
    <OperationContract()>
    Function RegistrarLogFC(ByVal type As String, ByVal obeid As Int64, ByVal obeFC As Object, ByVal oBELog As BELog) As Int64
    <OperationContract()>
    Function RegistrarLogLF(ByVal type As String, ByVal obeid As Int64, ByVal obeLF As Object, ByVal oBELog As BELog) As Int64
    <OperationContract()>
    Function RegistrarLogSBK(ByVal type As String, ByVal obeid As Int64, ByVal obeSBK As Object, ByVal oBELog As BELog) As Int64
    <OperationContract()>
    Function RegistrarLogIBK(ByVal type As String, ByVal obeid As Int64, ByVal obeIBK As Object, ByVal oBELog As BELog) As Int64
    <OperationContract()>
    Function RegistrarLogWU(ByVal type As String, ByVal obeid As Int64, ByVal obeWU As Object) As Int64
    <OperationContract()>
    Function RegistrarLogBanBif(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeBBVA As Object) As Int64
    <OperationContract()>
    Function RegistrarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As Int64
    <OperationContract()>
    Function ConsultarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As List(Of BELogNavegacion)
    <OperationContract()>
    Function RegistrarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Int64
    <OperationContract()>
    Function ValidarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
    <OperationContract()>
    Function ConsultarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
    <OperationContract()>
    Function EliminarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Int64
    <OperationContract()>
    Function RegistrarLogKasnet(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeKasnet As Object) As Int64
    '<add Peru.Com FaseIII>
    <OperationContract()>
    Function RegistrarLogRedirect(ByVal oBELogRedirect As BELogRedirect) As Integer

    <OperationContract()>
    Function ActualizarContraseniaOlvidada(ByVal be As BEUsuarioBase) As BEUsuarioBase

    <OperationContract()>
    Function RegistrarTokenCambioCorreo(ByVal be As BEUsuarioBase) As BEUsuarioBase

    <OperationContract()>
    Function SuscriptorRegistrar(ByVal oBESuscriptor As SPE.Entidades.BESuscriptor) As String

    <OperationContract()>
    Function ConsultarPlantillaTipoVariacion() As List(Of BEParametro)

    'Liquidaciones
    <OperationContract()>
    Function RecuperarCipsNoConciliados(ByVal idMoneda As Integer, ByVal fecha As DateTime) As List(Of BELiquidacion)


    <OperationContract()>
    Function RecuperarDepositosBCP(ByVal idMoneda As Integer, ByVal empresas As String) As List(Of BELiquidacion)

    <OperationContract()>
    Function ActualizarPeriodo(ByVal be As BEPeriodoLiquidacion) As Integer

    <OperationContract()>
    Function RegistrarPeriodo(ByVal be As BEPeriodoLiquidacion) As Integer

    <OperationContract()>
    Function ConsultarIdDescripcionPeriodo() As List(Of BEPeriodoLiquidacion)

    <OperationContract()>
    Function RecuperarDatosPeriodoLiquidacion(ByVal be As BEPeriodoLiquidacion) As List(Of BEPeriodoLiquidacion)

    <OperationContract()>
    Function DeletePeriodo(ByVal be As BEPeriodoLiquidacion) As Integer

    'M MoMo
    <OperationContract()>
    Function RegistrarLogMoMo(ByVal type As String, ByVal obeid As Int64, ByVal obeMoMo As Object, ByVal oBELog As BELog) As Int64


End Interface
