Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IConciliacion
    <OperationContract()>
    Function ProcesarArchivoConciliacion(ByVal request As BEConciliacionArchivo) As BEConciliacionResponse
    <OperationContract()>
    Function ConsultarUltimasConciliaciones(ByVal request As BEConciliacionRequest) As List(Of BEConciliacionResponse)
    <OperationContract()>
    Function ConsultarConciliacionEntidad(ByVal request As BEConciliacionEntidad) As List(Of BEConciliacionEntidad)
    <OperationContract()>
    Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionArchivo)
    <OperationContract()>
    Function ConsultarDetalleConciliacion(ByVal request As BEConciliacionDetalle) As List(Of BEConciliacionDetalle)
    <OperationContract()>
    Sub RecalcularCIPsConciliacion(ByVal IdUsuario As Integer)
    <OperationContract()>
    Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ProcesarArchivoPreConciliacion(ByVal request As BEPreConciliacionRequest) As BEConciliacionResponse
    <OperationContract()>
    Function ConsultarConciliacionDiaria(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionDiaria)
    <OperationContract()>
    Function ConsultarPreConciliacionDetalle(ByVal request As BEConciliacionArchivo) As List(Of BEPreConciliacionDetalle)
End Interface
