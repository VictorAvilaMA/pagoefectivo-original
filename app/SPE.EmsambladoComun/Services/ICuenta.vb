Imports System.ServiceModel
<ServiceContract()>
Public Interface ICuenta

    <OperationContract()>
    Function ConsultarMovimientoCuenta(ByVal be As Entidades.BEMovimientoCuentaBanco) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarDetMovCuentaPorIdMovCuentaBanco(ByVal be As Entidades.BEDetMovimientoCuenta) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
End Interface
