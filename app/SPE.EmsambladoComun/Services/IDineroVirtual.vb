﻿Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IDineroVirtual
#Region "Victor"



    <OperationContract()>
    Function RegistrarCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As Integer

    <OperationContract()>
    Function ActualizarEstadoCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As Integer

    <OperationContract()>
    Function ConsultarMovimientoXIdCuenta(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)

    <OperationContract()>
    Function ConsultarMovimientoXIdCuentaExportar(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)

    <OperationContract()>
    Function ActualizarMontoRecargaDineroVirtual(ByVal oBEMontoRecargaMonedero As List(Of BEMontoRecargaMonedero)) As Integer

    <OperationContract()>
    Function ConsultarMonedaXIdCliente(ByVal oBEMontoRecargaMonedero As BEMontoRecargaMonedero) As List(Of BEMontoRecargaMonedero)

    <OperationContract()>
    Function ConsultarClienteXIdClienteDV(ByVal entidad As BECliente) As BECliente

    <OperationContract()>
    Function ActualizarEstadoHabilitarMonederoClienteDV(ByVal oBECliente As BECliente) As Integer

    <OperationContract()>
    Function ConsultarMonedaMonederoVirtual() As List(Of BEMoneda)


#End Region


#Region "Joe"

    <OperationContract()>
    Function ActualizarCuentaDineroVirtual(ByVal entidad As BECuentaDineroVirtual) As Boolean

    <OperationContract()>
    Function ConsultarCuentaDineroVirtualUsuarioByNumero(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual

    <OperationContract()>
    Function ConsultarCuentaDineroVirtualUsuario(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)

    <OperationContract()>
    Function ConsultarCuentaDineroVirtualUsuarioByCuentaId(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual

    <OperationContract()>
    Function ConsultarSolicitudRetiroMonedero(ByVal entidad As BESolicitudRetiroDineroVirtual) As List(Of BESolicitudRetiroDineroVirtual)

    <OperationContract()>
    Function ConsultarSolicitudRetiroMonederoById(ByVal entidad As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual

    <OperationContract()>
    Function ActualizarSolicitudRetiroDinero(ByVal entidad As BESolicitudRetiroDineroVirtual) As Boolean
#End Region


#Region "IMPLEMENTADO POR MXRT¡N PATORA"
    <OperationContract()>
    Function ConsultarCuentasVirtualesAdministrador(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)

    <OperationContract()>
    Function ObtenerOrdenPagoCompletoPorNroOrdenPago(ByVal nroOrdenPago As Long) As BEOrdenPago

    <OperationContract()>
    Function RegistrarTokenMonedero(ByVal token As BETokenDineroVirtual) As BETokenDineroVirtual

    <OperationContract()>
    Function RegistrarPagoCIP(ByVal beMovimiento As BEMovimientoCuenta) As String

    <OperationContract()>
    Function RegistrarPagoCIPPortal(ByVal beMovimiento As BEMovimientoCuenta, ByVal oBESolicitudPago As BESolicitudPago) As String

    <OperationContract()>
    Function RegistrarSolicitudRetiroDinero(ByVal beSolicitudRetiroDinero As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual

    <OperationContract()>
    Function ConsultarCuentaDineroVirtualPorNroCuenta(ByVal NroCuenta As String) As BECuentaDineroVirtual

    <OperationContract()>
    Function ConsultarReglaMovimientoSospechoso() As List(Of BEReglaMovimientoSospechoso)

    <OperationContract()>
    Function RegistrarTransferenciaDinero(ByVal beMovimiento As BEMovimientoCuenta) As String

    <OperationContract()>
    Function ConsultarRegistroEjecucion(ByVal IdRegla As Integer, ByVal Anio As Integer, ByVal Mes As Integer) As List(Of BERegistroEjecucionRegla)

    <OperationContract()>
    Function ConsultarResultadoEjecucionRegla(ByVal entidad As BEResultadoEjecucionRegla) As List(Of BEResultadoEjecucionRegla)

    <OperationContract()>
    Function ConsultarDetalleResultadoEjecucion(ByVal entidad As BEDetalleResultadoEjecucion) As List(Of BEDetalleResultadoEjecucion)

    <OperationContract()>
    Function ReenviarEmailToken(ByVal IdToken As Long) As BETokenDineroVirtual
	
	<OperationContract()>
    Function ConsultarTokenByID(ByVal IdToken As Long) As BETokenDineroVirtual
	
	
#End Region
    'MDS
    <OperationContract()>
    Function ValidarRegistrarCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual

    <OperationContract()>
    Function ObtenerParametroMonederoByID(ByVal IdParametro As Integer) As BEParametroMonedero

    <OperationContract()>
    Function ConsultaUsuarioByIdCuenta(ByVal IdCuenta As Integer) As Integer

    <OperationContract()>
    Function ConsultaEstadoUsuarioByIdUsuario(ByVal IdUsuario As Integer) As Integer


End Interface
