Imports System.ServiceModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IEmail

    <OperationContract()>
    Function EnviarCorreoConfirmacionUsuario(ByVal userId As Integer, ByVal nombrecliente As String, ByVal clave As String, ByVal pguid As String, ByVal pto As String) As Integer
    <OperationContract()>
    Function EnviarCorreoAAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer
    'agregado
    <OperationContract()>
    Function EnviarCorreoContactarAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer

    <OperationContract()>
    Function EnviarEmailGeneral(ByVal phost As String, ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer
    <OperationContract()>
    Function EnviarEmail(ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer
    <OperationContract()>
    Function EnviarEmailContactenos(ByVal obeMail As BEMailContactenos) As Int32
    'jefferson mendoza
    <OperationContract()>
    Function EnviarEmailContactenosEmpresa(ByVal obeMail As BEMailContactenosEmpresa) As Int32
    <OperationContract()>
    Function EnviarCorreoSuscriptor(ByVal EmailSuscriptor As String) As Int32
#Region "ToGenerado"
    <OperationContract()>
    Function EnviarEmailCambioDeEstadoCipDeExpAGen(ByVal oBEOrdenPago As BEOrdenPago) As Int32
#End Region


End Interface
