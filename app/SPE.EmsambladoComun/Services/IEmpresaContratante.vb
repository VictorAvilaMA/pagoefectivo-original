Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IEmpresaContratante

    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase
    <OperationContract()>
    Function OcultarEmpresa(ByVal idservicio As Integer) As BEOcultarEmpresa
    <OperationContract()>
    Function RegistrarTransaccionesaLiquidar(ByVal be As BETransferencia) As Integer
    <OperationContract()>
    Function ConsultarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ObtenerFechaInicioTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime
    <OperationContract()>
    Function ObtenerFechaInicioTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime
    <OperationContract()>
    Function ObtenerFechaFinTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime
    <OperationContract()>
    Function ObtenerFechaFinTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime
    <OperationContract()>
    Function ActualizarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    <OperationContract()>
    Function ObtenerTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BETransferencia
    <OperationContract()>
    Function ValidarPendientesaConciliar(ByVal be As BETransferencia) As Integer
#Region "Configuración de cuenta y banco de empresas"
    <OperationContract()>
    Function InsertarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    <OperationContract()>
    Function ActualizarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    <OperationContract()>
    Function ConsultarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    <OperationContract()>
    Function ConsultarCuentaEmpresaPorId(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase
#End Region
#Region "LiquidacionesLotes"
    <OperationContract()>
    Function RecuperarEmpresasLiquidacion(ByVal txtFechaDe As Date, ByVal txtFechaAl As Date, ByVal idPeriodoLiquidacion As Integer, ByVal idMoneda As Integer) As System.Collections.Generic.List(Of BEEmpresaContratante)
#End Region
End Interface
