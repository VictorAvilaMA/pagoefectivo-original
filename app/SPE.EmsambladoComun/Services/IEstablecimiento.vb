Imports System.ServiceModel
Imports System
Imports System.Collections.Generic

Imports SPE.Entidades

<ServiceContract()>
Public Interface IEstablecimiento
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase

	<OperationContract()>
    Function ObtenerOperadores() As List(Of BEOperador)
	<OperationContract()>
    Function ConsultarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
	<OperationContract()>
    Function CancelarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
	<OperationContract()>
    Function AnularOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
    <OperationContract(Name:="AperturarCaja2")>
    Function AperturarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
    <OperationContract(Name:="AperturarCaja")>
    Function AperturarCaja() As String
	<OperationContract()>
    Function CerrarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
	<OperationContract()>
    Function CerrarAperturarCaja() As String
	<OperationContract()>
    Function GetLiquidacionPendientexIdEstablecimiento(ByVal objEstablecimiento As BEEstablecimiento) As List(Of BEEstablecimiento)
	<OperationContract()>
    Function GetPendientesCancelacion(ByVal objComercio As BEComercio) As List(Of BEEstablecimiento)
	<OperationContract()>
    Function LiquidarBloque(ByVal objAperturaOrigen As List(Of BEAperturaOrigen)) As Integer
	<OperationContract()>
    Function ConsultarCajasLiquidadas(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEMovimiento)

End Interface
