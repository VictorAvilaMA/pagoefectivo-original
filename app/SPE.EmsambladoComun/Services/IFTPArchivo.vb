Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades


<ServiceContract()>
Public Interface IFTPArchivo
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase
	<OperationContract()>
    Function ProcesarArchivoFTPParaNotificar() As List(Of BEServicio)
	<OperationContract()>
    Function ObtenerFTPArchivoPorId(ByVal id As Object, ByVal OrderBy As String, ByVal IsAccending As Boolean) As BEFTPArchivo

End Interface
