Imports System.ServiceModel
Imports System
Imports SPE.Entidades
Imports _3Dev.FW.Entidades

<ServiceContract()>
Public Interface IOrdenPago
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase

    'formulario-------------------------------------->
    <OperationContract()>
    Function ConsutarFormularioActivo(ByVal IdServicio As Integer) As Boolean

    <OperationContract()>
    Function ConsultarOrdenPagoPorOrdenIdComercio(ByVal OrdenIdComercio As String, ByVal IdServicio As Integer) As BEOPXServClienCons
    <OperationContract()>
    Function ConsultarOrdenPagoPorNumeroOrdenPago(ByVal NumeroOrdenPago As String) As Integer
    <OperationContract()>
    Function GenerarOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
    'Function GenerarOrdenPagoXML(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
    <OperationContract()>
    Function GenerarPreOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
    <OperationContract()>
    Function ConsultarOrdenPagoRecepcionar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
    <OperationContract()>
    Function ActualizarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
    <OperationContract()>
    Function ProcesoRecepcionarOrdenPago(ByVal obeMovimiento As BEMovimiento, ByVal obeOrdenPago As BEOrdenPago, ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
    <OperationContract()>
    Function ConsultarOrdenPagoPendientes(ByVal obeOrdePago As SPE.Entidades.BEOrdenPago, ByVal orderedBy As String, ByVal isAccending As Boolean) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ConsultarOrdenPagoPorId(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
    <OperationContract()>
    Function ConsultarDetalleOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As BEDetalleOrdenPago
    <OperationContract()>
    Function ConsultarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
    <OperationContract()>
    Function ExpirarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
    <OperationContract()>
    Function ConsultarOrdenesPagoListaParaExpiradas() As List(Of BEOrdenPago)
    <OperationContract()>
    Function RealizarProcesoExpiracionOrdenPago() As Integer
    <OperationContract()>
    Function ConsultarOrdenPagoNotificar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
    <OperationContract()>
    Function ValidarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String
    <OperationContract()>
    Function GenerarCIP(ByVal request As BEGenCIPRequest) As BEGenCIPResponse
    <OperationContract()>
    Function AnularOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal obeMovimiento As BEMovimiento) As Integer
    <OperationContract()>
    Function ObtenerCIPInfoByUrLServicio(ByVal request As BEObtCIPInfoByUrLServRequest) As BEObtCIPInfoByUrLServResponse

    <OperationContract()>
    Function WSActualizarCIP(ByVal request As BEWSActualizaCIPRequest) As BEWSActualizaCIPResponse

    <OperationContract()>
    Function WSEliminarCIP(ByVal request As BEWSElimCIPRequest) As BEWSElimCIPResponse
    <OperationContract()>
    Function WSGenerarCIP(ByVal request As BEWSGenCIPRequest) As BEWSGenCIPResponse

    <OperationContract()>
    Function WSConsultarCIPConciliados(ByVal request As BEWSConsCIPsConciliadosRequest) As BEWSConsCIPsConciliadosResponse
    <OperationContract()>
    Function WSConsultarCIP(ByVal request As BEWSConsultarCIPRequest) As BEWSConsultarCIPResponse
    <OperationContract()>
    Function WSConsultarCIPv2(ByVal request As BEWSConsultarCIPv2Request) As BEWSConsultarCIPv2Response
    <OperationContract()>
    Function WSConsultarCIPMod1(ByVal request As BEWSConsultarCIPRequestMod1) As BEWSConsultarCIPResponseMod1
    <OperationContract()>
    Function WSEliminarCIPMod1(ByVal request As BEWSElimCIPRequestMod1) As BEWSElimCIPResponseMod1
    <OperationContract()>
    Function WSActualizarCIPMod1(ByVal request As BEWSActualizaCIPRequestMod1) As BEWSActualizaCIPResponseMod1
    <OperationContract()>
    Function WSGenerarCIPMod1(ByVal request As BEWSGenCIPRequestMod1) As BEWSGenCIPResponseMod1

    '19/11/2010 - Walter Rojas Aguilar
    <OperationContract()>
    Function ConsultarOPsByServicio(ByVal oBEPagoServicio As BEPagoServicio) As List(Of BEPagoServicio)
    <OperationContract()>
    Function ConsultarOPsByCIPs(ByVal oBEPagoServicio As BEPagoServicio) As List(Of BEPagoServicio)

    '<add Peru.Com FaseIII>
    <OperationContract()>
    Function WSSolicitarPago(ByVal request As BEWSSolicitarRequest) As BEWSSolicitarResponse
    <OperationContract()>
    Function WSConsultarSolicitudPago(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse
    <OperationContract()>
    Function ConsultarSolicitudPagoPorToken(ByVal obeSolicitudPago As BESolicitudPago) As BESolicitudPago
    <OperationContract()>
    Function GenerarOrdenPagoFromSolicitud(ByVal obeSolicitudPago As BESolicitudPago) As BEOrdenPago
    <OperationContract()>
    Function ConsultarSolicitudXMLPorID(ByVal obeSolicitudPago As BESolicitudPago) As String
    <OperationContract()>
    Function RealizarProcesoExpiracionSolicitudPago() As Integer

    '<add Peru.Com FaseIII MDS>
    <OperationContract()>
    Function ObtenerOrdenPagCabeceraPorNumeroOrden(ByVal numeroOrden As String) As BEOrdenPago
    <OperationContract()>
    Function ConsultarDetalleOrdenPagoPorIdOrdenPago(ByVal idOrdenPago As Integer) As List(Of BEDetalleOrdenPago)
    <OperationContract()>
    Function ConsultarHistoricoMovimientosporIdOrden(ByVal idOrdenPago As Integer) As List(Of BEMovimiento)



#Region "Devoluciones"
    <OperationContract()>
    Function RegistrarDevolucion(ByVal oBEDevolucion As BEDevolucion) As BEDevolucion
    <OperationContract()>
    Function ConsultarDevoluciones(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
    <OperationContract()>
    Function ActualizarDevolucion(ByVal oBEDevolucion As BEDevolucion) As Int64
    'DevolucionTelefonica
    <OperationContract()>
    Function ConsultarCipDevolucion(ByVal NroCip As Int64, ByVal IdUsuario As Int32) As BEOrdenPago
    <OperationContract()>
    Function ObtenerArchivoDevolucion(ByVal Archivo As String) As Byte()
    <OperationContract()>
    Function ConsultarDevolucionesExcel(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
#End Region
    <OperationContract()>
    Function ActualizarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64
    <OperationContract()>
    Function EliminarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64
    <OperationContract()>
    Function ConsultarOrdenPagoPorIdOrdenYIdRepresentante(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
    <OperationContract()>
    Function EliminarCIPWeb(ByVal oBEOrdenPago As BEOrdenPago) As Int64
#Region "SagaInicio"
    <OperationContract()>
    Function WSConsultarSolicitudPagov2(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse
    'SagaFin
#End Region
#Region "ToGenerado"
    <OperationContract()>
    Function ConsultarOrdenPagoPorIdSoloGenYExp(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago

    <OperationContract()>
    Function ActualizarCIPDeExpAGen(ByVal oBEOrdenPago As BEOrdenPago) As Int64
#End Region


#Region "ExtornarCIP"
    <OperationContract()>
    Function ExtornarCIP(ByVal nroCIP As String, ByVal tipoExtorno As Integer) As Int64
#End Region

#Region "Messaging"
    <OperationContract()>
    Function RegisterCellPhoneNumber(ByVal messaging As Entidades.BEMobileMessaging) As Integer
    <OperationContract()>
    Function ValidateCipExpiration(ByVal paymentOrderId As Long) As String
    <OperationContract()>
    Function GetMessageSms(smsTemplateId As Integer) As String
#End Region
End Interface
