Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IParametro
    '
    <OperationContract()>
    Function ConsultarParametroPorCodigoGrupo(ByVal CodigoGrupo As String) As List(Of BEParametro)
    <OperationContract()>
    Function ListarVersionServicio() As List(Of BEParametro)
    '
End Interface
