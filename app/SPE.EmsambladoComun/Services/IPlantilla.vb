Imports System.ServiceModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IPlantilla

    <OperationContract()>
    Function ConsultarPlantillaPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As Entidades.BEPlantilla

    <OperationContract()>
    Function ConsultarPlantillaPorTipoYVariacionExiste(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As Entidades.BEPlantilla

    '<add Peru.Com FaseIII>
    <OperationContract()>
    Function ConsultarPlantillaTipoPorMoneda(ByVal idMoneda As Integer) As List(Of BEPlantillaTipo)

    <OperationContract()>
    Function ConsultarPlantillaTipoPorMonedaYServicio(ByVal idMoneda As Int32, ByVal idServicio As Int32) As List(Of BEPlantillaTipo)

    <OperationContract()>
    Function GenerarPlantillaDesdePlantillaBase(ByVal oBEPlantilla As BEPlantilla) As BEPlantilla
    <OperationContract()>
    Function ConsultarPlantillaParametroPorIdPlantilla(ByVal idPlantilla As Int32) As List(Of BEPlantillaParametro)
    <OperationContract()>
    Function ConsultarParametrosFuentes() As List(Of BEPlantillaParametro)
    <OperationContract()>
    Function RegistrarPlantillaParametros(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
    <OperationContract()>
    Function ActualizarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
    <OperationContract()>
    Function EliminarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
    <OperationContract()>
    Function ActualizarPlantilla(ByVal oBEPlantilla As BEPlantilla) As Integer

    '************************************************************************************************** 
    ' M�todo          : ConsultarPLantillasXServicioGenPago
    ' Descripci�n     : Consulta las plantillas de los correos correspondientes a un servicio para la pasarela de Pagos
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 29/05/2012
    ' Parametros_In   : idServicio , idMoneda
    ' Parametros_Out  : Cadena HTML descripci�n de bancos
    ''**************************************************************************************************
    <OperationContract()>
    Function ConsultarPLantillasXServicioGenPago(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal codPlantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String
    '************************************************************************************************** 
    ' M�todo          : GenerarPlantillaToHtmlByTipo
    ' Descripci�n     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados apartir del IdServicio y Codigo de tipo de PLantilla
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 30/05/2012
    ' Parametros_In   : IdServicio, codTipoPLantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    <OperationContract()>
    Function GenerarPlantillaToHtmlByTipo(ByVal idServicio As Integer, ByVal codPLantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String
    <OperationContract()>
    Function ConsultarPlantillasXServicioQueEsPE(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String


    <OperationContract()>
    Function ObtenerSeccionesPlantillaPorServicio(ByVal idServicio As Integer, ByVal idPlantillaFormato As Integer) As List(Of BEPlantillaSeccion)

    <OperationContract()>
    Function ActualizarServicioSeccion(ByVal lstPlantillaSeccion As List(Of BEPlantillaSeccion), ByVal idUsuario As Integer) As Integer

    <OperationContract()>
    Function ConsultarPlantillaYParametrosPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla

End Interface


