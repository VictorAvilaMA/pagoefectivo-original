Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IPuntoVenta
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase

    'FullCarga
	<OperationContract()>
    Function ConsultarFC(ByVal request As BEFCConsultarRequest) As BEFCResponse(Of BEFCConsultarResponse)
	<OperationContract()>
    Function CancelarFC(ByVal request As BEFCCancelarRequest) As BEFCResponse(Of BEFCCancelarResponse)
    <OperationContract()>
    Function AnularFC(ByVal request As BEFCAnularRequest) As BEFCResponse(Of BEFCAnularResponse)

    'LIFE!
    <OperationContract()>
    Function ConsultarLF(ByVal request As BELFConsultarRequest) As BELFResponse(Of BELFConsultarResponse)
    <OperationContract()>
    Function CancelarLF(ByVal request As BELFCancelarRequest) As BELFResponse(Of BELFCancelarResponse)

    'M Momo
    <OperationContract()>
    Function ConsultarMoMo(ByVal request As BEMoMoConsultarRequest) As BEMoMoResponse(Of BEMoMoConsultarResponse)
    <OperationContract()>
    Function CancelarMoMo(ByVal request As BEMoMoCancelarRequest) As BEMoMoResponse(Of BEMoMoCancelarResponse)
    <OperationContract()>
    Function AnularMoMo(ByVal request As BEMoMoAnularRequest) As BEMoMoResponse(Of BEMoMoAnularResponse)

End Interface
