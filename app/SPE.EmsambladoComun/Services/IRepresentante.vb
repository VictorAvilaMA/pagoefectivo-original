Imports System.ServiceModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IRepresentante
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase

    <OperationContract()>
    Function ObtenerEmpresaporIDRepresentante(ByVal objRepresentante As BERepresentante) As BERepresentante
    <OperationContract()>
    Function ConsultarEmpresasPorIdUsuario(ByVal be As BEUsuarioBase) As List(Of BEEmpresaContratante)
End Interface
