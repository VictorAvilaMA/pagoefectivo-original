Imports System.ServiceModel
<ServiceContract()>
Public Interface IServicioComun
    Inherits _3Dev.FW.EmsambladoComun.IMaintenanceBase
	<OperationContract()>
    Function ConsultarOPsConciliadas() As System.Data.DataSet
	<OperationContract()>
    Function ActualizarOPAMigradoRecaudacion(ByVal pidOrdenes As Int64()) As Integer

End Interface
