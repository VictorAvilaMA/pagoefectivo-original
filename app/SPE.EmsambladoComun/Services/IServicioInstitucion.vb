﻿Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IServicioInstitucion
    <OperationContract()>
    Function ProcesarArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As BEServicioInstitucion
    <OperationContract()>
    Function ValidarCarga(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function MostrarResumenArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarDetallePagoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarInstitucionCIP(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
    <OperationContract()>
    Function ConsultarArchivosDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
    <OperationContract()>
    Function ConsultarDetalleArchivoDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)

End Interface
