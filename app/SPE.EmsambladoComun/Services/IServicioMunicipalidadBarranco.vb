﻿Imports System.ServiceModel
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades

<ServiceContract()>
Public Interface IServicioMunicipalidadBarranco

    <OperationContract()>
    Function ProcesarArchivoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As BEServicioMunicipalidadBarranco
    <OperationContract()>
    Function ValidarCargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function MostrarResumenArchivoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarDetallePagoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarMunicipalidadBarrancoCIP(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ActualizarDocumentosCIPMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function ActualizarDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
    <OperationContract()>
    Function ConsultarArchivosDescargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
    <OperationContract()>
    Function ConsultarDetalleArchivoDescargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)

End Interface
