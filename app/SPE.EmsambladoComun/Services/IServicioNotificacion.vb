Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IServicioNotificacion


    <OperationContract()>
    Function ConsultarNotificacionesListasANotificar() As BEConsultServNotifResponse
    <OperationContract()>
    Function ConsultarNotificacionesPendientes() As List(Of BEServicioNotificacion)
    <OperationContract()>
    Function ConsultarServicioNotificacionPorIdEstado(ByVal idEstado As Integer) As List(Of BEServicioNotificacion)
    <OperationContract()>
    Function ActualizarEstadoNotificaciones(ByVal listaNotificacionesCorrectas As List(Of BEServicioNotificacion), ByVal listaNotificacionesErroneas As List(Of BEServicioNotificacion)) As Integer
#Region "Saga" 'SagaInicio


    <OperationContract()>
    Function ConsultarNotificacionesListasANotificarSaga() As BEConsultServNotifResponse
    <OperationContract()>
    Function ConsultarDatosParaServicioSaga(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
    <OperationContract()>
    Function ConsultarOrdenesPagoSaga() As List(Of BEServicioNotificacion)
    <OperationContract()>
    Function RegistrarLogConsumoWSSagaRequest(ByVal obeLogWSSagaRequest As BELogWebServiceSagaRequest) As Integer
    <OperationContract()>
    Function RegistrarLogConsumoWSSagaResponse(ByVal obeLogWSSagaResponse As BELogWebServiceSagaResponse) As Integer
#End Region

#Region "Ripley"
    <OperationContract()>
    Function ConsultarNotificacionesListasANotificarRipley() As BEConsultServNotifResponse
    <OperationContract()>
    Function ConsultarDatosParaServicioRipley(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
    <OperationContract()>
    Function ConsultarOrdenesPagoRipley() As List(Of BEServicioNotificacion)
    <OperationContract()>
    Function RegistrarLogConsumoWSRipleyRequest(ByVal obeLogWSRipleyRequest As BELogWebServiceRipleyRequest) As Integer
    <OperationContract()>
    Function RegistrarLogConsumoWSRipleyResponse(ByVal obeLogWSRipleyResponse As BELogWebServiceRipleyResponse) As Integer

#End Region

#Region "Sodimac"
    <OperationContract()>
    Function ConsultarServicioNotificacionReenvioSodimac() As List(Of BEServicioNotificacion)
    <OperationContract()>
    Function ActualizarEstadoNotificacionReenvioSodimac(ByVal obeBEServicioNotificacion As BEServicioNotificacion) As Integer
    <OperationContract()>
    Function ConsultarNotificacionesListasANotificarSodimac() As BEConsultServNotifResponse
    <OperationContract()>
    Function ConsultarDatosParaServicioSodimac(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
    <OperationContract()>
    Function ConsultarOrdenesPagoSodimac() As List(Of BEServicioNotificacion)
    <OperationContract()>
    Function RegistrarLogConsumoWSSodimacRequest(ByVal obeLogWSSodimacRequest As BELogWebServiceSodimacRequest) As Integer
    <OperationContract()>
    Function RegistrarLogConsumoWSSodimacResponse(ByVal obeLogWSSodimacResponse As BELogWebServiceSodimacResponse) As Integer

#End Region

    


End Interface
