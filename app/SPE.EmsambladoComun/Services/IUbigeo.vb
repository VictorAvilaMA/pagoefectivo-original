Imports System.ServiceModel
Imports System
Imports SPE.Entidades

<ServiceContract()>
Public Interface IUbigeo

	<OperationContract()>
    Function ConsultarPais() As List(Of BEPais)
	<OperationContract()>
    Function ConsultarDepartamentoPorIdPais(ByVal IdPais As Integer) As List(Of BEDepartamento)
	<OperationContract()>
    Function ConsultarCiudadPorIdDepartamento(ByVal IdDepartamento As Integer) As List(Of BECiudad)

End Interface
