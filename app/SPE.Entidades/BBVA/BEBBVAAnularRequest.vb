Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEBBVAAnularRequest

    Public Sub New()

    End Sub

    Private _codigoOperacion As String
	<DataMember()> _
    Public Property codigoOperacion() As String
        Get
            Return _codigoOperacion
        End Get
        Set(ByVal value As String)
            _codigoOperacion = value
        End Set
    End Property

    Private _numeroOperacion As String
	<DataMember()> _
    Public Property numeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property


    Private _codigoBanco As String
	<DataMember()> _
    Public Property codigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property

    Private _codigoConvenio As String
	<DataMember()> _
    Public Property codigoConvenio() As String
        Get
            Return _codigoConvenio
        End Get
        Set(ByVal value As String)
            _codigoConvenio = value
        End Set
    End Property

    Private _cuentaRecaudadora As String
	<DataMember()> _
    Public Property cuentaRecaudadora() As String
        Get
            Return _cuentaRecaudadora
        End Get
        Set(ByVal value As String)
            _cuentaRecaudadora = value
        End Set
    End Property

    Private _tipoCliente As String
	<DataMember()> _
    Public Property tipoCliente() As String
        Get
            Return _tipoCliente
        End Get
        Set(ByVal value As String)
            _tipoCliente = value
        End Set
    End Property

    Private _codigoCliente As String
	<DataMember()> _
    Public Property codigoCliente() As String
        Get
            Return _codigoCliente
        End Get
        Set(ByVal value As String)
            _codigoCliente = value
        End Set
    End Property

    Private _numeroReferenciaDeuda As String
	<DataMember()> _
    Public Property numeroReferenciaDeuda() As String
        Get
            Return _numeroReferenciaDeuda
        End Get
        Set(ByVal value As String)
            _numeroReferenciaDeuda = value
        End Set
    End Property

    Private _cantidadDocumentos As String
	<DataMember()> _
    Public Property cantidadDocumentos() As String
        Get
            Return _cantidadDocumentos
        End Get
        Set(ByVal value As String)
            _cantidadDocumentos = value
        End Set
    End Property

    Private _formaPago As String
	<DataMember()> _
    Public Property formaPago() As String
        Get
            Return _formaPago
        End Get
        Set(ByVal value As String)
            _formaPago = value
        End Set
    End Property

    Private _codigoMoneda As String
	<DataMember()> _
    Public Property codigoMoneda() As String
        Get
            Return _codigoMoneda
        End Get
        Set(ByVal value As String)
            _codigoMoneda = value
        End Set
    End Property

    Private _importeTotalPagado As String
	<DataMember()> _
    Public Property importeTotalPagado() As String
        Get
            Return _importeTotalPagado
        End Get
        Set(ByVal value As String)
            _importeTotalPagado = value
        End Set
    End Property

    Private _numeroOperacionOriginal As String
	<DataMember()> _
    Public Property numeroOperacionOriginal() As String
        Get
            Return _numeroOperacionOriginal
        End Get
        Set(ByVal value As String)
            _numeroOperacionOriginal = value
        End Set
    End Property

    Private _fechaOperacionOriginal As String
	<DataMember()> _
    Public Property fechaOperacionOriginal() As String
        Get
            Return _fechaOperacionOriginal
        End Get
        Set(ByVal value As String)
            _fechaOperacionOriginal = value
        End Set
    End Property

    Private _referenciaDeudaAdicional As String
	<DataMember()> _
    Public Property referenciaDeudaAdicional() As String
        Get
            Return _referenciaDeudaAdicional
        End Get
        Set(ByVal value As String)
            _referenciaDeudaAdicional = value
        End Set
    End Property

    Private _detalle As List(Of BEBBVAAnularDetalle)
	<DataMember()> _
    Public Property detalle() As List(Of BEBBVAAnularDetalle)
        Get
            Return _detalle
        End Get
        Set(ByVal value As List(Of BEBBVAAnularDetalle))
            _detalle = value
        End Set
    End Property


    Private _canalOperacion As String
	<DataMember()> _
    Public Property canalOperacion() As String
        Get
            Return _canalOperacion
        End Get
        Set(ByVal value As String)
            _canalOperacion = value
        End Set
    End Property

    Private _codigoOficina As String
	<DataMember()> _
    Public Property codigoOficina() As String
        Get
            Return _codigoOficina
        End Get
        Set(ByVal value As String)
            _codigoOficina = value
        End Set
    End Property

    Private _fechaOperacion As String
	<DataMember()> _
    Public Property fechaOperacion() As String
        Get
            Return _fechaOperacion
        End Get
        Set(ByVal value As String)
            _fechaOperacion = value
        End Set
    End Property

    Private _horaOperacion As String
	<DataMember()> _
    Public Property horaOperacion() As String
        Get
            Return _horaOperacion
        End Get
        Set(ByVal value As String)
            _horaOperacion = value
        End Set
    End Property

    Private _datosEmpresa As String
	<DataMember()> _
    Public Property datosEmpresa() As String
        Get
            Return _datosEmpresa
        End Get
        Set(ByVal value As String)
            _datosEmpresa = value
        End Set
    End Property


End Class
