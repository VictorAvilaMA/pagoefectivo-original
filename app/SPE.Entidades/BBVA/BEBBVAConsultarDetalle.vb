Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBBVAConsultarDetalle

    Public Sub New()

    End Sub

    Private _numeroReferenciaDocumento As String
	<DataMember()> _
    Public Property numeroReferenciaDocumento() As String
        Get
            Return _numeroReferenciaDocumento
        End Get
        Set(ByVal value As String)
            _numeroReferenciaDocumento = value
        End Set
    End Property

    Private _importeDeudaDocumento As String
	<DataMember()> _
    Public Property importeDeudaDocumento() As String
        Get
            Return _importeDeudaDocumento
        End Get
        Set(ByVal value As String)
            _importeDeudaDocumento = value
        End Set
    End Property

    Private _importeDeudaMinimaDocumento As String
	<DataMember()> _
    Public Property importeDeudaMinimaDocumento() As String
        Get
            Return _importeDeudaMinimaDocumento
        End Get
        Set(ByVal value As String)
            _importeDeudaMinimaDocumento = value
        End Set
    End Property

    Private _fechaVencimientoDocumento As String
	<DataMember()> _
    Public Property fechaVencimientoDocumento() As String
        Get
            Return _fechaVencimientoDocumento
        End Get
        Set(ByVal value As String)
            _fechaVencimientoDocumento = value
        End Set
    End Property

    Private _fechaEmisionDocumento As String
	<DataMember()> _
    Public Property fechaEmisionDocumento() As String
        Get
            Return _fechaEmisionDocumento
        End Get
        Set(ByVal value As String)
            _fechaEmisionDocumento = value
        End Set
    End Property

    Private _descripcionDocumento As String
	<DataMember()> _
    Public Property descripcionDocumento() As String
        Get
            Return _descripcionDocumento
        End Get
        Set(ByVal value As String)
            _descripcionDocumento = value
        End Set
    End Property

    Private _numeroDocumento As String
	<DataMember()> _
    Public Property numeroDocumento() As String
        Get
            Return _numeroDocumento
        End Get
        Set(ByVal value As String)
            _numeroDocumento = value
        End Set
    End Property

    Private _indicadorRestriccPago As String
	<DataMember()> _
    Public Property indicadorRestriccPago() As String
        Get
            Return _indicadorRestriccPago
        End Get
        Set(ByVal value As String)
            _indicadorRestriccPago = value
        End Set
    End Property

    Private _indicadorSituacionDocumento As String
	<DataMember()> _
    Public Property indicadorSituacionDocumento() As String
        Get
            Return _indicadorSituacionDocumento
        End Get
        Set(ByVal value As String)
            _indicadorSituacionDocumento = value
        End Set
    End Property

    Private _cantidadSubconceptos As String
	<DataMember()> _
    Public Property cantidadSubconceptos() As String
        Get
            Return _cantidadSubconceptos
        End Get
        Set(ByVal value As String)
            _cantidadSubconceptos = value
        End Set
    End Property

    Private _subConceptos As List(Of BEBBVAConsultarSubConcepto)
	<DataMember()> _
    Public Property subConceptos() As List(Of BEBBVAConsultarSubConcepto)
        Get
            Return _subConceptos
        End Get
        Set(ByVal value As List(Of BEBBVAConsultarSubConcepto))
            _subConceptos = value
        End Set
    End Property


End Class
