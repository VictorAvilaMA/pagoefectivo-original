Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBBVAConsultarRequest

    Public Sub New()

    End Sub

    Private _codigoOperacion As String
	<DataMember()> _
    Public Property codigoOperacion() As String
        Get
            Return _codigoOperacion
        End Get
        Set(ByVal value As String)
            _codigoOperacion = value
        End Set
    End Property

    Private _numeroOperacion As String
	<DataMember()> _
    Public Property numeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property

    Private _codigoBanco As String
	<DataMember()> _
    Public Property codigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property

    Private _codigoConvenio As String
	<DataMember()> _
    Public Property codigoConvenio() As String
        Get
            Return _codigoConvenio
        End Get
        Set(ByVal value As String)
            _codigoConvenio = value
        End Set
    End Property

    Private _tipocliente As String
	<DataMember()> _
    Public Property tipocliente() As String
        Get
            Return _tipocliente
        End Get
        Set(ByVal value As String)
            _tipocliente = value
        End Set
    End Property

    Private _codigocliente As String
	<DataMember()> _
    Public Property codigocliente() As String
        Get
            Return _codigocliente
        End Get
        Set(ByVal value As String)
            _codigocliente = value
        End Set
    End Property

    Private _numeroReferenciaDeuda As String
	<DataMember()> _
    Public Property numeroReferenciaDeuda() As String
        Get
            Return _numeroReferenciaDeuda
        End Get
        Set(ByVal value As String)
            _numeroReferenciaDeuda = value
        End Set
    End Property

    Private _referenciaDeudaAdicional As String
	<DataMember()> _
    Public Property referenciaDeudaAdicional() As String
        Get
            Return _referenciaDeudaAdicional
        End Get
        Set(ByVal value As String)
            _referenciaDeudaAdicional = value
        End Set
    End Property

    Private _canalOperacion As String
	<DataMember()> _
    Public Property canalOperacion() As String
        Get
            Return _canalOperacion
        End Get
        Set(ByVal value As String)
            _canalOperacion = value
        End Set
    End Property

    Private _codigoOficina As String
	<DataMember()> _
    Public Property codigoOficina() As String
        Get
            Return _codigoOficina
        End Get
        Set(ByVal value As String)
            _codigoOficina = value
        End Set
    End Property

    Private _fechaOperacion As String
	<DataMember()> _
    Public Property fechaOperacion() As String
        Get
            Return _fechaOperacion
        End Get
        Set(ByVal value As String)
            _fechaOperacion = value
        End Set
    End Property

    Private _horaOperacion As String
	<DataMember()> _
    Public Property horaOperacion() As String
        Get
            Return _horaOperacion
        End Get
        Set(ByVal value As String)
            _horaOperacion = value
        End Set
    End Property

    Private _datosEmpresa As String
	<DataMember()> _
    Public Property datosEmpresa() As String
        Get
            Return _datosEmpresa
        End Get
        Set(ByVal value As String)
            _datosEmpresa = value
        End Set
    End Property

End Class
