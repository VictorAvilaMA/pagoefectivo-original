Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBBVAConsultarSubConcepto

    Public Sub New()

    End Sub

    Private _codigoSubconcepto As String
	<DataMember()> _
    Public Property codigoSubconcepto() As String
        Get
            Return _codigoSubconcepto
        End Get
        Set(ByVal value As String)
            _codigoSubconcepto = value
        End Set
    End Property

    Private _descripcionSubconcepto As String
	<DataMember()> _
    Public Property descripcionSubconcepto() As String
        Get
            Return _descripcionSubconcepto
        End Get
        Set(ByVal value As String)
            _descripcionSubconcepto = value
        End Set
    End Property

    Private _importeSubconcepto As String
	<DataMember()> _
    Public Property importeSubconcepto() As String
        Get
            Return _importeSubconcepto
        End Get
        Set(ByVal value As String)
            _importeSubconcepto = value
        End Set
    End Property


End Class
