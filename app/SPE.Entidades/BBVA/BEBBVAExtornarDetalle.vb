Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBBVAExtornarDetalle

    Public Sub New()

    End Sub

    Private _numeroReferenciaDocumento As String
	<DataMember()> _
    Public Property numeroReferenciaDocumento() As String
        Get
            Return _numeroReferenciaDocumento
        End Get
        Set(ByVal value As String)
            _numeroReferenciaDocumento = value
        End Set
    End Property

    Private _descripcionDocumento As String
	<DataMember()> _
    Public Property descripcionDocumento() As String
        Get
            Return _descripcionDocumento
        End Get
        Set(ByVal value As String)
            _descripcionDocumento = value
        End Set
    End Property

    Private _numeroOperacionBanco As String
	<DataMember()> _
    Public Property numeroOperacionBanco() As String
        Get
            Return _numeroOperacionBanco
        End Get
        Set(ByVal value As String)
            _numeroOperacionBanco = value
        End Set
    End Property

    Private _importeDeudaPagada As String
	<DataMember()> _
    Public Property importeDeudaPagada() As String
        Get
            Return _importeDeudaPagada
        End Get
        Set(ByVal value As String)
            _importeDeudaPagada = value
        End Set
    End Property

    Private _referenciaPagoAdicional As String
	<DataMember()> _
    Public Property referenciaPagoAdicional() As String
        Get
            Return _referenciaPagoAdicional
        End Get
        Set(ByVal value As String)
            _referenciaPagoAdicional = value
        End Set
    End Property


End Class
