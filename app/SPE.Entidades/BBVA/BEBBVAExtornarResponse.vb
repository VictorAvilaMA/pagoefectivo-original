Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEBBVAExtornarResponse

    Public Sub New()

    End Sub


    Private _codigoOperacion As String
	<DataMember()> _
    Public Property codigoOperacion() As String
        Get
            Return _codigoOperacion
        End Get
        Set(ByVal value As String)
            _codigoOperacion = value
        End Set
    End Property

    Private _numeroOperacion As String
	<DataMember()> _
    Public Property numeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property

    Private _codigoBanco As String
	<DataMember()> _
    Public Property codigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property

    Private _codigoConvenio As String
	<DataMember()> _
    Public Property codigoConvenio() As String
        Get
            Return _codigoConvenio
        End Get
        Set(ByVal value As String)
            _codigoConvenio = value
        End Set
    End Property

    Private _tipoCliente As String
	<DataMember()> _
    Public Property tipoCliente() As String
        Get
            Return _tipoCliente
        End Get
        Set(ByVal value As String)
            _tipoCliente = value
        End Set
    End Property

    Private _codigoCliente As String
	<DataMember()> _
    Public Property codigoCliente() As String
        Get
            Return _codigoCliente
        End Get
        Set(ByVal value As String)
            _codigoCliente = value
        End Set
    End Property

    Private _numeroReferenciaDeuda As String
	<DataMember()> _
    Public Property numeroReferenciaDeuda() As String
        Get
            Return _numeroReferenciaDeuda
        End Get
        Set(ByVal value As String)
            _numeroReferenciaDeuda = value
        End Set
    End Property

    Private _numeroOperacionEmpresa As String
	<DataMember()> _
    Public Property numeroOperacionEmpresa() As String
        Get
            Return _numeroOperacionEmpresa
        End Get
        Set(ByVal value As String)
            _numeroOperacionEmpresa = value
        End Set
    End Property

    Private _referenciaDeudaAdicional As String
	<DataMember()> _
    Public Property referenciaDeudaAdicional() As String
        Get
            Return _referenciaDeudaAdicional
        End Get
        Set(ByVal value As String)
            _referenciaDeudaAdicional = value
        End Set
    End Property

    Private _codigoResultado As String
	<DataMember()> _
    Public Property codigoResultado() As String
        Get
            Return _codigoResultado
        End Get
        Set(ByVal value As String)
            _codigoResultado = value
        End Set
    End Property

    Private _mensajeResultado As String
	<DataMember()> _
    Public Property mensajeResultado() As String
        Get
            Return _mensajeResultado
        End Get
        Set(ByVal value As String)
            _mensajeResultado = value
        End Set
    End Property

    Private _datosEmpresa As String
	<DataMember()> _
    Public Property datosEmpresa() As String
        Get
            Return _datosEmpresa
        End Get
        Set(ByVal value As String)
            _datosEmpresa = value
        End Set
    End Property


End Class
