Imports System.Runtime.Serialization
Imports System
Imports System.Collections
Imports System.Collections.Generic

<Serializable()> <DataContract()> _
Public Class BEBBVAResponse(Of T)
    Public Sub New()

    End Sub

    Private _objBBVA As T
	<DataMember()> _
    Public Property ObjBBVA() As T
        Get
            Return _objBBVA
        End Get
        Set(ByVal value As T)
            _objBBVA = value
        End Set
    End Property


    Private _mensaje As String
	<DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    'valores
    '(-1).- error de excepcion
    '( 0).- VALIDACION LOGIC
    '( 1).- funcionalidad correcta

    Private _estado As Integer
	<DataMember()> _
    Public Property Estado() As Integer
        Get
            Return _estado
        End Get
        Set(ByVal value As Integer)
            _estado = value
        End Set
    End Property

End Class
