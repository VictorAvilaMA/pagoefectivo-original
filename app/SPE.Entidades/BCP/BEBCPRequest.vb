﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports MongoDB.Bson
Imports MongoDB.Bson.Serialization.Attributes

Public Class BEBCPRequest
    'ASD
    <DataMember()> _
    Public Property id() As ObjectId

    <DataMember()> _
    Public Property Codigo As String

    <DataMember()> _
    Public Property CodServicio As String

    <DataMember()> _
    Public Property Moneda As String

    <DataMember()> _
    Public Property NroOperacion As String

    <DataMember()> _
    Public Property NroOperacionAnulada As String

    <DataMember()> _
    Public Property Agencia As String

    <DataMember()> _
    Public Property NroDocs As String

    <DataMember()> _
    Public Property NDoc1 As String

    <DataMember()> _
    Public Property Mdoc1 As String

    <DataMember()> _
    Public Property NDoc2 As String

    <DataMember()> _
    Public Property Mdoc2 As String

    <DataMember()> _
    Public Property NDoc3 As String

    <DataMember()> _
    Public Property Mdoc3 As String

    <DataMember()> _
    Public Property NDoc4 As String

    <DataMember()> _
    Public Property Mdoc4 As String

    <DataMember()> _
    Public Property NDoc5 As String

    <DataMember()> _
    Public Property Mdoc5 As String

    <DataMember()> _
    Public Property NDoc6 As String

    <DataMember()> _
    Public Property Mdoc6 As String

    <DataMember()> _
    Public Property NDoc7 As String

    <DataMember()> _
    Public Property Mdoc7 As String

    <DataMember()> _
    Public Property NDoc8 As String

    <DataMember()> _
    Public Property Mdoc8 As String

    <DataMember()> _
    Public Property NDoc9 As String

    <DataMember()> _
    Public Property Mdoc9 As String

    <DataMember()> _
    Public Property NDoc10 As String

    <DataMember()> _
    Public Property Mdoc10 As String

    <DataMember()> _
    Public Property Monto As String

    <DataMember()> _
    Public Property CodOperacion As String

    <DataMember()> _
    Public Property Ip As String

    <DataMember()> _
    Public Property CadenaConexionPagoEfectivoSaldos() As String

    <DataMember()> _
    Public Property DNIInvalido1() As String

    <DataMember()> _
    Public Property DNIInvalido2() As String

End Class
