Imports System.Runtime.Serialization
Imports System
Imports System.Collections
Imports System.Collections.Generic

<Serializable()>
<DataContract()> _
Public Class BEBCPResponse

    <DataMember()> _
    Public Property IdRequest As String

    <DataMember()> _
    Public Property Codigo As String

    <DataMember()> _
    Public Property Descripcion1 As String

    <DataMember()> _
    Public Property Descripcion2 As String

    <DataMember()> _
    Public Property Estado As Integer

    <DataMember()> _
    Public Property DescEstado As String

    <DataMember()> _
    Public Property Respuesta() As String()

    <DataMember()> _
    Public Property CIP As String

End Class