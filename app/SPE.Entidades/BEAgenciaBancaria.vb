Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()>
Public Class BEAgenciaBancaria
    Inherits BEAuditoria

    Private _idAgenciaBancaria As Integer
    Private _idBanco As Integer
    Private _codigo As String
    Private _descripcion As String
    Private _direccion As String
    Private _idEstado As Integer
    Private _idCiudad As Integer
    Private _idDepartamento As Integer
    Private _idPais As Integer
    Private _descripcionEstado As String


    Private _orderBy As String
    Private _isAccending As Boolean

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal opcion As String)

        IdAgenciaBancaria = DataUtil.ObjectToInt32(reader("IdAgenciaBancaria"))
        IdBanco = DataUtil.ObjectToInt32(reader("IdBanco"))
        Codigo = reader("Codigo").ToString()
        Descripcion = reader("Descripcion").ToString()
        Direccion = reader("Direccion").ToString()
        IdEstado = DataUtil.ObjectToInt32(reader("IdEstado"))
        IdCiudad = DataUtil.ObjectToInt32(reader("IdCiudad"))

        If opcion = "busqueda" Then
            IdDepartamento = DataUtil.ObjectToInt32(reader("IdDepartamento"))
            IdPais = DataUtil.ObjectToInt32(reader("IdPais"))
            DescripcionEstado = reader("DescripcionEstado").ToString()
        End If


    End Sub


	<DataMember()> _
    Public Property IdAgenciaBancaria() As Integer
        Get
            Return _idAgenciaBancaria
        End Get
        Set(ByVal value As Integer)
            _idAgenciaBancaria = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property

	<DataMember()> _
    Public Property Codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

	<DataMember()> _
    Public Property Direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdCiudad() As Integer
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Integer)
            _idCiudad = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdDepartamento() As Integer
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Integer)
            _idDepartamento = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdPais() As Integer
        Get
            Return _idPais
        End Get
        Set(ByVal value As Integer)
            _idPais = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _descripcionEstado
        End Get
        Set(ByVal value As String)
            _descripcionEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrderBy() As String
        Get
            Return _orderBy
        End Get
        Set(ByVal value As String)
            _orderBy = value
        End Set
    End Property

	<DataMember()> _
    Public Property IsAccending() As Boolean
        Get
            Return _isAccending
        End Get
        Set(ByVal value As Boolean)
            _isAccending = value
        End Set
    End Property



    Public Class DescAgenciaBancariaBEACComparer
        Implements IComparer(Of BEAgenciaBancaria)
        Public Function Compare(ByVal x As BEAgenciaBancaria, ByVal y As BEAgenciaBancaria) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaBancaria).Compare
            Return x.Descripcion.CompareTo(y.Descripcion)
        End Function
    End Class

    Public Class CodigoAgenciaBancariaBEACComparer
        Implements IComparer(Of BEAgenciaBancaria)
        Public Function Compare(ByVal x As BEAgenciaBancaria, ByVal y As BEAgenciaBancaria) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaBancaria).Compare
            Return x.Codigo.CompareTo(y.Codigo)
        End Function
    End Class

    Public Class DireccionAgenciaBancariaBEACComparer
        Implements IComparer(Of BEAgenciaBancaria)
        Public Function Compare(ByVal x As BEAgenciaBancaria, ByVal y As BEAgenciaBancaria) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaBancaria).Compare
            Return x.Direccion.CompareTo(y.Direccion)
        End Function
    End Class

    Public Class EstadoAgenciaBancariaBEACComparer
        Implements IComparer(Of BEAgenciaBancaria)
        Public Function Compare(ByVal x As BEAgenciaBancaria, ByVal y As BEAgenciaBancaria) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaBancaria).Compare
            Return x.DescripcionEstado.CompareTo(y.DescripcionEstado)
        End Function
    End Class
   
End Class
