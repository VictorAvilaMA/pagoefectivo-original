Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEAgenciaRecaudadora

    Private _contacto As String
    Private _direccion As String
    Private _ubigeo As String
    Private _email As String
    Private _fax As String
    Private _ruc As String
    Private _fechaActualizacion As DateTime
    Private _fechaCreacion As DateTime
    Private _idEstado As Integer
    Private _idCiudad As Integer
    Private _idDepartamento As Integer
    Private _idPais As Integer
    Private _idIdUsuarioCreacion As Integer
    Private _idUsuarioActualizacion As Integer
    Private _nombreComercial As String
    Private _telefono As String
    Private _idAgenciaRecaudadora As Integer
    Private _idTipoAgenciaRecaudadora As Integer
    Private _DescripcionEstado As String

    Private _orderBy As String
    Private _isAccending As Boolean

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)

        ActCampos(reader)
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Consulta As String)

        Me.NombreComercial = reader("NombreComercial").ToString()
        Me.Ruc = reader("Ruc").ToString
        Me.Direccion = reader("Direccion").ToString()
        Me.Email = reader("Email").ToString()
        Me.Ubigeo = reader("Ubigeo").ToString
        Me.Telefono = reader("Telefono").ToString()

    End Sub
    Private Sub ActCampos(ByVal reader As IDataReader)
        Me.Contacto = ObjectToString(reader("Contacto"))
        Me.Direccion = ObjectToString(reader("Direccion"))
        Me.Email = ObjectToString(reader("Email"))
        Me.Fax = ObjectToString(reader("Fax"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        Me.IdEstado = ObjectToInt32(reader("IdEstado"))
        Me.IdCiudad = ObjectToInt32(reader("IdCiudad"))
        Me.IdDepartamento = ObjectToInt32(reader("IdDepartamento"))
        Me.IdPais = ObjectToInt32(reader("IdPais"))
        Me.IdUsuarioCreacion = ObjectToInt32(reader("IdUsuarioCreacion"))
        Me.IdUsuarioActualizacion = ObjectToInt32(reader("IdUsuarioActualizacion"))
        Me.NombreComercial = ObjectToString(reader("NombreComercial"))
        Me.Ruc = ObjectToString(reader("Ruc"))
        Me.Telefono = ObjectToString(reader("Telefono"))
        Me.IdAgenciaRecaudadora = ObjectToInt32(reader("IdAgenciaRecaudadora"))
        Me.IdTipoAgenciaRecaudadora = ObjectToInt32(reader("IdTipoAgenciaRecaudadora"))

    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String, ByVal tipo2 As String)
        If (tipo = "busq") Then
            ActCampos(reader)
            _DescripcionEstado = reader("DescripcionEstado")
        End If
    End Sub

	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _DescripcionEstado
        End Get
        Set(ByVal value As String)
            _DescripcionEstado = value
        End Set
    End Property


	<DataMember()> _
    Public Property NombreComercial() As String
        Get
            Return _nombreComercial
        End Get
        Set(ByVal value As String)
            _nombreComercial = value
        End Set
    End Property

	<DataMember()> _
    Public Property Contacto() As String
        Get

            Return _contacto
        End Get
        Set(ByVal value As String)
            _contacto = value
        End Set
    End Property

	<DataMember()> _
    Public Property Telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

	<DataMember()> _
    Public Property Fax() As String
        Get
            Return _fax
        End Get
        Set(ByVal value As String)
            _fax = value
        End Set
    End Property

	<DataMember()> _
    Public Property Ruc() As String
        Get
            Return _ruc
        End Get
        Set(ByVal value As String)
            _ruc = value
        End Set
    End Property

	<DataMember()> _
    Public Property Direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

	<DataMember()> _
    Public Property Ubigeo() As String
        Get
            Return _ubigeo
        End Get
        Set(ByVal value As String)
            _ubigeo = value
        End Set
    End Property

	<DataMember()> _
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaActualizacion() As DateTime
        Get
            Return _fechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _fechaActualizacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdUsuarioCreacion() As Integer
        Get
            Return _idIdUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _idIdUsuarioCreacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdUsuarioActualizacion() As Integer
        Get
            Return _idUsuarioActualizacion
        End Get
        Set(ByVal value As Integer)
            _idUsuarioActualizacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdAgenciaRecaudadora() As Integer
        Get
            Return _idAgenciaRecaudadora
        End Get
        Set(ByVal value As Int32)
            _idAgenciaRecaudadora = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdTipoAgenciaRecaudadora() As Integer
        Get
            Return _idTipoAgenciaRecaudadora
        End Get
        Set(ByVal value As Integer)
            _idTipoAgenciaRecaudadora = value
        End Set
    End Property


	<DataMember()> _
    Public Property IdCiudad() As Integer
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Integer)
            _idCiudad = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdDepartamento() As Integer
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Integer)
            _idDepartamento = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdPais() As Integer
        Get
            Return _idPais
        End Get
        Set(ByVal value As Integer)
            _idPais = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrderBy() As String
        Get
            Return _orderBy
        End Get
        Set(ByVal value As String)
            _orderBy = value
        End Set
    End Property

	<DataMember()> _
    Public Property IsAccending() As Boolean
        Get
            Return _isAccending
        End Get
        Set(ByVal value As Boolean)
            _isAccending = value
        End Set
    End Property


    Public Class NombreComercialBEACComparer
        Implements IComparer(Of BEAgenciaRecaudadora)
        Public Function Compare(ByVal x As BEAgenciaRecaudadora, ByVal y As BEAgenciaRecaudadora) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaRecaudadora).Compare
            Return x.NombreComercial.CompareTo(y.NombreComercial)
        End Function
    End Class

    Public Class ContactoBEACComparer
        Implements IComparer(Of BEAgenciaRecaudadora)
        Public Function Compare(ByVal x As BEAgenciaRecaudadora, ByVal y As BEAgenciaRecaudadora) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaRecaudadora).Compare
            Return x.Contacto.CompareTo(y.Contacto)
        End Function
    End Class

    Public Class TelefonoBEACComparer
        Implements IComparer(Of BEAgenciaRecaudadora)
        Public Function Compare(ByVal x As BEAgenciaRecaudadora, ByVal y As BEAgenciaRecaudadora) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaRecaudadora).Compare
            Return x.Telefono.CompareTo(y.Telefono)
        End Function
    End Class

    Public Class EmailBEACComparer
        Implements IComparer(Of BEAgenciaRecaudadora)
        Public Function Compare(ByVal x As BEAgenciaRecaudadora, ByVal y As BEAgenciaRecaudadora) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaRecaudadora).Compare
            Return x.Email.CompareTo(y.Email)
        End Function
    End Class

    Public Class DescripcionEstadoBEACComparer
        Implements IComparer(Of BEAgenciaRecaudadora)
        Public Function Compare(ByVal x As BEAgenciaRecaudadora, ByVal y As BEAgenciaRecaudadora) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaRecaudadora).Compare
            Return x.DescripcionEstado.CompareTo(y.DescripcionEstado)
        End Function
    End Class








End Class
