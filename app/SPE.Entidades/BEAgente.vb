Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()> Public Class BEAgente
    Inherits BEUsuarioBase
    Private _idAgente As Integer
    Private _idAgenciaRecaudadora As Integer
    Private _idTipoAgente As Integer
    Private _descTipoAgente As String
    Private _descAgenciaRecaudadora As String

    Private _orderBy As String
    Private _isAccending As Boolean

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        '
        MyBase.New(reader)
        '
        Me.IdAgente = Convert.ToInt32(reader("IdAgente").ToString())
        Me.IdTipoAgente = Convert.ToInt32(reader("IdTipoAgente").ToString())
        Me.IdAgenciaRecaudadora = Convert.ToInt32(reader("IdAgenciaRecaudadora").ToString())
        Me.DescTipoAgente = reader("DescTipoAgente").ToString()
        Me.DescAgenciaRecaudadora = reader("DescAgenciaRecaudadora").ToString()
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal opcion As String)
        '
        MyBase.New(reader)
        '
        Me.IdAgente = Convert.ToInt32(reader("IdAgente").ToString())
        Me.IdTipoAgente = Convert.ToInt32(reader("IdTipoAgente").ToString())
        Me.IdAgenciaRecaudadora = Convert.ToInt32(reader("IdAgenciaRecaudadora").ToString())
        Me.DescTipoAgente = reader("DescTipoAgente").ToString()
        Me.DescAgenciaRecaudadora = reader("DescAgenciaRecaudadora").ToString()
    End Sub

	<DataMember()> _
    Public Property IdAgente() As Integer
        Get
            Return _idAgente
        End Get
        Set(ByVal value As Integer)
            _idAgente = value
        End Set
    End Property
    '
	<DataMember()> _
    Public Property IdTipoAgente() As Integer
        Get
            Return _idTipoAgente
        End Get
        Set(ByVal value As Integer)
            _idTipoAgente = value
        End Set
    End Property
    '
	<DataMember()> _
    Public Property IdAgenciaRecaudadora() As Integer
        Get
            Return _idAgenciaRecaudadora
        End Get
        Set(ByVal value As Integer)
            _idAgenciaRecaudadora = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescAgenciaRecaudadora() As String
        Get
            Return _descAgenciaRecaudadora
        End Get
        Set(ByVal value As String)
            _descAgenciaRecaudadora = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescTipoAgente() As String
        Get
            Return _descTipoAgente
        End Get
        Set(ByVal value As String)
            _descTipoAgente = value
        End Set
    End Property

    
	<DataMember()> _
    Public Property OrderBy() As String
        Get
            Return _orderBy
        End Get
        Set(ByVal value As String)
            _orderBy = value
        End Set
    End Property

	<DataMember()> _
    Public Property IsAccending() As Boolean
        Get
            Return _isAccending
        End Get
        Set(ByVal value As Boolean)
            _isAccending = value
        End Set
    End Property

    Public Class DescAgenciaRecaudadoraBEACComparer
        Implements IComparer(Of BEAgente)
        Public Function Compare(ByVal x As BEAgente, ByVal y As BEAgente) As Integer Implements System.Collections.Generic.IComparer(Of BEAgente).Compare
            Return x.DescAgenciaRecaudadora.CompareTo(y.DescAgenciaRecaudadora)
        End Function
    End Class

    Public Class DescTipoAgenteBEACComparer
        Implements IComparer(Of BEAgente)
        Public Function Compare(ByVal x As BEAgente, ByVal y As BEAgente) As Integer Implements System.Collections.Generic.IComparer(Of BEAgente).Compare
            Return x.DescTipoAgente.CompareTo(y.DescTipoAgente)
        End Function
    End Class

    Public Class NombresBEACComparer
        Implements IComparer(Of BEAgente)
        Public Function Compare(ByVal x As BEAgente, ByVal y As BEAgente) As Integer Implements System.Collections.Generic.IComparer(Of BEAgente).Compare
            Return x.Nombres.CompareTo(y.Nombres)
        End Function
    End Class

    Public Class ApellidosBEACComparer
        Implements IComparer(Of BEAgente)
        Public Function Compare(ByVal x As BEAgente, ByVal y As BEAgente) As Integer Implements System.Collections.Generic.IComparer(Of BEAgente).Compare
            Return x.Apellidos.CompareTo(y.Apellidos)
        End Function
    End Class

    Public Class DescripcionEstadoBEACComparer
        Implements IComparer(Of BEAgente)
        Public Function Compare(ByVal x As BEAgente, ByVal y As BEAgente) As Integer Implements System.Collections.Generic.IComparer(Of BEAgente).Compare
            Return x.DescripcionEstado.CompareTo(y.DescripcionEstado)
        End Function
    End Class
    '
End Class

