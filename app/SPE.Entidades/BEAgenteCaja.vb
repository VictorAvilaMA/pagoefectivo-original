Imports System.Runtime.Serialization
Imports _3Dev.FW.Entidades.Seguridad
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEAgenteCaja
    Inherits BEAuditoria

    Private _idEstado As Integer
    Private _fechaAgrupar As Date
    Private _fechaApertura As Date
    Private _fechaCierre As String
    Private _fechaValuta As String
    Private _estadoFechaValuta As Integer
    Private _idAgenteCaja As Integer
    Private _idOrdenPago As Integer
    Private _idCaja As Integer
    Private _idAgente As Integer
    Private _idAgenciaRecaudadora As Integer
    Private _nombreAgencia As String
    Private _nombreAgente As String
    Private _nombreCaja As String
    Private _nombreEstado As String
    Private _nroOPRecibidas As Integer
    Private _nroOPAnuladas As Integer
    Private _liquidar As Boolean
    Private _medioPago As String
    Private _simboloMoneda As String
    Private _moneda As String
    Private _monto As Decimal

    Private _emailSupervisorApertura As String
    Private _emailSupervisorTermino As String

    Private _orderBy As String
    Private _isAccending As Boolean

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdEstado = reader("IdEstado").ToString
        Me.FechaApertura = Convert.ToDateTime(reader("FechaApertura").ToString)
        Me.FechaCierre = reader("FechaCierre").ToString()
        Me.IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja").ToString)
        Me.NombreCaja = reader("NombreCaja").ToString
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Operacion As String)
        If Operacion = "ConsultarCajaAvanzada" Then

            Me.IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja").ToString)
            Me.NombreAgente = reader("Agente").ToString()
            Me.NombreCaja = reader("NombreCaja").ToString()
            Me.FechaApertura = Convert.ToDateTime(reader("FechaApertura").ToString)
            Me.FechaCierre = reader("FechaCierre").ToString()
            Me.NroOPRecibidas = Convert.ToInt32(reader("OPRecibidas").ToString)
            Me.NroOPAnuladas = Convert.ToInt32(reader("OPAnuladas").ToString)
            Me.NombreEstado = reader("DescEstado").ToString

        ElseIf Operacion = "ConsultaCajasLiquidadas" Then

            Me.IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja").ToString)
            Me.NombreCaja = reader("NombreCaja").ToString()
            Me.NombreAgencia = reader("NombreAgencia").ToString
            Me.NombreAgente = reader("Agente").ToString()
            Me.NombreEstado = reader("FechaAgrupar").ToString()
            Me.FechaApertura = Convert.ToDateTime(reader("FechaApertura").ToString)
            Me.FechaCierre = Convert.ToDateTime(reader("FechaCierre").ToString)
            Me.FechaCreacion = Convert.ToDateTime(reader("FechaCierre").ToString)
            Me.MedioPago = reader("MedioPago").ToString()
            Me.Moneda = reader("Moneda").ToString()
            Me.SimboloMoneda = reader("SimboloMoneda").ToString()
            Me.Monto = Convert.ToDecimal(reader("Monto").ToString())
            Me.NroOPRecibidas = Convert.ToInt32(reader("NumeroOrdenPago"))

        ElseIf Operacion = "ConsultaParaAperturarCaja" Then
            Me.IdEstado = reader("IdEstado").ToString
            Me.FechaApertura = Convert.ToDateTime(reader("FechaApertura").ToString)
            Me.FechaCierre = reader("FechaCierre").ToString
            Me.FechaValuta = reader("FechaValuta").ToString()
            Me.EstadoFechaValuta = Convert.ToInt32(reader("EstadoFechaValuta").ToString)
            Me.IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja").ToString)
            Me.IdCaja = Convert.ToInt32(reader("IdCaja").ToString)
            Me.IdAgente = Convert.ToInt32(reader("IdAgente").ToString)
            Me.NombreCaja = reader("NombreCaja").ToString
        Else

            Me.IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja").ToString)
            Me.NombreAgente = reader("Agente").ToString()
            Me.NombreCaja = reader("NombreCaja").ToString()
            Me.FechaApertura = Convert.ToDateTime(reader("FechaApertura").ToString)
            Me.FechaCierre = reader("FechaCierre").ToString()
            Me.NroOPRecibidas = Convert.ToInt32(reader("NroTransacciones").ToString)
            Me.NroOPAnuladas = Convert.ToInt32(reader("Anulados").ToString)
            Me.Liquidar = reader("Liquidar").ToString
        End If

    End Sub

	<DataMember()> _
    Public Property EmailSupervisorApertura() As String
        Get
            Return _emailSupervisorApertura
        End Get

        Set(ByVal value As String)
            _emailSupervisorApertura = value
        End Set
    End Property

	<DataMember()> _
    Public Property EmailSupervisorTermino() As String
        Get
            Return _emailSupervisorTermino
        End Get

        Set(ByVal value As String)
            _emailSupervisorTermino = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idOrdenPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdAgenteCaja() As Integer
        Get
            Return _idAgenteCaja
        End Get
        Set(ByVal value As Integer)
            _idAgenteCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdCaja() As Integer
        Get
            Return _idCaja
        End Get
        Set(ByVal value As Integer)
            _idCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdAgente() As Integer
        Get
            Return _idAgente
        End Get
        Set(ByVal value As Integer)
            _idAgente = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdAgenciaRecaudadora() As Integer
        Get
            Return _idAgenciaRecaudadora
        End Get
        Set(ByVal value As Integer)
            _idAgenciaRecaudadora = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaAgrupar() As Date
        Get
            Return _fechaAgrupar
        End Get
        Set(ByVal value As Date)
            _fechaAgrupar = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaApertura() As Date
        Get
            Return _fechaApertura
        End Get
        Set(ByVal value As Date)
            _fechaApertura = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaCierre() As String
        Get
            Return _fechaCierre
        End Get
        Set(ByVal value As String)
            _fechaCierre = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaValuta() As String
        Get
            Return _fechaValuta
        End Get
        Set(ByVal value As String)
            _fechaValuta = value
        End Set
    End Property

	<DataMember()> _
    Public Property EstadoFechaValuta() As Integer
        Get
            Return _estadoFechaValuta
        End Get
        Set(ByVal value As Integer)
            _estadoFechaValuta = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property NombreAgencia() As String
        Get
            Return _nombreAgencia
        End Get
        Set(ByVal value As String)
            _nombreAgencia = value
        End Set
    End Property

	<DataMember()> _
    Public Property NombreAgente() As String
        Get
            Return _nombreAgente
        End Get
        Set(ByVal value As String)
            _nombreAgente = value
        End Set
    End Property

	<DataMember()> _
    Public Property NombreCaja() As String
        Get
            Return _nombreCaja
        End Get
        Set(ByVal value As String)
            _nombreCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property NombreEstado() As String
        Get
            Return _nombreEstado
        End Get
        Set(ByVal value As String)
            _nombreEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property NroOPRecibidas() As Integer
        Get
            Return _nroOPRecibidas
        End Get
        Set(ByVal value As Integer)
            _nroOPRecibidas = value
        End Set
    End Property

	<DataMember()> _
    Public Property NroOPAnuladas() As Integer
        Get
            Return _nroOPAnuladas
        End Get
        Set(ByVal value As Integer)
            _nroOPAnuladas = value
        End Set
    End Property

	<DataMember()> _
    Public Property Liquidar() As Boolean
        Get
            Return _liquidar
        End Get
        Set(ByVal value As Boolean)
            _liquidar = value
        End Set
    End Property

	<DataMember()> _
    Public Property MedioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property SimboloMoneda() As String
        Get
            Return _simboloMoneda
        End Get
        Set(ByVal value As String)
            _simboloMoneda = value
        End Set
    End Property

	<DataMember()> _
    Public Property Moneda() As String
        Get
            Return _moneda
        End Get
        Set(ByVal value As String)
            _moneda = value
        End Set
    End Property

	<DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrderBy() As String
        Get
            Return _orderBy
        End Get
        Set(ByVal value As String)
            _orderBy = value
        End Set
    End Property

	<DataMember()> _
    Public Property IsAccending() As Boolean
        Get
            Return _isAccending
        End Get
        Set(ByVal value As Boolean)
            _isAccending = value
        End Set
    End Property

    Public Class NombreCajaBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.NombreCaja.CompareTo(y.NombreCaja)
        End Function
    End Class
    Public Class FechaAperturaBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.FechaApertura.CompareTo(y.FechaApertura)
        End Function
    End Class
    Public Class FechaCierreBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.FechaCierre.CompareTo(y.FechaCierre)
        End Function
    End Class
    Public Class NroOPRecibidasBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.NroOPRecibidas.CompareTo(y.NroOPRecibidas)
        End Function
    End Class
    Public Class NroOPAnuladasBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.NroOPAnuladas.CompareTo(y.NroOPAnuladas)
        End Function
    End Class
    Public Class NombreEstadoBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.NombreEstado.CompareTo(y.NombreEstado)
        End Function
    End Class
    Public Class NombreAgenteBEACComparer
        Implements IComparer(Of BEAgenteCaja)
        Public Function Compare(ByVal x As BEAgenteCaja, ByVal y As BEAgenteCaja) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenteCaja).Compare
            Return x.NombreAgente.CompareTo(y.NombreAgente)
        End Function
    End Class
End Class
