Imports System.Runtime.Serialization
Imports System.Collections.Generic
Imports System
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()> Public Class BEAperturaOrigen


    Private _idEstablecimiento As Integer


	<DataMember()> _
    Public Property idEstablecimiento() As Integer
        Get
            Return _idEstablecimiento
        End Get
        Set(ByVal value As Integer)
            _idEstablecimiento = value
        End Set
    End Property



End Class
