Imports System.Runtime.Serialization
Imports System
Imports System.Text
Imports System.Data
Imports System.Collections.Generic

Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEAuditoria
    Inherits _3Dev.FW.Entidades.BusinessEntityBase
    Implements IBEAuditoria

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        AsignarAuditoria(reader)
    End Sub

    Protected Sub AsignarAuditoria(ByVal reader As IDataReader)
        Me.IdUsuarioCreacion = ObjectToInt32(reader, "IdUsuarioCreacion")
        Me.IdUsuarioActualizacion = ObjectToInt32(reader, "IdUsuarioActualizacion")
        Me.FechaCreacion = ObjectToDateTime(reader, "FechaCreacion")
        Me.FechaActualizacion = ObjectToDateTime(reader, "FechaActualizacion")
        Me._usuarioCreacionNombre = ObjectToString(reader, "usuarioCreacionNombre")
        Me._usuarioActualizacionNombre = ObjectToString(reader, "usuarioActualizacionNombre")
    End Sub

    Private _idUsuarioCreacion As Integer
	<DataMember()> _
    Public Property IdUsuarioCreacion() As Integer Implements IBEAuditoria.IdUsuarioCreacion
        Get
            Return _idUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _idUsuarioCreacion = value
        End Set
    End Property

    Private _idUsuarioActualizacion As Integer
	<DataMember()> _
    Public Property IdUsuarioActualizacion() As Integer Implements IBEAuditoria.IdUsuarioActualizacion
        Get
            Return _idUsuarioActualizacion
        End Get
        Set(ByVal value As Integer)
            _idUsuarioActualizacion = value
        End Set
    End Property

    Private _fechaCreacion As DateTime
	<DataMember()> _
    Public Property FechaCreacion() As DateTime Implements IBEAuditoria.FechaCreacion
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

    Private _fechaActualizacion As DateTime
	<DataMember()> _
    Public Property FechaActualizacion() As DateTime Implements IBEAuditoria.FechaActualizacion
        Get
            Return _fechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _fechaActualizacion = value
        End Set
    End Property

    Private _usuarioCreacionNombre As String
	<DataMember()> _
    Public Property UsuarioCreacionNombre() As String Implements IBEAuditoria.UsuarioCreacionNombre
        Get
            Return _usuarioCreacionNombre
        End Get
        Set(ByVal value As String)
            _usuarioCreacionNombre = value
        End Set
    End Property

    Private _usuarioActualizacionNombre As String
	<DataMember()> _
    Public Property UsuarioActualizacionNombre() As String Implements IBEAuditoria.UsuarioActualizacionNombre
        Get
            Return _usuarioActualizacionNombre
        End Get
        Set(ByVal value As String)
            _usuarioActualizacionNombre = value
        End Set
    End Property

End Class
