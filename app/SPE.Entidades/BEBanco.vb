Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> Public Class BEBanco
    Inherits BEAuditoria

    Private _idBanco As Integer
    Private _codigo As String
    Private _descripcion As String
    Private _idEstado As Integer

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdBanco = DataUtil.ObjectToInt32(reader("IdBanco").ToString)
        Me.Codigo = reader("Codigo").ToString
        Me.Descripcion = reader("Descripcion").ToString
    End Sub

	<DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property

	<DataMember()> _
    Public Property Codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

End Class
