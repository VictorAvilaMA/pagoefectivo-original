Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BECaja
    Inherits BEAuditoria
    Private _idCaja As Integer
    Private _nombre As String
    Private _idUsoCaja As Integer
    Private _idEstado As Integer
    Private _idAgenciaRecaudadora As Integer
    Private _estado As String

    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.IdCaja = Convert.ToInt32(reader("IdCaja").ToString)
        Me.Nombre = reader("Nombre").ToString
        Me.IdUsoCaja = Convert.ToInt32(reader("IdUsoCaja"))
        Me.IdEstado = Convert.ToInt32(reader("IdEstado").ToString)
        Me.IdAgenciaRecaudadora = Convert.ToInt32(reader("IdAgenciaRecaudadora").ToString)
        Me.Estado = reader("Estado").ToString
    End Sub

	<DataMember()> _
    Public Property IdAgenciaRecaudadora() As Integer
        Get
            Return _idAgenciaRecaudadora
        End Get
        Set(ByVal value As Integer)
            _idAgenciaRecaudadora = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdCaja() As Integer
        Get
            Return _idCaja
        End Get
        Set(ByVal value As Integer)
            _idCaja = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsoCaja() As Integer
        Get
            Return _idUsoCaja
        End Get
        Set(ByVal value As Integer)
            _idUsoCaja = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

	<DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

End Class
