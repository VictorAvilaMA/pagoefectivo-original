Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BECiudad

    Private _idciudad As Integer
    Private _descripcion As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdCiudad = ObjectToInt32(reader("IdCiudad"))
        Me.Descripcion = ObjectToString(reader("Descripcion"))
    End Sub

	<DataMember()> _
    Public Property IdCiudad() As Integer
        Get
            Return _idciudad
        End Get
        Set(ByVal value As Integer)
            _idciudad = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class
