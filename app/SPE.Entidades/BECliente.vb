Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BECliente
    Inherits BEUsuarioBase

    Private _aliasCliente As String
    Private _emailAltenativo As String
    Private _idCliente As Integer

    Private _ordenPago As BEOrdenPago = Nothing

    'Private _idGenero As Integer
    Private _idOrigenRegistro As Integer
    'Private _fechaNacimiento As Date

    Private _habilitarMonedero As Integer
    Private _observacion As String

    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal reader As IDataReader)
        MyBase.New(reader)
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.EmailAlternativo = ObjectToString(reader("EmailAlternativo"))
        Me.AliasCliente = ObjectToString(reader("Alias"))
        Me.IdUsuario = ObjectToInt32(reader, "idUsuario")
    End Sub

    ' Flag para saber si el cliente fue registrado en la actual generación del CIP
    ' o si fue registrado con anterioridad, falso por predeterminado
    ' utilizado en ObtenerClienteParaWS del BLOrdenPago
    Private _RecienRegistrado As Boolean = False
    Public Property RecienRegistrado() As Boolean
        Get
            Return _RecienRegistrado
        End Get
        Set(ByVal value As Boolean)
            _RecienRegistrado = value
        End Set
    End Property

    Public Sub New(ByVal reader As IDataReader, ByVal flag As Integer)
        MyBase.New(reader, 1)
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.EmailAlternativo = ObjectToString(reader("EmailAlternativo"))
        Me.AliasCliente = ObjectToString(reader("Alias"))
        Me.IdUsuario = ObjectToInt32(reader, "idUsuario")
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal flag As Integer, ByVal flag2 As String)
        MyBase.New(reader, flag, flag2)
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.AliasCliente = ObjectToString(reader("Alias"))
        Me.HabilitarMonedero = ObjectToInt32(reader("HabilitarMonedero"))

    End Sub

    <DataMember()> _
    Public Property EmailAlternativo() As String
        Get
            Return _emailAltenativo
        End Get
        Set(ByVal value As String)
            _emailAltenativo = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property HabilitarMonedero() As Integer
        Get
            Return _habilitarMonedero
        End Get
        Set(ByVal value As Integer)
            _habilitarMonedero = value
        End Set
    End Property

    <DataMember()> _
    Public Property AliasCliente() As String
        Get
            Return _aliasCliente
        End Get
        Set(ByVal value As String)
            _aliasCliente = value
        End Set
    End Property


    <DataMember()> _
    Public Property OrdenPago() As BEOrdenPago
        Get
            Return _ordenPago
        End Get
        Set(ByVal value As BEOrdenPago)
            _ordenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    Public Class EmailComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BECliente).Email.CompareTo(CType(y, BECliente).Email)
        End Function
    End Class
    Public Class NombresComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BECliente).Nombres.CompareTo(CType(y, BECliente).Nombres)
        End Function
    End Class
    Public Class ApellidosComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BECliente).Apellidos.CompareTo(CType(y, BECliente).Apellidos)
        End Function
    End Class
    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BECliente).DescripcionEstado.CompareTo(CType(y, BECliente).DescripcionEstado)
        End Function
    End Class

    Function MakePG(reader As IDataReader) As _3Dev.FW.Entidades.BusinessEntityBase
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.EmailAlternativo = ObjectToString(reader("EmailAlternativo"))
        Me.AliasCliente = ObjectToString(reader("Alias"))
        Me.IdUsuario = ObjectToInt32(reader, "idUsuario")
        Me.IdUsuario = ObjectToInt32(reader("IdUsuario"))
        Me.Nombres = ObjectToString(reader("Nombres"))
        Me.Apellidos = ObjectToString(reader("Apellidos"))
        Me.NombresApellidos = ObjectToString(reader("Nombres")) + " " + ObjectToString(reader("Apellidos"))
        Me.IdTipoDocumento = ObjectToInt32(reader("IdTipoDocumento"))
        Me.NumeroDocumento = ObjectToString(reader("NumeroDocumento"))
        Me.Direccion = ObjectToString(reader("Direccion"))
        Me.Telefono = ObjectToString(reader("Telefono"))
        Me.Email = ObjectToString(reader("Email"))
        Me.IdEstado = ObjectToInt32(reader("IdEstado"))
        Me.IdUsuarioCreacion = ObjectToInt32(reader("IdUsuarioCreacion"))
        Me.FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        Me.IdUsuarioActualizacion = ObjectToInt32(reader("IdUsuarioActualizacion"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.IdPais = ObjectToInt32(reader("IdPais"))
        Me.IdDepartamento = ObjectToInt32(reader("IdDepartamento"))
        Me.IdCiudad = ObjectToInt32(reader("IdCiudad"))
        Me.DescripcionEstado = ObjectToString(reader("DescripcionEstado"))
        Me.IdGenero = ObjectToInt32(reader("IdGenero"))
        Me.FechaNacimiento = ObjectToDateTime(reader("FechaNacimiento"))
        Me.TotalPageNumbers = ObjectToInt32(reader("TOTAL_RESULT"))
        Return Me
    End Function

End Class


