Imports System.Runtime.Serialization
Imports System.Collections.Generic
Imports System
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil


<Serializable()> <DataContract()> _
Public Class BEComercio
    Inherits BEAuditoria

    Private _idComercio As Integer
    Private _codComercio As String
    Private _razonSocial As String
    Private _ruc As String
    Private _contacto As String
    Private _telefono As String
    Private _direccion As String
    Private _idCiudad As Integer
    Private _ciudad As String
    Private _idDepartamento As Integer
    Private _idPais As Integer
    Private _idEstado As Integer
    Private _DescripcionEstado As String
    Private _FechaMinima As DateTime
    Private _FechaMaxima As DateTime
    Private _Cantidad As Integer


    Public Sub New()

    End Sub


    Private Sub ActCampos(ByVal reader As IDataReader)
        Me.idComercio = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("idComercio"))
        Me.codComercio = _3Dev.FW.Util.DataUtil.ObjectToString(reader("Codigo"))
        Me.razonSocial = _3Dev.FW.Util.DataUtil.ObjectToString(reader("RazonSocial"))
        Me.ruc = DataUtil.ObjectToString(reader("RUC"))
        Me.contacto = DataUtil.ObjectToString(reader("Contacto"))
        Me.telefono = DataUtil.ObjectToString(reader("Telefono"))
        Me.direccion = DataUtil.ObjectToString(reader("Direccion"))
        Me.idCiudad = DataUtil.ObjectToString(reader("IdCiudad"))
        Me.Ciudad = DataUtil.ObjectToString(reader("dciudis"))
        Me.idEstado = DataUtil.ObjectToInt32(reader("IdEstado"))
        Me.DescripcionEstado = ObjectTostring(reader("DescripcionEstado"))

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        If (tipo = "busq") Then
            ActCampos(reader)
        ElseIf (tipo = "busqByID") Then
            ActCampos(reader)
            Me.idDepartamento = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("IdDepartamento"))
            Me.idPais = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("IdPais"))
        ElseIf (tipo = "busqWithAO") Then
            Me.idComercio = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("idComercio"))
            Me.razonSocial = _3Dev.FW.Util.DataUtil.ObjectToString(reader("RazonSocial"))
            Me.FechaMinima = _3Dev.FW.Util.DataUtil.ObjectToDateTime(reader("MinFecha"))
            Me.FechaMaxima = _3Dev.FW.Util.DataUtil.ObjectToDateTime(reader("MaxFecha"))
            Me.Cantidad = _3Dev.FW.Util.DataUtil.ObjectToInt32(reader("Cantidad"))
            Me.codComercio = _3Dev.FW.Util.DataUtil.ObjectToString(reader("Codigo"))

        End If
    End Sub
	<DataMember()> _
    Public Property codComercio() As String
        Get
            Return _codComercio
        End Get
        Set(ByVal value As String)
            _codComercio = value
        End Set
    End Property
	<DataMember()> _
    Public Property Cantidad() As Integer
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Integer)
            _Cantidad = value
        End Set
    End Property

	<DataMember()> _
    Public Property idComercio() As Integer
        Get
            Return _idComercio
        End Get
        Set(ByVal value As Integer)
            _idComercio = value
        End Set
    End Property



	<DataMember()> _
    Public Property razonSocial() As String
        Get
            Return _razonSocial
        End Get
        Set(ByVal value As String)
            _razonSocial = value
        End Set
    End Property

	<DataMember()> _
    Public Property ruc() As String
        Get
            Return _ruc
        End Get
        Set(ByVal value As String)
            _ruc = value
        End Set
    End Property

	<DataMember()> _
    Public Property contacto() As String
        Get
            Return _contacto
        End Get
        Set(ByVal value As String)
            _contacto = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaMinima() As DateTime
        Get
            Return _FechaMinima
        End Get
        Set(ByVal value As DateTime)
            _FechaMinima = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaMaxima() As DateTime
        Get
            Return _FechaMaxima
        End Get
        Set(ByVal value As DateTime)
            _FechaMaxima = value
        End Set
    End Property

	<DataMember()> _
    Public Property telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

	<DataMember()> _
    Public Property direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

	<DataMember()> _
    Public Property idCiudad() As Integer
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Integer)
            _idCiudad = value
        End Set
    End Property

	<DataMember()> _
    Public Property Ciudad() As String
        Get
            Return _ciudad
        End Get
        Set(ByVal value As String)
            _ciudad = value
        End Set
    End Property

	<DataMember()> _
    Public Property idPais() As Integer
        Get
            Return _idPais
        End Get
        Set(ByVal value As Integer)
            _idPais = value
        End Set
    End Property
	<DataMember()> _
    Public Property idDepartamento() As Integer
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Integer)
            _idDepartamento = value
        End Set
    End Property
	<DataMember()> _
    Public Property idEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _DescripcionEstado
        End Get
        Set(ByVal value As String)
            _DescripcionEstado = value
        End Set
    End Property


    Public Class RazonSocialComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEComercio).razonSocial.CompareTo(CType(y, BEComercio).razonSocial)
        End Function
    End Class

    Public Class ContactoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEComercio).contacto.CompareTo(CType(y, BEComercio).contacto)
        End Function
    End Class

    Public Class RUCComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEComercio).ruc.CompareTo(CType(y, BEComercio).ruc)
        End Function
    End Class

    Public Class TelefonoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEComercio).telefono.CompareTo(CType(y, BEComercio).telefono)
        End Function
    End Class


End Class
