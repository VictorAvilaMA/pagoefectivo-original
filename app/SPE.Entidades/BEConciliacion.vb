Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
<Serializable()> <DataContract()> Public Class BEConciliacion
    Inherits BEAuditoria
    Public Sub New()
    End Sub
    Private _idConciliacion As Integer
    Private _nombreArchivo As String
    Private _fechaConciliacion As DateTime
    Private _idEstado As Integer
    Private _operacionBancaria As String
    Private _numeroOrdenPago As String
    Private _fechaInicio As DateTime
    Private _fechaFin As DateTime
    Private _listaOperaciones As List(Of String)
    Private _listaOperacionesConciliadas As List(Of BEOperacionBancaria)
    Private _listaOperacionesNoConciliadas As List(Of BEOperacionBancaria)


    Public Sub New(ByVal reader As IDataReader)
        IdConciliacion = Convert.ToInt32(reader("IdConciliacion"))
        FechaConciliacion = Convert.ToDateTime("FechaConciliacion")
        NombreArchivo = reader("NombreArchivo")
        IdEStado = Convert.ToInt32(reader("IdEstado"))
    End Sub
	<DataMember()> _
    Public Property IdConciliacion() As Integer
        Get
            Return _idConciliacion
        End Get
        Set(ByVal value As Integer)
            _idConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaConciliacion() As DateTime
        Get
            Return _fechaConciliacion
        End Get
        Set(ByVal value As DateTime)
            _fechaConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEStado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property OperacionBancaria() As String
        Get
            Return _operacionBancaria
        End Get
        Set(ByVal value As String)
            _operacionBancaria = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As DateTime)
            _fechaFin = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesBancarias() As List(Of String)
        Get
            Return _listaOperaciones
        End Get
        Set(ByVal value As List(Of String))
            _listaOperaciones = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesConciliadas() As List(Of BEOperacionBancaria)
        Get
            Return _listaOperacionesConciliadas
        End Get
        Set(ByVal value As List(Of BEOperacionBancaria))
            _listaOperacionesConciliadas = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesNoConciliadas() As List(Of BEOperacionBancaria)
        Get
            Return _listaOperacionesNoConciliadas
        End Get
        Set(ByVal value As List(Of BEOperacionBancaria))
            _listaOperacionesNoConciliadas = value
        End Set
    End Property

End Class
