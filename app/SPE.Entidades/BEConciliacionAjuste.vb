Imports _3Dev.FW.Util.DataUtil

<Serializable()> _
Public Class BEConciliacionAjuste

    Private _IdBanco As Integer
    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(ByVal value As Integer)
            _IdBanco = value
        End Set
    End Property

    Private _IdDetConciliacion As Integer
    Public Property IdDetConciliacion() As Integer
        Get
            Return _IdDetConciliacion
        End Get
        Set(ByVal value As Integer)
            _IdDetConciliacion = value
        End Set
    End Property

    Private _CIP As String
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _RazonSocial As String
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Private _Servicio As String
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal value As String)
            _Servicio = value
        End Set
    End Property

    Private _Banco As String
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property

    Private _NombreArchivo As String
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property

    Private _Fecha As DateTime
    Public Property Fecha() As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        IdBanco = ObjectToString(reader, "IdBanco")
        CodigoBanco = ObjectToString(reader, "CodigoBanco")
        IdDetConciliacion = ObjectToInt64(reader, "IdDetConciliacion")
        CIP = ObjectToString(reader, "CIP")
        RazonSocial = ObjectToString(reader, "RazonSocial")
        Servicio = ObjectToString(reader, "Servicio")
        Banco = ObjectToString(reader, "Banco")
        NombreArchivo = ObjectToString(reader, "NombreArchivo")
        Fecha = ObjectToDateTime(reader, "Fecha")
    End Sub

    Private _CodigoBanco As String
    Public Property CodigoBanco() As String
        Get
            Return _CodigoBanco
        End Get
        Set(ByVal value As String)
            _CodigoBanco = value
        End Set
    End Property

    Private _CodigoServicio As String
    Public Property CodigoServicio() As String
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As String)
            _CodigoServicio = value
        End Set
    End Property

    Private _FechaInicio As DateTime
    Public Property FechaInicio() As DateTime
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As DateTime)
            _FechaInicio = value
        End Set
    End Property

    Private _FechaFin As DateTime
    Public Property FechaFin() As DateTime
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As DateTime)
            _FechaFin = value
        End Set
    End Property

End Class
