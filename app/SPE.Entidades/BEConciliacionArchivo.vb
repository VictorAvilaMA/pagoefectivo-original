Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
<Serializable()> <DataContract()> Public Class BEConciliacionArchivo
    Inherits BEAuditoria
    Public Sub New()
    End Sub
    Private _idConciliacionArchivo As Int64
    Private _idBanco As Integer
    Private _codigoBanco As String
    Private _Banco As String
    Private _idEstado As Integer
    Private _idTipoConciliacion As Integer
    Private _IdOrigenConciliacion As Integer
    Private _descEstado As String
    Private _nombreArchivo As String
    Private _fecha As DateTime
    Private _fechaInicio As DateTime
    Private _fechaFin As Nullable(Of DateTime)
    Private _idUsuarioCreacionCA As Integer
    Private _IdUsuarioActualizacionCA As Nullable(Of Integer)
    Private _FechaActualizacionCA As Nullable(Of DateTime)
    Private _observacion As String
    Private _DescTipoConciliacion As String
    Private _DescOrigenConciliacion As String
    Private _Bytes As Byte()
    Private _ListaOperacionesConciliadas As List(Of BEConciliacionDetalle)
    Private _ListaOperacionesNoConciliadas As List(Of BEConciliacionDetalle)
    Private _CantCIPs As Integer
    Private _CantCIPsConciliados As Integer

    Public Sub New(ByVal reader As IDataReader)
        IdConciliacionArchivo = Convert.ToInt32(reader("IdConciliacionArchivo"))
        DescEstado = Convert.ToString(reader("DescEstado"))
        IdBanco = Convert.ToInt32(reader("IdBanco"))
        Banco = Convert.ToString(reader("Banco"))
        CodigoBanco = Convert.ToString(reader("CodigoBanco"))
        IdEstado = Convert.ToInt32(reader("IdEstado"))
        IdTipoConciliacion = Convert.ToInt32(reader("IdTipoConciliacion"))
        IdOrigenConciliacion = Convert.ToInt32(reader("IdOrigenConciliacion"))
        DescTipoConciliacion = Convert.ToString(reader("DescTipoConciliacion"))
        DescOrigenConciliacion = Convert.ToString(reader("DescOrigenConciliacion"))
        CantCIPs = DataUtil.ObjectToInt32(reader("CantCIPs"))
        CantCIPsConciliados = DataUtil.ObjectToInt32(reader("CantCIPsConciliados"))
        NombreArchivo = Convert.ToString(reader("NombreArchivo"))
        Fecha = Convert.ToDateTime(reader("Fecha"))
        FechaInicio = Convert.ToDateTime(reader("FechaInicio"))
        FechaFin = DataUtil.ObjectToDateTimeNull(reader("FechaFin"))
        Observacion = Convert.ToString(reader("Observacion"))
    End Sub

	<DataMember()> _
    Public Property IdConciliacionArchivo() As Int64
        Get
            Return _idConciliacionArchivo
        End Get
        Set(ByVal value As Int64)
            _idConciliacionArchivo = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescEstado() As String
        Get
            Return _descEstado
        End Get
        Set(ByVal value As String)
            _descEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdTipoConciliacion() As Integer
        Get
            Return _idTipoConciliacion
        End Get
        Set(ByVal value As Integer)
            _idTipoConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdOrigenConciliacion() As Integer
        Get
            Return _IdOrigenConciliacion
        End Get
        Set(ByVal value As Integer)
            _IdOrigenConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property
	<DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
	<DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescTipoConciliacion() As String
        Get
            Return _DescTipoConciliacion
        End Get
        Set(ByVal value As String)
            _DescTipoConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescOrigenConciliacion() As String
        Get
            Return _DescOrigenConciliacion
        End Get
        Set(ByVal value As String)
            _DescOrigenConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property Fecha() As DateTime
        Get
            Return _fecha
        End Get
        Set(ByVal value As DateTime)
            _fecha = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaFin() As Nullable(Of DateTime)
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _fechaFin = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsuarioCreacionCA() As Integer
        Get
            Return _idUsuarioCreacionCA
        End Get
        Set(ByVal value As Integer)
            _idUsuarioCreacionCA = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsuarioActualizacionCA() As Nullable(Of Integer)
        Get
            Return _IdUsuarioActualizacionCA
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdUsuarioActualizacionCA = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaActualizacionCA() As Nullable(Of DateTime)
        Get
            Return _FechaActualizacionCA
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaActualizacionCA = value
        End Set
    End Property
	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property Bytes() As Byte()
        Get
            Return _Bytes
        End Get
        Set(ByVal value As Byte())
            _Bytes = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesConciliadas() As List(Of BEConciliacionDetalle)
        Get
            Return _ListaOperacionesConciliadas
        End Get
        Set(ByVal value As List(Of BEConciliacionDetalle))
            _ListaOperacionesConciliadas = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesNoConciliadas() As List(Of BEConciliacionDetalle)
        Get
            Return _ListaOperacionesNoConciliadas
        End Get
        Set(ByVal value As List(Of BEConciliacionDetalle))
            _ListaOperacionesNoConciliadas = value
        End Set
    End Property
	<DataMember()> _
    Public Property CantCIPs() As Integer
        Get
            Return _CantCIPs
        End Get
        Set(ByVal value As Integer)
            _CantCIPs = value
        End Set
    End Property
	<DataMember()> _
    Public Property CantCIPsConciliados() As Integer
        Get
            Return _CantCIPsConciliados
        End Get
        Set(ByVal value As Integer)
            _CantCIPsConciliados = value
        End Set
    End Property

#Region " PreConciliacion "
    Private _lote As String
    <DataMember()> _
    Public Property Lote() As String
        Get
            Return _lote
        End Get
        Set(ByVal value As String)
            _lote = value
        End Set
    End Property
#End Region

End Class
