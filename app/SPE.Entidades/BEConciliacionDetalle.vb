Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEConciliacionDetalle
    Private _IdConciliacionDetalle As Int64
    Private _IdEstado As Integer
    Private _Observacion As String
    Private _CIP As String
    Private _CancelarCIP As Boolean
    Private _CodigoAgenciaBancaria As String
    Private _CodigoPuntoVenta As String
    Private _NumeroSerieTerminal As String
    Private _NotificarCreacionAB As Boolean
    Private _NotificarCreacionPV As Boolean
    Private _NotificarCreacionT As Boolean
    Private _NumeroOperacion As String
    Private _Monto As Decimal
    Private _CodMonedaBanco As String
    Private _CodigoServicio As String
    Private _CodigoMedioPago As String


    <DataMember()> _
    Public Property IdConciliacionDetalle() As Int64
        Get
            Return _IdConciliacionDetalle
        End Get
        Set(ByVal value As Int64)
            _IdConciliacionDetalle = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property
    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property CancelarCIP() As Boolean
        Get
            Return _CancelarCIP
        End Get
        Set(ByVal value As Boolean)
            _CancelarCIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoPuntoVenta() As String
        Get
            Return _CodigoPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodigoPuntoVenta = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroSerieTerminal() As String
        Get
            Return _NumeroSerieTerminal
        End Get
        Set(ByVal value As String)
            _NumeroSerieTerminal = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoAgenciaBancaria() As String
        Get
            Return _CodigoAgenciaBancaria
        End Get
        Set(ByVal value As String)
            _CodigoAgenciaBancaria = value
        End Set
    End Property
    <DataMember()> _
    Public Property NotificarCreacionAB() As Boolean
        Get
            Return _NotificarCreacionAB
        End Get
        Set(ByVal value As Boolean)
            _NotificarCreacionAB = value
        End Set
    End Property
    <DataMember()> _
    Public Property NotificarCreacionPV() As Boolean
        Get
            Return _NotificarCreacionPV
        End Get
        Set(ByVal value As Boolean)
            _NotificarCreacionPV = value
        End Set
    End Property
    <DataMember()> _
    Public Property NotificarCreacionT() As Boolean
        Get
            Return _NotificarCreacionT
        End Get
        Set(ByVal value As Boolean)
            _NotificarCreacionT = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _NumeroOperacion
        End Get
        Set(ByVal value As String)
            _NumeroOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _Monto
        End Get
        Set(ByVal value As Decimal)
            _Monto = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodMonedaBanco() As String
        Get
            Return _CodMonedaBanco
        End Get
        Set(ByVal value As String)
            _CodMonedaBanco = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoMedioPago() As String
        Get
            Return _CodigoMedioPago
        End Get
        Set(ByVal value As String)
            _CodigoMedioPago = value
        End Set
    End Property

    Private _IdConciliacionArchivo As Int64
    Private _Fecha As DateTime
    Private _FechaCancelacion As Nullable(Of DateTime)
    Private _NombreArchivo As String
    Private _CodigoBanco As String
    Private _Banco As String
    Private _Estado As String
    Private _FechaInicio As DateTime
    Private _FechaFin As DateTime

    <DataMember()> _
    Public Property Fecha() As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCancelacion() As Nullable(Of DateTime)
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaCancelacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoBanco() As String
        Get
            Return _CodigoBanco
        End Get
        Set(ByVal value As String)
            _CodigoBanco = value
        End Set
    End Property
    <DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As DateTime)
            _FechaInicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As DateTime)
            _FechaFin = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdConciliacionArchivo() As Int64
        Get
            Return _IdConciliacionArchivo
        End Get
        Set(ByVal value As Int64)
            _IdConciliacionArchivo = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoServicio() As String
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As String)
            _CodigoServicio = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Fecha = Convert.ToDateTime(reader("Fecha"))
        If IsDBNull(reader("FechaCancelacion")) Then
            FechaCancelacion = Nothing
        Else
            FechaCancelacion = Convert.ToDateTime(reader("FechaCancelacion"))
        End If
        CIP = Convert.ToString(reader("CIP"))
        NombreArchivo = Convert.ToString(reader("NombreArchivo"))
        Banco = Convert.ToString(reader("Banco"))
        Estado = Convert.ToString(reader("Estado"))
        Observacion = Convert.ToString(reader("Observacion"))
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Banco As String)
        IdConciliacionDetalle = Convert.ToInt64(reader("IdResultado"))
        IdEstado = Convert.ToInt32(reader("IdEstadoResultado"))
        Observacion = Convert.ToString(reader("Observacion"))
        CIP = Convert.ToString(reader("CIP"))
        CancelarCIP = Convert.ToBoolean(reader("CancelarCIP"))
        NumeroOperacion = Convert.ToString(reader("NumeroOperacion"))
        Monto = Convert.ToDecimal(reader("Monto"))
        CodMonedaBanco = Convert.ToString(reader("CodMonedaBanco"))
        CodigoServicio = Convert.ToString(reader("CodigoServicio"))
        Select Case Banco
            Case "BCP"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
            Case "BBVA", "SBK"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
            Case "FC"
                CodigoPuntoVenta = Convert.ToString(reader("CodigoPuntoVenta"))
                NotificarCreacionPV = Convert.ToBoolean(reader("NotificarCreacionPV"))
                NumeroSerieTerminal = Convert.ToString(reader("NumeroSerieTerminal"))
                NotificarCreacionT = Convert.ToBoolean(reader("NotificarCreacionT"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
        End Select
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Banco As String, ByVal Conc As Integer)
        IdConciliacionDetalle = Convert.ToInt64(reader("IdResultado"))
        IdEstado = Convert.ToInt32(reader("IdEstadoResultado"))
        Observacion = Convert.ToString(reader("Observacion"))
        CIP = Convert.ToString(reader("CIP"))
        CancelarCIP = Convert.ToBoolean(reader("CancelarCIP"))
        NumeroOperacion = Convert.ToString(reader("NumeroOperacion"))
        Monto = Convert.ToDecimal(reader("Monto"))
        CodMonedaBanco = Convert.ToString(reader("CodMonedaBanco"))
        CodigoServicio = Convert.ToString(reader("CodigoServicio"))
        Select Case Banco
            Case "BCP"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoPuntoVenta = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "BBVA"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
                CodigoPuntoVenta = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "SBK"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
                CodigoPuntoVenta = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "FC"
                CodigoPuntoVenta = Convert.ToString(reader("CodigoPuntoVenta"))
                NotificarCreacionPV = Convert.ToBoolean(reader("NotificarCreacionPV"))
                NumeroSerieTerminal = Convert.ToString(reader("NumeroSerieTerminal"))
                NotificarCreacionT = Convert.ToBoolean(reader("NotificarCreacionT"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
                CodigoAgenciaBancaria = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "WU"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoPuntoVenta = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "IBK"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoPuntoVenta = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "BIF"
                CodigoAgenciaBancaria = Convert.ToString(reader("CodigoAgenciaBancaria"))
                NotificarCreacionAB = Convert.ToBoolean(reader("NotificarCreacionAB"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
                CodigoPuntoVenta = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))
            Case "Kasnet"
                CodigoPuntoVenta = Convert.ToString(reader("CodigoPuntoVenta"))
                NotificarCreacionPV = Convert.ToBoolean(reader("NotificarCreacionPV"))
                NumeroSerieTerminal = Convert.ToString(reader("NumeroSerieTerminal"))
                NotificarCreacionT = Convert.ToBoolean(reader("NotificarCreacionT"))
                CodigoMedioPago = Convert.ToString(reader("CodigoMedioPago"))
                CodigoAgenciaBancaria = Convert.ToString(LTrim(RTrim(reader("FechaCancelacion"))))

        End Select
    End Sub

#Region " Pre-Conciliacion "
    Public Sub New(ByVal reader As IDataReader, ByVal val1 As Integer, ByVal val2 As Integer)
        CIP = DataUtil.ObjectToString(reader("CodigoPago_t"))
        Monto = DataUtil.ObjectToDecimal(reader("Monto_t"))
        FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion_t"))
        NumeroOperacion = DataUtil.ObjectToString(reader("NumeroOperacion_t"))
        IdEstado = DataUtil.ObjectToInt32(reader("IdEstado_t"))
        NumeroSerieTerminal = DataUtil.ObjectToString(reader("OficinaSucursal_t")) 'Sucursal u oficina segun sea el banco para evaluar extornos.
        Observacion = DataUtil.ObjectToString(reader("Observacion_t"))
    End Sub
#End Region

End Class
