Imports _3Dev.FW.Util.DataUtil

<Serializable()> _
Public Class BEConciliacionDetalleAjuste

    Private _NombreArchivo As String
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property

    Private _Banco As String
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property

    Private _Linea As String
    Public Property Linea() As String
        Get
            Return _Linea
        End Get
        Set(ByVal value As String)
            _Linea = value
        End Set
    End Property

    Private _MonedaDetalle As String
    Public Property MonedaDetalle() As String
        Get
            Return _MonedaDetalle
        End Get
        Set(ByVal value As String)
            _MonedaDetalle = value
        End Set
    End Property

    Private _MontoDetalle As Decimal
    Public Property MontoDetalle() As Decimal
        Get
            Return _MontoDetalle
        End Get
        Set(ByVal value As Decimal)
            _MontoDetalle = value
        End Set
    End Property

    Private _EstadoDetalle As String
    Public Property EstadoDetalle() As String
        Get
            Return _EstadoDetalle
        End Get
        Set(ByVal value As String)
            _EstadoDetalle = value
        End Set
    End Property

    Private _Fecha As DateTime
    Public Property Fecha() As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property

    Private _FechaInicio As DateTime
    Public Property FechaInicio() As DateTime
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As DateTime)
            _FechaInicio = value
        End Set
    End Property

    Private _FechaFin As DateTime
    Public Property FechaFin() As DateTime
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As DateTime)
            _FechaFin = value
        End Set
    End Property

    Private _Observacion As String
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property

    Private _CIP As String
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _Moneda As String
    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property

    Private _Monto As Decimal
    Public Property Monto() As Decimal
        Get
            Return _Monto
        End Get
        Set(ByVal value As Decimal)
            _Monto = value
        End Set
    End Property

    Private _Estado As String
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property

    Private _Servicio As String
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal value As String)
            _Servicio = value
        End Set
    End Property

    Private _FechaEmision As DateTime
    Public Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property

    Private _FechaExpiracion As Nullable(Of DateTime)
    Public Property FechaExpiracion() As Nullable(Of DateTime)
        Get
            Return _FechaExpiracion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaExpiracion = value
        End Set
    End Property

    Private _FechaAnulacion As Nullable(Of DateTime)
    Public Property FechaAnulacion() As Nullable(Of DateTime)
        Get
            Return _FechaAnulacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaAnulacion = value
        End Set
    End Property

    Private _FechaCancelacion As Nullable(Of DateTime)
    Public Property FechaCancelacion() As Nullable(Of DateTime)
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaCancelacion = value
        End Set
    End Property

    Private _FechaEliminacion As Nullable(Of DateTime)
    Public Property FechaEliminacion() As Nullable(Of DateTime)
        Get
            Return _FechaEliminacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaEliminacion = value
        End Set
    End Property

    Private _EstadoConciliacion As String
    Public Property EstadoConciliacion() As String
        Get
            Return _EstadoConciliacion
        End Get
        Set(ByVal value As String)
            _EstadoConciliacion = value
        End Set
    End Property

    Private _FechaConciliacion As Nullable(Of DateTime)
    Public Property FechaConciliacion() As Nullable(Of DateTime)
        Get
            Return _FechaConciliacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaConciliacion = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        NombreArchivo = ObjectToString(reader, "NombreArchivo")
        Banco = ObjectToString(reader, "Banco")
        Linea = ObjectToString(reader, "Linea")
        MonedaDetalle = ObjectToString(reader, "MonedaDetalle")
        MontoDetalle = ObjectToDecimal(reader, "MontoDetalle")
        EstadoDetalle = ObjectToString(reader, "EstadoDetalle")
        Fecha = ObjectToDateTime(reader, "Fecha")
        FechaInicio = ObjectToDateTime(reader, "FechaInicio")
        FechaFin = ObjectToDateTime(reader, "FechaFin")
        Observacion = ObjectToString(reader, "ObservacionDetalle")

        CIP = ObjectToString(reader, "CIP")
        Moneda = ObjectToString(reader, "Moneda")
        Monto = ObjectToDecimal(reader, "Monto")
        Estado = ObjectToString(reader, "Estado")
        Servicio = ObjectToString(reader, "Servicio")
        FechaEmision = ObjectToDateTime(reader, "FechaEmision")
        FechaCancelacion = ObjectToDateTimeNull(reader("FechaCancelacion"))
        FechaEliminacion = ObjectToDateTimeNull(reader("FechaEliminacion"))
        FechaExpiracion = ObjectToDateTimeNull(reader("FechaExpiracion"))
        FechaAnulacion = ObjectToDateTimeNull(reader("FechaAnulacion"))
        EstadoConciliacion = ObjectToString(reader, "EstadoConciliacion")
        FechaConciliacion = ObjectToDateTimeNull(reader("FechaConciliacion"))
    End Sub

End Class
