﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEConciliacionDiaria

    Private _IdOrdenPago As String
    Private _OrderIdComercio As String
    Private _Moneda As String
    Private _Banco As String
    Private _Estado As String
    Private _Servicio As String
    Private _Total As Decimal
    Private _FechaCancelacion As DateTime
    Private _UsuarioEmail As String
    Private _Observacion As String





    <DataMember()> _
    Public Property IdOrdenPago() As String
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As String)
            _IdOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property OrderIdComercio() As String
        Get
            Return _OrderIdComercio
        End Get
        Set(ByVal value As String)
            _OrderIdComercio = value
        End Set
    End Property
    <DataMember()> _
    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property
    <DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property
    <DataMember()> _
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal value As String)
            _Servicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            _Total = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCancelacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioEmail() As String
        Get
            Return _UsuarioEmail
        End Get
        Set(ByVal value As String)
            _UsuarioEmail = value
        End Set
    End Property
    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    

    Public Sub New()

    End Sub


    Public Sub New(ByVal reader As IDataReader)
        IdOrdenPago = Convert.ToString(reader("IdOrdenPago"))
        OrderIdComercio = Convert.ToString(reader("OrderIdComercio"))
        Moneda = Convert.ToString(reader("Moneda"))
        Banco = Convert.ToString(reader("Banco"))
        Estado = Convert.ToString(reader("Estado"))
        Servicio = Convert.ToString(reader("Servicio"))
        Total = Convert.ToDecimal(reader("Total"))
        FechaCancelacion = Convert.ToDateTime(reader("FechaCancelacion"))
        UsuarioEmail = Convert.ToString(reader("UsuarioEmail"))
        Observacion = Convert.ToString(reader("Observacion"))
    End Sub



End Class

