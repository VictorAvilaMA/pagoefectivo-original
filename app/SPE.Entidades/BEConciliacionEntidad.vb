Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
<Serializable()> <DataContract()> Public Class BEConciliacionEntidad
    Inherits BEAuditoria
    Public Sub New()
    End Sub
    Private _idConciliacionEntidad As Int64
    Private _idConciliacion As Int64
    Private _idBanco As Integer
    Private _codigoBanco As String
    Private _idEstado As Integer
    Private _descEstado As String
    Private _Banco As String
    Private _fecha As DateTime
    Private _fechaInicio As Nullable(Of DateTime)
    Private _fechaFin As Nullable(Of DateTime)
    Private _idUsuarioCreacion As Integer
    Private _IdUsuarioActualizacion As Nullable(Of Integer)
    Private _FechaActualizacion As Nullable(Of DateTime)
    Private _observacion As String
    Private _CantCIPS As Integer
    Private _CantCIPSConciliados As Integer
    Private _listaConcilacionesArchivo As List(Of BEConciliacionArchivo)
     
    Public Sub New(ByVal reader As IDataReader)
        IdConciliacionEntidad = Convert.ToInt64(reader("IdConciliacionEntidad"))
        IdConciliacion = Convert.ToInt64(reader("IdConciliacion"))
        IdBanco = Convert.ToInt32(reader("IdBanco"))
        IdEstado = Convert.ToInt32(reader("IdEstado"))
        DescEstado = reader("DescEstado")
        Banco = reader("Banco")
        CodigoBanco = reader("CodigoBanco")
        CantCIPS = DataUtil.ObjectToInt32Null(reader("CantCIPS"))
        CantCIPSConciliados = DataUtil.ObjectToInt32Null(reader("CantCIPSConciliados"))
        Fecha = Convert.ToDateTime(reader("Fecha"))
        FechaInicio = Convert.ToDateTime(reader("FechaInicio"))
        FechaFin = DataUtil.ObjectToDateTimeNull(reader("FechaFin"))
        IdUsuarioCreacion = Convert.ToInt32(reader("IdUsuarioCreacion"))
        IdUsuarioActualizacionCE = DataUtil.ObjectToInt32Null(reader("IdUsuarioActualizacion"))
        FechaActualizacionCE = DataUtil.ObjectToDateTimeNull(reader("FechaActualizacion"))
        Observacion = reader("Observacion").ToString()
    End Sub
	<DataMember()> _
    Public Property IdConciliacionEntidad() As Int64
        Get
            Return _idConciliacionEntidad
        End Get
        Set(ByVal value As Int64)
            _idConciliacionEntidad = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdConciliacion() As Int64
        Get
            Return _idConciliacion
        End Get
        Set(ByVal value As Int64)
            _idConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescEstado() As String
        Get
            Return _descEstado
        End Get
        Set(ByVal value As String)
            _descEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property
	<DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
	<DataMember()> _
    Public Property Fecha() As DateTime
        Get
            Return _fecha
        End Get
        Set(ByVal value As DateTime)
            _fecha = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaInicio() As Nullable(Of DateTime)
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _fechaInicio = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaFin() As Nullable(Of DateTime)
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _fechaFin = value
        End Set
    End Property
	<DataMember()> _
    Public Property CantCIPS() As Integer
        Get
            Return _CantCIPS
        End Get
        Set(ByVal value As Integer)
            _CantCIPS = value
        End Set
    End Property
	<DataMember()> _
    Public Property CantCIPSConciliados() As Integer
        Get
            Return _CantCIPSConciliados
        End Get
        Set(ByVal value As Integer)
            _CantCIPSConciliados = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsuarioActualizacionCE() As Nullable(Of Integer)
        Get
            Return _IdUsuarioActualizacion
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdUsuarioActualizacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaActualizacionCE() As Nullable(Of DateTime)
        Get
            Return _FechaActualizacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaActualizacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaConcilacionesArchivo() As List(Of BEConciliacionArchivo)
        Get
            Return _listaConcilacionesArchivo
        End Get
        Set(ByVal value As List(Of BEConciliacionArchivo))
            _listaConcilacionesArchivo = value
        End Set
    End Property
End Class
