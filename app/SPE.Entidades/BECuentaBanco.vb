﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Entidades

<Serializable()> <DataContract()>
Public Class BECuentaBanco
    Inherits BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        _idCuentaBanco = CInt(reader("IdCuentaBanco"))
        _nroCuenta = reader("NroCuenta").ToString
        _idBanco = CInt(reader("IdBanco").ToString)
        _idMoneda = CInt(reader("IdMoneda"))
        _MonedaNombre = Convert.ToString(reader("MonedaNombre"))
        _BancoNombre = Convert.ToString(reader("BancoNombre"))
        _CustomNombre = Convert.ToString(reader("CustomNombre"))


    End Sub

    Private _idCuentaBanco As Integer
    Private _nroCuenta As String
    Private _idBanco As Integer
    Private _idMoneda As Integer
    Private _MonedaNombre As String
    Private _BancoNombre As String
    Private _CustomNombre As String

    <DataMember()>
    Public Property IdCuentaBanco() As Integer
        Get
            Return _idCuentaBanco
        End Get
        Set(ByVal value As Integer)
            _idCuentaBanco = value
        End Set
    End Property
    <DataMember()>
    Public Property NroCuenta() As String
        Get
            Return _nroCuenta
        End Get
        Set(ByVal value As String)
            _nroCuenta = value
        End Set
    End Property


    <DataMember()>
    Public Property MonedaNombre() As String
        Get
            Return _MonedaNombre
        End Get
        Set(ByVal value As String)
            _MonedaNombre = value
        End Set
    End Property

    <DataMember()>
    Public Property BancoNombre() As String
        Get
            Return _BancoNombre
        End Get
        Set(ByVal value As String)
            _BancoNombre = value
        End Set
    End Property

    <DataMember()>
    Public Property CustomNombre() As String
        Get
            Return _CustomNombre
        End Get
        Set(ByVal value As String)
            _CustomNombre = value
        End Set
    End Property

    <DataMember()>
    Public Property IdBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property
    <DataMember()>
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

End Class
