﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BECuentaEmpresa
    Inherits BEAuditoria
    Private _idCuentaEmpresa As Integer
    Private _idEmpresaContratante As Integer
    Private _idMoneda As Integer
    Private _banco As String
    Private _nroCuenta As String
    Private _idEstado As Integer

    Private _nombreEmpresaContratante As String
    Private _descripcionMoneda As String
    Private _descripcionEstado As String
    Private _idTipoCuenta As Integer

    Sub New()
    End Sub

    Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        Select Case tipo
            Case "porID"
                IdCuentaEmpresa = DataUtil.ObjectToInt(reader("IdCuentaEmpresa"))
                IdEmpresaContratante = DataUtil.ObjectToInt(reader("IdEmpresaContratante"))
                IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Banco = DataUtil.ObjectToString(reader("BancoDeposito"))
                NroCuenta = DataUtil.ObjectToString(reader("CuentaDeposito"))
                IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                IdTipoCuenta = DataUtil.ObjectToInt(reader("IdTipoCuenta"))
            Case "busq"
                IdCuentaEmpresa = DataUtil.ObjectToInt(reader("IdCuentaEmpresa"))
                IdEmpresaContratante = DataUtil.ObjectToInt(reader("IdEmpresaContratante"))
                IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Banco = DataUtil.ObjectToString(reader("BancoDeposito"))
                NroCuenta = DataUtil.ObjectToString(reader("CuentaDeposito"))
                IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
                FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
                NombreEmpresaContratante = DataUtil.ObjectToString(reader("RazonSocial"))
                DescripcionMoneda = DataUtil.ObjectToString(reader("dmnd000"))
                DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
                TotalPageNumbers = DataUtil.ObjectToInt(reader("TOTAL_RESULT"))
        End Select
    End Sub

    <DataMember()> _
    Public Property IdCuentaEmpresa() As Integer
        Get
            Return _idCuentaEmpresa
        End Get
        Set(ByVal value As Integer)
            _idCuentaEmpresa = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEmpresaContratante() As Integer
        Get
            Return _idEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _idEmpresaContratante = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property
    <DataMember()> _
    Public Property Banco() As String
        Get
            Return _banco
        End Get
        Set(ByVal value As String)
            _banco = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroCuenta() As String
        Get
            Return _nroCuenta
        End Get
        Set(ByVal value As String)
            _nroCuenta = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
    <DataMember()> _
    Public Property NombreEmpresaContratante() As String
        Get
            Return _nombreEmpresaContratante
        End Get
        Set(ByVal value As String)
            _nombreEmpresaContratante = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionMoneda() As String
        Get
            Return _descripcionMoneda
        End Get
        Set(ByVal value As String)
            _descripcionMoneda = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _descripcionEstado
        End Get
        Set(ByVal value As String)
            _descripcionEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTipoCuenta() As Integer
        Get
            Return _idTipoCuenta
        End Get
        Set(ByVal value As Integer)
            _idTipoCuenta = value
        End Set
    End Property

End Class
