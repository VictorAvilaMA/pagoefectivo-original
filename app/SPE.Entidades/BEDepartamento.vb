Imports System.Runtime.Serialization


Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEDepartamento

    Private _idDepartamento As Integer
    Private _descripcion As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdDepartamento = Convert.ToInt32(reader("IdDepartamento").ToString)
        Me.Descripcion = reader("Descripcion").ToString
    End Sub

	<DataMember()> _
    Public Property IdDepartamento() As Integer
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Integer)
            _idDepartamento = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class
