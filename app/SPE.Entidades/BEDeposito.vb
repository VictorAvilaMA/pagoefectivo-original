﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()>
Public Class BEDeposito
    Inherits BEAuditoria

    Private _idDeposito As Int64
    Private _fechaDeposito As DateTime
    Private _codigoOperacion As String
    Private _comentarios As String
    Private _montoDepositado As Decimal
    Private _nroTransaccion As String
    Private _idFactura As Int64
    Private _idConciliacionArchivo As Int64
    Private _idBanco As Integer

    'para registro de factura
    Private _totalComision As Decimal

    'para consulta
    Private _fechaDesde As DateTime
    Private _fechaHasta As DateTime
    Private _descripcionBanco As String
    Private _flagSeleccionado As Boolean
    Private _CIP As String


    Public Sub New()
    End Sub
    Public Sub New(ByVal reader As IDataReader)

    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipoConsulta As String)

        Select Case tipoConsulta
            Case "consultaRegistroFactura"
                DescripcionBanco = ObjectToString(reader("DescripcionBanco"))
                CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                NroTransaccion = ObjectToString(reader("NroTransaccion"))
                FechaDeposito = ObjectToDateTime(reader("FechaDeposito"))
                MontoDepositado = ObjectToDecimal(reader("MontoDepositado"))
                IdDeposito = ObjectToInt64(reader("IdDeposito"))
                IdFactura = ObjectToInt64(reader("IdFactura"))
                Comentarios = ObjectToString(reader("Comentarios"))
                TotalComision = ObjectToDecimal(reader("TotalComision"))
            Case "consultaRegistro"
                TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))
                DescripcionBanco = ObjectToString(reader("DescripcionBanco"))
                CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                NroTransaccion = ObjectToString(reader("NroTransaccion"))
                FechaDeposito = ObjectToDateTime(reader("FechaDeposito"))
                MontoDepositado = ObjectToDecimal(reader("MontoDepositado"))
                IdDeposito = ObjectToInt64(reader("IdDeposito"))
                IdFactura = ObjectToInt64(reader("IdFactura"))
                IdConciliacionArchivo = ObjectToInt32(reader("IdConciliacionArchivo"))
            Case "consultaByIdFactura"
                DescripcionBanco = ObjectToString(reader("DescripcionBanco"))
                CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                NroTransaccion = ObjectToString(reader("NroTransaccion"))
                FechaDeposito = ObjectToDateTime(reader("FechaDeposito"))
                MontoDepositado = ObjectToDecimal(reader("MontoDepositado"))
                IdDeposito = ObjectToInt64(reader("IdDeposito"))
                IdFactura = ObjectToInt64(reader("IdFactura"))
                Comentarios = ObjectToString(reader("Comentarios"))
            Case Else
        End Select
    End Sub

    <DataMember()> _
    Public Property IdDeposito() As Int64
        Get
            Return _idDeposito
        End Get
        Set(ByVal value As Int64)
            _idDeposito = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaDeposito() As DateTime
        Get
            Return _fechaDeposito
        End Get
        Set(ByVal value As DateTime)
            _fechaDeposito = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoOperacion() As String
        Get
            Return _codigoOperacion
        End Get
        Set(ByVal value As String)
            _codigoOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property Comentarios() As String
        Get
            Return _comentarios
        End Get
        Set(ByVal value As String)
            _comentarios = value
        End Set
    End Property
    <DataMember()> _
    Public Property MontoDepositado() As Decimal
        Get
            Return _montoDepositado
        End Get
        Set(ByVal value As Decimal)
            _montoDepositado = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroTransaccion() As String
        Get
            Return _nroTransaccion
        End Get
        Set(ByVal value As String)
            _nroTransaccion = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdFactura() As Int64
        Get
            Return _idFactura
        End Get
        Set(ByVal value As Int64)
            _idFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdConciliacionArchivo() As Int64
        Get
            Return _idConciliacionArchivo
        End Get
        Set(ByVal value As Int64)
            _idConciliacionArchivo = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaDesde() As DateTime
        Get
            Return _fechaDesde
        End Get
        Set(ByVal value As DateTime)
            _fechaDesde = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaHasta() As DateTime
        Get
            Return _fechaHasta
        End Get
        Set(ByVal value As DateTime)
            _fechaHasta = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionBanco() As String
        Get
            Return _descripcionBanco
        End Get
        Set(ByVal value As String)
            _descripcionBanco = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlagSeleccionado() As String
        Get
            Return _flagSeleccionado
        End Get
        Set(ByVal value As String)
            _flagSeleccionado = value
        End Set
    End Property
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property TotalComision() As Decimal
        Get
            Return _totalComision
        End Get
        Set(ByVal value As Decimal)
            _totalComision = value
        End Set
    End Property
End Class
