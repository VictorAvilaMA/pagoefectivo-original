Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad
<Serializable()> <DataContract()> Public Class BEDetConciliacionBCP
    Inherits BEAuditoria
    Private _IdDetConciliacionBCP As Int64
    Private _IdConciliacionArchivo As Integer
    Private _idEstado As Integer
    Private _DescEstado As String
    Private _TipoRegistro As String
    Private _CodigoSucursal As String
    Private _CodigoMoneda As String
    Private _NumeroCuenta As String
    Private _CodigoDepositante As String
    Private _InformacionRetorno As String
    Private _FechaConciliacion As DateTime
    Private _FechaPago As DateTime
    Private _FechaVencimiento As Nullable(Of DateTime)
    Private _fechaFin As Nullable(Of DateTime)
    Private _Monto As String
    Private _Mora As String
    Private _MontoTotal As Decimal
    Private _CodigoOficina As String
    Private _NumeroOperacion As String
    Private _Referencia As String
    Private _IdentificacionTerminal As String
    Private _MedioAtencion As String
    Private _HoraAtencion As String
    Private _NumeroCheque As String
    Private _CodigoBanco As String
    Private _CargoFijoPagado As String
    Private _Filler As String
    Private _Observacion As String
    Private _OperacionBancaria As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal filtro As String)
        Select Case filtro
            Case "RegistrarDetConciliacionBCP"
                IdDetConciliacionBCP = Convert.ToInt64(reader("IdDetConciliacionBCP"))
                Observacion = reader("Observacion")
        End Select
    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.IdDetConciliacionBCP = Convert.ToInt64(reader("IdDetConciliacionBCP"))
        Me.IdConciliacionArchivo = Convert.ToInt32(reader("IdConciliacionArchivo"))
        IdEstado = Convert.ToInt32(reader("IdEstado"))
        DescEstado = reader("DescEstado")
        Me.TipoRegistro = reader("TipoRegistro")
        Me.CodigoSucursal = reader("CodigoSucursal")
        Me.CodigoMoneda = reader("CodigoMoneda")
        Me.NumeroCuenta = reader("NumeroCuenta")
        Me.CodigoDepositante = reader("CodigoDepositante")
        Me.InformacionRetorno = reader("InformacionRetorno")
        Me.FechaConciliacion = reader("FechaConciliacion")
        FechaPago = Convert.ToDateTime(reader("FechaPago"))
        FechaVencimiento = DataUtil.ObjectToDateTimeNull(reader("FechaVencimiento"))
        Me.Monto = reader("Monto")
        Me.Mora = reader("Mora")
        Me.MontoTotal = Convert.ToDecimal(reader("MontoTotal"))
        Me.NumeroOperacion = reader("NumeroOperacion")
        Me.CodigoOficina = reader("CodigoOficina")
        Me.NumeroOperacion = reader("NumeroOperacion")
        Me.Referencia = reader("Referencia")
        Me.IdentificacionTerminal = reader("IdentificacionTerminal")
        Me.MedioAtencion = reader("MedioAtencion")
        Me.HoraAtencion = reader("HoraAtencion")
        Me.NumeroCheque = reader("NumeroCheque")
        Me.CodigoBanco = reader("CodigoBanco")
        Me.CargoFijoPagado = reader("CargoFijoPagado")
        Me.Filler = reader("Filler")
        Me.Observacion = reader("Observacion")
    End Sub
	<DataMember()> _
    Public Property IdDetConciliacionBCP() As Int64
        Get
            Return _IdDetConciliacionBCP
        End Get
        Set(ByVal value As Int64)
            _IdDetConciliacionBCP = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdConciliacionArchivo() As Integer
        Get
            Return _IdConciliacionArchivo
        End Get
        Set(ByVal value As Integer)
            _IdConciliacionArchivo = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescEstado() As String
        Get
            Return _descEstado
        End Get
        Set(ByVal value As String)
            _descEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property OperacionBancaria() As String
        Get
            Return _OperacionBancaria
        End Get
        Set(ByVal value As String)
            _OperacionBancaria = value
        End Set
    End Property
	<DataMember()> _
    Public Property TipoRegistro() As String
        Get
            Return _TipoRegistro
        End Get
        Set(ByVal value As String)
            _TipoRegistro = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoSucursal() As String
        Get
            Return _CodigoSucursal
        End Get
        Set(ByVal value As String)
            _CodigoSucursal = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoMoneda() As String
        Get
            Return _CodigoMoneda
        End Get
        Set(ByVal value As String)
            _CodigoMoneda = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroCuenta() As String
        Get
            Return _NumeroCuenta
        End Get
        Set(ByVal value As String)
            _NumeroCuenta = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoDepositante() As String
        Get
            Return _CodigoDepositante
        End Get
        Set(ByVal value As String)
            _CodigoDepositante = value
        End Set
    End Property
	<DataMember()> _
    Public Property InformacionRetorno() As String
        Get
            Return _InformacionRetorno
        End Get
        Set(ByVal value As String)
            _InformacionRetorno = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaConciliacion() As DateTime
        Get
            Return _FechaConciliacion
        End Get
        Set(ByVal value As DateTime)
            _FechaConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaPago() As DateTime
        Get
            Return _FechaPago
        End Get
        Set(ByVal value As DateTime)
            _FechaPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaVencimiento() As Nullable(Of DateTime)
        Get
            Return _FechaVencimiento
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaVencimiento = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaFin() As Nullable(Of DateTime)
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _fechaFin = value
        End Set
    End Property
	<DataMember()> _
    Public Property Monto() As String
        Get
            Return _Monto
        End Get
        Set(ByVal value As String)
            _Monto = value
        End Set
    End Property
	<DataMember()> _
    Public Property Mora() As String
        Get
            Return _Mora
        End Get
        Set(ByVal value As String)
            _Mora = value
        End Set
    End Property
	<DataMember()> _
    Public Property MontoTotal() As Decimal
        Get
            Return _MontoTotal
        End Get
        Set(ByVal value As Decimal)
            _MontoTotal = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoOficina() As String
        Get
            Return _CodigoOficina
        End Get
        Set(ByVal value As String)
            _CodigoOficina = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _NumeroOperacion
        End Get
        Set(ByVal value As String)
            _NumeroOperacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property Referencia() As String
        Get
            Return _Referencia
        End Get
        Set(ByVal value As String)
            _Referencia = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdentificacionTerminal() As String
        Get
            Return _IdentificacionTerminal
        End Get
        Set(ByVal value As String)
            _IdentificacionTerminal = value
        End Set
    End Property
	<DataMember()> _
    Public Property MedioAtencion() As String
        Get
            Return _MedioAtencion
        End Get
        Set(ByVal value As String)
            _MedioAtencion = value
        End Set
    End Property
	<DataMember()> _
    Public Property HoraAtencion() As String
        Get
            Return _HoraAtencion
        End Get
        Set(ByVal value As String)
            _HoraAtencion = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroCheque() As String
        Get
            Return _NumeroCheque
        End Get
        Set(ByVal value As String)
            _NumeroCheque = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoBanco() As String
        Get
            Return _CodigoBanco
        End Get
        Set(ByVal value As String)
            _CodigoBanco = value
        End Set
    End Property
	<DataMember()> _
    Public Property CargoFijoPagado() As String
        Get
            Return _CargoFijoPagado
        End Get
        Set(ByVal value As String)
            _CargoFijoPagado = value
        End Set
    End Property
	<DataMember()> _
    Public Property Filler() As String
        Get
            Return _Filler
        End Get
        Set(ByVal value As String)
            _Filler = value
        End Set
    End Property
	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
End Class
