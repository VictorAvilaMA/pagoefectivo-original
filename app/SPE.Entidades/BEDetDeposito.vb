﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BEDetDeposito
    Inherits BEAuditoria
    'Datos deposito
    Private _idDeposito As Int64
    Private _nroTransaccion As String
    Private _fechaDeposito As DateTime
    Private _montoDepositado As Decimal
    Private _nroCuenta As String
    Private _codigoOperacion As String
    Private _medioPago As String
    'Datos CIP
    Private _numeroOrdenPago As String
    Private _fechaCancelacion As DateTime
    Private _total As Decimal
    Private _servicioDescripcion As String
    Private _secuenciaServicio As String
    Private _codigoEmpresa As String 'para verificar el tipo de servicio INTERNO/EXTERNO
    Private _tipo As String
    Private _numeroOperacion As String

    'Datos para el reporte de Factura
    Private _nroFactura As String
    Private _montoFactura As String
    Private _fechaEmisionFactura As DateTime
    Private _comision As Decimal
    Private _idFactura As Int64

    'Datos TX
    Private _estadoTX As String
    Private _montoPE As Decimal
    Private _estadoFactura As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal reader As IDataReader)

    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipoConsulta As String)
        Select Case tipoConsulta
            Case "consultaDepositoReporte"
                IdDeposito = ObjectToInt64(reader("IdDeposito"))
                NroTransaccion = ObjectToString(reader("NroTransaccion"))
                FechaDeposito = ObjectToDateTime(reader("FechaDeposito"))
                MontoDepositado = ObjectToDecimal(reader("MontoDepositado"))
                NroCuenta = ObjectToString(reader("NroCuenta"))
                CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                MedioPago = ObjectToString(reader("ddocgrl"))

                NumeroOrdenPago = ObjectToInt64(reader("NumeroOrdenPago"))
                FechaCancelacion = ObjectToDateTime(reader("FechaCancelacion"))
                Total = ObjectToDecimal(reader("Total"))
                ServicioDescripcion = ObjectToString(reader("Nombre"))
                SecuenciaServicio = ObjectToString(reader("SecuenciaServicio"))
                CodigoEmpresa = ObjectToString(reader("Codigo"))
                NumeroOperacion = ObjectToString(reader("NumeroOperacion"))
                Comision = ObjectToDecimal(reader("Comision"))

            Case "consultaFacturaReporte"
                NroFactura = ObjectToString(reader("NroFactura"))
                MontoFactura = ObjectToDecimal(reader("MontoFactura"))
                FechaEmisionFactura = ObjectToDateTime(reader("FechaEmision"))

                IdDeposito = ObjectToInt64(reader("IdDeposito"))
                CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                FechaDeposito = ObjectToDateTime(reader("FechaDeposito"))
                MontoDepositado = ObjectToDecimal(reader("MontoDepositado"))
                NroCuenta = ObjectToString(reader("NroCuenta"))
                NroTransaccion = ObjectToString(reader("NroTransaccion"))

                NumeroOrdenPago = ObjectToString(reader("NumeroOrdenPago"))
                FechaCancelacion = ObjectToDateTime(reader("FechaCancelacion"))
                Total = ObjectToDecimal(reader("Total"))
                Comision = ObjectToDecimal(reader("Comision"))
                ServicioDescripcion = ObjectToString(reader("Nombre"))
                SecuenciaServicio = ObjectToString(reader("CodServicio"))
                CodigoEmpresa = ObjectToString(reader("Codigo"))
                
                IdFactura = ObjectToInt64(reader("IdFactura"))
                MontoPE = Total - Comision
            Case Else
        End Select
    End Sub

    <DataMember()> _
    Public Property IdDeposito() As Int64
        Get
            Return _idDeposito
        End Get
        Set(ByVal value As Int64)
            _idDeposito = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroTransaccion() As String
        Get
            Return _nroTransaccion
        End Get
        Set(ByVal value As String)
            _nroTransaccion = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaDeposito() As DateTime
        Get
            Return _fechaDeposito
        End Get
        Set(ByVal value As DateTime)
            _fechaDeposito = value
        End Set
    End Property
    <DataMember()> _
    Public Property MontoDepositado() As Decimal
        Get
            Return _montoDepositado
        End Get
        Set(ByVal value As Decimal)
            _montoDepositado = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroCuenta() As String
        Get
            Return _nroCuenta
        End Get
        Set(ByVal value As String)
            _nroCuenta = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoOperacion() As String
        Get
            Return _codigoOperacion
        End Get
        Set(ByVal value As String)
            _codigoOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property MedioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _fechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCancelacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _total
        End Get
        Set(ByVal value As Decimal)
            _total = value
        End Set
    End Property
    <DataMember()> _
    Public Property ServicioDescripcion() As String
        Get
            Return _servicioDescripcion
        End Get
        Set(ByVal value As String)
            _servicioDescripcion = value
        End Set
    End Property
    <DataMember()> _
    Public Property SecuenciaServicio() As String
        Get
            Return _secuenciaServicio
        End Get
        Set(ByVal value As String)
            _secuenciaServicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoEmpresa() As String
        Get
            Return _codigoEmpresa
        End Get
        Set(ByVal value As String)
            _codigoEmpresa = value
        End Set
    End Property
    <DataMember()> _
    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroFactura() As String
        Get
            Return _nroFactura
        End Get
        Set(ByVal value As String)
            _nroFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property MontoFactura() As Decimal
        Get
            Return _montoFactura
        End Get
        Set(ByVal value As Decimal)
            _montoFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEmisionFactura() As DateTime
        Get
            Return _fechaEmisionFactura
        End Get
        Set(ByVal value As DateTime)
            _fechaEmisionFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property Comision() As Decimal
        Get
            Return _comision
        End Get
        Set(ByVal value As Decimal)
            _comision = value
        End Set
    End Property
    <DataMember()>
    Public Property IdFactura() As Decimal
        Get
            Return _idFactura
        End Get
        Set(ByVal value As Decimal)
            _idFactura = value
        End Set
    End Property
    <DataMember()>
    Public Property EstadoTX() As String
        Get
            Return _estadoTX
        End Get
        Set(ByVal value As String)
            _estadoTX = value
        End Set
    End Property
    <DataMember()>
    Public Property MontoPE As Decimal
        Get
            Return _montoPE
        End Get
        Set(value As Decimal)
            _montoPE = value
        End Set
    End Property
    <DataMember()>
    Public Property EstadoFactura As String
        Get
            Return _estadoFactura
        End Get
        Set(value As String)
            _estadoFactura = value
        End Set
    End Property

End Class
