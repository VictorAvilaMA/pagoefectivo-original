﻿Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEDetMovimientoCuenta
    Inherits BEAuditoria
    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal operacion As String)
        Select Case operacion
            Case "PorID"
                Me.IdMovimientoCuentaMonedero = ObjectToInt64(reader("IdMovimientoCuentaMonedero"))
                Me.BancoDescripcion = ObjectToString(reader("BancoDescripcion"))
                Me.CuentaDineroVirtual = ObjectToString(reader("CuentaVirtualAfectada"))
                Me.FechaRegistro = ObjectToDateTime(reader("FechaRegistro"))
                Me.Monto = ObjectToInt(reader("Monto"))
                Me.ArchivoDescripcion = ObjectToString(reader("ArchivoDescripcion"))
                Me.TipoMovimientoDescripcion = ObjectToString(reader("TipoMovimientoDescripcion"))
        End Select
    End Sub

    'Agregado para Trazabilidad
    Private _idCuentaBanco As Integer
    <DataMember()> _
    Public Property IdCuentaBanco() As Integer
        Get
            Return _idCuentaBanco
        End Get
        Set(ByVal value As Integer)
            _idCuentaBanco = value
        End Set
    End Property
    Private _idMovimientoCuentaBanco As Int64
    <DataMember()> _
    Public Property IdMovimientoCuentaBanco() As Int64
        Get
            Return _idMovimientoCuentaBanco
        End Get
        Set(ByVal value As Int64)
            _idMovimientoCuentaBanco = value
        End Set
    End Property
    Private _idMovimientoCuentaBancoAsociado As Int64
    <DataMember()> _
    Public Property IdMovimientoCuentaBancoAsociado() As Int64
        Get
            Return _idMovimientoCuentaBancoAsociado
        End Get
        Set(ByVal value As Int64)
            _idMovimientoCuentaBancoAsociado = value
        End Set
    End Property
    Private _idMovimientoCuentaMonedero As Int64
    <DataMember()> _
    Public Property IdMovimientoCuentaMonedero() As Int64
        Get
            Return _idMovimientoCuentaMonedero
        End Get
        Set(ByVal value As Int64)
            _idMovimientoCuentaMonedero = value
        End Set
    End Property
    Private _descripcionBanco As String
    <DataMember()> _
    Public Property DescripcionBanco() As String
        Get
            Return _descripcionBanco
        End Get
        Set(ByVal value As String)
            _descripcionBanco = value
        End Set
    End Property
    Private _idBanco As Int64
    <DataMember()> _
    Public Property IdBanco() As String
        Get
            Return _idBanco
        End Get
        Set(ByVal value As String)
            _idBanco = value
        End Set
    End Property
    Private _fechaMovimientoDesde As DateTime
    <DataMember()> _
    Public Property FechaMovimientoDesde() As DateTime
        Get
            Return _fechaMovimientoDesde
        End Get
        Set(ByVal value As DateTime)
            _fechaMovimientoDesde = value
        End Set
    End Property
    Private _fechaMovimientoHasta As DateTime
    <DataMember()> _
    Public Property FechaMovimientoHasta() As DateTime
        Get
            Return _fechaMovimientoHasta
        End Get
        Set(ByVal value As DateTime)
            _fechaMovimientoHasta = value
        End Set
    End Property
    Private _tipoMovimientoDescripcion As String
    <DataMember()> _
    Public Property TipoMovimientoDescripcion() As String
        Get
            Return _tipoMovimientoDescripcion
        End Get
        Set(value As String)
            _tipoMovimientoDescripcion = value
        End Set
    End Property
    Private _archivoDescripcion As String
    <DataMember()> _
    Public Property ArchivoDescripcion() As String
        Get
            Return _archivoDescripcion
        End Get
        Set(value As String)
            _archivoDescripcion = value
        End Set
    End Property

    Private _bancoDescripcion
    <DataMember()> _
    Public Property BancoDescripcion() As String
        Get
            Return _bancoDescripcion
        End Get
        Set(value As String)
            _bancoDescripcion = value
        End Set
    End Property

    Private _cuentaDineroVirtual
    <DataMember()> _
    Public Property CuentaDineroVirtual() As String
        Get
            Return _cuentaDineroVirtual
        End Get
        Set(value As String)
            _cuentaDineroVirtual = value
        End Set
    End Property
    Private _fechaRegistro
    <DataMember()> _
    Public Property FechaRegistro() As DateTime
        Get
            Return _fechaRegistro
        End Get
        Set(value As DateTime)
            _fechaRegistro = value
        End Set
    End Property
    Private _monto
    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(value As Decimal)
            _monto = value
        End Set
    End Property
End Class
