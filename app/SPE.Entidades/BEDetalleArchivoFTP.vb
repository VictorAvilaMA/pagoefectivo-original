Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEDetalleArchivoFTP
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Private _idFtpArchivoDetalle As Integer
    Private _idFtpArchivo As Integer
    Private _idUsuario As String
    Private _ordenIdComercio As String
    Private _tipoPago As String
    Private _fechaPago As DateTime
    Private _idMoneda As Integer
    Private _moneda As String
    Private _monto As Decimal


    Private _idOrdenPago As Integer


    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        AsignarValores(reader)
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal opcion As String)
        If (opcion = "ParaGenerarArchivoFTP") Then
            AsignarValores(reader)
            IdOrdenPago = CInt(reader("IdOrdenPago").ToString())
        Else
            AsignarValores(reader)
        End If
    End Sub

    Public Sub AsignarValores(ByVal reader As IDataReader)
        IdFtpArchivoDetalle = CInt(reader("IdFtpArchivoDetalle").ToString())
        IdFtpArchivo = CInt(reader("IdFtpArchivo").ToString())
        IdUsuario = reader("IdUsuario").ToString()
        OrdenIdComercio = reader("OrdenIdComercio").ToString()
        TipoPago = reader("TipoDePago").ToString()
        FechaPago = _3Dev.FW.Util.DataUtil.ObjectToDateTime(reader("FechaPago"))
        IdMoneda = CInt(reader("IdMoneda").ToString())
        Moneda = reader("Moneda").ToString()
        Monto = CDec(reader("Monto").ToString())
    End Sub


	<DataMember()> _
    Public Property IdFtpArchivoDetalle() As Integer
        Get
            Return _idFtpArchivoDetalle
        End Get
        Set(ByVal value As Integer)
            _idFtpArchivoDetalle = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdFtpArchivo() As Integer
        Get
            Return _idFtpArchivo
        End Get
        Set(ByVal value As Integer)
            _idFtpArchivo = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdUsuario() As String
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As String)
            _idUsuario = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrdenIdComercio() As String
        Get
            Return _ordenIdComercio
        End Get
        Set(ByVal value As String)
            _ordenIdComercio = value
        End Set
    End Property

	<DataMember()> _
    Public Property TipoPago() As String
        Get
            Return _tipoPago
        End Get
        Set(ByVal value As String)
            _tipoPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaPago() As DateTime
        Get
            Return _fechaPago
        End Get
        Set(ByVal value As DateTime)
            _fechaPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

	<DataMember()> _
    Public Property Moneda() As String
        Get
            Return _moneda
        End Get
        Set(ByVal value As String)
            _moneda = value
        End Set
    End Property

	<DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idOrdenPago = value
        End Set
    End Property


    Public Class IdUsuarioComparer
        Implements IComparer(Of BEDetalleArchivoFTP)
        Public Function Compare(ByVal x As BEDetalleArchivoFTP, ByVal y As BEDetalleArchivoFTP) As Integer Implements System.Collections.Generic.IComparer(Of BEDetalleArchivoFTP).Compare
            Return CType(x, BEDetalleArchivoFTP).IdUsuario.CompareTo(CType(y, BEDetalleArchivoFTP).IdUsuario)
        End Function
    End Class

    Public Class OrdenIdComercioComparer
        Implements IComparer(Of BEDetalleArchivoFTP)
        Public Function Compare(ByVal x As BEDetalleArchivoFTP, ByVal y As BEDetalleArchivoFTP) As Integer Implements System.Collections.Generic.IComparer(Of BEDetalleArchivoFTP).Compare
            Return CType(x, BEDetalleArchivoFTP).OrdenIdComercio.CompareTo(CType(y, BEDetalleArchivoFTP).OrdenIdComercio)
        End Function
    End Class

    Public Class TipoPagoComparer
        Implements IComparer(Of BEDetalleArchivoFTP)
        Public Function Compare(ByVal x As BEDetalleArchivoFTP, ByVal y As BEDetalleArchivoFTP) As Integer Implements System.Collections.Generic.IComparer(Of BEDetalleArchivoFTP).Compare
            Return CType(x, BEDetalleArchivoFTP).TipoPago.CompareTo(CType(y, BEDetalleArchivoFTP).TipoPago)
        End Function
    End Class

    Public Class FechaPagoComparer
        Implements IComparer(Of BEDetalleArchivoFTP)
        Public Function Compare(ByVal x As BEDetalleArchivoFTP, ByVal y As BEDetalleArchivoFTP) As Integer Implements System.Collections.Generic.IComparer(Of BEDetalleArchivoFTP).Compare
            Return CType(x, BEDetalleArchivoFTP).FechaPago.CompareTo(CType(y, BEDetalleArchivoFTP).FechaPago)
        End Function
    End Class

    Public Class MonedaComparer
        Implements IComparer(Of BEDetalleArchivoFTP)
        Public Function Compare(ByVal x As BEDetalleArchivoFTP, ByVal y As BEDetalleArchivoFTP) As Integer Implements System.Collections.Generic.IComparer(Of BEDetalleArchivoFTP).Compare
            Return CType(x, BEDetalleArchivoFTP).Moneda.CompareTo(CType(y, BEDetalleArchivoFTP).Moneda)
        End Function
    End Class

    Public Class MontoComparer
        Implements IComparer(Of BEDetalleArchivoFTP)
        Public Function Compare(ByVal x As BEDetalleArchivoFTP, ByVal y As BEDetalleArchivoFTP) As Integer Implements System.Collections.Generic.IComparer(Of BEDetalleArchivoFTP).Compare
            Return CType(x, BEDetalleArchivoFTP).Monto.CompareTo(CType(y, BEDetalleArchivoFTP).Monto)
        End Function
    End Class


End Class
