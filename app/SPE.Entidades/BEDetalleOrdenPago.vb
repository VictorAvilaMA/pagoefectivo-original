Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEDetalleOrdenPago

    Private _conceptoPago As String
    Private _idEstado As Integer
    Private _idOrdenPago As Integer
    Private _idDetalleOrdenPago As Integer
    Private _importe As Decimal

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdDetalleOrdenPago = Convert.ToInt32(reader("IdDetalleOrdenPago"))

        Me.IdOrdenPago = Convert.ToInt32(reader("IdOrdenPago"))
        Me.ConceptoPago = Convert.ToString(reader("ConceptoPago"))
        Me.Importe = Convert.ToDecimal(reader("Importe"))
    End Sub
    

	<DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idOrdenPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdDetalleOrdenPago() As Integer
        Get
            Return _idDetalleOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idDetalleOrdenPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property Importe() As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal value As Decimal)
            _importe = value
        End Set
    End Property
    Private _codOrigen As String
	<DataMember()> _
    Public Property codOrigen() As String
        Get
            Return _codOrigen
        End Get
        Set(ByVal value As String)
            _codOrigen = value
        End Set
    End Property
    Private _tipoOrigen As String
	<DataMember()> _
    Public Property tipoOrigen() As String
        Get
            Return _tipoOrigen
        End Get
        Set(ByVal value As String)
            _tipoOrigen = value
        End Set
    End Property
    Private _campo1 As String
	<DataMember()> _
    Public Property campo1() As String
        Get
            Return _campo1
        End Get
        Set(ByVal value As String)
            _campo1 = value
        End Set
    End Property
    Private _campo2 As String
	<DataMember()> _
    Public Property campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property
    Private _campo3 As String
	<DataMember()> _
    Public Property campo3() As String
        Get
            Return _campo3
        End Get
        Set(ByVal value As String)
            _campo3 = value
        End Set
    End Property


End Class
