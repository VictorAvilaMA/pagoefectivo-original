﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEDetalleSolicitudPago

    Private _idSolicitudPago As Integer
    Private _conceptoPago As String
    Private _importe As Decimal
    Private _tipoOrigen As String
    Private _codOrigen As String    
    Private _campo1 As String
    Private _campo2 As String
    Private _campo3 As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdSolicitudPago = Convert.ToInt32(reader("IdSolicitudPago"))
        Me.ConceptoPago = Convert.ToString(reader("ConceptoPago"))
        Me.Importe = Convert.ToDecimal(reader("Importe"))
        Me.tipoOrigen = Convert.ToString(reader("TipoOrigen"))
        Me.codOrigen = Convert.ToString(reader("CodOrigen"))
        Me.campo1 = Convert.ToString(reader("Campo1"))
        Me.campo2 = Convert.ToString(reader("Campo2"))
        Me.campo3 = Convert.ToString(reader("Campo3"))
    End Sub


    <DataMember()> _
    Public Property IdSolicitudPago() As Integer
        Get
            Return _idSolicitudPago
        End Get
        Set(ByVal value As Integer)
            _idSolicitudPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property Importe() As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal value As Decimal)
            _importe = value
        End Set
    End Property
    <DataMember()> _
    Public Property codOrigen() As String
        Get
            Return _codOrigen
        End Get
        Set(ByVal value As String)
            _codOrigen = value
        End Set
    End Property
    <DataMember()> _
    Public Property tipoOrigen() As String
        Get
            Return _tipoOrigen
        End Get
        Set(ByVal value As String)
            _tipoOrigen = value
        End Set
    End Property

    <DataMember()> _
    Public Property campo1() As String
        Get
            Return _campo1
        End Get
        Set(ByVal value As String)
            _campo1 = value
        End Set
    End Property
    <DataMember()> _
    Public Property campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property

    <DataMember()> _
    Public Property campo3() As String
        Get
            Return _campo3
        End Get
        Set(ByVal value As String)
            _campo3 = value
        End Set
    End Property


End Class
