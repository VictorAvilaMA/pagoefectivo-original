Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEDetalleTarjeta

    Private _bin As String
    Private _fechaVencimiento As DateTime
    Private _idDetalleTarjeta As Integer
    Private _idMovimiento As Integer
    Private _idTarjeta As Integer
    Private _idTipoTarjeta As Integer
    Private _lote As String
    Private _numero As String

	<DataMember()> _
    Public Property IdDetalleTarjeta() As Integer
        Get
            Return _idDetalleTarjeta
        End Get
        Set(ByVal value As Integer)
            _idDetalleTarjeta = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdTarjeta() As Integer
        Get
            Return _idTarjeta
        End Get
        Set(ByVal value As Integer)
            _idTarjeta = value
        End Set
    End Property

	<DataMember()> _
    Public Property Numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaVencimiento() As DateTime
        Get
            Return _fechaVencimiento
        End Get
        Set(ByVal value As DateTime)
            _fechaVencimiento = value
        End Set
    End Property

	<DataMember()> _
    Public Property Lote() As String
        Get
            Return _lote
        End Get
        Set(ByVal value As String)
            _lote = value
        End Set
    End Property

	<DataMember()> _
    Public Property Bin() As String
        Get
            Return _bin
        End Get
        Set(ByVal value As String)
            _bin = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdMovimiento() As Integer
        Get
            Return _idMovimiento
        End Get
        Set(ByVal value As Integer)
            _idMovimiento = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdTipoTarjeta() As Integer
        Get
            Return _idTipoTarjeta
        End Get
        Set(ByVal value As Integer)
            _idTipoTarjeta = value
        End Set
    End Property

End Class
