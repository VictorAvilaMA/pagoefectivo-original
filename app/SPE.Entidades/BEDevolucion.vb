﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> Public Class BEDevolucion
    Inherits BEAuditoria

    Private _IdDevolucion As Int64
    Private _NroCIP As String
    Private _ClienteNroDNI As String
    Private _ClienteNombres As String
    Private _ClienteApellidos As String
    Private _ClienteDireccion As String
    Private _IdEstadoCliente As Integer
    Private _IdEstadoAdmin As Integer
    Private _Observaciones As String

    Private _IdServicio As Integer
    Private _IdEmpresaContratante As Integer
    Private _DescripcionEstadoCliente As String
    Private _DescripcionEstadoAdmin As String
    Private _IdOrdenPago As Int64
    Private _Monto As Decimal
    Private _EmailRepresentante As String
    Private _SimboloMoneda As String

    Private _FechaInicio As DateTime
    Private _FechaFinal As DateTime
    Private _ArchivoDevolucion As Byte()
    Private _NombreArchivoDevolucion As String
    Private _ExtensionArchivoDevolucion As String
    Private _MensajeArchivoDevolucion As String
    Private _ServicioDescripcion As String
    Private _EstadoCipDescripcion As String
    Private _ConceptoPago As String
    Private _FechaEmision As DateTime
    Private _FechaCancelacion As DateTime
    Private _FlagEnProceso As Boolean
    Private _FechaEnProceso As DateTime
    Private _FechaChequeGenerado As DateTime
    Private _FechaEmitido As DateTime
    Private _FechaExpirado As DateTime
    Private _EstadoDevolucionDescripcion As String
    Private _NombreCompletoCliente As String
    Private _IdRetorno As Int32
    Private _MensajeRetorno As String
    Private _CodigoError As Int32


    Public Sub New()

    End Sub

    Public Sub New(reader As IDataReader, ByVal tipoConsulta As String)

        'Select Case tipoConsulta
        '    Case "ConsultaGeneral"
        _IdDevolucion = DataUtil.ObjectToInt64(reader("IdDevolucion"))
        _NroCIP = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
        _ClienteNroDNI = DataUtil.ObjectToString(reader("ClienteDNI"))
        _ClienteNombres = DataUtil.ObjectToString(reader("ClienteNombre"))
        _ClienteApellidos = DataUtil.ObjectToString(reader("ClienteApellido"))
        _DescripcionEstadoCliente = DataUtil.ObjectToString(reader("DescripcionEstadoCliente"))
        _DescripcionEstadoAdmin = DataUtil.ObjectToString(reader("DescripcionEstadoAdmin"))
        _ClienteDireccion = DataUtil.ObjectToString(reader("ClienteDireccion"))
        _IdEstadoCliente = DataUtil.ObjectToInt32(reader("IdEstadoCliente"))
        _IdEstadoAdmin = DataUtil.ObjectToInt32(reader("IdEstadoAdmin"))
        _Monto = DataUtil.ObjectToDecimal(reader("Total"))
        _Observaciones = DataUtil.ObjectToString(reader("Observaciones"))
        _EmailRepresentante = DataUtil.ObjectToString(reader("EmailRepresentante"))
        _SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
        FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
        TotalPageNumbers = DataUtil.ObjectToInt32(reader("TOTAL_RESULT"))
        EstadoCipDescripcion = DataUtil.ObjectToString(reader("EstadoCip"))
        ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
        ServicioDescripcion = DataUtil.ObjectToString(reader("ServicioDescripcion"))
        NombreArchivoDevolucion = DataUtil.ObjectToString(reader("NombreArchivo"))
        FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
        FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
        FlagEnProceso = DataUtil.ObjectToBool(reader("FlagEnProceso"))
        FechaEnProceso = DataUtil.ObjectToDateTime(reader("FechaEnProceso"))
        FechaChequeGenerado = DataUtil.ObjectToDateTime(reader("FechaChequeGenerado"))
        FechaEmitido = DataUtil.ObjectToDateTime(reader("FechaEmitido"))
        FechaExpirado = DataUtil.ObjectToDateTime(reader("FechaExpirado"))
        NombreCompletoCliente = DataUtil.ObjectToString(reader("NombreCompletoCliente"))
        'End Select
    End Sub

    <DataMember()> _
    Public Property IdRetorno() As Int32
        Get
            Return _IdRetorno
        End Get
        Set(ByVal value As Int32)
            _IdRetorno = value
        End Set
    End Property
    <DataMember()> _
    Public Property MensajeRetorno() As String
        Get
            Return _MensajeRetorno
        End Get
        Set(ByVal value As String)
            _MensajeRetorno = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoError() As Int32
        Get
            Return _CodigoError
        End Get
        Set(ByVal value As Int32)
            _CodigoError = value
        End Set
    End Property

    <DataMember()> _
Public Property NombreCompletoCliente() As String
        Get
            Return _NombreCompletoCliente
        End Get
        Set(ByVal value As String)
            _NombreCompletoCliente = value
        End Set
    End Property
    <DataMember()> _
    Public Property EstadoDevolucionDescripcion() As String
        Get
            Return _EstadoDevolucionDescripcion
        End Get
        Set(ByVal value As String)
            _EstadoDevolucionDescripcion = value
        End Set
    End Property

    <DataMember()> _
Public Property FechaEnProceso() As DateTime
        Get
            Return _FechaEnProceso
        End Get
        Set(ByVal value As DateTime)
            _FechaEnProceso = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEmitido() As DateTime
        Get
            Return _FechaEmitido
        End Get
        Set(ByVal value As DateTime)
            _FechaEmitido = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaExpirado() As DateTime
        Get
            Return _FechaExpirado
        End Get
        Set(ByVal value As DateTime)
            _FechaExpirado = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaChequeGenerado() As DateTime
        Get
            Return _FechaChequeGenerado
        End Get
        Set(ByVal value As DateTime)
            _FechaChequeGenerado = value
        End Set
    End Property
    <DataMember()> _
Public Property FlagEnProceso() As Boolean
        Get
            Return _FlagEnProceso
        End Get
        Set(ByVal value As Boolean)
            _FlagEnProceso = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCancelacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdDevolucion() As Int64
        Get
            Return _IdDevolucion
        End Get
        Set(ByVal value As Int64)
            _IdDevolucion = value
        End Set
    End Property
    <DataMember()> _
    Public Property EstadoCipDescripcion() As String
        Get
            Return _EstadoCipDescripcion
        End Get
        Set(ByVal value As String)
            _EstadoCipDescripcion = value
        End Set
    End Property
    <DataMember()> _
    Public Property ServicioDescripcion() As String
        Get
            Return _ServicioDescripcion
        End Get
        Set(ByVal value As String)
            _ServicioDescripcion = value
        End Set
    End Property
    <DataMember()> _
    Public Property MensajeArchivoDevolucion() As String
        Get
            Return _MensajeArchivoDevolucion
        End Get
        Set(ByVal value As String)
            _MensajeArchivoDevolucion = value
        End Set
    End Property
    <DataMember()> _
    Public Property NombreArchivoDevolucion() As String
        Get
            Return _NombreArchivoDevolucion
        End Get
        Set(ByVal value As String)
            _NombreArchivoDevolucion = value
        End Set
    End Property
    <DataMember()> _
    Public Property ExtensionArchivoDevolucion() As String
        Get
            Return _ExtensionArchivoDevolucion
        End Get
        Set(ByVal value As String)
            _ExtensionArchivoDevolucion = value
        End Set
    End Property
    <DataMember()> _
    Public Property ArchivoDevolucion() As Byte()
        Get
            Return _ArchivoDevolucion
        End Get
        Set(ByVal value As Byte())
            _ArchivoDevolucion = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroCIP() As String
        Get
            Return _NroCIP
        End Get
        Set(ByVal value As String)
            _NroCIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property ClienteNroDNI() As String
        Get
            Return _ClienteNroDNI
        End Get
        Set(ByVal value As String)
            _ClienteNroDNI = value
        End Set
    End Property
    <DataMember()> _
    Public Property ClienteNombres() As String
        Get
            Return _ClienteNombres
        End Get
        Set(ByVal value As String)
            _ClienteNombres = value
        End Set
    End Property
    <DataMember()> _
    Public Property ClienteApellidos() As String
        Get
            Return _ClienteApellidos
        End Get
        Set(ByVal value As String)
            _ClienteApellidos = value
        End Set
    End Property
    <DataMember()> _
    Public Property ClienteDireccion() As String
        Get
            Return _ClienteDireccion
        End Get
        Set(ByVal value As String)
            _ClienteDireccion = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEstadoCliente() As Integer
        Get
            Return _IdEstadoCliente
        End Get
        Set(ByVal value As Integer)
            _IdEstadoCliente = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEstadoAdmin() As Integer
        Get
            Return _IdEstadoAdmin
        End Get
        Set(ByVal value As Integer)
            _IdEstadoAdmin = value
        End Set
    End Property
    <DataMember()> _
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionEstadoCliente() As String
        Get
            Return _DescripcionEstadoCliente
        End Get
        Set(ByVal value As String)
            _DescripcionEstadoCliente = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionEstadoAdmin() As String
        Get
            Return _DescripcionEstadoAdmin
        End Get
        Set(ByVal value As String)
            _DescripcionEstadoAdmin = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdOrdenPago() As Int64
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Int64)
            _IdOrdenPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _Monto
        End Get
        Set(ByVal value As Decimal)
            _Monto = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEmpresaContratante() As Integer
        Get
            Return _IdEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _IdEmpresaContratante = value
        End Set
    End Property
    <DataMember()> _
    Public Property EmailRepresentante() As String
        Get
            Return _EmailRepresentante
        End Get
        Set(ByVal value As String)
            _EmailRepresentante = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As DateTime)
            _FechaInicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaFinal() As DateTime
        Get
            Return _FechaFinal
        End Get
        Set(ByVal value As DateTime)
            _FechaFinal = value
        End Set
    End Property
    <DataMember()> _
    Public Property SimboloMoneda() As String
        Get
            Return _SimboloMoneda
        End Get
        Set(ByVal value As String)
            _SimboloMoneda = value
        End Set
    End Property

    Public Sub New(reader As IDataReader, ByVal Valor1 As Int32)
        IdRetorno = DataUtil.ObjectToInt32(reader("IdRetorno"))
        MensajeRetorno = DataUtil.ObjectToString(reader("MensajeRetorno"))
        CodigoError = DataUtil.ObjectToString(reader("CodigoError"))
    End Sub
End Class
