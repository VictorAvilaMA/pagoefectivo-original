Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEEmpresaContratante
    Inherits BEAuditoria
    Implements SPE.Entidades.IBEUbigeo

    Private _contacto As String
    Private _direccion As String
    Private _email As String
    Private _idEstado As Integer
    Private _fax As String
    Private _idEmpresaContratante As Integer
    Private _razonSocial As String
    Private _ruc As String
    Private _sitioWeb As String
    Private _telefono As String
    Private _imagenLogo As Byte()
    Private _idCiudad As Integer
    Private _idDepartamento As Integer
    Private _idPais As Integer
    Private _DescripcionEstado As String
    Private _OcultarEmpresa As Boolean
    Private _descripcionOrigenCancelacion As String

    'liquidacionLote
    Private _horaCorte As String
    Private _idPeriodoLiquidacion As Integer
    Private _PeriodoLiquidacion As String
    Private _TipoComision As String
    Private _Nombre As String
    'Private _razonSocial As String
    Private _MonedaOP As String
    Private _TotalOP As Decimal
    Private _TotalPago As Decimal
    Private _TotalComision As Decimal
    Private _MontoDepositar As Decimal
    'Private _IdEmpresaContratante As Integer
    Private _IdServicio As Integer
    Private _CantOP As Integer







    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        ActCampos(reader)
        Me.AsignarAuditoria(reader)
    End Sub
    Private Sub ActCampos(ByVal reader As IDataReader)
        Me.Contacto = ObjectToString(reader("Contacto"))
        Me.Direccion = ObjectToString(reader("Direccion"))
        Me.Email = ObjectToString(reader("Email"))
        Me.IdEstado = ObjectToInt32(reader("IdEstado"))
        Me.Fax = ObjectToString(reader("Fax"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        Me.IdEmpresaContratante = ObjectToInt32(reader("IdEmpresaContratante"))
        Me.IdUsuarioActualizacion = ObjectToInt32(reader("IdUsuarioActualizacion"))
        Me.IdUsuarioCreacion = ObjectToInt32(reader("IdUsuarioCreacion"))
        Me.RazonSocial = ObjectToString(reader("RazonSocial"))
        Me.RUC = ObjectToString(reader("RUC"))
        Me.SitioWeb = ObjectToString(reader("SitioWeb"))
        Me.Telefono = ObjectToString(reader("Telefono"))
        Me.IdCiudad = ObjectToInt32(reader("IdCiudad"))
        Me.IdDepartamento = ObjectToInt32(reader("IdDepartamento"))
        Me.IdPais = ObjectToInt32(reader("IdPais"))
        Me.ImagenLogo = reader("ImagenLogo")
        Me.OcultarEmpresa = _3Dev.FW.Util.DataUtil.StringToBool(reader("OcultarEmpresa"))
        Me._codigo = ObjectToString(reader, "Codigo")
        Me.HoraCorte = ObjectToString(reader, "HoraCorte")
        Me.IdPeriodoLiquidacion = ObjectToInt32(reader("IdPeriodoLiquidacion"))

    End Sub


    Private Sub ActCamposLiqLot(ByVal reader As IDataReader)
        'Me.HoraCorte = ObjectToString(reader("Contacto"))
        Me.PeriodoLiquidacion = ObjectToString(reader("PeriodoLiquidacion"))
        Me.TipoComision = ObjectToString(reader("TipoComision"))
        'Me.Nombre = ObjectToString(reader("Nombre"))
        Me.RazonSocial = ObjectToString(reader("RazonSocial"))
        Me.MonedaOP = ObjectToString(reader("MonedaOP"))
        Me.TotalOP = ObjectToDecimal(reader("TotalOP"))
        Me.TotalPago = ObjectToDecimal(reader("TotalPago"))
        Me.TotalComision = ObjectToDecimal(reader("TotalComision"))
        Me.MontoDepositar = ObjectToInt32(reader("MontoDepositar"))
        Me.IdEmpresaContratante = ObjectToInt32(reader("IdEmpresaContratante"))
        'Me.IdServicio = ObjectToInt32(reader("IdServicio"))
        Me.CantOP = ObjectToInt32(reader("CantOP"))
        

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)

        Select Case tipo
            Case "busq"
                ActCampos(reader)
                _DescripcionEstado = ObjectToString(reader("DescripcionEstado"))
            Case "porID"
                ActCampos(reader)
                Me.AsignarAuditoria(reader)
            Case "LiqLot"
                ActCamposLiqLot(reader)
        End Select
    End Sub
	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _DescripcionEstado
        End Get
        Set(ByVal value As String)
            _DescripcionEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEmpresaContratante() As Integer
        Get
            Return _idEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _idEmpresaContratante = value
        End Set
    End Property

	<DataMember()> _
    Public Property RazonSocial() As String
        Get
            Return _razonSocial
        End Get
        Set(ByVal value As String)
            _razonSocial = value
        End Set
    End Property

	<DataMember()> _
    Public Property RUC() As String
        Get
            Return _ruc
        End Get
        Set(ByVal value As String)
            _ruc = value
        End Set
    End Property

	<DataMember()> _
    Public Property Contacto() As String
        Get
            Return _contacto
        End Get
        Set(ByVal value As String)
            _contacto = value
        End Set
    End Property

	<DataMember()> _
    Public Property Telefono() As String
        Get
            Return _telefono
        End Get
        Set(ByVal value As String)
            _telefono = value
        End Set
    End Property

	<DataMember()> _
    Public Property Fax() As String
        Get
            Return _fax
        End Get
        Set(ByVal value As String)
            _fax = value
        End Set
    End Property

	<DataMember()> _
    Public Property Direccion() As String
        Get
            Return _direccion
        End Get
        Set(ByVal value As String)
            _direccion = value
        End Set
    End Property

	<DataMember()> _
    Public Property SitioWeb() As String
        Get
            Return _sitioWeb
        End Get
        Set(ByVal value As String)
            _sitioWeb = value
        End Set
    End Property

	<DataMember()> _
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Int32
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Int32)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property ImagenLogo() As Byte()
        Get
            Return _imagenLogo
        End Get
        Set(ByVal value As Byte())
            _imagenLogo = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdCiudad() As Int32 Implements IBEUbigeo.IdCiudad
        Get
            Return _idCiudad
        End Get
        Set(ByVal value As Int32)
            _idCiudad = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdDepartamento() As Int32 Implements IBEUbigeo.IdDepartamento
        Get
            Return _idDepartamento
        End Get
        Set(ByVal value As Int32)
            _idDepartamento = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdPais() As Int32 Implements IBEUbigeo.IdPais
        Get
            Return _idPais
        End Get
        Set(ByVal value As Int32)
            _idPais = value
        End Set
    End Property


	<DataMember()> _
    Public Property OcultarEmpresa() As Boolean
        Get
            Return _OcultarEmpresa
        End Get
        Set(ByVal value As Boolean)
            _OcultarEmpresa = value
        End Set
    End Property



    Private _codigo As String
	<DataMember()> _
    Public Property Codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property


    <DataMember()> _
    Public Property DescripcionOrigenCancelacion() As String
        Get
            Return _descripcionOrigenCancelacion
        End Get
        Set(ByVal value As String)
            _descripcionOrigenCancelacion = value
        End Set
    End Property



    'liquidacionLote

    <DataMember()> _
Public Property HoraCorte() As String
        Get
            Return _horaCorte
        End Get
        Set(ByVal value As String)
            _horaCorte = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdPeriodoLiquidacion() As Integer
        Get
            Return _idPeriodoLiquidacion
        End Get
        Set(ByVal value As Integer)
            _idPeriodoLiquidacion = value
        End Set
    End Property

    
    'liquidacionLote
    <DataMember()> _
    Public Property PeriodoLiquidacion() As String
        Get
            Return _PeriodoLiquidacion
        End Get
        Set(ByVal value As String)
            _PeriodoLiquidacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property TipoComision() As String
        Get
            Return _TipoComision
        End Get
        Set(ByVal value As String)
            _TipoComision = value
        End Set
    End Property


    <DataMember()> _
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    <DataMember()> _
    Public Property MonedaOP() As String
        Get
            Return _MonedaOP
        End Get
        Set(ByVal value As String)
            _MonedaOP = value
        End Set
    End Property




    <DataMember()> _
    Public Property TotalOP() As Decimal
        Get
            Return _TotalOP
        End Get
        Set(ByVal value As Decimal)
            _TotalOP = value
        End Set
    End Property

    <DataMember()> _
    Public Property TotalPago() As Decimal
        Get
            Return _TotalPago
        End Get
        Set(ByVal value As Decimal)
            _TotalPago = value
        End Set
    End Property


    <DataMember()> _
    Public Property TotalComision() As Decimal
        Get
            Return _TotalComision
        End Get
        Set(ByVal value As Decimal)
            _TotalComision = value
        End Set
    End Property


    <DataMember()> _
    Public Property MontoDepositar() As Decimal
        Get
            Return _MontoDepositar
        End Get
        Set(ByVal value As Decimal)
            _MontoDepositar = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantOP() As Integer
        Get
            Return _CantOP
        End Get
        Set(ByVal value As Integer)
            _CantOP = value
        End Set
    End Property



    Public Class CodigoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).Codigo.CompareTo(CType(y, BEEmpresaContratante).Codigo)
        End Function
    End Class

    Public Class RazonSocialComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).RazonSocial.CompareTo(CType(y, BEEmpresaContratante).RazonSocial)
        End Function
    End Class
    Public Class ContactoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).Contacto.CompareTo(CType(y, BEEmpresaContratante).Contacto)
        End Function
    End Class
    Public Class RucComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).RUC.CompareTo(CType(y, BEEmpresaContratante).RUC)
        End Function
    End Class
    Public Class TelefonoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).Telefono.CompareTo(CType(y, BEEmpresaContratante).Telefono)
        End Function
    End Class
    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).DescripcionEstado.CompareTo(CType(y, BEEmpresaContratante).DescripcionEstado)
        End Function
    End Class
    Public Class DescripcionOrigenCancelacionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEmpresaContratante).DescripcionOrigenCancelacion.CompareTo(CType(y, BEEmpresaContratante).DescripcionOrigenCancelacion)
        End Function
    End Class

   
End Class
