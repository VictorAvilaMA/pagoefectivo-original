﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Public Class BEEmpresaPeriodo
    Inherits BEAuditoria
    Private _idEmpresaPeriodo As Integer
    Private _idEmpresa As Integer
    Private _idTipoPeriodo As Integer
    Private _valorMensual As Integer
    Private _valorSemanal As String
    Sub New()
    End Sub

    Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        Select Case tipo
            Case "porIDEmpresa"
                IdEmpresaPeriodo = DataUtil.ObjectToInt(reader("IdEmpresaPeriodo"))
                IdEmpresa = DataUtil.ObjectToInt(reader("IdEmpresa"))
                IdTipoPeriodo = DataUtil.ObjectToInt(reader("IdTipoPeriodo"))
                ValorMensual = DataUtil.ObjectToInt(reader("ValorMensual"))
                ValorSemanal = DataUtil.ObjectToString(reader("ValorSemanal"))
        End Select
    End Sub
    <DataMember()> _
    Public Property IdEmpresaPeriodo() As Integer
        Get
            Return _idEmpresaPeriodo
        End Get
        Set(ByVal value As Integer)
            _idEmpresaPeriodo = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEmpresa() As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdTipoPeriodo() As Integer
        Get
            Return _idTipoPeriodo
        End Get
        Set(ByVal value As Integer)
            _idTipoPeriodo = value
        End Set
    End Property
    <DataMember()> _
    Public Property ValorMensual() As Integer
        Get
            Return _valorMensual
        End Get
        Set(ByVal value As Integer)
            _valorMensual = value
        End Set
    End Property
    <DataMember()> _
    Public Property ValorSemanal() As String
        Get
            Return _valorSemanal
        End Get
        Set(ByVal value As String)
            _valorSemanal = value
        End Set
    End Property
End Class
