﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades

<Serializable()> <DataContract()> Public Class BEEquipoRegistradoXusuario
    Inherits BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        'Constructor
        '_idusuario = ObjectToString(reader("IdUsuario"))
        _idEquipoRegistrado = ObjectToInt64(reader("IdEquipoRegistrado"))
        _pcName = ObjectToString(reader("PcName"))
        _fechaRegistro = ObjectToDateTime(reader("FechaRegistro"))
        TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal segun As Integer)
        'Constructor 2
        _flagRegistarPc = ObjectToBool(reader("FlagRegistrarPc"))
        _resultado = ObjectToBool(reader("Flag"))
    End Sub

    Private _idEquipoRegistrado As Int64
    <DataMember()> _
    Public Property IdEquipoRegistrado() As Int64
        Get
            Return _idEquipoRegistrado
        End Get
        Set(ByVal value As Int64)
            _idEquipoRegistrado = value
        End Set
    End Property

    Private _idusuario As Integer
    <DataMember()> _
    Public Property IdUsuario() As Integer
        Get
            Return _idusuario
        End Get
        Set(ByVal value As Integer)
            _idusuario = value
        End Set
    End Property

    Private _usuario As String
    <DataMember()> _
    Public Property Usuario() As String
        Get
            Return _usuario
        End Get
        Set(ByVal value As String)
            _usuario = value
        End Set
    End Property

    Private _token As String
    <DataMember()> _
    Public Property Token() As String
        Get
            Return _token
        End Get
        Set(ByVal value As String)
            _token = value
        End Set
    End Property

    Private _pcName As String
    <DataMember()> _
    Public Property PcName() As String
        Get
            Return _pcName
        End Get
        Set(ByVal value As String)
            _pcName = value
        End Set
    End Property

    Private _resultado As Boolean
    <DataMember()> _
    Public Property Resultado() As Boolean
        Get
            Return _resultado
        End Get
        Set(ByVal value As Boolean)
            _resultado = value
        End Set
    End Property

    Private _fechaRegistro As DateTime
    <DataMember()> _
    Public Property FechaRegistro() As DateTime
        Get
            Return _fechaRegistro
        End Get
        Set(ByVal value As DateTime)
            _fechaRegistro = value
        End Set
    End Property

    Private _flagRegistarPc As Boolean
    <DataMember()> _
    Public Property FlagRegistarPc() As Boolean
        Get
            Return _flagRegistarPc
        End Get
        Set(ByVal value As Boolean)
            _flagRegistarPc = value
        End Set
    End Property

End Class
