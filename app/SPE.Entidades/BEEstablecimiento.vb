Imports System.Runtime.Serialization
Imports System.Collections.Generic
Imports System
Imports _3Dev.FW.Entidades.Seguridad


<Serializable()> <DataContract()> Public Class BEEstablecimiento
    Inherits BEAuditoria

    Private _idEstablecimiento As Integer
    Private _serieTerminal As String
    Private _Descripcion As String
    Private _idEstado As Integer
    Private _idComercio As Integer
    Private _DescripcionComercio As String
    Private _idOperador As Integer
    Private _descripcionEstado As String
    Private _medioPago As String
    Private _montoTotal As Decimal
    Private _idAperturaOrigen As Integer
    Private _FechaApertura As DateTime
    Private _FechaCierre As DateTime
    Private _NumOperaciones As Integer
    Private _RazonSocial As String
    Private _CodigoComercio As String



    Private _Moneda As String

    Public Sub New()

    End Sub

    Private Sub ActCampos(ByVal reader As IDataReader)
        Me.IdEstablecimiento = Convert.ToInt32(reader("IdEstablecimiento"))
        Me.SerieTerminal = Convert.ToString(reader("SerieTerminal"))
        Me.IdOperador = Convert.ToInt32(reader("IdOperador"))
        Me.IdEstado = Convert.ToInt32(reader("IdEstado"))
        Me.DescripcionEstado = Convert.ToString(reader("DescripcionEstado"))
        Me.Descripcion = Convert.ToString(reader("Descripcion"))
        Me.DescripcionComercio = Convert.ToString(reader("RazonSocial"))

    End Sub


    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        If (tipo = "busq") Then
            ActCampos(reader)
            Me.DescripcionComercio = Convert.ToString(reader("RazonSocial"))
            Me.CodigoComercio = Convert.ToInt32(reader("idComercio"))
        ElseIf (tipo = "busqModificar") Then
            ActCampos(reader)
            Me.idComercio = Convert.ToString(reader("idComercio"))
        ElseIf (tipo = "busqPendientes") Then
            Me.SerieTerminal = reader("SerieTerminal")
            Me.medioPago = reader("MedioPago")
            Me.montoTotal = reader("MontoTotal")
            Me.moneda = reader("Moneda")
        ElseIf (tipo = "busqPendientesEstablecimiento") Then
            Me.IdEstablecimiento = Convert.ToInt32(reader("idEstablecimiento"))
            Me.SerieTerminal = Convert.ToString(reader("SerieTerminal"))
            Me.idAperturaOrigen = Convert.ToInt32(reader("idAperturaOrigen"))
            Me.FechaApertura = Convert.ToDateTime(reader("FechaApertura"))
            Me.FechaCierre = Convert.ToDateTime(reader("FechaCierre"))
            Me.NumOperaciones = Convert.ToInt32(reader("NumOperaciones"))
            Me.RazonSocial = Convert.ToString(reader("RazonSocial"))
            Me.CodigoComercio = Convert.ToString(reader("Codigo"))


        End If
    End Sub
	<DataMember()> _
    Public Property CodigoComercio() As String
        Get
            Return _CodigoComercio
        End Get
        Set(ByVal value As String)
            _CodigoComercio = value
        End Set
    End Property
	<DataMember()> _
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

	<DataMember()> _
    Public Property NumOperaciones() As Integer
        Get
            Return _NumOperaciones
        End Get
        Set(ByVal value As Integer)
            _NumOperaciones = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaCierre() As DateTime
        Get
            Return _FechaCierre

        End Get
        Set(ByVal value As DateTime)
            _FechaCierre = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaApertura() As DateTime
        Get
            Return _FechaApertura
        End Get
        Set(ByVal value As DateTime)
            _FechaApertura = value
        End Set
    End Property
	<DataMember()> _
    Public Property idAperturaOrigen() As Integer
        Get
            Return _idAperturaOrigen
        End Get
        Set(ByVal value As Integer)
            _idAperturaOrigen = value
        End Set
    End Property
	<DataMember()> _
    Public Property moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property
	<DataMember()> _
    Public Property montoTotal() As Decimal
        Get
            Return _montoTotal
        End Get
        Set(ByVal value As Decimal)
            _montoTotal = value
        End Set
    End Property
	<DataMember()> _
    Public Property medioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstablecimiento() As Integer
        Get
            Return _idEstablecimiento
        End Get
        Set(ByVal value As Integer)
            _idEstablecimiento = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescripcionComercio() As String
        Get
            Return _DescripcionComercio
        End Get
        Set(ByVal value As String)
            _DescripcionComercio = value
        End Set
    End Property
	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

	<DataMember()> _
    Public Property SerieTerminal() As String
        Get
            Return _serieTerminal
        End Get
        Set(ByVal value As String)
            _serieTerminal = value
        End Set
    End Property


	<DataMember()> _
    Public Property IdOperador() As Integer
        Get
            Return _idOperador
        End Get
        Set(ByVal value As Integer)
            _idOperador = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property



	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _descripcionEstado
        End Get
        Set(ByVal value As String)
            _descripcionEstado = value
        End Set
    End Property



	<DataMember()> _
    Public Property idComercio() As Integer
        Get
            Return _idComercio
        End Get
        Set(ByVal value As Integer)
            _idComercio = value
        End Set
    End Property

    Public Class SerieTerminalComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEstablecimiento).SerieTerminal.CompareTo(CType(y, BEEstablecimiento).SerieTerminal)
        End Function
    End Class

    Public Class EstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEEstablecimiento).IdEstado.CompareTo(CType(y, BEEstablecimiento).IdEstado)
        End Function
    End Class

End Class
