Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()> Public Class BEFTPArchivo
    Inherits BEAuditoria

    Private _idFtpArchivo As Integer
    Private _idServicio As Integer
    Private _Servicio As String
    Private _nombreArchivo As String
    Private _fechaGenerado As Date
    Private _fechaHasta As Date
    Private _idTipoArchivo As Integer
    Private _tipoArchivo As String
    Private _idEstado As Integer
    Private _descripcionEstado As String
    Private _detalleArchivoFTP As List(Of BEDetalleArchivoFTP) = Nothing


    Private _orderByDetalle As String
    Private _isAccendingDetalle As Boolean



    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        IdFtpArchivo = CInt(reader("IdFtpArchivo").ToString())
        IdServicio = CInt(reader("IdServicio").ToString())
        Servicio = reader("Servicio").ToString()
        NombreArchivo = reader("NombreArchivo").ToString()
        FechaGenerado = Convert.ToDateTime(reader("FechaGenerado"))
        IdTipoArchivo = CInt(reader("IdTipoArchivo").ToString())
        TipoArchivo = reader("TipoArchivo").ToString()
        IdEstado = CInt(reader("IdEstado").ToString())
        DescripcionEstado = reader("DescripcionEstado").ToString()
    End Sub

	<DataMember()> _
    Public Property IdFtpArchivo() As Integer
        Get
            Return _idFtpArchivo
        End Get
        Set(ByVal value As Integer)
            _idFtpArchivo = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

	<DataMember()> _
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal value As String)
            _Servicio = value
        End Set
    End Property

	<DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaGenerado() As Date
        Get
            Return _fechaGenerado
        End Get
        Set(ByVal value As Date)
            _fechaGenerado = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaHasta() As Date
        Get
            Return _fechaHasta
        End Get
        Set(ByVal value As Date)
            _fechaHasta = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdTipoArchivo() As Integer
        Get
            Return _idTipoArchivo
        End Get
        Set(ByVal value As Integer)
            _idTipoArchivo = value
        End Set
    End Property

	<DataMember()> _
    Public Property TipoArchivo() As String
        Get
            Return _tipoArchivo
        End Get
        Set(ByVal value As String)
            _tipoArchivo = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _descripcionEstado
        End Get
        Set(ByVal value As String)
            _descripcionEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property DetalleArchivoFTP() As List(Of BEDetalleArchivoFTP)
        Get
            Return _detalleArchivoFTP
        End Get
        Set(ByVal value As List(Of BEDetalleArchivoFTP))
            _detalleArchivoFTP = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrderByDetalle() As String
        Get
            Return _orderByDetalle
        End Get
        Set(ByVal value As String)
            _orderByDetalle = value
        End Set
    End Property

	<DataMember()> _
    Public Property IsAccendingDetalle() As Boolean
        Get
            Return _isAccendingDetalle
        End Get
        Set(ByVal value As Boolean)
            _isAccendingDetalle = value
        End Set
    End Property

    Private _ArchivoBinario As Byte()

	<DataMember()> _
    Public Property ArchivoBinario() As Byte()
        Get
            Return _ArchivoBinario
        End Get
        Set(ByVal value As Byte())
            _ArchivoBinario = value
        End Set
    End Property



    Public Class ServicioComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEFTPArchivo).Servicio.CompareTo(CType(y, BEFTPArchivo).Servicio)
        End Function
    End Class

    Public Class NombreArchivoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEFTPArchivo).NombreArchivo.CompareTo(CType(y, BEFTPArchivo).NombreArchivo)
        End Function
    End Class

    Public Class FechaGeneracionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEFTPArchivo).FechaGenerado.CompareTo(CType(y, BEFTPArchivo).FechaGenerado)
        End Function
    End Class

    Public Class TipoArchivoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEFTPArchivo).TipoArchivo.CompareTo(CType(y, BEFTPArchivo).TipoArchivo)
        End Function
    End Class

    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEFTPArchivo).DescripcionEstado.CompareTo(CType(y, BEFTPArchivo).DescripcionEstado)
        End Function
    End Class

End Class
