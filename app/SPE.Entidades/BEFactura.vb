﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()>
Public Class BEFactura
    Inherits BEAuditoria

    Private _idFactura As Int64
    Private _nroFactura As String
    Private _montoFactura As Decimal
    Private _fechaEmision As DateTime
    Private _Observacion As String

    'Consulta
    Private _codigoOperacion As String
    Private _descripcionBanco As String


    Public Sub New()
    End Sub
    Public Sub New(ByVal reader As IDataReader)

    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipoConsulta As String)
        Select Case tipoConsulta
            Case "consultaFactura"
                Me.TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))
                IdFactura = ObjectToInt32(reader("IdFactura"))
                NroFactura = ObjectToString(reader("NroFactura"))
                MontoFactura = ObjectToDecimal(reader("MontoFactura"))
                FechaEmision = ObjectToDateTime(reader("FechaEmision"))
                Observacion = ObjectToString(reader("Observacion"))
                DescripcionBanco = ObjectToString(reader("DescripcionBanco"))
                'CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                'DescripcionBanco = ObjectToString(reader("DescripcionBanco"))
                'CodigoOperacion = ObjectToString(reader("CodigoOperacion"))
                'NroTransaccion = ObjectToString(reader("NroTransaccion"))
                'FechaDeposito = ObjectToDateTime(reader("FechaDeposito"))
                'FechaEmision = ObjectToDateTime(reader("FechaEmision"))
                'MontoDepositado = ObjectToDecimal(reader("MontoDepositado"))
                'IdDeposito = ObjectToInt64(reader("IdDeposito"))
                'IdFactura = ObjectToInt64(reader("IdFactura"))
            Case Else
        End Select
    End Sub

    <DataMember()> _
    Public Property IdFactura() As Int64
        Get
            Return _idFactura
        End Get
        Set(ByVal value As Int64)
            _idFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property NroFactura() As String
        Get
            Return _nroFactura
        End Get
        Set(ByVal value As String)
            _nroFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property MontoFactura() As Decimal
        Get
            Return _montoFactura
        End Get
        Set(ByVal value As Decimal)
            _montoFactura = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _fechaEmision
        End Get
        Set(ByVal value As DateTime)
            _fechaEmision = value
        End Set
    End Property
    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoOperacion() As String
        Get
            Return _codigoOperacion
        End Get
        Set(ByVal value As String)
            _codigoOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionBanco() As String
        Get
            Return _descripcionBanco
        End Get
        Set(ByVal value As String)
            _descripcionBanco = value
        End Set
    End Property
End Class
