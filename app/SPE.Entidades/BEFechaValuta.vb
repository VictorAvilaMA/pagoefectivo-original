Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEFechaValuta
    Inherits BEAuditoria

    Private _idEstado As Integer
    Private _fecha As DateTime
    Private _idAgente As Integer
    Private _idCaja As Integer
    Private _idFechaValuta As Integer
    Private _idTipoMovimiento As Integer
    Private _idAgenteCaja As Integer
    Private _idTipoFechaValuta As Integer

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        MyBase.New(reader)
        Me.IdFechaValuta = Convert.ToInt32(reader("IdFechaValuta").ToString)
        Me.Fecha = Convert.ToDateTime(reader("Fecha").ToString)
        'Me.IdCaja = Convert.ToInt32(reader("IdCaja").ToString)
        'Me.IdAgente = Convert.ToInt32(reader("IdAgente").ToString)
        Me.IdTipoMovimiento = Convert.ToInt32(reader("IdTipoMovimiento"))
        Me.IdEstado = Convert.ToInt32(reader("IdEstado").ToString)
        Me.IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja").ToString)


    End Sub
	<DataMember()> _
    Public Property IdAgenteCaja() As Integer
        Get
            Return _idAgenteCaja
        End Get
        Set(ByVal value As Integer)
            _idAgenteCaja = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdFechaValuta() As Integer
        Get
            Return _idFechaValuta
        End Get
        Set(ByVal value As Integer)
            _idFechaValuta = value
        End Set
    End Property

	<DataMember()> _
    Public Property Fecha() As DateTime
        Get
            Return _fecha
        End Get
        Set(ByVal value As Date)
            _fecha = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdCaja() As Integer
        Get
            Return _idCaja
        End Get
        Set(ByVal value As Integer)
            _idCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdAgente() As Integer
        Get
            Return _idAgente
        End Get
        Set(ByVal value As Integer)
            _idAgente = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdTipoMovimiento() As Integer
        Get
            Return _idTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            _idTipoMovimiento = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdTipoFechaValuta() As Integer
        Get
            Return _idTipoFechaValuta
        End Get
        Set(ByVal value As Integer)
            _idTipoFechaValuta = value
        End Set
    End Property

End Class
