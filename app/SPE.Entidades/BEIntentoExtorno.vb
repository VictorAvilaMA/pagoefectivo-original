Imports System.Runtime.Serialization
Imports System.Collections.Generic
Imports System
Imports _3Dev.FW.Entidades.Seguridad


<Serializable()> <DataContract()> Public Class BEIntentoExtorno
    Inherits BEAuditoria

    Private _idIntencionExtorno As Integer
    Private _IdOrdenPago As Integer
    Private _NumeroOperacion As String
    Private _CodBanco As String
    Private _CodServicio As String
    Private _CodAgencia As String
    Private _Procesado As Boolean
    
    Public Sub New()

    End Sub


    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        If (tipo = "Consultar") Then
            Me.FechaCreacion = Convert.ToDateTime(reader("FechaCreacion"))
            Me.IdOrdenPago = Convert.ToInt64(reader("IdOrdenPago"))
            Me.NumeroOperacion = Convert.ToString(reader("NumeroOperacion"))
            Me.CodBanco = Convert.ToString(reader("CodBanco"))
            Me.CodServicio = Convert.ToString(reader("CodServicio"))
            Me.CodAgencia = Convert.ToString(reader("CodAgencia"))
            Me.Procesado = Convert.ToBoolean(reader("Procesado"))
        End If
    End Sub
    <DataMember()> _
    Public Property IdIntencionExtorno() As Integer
        Get
            Return _idIntencionExtorno
        End Get
        Set(ByVal value As Integer)
            _idIntencionExtorno = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Integer)
            _IdOrdenPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _NumeroOperacion

        End Get
        Set(ByVal value As String)
            _NumeroOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodBanco() As String
        Get
            Return _CodBanco

        End Get
        Set(ByVal value As String)
            _CodBanco = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio

        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodAgencia() As String
        Get
            Return _CodAgencia

        End Get
        Set(ByVal value As String)
            _CodAgencia = value
        End Set
    End Property
    <DataMember()> _
    Public Property Procesado() As Boolean
        Get
            Return _Procesado

        End Get
        Set(ByVal value As Boolean)
            _Procesado = value
        End Set
    End Property

End Class
