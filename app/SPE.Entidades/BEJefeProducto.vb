Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
<Serializable()> <DataContract()> _
Public Class BEJefeProducto
    Inherits BEUsuarioBase

    Private _idJefeProducto As Integer
    Private _listaTipoOrigenCancelacion As List(Of BETipoOrigenCancelacion)



    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal reader As IDataReader)
        MyBase.New(reader)
        Me.IdJefeProducto = ObjectToInt32(reader("IdJefeProducto"))
        'Me.IdUsuario = Convert.ToInt32(reader("IdUsuario"))
        'Me.Nombres = Convert.ToString(reader("Nombres"))
        'Me.Apellidos = Convert.ToString(reader("Apellidos"))
        'Me.IdTipoDocumento = Convert.ToString(reader("IdTipoDocumento"))
        'Me.NumeroDocumento = Convert.ToString(reader("NumeroDocumento"))
        'Me.Telefono = Convert.ToString(reader("Telefono"))
        'Me.Direccion = Convert.ToString(reader("Direccion"))
        'Me.IdPais = Convert.ToInt32(reader("IdPais"))
        'Me.IdDepartamento = Convert.ToInt32(reader("IdDepartamento"))
        'Me.IdCiudad = Convert.ToInt32(reader("IdCiudad"))
        'Me.Email = Convert.ToString(reader("Email"))
        'Me.Estado = Convert.ToString(reader("Estado"))
        'Me.IdEstado = Convert.ToInt32(reader("IdEstado"))
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        MyBase.New(reader)
        Me.IdJefeProducto = ObjectToInt32(reader("IdJefeProducto"))

        Select Case tipo
            Case "consulta"
                Me.TipoDoc = Convert.ToString(reader("TipoDoc"))
        End Select



        'Me.IdUsuario = Convert.ToInt32(reader("IdUsuario"))
        'Me.Nombres = Convert.ToString(reader("Nombres"))
        'Me.Apellidos = Convert.ToString(reader("Apellidos"))
        'Me.IdTipoDocumento = Convert.ToString(reader("IdTipoDocumento"))
        'Me.NumeroDocumento = Convert.ToString(reader("NumeroDocumento"))
        'Me.Telefono = Convert.ToString(reader("Telefono"))
        'Me.Direccion = Convert.ToString(reader("Direccion"))
        'Me.IdPais = Convert.ToInt32(reader("IdPais"))
        'Me.IdDepartamento = Convert.ToInt32(reader("IdDepartamento"))
        'Me.IdCiudad = Convert.ToInt32(reader("IdCiudad"))
        'Me.Email = Convert.ToString(reader("Email"))
        'Me.Estado = Convert.ToString(reader("Estado"))
        'Me.IdEstado = Convert.ToInt32(reader("IdEstado"))
    End Sub

	<DataMember()> _
    Public Property IdJefeProducto()
        Get
            Return _idJefeProducto
        End Get
        Set(ByVal value)
            _idJefeProducto = value
        End Set
    End Property

	<DataMember()> _
    Public Property ListaTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
        Get
            Return _listaTipoOrigenCancelacion
        End Get
        Set(ByVal value As List(Of BETipoOrigenCancelacion))
            _listaTipoOrigenCancelacion = value
        End Set

    End Property

 


End Class
