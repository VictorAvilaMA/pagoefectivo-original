﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util


<Serializable()> <DataContract()> Public Class BELiquidacion

    'Registros no conciliados
    Private _Cip As Integer
    Private _IdComercio As String
    Private _Servicio As String
    Private _Moneda As String
    Private _Comision As Decimal
    Private _Total As Decimal
    Private _Estado As String
    Private _Banco As String
    Private _FechaEmision As DateTime
    Private _FechaCancelacion As DateTime
    Private _NombreArchivo As String
    Private _DataAdicional As String
    Private _ConceptoPago As String




    Private _TipoCuenta As String
    Private _CuentaAbono As String
    Private _TipoDocumento As String
    Private _NumeroDocumento As String
    Private _NombreProveedor As String
    Private _MontoAbono As Decimal


    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "ConsLiqNoConc"
                Me.Cip = DataUtil.ObjectToInt32(reader("Cip"))
                Me.IdComercio = DataUtil.ObjectToString(reader("IdComercio"))
                Me.Servicio = DataUtil.ObjectToString(reader("Servicio"))
                Me.Moneda = DataUtil.ObjectToString(reader("Moneda"))
                Me.Comision = DataUtil.ObjectToDecimal(reader("Comision"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.Estado = DataUtil.ObjectToString(reader("Estado"))
                Me.Banco = DataUtil.ObjectToString(reader("Banco"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.NombreArchivo = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Case "DepositosBCP"
                Me.TipoCuenta = DataUtil.ObjectToString(reader("TipoCuenta"))
                Me.CuentaAbono = DataUtil.ObjectToString(reader("CuentaAbono"))
                Me.TipoDocumento = DataUtil.ObjectToString(reader("TipoDocumento"))
                Me.NumeroDocumento = DataUtil.ObjectToString(reader("NumeroDocumento"))
                Me.NombreProveedor = DataUtil.ObjectToString(reader("NombreProveedor"))
                Me.MontoAbono = DataUtil.ObjectToDecimal(reader("MontoAbono"))
        End Select
    End Sub


#Region "Liquidaciones"



    <DataMember()> _
    Public Property Cip() As Integer
        Get
            Return _Cip
        End Get
        Set(ByVal value As Integer)
            _Cip = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdComercio() As String
        Get
            Return _IdComercio
        End Get
        Set(ByVal value As String)
            _IdComercio = value
        End Set
    End Property


    <DataMember()> _
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal value As String)
            _Servicio = value
        End Set
    End Property



    <DataMember()> _
    Public Property Moneda As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property


    <DataMember()> _
    Public Property Comision() As Decimal
        Get
            Return _Comision
        End Get
        Set(ByVal value As Decimal)
            _Comision = value
        End Set
    End Property




    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            _Total = value
        End Set
    End Property

    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property


    <DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaEmision() As Date
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As Date)
            _FechaEmision = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaCancelacion() As Date
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As Date)
            _FechaCancelacion = value
        End Set
    End Property


    <DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property



    <DataMember()> _
    Public Property DataAdicional() As String
        Get
            Return _DataAdicional
        End Get
        Set(ByVal value As String)
            _DataAdicional = value
        End Set
    End Property


    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property



#End Region


#Region "Depósitos Banco"

    <DataMember()> _
    Public Property TipoCuenta() As String
        Get
            Return _TipoCuenta
        End Get
        Set(ByVal value As String)
            _TipoCuenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property CuentaAbono() As String
        Get
            Return _CuentaAbono
        End Get
        Set(ByVal value As String)
            _CuentaAbono = value
        End Set
    End Property


    <DataMember()> _
    Public Property TipoDocumento() As String
        Get
            Return _TipoDocumento
        End Get
        Set(ByVal value As String)
            _TipoDocumento = value
        End Set
    End Property


    <DataMember()> _
    Public Property NumeroDocumento() As String
        Get
            Return _NumeroDocumento
        End Get
        Set(ByVal value As String)
            _NumeroDocumento = value
        End Set
    End Property


    <DataMember()> _
    Public Property NombreProveedor() As String
        Get
            Return _NombreProveedor
        End Get
        Set(ByVal value As String)
            _NombreProveedor = value
        End Set
    End Property



    <DataMember()> _
    Public Property MontoAbono() As Decimal
        Get
            Return _MontoAbono
        End Get
        Set(ByVal value As Decimal)
            _MontoAbono = value
        End Set
    End Property
#End Region


End Class