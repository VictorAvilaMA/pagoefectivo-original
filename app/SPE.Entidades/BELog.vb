Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades
Imports MongoDB.Bson

<Serializable()> <DataContract()> _
Public Class BELog
    Inherits BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal origen As String, ByVal descripcion As String, ByVal descripcion2 As String)
        _origen = origen
        _descripcion = descripcion
        _descripcion2 = descripcion2
    End Sub

    Public Sub New(ByVal reader As IDataReader)
        _idLog = ObjectToInt64(reader("IdLog"))
        _idTipo = ObjectToInt64(reader("IdTipo"))
        _origen = ObjectToString(reader("Origen"))
        _descripcion = ObjectToString(reader("Descripcion"))
        _descripcion2 = ObjectToString(reader("Descripcion2"))
        _parametro1 = ObjectToString(reader("Parametro1"))
        _parametro2 = ObjectToString(reader("Parametro2"))
        _parametro3 = ObjectToString(reader("Parametro3"))
        _parametro4 = ObjectToString(reader("Parametro4"))
        _parametro5 = ObjectToString(reader("Parametro5"))
        _parametro6 = ObjectToString(reader("Parametro6"))
        _parametro7 = ObjectToString(reader("Parametro7"))
        _parametro8 = ObjectToString(reader("Parametro8"))
    End Sub

    Private _id As ObjectId
    <DataMember()> _
    Public Property id() As ObjectId
        Get
            Return _id
        End Get
        Set(value As ObjectId)
            _id = value
        End Set
    End Property


    Private _idLog As Int64
	<DataMember()> _
    Public Property IdLog() As Int64
        Get
            Return _idLog
        End Get
        Set(ByVal value As Int64)
            _idLog = value
        End Set
    End Property

    Private _idTipo As Integer
	<DataMember()> _
    Public Property IdTipo() As Integer
        Get
            Return _idTipo
        End Get
        Set(ByVal value As Integer)
            _idTipo = value
        End Set
    End Property

    Private _origen As String
	<DataMember()> _
    Public Property Origen() As String
        Get
            Return _origen
        End Get
        Set(ByVal value As String)
            _origen = value
        End Set
    End Property

    Private _descripcion As String
	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Private _descripcion2 As String
	<DataMember()> _
    Public Property Descripcion2() As String
        Get
            Return _descripcion2
        End Get
        Set(ByVal value As String)
            _descripcion2 = value
        End Set
    End Property

    Private _parametro1 As String
	<DataMember()> _
    Public Property Parametro1() As String
        Get
            Return _parametro1
        End Get
        Set(ByVal value As String)
            _parametro1 = value
        End Set
    End Property

    Private _parametro2 As String
	<DataMember()> _
    Public Property Parametro2() As String
        Get
            Return _parametro2
        End Get
        Set(ByVal value As String)
            _parametro2 = value
        End Set
    End Property

    Private _parametro3 As String
	<DataMember()> _
    Public Property Parametro3() As String
        Get
            Return _parametro3
        End Get
        Set(ByVal value As String)
            _parametro3 = value
        End Set
    End Property

    Private _parametro4 As String
	<DataMember()> _
    Public Property Parametro4() As String
        Get
            Return _parametro4
        End Get
        Set(ByVal value As String)
            _parametro4 = value
        End Set
    End Property

    Private _parametro5 As String
	<DataMember()> _
    Public Property Parametro5() As String
        Get
            Return _parametro5
        End Get
        Set(ByVal value As String)
            _parametro5 = value
        End Set
    End Property

    Private _parametro6 As String
	<DataMember()> _
    Public Property Parametro6() As String
        Get
            Return _parametro6
        End Get
        Set(ByVal value As String)
            _parametro6 = value
        End Set
    End Property

    Private _parametro7 As String
	<DataMember()> _
    Public Property Parametro7() As String
        Get
            Return _parametro7
        End Get
        Set(ByVal value As String)
            _parametro7 = value
        End Set
    End Property

    Private _parametro8 As String
	<DataMember()> _
    Public Property Parametro8() As String
        Get
            Return _parametro8
        End Get
        Set(ByVal value As String)
            _parametro8 = value
        End Set
    End Property

    Private _fechaCreacion As DateTime
	<DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

End Class
