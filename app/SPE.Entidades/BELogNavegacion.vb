﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades

<Serializable()> <DataContract()> Public Class BELogNavegacion
    Inherits BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        'Constructor
        _idLogNavegacion = ObjectToInt64(reader("IdLogNavegacion"))
        _fechaNavegacion = ObjectToDateTime(reader("FechaNavegacion"))
        _urlPage = ObjectToString(reader("UrlPage"))
        _pCName = ObjectToString(reader("PCName"))
        _titulo = ObjectToString(reader("Titulo"))
        TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))

    End Sub

    Private _idLogNavegacion As Int64
    <DataMember()> _
    Public Property IdLogNavegacion() As Int64
        Get
            Return _idLogNavegacion
        End Get
        Set(ByVal value As Int64)
            _idLogNavegacion = value
        End Set
    End Property

    Private _idUsuario As Integer
    <DataMember()> _
    Public Property IdUsuario() As Integer
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As Integer)
            _idUsuario = value
        End Set
    End Property

    Private _fechaNavegacion As DateTime
    <DataMember()> _
    Public Property FechaNavegacion() As DateTime
        Get
            Return _fechaNavegacion
        End Get
        Set(ByVal value As DateTime)
            _fechaNavegacion = value
        End Set
    End Property

    Private _fechaNavegacionDesde As DateTime
    <DataMember()> _
    Public Property FechaNavegacionDesde() As DateTime
        Get
            Return _fechaNavegacionDesde
        End Get
        Set(ByVal value As DateTime)
            _fechaNavegacionDesde = value
        End Set
    End Property

    Private _fechaNavegacionHasta As DateTime
    <DataMember()> _
    Public Property FechaNavegacionHasta() As DateTime
        Get
            Return _fechaNavegacionHasta
        End Get
        Set(ByVal value As DateTime)
            _fechaNavegacionHasta = value
        End Set
    End Property

    Private _urlPage As String
    <DataMember()> _
    Public Property UrlPage() As String
        Get
            Return _urlPage
        End Get
        Set(ByVal value As String)
            _urlPage = value
        End Set
    End Property

    Private _accion As String
    <DataMember()> _
    Public Property Accion() As String
        Get
            Return _accion
        End Get
        Set(ByVal value As String)
            _accion = value
        End Set
    End Property

    Private _pCName As String
    <DataMember()> _
    Public Property PCName() As String
        Get
            Return _pCName
        End Get
        Set(ByVal value As String)
            _pCName = value
        End Set
    End Property

    Private _titulo As String
    <DataMember()> _
    Public Property Titulo() As String
        Get
            Return _titulo
        End Get
        Set(ByVal value As String)
            _titulo = value
        End Set
    End Property

End Class
