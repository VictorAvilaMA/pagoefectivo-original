﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades

<Serializable()> <DataContract()> _
Public Class BELogRedirect
    Inherits BusinessEntityBase

    Public Sub New()

    End Sub


    Private _idLogRedirect As Int64
    Private _idTipo As Integer
    Private _urlOrigen As String
    Private _urlDestino As String
    Private _descripcion As String
    Private _parametro1 As String
    Private _parametro2 As String
    Private _parametro3 As String
    Private _parametro4 As String
    Private _parametro5 As String
    Private _parametro6 As String
    Private _parametro7 As String
    Private _parametro8 As String
    Private _fechaCreacion As DateTime


    <DataMember()> _
    Public Property IdLogRedirect() As Int64
        Get
            Return _idLogRedirect
        End Get
        Set(ByVal value As Int64)
            _idLogRedirect = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdTipo() As Integer
        Get
            Return _idTipo
        End Get
        Set(ByVal value As Integer)
            _idTipo = value
        End Set
    End Property
    <DataMember()> _
    Public Property UrlOrigen() As String
        Get
            Return _urlOrigen
        End Get
        Set(ByVal value As String)
            _urlOrigen = value
        End Set
    End Property
    <DataMember()> _
    Public Property UrlDestino() As String
        Get
            Return _urlDestino
        End Get
        Set(ByVal value As String)
            _urlDestino = value
        End Set
    End Property
    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro1() As String
        Get
            Return _parametro1
        End Get
        Set(ByVal value As String)
            _parametro1 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro2() As String
        Get
            Return _parametro2
        End Get
        Set(ByVal value As String)
            _parametro2 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro3() As String
        Get
            Return _parametro3
        End Get
        Set(ByVal value As String)
            _parametro3 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro4() As String
        Get
            Return _parametro4
        End Get
        Set(ByVal value As String)
            _parametro4 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro5() As String
        Get
            Return _parametro5
        End Get
        Set(ByVal value As String)
            _parametro5 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro6() As String
        Get
            Return _parametro6
        End Get
        Set(ByVal value As String)
            _parametro6 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro7() As String
        Get
            Return _parametro7
        End Get
        Set(ByVal value As String)
            _parametro7 = value
        End Set
    End Property
    <DataMember()> _
    Public Property Parametro8() As String
        Get
            Return _parametro8
        End Get
        Set(ByVal value As String)
            _parametro8 = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

End Class

