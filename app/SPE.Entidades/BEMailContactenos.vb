Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEMailContactenos

    Public Sub New()

    End Sub

    Public Sub New(ByVal nombrecompleto As String, ByVal email As String, ByVal tema As String, ByVal contenido As String)
        _NombreCompleto = nombrecompleto
        _email = email
        _tema = tema
        _Contenido = contenido
    End Sub


    Private _NombreCompleto As String
    <DataMember()> _
    Public Property NombreCompleto() As String
        Get
            Return _NombreCompleto
        End Get
        Set(ByVal value As String)
            _NombreCompleto = value
        End Set
    End Property


    Private _email As String
    <DataMember()> _
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property


    Private _tema As String
    <DataMember()> _
    Public Property Tema() As String
        Get
            Return _tema
        End Get
        Set(ByVal value As String)
            _tema = value
        End Set
    End Property



    Private _Contenido As String
    <DataMember()> _
    Public Property Contenido() As String
        Get
            Return _Contenido
        End Get
        Set(ByVal value As String)
            _Contenido = value
        End Set
    End Property

End Class
