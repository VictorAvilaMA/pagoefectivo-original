﻿Imports System.Runtime.Serialization
<Serializable()> _
<DataContract()> _
Public Class BEMailContactenosEmpresa
    Public Sub New()

    End Sub

    Public Sub New(ByVal nombrecompleto As String, ByVal email As String, ByVal tema As String, ByVal contenido As String, ByVal razonSocial As String, ByVal ruc As String, ByVal rubro As String, ByVal telefonoempresa As String, ByVal paginaweb As String)
        _NombreCompleto = nombrecompleto
        _email = email
        _tema = tema
        _Contenido = contenido
        _RazonSocial = razonSocial
        _RUC = ruc
        _Rubro = rubro
        _TelefonoEmpresa = telefonoempresa
        _PaginaWeb = paginaweb
    End Sub


    Private _NombreCompleto As String
    <DataMember()> _
    Public Property NombreCompleto() As String
        Get
            Return _NombreCompleto
        End Get
        Set(ByVal value As String)
            _NombreCompleto = value
        End Set
    End Property


    Private _email As String
    <DataMember()> _
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property


    Private _tema As String
    <DataMember()> _
    Public Property Tema() As String
        Get
            Return _tema
        End Get
        Set(ByVal value As String)
            _tema = value
        End Set
    End Property



    Private _Contenido As String
    <DataMember()> _
    Public Property Contenido() As String
        Get
            Return _Contenido
        End Get
        Set(ByVal value As String)
            _Contenido = value
        End Set
    End Property

    'jeffersonmendoza
    Private _RazonSocial As String
    <DataMember()> _
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Private _RUC As String
    <DataMember()> _
    Public Property RUC() As String
        Get
            Return _RUC
        End Get
        Set(ByVal value As String)
            _RUC = value
        End Set
    End Property

    Private _Rubro As String
    <DataMember()> _
    Public Property Rubro() As String
        Get
            Return _Rubro
        End Get
        Set(ByVal value As String)
            _Rubro = value
        End Set
    End Property

    Private _TelefonoEmpresa As String
    <DataMember()> _
    Public Property TelefonoEmpresa() As String
        Get
            Return _TelefonoEmpresa
        End Get
        Set(ByVal value As String)
            _TelefonoEmpresa = value
        End Set
    End Property

    Private _PaginaWeb As String
    <DataMember()> _
    Public Property PaginaWeb() As String
        Get
            Return _PaginaWeb
        End Get
        Set(ByVal value As String)
            _PaginaWeb = value
        End Set
    End Property
End Class
