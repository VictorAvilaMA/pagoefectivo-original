Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEMedioPago


    Private _descripcion As String
    Private _idMedioPago As Integer

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdMedioPago = Convert.ToInt32(reader("IdMedioPago").ToString)
        Me.Descripcion = reader("Descripcion").ToString
    End Sub

	<DataMember()> _
    Public Property IdMedioPago() As Integer
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As Integer)
            _idMedioPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class
