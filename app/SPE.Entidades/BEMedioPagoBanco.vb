﻿Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
<Serializable()>
<DataContract()>
Public Class BEMedioPagoBanco
    Private _descripcion As String
    Private _idMedioPago As Integer
    Private _codigoMedPago As String
    Private _codigoMedPagoConc As String
    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdMedioPago = Convert.ToInt32(reader("IdMedioPago").ToString)
        Me.Descripcion = reader("Descripcion").ToString
        Me.CodigoMedPago = reader("CodMedioPago").ToString
        Me.CodigoMedPagoConc = reader("CodMedioPagoConciliacion").ToString
    End Sub

    <DataMember()> _
    Public Property IdMedioPago() As Integer
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As Integer)
            _idMedioPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoMedPago() As String
        Get
            Return _codigoMedPago
        End Get
        Set(ByVal value As String)
            _codigoMedPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodigoMedPagoConc() As String
        Get
            Return _codigoMedPagoConc
        End Get
        Set(ByVal value As String)
            _codigoMedPagoConc = value
        End Set
    End Property

End Class
