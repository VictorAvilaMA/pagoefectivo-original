﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEMobileMessaging

    Private _paymentOrderId As Int64
    Private _countryCode As String
    Private _phoneNumber As String

    Public Sub New()

    End Sub

    <DataMember()> _
    Public Property PaymentOrderId() As Int64
        Get
            Return _PaymentOrderId
        End Get
        Set(ByVal value As Int64)
            _PaymentOrderId = value
        End Set
    End Property

    <DataMember()> _
    Public Property CountryCode() As String
        Get
            Return _CountryCode
        End Get
        Set(ByVal value As String)
            _CountryCode = value
        End Set
    End Property

    <DataMember()> _
    Public Property PhoneNumber() As String
        Get
            Return _PhoneNumber
        End Get
        Set(ByVal value As String)
            _PhoneNumber = value
        End Set
    End Property

End Class