Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEMoneda

    Private _descripcion As String
    Private _simbolo As String
    Private _idMoneda As Integer

    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.IdMoneda = Convert.ToInt32(reader("IdMoneda").ToString)
        Me.Descripcion = reader("Descripcion").ToString
        Me.Simbolo = reader("Simbolo").ToString
    End Sub
    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    <DataMember()> _
    Public Property Simbolo() As String
        Get
            Return _simbolo
        End Get
        Set(ByVal value As String)
            _simbolo = value
        End Set
    End Property

End Class
