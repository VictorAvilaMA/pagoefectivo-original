Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> _
Public Class BEMonedaBanco

    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.idMoneda = ObjectToInt32(reader("idMoneda"))
        Me.idBanco = ObjectToInt32(reader("idBanco"))
        Me.codMoneda = ObjectToString(reader("codMoneda"))
        Me.codBanco = ObjectToString(reader("codBanco"))
        Me.codMonedaBanco = ObjectToString(reader("codMonedaBanco"))
        Me.codMonedaConciliacion = ObjectToString(reader("codMonedaConciliacion"))
        Me.idEstado = ObjectToInt32(reader("idEstado"))
    End Sub

    Private _idMoneda As Integer
	<DataMember()> _
    Public Property idMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    Private _idBanco As Integer
	<DataMember()> _
    Public Property idBanco() As Integer
        Get
            Return _idBanco
        End Get
        Set(ByVal value As Integer)
            _idBanco = value
        End Set
    End Property

    Private _codBanco As Integer
	<DataMember()> _
    Public Property codBanco() As Integer
        Get
            Return _codBanco
        End Get
        Set(ByVal value As Integer)
            _codBanco = value
        End Set
    End Property

    Private _CodMoneda As String
	<DataMember()> _
    Public Property codMoneda() As String
        Get
            Return _CodMoneda
        End Get
        Set(ByVal value As String)
            _CodMoneda = value
        End Set
    End Property

    Private _CodMonedaBanco As String
	<DataMember()> _
    Public Property codMonedaBanco() As String
        Get
            Return _CodMonedaBanco
        End Get
        Set(ByVal value As String)
            _CodMonedaBanco = value
        End Set
    End Property

    Private _CodMonedaConciliacion As String
	<DataMember()> _
    Public Property codMonedaConciliacion() As String
        Get
            Return _CodMonedaConciliacion
        End Get
        Set(ByVal value As String)
            _CodMonedaConciliacion = value
        End Set
    End Property

    Private _idEstado As Integer
	<DataMember()> _
    Public Property idEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property


End Class
