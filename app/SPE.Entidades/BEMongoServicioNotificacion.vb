﻿Imports MongoDB.Bson
Imports MongoDB.Bson.Serialization.Attributes

Public Class BEMongoServicioNotificacion
    Public Property id() As ObjectId
        Get
            Return m_id
        End Get
        Set(value As ObjectId)
            m_id = Value
        End Set
    End Property
    Private m_id As ObjectId
    Public Property IdLogServicioNotificacion() As Int32
        Get
            Return m_IdLogServicioNotificacion
        End Get
        Set(value As Int32)
            m_IdLogServicioNotificacion = value
        End Set
    End Property
    Private m_IdLogServicioNotificacion As Int32
    Public Property IdServicioNotificacion() As Int32
        Get
            Return m_IdServicioNotificacion
        End Get
        Set(value As Int32)
            m_IdServicioNotificacion = Value
        End Set
    End Property
    Private m_IdServicioNotificacion As Int32
    Public Property IdOrdenPago() As Int32
        Get
            Return m_IdOrdenPago
        End Get
        Set(value As Int32)
            m_IdOrdenPago = Value
        End Set
    End Property
    Private m_IdOrdenPago As Int32
    Public Property IdTipoServNotificacion() As Int32
        Get
            Return m_IdTipoServNotificacion
        End Get
        Set(value As Int32)
            m_IdTipoServNotificacion = Value
        End Set
    End Property
    Private m_IdTipoServNotificacion As Int32
    Public Property IdMovimiento() As Int32
        Get
            Return m_IdMovimiento
        End Get
        Set(value As Int32)
            m_IdMovimiento = Value
        End Set
    End Property
    Private m_IdMovimiento As Int32
    Public Property UrlRespuesta() As [String]
        Get
            Return m_UrlRespuesta
        End Get
        Set(value As [String])
            m_UrlRespuesta = Value
        End Set
    End Property
    Private m_UrlRespuesta As [String]
    Public Property CantNotificaciones() As Int32
        Get
            Return m_CantNotificaciones
        End Get
        Set(value As Int32)
            m_CantNotificaciones = Value
        End Set
    End Property
    Private m_CantNotificaciones As Int32
    Public Property IdEstado() As Int32
        Get
            Return m_IdEstado
        End Get
        Set(value As Int32)
            m_IdEstado = Value
        End Set
    End Property
    Private m_IdEstado As Int32
    Public Property FechaCreacion() As [String]
        Get
            Return m_FechaCreacion
        End Get
        Set(value As [String])
            m_FechaCreacion = Value
        End Set
    End Property
    Private m_FechaCreacion As [String]
    Public Property IdUsuarioCreacion() As Int32
        Get
            Return m_IdUsuarioCreacion
        End Get
        Set(value As Int32)
            m_IdUsuarioCreacion = Value
        End Set
    End Property
    Private m_IdUsuarioCreacion As Int32
    Public Property FechaNotificacion() As [String]
        Get
            Return m_FechaNotificacion
        End Get
        Set(value As [String])
            m_FechaNotificacion = Value
        End Set
    End Property
    Private m_FechaNotificacion As [String]
    Public Property Observacion() As [String]
        Get
            Return m_Observacion
        End Get
        Set(value As [String])
            m_Observacion = Value
        End Set
    End Property
    Private m_Observacion As [String]
    Public Property IdOrigenRegistro() As Int32
        Get
            Return m_IdOrigenRegistro
        End Get
        Set(value As Int32)
            m_IdOrigenRegistro = Value
        End Set
    End Property
    Private m_IdOrigenRegistro As Int32
    Public Property FechaLog() As [String]
        Get
            Return m_FechaLog
        End Get
        Set(value As [String])
            m_FechaLog = Value
        End Set
    End Property
    Private m_FechaLog As [String]

    '----------------------------------------------------------------------------------------------------

    Public Sub New(ByVal oOriginal As BEServicioNotificacion)
        'IdLogServicioNotificacion = oOriginal.idser
        IdServicioNotificacion = oOriginal.IdServicioNotificacion
        IdOrdenPago = oOriginal.IdOrdenPago
        IdTipoServNotificacion = oOriginal.IdTipoServNotificacion
        IdMovimiento = oOriginal.IdMovimiento
        UrlRespuesta = oOriginal.UrlRespuesta
        CantNotificaciones = oOriginal.CantNotificaciones
        IdEstado = oOriginal.IdEstado
        FechaCreacion = oOriginal.FechaCreacion
        IdUsuarioCreacion = oOriginal.IdUsuarioCreacion
        FechaNotificacion = oOriginal.FechaNotificacion
        Observacion = oOriginal.Observacion
        IdOrigenRegistro = oOriginal.IdOrigenRegistro
        FechaLog = Date.Now.ToString("dd/MM/yyyy hh:mm:ss tt")
    End Sub

End Class
