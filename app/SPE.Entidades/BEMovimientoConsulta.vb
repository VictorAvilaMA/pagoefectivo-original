Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEMovimientoConsulta

    Private _descAgenciaRecaudadora As String
    Private _idAgenteCaja As Integer
    Private _descAgente As String
    Private _descCaja As String
    Private _descEstadoCaja As String
    Private _descSupervisorApertura As String
    Private _descSupervisorAnulacion As String
    Private _descSupervisorLiquidacion As String
    Private _fechaApertura As String
    Private _fechaCierre As String
    Private _fechaAnulacion As String
    Private _fechaLiquidacion As String
    Private _descTipoMovimiento As String
    Private _numeroOrdenPago As String
    Private _fechaMovimiento As String
    Private _descMedioPago As String
    Private _descMoneda As String
    Private _descSimboloMoneda As String
    Private _monto As Decimal

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal opcion As String)
        '
        If opcion = "prc_ConsultarDetalleMovimientosCaja" Then
            '
            DescAgenciaRecaudadora = reader("AgenciaRecaudadora").ToString
            IdAgenteCaja = Convert.ToInt32(reader("IdAgenteCaja"))
            DescAgente = reader("Agente").ToString
            DescCaja = reader("Caja").ToString
            DescEstadoCaja = reader("Estado").ToString
            DescSupervisorApertura = reader("SupervisorApertura").ToString
            DescSupervisorAnulacion = reader("SupervisorAnulacion").ToString
            DescSupervisorLiquidacion = reader("SupervisorLiquidacion").ToString
            FechaApertura = reader("FechaApertura").ToString
            FechaCierre = reader("FechaCierre").ToString
            FechaAnulacion = reader("FechaAnulacion").ToString
            FechaLiquidacion = reader("FechaLiquidacion").ToString
            DescTipoMovimiento = reader("TipoMov").ToString
            NumeroOrdenPago = reader("NumeroOP").ToString
            FechaMovimiento = reader("FechaMov").ToString
            DescMedioPago = reader("MedioPago").ToString
            DescMoneda = reader("DescMoneda").ToString
            DescSimboloMoneda = reader("SimboloMoneda").ToString
            Monto = Convert.ToDecimal(reader("Monto").ToString)
            '
        End If
        '
    End Sub

	<DataMember()> _
    Public Property DescAgenciaRecaudadora() As String
        Get
            Return _descAgenciaRecaudadora
        End Get
        Set(ByVal value As String)
            _descAgenciaRecaudadora = value
        End Set
    End Property


	<DataMember()> _
    Public Property IdAgenteCaja() As Integer
        Get
            Return _idAgenteCaja
        End Get
        Set(ByVal value As Integer)
            _idAgenteCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescAgente() As String
        Get
            Return _descAgente
        End Get
        Set(ByVal value As String)
            _descAgente = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescCaja() As String
        Get
            Return _descCaja
        End Get
        Set(ByVal value As String)
            _descCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescEstadoCaja() As String
        Get
            Return _descEstadoCaja
        End Get
        Set(ByVal value As String)
            _descEstadoCaja = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescSupervisorApertura() As String
        Get
            Return _descSupervisorApertura
        End Get
        Set(ByVal value As String)
            _descSupervisorApertura = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescSupervisorAnulacion() As String
        Get
            Return _descSupervisorAnulacion
        End Get
        Set(ByVal value As String)
            _descSupervisorAnulacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescSupervisorLiquidacion() As String
        Get
            Return _descSupervisorLiquidacion
        End Get
        Set(ByVal value As String)
            _descSupervisorLiquidacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaApertura() As String
        Get
            Return _fechaApertura
        End Get
        Set(ByVal value As String)
            _fechaApertura = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaCierre() As String
        Get
            Return _fechaCierre
        End Get
        Set(ByVal value As String)
            _fechaCierre = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaAnulacion() As String
        Get
            Return _fechaAnulacion
        End Get
        Set(ByVal value As String)
            _fechaAnulacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaLiquidacion() As String
        Get
            Return _fechaLiquidacion
        End Get
        Set(ByVal value As String)
            _fechaLiquidacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescTipoMovimiento() As String
        Get
            Return _descTipoMovimiento
        End Get
        Set(ByVal value As String)
            _descTipoMovimiento = value
        End Set
    End Property

	<DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaMovimiento() As String
        Get
            Return _fechaMovimiento
        End Get
        Set(ByVal value As String)
            _fechaMovimiento = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescMedioPago() As String
        Get
            Return _descMedioPago
        End Get
        Set(ByVal value As String)
            _descMedioPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescMoneda() As String
        Get
            Return _descMoneda
        End Get
        Set(ByVal value As String)
            _descMoneda = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescSimboloMoneda() As String
        Get
            Return _descSimboloMoneda
        End Get
        Set(ByVal value As String)
            _descSimboloMoneda = value
        End Set
    End Property

	<DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

End Class
