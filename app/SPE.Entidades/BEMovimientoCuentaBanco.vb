Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEMovimientoCuentaBanco
    Inherits BEAuditoria

    Private _idMovimiento As Integer
    Private _monto As Decimal
    Private _idTipoMovimiento As Integer
    Private _idOrdenPago As Integer
    Private _idMoneda As Integer
    Private _idAgenteCaja As Integer
    Private _fechaMovimiento As DateTime
    Private _FechaCierre As DateTime
    Private _FechaApertura As DateTime
    Private _FechaLiquidacion As DateTime
    Private _idMedioPago As Integer
    Private _estado As Integer
    Private _moneda As String
    Private _medioPago As String
    Private _numeroOrdenPago As String
    Private _numeroOperacion As String
    Private _numeroOperacionPago As String
    Private _codigoAgenciaBancaria As String
    Private _idAperturaOrigen As Integer
    Private _SerieTerminal As String
    Private _SimboloMoneda As String
    Private _RazonSocial As String
    Private _CodMonedaBanco As String
    Private _DescripcionTipoMovimiento As String
    Private _DescripcionOrigenCanc As String
    Private _DescripcionAgencia As String


    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        '
        Me.IdMovimiento = ObjectToInt64(reader("IdMovimiento"))
        Me.IdOrdenPago = ObjectToInt64(reader("IdOrdenPago"))
        Me.IdAgenteCaja = ObjectToInt(reader("IdAgenteCaja"))
        Me.IdMoneda = ObjectToInt(reader("IdMoneda"))
        Me.IdTipoMovimiento = ObjectToInt(reader("IdTipoMovimiento"))
        Me.IdMedioPago = ObjectToInt(reader("IdMedioPago"))
        Me.Monto = ObjectToDecimal(reader("Monto"))
        Me.FechaMovimiento = ObjectToDateTime(reader("FechaMovimiento"))
        Me.FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        Me.IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.IdUsuarioActualizacion = ObjectToInt(reader("IdUsuarioActualizacion"))

        '
    End Sub


    Public Sub New(ByVal reader As IDataReader, ByVal operacion As String)
        Select Case operacion
            Case "Consulta"
                Me.IdMovimiento = ObjectToInt64(reader("IdMovimiento"))
                Me.IdOrdenPago = ObjectToInt64(reader("IdOrdenPago"))
                Me.IdAgenteCaja = ObjectToInt(reader("IdAgenteCaja"))
                Me.IdMoneda = ObjectToInt(reader("IdMoneda"))
                Me.IdTipoMovimiento = ObjectToInt(reader("IdTipoMovimiento"))
                Me.IdMedioPago = ObjectToInt(reader("IdMedioPago"))
                Me.Monto = ObjectToDecimal(reader("Monto"))
                Me.FechaMovimiento = ObjectToDateTime(reader("FechaMovimiento"))
                Me.FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
                Me.IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
                Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
                Me.IdUsuarioActualizacion = ObjectToInt(reader("IdUsuarioActualizacion"))
            Case "Obtener"
                Me.IdMovimiento = ObjectToInt64(reader("IdMovimiento"))
                Me.IdOrdenPago = ObjectToInt64(reader("IdOrdenPago"))
                Me.IdAgenteCaja = ObjectToInt(reader("IdAgenteCaja"))
                Me.IdMoneda = ObjectToInt(reader("IdMoneda"))
                Me.IdTipoMovimiento = ObjectToInt(reader("IdTipoMovimiento"))
                Me.IdMedioPago = ObjectToInt(reader("IdMedioPago"))
                Me.Monto = ObjectToDecimal(reader("Monto"))
                Me.FechaMovimiento = ObjectToDateTime(reader("FechaMovimiento"))
            Case "ConsultaTrazabilidad"
                Me.NumeroOperacion = ObjectToString(reader("NumeroOperacion"))
                Me.IdTipoMovimiento = ObjectToInt32(reader("IdMovCuentaBanco"))
                Me.IdMovimientoCuentaBanco = ObjectToInt64(reader("IdMovCuentaBanco"))
                Me.IdCuentaBanco = ObjectToInt64(reader("IdCuentaBanco"))
                Me.Monto = ObjectToInt64(reader("Monto"))
                Me.IdTipoMovimiento = ObjectToInt64(reader("IdTipoMovimiento"))
                Me.IdMovimientoCuentaBancoAsociado = ObjectToInt64(reader("IdMovCuentaBancoAsociado"))
                Me.FechaMovimiento = ObjectToDateTime(reader("FechaMovimiento"))
                Me.TipoMovimientoDescripcion = ObjectToString(reader("TipoMotivacionDescripcion"))
                Me.DescripcionBanco = ObjectToString(reader("BancoDescripcion"))
                Me.ArchivoDescripcion = ObjectToString(reader("NombreArchivo"))
                Me.CuentaComercio = ObjectToString(reader("NroCtaBanco"))
                Me.CodMonedaBanco = ObjectToString(reader("DscMoneda"))
                Me.TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))
        End Select

    End Sub
    <DataMember()> _
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    <DataMember()> _
    Public Property SimboloMoneda() As String
        Get
            Return _SimboloMoneda
        End Get
        Set(ByVal value As String)
            _SimboloMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaLiquidacion() As DateTime
        Get
            Return _FechaLiquidacion
        End Get
        Set(ByVal value As DateTime)
            _FechaLiquidacion = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaCierre() As DateTime
        Get
            Return _FechaCierre
        End Get
        Set(ByVal value As DateTime)
            _FechaCierre = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaApertura() As DateTime
        Get
            Return _FechaApertura
        End Get
        Set(ByVal value As DateTime)
            _FechaApertura = value

        End Set
    End Property
    <DataMember()> _
    Public Property SerieTerminal() As String
        Get
            Return _SerieTerminal
        End Get
        Set(ByVal value As String)
            _SerieTerminal = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdMovimiento() As Integer
        Get
            Return _idMovimiento
        End Get
        Set(ByVal value As Integer)
            _idMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property Estado() As Integer
        Get
            Return _estado
        End Get
        Set(ByVal value As Integer)
            _estado = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdAgenteCaja() As Integer
        Get
            Return _idAgenteCaja
        End Get
        Set(ByVal value As Integer)
            _idAgenteCaja = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property
    <DataMember()> _
    Public Property CodMonedaBanco() As String
        Get
            Return _CodMonedaBanco
        End Get
        Set(ByVal value As String)
            _CodMonedaBanco = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdTipoMovimiento() As Integer
        Get
            Return _idTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            _idTipoMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMedioPago() As Integer
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As Integer)
            _idMedioPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaMovimiento() As DateTime
        Get
            Return _fechaMovimiento
        End Get
        Set(ByVal value As DateTime)
            _fechaMovimiento = value
        End Set
    End Property
    <DataMember()> _
    Public Property Moneda() As String
        Get
            Return _moneda
        End Get
        Set(ByVal value As String)
            _moneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property MedioPago() As String
        Get
            Return _medioPago
        End Get
        Set(ByVal value As String)
            _medioPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOperacionPago() As String
        Get
            Return _numeroOperacionPago
        End Get
        Set(ByVal value As String)
            _numeroOperacionPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoAgenciaBancaria() As String
        Get
            Return _codigoAgenciaBancaria
        End Get
        Set(ByVal value As String)
            _codigoAgenciaBancaria = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdAperturaOrigen() As Integer
        Get
            Return _idAperturaOrigen
        End Get
        Set(ByVal value As Integer)
            _idAperturaOrigen = value
        End Set
    End Property

    Dim _codigoBanco As String = ""
    <DataMember()> _
    Public Property CodigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property

    Private _NumeroSerieTerminal As String
    <DataMember()> _
    Public Property NumeroSerieTerminal() As String
        Get
            Return _NumeroSerieTerminal
        End Get
        Set(ByVal value As String)
            _NumeroSerieTerminal = value
        End Set
    End Property

    Private _CodigoPuntoVenta As String
    <DataMember()> _
    Public Property CodigoPuntoVenta() As String
        Get
            Return _CodigoPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodigoPuntoVenta = value
        End Set
    End Property

    Private _CodigoMedioPago As String
    <DataMember()> _
    Public Property CodigoMedioPago() As String
        Get
            Return _CodigoMedioPago
        End Get
        Set(ByVal value As String)
            _CodigoMedioPago = value
        End Set
    End Property

    Private _IdTipoOrigenCancelacion As Integer
    <DataMember()> _
    Public Property IdTipoOrigenCancelacion() As Integer
        Get
            Return _IdTipoOrigenCancelacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoOrigenCancelacion = value
        End Set
    End Property

    Private _CodigoServicio As String
    <DataMember()> _
    Public Property CodigoServicio() As String
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As String)
            _CodigoServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionTipoMovimiento() As String
        Get
            Return _DescripcionTipoMovimiento
        End Get
        Set(ByVal value As String)
            _DescripcionTipoMovimiento = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionOrigenCanc() As String
        Get
            Return _DescripcionOrigenCanc
        End Get
        Set(ByVal value As String)
            _DescripcionOrigenCanc = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionAgencia() As String
        Get
            Return _DescripcionAgencia
        End Get
        Set(ByVal value As String)
            _DescripcionAgencia = value
        End Set
    End Property


    'Agregado para Trazabilidad
    Private _idCuentaBanco As Integer
    <DataMember()> _
    Public Property IdCuentaBanco() As Integer
        Get
            Return _idCuentaBanco
        End Get
        Set(ByVal value As Integer)
            _idCuentaBanco = value
        End Set
    End Property
    Private _idMovimientoCuentaBanco As Int64
    <DataMember()> _
    Public Property IdMovimientoCuentaBanco() As Int64
        Get
            Return _idMovimientoCuentaBanco
        End Get
        Set(ByVal value As Int64)
            _idMovimientoCuentaBanco = value
        End Set
    End Property
    Private _idMovimientoCuentaBancoAsociado As Int64
    <DataMember()> _
    Public Property IdMovimientoCuentaBancoAsociado() As Int64
        Get
            Return _idMovimientoCuentaBancoAsociado
        End Get
        Set(ByVal value As Int64)
            _idMovimientoCuentaBancoAsociado = value
        End Set
    End Property
    Private _descripcionBanco As String
    <DataMember()> _
    Public Property DescripcionBanco() As String
        Get
            Return _descripcionBanco
        End Get
        Set(ByVal value As String)
            _descripcionBanco = value
        End Set
    End Property
    Private _idBanco As Int64
    <DataMember()> _
    Public Property IdBanco() As String
        Get
            Return _idBanco
        End Get
        Set(ByVal value As String)
            _idBanco = value
        End Set
    End Property
    Private _fechaMovimientoDesde As DateTime
    <DataMember()> _
    Public Property FechaMovimientoDesde() As DateTime
        Get
            Return _fechaMovimientoDesde
        End Get
        Set(ByVal value As DateTime)
            _fechaMovimientoDesde = value
        End Set
    End Property
    Private _fechaMovimientoHasta As DateTime
    <DataMember()> _
    Public Property FechaMovimientoHasta() As DateTime
        Get
            Return _fechaMovimientoHasta
        End Get
        Set(ByVal value As DateTime)
            _fechaMovimientoHasta = value
        End Set
    End Property
    Private _tipoMovimientoDescripcion As String
    <DataMember()> _
    Public Property TipoMovimientoDescripcion() As String
        Get
            Return _tipoMovimientoDescripcion
        End Get
        Set(value As String)
            _tipoMovimientoDescripcion = value
        End Set
    End Property
    Private _archivoDescripcion As String
    <DataMember()> _
    Public Property ArchivoDescripcion() As String
        Get
            Return _archivoDescripcion
        End Get
        Set(value As String)
            _archivoDescripcion = value
        End Set
    End Property
    Private _cuentaComercio As String
    <DataMember()> _
    Public Property CuentaComercio() As String
        Get
            Return _cuentaComercio
        End Get
        Set(value As String)
            _cuentaComercio = value
        End Set
    End Property
End Class
