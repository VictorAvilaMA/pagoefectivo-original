Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util
Imports _3Dev.FW.Entidades.Seguridad
<Serializable()> <DataContract()> Public Class BEOperacionBancaria
    Inherits BEAuditoria
    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.NumeroOperacion = reader("NumeroOperacion")
        Me.NumeroOrdenPago = reader("NumeroOrdenPago")
        Me.FechaConciliacion = reader("FechaConciliacion")
        Me.DescripcionEstado = reader("Descripcion")
        Me.Observacion = reader("Observacion")
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal consulta As String)
        If consulta = "NoConciliadas" Then
            Me.CodigoOficina = reader("Codigo")
            Me.NumeroOperacion = reader("NumeroOperacion")
            Me.CodigoDepositante = reader("NumeroOrdenPago")
            Me.FechaCancelacion = Convert.ToDateTime(reader("FechaPago"))
        ElseIf consulta = "CancelacionOP" Then
            Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
            Me.CodigoOficina = DataUtil.ObjectToString(reader("CodigoOficina"))
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.OcultarEmpresa = DataUtil.ObjectToString(reader("OcultarEmpresa"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.Moneda = DataUtil.ObjectToString(reader("Moneda"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.Email = DataUtil.ObjectToString(reader("Email"))
            Me.IdMovimiento = DataUtil.ObjectToInt64(reader("IdMovimiento"))

        ElseIf consulta = "RegistroOperacionBancaria" Then
            Me.IdConciliacion = DataUtil.ObjectToInt(reader("Resultado"))
            Me.CodigoOficina = DataUtil.ObjectToString(reader("AgenciaBancaria"))

        ElseIf consulta = "PagarCIPDesdeBanco" Then
            Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
            Me.CodigoOficina = DataUtil.ObjectToString(reader("CodigoOficina"))
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.OcultarEmpresa = DataUtil.ObjectToString(reader("OcultarEmpresa"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.Moneda = DataUtil.ObjectToString(reader("Moneda"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.Email = DataUtil.ObjectToString(reader("Email"))
            Me.IdMovimiento = DataUtil.ObjectToInt64(reader("IdMovimiento"))

        End If
    End Sub
    Private _idConciliacion As Integer
    Private _codigoDepositante As String
    Private _fechaCancelacion As DateTime
    Private _codigoSucursal As String
    Private _operacionBancarias As String
    Private _codigoOficina As String
    Private _numeroOperacion As String
    Private _descripcionEstado As String
    Private _numeroOrdenPago As String
    Private _fechaConciliacion As String
    Private _observacion As String
    Private _idOrdenPago As Integer
    'Para la cancelacion de OP
    Private _ocultarEmpresa As String
    Private _descripcionEmpresa As String
    Private _moneda As String
    Private _descripcionServicio As String
    Private _total As Decimal
    Private _conceptoPago As String
    Private _email As String
	<DataMember()> _
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property
	<DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property


	<DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _total
        End Get
        Set(ByVal value As Decimal)
            _total = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescripcionEmpresa() As String
        Get
            Return _descripcionEmpresa
        End Get
        Set(ByVal value As String)
            _descripcionEmpresa = value
        End Set
    End Property
	<DataMember()> _
    Public Property Moneda() As String
        Get
            Return _moneda
        End Get
        Set(ByVal value As String)
            _moneda = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescripcionServicio() As String
        Get
            Return _descripcionServicio
        End Get
        Set(ByVal value As String)
            _descripcionServicio = value
        End Set
    End Property
	<DataMember()> _
    Public Property OcultarEmpresa() As String
        Get
            Return _ocultarEmpresa
        End Get
        Set(ByVal value As String)
            _ocultarEmpresa = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdConciliacion() As Integer
        Get
            Return _idConciliacion
        End Get
        Set(ByVal value As Integer)
            _idConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoDepositante() As String
        Get
            Return _codigoDepositante
        End Get
        Set(ByVal value As String)
            _codigoDepositante = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _fechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCancelacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoSucursal() As String
        Get
            Return _codigoSucursal
        End Get
        Set(ByVal value As String)
            _codigoSucursal = value
        End Set
    End Property

	<DataMember()> _
    Public Property OperacionBancaria() As String
        Get
            Return _operacionBancarias
        End Get
        Set(ByVal value As String)
            _operacionBancarias = value
        End Set
    End Property
	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoOficina() As String
        Get
            Return _codigoOficina
        End Get
        Set(ByVal value As String)
            _codigoOficina = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property FechaConciliacion() As String
        Get
            Return _fechaConciliacion
        End Get
        Set(ByVal value As String)
            _fechaConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _descripcionEstado
        End Get
        Set(ByVal value As String)
            _descripcionEstado = value

        End Set
    End Property
	<DataMember()> _
    Public Property IdOrdenPago() As Int64
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Int64)
            _idOrdenPago = value
        End Set
    End Property

    Private _idMovimiento As Int64
	<DataMember()> _
    Public Property IdMovimiento() As Int64
        Get
            Return _idMovimiento
        End Get
        Set(ByVal value As Int64)
            _idMovimiento = value
        End Set
    End Property
    'Public Class EstadoAgenciaBancariaBEACComparer
    '    Implements IComparer(Of BEAgenciaBancaria)
    '    Public Function Compare(ByVal x As BEAgenciaBancaria, ByVal y As BEAgenciaBancaria) As Integer Implements System.Collections.Generic.IComparer(Of BEAgenciaBancaria).Compare
    '        Return x.DescripcionEstado.CompareTo(y.DescripcionEstado)
    '    End Function
    'End Class



    Public Class NumeroOperacionBEACComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperacionBancaria).NumeroOperacion.CompareTo(CType(y, BEOperacionBancaria).NumeroOperacion)
            'Return CType(x, BERepresentante).Nombres.CompareTo(CType(y, BERepresentante).Nombres)

        End Function
    End Class


    Public Class NumeroOrdenPagoBEACComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperacionBancaria).NumeroOrdenPago.CompareTo(CType(y, BEOperacionBancaria).NumeroOrdenPago)
        End Function
    End Class

    Public Class FechaConciliacionBEACComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperacionBancaria).FechaConciliacion.CompareTo(CType(y, BEOperacionBancaria).FechaConciliacion)
        End Function
    End Class

    Public Class DescripcionEstadoBEACComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperacionBancaria).DescripcionEstado.CompareTo(CType(y, BEOperacionBancaria).DescripcionEstado)
        End Function
    End Class

    Public Class ObservacionBEACComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)

        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperacionBancaria).Observacion.CompareTo(CType(y, BEOperacionBancaria).Observacion)
        End Function
    End Class
End Class
