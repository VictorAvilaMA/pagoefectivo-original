Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()> Public Class BEOperador
    Inherits BEAuditoria
    Private _idOperador As Integer
    Private _Codigo As String
    Private _Descripcion As String
    Private _idEstado As Integer
    Private _DescripcionEstado As String
    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        IdOperador = Convert.ToInt32(reader("IdOperador").ToString)
        Codigo = reader("Codigo").ToString
        Descripcion = reader("Descripcion").ToString
        IdEstado = Convert.ToInt32(reader("IdEstado").ToString)
        DescripcionEstado = reader("DescripcionEstado").ToString
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)

        If (tipo = "ObtenerOperadores") Then
            IdOperador = Convert.ToInt32(reader("IdOperador").ToString)
            Descripcion = reader("Descripcion").ToString
        End If

    End Sub

	<DataMember()> _
    Public Property IdOperador() As Integer
        Get
            Return _idOperador
        End Get
        Set(ByVal value As Integer)
            _idOperador = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property

	<DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _DescripcionEstado
        End Get
        Set(ByVal value As String)
            _DescripcionEstado = value
        End Set
    End Property

    Public Class DescripcionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperador).Descripcion.CompareTo(CType(y, BEOperador).Descripcion)
        End Function
    End Class
    Public Class CodigoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperador).Codigo.CompareTo(CType(y, BEOperador).Codigo)
        End Function
    End Class
    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOperador).DescripcionEstado.CompareTo(CType(y, BEOperador).DescripcionEstado)
        End Function
    End Class
End Class
