Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util



<Serializable()> <DataContract()> Public Class BEOrdenPago
    Inherits BEAuditoria

    <DataMember()> _
    Public Property Token As String
    <DataMember()> _
    Public Property IdEnteGenerador As String
    <DataMember()> _
    Public Property IdEnte As Integer
    <DataMember()> _
    Public Property FechaEmision As DateTime
    <DataMember()> _
    Public Property FechaCancelacion As DateTime
    <DataMember()> _
    Public Property IdOrdenPago As Integer
    <DataMember()> _
    Public Property Total As Decimal
    <DataMember()> _
    Public Property IdMoneda As Integer
    <DataMember()> _
    Public Property IdEmpresa As Integer
    <DataMember()> _
    Public Property IdEstado As Integer
    <DataMember()> _
    Public Property IdCliente As Integer
    <DataMember()> _
    Public Property MailComercio As String
    <DataMember()> _
    Public Property UrlOk As String
    <DataMember()> _
    Public Property UrlError As String
    <DataMember()> _
    Public Property OrderIdComercio As String
    <DataMember()> _
    Public Property NumeroOrdenPago As String

    <DataMember()> _
    Public Property UsuarioID As String
    <DataMember()> _
    Public Property DataAdicional As String
    <DataMember()> _
    Public Property UsuarioNombre As String
    <DataMember()> _
    Public Property UsuarioApellidos As String
    <DataMember()> _
    Public Property UsuarioDomicilio As String
    <DataMember()> _
    Public Property UsuarioProvincia As String
    <DataMember()> _
    Public Property UsuarioPais As String
    <DataMember()> _
    Public Property UsuarioLocalidad As String
    <DataMember()> _
    Public Property UsuarioAlias As String
    <DataMember()> _
    Public Property UsuarioEmail As String
    <DataMember()> _
    Public Property UsuarioNombreReducido As String

    <DataMember()> _
    Public Property FechaVencimiento As DateTime
    <DataMember()> _
    Public Property SimboloMoneda As String
    <DataMember()> _
    Public Property DescripcionEmpresa As String
    <DataMember()> _
    Public Property DescripcionServicio As String
    <DataMember()> _
    Public Property DescripcionEstado As String
    <DataMember()> _
    Public Property DescripcionMoneda As String
    <DataMember()> _
    Public Property IdServicio As Integer

    <DataMember()> _
    Public Property MerchantID As String
    <DataMember()> _
    Public Property DatosEnc As String
    <DataMember()> _
    Public Property ParUtil As String
    <DataMember()> _
    Public Property Url As String
    <DataMember()> _
    Public Property XmlTramaServRuta As String

    <DataMember()> _
    Public Property ConceptoPago As String
    <DataMember()> _
    Public Property DescripcionCliente As String
    <DataMember()> _
    Public Property Dni As String
    <DataMember()> _
    Public Property LogoEmpresa As Byte()
    <DataMember()> _
    Public Property LogoServicio As Byte()
    <DataMember()> _
    Public Property CodigoOrigenCancelacion As String
    <DataMember()> _
    Public Property DetallesOrdenPago As List(Of BEDetalleOrdenPago) = Nothing

    <DataMember()> _
    Public Property OrderBy As String
    <DataMember()> _
    Public Property IsAccending As Boolean
    <DataMember()> _
    Public Property IdTipoOrigenCancelacion As Integer
    <DataMember()> _
    Public Property DescripcionTipoOrigenCancelacion As String
    <DataMember()> _
    Public Property OcultarEmpresa As Boolean
    <DataMember()> _
    Public Property IdTipoNotificacion As Integer

    <DataMember()> _
    Public Property OcultarImagenEmpresa As Boolean
    <DataMember()> _
    Public Property OcultarImagenServicio As Boolean
    <DataMember()> _
    Public Property PermiteOCAgenciaRecaudadora As Boolean
    <DataMember()> _
    Public Property MonedaAgencia As String
    <DataMember()> _
    Public Property MensajeRetorno As String

    'Kgordillo
    <DataMember()> _
    Public Property XmlString As String
    <DataMember()> _
    Public Property UrlServicio As String


    <DataMember()> _
    Public Property IdEstadoConciliacion As Integer

    '<add Peru.Com FaseII>
    <DataMember()> _
    Public Property IdsServicios As String
    <DataMember()> _
    Public Property VersionOrdenPago As Integer

    '
    <DataMember()> _
    Public Property DescripcionEstadoConc As String
    <DataMember()> _
    Public Property FechaConciliacion As DateTime
    <DataMember()> _
    Public Property ArchivoConciliacion As String
    <DataMember()> _
    Public Property CodigoMedioPago As String

    '<add Proc.Tesoreria>
    <DataMember()> _
    Public Property Observacion As String
    <DataMember()> _
    Public Property FlagSeleccionado As Boolean
    <DataMember()> _
    Public Property Comision As Decimal

    'pmonzon
    <DataMember()> _
    Public Property CodigoVoucher As String

    <DataMember()> _
    Public Property CodPlantillaTipoVariacion As String
    <DataMember()> _
    Public Property NombreServicio As String

    'ToGenerado
    <DataMember()> _
    Public Property MotivoDeExpAGen As String


    <DataMember()> _
    Public Property FechaExpirada() As DateTime
    <DataMember()> _
    Public Property FechaEliminado() As DateTime
    <DataMember()> _
    Public Property FechaAnulada() As DateTime


    <DataMember()> _
    Public Property EmailANotifGeneracion() As String


    <DataMember()> _
    Public Property ClaveAPI() As String
    <DataMember()> _
    Public Property ClaveSecreta() As String
    <DataMember()> _
    Public Property TramaSolicitud() As String
    <DataMember()> _
    Public Property IdTramaTipo() As Integer
    <DataMember()> _
    Public Property ServicioIdTipoNotificacion() As Integer
    <DataMember()> _
    Public Property ServicioUsaUsuariosAnonimos() As Boolean
    <DataMember()> _
    Public Property ServicioIdTipoIntegracion() As Integer
    <DataMember()> _
    Public Property TiempoExpiracion() As Decimal
    <DataMember()> _
    Public Property FechaAExpirar() As Date
    <DataMember()> _
    Public Property IdOrigenEliminacion() As Integer
    <DataMember()> _
    Public Property ClienteAlias() As String
    <DataMember()> _
    Public Property ClienteNombre() As String
    <DataMember()> _
    Public Property ClienteApellidos() As String
    <DataMember()> _
    Public Property ClienteIdTipoDocumento() As Integer
    <DataMember()> _
    Public Property ClienteTipoDocumento() As String
    <DataMember()> _
    Public Property ClienteNroDocumento() As String
    <DataMember()> _
    Public Property ClienteTelefono() As String
    <DataMember()> _
    Public Property ClienteEmail() As String
    <DataMember()> _
    Public Property ClienteEmailReducido() As String
    <DataMember()> _
    Public Property CipListoParaExpirar() As Boolean
    <DataMember()> _
    Public Property codMonedaBanco() As String
    <DataMember()> _
    Public Property CodigoBanco() As String
    <DataMember()> _
    Public Property CodigoServicio() As String
    <DataMember()> _
    Public Property oBECliente() As BECliente
    <DataMember()> _
    Public Property oBEServicio() As BEServicio

    Public Sub New()
    End Sub

    <DataMember()> _
    Property ListaTipoOrigenCancelacion As List(Of BETipoOrigenCancelacion)



    Public Sub AsignarValoresConstructor(ByVal reader As IDataReader)
        Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
        Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
        Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
        Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
        Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
        Me.NumeroOrdenPago = reader("NumeroOrdenPago")
        Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
        Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
        Me.FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
        Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
        Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
        Me.UrlOk = DataUtil.ObjectToString(reader("UrlOk"))
        Me.UrlError = DataUtil.ObjectToString(reader("UrlError"))
        Me.MailComercio = DataUtil.ObjectToString(reader("MailComercio"))
        Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
        Me.IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
        Me.IdUsuarioActualizacion = DataUtil.ObjectToInt(reader("IdUsuarioActualizacion"))
    End Sub

    Public Sub New(ByVal reader As IDataReader)
        AsignarValoresConstructor(reader)
        Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
        Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
        Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
        Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
        Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Valor1 As Int32, ByVal Valor2 As Int32)
        Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
        Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
        Me.NombreServicio = DataUtil.ObjectToString(reader("Nombre"))
        Me.DescripcionEstado = DataUtil.ObjectToString(reader("Estado"))
        Me.SimboloMoneda = DataUtil.ObjectToString(reader("Moneda"))
        Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
        Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
        Me.MensajeRetorno = DataUtil.ObjectToString(reader("MensajeRetorno"))
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Consulta As String)

        If (Consulta = "InfoExpiradas") Then
            AsignarValoresConstructor(reader)
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

        ElseIf Consulta = "ConceptoPago" Then
            AsignarValoresConstructor(reader)
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.IdEmpresa = DataUtil.ObjectToInt(reader("IdEmpresa"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.OcultarEmpresa = DataUtil.ObjectToString(reader("OcultarEmpresa")) = "1"
            Dim img As Byte()
            img = reader("ImagenEmpresa")
            Me.OcultarImagenEmpresa = ((Not (img Is Nothing)) And (img.Length = 0))
            img = Nothing
            img = reader("ImagenServicio")
            Me.OcultarImagenServicio = ((Not (img Is Nothing)) And (img.Length = 0))
            img = Nothing

        ElseIf Consulta = "ConsultarOrdenPagoAgenciaBancaria" Then
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.FechaVencimiento = DataUtil.ObjectToDateTime(reader("FechaVencimiento"))
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
            Me.CipListoParaExpirar = DataUtil.ObjectToInt(reader("CipListoParaExpirar")) = 1

        ElseIf Consulta = "ConsultarCIPDesdeBanco" Then
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.FechaVencimiento = DataUtil.ObjectToDateTime(reader("FechaVencimiento"))
            Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
            Me.CipListoParaExpirar = DataUtil.ObjectToInt(reader("CipListoParaExpirar")) = 1
            Me.codMonedaBanco = DataUtil.ObjectToString(reader("codMonedaBanco"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DatoCliente"))
            'Me.Dni = DataUtil.ObjectToString(reader("Dni"))

            Me.CodigoServicio = DataUtil.ObjectToString(reader("CodigoServicio"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.IdCliente = DataUtil.ObjectToString(reader("IdCliente"))
            Me.UsuarioNombre = DataUtil.ObjectToString(reader("UsuarioNombre"))

            'Me.CodigoVoucher = DataUtil.ObjectToString(reader("CodigoVoucher"))

        ElseIf Consulta = "Historial" Then
            AsignarValoresConstructor(reader)
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
            Me.FechaActualizacion = DataUtil.ObjectToString(reader("FechaGeneral"))
            Me.DescripcionTipoOrigenCancelacion = DataUtil.ObjectToString(reader("DescripcionTipoOrigenCancelacion"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
            Me.FechaAnulada = DataUtil.ObjectToDateTime(reader("FechaAnulada"))
            Me.FechaExpirada = DataUtil.ObjectToDateTime(reader("FechaExpirada"))
            Me.FechaEliminado = DataUtil.ObjectToDateTime(reader("FechaEliminado"))

            _clienteAlias = DataUtil.ObjectToString(reader("clienteAlias"))
            _clienteNombre = DataUtil.ObjectToString(reader("clienteNombre"))
            _clienteApellidos = DataUtil.ObjectToString(reader("clienteApellidos"))
            _clienteIdTipoDocumento = DataUtil.ObjectToInt(reader("clienteIdTipoDocumento"))
            _clienteTipoDocumento = DataUtil.ObjectToString(reader("clienteTipoDocumento"))
            _clienteNroDocumento = DataUtil.ObjectToString(reader("clienteNroDocumento"))
            _clienteTelefono = DataUtil.ObjectToString(reader("clienteTelefono"))
            _clienteEmail = DataUtil.ObjectToString(reader("ClienteEmail"))
            _clienteEmailReducido = DataUtil.ObjectToString(reader("ClienteEmailReducido"))
            _orderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
            ' _UsuarioNombreReducido = DataUtil.ObjectToString(reader("ClienteNombreReducido"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

        ElseIf Consulta = "HistorialPG" Then
            AsignarValoresConstructor(reader)
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
            Me.FechaActualizacion = DataUtil.ObjectToString(reader("FechaGeneral"))
            Me.DescripcionTipoOrigenCancelacion = DataUtil.ObjectToString(reader("DescripcionTipoOrigenCancelacion"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
            Me.FechaAnulada = DataUtil.ObjectToDateTime(reader("FechaAnulada"))
            Me.FechaExpirada = DataUtil.ObjectToDateTime(reader("FechaExpirada"))
            Me.FechaEliminado = DataUtil.ObjectToDateTime(reader("FechaEliminado"))

            _clienteAlias = DataUtil.ObjectToString(reader("clienteAlias"))
            _clienteNombre = DataUtil.ObjectToString(reader("clienteNombre"))
            _clienteApellidos = DataUtil.ObjectToString(reader("clienteApellidos"))
            _clienteIdTipoDocumento = DataUtil.ObjectToInt(reader("clienteIdTipoDocumento"))
            _clienteTipoDocumento = DataUtil.ObjectToString(reader("clienteTipoDocumento"))
            _clienteNroDocumento = DataUtil.ObjectToString(reader("clienteNroDocumento"))
            _clienteTelefono = DataUtil.ObjectToString(reader("clienteTelefono"))
            _clienteEmail = DataUtil.ObjectToString(reader("ClienteEmail"))
            _clienteEmailReducido = DataUtil.ObjectToString(reader("ClienteEmailReducido"))
            _orderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
            Me.TotalPageNumbers = DataUtil.ObjectToString(reader("TOTAL_RESULT")) ''AGREGADO PARA LA PAGINACIÓN FASE III
            '  _UsuarioNombreReducido = DataUtil.ObjectToString(reader("ClienteNombreReducido"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

        ElseIf Consulta = "ConsultarOrdenPagoxIdComercio" Then

            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
            Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
            Me.OrderIdComercio = DataUtil.ObjectToInt(reader("OrderIdComercio"))

        ElseIf Consulta = "ConsultarOrdenPagoxNumeroOrdenPago" Then
            ' AsignarValoresConstructor(reader)
            Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))


        ElseIf Consulta = "ConsultarOrdenPagoAvanzada" Then
            AsignarValoresConstructor(reader)
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
            Me.TotalPageNumbers = DataUtil.ObjectToInt(reader("TOTAL_RESULT"))


        ElseIf Consulta = "ConsultaNotificacionMailPOS" Then
            AsignarValoresConstructor(reader)
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("MailCliente"))
            Me.OcultarEmpresa = DataUtil.ObjectToString(reader("OcultarEmpresa")) = "1"

        ElseIf Consulta = "Pendientes" Then
            AsignarValoresConstructor(reader)
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
        ElseIf Consulta = "PendientesPG" Then
            AsignarValoresConstructor(reader)
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.TotalPageNumbers = DataUtil.ObjectToString(reader("TOTAL_RESULT"))
        ElseIf Consulta = "ConsultaPagoServicios" Then
            Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
            Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
            'Me.ConceptoPago = DataUtil.ObjectToDateTime(reader("Fecha"))
            Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("Fecha"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))

        ElseIf Consulta = "ConsultaPreOrdenPago" Then
            Me.Url = DataUtil.ObjectToString(reader("Url"))
            Me.XmlString = DataUtil.ObjectToString(reader("StringXml"))
            Me.UrlServicio = DataUtil.ObjectToString(reader("UrlServicio"))

        ElseIf Consulta = "ConsultaRecepcionarOrdenPago" Then
            Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
            Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
            Me.IdEmpresa = DataUtil.ObjectToInt(reader("IdEmpresaContratante"))
            Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.FechaEmision = Convert.ToDateTime(reader("FechaEmision"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
            Me.MailComercio = DataUtil.ObjectToString(reader("EmailCliente"))
            Me.LogoEmpresa = reader("LogoEmpresa")
            Me.LogoServicio = reader("LogoServicio")
            Me.PermiteOCAgenciaRecaudadora = DataUtil.ObjectToString(reader("OCAgenciaRecadudadora")) = "2" ' El C?digo de Identificaci?n de Pago Esta Permitida a ser cancelada en Agencia Recaudadora
        ElseIf Consulta = "ConsultaNotificarOrdenPago" Then
            Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
            Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
            Me.UrlOk = DataUtil.ObjectToString(reader("UrlOK"))
            Me.UrlError = DataUtil.ObjectToString(reader("UrlError"))
            Me.MailComercio = DataUtil.ObjectToString(reader("MailComercio"))
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.Url = DataUtil.ObjectToString(reader("UrlServicio"))
            Me.IdTipoNotificacion = DataUtil.ObjectToString(reader("IdTipoNotificacion"))

        ElseIf Consulta = "Completo" Then
            'ToGenerado
            If DataUtil.ObjectToString(reader("DescripcionEstado")) IsNot DBNull.Value Then
                Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
            End If
            If DataUtil.ObjectToDateTime(reader("FechaExpirada")).ToString() IsNot DBNull.Value Then
                Me.FechaExpirada = DataUtil.ObjectToDateTime(reader("FechaExpirada"))
            End If
            'ToGenerado Fin
            Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
            Me.IdEnteGenerador = DataUtil.ObjectToInt(reader("IdEnteGenerador"))
            Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
            Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
            Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
            Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
            Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
            Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
            Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
            Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
            Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
            Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
            Me.UrlOk = DataUtil.ObjectToString(reader("UrlOk"))
            Me.UrlError = DataUtil.ObjectToString(reader("UrlError"))
            Me.MailComercio = DataUtil.ObjectToString(reader("MailComercio"))
            Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
            Me.IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
            Me.FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
            Me.IdUsuarioActualizacion = DataUtil.ObjectToInt(reader("IdUsuarioActualizacion"))
            Me.UsuarioID = DataUtil.ObjectToString(reader("UsuarioID"))
            Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
            Me.UsuarioNombre = DataUtil.ObjectToString(reader("UsuarioNombre"))
            Me.UsuarioApellidos = DataUtil.ObjectToString(reader("UsuarioApellidos"))
            Me.UsuarioDomicilio = DataUtil.ObjectToString(reader("UsuarioDomicilio"))
            Me.UsuarioLocalidad = DataUtil.ObjectToString(reader("UsuarioLocalidad"))
            Me.UsuarioProvincia = DataUtil.ObjectToString(reader("UsuarioProvincia"))
            Me.UsuarioPais = DataUtil.ObjectToString(reader("UsuarioPais"))
            Me.UsuarioAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
            Me.UsuarioEmail = DataUtil.ObjectToString(reader("UsuarioEmail"))
            Me.TramaSolicitud = DataUtil.ObjectToString(reader("TramaSolicitud"))
            Me.IdTramaTipo = DataUtil.ObjectToInt(reader("IdTramaTipo"))

            Me.ServicioIdTipoNotificacion = DataUtil.ObjectToInt(reader("IdTipoNotificacion"))
            Me.ServicioUsaUsuariosAnonimos = DataUtil.ObjectToBool(reader("UsaUsuariosAnonimos"))
            Me.ServicioIdTipoIntegracion = DataUtil.ObjectToInt(reader("IdTipoIntegracion"))

            Me.ClaveAPI = DataUtil.ObjectToString(reader("ClaveAPI"))
            Me.ClaveSecreta = DataUtil.ObjectToString(reader("ClaveSecreta"))

            'Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
            Me.FechaVencimiento = DataUtil.ObjectToDateTime(reader("FechaVencimiento"))

            ''''
            Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
            Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
            Me.IdEmpresa = DataUtil.ObjectToInt(reader("IdEmpresa"))
            Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
            Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
            Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
            Me.OcultarEmpresa = DataUtil.ObjectToString(reader("OcultarEmpresa")) = "1"
            Me.CodPlantillaTipoVariacion = DataUtil.ObjectToString(reader("CodPlantillaTipoVariacion"))

            Dim img As Byte()
            img = reader("ImagenEmpresa")
            Me.OcultarImagenEmpresa = ((Not (img Is Nothing)) And (img.Length = 0))
            img = Nothing
            img = reader("ImagenServicio")
            Me.OcultarImagenServicio = ((Not (img Is Nothing)) And (img.Length = 0))
            img = Nothing
            Me.TiempoExpiracion = DataUtil.ObjectToDecimal(reader("TiempoExpiracion"))
            ''''
            ElseIf Consulta = "ConsultarCabeceraPorNumeroOrdenPago" Then
                Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("moneda"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("servicio"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.DescripcionEstado = DataUtil.ObjectToString(reader("Estado"))
                Me.Total = DataUtil.ObjectToDecimal(reader("monto"))
                Me.UsuarioNombre = DataUtil.ObjectToString(reader("UsuarioNombre"))
                Me.UsuarioApellidos = DataUtil.ObjectToString(reader("UsuarioApellidos"))
                Me.UsuarioEmail = DataUtil.ObjectToString(reader("UsuarioEmail"))
                Me.UsuarioAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
                Me.ClienteTipoDocumento = DataUtil.ObjectToString(reader("TipoDocumento"))
                Me.ClienteNroDocumento = DataUtil.ObjectToString(reader("NumeroDocumento"))
                Me.ClienteTelefono = DataUtil.ObjectToString(reader("Telefono"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.FechaAnulada = DataUtil.ObjectToDateTime(reader("FechaAnulada"))
                Me.FechaExpirada = DataUtil.ObjectToDateTime(reader("FechaExpirada"))
                Me.FechaEliminado = DataUtil.ObjectToDateTime(reader("FechaEliminado"))
                Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.DescripcionEstadoConc = DataUtil.ObjectToString(reader("EstadoConciliacion"))
                Me.FechaConciliacion = DataUtil.ObjectToDateTime(reader("FechaConciliacion"))
                Me.ArchivoConciliacion = DataUtil.ObjectToString(reader("ArchivoConciliacion"))
            ElseIf Consulta = "ConsultarPorNumeroPagoYCodigoBanco" Then
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.codMonedaBanco = DataUtil.ObjectToString(reader("CodMoneda"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Monto"))
                Me.CodigoMedioPago = DataUtil.ObjectToString(reader("cdocgrl"))
                '<add Proc.Tesoreria>
            ElseIf Consulta = "OpsConArch" Then
                Me.FechaConciliacion = DataUtil.ObjectToDateTime(reader("Fecha"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("CIP"))
                Me.ArchivoConciliacion = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.DescripcionEstado = DataUtil.ObjectToString(reader("Estado"))
                Me.Observacion = DataUtil.ObjectToString(reader("Observacion"))
                Me.IdOrdenPago = DataUtil.ObjectToInt32(reader("IdOrdenPago"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.Comision = DataUtil.ObjectToDecimal(reader("Comision"))
                Me.FlagSeleccionado = True
            ElseIf Consulta = "DepositoDetalleByIdDeposito" Then
                Me.FechaConciliacion = DataUtil.ObjectToDateTime(reader("Fecha"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("CIP"))
                Me.ArchivoConciliacion = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.DescripcionEstado = DataUtil.ObjectToString(reader("Estado"))
                Me.IdOrdenPago = DataUtil.ObjectToInt32(reader("IdOrdenPago"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.Comision = DataUtil.ObjectToDecimal(reader("Comision"))
            ElseIf Consulta = "MinInfoCIP" Then
                Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.oBEServicio = New BEServicio
                Me.oBEServicio.Nombre = DataUtil.ObjectToString(reader("Nombre"))
                Me.DescripcionEstado = DataUtil.ObjectToString(reader("EstadoDescripcion"))
            Else
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.IdEmpresa = DataUtil.ObjectToInt(reader("IdEmpresaContratante"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
                Me.FechaEmision = Convert.ToDateTime(reader("FechaEmision"))
                Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("DescripcionServicio"))
                Me.DescripcionEstado = DataUtil.ObjectToString(reader("DescripcionEstado"))
                Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("DescripcionEmpresa"))
                Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
                Me.MailComercio = DataUtil.ObjectToString(reader("EmailCliente"))
                Me.LogoEmpresa = reader("LogoEmpresa")
                Me.LogoServicio = reader("LogoServicio")
                '
            End If
            '
    End Sub


    Public Class FechaEmisionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).FechaEmision.CompareTo(CType(y, BEOrdenPago).FechaEmision)
        End Function
    End Class
    Public Class FechaCancelacionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).FechaCancelacion.CompareTo(CType(y, BEOrdenPago).FechaCancelacion)
        End Function
    End Class
    Public Class DescripcionEmpresaComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).DescripcionEmpresa.CompareTo(CType(y, BEOrdenPago).DescripcionEmpresa)
        End Function
    End Class
    Public Class DescripcionServicioComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).DescripcionServicio.CompareTo(CType(y, BEOrdenPago).DescripcionServicio)
        End Function
    End Class
    Public Class NumeroOrdenPagoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).NumeroOrdenPago.CompareTo(CType(y, BEOrdenPago).NumeroOrdenPago)
        End Function
    End Class
    Public Class SimboloMonedaComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).SimboloMoneda.CompareTo(CType(y, BEOrdenPago).SimboloMoneda)
        End Function
    End Class
    Public Class TotalComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).Total.CompareTo(CType(y, BEOrdenPago).Total)
        End Function
    End Class
    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).DescripcionEstado.CompareTo(CType(y, BEOrdenPago).DescripcionEstado)
        End Function
    End Class
    ''
    Public Class DescripcionServicioBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.DescripcionServicio.CompareTo(y.DescripcionServicio)
        End Function
    End Class
    Public Class FechaEmisionBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.FechaEmision.CompareTo(y.FechaEmision)
        End Function
    End Class
    Public Class FechaCancelacionBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.FechaCancelacion.CompareTo(y.FechaCancelacion)
        End Function
    End Class
    Public Class NumeroOrdenPagoBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.NumeroOrdenPago.CompareTo(y.NumeroOrdenPago)
        End Function
    End Class

    Public Class SimboloMonedaBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.SimboloMoneda.CompareTo(y.SimboloMoneda)
        End Function
    End Class

    Public Class TotalBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.Total.CompareTo(y.Total)
        End Function
    End Class

    Public Class ConceptoPagoBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.ConceptoPago.CompareTo(y.ConceptoPago)
        End Function
    End Class

    Public Class DescripcionClienteBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.DescripcionCliente.CompareTo(y.DescripcionCliente)
        End Function
    End Class

    Public Class DescripcionEstadoBEOPComparer
        Implements IComparer(Of BEOrdenPago)
        Public Function Compare(ByVal x As BEOrdenPago, ByVal y As BEOrdenPago) As Integer Implements System.Collections.Generic.IComparer(Of BEOrdenPago).Compare
            Return x.DescripcionEstado.CompareTo(y.DescripcionEstado)
        End Function
    End Class

    Public Class ClienteEmailReducidoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).ClienteEmailReducido.CompareTo(CType(y, BEOrdenPago).ClienteEmailReducido)
        End Function
    End Class

    Public Class ClienteEmailComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).ClienteEmail.CompareTo(CType(y, BEOrdenPago).ClienteEmail)
        End Function
    End Class

    Public Class DescripcionClienteComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEOrdenPago).DescripcionCliente.CompareTo(CType(y, BEOrdenPago).DescripcionCliente)
        End Function
    End Class

End Class


