Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util


<Serializable()> <DataContract()> Public Class BEPagoServicio
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Private _fechaInicio As DateTime
    Private _fechaCancelacion As DateTime
    Private _fechaEmision As DateTime
    Private _fechaConciliacion As DateTime
    Private _idOrdenPago As Integer
    Private _total As Decimal
    Private _idMoneda As Integer
    Private _idEmpresa As Integer
    Private _idEstado As Integer
    Private _idCliente As Integer
    Private _mailComercio As String
    Private _urlOk As String
    Private _urlError As String
    Private _orderIdComercio As String
    Private _numeroOrdenPago As String

    Private _simboloMoneda As String
    Private _descripcionEmpresa As String
    Private _descripcionServicio As String
    Private _descripcionEstado As String
    Private _idServicio As Integer

    Private _descripcionCliente As String
    Private _descripcionAgencia As String

    Private _idTipoOrigenCancelacion As Integer
    Private _descripcionTipoOrigenCancelacion As String

    Private _MerchantID As String
    Private _fechaAnulada As DateTime
    Private _fechaEliminado As DateTime
    Private _fechaExpirada As DateTime
    Private _tiempoExpiracion As Decimal
    Private _idEstadoConciliacion As Integer
    Private _idIdUsuarioCreacion As Integer
    Private _DataAdicional As String
    Private _UsuarioNombre As String
    Private _UsuarioApellidos As String
    Private _UsuarioDomicilio As String
    Private _UsuarioProvincia As String
    Private _UsuarioPais As String
    Private _UsuarioLocalidad As String
    Private _UsuarioAlias As String
    Private _UsuarioEmail As String

    Private _fechaFin As DateTime
    Private _tipo As String
    Private _cips As String
    Private _ConceptoPago As String


    '--agregar los nuevos campos formulario

    Private _DataFormularioCelular As String
    Private _DataFormularioAnexo As String
    Private _DataFormularioDatoAdicional As String
    Private _DataFormularioTipoComprobante As String
    Private _DataFormularioRuc As String

    Private _DataFormularioRazonSocial As String
    Private _DataFormularioDireccion As String
    Private _DataFormularioCanalPago As String
    Private _DataFormularioIdDetalle As String
    Private _DataFormularioProducto As String
    Private _DataFormularioImporte As Decimal
    Private _DataFormularioCantidad As String
    Private _DataFormularioSubtotal As Decimal
    Private _DataFormularioTelefono As String
    Private _DataFormularioDireccionUsuario As String









    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
        Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
        Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
        Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
        Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
        Me.NumeroOrdenPago = reader("NumeroOrdenPago")
        Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
        Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
        Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
        Me.DescripcionServicio = DataUtil.ObjectToString(reader("Servicio"))
        Me.DescripcionAgencia = DataUtil.ObjectToString(reader("Agencia"))
        Me.DescripcionCliente = DataUtil.ObjectToString(reader("Email"))
        Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
        Me.DescripcionTipoOrigenCancelacion = DataUtil.ObjectToString(reader("DescripcionTipoOrigenCancelacion"))


        '_clienteAlias = DataUtil.ObjectToString(reader("clienteAlias"))
        '_clienteNombre = DataUtil.ObjectToString(reader("clienteNombre"))
        '_clienteApellidos = DataUtil.ObjectToString(reader("clienteApellidos"))
        '_clienteIdTipoDocumento = DataUtil.ObjectToInt(reader("clienteIdTipoDocumento"))
        '_clienteTipoDocumento = DataUtil.ObjectToString(reader("clienteTipoDocumento"))
        '_clienteNroDocumento = DataUtil.ObjectToString(reader("clienteNroDocumento"))
        '_clienteTelefono = DataUtil.ObjectToString(reader("clienteTelefono"))
        '_clienteEmail = DataUtil.ObjectToString(reader("ClienteEmail"))
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal Consulta As String)
        Select Case Consulta
            Case "ConsOPsByServ"
                Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.FechaConciliacion = DataUtil.ObjectToDateTime(reader("FechaConciliacion"))
                Me.IdEstadoConciliacion = DataUtil.ObjectToInt(reader("IdEstadoConciliacion"))
                Me.IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
                Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
                Me.UsuarioNombre = DataUtil.ObjectToString(reader("UsuarioNombre"))
                Me.UsuarioApellidos = DataUtil.ObjectToString(reader("UsuarioApellidos"))
                Me.UsuarioAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
                Me.UsuarioEmail = DataUtil.ObjectToString(reader("UsuarioEmail"))

            Case "ConsOPsByCIP"
                Me.IdOrdenPago = DataUtil.ObjectToInt64(reader("IdOrdenPago"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.FechaConciliacion = DataUtil.ObjectToDateTime(reader("FechaConciliacion"))
                Me.FechaAnulada = DataUtil.ObjectToDateTime(reader("FechaAnulada"))
                Me.FechaExpirada = DataUtil.ObjectToDateTime(reader("FechaExpirada"))
                Me.FechaEliminado = DataUtil.ObjectToDateTime(reader("FechaEliminado"))
                Me.TiempoExpiracion = DataUtil.ObjectToDecimal(reader("TiempoExpiracion"))
                Me.IdEstadoConciliacion = DataUtil.ObjectToInt(reader("IdEstadoConciliacion"))
                Me.MailComercio = DataUtil.ObjectToString(reader("MailComercio"))
                Me.IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
                Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
                Me.UsuarioNombre = DataUtil.ObjectToString(reader("UsuarioNombre"))
                Me.UsuarioApellidos = DataUtil.ObjectToString(reader("UsuarioApellidos"))
                Me.UsuarioAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
                Me.UsuarioEmail = DataUtil.ObjectToString(reader("UsuarioEmail"))
                Me.UsuarioDomicilio = DataUtil.ObjectToString(reader("UsuarioDomicilio"))
                Me.UsuarioLocalidad = DataUtil.ObjectToString(reader("UsuarioLocalidad"))
                Me.UsuarioProvincia = DataUtil.ObjectToString(reader("UsuarioProvincia"))
                Me.UsuarioPais = DataUtil.ObjectToString(reader("UsuarioPais"))

            Case "ConsPagServPag"
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.NumeroOrdenPago = reader("NumeroOrdenPago")
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("Servicio"))
                Me.DescripcionAgencia = DataUtil.ObjectToString(reader("Agencia"))
                Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
                Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
                Me.DescripcionTipoOrigenCancelacion = DataUtil.ObjectToString(reader("DescripcionTipoOrigenCancelacion"))
                Me.TotalPageNumbers = DataUtil.ObjectToInt32(reader("TOTAL_RESULT"))
                Me.ClienteNombre = DataUtil.ObjectToString(reader("Nombres"))
                Me.ClienteApellidos = DataUtil.ObjectToString(reader("Apellidos"))
                Me.ClienteTipoDocumento = DataUtil.ObjectToString(reader("TipoDocumento"))
                Me.ClienteNroDocumento = DataUtil.ObjectToString(reader("NumeroDocumento"))
                Me.ClienteAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
                Me.ClienteTelefono = DataUtil.ObjectToString(reader("Telefono"))
                Me.ClienteEmail = DataUtil.ObjectToString(reader("Email"))
                Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

                'Nuevos campos formulario------------------

                'Private _DataFormualrioCelular As String
                Me.DataFormularioCelular = DataUtil.ObjectToString(reader("Celular"))

                Me.DataFormularioTelefono = DataUtil.ObjectToString(reader("DataTelefono"))

                'Private _DataFormularioAnexo As String
                Me.DataFormularioAnexo = DataUtil.ObjectToString(reader("Anexo"))

                Me.DataFormularioDireccionUsuario = DataUtil.ObjectToString(reader("DireccionUsuario"))

                'Private _DataFormularioDatoAdicional As String
                Me.DataFormularioDatoAdicional = DataUtil.ObjectToString(reader("DatoAdicional"))

                'Private _DataFormularioTipoComprobante As String
                Me.DataFormularioTipoComprobante = DataUtil.ObjectToString(reader("TipoComprobante"))

                'Private _DataFormularioRuc As String
                Me.DataFormularioRuc = DataUtil.ObjectToString(reader("Ruc"))

                'Private _DataFormularioRazonSocial As String
                Me.DataFormularioRazonSocial = DataUtil.ObjectToString(reader("RazonSocial"))

                'Private _DataFormularioDireccion As String
                Me.DataFormularioDireccion = DataUtil.ObjectToString(reader("Direccion"))

                'Private _DataFormularioCanalPago As String
                Me.DataFormularioCanalPago = DataUtil.ObjectToString(reader("MetodoPago"))

            Case "ConsPagServPagFormulario"

                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.NumeroOrdenPago = reader("NumeroOrdenPago")
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("Servicio"))
                Me.DescripcionAgencia = DataUtil.ObjectToString(reader("Agencia"))
                Me.DescripcionCliente = DataUtil.ObjectToString(reader("DescripcionCliente"))
                Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
                Me.DescripcionTipoOrigenCancelacion = DataUtil.ObjectToString(reader("DescripcionTipoOrigenCancelacion"))
                Me.TotalPageNumbers = DataUtil.ObjectToInt32(reader("TOTAL_RESULT"))
                Me.ClienteNombre = DataUtil.ObjectToString(reader("Nombres"))
                Me.ClienteApellidos = DataUtil.ObjectToString(reader("Apellidos"))
                Me.ClienteTipoDocumento = DataUtil.ObjectToString(reader("TipoDocumento"))
                Me.ClienteNroDocumento = DataUtil.ObjectToString(reader("NumeroDocumento"))
                Me.ClienteAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
                Me.ClienteTelefono = DataUtil.ObjectToString(reader("Telefono"))
                Me.ClienteEmail = DataUtil.ObjectToString(reader("Email"))
                Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))


                'Private _DataFormualrioCelular As String
                Me.DataFormularioCelular = DataUtil.ObjectToString(reader("Celular"))


                Me.DataFormularioTelefono = DataUtil.ObjectToString(reader("DataTelefono"))

                'Private _DataFormularioAnexo As String
                Me.DataFormularioAnexo = DataUtil.ObjectToString(reader("Anexo"))

                Me.DataFormularioDireccionUsuario = DataUtil.ObjectToString(reader("DireccionUsuario"))

                'Private _DataFormularioDatoAdicional As String
                Me.DataFormularioDatoAdicional = DataUtil.ObjectToString(reader("DatoAdicional"))


                'Private _DataFormularioTipoComprobante As String
                Me.DataFormularioTipoComprobante = DataUtil.ObjectToString(reader("TipoComprobante"))


                'Private _DataFormularioRuc As String
                Me.DataFormularioRuc = DataUtil.ObjectToString(reader("Ruc"))

                'Private _DataFormularioRazonSocial As String
                Me.DataFormularioRazonSocial = DataUtil.ObjectToString(reader("RazonSocial"))


                'Private _DataFormularioDireccion As String
                Me.DataFormularioDireccion = DataUtil.ObjectToString(reader("Direccion"))


                'Private _DataFormularioCanalPago As String
                Me.DataFormularioCanalPago = DataUtil.ObjectToString(reader("MetodoPago"))


                'Private _DataFormuluraioIdDetalle As Integer
                Me.DataFormularioIdDetalle = DataUtil.ObjectToInt32(reader("IdDetalle"))


                'Private _DataAdicionalProducto As String
                Me.DataFormularioProducto = DataUtil.ObjectToString(reader("Producto"))


                'Private _DataAdicionalImporte As Decimal
                Me.DataFormularioImporte = DataUtil.ObjectToDecimal(reader("Importe"))

                'Private _DataAdiconalCantidad As String
                Me.DataFormularioCantidad = DataUtil.ObjectToString(reader("Cantidad"))

                'Private _DataAdicionalSubtotal As Decimal
                Me.DataFormularioSubtotal = DataUtil.ObjectToDecimal(reader("Subtotal"))



        End Select


    End Sub

    <DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As DateTime)
            _fechaFin = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _fechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCancelacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaConciliacion() As DateTime
        Get
            Return _fechaConciliacion
        End Get
        Set(ByVal value As DateTime)
            _fechaConciliacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _fechaEmision
        End Get
        Set(ByVal value As DateTime)
            _fechaEmision = value
        End Set
    End Property

    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _total
        End Get
        Set(ByVal value As Decimal)
            _total = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property


    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property


    <DataMember()> _
    Public Property SimboloMoneda() As String
        Get
            Return _simboloMoneda
        End Get
        Set(ByVal value As String)
            _simboloMoneda = value

        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionServicio() As String
        Get
            Return _descripcionServicio
        End Get
        Set(ByVal value As String)
            _descripcionServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionAgencia() As String
        Get
            Return _descripcionAgencia
        End Get
        Set(ByVal value As String)
            _descripcionAgencia = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionCliente() As String
        Get
            Return _descripcionCliente
        End Get
        Set(ByVal value As String)
            _descripcionCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEmpresa() As Integer
        Get
            Return _idEmpresa
        End Get
        Set(ByVal value As Integer)
            _idEmpresa = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdTipoOrigenCancelacion() As Integer
        Get
            Return _idTipoOrigenCancelacion
        End Get
        Set(ByVal value As Integer)
            _idTipoOrigenCancelacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionTipoOrigenCancelacion() As String
        Get
            Return _descripcionTipoOrigenCancelacion
        End Get
        Set(ByVal value As String)
            _descripcionTipoOrigenCancelacion = value
        End Set
    End Property

    Private _clienteAlias As String
    <DataMember()> _
    Public Property ClienteAlias() As String
        Get
            Return _clienteAlias
        End Get
        Set(ByVal value As String)
            _clienteAlias = value
        End Set
    End Property

    Private _clienteNombre As String
    <DataMember()> _
    Public Property ClienteNombre() As String
        Get
            Return _clienteNombre
        End Get
        Set(ByVal value As String)
            _clienteNombre = value
        End Set
    End Property

    Private _clienteApellidos As String
    <DataMember()> _
    Public Property ClienteApellidos() As String
        Get
            Return _clienteApellidos
        End Get
        Set(ByVal value As String)
            _clienteApellidos = value
        End Set
    End Property

    Private _clienteIdTipoDocumento As Integer
    <DataMember()> _
    Public Property ClienteIdTipoDocumento() As Integer
        Get
            Return _clienteIdTipoDocumento
        End Get
        Set(ByVal value As Integer)
            _clienteIdTipoDocumento = value
        End Set
    End Property

    Private _clienteTipoDocumento As String
    <DataMember()> _
    Public Property ClienteTipoDocumento() As String
        Get
            Return _clienteTipoDocumento
        End Get
        Set(ByVal value As String)
            _clienteTipoDocumento = value
        End Set
    End Property

    Private _clienteNroDocumento As String
    <DataMember()> _
    Public Property ClienteNroDocumento() As String
        Get
            Return _clienteNroDocumento
        End Get
        Set(ByVal value As String)
            _clienteNroDocumento = value
        End Set
    End Property

    Private _clienteTelefono As String
    <DataMember()> _
    Public Property ClienteTelefono() As String
        Get
            Return _clienteTelefono
        End Get
        Set(ByVal value As String)
            _clienteTelefono = value
        End Set
    End Property

    Private _clienteEmail As String
    <DataMember()> _
    Public Property ClienteEmail() As String
        Get
            Return _clienteEmail
        End Get
        Set(ByVal value As String)
            _clienteEmail = value
        End Set
    End Property


    Public Class DescripcionServicioComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).DescripcionServicio.CompareTo(CType(y, BEPagoServicio).DescripcionServicio)
        End Function
    End Class

    Public Class DescripcionAgenciaComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).DescripcionAgencia.CompareTo(CType(y, BEPagoServicio).DescripcionAgencia)
        End Function
    End Class
    Public Class DescripcionClienteComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).DescripcionCliente.CompareTo(CType(y, BEPagoServicio).DescripcionCliente)
        End Function
    End Class

    Public Class FechaEmisionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).FechaEmision.CompareTo(CType(y, BEPagoServicio).FechaEmision)
        End Function
    End Class

    Public Class FechaCancelacionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).FechaCancelacion.CompareTo(CType(y, BEPagoServicio).FechaCancelacion)
        End Function
    End Class

    Public Class NumeroOrdenPagoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).NumeroOrdenPago.CompareTo(CType(y, BEPagoServicio).NumeroOrdenPago)
        End Function
    End Class

    Public Class SimboloMonedaComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).SimboloMoneda.CompareTo(CType(y, BEPagoServicio).SimboloMoneda)
        End Function
    End Class
    Public Class TotalComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEPagoServicio).Total.CompareTo(CType(y, BEPagoServicio).Total)
        End Function
    End Class
    <DataMember()> _
    Public Property OrderIdComercio() As String
        Get
            Return _orderIdComercio
        End Get
        Set(ByVal value As String)
            _orderIdComercio = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEstadoConciliacion() As Integer
        Get
            Return _idEstadoConciliacion
        End Get
        Set(ByVal value As Integer)
            _idEstadoConciliacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdUsuarioCreacion() As Integer
        Get
            Return _idIdUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _idIdUsuarioCreacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property DataAdicional() As String
        Get
            Return _DataAdicional
        End Get
        Set(ByVal value As String)
            _DataAdicional = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioNombre() As String
        Get
            Return _UsuarioNombre
        End Get
        Set(ByVal value As String)
            _UsuarioNombre = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioApellidos() As String
        Get
            Return _UsuarioApellidos
        End Get
        Set(ByVal value As String)
            _UsuarioApellidos = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioDomicilio() As String
        Get
            Return _UsuarioDomicilio
        End Get
        Set(ByVal value As String)
            _UsuarioDomicilio = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioProvincia() As String
        Get
            Return _UsuarioProvincia
        End Get
        Set(ByVal value As String)
            _UsuarioProvincia = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioPais() As String
        Get
            Return _UsuarioPais
        End Get
        Set(ByVal value As String)
            _UsuarioPais = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioAlias() As String
        Get
            Return _UsuarioAlias
        End Get
        Set(ByVal value As String)
            _UsuarioAlias = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioLocalidad() As String
        Get
            Return _UsuarioLocalidad
        End Get
        Set(ByVal value As String)
            _UsuarioLocalidad = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioEmail() As String
        Get
            Return _UsuarioEmail
        End Get
        Set(ByVal value As String)
            _UsuarioEmail = value
        End Set
    End Property
    <DataMember()> _
    Public Property MerchantID() As String
        Get
            Return _MerchantID
        End Get
        Set(ByVal value As String)
            _MerchantID = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaAnulada() As DateTime
        Get
            Return _fechaAnulada
        End Get
        Set(ByVal value As DateTime)
            _fechaAnulada = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaExpirada() As DateTime
        Get
            Return _fechaExpirada
        End Get
        Set(ByVal value As DateTime)
            _fechaExpirada = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEliminado() As DateTime
        Get
            Return _fechaEliminado
        End Get
        Set(ByVal value As DateTime)
            _fechaEliminado = value
        End Set
    End Property
    <DataMember()> _
    Public Property TiempoExpiracion() As Decimal
        Get
            Return _tiempoExpiracion
        End Get
        Set(ByVal value As Decimal)
            _tiempoExpiracion = value
        End Set
    End Property
    <DataMember()> _
    Public Property MailComercio() As String
        Get
            Return _mailComercio
        End Get
        Set(ByVal value As String)
            _mailComercio = value
        End Set
    End Property

    <DataMember()> _
    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property
    <DataMember()> _
    Public Property CIPS() As String
        Get
            Return _cips
        End Get
        Set(ByVal value As String)
            _cips = value
        End Set
    End Property
    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property



    '--agregar los nuevos campos formulario.------------------------

    <DataMember()> _
    Public Property DataFormularioCelular() As String
        Get
            Return _DataFormularioCelular
        End Get
        Set(ByVal value As String)
            _DataFormularioCelular = value
        End Set
    End Property


    <DataMember()> _
    Public Property DataFormularioAnexo() As String
        Get
            Return _DataFormularioAnexo
        End Get
        Set(ByVal value As String)
            _DataFormularioAnexo = value
        End Set
    End Property


    <DataMember()> _
    Public Property DataFormularioDatoAdicional() As String
        Get
            Return _DataFormularioDatoAdicional
        End Get
        Set(ByVal value As String)
            _DataFormularioDatoAdicional = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioTipoComprobante() As String
        Get
            Return _DataFormularioTipoComprobante
        End Get
        Set(ByVal value As String)
            _DataFormularioTipoComprobante = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioRuc() As String
        Get
            Return _DataFormularioRuc
        End Get
        Set(ByVal value As String)
            _DataFormularioRuc = value
        End Set
    End Property


    <DataMember()> _
    Public Property DataFormularioRazonSocial() As String
        Get
            Return _DataFormularioRazonSocial
        End Get
        Set(ByVal value As String)
            _DataFormularioRazonSocial = value
        End Set
    End Property


    <DataMember()> _
    Public Property DataFormularioDireccion() As String
        Get
            Return _DataFormularioDireccion
        End Get
        Set(ByVal value As String)
            _DataFormularioDireccion = value
        End Set
    End Property


    <DataMember()> _
    Public Property DataFormularioCanalPago() As String
        Get
            Return _DataFormularioCanalPago
        End Get
        Set(ByVal value As String)
            _DataFormularioCanalPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioIdDetalle() As String
        Get
            Return _DataFormularioIdDetalle
        End Get
        Set(ByVal value As String)
            _DataFormularioIdDetalle = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioProducto() As String
        Get
            Return _DataFormularioProducto
        End Get
        Set(ByVal value As String)
            _DataFormularioProducto = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioImporte() As Decimal
        Get
            Return _DataFormularioImporte
        End Get
        Set(ByVal value As Decimal)
            _DataFormularioImporte = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioCantidad() As String
        Get
            Return _DataFormularioCantidad
        End Get
        Set(ByVal value As String)
            _DataFormularioCantidad = value
        End Set
    End Property

    <DataMember()> _
    Public Property DataFormularioSubtotal() As Decimal
        Get
            Return _DataFormularioSubtotal
        End Get
        Set(ByVal value As Decimal)
            _DataFormularioSubtotal = value
        End Set
    End Property


    <DataMember()> _
    Public Property DataFormularioTelefono() As String
        Get
            Return _DataFormularioTelefono
        End Get
        Set(ByVal value As String)
            _DataFormularioTelefono = value
        End Set
    End Property



    <DataMember()> _
    Public Property DataFormularioDireccionUsuario() As String
        Get
            Return _DataFormularioDireccionUsuario
        End Get
        Set(ByVal value As String)
            _DataFormularioDireccionUsuario = value
        End Set
    End Property

End Class