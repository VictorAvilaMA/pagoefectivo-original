Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BEPais

    Private _idPais As Integer
    Private _descripcion As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdPais = Convert.ToInt32(reader("IdPais").ToString)
        Me.Descripcion = reader("Descripcion").ToString
    End Sub

	<DataMember()> _
    Public Property IdPais() As Integer
        Get
            Return _idPais
        End Get
        Set(ByVal value As Integer)
            _idPais = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class
