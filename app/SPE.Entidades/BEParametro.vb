Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> Public Class BEParametro

    Private _id As Integer
    Private _grupoCodigo As String
    Private _descripcion As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)

        Me.Id = Convert.ToInt32(reader("Id").ToString)
        Me.GrupoCodigo = reader("GrupoCodigo").ToString
        Me.Descripcion = reader("Descripcion").ToString
    End Sub
    <DataMember()> _
    Public Property Id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    <DataMember()> _
    Public Property GrupoCodigo() As String
        Get
            Return _grupoCodigo

        End Get
        Set(ByVal value As String)
            _grupoCodigo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class


<Serializable()> <DataContract()> Public Class BEParametroMonedero

    Private _id As Integer
    Private _Descripcion As String
    Private _ValorInteger As Integer
    Private _ValorDecimal As Decimal
    Private _ValorString As String

    <DataMember()> _
    Public Property IdParametro() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property


    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion

        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property



    <DataMember()> _
    Public Property ValorInteger() As Integer
        Get
            Return _ValorInteger
        End Get
        Set(ByVal value As Integer)
            _ValorInteger = value
        End Set
    End Property

    <DataMember()> _
    Public Property ValorDecimal() As Decimal
        Get
            Return _ValorDecimal
        End Get
        Set(ByVal value As Decimal)
            _ValorDecimal = value
        End Set
    End Property

    <DataMember()> _
    Public Property ValorString() As String
        Get
            Return _ValorString
        End Get
        Set(ByVal value As String)
            _ValorString = value
        End Set
    End Property



    Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdParametro = DataUtil.ObjectToInt32(reader("IdParametro"))
        Me.Descripcion = DataUtil.ObjectToString(reader("Descripcion"))
        Me.ValorDecimal = DataUtil.ObjectToDecimal(reader("ValorDecimal"))
        Me.ValorInteger = DataUtil.ObjectToInt32(reader("ValorInteger"))
        Me.ValorString = DataUtil.ObjectToString(reader("ValorString"))
    End Sub

End Class
