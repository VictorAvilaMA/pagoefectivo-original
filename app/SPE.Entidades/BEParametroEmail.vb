﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util


<Serializable()> <DataContract()>
Public Class BEParametroEmail    
    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdSolicitudPago = Convert.ToInt32(reader("IdSolicitud"))
        Me.Valor = Convert.ToString(reader("Valor"))
        Me.Nombre = Convert.ToString(reader("Nombre"))
    End Sub

    Private _idSolicitudPago As String
    <DataMember()> _
    Public Property IdSolicitudPago() As String
        Get
            Return _idSolicitudPago
        End Get
        Set(ByVal value As String)
            _idSolicitudPago = value

        End Set
    End Property
    Private _nombre As String
    <DataMember()> _
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value

        End Set
    End Property
    Private _valor As String
    <DataMember()> _
    Public Property Valor() As String
        Get
            Return _valor
        End Get
        Set(ByVal value As String)
            _valor = value

        End Set
    End Property
End Class
