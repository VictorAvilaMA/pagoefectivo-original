Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> Public Class BEPasarela
    Inherits BEAuditoria

    Enum varEstado
        Pendiente = 0
        Errado = 1
        Aceptado = 2
        Rechazado = 3
    End Enum

    Private _cclave As String
    Private _idPasarela As Integer
    Private _idServicio As Integer
    Private _idPasMedPago As Integer
    Private _OrdenID As String
    Private _Monto As Decimal
    Private _UsuarioID As String
    Private _URLOk As String
    Private _URLError As String
    Private _idEstado As varEstado
    Private _observacion As String
    Private _respuesta As String

    Public Sub New()

    End Sub

	<DataMember()> _
    Public Property CClave() As String
        Get
            Return _cclave
        End Get
        Set(ByVal value As String)
            _cclave = value
        End Set
    End Property

	<DataMember()> _
    Public Property IDPasarela() As Integer
        Get
            Return _idPasarela
        End Get
        Set(ByVal value As Integer)
            _idPasarela = value
        End Set
    End Property

	<DataMember()> _
    Public Property IDServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

	<DataMember()> _
    Public Property IDPasarelaMedPago() As Integer
        Get
            Return _idPasMedPago
        End Get
        Set(ByVal value As Integer)
            _idPasMedPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrdenID() As String
        Get
            Return _OrdenID
        End Get
        Set(ByVal value As String)
            _OrdenID = value
        End Set
    End Property

	<DataMember()> _
    Public Property URLOk() As String
        Get
            Return _URLOk
        End Get
        Set(ByVal value As String)
            _URLOk = value
        End Set
    End Property

	<DataMember()> _
    Public Property URLError() As String
        Get
            Return _URLError
        End Get
        Set(ByVal value As String)
            _URLError = value
        End Set
    End Property

	<DataMember()> _
    Public Property Estado() As varEstado
        Get
            Return _idEstado
        End Get
        Set(ByVal value As varEstado)
            _idEstado = value
        End Set
    End Property

	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

	<DataMember()> _
    Public Property Respuesta() As String
        Get
            Return _respuesta
        End Get
        Set(ByVal value As String)
            _respuesta = value
        End Set
    End Property


End Class
