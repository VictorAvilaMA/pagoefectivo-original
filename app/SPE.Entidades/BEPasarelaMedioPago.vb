Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> _
Public Class BEPasarelaMedioPago

  Private _idPasarelaMedioPago As Integer
  Private _descripcion As String
  Private _idEstado As Integer

  Public Sub New()
  End Sub

  Public Sub New(ByVal reader As IDataReader)
    Me.IdPasarelaMedioPago = Convert.ToInt32(reader("idPasarelaMedioPago").ToString())
    Me.Descripcion = reader("Descripcion").ToString()
  End Sub

  Public Sub New(ByVal reader As IDataReader, ByVal parametro As String)
    If parametro = "Carga" Then
      Me.IdPasarelaMedioPago = Convert.ToInt32(reader("idPasarelaMedioPago").ToString())
    End If
  End Sub

	<DataMember()> _
  Public Property IdPasarelaMedioPago() As Integer
    Get
      Return _idPasarelaMedioPago
    End Get
    Set(ByVal value As Integer)
      _idPasarelaMedioPago = value
    End Set
  End Property

	<DataMember()> _
  Public Property Descripcion() As String
    Get
      Return _descripcion
    End Get
    Set(ByVal value As String)
      _descripcion = value
    End Set
  End Property

	<DataMember()> _
  Public Property IdEstado() As Integer
    Get
      Return _idEstado
    End Get
    Set(ByVal value As Integer)
      _idEstado = value
    End Set
  End Property
End Class
