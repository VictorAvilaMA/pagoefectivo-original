﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> Public Class BEPeriodoLiquidacion

    Private _IdPeriodo As Integer
    Private _Descripcion As String
    Private _IdEstado As Integer
    Private _DiasLiquidacion As String
    Private _FechasFin As String
    Private _FechaCreacion As Date

    'detalles
    Private _DesEstado As String
    Private _ValoresPeriodo As String


    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "PeriodoLiquidacion"
                Me.IdPeriodo = DataUtil.ObjectToInt32(reader("IdPeriodo"))
                Me.Descripcion = DataUtil.ObjectToString(reader("Descripcion"))
                Me.IdEstado = DataUtil.ObjectToInt32(reader("IdEstado"))
                Me.DiasLiquidacion = DataUtil.ObjectToString(reader("DiasLiquidacion"))
                Me.FechasFin = DataUtil.ObjectToString(reader("FechasFin"))
            Case "parametros"
                Me.IdPeriodo = DataUtil.ObjectToInt32(reader("IdPeriodo"))
                Me.Descripcion = DataUtil.ObjectToString(reader("Descripcion"))
            Case "ListaDetalle"
                Me.IdPeriodo = DataUtil.ObjectToInt32(reader("IdPeriodo"))
                Me.Descripcion = DataUtil.ObjectToString(reader("Descripcion"))
                Me.IdEstado = DataUtil.ObjectToInt32(reader("IdEstado"))
                Me.DesEstado = DataUtil.ObjectToString(reader("DesEstado"))
                Me.DiasLiquidacion = DataUtil.ObjectToString(reader("DiasLiquidacion"))
                Me.FechasFin = DataUtil.ObjectToString(reader("FechasFin"))
                Me.ValoresPeriodo = DataUtil.ObjectToString(reader("ValoresPeriodo"))
        End Select
    End Sub



    <DataMember()> _
    Public Property IdPeriodo() As Integer
        Get
            Return _IdPeriodo
        End Get
        Set(ByVal value As Integer)
            _IdPeriodo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property DiasLiquidacion() As String
        Get
            Return _DiasLiquidacion
        End Get
        Set(ByVal value As String)
            _DiasLiquidacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechasFin() As String
        Get
            Return _FechasFin
        End Get
        Set(ByVal value As String)
            _FechasFin = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaCreacion() As String
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As String)
            _FechaCreacion = value
        End Set
    End Property





    <DataMember()> _
    Public Property DesEstado() As String
        Get
            Return _DesEstado
        End Get
        Set(ByVal value As String)
            _DesEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property ValoresPeriodo() As String
        Get
            Return _ValoresPeriodo
        End Get
        Set(ByVal value As String)
            _ValoresPeriodo = value
        End Set
    End Property
End Class
