Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BEPlantilla
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        _idPlantilla = ObjectToInt(reader("idPlantilla"))
        _IdPlantillaTipo = ObjectToInt(reader("idPlantillaTipo"))
        _IdServicio = ObjectToInt(reader("idServicio"))
        _Descripcion = ObjectToString(reader("Descripcion"))
        _PlantillaHtml = ObjectToString(reader("PlantillaHtml"))
        _IdEstado = ObjectToInt(reader("idEstado"))
        _IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
        _FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        _IdUsuarioActualizacion = ObjectToInt(reader("IdUsuarioActualizacion"))
        _FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        _FileS3 = ObjectToString(reader("FileS3"))
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal tipo As Integer)
        IdPlantilla = ObjectToInt(reader("idPlantilla"))
        Descripcion = ObjectToString(reader("Descripcion"))
        PlantillaHtml = ObjectToString(reader("PlantillaHtml"))
        IdEstado = ObjectToInt(reader("IdEstado"))
        IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
        FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        IdUsuarioActualizacion = ObjectToInt(reader("IdUsuarioActualizacion"))
        FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        Select Case tipo
            Case "PlantillaGenPago", "PlantillaQueEsPE"
                _idPlantilla = ObjectToInt(reader("idPlantilla"))
                _IdPlantillaTipo = ObjectToInt(reader("idPlantillaTipo"))
                _IdServicio = ObjectToInt(reader("idServicio"))
                _Descripcion = ObjectToString(reader("Descripcion"))
                _PlantillaHtml = ObjectToString(reader("PlantillaHtml"))
                _IdEstado = ObjectToInt(reader("idEstado"))
                _IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
                _FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
                _IdUsuarioActualizacion = ObjectToInt(reader("IdUsuarioActualizacion"))
                _FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
                _CodigoPlantillaTipo = ObjectToString(reader("CodigoPlantillaTipo"))
        End Select
    End Sub

    Private _idPlantilla As Integer
    <DataMember()> _
    Public Property IdPlantilla() As Integer
        Get
            Return _idPlantilla
        End Get
        Set(ByVal value As Integer)
            _idPlantilla = value
        End Set
    End Property

    Private _IdPlantillaTipo As Integer
    <DataMember()> _
    Public Property IdPlantillaTipo() As Integer
        Get
            Return _IdPlantillaTipo
        End Get
        Set(ByVal value As Integer)
            _IdPlantillaTipo = value
        End Set
    End Property

    Private _IdServicio As Integer
    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property

    Private _Descripcion As String
    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Private _PlantillaHtml As String
    <DataMember()> _
    Public Property PlantillaHtml() As String
        Get
            Return _PlantillaHtml
        End Get
        Set(ByVal value As String)
            _PlantillaHtml = value
        End Set
    End Property

    Private _IdEstado As Integer
    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    Private _IdUsuarioCreacion As Integer
    <DataMember()> _
    Public Property IdUsuarioCreacion() As Integer
        Get
            Return _IdUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioCreacion = value
        End Set
    End Property

    Private _FechaCreacion As DateTime
    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCreacion = value
        End Set
    End Property

    Private _IdUsuarioActualizacion As Integer
    <DataMember()> _
    Public Property IdUsuarioActualizacion() As Integer
        Get
            Return _IdUsuarioActualizacion
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioActualizacion = value
        End Set
    End Property

    Private _FechaActualizacion As DateTime
    <DataMember()> _
    Public Property FechaActualizacion() As DateTime
        Get
            Return _FechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _FechaActualizacion = value
        End Set
    End Property

    Private _CodigoPlantillaTipo As String
    <DataMember()> _
    Public Property CodigoPlantillaTipo() As String
        Get
            Return _CodigoPlantillaTipo
        End Get
        Set(ByVal value As String)
            _CodigoPlantillaTipo = value
        End Set
    End Property

    Private _CodigoPlantillaTipoVariacion As String
    <DataMember()> _
    Public Property CodigoPlantillaTipoVariacion() As String
        Get
            Return _CodigoPlantillaTipoVariacion
        End Get
        Set(ByVal value As String)
            _CodigoPlantillaTipoVariacion = value
        End Set
    End Property

    Private _ListaParametros As List(Of BEPlantillaParametro)
    <DataMember()> _
    Public Property ListaParametros() As List(Of BEPlantillaParametro)
        Get
            Return _ListaParametros
        End Get
        Set(ByVal value As List(Of BEPlantillaParametro))
            _ListaParametros = value
        End Set
    End Property

    Private _FileS3 As String
    <DataMember()> _
    Public Property FileS3() As String
        Get
            Return _FileS3
        End Get
        Set(ByVal value As String)
            _FileS3 = value
        End Set
    End Property

End Class
