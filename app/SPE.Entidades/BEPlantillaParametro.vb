Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEPlantillaParametro

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, Optional ByVal opcion As String = "")

        Select Case opcion
            Case "ConsultaParametrosFuente"
                _IdPlantillaFuenteParametro = ObjectToInt(reader("IdPlantillaFuenteParametro"))
                _FuenteCodigo = ObjectToString(reader("Codigo"))
                _FuenteEtiqueta = ObjectToString(reader("Etiqueta"))
                _FuenteDescripcion = ObjectToString(reader("Descripcion"))
                _FuenteOrigen = ObjectToString(reader("FuenteOrigen"))

            Case "ConsultaParametroEmail"
                IdPlantillaParametro = ObjectToInt(reader("IdPlantillaCorreoParametro"))
                IdPlantilla = ObjectToInt(reader("IdPlantilla"))
                IdTipoParametro = ObjectToInt(reader("IdTipoParametro"))
                Etiqueta = ObjectToString(reader("Etiqueta"))
                Valor = ObjectToString(reader("Valor"))
            Case Else
                _idPlantillaParametro = ObjectToInt(reader("idPlantillaParametro"))
                _IdPlantilla = ObjectToInt(reader("idPlantilla"))
                _IdPlantillaFuenteParametro = ObjectToInt(reader("IdPlantillaFuenteParametro"))
                _IdTipoParametro = ObjectToInt(reader("IdTipoParametro"))
                _Etiqueta = ObjectToString(reader("Etiqueta"))
                _Valor = ObjectToString(reader("Valor"))
                _FuenteCodigo = ObjectToString(reader("FuenteCodigo"))
                _FuenteEtiqueta = ObjectToString(reader("FuenteEtiqueta"))
                _FuenteDescripcion = ObjectToString(reader("FuenteDescripcion"))
                _FuenteOrigen = ObjectToString(reader("FuenteOrigen"))
        End Select
    End Sub

    Private _idPlantillaParametro As Integer
	<DataMember()> _
    Public Property IdPlantillaParametro() As Integer
        Get
            Return _idPlantillaParametro
        End Get
        Set(ByVal value As Integer)
            _idPlantillaParametro = value
        End Set
    End Property

    Private _IdPlantilla As Integer
	<DataMember()> _
    Public Property IdPlantilla() As Integer
        Get
            Return _IdPlantilla
        End Get
        Set(ByVal value As Integer)
            _IdPlantilla = value
        End Set
    End Property

    Private _IdPlantillaFuenteParametro As Integer
	<DataMember()> _
    Public Property IdPlantillaFuenteParametro() As Integer
        Get
            Return _IdPlantillaFuenteParametro
        End Get
        Set(ByVal value As Integer)
            _IdPlantillaFuenteParametro = value
        End Set
    End Property

    Private _IdTipoParametro As Integer
	<DataMember()> _
    Public Property IdTipoParametro() As Integer
        Get
            Return _IdTipoParametro
        End Get
        Set(ByVal value As Integer)
            _IdTipoParametro = value
        End Set
    End Property

    Private _Etiqueta As String
	<DataMember()> _
    Public Property Etiqueta() As String
        Get
            Return _Etiqueta
        End Get
        Set(ByVal value As String)
            _Etiqueta = value
        End Set
    End Property

    Private _Valor As String
	<DataMember()> _
    Public Property Valor() As String
        Get
            Return _Valor
        End Get
        Set(ByVal value As String)
            _Valor = value
        End Set
    End Property


    Private _FuenteCodigo As String
	<DataMember()> _
    Public Property FuenteCodigo() As String
        Get
            Return _FuenteCodigo
        End Get
        Set(ByVal value As String)
            _FuenteCodigo = value
        End Set
    End Property

    Private _FuenteEtiqueta As String
	<DataMember()> _
    Public Property FuenteEtiqueta() As String
        Get
            Return _FuenteEtiqueta
        End Get
        Set(ByVal value As String)
            _FuenteEtiqueta = value
        End Set
    End Property

    Private _FuenteDescripcion As String
	<DataMember()> _
    Public Property FuenteDescripcion() As String
        Get
            Return _FuenteDescripcion
        End Get
        Set(ByVal value As String)
            _FuenteDescripcion = value
        End Set
    End Property

    Private _FuenteOrigen As String
	<DataMember()> _
    Public Property FuenteOrigen() As String
        Get
            Return _FuenteOrigen
        End Get
        Set(ByVal value As String)
            _FuenteOrigen = value
        End Set
    End Property

    Private _TipoDescripcion As String
    <DataMember()>
    Public Property TipoDescripcion() As String
        Get
            Return _TipoDescripcion
        End Get
        Set(ByVal value As String)
            _TipoDescripcion = value
        End Set
    End Property
End Class
