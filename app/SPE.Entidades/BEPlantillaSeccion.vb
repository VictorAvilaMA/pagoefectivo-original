﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BEPlantillaSeccion

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        IdServicio = ObjectToInt(reader("idServicio"))
        IdPlantillaFormato = ObjectToInt(reader("idPlantillaFormato"))
        IdSeccion = ObjectToInt(reader("idSeccion"))
        Contenido = ObjectToString(reader("Contenido"))
    End Sub

    Private _idServicio As Integer
    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    Private _idPlantillaFormato As Integer
    <DataMember()> _
    Public Property IdPlantillaFormato() As Integer
        Get
            Return _idPlantillaFormato
        End Get
        Set(ByVal value As Integer)
            _idPlantillaFormato = value
        End Set
    End Property

    Private _idSeccion As Integer
    <DataMember()> _
    Public Property IdSeccion() As Integer
        Get
            Return _idSeccion
        End Get
        Set(ByVal value As Integer)
            _idSeccion = value
        End Set
    End Property

    Private _contenido As String
    <DataMember()> _
    Public Property Contenido() As String
        Get
            Return _contenido
        End Get
        Set(ByVal value As String)
            _contenido = value
        End Set
    End Property



End Class
