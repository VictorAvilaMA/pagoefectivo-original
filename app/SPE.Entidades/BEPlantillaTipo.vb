﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BEPlantillaTipo
    Inherits _3Dev.FW.Entidades.BusinessEntityBase
    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, Optional ByVal mode As String = "Default")
        Select Case mode
            Case "Default"
                _IdPlantillaTipo = ObjectToInt(reader("idPlantillaTipo"))
                _Descripcion = ObjectToString(reader("Descripcion"))
        End Select

    End Sub

    Private _IdPlantillaTipo As Integer
    <DataMember()> _
    Public Property IdPlantillaTipo() As Integer
        Get
            Return _IdPlantillaTipo
        End Get
        Set(ByVal value As Integer)
            _IdPlantillaTipo = value
        End Set
    End Property

    Private _Codigo As String
    <DataMember()> _
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property

    Private _Descripcion As String
    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Private _IdEstado As Integer
    <DataMember()> _
    Public Property IdEstadopcion() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    Private _IdUsuarioCreacion As Integer
    <DataMember()> _
    Public Property IdUsuarioCreacion() As Integer
        Get
            Return _IdUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioCreacion = value
        End Set
    End Property

    Private _FechaCreacion As DateTime
    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCreacion = value
        End Set
    End Property

    Private _IdUsuarioActualizacion As Integer
    <DataMember()> _
    Public Property IdUsuarioActualizacion() As Integer
        Get
            Return _IdUsuarioActualizacion
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioActualizacion = value
        End Set
    End Property

    Private _FechaActualizacion As DateTime
    <DataMember()> _
    Public Property FechaActualizacion() As DateTime
        Get
            Return _FechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _FechaActualizacion = value
        End Set
    End Property

    Private _IdMoneda As Integer
    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property
End Class
