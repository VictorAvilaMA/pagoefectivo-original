﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEPreConciliacionDetalle
    Private _Banco As String
    Private _CIP As String
    Private _Estado As String
    Private _Servicio As String
    Private _ServicioDes As String
    Private _Moneda As String
    Private _Total As Decimal
    Private _FechaCreacion As String
    Private _FechaCancelacion As String
    Private _FechaExpiracion As String
    Private _FechaAnulacion As String
    Private _OrderIdComercio As String
    Private _Observacion As String
    Private _NombreArchivo As String


    <DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property


    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property

    <DataMember()> _
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal value As String)
            _Servicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property ServicioDes() As String
        Get
            Return _ServicioDes
        End Get
        Set(ByVal value As String)
            _ServicioDes = value
        End Set
    End Property


    <DataMember()> _
    Public Property Moneda() As String
        Get
            Return _Moneda
        End Get
        Set(ByVal value As String)
            _Moneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            _Total = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaCreacion() As String
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As String)
            _FechaCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCancelacion() As String
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As String)
            _FechaCancelacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaExpiracion() As String
        Get
            Return _FechaExpiracion
        End Get
        Set(ByVal value As String)
            _FechaExpiracion = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaAnulacion() As String
        Get
            Return _FechaAnulacion
        End Get
        Set(ByVal value As String)
            _FechaAnulacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property OrderIdComercio() As String
        Get
            Return _OrderIdComercio
        End Get
        Set(ByVal value As String)
            _OrderIdComercio = value
        End Set
    End Property

    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property



    Public Sub New(ByVal reader As IDataReader)
        Banco = Convert.ToString(reader("Banco"))
        CIP = Convert.ToString(reader("CIP"))
        Estado = Convert.ToString(reader("Estado"))
        Servicio = Convert.ToString(reader("Servicio"))
        ServicioDes = Convert.ToString(reader("ServicioDes"))
        Moneda = Convert.ToString(reader("Moneda"))
        Total = Convert.ToDecimal(reader("Total"))
        FechaCreacion = Convert.ToString(reader("FechaCreacion"))
        FechaCancelacion = Convert.ToString(reader("FechaCancelacion"))
        FechaExpiracion = Convert.ToString(reader("FechaExpiracion"))
        FechaAnulacion = Convert.ToString(reader("FechaAnulacion"))
        OrderIdComercio = Convert.ToString(reader("OrderIdComercio"))
        Observacion = Convert.ToString(reader("Observacion"))
        NombreArchivo = Convert.ToString(reader("NombreArchivo"))
    End Sub

End Class
