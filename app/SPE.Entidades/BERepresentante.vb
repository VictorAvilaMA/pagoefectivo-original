Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BERepresentante
    Inherits BEUsuarioBase

    Private _idRepresentante As Integer
    Private _idEmpresaContratante As Integer
    Private _emRazonSocial As String
    Private _emRUC As String
    Private _emIdsEmpresas As List(Of String)


    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub New(ByVal reader As IDataReader)

        MyBase.ActualizarCampos(reader)
        ActCampos(reader)
        Me.IdRepresentante = ObjectToInt32(reader("IdRepresentante"))
        Me.IdEmpresaContratante = ObjectToInt32(reader("IdEmpresaContratante"))
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)
        Select Case tipo
            Case "Busq"
                MyBase.ActualizarCampos(reader)
                ActCampos(reader)
                Me.DescripcionEstado = ObjectToString(reader("DescripcionEstado"))
            Case "Obt"
                Me.IdRepresentante = ObjectToInt(reader("IdRepresentante"))
                Me.IdUsuario = ObjectToInt(reader("IdUsuario"))
                Me.IdEmpresaContratante = ObjectToInt(reader("IdEmpresaContratante"))
                Me.EmRazonSocial = ObjectToString(reader("RazonSocial"))
                Me.EmRUC = ObjectToString(reader("RUC"))
        End Select
    End Sub

    Private Sub ActCampos(ByVal reader As IDataReader)
        Me.IdRepresentante = ObjectToInt32(reader("IdRepresentante"))
        Me.IdEmpresaContratante = ObjectToInt32(reader("IdEmpresaContratante"))

        'Me.IdUsuario = Convert.ToInt32(reader("IdUsuario").ToString())
        'Me.Nombres = reader("Nombres").ToString()
        'Me.Apellidos = reader("Apellidos").ToString()
        'Me.IdTipoDocumento = Convert.ToInt32(reader("IdTipoDocumento").ToString())
        'Me.NumeroDocumento = reader("NumeroDocumento").ToString()
        'Me.Direccion = reader("Direccion").ToString()
        'Me.Telefono = reader("Telefono").ToString()
        'Me.Email = reader("Email").ToString()
        'Me.Password = reader("Password").ToString()
        'Me.IdEstado = Convert.ToInt32(reader("IdEstado").ToString())
        'Me.IdUsuarioCreacion = Convert.ToInt32(reader("IdUsuarioCreacion").ToString())
        'Me.FechaCreacion = Convert.ToDateTime(reader("FechaCreacion").ToString())
        'Me.IdUsuarioActualizacion = Convert.ToInt32(reader("IdUsuarioActualizacion").ToString())
        'Me.FechaActualizacion = Convert.ToDateTime(reader("FechaActualizacion").ToString())
        'Me.IdPais = Convert.ToInt32(reader("IdPais").ToString())
        'Me.IdDepartamento = Convert.ToInt32(reader("IdDepartamento").ToString())
        'Me.IdCiudad = Convert.ToInt32(reader("IdCiudad").ToString())
        Me.EmRazonSocial = ObjectToString(reader("RazonSocial"))
        Me.EmRUC = ObjectToString(reader("RUC"))
    End Sub

	<DataMember()> _
    Public Property IdRepresentante() As Integer
        Get
            Return _idRepresentante
        End Get
        Set(ByVal value As Integer)
            _idRepresentante = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdEmpresaContratante() As Integer
        Get
            Return _idEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _idEmpresaContratante = value
        End Set
    End Property

	<DataMember()> _
    Public Property EmRazonSocial() As String
        Get
            Return _emRazonSocial
        End Get
        Set(ByVal value As String)
            _emRazonSocial = value
        End Set
    End Property

	<DataMember()> _
    Public Property EmRUC() As String
        Get
            Return _emRUC
        End Get
        Set(ByVal value As String)
            _emRUC = value
        End Set
    End Property

    <DataMember()> _
    Public Property EmIdsEmpresas() As List(Of String)
        Get
            Return _emIdsEmpresas
        End Get
        Set(ByVal value As List(Of String))
            _emIdsEmpresas = value
        End Set
    End Property

    Public Class NombresComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BERepresentante).Nombres.CompareTo(CType(y, BERepresentante).Nombres)
        End Function
    End Class
    Public Class ApellidosComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BERepresentante).Apellidos.CompareTo(CType(y, BERepresentante).Apellidos)
        End Function
    End Class
    Public Class TelefonoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BERepresentante).Telefono.CompareTo(CType(y, BERepresentante).Telefono)
        End Function
    End Class
    Public Class EmailComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BERepresentante).Email.CompareTo(CType(y, BERepresentante).Email)
        End Function
    End Class
    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BERepresentante).DescripcionEstado.CompareTo(CType(y, BERepresentante).DescripcionEstado)
        End Function
    End Class
End Class
