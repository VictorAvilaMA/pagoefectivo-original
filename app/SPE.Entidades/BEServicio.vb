Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Web
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEServicio
    Inherits BEAuditoria

    Private _IdServicio As Integer
    Private _IdEmpresaContratante As Integer
    Private _Nombre As String
    Private _IdEstado As Integer
    Private _TiempoExpiracion As Decimal
    Private _LogoImagen As Byte()
    Private _NombreEmpresaContrante As String
    Private _Url As String
    Private _DescripcionEstado As String
    Private _idTipoOrigenCancelacion As Integer
    Private _descripcionTipoOrigenCancelacion As String
    Private _idTipoNotificacion As Integer
    Private _urlFTP As String
    Private _usuario As String
    Private _password As String
    Private _urlNotificacion As String
    Private _listaTipoOrigenCancelacion As List(Of BETipoOrigenCancelacion)

    '***********Actualizacion Masiva***********
    Private _IdCanalVentaAM As Integer
    Private _PlantillaFormato As Integer

    '****************API*****
    'Private _IdTipoIntegracion As Integer
    Private _ClaveAPI As String
    Private _ClaveSecreta As String
    '****************API*****

    '************Usuarios Anonimos*********
    Private _usaUsuariosAnonimos As Boolean
    Private _idTipoIntegracion As Integer

    '************ Plugin Pasarela *********
    Private _llaveEncripta As String
    Private _pasarelaUrlOrigen As String
    Private _pasarelaUrlRespuesta As String
    Private _idPasarelaMedioPago As Integer
    Private _descripcionPasarelaMedioPago As String
    Private _listaPasarelaMedioPago As List(Of BEPasarelaMedioPago)
    Private _HabilitaPasarelaMedioPagoCIP As Boolean
    '************ Consulta Integracion Pasarela CIP;VIS;MAS *********
    Private _integraPasarelaMedioPagoCIP As String
    Private _integraPasarelaMedioPagoVIS As String
    Private _integraPasarelaMedioPagoMAS As String
    Private _TipoComision As String
    Private _MontoComision As Decimal
    Private _ContratoXML As String


    '************
    Private _PorcentajeComision As Decimal
    Private _CantidadLimite As Integer
    Private _idMoneda As Integer
    Private _DescripcionMoneda As String
    Private _idTipoComision As Integer
    Private _urlRedireccionamiento As String
    Private _rutaClavePublica As String
    Private _rutaClavePrivate As String

    'Autor: Jonathan Bastidas - jonathan.bastidas@ec.pe
    'Fecha Act: 15/08/2012
    'Se agrego el monto limite que servira para aplicar la comisión mixta
    Private _montoLimite As Decimal

    '************ Mantenimiento de Claves *********
    Private _PrivateKey() As Byte
    Private _PublicKey() As Byte

    Private _CodigoServicioVersion As String
    Private _CentrosDePago As String
    Private _extornoUnico As Boolean

    'Opciones de envio de Correos
    Private _FlgEmailCCAdmin As Boolean
    Private _FlgEmailGenUsuario As Boolean
    Private _FlgEmailGenCIP As Boolean
    Private _FlgEmailPagoCIP As Boolean
    'Opcion para alertar expiración de CIP
    Private _FlgEmailExpCIP As Boolean
    Private _TiempoEmailExpiracion As Integer

    'Configuracion Servicios de Notificacion
    Private _IdGrupoNotificacion As Integer
    Private _FlgNotificaGeneracion As Boolean
    Private _FlgNotificaMovimiento As Boolean
    Private _FlgNotificaExpiracion As Boolean

    'Clasificacion de Servicios
    Private _Clasificacion As String

    'Servicio Productos
    Private _RutaClaPrivas As String
    Private _IdUsuario As Integer
    Private _MailComercio As String

    'Formulario
    Private _formularioActivo As Boolean
    Private _formularioNombreUrl As String
    Private _formularioItemsMaximo As Integer
    Private _formularioCantidad As Integer
    Private _formularioPlaceHolderDatosAdicionales As String
    Private _formularioMensajePersonalizado As String
    Private _formularioUrlTerminos As String
    Private _formularioRutaLogo As String
    Private _formularioRutaLogoResponsive As String
    Private _formularioRutaFondoHeader As String
    Private _formularioRutaFondoHeaderResponsive As String
    Private _formularioCorreo As String
    Private _EstadoDireccion As String

    'Cobro Masivo - Inicio
    Private _CanalVenta As Integer
    Private _WebService As Integer
    Private _Ip As String

    Private _CantidaMaximaPlanillaMes As Integer
    Private _CantidaCobrosPlanilla As Integer
    Private _MontoMinimoCobro As Decimal
    Private _MontoMaximoCobro As Decimal
    Private _MontoMinimoPlanilla As Decimal
    Private _MontoMaximoPlanilla As Decimal
    Private _VigenciaMinimaDias As Integer
    Private _VigenciaMinimaHoras As Integer
    Private _VigenciaMaximaDias As Integer
    Private _VigenciaMaximaHoras As Integer
    Private _DiaMaximoInicioCobranza As Integer
    Private _HorasAvisoExpiracion As Integer
    Private _EnvioGenerarCip As Boolean
    Private _EnvioEmailOperador As Boolean
    Private _EmailOperador As String
    Private _NombreLogo As String
    'Cobro Masivo - Fin


    'PagoEfectivo Express
    Private _TipoPlantilla As Integer
    Private _LogoServicio As String



    <DataMember()> _
    Public Property IdCanalVentaAM As Integer
        Get
            Return _IdCanalVentaAM
        End Get
        Set(ByVal value As Integer)
            _IdCanalVentaAM = value
        End Set
    End Property
    <DataMember()> _
    Public Property PlantillaFormato As Integer
        Get
            Return _PlantillaFormato
        End Get
        Set(ByVal value As Integer)
            _PlantillaFormato = value
        End Set
    End Property

    <DataMember()> _
    Public Property ExtornoUnico As Boolean
        Get
            Return _extornoUnico
        End Get
        Set(ByVal value As Boolean)
            _extornoUnico = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoServicioVersion() As String
        Get
            Return _CodigoServicioVersion
        End Get
        Set(ByVal value As String)
            _CodigoServicioVersion = value
        End Set
    End Property
    <DataMember()> _
    Public Property MontoLimite() As Decimal
        Get
            Return _montoLimite
        End Get
        Set(ByVal value As Decimal)
            _montoLimite = value
        End Set
    End Property
    'Autor: Jonathan Bastidas - jonathan.bastidas@ec.pe
    'Fecha Act: 15/08/2012
    'Se agrego el monto limite para la comisión cuando se aplica el porcentaje, la comisión no puede superar este limite
    Private _montoLimitePorcentaje As Decimal
    <DataMember()> _
    Public Property MontoLimitePorcentaje() As Decimal
        Get
            Return _montoLimitePorcentaje
        End Get
        Set(ByVal value As Decimal)
            _montoLimitePorcentaje = value
        End Set
    End Property
    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        ActCampos(reader)
    End Sub

    Private Sub ActCampos(ByVal reader As IDataReader)

        Me._IdServicio = ObjectToInt32(reader("IdServicio"))
        Me._IdEmpresaContratante = ObjectToInt32(reader("IdEmpresaContratante"))
        Me._Nombre = ObjectToString(reader("Nombre"))
        Me._Url = ObjectToString(reader("Url"))
        Me._IdEstado = ObjectToInt(reader("IdEstado"))
        Me._TiempoExpiracion = ObjectToDecimal(reader("TiempoExpiracion"))
        Me._LogoImagen = reader("LogoImagen")
        Me._NombreEmpresaContrante = ObjectToString(reader("NombreEmpresaContrante"))
        Me._codigo = ObjectToString(reader, "Codigo")
        FechaCreacion = ObjectToDateTime(reader, "FechaCreacion")
        FechaActualizacion = ObjectToDateTime(reader, "FechaActualizacion")

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal tipo As String)

        Select Case tipo
            Case "busq"
                ActCampos(reader)
                _DescripcionEstado = ObjectToString(reader("DescripcionEstado"))
                _IdCanalVentaAM = ObjectToInt(reader("IdCanalVenta"))
                _PlantillaFormato = ObjectToInt(reader("PlantillaFormato"))

                _integraPasarelaMedioPagoCIP = ObjectToString(reader, "HabilitaCIP")
                _integraPasarelaMedioPagoVIS = ObjectToString(reader, "MPVIS")
                _integraPasarelaMedioPagoMAS = ObjectToString(reader, "MPMAS")

            Case "mantenimiento", "busqPorUrl", "ConsultarServicioPorAPI"
                ActCampos(reader)
                _idTipoNotificacion = ObjectToInt(reader("idTipoNotificacion"))
                _urlFTP = ObjectToString(reader("UrlFTP"))
                _usuario = ObjectToString(reader("Usuario"))
                _password = ObjectToString(reader("Password"))

                '************* API ******************
                '_IdTipoIntegracion = _3Dev.FW.Util.DataUtil.ObjectToInt(reader("idTipoNotificacion"))
                _ClaveAPI = ObjectToString(reader("ClaveAPI"))
                _ClaveSecreta = ObjectToString(reader("ClaveSecreta"))
                '************* API ******************
                _usaUsuariosAnonimos = ObjectToBool(reader("UsaUsuariosAnonimos"))
                _idTipoIntegracion = ObjectToInt(reader("IdTipoIntegracion"))
                If tipo = "ConsultarServicioPorAPI" Then
                    CodigoServicioVersion = ObjectToString(reader("CodigoServicioVersion"))
                    FlgEmailCCAdmin = ObjectToBool(reader("FlgEmailCCAdmin"))
                    FlgEmailGenCIP = ObjectToBool(reader("FlgEmailGenCIP"))
                    FlgEmailGenUsuario = ObjectToBool(reader("FlgEmailGenUsuario"))
                    FlgEmailPagoCIP = ObjectToBool(reader("FlgEmailPagoCIP"))
                    FlgEmailExpCIP = ObjectToBool(reader("FlgEmailExpCIP"))
                    TiempoEmailExpiracion = ObjectToInt32(reader("TiempoEmailExpiracion"))
                    _IdCanalVentaAM = ObjectToInt(reader("IdCanalVenta"))
                    _PlantillaFormato = ObjectToInt(reader("PlantillaFormato"))
                End If
                Me.AsignarAuditoria(reader)

                '' COMENTADO PARA ESTA VERSION
                '' AUTOR: GERARDO -- DJ
                '' fecha: 01-06-2011

                ''************* LLAVE ENCRIPTACION ******************
                '_llaveEncripta = ObjectToString(reader("LlaveEncriptacion"))
                ''************* PASARELA URL ORIGEN ******************
                '_pasarelaUrlOrigen = ObjectToString(reader("PasarelaUrlOrigSolPago"))
                ''************* PASARELA URL RESPUESTA ***************
                '_pasarelaUrlRespuesta = ObjectToString(reader("PasarelaUrlResPago"))
                ''************* PASARELA INTEGRA CIP ***************
                '_HabilitaPasarelaMedioPagoCIP = ObjectToString(reader("HabilitaCIP"))
            Case "conServicioXEmpresa"
                ActCampos(reader)
                _DescripcionEstado = ObjectToString(reader("DescripcionEstado"))

                _integraPasarelaMedioPagoCIP = ObjectToString(reader, "HabilitaCIP")
                _integraPasarelaMedioPagoVIS = ObjectToString(reader, "MPVIS")
                _integraPasarelaMedioPagoMAS = ObjectToString(reader, "MPMAS")
                TotalPageNumbers = ObjectToInt32(reader, "TOTAL_RESULT")
                _TipoComision = ObjectToString(reader("TipoComision"))
                _idTipoComision = ObjectToInt32(reader, "idTipoComision")
                _MontoComision = ObjectToDecimal(reader("MontoComision"))
                _PorcentajeComision = ObjectToDecimal(reader("PorcentajeComision"))
                _montoLimite = ObjectToDecimal(reader("MontoLimite"))
                _DescripcionMoneda = ObjectToString(reader("DescripcionMoneda"))
                IdMoneda = ObjectToString(reader("idMoneda"))
                FechaCreacion = ObjectToDateTime(reader, "FechaCracionComision")
                FechaActualizacion = ObjectToDateTime(reader, "FechaActualizacionComision")
                IdEstadoComision = ObjectToInt32(reader, "IdEstadoComision")
            Case "MantenimientoPorId"
                ActCampos(reader)
                _idTipoNotificacion = ObjectToInt(reader("idTipoNotificacion"))
                _urlFTP = ObjectToString(reader("UrlFTP"))
                _usuario = ObjectToString(reader("Usuario"))
                _password = ObjectToString(reader("Password"))
                _ClaveAPI = ObjectToString(reader("ClaveAPI"))
                _ClaveSecreta = ObjectToString(reader("ClaveSecreta"))
                _usaUsuariosAnonimos = ObjectToBool(reader("UsaUsuariosAnonimos"))
                _idTipoIntegracion = ObjectToInt(reader("IdTipoIntegracion"))
                ' _TipoComision = ObjectToString(reader("TipoComision"))
                ' _MontoComision = ObjectToDecimal(reader("MontoComision"))
                _ContratoXML = ObjectToString(reader("ContratoXML"))
                '_PorcentajeComision = ObjectToDecimal(reader("PorcentajeComision"))
                '_CantidadLimite = ObjectToInt32(reader("CantidadLimite"))
                _urlNotificacion = ObjectToString(reader("UrlNotificacion"))
                _urlRedireccionamiento = ObjectToString(reader("UrlRedirect"))
                _rutaClavePublica = ObjectToString(reader("RutaClaPub"))
                _rutaClavePrivate = ObjectToString(reader("RutaClaPriv"))
                _CodigoServicioVersion = ObjectToString(reader("CodigoServicioVersion"))
                _proximoAfiliado = ObjectToBool(reader("ProximoAfiliado"))
                _visibleEnPortada = ObjectToBool(reader("VisibleEnPortada"))
                _extornoUnico = ObjectToBool(reader, "FlgExtUnico")
                'Agregado para la configuracion de correos
                _FlgEmailCCAdmin = ObjectToBool(reader("FlgEmailCCAdmin"))
                _FlgEmailGenCIP = ObjectToBool(reader("FlgEmailGenCIP"))
                _FlgEmailGenUsuario = ObjectToBool(reader("FlgEmailGenUsuario"))
                _FlgEmailPagoCIP = ObjectToBool(reader("FlgEmailPagoCIP"))
                _FlgEmailExpCIP = ObjectToBool(reader("FlgEmailExpCIP"))
                _TiempoEmailExpiracion = ObjectToInt32(reader("TiempoEmailExpiracion"))
                'Agregado para la configuracion de Notificaciones
                _IdGrupoNotificacion = ObjectToInt32(reader("IdGrupoNotificacion"))
                _FlgNotificaGeneracion = ObjectToBool(reader("FlgNotificaGeneracion"))
                _FlgNotificaMovimiento = ObjectToBool(reader("FlgNotificaMovimiento"))
                _FlgNotificaExpiracion = ObjectToBool(reader("FlgNotificaExpiracion"))

                'FormularioActivo SERVICIO
                _formularioActivo = ObjectToBool(reader("Activo"))
                _formularioNombreUrl = ObjectToString(reader("NombreUrl"))
                _formularioItemsMaximo = ObjectToInt32(reader("ItemsMaximo"))
                _formularioCantidad = ObjectToInt32(reader("Cantidad"))
                _formularioPlaceHolderDatosAdicionales = ObjectToString(reader("PlaceHolderDatosAdicionales"))
                _formularioMensajePersonalizado = ObjectToString(reader("MensajePersonalizado"))
                _formularioUrlTerminos = ObjectToString(reader("UrlTerminos"))

                _formularioRutaLogo = ObjectToString(reader("RutaLogo"))
                _formularioRutaLogoResponsive = ObjectToString(reader("RutaLogoResponsive"))
                _formularioRutaFondoHeader = ObjectToString(reader("RutaFondoHeader"))
                _formularioRutaFondoHeaderResponsive = ObjectToString(reader("RutaFondoHeaderResponsive"))
                _formularioCorreo = ObjectToString(reader("Correo"))
                _EstadoDireccion = ObjectToString(reader("EstadoDireccion"))

                'PagoEfectivoExpress
                _TipoPlantilla = ObjectToInt32(reader("TipoPlantilla"))
                _PlantillaFormato = ObjectToInt32(reader("TipoPlantilla"))
                _LogoServicio = ObjectToString(reader("LogoServicio"))

                'Cobranzas
                _CanalVenta = ObjectToInt32(reader("IdCanalVenta"))
                _WebService = ObjectToInt32(reader("IdWebService"))
                _Ip = ObjectToString(reader("Ip"))

                _CantidaMaximaPlanillaMes = ObjectToInt32(reader("CantidadMaximaPlanillaMes"))
                _CantidaCobrosPlanilla = ObjectToInt32(reader("CantidadCobrosPlanilla"))
                _MontoMinimoCobro = ObjectToDecimal(reader("MontoMinimoCobro"))
                _MontoMaximoCobro = ObjectToDecimal(reader("MontoMaximoCobro"))
                _MontoMinimoPlanilla = ObjectToDecimal(reader("MontoMinimoPlanilla"))
                _MontoMaximoPlanilla = ObjectToDecimal(reader("MontoMaximoPlanilla"))
                _VigenciaMinimaDias = ObjectToInt32(reader("VigenciaMinimaDias"))
                _VigenciaMinimaHoras = ObjectToInt32(reader("VigenciaMinimaHoras"))
                _VigenciaMaximaDias = ObjectToInt32(reader("VigenciaMaximaDias"))
                _VigenciaMaximaHoras = ObjectToInt32(reader("VigenciaMaximaHoras"))
                _DiaMaximoInicioCobranza = ObjectToInt32(reader("DiaMaximoInicioCobranza"))
                _HorasAvisoExpiracion = ObjectToInt32(reader("HorasAvisoExpiracion"))

                _EnvioGenerarCip = ObjectToBool(reader("EnvioGenerarCip"))
                _EnvioEmailOperador = ObjectToBool(reader("EnvioEmailOperador"))
                _EmailOperador = ObjectToString(reader("EmailOperador"))
                _NombreLogo = ObjectToString(reader("NombreLogo"))


                Me.AsignarAuditoria(reader)
            Case "consultaTransaccion"
                'Propiedades reutilizadas para almacenar:Num.Operaciones,TotalPagos,TotalCOmision
                _CantidadLimite = ObjectToInt(reader("NumOperaciones"))
                _MontoComision = ObjectToDecimal(reader("TotalPagos"))
                _PorcentajeComision = ObjectToDecimal(reader("TotalComision"))
                _DescripcionMoneda = ObjectToString(reader("Moneda"))
            Case "MantenimientoSCPorId"
                ActCampos(reader)
                '_DescripcionEstado = ObjectToString(reader("DescripcionEstado"))

                _integraPasarelaMedioPagoCIP = ObjectToString(reader, "HabilitaCIP")
                _TipoComision = ObjectToString(reader("TipoComision"))
                _idTipoComision = ObjectToInt32(reader, "idTipoComision")
                _MontoComision = ObjectToDecimal(reader("MontoComision"))
                _PorcentajeComision = ObjectToDecimal(reader("PorcentajeComision"))
                _montoLimite = ObjectToDecimal(reader("MontoLimite"))
                _montoLimitePorcentaje = ObjectToDecimal(reader("MontoLimitePorc"))
                DescripcionMoneda = ObjectToString(reader("DescripcionMoneda"))
                IdMoneda = ObjectToInt(reader("idMoneda"))
                IdEstadoComision = ObjectToInt32(reader, "IdEstadoComision")
            Case "ServiciosPorIdEmpresa"
                _IdServicio = ObjectToInt32(reader, "IdServicio")
                _Nombre = ObjectToString(reader, "Nombre")
            Case "ServicioInstitucion"
                _codigo = ObjectToString(reader("Codigo"))
                _IdServicio = ObjectToInt32(reader, "IdServicio")
                _IdEmpresaContratante = ObjectToInt32(reader, "IdEmpresaContratante")
                _Nombre = ObjectToString(reader, "Nombre")
                _Url = ObjectToString(reader, "Url")
                _urlNotificacion = ObjectToString(reader, "UrlNotificacion")
                _LogoImagen = reader("LogoImagen")
            Case "ServicioProductos"
                _codigo = ObjectToString(reader("Codigo"))
                _IdServicio = ObjectToInt32(reader, "IdServicio")
                _IdEmpresaContratante = ObjectToInt32(reader, "IdEmpresaContratante")
                _Nombre = ObjectToString(reader, "Nombre")
                _Url = ObjectToString(reader, "Url")
                _urlNotificacion = ObjectToString(reader, "UrlNotificacion")
                _LogoImagen = reader("LogoImagen")
                _RutaClaPrivas = ObjectToString(reader("RutaClaPrivas"))
                _IdUsuario = ObjectToString(reader("IdUsuario"))
                _MailComercio = ObjectToString(reader("MailComercio"))
                _ClaveAPI = ObjectToString(reader("ClaveAPI"))
            Case "ServicioFormulario"

                _formularioNombreUrl = ObjectToString(reader("NombreUrl"))

        End Select
    End Sub

    <DataMember()> _
    Public Property TipoPlantilla() As Integer
        Get
            Return _TipoPlantilla
        End Get
        Set(ByVal value As Integer)
            _TipoPlantilla = value
        End Set
    End Property

    <DataMember()> _
    Public Property LogoServicio() As String
        Get
            Return _LogoServicio
        End Get
        Set(ByVal value As String)
            _LogoServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionEstado() As String
        Get
            Return _DescripcionEstado
        End Get
        Set(ByVal value As String)
            _DescripcionEstado = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdTipoComision() As Integer
        Get
            Return _idTipoComision
        End Get
        Set(ByVal value As Integer)
            _idTipoComision = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado

        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEmpresaContratante() As Integer
        Get
            Return _IdEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _IdEmpresaContratante = value
        End Set
    End Property

    <DataMember()> _
    Public Property TiempoExpiracion() As Decimal
        Get
            Return _TiempoExpiracion
        End Get
        Set(ByVal value As Decimal)
            _TiempoExpiracion = value
        End Set
    End Property

    <DataMember()> _
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreEmpresaContrante() As String
        Get
            Return _NombreEmpresaContrante
        End Get
        Set(ByVal value As String)
            _NombreEmpresaContrante = value
        End Set
    End Property

    <DataMember()> _
    Public Property Url() As String
        Get
            Return _Url
        End Get
        Set(ByVal value As String)
            _Url = value
        End Set
    End Property

    <DataMember()> _
    Public Property LogoImagen() As Byte()
        Get
            Return _LogoImagen
        End Get
        Set(ByVal value As Byte())
            _LogoImagen = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdTipoOrigenCancelacion() As Integer
        Get
            Return _idTipoOrigenCancelacion
        End Get
        Set(ByVal value As Integer)
            _idTipoOrigenCancelacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionTipoOrigenCancelacion() As String
        Get
            Return _descripcionTipoOrigenCancelacion
        End Get
        Set(ByVal value As String)
            _descripcionTipoOrigenCancelacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTipoNotificacion() As Integer
        Get
            Return _idTipoNotificacion
        End Get
        Set(ByVal value As Integer)
            _idTipoNotificacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property UrlFTP() As String
        Get
            Return _urlFTP
        End Get
        Set(ByVal value As String)
            _urlFTP = value
        End Set
    End Property

    <DataMember()> _
    Public Property Usuario() As String
        Get
            Return _usuario
        End Get
        Set(ByVal value As String)
            _usuario = value
        End Set
    End Property

    <DataMember()> _
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    Private _beFTPArchivos As List(Of BEFTPArchivo) = Nothing
    <DataMember()> _
    Public Property FTPArchivos() As List(Of BEFTPArchivo)
        Get
            Return _beFTPArchivos
        End Get
        Set(ByVal value As List(Of BEFTPArchivo))
            _beFTPArchivos = value
        End Set
    End Property

    <DataMember()> _
    Public Property ListaTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
        Get
            Return _listaTipoOrigenCancelacion
        End Get
        Set(ByVal value As List(Of BETipoOrigenCancelacion))
            _listaTipoOrigenCancelacion = value
        End Set
    End Property


    <DataMember()> _
    Public Property ClaveAPI() As String
        Get
            Return _ClaveAPI
        End Get
        Set(ByVal value As String)
            _ClaveAPI = value
        End Set
    End Property

    <DataMember()> _
    Public Property ClaveSecreta() As String
        Get
            Return _ClaveSecreta
        End Get
        Set(ByVal value As String)
            _ClaveSecreta = value
        End Set
    End Property
    '****************API*****
    <DataMember()> _
    Public Property UsaUsuariosAnonimos() As String
        Get
            Return _usaUsuariosAnonimos
        End Get
        Set(ByVal value As String)
            _usaUsuariosAnonimos = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdTipoIntegracion() As String
        Get
            Return _idTipoIntegracion
        End Get
        Set(ByVal value As String)
            _idTipoIntegracion = value
        End Set
    End Property
    '************* Integracion


    Private _codigo As String
    <DataMember()> _
    Public Property Codigo() As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    '************* LlaveEncriptacion
    <DataMember()> _
    Public Property LLaveEncripta() As String
        Get
            Return _llaveEncripta
        End Get
        Set(ByVal value As String)
            _llaveEncripta = value
        End Set
    End Property
    '************* PasarelaUrlOrigSolPago
    <DataMember()> _
    Public Property PasarelaUrlOrigen() As String
        Get
            Return _pasarelaUrlOrigen
        End Get
        Set(ByVal value As String)
            _pasarelaUrlOrigen = value
        End Set
    End Property
    '************* PasarelaUrlResPago
    <DataMember()> _
    Public Property PasarelaUrlRespuesta() As String
        Get
            Return _pasarelaUrlRespuesta
        End Get
        Set(ByVal value As String)
            _pasarelaUrlRespuesta = value
        End Set
    End Property
    '************* PasarelaMedioPagoID
    <DataMember()> _
    Public Property IdPasarelaMedioPago() As Integer
        Get
            Return _idPasarelaMedioPago
        End Get
        Set(ByVal value As Integer)
            _idPasarelaMedioPago = value
        End Set
    End Property
    '************* PasarelaMedioPagoDescripcion
    <DataMember()> _
    Public Property DescripcionPasarelaMedioPago() As String
        Get
            Return _descripcionPasarelaMedioPago
        End Get
        Set(ByVal value As String)
            _descripcionPasarelaMedioPago = value
        End Set
    End Property
    '************* PasarelaMedioPagoLista
    <DataMember()> _
    Public Property ListaPasarelaMedioPago() As List(Of BEPasarelaMedioPago)
        Get
            Return _listaPasarelaMedioPago
        End Get
        Set(ByVal value As List(Of BEPasarelaMedioPago))
            _listaPasarelaMedioPago = value
        End Set
    End Property
    '************* Habilita PasarelaMedioPagoCIP
    <DataMember()> _
    Public Property HabilitaPasarelaMedioPagoCIP() As Boolean
        Get
            Return _HabilitaPasarelaMedioPagoCIP
        End Get
        Set(ByVal value As Boolean)
            _HabilitaPasarelaMedioPagoCIP = value
        End Set
    End Property
    '************* PasarelaMedioPagoCIP
    <DataMember()> _
    Public Property IntegraPasarelaMedioPagoCIP() As String
        Get
            Return _integraPasarelaMedioPagoCIP
        End Get
        Set(ByVal value As String)
            _integraPasarelaMedioPagoCIP = value
        End Set
    End Property
    '************* PasarelaMedioPagoVIS
    <DataMember()> _
    Public Property IntegraPasarelaMedioPagoVIS() As String
        Get
            Return _integraPasarelaMedioPagoVIS
        End Get
        Set(ByVal value As String)
            _integraPasarelaMedioPagoVIS = value
        End Set
    End Property
    '************* PasarelaMedioPagoMAS
    <DataMember()> _
    Public Property IntegraPasarelaMedioPagoMAS() As String
        Get
            Return _integraPasarelaMedioPagoMAS
        End Get
        Set(ByVal value As String)
            _integraPasarelaMedioPagoMAS = value
        End Set
    End Property
    '************* 

    <DataMember()> _
    Public Property TipoComision() As String
        Get
            Return _TipoComision

        End Get
        Set(ByVal value As String)
            _TipoComision = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoComision() As Decimal
        Get
            Return _MontoComision

        End Get
        Set(ByVal value As Decimal)
            _MontoComision = value
        End Set
    End Property

    <DataMember()> _
    Public Property ContratoXML() As String
        Get
            Return _ContratoXML

        End Get
        Set(ByVal value As String)
            _ContratoXML = value
        End Set
    End Property
    <DataMember()> _
    Public Property PorcentajeComision() As Decimal
        Get
            Return _PorcentajeComision
        End Get
        Set(ByVal value As Decimal)
            _PorcentajeComision = value
        End Set
    End Property
    <DataMember()> _
    Public Property CantidadLimite() As Integer
        Get
            Return _CantidadLimite
        End Get
        Set(ByVal value As Integer)
            _CantidadLimite = value
        End Set
    End Property
    <DataMember()> _
    Public Property UrlNotificacion() As String
        Get
            Return _urlNotificacion
        End Get
        Set(ByVal value As String)
            _urlNotificacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionMoneda() As String
        Get
            Return _DescripcionMoneda
        End Get
        Set(ByVal value As String)
            _DescripcionMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property UrlRedireccionamiento() As String
        Get
            Return _urlRedireccionamiento
        End Get
        Set(ByVal value As String)
            _urlRedireccionamiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property RutaClavePublica() As String
        Get
            Return _rutaClavePublica
        End Get
        Set(ByVal value As String)
            _rutaClavePublica = value
        End Set
    End Property
    <DataMember()> _
    Public Property RutaClavePrivate() As String
        Get
            Return _rutaClavePrivate
        End Get
        Set(ByVal value As String)
            _rutaClavePrivate = value
        End Set
    End Property
    Private _idEstadoComision As Integer
    <DataMember()> _
    Public Property IdEstadoComision() As Integer
        Get
            Return _idEstadoComision
        End Get
        Set(ByVal value As Integer)
            _idEstadoComision = value
        End Set
    End Property

    Private _visibleEnPortada As Boolean
    <DataMember()> _
    Public Property VisibleEnPortada() As Boolean
        Get
            Return _visibleEnPortada
        End Get
        Set(ByVal value As Boolean)
            _visibleEnPortada = value
        End Set
    End Property

    Private _proximoAfiliado As Boolean
    <DataMember()> _
    Public Property ProximoAfiliado() As Boolean
        Get
            Return _proximoAfiliado
        End Get
        Set(ByVal value As Boolean)
            _proximoAfiliado = value
        End Set
    End Property
    <DataMember()> _
    Public Property PrivateKey As Byte()
        Get
            Return _PrivateKey
        End Get
        Set(ByVal value As Byte())
            _PrivateKey = value
        End Set
    End Property
    <DataMember()> _
    Public Property PublicKey As Byte()
        Get
            Return _PublicKey
        End Get
        Set(ByVal value As Byte())
            _PublicKey = value
        End Set
    End Property
    <DataMember()> _
    Public Property CentrosDePago As String
        Get
            Return _CentrosDePago
        End Get
        Set(ByVal value As String)
            _CentrosDePago = value
        End Set
    End Property

    <DataMember()> _
    Public Property FlgEmailCCAdmin As Boolean
        Get
            Return _FlgEmailCCAdmin
        End Get
        Set(ByVal value As Boolean)
            _FlgEmailCCAdmin = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgEmailGenUsuario As Boolean
        Get
            Return _FlgEmailGenUsuario
        End Get
        Set(ByVal value As Boolean)
            _FlgEmailGenUsuario = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgEmailGenCIP As Boolean
        Get
            Return _FlgEmailGenCIP
        End Get
        Set(ByVal value As Boolean)
            _FlgEmailGenCIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgEmailPagoCIP As Boolean
        Get
            Return _FlgEmailPagoCIP
        End Get
        Set(ByVal value As Boolean)
            _FlgEmailPagoCIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgEmailExpCIP As Boolean
        Get
            Return _FlgEmailExpCIP
        End Get
        Set(ByVal value As Boolean)
            _FlgEmailExpCIP = value
        End Set
    End Property
    <DataMember()> _
    Public Property TiempoEmailExpiracion As Integer
        Get
            Return _TiempoEmailExpiracion
        End Get
        Set(ByVal value As Integer)
            _TiempoEmailExpiracion = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdGrupoNotificacion As Integer
        Get
            Return _IdGrupoNotificacion
        End Get
        Set(ByVal value As Integer)
            _IdGrupoNotificacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgNotificaGeneracion As Boolean
        Get
            Return _FlgNotificaGeneracion
        End Get
        Set(ByVal value As Boolean)
            _FlgNotificaGeneracion = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgNotificaMovimiento As Boolean
        Get
            Return _FlgNotificaMovimiento
        End Get
        Set(ByVal value As Boolean)
            _FlgNotificaMovimiento = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlgNotificaExpiracion As Boolean
        Get
            Return _FlgNotificaExpiracion
        End Get
        Set(ByVal value As Boolean)
            _FlgNotificaExpiracion = value
        End Set
    End Property
    <DataMember()> _
    Public Property Clasificacion As String
        Get
            Return _Clasificacion
        End Get
        Set(ByVal value As String)
            _Clasificacion = value
        End Set
    End Property


    'Servicio Productos
    <DataMember()> _
    Public Property RutaClaPrivas As String
        Get
            Return _RutaClaPrivas
        End Get
        Set(ByVal value As String)
            _RutaClaPrivas = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuario As Integer
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As Integer)
            _IdUsuario = value
        End Set
    End Property

    <DataMember()> _
    Public Property MailComercio As String
        Get
            Return _MailComercio
        End Get
        Set(ByVal value As String)
            _MailComercio = value
        End Set
    End Property


    'Formulario Servicio
    <DataMember()> _
    Public Property FormularioActivo() As Boolean
        Get
            Return _formularioActivo
        End Get
        Set(ByVal value As Boolean)
            _formularioActivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioNombreUrl() As String
        Get
            Return _formularioNombreUrl
        End Get
        Set(ByVal value As String)
            _formularioNombreUrl = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioItemsMaximo() As Integer
        Get
            Return _formularioItemsMaximo
        End Get
        Set(ByVal value As Integer)
            _formularioItemsMaximo = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioCantidad() As Integer
        Get
            Return _formularioCantidad
        End Get
        Set(ByVal value As Integer)
            _formularioCantidad = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioPlaceHolderDatosAdicionales() As String
        Get
            Return _formularioPlaceHolderDatosAdicionales
        End Get
        Set(ByVal value As String)
            _formularioPlaceHolderDatosAdicionales = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioMensajePersonalizado() As String
        Get
            Return _formularioMensajePersonalizado
        End Get
        Set(ByVal value As String)
            _formularioMensajePersonalizado = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioUrlTerminos() As String
        Get
            Return _formularioUrlTerminos
        End Get
        Set(ByVal value As String)
            _formularioUrlTerminos = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioRutaLogo() As String
        Get
            Return _formularioRutaLogo
        End Get
        Set(ByVal value As String)
            _formularioRutaLogo = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioRutaLogoResponsive() As String
        Get
            Return _formularioRutaLogoResponsive
        End Get
        Set(ByVal value As String)
            _formularioRutaLogoResponsive = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioRutaFondoHeader() As String
        Get
            Return _formularioRutaFondoHeader
        End Get
        Set(ByVal value As String)
            _formularioRutaFondoHeader = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioRutaFondoHeaderResponsive() As String
        Get
            Return _formularioRutaFondoHeaderResponsive
        End Get
        Set(ByVal value As String)
            _formularioRutaFondoHeaderResponsive = value
        End Set
    End Property

    <DataMember()> _
    Public Property FormularioCorreo() As String
        Get
            Return _formularioCorreo
        End Get
        Set(ByVal value As String)
            _formularioCorreo = value
        End Set
    End Property

    <DataMember()> _
    Public Property EstadoDireccion() As String
        Get
            Return _EstadoDireccion
        End Get
        Set(ByVal value As String)
            _EstadoDireccion = value
        End Set
    End Property

    'Cobro Masivo - Inicio
    <DataMember()> _
    Public Property IdCanalVenta() As Integer
        Get
            Return _CanalVenta
        End Get
        Set(ByVal value As Integer)
            _CanalVenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdWebService() As Integer
        Get
            Return _WebService
        End Get
        Set(ByVal value As Integer)
            _WebService = value
        End Set
    End Property

    <DataMember()> _
    Public Property Ip() As String
        Get
            Return _Ip
        End Get
        Set(ByVal value As String)
            _Ip = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantidadMaximaPlanillaMes() As Integer
        Get
            Return _CantidaMaximaPlanillaMes
        End Get
        Set(ByVal value As Integer)
            _CantidaMaximaPlanillaMes = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantidadCobrosPlanilla() As Integer
        Get
            Return _CantidaCobrosPlanilla
        End Get
        Set(ByVal value As Integer)
            _CantidaCobrosPlanilla = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoMinimoCobro() As Decimal
        Get
            Return _MontoMinimoCobro
        End Get
        Set(ByVal value As Decimal)
            _MontoMinimoCobro = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoMaximoCobro() As Decimal
        Get
            Return _MontoMaximoCobro
        End Get
        Set(ByVal value As Decimal)
            _MontoMaximoCobro = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoMinimoPlanilla() As Decimal
        Get
            Return _MontoMinimoPlanilla
        End Get
        Set(ByVal value As Decimal)
            _MontoMinimoPlanilla = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoMaximoPlanilla() As Decimal
        Get
            Return _MontoMaximoPlanilla
        End Get
        Set(ByVal value As Decimal)
            _MontoMaximoPlanilla = value
        End Set
    End Property

    <DataMember()> _
    Public Property VigenciaMinimaDias() As Integer
        Get
            Return _VigenciaMinimaDias
        End Get
        Set(ByVal value As Integer)
            _VigenciaMinimaDias = value
        End Set
    End Property

    <DataMember()> _
    Public Property VigenciaMinimaHoras() As Integer
        Get
            Return _VigenciaMinimaHoras
        End Get
        Set(ByVal value As Integer)
            _VigenciaMinimaHoras = value
        End Set
    End Property

    <DataMember()> _
    Public Property VigenciaMaximaDias() As Integer
        Get
            Return _VigenciaMaximaDias
        End Get
        Set(ByVal value As Integer)
            _VigenciaMaximaDias = value
        End Set
    End Property

    <DataMember()> _
    Public Property VigenciaMaximaHoras() As Integer
        Get
            Return _VigenciaMaximaHoras
        End Get
        Set(ByVal value As Integer)
            _VigenciaMaximaHoras = value
        End Set
    End Property

    <DataMember()> _
    Public Property DiaMaximoInicioCobranza() As Integer
        Get
            Return _DiaMaximoInicioCobranza
        End Get
        Set(ByVal value As Integer)
            _DiaMaximoInicioCobranza = value
        End Set
    End Property

    <DataMember()> _
    Public Property HorasAvisoExpiracion() As Integer
        Get
            Return _HorasAvisoExpiracion
        End Get
        Set(ByVal value As Integer)
            _HorasAvisoExpiracion = value
        End Set
    End Property

    <DataMember()> _
    Public Property EnvioGenerarCip() As Boolean
        Get
            Return _EnvioGenerarCip
        End Get
        Set(ByVal value As Boolean)
            _EnvioGenerarCip = value
        End Set
    End Property

    <DataMember()> _
    Public Property EnvioEmailOperador() As Boolean
        Get
            Return _EnvioEmailOperador
        End Get
        Set(ByVal value As Boolean)
            _EnvioEmailOperador = value
        End Set
    End Property

    <DataMember()> _
    Public Property EmailOperador() As String
        Get
            Return _EmailOperador
        End Get
        Set(ByVal value As String)
            _EmailOperador = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreLogo() As String
        Get
            Return _NombreLogo
        End Get
        Set(ByVal value As String)
            _NombreLogo = value
        End Set
    End Property
    'Cobro Masivo - Fin


    Public Class CodigoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEServicio).Codigo.CompareTo(CType(y, BEServicio).Codigo)
        End Function
    End Class
    Public Class NombreEmpresaContranteComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEServicio).NombreEmpresaContrante.CompareTo(CType(y, BEServicio).NombreEmpresaContrante)
        End Function
    End Class
    Public Class NombreComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEServicio).Nombre.CompareTo(CType(y, BEServicio).Nombre)
        End Function
    End Class
    Public Class TiempoExpiracionComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEServicio).TiempoExpiracion.CompareTo(CType(y, BEServicio).TiempoExpiracion)
        End Function
    End Class
    Public Class HabilitaCIP
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEServicio).IntegraPasarelaMedioPagoCIP.CompareTo(CType(y, BEServicio).IntegraPasarelaMedioPagoCIP)
        End Function
    End Class

    Public Class DescripcionEstadoComparer
        Implements IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Public Function Compare(ByVal x As _3Dev.FW.Entidades.BusinessEntityBase, ByVal y As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements System.Collections.Generic.IComparer(Of _3Dev.FW.Entidades.BusinessEntityBase).Compare
            Return CType(x, BEServicio).DescripcionEstado.CompareTo(CType(y, BEServicio).DescripcionEstado)
        End Function
    End Class

End Class
