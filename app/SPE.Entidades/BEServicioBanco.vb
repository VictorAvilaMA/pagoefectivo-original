﻿
Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util



<Serializable()> <DataContract()> Public Class BEServicioBanco
    Inherits BEAuditoria

    Private _IdServicioBanco As Integer
    Private _IdServicio As Integer
    Private _DescripcionServicio As String 'N
    Private _IdEmpresaContratante As Integer
    Private _DescripcionEmpresa As String
    Private _IdMoneda As Integer
    Private _DescripcionMoneda As String
    Private _IdBanco As Integer
    Private _DescripcionBanco As String 'N
    Private _CodigoServicio As String 'N
    Private _IdEstado As Integer
    Private _EstadoDescripcion As String
    Private _idCuentaBanco As Integer 'N
    Private _NumeroCuenta As String 'N
    Private _FlagVisibleGenPago As Boolean

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "Consultar"
                Me.IdServicioBanco = DataUtil.ObjectToInt32(reader("IdServicioBanco"))
                Me.idEmpresaContratante = DataUtil.ObjectToInt(reader("IdEmpresaContratante"))
                Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("RazonSocial"))
                Me.idServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("servicio"))
                Me.IdBanco = DataUtil.ObjectToInt(reader("IdBanco"))
                Me.DescripcionBanco = DataUtil.ObjectToString(reader("banco"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("moneda"))
                Me.CodigoServicio = DataUtil.ObjectToString(reader("Codigo"))
                Me.IdEstado = DataUtil.ObjectToString(reader("IdEstado"))
                Me.EstadoDescripcion = DataUtil.ObjectToString(reader("Estado"))
                Me.idCuentaBanco = DataUtil.ObjectToInt(reader("IdCuentaBanco"))
                Me.NumeroCuenta = DataUtil.ObjectToString(reader("NroCuenta"))
                Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaRegistro"))
                Me.FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
                Me.TotalPageNumbers = DataUtil.ObjectToInt32(reader("TOTAL_RESULT"))
                
            Case "Obtener"
                Me.idEmpresaContratante = DataUtil.ObjectToInt(reader("IdEmpresaContratante"))
                Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("RazonSocial"))
                Me.idServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("servicio"))
                Me.IdBanco = DataUtil.ObjectToInt(reader("IdBanco"))
                Me.DescripcionBanco = DataUtil.ObjectToString(reader("banco"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("moneda"))
                Me.CodigoServicio = DataUtil.ObjectToString(reader("Codigo"))
                Me.IdEstado = DataUtil.ObjectToString(reader("IdEstado"))
                Me.EstadoDescripcion = DataUtil.ObjectToString(reader("Estado"))
                Me.idCuentaBanco = DataUtil.ObjectToInt(reader("IdCuentaBanco"))
                Me.NumeroCuenta = DataUtil.ObjectToString(reader("NroCuenta"))
                Me.FlagVisibleGenPago = DataUtil.ObjectToBool(reader("FlagVisibleGenPago"))
        End Select
    End Sub

    <DataMember()>
    Public Property IdServicioBanco() As String
        Get
            Return _IdServicioBanco
        End Get
        Set(ByVal value As String)
            _IdServicioBanco = value

        End Set
    End Property

    <DataMember()> _
    Public Property CodigoServicio() As String
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As String)
            _CodigoServicio = value

        End Set
    End Property
    <DataMember()> _
    Public Property idCuentaBanco() As Integer
        Get
            Return _idCuentaBanco
        End Get
        Set(ByVal value As Integer)
            _idCuentaBanco = value

        End Set
    End Property

    <DataMember()> _
    Public Property idServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value

        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionServicio() As String
        Get
            Return _DescripcionServicio
        End Get
        Set(ByVal value As String)
            _DescripcionServicio = value

        End Set
    End Property

    <DataMember()> _
    Public Property idEmpresaContratante() As Integer
        Get
            Return _IdEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _IdEmpresaContratante = value

        End Set
    End Property

    <DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(ByVal value As Integer)
            _IdBanco = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionBanco() As String
        Get
            Return _DescripcionBanco
        End Get
        Set(ByVal value As String)
            _DescripcionBanco = value

        End Set
    End Property

    <DataMember()> _
    Public Property NumeroCuenta() As String
        Get
            Return _NumeroCuenta
        End Get
        Set(ByVal value As String)
            _NumeroCuenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property EstadoDescripcion() As String
        Get
            Return _EstadoDescripcion
        End Get
        Set(ByVal value As String)
            _EstadoDescripcion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value

        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionEmpresa() As String
        Get
            Return _DescripcionEmpresa
        End Get
        Set(ByVal value As String)
            _DescripcionEmpresa = value

        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionMoneda() As String
        Get
            Return _DescripcionMoneda
        End Get
        Set(ByVal value As String)
            _DescripcionMoneda = value

        End Set
    End Property
    <DataMember()> _
    Public Property FlagVisibleGenPago() As Boolean
        Get
            Return _FlagVisibleGenPago
        End Get
        Set(ByVal value As Boolean)
            _FlagVisibleGenPago = value

        End Set
    End Property


End Class
