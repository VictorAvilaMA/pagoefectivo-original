﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEServicioInstitucion

    'cabecera
    Private _MerchantID As String
    Private _CodUsuario As String
    Private _CodUsuarioComp As String
    Private _NombreUsuario As String
    Private _ApePatUsuario As String
    Private _ApeMatUsuario As String
    Private _EmailUsuario As String

    'detalle
    Private _NroDocumento As String
    Private _IdOrden As String
    Private _DescripcionOrden As String
    Private _Moneda As Integer
    Private _Importe As Decimal
    Private _FechaEmision As DateTime
    Private _FechaVencimiento As DateTime
    Private _Mora As Decimal
    Private _NumeroCargaDiaria As Integer
    Private _Tiempo As Integer
    Private _CantidadCarga As Integer
    Private _IdOrdenPago As Integer
    Private _Total As Decimal
    Private _ClaveAPI As String
    Private _IdEstado As Integer
    'respuesta
    Private _TipoError As Integer


    'descarga de archivo
    Private _IdServicio As Integer
    Private _NombreServicio As String
    Private _FechaCarga As DateTime
    Private _Tamano As Decimal
    Private _NombreArchivo As String
    Private _NombreArchivoTXT As String
    Private _NombreArchivoXLS As String
    Private _Orden As Integer

    'detalle archivo
    Private _NumeroOrdenPago As String
    Private _FechaCancelacion As DateTime


    'detalle carga
    Private _IdError As Integer = 0
    Private _CantCabArchivo As Integer = 0
    Private _CantDetArchivo As Integer = 0
    Private _CantCabProc As Integer = 0
    Private _CantDetProc As Integer = 0
    Private _CadenaErrores As String = ""



    <DataMember()> _
    Public Property MerchantID() As String
        Get
            Return _MerchantID
        End Get
        Set(ByVal value As String)
            _MerchantID = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodUsuario() As String
        Get
            Return _CodUsuario
        End Get
        Set(ByVal value As String)
            _CodUsuario = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodUsuarioComp() As String
        Get
            Return _CodUsuarioComp
        End Get
        Set(ByVal value As String)
            _CodUsuarioComp = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreUsuario() As String
        Get
            Return _NombreUsuario
        End Get
        Set(ByVal value As String)
            _NombreUsuario = value
        End Set
    End Property


    <DataMember()> _
    Public Property ApePatUsuario() As String
        Get
            Return _ApePatUsuario
        End Get
        Set(ByVal value As String)
            _ApePatUsuario = value
        End Set
    End Property

    <DataMember()> _
    Public Property ApeMatUsuario() As String
        Get
            Return _ApeMatUsuario
        End Get
        Set(ByVal value As String)
            _ApeMatUsuario = value
        End Set
    End Property


    <DataMember()> _
    Public Property EmailUsuario() As String
        Get
            Return _EmailUsuario
        End Get
        Set(ByVal value As String)
            _EmailUsuario = value
        End Set
    End Property




    <DataMember()> _
    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As String)
            _NroDocumento = value
        End Set
    End Property



    <DataMember()> _
    Public Property IdOrden() As Integer
        Get
            Return _IdOrden
        End Get
        Set(ByVal value As Integer)
            _IdOrden = value
        End Set
    End Property



    <DataMember()> _
    Public Property DescripcionOrden() As String
        Get
            Return _DescripcionOrden
        End Get
        Set(ByVal value As String)
            _DescripcionOrden = value
        End Set
    End Property



    <DataMember()> _
    Public Property Moneda() As Integer
        Get
            Return _Moneda
        End Get
        Set(ByVal value As Integer)
            _Moneda = value
        End Set
    End Property


    <DataMember()> _
    Public Property Importe() As Decimal
        Get
            Return _Importe
        End Get
        Set(ByVal value As Decimal)
            _Importe = value
        End Set
    End Property






    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaVencimiento() As DateTime
        Get
            Return _FechaVencimiento
        End Get
        Set(ByVal value As DateTime)
            _FechaVencimiento = value
        End Set
    End Property


    <DataMember()> _
    Public Property Mora() As Decimal
        Get
            Return _Mora
        End Get
        Set(ByVal value As Decimal)
            _Mora = value
        End Set
    End Property




    <DataMember()> _
    Public Property NumeroCargaDiaria() As Integer
        Get
            Return _NumeroCargaDiaria
        End Get
        Set(ByVal value As Integer)
            _NumeroCargaDiaria = value
        End Set
    End Property




    <DataMember()> _
    Public Property Tiempo() As Integer
        Get
            Return _Tiempo
        End Get
        Set(ByVal value As Integer)
            _Tiempo = value
        End Set

    End Property



    <DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Integer)
            _IdOrdenPago = value
        End Set

    End Property

    <DataMember()> _
    Public Property CantidadCarga() As Integer
        Get
            Return _CantidadCarga
        End Get
        Set(ByVal value As Integer)
            _CantidadCarga = value
        End Set

    End Property


    <DataMember()> _
    Public Property TipoError() As Integer
        Get
            Return _TipoError
        End Get
        Set(ByVal value As Integer)
            _TipoError = value
        End Set

    End Property


    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            _Total = value
        End Set
    End Property


    <DataMember()> _
    Public Property ClaveAPI() As String
        Get
            Return _ClaveAPI
        End Get
        Set(ByVal value As String)
            _ClaveAPI = value
        End Set
    End Property




    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property



    <DataMember()> _
    Public Property NombreServicio() As String
        Get
            Return _NombreServicio
        End Get
        Set(ByVal value As String)
            _NombreServicio = value
        End Set
    End Property



    <DataMember()> _
    Public Property FechaCarga() As DateTime
        Get
            Return _FechaCarga
        End Get
        Set(ByVal value As DateTime)
            _FechaCarga = value
        End Set
    End Property



    <DataMember()> _
    Public Property Tamano() As Decimal
        Get
            Return _Tamano
        End Get
        Set(ByVal value As Decimal)
            _Tamano = value
        End Set
    End Property


    <DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property





    <DataMember()> _
    Public Property NombreArchivoTXT() As String
        Get
            Return _NombreArchivoTXT
        End Get
        Set(ByVal value As String)
            _NombreArchivoTXT = value
        End Set
    End Property



    <DataMember()> _
    Public Property NombreArchivoXLS() As String
        Get
            Return _NombreArchivoXLS
        End Get
        Set(ByVal value As String)
            _NombreArchivoXLS = value
        End Set
    End Property


    <DataMember()> _
    Public Property Orden() As Integer
        Get
            Return _Orden
        End Get
        Set(ByVal value As Integer)
            _Orden = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCancelacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _NumeroOrdenPago
        End Get
        Set(ByVal value As String)
            _NumeroOrdenPago = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdError() As Integer
        Get
            Return _IdError
        End Get
        Set(ByVal value As Integer)
            _IdError = value
        End Set
    End Property


    <DataMember()> _
    Public Property CantCabArchivo() As Integer
        Get
            Return _CantCabArchivo
        End Get
        Set(ByVal value As Integer)
            _CantCabArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantDetArchivo() As Integer
        Get
            Return _CantDetArchivo
        End Get
        Set(ByVal value As Integer)
            _CantDetArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantCabProc() As Integer
        Get
            Return _CantCabProc
        End Get
        Set(ByVal value As Integer)
            _CantCabProc = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantDetProc() As Integer
        Get
            Return _CantDetProc
        End Get
        Set(ByVal value As Integer)
            _CantDetProc = value
        End Set
    End Property

    <DataMember()> _
    Public Property CadenaErrores() As String
        Get
            Return _CadenaErrores
        End Get
        Set(ByVal value As String)
            _CadenaErrores = value
        End Set
    End Property



    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property


    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "ConsultaPago"
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.CodUsuario = DataUtil.ObjectToString(reader("CodUsuario"))
                Me.NombreUsuario = DataUtil.ObjectToString(reader("NombreUsuario"))
                Me.ApePatUsuario = DataUtil.ObjectToString(reader("ApePatUsuario"))
                Me.ApeMatUsuario = DataUtil.ObjectToString(reader("ApeMatUsuario"))
                Me.EmailUsuario = DataUtil.ObjectToString(reader("EmailUsuario"))
                Me.NroDocumento = DataUtil.ObjectToString(reader("NroDocumento"))
                Me.IdOrden = DataUtil.ObjectToInt(reader("IdOrden"))
                Me.DescripcionOrden = DataUtil.ObjectToString(reader("DescripcionOrden"))
                Me.Moneda = DataUtil.ObjectToInt(reader("Moneda"))
                Me.Importe = DataUtil.ObjectToDecimal(reader("Importe"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaVencimiento = DataUtil.ObjectToDateTime(reader("FechaVencimiento"))
                Me.Mora = DataUtil.ObjectToDecimal(reader("Mora"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenpago"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.ClaveAPI = DataUtil.ObjectToString(reader("ClaveAPI"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))

            Case "AnularCIP"
                Me.NroDocumento = DataUtil.ObjectToString(reader("NroDocumento"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenpago"))

            Case "ConsultarDescarga"
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.NombreServicio = DataUtil.ObjectToString(reader("NombreServicio"))
                Me.FechaCarga = DataUtil.ObjectToDateTime(reader("FechaCarga"))
                Me.Tamano = DataUtil.ObjectToDecimal(reader("Tamano"))
                Me.NombreArchivoTXT = DataUtil.ObjectToString(reader("NombreArchivoTXT"))
                Me.NombreArchivoXLS = DataUtil.ObjectToString(reader("NombreArchivoXLS"))
                Me.Orden = DataUtil.ObjectToInt(reader("Orden"))

            Case "ConsultarDetalle"
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.CodUsuario = DataUtil.ObjectToString(reader("CodUsuario"))
                Me.CodUsuarioComp = DataUtil.ObjectToString(reader("CodUsuarioComp"))
                Me.IdOrden = DataUtil.ObjectToInt(reader("IdOrden"))
                Me.NroDocumento = DataUtil.ObjectToString(reader("NroDocumento"))
                Me.DescripcionOrden = DataUtil.ObjectToString(reader("DescripcionOrden"))
                Me.Moneda = DataUtil.ObjectToInt(reader("Moneda"))
                Me.Importe = DataUtil.ObjectToDecimal(reader("Importe"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaVencimiento = DataUtil.ObjectToDateTime(reader("FechaVencimiento"))
                Me.Mora = DataUtil.ObjectToDecimal(reader("Mora"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.NumeroOrdenPago = DataUtil.ObjectToInt(reader("NumeroOrdenPago"))
        End Select
    End Sub



End Class

