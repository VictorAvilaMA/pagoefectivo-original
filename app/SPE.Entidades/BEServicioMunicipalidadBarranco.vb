﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEServicioMunicipalidadBarranco

    'Cabecera
    Private _MerchantID As String
    Private _CodigoActual As String
    Private _CodigoContribuyenteComp As String
    Private _NombreContribuyente As String
    Private _ApellidoPaternoContribuyente As String
    Private _ApellidoMaternoContribuyente As String
    Private _EmailContribuyente As String

    'Detalle
    Private _IdOrden As String
    Private _SerieNroDocumento As String
    Private _DescripcionOrden As String
    Private _ConceptoDocPago As String
    Private _MonedaDocPago As String
    Private _ImporteDocPago As Decimal
    Private _FechaEmision As DateTime
    Private _FechaVencimiento As DateTime
    Private _ImporteMora As Decimal
    Private _NumeroCargaDiaria As Integer
    Private _Tiempo As Integer
    Private _IdOrdenPago As Int64
    Private _FechaCarga As DateTime
    Private _Total As Decimal

    Private _strFechaVencimiento As String
    Private _strImporteMora As String

    'respuesta
    Private _ClaveAPI As String
    Private _IdEstado As Integer
    Private _TipoError As Integer

    'Descarga de archivo
    Private _IdServicio As Integer
    Private _NombreServicio As String
    Private _Tamano As Decimal
    Private _NombreArchivo As String
    Private _NombreArchivoTXT As String
    Private _NombreArchivoXLS As String
    Private _Orden As Integer

    'Detalle archivo
    Private _NumeroOrdenPago As String
    Private _FechaCancelacion As DateTime

    'Detalle carga
    Private _IdError As Integer = 0
    Private _CantCabArchivo As Integer = 0
    Private _CantDetArchivo As Integer = 0
    Private _CantCabProc As Integer = 0
    Private _CantDetProc As Integer = 0
    Private _CadenaErrores As String = ""

    'Cabecera

    <DataMember()> _
    Public Property MerchantID() As String
        Get
            Return _MerchantID
        End Get
        Set(ByVal value As String)
            _MerchantID = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoActual() As String
        Get
            Return _CodigoActual
        End Get
        Set(ByVal value As String)
            _CodigoActual = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoContribuyenteComp() As String
        Get
            Return _CodigoContribuyenteComp
        End Get
        Set(ByVal value As String)
            _CodigoContribuyenteComp = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreContribuyente() As String
        Get
            Return _NombreContribuyente
        End Get
        Set(ByVal value As String)
            _NombreContribuyente = value
        End Set
    End Property

    <DataMember()> _
    Public Property ApellidoPaternoContribuyente() As String
        Get
            Return _ApellidoPaternoContribuyente
        End Get
        Set(ByVal value As String)
            _ApellidoPaternoContribuyente = value
        End Set
    End Property

    <DataMember()> _
    Public Property ApellidoMaternoContribuyente() As String
        Get
            Return _ApellidoMaternoContribuyente
        End Get
        Set(ByVal value As String)
            _ApellidoMaternoContribuyente = value
        End Set
    End Property

    <DataMember()> _
    Public Property EmailContribuyente() As String
        Get
            Return _EmailContribuyente
        End Get
        Set(ByVal value As String)
            _EmailContribuyente = value
        End Set
    End Property

    'Fin de cabecera

    'Detalle

    <DataMember()> _
    Public Property IdOrden() As String
        Get
            Return _IdOrden
        End Get
        Set(ByVal value As String)
            _IdOrden = value
        End Set
    End Property

    <DataMember()> _
    Public Property SerieNroDocumento() As String
        Get
            Return _SerieNroDocumento
        End Get
        Set(ByVal value As String)
            _SerieNroDocumento = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionOrden() As String
        Get
            Return _DescripcionOrden
        End Get
        Set(ByVal value As String)
            _DescripcionOrden = value
        End Set
    End Property

    <DataMember()> _
    Public Property ConceptoDocPago() As String
        Get
            Return _ConceptoDocPago
        End Get
        Set(ByVal value As String)
            _ConceptoDocPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property MonedaDocPago() As String
        Get
            Return _MonedaDocPago
        End Get
        Set(ByVal value As String)
            _MonedaDocPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property ImporteDocPago() As Decimal
        Get
            Return _ImporteDocPago
        End Get
        Set(ByVal value As Decimal)
            _ImporteDocPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaVencimiento() As DateTime
        Get
            Return _FechaVencimiento
        End Get
        Set(ByVal value As DateTime)
            _FechaVencimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property ImporteMora() As Decimal
        Get
            Return _ImporteMora
        End Get
        Set(ByVal value As Decimal)
            _ImporteMora = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroCargaDiaria() As Integer
        Get
            Return _NumeroCargaDiaria
        End Get
        Set(ByVal value As Integer)
            _NumeroCargaDiaria = value
        End Set
    End Property

    <DataMember()> _
    Public Property Tiempo() As Integer
        Get
            Return _Tiempo
        End Get
        Set(ByVal value As Integer)
            _Tiempo = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrdenPago() As Int64
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Int64)
            _IdOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCarga() As DateTime
        Get
            Return _FechaCarga
        End Get
        Set(ByVal value As DateTime)
            _FechaCarga = value
        End Set
    End Property

    <DataMember()> _
    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            _Total = value
        End Set
    End Property

    <DataMember()> _
    Public Property ClaveAPI() As String
        Get
            Return _ClaveAPI
        End Get
        Set(ByVal value As String)
            _ClaveAPI = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property TipoError() As Integer
        Get
            Return _TipoError
        End Get
        Set(ByVal value As Integer)
            _TipoError = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreServicio() As String
        Get
            Return _NombreServicio
        End Get
        Set(ByVal value As String)
            _NombreServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property Tamano() As Decimal
        Get
            Return _Tamano
        End Get
        Set(ByVal value As Decimal)
            _Tamano = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _NombreArchivo
        End Get
        Set(ByVal value As String)
            _NombreArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreArchivoTXT() As String
        Get
            Return _NombreArchivoTXT
        End Get
        Set(ByVal value As String)
            _NombreArchivoTXT = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreArchivoXLS() As String
        Get
            Return _NombreArchivoXLS
        End Get
        Set(ByVal value As String)
            _NombreArchivoXLS = value
        End Set
    End Property

    <DataMember()> _
    Public Property Orden() As Integer
        Get
            Return _Orden
        End Get
        Set(ByVal value As Integer)
            _Orden = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCancelacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOrdenPago() As Integer
        Get
            Return _NumeroOrdenPago
        End Get
        Set(ByVal value As Integer)
            _NumeroOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantCabArchivo() As Integer
        Get
            Return _CantCabArchivo
        End Get
        Set(ByVal value As Integer)
            _CantCabArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantDetArchivo() As Integer
        Get
            Return _CantDetArchivo
        End Get
        Set(ByVal value As Integer)
            _CantDetArchivo = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantCabProc() As Integer
        Get
            Return _CantCabProc
        End Get
        Set(ByVal value As Integer)
            _CantCabProc = value
        End Set
    End Property

    <DataMember()> _
    Public Property CantDetProc() As Integer
        Get
            Return _CantDetProc
        End Get
        Set(ByVal value As Integer)
            _CantDetProc = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdError() As Integer
        Get
            Return _IdError
        End Get
        Set(ByVal value As Integer)
            _IdError = value
        End Set
    End Property

    <DataMember()> _
    Public Property CadenaErrores() As String
        Get
            Return _CadenaErrores
        End Get
        Set(ByVal value As String)
            _CadenaErrores = value
        End Set
    End Property

    <DataMember()> _
    Public Property strFechaVencimiento() As String
        Get
            Return _strFechaVencimiento
        End Get
        Set(ByVal value As String)
            _strFechaVencimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property strImporteMora() As String
        Get
            Return _strImporteMora
        End Get
        Set(ByVal value As String)
            _strImporteMora = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "ConsultaPago"
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.CodigoActual = DataUtil.ObjectToString(reader("CodigoActual"))
                Me.NombreContribuyente = DataUtil.ObjectToString(reader("NombreContribuyente"))
                Me.ApellidoPaternoContribuyente = DataUtil.ObjectToString(reader("ApellidoPaternoContribuyente"))
                Me.ApellidoMaternoContribuyente = DataUtil.ObjectToString(reader("ApellidoMaternoContribuyente"))
                Me.EmailContribuyente = DataUtil.ObjectToString(reader("EmailContribuyente"))
                Me.SerieNroDocumento = DataUtil.ObjectToString(reader("SerieNroDocumento"))
                Me.IdOrden = DataUtil.ObjectToInt(reader("IdOrden"))
                Me.DescripcionOrden = DataUtil.ObjectToString(reader("DescripcionOrden"))
                Me.MonedaDocPago = DataUtil.ObjectToInt(reader("MonedaDocPago"))
                Me.ImporteDocPago = DataUtil.ObjectToDecimal(reader("ImporteDocPago"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaVencimiento = DataUtil.ObjectToDateTime(reader("FechaVencimiento"))
                Me.ImporteMora = DataUtil.ObjectToDecimal(reader("ImporteMora"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenpago"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.ClaveAPI = DataUtil.ObjectToString(reader("ClaveAPI"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
            Case "AnularCIP"
                Me.SerieNroDocumento = DataUtil.ObjectToString(reader("SerieNroDocumento"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenpago"))
            Case "ConsultarDescarga"
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.NombreServicio = DataUtil.ObjectToString(reader("NombreServicio"))
                Me.FechaCarga = DataUtil.ObjectToDateTime(reader("FechaCarga"))
                Me.Tamano = DataUtil.ObjectToDecimal(reader("Tamano"))
                Me.NombreArchivoTXT = DataUtil.ObjectToString(reader("NombreArchivoTXT"))
                Me.NombreArchivoXLS = DataUtil.ObjectToString(reader("NombreArchivoXLS"))
                Me.Orden = DataUtil.ObjectToInt(reader("Orden"))
            Case "ConsultarDetalle"
                Me.MerchantID = DataUtil.ObjectToString(reader("MerchantID"))
                Me.CodigoActual = DataUtil.ObjectToString(reader("CodigoActual"))
                Me.CodigoContribuyenteComp = DataUtil.ObjectToString(reader("CodigoContribuyenteComp"))
                Me.IdOrden = DataUtil.ObjectToInt(reader("IdOrden"))
                Me.SerieNroDocumento = DataUtil.ObjectToString(reader("SerieNroDocumento"))
                Me.DescripcionOrden = DataUtil.ObjectToString(reader("DescripcionOrden"))
                Me.MonedaDocPago = DataUtil.ObjectToInt(reader("MonedaDocPago"))
                Me.ImporteDocPago = DataUtil.ObjectToDecimal(reader("ImporteDocPago"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))

                If DataUtil.ObjectToString(reader("FechaVencimiento")) = String.Empty Then
                    Me.strFechaVencimiento = String.Empty
                Else
                    Me.strFechaVencimiento = DataUtil.ObjectToString(reader("FechaVencimiento"))
                End If

                If DataUtil.ObjectToString(reader("ImporteMora")) = String.Empty Then
                    Me.strImporteMora = String.Empty
                Else
                    Me.strImporteMora = DataUtil.ObjectToString(reader("ImporteMora"))
                End If

                'Me.strFechaVencimiento = DataUtil.ObjectToString(reader("FechaVencimiento"))
                'Me.strImporteMora = DataUtil.ObjectToString(reader("ImporteMora"))
                Me.Total = DataUtil.ObjectToDecimal(reader("Total"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.NumeroOrdenPago = DataUtil.ObjectToInt(reader("NumeroOrdenPago"))
        End Select
    End Sub

End Class

