﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports System.Web

<Serializable()> <DataContract()>
Public Class BEServicioMunicipalidadBarrancoRequest


    Private _bytes As Byte()
    <DataMember()> _
    Public Property Bytes() As Byte()
        Get
            Return _bytes
        End Get
        Set(ByVal value As Byte())
            _bytes = value
        End Set
    End Property

    Private _tipoArchivo As String
    <DataMember()> _
    Public Property TipoArchivo() As String
        Get
            Return _tipoArchivo
        End Get
        Set(ByVal value As String)
            _tipoArchivo = value
        End Set
    End Property

    Private _nombreArchivo As String
    <DataMember()> _
    Public Property NombreArchivo() As String
        Get
            Return _nombreArchivo
        End Get
        Set(ByVal value As String)
            _nombreArchivo = value
        End Set
    End Property



    Private _idServicio As Integer
    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    Private _MerchantID As String
    <DataMember()> _
    Public Property MerchantID() As String
        Get
            Return _MerchantID
        End Get
        Set(ByVal value As String)
            _MerchantID = value
        End Set
    End Property

    Private _FechaEmision As DateTime
    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property


    Private _NumeroCargaDiaria As Integer
    <DataMember()> _
    Public Property NumeroCargaDiaria() As Integer
        Get
            Return _NumeroCargaDiaria
        End Get
        Set(ByVal value As Integer)
            _NumeroCargaDiaria = value
        End Set
    End Property


    Private _CodUsuario As String
    <DataMember()> _
    Public Property CodUsuario() As String
        Get
            Return _CodUsuario
        End Get
        Set(ByVal value As String)
            _CodUsuario = value
        End Set
    End Property

    Private _IdOrdenPago As Integer
    <DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Integer)
            _IdOrdenPago = value
        End Set

    End Property

    Private _NroDocumento As String
    <DataMember()> _
    Public Property NroDocumento() As String
        Get
            Return _NroDocumento
        End Get
        Set(ByVal value As String)
            _NroDocumento = value
        End Set
    End Property






    Private _FechaCargaInicial As DateTime
    <DataMember()> _
    Public Property FechaCargaInicial() As DateTime
        Get
            Return _FechaCargaInicial
        End Get
        Set(ByVal value As DateTime)
            _FechaCargaInicial = value
        End Set
    End Property


    Private _FechaCargaFinal As DateTime
    <DataMember()> _
    Public Property FechaCargaFinal() As DateTime
        Get
            Return _FechaCargaFinal
        End Get
        Set(ByVal value As DateTime)
            _FechaCargaFinal = value
        End Set
    End Property




    Private _IdUsuarioActualizacion As Integer
    <DataMember()> _
    Public Property IdUsuarioActualizacion() As Integer
        Get
            Return _IdUsuarioActualizacion
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioActualizacion = value
        End Set

    End Property


    Private _IdOrigenEliminacion As Integer
    <DataMember()> _
    Public Property IdOrigenEliminacion() As Integer
        Get
            Return _IdOrigenEliminacion
        End Get
        Set(ByVal value As Integer)
            _IdOrigenEliminacion = value
        End Set

    End Property


    Private _GCParametro As String
    <DataMember()> _
    Public Property GCParametro() As String
        Get
            Return _GCParametro
        End Get
        Set(ByVal value As String)
            _GCParametro = value
        End Set
    End Property


    Private _DesParametro As String
    <DataMember()> _
    Public Property DesParametro() As String
        Get
            Return _DesParametro
        End Get
        Set(ByVal value As String)
            _DesParametro = value
        End Set
    End Property




End Class
