Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Web
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Entidades

<Serializable()> <DataContract()> Public Class BEServicioNotificacion
    Inherits BusinessEntityBase

    Private _IdServicioNotificacion As Int64
    Private _IdOrdenPago As Int64
    Private _IdMovimiento As Int64
    Private _IdTipoServNotificacion As Integer
    Private _UrlRespuesta As String
    Private _IdEstado As Integer
    Private _FechaCreacion As DateTime
    Private _IdUsuarioCreacion As Integer
    Private _FechaNotificacion As DateTime
    Private _Observacion As String
    Private _IdOrigenRegistro As Integer
    '<add Peru.Com FaseIII>
    Private _IdSolicitudPago
    Private _IdTipoMovimiento As Integer
    Private _IdGrupoNotificacion As Integer

    'SagaInicio
    Private _OrderIdSaga As String
    Private _FechaPagoSaga As DateTime
    Private _FechaContableSaga As DateTime
    Private _MontoCanceladoSaga As Decimal
    Private _CajaSaga As String
    Private _CodigoAutorizacionServipagSaga As String
    Private _TipoTransaccionSaga As Integer
    'SagaFin
    Private _Negocio As String
    Private _Canal As String

    'RipleyInicio
    Private _OrderIdRipley As String
    Private _NumeroOrdenDePago As String
    Private _FechaPagoRipley As DateTime
    Private _FechaContableRipley As DateTime
    Private _MontoCanceladoRipley As Decimal
    Private _CajaRipley As String
    Private _CodigoAutorizacionServipagRipley As String
    Private _TipoTransaccionRipley As Integer

    Private _TipoTransaccionSodimac As Integer

    'RipleyFin

    Public Sub New()

    End Sub

    Public Enum Servicio
        Saga
        Ripley
        Sodimac
    End Enum


    Public Sub New(ByVal reader As IDataReader)
        _IdServicioNotificacion = ObjectToInt64(reader("idServicioNotificacion"))
        _IdOrdenPago = ObjectToInt64(reader("IdOrdenPago"))
        _IdMovimiento = ObjectToInt64(reader("IdMovimiento"))
        _IdTipoServNotificacion = ObjectToInt(reader("IdTipoServNotificacion"))
        _UrlRespuesta = ObjectToString(reader("UrlRespuesta"))
        _IdEstado = ObjectToInt(reader("IdEstado"))
        _FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        _IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
        _FechaNotificacion = ObjectToDateTime(reader("FechaNotificacion"))
        _cantNotificaciones = ObjectToInt(reader("CantNotificaciones"))
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal num As Integer)
        _IdServicioNotificacion = ObjectToInt64(reader("idServicioNotificacion"))
        _IdOrdenPago = ObjectToInt64(reader("IdOrdenPago"))
        _IdMovimiento = ObjectToInt64(reader("IdMovimiento"))
        _IdTipoServNotificacion = ObjectToInt(reader("IdTipoServNotificacion"))
        _UrlRespuesta = ObjectToString(reader("UrlRespuesta"))
        _IdEstado = ObjectToInt(reader("IdEstado"))
        _FechaCreacion = ObjectToDateTime(reader("FechaCreacion"))
        _IdUsuarioCreacion = ObjectToInt(reader("IdUsuarioCreacion"))
        _FechaNotificacion = ObjectToDateTime(reader("FechaNotificacion"))
        _cantNotificaciones = ObjectToInt(reader("CantNotificaciones"))
        _IdOrigenRegistro = ObjectToInt(reader("IdOrigenRegistro"))
    End Sub

    'SagaInicio
    Public Sub New(ByVal reader As IDataReader, ByVal num As Integer, ByVal num2 As Int32, ByVal num3 As Int32, ByVal tipoServicio As Servicio)
        If tipoServicio = Servicio.Saga Then
            _OrderIdSaga = ObjectToString(reader("OrderIdComercio"))
            _FechaPagoSaga = ObjectToDateTime(reader("FechaCancelacion"))
            _FechaContableSaga = ObjectToDateTime(reader("FechaCancelacion"))
            _MontoCanceladoSaga = ObjectToDecimal(reader("Total"))
            _Canal = ObjectToString(reader("Canal"))
            _CajaSaga = ObjectToString(reader("CodigoBanco"))
            _CodigoAutorizacionServipagSaga = ObjectToString(reader("NumeroOperacion"))
            _TipoTransaccionSaga = ObjectToInt(reader("TipoTransaccionSaga"))
        ElseIf tipoServicio = Servicio.Ripley Then
            _OrderIdRipley = ObjectToString(reader("OrderIdComercio"))
            _NumeroOrdenDePago = ObjectToString(reader("NumeroOrdenDePago"))
            _FechaPagoRipley = ObjectToDateTime(reader("FechaCancelacion"))
            _FechaContableRipley = ObjectToDateTime(reader("FechaCancelacion"))
            _MontoCanceladoRipley = ObjectToDecimal(reader("Total"))
            _Canal = ObjectToString(reader("Canal"))
            _CajaRipley = ObjectToString(reader("CodigoBanco"))
            _CodigoAutorizacionServipagRipley = ObjectToString(reader("NumeroOperacion"))
            _TipoTransaccionRipley = ObjectToInt(reader("TipoTransaccionRipley"))
        ElseIf tipoServicio = Servicio.Sodimac Then
            _OrderIdSodimac = ObjectToString(reader("OrderIdComercio"))
            _FechaPagoSodimac = ObjectToDateTime(reader("FechaCancelacion"))
            _FechaContableSodimac = ObjectToDateTime(reader("FechaCancelacion"))
            _MontoCanceladoSodimac = ObjectToDecimal(reader("Total"))
            _Canal = ObjectToString(reader("Canal"))
            _CajaSodimac = ObjectToString(reader("CodigoBanco"))
            _CodigoAutorizacionServipagSodimac = ObjectToString(reader("NumeroOperacion"))
            _TipoTransaccionSodimac = ObjectToInt(reader("TipoTransaccionSodimac"))
        End If

    End Sub


    Public Sub New(ByVal reader As IDataReader, ByVal num As Integer, ByVal num2 As Int32, ByVal tipoServicio As Servicio)
        If tipoServicio = Servicio.Saga Then
            _OrderIdSaga = ObjectToString(reader("OrderIdComercio"))
            _FechaPagoSaga = ObjectToDateTime(reader("FechaCancelacion"))
            _FechaContableSaga = ObjectToDateTime(reader("FechaCancelacion"))
            _MontoCanceladoSaga = ObjectToDecimal(reader("Total"))
            _Canal = ObjectToString(reader("Canal"))
            _CajaSaga = ObjectToString(reader("CodigoBanco"))
            _CodigoAutorizacionServipagSaga = ObjectToString(reader("NumeroOperacion"))
        ElseIf tipoServicio = Servicio.Ripley Then
            _OrderIdRipley = ObjectToString(reader("OrderIdComercio"))
            _FechaPagoRipley = ObjectToDateTime(reader("FechaCancelacion"))
            _FechaContableRipley = ObjectToDateTime(reader("FechaCancelacion"))
            _MontoCanceladoRipley = ObjectToDecimal(reader("Total"))
            _Canal = ObjectToString(reader("Canal"))
            _CajaRipley = ObjectToString(reader("CodigoBanco"))
            _CodigoAutorizacionServipagRipley = ObjectToString(reader("NumeroOperacion"))
            _NumeroOrdenDePago = ObjectToString(reader("CIP"))
        ElseIf tipoServicio = Servicio.Sodimac Then
            _OrderIdSodimac = ObjectToString(reader("OrderIdComercio"))
            _FechaPagoSodimac = ObjectToDateTime(reader("FechaCancelacion"))
            _FechaContableSodimac = ObjectToDateTime(reader("FechaCancelacion"))
            _MontoCanceladoSodimac = ObjectToDecimal(reader("Total"))
            _Canal = ObjectToString(reader("Canal"))
            _CajaSodimac = ObjectToString(reader("CodigoBanco"))
            _CodigoAutorizacionServipagSodimac = ObjectToString(reader("NumeroOperacion"))
        End If

    End Sub

    'SagaFin

    <DataMember()> _
    Public Property IdServicioNotificacion() As Int64
        Get
            Return _IdServicioNotificacion
        End Get
        Set(ByVal value As Int64)
            _IdServicioNotificacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrdenPago() As Int64
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Int64)
            _IdOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMovimiento() As Int64
        Get
            Return _IdMovimiento
        End Get
        Set(ByVal value As Int64)
            _IdMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTipoServNotificacion() As Integer
        Get
            Return _IdTipoServNotificacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoServNotificacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property UrlRespuesta() As String
        Get
            Return _UrlRespuesta
        End Get
        Set(ByVal value As String)
            _UrlRespuesta = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuarioCreacion() As Int64
        Get
            Return _IdUsuarioCreacion
        End Get
        Set(ByVal value As Int64)
            _IdUsuarioCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaNotificacion() As DateTime
        Get
            Return _FechaNotificacion
        End Get
        Set(ByVal value As DateTime)
            _FechaNotificacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property

    Private _cantNotificaciones As Integer
    <DataMember()> _
    Public Property CantNotificaciones() As Integer
        Get
            Return _cantNotificaciones
        End Get
        Set(ByVal value As Integer)
            _cantNotificaciones = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrigenRegistro() As Integer
        Get
            Return _IdOrigenRegistro
        End Get
        Set(ByVal value As Integer)
            _IdOrigenRegistro = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdSolicitudPago() As Integer
        Get
            Return _IdSolicitudPago
        End Get
        Set(ByVal value As Integer)
            _IdSolicitudPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdGrupoNotificacion() As Integer
        Get
            Return _IdGrupoNotificacion
        End Get
        Set(ByVal value As Integer)
            _IdGrupoNotificacion = value
        End Set
    End Property

    'SagaInicio
    <DataMember()> _
    Public Property TipoTransaccionSaga() As Integer
        Get
            Return _TipoTransaccionSaga
        End Get
        Set(ByVal value As Integer)
            _TipoTransaccionSaga = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoAutorizacionServipagSaga() As String
        Get
            Return _CodigoAutorizacionServipagSaga
        End Get
        Set(ByVal value As String)
            _CodigoAutorizacionServipagSaga = value
        End Set
    End Property

    <DataMember()> _
    Public Property CajaSaga() As String
        Get
            Return _CajaSaga
        End Get
        Set(ByVal value As String)
            _CajaSaga = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoCanceladoSaga() As Decimal
        Get
            Return _MontoCanceladoSaga
        End Get
        Set(ByVal value As Decimal)
            _MontoCanceladoSaga = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaContableSaga() As DateTime
        Get
            Return _FechaContableSaga
        End Get
        Set(ByVal value As DateTime)
            _FechaContableSaga = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaPagoSaga() As DateTime
        Get
            Return _FechaPagoSaga
        End Get
        Set(ByVal value As DateTime)
            _FechaPagoSaga = value
        End Set
    End Property

    <DataMember()> _
    Public Property OrderIdSaga() As String
        Get
            Return _OrderIdSaga
        End Get
        Set(ByVal value As String)
            _OrderIdSaga = value
        End Set
    End Property


    <DataMember()> _
    Public Property Negocio() As String
        Get
            Return _Negocio
        End Get
        Set(ByVal value As String)
            _Negocio = value
        End Set
    End Property

    <DataMember()> _
    Public Property Canal() As String
        Get
            Return _Canal
        End Get
        Set(ByVal value As String)
            _Canal = value
        End Set
    End Property

    'SagaFin

#Region "Propiedades de Sodimac"


    Private _OrderIdSodimac As String

    <DataMember()> _
    Public Property OrderIdSodimac() As String
        Get
            Return _OrderIdSodimac
        End Get
        Set(ByVal value As String)
            _OrderIdSodimac = value
        End Set
    End Property


    Private _FechaPagoSodimac As Date

    <DataMember()> _
    Public Property FechaPagoSodimac() As Date
        Get
            Return _FechaPagoSodimac
        End Get
        Set(ByVal value As Date)
            _FechaPagoSodimac = value
        End Set
    End Property

    Private _FechaContableSodimac As Date

    <DataMember()> _
    Public Property FechaContableSodimac() As Date
        Get
            Return _FechaContableSodimac
        End Get
        Set(ByVal value As Date)
            _FechaContableSodimac = value
        End Set
    End Property


    Private _MontoCanceladoSodimac As Decimal

    <DataMember()> _
    Public Property MontoCanceladoSodimac() As Decimal
        Get
            Return _MontoCanceladoSodimac
        End Get
        Set(ByVal value As Decimal)
            _MontoCanceladoSodimac = value
        End Set
    End Property


    Private _CajaSodimac As String

    <DataMember()> _
    Public Property CajaSodimac() As String
        Get
            Return _CajaSodimac
        End Get
        Set(ByVal value As String)
            _CajaSodimac = value
        End Set
    End Property


    Private _CodigoAutorizacionServipagSodimac As String

    <DataMember()> _
    Public Property CodigoAutorizacionServipagSodimac() As String
        Get
            Return _CodigoAutorizacionServipagSodimac
        End Get
        Set(ByVal value As String)
            _CodigoAutorizacionServipagSodimac = value
        End Set
    End Property

    <DataMember()> _
    Public Property TipoTransaccionSodimac() As Integer
        Get
            Return _TipoTransaccionSodimac
        End Get
        Set(ByVal value As Integer)
            _TipoTransaccionSodimac = value
        End Set
    End Property

#End Region

#Region "Propiedades de Ripley"


    <DataMember()> _
    Public Property OrderIdRipley() As String
        Get
            Return _OrderIdRipley
        End Get
        Set(ByVal value As String)
            _OrderIdRipley = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOrdenDePago() As String
        Get
            Return _NumeroOrdenDePago
        End Get
        Set(ByVal value As String)
            _NumeroOrdenDePago = value
        End Set
    End Property


    <DataMember()> _
    Public Property FechaPagoRipley() As Date
        Get
            Return _FechaPagoRipley
        End Get
        Set(ByVal value As Date)
            _FechaPagoRipley = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaContableRipley() As Date
        Get
            Return _FechaContableRipley
        End Get
        Set(ByVal value As Date)
            _FechaContableRipley = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoCanceladoRipley() As Decimal
        Get
            Return _MontoCanceladoRipley
        End Get
        Set(ByVal value As Decimal)
            _MontoCanceladoRipley = value
        End Set
    End Property

    <DataMember()> _
    Public Property CajaRipley() As String
        Get
            Return _CajaRipley
        End Get
        Set(ByVal value As String)
            _CajaRipley = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoAutorizacionServipagRipley() As String
        Get
            Return _CodigoAutorizacionServipagRipley
        End Get
        Set(ByVal value As String)
            _CodigoAutorizacionServipagRipley = value
        End Set
    End Property

#End Region

End Class

