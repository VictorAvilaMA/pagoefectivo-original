﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util


<Serializable()> <DataContract()> _
Public Class BEServicioProducto
    Private _idProducto As Integer
    Private _idServicio As Integer
    Private _Descripcion As String
    Private _Monto As Decimal
    Private _idMoneda As Integer
    Private _DescMoneda As String
    Private _idEditable As Integer
    Private _idEstado As Integer
    Private _FechaCreacion As DateTime
    Private _UsuarioId As String



    <DataMember()> _
    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property

    <DataMember()> _
    Public Property idServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _Monto
        End Get
        Set(ByVal value As Decimal)
            _Monto = value
        End Set
    End Property

    <DataMember()> _
    Public Property idMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescMoneda() As String
        Get
            Return _DescMoneda
        End Get
        Set(ByVal value As String)
            _DescMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property idEditable() As Integer
        Get
            Return _idEditable
        End Get
        Set(ByVal value As Integer)
            _idEditable = value
        End Set
    End Property

    <DataMember()> _
    Public Property idEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _FechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property UsuarioId() As String
        Get
            Return _UsuarioId
        End Get
        Set(ByVal value As String)
            _UsuarioId = value
        End Set
    End Property



    Public Sub New()

    End Sub





    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "ConsultaProducto"
                Me.idProducto = DataUtil.ObjectToInt(reader("idProducto"))
                Me.idServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.Descripcion = DataUtil.ObjectToString(reader("Descripcion"))
                Me.Monto = DataUtil.ObjectToDecimal(reader("Monto"))
                Me.idMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.DescMoneda = DataUtil.ObjectToString(reader("DescMoneda"))
                Me.idEditable = DataUtil.ObjectToInt(reader("idEditable"))
                Me.idEstado = DataUtil.ObjectToInt(reader("idEstado"))
                Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
                Me.UsuarioId = DataUtil.ObjectToString(reader("UsuarioId"))
            Case "ListaProducto"
                Me.idProducto = DataUtil.ObjectToInt(reader("IdProducto"))
                Me.Descripcion = DataUtil.ObjectToString(reader("Descripcion"))
                Me.Monto = DataUtil.ObjectToDecimal(reader("Monto"))
                Me.idEditable = DataUtil.ObjectToInt(reader("idEditable"))
                Me.idMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
        End Select
    End Sub










End Class
