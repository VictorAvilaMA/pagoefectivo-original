﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util


<Serializable()> <DataContract()> _
Public Class BEServicioProductoRequest


    Private _idProducto As Integer
    <DataMember()> _
    Public Property idProducto() As Integer
        Get
            Return _idProducto
        End Get
        Set(ByVal value As Integer)
            _idProducto = value
        End Set
    End Property


    Private _idServicio As Integer
    <DataMember()> _
    Public Property idServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    


End Class
