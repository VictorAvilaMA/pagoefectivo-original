﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util



<Serializable()> <DataContract()> Public Class BESolicitudPago
    Inherits BEAuditoria

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "PorID"
                Me.IdSolicitudPago = DataUtil.ObjectToInt64(reader("IdSolicitudPago"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.Idestado = DataUtil.ObjectToInt(reader("IdEstado"))
                Me.IdServicio = DataUtil.ObjectToInt(reader("IdServicio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.MetodosPago = DataUtil.ObjectToString(reader("MetodosPago"))
                Me.Total = DataUtil.ObjectToDouble(reader("Total"))
                Me.CodServicio = DataUtil.ObjectToString(reader("CodServicio"))
                Me.CodTransaccion = DataUtil.ObjectToString(reader("CodTransaccion"))
                Me.MailComercio = DataUtil.ObjectToString(reader("MailComercio"))
                Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
                Me.FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
                Me.IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
                Me.IdUsuarioActualizacion = DataUtil.ObjectToInt(reader("IdUsuarioActualizacion"))
                Me.FechaExpiracion = DataUtil.ObjectToDateTime(reader("FechaExpiracion"))
                Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
                Me.UsuarioId = DataUtil.ObjectToString(reader("UsuarioId"))
                Me.UsuarioNombre = DataUtil.ObjectToString(reader("UsuarioNombre"))
                Me.UsuarioApellidos = DataUtil.ObjectToString(reader("UsuarioApellidos"))
                Me.UsuarioLocalidad = DataUtil.ObjectToString(reader("UsuarioLocalidad"))
                Me.UsuarioProvincia = DataUtil.ObjectToString(reader("UsuarioProvinicia"))
                Me.UsuarioPais = DataUtil.ObjectToString(reader("UsuarioPais"))
                Me.UsuarioAlias = DataUtil.ObjectToString(reader("UsuarioAlias"))
                Me.UsuarioEmail = DataUtil.ObjectToString(reader("UsuarioEmail"))
                Me.UsuarioTipoDoc = DataUtil.ObjectToString(reader("UsuarioTipoDoc"))
                Me.UsuarioNumDoc = DataUtil.ObjectToString(reader("UsuarioNumDoc"))
                Me.TramaSolicitud = DataUtil.ObjectToString(reader("TramaSolicitud"))
                Me.TiempoExpiracion = DataUtil.ObjectToDouble(reader("TiempoExpiracion"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))
                Me.Token = DataUtil.ObjectToString(reader("Token"))
                Me.AbreviaturaMoneda = DataUtil.ObjectToString(reader("AbrevMoneda"))
                Me.NombreMoneda = DataUtil.ObjectToString(reader("NombMoneda"))

            Case ("InfoExpiradas")
                Me.IdSolicitudPago = DataUtil.ObjectToInt64(reader("IdSolicitudPago"))
                Me.IdServicio = DataUtil.ObjectToInt64(reader("IdServicio"))
                Me.IdUsuarioCreacion = DataUtil.ObjectToInt64(reader("IdUsuarioCreacion"))
        End Select
    End Sub

    Private _idSolicitudPago As Int64
    <DataMember()> _
    Public Property IdSolicitudPago() As Int64
        Get
            Return _idSolicitudPago
        End Get
        Set(ByVal value As Int64)
            _idSolicitudPago = value

        End Set
    End Property
    Private _idOrdenPago As Int64
    <DataMember()> _
    Public Property IdOrdenPago() As Int64
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Int64)
            _idOrdenPago = value

        End Set
    End Property
    Private _idCliente As Integer
    <DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value

        End Set
    End Property
    Private _idestado As Integer
    <DataMember()> _
    Public Property Idestado() As Integer
        Get
            Return _idestado
        End Get
        Set(ByVal value As Integer)
            _idestado = value

        End Set
    End Property
    Private _idServicio As Integer
    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value

        End Set
    End Property
    Private _idMoneda As Integer
    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value

        End Set
    End Property
    Private _metodosPago As String
    <DataMember()> _
    Public Property MetodosPago() As String
        Get
            Return _metodosPago
        End Get
        Set(ByVal value As String)
            _metodosPago = value

        End Set
    End Property
    Private _total As Double
    <DataMember()> _
    Public Property Total As Double
        Get
            Return _total
        End Get
        Set(ByVal value As Double)
            _total = value

        End Set
    End Property
    Private _codServicio As String
    <DataMember()> _
    Public Property CodServicio As String
        Get
            Return _codServicio
        End Get
        Set(ByVal value As String)
            _codServicio = value

        End Set
    End Property
    Private _codTransaccion As String
    <DataMember()> _
    Public Property CodTransaccion As String
        Get
            Return _codTransaccion
        End Get
        Set(ByVal value As String)
            _codTransaccion = value
        End Set
    End Property
    Private _mailComercio As String
    <DataMember()> _
    Public Property MailComercio() As String
        Get
            Return _mailComercio
        End Get
        Set(ByVal value As String)
            _mailComercio = value

        End Set
    End Property
    Private _fechaExpiracion As DateTime
    <DataMember()> _
    Public Property FechaExpiracion() As DateTime
        Get
            Return _fechaExpiracion
        End Get
        Set(ByVal value As DateTime)
            _fechaExpiracion = value
        End Set
    End Property
    Private _dataAdicional As String
    <DataMember()> _
    Public Property DataAdicional() As String
        Get
            Return _dataAdicional
        End Get
        Set(ByVal value As String)
            _dataAdicional = value

        End Set
    End Property
    Private _usuariId As String
    <DataMember()> _
    Public Property UsuarioId() As String
        Get
            Return _usuariId
        End Get
        Set(ByVal value As String)
            _usuariId = value
        End Set
    End Property
    Private _usuarionNombre As String
    <DataMember()> _
    Public Property UsuarioNombre() As String
        Get
            Return _usuarionNombre
        End Get
        Set(ByVal value As String)
            _usuarionNombre = value
        End Set
    End Property
    Private _usuarioApellidos As String
    <DataMember()> _
    Public Property UsuarioApellidos() As String
        Get
            Return _usuarioApellidos
        End Get
        Set(ByVal value As String)
            _usuarioApellidos = value
        End Set
    End Property
    Private _usuarioLocalidad As String
    <DataMember()> _
    Public Property UsuarioLocalidad() As String
        Get
            Return _usuarioLocalidad
        End Get
        Set(ByVal value As String)
            _usuarioLocalidad = value
        End Set
    End Property
    Private _usuarioProvincia As String
    <DataMember()> _
    Public Property UsuarioProvincia() As String
        Get
            Return _usuarioProvincia
        End Get
        Set(ByVal value As String)
            _usuarioProvincia = value
        End Set
    End Property
    Private _usuarioPais As String
    <DataMember()> _
    Public Property UsuarioPais() As String
        Get
            Return _usuarioPais
        End Get
        Set(ByVal value As String)
            _usuarioPais = value
        End Set
    End Property
    Private _usuarioALias As String
    <DataMember()> _
    Public Property UsuarioAlias() As String
        Get
            Return _usuarioALias
        End Get
        Set(ByVal value As String)
            _usuarioALias = value
        End Set
    End Property
    Private _usuarioEmail As String
    <DataMember()> _
    Public Property UsuarioEmail() As String
        Get
            Return _usuarioEmail
        End Get
        Set(ByVal value As String)
            _usuarioEmail = value
        End Set
    End Property
    Private _usuarioTipoDoc As String
    <DataMember()> _
    Public Property UsuarioTipoDoc() As String
        Get
            Return _usuarioTipoDoc
        End Get
        Set(ByVal value As String)
            _usuarioTipoDoc = value
        End Set
    End Property
    Private _usuarioNumDoc As String
    <DataMember()> _
    Public Property UsuarioNumDoc() As String
        Get
            Return _usuarioNumDoc
        End Get
        Set(ByVal value As String)
            _usuarioNumDoc = value
        End Set
    End Property
    Private _tramaSolicitud As String
    <DataMember()> _
    Public Property TramaSolicitud() As String
        Get
            Return _tramaSolicitud
        End Get
        Set(ByVal value As String)
            _tramaSolicitud = value
        End Set
    End Property
    Private _tiempoExpiracion As String
    <DataMember()> _
    Public Property TiempoExpiracion() As String
        Get
            Return _tiempoExpiracion
        End Get
        Set(ByVal value As String)
            _tiempoExpiracion = value
        End Set
    End Property
    Private _conceptoPago As String
    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property
    Private _servicioUsaUsuariosAnonimos As String
    <DataMember()> _
    Public Property ServicioUsaUsuariosAnonimos() As String
        Get
            Return _servicioUsaUsuariosAnonimos
        End Get
        Set(ByVal value As String)
            _servicioUsaUsuariosAnonimos = value
        End Set
    End Property
    Private _token As String
    <DataMember()> _
    Public Property Token() As String
        Get
            Return _token
        End Get
        Set(ByVal value As String)
            _token = value
        End Set
    End Property
    Private _parametrosURL As List(Of BEParametroURL) = Nothing
    <DataMember()> _
    Public Property ParametrosURL() As List(Of BEParametroURL)
        Get
            Return _parametrosURL
        End Get
        Set(ByVal value As List(Of BEParametroURL))
            _parametrosURL = value
        End Set
    End Property
    Private _parametrosEmail As List(Of BEParametroEmail) = Nothing
    <DataMember()> _
    Public Property ParametrosEmail() As List(Of BEParametroEmail)
        Get
            Return _parametrosEmail
        End Get
        Set(ByVal value As List(Of BEParametroEmail))
            _parametrosEmail = value
        End Set
    End Property
    Private _detallesSolicitudPago As List(Of BEDetalleSolicitudPago) = Nothing
    <DataMember()> _
    Public Property DetallesSolicitudPago() As List(Of BEDetalleSolicitudPago)
        Get
            Return _detallesSolicitudPago
        End Get
        Set(ByVal value As List(Of BEDetalleSolicitudPago))
            _detallesSolicitudPago = value
        End Set
    End Property

    Private _beOrdenPago As BEOrdenPago = Nothing
    <DataMember()> _
    Public Property BEOrdenPago() As BEOrdenPago
        Get
            Return _beOrdenPago
        End Get
        Set(ByVal value As BEOrdenPago)
            _beOrdenPago = value
        End Set
    End Property
    Private _AbreviaturaMoneda As String
    <DataMember()> _
    Public Property AbreviaturaMoneda() As String
        Get
            Return _AbreviaturaMoneda
        End Get
        Set(ByVal value As String)
            _AbreviaturaMoneda = value
        End Set
    End Property
    Private _NombreMoneda As String
    <DataMember()> _
    Public Property NombreMoneda() As String
        Get
            Return _NombreMoneda
        End Get
        Set(ByVal value As String)
            _NombreMoneda = value
        End Set
    End Property


    'datos adicionales-------------------------------------------
    Private _Telefono As String
    <DataMember()> _
    Public Property Telefono() As String
        Get
            Return _Telefono
        End Get
        Set(ByVal value As String)
            _Telefono = value
        End Set
    End Property



    Private _Anexo As String
    <DataMember()> _
    Public Property Anexo() As String
        Get
            Return _Anexo
        End Get
        Set(ByVal value As String)
            _Anexo = value
        End Set
    End Property




    Private _Celular As String
    <DataMember()> _
    Public Property Celular() As String
        Get
            Return _Celular
        End Get
        Set(ByVal value As String)
            _Celular = value
        End Set
    End Property



    Private _DatoAdicional As String
    <DataMember()> _
    Public Property DatoAdicional() As String
        Get
            Return _DatoAdicional
        End Get
        Set(ByVal value As String)
            _DatoAdicional = value
        End Set
    End Property




    Private _CanalPago As String
    <DataMember()> _
    Public Property CanalPago() As String
        Get
            Return _CanalPago
        End Get
        Set(ByVal value As String)
            _CanalPago = value
        End Set
    End Property


    Private _CodigoFormulario As String
    <DataMember()> _
    Public Property CodigoFormulario() As String
        Get
            Return _CodigoFormulario
        End Get
        Set(ByVal value As String)
            _CodigoFormulario = value
        End Set
    End Property

    Private _TipoComprobante As String
    <DataMember()> _
    Public Property TipoComprobante() As String
        Get
            Return _TipoComprobante
        End Get
        Set(ByVal value As String)
            _TipoComprobante = value
        End Set
    End Property

    Private _Ruc As String
    <DataMember()> _
    Public Property Ruc() As String
        Get
            Return _Ruc
        End Get
        Set(ByVal value As String)
            _Ruc = value
        End Set
    End Property

    Private _RazonSocial As String
    <DataMember()> _
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Private _Direccion As String
    <DataMember()> _
    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property



   


End Class


