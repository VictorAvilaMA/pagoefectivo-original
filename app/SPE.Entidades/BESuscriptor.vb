﻿Imports System.Runtime.Serialization
<Serializable()> _
<DataContract()> _
Public Class BESuscriptor
    Public Sub New()

    End Sub

    Public Sub New(ByVal idSuscriptor As Int32, ByVal correo As String, ByVal fechaRegistro As DateTime)
        _IdSuscriptor = idSuscriptor
        _Correo = correo
        _FechaRegistro = fechaRegistro
    End Sub

    Private _IdSuscriptor As Int32
    <DataMember()> _
    Public Property IdSuscriptor() As Int32
        Get
            Return _IdSuscriptor
        End Get
        Set(ByVal value As Int32)
            _IdSuscriptor = value
        End Set
    End Property

    Private _Correo As String
    <DataMember()> _
    Public Property Correo() As String
        Get
            Return _Correo
        End Get
        Set(ByVal value As String)
            _Correo = value
        End Set
    End Property

    Private _FechaRegistro As String
    <DataMember()> _
    Public Property FechaRegistro() As String
        Get
            Return _FechaRegistro
        End Get
        Set(ByVal value As String)
            _FechaRegistro = value
        End Set
    End Property
End Class
