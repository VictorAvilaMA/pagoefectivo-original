Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BETareaConciliacion
    Inherits BEAuditoria
    Private _idTareaConciliacion As Integer
    Private _IdTipoConciliacion As Integer
    Private _fechaEjecucion As Nullable(Of DateTime)
    Private _idEstado As Integer
    Private _codigoEntidad As String
    Private _nombreProceso As String
    Private _rutaArchivoLectura As String
    Private _rutaArchivoBackup As String
    Private _idUsuarioCreacion As Integer
    Private _fechaCreacion As DateTime
    Private _horasAnticipacion As Integer
    Private _estructuraNombreArchivo As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal reader As IDataReader)
        IdTareaConciliacion = Convert.ToInt32(reader("IdTareaConciliacion"))
        IdTipoConciliacion = Convert.ToInt32(reader("IdTipoConciliacion"))
        FechaEjecucion = ObjectToDateTimeNull(reader("FechaEjecucion"))
        IdEstado = Convert.ToInt32(reader("IdEstado"))
        CodigoEntidad = reader("CodigoEntidad")
        NombreProceso = reader("NombreProceso")
        RutaArchivoLectura = reader("RutaArchivoLectura")
        RutaArchivoBackup = reader("RutaArchivoBackup")
        IdUsuarioCreacion = Convert.ToInt32(reader("IdUsuarioCreacion"))
        FechaCreacion = Convert.ToDateTime(reader("FechaCreacion"))
        HorasAnticipacion = Convert.ToInt32(reader("HorasAnticipacion"))
        EstructuraNombreArchivo = reader("EstructuraNombreArchivo")
    End Sub
	<DataMember()> _
    Public Property IdTareaConciliacion() As Integer
        Get
            Return _idTareaConciliacion
        End Get
        Set(ByVal value As Integer)
            _idTareaConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdTipoConciliacion() As Integer
        Get
            Return _IdTipoConciliacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaEjecucion() As Nullable(Of DateTime)
        Get
            Return _fechaEjecucion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _fechaEjecucion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property CodigoEntidad() As String
        Get
            Return _codigoEntidad
        End Get
        Set(ByVal value As String)
            _codigoEntidad = value
        End Set
    End Property
	<DataMember()> _
    Public Property NombreProceso() As String
        Get
            Return _nombreProceso
        End Get
        Set(ByVal value As String)
            _nombreProceso = value
        End Set
    End Property
	<DataMember()> _
    Public Property RutaArchivoLectura() As String
        Get
            Return _rutaArchivoLectura
        End Get
        Set(ByVal value As String)
            _rutaArchivoLectura = value
        End Set
    End Property
	<DataMember()> _
    Public Property RutaArchivoBackup() As String
        Get
            Return _rutaArchivoBackup
        End Get
        Set(ByVal value As String)
            _rutaArchivoBackup = value
        End Set
    End Property
	<DataMember()> _
    Public Property HorasAnticipacion() As Integer
        Get
            Return _horasAnticipacion
        End Get
        Set(ByVal value As Integer)
            _horasAnticipacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property EstructuraNombreArchivo() As String
        Get
            Return _estructuraNombreArchivo
        End Get
        Set(ByVal value As String)
            _estructuraNombreArchivo = value
        End Set
    End Property
End Class
