Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BETarjeta

    Private _idTarjeta As Integer
    Private _ctrjcrd As String
    Private _descripcion As String
    Private _codEnteAdmin As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdTarjeta = Convert.ToInt32(reader("IdTarjeta"))
        Me.Ctrjcrd = reader("ctrjcrd").ToString
        Me.Descripcion = reader("Descripcion").ToString
        Me.CodEnteAdmin = reader("cod_enteadmin").ToString
    End Sub

	<DataMember()> _
    Public Property IdTarjeta() As Integer
        Get
            Return _idTarjeta
        End Get
        Set(ByVal value As Integer)
            _idTarjeta = value
        End Set
    End Property

	<DataMember()> _
    Public Property Ctrjcrd() As String
        Get
            Return _ctrjcrd
        End Get
        Set(ByVal value As String)
            _ctrjcrd = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

	<DataMember()> _
    Public Property CodEnteAdmin() As String
        Get
            Return _codEnteAdmin
        End Get
        Set(ByVal value As String)
            _codEnteAdmin = value
        End Set
    End Property
End Class

