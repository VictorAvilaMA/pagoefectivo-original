Imports System.Runtime.Serialization

Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data

<Serializable()> <DataContract()> Public Class BETipoAgenciaRecaudadora

    Private _idTipoAgencia As Integer
    Private _descripcion As String

    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.IdTipoAgencia = Convert.ToInt32(reader("IdTipoAgencia").ToString())
        Me.Descripcion = reader("Descripcion").ToString()
    End Sub
	<DataMember()> _
    Public Property IdTipoAgencia() As Integer
        Get
            Return _idTipoAgencia
        End Get
        Set(ByVal value As Integer)
            _idTipoAgencia = value
        End Set
    End Property

	<DataMember()> _
    Public Property Descripcion() As Integer
        Get
            Return _descripcion
        End Get
        Set(ByVal value As Integer)
            _descripcion = value
        End Set
    End Property
End Class
