Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
<Serializable()> <DataContract()> Public Class BETipoOrigenCancelacion
    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        Me.IdTipoOrigenCancelacion = Convert.ToInt32(reader("IdTipoOrigenCancelacion").ToString())
        Me.Descripcion = reader("Descripcion").ToString()
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal parametro As String)

        If parametro = "Carga" Then
            Me.IdTipoOrigenCancelacion = Convert.ToInt32(reader("IdTipoOrigenCancelacion").ToString())
        End If
    End Sub
    Private _idTipoOrigenCancelacion As Integer
    Private _descripcion As String
    Private _idEstado As Integer

	<DataMember()> _
    Public Property IdTipoOrigenCancelacion() As Integer
        Get
            Return _idTipoOrigenCancelacion
        End Get
        Set(ByVal value As Integer)
            _idTipoOrigenCancelacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

End Class
