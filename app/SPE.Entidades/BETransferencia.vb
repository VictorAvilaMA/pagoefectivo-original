﻿
Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util



<Serializable()> <DataContract()> Public Class BETransferencia
    Inherits BEAuditoria
    Private _IdTransfenciaEmpresas As Integer
    Private _IdServicio As Integer
    Private _IdEmpresaContratante As Integer
    Private _IdMoneda As Integer
    Private _FechaInicio As DateTime
    Private _FechaFin As DateTime
    Private _FechaLiquidacion As DateTime
    Private _IdBanco As Integer
    Private _TotalOperaciones As Integer
    Private _TotalPago As Decimal
    Private _TotalComision As Decimal
    Private _FechaDeposito As DateTime
    Private _NumeroOperacion As String
    Private _NumeroCuenta As String
    Private _Observacion As String
    Private _ArchivoAdjunto As String
    Private _Estado As String
    Private _DescripcionEmpresa As String
    Private _DescripcionMoneda As String
    Private _DescripcionBanco As String
    Private _IdOrdenPago As Integer
    Private _NumeroOrdenPago As String
    Private _DescripcionServicio As String
    Private _DescipcionEstadoT As String
    Private _Resultado As Decimal
    Private _idEstado As Integer
    Private _FechaCreacionOP As DateTime
    Private _FechaCancelacion As DateTime
    Private _FechaEmision As DateTime
    Private _IdCliente As Integer
    Private _IdOrderComercio As String
    Private _FechaAnulacion As DateTime
    Private _SimboloMoneda As String
    Private _DescripcionAgencia As String
    Private _FlagConciliado As Boolean
    Private _DataAdicional As String
    Private _ConceptoPago As String
    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode

            Case "valFecCre"
                Me.FechaInicio = DataUtil.ObjectToDateTime(reader("FechaInicio"))
            Case "ObtxId"
                Me.idTransfenciaEmpresas = DataUtil.ObjectToInt(reader("IdTransfenciaEmpresas"))
                Me.idEmpresaContratante = DataUtil.ObjectToInt(reader("IdEmpresa"))
                Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("Empresa"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.FechaInicio = DataUtil.ObjectToDateTime(reader("FechaInicio"))
                Me.FechaFin = DataUtil.ObjectToDateTime(reader("FechaFin"))
                Me.FechaLiquidacion = DataUtil.ObjectToDateTime(reader("FechaLiquidacion"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("Moneda"))
                'Me.IdBanco = DataUtil.ObjectToInt(reader("IdBanco"))
                Me.DescripcionBanco = DataUtil.ObjectToString(reader("Banco"))
                Me.TotalOperaciones = DataUtil.ObjectToInt(reader("TotalOperaciones"))
                Me.TotalPago = DataUtil.ObjectToDecimal(reader("TotalPago"))
                Me.TotalComision = DataUtil.ObjectToDecimal(reader("TotalComision"))
                Me.FechaDeposito = DataUtil.ObjectToDateTime(reader("FechaDeposito"))
                Me.NumeroCuenta = DataUtil.ObjectToString(reader("NumeroCuenta"))
                Me.Observacion = DataUtil.ObjectToString(reader("Observacion"))
                Me.ArchivoAdjunto = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.Estado = DataUtil.ObjectToString(reader("IdEstado"))
                Me.NumeroOperacion = DataUtil.ObjectToString(reader("NumeroOperacion"))
                IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
                FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaRegistro"))
                IdUsuarioActualizacion = DataUtil.ObjectToInt(reader("IdUsuarioActualizacion"))
                FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
                DescipcionEstadoT = DataUtil.ObjectToString(reader("DescripcionEstado"))

            Case "ConsTrans"
                Me.idTransfenciaEmpresas = DataUtil.ObjectToInt(reader("IdTransfenciaEmpresas"))
                Me.idEmpresaContratante = DataUtil.ObjectToInt(reader("IdEmpresa"))
                Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("Empresa"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.FechaInicio = DataUtil.ObjectToDateTime(reader("FechaInicio"))
                Me.FechaFin = DataUtil.ObjectToDateTime(reader("FechaFin"))
                Me.FechaLiquidacion = DataUtil.ObjectToDateTime(reader("FechaLiquidacion"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("Moneda"))
                Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
                'Me.IdBanco = DataUtil.ObjectToInt(reader("IdBanco"))
                Me.DescripcionBanco = DataUtil.ObjectToString(reader("Banco"))
                Me.TotalOperaciones = DataUtil.ObjectToInt(reader("TotalOperaciones"))
                Me.TotalPago = DataUtil.ObjectToDecimal(reader("TotalPago"))
                Me.TotalComision = DataUtil.ObjectToDecimal(reader("TotalComision"))
                Me.FechaDeposito = DataUtil.ObjectToDateTime(reader("FechaDeposito"))
                Me.NumeroCuenta = DataUtil.ObjectToString(reader("NumeroCuenta"))
                Me.Observacion = DataUtil.ObjectToString(reader("Observacion"))
                Me.ArchivoAdjunto = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.Estado = DataUtil.ObjectToString(reader("IdEstado"))
                Me.NumeroOperacion = DataUtil.ObjectToString(reader("NumeroOperacion"))
                IdUsuarioCreacion = DataUtil.ObjectToInt(reader("IdUsuarioCreacion"))
                FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaRegistro"))
                IdUsuarioActualizacion = DataUtil.ObjectToInt(reader("IdUsuarioActualizacion"))
                FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
                Me.Resultado = DataUtil.ObjectToDecimal(reader("Resultado"))
                Me.TotalPageNumbers = DataUtil.ObjectToString(reader("TOTAL_RESULT"))
                DescipcionEstadoT = DataUtil.ObjectToString(reader("DescripcionEstado"))

            Case "consultaDetalleTransaccion"

                Me.DescripcionEmpresa = DataUtil.ObjectToString(reader("Empresa"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("Moneda"))
                Me.FechaCreacionOP = DataUtil.ObjectToDateTime(reader("TFechaRegistro"))
                Me.FechaLiquidacion = DataUtil.ObjectToDateTime(reader("TFechaLiquidacion"))
                Me.DescipcionEstadoT = DataUtil.ObjectToString(reader("EstadoT"))
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("Nombre"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.TotalComision = DataUtil.ObjectToDecimal(reader("Comision"))
                Me.TotalPago = DataUtil.ObjectToDecimal(reader("Total"))
                Me.Estado = DataUtil.ObjectToString(reader("Descripcion"))
                Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.FechaAnulacion = DataUtil.ObjectToDateTime(reader("FechaAnulada"))
                Me.IdOrderComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                Me.DescripcionAgencia = DataUtil.ObjectToString(reader("NombreAgencia"))
                Me.DescripcionBanco = DataUtil.ObjectToString(reader("DescripcionBanco"))
                Me.ArchivoAdjunto = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

            Case "consultaDetalleTransaccionP"
                Me.DescripcionServicio = DataUtil.ObjectToString(reader("Nombre"))
                Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
                Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
                Me.DescripcionMoneda = DataUtil.ObjectToString(reader("Moneda"))
                Me.TotalComision = DataUtil.ObjectToDecimal(reader("Comision"))
                Me.TotalPago = DataUtil.ObjectToDecimal(reader("Total"))
                Me.Estado = DataUtil.ObjectToString(reader("Estado"))
                Me.FechaCreacion = DataUtil.ObjectToDateTime(reader("FechaCreacion"))
                Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
                Me.FechaCancelacion = DataUtil.ObjectToDateTime(reader("FechaCancelacion"))
                Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
                Me.FechaAnulacion = DataUtil.ObjectToDateTime(reader("FechaAnulada"))
                Me.IdOrderComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
                Me.IdMoneda = DataUtil.ObjectToInt(reader("IdMoneda"))
                Me.IdEstado = DataUtil.ObjectToInt(reader("IdEstado"))
                Me.DescripcionBanco = DataUtil.ObjectToString(reader("DescripcionBanco"))
                Me.ArchivoAdjunto = DataUtil.ObjectToString(reader("NombreArchivo"))
                Me.DataAdicional = DataUtil.ObjectToString(reader("DataAdicional"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

        End Select
    End Sub

    <DataMember()> _
    Public Property idTransfenciaEmpresas() As Integer
        Get
            Return _IdTransfenciaEmpresas
        End Get
        Set(ByVal value As Integer)
            _IdTransfenciaEmpresas = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionBanco() As String
        Get
            Return _DescripcionBanco
        End Get
        Set(ByVal value As String)
            _DescripcionBanco = value
        End Set
    End Property
    <DataMember()> _
    Public Property idServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value

        End Set
    End Property

    <DataMember()> _
    Public Property idEmpresaContratante() As Integer
        Get
            Return _IdEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _IdEmpresaContratante = value

        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _FechaInicio
        End Get
        Set(ByVal value As DateTime)
            _FechaInicio = value

        End Set
    End Property

    <DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _FechaFin
        End Get
        Set(ByVal value As DateTime)
            _FechaFin = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaLiquidacion() As DateTime
        Get
            Return _FechaLiquidacion
        End Get
        Set(ByVal value As DateTime)
            _FechaLiquidacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(ByVal value As Integer)
            _IdBanco = value
        End Set
    End Property
    <DataMember()> _
    Public Property TotalOperaciones() As Integer
        Get
            Return _TotalOperaciones
        End Get
        Set(ByVal value As Integer)
            _TotalOperaciones = value
        End Set
    End Property
    <DataMember()> _
    Public Property TotalPago() As Decimal
        Get
            Return _TotalPago
        End Get
        Set(ByVal value As Decimal)
            _TotalPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property TotalComision() As Decimal
        Get
            Return _TotalComision
        End Get
        Set(ByVal value As Decimal)
            _TotalComision = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _NumeroOperacion
        End Get
        Set(ByVal value As String)
            _NumeroOperacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroCuenta() As String
        Get
            Return _NumeroCuenta
        End Get
        Set(ByVal value As String)
            _NumeroCuenta = value
        End Set
    End Property
    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property ArchivoAdjunto() As String
        Get
            Return _ArchivoAdjunto
        End Get
        Set(ByVal value As String)
            _ArchivoAdjunto = value
        End Set
    End Property
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaDeposito() As DateTime
        Get
            Return _FechaDeposito
        End Get
        Set(ByVal value As DateTime)
            _FechaDeposito = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value

        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionEmpresa() As String
        Get
            Return _DescripcionEmpresa
        End Get
        Set(ByVal value As String)
            _DescripcionEmpresa = value

        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionMoneda() As String
        Get
            Return _DescripcionMoneda
        End Get
        Set(ByVal value As String)
            _DescripcionMoneda = value

        End Set
    End Property
    <DataMember()> _
    Public Property Resultado() As Decimal
        Get
            Return _Resultado
        End Get
        Set(ByVal value As Decimal)
            _Resultado = value

        End Set
    End Property
    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value

        End Set
    End Property

    <DataMember()> _
    Public Property FechaCancelacion() As DateTime
        Get
            Return _FechaCancelacion
        End Get
        Set(ByVal value As DateTime)
            _FechaCancelacion = value

        End Set
    End Property
    <DataMember()> _
    Public Property FechaCreacionOP() As DateTime
        Get
            Return _FechaCreacionOP
        End Get
        Set(ByVal value As DateTime)
            _FechaCreacionOP = value

        End Set
    End Property
    <DataMember()> _
    Public Property DescipcionEstadoT() As String
        Get
            Return _DescipcionEstadoT
        End Get
        Set(ByVal value As String)
            _DescipcionEstadoT = value

        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionServicio() As String
        Get
            Return _DescripcionServicio
        End Get
        Set(ByVal value As String)
            _DescripcionServicio = value

        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _NumeroOrdenPago
        End Get
        Set(ByVal value As String)
            _NumeroOrdenPago = value

        End Set
    End Property
    <DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As Integer)
            _IdOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As DateTime)
            _FechaEmision = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrderComercio() As String
        Get
            Return _IdOrderComercio
        End Get
        Set(ByVal value As String)
            _IdOrderComercio = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaAnulacion() As DateTime
        Get
            Return _FechaAnulacion
        End Get
        Set(ByVal value As DateTime)
            _FechaAnulacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property SimboloMoneda() As String
        Get
            Return _SimboloMoneda
        End Get
        Set(ByVal value As String)
            _SimboloMoneda = value
        End Set
    End Property
    <DataMember()> _
    Public Property DescripcionAgencia() As String
        Get
            Return _DescripcionAgencia
        End Get
        Set(ByVal value As String)
            _DescripcionAgencia = value
        End Set
    End Property
    <DataMember()> _
    Public Property FlagConciliado() As Boolean
        Get
            Return _FlagConciliado
        End Get
        Set(ByVal value As Boolean)
            _FlagConciliado = value
        End Set
    End Property
    <DataMember()> _
    Public Property DataAdicional() As String
        Get
            Return _DataAdicional
        End Get
        Set(ByVal value As String)
            _DataAdicional = value
        End Set
    End Property

    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property
End Class

