﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util


<Serializable()> <DataContract()> Public Class BETransferenciaSaga

    Private _Fechainicio As Date
    Private _Fechafin As Date
    Private _FechaRegistro As Date
    Private _FechaDeposito As Date
    Private _CodigoCliente As String
    Private _NombreCliente As String
    Private _Liq_Numc As String
    Private _Liq_Fproc As Date
    Private _Liq_Fcom As Date
    Private _Liq_Micr As String
    Private _Liq_Numta As String
    Private _Liq_Marca As String
    Private _Liq_Monto As Decimal
    Private _Liq_Moneda As String
    Private _Liq_Txs As String
    Private _Liq_Rete As String
    Private _Liq_Cprin As String
    Private _Liq_Fpago As Date
    Private _Liq_Nro_Unico As String
    Private _Liq_codaut As String
    Private _Liq_Monto_Com_Neta As Decimal
    Private _Liq_Monto_Com_Afect As String
    Private _Liq_Monto_igv As Decimal
    Private _Liq_Monto_Liquidar As Decimal
    Private _Liq_Num_Cuenta_banco As String
    Private _Liq_Numero_orden As String
    Private _Sal_Contador As Integer
    Private _Sal_Monto As Decimal


    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal mode As String)
        Select Case mode
            Case "ConsSaga"
                Me.Fechainicio = DataUtil.ObjectToDateTime(reader("Fechainicio"))
                Me.Fechafin = DataUtil.ObjectToDateTime(reader("Fechafin"))
                Me.FechaRegistro = DataUtil.ObjectToDateTime(reader("FechaRegistro"))
                Me.FechaDeposito = DataUtil.ObjectToDateTime(reader("FechaDeposito"))
                Me.CodigoCliente = DataUtil.ObjectToString(reader("CodigoCliente"))
                Me.NombreCliente = DataUtil.ObjectToString(reader("NombreCliente"))
                Me.Liq_Numc = DataUtil.ObjectToString(reader("Liq_Numc"))
                Me.Liq_Fproc = DataUtil.ObjectToDateTime(reader("Liq_Fproc"))
                Me.Liq_Fcom = DataUtil.ObjectToDateTime(reader("Liq_Fcom"))
                Me.Liq_Micr = DataUtil.ObjectToString(reader("Liq_Micr"))
                Me.Liq_Numta = DataUtil.ObjectToString(reader("Liq_Numta"))
                Me.Liq_Marca = DataUtil.ObjectToString(reader("Liq_Marca"))
                Me.Liq_Monto = DataUtil.ObjectToDecimal(reader("Liq_Monto"))
                Me.Liq_Moneda = DataUtil.ObjectToString(reader("Liq_Moneda"))
                Me.Liq_Txs = DataUtil.ObjectToString(reader("Liq_Txs"))
                Me.Liq_Rete = DataUtil.ObjectToString(reader("Liq_Rete"))
                Me.Liq_Cprin = DataUtil.ObjectToString(reader("Liq_Cprin"))
                Me.Liq_Fpago = DataUtil.ObjectToDateTime(reader("Liq_Fpago"))
                Me.Liq_Nro_Unico = DataUtil.ObjectToString(reader("Liq_Nro_Unico"))
                Me.Liq_codaut = DataUtil.ObjectToString(reader("Liq_codaut"))
                Me.Liq_Monto_Com_Neta = DataUtil.ObjectToDecimal(reader("Liq_Monto_Com_Neta"))
                Me.Liq_Monto_Com_Afect = DataUtil.ObjectToString(reader("Liq_Monto_Com_Afect"))
                Me.Liq_Monto_igv = DataUtil.ObjectToDecimal(reader("Liq_Monto_igv"))
                Me.Liq_Monto_Liquidar = DataUtil.ObjectToDecimal(reader("Liq_Monto_Liquidar"))
                Me.Liq_Num_Cuenta_banco = DataUtil.ObjectToString(reader("Liq_Num_Cuenta_banco"))
                Me.Liq_Numero_orden = DataUtil.ObjectToString(reader("Liq_Numero_orden"))
                Me.Sal_Contador = DataUtil.ObjectToString(reader("Sal_Contador"))
                Me.Sal_Monto = DataUtil.ObjectToString(reader("Sal_Monto"))



        End Select
    End Sub




    <DataMember()> _
    Public Property Fechainicio() As Date
        Get
            Return _Fechainicio
        End Get
        Set(ByVal value As Date)
            _Fechainicio = value
        End Set
    End Property



    <DataMember()> _
    Public Property Fechafin() As Date
        Get
            Return _Fechafin
        End Get
        Set(ByVal value As Date)
            _Fechafin = value
        End Set
    End Property



    <DataMember()> _
    Public Property FechaRegistro() As Date
        Get
            Return _FechaRegistro
        End Get
        Set(ByVal value As Date)
            _FechaRegistro = value
        End Set
    End Property



    <DataMember()> _
    Public Property FechaDeposito() As Date
        Get
            Return _FechaDeposito
        End Get
        Set(ByVal value As Date)
            _FechaDeposito = value
        End Set
    End Property



    <DataMember()> _
    Public Property CodigoCliente() As String
        Get
            Return _CodigoCliente
        End Get
        Set(ByVal value As String)
            _CodigoCliente = value
        End Set
    End Property


    <DataMember()> _
    Public Property NombreCliente() As String
        Get
            Return _NombreCliente
        End Get
        Set(ByVal value As String)
            _NombreCliente = value
        End Set
    End Property


    <DataMember()> _
    Public Property Liq_Numc() As String
        Get
            Return _Liq_Numc
        End Get
        Set(ByVal value As String)
            _Liq_Numc = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Fproc() As Date
        Get
            Return _Liq_Fproc
        End Get
        Set(ByVal value As Date)
            _Liq_Fproc = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Fcom() As Date
        Get
            Return _Liq_Fcom
        End Get
        Set(ByVal value As Date)
            _Liq_Fcom = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Micr() As String
        Get
            Return _Liq_Micr
        End Get
        Set(ByVal value As String)
            _Liq_Micr = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Numta() As String
        Get
            Return _Liq_Numta
        End Get
        Set(ByVal value As String)
            _Liq_Numta = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Marca() As String
        Get
            Return _Liq_Marca
        End Get
        Set(ByVal value As String)
            _Liq_Marca = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Monto() As Decimal
        Get
            Return _Liq_Monto
        End Get
        Set(ByVal value As Decimal)
            _Liq_Monto = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Moneda() As String
        Get
            Return _Liq_Moneda
        End Get
        Set(ByVal value As String)
            _Liq_Moneda = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Txs() As String
        Get
            Return _Liq_Txs
        End Get
        Set(ByVal value As String)
            _Liq_Txs = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Rete() As String
        Get
            Return _Liq_Rete
        End Get
        Set(ByVal value As String)
            _Liq_Rete = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Cprin() As String
        Get
            Return _Liq_Cprin
        End Get
        Set(ByVal value As String)
            _Liq_Cprin = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Fpago() As Date
        Get
            Return _Liq_Fpago
        End Get
        Set(ByVal value As Date)
            _Liq_Fpago = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Nro_Unico() As String
        Get
            Return _Liq_Nro_Unico
        End Get
        Set(ByVal value As String)
            _Liq_Nro_Unico = value
        End Set
    End Property


    <DataMember()> _
    Public Property Liq_codaut() As String
        Get
            Return _Liq_codaut
        End Get
        Set(ByVal value As String)
            _Liq_codaut = value
        End Set
    End Property




    <DataMember()> _
    Public Property Liq_Monto_Com_Neta() As Decimal
        Get
            Return _Liq_Monto_Com_Neta
        End Get
        Set(ByVal value As Decimal)
            _Liq_Monto_Com_Neta = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Monto_Com_Afect() As String
        Get
            Return _Liq_Monto_Com_Afect
        End Get
        Set(ByVal value As String)
            _Liq_Monto_Com_Afect = value
        End Set
    End Property


    <DataMember()> _
    Public Property Liq_Monto_igv() As Decimal
        Get
            Return _Liq_Monto_igv
        End Get
        Set(ByVal value As Decimal)
            _Liq_Monto_igv = value
        End Set
    End Property



    <DataMember()> _
    Public Property Liq_Monto_Liquidar() As Decimal
        Get
            Return _Liq_Monto_Liquidar
        End Get
        Set(ByVal value As Decimal)
            _Liq_Monto_Liquidar = value
        End Set
    End Property




    <DataMember()> _
    Public Property Liq_Num_Cuenta_banco() As String
        Get
            Return _Liq_Num_Cuenta_banco
        End Get
        Set(ByVal value As String)
            _Liq_Num_Cuenta_banco = value
        End Set
    End Property


    <DataMember()> _
    Public Property Liq_Numero_orden() As String
        Get
            Return _Liq_Numero_orden
        End Get
        Set(ByVal value As String)
            _Liq_Numero_orden = value
        End Set
    End Property

    <DataMember()> _
    Public Property Sal_Contador() As Integer
        Get
            Return _Sal_Contador
        End Get
        Set(ByVal value As Integer)
            _Sal_Contador = value
        End Set
    End Property


    <DataMember()> _
    Public Property Sal_Monto() As Decimal
        Get
            Return _Sal_Monto
        End Get
        Set(ByVal value As Decimal)
            _Sal_Monto = value
        End Set
    End Property



End Class