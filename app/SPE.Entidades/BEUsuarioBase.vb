Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEUsuarioBase
    Inherits _3Dev.FW.Entidades.Seguridad.BEUsuario

    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        MyBase.New(reader)

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal uno As Integer)
        MyBase.New(reader, 1, 1)

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal consulta As String)
        MyBase.New(reader, consulta)
        If (consulta = "ValidarUsuarioEmail") Then
            CodigoRegistro = ObjectToString(reader("CodigoRegistro"))
        End If
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal val1 As Integer, ByVal val2 As String)
        MyBase.New(reader, val1, val2)
    End Sub

    Private _idOrigenRegistro As Integer
    <DataMember()> _
    Public Property IdOrigenRegistro() As Integer
        Get
            Return _idOrigenRegistro
        End Get
        Set(ByVal value As Integer)
            _idOrigenRegistro = value
        End Set
    End Property

    Private _codigoRegistro As String

    <DataMember()> _
    Public Property CodigoRegistro() As String
        Get
            Return _codigoRegistro
        End Get
        Set(ByVal value As String)
            _codigoRegistro = value
        End Set
    End Property

    Private _TokenGUIDPassword As String

    <DataMember()> _
    Public Property TokenGUIDPassword() As String
        Get
            Return _TokenGUIDPassword
        End Get
        Set(ByVal value As String)
            _TokenGUIDPassword = value
        End Set
    End Property

    Private _FechaCreacionTokenGUID As Date

    <DataMember()> _
    Public Property FechaCreacionTokenGUID() As Date
        Get
            Return _FechaCreacionTokenGUID
        End Get
        Set(ByVal value As Date)
            _FechaCreacionTokenGUID = value
        End Set
    End Property

End Class
