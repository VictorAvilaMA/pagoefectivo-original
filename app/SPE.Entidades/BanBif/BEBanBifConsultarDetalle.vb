Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBanBifConsultarDetalle

    Public Sub New()

    End Sub

    <DataMember()> _
    Public Property Recibo() As String

    'mfx:SvcId 
    <DataMember()> _
    Public Property SvcId() As String

    <DataMember()> _
    Public Property Descripcion() As String

    <DataMember()> _
    Public Property FecVct() As String

    <DataMember()> _
    Public Property FecEmision() As String

    <DataMember()> _
    Public Property MonCod() As String

    <DataMember()> _
    Public Property Total() As Decimal

    <DataMember()> _
    Public Property Ctd() As String

    <DataMember()> _
    Public Property DocRef() As String

    <DataMember()> _
    Public Property subConceptos() As List(Of BEBanBifConsultarSubConcepto)

End Class