Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBanBifConsultarResponse

    Public Sub New()

    End Sub

    <DataMember()> _
    Public Property idLogBifConsReq() As Integer

    <DataMember()> _
    Public Property StatusCod() As String

    <DataMember()> _
    Public Property Severidad() As String

    <DataMember()> _
    Public Property ServStatusCod() As String

    <DataMember()> _
    Public Property StatusDes() As String

    <DataMember()> _
    Public Property RqUID() As String

    'mfx:Login 
    <DataMember()> _
    Public Property Login() As String

    'mfx:SegRol 
    <DataMember()> _
    Public Property SegRol() As String

    'mfx:ClientFec 
    <DataMember()> _
    Public Property ServFec() As String

    'Spcod
    <DataMember()> _
    Public Property Spcod() As String

    'mfx:PagId 
    <DataMember()> _
    Public Property PagId() As String

    <DataMember()> _
    Public Property NomCompleto() As String

    'Clasificación
    <DataMember()> _
    Public Property Clasificacion() As String

    <DataMember()> _
    Public Property Ctd() As String

    'mfx:OpnNro 
    <DataMember()> _
    Public Property OpnNro() As String

    <DataMember()> _
    Public Property detalle() As List(Of BEBanBifConsultarDetalle)

End Class
