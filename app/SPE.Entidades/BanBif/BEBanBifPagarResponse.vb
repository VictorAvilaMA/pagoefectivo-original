Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEBanBifPagarResponse

    Public Sub New()

    End Sub

    <DataMember()> _
    Public Property idLogBifPagarReq() As Integer

    <DataMember()> _
    Public Property StatusCod() As String

    <DataMember()> _
    Public Property Severidad() As String

    <DataMember()> _
    Public Property StatusDes() As String

    <DataMember()> _
    Public Property ServStatusCod() As String

    <DataMember()> _
    Public Property ExtornoTp() As String

    'mfx:RqUID d
    <DataMember()> _
    Public Property RqUID() As String

    'mfx:Login 
    <DataMember()> _
    Public Property Login() As String

    'mfx:SegRol 
    <DataMember()> _
    Public Property SegRol() As String

    <DataMember()> _
    Public Property ServFec() As String

    'mfx:OpnNro 
    <DataMember()> _
    Public Property OpnNro() As String

    <DataMember()> _
    Public Property Ctd() As String

    'mfx:SvcId 
    <DataMember()> _
    Public Property SvcId() As String

    'Spcod
    <DataMember()> _
    Public Property Spcod() As String

    'mfx:PagId 
    <DataMember()> _
    Public Property PagId() As String


    <DataMember()> _
    Public Property Recibo() As String

    'Clasificación
    <DataMember()> _
    Public Property Clasificacion() As String

    <DataMember()> _
    Public Property Moneda() As String

    <DataMember()> _
    Public Property Mto() As String


End Class