﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEBanBifPagoExtornoRequest

    Public Sub New()

    End Sub

    'mfx:RqUID d
    <DataMember()> _
    Public Property RqUID() As String

    'mfx:Login 
    <DataMember()> _
    Public Property Login() As String

    'mfx:SegRol 
    <DataMember()> _
    Public Property SegRol() As String

    <DataMember()> _
    Public Property ExtornoTp() As String

    'mfx:ClientFec 
    <DataMember()> _
    Public Property ClientFec() As String

    'mfx:OpnNro 
    <DataMember()> _
    Public Property OpnNro() As String

    'mfx:OrgCod 
    <DataMember()> _
    Public Property OrgCod() As String

    'mfx:ClientCod 
    <DataMember()> _
    Public Property ClientCod() As String

    <DataMember()> _
    Public Property Ctd() As Int16

    'mfx:SvcId 
    <DataMember()> _
    Public Property SvcId() As String

    'Spcod
    <DataMember()> _
    Public Property Spcod() As String

    'mfx:PagId 
    <DataMember()> _
    Public Property PagId() As String

    <DataMember()> _
    Public Property Recibo() As String

    'Clasificación
    <DataMember()> _
    Public Property Clasificacion() As String

    <DataMember()> _
    Public Property MedioAbonoCod() As String

    <DataMember()> _
    Public Property Moneda() As String

    <DataMember()> _
    Public Property Mto() As Decimal

    <DataMember()> _
    Public Property NumeroOperacionPago() As String
End Class
