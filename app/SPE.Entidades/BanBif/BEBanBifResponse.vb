
Imports System.Runtime.Serialization
Imports System
Imports System.Collections
Imports System.Collections.Generic

<Serializable()> <DataContract()> _
Public Class BEBanBifResponse(Of T)
    Public Sub New()

    End Sub

    <DataMember()> _
    Public Property ObjBanBif() As T


    <DataMember()> _
    Public Property Mensaje() As String

    'valores
    '(-1).- error de excepcion
    '( 0).- VALIDACION LOGIC
    '( 1).- funcionalidad correcta

    <DataMember()> _
    Public Property Estado() As Integer

End Class
