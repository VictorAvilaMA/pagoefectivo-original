﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()>
Public Class BECuentaDineroVirtual
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Private _idCuentaDineroVirtual As Long
    Private _numero As String
    Private _aliasCuenta As String 'Alias es palabra reservada
    Private _idMoneda As Integer
    Private _idCliente As Integer
    Private _saldo As Decimal
    Private _saldoRetenido As Decimal
    Private _fechaSolicitud As DateTime
    Private _fechaAprobacion As DateTime
    Private _fechaRechazo As DateTime
    Private _observacion As String
    Private _observacionBloqueo As String
    Private _idUsuarioRespuesta As Integer
    Private _fechaActualizacion As DateTime
    Private _idUsuarioActualizacion As Integer
    Private _idEstado As Integer
    Private _idUsuario As Long
    Private _diasTranscurridosSolicitud As Integer
    '-------
    Private _abrevMoneda As String
    Private _descMoneda As String
    Private _idTipoComision As Integer
    Private _factorComision As Decimal
    Private _IsUserMonedero As Integer
    Private _MontoRecarga As Decimal
    Private _parametrosEmailMonedero As BEParametrosEmailMonedero


#Region "CONSTRUCTORES"
    Sub New(ByVal reader As IDataReader)
        Me.IdCuentaDineroVirtual = ObjectToInt32(reader("IdCuenta"))
        Me.Numero = ObjectToString(reader("Numero"))
        Me.AliasCuenta = ObjectToString(reader("Alias"))
        Me.IdMoneda = ObjectToInt32(reader("IdMoneda"))
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.Saldo = ObjectToDecimal(reader("Saldo"))
        Me.SaldoRetenido = ObjectToDecimal(reader("SaldoRetenido"))
        Me.FechaSolicitud = ObjectToDateTime(reader("FechaSolicitud"))
        Me.FechaAprobacion = ObjectToDateTime(reader("FechaAprobacion"))
        Me.FechaRechazo = ObjectToDateTime(reader("FechaRechazo"))
        Me.Observacion = ObjectToString(reader("Observacion"))
        Me.IdUsuarioAprobacion = ObjectToInt32(reader("IdUsuarioAprobacion"))
        Me.IdUsuarioRechazo = ObjectToInt32(reader("IdUsuarioRechazo"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.IdUsuarioActualizacion = ObjectToInt32(reader("IdUsuarioActualizacion"))
        Me.IdEstado = ObjectToInt32(reader("IdEstado"))
        Me.ClienteNombre = ObjectToString(reader("CLIENTE"))
        Me.AbrevMoneda = ObjectToString(reader("ABREV_MONEDA"))
        Me.DiasTranscurridosSolicitud = ObjectToInt32(reader("DIAS_TRANSC"))
        Me.EstadoDescripcion = ObjectToString(reader("DESC_ESTADO"))
        Me.TotalPageNumbers = ObjectToInt32(reader("TOTAL"))
    End Sub

    Sub New(ByVal reader As IDataReader, ByVal nro As Integer)
        Me.IdCuentaDineroVirtual = ObjectToInt32(reader("IdCuenta"))
        Me.Numero = ObjectToString(reader("Numero"))
        Me.AliasCuenta = ObjectToString(reader("Alias"))
        Me.IdMoneda = ObjectToInt32(reader("IdMoneda"))
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.Saldo = ObjectToDecimal(reader("Saldo"))
        Me.SaldoRetenido = ObjectToDecimal(reader("SaldoRetenido"))
        Me.FechaSolicitud = ObjectToDateTime(reader("FechaSolicitud"))
        Me.FechaAprobacion = ObjectToDateTime(reader("FechaAprobacion"))
        Me.FechaRechazo = ObjectToDateTime(reader("FechaRechazo"))
        Me.Observacion = ObjectToString(reader("Observacion"))
        Me.IdUsuarioAprobacion = ObjectToInt32(reader("IdUsuarioAprobacion"))
        Me.IdUsuarioRechazo = ObjectToInt32(reader("IdUsuarioRechazo"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.IdUsuarioActualizacion = ObjectToInt32(reader("IdUsuarioActualizacion"))
        Me.IdEstado = ObjectToInt32(reader("IdEstado"))
        Me.DescMoneda = ObjectToString(reader("MonedaDescripcion"))
        Me.AbrevMoneda = ObjectToString(reader("MonedaSimbolo"))
        Me.ClienteNombre = ObjectToString(reader("ClienteNombre"))
        Me.IdTipoComision = ObjectToInt32(reader("TipoComision"))
        Me.FactorComision = ObjectToDecimal(reader("FactorComision"))
        Me.MontoMinTransferencia = ObjectToDecimal(reader("MinTransferencia"))

    End Sub

    Sub New()
        ' TODO: Complete member initialization 
    End Sub

#End Region

    <DataMember()> _
    Public Property IdCuentaDineroVirtual() As Long
        Get
            Return _idCuentaDineroVirtual
        End Get
        Set(ByVal value As Long)
            _idCuentaDineroVirtual = value
        End Set
    End Property

    <DataMember()> _
    Public Property Numero() As String
        Get
            Return _numero
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property Saldo() As Decimal
        Get
            Return _saldo
        End Get
        Set(ByVal value As Decimal)
            _saldo = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaSolicitud() As DateTime
        Get
            Return _fechaSolicitud
        End Get
        Set(ByVal value As DateTime)
            _fechaSolicitud = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaAprobacion() As DateTime
        Get
            Return _fechaAprobacion
        End Get
        Set(ByVal value As DateTime)
            _fechaAprobacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaRechazo() As DateTime
        Get
            Return _fechaRechazo
        End Get
        Set(ByVal value As DateTime)
            _fechaRechazo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuarioRespuesta() As Integer
        Get
            Return _idUsuarioRespuesta
        End Get
        Set(ByVal value As Integer)
            _idUsuarioRespuesta = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuario() As Long
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As Long)
            _idUsuario = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuarioActualizacion() As Long
        Get
            Return _idUsuarioActualizacion
        End Get
        Set(ByVal value As Long)
            _idUsuarioActualizacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaActualizacion() As DateTime
        Get
            Return _fechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _fechaActualizacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property AbrevMoneda() As String
        Get
            Return _abrevMoneda
        End Get
        Set(ByVal value As String)
            _abrevMoneda = value
        End Set
    End Property





    <DataMember()> _
    Public Property AliasCuenta() As String
        Get
            Return _aliasCuenta
        End Get
        Set(ByVal value As String)
            _aliasCuenta = value
        End Set
    End Property

    Private _monedaDescripcion As String
    <DataMember()> _
    Public Property MonedaDescripcion As String
        Get
            Return _monedaDescripcion
        End Get
        Set(ByVal value As String)
            _monedaDescripcion = value
        End Set
    End Property

    Private _monedaSimbolo As String
    <DataMember()> _
    Public Property MonedaSimbolo As String
        Get
            Return _monedaSimbolo
        End Get
        Set(ByVal value As String)
            _monedaSimbolo = value
        End Set
    End Property

    'Private _saldoRetenido As String
    <DataMember()> _
    Public Property SaldoRetenido As Decimal
        Get
            Return _saldoRetenido
        End Get
        Set(ByVal value As Decimal)
            _saldoRetenido = value
        End Set
    End Property

    Private _estadoDescripcion As String
    <DataMember()> _
    Public Property EstadoDescripcion As String
        Get
            Return _estadoDescripcion
        End Get
        Set(ByVal value As String)
            _estadoDescripcion = value
        End Set
    End Property




    Private _usuarioModificador As String
    <DataMember()> _
    Public Property UsuarioActualizacion As String
        Get
            Return _usuarioModificador
        End Get
        Set(ByVal value As String)
            _usuarioModificador = value
        End Set
    End Property

    Private _clienteNombre As String
    <DataMember()> _
    Public Property ClienteNombre As String
        Get
            Return _clienteNombre
        End Get
        Set(ByVal value As String)
            _clienteNombre = value
        End Set
    End Property

    Private _idUsuarioAprobacion As String
    <DataMember()> _
    Public Property IdUsuarioAprobacion As String
        Get
            Return _idUsuarioAprobacion
        End Get
        Set(ByVal value As String)
            _idUsuarioAprobacion = value
        End Set
    End Property

    Private _usuarioAprobacion As String
    <DataMember()> _
    Public Property UsuarioAprobacion As String
        Get
            Return _usuarioAprobacion
        End Get
        Set(ByVal value As String)
            _usuarioAprobacion = value
        End Set
    End Property

    Private _idUsuarioRechazo As String
    <DataMember()> _
    Public Property IdUsuarioRechazo As String
        Get
            Return _idUsuarioRechazo
        End Get
        Set(ByVal value As String)
            _idUsuarioRechazo = value
        End Set
    End Property

    Private _usuarioRechazo As String
    <DataMember()> _
    Public Property UsuarioRechazo As String
        Get
            Return _usuarioRechazo
        End Get
        Set(ByVal value As String)
            _usuarioRechazo = value
        End Set
    End Property






    Private _email As String
    <DataMember()> _
    Public Property Email As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property


    <DataMember()>
    Public Property DiasTranscurridosSolicitud As Integer
        Get
            Return _diasTranscurridosSolicitud
        End Get
        Set(ByVal value As Integer)
            _diasTranscurridosSolicitud = value
        End Set
    End Property


    <DataMember()> _
    Public Property DescMoneda() As String
        Get
            Return _descMoneda
        End Get
        Set(ByVal value As String)
            _descMoneda = value
        End Set
    End Property


    <DataMember()> _
    Public Property IdTipoComision() As Integer
        Get
            Return _idTipoComision
        End Get
        Set(ByVal value As Integer)
            _idTipoComision = value
        End Set
    End Property

    <DataMember()> _
    Public Property FactorComision() As Decimal
        Get
            Return _factorComision
        End Get
        Set(ByVal value As Decimal)
            _factorComision = value
        End Set
    End Property

    <DataMember()> _
    Public Property IsUserMonedero As Integer
        Get
            Return _IsUserMonedero
        End Get
        Set(ByVal value As Integer)
            _IsUserMonedero = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoRecarga As Decimal
        Get
            Return _MontoRecarga
        End Get
        Set(ByVal value As Decimal)
            _MontoRecarga = value
        End Set
    End Property

    Dim _EquivalenciaBancaria As String
    <DataMember()> _
    Public Property EquivalenciaBancaria() As String
        Get
            Return _EquivalenciaBancaria
        End Get
        Set(ByVal value As String)
            _EquivalenciaBancaria = value
        End Set
    End Property

    <DataMember()> _
    Public Property ParametrosEmailMonedero() As BEParametrosEmailMonedero
        Get
            Return _parametrosEmailMonedero
        End Get
        Set(ByVal value As BEParametrosEmailMonedero)
            _parametrosEmailMonedero = value
        End Set
    End Property

    <DataMember()> _
    Public Property ObservacionBloqueo As String
        Get
            Return _observacionBloqueo
        End Get
        Set(ByVal value As String)
            _observacionBloqueo = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoMinTransferencia As Decimal








    Function prc_ConsultarCuentaDineroVirtual(ByVal reader As IDataReader) As _3Dev.FW.Entidades.BusinessEntityBase

        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.IdUsuario = ObjectToInt32(reader("IdUsuario"))
        Me.IdCuentaDineroVirtual = ObjectToInt32(reader("IdCuenta"))
        Me.Numero = ObjectToString(reader("Numero"))
        Me.Email = ObjectToString(reader("Email"))
        Me.AliasCuenta = ObjectToString(reader("Alias"))
        Me.IdMoneda = ObjectToInt32(reader("IdMoneda"))
        Me.MonedaDescripcion = ObjectToString(reader("MonedaDescripcion"))
        Me.MonedaSimbolo = ObjectToString(reader("MonedaSimbolo"))
        Me.IdCliente = ObjectToInt32(reader("IdCliente"))
        Me.ClienteNombre = ObjectToString(reader("ClienteNombre"))
        Me.Saldo = ObjectToDecimal(reader("Saldo"))
        Me.SaldoRetenido = ObjectToDecimal(reader("SaldoRetenido"))
        Me.FechaSolicitud = ObjectToDateTime(reader("FechaSolicitud"))
        Me.FechaAprobacion = ObjectToDateTime(reader("FechaAprobacion"))
        Me.FechaRechazo = ObjectToDateTime(reader("FechaRechazo"))

        Me.Observacion = ObjectToString(reader("Observacion"))
        Me.IdUsuarioAprobacion = ObjectToString(reader("IdUsuarioAprobacion"))
        Me.UsuarioAprobacion = ObjectToString(reader("UsuarioAprobacion"))
        Me.IdUsuarioRechazo = ObjectToString(reader("IdUsuarioRechazo"))
        Me.UsuarioRechazo = ObjectToString(reader("UsuarioRechazo"))
        Me.FechaActualizacion = ObjectToDateTime(reader("FechaActualizacion"))
        Me.UsuarioActualizacion = ObjectToString(reader("UsuarioActualizacion"))
        Me.IdUsuarioActualizacion = ObjectToInt64(reader("IdUsuarioActualizacion"))
        Me.EstadoDescripcion = ObjectToString(reader("EstadoDescripcion"))
        Me.IdEstado = ObjectToInt32(reader("IdEstado"))
        Me.IsUserMonedero = ObjectToInt32(reader("IsUserMonedero"))
        Me.MontoRecarga = ObjectToDecimal(reader("MontoRecarga"))
        Me.EquivalenciaBancaria = ObjectToString(reader("EquivalenciaBancaria"))
        Me.ObservacionBloqueo = DataUtil.ObjectToString(reader("ObservacionBloqueo"))

        Return Me
    End Function





End Class
