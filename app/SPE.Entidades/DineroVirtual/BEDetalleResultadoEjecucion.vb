﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BEDetalleResultadoEjecucion
    Inherits BEAuditoria
    Private _IdDetalle As Long
    Private _IdResultado As Long
    Private _IdCuenta As Long
    Private _MontoTransferencia As Decimal

    Private _NumeroCuenta As String
    Private _Cliente As String
    Private _Email As String

    <DataMember()>
    Public Property IdDetalle() As Long
        Get
            Return _IdDetalle
        End Get
        Set(ByVal value As Long)
            _IdDetalle = value
        End Set
    End Property
    <DataMember()>
    Public Property IdResultado() As Long
        Get
            Return _IdResultado
        End Get
        Set(ByVal value As Long)
            _IdResultado = value
        End Set
    End Property
    <DataMember()>
    Public Property IdCuenta() As Long
        Get
            Return _IdCuenta
        End Get
        Set(ByVal value As Long)
            _IdCuenta = value
        End Set
    End Property
    <DataMember()>
    Public Property MontoTransferencia() As Decimal
        Get
            Return _MontoTransferencia
        End Get
        Set(ByVal value As Decimal)
            _MontoTransferencia = value
        End Set
    End Property

    <DataMember()>
    Public Property NumeroCuenta() As String
        Get
            Return _NumeroCuenta
        End Get
        Set(ByVal value As String)
            _NumeroCuenta = value
        End Set
    End Property
    <DataMember()>
    Public Property Cliente() As String
        Get
            Return _Cliente
        End Get
        Set(ByVal value As String)
            _Cliente = value
        End Set
    End Property
    <DataMember()>
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property

    Public Sub New(ByVal reader As IDataReader)
        IdDetalle = ObjectToInt64(reader("IdResultado"))
        IdResultado = ObjectToInt64(reader("IdResultado"))
        IdCuenta = ObjectToInt64(reader("IdCuenta"))
        MontoTransferencia = ObjectToDecimal(reader("MontoTransferencia"))
        NumeroCuenta = ObjectToString(reader("NumeroCuenta"))
        Cliente = ObjectToString(reader("Cliente"))
        Email = ObjectToString(reader("Email"))
        Me.TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))
    End Sub
    Public Sub New()
    End Sub
End Class
