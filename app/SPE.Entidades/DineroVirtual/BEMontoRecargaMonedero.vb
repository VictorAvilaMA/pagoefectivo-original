﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()>
Public Class BEMontoRecargaMonedero

    Private _idMoneda As Integer
    Private _descMoneda As String
    Private _idCliente As Integer
    Private _idUsuario As Long
    Private _monto As Integer
    Private _fechaCreacion As DateTime
    Private _fechaActualizacion As DateTime

#Region "Constructor"

    Public Sub New()
    End Sub

    Public Sub New(ByVal reader As IDataReader)
        _idMoneda = ObjectToInt32(reader("NumeroOperacion"))
        _idCliente = ObjectToInt32(reader("FechaMovimiento"))
        _monto = ObjectToInt32(reader("Descripcion"))
        _fechaCreacion = ObjectToDateTime(reader("ConceptoPago"))
        _fechaActualizacion = ObjectToDateTime(reader("Monto"))
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal val1 As Integer)
        _idMoneda = ObjectToInt32(reader("IdMoneda"))
        _descMoneda = ObjectToString(reader("DescMoneda"))
        _idCliente = ObjectToInt32(reader("IdCliente"))
        _monto = ObjectToInt32(reader("Monto"))

    End Sub

#End Region

    <DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Integer)
            _idMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescMoneda() As String
        Get
            Return _descMoneda
        End Get
        Set(ByVal value As String)
            _descMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuario() As Long
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As Long)
            _idUsuario = value
        End Set
    End Property

    <DataMember()> _
    Public Property Monto() As Integer
        Get
            Return _monto
        End Get
        Set(ByVal value As Integer)
            _monto = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaActualizacion() As DateTime
        Get
            Return _fechaActualizacion
        End Get
        Set(ByVal value As DateTime)
            _fechaActualizacion = value
        End Set
    End Property

End Class
