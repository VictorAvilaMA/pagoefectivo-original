﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()>
Public Class BEMovimientoCuenta
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        'Constructor
        _numeroOperacion = ObjectToString(reader("NumeroOperacion"))
        _fechaMovimiento = ObjectToDateTime(reader("FechaMovimiento"))
        _descTipoMovimiento = ObjectToString(reader("Descripcion"))
        _observacion = ObjectToString(reader("Observacion"))
        _conceptoPago = ObjectToString(reader("ConceptoPago"))
        _monto = ObjectToDecimal(reader("Monto"))
        _saldoPostOperacion = ObjectToDecimal(reader("SaldoPostOperacion"))
        _codigoReferencia = ObjectToString(reader("CodigoReferencia"))
        _titularCuenta = ObjectToString(reader("TitularCuenta"))
        _nombrePortal = ObjectToString(reader("NombrePortal"))
        _idTipoMovimiento = ObjectToInt32(reader("IdTipoMovimiento"))
        TotalPageNumbers = ObjectToInt32(reader("TOTAL"))

    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal val1 As Integer)
        'Constructor
        _numeroOperacion = ObjectToString(reader("NumeroOperacion"))
        _fechaMovimiento = ObjectToDateTime(reader("FechaMovimiento"))
        _descTipoMovimiento = ObjectToString(reader("Descripcion"))
        _conceptoPago = ObjectToString(reader("ConceptoPago"))
        _observacion = ObjectToString(reader("Observacion"))
        _monto = ObjectToDecimal(reader("Monto"))
        _saldoPostOperacion = ObjectToDecimal(reader("SaldoPostOperacion"))
    End Sub

    Private _idMovimientoCuenta As Long
    Private _idCuentaVirtual As Long
    Private _idTipoMovimiento As Integer
    Private _monto As Decimal
    Private _observacion As String
    Private _saldoPreOperacion As Decimal
    Private _saldoPostOperacion As Decimal
    Private _idTokenDineroVirtual As Long
    Private _codigoReferencia As String
    Private _nombrePortal As String
    Private _conceptoPago As String
    Private _titularCuenta As String
    Private _idTransferenciaDe As Long
    Private _idMovimientoAsociado As Long
    Private _idSolicitudRetiroDineroVirtual As Long
    Private _idOrigenMovimiento As Integer
    Private _idAperturaOrigen As Integer
    Private _numeroOperacion As String
    Private _fechaCreacion As DateTime
    Private _fechaMovimiento As DateTime
    Private _idUsuarioCreacion As Integer
    Private _fechaInicio As DateTime
    Private _fechaFin As DateTime

    Private _idTipoComision As Integer
    Private _valorComision As Decimal

    '-------------
    Private _descTipoMovimiento As String
    Private _nroToken As String
    Private _observacionTransferencia As String

    <DataMember()> _
    Public Property IdMovimientoCuenta() As Long
        Get
            Return _idMovimientoCuenta
        End Get
        Set(ByVal value As Long)
            _idMovimientoCuenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdCuentaVirtual() As Long
        Get
            Return _idCuentaVirtual
        End Get
        Set(ByVal value As Long)
            _idCuentaVirtual = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTipoMovimiento() As Integer
        Get
            Return _idTipoMovimiento
        End Get
        Set(ByVal value As Integer)
            _idTipoMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaMovimiento() As DateTime
        Get
            Return _fechaMovimiento
        End Get
        Set(ByVal value As DateTime)
            _fechaMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(ByVal value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As DateTime)
            _fechaFin = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdUsuario() As Integer
        Get
            Return _idUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _idUsuarioCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property CodigoReferencia() As String
        Get
            Return _codigoReferencia
        End Get
        Set(ByVal value As String)
            _codigoReferencia = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombrePortal() As String
        Get
            Return _nombrePortal
        End Get
        Set(ByVal value As String)
            _nombrePortal = value
        End Set
    End Property

    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property TitularCuenta() As String
        Get
            Return _titularCuenta
        End Get
        Set(ByVal value As String)
            _titularCuenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTransferenciaDe() As Long
        Get
            Return _idTransferenciaDe
        End Get
        Set(ByVal value As Long)
            _idTransferenciaDe = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdMovimientoAsociado() As Long
        Get
            Return _idMovimientoAsociado
        End Get
        Set(ByVal value As Long)
            _idMovimientoAsociado = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdOrigenMovimiento() As Integer
        Get
            Return _idOrigenMovimiento
        End Get
        Set(ByVal value As Integer)
            _idOrigenMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdAperturaOrigen() As Integer
        Get
            Return _idAperturaOrigen
        End Get
        Set(ByVal value As Integer)
            _idAperturaOrigen = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOperacion() As String
        Get
            Return _numeroOperacion
        End Get
        Set(ByVal value As String)
            _numeroOperacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property SaldoPreOperacion() As Decimal
        Get
            Return _saldoPreOperacion
        End Get
        Set(ByVal value As Decimal)
            _saldoPreOperacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property SaldoPostOperacion() As Decimal
        Get
            Return _saldoPostOperacion
        End Get
        Set(ByVal value As Decimal)
            _saldoPostOperacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTokenDineroVirtual() As Long
        Get
            Return _idTokenDineroVirtual
        End Get
        Set(ByVal value As Long)
            _idTokenDineroVirtual = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescTipoMovimiento() As String
        Get
            Return _descTipoMovimiento
        End Get
        Set(ByVal value As String)
            _descTipoMovimiento = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTipoComision() As Integer
        Get
            Return _idTipoComision
        End Get
        Set(ByVal value As Integer)
            _idTipoComision = value
        End Set
    End Property

    <DataMember()> _
    Public Property ValorComision() As Decimal
        Get
            Return _valorComision
        End Get
        Set(ByVal value As Decimal)
            _valorComision = value
        End Set
    End Property


    <DataMember()> _
    Public Property NroToken() As String
        Get
            Return _nroToken
        End Get
        Set(ByVal value As String)
            _nroToken = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdSolicitudRetiroDineroVirtual() As Long
        Get
            Return _idSolicitudRetiroDineroVirtual
        End Get
        Set(ByVal value As Long)
            _idSolicitudRetiroDineroVirtual = value
        End Set
    End Property
    <DataMember()> _
    Public Property ObservacionTransferencia() As String
        Get
            Return _observacionTransferencia
        End Get
        Set(ByVal value As String)
            _observacionTransferencia = value
        End Set
    End Property


End Class
