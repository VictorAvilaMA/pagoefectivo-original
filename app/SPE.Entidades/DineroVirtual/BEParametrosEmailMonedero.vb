﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()>
Public Class BEParametrosEmailMonedero

    Private _nombreCliente As String
    Private _tipoOperacion As String
    Private _tiempoExpiracion As Integer
    Private _emailCliente As String
    Private _fechaSolicitud As DateTime
    Private _descripcionMoneda As String
    Private _aliasCuenta As String
    Private _fechaExpiracion As DateTime
    Private _numeroOrdenPago As String
    Private _descripcionEmpresa As String
    Private _simboloMoneda As String
    Private _monto As Decimal
    Private _conceptoPago As String
    Private _nombreClienteOrigen As String
    Private _nombreClienteDestino As String
    Private _cuentaOrigen As String
    Private _cuentaDestino As String
    Private _titularCuentaOrigen As String
    Private _titularCuentaDestino As String
    Private _montoComision As Decimal
    Private _numeroSolicitud As String
    Private _nombreBanco As String
    Private _nombrePC As String
    Private _fechaRegistro As DateTime
    Private _observacion As String
    Private _resultadoInt As Integer
    Private _pTo As String
    Private _pFrom As String
    Private _observacionTransferencia As String

    Public Sub New()
    End Sub

    Public Sub New(ByVal reader As IDataReader, ByVal Consulta As String)
        Select Case Consulta
            Case "SolicitudRetiroEmail"
                Me.NombreCliente = DataUtil.ObjectToString(reader("NombreCliente"))
                Me.CuentaOrigen = DataUtil.ObjectToString(reader("CuentaOrigen"))
                Me.Monto = DataUtil.ObjectToDecimal(reader("Monto"))
                Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
                Me.NombreBanco = DataUtil.ObjectToString(reader("NombreBanco"))
                Me.CuentaDestino = DataUtil.ObjectToString(reader("CuentaDestino"))
                Me.TitularCuentaDestino = DataUtil.ObjectToString(reader("TitularCuentaDestino"))
                Me.NumeroSolicitud = DataUtil.ObjectToString(reader("SolicitudRetiroMonedero"))

            Case "SolicitudPagoPortal"

                Me.NumeroSolicitud = DataUtil.ObjectToString(reader("IdSolicitudPago"))
                Me.NombreCliente = DataUtil.ObjectToString(reader("ClienteNombre"))
                Me.SimboloMoneda = DataUtil.ObjectToString(reader("SimboloMoneda"))
                Me.Monto = DataUtil.ObjectToDecimal(reader("Total"))
                Me.ConceptoPago = DataUtil.ObjectToString(reader("ConceptoPago"))

        End Select
    End Sub

    <DataMember()> _
    Public Property PTo() As String
        Get
            Return _pTo
        End Get
        Set(ByVal value As String)
            _pTo = value
        End Set
    End Property

    <DataMember()> _
    Public Property PFrom() As String
        Get
            Return _pFrom
        End Get
        Set(ByVal value As String)
            _pFrom = value
        End Set
    End Property


    <DataMember()> _
    Public Property NombreCliente() As String
        Get
            Return _nombreCliente
        End Get
        Set(ByVal value As String)
            _nombreCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property TipoOperacion() As String
        Get
            Return _tipoOperacion
        End Get
        Set(ByVal value As String)
            _tipoOperacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property TiempoExpiracion() As Integer
        Get
            Return _tiempoExpiracion
        End Get
        Set(ByVal value As Integer)
            _tiempoExpiracion = value
        End Set
    End Property

    <DataMember()> _
    Public Property EmailCliente() As String
        Get
            Return _emailCliente
        End Get
        Set(ByVal value As String)
            _emailCliente = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaSolicitud() As DateTime
        Get
            Return _fechaSolicitud
        End Get
        Set(ByVal value As DateTime)
            _fechaSolicitud = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionMoneda() As String
        Get
            Return _descripcionMoneda
        End Get
        Set(ByVal value As String)
            _descripcionMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property AliasCuenta() As String
        Get
            Return _aliasCuenta
        End Get
        Set(ByVal value As String)
            _aliasCuenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaExpiracion() As DateTime
        Get
            Return _fechaExpiracion
        End Get
        Set(ByVal value As DateTime)
            _fechaExpiracion = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property DescripcionEmpresa() As String
        Get
            Return _descripcionEmpresa
        End Get
        Set(ByVal value As String)
            _descripcionEmpresa = value
        End Set
    End Property

    <DataMember()> _
    Public Property SimboloMoneda() As String
        Get
            Return _simboloMoneda
        End Get
        Set(ByVal value As String)
            _simboloMoneda = value
        End Set
    End Property

    <DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreClienteOrigen() As String
        Get
            Return _nombreClienteOrigen
        End Get
        Set(ByVal value As String)
            _nombreClienteOrigen = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreClienteDestino() As String
        Get
            Return _nombreClienteDestino
        End Get
        Set(ByVal value As String)
            _nombreClienteDestino = value
        End Set
    End Property

    <DataMember()> _
    Public Property CuentaOrigen() As String
        Get
            Return _cuentaOrigen
        End Get
        Set(ByVal value As String)
            _cuentaOrigen = value
        End Set
    End Property

    <DataMember()> _
    Public Property CuentaDestino() As String
        Get
            Return _cuentaDestino
        End Get
        Set(ByVal value As String)
            _cuentaDestino = value
        End Set
    End Property

    <DataMember()> _
    Public Property TitularCuentaOrigen() As String
        Get
            Return _titularCuentaOrigen
        End Get
        Set(ByVal value As String)
            _titularCuentaOrigen = value
        End Set
    End Property

    <DataMember()> _
    Public Property TitularCuentaDestino() As String
        Get
            Return _titularCuentaDestino
        End Get
        Set(ByVal value As String)
            _titularCuentaDestino = value
        End Set
    End Property

    <DataMember()> _
    Public Property MontoComision() As Decimal
        Get
            Return _montoComision
        End Get
        Set(ByVal value As Decimal)
            _montoComision = value
        End Set
    End Property

    <DataMember()> _
    Public Property NumeroSolicitud() As String
        Get
            Return _numeroSolicitud
        End Get
        Set(ByVal value As String)
            _numeroSolicitud = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombreBanco() As String
        Get
            Return _nombreBanco
        End Get
        Set(ByVal value As String)
            _nombreBanco = value
        End Set
    End Property

    <DataMember()> _
    Public Property NombrePC() As String
        Get
            Return _nombrePC
        End Get
        Set(ByVal value As String)
            _nombrePC = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaRegistro() As DateTime
        Get
            Return _fechaRegistro
        End Get
        Set(ByVal value As DateTime)
            _fechaRegistro = value
        End Set
    End Property

    <DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property ResultadoInt() As Integer
        Get
            Return _resultadoInt
        End Get
        Set(ByVal value As Integer)
            _resultadoInt = value
        End Set
    End Property

    <DataMember()> _
    Public Property ObservacionTransferencia() As String
        Get
            Return _observacionTransferencia
        End Get
        Set(ByVal value As String)
            _observacionTransferencia = value
        End Set
    End Property
End Class
