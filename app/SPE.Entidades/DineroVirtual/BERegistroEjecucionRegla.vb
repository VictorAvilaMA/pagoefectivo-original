﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BERegistroEjecucionRegla
    Private _IdRegistro As Long
    Private _IdRegla As Integer
    Private _FechaEjecucion As DateTime

    <DataMember()>
    Public Property IdRegistro() As Long
        Get
            Return _IdRegistro
        End Get
        Set(value As Long)
            _IdRegistro = value
        End Set
    End Property

    <DataMember()>
    Public Property IdRegla() As Integer
        Get
            Return _IdRegla
        End Get
        Set(value As Integer)
            _IdRegla = value
        End Set
    End Property

    <DataMember()>
    Public Property FechaEjecucion() As DateTime
        Get
            Return _FechaEjecucion
        End Get
        Set(value As DateTime)
            _FechaEjecucion = value
        End Set
    End Property

    Public Sub New(ByVal reader As IDataReader)
        IdRegistro = ObjectToInt64(reader("IdRegistro"))
        IdRegla = ObjectToInt64(reader("IdRegla"))
        FechaEjecucion = ObjectToDateTime(reader("FechaEjecucion"))
    End Sub

End Class
