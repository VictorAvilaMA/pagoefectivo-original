﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil

<Serializable()>
<DataContract()>
Public Class BEReglaMovimientoSospechoso
    Private _IdRegla As Integer
    Private _Descripcion As String
    Private _FechaEjecucion As DateTime
    Private _Activo As Boolean
    Private _PeriodoEjecucionDias As Integer
    Private _AbrevRegla As String

    <DataMember()>
    Public Property IdRegla() As Integer
        Get
            Return _IdRegla
        End Get
        Set(value As Integer)
            _IdRegla = value
        End Set
    End Property
    <DataMember()>
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(value As String)
            _Descripcion = value
        End Set
    End Property
    <DataMember()>
    Public Property FechaEjecucion() As DateTime
        Get
            Return _FechaEjecucion
        End Get
        Set(value As DateTime)
            _FechaEjecucion = value
        End Set
    End Property
    <DataMember()>
    Public Property Activo() As Boolean
        Get
            Return _Activo
        End Get
        Set(value As Boolean)
            _Activo = value
        End Set
    End Property
    <DataMember()>
    Public Property PeriodoEjecucionDias() As Integer
        Get
            Return _PeriodoEjecucionDias
        End Get
        Set(value As Integer)
            _PeriodoEjecucionDias = value
        End Set
    End Property
    <DataMember()>
    Public Property AbrevRegla() As String
        Get
            Return _AbrevRegla
        End Get
        Set(value As String)
            _AbrevRegla = value
        End Set
    End Property

    Public Sub New(ByVal reader As IDataReader)
        IdRegla = ObjectToInt32(reader("IdRegla"))
        Descripcion = ObjectToString(reader("Descripcion"))
        FechaEjecucion = ObjectToDateTime(reader("FechaEjecucion"))
        Activo = ObjectToBool(reader("Activo"))
        PeriodoEjecucionDias = ObjectToInt32(reader("PeriodoEjecucionDias"))
        AbrevRegla = ObjectToString(reader("AbrevRegla"))

    End Sub
End Class
