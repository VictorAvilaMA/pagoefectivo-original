﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Util

<Serializable()>
<DataContract()>
Public Class BEResultadoEjecucionRegla
    Inherits BEAuditoria
    Private _IdResultado As Long
    Private _IdRegistro As Long
    Private _IdCuenta As Long
    Private _NroTransferenciaRealizadas As Integer
    Private _TotalTransferido As Decimal

    Private _NumeroCuenta As String
    Private _Cliente As String
    Private _Email As String

    <DataMember()>
    Public Property IdResultado() As Long
        Get
            Return _IdResultado
        End Get
        Set(ByVal value As Long)
            _IdResultado = value
        End Set
    End Property
    <DataMember()>
    Public Property IdRegistro() As Long
        Get
            Return _IdRegistro
        End Get
        Set(ByVal value As Long)
            _IdRegistro = value
        End Set
    End Property
    <DataMember()>
    Public Property IdCuenta() As Long
        Get
            Return _IdCuenta
        End Get
        Set(ByVal value As Long)
            _IdCuenta = value
        End Set
    End Property
    <DataMember()>
    Public Property NroTransferenciaRealizadas() As Integer
        Get
            Return _NroTransferenciaRealizadas
        End Get
        Set(ByVal value As Integer)
            _NroTransferenciaRealizadas = value
        End Set
    End Property
    <DataMember()>
    Public Property TotalTransferido() As Decimal
        Get
            Return _TotalTransferido
        End Get
        Set(ByVal value As Decimal)
            _TotalTransferido = value
        End Set
    End Property

    <DataMember()>
    Public Property NumeroCuenta() As String
        Get
            Return _NumeroCuenta
        End Get
        Set(ByVal value As String)
            _NumeroCuenta = value
        End Set
    End Property
    <DataMember()>
    Public Property Cliente() As String
        Get
            Return _Cliente
        End Get
        Set(ByVal value As String)
            _Cliente = value
        End Set
    End Property
    <DataMember()>
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property


    Public Sub New()

    End Sub
    Public Sub New(ByVal reader As IDataReader)
        IdResultado = ObjectToInt64(reader("IdResultado"))
        IdRegistro = ObjectToInt64(reader("IdRegistro"))
        IdCuenta = ObjectToInt64(reader("IdCuenta"))
        NroTransferenciaRealizadas = ObjectToInt32(reader("NroTransferenciaRealizada"))
        TotalTransferido = ObjectToDecimal(reader("TotalTransferido"))
        NumeroCuenta = ObjectToString(reader("NumeroCuenta"))
        Cliente = ObjectToString(reader("Cliente"))
        Email = ObjectToString(reader("Email"))
        Me.TotalPageNumbers = ObjectToString(reader("TOTAL_RESULT"))

    End Sub
End Class