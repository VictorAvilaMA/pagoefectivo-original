﻿Imports System.Runtime.Serialization
<Serializable()> <DataContract()>
Public Class BESolicitudRetiroDineroVirtual
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    '''
    ''' Gets or Sets IdSolicitud
    '''
    <DataMember()>
    Public Property IdSolicitud() As Long
        Get
            Return _IdSolicitud
        End Get
        Set(value As Long)
            _IdSolicitud = value
        End Set
    End Property
    Private _IdSolicitud As Long


    Private _IdCuenta As Long
    <DataMember()>
    Public Property IdCuenta() As Long
        Get
            Return _IdCuenta
        End Get
        Set(value As Long)
            _IdCuenta = value
        End Set
    End Property

    '''
    ''' Gets or Sets MontoSolicitado
    '''
    <DataMember()>
    Public Property MontoSolicitado() As Decimal
        Get
            Return _MontoSolicitado
        End Get
        Set(value As Decimal)
            _MontoSolicitado = value
        End Set
    End Property
    Private _MontoSolicitado As Decimal

    '''
    ''' Gets or Sets FechaSolicitud
    '''
    <DataMember()>
    Public Property FechaSolicitud() As Date
        Get
            Return _FechaSolicitud
        End Get
        Set(value As Date)
            _FechaSolicitud = value
        End Set
    End Property
    Private _FechaSolicitud As Date

    '''
    ''' Gets or Sets IdBanco
    '''
    <DataMember()>
    Public Property IdBanco() As Integer
        Get
            Return _IdBanco
        End Get
        Set(value As Integer)
            _IdBanco = value
        End Set
    End Property
    Private _IdBanco As Integer

    '''
    ''' Gets or Sets BancoDestinoDescripcion
    '''
    <DataMember()>
    Public Property BancoDestinoDescripcion() As String
        Get
            Return _BancoDestinoDescripcion
        End Get
        Set(value As String)
            _BancoDestinoDescripcion = value
        End Set
    End Property
    Private _BancoDestinoDescripcion As String

    '''
    ''' Gets or Sets CuentaBancariaDestino
    '''
    <DataMember()>
    Public Property CuentaBancariaDestino() As String
        Get
            Return _CuentaBancariaDestino
        End Get
        Set(value As String)
            _CuentaBancariaDestino = value
        End Set
    End Property
    Private _CuentaBancariaDestino As String

    '''
    ''' Gets or Sets TitularCuentaBancariaDestino
    '''
    <DataMember()>
    Public Property TitularCuentaBancariaDestino() As String
        Get
            Return _TitularCuentaBancariaDestino
        End Get
        Set(value As String)
            _TitularCuentaBancariaDestino = value
        End Set
    End Property
    Private _TitularCuentaBancariaDestino As String

    '''
    ''' Gets or Sets ObservacionAdministrador
    '''
    <DataMember()>
    Public Property ObservacionAdministrador() As String
        Get
            Return _ObservacionAdministrador
        End Get
        Set(value As String)
            _ObservacionAdministrador = value
        End Set
    End Property
    Private _ObservacionAdministrador As String

    '''
    ''' Gets or Sets ObservacionTesoreria
    '''
    <DataMember()>
    Public Property ObservacionTesoreria() As String
        Get
            Return _ObservacionTesoreria
        End Get
        Set(value As String)
            _ObservacionTesoreria = value
        End Set
    End Property
    Private _ObservacionTesoreria As String

    '''
    ''' Gets or Sets RutaDocAdjFtp
    '''
    <DataMember()>
    Public Property RutaDocAdjFtp() As String
        Get
            Return _RutaDocAdjFtp
        End Get
        Set(value As String)
            _RutaDocAdjFtp = value
        End Set
    End Property
    Private _RutaDocAdjFtp As String

    '''
    ''' Gets or Sets NombreDocAdjFtp
    '''
    <DataMember()>
    Public Property NombreDocAdjFtp() As String
        Get
            Return _NombreDocAdjFtp
        End Get
        Set(value As String)
            _NombreDocAdjFtp = value
        End Set
    End Property
    Private _NombreDocAdjFtp As String

    '''
    ''' Gets or Sets NombreInicialDocAdj
    '''
    <DataMember()>
    Public Property NombreInicialDocAdj() As String
        Get
            Return _NombreInicialDocAdj
        End Get
        Set(value As String)
            _NombreInicialDocAdj = value
        End Set
    End Property
    Private _NombreInicialDocAdj As String

    '''
    ''' Gets or Sets IdToken
    '''
    <DataMember()>
    Public Property IdToken() As Long
        Get
            Return _IdToken
        End Get
        Set(value As Long)
            _IdToken = value
        End Set
    End Property
    Private _IdToken As Long

    '''
    ''' Gets or Sets FechaAprobacion
    '''
    <DataMember()>
    Public Property FechaAprobacion() As Date
        Get
            Return _FechaAprobacion
        End Get
        Set(value As Date)
            _FechaAprobacion = value
        End Set
    End Property
    Private _FechaAprobacion As Date

    '''
    ''' Gets or Sets IdUsuarioAprobacion
    '''
    <DataMember()>
    Public Property IdUsuarioAprobacion() As Integer
        Get
            Return _IdUsuarioAprobacion
        End Get
        Set(value As Integer)
            _IdUsuarioAprobacion = value
        End Set
    End Property
    Private _IdUsuarioAprobacion As Integer

    '''
    ''' Gets or Sets UsuarioAprobacion
    '''
    <DataMember()>
    Public Property UsuarioAprobacion() As String
        Get
            Return _UsuarioAprobacion
        End Get
        Set(value As String)
            _UsuarioAprobacion = value
        End Set
    End Property
    Private _UsuarioAprobacion As String

    '''
    ''' Gets or Sets FechaRechazo
    '''
    <DataMember()>
    Public Property FechaRechazo() As Date
        Get
            Return _FechaRechazo
        End Get
        Set(value As Date)
            _FechaRechazo = value
        End Set
    End Property
    Private _FechaRechazo As Date

    '''
    ''' Gets or Sets IdUsuarioRechazo
    '''
    <DataMember()>
    Public Property IdUsuarioRechazo() As Integer
        Get
            Return _IdUsuarioRechazo
        End Get
        Set(value As Integer)
            _IdUsuarioRechazo = value
        End Set
    End Property
    Private _IdUsuarioRechazo As Integer

    '''
    ''' Gets or Sets UsuarioRechazo
    '''
    <DataMember()>
    Public Property UsuarioRechazo() As String
        Get
            Return _UsuarioRechazo
        End Get
        Set(value As String)
            _UsuarioRechazo = value
        End Set
    End Property
    Private _UsuarioRechazo As String

    '''
    ''' Gets or Sets FechaLiquidacion
    '''
    <DataMember()>
    Public Property FechaLiquidacion() As Date
        Get
            Return _FechaLiquidacion
        End Get
        Set(value As Date)
            _FechaLiquidacion = value
        End Set
    End Property
    Private _FechaLiquidacion As Date

    '''
    ''' Gets or Sets IdUsuarioLiquidacion
    '''
    <DataMember()>
    Public Property IdUsuarioLiquidacion() As Integer
        Get
            Return _IdUsuarioLiquidacion
        End Get
        Set(value As Integer)
            _IdUsuarioLiquidacion = value
        End Set
    End Property
    Private _IdUsuarioLiquidacion As Integer

    '''
    ''' Gets or Sets UsuarioLiquidacion
    '''
    <DataMember()>
    Public Property UsuarioLiquidacion() As String
        Get
            Return _UsuarioLiquidacion
        End Get
        Set(value As String)
            _UsuarioLiquidacion = value
        End Set
    End Property
    Private _UsuarioLiquidacion As String

    '''
    ''' Gets or Sets FechaAnulacion
    '''
    <DataMember()>
    Public Property FechaAnulacion() As Date
        Get
            Return _FechaAnulacion
        End Get
        Set(value As Date)
            _FechaAnulacion = value
        End Set
    End Property
    Private _FechaAnulacion As Date

    '''
    ''' Gets or Sets IdUsuarioAnulacion
    '''
    <DataMember()>
    Public Property IdUsuarioAnulacion() As Integer
        Get
            Return _IdUsuarioAnulacion
        End Get
        Set(value As Integer)
            _IdUsuarioAnulacion = value
        End Set
    End Property
    Private _IdUsuarioAnulacion As Integer

    '''
    ''' Gets or Sets UsuarioAnulacion
    '''
    <DataMember()>
    Public Property UsuarioAnulacion() As String
        Get
            Return _UsuarioAnulacion
        End Get
        Set(value As String)
            _UsuarioAnulacion = value
        End Set
    End Property
    Private _UsuarioAnulacion As String

    '''
    ''' Gets or Sets IdEstado
    '''
    <DataMember()>
    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(value As Integer)
            _IdEstado = value
        End Set
    End Property
    Private _IdEstado As Integer

    '''
    ''' Gets or Sets EstadoDescripcion
    '''
    <DataMember()>
    Public Property EstadoDescripcion() As String
        Get
            Return _EstadoDescripcion
        End Get
        Set(value As String)
            _EstadoDescripcion = value
        End Set
    End Property
    Private _EstadoDescripcion As String

    '''
    ''' Gets or Sets Numero
    '''
    <DataMember()>
    Public Property Numero() As String
        Get
            Return _Numero
        End Get
        Set(value As String)
            _Numero = value
        End Set
    End Property
    Private _Numero As String

    '''
    ''' Gets or Sets Alias
    '''
    <DataMember()>
    Public Property AliasNombre() As String
        Get
            Return _Alias
        End Get
        Set(value As String)
            _Alias = value
        End Set
    End Property
    Private _Alias As String

    '''
    ''' Gets or Sets Saldo
    '''
    <DataMember()>
    Public Property Saldo() As Decimal
        Get
            Return _Saldo
        End Get
        Set(value As Decimal)
            _Saldo = value
        End Set
    End Property
    Private _Saldo As Decimal

    '''
    ''' Gets or Sets MonedaDescripcion
    '''
    <DataMember()>
    Public Property MonedaDescripcion() As String
        Get
            Return _MonedaDescripcion
        End Get
        Set(value As String)
            _MonedaDescripcion = value
        End Set
    End Property
    Private _MonedaDescripcion As String

    '''
    ''' Gets or Sets MonedaSimbolo
    '''
    <DataMember()>
    Public Property MonedaSimbolo() As String
        Get
            Return _MonedaSimbolo
        End Get
        Set(value As String)
            _MonedaSimbolo = value
        End Set
    End Property
    Private _MonedaSimbolo As String

    '''
    ''' Gets or Sets ClienteNombre
    '''
    <DataMember()>
    Public Property ClienteNombre() As String
        Get
            Return _ClienteNombre
        End Get
        Set(value As String)
            _ClienteNombre = value
        End Set
    End Property
    Private _ClienteNombre As String

    '''
    ''' Gets or Sets Email
    '''
    <DataMember()>
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(value As String)
            _Email = value
        End Set
    End Property
    Private _Email As String

    <DataMember()>
    Public Property IdCliente() As String
        Get
            Return _idCliente
        End Get
        Set(value As String)
            _idCliente = value
        End Set
    End Property
    Private _idCliente As String

    <DataMember()>
    Public Property FechaInicio() As Date
        Get
            Return _fechaInicio
        End Get
        Set(value As Date)
            _fechaInicio = value
        End Set
    End Property
    Private _fechaInicio As Date

    <DataMember()>
    Public Property FechaFin() As Date
        Get
            Return _fechaFin
        End Get
        Set(value As Date)
            _fechaFin = value
        End Set
    End Property
    Private _fechaFin As Date


    <DataMember()>
    Public Property IdUsuario() As Integer
        Get
            Return _idUsuario
        End Get
        Set(value As Integer)
            _idUsuario = value
        End Set
    End Property
    Private _idUsuario As Integer


    <DataMember()>
    Public Property InfoCuenta() As String
        Get
            Return _InfoCuenta
        End Get
        Set(value As String)
            _InfoCuenta = value
        End Set
    End Property
    Private _InfoCuenta As String



    <DataMember()>
    Public Property InfoSolicitud() As String
        Get
            Return _InfoSolicitud
        End Get
        Set(value As String)
            _InfoSolicitud = value
        End Set
    End Property
    Private _InfoSolicitud As String

    <DataMember()>
    Public Property NroToken() As String
        Get
            Return _NroToken
        End Get
        Set(value As String)
            _NroToken = value
        End Set
    End Property
    Private _NroToken As String


    <DataMember()>
    Public Property NumeroCuentaComercioId As Integer
        Get
            Return _NumeroCuentaComercioId
        End Get
        Set(value As Integer)
            _NumeroCuentaComercioId = value
        End Set
    End Property
    Private _NumeroCuentaComercioId As Integer

    <DataMember()>
    Public Property IdPerfil As Integer
        Get
            Return _IdPerfil
        End Get
        Set(ByVal value As Integer)
            _IdPerfil = value
        End Set
    End Property
    Private _IdPerfil As Integer

    Function prc_ConsultarSolicitudRetiro(dataReader As IDataReader) As BESolicitudRetiroDineroVirtual
        Dim obj As New BESolicitudRetiroDineroVirtual()

        obj.IdCliente = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdCliente"))
        obj.IdSolicitud = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdSolicitud"))
        obj.MontoSolicitado = _3Dev.FW.Util.DataUtil.ObjectToDecimal(dataReader("MontoSolicitado"))
        obj.FechaSolicitud = _3Dev.FW.Util.DataUtil.ObjectToDateTime(dataReader("FechaSolicitud"))
        obj.IdBanco = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdBanco"))
        obj.BancoDestinoDescripcion = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("BancoDestinoDescripcion"))
        obj.CuentaBancariaDestino = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("CuentaBancariaDestino"))
        obj.TitularCuentaBancariaDestino = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("TitularCuentaBancariaDestino"))
        obj.ObservacionAdministrador = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("ObservacionAdministrador"))
        obj.ObservacionTesoreria = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("ObservacionTesoreria"))
        obj.RutaDocAdjFtp = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("RutaDocAdjFtp"))
        obj.NombreDocAdjFtp = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("NombreDocAdjFtp"))
        obj.NombreInicialDocAdj = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("NombreInicialDocAdj"))
        obj.IdToken = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdToken"))
        obj.FechaAprobacion = _3Dev.FW.Util.DataUtil.ObjectToDateTime(dataReader("FechaAprobacion"))
        obj.IdUsuarioAprobacion = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdUsuarioAprobacion"))
        obj.UsuarioAprobacion = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("UsuarioAprobacion"))
        obj.FechaRechazo = _3Dev.FW.Util.DataUtil.ObjectToDateTime(dataReader("FechaRechazo"))
        obj.IdUsuarioRechazo = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdUsuarioRechazo"))
        obj.UsuarioRechazo = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("UsuarioRechazo"))
        obj.FechaLiquidacion = _3Dev.FW.Util.DataUtil.ObjectToDateTime(dataReader("FechaLiquidacion"))
        obj.IdUsuarioLiquidacion = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdUsuarioLiquidacion"))
        obj.UsuarioLiquidacion = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("UsuarioLiquidacion"))
        obj.FechaAnulacion = _3Dev.FW.Util.DataUtil.ObjectToDateTime(dataReader("FechaAnulacion"))
        obj.IdUsuarioAnulacion = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdUsuarioAnulacion"))
        obj.UsuarioAnulacion = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("UsuarioAnulacion"))
        obj.IdEstado = _3Dev.FW.Util.DataUtil.ObjectToInt64(dataReader("IdEstado"))
        obj.EstadoDescripcion = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("EstadoDescripcion"))
        obj.Numero = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("Numero"))
        obj.AliasNombre = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("Alias"))
        obj.Saldo = _3Dev.FW.Util.DataUtil.ObjectToDecimal(dataReader("Saldo"))
        obj.MonedaDescripcion = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("MonedaDescripcion"))
        obj.MonedaSimbolo = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("MonedaSimbolo"))
        obj.ClienteNombre = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("ClienteNombre"))
        obj.Email = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("Email"))
        obj.InfoCuenta = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("InfoCuenta"))
        obj.InfoSolicitud = _3Dev.FW.Util.DataUtil.ObjectToString(dataReader("InfoSolicitud"))
        obj.NumeroCuentaComercioId = _3Dev.FW.Util.DataUtil.ObjectToInt32(dataReader("IdBancoComercio"))
        obj.TotalPageNumbers = _3Dev.FW.Util.DataUtil.ObjectToInt16(dataReader("TOTAL"))


        Return obj

    End Function

    Function IsUserMonedero() As Boolean
        Throw New NotImplementedException
    End Function



 
End Class

