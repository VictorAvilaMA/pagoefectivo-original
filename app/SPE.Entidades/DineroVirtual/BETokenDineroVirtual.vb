﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()>
Public Class BETokenDineroVirtual
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Private _idTokenDineroVirtual As Long
    Private _idCuenta As Long
    Private _idTipoOperacion As Integer
    Private _token As String
    Private _fechaCreacion As DateTime
    Private _fechaCaducidad As DateTime
    Private _activo As Boolean
    Private _utilizado As Boolean
    Private _parametrosEmailMonedero As BEParametrosEmailMonedero
    

    <DataMember()> _
    Public Property IdTokenDineroVirtual() As Long
        Get
            Return _idTokenDineroVirtual
        End Get
        Set(value As Long)
            _idTokenDineroVirtual = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdCuenta() As Long
        Get
            Return _idCuenta
        End Get
        Set(value As Long)
            _idCuenta = value
        End Set
    End Property

    <DataMember()> _
    Public Property Token() As String
        Get
            Return _token
        End Get
        Set(value As String)
            _token = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCreacion() As DateTime
        Get
            Return _fechaCreacion
        End Get
        Set(value As DateTime)
            _fechaCreacion = value
        End Set
    End Property

    <DataMember()> _
    Public Property FechaCaducidad() As DateTime
        Get
            Return _fechaCaducidad
        End Get
        Set(value As DateTime)
            _fechaCaducidad = value
        End Set
    End Property

    <DataMember()> _
    Public Property Activo() As Boolean
        Get
            Return _activo
        End Get
        Set(value As Boolean)
            _activo = value
        End Set
    End Property

    <DataMember()> _
    Public Property Utilizado() As Boolean
        Get
            Return _utilizado
        End Get
        Set(value As Boolean)
            _utilizado = value
        End Set
    End Property

    <DataMember()> _
    Public Property IdTipoOperacion() As Integer
        Get
            Return _idTipoOperacion
        End Get
        Set(value As Integer)
            _idTipoOperacion = value
        End Set
    End Property

    '**********************************************
    Public Property ParametrosEmailMonedero() As BEParametrosEmailMonedero
        Get
            Return _parametrosEmailMonedero
        End Get
        Set(ByVal value As BEParametrosEmailMonedero)
            _parametrosEmailMonedero = value
        End Set
    End Property

    Sub Clean()
        Activo = Nothing
        FechaCaducidad = Nothing
        FechaCreacion = Nothing
        IdCuenta = Nothing
        IdTipoOperacion = Nothing
        PageNumber = Nothing
        PageSize = Nothing
        ParametrosEmailMonedero = Nothing
        PropOrder = Nothing
        TipoOrder = Nothing
        TotalPageNumbers = Nothing
        Utilizado = Nothing
    End Sub


End Class
