Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> Public Class BEOPPOSResultado
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.IdMovimiento = ObjectToInt64(reader("IdMovimiento"))
        Me.XMLRespuesta = ObjectToString(reader("MENSAJE"))
    End Sub

    Private _idMovimiento As Int64
	<DataMember()> _
    Public Property IdMovimiento() As Int64
        Get
            Return _idMovimiento
        End Get
        Set(ByVal value As Int64)
            _idMovimiento = value
        End Set
    End Property

    Private _XMLRespuesta As String
	<DataMember()> _
    Public Property XMLRespuesta() As String
        Get
            Return _XMLRespuesta
        End Get
        Set(ByVal value As String)
            _XMLRespuesta = value
        End Set
    End Property



End Class
