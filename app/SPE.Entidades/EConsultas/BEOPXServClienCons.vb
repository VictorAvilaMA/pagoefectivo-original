Imports System.Runtime.Serialization
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

<Serializable()> <DataContract()> _
    Public Class BEOPXServClienCons
    Inherits _3Dev.FW.Entidades.BusinessEntityBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        Me.NumeroOrdenPago = DataUtil.ObjectToString(reader("NumeroOrdenPago"))
        Me.IdOrdenPago = DataUtil.ObjectToInt(reader("IdOrdenPago"))
        Me.IdCliente = DataUtil.ObjectToInt(reader("IdCliente"))
        Me.IdUsuario = DataUtil.ObjectToInt(reader("IdUsuario"))
        Me.OrderIdComercio = DataUtil.ObjectToString(reader("OrderIdComercio"))
        Me.TiempoExpiracion = DataUtil.ObjectToDecimal(reader("TiempoExpiracion"))
        Me.FechaEmision = DataUtil.ObjectToDateTime(reader("FechaEmision"))
    End Sub


    Private _numeroOrdenPago As String
    Private _orderIdComercio As String
    Private _idCliente As Integer
    Private _idUsuario As Integer
    Private _idOrdenPago As Integer

	<DataMember()> _
    Public Property IdOrdenPago() As Integer
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Integer)
            _idOrdenPago = value
        End Set
    End Property


	<DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property



	<DataMember()> _
    Public Property IdUsuario() As Integer
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As Integer)
            _idUsuario = value
        End Set
    End Property


	<DataMember()> _
    Public Property IdCliente() As Integer
        Get
            Return _idCliente
        End Get
        Set(ByVal value As Integer)
            _idCliente = value
        End Set
    End Property

	<DataMember()> _
    Public Property OrderIdComercio() As String
        Get
            Return _orderIdComercio
        End Get
        Set(ByVal value As String)
            _orderIdComercio = value
        End Set
    End Property

    Private _tiempoExpiracion As Decimal
	<DataMember()> _
    Public Property TiempoExpiracion() As Decimal
        Get
            Return _tiempoExpiracion
        End Get
        Set(ByVal value As Decimal)
            _tiempoExpiracion = value
        End Set
    End Property

    Private _fechaEmision As DateTime
	<DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _fechaEmision
        End Get
        Set(ByVal value As DateTime)
            _fechaEmision = value
        End Set
    End Property


End Class
