Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()> Public Class BEOcultarEmpresa
    Inherits _3Dev.FW.Entidades.BusinessEntityBase
    Private _idServicio As Integer
    Private _idEmpresaContratante As Integer
    Private _ServicioDescripcion As String
    Private _EmpresaContratanteDescripcion As String
    Private _OcultarEmpresa As Boolean
    Private _OcultarImagenEmpresa As Boolean
    Private _OcultarImagenServicio As Boolean

    Public Sub New(ByVal reader As IDataReader)
        ActCampos(reader)
    End Sub
    Public Sub New()

    End Sub
    Private Sub ActCampos(ByVal reader As IDataReader)
        Me.idServicio = _3Dev.FW.Util.DataUtil.ObjectToInt(reader("idServicio"))
        Me.idEmpresaContratante = _3Dev.FW.Util.DataUtil.ObjectToInt(reader("idEmpresaContratante"))
        Me.ServicioDescripcion = _3Dev.FW.Util.DataUtil.ObjectToString(reader("ServicioDescripcion"))
        Me.EmpresaContratanteDescripcion = _3Dev.FW.Util.DataUtil.ObjectToString(reader("EmpresaContratanteDescripcion"))
        Me.OcultarEmpresa = _3Dev.FW.Util.DataUtil.StringToBool(reader("OcultarEmpresa"))
        Dim img As Byte()
        img = reader("ImagenEmpresa")
        Me.OcultarImagenEmpresa = ((Not (img Is Nothing)) And (img.Length = 0))
        img = Nothing
        img = reader("ImagenServicio")
        Me.OcultarImagenServicio = ((Not (img Is Nothing)) And (img.Length = 0))
        img = Nothing
    End Sub

	<DataMember()> _
    Public Property idEmpresaContratante() As Integer
        Get
            Return _idEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _idEmpresaContratante = value
        End Set
    End Property

	<DataMember()> _
    Public Property idServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

	<DataMember()> _
    Public Property ServicioDescripcion() As String
        Get
            Return _ServicioDescripcion
        End Get
        Set(ByVal value As String)
            _ServicioDescripcion = value
        End Set
    End Property
	<DataMember()> _
    Public Property EmpresaContratanteDescripcion() As String
        Get
            Return _EmpresaContratanteDescripcion
        End Get
        Set(ByVal value As String)
            _EmpresaContratanteDescripcion = value
        End Set
    End Property
	<DataMember()> _
    Public Property OcultarEmpresa() As Boolean
        Get
            Return _OcultarEmpresa
        End Get
        Set(ByVal value As Boolean)
            _OcultarEmpresa = value
        End Set
    End Property
	<DataMember()> _
    Public Property OcultarImagenServicio() As Boolean
        Get
            Return _OcultarImagenServicio
        End Get
        Set(ByVal value As Boolean)
            _OcultarImagenServicio = value
        End Set
    End Property
	<DataMember()> _
    Public Property OcultarImagenEmpresa() As Boolean
        Get
            Return _OcultarImagenEmpresa
        End Get
        Set(ByVal value As Boolean)
            _OcultarImagenEmpresa = value
        End Set
    End Property
End Class
