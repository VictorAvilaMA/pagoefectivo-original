Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad

<Serializable()> <DataContract()> _
Public Class BEParametroPOS


    Private _codComercio As String
    Private _nroEstablecimiento As String
    Private _nroOrdenPago As String
    Private _nroOperacion As String
    Private _nroOperacionAnular As String
    Private _monto As Decimal
    Private _idmoneda As Integer
    Private _idMedioPago As Integer
    Private _montoefectivosoles As Decimal
    Private _montoefectivodolares As Decimal
    Private _montotarjetasoles As Decimal
    Private _montotarjetadolares As Decimal

	<DataMember()> _
    Public Property codComercio() As String
        Get
            Return _codComercio
        End Get
        Set(ByVal value As String)
            _codComercio = value
        End Set
    End Property
	<DataMember()> _
    Public Property NroEstablecimiento() As String
        Get
            Return _nroEstablecimiento
        End Get
        Set(ByVal value As String)
            _nroEstablecimiento = value
        End Set
    End Property

	<DataMember()> _
    Public Property NroOrdenPago() As String
        Get
            Return _nroOrdenPago
        End Get
        Set(ByVal value As String)
            _nroOrdenPago = value
        End Set
    End Property

	<DataMember()> _
    Public Property NroOperacionAnular() As String
        Get
            Return _nroOperacionAnular
        End Get
        Set(ByVal value As String)
            _nroOperacionAnular = value
        End Set
    End Property

	<DataMember()> _
    Public Property NroOperacion() As String
        Get
            Return _nroOperacion
        End Get
        Set(ByVal value As String)
            _nroOperacion = value
        End Set
    End Property


	<DataMember()> _
    Public Property Monto() As Decimal
        Get
            Return _monto
        End Get
        Set(ByVal value As Decimal)
            _monto = value
        End Set
    End Property

	<DataMember()> _
    Public Property IdMoneda() As Integer
        Get
            Return _idmoneda
        End Get
        Set(ByVal value As Integer)
            _idmoneda = value
        End Set
    End Property


	<DataMember()> _
    Public Property IdMedioPago() As Integer
        Get
            Return _idMedioPago
        End Get
        Set(ByVal value As Integer)
            _idMedioPago = value
        End Set
    End Property


	<DataMember()> _
    Public Property MontoEfectivoSoles() As Decimal
        Get
            Return _montoefectivosoles
        End Get
        Set(ByVal value As Decimal)
            _montoefectivosoles = value
        End Set
    End Property

	<DataMember()> _
    Public Property MontoEfectivoDolares() As Decimal
        Get
            Return _montoefectivodolares
        End Get
        Set(ByVal value As Decimal)
            _montoefectivodolares = value
        End Set
    End Property

	<DataMember()> _
    Public Property MontoTarjetaSoles() As Decimal
        Get
            Return _montotarjetasoles
        End Get
        Set(ByVal value As Decimal)
            _montotarjetasoles = value
        End Set
    End Property

	<DataMember()> _
    Public Property MontoTarjetaDolares() As Decimal
        Get
            Return _montotarjetadolares
        End Get
        Set(ByVal value As Decimal)
            _montotarjetadolares = value
        End Set
    End Property




End Class
