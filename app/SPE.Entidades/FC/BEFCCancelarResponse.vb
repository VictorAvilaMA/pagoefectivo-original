Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEFCCancelarResponse

    Private _CodResultado As String
    <DataMember()> _
    Public Property CodResultado() As String
        Get
            Return _CodResultado
        End Get
        Set(ByVal value As String)
            _CodResultado = value
        End Set
    End Property

    Private _MensajeResultado As String
    <DataMember()> _
    Public Property MensajeResultado() As String
        Get
            Return _MensajeResultado
        End Get
        Set(ByVal value As String)
            _MensajeResultado = value
        End Set
    End Property

    Private _CodMovimiento As String
    <DataMember()> _
    Public Property CodMovimiento() As String
        Get
            Return _CodMovimiento
        End Get
        Set(ByVal value As String)
            _CodMovimiento = value
        End Set
    End Property

    Private _CodServicio As String
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio
        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property

End Class
