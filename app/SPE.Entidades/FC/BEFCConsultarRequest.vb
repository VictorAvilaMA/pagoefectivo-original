Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEFCConsultarRequest

    Private _CodPuntoVenta As String
    <DataMember()> _
    Public Property CodPuntoVenta() As String
        Get
            Return _CodPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodPuntoVenta = value
        End Set
    End Property

    Private _NroSerieTerminal As String
    <DataMember()> _
    Public Property NroSerieTerminal() As String
        Get
            Return _NroSerieTerminal
        End Get
        Set(ByVal value As String)
            _NroSerieTerminal = value
        End Set
    End Property

    Private _NroOperacionFullCarga As String
    <DataMember()> _
    Public Property NroOperacionFullCarga() As String
        Get
            Return _NroOperacionFullCarga
        End Get
        Set(ByVal value As String)
            _NroOperacionFullCarga = value
        End Set
    End Property

    Private _CIP As String
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _CodServicio As String
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio
        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property

End Class
