Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEFCConsultarResponse

    Private _CIP As String
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _CodResultado As String
    <DataMember()> _
    Public Property CodResultado() As String
        Get
            Return _CodResultado
        End Get
        Set(ByVal value As String)
            _CodResultado = value
        End Set
    End Property

    Private _MensajeResultado As String
    <DataMember()> _
    Public Property MensajeResultado() As String
        Get
            Return _MensajeResultado
        End Get
        Set(ByVal value As String)
            _MensajeResultado = value
        End Set
    End Property

    Private _Monto As String
    <DataMember()> _
    Public Property Monto() As String
        Get
            Return _Monto
        End Get
        Set(ByVal value As String)
            _Monto = value
        End Set
    End Property

    Private _CodMoneda As String
    <DataMember()> _
    Public Property CodMoneda() As String
        Get
            Return _CodMoneda
        End Get
        Set(ByVal value As String)
            _CodMoneda = value
        End Set
    End Property

    Private _ConceptoPago As String
    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property

    Private _FechaVencimiento As String
    <DataMember()> _
    Public Property FechaVencimiento() As String
        Get
            Return _FechaVencimiento
        End Get
        Set(ByVal value As String)
            _FechaVencimiento = value
        End Set
    End Property

    Private _CodServicio As String
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio
        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property

End Class
