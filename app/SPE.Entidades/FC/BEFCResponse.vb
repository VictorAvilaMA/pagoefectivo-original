Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEFCResponse(Of T)

    Public Sub New()

    End Sub

    Private _objFC As T
    <DataMember()> _
    Public Property ObjFC() As T
        Get
            Return _objFC
        End Get
        Set(ByVal value As T)
            _objFC = value
        End Set
    End Property

    Private _descripcion1 As String
    <DataMember()> _
    Public Property Descripcion1() As String
        Get
            Return _descripcion1
        End Get
        Set(ByVal value As String)
            _descripcion1 = value
        End Set
    End Property

    Private _descripcion2 As String
    <DataMember()> _
    Public Property Descripcion2() As String
        Get
            Return _descripcion2
        End Get
        Set(ByVal value As String)
            _descripcion2 = value
        End Set
    End Property

End Class
