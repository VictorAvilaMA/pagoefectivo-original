﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Excepciones

<Serializable()> <DataContract()>
Public Class FCBusinessException
    Private _code As Integer
    Private _message As String
    Private _StackTrace As String

    Public Sub New(ByVal code As Integer, ByVal message As String, ByVal StackTrace As String)
        _code = code
        _message = message
        _StackTrace = StackTrace
    End Sub

    <DataMember()> _
    Public Property code As Integer
        Get
            Return _code
        End Get
        Set(ByVal value As Integer)
            _code = value
        End Set
    End Property

    <DataMember()> _
    Public Property message As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property

    <DataMember()> _
    Public Property StackTrace() As String
        Get
            Return _StackTrace
        End Get
        Set(ByVal value As String)
            _StackTrace = value
        End Set
    End Property

End Class
