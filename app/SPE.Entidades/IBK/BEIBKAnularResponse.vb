<Serializable()> _
Public Class BEIBKAnularResponse

    Private _MessageTypeIdentification As BEBKCampoResponse
    Public Property MessageTypeIdentification() As BEBKCampoResponse
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoResponse
    Public Property PrimaryBitMap() As BEBKCampoResponse
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoResponse
    Public Property SecondaryBitMap() As BEBKCampoResponse
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _PrimaryAccountNumber As BEBKCampoResponse
    Public Property PrimaryAccountNumber() As BEBKCampoResponse
        Get
            Return _PrimaryAccountNumber
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _PrimaryAccountNumber = value
        End Set
    End Property

    Private _ProcessingCode As BEBKCampoResponse
    Public Property ProcessingCode() As BEBKCampoResponse
        Get
            Return _ProcessingCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ProcessingCode = value
        End Set
    End Property

    Private _AmountTransaction As BEBKCampoResponse
    Public Property AmountTransaction() As BEBKCampoResponse
        Get
            Return _AmountTransaction
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _AmountTransaction = value
        End Set
    End Property

    Private _Trace As BEBKCampoResponse
    Public Property Trace() As BEBKCampoResponse
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Trace = value
        End Set
    End Property

    Private _TimeLocalTransaction As BEBKCampoResponse
    Public Property TimeLocalTransaction() As BEBKCampoResponse
        Get
            Return _TimeLocalTransaction
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TimeLocalTransaction = value
        End Set
    End Property

    Private _DateLocalTransaction As BEBKCampoResponse
    Public Property DateLocalTransaction() As BEBKCampoResponse
        Get
            Return _DateLocalTransaction
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DateLocalTransaction = value
        End Set
    End Property

    Private _POSEntryMode As BEBKCampoResponse
    Public Property POSEntryMode() As BEBKCampoResponse
        Get
            Return _POSEntryMode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _POSEntryMode = value
        End Set
    End Property

    Private _POSConditionCode As BEBKCampoResponse
    Public Property POSConditionCode() As BEBKCampoResponse
        Get
            Return _POSConditionCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _POSConditionCode = value
        End Set
    End Property

    Private _AcquirerInstitutionIDCode As BEBKCampoResponse
    Public Property AcquirerInstitutionIDCode() As BEBKCampoResponse
        Get
            Return _AcquirerInstitutionIDCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _AcquirerInstitutionIDCode = value
        End Set
    End Property

    Private _ForwardInstitutionIDCode As BEBKCampoResponse
    Public Property ForwardInstitutionIDCode() As BEBKCampoResponse
        Get
            Return _ForwardInstitutionIDCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ForwardInstitutionIDCode = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoResponse
    Public Property RetrievalReferenceNumber() As BEBKCampoResponse
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _ApprovalCode As BEBKCampoResponse
    Public Property ApprovalCode() As BEBKCampoResponse
        Get
            Return _ApprovalCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ApprovalCode = value
        End Set
    End Property

    Private _ResponseCode As BEBKCampoResponse
    Public Property ResponseCode() As BEBKCampoResponse
        Get
            Return _ResponseCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ResponseCode = value
        End Set
    End Property

    Private _CardAcceptorTerminalID As BEBKCampoResponse
    Public Property CardAcceptorTerminalID() As BEBKCampoResponse
        Get
            Return _CardAcceptorTerminalID
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CardAcceptorTerminalID = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoResponse
    Public Property TransactionCurrencyCode() As BEBKCampoResponse
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _LongitudCampo As BEBKCampoResponse
    Public Property LongitudCampo() As BEBKCampoResponse
        Get
            Return _LongitudCampo
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _LongitudCampo = value
        End Set
    End Property

    Private _CIP As BEBKCampoResponse
    Public Property CIP() As BEBKCampoResponse
        Get
            Return _CIP
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CIP = value
        End Set
    End Property

    Private _CodigoServicio As BEBKCampoResponse
    Public Property CodigoServicio() As BEBKCampoResponse
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoServicio = value
        End Set
    End Property

    Private _CodigoRetorno As BEBKCampoResponse
    Public Property CodigoRetorno() As BEBKCampoResponse
        Get
            Return _CodigoRetorno
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoRetorno = value
        End Set
    End Property

    Private _DescripcionCodigoRetorno As BEBKCampoResponse
    Public Property DescripcionCodigoRetorno() As BEBKCampoResponse
        Get
            Return _DescripcionCodigoRetorno
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DescripcionCodigoRetorno = value
        End Set
    End Property

    Public Sub New()
        MessageTypeIdentification = New BEBKCampoResponse(4, BEBKTipoDato.Numerico)
        PrimaryBitMap = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        SecondaryBitMap = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        PrimaryAccountNumber = New BEBKCampoResponse(19, BEBKTipoDato.Numerico)
        ProcessingCode = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        AmountTransaction = New BEBKCampoResponse(12, BEBKTipoDato.Numerico)
        Trace = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        TimeLocalTransaction = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        DateLocalTransaction = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        POSEntryMode = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        POSConditionCode = New BEBKCampoResponse(2, BEBKTipoDato.Numerico)
        AcquirerInstitutionIDCode = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        ForwardInstitutionIDCode = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        RetrievalReferenceNumber = New BEBKCampoResponse(12, BEBKTipoDato.Alfanumerico)
        ApprovalCode = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        ResponseCode = New BEBKCampoResponse(2, BEBKTipoDato.Numerico)
        CardAcceptorTerminalID = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        TransactionCurrencyCode = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        LongitudCampo = New BEBKCampoResponse(4, BEBKTipoDato.Numerico)
        CIP = New BEBKCampoResponse(14, BEBKTipoDato.Alfanumerico)
        CodigoServicio = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        CodigoRetorno = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        DescripcionCodigoRetorno = New BEBKCampoResponse(30, BEBKTipoDato.Alfanumerico)
    End Sub

    Public Overrides Function ToString() As String
        Return String.Concat(MessageTypeIdentification.ToString, PrimaryBitMap.ToString, SecondaryBitMap.ToString, _
            PrimaryAccountNumber.ToString, ProcessingCode.ToString, AmountTransaction.ToString, Trace.ToString, _
            TimeLocalTransaction.ToString, DateLocalTransaction.ToString, POSEntryMode.ToString, _
            POSConditionCode.ToString, AcquirerInstitutionIDCode.ToString, ForwardInstitutionIDCode.ToString, _
            RetrievalReferenceNumber.ToString, ApprovalCode.ToString, ResponseCode.ToString, _
            CardAcceptorTerminalID.ToString, TransactionCurrencyCode.ToString, LongitudCampo.ToString, _
            CIP.ToString, CodigoServicio.ToString, CodigoRetorno.ToString, DescripcionCodigoRetorno.ToString)
    End Function

End Class