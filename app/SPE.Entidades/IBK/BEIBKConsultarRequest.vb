<Serializable()> _
Public Class BEIBKConsultarRequest
    Inherits BEBKBaseRequest

    Private _MessageTypeIdentification As BEBKCampoRequest
    Public Property MessageTypeIdentification() As BEBKCampoRequest
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoRequest
    Public Property PrimaryBitMap() As BEBKCampoRequest
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoRequest
    Public Property SecondaryBitMap() As BEBKCampoRequest
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _PrimaryAccountNumber As BEBKCampoRequest
    Public Property PrimaryAccountNumber() As BEBKCampoRequest
        Get
            Return _PrimaryAccountNumber
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PrimaryAccountNumber = value
        End Set
    End Property

    Private _ProcessingCode As BEBKCampoRequest
    Public Property ProcessingCode() As BEBKCampoRequest
        Get
            Return _ProcessingCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ProcessingCode = value
        End Set
    End Property

    Private _AmountTransaction As BEBKCampoRequest
    Public Property AmountTransaction() As BEBKCampoRequest
        Get
            Return _AmountTransaction
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _AmountTransaction = value
        End Set
    End Property

    Private _Trace As BEBKCampoRequest
    Public Property Trace() As BEBKCampoRequest
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Trace = value
        End Set
    End Property

    Private _TimeLocalTransaction As BEBKCampoRequest
    Public Property TimeLocalTransaction() As BEBKCampoRequest
        Get
            Return _TimeLocalTransaction
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TimeLocalTransaction = value
        End Set
    End Property

    Private _DateLocalTransaction As BEBKCampoRequest
    Public Property DateLocalTransaction() As BEBKCampoRequest
        Get
            Return _DateLocalTransaction
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DateLocalTransaction = value
        End Set
    End Property

    Private _POSEntryMode As BEBKCampoRequest
    Public Property POSEntryMode() As BEBKCampoRequest
        Get
            Return _POSEntryMode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _POSEntryMode = value
        End Set
    End Property

    Private _POSConditionCode As BEBKCampoRequest
    Public Property POSConditionCode() As BEBKCampoRequest
        Get
            Return _POSConditionCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _POSConditionCode = value
        End Set
    End Property

    Private _AcquirerInstitutionIDCode As BEBKCampoRequest
    Public Property AcquirerInstitutionIDCode() As BEBKCampoRequest
        Get
            Return _AcquirerInstitutionIDCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _AcquirerInstitutionIDCode = value
        End Set
    End Property

    Private _ForwardInstitutionIDCode As BEBKCampoRequest
    Public Property ForwardInstitutionIDCode() As BEBKCampoRequest
        Get
            Return _ForwardInstitutionIDCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ForwardInstitutionIDCode = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoRequest
    Public Property RetrievalReferenceNumber() As BEBKCampoRequest
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _CardAcceptorTerminalID As BEBKCampoRequest
    Public Property CardAcceptorTerminalID() As BEBKCampoRequest
        Get
            Return _CardAcceptorTerminalID
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CardAcceptorTerminalID = value
        End Set
    End Property

    Private _CardAcceptorIDCode As BEBKCampoRequest
    Public Property CardAcceptorIDCode() As BEBKCampoRequest
        Get
            Return _CardAcceptorIDCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CardAcceptorIDCode = value
        End Set
    End Property

    Private _CardAcceptorNameLocation As BEBKCampoRequest
    Public Property CardAcceptorNameLocation() As BEBKCampoRequest
        Get
            Return _CardAcceptorNameLocation
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CardAcceptorNameLocation = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoRequest
    Public Property TransactionCurrencyCode() As BEBKCampoRequest
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _LongitudCampo As BEBKCampoRequest
    Public Property LongitudCampo() As BEBKCampoRequest
        Get
            Return _LongitudCampo
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _LongitudCampo = value
        End Set
    End Property

    Private _CIP As BEBKCampoRequest
    Public Property CIP() As BEBKCampoRequest
        Get
            Return _CIP
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CIP = value
        End Set
    End Property

    Private _CodigoServicio As BEBKCampoRequest
    Public Property CodigoServicio() As BEBKCampoRequest
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoServicio = value
        End Set
    End Property

    Public Sub New(ByVal Trama As String)
        MyBase.New(Trama)
        MessageTypeIdentification = New BEBKCampoRequest(Trama, 0, 4)
        PrimaryBitMap = New BEBKCampoRequest(Trama, 4, 16)
        SecondaryBitMap = New BEBKCampoRequest(Trama, 20, 16)
        PrimaryAccountNumber = New BEBKCampoRequest(Trama, 36, 19)
        ProcessingCode = New BEBKCampoRequest(Trama, 55, 6)
        AmountTransaction = New BEBKCampoRequest(Trama, 61, 12)
        Trace = New BEBKCampoRequest(Trama, 73, 6)
        TimeLocalTransaction = New BEBKCampoRequest(Trama, 79, 6)
        DateLocalTransaction = New BEBKCampoRequest(Trama, 85, 8)
        POSEntryMode = New BEBKCampoRequest(Trama, 93, 3)
        POSConditionCode = New BEBKCampoRequest(Trama, 96, 2)
        AcquirerInstitutionIDCode = New BEBKCampoRequest(Trama, 98, 8)
        ForwardInstitutionIDCode = New BEBKCampoRequest(Trama, 106, 8)
        RetrievalReferenceNumber = New BEBKCampoRequest(Trama, 114, 12)
        CardAcceptorTerminalID = New BEBKCampoRequest(Trama, 126, 8)
        CardAcceptorIDCode = New BEBKCampoRequest(Trama, 134, 15)
        CardAcceptorNameLocation = New BEBKCampoRequest(Trama, 149, 40)
        TransactionCurrencyCode = New BEBKCampoRequest(Trama, 189, 3)
        LongitudCampo = New BEBKCampoRequest(Trama, 192, 4)
        CIP = New BEBKCampoRequest(Trama, 196, 14)
        CodigoServicio = New BEBKCampoRequest(Trama, 210, 2)
    End Sub

End Class
