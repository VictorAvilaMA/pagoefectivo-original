Imports System.Runtime.Serialization
Public Interface IBEAuditoria

    Property IdUsuarioCreacion() As Integer
    Property IdUsuarioActualizacion() As Integer
    Property FechaCreacion() As DateTime
    Property FechaActualizacion() As DateTime
    Property UsuarioCreacionNombre() As String
    Property UsuarioActualizacionNombre() As String

End Interface
