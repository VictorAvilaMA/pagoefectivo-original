Imports System.Runtime.Serialization
Public Interface IBEUbigeo

    Property IdPais() As Integer
    Property IdDepartamento() As Integer
    Property IdCiudad() As Integer

End Interface
