Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEBKBaseRequest
    Implements IDisposable

    Private _Trama As String
    <DataMember()> _
    Public Property Trama() As String
        Get
            Return _Trama
        End Get
        Set(ByVal value As String)
            _Trama = value
        End Set
    End Property

    Private _IdLog As Long
    <DataMember()> _
    Public Property IdLog() As Long
        Get
            Return _IdLog
        End Get
        Set(ByVal value As Long)
            _IdLog = value
        End Set
    End Property

    Private _EsValido As Boolean
    <DataMember()> _
    Public Property EsValido() As Boolean
        Get
            Return _EsValido
        End Get
        Set(ByVal value As Boolean)
            _EsValido = value
        End Set
    End Property

    Private _IP As String
    <DataMember()> _
    Public Property IP() As String
        Get
            Return _IP
        End Get
        Set(ByVal value As String)
            _IP = value
        End Set
    End Property

    Public Sub New(ByVal Trama As String)
        _Trama = Trama
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
