Imports System.Runtime.Serialization

<DataContract()> _
Public Enum BEBKTipoDato
    <EnumMember()> Alfanumerico = 1
    <EnumMember()> Numerico = 2
    <EnumMember()> NumericoYBlanco = 3
End Enum

<DataContract()> _
<Serializable()> _
Public Class BEBKCampo
    Implements IConvertible

    Protected Friend _Longitud As Integer = Nothing
    <DataMember()> _
    Public Property Longitud() As Integer
        Get
            Return _Longitud
        End Get
        Set(ByVal value As Integer)
            _Longitud = value
        End Set
    End Property

    Protected Friend _Valor As String = Nothing
    <DataMember()> _
    Public Property Valor() As String
        Get
            Return ObtenerValor()
        End Get
        Set(ByVal value As String)
            _Valor = value
        End Set
    End Property

    Public Sub EstablecerValor(ByVal Valor As Date, ByVal Formato As String)
        _Valor = Valor.ToString(Formato)
    End Sub

    Public Sub EstablecerValor(ByVal Valor As Decimal, ByVal Formato As String, Optional ByVal QuitarPuntos As Boolean = False)
        If QuitarPuntos Then
            _Valor = Valor.ToString(Formato).Replace(".", "")
        Else
            _Valor = Valor.ToString(Formato)
        End If
    End Sub

    Public Sub EstablecerValor(ByVal Valor As Integer, ByVal Formato As String, Optional ByVal QuitarPuntos As Boolean = False)
        If QuitarPuntos Then
            _Valor = Valor.ToString.PadLeft(Longitud, "0").Replace(".", "")
        Else
            _Valor = Valor.ToString.PadLeft(Longitud, "0")
        End If
    End Sub

    Public Sub RellenarConCaracter(ByVal Caracter As String)
        _Valor = New String(Caracter, Longitud)
    End Sub

    Public Sub RellenarConCeros()
        RellenarConCaracter("0")
    End Sub

    Public Sub RellenarConEspaciosEnBlanco()
        RellenarConCaracter(" ")
    End Sub

    Public Function ObtenerValor() As String
        If _Valor Is Nothing Then
            Return New String(" ", Longitud)
        End If
        Return _Valor
    End Function

    Public Overrides Function ToString() As String
        Return ObtenerValor()
    End Function

#Region "IConvertible Members"

    Public Function GetTypeCode() As TypeCode Implements IConvertible.GetTypeCode
        Return TypeCode.Object
    End Function
    Function ToBoolean(ByVal provider As IFormatProvider) As Boolean Implements IConvertible.ToBoolean
        Throw New InvalidCastException
    End Function
    Function ToByte(ByVal provider As IFormatProvider) As Byte Implements IConvertible.ToByte
        Throw New InvalidCastException
    End Function
    Function ToChar(ByVal provider As IFormatProvider) As Char Implements IConvertible.ToChar
        Throw New InvalidCastException
    End Function
    Function ToDateTime(ByVal provider As IFormatProvider) As DateTime Implements IConvertible.ToDateTime
        Throw New InvalidCastException
    End Function
    Function ToDecimal(ByVal provider As IFormatProvider) As Decimal Implements IConvertible.ToDecimal
        Throw New InvalidCastException
    End Function
    Function ToDouble(ByVal provider As IFormatProvider) As Double Implements IConvertible.ToDouble
        Throw New InvalidCastException
    End Function
    Function ToInt16(ByVal provider As IFormatProvider) As Short Implements IConvertible.ToInt16
        Throw New InvalidCastException
    End Function
    Function ToInt32(ByVal provider As IFormatProvider) As Integer Implements IConvertible.ToInt32
        Throw New InvalidCastException
    End Function
    Function ToInt64(ByVal provider As IFormatProvider) As Long Implements IConvertible.ToInt64
        Throw New InvalidCastException
    End Function
    Function ToSByte(ByVal provider As IFormatProvider) As SByte Implements IConvertible.ToSByte
        Throw New InvalidCastException
    End Function
    Function ToSingle(ByVal provider As IFormatProvider) As Single Implements IConvertible.ToSingle
        Throw New InvalidCastException
    End Function
    Overloads Function ToString(ByVal provider As IFormatProvider) As String Implements IConvertible.ToString
        Return ObtenerValor()
    End Function
    Function ToType(ByVal conversionType As Type, ByVal provider As IFormatProvider) As Object Implements IConvertible.ToType
        Throw New InvalidCastException
    End Function
    Function ToUInt16(ByVal provider As IFormatProvider) As UInt16 Implements IConvertible.ToUInt16
        Throw New InvalidCastException
    End Function
    Function ToUInt32(ByVal provider As IFormatProvider) As UInt32 Implements IConvertible.ToUInt32
        Throw New InvalidCastException
    End Function
    Function ToUInt64(ByVal provider As IFormatProvider) As UInt64 Implements IConvertible.ToUInt64
        Throw New InvalidCastException
    End Function

#End Region

End Class
