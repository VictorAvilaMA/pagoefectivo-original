Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEBKCampoRequest
    Inherits BEBKCampo

    Private _Inicio As Integer
    <DataMember()> _
    Public Property Inicio() As Integer
        Get
            Return _Inicio
        End Get
        Set(ByVal value As Integer)
            _Inicio = value
        End Set
    End Property

    Public Sub New(ByVal Trama As String, ByVal Inicio As Integer, ByVal Longitud As Integer)
        _Inicio = Inicio
        MyBase._Longitud = Longitud
        If Trama.Length < Inicio Then
            MyBase._Valor = "".PadLeft(Longitud, "")
        Else
            If Trama.Length < Inicio + Longitud Then
                MyBase._Valor = Trama.Substring(Inicio, Trama.Length - Inicio) + "".PadLeft(Inicio + Longitud - Trama.Length, "")
            Else
                MyBase._Valor = Trama.Substring(Inicio, Longitud)
            End If
        End If
    End Sub

End Class
