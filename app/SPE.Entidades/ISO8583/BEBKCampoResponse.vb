Imports System.Runtime.Serialization
Imports System.Text.RegularExpressions

<Serializable()> _
<DataContract()> _
Public Class BEBKCampoResponse
    Inherits BEBKCampo

    Private _TipoDato As BEBKTipoDato
    <DataMember()> _
    Public Property TipoDato() As BEBKTipoDato
        Get
            Return _TipoDato
        End Get
        Set(ByVal value As BEBKTipoDato)
            _TipoDato = value
        End Set
    End Property

    Public Sub New(ByVal Longitud As Integer, ByVal TipoDato As BEBKTipoDato)
        MyBase._Longitud = Longitud
        _TipoDato = TipoDato
    End Sub

    Public Overloads Sub EstablecerValor(ByVal Valor As String, ByVal Normalizar As Boolean)
        If Normalizar Then
            Valor = Valor.Normalize(Text.NormalizationForm.FormD)
            Valor = New Regex("[^a-zA-Z0-9 ]").Replace(Valor.Trim, "")
            Valor = New Regex("[\/_| -]+").Replace(Valor, " ")
        End If
        EstablecerValor(Valor)
    End Sub

    Public Overloads Sub EstablecerValor(ByVal Valor As String)
        If Valor.Length = Longitud Then
            MyBase._Valor = Valor
        ElseIf Valor.Length < Longitud Then
            Select Case TipoDato
                Case BEBKTipoDato.Alfanumerico, BEBKTipoDato.NumericoYBlanco
                    MyBase._Valor = Valor.PadRight(Longitud, " ")
                Case BEBKTipoDato.Numerico
                    MyBase._Valor = Valor.PadLeft(Longitud, "0")
            End Select
        Else
            MyBase._Valor = Valor.Substring(0, Longitud)
        End If
    End Sub

    Public Overloads Sub EstablecerValor(ByVal request As BEBKCampoRequest)
        EstablecerValor(request.Valor)
    End Sub

End Class
