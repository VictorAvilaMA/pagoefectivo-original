Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEKasnetConsultarDetalle

    Public Sub New()

    End Sub

    Private _NroRecibo As String
    <DataMember()> _
    Public Property NroRecibo() As String
        Get
            Return _NroRecibo
        End Get
        Set(ByVal value As String)
            _NroRecibo = value
        End Set
    End Property

    Private _TotalPagar As Decimal
    <DataMember()> _
    Public Property TotalPagar() As Decimal
        Get
            Return _TotalPagar
        End Get
        Set(ByVal value As Decimal)
            _TotalPagar = value
        End Set
    End Property

    Private _MontoPagar As Decimal
    <DataMember()> _
    Public Property MontoPagar() As Decimal
        Get
            Return _MontoPagar
        End Get
        Set(ByVal value As Decimal)
            _MontoPagar = value
        End Set
    End Property

    Private _MoraPagar As Decimal
    <DataMember()> _
    Public Property MoraPagar() As Decimal
        Get
            Return _MoraPagar
        End Get
        Set(ByVal value As Decimal)
            _MoraPagar = value
        End Set
    End Property

    Private _CodMoneda As Integer
    <DataMember()> _
    Public Property CodMoneda() As Integer
        Get
            Return _CodMoneda
        End Get
        Set(ByVal value As Integer)
            _CodMoneda = value
        End Set
    End Property

    Private _MontoTipoCambio As Decimal
    <DataMember()> _
    Public Property MontoTipoCambio() As Decimal
        Get
            Return _MontoTipoCambio
        End Get
        Set(ByVal value As Decimal)
            _MontoTipoCambio = value
        End Set
    End Property

    Private _MontoConvertido As Decimal
    <DataMember()> _
    Public Property MontoConvertido() As Decimal
        Get
            Return _MontoConvertido
        End Get
        Set(ByVal value As Decimal)
            _MontoConvertido = value
        End Set
    End Property

    Private _FecEmision As String
    <DataMember()> _
    Public Property FecEmision() As String
        Get
            Return _FecEmision
        End Get
        Set(ByVal value As String)
            _FecEmision = value
        End Set
    End Property

    Private _FecVencimiento As String
    <DataMember()> _
    Public Property FecVencimiento() As String
        Get
            Return _FecVencimiento
        End Get
        Set(ByVal value As String)
            _FecVencimiento = value
        End Set
    End Property

    Private _ConceptoPago As String
    <DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property

    Private _CodConvenio As String
    <DataMember()> _
    Public Property CodConvenio() As String
        Get
            Return _CodConvenio
        End Get
        Set(ByVal value As String)
            _CodConvenio = value
        End Set
    End Property


End Class
