Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEKasnetConsultarRequest

    Public Sub New()

    End Sub

    Private _CodPuntoVenta As String
    <DataMember()> _
    Public Property CodPuntoVenta() As String
        Get
            Return _CodPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodPuntoVenta = value
        End Set
    End Property

    Private _NroTerminal As String
    <DataMember()> _
    Public Property NroTerminal() As String
        Get
            Return _NroTerminal
        End Get
        Set(ByVal value As String)
            _NroTerminal = value
        End Set
    End Property

    Private _NroOperacionConsulta As String
    <DataMember()> _
    Public Property NroOperacionConsulta() As String
        Get
            Return _NroOperacionConsulta
        End Get
        Set(ByVal value As String)
            _NroOperacionConsulta = value
        End Set
    End Property

    Private _CodOrdenPago As String
    <DataMember()> _
    Public Property CodOrdenPago() As String
        Get
            Return _CodOrdenPago
        End Get
        Set(ByVal value As String)
            _CodOrdenPago = value
        End Set
    End Property

    Private _CodConvenio As String
    <DataMember()> _
    Public Property CodConvenio() As String
        Get
            Return _CodConvenio
        End Get
        Set(ByVal value As String)
            _CodConvenio = value
        End Set
    End Property

    Private _CodCanal As Integer
    <DataMember()> _
    Public Property CodCanal() As Integer
        Get
            Return _CodCanal
        End Get
        Set(ByVal value As Integer)
            _CodCanal = value
        End Set
    End Property

    Private _CodUbigeo As String
    <DataMember()> _
    Public Property CodUbigeo() As String
        Get
            Return _CodUbigeo
        End Get
        Set(ByVal value As String)
            _CodUbigeo = value
        End Set
    End Property


    Private _FecConsulta As String
    <DataMember()> _
    Public Property FecConsulta() As String
        Get
            Return _FecConsulta
        End Get
        Set(ByVal value As String)
            _FecConsulta = value
        End Set
    End Property


End Class
