Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEKasnetPagarRequest

    Public Sub New()

    End Sub

    Private _CodPuntoVenta As String
    <DataMember()> _
    Public Property CodPuntoVenta() As String
        Get
            Return _CodPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodPuntoVenta = value
        End Set
    End Property

    Private _NroTerminal As String
    <DataMember()> _
    Public Property NroTerminal() As String
        Get
            Return _NroTerminal
        End Get
        Set(ByVal value As String)
            _NroTerminal = value
        End Set
    End Property

    Private _NroOperacionPago As String
    <DataMember()> _
    Public Property NroOperacionPago() As String
        Get
            Return _NroOperacionPago
        End Get
        Set(ByVal value As String)
            _NroOperacionPago = value
        End Set
    End Property

    Private _CodOrdenPago As String
    <DataMember()> _
    Public Property CodOrdenPago() As String
        Get
            Return _CodOrdenPago
        End Get
        Set(ByVal value As String)
            _CodOrdenPago = value
        End Set
    End Property


    Private _FecPago As String
    <DataMember()> _
    Public Property FecPago() As String
        Get
            Return _FecPago
        End Get
        Set(ByVal value As String)
            _FecPago = value
        End Set
    End Property

    Private _CodConvenio As String
    <DataMember()> _
    Public Property CodConvenio() As String
        Get
            Return _CodConvenio
        End Get
        Set(ByVal value As String)
            _CodConvenio = value
        End Set
    End Property


    Private _CodCanal As Integer
    <DataMember()> _
    Public Property CodCanal() As Integer
        Get
            Return _CodCanal
        End Get
        Set(ByVal value As Integer)
            _CodCanal = value
        End Set
    End Property


    Private _CodUbigeo As String
    <DataMember()> _
    Public Property CodUbigeo() As String
        Get
            Return _CodUbigeo
        End Get
        Set(ByVal value As String)
            _CodUbigeo = value
        End Set
    End Property

    Private _CodMedioPago As Integer
    <DataMember()> _
    Public Property CodMedioPago() As Integer
        Get
            Return _CodMedioPago
        End Get
        Set(ByVal value As Integer)
            _CodMedioPago = value
        End Set
    End Property


    Private _CantDocumentos As Integer
    <DataMember()> _
    Public Property CantDocumentos() As Integer
        Get
            Return _CantDocumentos
        End Get
        Set(ByVal value As Integer)
            _CantDocumentos = value
        End Set
    End Property

    Private _DetDocumentos As List(Of BEKasnetPagarDetalle)
    <DataMember()> _
    Public Property DetDocumentos() As List(Of BEKasnetPagarDetalle)
        Get
            Return _DetDocumentos
        End Get
        Set(ByVal value As List(Of BEKasnetPagarDetalle))
            _DetDocumentos = value
        End Set
    End Property


End Class
