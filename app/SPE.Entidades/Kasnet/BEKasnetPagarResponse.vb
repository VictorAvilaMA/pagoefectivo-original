Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEKasnetPagarResponse

    Public Sub New()

    End Sub

    Private _CodRespuesta As Integer
    <DataMember()> _
    Public Property CodRespuesta() As Integer
        Get
            Return _CodRespuesta
        End Get
        Set(ByVal value As Integer)
            _CodRespuesta = value
        End Set
    End Property

    Private _CodMensaje As String
    <DataMember()> _
    Public Property CodMensaje() As String
        Get
            Return _CodMensaje
        End Get
        Set(ByVal value As String)
            _CodMensaje = value
        End Set
    End Property

    Private _Mensaje As String
    <DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _Mensaje
        End Get
        Set(ByVal value As String)
            _Mensaje = value
        End Set
    End Property

    Private _FecRespuesta As String
    <DataMember()> _
    Public Property FecRespuesta() As String
        Get
            Return _FecRespuesta
        End Get
        Set(ByVal value As String)
            _FecRespuesta = value
        End Set
    End Property

    Private _CodOperacionEmpresa As String
    <DataMember()> _
    Public Property CodOperacionEmpresa() As String
        Get
            Return _CodOperacionEmpresa
        End Get
        Set(ByVal value As String)
            _CodOperacionEmpresa = value
        End Set
    End Property

    Private _CodPuntoVenta As String
    <DataMember()> _
    Public Property CodPuntoVenta() As String
        Get
            Return _CodPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodPuntoVenta = value
        End Set
    End Property

    Private _NroTerminal As String
    <DataMember()> _
    Public Property NroTerminal() As String
        Get
            Return _NroTerminal
        End Get
        Set(ByVal value As String)
            _NroTerminal = value
        End Set
    End Property

    Private _NroOperacionPago As String
    <DataMember()> _
    Public Property NroOperacionPago() As String
        Get
            Return _NroOperacionPago
        End Get
        Set(ByVal value As String)
            _NroOperacionPago = value
        End Set
    End Property

    Private _CodOrdenPago As String
    <DataMember()> _
    Public Property CodOrdenPago() As String
        Get
            Return _CodOrdenPago
        End Get
        Set(ByVal value As String)
            _CodOrdenPago = value
        End Set
    End Property

    Private _CantDocumentos As Integer
    <DataMember()> _
    Public Property CantDocumentos() As Integer
        Get
            Return _CantDocumentos
        End Get
        Set(ByVal value As Integer)
            _CantDocumentos = value
        End Set
    End Property

End Class
