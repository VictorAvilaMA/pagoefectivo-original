Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BELFCancelarRequest

    Private _CIP As String
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _Monto As String
    <DataMember()> _
    Public Property Monto() As String
        Get
            Return _Monto
        End Get
        Set(ByVal value As String)
            _Monto = value
        End Set
    End Property

    Private _CodMoneda As String
    <DataMember()> _
    Public Property CodMoneda() As String
        Get
            Return _CodMoneda
        End Get
        Set(ByVal value As String)
            _CodMoneda = value
        End Set
    End Property

    Private _CodServicio As String
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio
        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property

End Class
