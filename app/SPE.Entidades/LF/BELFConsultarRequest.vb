Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BELFConsultarRequest

    Private _CIP As String
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _CodServicio As String
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio
        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property

End Class
