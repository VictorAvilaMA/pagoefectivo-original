Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports System.Web
<Serializable()> <DataContract()> Public Class BEConciliacionRequest 'wjra 12/01/2010
    Inherits BEAuditoria
    Public Sub New()
    End Sub
    Private _idConciliacion As Int64
    Private _idEstado As Integer
    'Private _idTipoConciliacion As Integer
    'Private _IdOrigenConciliacion As Integer
    Private _DescTipoConciliacion As String
    Private _DescOrigenConciliacion As String
    Private _DescEstado As String
    Private _fechaConciliacion As DateTime
    Private _fechaInicio As DateTime
    Private _fechaFin As DateTime
    Private _idUsuarioCreacion As Integer
    Private _envioNotificacion As Boolean
    Private _fechaNotificacion As Nullable(Of DateTime)
    Private _operacionBancaria As String
    Private _numeroOrdenPago As String
    Private _idUsuarioActualizacionC As Integer
    Private _fechaActualizacionC As DateTime
    Private _observacion As String
    Private _listaConciliacionesEntidad As List(Of BEConciliacionEntidad)
    Private _flagConciliacionPendiente As Integer
    'Private _listaOperacionesConciliadas As List(Of BEOperacionBancaria)
    'Private _listaOperacionesNoConciliadas As List(Of BEOperacionBancaria)
    Private _listaOperacionesArchivo As List(Of String)
    'Private _ListaOperacionesArchivoBytes As Byte()
    Private _nombreArchivoCierre As String
    Public Sub New(ByVal reader As IDataReader)
        IdConciliacion = Convert.ToInt64(reader("IdConciliacion"))
        IdEstado = Convert.ToInt32(reader("IdEstado"))
        'IdTipoConciliacion = Convert.ToInt32(reader("IdTipoConciliacion"))
        'IdOrigenConciliacion = Convert.ToInt32(reader("IdOrigenConciliacion"))
        DescTipoConciliacion = reader("DescTipoConciliacion")
        DescOrigenConciliacion = reader("DescOrigenConciliacion")
        DescEstado = reader("DescTipoConciliacion")
        FechaConciliacion = Convert.ToDateTime("FechaConciliacion")
        FechaInicio = DataUtil.ObjectToDateTime("FechaInicio")
        FechaFin = DataUtil.ObjectToDateTime(reader("FechaFin"))
        EnvioNotificacion = Convert.ToBoolean("EnvioNotificacion")
        FechaNotificacion = DataUtil.ObjectToDateTime("FechaNotificacion")
        Observacion = reader("Observacion")
    End Sub
	<DataMember()> _
    Public Property IdConciliacion() As Int64
        Get
            Return _idConciliacion
        End Get
        Set(ByVal value As Int64)
            _idConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
    'Public Property IdTipoConciliacion() As Integer
    '    Get
    '        Return _idTipoConciliacion
    '    End Get
    '    Set(ByVal value As Integer)
    '        _idTipoConciliacion = value
    '    End Set
    'End Property
    'Public Property IdOrigenConciliacion() As Integer
    '    Get
    '        Return _IdOrigenConciliacion
    '    End Get
    '    Set(ByVal value As Integer)
    '        _IdOrigenConciliacion = value
    '    End Set
    'End Property
	<DataMember()> _
    Public Property DescTipoConciliacion() As String
        Get
            Return _DescTipoConciliacion
        End Get
        Set(ByVal value As String)
            _DescTipoConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescOrigenConciliacion() As String
        Get
            Return _DescOrigenConciliacion
        End Get
        Set(ByVal value As String)
            _DescOrigenConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescEstado() As String
        Get
            Return _DescEstado
        End Get
        Set(ByVal value As String)
            _DescEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaConciliacion() As DateTime
        Get
            Return _fechaConciliacion
        End Get
        Set(ByVal value As DateTime)
            _fechaConciliacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As DateTime)
            _fechaFin = value
        End Set
    End Property
	<DataMember()> _
    Public Property EnvioNotificacion() As Boolean
        Get
            Return _envioNotificacion
        End Get
        Set(ByVal value As Boolean)
            _envioNotificacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaNotificacion() As Nullable(Of DateTime)
        Get
            Return _fechaNotificacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _fechaNotificacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property OperacionBancaria() As String
        Get
            Return _operacionBancaria
        End Get
        Set(ByVal value As String)
            _operacionBancaria = value
        End Set
    End Property
	<DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsuarioActualizacionC() As Integer
        Get
            Return _idUsuarioActualizacionC
        End Get
        Set(ByVal value As Integer)
            _idUsuarioActualizacionC = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaActualizacionC() As DateTime
        Get
            Return _fechaActualizacionC
        End Get
        Set(ByVal value As DateTime)
            _fechaActualizacionC = value
        End Set
    End Property
	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _observacion
        End Get
        Set(ByVal value As String)
            _observacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesArchivo() As List(Of String)
        Get
            Return _listaOperacionesArchivo
        End Get
        Set(ByVal value As List(Of String))
            _listaOperacionesArchivo = value
        End Set
    End Property
    'Public Property ListaOperacionesArchivoBytes() As Byte() 'System.IO.StreamReader 'HttpPostedFile 'Byte()
    '    Get
    '        Return _ListaOperacionesArchivoBytes
    '    End Get
    '    Set(ByVal value As Byte()) 'HttpPostedFile) 'Byte())
    '        _ListaOperacionesArchivoBytes = value
    '    End Set
    'End Property
	<DataMember()> _
    Public Property ListaConciliacionesEntidad() As List(Of BEConciliacionEntidad)
        Get
            Return _listaConciliacionesEntidad
        End Get
        Set(ByVal value As List(Of BEConciliacionEntidad))
            _listaConciliacionesEntidad = value
        End Set
    End Property
	<DataMember()> _
    Public Property FlagConciliacionPendiente() As Integer
        Get
            Return _flagConciliacionPendiente
        End Get
        Set(ByVal value As Integer)
            _flagConciliacionPendiente = value
        End Set
    End Property
	<DataMember()> _
    Public Property NombreArchivoCierre() As String
        Get
            Return _nombreArchivoCierre
        End Get
        Set(ByVal value As String)
            _nombreArchivoCierre = value
        End Set
    End Property
    'Public Property ListaOperacionesConciliadas() As List(Of BEOperacionBancaria)
    '    Get
    '        Return _listaOperacionesConciliadas
    '    End Get
    '    Set(ByVal value As List(Of BEOperacionBancaria))
    '        _listaOperacionesConciliadas = value
    '    End Set
    'End Property
    'Public Property ListaOperacionesNoConciliadas() As List(Of BEOperacionBancaria)
    '    Get
    '        Return _listaOperacionesNoConciliadas
    '    End Get
    '    Set(ByVal value As List(Of BEOperacionBancaria))
    '        _listaOperacionesNoConciliadas = value
    '    End Set
    'End Property
End Class
