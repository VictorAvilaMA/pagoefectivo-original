Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEConciliacionResponse
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Private _idConciliacion As String
    Private _idEstado As Integer
    Private _Fecha As DateTime
    Private _fechaInicio As DateTime
    Private _fechaFin As DateTime
    Private _IdUsuarioCreacion As Integer
    Private _EnvioNotificacion As Boolean
    Private _FechaEnvioNotificacion As Nullable(Of DateTime)
    Private _Observacion As String
    Private _DescEstado As String
    Private _DescTipoConciliacion As String
    Private _DescOrigenConciliacion As String
    Private _IdUsuarioActualizacion As Nullable(Of Integer)
    Private _FechaActualizacion As Nullable(Of DateTime)
    Private _listaOperacionesConciliadas As List(Of BEConciliacionDetalle)
    Private _listaOperacionesNoConciliadas As List(Of BEConciliacionDetalle)
    Private _CantCIPS As Integer
    Private _CantCIPSConciliados As Integer

    Public Sub New()
    End Sub
    Public Sub New(ByVal reader As IDataReader, ByVal filtro As String)
        Select Case filtro
            Case "UltimasConciliaciones"
                IdConciliacion = Convert.ToInt32(reader("IdConciliacion"))
                IdEstado = Convert.ToInt32(reader("IdEstado"))
                Fecha = DataUtil.ObjectToDateTime(reader("Fecha"))
                FechaInicio = DataUtil.ObjectToDateTime(reader("FechaInicio"))
                FechaFin = DataUtil.ObjectToDateTime(reader("FechaFin"))
                IdUsuarioCreacion = Convert.ToInt32(reader("IdUsuarioCreacion"))
                EnvioNotificacion = Convert.ToBoolean(reader("EnvioNotificacion"))
                FechaEnvioNotificacion = DataUtil.ObjectToDateTime(reader("FechaEnvioNotificacion"))
                Observacion = reader("Observacion")
                DescEstado = reader("DescEstado")
                IdUsuarioActualizacion = DataUtil.ObjectToInt32Null(reader("IdUsuarioActualizacion"))
                FechaActualizacion = DataUtil.ObjectToDateTime(reader("FechaActualizacion"))
                CantCIPS = Convert.ToInt32(reader("CantCIPS"))
                CantCIPSConciliados = Convert.ToInt32(reader("CantCIPSConciliados"))
        End Select
    End Sub

	<DataMember()> _
    Public Property IdConciliacion() As Integer
        Get
            Return _idConciliacion
        End Get
        Set(ByVal value As Integer)
            _idConciliacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdEstado() As Integer
        Get
            Return _idEstado
        End Get
        Set(ByVal value As Integer)
            _idEstado = value
        End Set
    End Property
    <DataMember()> _
    Public Property Fecha() As Nullable(Of DateTime)
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _Fecha = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaInicio() As DateTime
        Get
            Return _fechaInicio
        End Get
        Set(ByVal value As DateTime)
            _fechaInicio = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaFin() As DateTime
        Get
            Return _fechaFin
        End Get
        Set(ByVal value As DateTime)
            _fechaFin = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsuarioCreacion() As Integer
        Get
            Return _IdUsuarioCreacion
        End Get
        Set(ByVal value As Integer)
            _IdUsuarioCreacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property EnvioNotificacion() As Boolean
        Get
            Return _EnvioNotificacion
        End Get
        Set(ByVal value As Boolean)
            _EnvioNotificacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaEnvioNotificacion() As Nullable(Of DateTime)
        Get
            Return _FechaEnvioNotificacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaEnvioNotificacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property DescEstado() As String
        Get
            Return _DescEstado
        End Get
        Set(ByVal value As String)
            _DescEstado = value
        End Set
    End Property
	<DataMember()> _
    Public Property IdUsuarioActualizacion() As Nullable(Of Integer)
        Get
            Return _IdUsuarioActualizacion
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _IdUsuarioActualizacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaActualizacion() As Nullable(Of DateTime)
        Get
            Return _FechaActualizacion
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _FechaActualizacion = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesConciliadas() As List(Of BEConciliacionDetalle)
        Get
            Return _listaOperacionesConciliadas
        End Get
        Set(ByVal value As List(Of BEConciliacionDetalle))
            _listaOperacionesConciliadas = value
        End Set
    End Property
	<DataMember()> _
    Public Property ListaOperacionesNoConciliadas() As List(Of BEConciliacionDetalle)
        Get
            Return _listaOperacionesNoConciliadas
        End Get
        Set(ByVal value As List(Of BEConciliacionDetalle))
            _listaOperacionesNoConciliadas = value
        End Set
    End Property
	<DataMember()> _
    Public Property CantCIPS() As Integer
        Get
            Return _CantCIPS
        End Get
        Set(ByVal value As Integer)
            _CantCIPS = value
        End Set
    End Property

	<DataMember()> _
    Public Property CantCIPSConciliados() As Integer
        Get
            Return _CantCIPSConciliados
        End Get
        Set(ByVal value As Integer)
            _CantCIPSConciliados = value
        End Set
    End Property

End Class
