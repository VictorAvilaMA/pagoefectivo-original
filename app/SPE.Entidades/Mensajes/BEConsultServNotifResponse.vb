Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEConsultServNotifResponse
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Public Sub New()

    End Sub


    Private _servNotificacionPendiente As List(Of BEServicioNotificacion) = Nothing
	<DataMember()> _
    Public Property ServNotificacionPendiente() As List(Of BEServicioNotificacion)
        Get
            Return _servNotificacionPendiente
        End Get
        Set(ByVal value As List(Of BEServicioNotificacion))
            _servNotificacionPendiente = value
        End Set
    End Property

    Private _servNotificacionErroneas As List(Of BEServicioNotificacion) = Nothing
	<DataMember()> _
    Public Property ServNotificacionErroneas() As List(Of BEServicioNotificacion)
        Get
            Return _servNotificacionErroneas
        End Get
        Set(ByVal value As List(Of BEServicioNotificacion))
            _servNotificacionErroneas = value
        End Set
    End Property


End Class
