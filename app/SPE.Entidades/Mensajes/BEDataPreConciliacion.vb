﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports System.Web

<Serializable()> <DataContract()> Public Class BEDataPreConciliacion

    Private _Campo0 As String
    <DataMember()> _
    Public Property Campo0() As String
        Get
            Return _Campo0
        End Get
        Set(ByVal value As String)
            _Campo0 = value
        End Set
    End Property

    Private _Campo1 As String
    <DataMember()> _
    Public Property Campo1() As String
        Get
            Return _Campo1
        End Get
        Set(ByVal value As String)
            _Campo1 = value
        End Set
    End Property

    Private _Campo2 As String
    <DataMember()> _
    Public Property Campo2() As String
        Get
            Return _Campo2
        End Get
        Set(ByVal value As String)
            _Campo2 = value
        End Set
    End Property

    Private _Campo3 As String
    <DataMember()> _
    Public Property Campo3() As String
        Get
            Return _Campo3
        End Get
        Set(ByVal value As String)
            _Campo3 = value
        End Set
    End Property

    Private _Campo4 As String
    <DataMember()> _
    Public Property Campo4() As String
        Get
            Return _Campo4
        End Get
        Set(ByVal value As String)
            _Campo4 = value
        End Set
    End Property

    Private _Campo5 As String
    <DataMember()> _
    Public Property Campo5() As String
        Get
            Return _Campo5
        End Get
        Set(ByVal value As String)
            _Campo5 = value
        End Set
    End Property

    Private _Campo6 As String
    <DataMember()> _
    Public Property Campo6() As String
        Get
            Return _Campo6
        End Get
        Set(ByVal value As String)
            _Campo6 = value
        End Set
    End Property

End Class
