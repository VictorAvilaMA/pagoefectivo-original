Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEGenCIPRequest
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Public Sub New()

    End Sub

    Private _obeOrdenPago As BEOrdenPago
	<DataMember()> _
    Public Property OrdenPago() As BEOrdenPago
        Get
            Return _obeOrdenPago
        End Get
        Set(ByVal value As BEOrdenPago)
            _obeOrdenPago = value
        End Set
    End Property


End Class
