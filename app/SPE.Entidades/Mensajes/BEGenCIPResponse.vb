Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEGenCIPResponse
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Private _numeroOrdenPago As String
	<DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _numeroOrdenPago
        End Get
        Set(ByVal value As String)
            _numeroOrdenPago = value
        End Set
    End Property

    Private _idOrdenPago As Int64
	<DataMember()> _
    Public Property IdOrdenPago() As Int64
        Get
            Return _idOrdenPago
        End Get
        Set(ByVal value As Int64)
            _idOrdenPago = value
        End Set
    End Property


    Private _fechaEmision As DateTime
	<DataMember()> _
    Public Property FechaEmision() As DateTime
        Get
            Return _fechaEmision
        End Get
        Set(ByVal value As DateTime)
            _fechaEmision = value
        End Set
    End Property

    Private _tiempoExpiracion As Decimal
	<DataMember()> _
    Public Property TiempoExpiracion() As Decimal
        Get
            Return _tiempoExpiracion
        End Get
        Set(ByVal value As Decimal)
            _tiempoExpiracion = value
        End Set
    End Property




End Class
