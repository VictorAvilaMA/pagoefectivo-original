Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEGenPasResponse
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Private _estado As String
    Private _mensaje As String
    Private _oPasarela As BEPasarela

    Public Sub New()

    End Sub

	<DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

	<DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

	<DataMember()> _
    Public Property Pasarela() As BEPasarela
        Get
            Return _oPasarela
        End Get
        Set(ByVal value As BEPasarela)
            _oPasarela = value
        End Set
    End Property

End Class
