Imports System.Runtime.Serialization


<Serializable()> <DataContract()> Public Class BEGenPlantillaHtmlRequest
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal pIdServicio As Integer, ByVal pcodPlantillaTipo As String, ByVal pvaloresDinamicos As IDictionary(Of String, String), ByVal pcodPlantillaTipoVariacion As String)
        _idServicio = pIdServicio
        _codPlantillaTipo = pcodPlantillaTipo
        _valoresDinamicos = pvaloresDinamicos
        _codPlantillaTipoVariacion = pcodPlantillaTipoVariacion
    End Sub

    Private _idServicio As Integer = Nothing
	<DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    Private _codPlantillaTipo As String = Nothing
	<DataMember()> _
    Public Property CodPlantillaTipo() As String
        Get
            Return _codPlantillaTipo
        End Get
        Set(ByVal value As String)
            _codPlantillaTipo = value
        End Set
    End Property

    Private _valoresDinamicos As IDictionary(Of String, String) = Nothing
	<DataMember()> _
    Public Property ValoresDinamicos() As IDictionary(Of String, String)
        Get
            Return _valoresDinamicos
        End Get
        Set(ByVal value As IDictionary(Of String, String))
            _valoresDinamicos = value
        End Set
    End Property
    Private _codPlantillaTipoVariacion As String = Nothing
    <DataMember()> _
    Public Property CodPlantillaTipoVariacion() As String
        Get
            Return _codPlantillaTipoVariacion
        End Get
        Set(ByVal value As String)
            _codPlantillaTipoVariacion = value
        End Set
    End Property
End Class
