Imports System.Runtime.Serialization
<Serializable()> <DataContract()> Public Class BEGenPlantillaHtmlResponse
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal pPlantilla As BEPlantilla, ByVal pParamValores As IDictionary(Of String, String), ByVal pHtml As String)
        _Plantilla = pPlantilla
        _ParamValores = pParamValores
        _Html = pHtml
    End Sub

    Private _Plantilla As BEPlantilla = Nothing
	<DataMember()> _
    Public Property Plantilla() As BEPlantilla
        Get
            Return _Plantilla
        End Get
        Set(ByVal value As BEPlantilla)
            _Plantilla = value
        End Set
    End Property

    Private _ParamValores As IDictionary(Of String, String) = Nothing
	<DataMember()> _
    Public Property ParamValores() As IDictionary(Of String, String)
        Get
            Return _ParamValores
        End Get
        Set(ByVal value As IDictionary(Of String, String))
            _ParamValores = value
        End Set
    End Property

    Private _Html As String = Nothing
	<DataMember()> _
    Public Property Html() As String
        Get
            Return _Html
        End Get
        Set(ByVal value As String)
            _Html = value
        End Set
    End Property


End Class
