Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEObtCIPInfoByUrLServRequest
    Inherits _3Dev.FW.Entidades.BusinessMessageBase

    Public Sub New()

    End Sub

    Private _strUrlServicio As String
	<DataMember()> _
    Public Property UrlServicio() As String
        Get
            Return _strUrlServicio
        End Get
        Set(ByVal value As String)
            _strUrlServicio = value
        End Set
    End Property

    Private _trama As String
	<DataMember()> _
    Public Property Trama() As String
        Get
            Return _trama
        End Get
        Set(ByVal value As String)
            _trama = value
        End Set
    End Property

    Private _cargarTramaSolicitud As Boolean
	<DataMember()> _
    Public Property CargarTramaSolicitud() As Boolean
        Get
            Return _cargarTramaSolicitud
        End Get
        Set(ByVal value As Boolean)
            _cargarTramaSolicitud = value
        End Set
    End Property

    Private _tipoTrama As Integer
	<DataMember()> _
    Public Property TipoTrama() As Integer
        Get
            Return _tipoTrama
        End Get
        Set(ByVal value As Integer)
            _tipoTrama = value
        End Set
    End Property

End Class
