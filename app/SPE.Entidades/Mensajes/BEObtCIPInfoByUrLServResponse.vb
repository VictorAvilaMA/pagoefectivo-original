Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BEObtCIPInfoByUrLServResponse
    Inherits _3Dev.FW.Entidades.BusinessMessageBase


    Public Sub New()

    End Sub

    Private _obeServicio As BEServicio
	<DataMember()> _
    Public Property Servicio() As BEServicio
        Get
            Return _obeServicio
        End Get
        Set(ByVal value As BEServicio)
            _obeServicio = value
        End Set
    End Property

    Private _obeOcultarEmpresa As BEOcultarEmpresa
	<DataMember()> _
    Public Property OcultarEmpresa() As BEOcultarEmpresa
        Get
            Return _obeOcultarEmpresa
        End Get
        Set(ByVal value As BEOcultarEmpresa)
            _obeOcultarEmpresa = value
        End Set
    End Property

    Private _obeOrdenPago As BEOrdenPago
	<DataMember()> _
    Public Property OrdenPago() As BEOrdenPago
        Get
            Return _obeOrdenPago
        End Get
        Set(ByVal value As BEOrdenPago)
            _obeOrdenPago = value
        End Set
    End Property

    Private _esquemaTrama As EsquemaTrama
	<DataMember()> _
    Public Property EsquemaTrama() As EsquemaTrama
        Get
            Return _esquemaTrama
        End Get
        Set(ByVal value As EsquemaTrama)
            _esquemaTrama = value
        End Set
    End Property

    'Private _estado As Boolean
    'Public Property Estado() As String
    '    Get
    '        Return _estado
    '    End Get
    '    Set(ByVal value As String)
    '        _estado = value
    '    End Set
    'End Property

    'Private _mensaje As String
    'Public Property Mensaje() As String
    '    Get
    '        Return _mensaje
    '    End Get
    '    Set(ByVal value As String)
    '        _mensaje = value
    '    End Set
    'End Property

    Private _obeMoneda As BEMoneda
	<DataMember()> _
    Public Property Moneda() As BEMoneda
        Get
            Return _obeMoneda
        End Get
        Set(ByVal value As BEMoneda)
            _obeMoneda = value
        End Set
    End Property



End Class
