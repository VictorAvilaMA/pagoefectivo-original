﻿Imports System.Runtime.Serialization
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Util
Imports System.Web

<Serializable()> <DataContract()> Public Class BEPreConciliacionRequest
    Inherits BEAuditoria

    Public Sub New()
    End Sub

    Private _bytes As Byte()
    <DataMember()> _
    Public Property Bytes() As Byte()
        Get
            Return _bytes
        End Get
        Set(ByVal value As Byte())
            _bytes = value
        End Set
    End Property

    Private _tipoArchivo As String
    <DataMember()> _
    Public Property TipoArchivo() As String
        Get
            Return _tipoArchivo
        End Get
        Set(ByVal value As String)
            _tipoArchivo = value
        End Set
    End Property

    Private _nombreArchivoPreCon As String
    <DataMember()> _
    Public Property NombreArchivoPreCon() As String
        Get
            Return _nombreArchivoPreCon
        End Get
        Set(ByVal value As String)
            _nombreArchivoPreCon = value
        End Set
    End Property

    Private _listaOperacionesConciliadas As List(Of BEConciliacionDetalle)
    <DataMember()> _
    Public Property ListaOperacionesConciliadas() As List(Of BEConciliacionDetalle)
        Get
            Return _listaOperacionesConciliadas
        End Get
        Set(ByVal value As List(Of BEConciliacionDetalle))
            _listaOperacionesConciliadas = value
        End Set
    End Property

    Private _listaOperacionesNoConciliadas As List(Of BEConciliacionDetalle)
    <DataMember()> _
    Public Property ListaOperacionesNoConciliadas() As List(Of BEConciliacionDetalle)
        Get
            Return _listaOperacionesNoConciliadas
        End Get
        Set(ByVal value As List(Of BEConciliacionDetalle))
            _listaOperacionesNoConciliadas = value
        End Set
    End Property

    Private _codigoBanco As String
    <DataMember()> _
    Public Property CodigoBanco() As String
        Get
            Return _codigoBanco
        End Get
        Set(ByVal value As String)
            _codigoBanco = value
        End Set
    End Property

    Private _archivo As BEConciliacionArchivo
    <DataMember()> _
    Public Property Archivo() As BEConciliacionArchivo
        Get
            Return _archivo
        End Get
        Set(ByVal value As BEConciliacionArchivo)
            _archivo = value
        End Set
    End Property

    Private _cantidadCodPago As Int32
    <DataMember()> _
    Public Property CantidadCodPago() As Int32
        Get
            Return _cantidadCodPago
        End Get
        Set(ByVal value As Int32)
            _cantidadCodPago = value
        End Set
    End Property

    Private _idMoneda As Int32
    <DataMember()> _
    Public Property IdMoneda() As Int32
        Get
            Return _idMoneda
        End Get
        Set(ByVal value As Int32)
            _idMoneda = value
        End Set
    End Property

End Class
