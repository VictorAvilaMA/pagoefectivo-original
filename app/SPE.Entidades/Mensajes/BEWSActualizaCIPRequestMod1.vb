﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEWSActualizaCIPRequestMod1


    Private _codServ As String
    <DataMember()> _
    Public Property CodServ() As String
        Get
            Return _codServ
        End Get
        Set(ByVal value As String)
            _codServ = value
        End Set
    End Property

    Private _Firma As String
    <DataMember()> _
    Public Property Firma() As String
        Get
            Return _Firma
        End Get
        Set(ByVal value As String)
            _Firma = value
        End Set
    End Property

    Private _cip As String
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _cip
        End Get
        Set(ByVal value As String)
            _cip = value
        End Set
    End Property

    Private _FechaExpira As DateTime
    <DataMember()> _
    Public Property FechaExpira() As DateTime
        Get
            Return _FechaExpira
        End Get
        Set(ByVal value As DateTime)
            _FechaExpira = value
        End Set
    End Property

    Private _infoRequest As String
    <DataMember()> _
    Public Property InfoRequest() As String
        Get
            Return _infoRequest
        End Get
        Set(ByVal value As String)
            _infoRequest = value
        End Set
    End Property

End Class
