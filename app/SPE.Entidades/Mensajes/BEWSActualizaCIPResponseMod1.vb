﻿Imports System.Runtime.Serialization

Public Class BEWSActualizaCIPResponseMod1

    Public Sub New()

    End Sub

    Private _estado As String
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Private _mensaje As String
    <DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    Private _infoResponse As String
    <DataMember()> _
    Public Property InfoResponse() As String
        Get
            Return _infoResponse
        End Get
        Set(ByVal value As String)
            _infoResponse = value
        End Set
    End Property
End Class
