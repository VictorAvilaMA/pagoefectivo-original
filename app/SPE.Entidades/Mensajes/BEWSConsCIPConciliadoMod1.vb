﻿Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEWSConsCIPConciliadoMod1
    Inherits BEWSConsCIPMod1

    Private _FechaConciliacion As String
    Private _EstadoConciliacion As String
    <DataMember()> _
    Public Property FechaConciliacion() As String
        Get
            Return _FechaConciliacion
        End Get
        Set(ByVal value As String)
            _FechaConciliacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property EstadoConciliacion() As String
        Get
            Return _EstadoConciliacion
        End Get
        Set(ByVal value As String)
            _EstadoConciliacion = value
        End Set
    End Property

    Public Sub New()
        MyBase.New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        MyBase.New(reader)
        FechaConciliacion = Convert.ToString(reader("FechaConciliacion"))
        EstadoConciliacion = Convert.ToString(reader("EstadoConciliacion"))
    End Sub

End Class
