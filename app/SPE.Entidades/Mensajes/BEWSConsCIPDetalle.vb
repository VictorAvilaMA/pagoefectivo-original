Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEWSConsCIPDetalle

    Private _IdDetalleOrdenPago As String
    Private _ConceptoPago As String
    Private _Importe As String
    Private _Tipo_Origen As String
    Private _Cod_Origen As String
    Private _Campo1 As String
    Private _Campo2 As String
    Private _Campo3 As String

	<DataMember()> _
    Public Property IdDetalleOrdenPago() As String
        Get
            Return _IdDetalleOrdenPago
        End Get
        Set(ByVal value As String)
            _IdDetalleOrdenPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property ConceptoPago() As String
        Get
            Return _ConceptoPago
        End Get
        Set(ByVal value As String)
            _ConceptoPago = value
        End Set
    End Property
	<DataMember()> _
    Public Property Importe() As String
        Get
            Return _Importe
        End Get
        Set(ByVal value As String)
            _Importe = value
        End Set
    End Property
	<DataMember()> _
    Public Property Tipo_Origen() As String
        Get
            Return _Tipo_Origen
        End Get
        Set(ByVal value As String)
            _Tipo_Origen = value
        End Set
    End Property
	<DataMember()> _
    Public Property Cod_Origen() As String
        Get
            Return _Cod_Origen
        End Get
        Set(ByVal value As String)
            _Cod_Origen = value
        End Set
    End Property
	<DataMember()> _
    Public Property Campo1() As String
        Get
            Return _Campo1
        End Get
        Set(ByVal value As String)
            _Campo1 = value
        End Set
    End Property
	<DataMember()> _
    Public Property Campo2() As String
        Get
            Return _Campo2
        End Get
        Set(ByVal value As String)
            _Campo2 = value
        End Set
    End Property
	<DataMember()> _
    Public Property Campo3() As String
        Get
            Return _Campo3
        End Get
        Set(ByVal value As String)
            _Campo3 = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        IdDetalleOrdenPago = Convert.ToString(reader("IdDetalleOrdenPago"))
        ConceptoPago = Convert.ToString(reader("ConceptoPago"))
        Importe = Convert.ToString(reader("Importe"))
        Tipo_Origen = Convert.ToString(reader("Tipo_Origen"))
        Cod_Origen = Convert.ToString(reader("Cod_Origen"))
        Campo1 = Convert.ToString(reader("Campo1"))
        Campo2 = Convert.ToString(reader("Campo2"))
        Campo3 = Convert.ToString(reader("Campo3"))
    End Sub

End Class