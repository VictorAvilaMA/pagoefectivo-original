Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEWSConsCIPsConciliadosRequest

    Private _cAPI As String
    Private _cClave As String
    Private _fechaDesde As String
    Private _fechaHasta As String
    Private _tipo As String
    Private _infoRequest As String

	<DataMember()> _
    Public Property CAPI() As String
        Get
            Return _cAPI
        End Get
        Set(ByVal value As String)
            _cAPI = value
        End Set
    End Property
	<DataMember()> _
    Public Property CClave() As String
        Get
            Return _cClave
        End Get
        Set(ByVal value As String)
            _cClave = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaDesde() As String
        Get
            Return _fechaDesde
        End Get
        Set(ByVal value As String)
            _fechaDesde = value
        End Set
    End Property
	<DataMember()> _
    Public Property FechaHasta() As String
        Get
            Return _fechaHasta
        End Get
        Set(ByVal value As String)
            _fechaHasta = value
        End Set
    End Property
	<DataMember()> _
    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property
	<DataMember()> _
    Public Property InfoRequest() As String
        Get
            Return _infoRequest
        End Get
        Set(ByVal value As String)
            _infoRequest = value
        End Set
    End Property

    Public Sub New()

    End Sub

End Class