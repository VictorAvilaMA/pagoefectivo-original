Imports System.Runtime.Serialization
<Serializable()> <DataContract()> _
Public Class BEWSConsCIPsConciliadosResponse

    Private _estado As String
    Private _mensaje As String
    Private _cips As List(Of BEWSConsCIPConciliado)
    Private _infoResponse As String

	<DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property
	<DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property
	<DataMember()> _
    Public Property CIPs() As List(Of BEWSConsCIPConciliado)
        Get
            Return _cips
        End Get
        Set(ByVal value As List(Of BEWSConsCIPConciliado))
            _cips = value
        End Set
    End Property
	<DataMember()> _
    Public Property InfoResponse() As String
        Get
            Return _infoResponse
        End Get
        Set(ByVal value As String)
            _infoResponse = value
        End Set
    End Property

    Public Sub New()

    End Sub

End Class