﻿
Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BEWSConsCIPv2

    Private _IdOrdenPago As String
    Private _IdCliente As String
    Private _NumeroOrdenPago As String
    Private _OrderIdComercio As String
    Private _IdMoneda As String
    Private _Total As String
    Private _FechaEmision As String
    Private _FechaCancelado As String
    Private _FechaConciliado As String
    <NonSerialized()> _
    Private _IdEstadoConciliado As String
    Private _EstadoConciliado As String
    Private _UsuarioID As String
    Private _UsuarioNombre As String
    Private _UsuarioApellidos As String
    Private _UsuarioAlias As String
    Private _UsuarioEmail As String
    Private _DataAdicional As String
    <NonSerialized()> _
    Private _IdEstado As String
    Private _Estado As String
    Private _ArchivoConciliacion As String
    Private _Banco As String
    Private _Detalle As List(Of BEWSConsCIPDetalle)

    <DataMember()> _
    Public Property IdOrdenPago() As String
        Get
            Return _IdOrdenPago
        End Get
        Set(ByVal value As String)
            _IdOrdenPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdCliente() As String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As String)
            _IdCliente = value
        End Set
    End Property
    <DataMember()> _
    Public Property NumeroOrdenPago() As String
        Get
            Return _NumeroOrdenPago
        End Get
        Set(ByVal value As String)
            _NumeroOrdenPago = value
        End Set
    End Property
    <DataMember()> _
    Public Property OrderIdComercio() As String
        Get
            Return _OrderIdComercio
        End Get
        Set(ByVal value As String)
            _OrderIdComercio = value
        End Set
    End Property
    <DataMember()> _
    Public Property IdMoneda() As String
        Get
            Return _IdMoneda
        End Get
        Set(ByVal value As String)
            _IdMoneda = value
        End Set
    End Property
    <DataMember()> _
    Public Property Total() As String
        Get
            Return _Total
        End Get
        Set(ByVal value As String)
            _Total = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaEmision() As String
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As String)
            _FechaEmision = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaCancelado() As String
        Get
            Return _FechaCancelado
        End Get
        Set(ByVal value As String)
            _FechaCancelado = value
        End Set
    End Property
    <DataMember()> _
    Public Property FechaConciliado() As String
        Get
            Return _FechaConciliado
        End Get
        Set(ByVal value As String)
            _FechaConciliado = value
        End Set
    End Property
    <IgnoreDataMember()> _
    Public Property IdEstadoConciliado() As String
        Get
            Return _IdEstadoConciliado
        End Get
        Set(ByVal value As String)
            _IdEstadoConciliado = value
        End Set
    End Property
    <DataMember()> _
    Public Property EstadoConciliado() As String
        Get
            Return _EstadoConciliado
        End Get
        Set(ByVal value As String)
            _EstadoConciliado = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioID() As String
        Get
            Return _UsuarioID
        End Get
        Set(ByVal value As String)
            _UsuarioID = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioNombre() As String
        Get
            Return _UsuarioNombre
        End Get
        Set(ByVal value As String)
            _UsuarioNombre = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioApellidos() As String
        Get
            Return _UsuarioApellidos
        End Get
        Set(ByVal value As String)
            _UsuarioApellidos = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioAlias() As String
        Get
            Return _UsuarioAlias
        End Get
        Set(ByVal value As String)
            _UsuarioAlias = value
        End Set
    End Property
    <DataMember()> _
    Public Property UsuarioEmail() As String
        Get
            Return _UsuarioEmail
        End Get
        Set(ByVal value As String)
            _UsuarioEmail = value
        End Set
    End Property
    <DataMember()> _
    Public Property DataAdicional() As String
        Get
            Return _DataAdicional
        End Get
        Set(ByVal value As String)
            _DataAdicional = value
        End Set
    End Property
    <IgnoreDataMember()> _
    Public Property IdEstado() As String
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As String)
            _IdEstado = value
        End Set
    End Property
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property
    <DataMember()> _
    Public Property ArchivoConciliacion() As String
        Get
            Return _ArchivoConciliacion
        End Get
        Set(ByVal value As String)
            _ArchivoConciliacion = value
        End Set
    End Property
    <DataMember()> _
    Public Property Banco() As String
        Get
            Return _Banco
        End Get
        Set(ByVal value As String)
            _Banco = value
        End Set
    End Property
    <DataMember()> _
    Public Property Detalle() As List(Of BEWSConsCIPDetalle)
        Get
            Return _Detalle
        End Get
        Set(ByVal value As List(Of BEWSConsCIPDetalle))
            _Detalle = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal reader As IDataReader)
        IdOrdenPago = Convert.ToString(reader("IdOrdenPago"))
        IdCliente = Convert.ToString(reader("IdCliente"))
        NumeroOrdenPago = Convert.ToString(reader("NumeroOrdenPago"))
        OrderIdComercio = Convert.ToString(reader("OrderIdComercio"))
        IdMoneda = Convert.ToString(reader("IdMoneda"))
        Total = Convert.ToString(reader("Total"))
        FechaEmision = Convert.ToString(reader("FechaEmision"))
        FechaCancelado = DataUtil.ObjectToString(reader("FechaCancelacion"))
        IdEstadoConciliado = Convert.ToString(reader("IdEstadoConciliado"))
        EstadoConciliado = Convert.ToString(reader("EstadoConciliado"))
        FechaConciliado = DataUtil.ObjectToString(reader("FechaConciliacion"))
        UsuarioID = Convert.ToString(reader("UsuarioID"))
        UsuarioNombre = Convert.ToString(reader("UsuarioNombre"))
        UsuarioApellidos = Convert.ToString(reader("UsuarioApellidos"))
        UsuarioAlias = Convert.ToString(reader("UsuarioAlias"))
        UsuarioEmail = Convert.ToString(reader("UsuarioEmail"))
        DataAdicional = Convert.ToString(reader("DataAdicional"))
        IdEstado = Convert.ToString(reader("IdEstado"))
        Estado = Convert.ToString(reader("Estado"))
        ArchivoConciliacion = Convert.ToString(reader("NombreArchivo"))
        Banco = Convert.ToString(reader("BancoDescripcion"))
    End Sub

End Class
