Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEWSConsultarCIPRequest

    Public Sub New()

    End Sub

    Private _cAPI As String
	<DataMember()> _
    Public Property CAPI() As String
        Get
            Return _cAPI
        End Get
        Set(ByVal value As String)
            _cAPI = value
        End Set
    End Property

    Private _cClave As String
	<DataMember()> _
    Public Property CClave() As String
        Get
            Return _cClave
        End Get
        Set(ByVal value As String)
            _cClave = value
        End Set
    End Property

    Private _cips As String
    <DataMember()> _
    Public Property CIPS() As String
        Get
            Return _cips
        End Get
        Set(ByVal value As String)
            _cips = value
        End Set
    End Property

    Private _infoRequest As String
	<DataMember()> _
    Public Property InfoRequest() As String
        Get
            Return _infoRequest
        End Get
        Set(ByVal value As String)
            _infoRequest = value
        End Set
    End Property

End Class
