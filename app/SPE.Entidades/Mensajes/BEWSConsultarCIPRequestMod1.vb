﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEWSConsultarCIPRequestMod1

    Public Sub New()

    End Sub

    Private _codServ As String
    <DataMember()> _
    Public Property CodServ() As String
        Get
            Return _codServ
        End Get
        Set(ByVal value As String)
            _codServ = value
        End Set
    End Property

    Private _Firma As String
    <DataMember()> _
    Public Property Firma() As String
        Get
            Return _Firma
        End Get
        Set(ByVal value As String)
            _Firma = value
        End Set
    End Property

    Private _cips As String
    <DataMember()> _
    Public Property CIPS() As String
        Get
            Return _cips
        End Get
        Set(ByVal value As String)
            _cips = value
        End Set
    End Property

    Private _infoRequest As String
    <DataMember()> _
    Public Property InfoRequest() As String
        Get
            Return _infoRequest
        End Get
        Set(ByVal value As String)
            _infoRequest = value
        End Set
    End Property

End Class
