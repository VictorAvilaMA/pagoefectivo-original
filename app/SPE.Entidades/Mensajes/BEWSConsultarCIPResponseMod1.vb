﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEWSConsultarCIPResponseMod1

    Private _estado As String
    Private _mensaje As String
    Private _cips As List(Of BEWSConsCIPMod1)
    Private _infoResponse As String

    Public Sub New()
    End Sub

    Private _xml As String
    <DataMember()> _
    Public Property XML() As String
        Get
            Return _xml
        End Get
        Set(ByVal value As String)
            _xml = value
        End Set
    End Property

    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    <DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    <DataMember()> _
    Public Property InfoResponse() As String
        Get
            Return _infoResponse
        End Get
        Set(ByVal value As String)
            _infoResponse = value
        End Set
    End Property
End Class
