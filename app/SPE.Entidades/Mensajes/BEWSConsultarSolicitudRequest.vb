﻿Imports System.Runtime.Serialization
Imports Microsoft.VisualBasic

<Serializable()> <DataContract()> Public Class BEWSConsultarSolicitudRequest

    Public Sub New()

    End Sub

    Private _cServ As String
    <DataMember()> _
    Public Property cServ() As String
        Get
            Return _cServ
        End Get
        Set(ByVal value As String)
            _cServ = value
        End Set
    End Property

    Private _cClave As String
    <DataMember()> _
    Public Property CClave() As String
        Get
            Return _cClave
        End Get
        Set(ByVal value As String)
            _cClave = value
        End Set
    End Property

    Private _xml As String
    <DataMember()> _
    Public Property Xml() As String
        Get
            Return _xml
        End Get
        Set(ByVal value As String)
            _xml = value
        End Set
    End Property

End Class
