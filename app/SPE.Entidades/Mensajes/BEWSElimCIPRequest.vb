Imports System.Runtime.Serialization

<Serializable()> <DataContract()> _
Public Class BEWSElimCIPRequest

    Public Sub New()

    End Sub

    Private _cAPI As String
	<DataMember()> _
    Public Property CAPI() As String
        Get
            Return _cAPI
        End Get
        Set(ByVal value As String)
            _cAPI = value
        End Set
    End Property

    Private _cClave As String
	<DataMember()> _
    Public Property CClave() As String
        Get
            Return _cClave
        End Get
        Set(ByVal value As String)
            _cClave = value
        End Set
    End Property

    Private _cip As String
	<DataMember()> _
    Public Property CIP() As String
        Get
            Return _cip
        End Get
        Set(ByVal value As String)
            _cip = value
        End Set
    End Property

    Private _infoRequest As String
	<DataMember()> _
    Public Property InfoRequest() As String
        Get
            Return _infoRequest
        End Get
        Set(ByVal value As String)
            _infoRequest = value
        End Set
    End Property

End Class
