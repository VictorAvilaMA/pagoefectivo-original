Imports System.Runtime.Serialization
Imports Microsoft.VisualBasic

<Serializable()> <DataContract()> Public Class BEWSGenCIPRequest

    Public Sub New()

    End Sub

    Private _cAPI As String
	<DataMember()> _
    Public Property CAPI() As String
        Get
            Return _cAPI
        End Get
        Set(ByVal value As String)
            _cAPI = value
        End Set
    End Property

    Private _cClave As String
	<DataMember()> _
    Public Property CClave() As String
        Get
            Return _cClave
        End Get
        Set(ByVal value As String)
            _cClave = value
        End Set
    End Property

    Private _email As String
	<DataMember()> _
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property

    Private _password As String
	<DataMember()> _
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    Private _xml As String
	<DataMember()> _
    Public Property Xml() As String
        Get
            Return _xml
        End Get
        Set(ByVal value As String)
            _xml = value
        End Set
    End Property

End Class
