Imports System.Runtime.Serialization
Imports Microsoft.VisualBasic

<Serializable()> <DataContract()> Public Class BEWSGenCIPResponse

    Public Sub New()

    End Sub

    Private _estado As String
	<DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Private _cIP As String
	<DataMember()> _
    Public Property CIP() As String
        Get
            Return _cIP
        End Get
        Set(ByVal value As String)
            _cIP = value
        End Set
    End Property

    Private _mensaje As String
	<DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    'Private _tiempoExpiracion As String
    'Public Property TiempoExpiracion() As String
    '    Get
    '        Return _tiempoExpiracion
    '    End Get
    '    Set(ByVal value As String)
    '        _tiempoExpiracion = value
    '    End Set
    'End Property

    Private _informacionCIP As String
	<DataMember()> _
    Public Property InformacionCIP() As String
        Get
            Return _informacionCIP
        End Get
        Set(ByVal value As String)
            _informacionCIP = value
        End Set
    End Property

End Class
