﻿Imports System.Runtime.Serialization
Imports Microsoft.VisualBasic

<Serializable()> <DataContract()> Public Class BEWSGenCIPResponseMod1
    Public Sub New()

    End Sub

    Private _estado As String
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Private _mensaje As String
    <DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    Private _Xml As String
    <DataMember()> _
    Public Property Xml() As String
        Get
            Return _Xml
        End Get
        Set(ByVal value As String)
            _Xml = value
        End Set
    End Property

End Class