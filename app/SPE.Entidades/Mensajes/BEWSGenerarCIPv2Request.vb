﻿Imports System.Runtime.Serialization
Imports Microsoft.VisualBasic

<Serializable()> <DataContract()> Public Class BEWSGenerarCIPv2Request

    Public Sub New()

    End Sub

    Private _codServ As String
    <DataMember()> _
    Public Property CodServ() As String
        Get
            Return _codServ
        End Get
        Set(ByVal value As String)
            _codServ = value
        End Set
    End Property

    Private _firma As String
    <DataMember()> _
    Public Property Firma() As String
        Get
            Return _firma
        End Get
        Set(ByVal value As String)
            _firma = value
        End Set
    End Property

    Private _xml As String
    <DataMember()> _
    Public Property Xml() As String
        Get
            Return _xml
        End Get
        Set(ByVal value As String)
            _xml = value
        End Set
    End Property

End Class
