﻿Imports System.Runtime.Serialization
Imports Microsoft.VisualBasic

<Serializable()> <DataContract()> Public Class BEWSSolicitarResponse
    Public Sub New()

    End Sub

    Private _estado As String
    <DataMember()> _
    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Private _mensaje As String
    <DataMember()> _
    Public Property Mensaje() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    'Private _tiempoExpiracion As String
    'Public Property TiempoExpiracion() As String
    '    Get
    '        Return _tiempoExpiracion
    '    End Get
    '    Set(ByVal value As String)
    '        _tiempoExpiracion = value
    '    End Set
    'End Property

    Private _Xml As String
    <DataMember()> _
    Public Property Xml() As String
        Get
            Return _XML
        End Get
        Set(ByVal value As String)
            _Xml = value
        End Set
    End Property

End Class
