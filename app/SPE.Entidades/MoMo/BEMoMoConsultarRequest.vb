Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEMoMoConsultarRequest

    Private _CodPuntoVenta As String
    <DataMember()> _
    Public Property CodPuntoVenta() As String
        Get
            Return _CodPuntoVenta
        End Get
        Set(ByVal value As String)
            _CodPuntoVenta = value
        End Set
    End Property

    Private _NroSerieTerminal As String
    <DataMember()> _
    Public Property NroSerieTerminal() As String
        Get
            Return _NroSerieTerminal
        End Get
        Set(ByVal value As String)
            _NroSerieTerminal = value
        End Set
    End Property

    Private _NroOperacionMoMo As String
    <DataMember()> _
    Public Property NroOperacionMoMo() As String
        Get
            Return _NroOperacionMoMo
        End Get
        Set(ByVal value As String)
            _NroOperacionMoMo = value
        End Set
    End Property

    Private _CIP As String
    <DataMember()> _
    Public Property CIP() As String
        Get
            Return _CIP
        End Get
        Set(ByVal value As String)
            _CIP = value
        End Set
    End Property

    Private _CodServicio As String
    <DataMember()> _
    Public Property CodServicio() As String
        Get
            Return _CodServicio
        End Get
        Set(ByVal value As String)
            _CodServicio = value
        End Set
    End Property

    Private _CodCliente As String
    <DataMember()> _
    Public Property CodCliente() As String
        Get
            Return _CodCliente
        End Get
        Set(ByVal value As String)
            _CodCliente = value
        End Set
    End Property
    Private _CelularCliente As String
    <DataMember()> _
    Public Property CelularCliente() As String
        Get
            Return _CelularCliente
        End Get
        Set(ByVal value As String)
            _CelularCliente = value
        End Set
    End Property
    Private _CodCanal As String
    <DataMember()> _
    Public Property CodCanal() As String
        Get
            Return _CodCanal
        End Get
        Set(ByVal value As String)
            _CodCanal = value
        End Set
    End Property
End Class
