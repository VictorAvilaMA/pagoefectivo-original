Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEMoMoResponse(Of T)

    Public Sub New()

    End Sub

    Private _objMoMo As T
    <DataMember()> _
    Public Property ObjMoMo() As T
        Get
            Return _objMoMo
        End Get
        Set(ByVal value As T)
            _objMoMo = value
        End Set
    End Property

    Private _descripcion1 As String
    <DataMember()> _
    Public Property Descripcion1() As String
        Get
            Return _descripcion1
        End Get
        Set(ByVal value As String)
            _descripcion1 = value
        End Set
    End Property

    Private _descripcion2 As String
    <DataMember()> _
    Public Property Descripcion2() As String
        Get
            Return _descripcion2
        End Get
        Set(ByVal value As String)
            _descripcion2 = value
        End Set
    End Property

End Class
