﻿
Imports System.Runtime.Serialization
Imports System
Imports System.Collections
Imports System.Collections.Generic

Public Class BEBCPRequestMongo

    Private _codigo As String
    <DataMember()> _
    Public Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(ByVal value As String)
            _codigo = value
        End Set
    End Property

    Private _codServicio As String
    <DataMember()> _
    Public Property CodServicio As String
        Get
            Return _codServicio
        End Get
        Set(ByVal value As String)
            _codServicio = value
        End Set
    End Property

    Private _moneda As String
    <DataMember()> _
    Public Property Moneda As String
        Get
            Return _moneda
        End Get
        Set(ByVal value As String)
            _moneda = value
        End Set
    End Property

    Private _nroOperacion As String
    <DataMember()> _
    Public Property NroOperacion As String
        Get
            Return _nroOperacion
        End Get
        Set(ByVal value As String)
            _nroOperacion = value
        End Set
    End Property

    Private _nroOperacionAnulada As String
    <DataMember()> _
    Public Property NroOperacionAnulada As String
        Get
            Return _nroOperacionAnulada
        End Get
        Set(ByVal value As String)
            _nroOperacionAnulada = value
        End Set
    End Property

    Private _agencia As String
    <DataMember()> _
    Public Property Agencia As String
        Get
            Return _agencia
        End Get
        Set(ByVal value As String)
            _agencia = value
        End Set
    End Property

    Private _nroDocs As String
    <DataMember()> _
    Public Property NroDocs As String
        Get
            Return _nroDocs
        End Get
        Set(ByVal value As String)
            _nroDocs = value
        End Set
    End Property

    Private _nDoc1 As String
    <DataMember()> _
    Public Property NDoc1 As String
        Get
            Return _nDoc1
        End Get
        Set(ByVal value As String)
            _nDoc1 = value
        End Set
    End Property

    Private _mdoc1 As String
    <DataMember()> _
    Public Property Mdoc1 As String
        Get
            Return _mdoc1
        End Get
        Set(ByVal value As String)
            _mdoc1 = value
        End Set
    End Property

    Private _nDoc2 As String
    <DataMember()> _
    Public Property NDoc2 As String
        Get
            Return _nDoc2
        End Get
        Set(ByVal value As String)
            _nDoc2 = value
        End Set
    End Property

    Private _mdoc2 As String
    <DataMember()> _
    Public Property Mdoc2 As String
        Get
            Return _mdoc2
        End Get
        Set(ByVal value As String)
            _mdoc2 = value
        End Set
    End Property

    Private _nDoc3 As String
    <DataMember()> _
    Public Property NDoc3 As String
        Get
            Return _nDoc3
        End Get
        Set(ByVal value As String)
            _nDoc3 = value
        End Set
    End Property

    Private _mdoc3 As String
    <DataMember()> _
    Public Property Mdoc3 As String
        Get
            Return _mdoc3
        End Get
        Set(ByVal value As String)
            _mdoc3 = value
        End Set
    End Property

    Private _nDoc4 As String
    <DataMember()> _
    Public Property NDoc4 As String
        Get
            Return _nDoc4
        End Get
        Set(ByVal value As String)
            _nDoc4 = value
        End Set
    End Property

    Private _mdoc4 As String
    <DataMember()> _
    Public Property Mdoc4 As String
        Get
            Return _mdoc4
        End Get
        Set(ByVal value As String)
            _mdoc4 = value
        End Set
    End Property

    Private _nDoc5 As String
    <DataMember()> _
    Public Property NDoc5 As String
        Get
            Return _nDoc5
        End Get
        Set(ByVal value As String)
            _nDoc5 = value
        End Set
    End Property

    Private _mdoc5 As String
    <DataMember()> _
    Public Property Mdoc5 As String
        Get
            Return _mdoc5
        End Get
        Set(ByVal value As String)
            _mdoc5 = value
        End Set
    End Property

    Private _nDoc6 As String
    <DataMember()> _
    Public Property NDoc6 As String
        Get
            Return _nDoc6
        End Get
        Set(ByVal value As String)
            _nDoc6 = value
        End Set
    End Property

    Private _mdoc6 As String
    <DataMember()> _
    Public Property Mdoc6 As String
        Get
            Return _mdoc6
        End Get
        Set(ByVal value As String)
            _mdoc6 = value
        End Set
    End Property

    Private _nDoc7 As String
    <DataMember()> _
    Public Property NDoc7 As String
        Get
            Return _nDoc7
        End Get
        Set(ByVal value As String)
            _nDoc7 = value
        End Set
    End Property

    Private _mdoc7 As String
    <DataMember()> _
    Public Property Mdoc7 As String
        Get
            Return _mdoc7
        End Get
        Set(ByVal value As String)
            _mdoc7 = value
        End Set
    End Property

    Private _nDoc8 As String
    <DataMember()> _
    Public Property NDoc8 As String
        Get
            Return _nDoc8
        End Get
        Set(ByVal value As String)
            _nDoc8 = value
        End Set
    End Property

    Private _mdoc8 As String
    <DataMember()> _
    Public Property Mdoc8 As String
        Get
            Return _mdoc8
        End Get
        Set(ByVal value As String)
            _mdoc8 = value
        End Set
    End Property

    Private _nDoc9 As String
    <DataMember()> _
    Public Property NDoc9 As String
        Get
            Return _nDoc9
        End Get
        Set(ByVal value As String)
            _nDoc9 = value
        End Set
    End Property

    Private _mdoc9 As String
    <DataMember()> _
    Public Property Mdoc9 As String
        Get
            Return _mdoc9
        End Get
        Set(ByVal value As String)
            _mdoc9 = value
        End Set
    End Property

    Private _nDoc10 As String
    <DataMember()> _
    Public Property NDoc10 As String
        Get
            Return _nDoc10
        End Get
        Set(ByVal value As String)
            _nDoc10 = value
        End Set
    End Property

    Private _mdoc10 As String
    <DataMember()> _
    Public Property Mdoc10 As String
        Get
            Return _mdoc10
        End Get
        Set(ByVal value As String)
            _mdoc10 = value
        End Set
    End Property

    Private _monto As String
    <DataMember()> _
    Public Property Monto As String
        Get
            Return _monto
        End Get
        Set(ByVal value As String)
            _monto = value
        End Set
    End Property

    Private _codOperacion As String
    <DataMember()> _
    Public Property CodOperacion As String
        Get
            Return _codOperacion
        End Get
        Set(ByVal value As String)
            _codOperacion = value
        End Set
    End Property

    Private _Ip As String
    <DataMember()> _
    Public Property Ip As String
        Get
            Return _Ip
        End Get
        Set(ByVal value As String)
            _Ip = value
        End Set
    End Property


End Class






