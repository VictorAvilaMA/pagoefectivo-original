﻿Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BELogWebServiceRipleyResponse

    Private _LogIDRequest As Integer
    <DataMember()> _
    Public Property LogIDRequest() As Integer
        Get
            Return _LogIDRequest
        End Get
        Set(ByVal value As Integer)
            _LogIDRequest = value
        End Set
    End Property

    Private _OrdenPago As String
    <DataMember()> _
    Public Property OrdenPago() As String
        Get
            Return _OrdenPago
        End Get
        Set(ByVal value As String)
            _OrdenPago = value
        End Set
    End Property

    Private _OrderIdRipley As String
    <DataMember()> _
    Public Property OrderIdRipley() As String
        Get
            Return _OrderIdRipley
        End Get
        Set(ByVal value As String)
            _OrderIdRipley = value
        End Set
    End Property

    Private _CodigoRespuesta As String
    <DataMember()> _
    Public Property CodigoRespuesta() As String
        Get
            Return _CodigoRespuesta
        End Get
        Set(ByVal value As String)
            _CodigoRespuesta = value
        End Set
    End Property

    Private _GlosaRespuesta As String
    <DataMember()> _
    Public Property GlosaRespuesta() As String
        Get
            Return _GlosaRespuesta
        End Get
        Set(ByVal value As String)
            _GlosaRespuesta = value
        End Set
    End Property

    Private _MensajeError As String
    <DataMember()> _
    Public Property MensajeError() As String
        Get
            Return _MensajeError
        End Get
        Set(ByVal value As String)
            _MensajeError = value
        End Set
    End Property

    Private _WebServiceRipleyURL As String
    <DataMember()> _
    Public Property WebServiceRipleyURL() As String
        Get
            Return _WebServiceRipleyURL
        End Get
        Set(ByVal value As String)
            _WebServiceRipleyURL = value
        End Set
    End Property

    Private _xml As String
    <DataMember()> _
    Public Property Xml() As String
        Get
            Return _xml
        End Get
        Set(ByVal value As String)
            _xml = value
        End Set
    End Property

End Class
