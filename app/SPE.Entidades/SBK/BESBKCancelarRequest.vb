Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BESBKCancelarRequest
    Inherits BEBKBaseRequest

    Private _MessageTypeIdentification As BEBKCampoRequest
    <DataMember()> _
    Public Property MessageTypeIdentification() As BEBKCampoRequest
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoRequest
    <DataMember()> _
    Public Property PrimaryBitMap() As BEBKCampoRequest
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoRequest
    <DataMember()> _
    Public Property SecondaryBitMap() As BEBKCampoRequest
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _NumeroTarjeta As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroTarjeta() As BEBKCampoRequest
        Get
            Return _NumeroTarjeta
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroTarjeta = value
        End Set
    End Property

    Private _CodigoProceso As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoProceso() As BEBKCampoRequest
        Get
            Return _CodigoProceso
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoProceso = value
        End Set
    End Property

    Private _Monto As BEBKCampoRequest
    <DataMember()> _
    Public Property Monto() As BEBKCampoRequest
        Get
            Return _Monto
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Monto = value
        End Set
    End Property

    Private _FechaHoraTransaccion As BEBKCampoRequest
    <DataMember()> _
    Public Property FechaHoraTransaccion() As BEBKCampoRequest
        Get
            Return _FechaHoraTransaccion
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _FechaHoraTransaccion = value
        End Set
    End Property

    Private _Trace As BEBKCampoRequest
    <DataMember()> _
    Public Property Trace() As BEBKCampoRequest
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Trace = value
        End Set
    End Property

    Private _FechaCaptura As BEBKCampoRequest
    <DataMember()> _
    Public Property FechaCaptura() As BEBKCampoRequest
        Get
            Return _FechaCaptura
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _FechaCaptura = value
        End Set
    End Property

    Private _ModoIngresoDatos As BEBKCampoRequest
    <DataMember()> _
    Public Property ModoIngresoDatos() As BEBKCampoRequest
        Get
            Return _ModoIngresoDatos
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ModoIngresoDatos = value
        End Set
    End Property

    Private _Canal As BEBKCampoRequest
    <DataMember()> _
    Public Property Canal() As BEBKCampoRequest
        Get
            Return _Canal
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Canal = value
        End Set
    End Property

    Private _BinAdquiriente As BEBKCampoRequest
    <DataMember()> _
    Public Property BinAdquiriente() As BEBKCampoRequest
        Get
            Return _BinAdquiriente
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BinAdquiriente = value
        End Set
    End Property

    Private _ForwardInstitutionCode As BEBKCampoRequest
    <DataMember()> _
    Public Property ForwardInstitutionCode() As BEBKCampoRequest
        Get
            Return _ForwardInstitutionCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ForwardInstitutionCode = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoRequest
    <DataMember()> _
    Public Property RetrievalReferenceNumber() As BEBKCampoRequest
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _TerminalID As BEBKCampoRequest
    <DataMember()> _
    Public Property TerminalID() As BEBKCampoRequest
        Get
            Return _TerminalID
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TerminalID = value
        End Set
    End Property

    Private _Comercio As BEBKCampoRequest
    <DataMember()> _
    Public Property Comercio() As BEBKCampoRequest
        Get
            Return _Comercio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Comercio = value
        End Set
    End Property

    Private _CardAcceptorLocation As BEBKCampoRequest
    <DataMember()> _
    Public Property CardAcceptorLocation() As BEBKCampoRequest
        Get
            Return _CardAcceptorLocation
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CardAcceptorLocation = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoRequest
    <DataMember()> _
    Public Property TransactionCurrencyCode() As BEBKCampoRequest
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _DatosReservados As BEBKCampoRequest
    <DataMember()> _
    Public Property DatosReservados() As BEBKCampoRequest
        Get
            Return _DatosReservados
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DatosReservados = value
        End Set
    End Property

    Private _LongitudTrama As BEBKCampoRequest
    <DataMember()> _
    Public Property LongitudTrama() As BEBKCampoRequest
        Get
            Return _LongitudTrama
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _LongitudTrama = value
        End Set
    End Property

    Private _CodigoFormato As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoFormato() As BEBKCampoRequest
        Get
            Return _CodigoFormato
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoFormato = value
        End Set
    End Property

    Private _BinProcesador As BEBKCampoRequest
    <DataMember()> _
    Public Property BinProcesador() As BEBKCampoRequest
        Get
            Return _BinProcesador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BinProcesador = value
        End Set
    End Property

    Private _CodigoAcreedor As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoAcreedor() As BEBKCampoRequest
        Get
            Return _CodigoAcreedor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoAcreedor = value
        End Set
    End Property

    Private _CodigoProductoServicio As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoProductoServicio() As BEBKCampoRequest
        Get
            Return _CodigoProductoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoProductoServicio = value
        End Set
    End Property

    Private _CodigoPlazaRecaudador As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoPlazaRecaudador() As BEBKCampoRequest
        Get
            Return _CodigoPlazaRecaudador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoPlazaRecaudador = value
        End Set
    End Property

    Private _CodigoAgenciaRecaudador As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoAgenciaRecaudador() As BEBKCampoRequest
        Get
            Return _CodigoAgenciaRecaudador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoAgenciaRecaudador = value
        End Set
    End Property

    Private _TipoDatoPago As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoDatoPago() As BEBKCampoRequest
        Get
            Return _TipoDatoPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoDatoPago = value
        End Set
    End Property

    Private _DatoPago As BEBKCampoRequest
    <DataMember()> _
    Public Property DatoPago() As BEBKCampoRequest
        Get
            Return _DatoPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DatoPago = value
        End Set
    End Property

    Private _CodigoCiudad As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoCiudad() As BEBKCampoRequest
        Get
            Return _CodigoCiudad
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoCiudad = value
        End Set
    End Property

    Private _NumeroProductoServicioPagado As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroProductoServicioPagado() As BEBKCampoRequest
        Get
            Return _NumeroProductoServicioPagado
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroProductoServicioPagado = value
        End Set
    End Property

    Private _NumeroTotalDocumentosPagados As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroTotalDocumentosPagados() As BEBKCampoRequest
        Get
            Return _NumeroTotalDocumentosPagados
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroTotalDocumentosPagados = value
        End Set
    End Property

    Private _Filler As BEBKCampoRequest
    <DataMember()> _
    Public Property Filler() As BEBKCampoRequest
        Get
            Return _Filler
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Filler = value
        End Set
    End Property

    Private _MedioPago As BEBKCampoRequest
    <DataMember()> _
    Public Property MedioPago() As BEBKCampoRequest
        Get
            Return _MedioPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _MedioPago = value
        End Set
    End Property

    Private _ImportePagadoEfectivo As BEBKCampoRequest
    <DataMember()> _
    Public Property ImportePagadoEfectivo() As BEBKCampoRequest
        Get
            Return _ImportePagadoEfectivo
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImportePagadoEfectivo = value
        End Set
    End Property

    Private _ImportePagoConCuenta As BEBKCampoRequest
    <DataMember()> _
    Public Property ImportePagoConCuenta() As BEBKCampoRequest
        Get
            Return _ImportePagoConCuenta
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImportePagoConCuenta = value
        End Set
    End Property

    Private _NumeroCheque1 As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroCheque1() As BEBKCampoRequest
        Get
            Return _NumeroCheque1
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroCheque1 = value
        End Set
    End Property

    Private _BancoGirador1 As BEBKCampoRequest
    <DataMember()> _
    Public Property BancoGirador1() As BEBKCampoRequest
        Get
            Return _BancoGirador1
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BancoGirador1 = value
        End Set
    End Property

    Private _ImporteCheque1 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteCheque1() As BEBKCampoRequest
        Get
            Return _ImporteCheque1
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteCheque1 = value
        End Set
    End Property

    Private _PlazaCheque1 As BEBKCampoRequest
    <DataMember()> _
    Public Property PlazaCheque1() As BEBKCampoRequest
        Get
            Return _PlazaCheque1
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PlazaCheque1 = value
        End Set
    End Property

    Private _NumeroCheque2 As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroCheque2() As BEBKCampoRequest
        Get
            Return _NumeroCheque2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroCheque2 = value
        End Set
    End Property

    Private _BancoGirador2 As BEBKCampoRequest
    <DataMember()> _
    Public Property BancoGirador2() As BEBKCampoRequest
        Get
            Return _BancoGirador2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BancoGirador2 = value
        End Set
    End Property

    Private _ImporteCheque2 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteCheque2() As BEBKCampoRequest
        Get
            Return _ImporteCheque2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteCheque2 = value
        End Set
    End Property

    Private _PlazaCheque2 As BEBKCampoRequest
    <DataMember()> _
    Public Property PlazaCheque2() As BEBKCampoRequest
        Get
            Return _PlazaCheque2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PlazaCheque2 = value
        End Set
    End Property

    Private _NumeroCheque3 As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroCheque3() As BEBKCampoRequest
        Get
            Return _NumeroCheque3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroCheque3 = value
        End Set
    End Property

    Private _BancoGirador3 As BEBKCampoRequest
    <DataMember()> _
    Public Property BancoGirador3() As BEBKCampoRequest
        Get
            Return _BancoGirador3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BancoGirador3 = value
        End Set
    End Property

    Private _ImporteCheque3 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteCheque3() As BEBKCampoRequest
        Get
            Return _ImporteCheque3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteCheque3 = value
        End Set
    End Property

    Private _PlazaCheque3 As BEBKCampoRequest
    <DataMember()> _
    Public Property PlazaCheque3() As BEBKCampoRequest
        Get
            Return _PlazaCheque3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PlazaCheque3 = value
        End Set
    End Property

    Private _MonedaPago As BEBKCampoRequest
    <DataMember()> _
    Public Property MonedaPago() As BEBKCampoRequest
        Get
            Return _MonedaPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _MonedaPago = value
        End Set
    End Property

    Private _TipoCambioAplicado As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoCambioAplicado() As BEBKCampoRequest
        Get
            Return _TipoCambioAplicado
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoCambioAplicado = value
        End Set
    End Property

    Private _PagoTotalRealizado As BEBKCampoRequest
    <DataMember()> _
    Public Property PagoTotalRealizado() As BEBKCampoRequest
        Get
            Return _PagoTotalRealizado
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PagoTotalRealizado = value
        End Set
    End Property

    Private _Filler2 As BEBKCampoRequest
    <DataMember()> _
    Public Property Filler2() As BEBKCampoRequest
        Get
            Return _Filler2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Filler2 = value
        End Set
    End Property

    Private _CodigoServicioPagado As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoServicioPagado() As BEBKCampoRequest
        Get
            Return _CodigoServicioPagado
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoServicioPagado = value
        End Set
    End Property

    Private _EstadoDeudor As BEBKCampoRequest
    <DataMember()> _
    Public Property EstadoDeudor() As BEBKCampoRequest
        Get
            Return _EstadoDeudor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _EstadoDeudor = value
        End Set
    End Property

    Private _ImporteTotalProductoServicio As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteTotalProductoServicio() As BEBKCampoRequest
        Get
            Return _ImporteTotalProductoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteTotalProductoServicio = value
        End Set
    End Property

    Private _NumeroCuentaAbono As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroCuentaAbono() As BEBKCampoRequest
        Get
            Return _NumeroCuentaAbono
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroCuentaAbono = value
        End Set
    End Property

    Private _NumeroReferenciaAbono As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroReferenciaAbono() As BEBKCampoRequest
        Get
            Return _NumeroReferenciaAbono
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroReferenciaAbono = value
        End Set
    End Property

    Private _NumeroDocumentosPagados As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroDocumentosPagados() As BEBKCampoRequest
        Get
            Return _NumeroDocumentosPagados
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroDocumentosPagados = value
        End Set
    End Property

    Private _Filler3 As BEBKCampoRequest
    <DataMember()> _
    Public Property Filler3() As BEBKCampoRequest
        Get
            Return _Filler3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Filler3 = value
        End Set
    End Property

    Private _TipoDocumentoPago As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoDocumentoPago() As BEBKCampoRequest
        Get
            Return _TipoDocumentoPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoDocumentoPago = value
        End Set
    End Property

    Private _NumeroDocumentoPago As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroDocumentoPago() As BEBKCampoRequest
        Get
            Return _NumeroDocumentoPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroDocumentoPago = value
        End Set
    End Property

    Private _PeriodoCotizacion As BEBKCampoRequest
    <DataMember()> _
    Public Property PeriodoCotizacion() As BEBKCampoRequest
        Get
            Return _PeriodoCotizacion
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PeriodoCotizacion = value
        End Set
    End Property

    Private _TipoDocumentoIDDeudor As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoDocumentoIDDeudor() As BEBKCampoRequest
        Get
            Return _TipoDocumentoIDDeudor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoDocumentoIDDeudor = value
        End Set
    End Property

    Private _NumeroDocumentoIDDeudor As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroDocumentoIDDeudor() As BEBKCampoRequest
        Get
            Return _NumeroDocumentoIDDeudor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroDocumentoIDDeudor = value
        End Set
    End Property

    Private _ImporteOriginalDeuda As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteOriginalDeuda() As BEBKCampoRequest
        Get
            Return _ImporteOriginalDeuda
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteOriginalDeuda = value
        End Set
    End Property

    Private _ImportePagadoDocumento As BEBKCampoRequest
    <DataMember()> _
    Public Property ImportePagadoDcumento() As BEBKCampoRequest
        Get
            Return _ImportePagadoDocumento
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImportePagadoDocumento = value
        End Set
    End Property

    Private _CodigoConcepto1 As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoConcepto1() As BEBKCampoRequest
        Get
            Return _CodigoConcepto1
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoConcepto1 = value
        End Set
    End Property

    Private _ImporteConcepto1 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteConcepto1() As BEBKCampoRequest
        Get
            Return _ImporteConcepto1
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteConcepto1 = value
        End Set
    End Property

    Private _CodigoConcepto2 As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoConcepto2() As BEBKCampoRequest
        Get
            Return _CodigoConcepto2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoConcepto2 = value
        End Set
    End Property

    Private _ImporteConcepto2 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteConcepto2() As BEBKCampoRequest
        Get
            Return _ImporteConcepto2
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteConcepto2 = value
        End Set
    End Property

    Private _CodigoConcepto3 As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoConcepto3() As BEBKCampoRequest
        Get
            Return _CodigoConcepto3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoConcepto3 = value
        End Set
    End Property

    Private _ImporteConcepto3 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteConcepto3() As BEBKCampoRequest
        Get
            Return _ImporteConcepto3
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteConcepto3 = value
        End Set
    End Property

    Private _CodigoConcepto4 As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoConcepto4() As BEBKCampoRequest
        Get
            Return _CodigoConcepto4
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoConcepto4 = value
        End Set
    End Property

    Private _ImporteConcepto4 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteConcepto4() As BEBKCampoRequest
        Get
            Return _ImporteConcepto4
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteConcepto4 = value
        End Set
    End Property

    Private _CodigoConcepto5 As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoConcepto5() As BEBKCampoRequest
        Get
            Return _CodigoConcepto5
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoConcepto5 = value
        End Set
    End Property

    Private _ImporteConcepto5 As BEBKCampoRequest
    <DataMember()> _
    Public Property ImporteConcepto5() As BEBKCampoRequest
        Get
            Return _ImporteConcepto5
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ImporteConcepto5 = value
        End Set
    End Property

    Private _ReferenciaDeuda As BEBKCampoRequest
    <DataMember()> _
    Public Property ReferenciaDeuda() As BEBKCampoRequest
        Get
            Return _ReferenciaDeuda
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ReferenciaDeuda = value
        End Set
    End Property

    Private _Filler4 As BEBKCampoRequest
    <DataMember()> _
    Public Property Filler4() As BEBKCampoRequest
        Get
            Return _Filler4
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Filler4 = value
        End Set
    End Property

    Public Sub New(ByVal Trama As String)
        MyBase.New(Trama)
        MessageTypeIdentification = New BEBKCampoRequest(Trama, 0, 4)
        PrimaryBitMap = New BEBKCampoRequest(Trama, 4, 16)
        SecondaryBitMap = New BEBKCampoRequest(Trama, 20, 16)
        NumeroTarjeta = New BEBKCampoRequest(Trama, 36, 18)
        CodigoProceso = New BEBKCampoRequest(Trama, 54, 6)
        Monto = New BEBKCampoRequest(Trama, 60, 12)
        FechaHoraTransaccion = New BEBKCampoRequest(Trama, 72, 10)
        Trace = New BEBKCampoRequest(Trama, 82, 6)
        FechaCaptura = New BEBKCampoRequest(Trama, 88, 4)
        ModoIngresoDatos = New BEBKCampoRequest(Trama, 92, 3)
        Canal = New BEBKCampoRequest(Trama, 95, 2)
        BinAdquiriente = New BEBKCampoRequest(Trama, 97, 8)
        ForwardInstitutionCode = New BEBKCampoRequest(Trama, 105, 8)
        RetrievalReferenceNumber = New BEBKCampoRequest(Trama, 113, 12)
        TerminalID = New BEBKCampoRequest(Trama, 125, 8)
        Comercio = New BEBKCampoRequest(Trama, 133, 15)
        CardAcceptorLocation = New BEBKCampoRequest(Trama, 148, 40)
        TransactionCurrencyCode = New BEBKCampoRequest(Trama, 188, 3)
        DatosReservados = New BEBKCampoRequest(Trama, 191, 5)
        LongitudTrama = New BEBKCampoRequest(Trama, 196, 3)
        CodigoFormato = New BEBKCampoRequest(Trama, 199, 2)
        BinProcesador = New BEBKCampoRequest(Trama, 201, 11)
        CodigoAcreedor = New BEBKCampoRequest(Trama, 212, 11)
        CodigoProductoServicio = New BEBKCampoRequest(Trama, 223, 8)
        CodigoPlazaRecaudador = New BEBKCampoRequest(Trama, 231, 4)
        CodigoAgenciaRecaudador = New BEBKCampoRequest(Trama, 235, 4)
        TipoDatoPago = New BEBKCampoRequest(Trama, 239, 2)
        DatoPago = New BEBKCampoRequest(Trama, 241, 21)
        CodigoCiudad = New BEBKCampoRequest(Trama, 262, 3)
        NumeroProductoServicioPagado = New BEBKCampoRequest(Trama, 265, 2)
        NumeroTotalDocumentosPagados = New BEBKCampoRequest(Trama, 267, 3)
        Filler = New BEBKCampoRequest(Trama, 270, 10)
        MedioPago = New BEBKCampoRequest(Trama, 280, 2)
        ImportePagadoEfectivo = New BEBKCampoRequest(Trama, 282, 11)
        ImportePagoConCuenta = New BEBKCampoRequest(Trama, 293, 11)
        NumeroCheque1 = New BEBKCampoRequest(Trama, 304, 15)
        BancoGirador1 = New BEBKCampoRequest(Trama, 319, 3)
        ImporteCheque1 = New BEBKCampoRequest(Trama, 322, 11)
        PlazaCheque1 = New BEBKCampoRequest(Trama, 333, 1)
        NumeroCheque2 = New BEBKCampoRequest(Trama, 334, 15)
        BancoGirador2 = New BEBKCampoRequest(Trama, 349, 3)
        ImporteCheque2 = New BEBKCampoRequest(Trama, 352, 11)
        PlazaCheque2 = New BEBKCampoRequest(Trama, 363, 1)
        NumeroCheque3 = New BEBKCampoRequest(Trama, 364, 15)
        BancoGirador3 = New BEBKCampoRequest(Trama, 379, 3)
        ImporteCheque3 = New BEBKCampoRequest(Trama, 382, 11)
        PlazaCheque3 = New BEBKCampoRequest(Trama, 393, 1)
        MonedaPago = New BEBKCampoRequest(Trama, 394, 3)
        TipoCambioAplicado = New BEBKCampoRequest(Trama, 397, 11)
        PagoTotalRealizado = New BEBKCampoRequest(Trama, 408, 11)
        Filler2 = New BEBKCampoRequest(Trama, 419, 10)
        CodigoServicioPagado = New BEBKCampoRequest(Trama, 429, 3)
        EstadoDeudor = New BEBKCampoRequest(Trama, 432, 2)
        ImporteTotalProductoServicio = New BEBKCampoRequest(Trama, 434, 11)
        NumeroCuentaAbono = New BEBKCampoRequest(Trama, 445, 19)
        NumeroReferenciaAbono = New BEBKCampoRequest(Trama, 464, 12)
        NumeroDocumentosPagados = New BEBKCampoRequest(Trama, 476, 2)
        Filler3 = New BEBKCampoRequest(Trama, 478, 10)
        TipoDocumentoPago = New BEBKCampoRequest(Trama, 488, 3)
        NumeroDocumentoPago = New BEBKCampoRequest(Trama, 491, 16)
        PeriodoCotizacion = New BEBKCampoRequest(Trama, 507, 6)
        TipoDocumentoIDDeudor = New BEBKCampoRequest(Trama, 513, 2)
        NumeroDocumentoIDDeudor = New BEBKCampoRequest(Trama, 515, 15)
        ImporteOriginalDeuda = New BEBKCampoRequest(Trama, 530, 11)
        ImportePagadoDcumento = New BEBKCampoRequest(Trama, 541, 11)
        CodigoConcepto1 = New BEBKCampoRequest(Trama, 552, 2)
        ImporteConcepto1 = New BEBKCampoRequest(Trama, 554, 11)
        CodigoConcepto2 = New BEBKCampoRequest(Trama, 565, 2)
        ImporteConcepto2 = New BEBKCampoRequest(Trama, 567, 11)
        CodigoConcepto3 = New BEBKCampoRequest(Trama, 578, 2)
        ImporteConcepto3 = New BEBKCampoRequest(Trama, 580, 11)
        CodigoConcepto4 = New BEBKCampoRequest(Trama, 591, 2)
        ImporteConcepto4 = New BEBKCampoRequest(Trama, 593, 11)
        CodigoConcepto5 = New BEBKCampoRequest(Trama, 604, 2)
        ImporteConcepto5 = New BEBKCampoRequest(Trama, 606, 11)
        ReferenciaDeuda = New BEBKCampoRequest(Trama, 617, 16)
        Filler4 = New BEBKCampoRequest(Trama, 633, 34)
    End Sub

End Class
