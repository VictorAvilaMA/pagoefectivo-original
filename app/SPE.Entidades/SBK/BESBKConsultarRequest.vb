Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BESBKConsultarRequest
    Inherits BEBKBaseRequest

    Private _MessageTypeIdentification As BEBKCampoRequest
    <DataMember()> _
    Public Property MessageTypeIdentification() As BEBKCampoRequest
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoRequest
    <DataMember()> _
    Public Property PrimaryBitMap() As BEBKCampoRequest
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoRequest
    <DataMember()> _
    Public Property SecondaryBitMap() As BEBKCampoRequest
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _NumeroTarjeta As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroTarjeta() As BEBKCampoRequest
        Get
            Return _NumeroTarjeta
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroTarjeta = value
        End Set
    End Property

    Private _CodigoProceso As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoProceso() As BEBKCampoRequest
        Get
            Return _CodigoProceso
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoProceso = value
        End Set
    End Property

    Private _Monto As BEBKCampoRequest
    <DataMember()> _
    Public Property Monto() As BEBKCampoRequest
        Get
            Return _Monto
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Monto = value
        End Set
    End Property

    Private _FechaHoraTransaccion As BEBKCampoRequest
    <DataMember()> _
    Public Property FechaHoraTransaccion() As BEBKCampoRequest
        Get
            Return _FechaHoraTransaccion
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _FechaHoraTransaccion = value
        End Set
    End Property

    Private _Trace As BEBKCampoRequest
    <DataMember()> _
    Public Property Trace() As BEBKCampoRequest
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Trace = value
        End Set
    End Property

    Private _FechaCaptura As BEBKCampoRequest
    <DataMember()> _
    Public Property FechaCaptura() As BEBKCampoRequest
        Get
            Return _FechaCaptura
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _FechaCaptura = value
        End Set
    End Property

    Private _ModoIngresoDatos As BEBKCampoRequest
    <DataMember()> _
    Public Property ModoIngresoCampos() As BEBKCampoRequest
        Get
            Return _ModoIngresoDatos
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ModoIngresoDatos = value
        End Set
    End Property

    Private _Canal As BEBKCampoRequest
    <DataMember()> _
    Public Property Canal() As BEBKCampoRequest
        Get
            Return _Canal
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Canal = value
        End Set
    End Property

    Private _BinAdquiriente As BEBKCampoRequest
    <DataMember()> _
    Public Property BinAdquiriente() As BEBKCampoRequest
        Get
            Return _BinAdquiriente
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BinAdquiriente = value
        End Set
    End Property

    Private _ForwardInstitutionCode As BEBKCampoRequest
    <DataMember()> _
    Public Property ForwardInstitutionCode() As BEBKCampoRequest
        Get
            Return _ForwardInstitutionCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ForwardInstitutionCode = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoRequest
    <DataMember()> _
    Public Property RetrievalReferenceNumber() As BEBKCampoRequest
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _TerminalID As BEBKCampoRequest
    <DataMember()> _
    Public Property TerminalID() As BEBKCampoRequest
        Get
            Return _TerminalID
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TerminalID = value
        End Set
    End Property

    Private _Comercio As BEBKCampoRequest
    <DataMember()> _
    Public Property Comercio() As BEBKCampoRequest
        Get
            Return _Comercio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Comercio = value
        End Set
    End Property

    Private _CardAcceptorLocation As BEBKCampoRequest
    <DataMember()> _
    Public Property CardAcceptorLocation() As BEBKCampoRequest
        Get
            Return _CardAcceptorLocation
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CardAcceptorLocation = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoRequest
    <DataMember()> _
    Public Property TransactionCurrencyCode() As BEBKCampoRequest
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _DatosReservados As BEBKCampoRequest
    <DataMember()> _
    Public Property DatosReservados() As BEBKCampoRequest
        Get
            Return _DatosReservados
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DatosReservados = value
        End Set
    End Property

    Private _LongitudRequerimiento As BEBKCampoRequest
    <DataMember()> _
    Public Property LongitudRequerimiento() As BEBKCampoRequest
        Get
            Return _LongitudRequerimiento
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _LongitudRequerimiento = value
        End Set
    End Property

    Private _CodigoFormato As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoFormato() As BEBKCampoRequest
        Get
            Return _CodigoFormato
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoFormato = value
        End Set
    End Property

    Private _BinProcesador As BEBKCampoRequest
    <DataMember()> _
    Public Property BinProcesador() As BEBKCampoRequest
        Get
            Return _BinProcesador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BinProcesador = value
        End Set
    End Property

    Private _CodigoAcreedor As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoAcreedor() As BEBKCampoRequest
        Get
            Return _CodigoAcreedor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoAcreedor = value
        End Set
    End Property

    Private _CodigoProductoServicio As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoProductoServicio() As BEBKCampoRequest
        Get
            Return _CodigoProductoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoProductoServicio = value
        End Set
    End Property

    Private _CodigoPlazaRecaudador As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoPlazaRecaudador() As BEBKCampoRequest
        Get
            Return _CodigoPlazaRecaudador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoPlazaRecaudador = value
        End Set
    End Property

    Private _CodigoAgenciaRecaudador As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoAgenciaRecaudador() As BEBKCampoRequest
        Get
            Return _CodigoAgenciaRecaudador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoAgenciaRecaudador = value
        End Set
    End Property

    Private _TipoDatoConsulta As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoDatoConsulta() As BEBKCampoRequest
        Get
            Return _TipoDatoConsulta
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoDatoConsulta = value
        End Set
    End Property

    Private _DatoConsulta As BEBKCampoRequest
    <DataMember()> _
    Public Property DatoConsulta() As BEBKCampoRequest
        Get
            Return _DatoConsulta
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DatoConsulta = value
        End Set
    End Property

    Private _CodigoCiudad As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoCiudad() As BEBKCampoRequest
        Get
            Return _CodigoCiudad
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoCiudad = value
        End Set
    End Property

    Private _CodigoServicio As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoServicio() As BEBKCampoRequest
        Get
            Return _CodigoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoServicio = value
        End Set
    End Property

    Private _NumeroDocumento As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroDocumento() As BEBKCampoRequest
        Get
            Return _NumeroDocumento
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroDocumento = value
        End Set
    End Property

    Private _NumeroOperacion As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroOperacion() As BEBKCampoRequest
        Get
            Return _NumeroOperacion
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroOperacion = value
        End Set
    End Property

    Private _Filler As BEBKCampoRequest
    <DataMember()> _
    Public Property Filler() As BEBKCampoRequest
        Get
            Return _Filler
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Filler = value
        End Set
    End Property

    Private _TamanoMaximoBloque As BEBKCampoRequest
    <DataMember()> _
    Public Property TamanoMaximoBloque() As BEBKCampoRequest
        Get
            Return _TamanoMaximoBloque
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TamanoMaximoBloque = value
        End Set
    End Property

    Private _PosicionUltimoDocumento As BEBKCampoRequest
    <DataMember()> _
    Public Property PosicionUltimoDocumento() As BEBKCampoRequest
        Get
            Return _PosicionUltimoDocumento
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PosicionUltimoDocumento = value
        End Set
    End Property

    Private _PunteroBaseDatos As BEBKCampoRequest
    <DataMember()> _
    Public Property PunteroBaseDatos() As BEBKCampoRequest
        Get
            Return _PunteroBaseDatos
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PunteroBaseDatos = value
        End Set
    End Property


    Public Sub New(ByVal Trama As String)
        MyBase.New(Trama)
        MessageTypeIdentification = New BEBKCampoRequest(Trama, 0, 4)
        PrimaryBitMap = New BEBKCampoRequest(Trama, 4, 16)
        SecondaryBitMap = New BEBKCampoRequest(Trama, 20, 16)
        NumeroTarjeta = New BEBKCampoRequest(Trama, 36, 18)
        CodigoProceso = New BEBKCampoRequest(Trama, 54, 6)
        Monto = New BEBKCampoRequest(Trama, 60, 12)
        FechaHoraTransaccion = New BEBKCampoRequest(Trama, 72, 10)
        Trace = New BEBKCampoRequest(Trama, 82, 6)
        FechaCaptura = New BEBKCampoRequest(Trama, 88, 4)
        ModoIngresoCampos = New BEBKCampoRequest(Trama, 92, 3)
        Canal = New BEBKCampoRequest(Trama, 95, 2)
        BinAdquiriente = New BEBKCampoRequest(Trama, 97, 8)
        ForwardInstitutionCode = New BEBKCampoRequest(Trama, 105, 8)
        RetrievalReferenceNumber = New BEBKCampoRequest(Trama, 113, 12)
        TerminalID = New BEBKCampoRequest(Trama, 125, 8)
        Comercio = New BEBKCampoRequest(Trama, 133, 15)
        CardAcceptorLocation = New BEBKCampoRequest(Trama, 148, 40)
        TransactionCurrencyCode = New BEBKCampoRequest(Trama, 188, 3)
        DatosReservados = New BEBKCampoRequest(Trama, 191, 5)
        LongitudRequerimiento = New BEBKCampoRequest(Trama, 196, 3)
        CodigoFormato = New BEBKCampoRequest(Trama, 199, 2)
        BinProcesador = New BEBKCampoRequest(Trama, 201, 11)
        CodigoAcreedor = New BEBKCampoRequest(Trama, 212, 11)
        CodigoProductoServicio = New BEBKCampoRequest(Trama, 223, 8)
        CodigoPlazaRecaudador = New BEBKCampoRequest(Trama, 231, 4)
        CodigoAgenciaRecaudador = New BEBKCampoRequest(Trama, 235, 4)
        TipoDatoConsulta = New BEBKCampoRequest(Trama, 239, 2)
        DatoConsulta = New BEBKCampoRequest(Trama, 241, 21)
        CodigoCiudad = New BEBKCampoRequest(Trama, 262, 3)
        CodigoServicio = New BEBKCampoRequest(Trama, 265, 3)
        NumeroDocumento = New BEBKCampoRequest(Trama, 268, 16)
        NumeroOperacion = New BEBKCampoRequest(Trama, 284, 12)
        Filler = New BEBKCampoRequest(Trama, 296, 20)
        TamanoMaximoBloque = New BEBKCampoRequest(Trama, 316, 5)
        PosicionUltimoDocumento = New BEBKCampoRequest(Trama, 321, 3)
        PunteroBaseDatos = New BEBKCampoRequest(Trama, 324, 10)
    End Sub

End Class
