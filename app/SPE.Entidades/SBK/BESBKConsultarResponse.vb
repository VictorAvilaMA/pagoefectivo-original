Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BESBKConsultarResponse

    Private _MessageTypeIdentification As BEBKCampoResponse
    <DataMember()> _
    Public Property MessageTypeIdentification() As BEBKCampoResponse
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoResponse
    <DataMember()> _
    Public Property PrimaryBitMap() As BEBKCampoResponse
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoResponse
    <DataMember()> _
    Public Property SecondaryBitMap() As BEBKCampoResponse
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _CodigoProceso As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoProceso() As BEBKCampoResponse
        Get
            Return _CodigoProceso
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoProceso = value
        End Set
    End Property

    Private _Monto As BEBKCampoResponse
    <DataMember()> _
    Public Property Monto() As BEBKCampoResponse
        Get
            Return _Monto
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Monto = value
        End Set
    End Property

    Private _FechaHoraTransaccion As BEBKCampoResponse
    <DataMember()> _
    Public Property FechaHoraTransaccion() As BEBKCampoResponse
        Get
            Return _FechaHoraTransaccion
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _FechaHoraTransaccion = value
        End Set
    End Property

    Private _Trace As BEBKCampoResponse
    <DataMember()> _
    Public Property Trace() As BEBKCampoResponse
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Trace = value
        End Set
    End Property

    Private _FechaCaptura As BEBKCampoResponse
    <DataMember()> _
    Public Property FechaCaptura() As BEBKCampoResponse
        Get
            Return _FechaCaptura
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _FechaCaptura = value
        End Set
    End Property

    Private _BinAdquiriente As BEBKCampoResponse
    <DataMember()> _
    Public Property BinAdquiriente() As BEBKCampoResponse
        Get
            Return _BinAdquiriente
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _BinAdquiriente = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoResponse
    <DataMember()> _
    Public Property RetrievalReferenceNumber() As BEBKCampoResponse
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _AuthorizationIDResponse As BEBKCampoResponse
    <DataMember()> _
    Public Property AuthorizationIDResponse() As BEBKCampoResponse
        Get
            Return _AuthorizationIDResponse
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _AuthorizationIDResponse = value
        End Set
    End Property

    Private _ResponseCode As BEBKCampoResponse
    <DataMember()> _
    Public Property ResponseCode() As BEBKCampoResponse
        Get
            Return _ResponseCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ResponseCode = value
        End Set
    End Property

    Private _TerminalID As BEBKCampoResponse
    <DataMember()> _
    Public Property TerminalID() As BEBKCampoResponse
        Get
            Return _TerminalID
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TerminalID = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoResponse
    <DataMember()> _
    Public Property TransactionCurrencyCode() As BEBKCampoResponse
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _DatosReservados As BEBKCampoResponse
    <DataMember()> _
    Public Property DatosReservados() As BEBKCampoResponse
        Get
            Return _DatosReservados
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DatosReservados = value
        End Set
    End Property

#Region "Cabecera Respuesta"

    Private _CabResLongitudCampo As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResLongitudCampo() As BEBKCampoResponse
        Get
            Return _CabResLongitudCampo
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResLongitudCampo = value
        End Set
    End Property

    Private _CabResCodigoFormato As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResCodigoFormato() As BEBKCampoResponse
        Get
            Return _CabResCodigoFormato
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResCodigoFormato = value
        End Set
    End Property

    Private _CabResBinProcesador As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResBinProcesador() As BEBKCampoResponse
        Get
            Return _CabResBinProcesador
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResBinProcesador = value
        End Set
    End Property

    Private _CabResBinAcreedor As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResBinAcreedor() As BEBKCampoResponse
        Get
            Return _CabResBinAcreedor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResBinAcreedor = value
        End Set
    End Property

    Private _CabResCodigoProductoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResCodigoProductoServicio() As BEBKCampoResponse
        Get
            Return _CabResCodigoProductoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResCodigoProductoServicio = value
        End Set
    End Property

    Private _CabResAgencia As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResAgencia() As BEBKCampoResponse
        Get
            Return _CabResAgencia
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResAgencia = value
        End Set
    End Property

    Private _CabResTipoIdentificacion As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResTipoIdentificacion() As BEBKCampoResponse
        Get
            Return _CabResTipoIdentificacion
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResTipoIdentificacion = value
        End Set
    End Property

    Private _CabResNumeroIdentificacion As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResNumeroIdentificacion() As BEBKCampoResponse
        Get
            Return _CabResNumeroIdentificacion
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResNumeroIdentificacion = value
        End Set
    End Property

    Private _CabResNombreDeudor As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResNombreDeudor() As BEBKCampoResponse
        Get
            Return _CabResNombreDeudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResNombreDeudor = value
        End Set
    End Property

    Private _CabResNumeroServiciosDevueltos As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResNumeroServiciosDevueltos() As BEBKCampoResponse
        Get
            Return _CabResNumeroServiciosDevueltos
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResNumeroServiciosDevueltos = value
        End Set
    End Property

    Private _CabResNumeroOperacionCobranza As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResNumeroOperacionCobranza() As BEBKCampoResponse
        Get
            Return _CabResNumeroOperacionCobranza
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResNumeroOperacionCobranza = value
        End Set
    End Property

    Private _CabResIndicadorSiHayDocumentos As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResIndicadorSiHayDocumentos() As BEBKCampoResponse
        Get
            Return _CabResIndicadorSiHayDocumentos
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResIndicadorSiHayDocumentos = value
        End Set
    End Property

    Private _CabResTamanoMaximoBloque As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResTamanoMaximoBloque() As BEBKCampoResponse
        Get
            Return _CabResTamanoMaximoBloque
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResTamanoMaximoBloque = value
        End Set
    End Property

    Private _CabResPosicionUltimoDocumento As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResPosicionUltimoDocumento() As BEBKCampoResponse
        Get
            Return _CabResPosicionUltimoDocumento
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResPosicionUltimoDocumento = value
        End Set
    End Property

    Private _CabResPunteroBaseDatos As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResPunteroBaseDatos() As BEBKCampoResponse
        Get
            Return _CabResPunteroBaseDatos
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResPunteroBaseDatos = value
        End Set
    End Property

    Private _CabResOrigenRespuesta As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResOrigenRespuesta() As BEBKCampoResponse
        Get
            Return _CabResOrigenRespuesta
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResOrigenRespuesta = value
        End Set
    End Property

    Private _CabResCodigoRespuesta As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResCodigoRespuesta() As BEBKCampoResponse
        Get
            Return _CabResCodigoRespuesta
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResCodigoRespuesta = value
        End Set
    End Property

    Private _CabResFiller As BEBKCampoResponse
    <DataMember()> _
    Public Property CabResFiller() As BEBKCampoResponse
        Get
            Return _CabResFiller
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CabResFiller = value
        End Set
    End Property

#End Region

#Region "Detalle Respuesta"

#Region "Cabecera Servicio"
    Private _DetResCabSerCodigoProductoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerCodigoProductoServicio() As BEBKCampoResponse
        Get
            Return _DetResCabSerCodigoProductoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerCodigoProductoServicio = value
        End Set
    End Property

    Private _DetResCabSerMoneda As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerMoneda() As BEBKCampoResponse
        Get
            Return _DetResCabSerMoneda
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerMoneda = value
        End Set
    End Property

    Private _DetResCabSerEstadoDeudor As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerEstadoDeudor() As BEBKCampoResponse
        Get
            Return _DetResCabSerEstadoDeudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerEstadoDeudor = value
        End Set
    End Property

    Private _DetResCabSerMensaje1Deudor As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerMensaje1Deudor() As BEBKCampoResponse
        Get
            Return _DetResCabSerMensaje1Deudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerMensaje1Deudor = value
        End Set
    End Property

    Private _DetResCabSerMensaje2Deudor As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerMensaje2Deudor() As BEBKCampoResponse
        Get
            Return _DetResCabSerMensaje2Deudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerMensaje2Deudor = value
        End Set
    End Property

    Private _DetResCabSerIndicadorCronologia As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerIndicadorCronologia() As BEBKCampoResponse
        Get
            Return _DetResCabSerIndicadorCronologia
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerIndicadorCronologia = value
        End Set
    End Property

    Private _DetResCabSerIndicadorPagosVencidos As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerIndicadorPagosVencidos() As BEBKCampoResponse
        Get
            Return _DetResCabSerIndicadorPagosVencidos
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerIndicadorPagosVencidos = value
        End Set
    End Property

    Private _DetResCabSerRestriccionPago As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerRestriccionPago() As BEBKCampoResponse
        Get
            Return _DetResCabSerRestriccionPago
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerRestriccionPago = value
        End Set
    End Property

    Private _DetResCabSerDocumentosServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerDocumentosServicio() As BEBKCampoResponse
        Get
            Return _DetResCabSerDocumentosServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerDocumentosServicio = value
        End Set
    End Property

    Private _DetResCabSerFiller As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResCabSerFiller() As BEBKCampoResponse
        Get
            Return _DetResCabSerFiller
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResCabSerFiller = value
        End Set
    End Property

#End Region

#Region "Detalle Servicio"

    Private _DetResDetSerTipoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResDetSerTipoServicio() As BEBKCampoResponse
        Get
            Return _DetResDetSerTipoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResDetSerTipoServicio = value
        End Set
    End Property

    Private _DetResDetSerNumeroDocumento As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResDetSerNumeroDocumento() As BEBKCampoResponse
        Get
            Return _DetResDetSerNumeroDocumento
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResDetSerNumeroDocumento = value
        End Set
    End Property

    Private _DetResDetSerReferenciaDeuda As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResDetSerReferenciaDeuda() As BEBKCampoResponse
        Get
            Return _DetResDetSerReferenciaDeuda
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResDetSerReferenciaDeuda = value
        End Set
    End Property

    Private _DetResDetSerFechaVencimiento As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResDetSerFechaVencimiento() As BEBKCampoResponse
        Get
            Return _DetResDetSerFechaVencimiento
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResDetSerFechaVencimiento = value
        End Set
    End Property

    Private _DetResDetSerImporteMinimo As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResDetSerImporteMinimo() As BEBKCampoResponse
        Get
            Return _DetResDetSerImporteMinimo
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResDetSerImporteMinimo = value
        End Set
    End Property

    Private _DetResDetSerImporteTotal As BEBKCampoResponse
    <DataMember()> _
    Public Property DetResDetSerImporteTotal() As BEBKCampoResponse
        Get
            Return _DetResDetSerImporteTotal
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DetResDetSerImporteTotal = value
        End Set
    End Property

#End Region

#End Region

    Public Sub New()
        MessageTypeIdentification = New BEBKCampoResponse(4, BEBKTipoDato.Numerico)
        PrimaryBitMap = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        SecondaryBitMap = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        CodigoProceso = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        Monto = New BEBKCampoResponse(12, BEBKTipoDato.Numerico)
        FechaHoraTransaccion = New BEBKCampoResponse(10, BEBKTipoDato.Numerico)
        Trace = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        FechaCaptura = New BEBKCampoResponse(4, BEBKTipoDato.Numerico)
        BinAdquiriente = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        RetrievalReferenceNumber = New BEBKCampoResponse(12, BEBKTipoDato.Alfanumerico)
        AuthorizationIDResponse = New BEBKCampoResponse(6, BEBKTipoDato.Alfanumerico)
        ResponseCode = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        TerminalID = New BEBKCampoResponse(8, BEBKTipoDato.Alfanumerico)
        TransactionCurrencyCode = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        DatosReservados = New BEBKCampoResponse(5, BEBKTipoDato.Alfanumerico)
        'Cabecera Respuesta
        CabResLongitudCampo = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        CabResCodigoFormato = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        CabResBinProcesador = New BEBKCampoResponse(11, BEBKTipoDato.Alfanumerico)
        CabResBinAcreedor = New BEBKCampoResponse(11, BEBKTipoDato.Alfanumerico)
        CabResCodigoProductoServicio = New BEBKCampoResponse(8, BEBKTipoDato.Alfanumerico)
        CabResAgencia = New BEBKCampoResponse(4, BEBKTipoDato.Alfanumerico)
        CabResTipoIdentificacion = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        CabResNumeroIdentificacion = New BEBKCampoResponse(21, BEBKTipoDato.Alfanumerico)
        CabResNombreDeudor = New BEBKCampoResponse(20, BEBKTipoDato.Alfanumerico)
        CabResNumeroServiciosDevueltos = New BEBKCampoResponse(2, BEBKTipoDato.Numerico)
        CabResNumeroOperacionCobranza = New BEBKCampoResponse(12, BEBKTipoDato.Numerico)
        CabResIndicadorSiHayDocumentos = New BEBKCampoResponse(1, BEBKTipoDato.Numerico)
        CabResTamanoMaximoBloque = New BEBKCampoResponse(5, BEBKTipoDato.Numerico)
        CabResPosicionUltimoDocumento = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        CabResPunteroBaseDatos = New BEBKCampoResponse(10, BEBKTipoDato.Alfanumerico)
        CabResOrigenRespuesta = New BEBKCampoResponse(1, BEBKTipoDato.Alfanumerico)
        CabResCodigoRespuesta = New BEBKCampoResponse(3, BEBKTipoDato.Alfanumerico)
        CabResFiller = New BEBKCampoResponse(10, BEBKTipoDato.Alfanumerico)
        'Detalle Respuesta
        ''Cabecera Servicio
        DetResCabSerCodigoProductoServicio = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        DetResCabSerMoneda = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        DetResCabSerEstadoDeudor = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        DetResCabSerMensaje1Deudor = New BEBKCampoResponse(40, BEBKTipoDato.Alfanumerico)
        DetResCabSerMensaje2Deudor = New BEBKCampoResponse(40, BEBKTipoDato.Alfanumerico)
        DetResCabSerIndicadorCronologia = New BEBKCampoResponse(1, BEBKTipoDato.Numerico)
        DetResCabSerIndicadorPagosVencidos = New BEBKCampoResponse(1, BEBKTipoDato.Numerico)
        DetResCabSerRestriccionPago = New BEBKCampoResponse(1, BEBKTipoDato.Numerico)
        DetResCabSerDocumentosServicio = New BEBKCampoResponse(2, BEBKTipoDato.Numerico)
        DetResCabSerFiller = New BEBKCampoResponse(5, BEBKTipoDato.Alfanumerico)
        ''Detalle Servicio
        DetResDetSerTipoServicio = New BEBKCampoResponse(3, BEBKTipoDato.Alfanumerico)
        DetResDetSerNumeroDocumento = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        DetResDetSerReferenciaDeuda = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        DetResDetSerFechaVencimiento = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        DetResDetSerImporteMinimo = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        DetResDetSerImporteTotal = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
    End Sub

    Public Overrides Function ToString() As String
        Return String.Concat(MessageTypeIdentification.ToString, PrimaryBitMap.ToString, SecondaryBitMap.ToString, _
            CodigoProceso.ToString, Monto.ToString, FechaHoraTransaccion.ToString, Trace.ToString, _
            FechaCaptura.ToString, BinAdquiriente.ToString, RetrievalReferenceNumber.ToString, _
            AuthorizationIDResponse.ToString, ResponseCode.ToString, TerminalID.ToString, _
            TransactionCurrencyCode.ToString, DatosReservados.ToString, CabResLongitudCampo.ToString, _
            CabResCodigoFormato.ToString, CabResBinProcesador.ToString, CabResBinAcreedor.ToString, _
            CabResCodigoProductoServicio.ToString, CabResAgencia.ToString, CabResTipoIdentificacion.ToString, _
            CabResNumeroIdentificacion.ToString, CabResNombreDeudor.ToString, CabResNumeroServiciosDevueltos.ToString, _
            CabResNumeroOperacionCobranza.ToString, CabResIndicadorSiHayDocumentos.ToString, _
            CabResTamanoMaximoBloque.ToString, CabResPosicionUltimoDocumento.ToString, CabResPunteroBaseDatos.ToString, _
            CabResOrigenRespuesta.ToString, CabResCodigoRespuesta.ToString, CabResFiller.ToString, _
            DetResCabSerCodigoProductoServicio.ToString, DetResCabSerMoneda.ToString, DetResCabSerEstadoDeudor.ToString, _
            DetResCabSerMensaje1Deudor.ToString, DetResCabSerMensaje2Deudor.ToString, _
            DetResCabSerIndicadorCronologia.ToString, DetResCabSerIndicadorPagosVencidos.ToString, _
            DetResCabSerRestriccionPago.ToString, DetResCabSerDocumentosServicio.ToString, DetResCabSerFiller.ToString, _
            DetResDetSerTipoServicio.ToString, DetResDetSerNumeroDocumento.ToString, DetResDetSerReferenciaDeuda.ToString, _
            DetResDetSerFechaVencimiento.ToString, DetResDetSerImporteMinimo.ToString, DetResDetSerImporteTotal.ToString)
    End Function

End Class