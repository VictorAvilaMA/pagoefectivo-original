Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BESBKExtornarAnulacionRequest
    Inherits BEBKBaseRequest

    Private _MessageTypeIdentification As BEBKCampoRequest
    <DataMember()> _
    Public Property MessageTypeIdentification() As BEBKCampoRequest
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoRequest
    <DataMember()> _
    Public Property PrimaryBitMap() As BEBKCampoRequest
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoRequest
    <DataMember()> _
    Public Property SecondaryBitMap() As BEBKCampoRequest
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _NumeroTarjeta As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroTarjeta() As BEBKCampoRequest
        Get
            Return _NumeroTarjeta
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroTarjeta = value
        End Set
    End Property

    Private _CodigoProceso As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoProceso() As BEBKCampoRequest
        Get
            Return _CodigoProceso
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoProceso = value
        End Set
    End Property

    Private _Monto As BEBKCampoRequest
    <DataMember()> _
    Public Property Monto() As BEBKCampoRequest
        Get
            Return _Monto
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Monto = value
        End Set
    End Property

    Private _FechaHoraTransaccion As BEBKCampoRequest
    <DataMember()> _
    Public Property FechaHoraTransaccion() As BEBKCampoRequest
        Get
            Return _FechaHoraTransaccion
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _FechaHoraTransaccion = value
        End Set
    End Property

    Private _Trace As BEBKCampoRequest
    <DataMember()> _
    Public Property Trace() As BEBKCampoRequest
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Trace = value
        End Set
    End Property

    Private _FechaCaptura As BEBKCampoRequest
    <DataMember()> _
    Public Property FechaCaptura() As BEBKCampoRequest
        Get
            Return _FechaCaptura
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _FechaCaptura = value
        End Set
    End Property

    Private _ModoIngresoDatos As BEBKCampoRequest
    <DataMember()> _
    Public Property ModoIngresoDatos() As BEBKCampoRequest
        Get
            Return _ModoIngresoDatos
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ModoIngresoDatos = value
        End Set
    End Property

    Private _Canal As BEBKCampoRequest
    <DataMember()> _
    Public Property Canal() As BEBKCampoRequest
        Get
            Return _Canal
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Canal = value
        End Set
    End Property

    Private _BinAdquiriente As BEBKCampoRequest
    <DataMember()> _
    Public Property BinAdquiriente() As BEBKCampoRequest
        Get
            Return _BinAdquiriente
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BinAdquiriente = value
        End Set
    End Property

    Private _ForwardInstitutionCode As BEBKCampoRequest
    <DataMember()> _
    Public Property ForwardInstitutionCode() As BEBKCampoRequest
        Get
            Return _ForwardInstitutionCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ForwardInstitutionCode = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoRequest
    <DataMember()> _
    Public Property RetrievalReferenceNumber() As BEBKCampoRequest
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _ResponseCode As BEBKCampoRequest
    <DataMember()> _
    Public Property ResponseCode() As BEBKCampoRequest
        Get
            Return _ResponseCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _ResponseCode = value
        End Set
    End Property

    Private _TerminalID As BEBKCampoRequest
    <DataMember()> _
    Public Property TerminalID() As BEBKCampoRequest
        Get
            Return _TerminalID
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TerminalID = value
        End Set
    End Property

    Private _Comercio As BEBKCampoRequest
    <DataMember()> _
    Public Property Comercio() As BEBKCampoRequest
        Get
            Return _Comercio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Comercio = value
        End Set
    End Property

    Private _CardAcceptorLocation As BEBKCampoRequest
    <DataMember()> _
    Public Property CardAcceptorLocation() As BEBKCampoRequest
        Get
            Return _CardAcceptorLocation
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CardAcceptorLocation = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoRequest
    <DataMember()> _
    Public Property TransactionCurrencyCode() As BEBKCampoRequest
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _OriDatEleMessageTypeIdentification As BEBKCampoRequest
    <DataMember()> _
    Public Property OriDatEleMessageTypeIdentification() As BEBKCampoRequest
        Get
            Return _OriDatEleMessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _OriDatEleMessageTypeIdentification = value
        End Set
    End Property

    Private _OriDatEleTrace As BEBKCampoRequest
    <DataMember()> _
    Public Property OriDatEleTrace() As BEBKCampoRequest
        Get
            Return _OriDatEleTrace
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _OriDatEleTrace = value
        End Set
    End Property

    Private _OriDatEleFechaHoraTransaccion As BEBKCampoRequest
    <DataMember()> _
    Public Property OriDatEleFechaHoraTransaccion() As BEBKCampoRequest
        Get
            Return _OriDatEleFechaHoraTransaccion
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _OriDatEleFechaHoraTransaccion = value
        End Set
    End Property

    Private _OriDatEleBinAdquiriente As BEBKCampoRequest
    <DataMember()> _
    Public Property OriDatEleBinAdquiriente() As BEBKCampoRequest
        Get
            Return _OriDatEleBinAdquiriente
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _OriDatEleBinAdquiriente = value
        End Set
    End Property

    Private _OriDatEleForwardInstitutionCode As BEBKCampoRequest
    <DataMember()> _
    Public Property OriDatEleForwardInstitutionCode() As BEBKCampoRequest
        Get
            Return _OriDatEleForwardInstitutionCode
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _OriDatEleForwardInstitutionCode = value
        End Set
    End Property

    Private _DatosReservados As BEBKCampoRequest
    <DataMember()> _
    Public Property DatosReservados() As BEBKCampoRequest
        Get
            Return _DatosReservados
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DatosReservados = value
        End Set
    End Property

    Private _LongitudDato As BEBKCampoRequest
    <DataMember()> _
    Public Property LongitudDato() As BEBKCampoRequest
        Get
            Return _LongitudDato
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _LongitudDato = value
        End Set
    End Property

    Private _CodigoFormato As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoFormato() As BEBKCampoRequest
        Get
            Return _CodigoFormato
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoFormato = value
        End Set
    End Property

    Private _BinProcesador As BEBKCampoRequest
    <DataMember()> _
    Public Property BinProcesador() As BEBKCampoRequest
        Get
            Return _BinProcesador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _BinProcesador = value
        End Set
    End Property

    Private _CodigoAcreedor As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoAcreedor() As BEBKCampoRequest
        Get
            Return _CodigoAcreedor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoAcreedor = value
        End Set
    End Property

    Private _CodigoProductoServicio As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoProductoServicio() As BEBKCampoRequest
        Get
            Return _CodigoProductoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoProductoServicio = value
        End Set
    End Property

    Private _CodigoPlazaRecaudador As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoPlazaRecaudador() As BEBKCampoRequest
        Get
            Return _CodigoPlazaRecaudador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoPlazaRecaudador = value
        End Set
    End Property

    Private _CodigoAgenciaRecaudador As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoAgenciaRecaudador() As BEBKCampoRequest
        Get
            Return _CodigoAgenciaRecaudador
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoAgenciaRecaudador = value
        End Set
    End Property

    Private _TipoDatoPago As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoDatoPago() As BEBKCampoRequest
        Get
            Return _TipoDatoPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoDatoPago = value
        End Set
    End Property

    Private _DatoPago As BEBKCampoRequest
    <DataMember()> _
    Public Property DatoPago() As BEBKCampoRequest
        Get
            Return _DatoPago
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _DatoPago = value
        End Set
    End Property

    Private _CodigoCiudad As BEBKCampoRequest
    <DataMember()> _
    Public Property CodigoCiudad() As BEBKCampoRequest
        Get
            Return _CodigoCiudad
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _CodigoCiudad = value
        End Set
    End Property

    Private _Filler As BEBKCampoRequest
    <DataMember()> _
    Public Property Filler() As BEBKCampoRequest
        Get
            Return _Filler
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Filler = value
        End Set
    End Property

    Private _TipoServicio As BEBKCampoRequest
    <DataMember()> _
    Public Property TipoServicio() As BEBKCampoRequest
        Get
            Return _TipoServicio
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _TipoServicio = value
        End Set
    End Property

    Private _NumeroDocumento As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroDocumento() As BEBKCampoRequest
        Get
            Return _NumeroDocumento
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroDocumento = value
        End Set
    End Property

    Private _Disponible As BEBKCampoRequest
    <DataMember()> _
    Public Property Disponible() As BEBKCampoRequest
        Get
            Return _Disponible
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _Disponible = value
        End Set
    End Property

    Private _NumeroTransaccionesCobrOri As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroTransaccionesCobrOri() As BEBKCampoRequest
        Get
            Return _NumeroTransaccionesCobrOri
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroTransaccionesCobrOri = value
        End Set
    End Property

    Private _NumeroOperacionesOriginalAcreedor As BEBKCampoRequest
    <DataMember()> _
    Public Property NumeroOperacionesOriginalAcreedor() As BEBKCampoRequest
        Get
            Return _NumeroOperacionesOriginalAcreedor
        End Get
        Set(ByVal value As BEBKCampoRequest)
            _NumeroOperacionesOriginalAcreedor = value
        End Set
    End Property

    Public Sub New(ByVal Trama As String)
        MyBase.New(Trama)
        MessageTypeIdentification = New BEBKCampoRequest(Trama, 0, 4)
        PrimaryBitMap = New BEBKCampoRequest(Trama, 4, 16)
        SecondaryBitMap = New BEBKCampoRequest(Trama, 20, 16)
        NumeroTarjeta = New BEBKCampoRequest(Trama, 36, 18)
        CodigoProceso = New BEBKCampoRequest(Trama, 54, 6)
        Monto = New BEBKCampoRequest(Trama, 60, 12)
        FechaHoraTransaccion = New BEBKCampoRequest(Trama, 72, 10)
        Trace = New BEBKCampoRequest(Trama, 82, 6)
        FechaCaptura = New BEBKCampoRequest(Trama, 88, 4)
        ModoIngresoDatos = New BEBKCampoRequest(Trama, 92, 3)
        Canal = New BEBKCampoRequest(Trama, 95, 2)
        BinAdquiriente = New BEBKCampoRequest(Trama, 97, 8)
        ForwardInstitutionCode = New BEBKCampoRequest(Trama, 105, 8)
        RetrievalReferenceNumber = New BEBKCampoRequest(Trama, 113, 12)
        ResponseCode = New BEBKCampoRequest(Trama, 125, 2)
        TerminalID = New BEBKCampoRequest(Trama, 127, 8)
        Comercio = New BEBKCampoRequest(Trama, 135, 15)
        CardAcceptorLocation = New BEBKCampoRequest(Trama, 150, 40)
        TransactionCurrencyCode = New BEBKCampoRequest(Trama, 190, 3)
        OriDatEleMessageTypeIdentification = New BEBKCampoRequest(Trama, 193, 4)
        OriDatEleTrace = New BEBKCampoRequest(Trama, 197, 6)
        OriDatEleFechaHoraTransaccion = New BEBKCampoRequest(Trama, 203, 10)
        OriDatEleBinAdquiriente = New BEBKCampoRequest(Trama, 213, 11)
        OriDatEleForwardInstitutionCode = New BEBKCampoRequest(Trama, 224, 11)
        DatosReservados = New BEBKCampoRequest(Trama, 235, 5)
        LongitudDato = New BEBKCampoRequest(Trama, 240, 3)
        CodigoFormato = New BEBKCampoRequest(Trama, 243, 2)
        BinProcesador = New BEBKCampoRequest(Trama, 245, 11)
        CodigoAcreedor = New BEBKCampoRequest(Trama, 256, 11)
        CodigoProductoServicio = New BEBKCampoRequest(Trama, 267, 8)
        CodigoPlazaRecaudador = New BEBKCampoRequest(Trama, 275, 4)
        CodigoAgenciaRecaudador = New BEBKCampoRequest(Trama, 279, 4)
        TipoDatoPago = New BEBKCampoRequest(Trama, 283, 2)
        DatoPago = New BEBKCampoRequest(Trama, 285, 21)
        CodigoCiudad = New BEBKCampoRequest(Trama, 306, 3)
        Filler = New BEBKCampoRequest(Trama, 309, 12)
        TipoServicio = New BEBKCampoRequest(Trama, 321, 3)
        NumeroDocumento = New BEBKCampoRequest(Trama, 324, 16)
        Disponible = New BEBKCampoRequest(Trama, 340, 31)
        NumeroTransaccionesCobrOri = New BEBKCampoRequest(Trama, 371, 12)
        NumeroOperacionesOriginalAcreedor = New BEBKCampoRequest(Trama, 383, 12)
    End Sub

End Class
