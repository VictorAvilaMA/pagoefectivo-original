Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BESBKExtornarCancelacionResponse

    Private _MessageTypeIdentification As BEBKCampoResponse
    <DataMember()> _
    Public Property MessageTypeIdentification() As BEBKCampoResponse
        Get
            Return _MessageTypeIdentification
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _MessageTypeIdentification = value
        End Set
    End Property

    Private _PrimaryBitMap As BEBKCampoResponse
    <DataMember()> _
    Public Property PrimaryBitMap() As BEBKCampoResponse
        Get
            Return _PrimaryBitMap
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _PrimaryBitMap = value
        End Set
    End Property

    Private _SecondaryBitMap As BEBKCampoResponse
    <DataMember()> _
    Public Property SecondaryBitMap() As BEBKCampoResponse
        Get
            Return _SecondaryBitMap
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _SecondaryBitMap = value
        End Set
    End Property

    Private _CodigoProceso As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoProceso() As BEBKCampoResponse
        Get
            Return _CodigoProceso
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoProceso = value
        End Set
    End Property

    Private _Monto As BEBKCampoResponse
    <DataMember()> _
    Public Property Monto() As BEBKCampoResponse
        Get
            Return _Monto
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Monto = value
        End Set
    End Property

    Private _FechaHoraTransaccion As BEBKCampoResponse
    <DataMember()> _
    Public Property FechaHoraTransaccion() As BEBKCampoResponse
        Get
            Return _FechaHoraTransaccion
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _FechaHoraTransaccion = value
        End Set
    End Property

    Private _Trace As BEBKCampoResponse
    <DataMember()> _
    Public Property Trace() As BEBKCampoResponse
        Get
            Return _Trace
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Trace = value
        End Set
    End Property

    Private _FechaCaptura As BEBKCampoResponse
    <DataMember()> _
    Public Property FechaCaptura() As BEBKCampoResponse
        Get
            Return _FechaCaptura
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _FechaCaptura = value
        End Set
    End Property

    Private _IdentificacionEmpresa As BEBKCampoResponse
    <DataMember()> _
    Public Property IdentificacionEmpresa() As BEBKCampoResponse
        Get
            Return _IdentificacionEmpresa
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _IdentificacionEmpresa = value
        End Set
    End Property

    Private _RetrievalReferenceNumber As BEBKCampoResponse
    <DataMember()> _
    Public Property RetrievalReferenceNumber() As BEBKCampoResponse
        Get
            Return _RetrievalReferenceNumber
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _RetrievalReferenceNumber = value
        End Set
    End Property

    Private _AuthorizationIDResponse As BEBKCampoResponse
    <DataMember()> _
    Public Property AuthorizationIDResponse() As BEBKCampoResponse
        Get
            Return _AuthorizationIDResponse
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _AuthorizationIDResponse = value
        End Set
    End Property

    Private _ResponseCode As BEBKCampoResponse
    <DataMember()> _
    Public Property ResponseCode() As BEBKCampoResponse
        Get
            Return _ResponseCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ResponseCode = value
        End Set
    End Property

    Private _TerminalID As BEBKCampoResponse
    <DataMember()> _
    Public Property TerminalID() As BEBKCampoResponse
        Get
            Return _TerminalID
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TerminalID = value
        End Set
    End Property

    Private _TransactionCurrencyCode As BEBKCampoResponse
    <DataMember()> _
    Public Property TransactionCurrencyCode() As BEBKCampoResponse
        Get
            Return _TransactionCurrencyCode
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TransactionCurrencyCode = value
        End Set
    End Property

    Private _DatosReservados As BEBKCampoResponse
    <DataMember()> _
    Public Property DatosReservados() As BEBKCampoResponse
        Get
            Return _DatosReservados
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DatosReservados = value
        End Set
    End Property

    Private _TamanoBloque As BEBKCampoResponse
    <DataMember()> _
    Public Property TamanoBloque() As BEBKCampoResponse
        Get
            Return _TamanoBloque
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TamanoBloque = value
        End Set
    End Property

    Private _CodigoFormato As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoFormato() As BEBKCampoResponse
        Get
            Return _CodigoFormato
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoFormato = value
        End Set
    End Property

    Private _BinProcesador As BEBKCampoResponse
    <DataMember()> _
    Public Property BinProcesador() As BEBKCampoResponse
        Get
            Return _BinProcesador
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _BinProcesador = value
        End Set
    End Property

    Private _CodigoAcreedor As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoAcreedor() As BEBKCampoResponse
        Get
            Return _CodigoAcreedor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoAcreedor = value
        End Set
    End Property

    Private _CodigoProductoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoProductoServicio() As BEBKCampoResponse
        Get
            Return _CodigoProductoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoProductoServicio = value
        End Set
    End Property

    Private _CodigoPlazaRecaudador As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoPlazaRecaudador() As BEBKCampoResponse
        Get
            Return _CodigoPlazaRecaudador
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoPlazaRecaudador = value
        End Set
    End Property

    Private _CodigoAgenciaRecaudador As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoAgenciaRecaudador() As BEBKCampoResponse
        Get
            Return _CodigoAgenciaRecaudador
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoAgenciaRecaudador = value
        End Set
    End Property

    Private _TipoDatoPago As BEBKCampoResponse
    <DataMember()> _
    Public Property TipoDatoPago() As BEBKCampoResponse
        Get
            Return _TipoDatoPago
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TipoDatoPago = value
        End Set
    End Property

    Private _DatoPago As BEBKCampoResponse
    <DataMember()> _
    Public Property DatoPago() As BEBKCampoResponse
        Get
            Return _DatoPago
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DatoPago = value
        End Set
    End Property

    Private _CodigoCiudad As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoCiudad() As BEBKCampoResponse
        Get
            Return _CodigoCiudad
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoCiudad = value
        End Set
    End Property

    Private _NumeroOperacionCobranza As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroOperacionCobranza() As BEBKCampoResponse
        Get
            Return _NumeroOperacionCobranza
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroOperacionCobranza = value
        End Set
    End Property

    Private _NumeroOperacionAcreedor As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroOperacionAcreedor() As BEBKCampoResponse
        Get
            Return _NumeroOperacionAcreedor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroOperacionAcreedor = value
        End Set
    End Property

    Private _NumeroProductoServicioPagado As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroProductoServicioPagado() As BEBKCampoResponse
        Get
            Return _NumeroProductoServicioPagado
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroProductoServicioPagado = value
        End Set
    End Property

    Private _NumeroTotalDocumentosPagados As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroTotalDocumentosPagados() As BEBKCampoResponse
        Get
            Return _NumeroTotalDocumentosPagados
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroTotalDocumentosPagados = value
        End Set
    End Property

    Private _Filler As BEBKCampoResponse
    <DataMember()> _
    Public Property Filler() As BEBKCampoResponse
        Get
            Return _Filler
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Filler = value
        End Set
    End Property

    Private _OrigenRespuesta As BEBKCampoResponse
    <DataMember()> _
    Public Property OrigenRespuesta() As BEBKCampoResponse
        Get
            Return _OrigenRespuesta
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _OrigenRespuesta = value
        End Set
    End Property

    Private _CodigoRespuestaExtendida As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoRespuestaExtendida() As BEBKCampoResponse
        Get
            Return _CodigoRespuestaExtendida
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoRespuestaExtendida = value
        End Set
    End Property

    Private _DescripcionRespuestaAplicativa As BEBKCampoResponse
    <DataMember()> _
    Public Property DescripcionRespuestaAplicativa() As BEBKCampoResponse
        Get
            Return _DescripcionRespuestaAplicativa
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DescripcionRespuestaAplicativa = value
        End Set
    End Property

    Private _NombreDeudor As BEBKCampoResponse
    <DataMember()> _
    Public Property NombreDeudor() As BEBKCampoResponse
        Get
            Return _NombreDeudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NombreDeudor = value
        End Set
    End Property

    Private _RUCDeudor As BEBKCampoResponse
    <DataMember()> _
    Public Property RUCDeudor() As BEBKCampoResponse
        Get
            Return _RUCDeudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _RUCDeudor = value
        End Set
    End Property

    Private _RUCAcreedor As BEBKCampoResponse
    <DataMember()> _
    Public Property RUCAcreedor() As BEBKCampoResponse
        Get
            Return _RUCAcreedor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _RUCAcreedor = value
        End Set
    End Property

    Private _CodigoZonaDeudor As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoZonaDeudor() As BEBKCampoResponse
        Get
            Return _CodigoZonaDeudor
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoZonaDeudor = value
        End Set
    End Property

    Private _Filler2 As BEBKCampoResponse
    <DataMember()> _
    Public Property Filler2() As BEBKCampoResponse
        Get
            Return _Filler2
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Filler2 = value
        End Set
    End Property

    Private _CodigoProductoServicio2 As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoProductoServicio2() As BEBKCampoResponse
        Get
            Return _CodigoProductoServicio2
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoProductoServicio2 = value
        End Set
    End Property

    Private _DescripcionProductoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property DescripcionProductoServicio() As BEBKCampoResponse
        Get
            Return _DescripcionProductoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DescripcionProductoServicio = value
        End Set
    End Property

    Private _ImporteTotalProductoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property ImporteTotalProductoServicio() As BEBKCampoResponse
        Get
            Return _ImporteTotalProductoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImporteTotalProductoServicio = value
        End Set
    End Property

    Private _Mensaje1 As BEBKCampoResponse
    <DataMember()> _
    Public Property Mensaje1() As BEBKCampoResponse
        Get
            Return _Mensaje1
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Mensaje1 = value
        End Set
    End Property

    Private _Mensaje2 As BEBKCampoResponse
    <DataMember()> _
    Public Property Mensaje2() As BEBKCampoResponse
        Get
            Return _Mensaje2
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Mensaje2 = value
        End Set
    End Property

    Private _NumeroDocumentos As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroDocumentos() As BEBKCampoResponse
        Get
            Return _NumeroDocumentos
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroDocumentos = value
        End Set
    End Property

    Private _Filler3 As BEBKCampoResponse
    <DataMember()> _
    Public Property Filler3() As BEBKCampoResponse
        Get
            Return _Filler3
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Filler3 = value
        End Set
    End Property

    Private _TipoServicio As BEBKCampoResponse
    <DataMember()> _
    Public Property TipoServicio() As BEBKCampoResponse
        Get
            Return _TipoServicio
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TipoServicio = value
        End Set
    End Property

    Private _DescripcionDocumento As BEBKCampoResponse
    <DataMember()> _
    Public Property DescripcionDocumento() As BEBKCampoResponse
        Get
            Return _DescripcionDocumento
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _DescripcionDocumento = value
        End Set
    End Property

    Private _NumeroDocumento As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroDocumento() As BEBKCampoResponse
        Get
            Return _NumeroDocumento
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroDocumento = value
        End Set
    End Property

    Private _PeriodoCotizacion As BEBKCampoResponse
    <DataMember()> _
    Public Property PeriodoCotizacion() As BEBKCampoResponse
        Get
            Return _PeriodoCotizacion
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _PeriodoCotizacion = value
        End Set
    End Property

    Private _TipoDocumentoIdentidad As BEBKCampoResponse
    <DataMember()> _
    Public Property TipoDocumentoIdentidad() As BEBKCampoResponse
        Get
            Return _TipoDocumentoIdentidad
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _TipoDocumentoIdentidad = value
        End Set
    End Property

    Private _NumeroDocumentoIdentidad As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroDocumentoIdentidad() As BEBKCampoResponse
        Get
            Return _NumeroDocumentoIdentidad
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroDocumentoIdentidad = value
        End Set
    End Property

    Private _FechaEmision As BEBKCampoResponse
    <DataMember()> _
    Public Property FechaEmision() As BEBKCampoResponse
        Get
            Return _FechaEmision
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _FechaEmision = value
        End Set
    End Property

    Private _FechaVencimiento As BEBKCampoResponse
    <DataMember()> _
    Public Property FechaVencimiento() As BEBKCampoResponse
        Get
            Return _FechaVencimiento
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _FechaVencimiento = value
        End Set
    End Property

    Private _ImportePagado As BEBKCampoResponse
    <DataMember()> _
    Public Property ImportePagado() As BEBKCampoResponse
        Get
            Return _ImportePagado
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImportePagado = value
        End Set
    End Property

    Private _CodigoConcepto1 As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoConcepto1() As BEBKCampoResponse
        Get
            Return _CodigoConcepto1
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoConcepto1 = value
        End Set
    End Property

    Private _ImporteConcepto1 As BEBKCampoResponse
    <DataMember()> _
    Public Property ImporteConcepto1() As BEBKCampoResponse
        Get
            Return _ImporteConcepto1
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImporteConcepto1 = value
        End Set
    End Property

    Private _CodigoConcepto2 As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoConcepto2() As BEBKCampoResponse
        Get
            Return _CodigoConcepto2
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoConcepto2 = value
        End Set
    End Property

    Private _ImporteConcepto2 As BEBKCampoResponse
    <DataMember()> _
    Public Property ImporteConcepto2() As BEBKCampoResponse
        Get
            Return _ImporteConcepto2
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImporteConcepto2 = value
        End Set
    End Property

    Private _CodigoConcepto3 As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoConcepto3() As BEBKCampoResponse
        Get
            Return _CodigoConcepto3
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoConcepto3 = value
        End Set
    End Property

    Private _ImporteConcepto3 As BEBKCampoResponse
    <DataMember()> _
    Public Property ImporteConcepto3() As BEBKCampoResponse
        Get
            Return _ImporteConcepto3
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImporteConcepto3 = value
        End Set
    End Property

    Private _CodigoConcepto4 As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoConcepto4() As BEBKCampoResponse
        Get
            Return _CodigoConcepto4
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoConcepto4 = value
        End Set
    End Property

    Private _ImporteConcepto4 As BEBKCampoResponse
    <DataMember()> _
    Public Property ImporteConcepto4() As BEBKCampoResponse
        Get
            Return _ImporteConcepto4
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImporteConcepto4 = value
        End Set
    End Property

    Private _CodigoConcepto5 As BEBKCampoResponse
    <DataMember()> _
    Public Property CodigoConcepto5() As BEBKCampoResponse
        Get
            Return _CodigoConcepto5
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _CodigoConcepto5 = value
        End Set
    End Property

    Private _ImporteConcepto5 As BEBKCampoResponse
    <DataMember()> _
    Public Property ImporteConcepto5() As BEBKCampoResponse
        Get
            Return _ImporteConcepto5
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ImporteConcepto5 = value
        End Set
    End Property

    Private _IndicadorFacturacion As BEBKCampoResponse
    <DataMember()> _
    Public Property IndicadorFacturacion() As BEBKCampoResponse
        Get
            Return _IndicadorFacturacion
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _IndicadorFacturacion = value
        End Set
    End Property

    Private _NumeroFactura As BEBKCampoResponse
    <DataMember()> _
    Public Property NumeroFactura() As BEBKCampoResponse
        Get
            Return _NumeroFactura
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _NumeroFactura = value
        End Set
    End Property

    Private _ReferenciaDeuda As BEBKCampoResponse
    <DataMember()> _
    Public Property ReferenciaDeuda() As BEBKCampoResponse
        Get
            Return _ReferenciaDeuda
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _ReferenciaDeuda = value
        End Set
    End Property

    Private _Filler4 As BEBKCampoResponse
    <DataMember()> _
    Public Property Filler4() As BEBKCampoResponse
        Get
            Return _Filler4
        End Get
        Set(ByVal value As BEBKCampoResponse)
            _Filler4 = value
        End Set
    End Property

    Public Sub New()
        MessageTypeIdentification = New BEBKCampoResponse(4, BEBKTipoDato.Numerico)
        PrimaryBitMap = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        SecondaryBitMap = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        CodigoProceso = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        Monto = New BEBKCampoResponse(12, BEBKTipoDato.Numerico)
        FechaHoraTransaccion = New BEBKCampoResponse(10, BEBKTipoDato.Numerico)
        Trace = New BEBKCampoResponse(6, BEBKTipoDato.Numerico)
        FechaCaptura = New BEBKCampoResponse(4, BEBKTipoDato.Numerico)
        IdentificacionEmpresa = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        RetrievalReferenceNumber = New BEBKCampoResponse(12, BEBKTipoDato.Alfanumerico)
        AuthorizationIDResponse = New BEBKCampoResponse(6, BEBKTipoDato.Alfanumerico)
        ResponseCode = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        TerminalID = New BEBKCampoResponse(8, BEBKTipoDato.Alfanumerico)
        TransactionCurrencyCode = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        DatosReservados = New BEBKCampoResponse(5, BEBKTipoDato.Alfanumerico)
        TamanoBloque = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        CodigoFormato = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        BinProcesador = New BEBKCampoResponse(11, BEBKTipoDato.Alfanumerico)
        CodigoAcreedor = New BEBKCampoResponse(11, BEBKTipoDato.Alfanumerico)
        CodigoProductoServicio = New BEBKCampoResponse(8, BEBKTipoDato.Alfanumerico)
        CodigoPlazaRecaudador = New BEBKCampoResponse(4, BEBKTipoDato.Alfanumerico)
        CodigoAgenciaRecaudador = New BEBKCampoResponse(4, BEBKTipoDato.Alfanumerico)
        TipoDatoPago = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        DatoPago = New BEBKCampoResponse(21, BEBKTipoDato.Alfanumerico)
        CodigoCiudad = New BEBKCampoResponse(3, BEBKTipoDato.Alfanumerico)
        NumeroOperacionCobranza = New BEBKCampoResponse(12, BEBKTipoDato.Alfanumerico)
        NumeroOperacionAcreedor = New BEBKCampoResponse(12, BEBKTipoDato.Alfanumerico)
        NumeroProductoServicioPagado = New BEBKCampoResponse(2, BEBKTipoDato.Numerico)
        NumeroTotalDocumentosPagados = New BEBKCampoResponse(3, BEBKTipoDato.Numerico)
        Filler = New BEBKCampoResponse(10, BEBKTipoDato.Alfanumerico)
        OrigenRespuesta = New BEBKCampoResponse(1, BEBKTipoDato.Alfanumerico)
        CodigoRespuestaExtendida = New BEBKCampoResponse(3, BEBKTipoDato.Alfanumerico)
        DescripcionRespuestaAplicativa = New BEBKCampoResponse(30, BEBKTipoDato.Alfanumerico)
        NombreDeudor = New BEBKCampoResponse(20, BEBKTipoDato.Alfanumerico)
        RUCDeudor = New BEBKCampoResponse(15, BEBKTipoDato.Alfanumerico)
        RUCAcreedor = New BEBKCampoResponse(15, BEBKTipoDato.Alfanumerico)
        CodigoZonaDeudor = New BEBKCampoResponse(6, BEBKTipoDato.Alfanumerico)
        Filler2 = New BEBKCampoResponse(20, BEBKTipoDato.Alfanumerico)
        CodigoProductoServicio2 = New BEBKCampoResponse(3, BEBKTipoDato.Alfanumerico)
        DescripcionProductoServicio = New BEBKCampoResponse(15, BEBKTipoDato.Alfanumerico)
        ImporteTotalProductoServicio = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        Mensaje1 = New BEBKCampoResponse(40, BEBKTipoDato.Alfanumerico)
        Mensaje2 = New BEBKCampoResponse(40, BEBKTipoDato.Alfanumerico)
        NumeroDocumentos = New BEBKCampoResponse(2, BEBKTipoDato.Numerico)
        Filler3 = New BEBKCampoResponse(20, BEBKTipoDato.Alfanumerico)
        TipoServicio = New BEBKCampoResponse(3, BEBKTipoDato.Alfanumerico)
        DescripcionDocumento = New BEBKCampoResponse(15, BEBKTipoDato.Alfanumerico)
        NumeroDocumento = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        PeriodoCotizacion = New BEBKCampoResponse(6, BEBKTipoDato.Alfanumerico)
        TipoDocumentoIdentidad = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        NumeroDocumentoIdentidad = New BEBKCampoResponse(15, BEBKTipoDato.Alfanumerico)
        FechaEmision = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        FechaVencimiento = New BEBKCampoResponse(8, BEBKTipoDato.Numerico)
        ImportePagado = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        CodigoConcepto1 = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        ImporteConcepto1 = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        CodigoConcepto2 = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        ImporteConcepto2 = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        CodigoConcepto3 = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        ImporteConcepto3 = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        CodigoConcepto4 = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        ImporteConcepto4 = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        CodigoConcepto5 = New BEBKCampoResponse(2, BEBKTipoDato.Alfanumerico)
        ImporteConcepto5 = New BEBKCampoResponse(11, BEBKTipoDato.Numerico)
        IndicadorFacturacion = New BEBKCampoResponse(1, BEBKTipoDato.Alfanumerico)
        NumeroFactura = New BEBKCampoResponse(11, BEBKTipoDato.Alfanumerico)
        ReferenciaDeuda = New BEBKCampoResponse(16, BEBKTipoDato.Alfanumerico)
        Filler4 = New BEBKCampoResponse(34, BEBKTipoDato.Alfanumerico)
    End Sub

    Public Overrides Function ToString() As String
        Return MessageTypeIdentification.ToString + PrimaryBitMap.ToString + SecondaryBitMap.ToString + _
            CodigoProceso.ToString + Monto.ToString + FechaHoraTransaccion.ToString + Trace.ToString + _
            FechaCaptura.ToString + IdentificacionEmpresa.ToString + RetrievalReferenceNumber.ToString + _
            AuthorizationIDResponse.ToString + ResponseCode.ToString + TerminalID.ToString + _
            TransactionCurrencyCode.ToString + DatosReservados.ToString + TamanoBloque.ToString + _
            CodigoFormato.ToString + BinProcesador.ToString + CodigoAcreedor.ToString + _
            CodigoProductoServicio.ToString + CodigoPlazaRecaudador.ToString + CodigoAgenciaRecaudador.ToString + _
            TipoDatoPago.ToString + DatoPago.ToString + CodigoCiudad.ToString + NumeroOperacionCobranza.ToString + _
            NumeroOperacionAcreedor.ToString + NumeroProductoServicioPagado.ToString + NumeroTotalDocumentosPagados.ToString + _
            Filler.ToString + OrigenRespuesta.ToString + CodigoRespuestaExtendida.ToString + _
            DescripcionRespuestaAplicativa.ToString + NombreDeudor.ToString + RUCDeudor.ToString + _
            RUCAcreedor.ToString + CodigoZonaDeudor.ToString + Filler2.ToString + CodigoProductoServicio2.ToString + _
            DescripcionProductoServicio.ToString + ImporteTotalProductoServicio.ToString + Mensaje1.ToString + _
            Mensaje2.ToString + NumeroDocumentos.ToString + Filler3.ToString + TipoServicio.ToString + _
            DescripcionDocumento.ToString + NumeroDocumento.ToString + PeriodoCotizacion.ToString + _
            TipoDocumentoIdentidad.ToString + NumeroDocumentoIdentidad.ToString + FechaEmision.ToString + _
            FechaVencimiento.ToString + ImportePagado.ToString + CodigoConcepto1.ToString + ImporteConcepto1.ToString + _
            CodigoConcepto2.ToString + ImporteConcepto2.ToString + CodigoConcepto3.ToString + ImporteConcepto3.ToString + _
            CodigoConcepto4.ToString + ImporteConcepto4.ToString + CodigoConcepto5.ToString + ImporteConcepto5.ToString + _
            IndicadorFacturacion.ToString + NumeroFactura.ToString + ReferenciaDeuda.ToString + Filler4.ToString
    End Function

End Class
