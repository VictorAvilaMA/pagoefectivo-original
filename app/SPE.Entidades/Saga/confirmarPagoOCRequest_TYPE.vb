﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class confirmarPagoOCRequest_TYPE
    Private _OrderId As String
    <DataMember()>
    Property OrderId As String
        Get
            Return _OrderId
        End Get
        Set(value As String)
            _OrderId = value
        End Set
    End Property
    Private _FechaPago As String
    <DataMember()>
    Property FechaPago As String
        Get
            Return _FechaPago
        End Get
        Set(value As String)
            _FechaPago = value
        End Set
    End Property
    Private _FechaContable As String
    <DataMember()>
    Property FechaContable As String
        Get
            Return _FechaContable
        End Get
        Set(value As String)
            _FechaContable = value
        End Set
    End Property
    Private _MontoCancelado As Decimal
    <DataMember()>
    Property MontoCancelado As Decimal
        Get
            Return _MontoCancelado
        End Get
        Set(value As Decimal)
            _MontoCancelado = value
        End Set
    End Property
    Private _Canal As String
    <DataMember()>
    Property Canal As String
        Get
            Return _Canal
        End Get
        Set(value As String)
            _Canal = value
        End Set
    End Property
    Private _Caja As String
    <DataMember()>
    Property Caja As String
        Get
            Return _Caja
        End Get
        Set(value As String)
            _Caja = value
        End Set
    End Property
    Private _CodigoAutorizacion As String
    <DataMember()>
    Property CodigoAutorizacion As String
        Get
            Return _CodigoAutorizacion
        End Get
        Set(value As String)
            _CodigoAutorizacion = value
        End Set
    End Property
End Class
