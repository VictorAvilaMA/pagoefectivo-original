﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class confirmarPagoOCResponse_TYPE
    Private _CodigoRespuesta As Integer
    <DataMember()>
    Property CodigoRespuesta As Integer
        Get
            Return _CodigoRespuesta
        End Get
        Set(value As Integer)
            _CodigoRespuesta = value
        End Set
    End Property
    Private _GlosaRespuesta As String
    <DataMember()>
    Property GlosaRespuesta As String
        Get
            Return _GlosaRespuesta
        End Get
        Set(value As String)
            _GlosaRespuesta = value
        End Set
    End Property
End Class
