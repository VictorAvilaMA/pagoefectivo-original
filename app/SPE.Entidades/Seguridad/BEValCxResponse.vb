Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BEValCxResponse

    Private _idServicio As Integer
    <DataMember()> _
    Public Property IdServicio() As Integer
        Get
            Return _idServicio
        End Get
        Set(ByVal value As Integer)
            _idServicio = value
        End Set
    End Property

    Private _idEmpresaContratante As Integer
    <DataMember()> _
    Public Property IdEmpresaContratante() As Integer
        Get
            Return _idEmpresaContratante
        End Get
        Set(ByVal value As Integer)
            _idEmpresaContratante = value
        End Set
    End Property


End Class
