﻿Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BELogWebServiceSodimacRequest
    Private _OrdenPago As String
    <DataMember()> _
    Public Property OrdenPago() As String
        Get
            Return _OrdenPago
        End Get
        Set(ByVal value As String)
            _OrdenPago = value
        End Set
    End Property

    Private _OrderIdSodimac As String
    <DataMember()> _
    Public Property OrderIdSodimac() As String
        Get
            Return _OrderIdSodimac
        End Get
        Set(ByVal value As String)
            _OrderIdSodimac = value
        End Set
    End Property

    Private _FechaPago As DateTime
    <DataMember()> _
    Public Property FechaPago() As DateTime
        Get
            Return _FechaPago
        End Get
        Set(ByVal value As DateTime)
            _FechaPago = value
        End Set
    End Property

    Private _FechaContable As DateTime
    <DataMember()> _
    Public Property FechaContable() As DateTime
        Get
            Return _FechaContable
        End Get
        Set(ByVal value As DateTime)
            _FechaContable = value
        End Set
    End Property

    Private _MontoCancelado As Decimal
    <DataMember()> _
    Public Property MontoCancelado() As Decimal
        Get
            Return _MontoCancelado
        End Get
        Set(ByVal value As Decimal)
            _MontoCancelado = value
        End Set
    End Property

    Private _CajaSodimac As String
    <DataMember()> _
    Public Property CajaSodimac() As String
        Get
            Return _CajaSodimac
        End Get
        Set(ByVal value As String)
            _CajaSodimac = value
        End Set
    End Property

    Private _FechaRegistro As String
    <DataMember()> _
    Public Property FechaRegistro() As String
        Get
            Return _FechaRegistro
        End Get
        Set(ByVal value As String)
            _FechaRegistro = value
        End Set
    End Property

    Private _WebServiceSodimacURL As String
    <DataMember()> _
    Public Property WebServiceSodimacURL() As String
        Get
            Return _WebServiceSodimacURL
        End Get
        Set(ByVal value As String)
            _WebServiceSodimacURL = value
        End Set
    End Property
    Private _TramaXML As String
    <DataMember()> _
    Public Property TramaXML() As String
        Get
            Return _TramaXML
        End Get
        Set(ByVal value As String)
            _TramaXML = value
        End Set
    End Property
End Class
