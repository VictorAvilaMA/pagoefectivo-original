﻿Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class BELogWebServiceSodimacResponse

    Private _LogIDRequest As Integer
    <DataMember()> _
    Public Property LogIDRequest() As Integer
        Get
            Return _LogIDRequest
        End Get
        Set(ByVal value As Integer)
            _LogIDRequest = value
        End Set
    End Property

    Private _OrdenPago As String
    <DataMember()> _
    Public Property OrdenPago() As String
        Get
            Return _OrdenPago
        End Get
        Set(ByVal value As String)
            _OrdenPago = value
        End Set
    End Property

    Private _OrderIdSodimac As String
    <DataMember()> _
    Public Property OrderIdSodimac() As String
        Get
            Return _OrderIdSodimac
        End Get
        Set(ByVal value As String)
            _OrderIdSodimac = value
        End Set
    End Property

    Private _CodigoRespuesta As String
    <DataMember()> _
    Public Property CodigoRespuesta() As String
        Get
            Return _CodigoRespuesta
        End Get
        Set(ByVal value As String)
            _CodigoRespuesta = value
        End Set
    End Property

    Private _GlosaRespuesta As String
    <DataMember()> _
    Public Property GlosaRespuesta() As String
        Get
            Return _GlosaRespuesta
        End Get
        Set(ByVal value As String)
            _GlosaRespuesta = value
        End Set
    End Property

    Private _MensajeError As String
    <DataMember()> _
    Public Property MensajeError() As String
        Get
            Return _MensajeError
        End Get
        Set(ByVal value As String)
            _MensajeError = value
        End Set
    End Property

    Private _WebServiceSodimacURL As String
    <DataMember()> _
    Public Property WebServiceSodimacURL() As String
        Get
            Return _WebServiceSodimacURL
        End Get
        Set(ByVal value As String)
            _WebServiceSodimacURL = value
        End Set
    End Property

    Private _xml As String
    <DataMember()> _
    Public Property Xml() As String
        Get
            Return _xml
        End Get
        Set(ByVal value As String)
            _xml = value
        End Set
    End Property

End Class
