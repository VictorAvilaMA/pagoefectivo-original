﻿Imports System.Runtime.Serialization

<Serializable()> <DataContract()> Public Class BETrackCip

    <DataMember(Name:="cip")> _
    Public Property IdOrdenPago As Int64
    <DataMember(Name:="solictudId")> _
    Public Property IdSolicitudPago As Int64
    <DataMember(Name:="currency")> _
    Public Property Moneda As String
    <DataMember(Name:="amount")> _
    Public Property Total As String
    <DataMember(Name:="transactionCode")> _
    Public Property CodTransaccion As String
    <DataMember(Name:="dateCreation")> _
    Public Property FechaCreacion As String
    <DataMember(Name:="timeExpiry")> _
    Public Property TiempoExpiracion As Double
    <DataMember(Name:="serviceId")> _
    Public Property IdServicio As Integer
    <DataMember(Name:="serviceCode")> _
    Public Property CodigoServicio As String
    <DataMember(Name:="serviceName")> _
    Public Property NombreServicio As String
    <DataMember(Name:="companyId")> _
    Public Property IdEmpresa As Integer
    <DataMember(Name:="userEmail")> _
    Public Property EmailUsuario As String
    <DataMember(Name:="dateGenerateCipStart")> _
    Public Property FechaInicio As String
    <DataMember(Name:="dateGenerateCipEnd")> _
    Public Property FechaFin As String
    <DataMember(Name:="timeGenerateCip")> _
    Public Property TiempoGeneracion As Double
    <DataMember(Name:="origin")> _
    Public Property Origen As Integer
End Class
