Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class EsquemaTrama

    Public Const ACodigo As String = "codigo"
    Public Const AAPI As String = "api"
    Public Const AUsaEncriptacion As String = "usaencriptacion"
    Public Const ATipoRecepcion As String = "tipoRecepcion"
    Public Const AUrl As String = "url"
    Public Const ADescripcion As String = "descripcion"
    Public Const AHome As String = "home"

    Public Const EDatosEnc As String = "DatosEnc"
    Public Const EMerchantId As String = "MerchantID"
    Public Const EOrdenId As String = "OrdenId"
    Public Const EUrlOk As String = "UrlOk"
    Public Const EUrlError As String = "UrlError"
    Public Const EMailCom As String = "MailCom"
    Public Const EOrdenDesc As String = "OrdenDesc"
    Public Const EMoneda As String = "Moneda"
    Public Const EMonto As String = "Monto"
    Public Const EFechaAExpirar As String = "FechaAExpirar"
    Public Const EUsuarioID As String = "UsuarioID"
    Public Const EDataAdicional As String = "DataAdicional"
    Public Const EUsuarioNombre As String = "UsuarioNombre"
    Public Const EUsuarioApellidos As String = "UsuarioApellidos"
    Public Const EUsuarioLocalidad As String = "UsuarioLocalidad"
    Public Const EUsuarioDomicilio As String = "UsuarioDomicilio"
    Public Const EUsuarioProvincia As String = "UsuarioProvincia"
    Public Const EUsuarioPais As String = "UsuarioPais"
    Public Const EUsuarioAlias As String = "UsuarioAlias"
    Public Const EUsuarioEmail As String = "UsuarioEmail"

    Public Const EDetalles As String = "Detalles"
    Public Const EDetalles_Detalle As String = "Detalles_Detalle"
    Public Const EDetalles_Detalle_CodigoOrigen As String = "Detalles_Detalle_CodigoOrigen"
    Public Const EDetalles_Detalle_TipoOrigen As String = "Detalles_Detalle_TipoOrigen"
    Public Const EDetalles_Detalle_ConceptoPago As String = "Detalles_Detalle_ConceptoPago"
    Public Const EDetalles_Detalle_Importe As String = "Detalles_Detalle_Importe"
    Public Const EDetalles_Detalle_Campo1 As String = "Detalles_Detalle_Campo1"
    Public Const EDetalles_Detalle_Campo2 As String = "Detalles_Detalle_Campo2"
    Public Const EDetalles_Detalle_Campo3 As String = "Detalles_Detalle_Campo3"
    ''
    Public Const EParUtil As String = "parUtil"
    Public Const EUsaEncriptacion As String = "usaencriptacion"

    Public Const PCClave As String = "cclave"
    Public Const PTipoTarjeta As String = "tipotarjeta"
    Public Const PMonedaId As String = "monedaid"
    Public Const PEmail As String = "email"
    Public Const PNombres As String = "nombres"
    Public Const PApellidos As String = "apellidos"

    'control de cambio

    Public Sub New()

    End Sub

    Private _codigoApi As String = ""
    Public Property CodigoApi() As String
        Get
            Return _codigoApi
        End Get
        Set(ByVal value As String)
            _codigoApi = value
        End Set
    End Property

    Private _usaEncriptacion As Boolean = False
    Public Property UsaEncriptacion() As Boolean
        Get
            Return _usaEncriptacion
        End Get
        Set(ByVal value As Boolean)
            _usaEncriptacion = value
        End Set
    End Property

    Private _tipoRecepcion As String = ""
    Public Property TipoRecepcion() As String
        Get
            Return _tipoRecepcion
        End Get
        Set(ByVal value As String)
            _tipoRecepcion = value
        End Set
    End Property

    Private _url As String = ""
    Public Property URL() As String
        Get
            Return _url
        End Get
        Set(ByVal value As String)
            _url = value
        End Set
    End Property

    Private _descripcion As String = ""
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Private _home As String = ""
    Public Property Home() As String
        Get
            Return _home
        End Get
        Set(ByVal value As String)
            _home = value
        End Set
    End Property

    Private _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = Nothing
    Public Property EsquemaNodo() As Dictionary(Of String, EsquemaTramaNodo)
        Get
            Return _esquemaNodo
        End Get
        Set(ByVal value As Dictionary(Of String, EsquemaTramaNodo))
            _esquemaNodo = value
        End Set
    End Property

End Class
