Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class EsquemaTramaNodo

    '<add Peru.Com FaseIII>
    Public Const aNombre As String = "nombre"
    Public Const aRequerido As String = "requerido"
    Public Const aPorDefecto As String = "porDefecto"
    Private _nodosHijos As Dictionary(Of String, EsquemaTramaNodo)
    <DataMember()> _
    Public Property NodosHijos() As Dictionary(Of String, EsquemaTramaNodo)
        Get
            Return _nodosHijos
        End Get
        Set(ByVal value As Dictionary(Of String, EsquemaTramaNodo))
            _nodosHijos = value
        End Set
    End Property

    Public Function TieneHijos() As Boolean
        If Me._nodosHijos Is Nothing Then
            Return False
        End If
        Return Me._nodosHijos.Count > 0
    End Function
    '</add Peru.Com FaseIII>


    Public Sub New()

    End Sub

    Private _nodoId As String
    <DataMember()> _
    Public Property NodoId() As String
        Get
            Return _nodoId
        End Get
        Set(ByVal value As String)
            _nodoId = value
        End Set
    End Property

    Private _nombre As String
    <DataMember()> _
    Public Property Nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Private _valorPorDefecto As String
    <DataMember()> _
    Public Property ValorPorDefecto() As String
        Get
            Return _valorPorDefecto
        End Get
        Set(ByVal value As String)
            _valorPorDefecto = value
        End Set
    End Property

    Private _requerido As Boolean
    <DataMember()> _
    Public Property Requerido() As Boolean
        Get
            Return _requerido
        End Get
        Set(ByVal value As Boolean)
            _requerido = value
        End Set
    End Property

    'Private _SeUtiliza As Boolean
    'Public Property SeUtiliza() As Boolean
    '    Get
    '        Return _SeUtiliza
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _SeUtiliza = value
    '    End Set
    'End Property


End Class
