﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Runtime.Serialization

<Serializable()> _
<DataContract()> _
Public Class EsquemaTramaV2

    Public Const ACodigo As String = "codigo"
    Public Const AAPI As String = "api"
    Public Const AUsaEncriptacion As String = "usaencriptacion"
    Public Const ATipoRecepcion As String = "tipoRecepcion"
    Public Const AUrl As String = "url"
    Public Const ADescripcion As String = "descripcion"
    Public Const AHome As String = "home"

    Public Const EIdMoneda As String = "IdMoneda"
    Public Const ETotal As String = "Total"
    Public Const EMetodosPago As String = "MetodosPago"
    Public Const ECodServicio As String = "CodServicio"
    Public Const ECodtransaccion As String = "Codtransaccion"
    Public Const EEmailComercio As String = "EmailComercio"
    Public Const EFechaAExpirar As String = "FechaAExpirar"
    Public Const EUsuarioId As String = "UsuarioId"
    Public Const EDataAdicional As String = "DataAdicional"
    Public Const EUsuarioNombre As String = "UsuarioNombre"
    Public Const EUsuarioApellidos As String = "UsuarioApellidos"
    Public Const EUsuarioLocalidad As String = "UsuarioLocalidad"
    Public Const EUsuarioProvincia As String = "UsuarioProvincia"
    Public Const EUsuarioPais As String = "UsuarioPais"
    Public Const EUsuarioAlias As String = "UsuarioAlias"
    Public Const EUsuarioTipoDoc As String = "UsuarioTipoDoc"
    Public Const EUsuarioNumeroDoc As String = "UsuarioNumeroDoc"
    Public Const EUsuarioEmail As String = "UsuarioEmail"
    Public Const EConceptoPago As String = "ConceptoPago"
    Public Const EDetalles As String = "Detalles"
    Public Const EDetalles_Detalle As String = "Detalle"
    Public Const EDetalles_Detalle_CodOrigen As String = "CodOrigen"
    Public Const EDetalles_Detalle_TipoOrigen As String = "TipoOrigen"
    Public Const EDetalles_Detalle_ConceptoPago As String = "ConceptoPago"
    Public Const EDetalles_Detalle_Importe As String = "Importe"
    Public Const EDetalles_Detalle_Campo1 As String = "Campo1"
    Public Const EDetalles_Detalle_Campo2 As String = "Campo2"
    Public Const EDetalles_Detalle_Campo3 As String = "Campo3"
    Public Const EParamsURL As String = "ParamsURL"
    Public Const EParamsURL_ParamURL As String = "ParamURL"
    Public Const EParamsURL_ParamURL_Nombre As String = "Nombre"
    Public Const EParamsURL_ParamURL_Valor As String = "Valor"
    Public Const EParamsEmail As String = "ParamsEmail"
    Public Const EParamsEmail_ParamEmail As String = "ParamEmail"
    Public Const EParamsEmail_ParamEmail_Nombre As String = "Nombre"
    Public Const EParamsEmail_ParamEmail_Valor As String = "Valor"


    'datos adicionales-------------------------------
    Public Const ETelefono As String = "Telefono"
    Public Const EAnexo As String = "Anexo"
    Public Const ECeular As String = "Celular"
    Public Const EDatoAdicional As String = "DatoAdicional"
    Public Const ECanalPago As String = "CanalPago"
    Public Const ETipoComprobante As String = "TipoComprobante"
    Public Const ERuc As String = "Ruc"
    Public Const ERazonSocial As String = "RazonSocial"
    Public Const EDireccion As String = "Direccion"





    Public Sub New()

    End Sub

    Private _codigoApi As String = ""
    <DataMember()> _
    Public Property CodigoApi() As String
        Get
            Return _codigoApi
        End Get
        Set(ByVal value As String)
            _codigoApi = value
        End Set
    End Property

    Private _usaEncriptacion As Boolean = False
    <DataMember()> _
    Public Property UsaEncriptacion() As Boolean
        Get
            Return _usaEncriptacion
        End Get
        Set(ByVal value As Boolean)
            _usaEncriptacion = value
        End Set
    End Property

    Private _tipoRecepcion As String = ""
    <DataMember()> _
    Public Property TipoRecepcion() As String
        Get
            Return _tipoRecepcion
        End Get
        Set(ByVal value As String)
            _tipoRecepcion = value
        End Set
    End Property

    Private _url As String = ""
    <DataMember()> _
    Public Property URL() As String
        Get
            Return _url
        End Get
        Set(ByVal value As String)
            _url = value
        End Set
    End Property

    Private _descripcion As String = ""
    <DataMember()> _
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Private _home As String = ""
    <DataMember()> _
    Public Property Home() As String
        Get
            Return _home
        End Get
        Set(ByVal value As String)
            _home = value
        End Set
    End Property

    Private _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = Nothing
    <DataMember()> _
    Public Property EsquemaNodo() As Dictionary(Of String, EsquemaTramaNodo)
        Get
            Return _esquemaNodo
        End Get
        Set(ByVal value As Dictionary(Of String, EsquemaTramaNodo))
            _esquemaNodo = value
        End Set
    End Property

End Class
