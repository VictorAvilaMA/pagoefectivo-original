﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BERecarga

    Public Sub New()

    End Sub

    <DataMember()> _
    Public Property gUsuario() As String

    'mfx:SvcId 
    <DataMember()> _
    Public Property iMontoRecarga() As Integer

    <DataMember()> _
    Public Property xCIPRecarga() As String 

    <DataMember()> _
    Public Property iMoneda() As String

    <DataMember()> _
    Public Property mMonto() As Decimal

    <DataMember()> _
    Public Property dCreado() As DateTime

    <DataMember()> _
    Public Property dModificado() As DateTime

    <DataMember()> _
    Public Property iEstado() As Integer

    <DataMember()> _
    Public Property xNumOperacionPEM() As String

    <DataMember()> _
    Public Property xNumOperacionEntidad() As String

    <DataMember()> _
    Public Property xNumExtornoEntidad() As String

    <DataMember()> _
    Public Property iNumRecargaMes() As Integer

    <DataMember()> _
    Public Property xCodBancoPE() As String

    'AGREGADO EDUARDO YUPANQUI
    <DataMember()> _
    Public Property xNumOperacionEntidadRecarga() As String
    <DataMember()> _
    Public Property xDocumento() As String
    <DataMember()> _
    Public Property xCodigoServicioBanco As String
    <DataMember()> _
    Public Property xCodigoRecarga As String
    <DataMember()> _
    Public Property xDigito11 As String
    <DataMember()> _
    Public Property xCodigoOperacion As String
    <DataMember()> _
    Public Property xCodigoCanal As String
    <DataMember()> _
    Public Property xCodigoOficina As String
    <DataMember()> _
    Public Property dFechaOperacion As DateTime

    Public Sub New(ByVal reader As IDataReader, ByVal Consulta As String)


        If Consulta = "ConsultarSolicitudRecarga" Then
            Me.gUsuario = DataUtil.ObjectToString(reader("gUsuario"))
            Me.iMontoRecarga = DataUtil.ObjectToString(reader("iMontoRecarga"))
            Me.xCIPRecarga = DataUtil.ObjectToString(reader("xCIPRecarga"))
            Me.iMoneda = DataUtil.ObjectToString(reader("iMoneda"))
            Me.mMonto = DataUtil.ObjectToDecimal(reader("mMonto"))
            Me.iEstado = DataUtil.ObjectToString(reader("iEstado"))
        End If
    End Sub


End Class
