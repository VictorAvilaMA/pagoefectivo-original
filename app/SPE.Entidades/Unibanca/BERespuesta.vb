﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()> _
Public Class BERespuesta
    Public Sub New()

    End Sub
    <DataMember()> _
    Public Property Resultado As Boolean

    <DataMember()> _
    Public Property Codigo As String
    <DataMember()> _
    Public Property Mensaje As String
    <DataMember()> _
    Public Property Monto As Decimal
    <DataMember()> _
    Public Property CIP As String
    <DataMember()> _
    Public Property _Error As Boolean
End Class
