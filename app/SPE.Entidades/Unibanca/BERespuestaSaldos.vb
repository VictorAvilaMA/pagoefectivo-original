﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util

<Serializable()> <DataContract()>
Public Class BERespuestaSaldos
    Inherits BERespuesta
    <DataMember()> _
    Public Property ClienteNombre As String
    <DataMember()> _
    Public Property ClienteEstado As String
    <DataMember()> _
    Public Property CodigoServicioBanco As String
    <DataMember()> _
    Public Property Moneda As String
    'Public Property Monto As String
    <DataMember()> _
    Public Property NumeroOperacion As String
    <DataMember()> _
    Public Property FechaDeOperacion As DateTime
    <DataMember()> _
    Public Property MensajeCodigo As String
    'Public Property Mensaje As String
    <DataMember()> _
    Public Property EmpresaDescripcion As String
    <DataMember()> _
    Public Property PagoConcepto As String
    <DataMember()> _
    Public Property FechaVencimiento As DateTime
End Class
