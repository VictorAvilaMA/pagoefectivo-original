﻿Imports System.Runtime.Serialization
Imports _3Dev.FW.Util
<Serializable()> <DataContract()>
Public Class BERespuestaSaldosBanbif
    Inherits BERespuesta
    'Resultado As Boolean
    'Codigo As String
    'Mensaje As String

    '_Error As Boolean
    Public Property PagoConcepto As String
    Public Property EmpresaDescripcion As String
    Public Property FechaVencimiento As DateTime
    Public Property FechaEmision As DateTime
    'CIP As String
    'Monto As Decimal
    Public Property IdEstado As Integer
    Public Property CipListoParaExpirar As String
    Public Property CodMonedaBanco As String
    Public Property DescripcionCliente As String

    Public Property CodigoServicio As String
    Public Property DescripcionServicio As String
    Public Property IdCliente As String
    Public Property UsuarioNombre As String

    Public Property Email As String
    Public Property IdOrdenPago As String
    Public Property IdMovimiento As String
End Class
