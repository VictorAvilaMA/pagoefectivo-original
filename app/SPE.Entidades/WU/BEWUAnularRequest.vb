﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class BEWUAnularRequest
    Private _hdReq As HeaderReq
    <DataMember()>
    Property HdReq As HeaderReq
        Get
            Return _hdReq
        End Get
        Set(value As HeaderReq)
            _hdReq = value
        End Set
    End Property
    Private _terminalOriginal As String
    <DataMember()>
    Property TerminalOriginal As String
        Get
            Return _terminalOriginal
        End Get
        Set(value As String)
            _terminalOriginal = value
        End Set
    End Property
    Private _cajeroOriginal As String
    <DataMember()>
    Property CajeroOriginal As String
        Get
            Return _cajeroOriginal
        End Get
        Set(value As String)
            _cajeroOriginal = value
        End Set
    End Property
    Private _fechaHoraOriginal As DateTime
    <DataMember()>
    Property FechaHoraOriginal As DateTime
        Get
            Return _fechaHoraOriginal
        End Get
        Set(value As DateTime)
            _fechaHoraOriginal = value
        End Set
    End Property
    Private _nroSecuenciaOriginal As String
    <DataMember()>
    Property NroSecuenciaOriginal As String
        Get
            Return _nroSecuenciaOriginal
        End Get
        Set(value As String)
            _nroSecuenciaOriginal = value
        End Set
    End Property
    Private _tipoReversa As String
    <DataMember()>
    Property TipoReversa As String
        Get
            Return _tipoReversa
        End Get
        Set(value As String)
            _tipoReversa = value
        End Set
    End Property
    Private _utility As String
    <DataMember()>
    Property Utility As String
        Get
            Return _utility
        End Get
        Set(value As String)
            _utility = value
        End Set
    End Property
    Private _amount As Integer
    <DataMember()>
    Property Amount As Integer
        Get
            Return _amount
        End Get
        Set(value As Integer)
            _amount = value
        End Set
    End Property

    Private _codigoServicio As String
    <DataMember()>
    Public Property CodigoServicio() As String
        Get
            Return _codigoServicio
        End Get
        Set(ByVal value As String)
            _codigoServicio = value
        End Set
    End Property
End Class
