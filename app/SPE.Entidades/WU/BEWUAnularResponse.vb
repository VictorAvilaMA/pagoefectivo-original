﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class BEWUAnularResponse
    Private _objBEBK As ReversaResp
    <DataMember()>
    Public Property ObjBEBK As ReversaResp
        Get
            Return _objBEBK
        End Get
        Set(value As ReversaResp)
            _objBEBK = value
        End Set
    End Property
    Private _obeLog As BELog
    <DataMember()>
    Public Property ObeLog As BELog
        Get
            Return _obeLog
        End Get
        Set(value As BELog)
            _obeLog = value
        End Set
    End Property
    Private _codErrorExtendido As String
    <DataMember()>
    Public Property CodErrorExtendido As String
        Get
            Return _codErrorExtendido
        End Get
        Set(value As String)
            _codErrorExtendido = value
        End Set
    End Property
    Private _CIP As String
    <DataMember()>
    Public Property CIP As String
        Get
            Return _CIP
        End Get
        Set(value As String)
            _CIP = value
        End Set
    End Property
End Class
