﻿Imports System.Runtime.Serialization

<DataContract()>
Public Class BEWUBarCode
    Private _codigoPais As String
    <DataMember()>
    Public Property CodigoPais As String
        Get
            Return _codigoPais
        End Get
        Set(value As String)
            _codigoPais = value
        End Set
    End Property
    Private _entidadServicio As String
    <DataMember()>
    Public Property EntidadServicio As String
        Get
            Return _entidadServicio
        End Get
        Set(value As String)
            _entidadServicio = value
        End Set
    End Property
    Private _moneda As String
    <DataMember()>
    Public Property Moneda As String
        Get
            Return _moneda
        End Get
        Set(value As String)
            _moneda = value
        End Set
    End Property
    Private _medioPago As String
    <DataMember()>
    Public Property MedioPago As String
        Get
            Return _medioPago
        End Get
        Set(value As String)
            _medioPago = value
        End Set
    End Property
    Private _codigoCliente As String
    <DataMember()>
    Public Property CodigoCliente As String
        Get
            Return _codigoCliente
        End Get
        Set(value As String)
            _codigoCliente = value
        End Set
    End Property
    Private _codigoCliente2 As String
    <DataMember()>
    Public Property CodigoCliente2 As String
        Get
            Return _codigoCliente2
        End Get
        Set(value As String)
            _codigoCliente2 = value
        End Set
    End Property
    Private _importe As String
    <DataMember()>
    Public Property Importe As String
        Get
            Return _importe
        End Get
        Set(value As String)
            _importe = value
        End Set
    End Property
    Private _fechaVencimiento As String
    <DataMember()>
    Public Property FechaVencimiento As String
        Get
            Return _fechaVencimiento
        End Get
        Set(value As String)
            _fechaVencimiento = value
        End Set
    End Property
    Public Sub New(ByVal Trama As String)
        _codigoPais = New BEBKCampoRequest(Trama, 0, 3).Valor()
        _entidadServicio = New BEBKCampoRequest(Trama, 3, 5).Valor()
        _moneda = New BEBKCampoRequest(Trama, 8, 1).Valor()
        _medioPago = New BEBKCampoRequest(Trama, 9, 1).Valor()
        _codigoCliente = New BEBKCampoRequest(Trama, 10, 15).Valor()
        _codigoCliente2 = New BEBKCampoRequest(Trama, 25, 20).Valor()
        _importe = New BEBKCampoRequest(Trama, 45, 10).Valor()
        _fechaVencimiento = New BEBKCampoRequest(Trama, 55, 5).Valor()
    End Sub
    Public Sub New()
    End Sub
End Class
