﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class BEWUCancelarRequest
    Private _hdReq As HeaderReq
    <DataMember()>
    Public Property HdReq As HeaderReq
        Get
            Return _hdReq
        End Get
        Set(value As HeaderReq)
            _hdReq = value
        End Set
    End Property

    Private _check As Check
    <DataMember()>
    Public Property Check As Check
        Get
            Return _check
        End Get
        Set(value As Check)
            _check = value
        End Set
    End Property

    Private _utility As String
    <DataMember()>
    Public Property Utility As String
        Get
            Return _utility
        End Get
        Set(value As String)
            _utility = value
        End Set
    End Property

    Private _barCode As String
    <DataMember()>
    Public Property BarCode As String
        Get
            Return _barCode
        End Get
        Set(value As String)
            _barCode = value
            _barCodeObject = New BEWUBarCode(value)
        End Set
    End Property

    Private _medioPago As String
    <DataMember()>
    Public Property MedioPago As String
        Get
            Return _medioPago
        End Get
        Set(value As String)
            _medioPago = value
        End Set
    End Property

    Private _creditCard As String
    <DataMember()>
    Public Property CreditCard As String
        Get
            Return _creditCard
        End Get
        Set(value As String)
            _creditCard = value
        End Set
    End Property

    Private _amount As Double
    <DataMember()>
    Public Property Amount As Double
        Get
            Return _amount
        End Get
        Set(value As Double)
            _amount = value
        End Set
    End Property
    Private _barCodeObject As BEWUBarCode
    <DataMember()>
    Public Property BarCodeObject As BEWUBarCode
        Get
            Return _barCodeObject
        End Get
        Set(value As BEWUBarCode)
            _barCodeObject = value
        End Set
    End Property
    Private _codigoServicio As String
    <DataMember()>
    Public Property CodigoServicio() As String
        Get
            Return _codigoServicio
        End Get
        Set(ByVal value As String)
            _codigoServicio = value
        End Set
    End Property
End Class
