﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class BEWUCancelarResponse
    Private _objBEBK As DirectaResp
    <DataMember()>
    Public Property ObjBEBK As DirectaResp
        Get
            Return _objBEBK
        End Get
        Set(value As DirectaResp)
            _objBEBK = value
        End Set
    End Property
    Private _obeLog As BELog
    <DataMember()>
    Public Property ObeLog As BELog
        Get
            Return _obeLog
        End Get
        Set(value As BELog)
            _obeLog = value
        End Set
    End Property
    Private _codErrorExtendido As String
    <DataMember()>
    Public Property CodErrorExtendido As String
        Get
            Return _codErrorExtendido
        End Get
        Set(value As String)
            _codErrorExtendido = value
        End Set
    End Property
End Class
