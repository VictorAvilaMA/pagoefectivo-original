﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class BEWUConsultarRequest
    Private _hdReq As HeaderReq
    <DataMember()>
    Public Property HdReq() As HeaderReq
        Get
            Return _hdReq
        End Get
        Set(ByVal value As HeaderReq)
            _hdReq = value
        End Set
    End Property

    Private _utility As String
    <DataMember()>
    Public Property Utility() As String
        Get
            Return _utility
        End Get
        Set(ByVal value As String)
            _utility = value
        End Set
    End Property

    Private _cod_barra As String
    <DataMember()>
    Public Property Cod_barra() As String
        Get
            Return _cod_barra
        End Get
        Set(ByVal value As String)
            _cod_barra = value
        End Set
    End Property

    Private _codigo_cliente As String
    <DataMember()>
    Public Property Codigo_cliente() As String
        Get
            Return _codigo_cliente
        End Get
        Set(ByVal value As String)
            _codigo_cliente = value
        End Set
    End Property

    Private _codigoServicio As String
    <DataMember()>
    Public Property CodigoServicio() As String
        Get
            Return _codigoServicio
        End Get
        Set(ByVal value As String)
            _codigoServicio = value
        End Set
    End Property
End Class
