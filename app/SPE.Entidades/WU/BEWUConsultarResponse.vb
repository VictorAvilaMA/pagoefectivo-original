﻿Imports System.Runtime.Serialization

<Serializable()>
<DataContract()>
Public Class BEWUConsultarResponse
    Private _objBEBK As ConsultaResp
    <DataMember()>
    Public Property ObjBEBK As ConsultaResp
        Get
            Return _objBEBK
        End Get
        Set(value As ConsultaResp)
            _objBEBK = value
        End Set
    End Property
    Private _obeLog As BELog
    <DataMember()>
    Public Property ObeLog As BELog
        Get
            Return _obeLog
        End Get
        Set(value As BELog)
            _obeLog = value
        End Set
    End Property
    Private _barCodeObject As BEWUBarCode
    <DataMember()>
    Public Property BarCodeObject As BEWUBarCode
        Get
            Return _barCodeObject
        End Get
        Set(value As BEWUBarCode)
            _barCodeObject = value
        End Set
    End Property
    Private _codErrorExtendido As String
    <DataMember()>
    Public Property CodErrorExtendido As String
        Get
            Return _codErrorExtendido
        End Get
        Set(value As String)
            _codErrorExtendido = value
        End Set
    End Property
End Class
