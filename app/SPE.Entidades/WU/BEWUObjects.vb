﻿Imports System.Runtime.Serialization

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://dataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class HeaderReq

    Private algoritmoField As String

    Private cajeroField As String

    Private codTrxField As String

    Private fechaHoraField As System.Nullable(Of Date)

    Private idMensajeField As String

    Private marcaField As String

    Private nroSecuenciaField As String

    Private plataformaField As String

    Private puestoField As String

    Private supervisorField As String

    Private terminalField As String

    Private versionField As String

    Private versionAutorizadorField As String


    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property algoritmo() As String
        Get
            Return Me.algoritmoField
        End Get
        Set(value As String)
            Me.algoritmoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cajero() As String
        Get
            Return Me.cajeroField
        End Get
        Set(value As String)
            Me.cajeroField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property codTrx() As String
        Get
            Return Me.codTrxField
        End Get
        Set(value As String)
            Me.codTrxField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property fechaHora() As System.Nullable(Of Date)
        Get
            Return Me.fechaHoraField
        End Get
        Set(value As System.Nullable(Of Date))
            Me.fechaHoraField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property idMensaje() As String
        Get
            Return Me.idMensajeField
        End Get
        Set(value As String)
            Me.idMensajeField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property marca() As String
        Get
            Return Me.marcaField
        End Get
        Set(value As String)
            Me.marcaField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property nroSecuencia() As String
        Get
            Return Me.nroSecuenciaField
        End Get
        Set(value As String)
            Me.nroSecuenciaField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property plataforma() As String
        Get
            Return Me.plataformaField
        End Get
        Set(value As String)
            Me.plataformaField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property puesto() As String
        Get
            Return Me.puestoField
        End Get
        Set(value As String)
            Me.puestoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property supervisor() As String
        Get
            Return Me.supervisorField
        End Get
        Set(value As String)
            Me.supervisorField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property terminal() As String
        Get
            Return Me.terminalField
        End Get
        Set(value As String)
            Me.terminalField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property version() As String
        Get
            Return Me.versionField
        End Get
        Set(value As String)
            Me.versionField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property versionAutorizador() As String
        Get
            Return Me.versionAutorizadorField
        End Get
        Set(value As String)
            Me.versionAutorizadorField = value
        End Set
    End Property

    

End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://dataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class ArrayFieldsQuerie

    Private cob_cod_barraField As String

    Private cob_prior_gpoField As String

    Private cob_prior_nroField As String

    Private cob_texto_feField As String

    Private cob_estadoField As String

    Private cob_cobro_tipoField As String

    Private cob_comp_impField As System.Nullable(Of Integer)

    Private numero_de_ordenField As System.Nullable(Of Integer)

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_cod_barra() As String
        Get
            Return Me.cob_cod_barraField
        End Get
        Set(value As String)
            Me.cob_cod_barraField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_prior_gpo() As String
        Get
            Return Me.cob_prior_gpoField
        End Get
        Set(value As String)
            Me.cob_prior_gpoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_prior_nro() As String
        Get
            Return Me.cob_prior_nroField
        End Get
        Set(value As String)
            Me.cob_prior_nroField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_texto_fe() As String
        Get
            Return Me.cob_texto_feField
        End Get
        Set(value As String)
            Me.cob_texto_feField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_estado() As String
        Get
            Return Me.cob_estadoField
        End Get
        Set(value As String)
            Me.cob_estadoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_cobro_tipo() As String
        Get
            Return Me.cob_cobro_tipoField
        End Get
        Set(value As String)
            Me.cob_cobro_tipoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_comp_imp() As System.Nullable(Of Integer)
        Get
            Return Me.cob_comp_impField
        End Get
        Set(value As System.Nullable(Of Integer))
            Me.cob_comp_impField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property numero_de_orden() As System.Nullable(Of Integer)
        Get
            Return Me.numero_de_ordenField
        End Get
        Set(value As System.Nullable(Of Integer))
            Me.numero_de_ordenField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://operationsDataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class ConsultaResp

    Private cob_cliente_nombField As String

    Private countField As System.Nullable(Of Integer)

    Private fieldsField() As ArrayFieldsQuerie

    Private headerField As HeaderResp

    Private seleccion_con_prioridadField As System.Nullable(Of Integer)

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cob_cliente_nomb() As String
        Get
            Return Me.cob_cliente_nombField
        End Get
        Set(value As String)
            Me.cob_cliente_nombField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property count() As System.Nullable(Of Integer)
        Get
            Return Me.countField
        End Get
        Set(value As System.Nullable(Of Integer))
            Me.countField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlArrayAttribute(IsNullable:=True), _
     System.Xml.Serialization.XmlArrayItemAttribute("item", [Namespace]:="http://operations.genericows.sibs.com.ar", IsNullable:=False)> _
    Public Property fields() As ArrayFieldsQuerie()
        Get
            Return Me.fieldsField
        End Get
        Set(value As ArrayFieldsQuerie())
            Me.fieldsField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property header() As HeaderResp
        Get
            Return Me.headerField
        End Get
        Set(value As HeaderResp)
            Me.headerField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property seleccion_con_prioridad() As System.Nullable(Of Integer)
        Get
            Return Me.seleccion_con_prioridadField
        End Get
        Set(value As System.Nullable(Of Integer))
            Me.seleccion_con_prioridadField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://dataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class HeaderResp

    Private algoritmoField As String

    Private cajeroField As String

    Private codErrorField As System.Nullable(Of Integer)

    Private codSeveridadField As System.Nullable(Of Integer)

    Private fechaHoraField As System.Nullable(Of Date)

    Private idMensajeField As String

    Private marcaField As String

    Private nroSecuenciaField As String

    Private terminalField As String

    Private versionField As String

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property algoritmo() As String
        Get
            Return Me.algoritmoField
        End Get
        Set(value As String)
            Me.algoritmoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property cajero() As String
        Get
            Return Me.cajeroField
        End Get
        Set(value As String)
            Me.cajeroField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property codError() As System.Nullable(Of Integer)
        Get
            Return Me.codErrorField
        End Get
        Set(value As System.Nullable(Of Integer))
            Me.codErrorField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property codSeveridad() As System.Nullable(Of Integer)
        Get
            Return Me.codSeveridadField
        End Get
        Set(value As System.Nullable(Of Integer))
            Me.codSeveridadField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property fechaHora() As System.Nullable(Of Date)
        Get
            Return Me.fechaHoraField
        End Get
        Set(value As System.Nullable(Of Date))
            Me.fechaHoraField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property idMensaje() As String
        Get
            Return Me.idMensajeField
        End Get
        Set(value As String)
            Me.idMensajeField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property marca() As String
        Get
            Return Me.marcaField
        End Get
        Set(value As String)
            Me.marcaField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property nroSecuencia() As String
        Get
            Return Me.nroSecuenciaField
        End Get
        Set(value As String)
            Me.nroSecuenciaField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property terminal() As String
        Get
            Return Me.terminalField
        End Get
        Set(value As String)
            Me.terminalField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property version() As String
        Get
            Return Me.versionField
        End Get
        Set(value As String)
            Me.versionField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://operationsDataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class ReversaResp

    Private estadoField As String

    Private headerField As HeaderResp

    Private operadorField As String

    Private ticketField As String

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property estado() As String
        Get
            Return Me.estadoField
        End Get
        Set(value As String)
            Me.estadoField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property header() As HeaderResp
        Get
            Return Me.headerField
        End Get
        Set(value As HeaderResp)
            Me.headerField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property operador() As String
        Get
            Return Me.operadorField
        End Get
        Set(value As String)
            Me.operadorField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property ticket() As String
        Get
            Return Me.ticketField
        End Get
        Set(value As String)
            Me.ticketField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://operationsDataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class DirectaResp

    Private headerField As HeaderResp

    Private msgField As String

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property header() As HeaderResp
        Get
            Return Me.headerField
        End Get
        Set(value As HeaderResp)
            Me.headerField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property msg() As String
        Get
            Return Me.msgField
        End Get
        Set(value As String)
            Me.msgField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.SerializableAttribute(), _
 System.Diagnostics.DebuggerStepThroughAttribute(), _
 System.ComponentModel.DesignerCategoryAttribute("code"), _
 System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://dataTypes.genericows.sibs.com.ar")> _
<DataContract()> Public Class Check

    Private accountIDField As String

    Private amountField As String

    Private bankBranchField As String

    Private bankIDField As String

    Private bankSquareField As String

    Private checkNumberField As String

    Private expirationField As System.Nullable(Of Date)

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property accountID() As String
        Get
            Return Me.accountIDField
        End Get
        Set(value As String)
            Me.accountIDField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property amount() As String
        Get
            Return Me.amountField
        End Get
        Set(value As String)
            Me.amountField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property bankBranch() As String
        Get
            Return Me.bankBranchField
        End Get
        Set(value As String)
            Me.bankBranchField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property bankID() As String
        Get
            Return Me.bankIDField
        End Get
        Set(value As String)
            Me.bankIDField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property bankSquare() As String
        Get
            Return Me.bankSquareField
        End Get
        Set(value As String)
            Me.bankSquareField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property checkNumber() As String
        Get
            Return Me.checkNumberField
        End Get
        Set(value As String)
            Me.checkNumberField = value
        End Set
    End Property

    '''<remarks/>
    <DataMember()> <System.Xml.Serialization.XmlElementAttribute(IsNullable:=True)> _
    Public Property expiration() As System.Nullable(Of Date)
        Get
            Return Me.expirationField
        End Get
        Set(value As System.Nullable(Of Date))
            Me.expirationField = value
        End Set
    End Property
End Class
