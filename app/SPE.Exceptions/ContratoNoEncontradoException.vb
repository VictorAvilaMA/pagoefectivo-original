Imports System

<Serializable()> _
Public Class ContratoNoEncontradoException
    Inherits SPEExceptionBase

    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
        Me._customMessage = LeerXmlCustom("E006")
    End Sub

End Class
