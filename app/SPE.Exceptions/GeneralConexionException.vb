Imports System

<Serializable()> _
Public Class GeneralConexionException
    Inherits SPEExceptionBase

    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
        Dim DatosError() = LeerXmlExceptions(mensaje)
        Me._errorCode = DatosError(0)
        Me._customMessage = DatosError(1)
    End Sub

End Class
