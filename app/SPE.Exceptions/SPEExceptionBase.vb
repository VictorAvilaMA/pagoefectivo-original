Imports System
Imports System.Xml

<Serializable()> _
Public Class SPEExceptionBase
    Inherits System.Exception

    Protected _customMessage As String
    Protected _errorCode As String

    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
    End Sub

    Public ReadOnly Property CustomMessage() As String
        Get
            Return _custommessage
        End Get
    End Property

    Protected Function LeerXmlExceptions(ByVal exMensaje As String) As String()
        Dim strRutaXML, Descripcion As String
        Dim DatosError(2) As String

        strRutaXML = System.Configuration.ConfigurationManager.AppSettings("xmlExceptions")
        strRutaXML = AppDomain.CurrentDomain.BaseDirectory + strRutaXML
        If System.IO.File.Exists(strRutaXML) Then

            Dim xmldoc As New XmlDocument()
            xmldoc.Load(strRutaXML)

            For Each node As XmlNode In xmldoc.ChildNodes(1)
                Descripcion = node.ChildNodes(1).Attributes("Desc").InnerText
                If exMensaje.IndexOf(Descripcion) > -1 Then
                    DatosError(0) = node.Attributes("codigo").InnerText
                    DatosError(1) = node.ChildNodes(0).Attributes("Desc").InnerText
                End If
            Next
            If DatosError.Equals("") Then
                DatosError(0) = ""
                DatosError(1) = xmldoc.ChildNodes(1).ChildNodes(0).Attributes("Desc").InnerText
            End If
        End If
        Return DatosError

    End Function

    Protected Function LeerXmlCustom(ByVal errorCodigo As String) As String
        Dim strRutaXML, Descripcion As String
        Dim DatosError As String = ""
        strRutaXML = System.Configuration.ConfigurationManager.AppSettings("xmlExceptions")
        strRutaXML = AppDomain.CurrentDomain.BaseDirectory + strRutaXML
        If System.IO.File.Exists(strRutaXML) Then

            Dim xmldoc As New XmlDocument()
            xmldoc.Load(strRutaXML)

            For Each node As XmlNode In xmldoc.ChildNodes(1)
                Descripcion = node.Attributes("codigo").InnerText
                If Descripcion.Equals(errorCodigo) Then
                    DatosError = node.ChildNodes(0).Attributes("Desc").InnerText
                End If
            Next

            If DatosError.Equals("") Then
                DatosError = xmldoc.ChildNodes(1).ChildNodes(0).Attributes("Desc").InnerText
            End If
        End If
        Return DatosError

    End Function
End Class
