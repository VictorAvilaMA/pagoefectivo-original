Imports System
Imports System.Text
Public Class TramaGetException
    Inherits SPEExceptionBase

    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
        Dim descMensaje As New StringBuilder()
        descMensaje.AppendFormat(LeerXmlCustom("E005"), mensaje)
        Me._customMessage = descMensaje.ToString()
    End Sub

End Class
