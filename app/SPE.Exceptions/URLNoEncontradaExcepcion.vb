Imports System

<Serializable()> _
Public Class URLNoEncontradaExcepcion
    Inherits SPEExceptionBase

    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
        Me._customMessage = LeerXmlCustom("E004")
    End Sub

End Class
