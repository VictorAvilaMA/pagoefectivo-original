﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SPE.Criptography;

namespace SPE.GeneradorClaves
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba el codigo de la clave a generar");
            String codigo = Console.ReadLine();
            String pathPrivateKey = "C:\\" + codigo + "_PrivateKey.1pz";
            String pathPublicKey = "C:\\" + codigo + "_PublicKey.1pz";
            Encripter encripter = new Encripter();
            Byte[] privateKey = encripter.GenerarKeyPriv(codigo);
            Byte[] publicKey = encripter.GenerarKeyPub(codigo);
            ByteUtil.ByteArrayToFile(pathPrivateKey, privateKey);
            ByteUtil.ByteArrayToFile(pathPublicKey, publicKey);
            Console.WriteLine("Se genero la clave con exito");
            Console.ReadLine();
        }
    }
}
