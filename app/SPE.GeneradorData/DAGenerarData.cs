﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SPE.GeneradorData.Proxy;

namespace SPE.GeneradorData
{
    public class DAGenerarData
    {
        public int RegLogGeneracionCIP(BEWSGenCIPRequest oBEWSGenCIPRequest){
            Database db = DatabaseFactory.CreateDatabase("SPESQLPagoEfectivo");
            int result = 0;
            result = (int)db.ExecuteScalar("PagoEfectivo.prc_RegistrarLogRequestGenerarCIP",
                oBEWSGenCIPRequest.CAPI,
                oBEWSGenCIPRequest.CClave,
                oBEWSGenCIPRequest.Email,
                "",
                oBEWSGenCIPRequest.Xml,
                oBEWSGenCIPRequest.Password);
            return result;
        }
        public int RegLogGeneracionCIPToCIP(BEWSGenCIPRequest oBEWSGenCIPRequest,string CIP, string Operacion, string monto, string codigoMoneda, string codigoServicio){
            Database db = DatabaseFactory.CreateDatabase("SPESQLPagoEfectivo");
            int result = 0;
            result = (int)db.ExecuteScalar("PagoEfectivo.prc_RegistrarLogRequestGenerarCIPToCIP",
                oBEWSGenCIPRequest.CAPI,
                oBEWSGenCIPRequest.CClave,
                oBEWSGenCIPRequest.Email,
                "",
                oBEWSGenCIPRequest.Xml,
                oBEWSGenCIPRequest.Password,
                CIP,
                Operacion,
                monto,
                codigoMoneda,
                codigoServicio);
            return result;
        }
        public int RegLogCIPConsultar(String CCAPI,String CCLAVE, String CIPS){
            Database db = DatabaseFactory.CreateDatabase("SPESQLPagoEfectivo");
            int result = 0;
            result = (int)db.ExecuteScalar("PagoEfectivo.prc_RegistrarCIPConsultar",
                CCAPI,
                CCLAVE,
                CIPS,
                "");
            return result;
        }
        public int RegLogCIPEliminar(String CCAPI,String CCLAVE, String CIPS){
            Database db = DatabaseFactory.CreateDatabase("SPESQLPagoEfectivo");
            int result = 0;
            result = (int)db.ExecuteScalar("PagoEfectivo.prc_RegistrarCIPEliminar",
                CCAPI,
                CCLAVE,
                CIPS,
                "");
            return result;
        }
    }
}
