﻿namespace SPE.GeneradorData
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtXml = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCCapi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCClave = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCorrelativoHasta = new System.Windows.Forms.TextBox();
            this.txtCorrelativoCorreoHasta = new System.Windows.Forms.TextBox();
            this.txtCorrelativoDesde = new System.Windows.Forms.TextBox();
            this.txtCorrelativoCorreoDesde = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCodMoneda = new System.Windows.Forms.TextBox();
            this.txtCodigoServ = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnGenerarData = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnGenerarCipBBVA = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtCIPS660 = new System.Windows.Forms.TextBox();
            this.txtGeneraConsultaCIP = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnGenerarEliminarCIP = new System.Windows.Forms.Button();
            this.txtCIPELiminar660 = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.lblgordillodeshit = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(75, 28);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(314, 20);
            this.txtEmail.TabIndex = 1;
            this.txtEmail.Text = resources.GetString("txtEmail.Text");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "EMAIL";
            // 
            // txtXml
            // 
            this.txtXml.Location = new System.Drawing.Point(75, 61);
            this.txtXml.Multiline = true;
            this.txtXml.Name = "txtXml";
            this.txtXml.Size = new System.Drawing.Size(314, 296);
            this.txtXml.TabIndex = 1;
            this.txtXml.Text = resources.GetString("txtXml.Text");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "XML";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Location = new System.Drawing.Point(12, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(438, 515);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.lblgordillodeshit);
            this.tabPage14.Controls.Add(this.label1);
            this.tabPage14.Controls.Add(this.txtCCapi);
            this.tabPage14.Controls.Add(this.label2);
            this.tabPage14.Controls.Add(this.txtCClave);
            this.tabPage14.Controls.Add(this.label11);
            this.tabPage14.Controls.Add(this.label9);
            this.tabPage14.Controls.Add(this.txtCorrelativoHasta);
            this.tabPage14.Controls.Add(this.txtCorrelativoCorreoHasta);
            this.tabPage14.Controls.Add(this.txtCorrelativoDesde);
            this.tabPage14.Controls.Add(this.txtCorrelativoCorreoDesde);
            this.tabPage14.Controls.Add(this.label10);
            this.tabPage14.Controls.Add(this.label8);
            this.tabPage14.Controls.Add(this.label5);
            this.tabPage14.Controls.Add(this.label7);
            this.tabPage14.Controls.Add(this.label6);
            this.tabPage14.Controls.Add(this.txtCodMoneda);
            this.tabPage14.Controls.Add(this.txtCodigoServ);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(430, 489);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "Config";
            this.tabPage14.UseVisualStyleBackColor = true;
            this.tabPage14.Click += new System.EventHandler(this.tabPage14_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "CCAPI";
            // 
            // txtCCapi
            // 
            this.txtCCapi.Location = new System.Drawing.Point(84, 47);
            this.txtCCapi.Name = "txtCCapi";
            this.txtCCapi.Size = new System.Drawing.Size(314, 20);
            this.txtCCapi.TabIndex = 21;
            this.txtCCapi.Text = "b3c76a14-fef0-43d8-880c-f163d9ab7c9b,b3c76a14-fef0-43d8-880c-f163d9ab7c9b,b3c76a1" +
    "4-fef0-43d8-880c-f163d9ab7c9b";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "CCLAVE";
            // 
            // txtCClave
            // 
            this.txtCClave.Location = new System.Drawing.Point(84, 73);
            this.txtCClave.Name = "txtCClave";
            this.txtCClave.Size = new System.Drawing.Size(314, 20);
            this.txtCClave.TabIndex = 20;
            this.txtCClave.Text = "dee455ca-e545-4892-9e6d-6dcf7a84c085,dee455ca-e545-4892-9e6d-6dcf7a84c085,dee455c" +
    "a-e545-4892-9e6d-6dcf7a84c085";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(222, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "hasta";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(222, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "hasta";
            // 
            // txtCorrelativoHasta
            // 
            this.txtCorrelativoHasta.Location = new System.Drawing.Point(261, 128);
            this.txtCorrelativoHasta.Name = "txtCorrelativoHasta";
            this.txtCorrelativoHasta.Size = new System.Drawing.Size(55, 20);
            this.txtCorrelativoHasta.TabIndex = 15;
            this.txtCorrelativoHasta.Text = "400";
            // 
            // txtCorrelativoCorreoHasta
            // 
            this.txtCorrelativoCorreoHasta.Location = new System.Drawing.Point(261, 101);
            this.txtCorrelativoCorreoHasta.Name = "txtCorrelativoCorreoHasta";
            this.txtCorrelativoCorreoHasta.Size = new System.Drawing.Size(55, 20);
            this.txtCorrelativoCorreoHasta.TabIndex = 14;
            this.txtCorrelativoCorreoHasta.Text = "45";
            // 
            // txtCorrelativoDesde
            // 
            this.txtCorrelativoDesde.Location = new System.Drawing.Point(166, 129);
            this.txtCorrelativoDesde.Name = "txtCorrelativoDesde";
            this.txtCorrelativoDesde.Size = new System.Drawing.Size(50, 20);
            this.txtCorrelativoDesde.TabIndex = 17;
            this.txtCorrelativoDesde.Text = "1";
            // 
            // txtCorrelativoCorreoDesde
            // 
            this.txtCorrelativoCorreoDesde.Location = new System.Drawing.Point(166, 102);
            this.txtCorrelativoCorreoDesde.Name = "txtCorrelativoCorreoDesde";
            this.txtCorrelativoCorreoDesde.Size = new System.Drawing.Size(50, 20);
            this.txtCorrelativoCorreoDesde.TabIndex = 16;
            this.txtCorrelativoCorreoDesde.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 132);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "[Correlativo] empieza en:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "[CorrelativoCorreo] empieza en:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Configuracion (XML) :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "[CodMoneda]";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "[CodServ]";
            // 
            // txtCodMoneda
            // 
            this.txtCodMoneda.Location = new System.Drawing.Point(86, 193);
            this.txtCodMoneda.Name = "txtCodMoneda";
            this.txtCodMoneda.Size = new System.Drawing.Size(68, 20);
            this.txtCodMoneda.TabIndex = 7;
            this.txtCodMoneda.Text = "1,2";
            // 
            // txtCodigoServ
            // 
            this.txtCodigoServ.Location = new System.Drawing.Point(84, 161);
            this.txtCodigoServ.Name = "txtCodigoServ";
            this.txtCodigoServ.Size = new System.Drawing.Size(110, 20);
            this.txtCodigoServ.TabIndex = 8;
            this.txtCodigoServ.Text = "NEO,NEO,NEO";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnGenerarData);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtXml);
            this.tabPage1.Controls.Add(this.txtEmail);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(430, 489);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnGenerarData
            // 
            this.btnGenerarData.Location = new System.Drawing.Point(130, 397);
            this.btnGenerarData.Name = "btnGenerarData";
            this.btnGenerarData.Size = new System.Drawing.Size(63, 44);
            this.btnGenerarData.TabIndex = 21;
            this.btnGenerarData.Text = "Generar CIP";
            this.btnGenerarData.UseVisualStyleBackColor = true;
            this.btnGenerarData.Click += new System.EventHandler(this.btnGenerarData_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnGenerarCipBBVA);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(430, 489);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnGenerarCipBBVA
            // 
            this.btnGenerarCipBBVA.Location = new System.Drawing.Point(110, 82);
            this.btnGenerarCipBBVA.Name = "btnGenerarCipBBVA";
            this.btnGenerarCipBBVA.Size = new System.Drawing.Size(66, 59);
            this.btnGenerarCipBBVA.TabIndex = 0;
            this.btnGenerarCipBBVA.Text = "Generar CIP para BBVA";
            this.btnGenerarCipBBVA.UseVisualStyleBackColor = true;
            this.btnGenerarCipBBVA.Click += new System.EventHandler(this.btnGenerarCipBBVA_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtCIPS660);
            this.tabPage3.Controls.Add(this.txtGeneraConsultaCIP);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(430, 489);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtCIPS660
            // 
            this.txtCIPS660.Location = new System.Drawing.Point(27, 21);
            this.txtCIPS660.MaxLength = 60000;
            this.txtCIPS660.Multiline = true;
            this.txtCIPS660.Name = "txtCIPS660";
            this.txtCIPS660.Size = new System.Drawing.Size(379, 346);
            this.txtCIPS660.TabIndex = 1;
            this.txtCIPS660.Text = resources.GetString("txtCIPS660.Text");
            // 
            // txtGeneraConsultaCIP
            // 
            this.txtGeneraConsultaCIP.Location = new System.Drawing.Point(272, 394);
            this.txtGeneraConsultaCIP.Name = "txtGeneraConsultaCIP";
            this.txtGeneraConsultaCIP.Size = new System.Drawing.Size(114, 58);
            this.txtGeneraConsultaCIP.TabIndex = 0;
            this.txtGeneraConsultaCIP.Text = "Generar COnsulta de CIP WSGeneral";
            this.txtGeneraConsultaCIP.UseVisualStyleBackColor = true;
            this.txtGeneraConsultaCIP.Click += new System.EventHandler(this.txtGeneraConsultaCIP_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnGenerarEliminarCIP);
            this.tabPage4.Controls.Add(this.txtCIPELiminar660);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(430, 489);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnGenerarEliminarCIP
            // 
            this.btnGenerarEliminarCIP.Location = new System.Drawing.Point(183, 381);
            this.btnGenerarEliminarCIP.Name = "btnGenerarEliminarCIP";
            this.btnGenerarEliminarCIP.Size = new System.Drawing.Size(150, 77);
            this.btnGenerarEliminarCIP.TabIndex = 3;
            this.btnGenerarEliminarCIP.Text = "Generar Eliminar CIP";
            this.btnGenerarEliminarCIP.UseVisualStyleBackColor = true;
            this.btnGenerarEliminarCIP.Click += new System.EventHandler(this.btnGenerarEliminarCIP_Click);
            // 
            // txtCIPELiminar660
            // 
            this.txtCIPELiminar660.Location = new System.Drawing.Point(24, 18);
            this.txtCIPELiminar660.MaxLength = 60000;
            this.txtCIPELiminar660.Multiline = true;
            this.txtCIPELiminar660.Name = "txtCIPELiminar660";
            this.txtCIPELiminar660.Size = new System.Drawing.Size(379, 346);
            this.txtCIPELiminar660.TabIndex = 2;
            this.txtCIPELiminar660.Text = resources.GetString("txtCIPELiminar660.Text");
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(430, 489);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(430, 489);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "tabPage6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(430, 489);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(430, 489);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "tabPage8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(430, 489);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "tabPage9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(430, 489);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "tabPage10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage11
            // 
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(430, 489);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "tabPage11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(430, 489);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "tabPage12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tabPage13
            // 
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(430, 489);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "tabPage13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // lblgordillodeshit
            // 
            this.lblgordillodeshit.AutoSize = true;
            this.lblgordillodeshit.Location = new System.Drawing.Point(343, 201);
            this.lblgordillodeshit.Name = "lblgordillodeshit";
            this.lblgordillodeshit.Size = new System.Drawing.Size(41, 13);
            this.lblgordillodeshit.TabIndex = 24;
            this.lblgordillodeshit.Text = "label12";
            this.lblgordillodeshit.Click += new System.EventHandler(this.lblgordillodeshit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 540);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtXml;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCCapi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCClave;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCorrelativoHasta;
        private System.Windows.Forms.TextBox txtCorrelativoCorreoHasta;
        private System.Windows.Forms.TextBox txtCorrelativoDesde;
        private System.Windows.Forms.TextBox txtCorrelativoCorreoDesde;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCodMoneda;
        private System.Windows.Forms.TextBox txtCodigoServ;
        private System.Windows.Forms.Button btnGenerarData;
        private System.Windows.Forms.Button btnGenerarCipBBVA;
        private System.Windows.Forms.Button txtGeneraConsultaCIP;
        public System.Windows.Forms.TextBox txtCIPS660;
        private System.Windows.Forms.Button btnGenerarEliminarCIP;
        public System.Windows.Forms.TextBox txtCIPELiminar660;
        private System.Windows.Forms.Label lblgordillodeshit;
    }
}

