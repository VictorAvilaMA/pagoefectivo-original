﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SPE.GeneradorData.Proxy;
using System.Threading;

namespace SPE.GeneradorData
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGenerarData_Click(object sender, EventArgs e)
        {

            
        }

        private void btnGenerarCipBBVA_Click(object sender, EventArgs e)
        {
            int correlativoDesde = Int32.Parse(txtCorrelativoDesde.Text);
            int correlativoHasta = Int32.Parse(txtCorrelativoHasta.Text);
            int correlativoCorreoDesde = Int32.Parse(txtCorrelativoCorreoDesde.Text);
            int correlativoCorreoHasta = Int32.Parse(txtCorrelativoCorreoHasta.Text);
            String[] CCAPI = txtCCapi.Text.Split(',');
            String[] CCLAVE = txtCClave.Text.Split(',');
            String[] Email = txtEmail.Text.Split(',');
            String[] CodigoServ = txtCodigoServ.Text.Split(',');
            String[] codMoneda = txtCodMoneda.Text.Split(',');
            string sCCAPI = CCAPI[1];
            string sCCLAVE = CCLAVE[1];
            string sCodServicio = CodigoServ[1];
            string sMoneda = codMoneda[0];
            string sCIP = String.Empty;
            string sOperacion = "ELIMINARCIP";
            string sMonto = "55.10";
            string sXML = String.Empty;
            int indiceCorreo = correlativoCorreoDesde;

            for (int i = correlativoDesde; i <= correlativoHasta; i++)
            {
                switch (i + 1 - correlativoDesde)
                {
                    case 31: sOperacion = "BCPCANCELAR"; break;
                    case 241: sOperacion = "BCPEXTORNO"; break;
                    case 271: sOperacion = "BBVACANCELAR"; break;
                    case 361: sOperacion = "BBVAEXTORNO"; break;
                    case 391: sOperacion = "BBVAEXTORNOA"; break;
                    case 421: sOperacion = "FCCANCELAR"; break;
                    case 631: sOperacion = "FCEXTORNO"; break;
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 60)
                {
                    /*sCCAPI = CCAPI[1];
                    sCCLAVE = CCLAVE[1];
                    sCodServicio = CodigoServ[1];*/
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 80)
                {
                    /*sCCAPI = CCAPI[2];
                    sCCLAVE = CCLAVE[2];
                    sCodServicio = CodigoServ[2];*/
                    //sMoneda = codMoneda[1];
                }

                Crypta oCrypta = new Crypta();
                WSCIP oWSCIP = new WSCIP();

                BEWSGenCIPRequest oRequest = new BEWSGenCIPRequest();
                oRequest.CAPI = sCCAPI;
                oRequest.CClave = sCCLAVE;
                oRequest.Email = "jpba_666@hotmail.com";//"correo" + indiceCorreo.ToString().PadLeft(2, '0') + "2@puentesarr.in";
                String xml = txtXml.Text.Replace("[correlativo]", i.ToString().PadLeft(2, '0'));
                xml = xml.Replace("[correlativoCorreo]", indiceCorreo.ToString().PadLeft(2, '0')+"2");
                xml = xml.Replace("[Moneda]", sMoneda);
                xml = xml.Replace("[CodServicio]", sCodServicio);
                sXML = xml;
                oRequest.Xml = oCrypta.BlackBox(xml);
                BEWSGenCIPResponse response = oWSCIP.GenerarCIP(oRequest);
                if (String.IsNullOrEmpty(response.CIP))
                {
                    return;
                }
                sCIP = response.CIP;
                oRequest.Password = oRequest.Xml;
                oRequest.Xml = sXML;
                new DAGenerarData().RegLogGeneracionCIPToCIP(oRequest, sCIP, sOperacion,sMonto,sMoneda,sCodServicio);
                indiceCorreo = ((correlativoCorreoHasta - indiceCorreo) > 0) ? indiceCorreo + 1 : correlativoCorreoDesde;
            }
        }

        private void txtGeneraConsultaCIP_Click(object sender, EventArgs e)
        {
            int correlativoDesde = Int32.Parse(txtCorrelativoDesde.Text);
            int correlativoHasta = Int32.Parse(txtCorrelativoHasta.Text);
            int correlativoCorreoDesde = Int32.Parse(txtCorrelativoCorreoDesde.Text);
            int correlativoCorreoHasta = Int32.Parse(txtCorrelativoCorreoHasta.Text);
            String[] CCAPI = txtCCapi.Text.Split(',');
            String[] CCLAVE = txtCClave.Text.Split(',');
            String[] Email = txtEmail.Text.Split(',');
            String[] CodigoServ = txtCodigoServ.Text.Split(',');
            String[] codMoneda = txtCodMoneda.Text.Split(',');
            String[] CIPS = txtCIPS660.Text.Split('%');
            string sCCAPI = CCAPI[0];
            string sCCLAVE = CCLAVE[0];
            string sCodServicio = CodigoServ[0];
            string sMoneda = codMoneda[0];
            string sCIP = String.Empty;
            string sOperacion = "ELIMINARCIP";
            string sMonto = "55.10";
            string sXML = String.Empty;
            int indiceCorreo = correlativoCorreoDesde;
            int indiceCIPS = 0;

            for (int i = correlativoDesde; i <= correlativoHasta; i++)
            {
                switch (i + 1 - correlativoDesde)
                {
                    case 31: sOperacion = "BCPCANCELAR"; break;
                    case 241: sOperacion = "BCPEXTORNO"; break;
                    case 271: sOperacion = "BBVACANCELAR"; break;
                    case 361: sOperacion = "BBVAEXTORNO"; break;
                    case 391: sOperacion = "BBVAEXTORNOA"; break;
                    case 421: sOperacion = "FCCANCELAR"; break;
                    case 631: sOperacion = "FCEXTORNO"; break;
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 60)
                {
                    sCCAPI = CCAPI[1];
                    sCCLAVE = CCLAVE[1];
                    sCodServicio = CodigoServ[1];
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 80)
                {
                    sCCAPI = CCAPI[2];
                    sCCLAVE = CCLAVE[2];
                    sCodServicio = CodigoServ[2];
                    //sMoneda = codMoneda[1];
                }

                Crypta oCrypta = new Crypta();
                WSCIP oWSCIP = new WSCIP();
                if (indiceCIPS > 659)
                    indiceCIPS = 0;

                sCCAPI = CIPS[indiceCIPS].Split(',')[1];
                sCCLAVE = CIPS[indiceCIPS].Split(',')[2];

                new DAGenerarData().RegLogCIPConsultar(CIPS[indiceCIPS].Split(',')[1],
                    CIPS[indiceCIPS].Split(',')[2],
                    CIPS[indiceCIPS].Split(',')[0]);
                indiceCorreo = ((correlativoCorreoHasta - indiceCorreo) > 0) ? indiceCorreo + 1 : correlativoCorreoDesde;
                indiceCIPS = indiceCIPS + 1;
            }
        }

        private void btnGenerarEliminarCIP_Click(object sender, EventArgs e)
        {
            int correlativoDesde = Int32.Parse(txtCorrelativoDesde.Text);
            int correlativoHasta = Int32.Parse(txtCorrelativoHasta.Text);
            int correlativoCorreoDesde = Int32.Parse(txtCorrelativoCorreoDesde.Text);
            int correlativoCorreoHasta = Int32.Parse(txtCorrelativoCorreoHasta.Text);
            String[] CCAPI = txtCCapi.Text.Split(',');
            String[] CCLAVE = txtCClave.Text.Split(',');
            String[] Email = txtEmail.Text.Split(',');
            String[] CodigoServ = txtCodigoServ.Text.Split(',');
            String[] codMoneda = txtCodMoneda.Text.Split(',');
            String[] CIPS = txtCIPELiminar660.Text.Split('%');
            string sCCAPI = CCAPI[0];
            string sCCLAVE = CCLAVE[0];
            string sCodServicio = CodigoServ[0];
            string sMoneda = codMoneda[0];
            string sCIP = String.Empty;
            string sOperacion = "ELIMINARCIP";
            string sMonto = "55.10";
            string sXML = String.Empty;
            int indiceCorreo = correlativoCorreoDesde;
            int indiceCIPS = 0;

            for (int i = correlativoDesde; i <= correlativoHasta; i++)
            {
                switch (i + 1 - correlativoDesde)
                {
                    case 31: sOperacion = "BCPCANCELAR"; break;
                    case 241: sOperacion = "BCPEXTORNO"; break;
                    case 271: sOperacion = "BBVACANCELAR"; break;
                    case 361: sOperacion = "BBVAEXTORNO"; break;
                    case 391: sOperacion = "BBVAEXTORNOA"; break;
                    case 421: sOperacion = "FCCANCELAR"; break;
                    case 631: sOperacion = "FCEXTORNO"; break;
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 60)
                {
                    sCCAPI = CCAPI[1];
                    sCCLAVE = CCLAVE[1];
                    sCodServicio = CodigoServ[1];
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 80)
                {
                    sCCAPI = CCAPI[2];
                    sCCLAVE = CCLAVE[2];
                    sCodServicio = CodigoServ[2];
                    //sMoneda = codMoneda[1];
                }

                Crypta oCrypta = new Crypta();
                WSCIP oWSCIP = new WSCIP();
                if (indiceCIPS > 659)
                    indiceCIPS = 0;

                sCCAPI = CIPS[indiceCIPS].Split(',')[1];
                sCCLAVE = CIPS[indiceCIPS].Split(',')[2];

                new DAGenerarData().RegLogCIPEliminar(CIPS[indiceCIPS].Split(',')[1],
                    CIPS[indiceCIPS].Split(',')[2],
                    CIPS[indiceCIPS].Split(',')[0]);
                indiceCorreo = ((correlativoCorreoHasta - indiceCorreo) > 0) ? indiceCorreo + 1 : correlativoCorreoDesde;
                indiceCIPS = indiceCIPS + 1;
            }
        }

        private void btnGenerarData_Click_1(object sender, EventArgs e)
        {
            int correlativoDesde = Int32.Parse(txtCorrelativoDesde.Text);
            int correlativoHasta = Int32.Parse(txtCorrelativoHasta.Text);
            int correlativoCorreoDesde = Int32.Parse(txtCorrelativoCorreoDesde.Text);
            int correlativoCorreoHasta = Int32.Parse(txtCorrelativoCorreoHasta.Text);
            String[] CCAPI = txtCCapi.Text.Split(',');
            String[] CCLAVE = txtCClave.Text.Split(',');
            String[] Email = txtEmail.Text.Split(',');
            String[] CodigoServ = txtCodigoServ.Text.Split(',');
            String[] codMoneda = txtCodMoneda.Text.Split(',');
            string sCCAPI = CCAPI[0];
            string sCCLAVE = CCLAVE[0];
            string sCodServicio = CodigoServ[0];
            string sMoneda = codMoneda[0];
            string sCIP = String.Empty;

            int indiceCorreo = correlativoCorreoDesde;

            for (int i = correlativoDesde; i <= correlativoHasta; i++)
            {
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 60)
                {
                    sCCAPI = CCAPI[1];
                    sCCLAVE = CCLAVE[1];
                    sCodServicio = CodigoServ[1];
                }
                if ((((i - correlativoDesde) * 100) / (correlativoHasta - correlativoDesde)) >= 80)
                {
                    sCCAPI = CCAPI[2];
                    sCCLAVE = CCLAVE[2];
                    sCodServicio = CodigoServ[2];
                    sMoneda = codMoneda[1];
                }

                Crypta oCrypta = new Crypta();
                WSCIP oWSCIP = new WSCIP();

                BEWSGenCIPRequest oRequest = new BEWSGenCIPRequest();
                oRequest.CAPI = sCCAPI;
                oRequest.CClave = sCCLAVE;
                oRequest.Email = "jpba_666@hotmail.com"; // "correo" + indiceCorreo.ToString().PadLeft(2, '0') + "@puentesarr.in";
                String xml = txtXml.Text.Replace("[correlativo]", i.ToString().PadLeft(2, '0'));
                xml = xml.Replace("[correlativoCorreo]", indiceCorreo.ToString().PadLeft(2, '0'));
                xml = xml.Replace("[Moneda]", sMoneda);
                xml = xml.Replace("[CodServicio]", sCodServicio);
                oRequest.Xml = xml;
                oRequest.Password = oCrypta.BlackBox(xml);

                new DAGenerarData().RegLogGeneracionCIP(oRequest);
                indiceCorreo = ((correlativoCorreoHasta - indiceCorreo) > 0) ? indiceCorreo + 1 : correlativoCorreoDesde;

            }
            //BEWSGenCIPResponse oBEWSGenCIPResponse= oWSCIP.GenerarCIP(oRequest);
        }

        private void tabPage14_Click(object sender, EventArgs e)
        {

        }

        private void lblgordillodeshit_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String asd = "asd";
            lblgordillodeshit.Text = asd.PadLeft(4, '0');
        }

    }
}
