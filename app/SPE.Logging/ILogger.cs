﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SPE.Logging
{
    public interface ILogger
    {
        void Trace(string message);

        void Debug(string message);

        void Info(string message);

        void Warn(string message);

        void Warn(Exception exception);

        void Warn(Exception exception, string message);

        void Error(string message);

        void Error(Exception exception);

        void Error(Exception exception, string message);

        void Fatal(string message);

        void Fatal(Exception exception);

        void Fatal(Exception exception, string message);
    }
}
