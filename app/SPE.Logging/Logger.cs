﻿using System;
using NLog;

namespace SPE.Logging
{
    public class Logger: ILogger
    {
        private static readonly NLog.Logger Log = LogManager.GetCurrentClassLogger();

        public void Trace(string message)
        {
            Log.Trace(message);
        }

        public void Debug(string message)
        {
            Log.Debug(message);
        }

        public void Info(string message)
        {
            Log.Info(message);
        }

        public void Warn(string message)
        {
            Log.Warn(message);
        }

        public void Warn(Exception exception)
        {
            Log.Warn(exception);
        }

        public void Warn(Exception exception, string message)
        {
            Log.Warn(exception, message);
        }

        public void Error(string message)
        {
            Log.Error(message);
        }

        public void Error(Exception exception)
        {
            Log.Error(exception);
        }

        public void Error(Exception exception, string message)
        {
            Log.Error(exception, message);
        }

        public void Fatal(string message)
        {
            Log.Fatal(message);
        }

        public void Fatal(Exception exception)
        {
            Log.Fatal(exception);
        }

        public void Fatal(Exception exception, string message)
        {
            Log.Fatal(exception, message);
        }
    }
}
