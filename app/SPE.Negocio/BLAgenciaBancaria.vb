﻿Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun.ParametrosSistema.WSBancoMensaje
Imports System.Configuration
Imports _3Dev.FW.Entidades
Imports System.ServiceModel
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Globalization
Imports SPE.Logging

Public Class BLAgenciaBancaria
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase
    Private ReadOnly _nlogger As ILogger = New Logger()

    Dim propStrMensaje As System.Text.StringBuilder

    Public Sub New()
        propStrMensaje = New System.Text.StringBuilder
    End Sub

    Private Function InstanciaDAOrdenPago() As DAAgenciaBancaria
        Return CType(DataAccessObject, SPE.AccesoDatos.DAAgenciaBancaria)
    End Function

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New DAAgenciaBancaria
    End Function

#Region "Administración"
    Public Overrides Function SearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Return MyBase.SearchByParameters(be)
    End Function

    Private _lstCodValidacion As Dictionary(Of Integer, String) = Nothing
    Public ReadOnly Property ListaCodValidacion() As Dictionary(Of Integer, String)
        Get
            If _lstCodValidacion Is Nothing Then

                _lstCodValidacion = New Dictionary(Of Integer, String)

                _lstCodValidacion.Add(0, "Transaccion fallida")

                _lstCodValidacion.Add(-1, "Orden de Pago expirada en el proceso de pago")
                _lstCodValidacion.Add(-2, "Codigo de Moneda incorrecta en el proceso de pago")
                _lstCodValidacion.Add(-3, "Importe del pago incorrecto en el proceso de pago")
                _lstCodValidacion.Add(-4, "Orden de Pago esta cancelada en el proceso de pago")
                _lstCodValidacion.Add(-5, "Envio de correo sobre la creacion de agencia bancaria fallida en el proceso de pago")
                _lstCodValidacion.Add(-6, "Envio de correo sobre cancelacion de pago fallida en el proceso de pago")
                _lstCodValidacion.Add(-7, "Registro de notificacion del pago fallida en el proceso de pago")
                _lstCodValidacion.Add(-8, "Orden de Pago no encontrada en el proceso de pago")
                _lstCodValidacion.Add(-9, "Envio de correo sobre la creacion de punto de venta y/o terminal en el proceso de pago")

                _lstCodValidacion.Add(-11, "Orden de Pago no encontrada en el proceso de anulacion")
                _lstCodValidacion.Add(-12, "IdMovimiento no encontrada en el proceso de anulacion")
                _lstCodValidacion.Add(-13, "Codigo de Agencia o Codigo Operacion del Banco invalidos en el proceso de anulacion")
                _lstCodValidacion.Add(-14, "Fecha de Cancelacion o estado de conciliacion invalidos en el proceso de anulacion")
                _lstCodValidacion.Add(-15, "Registro de notificacion de la anulacion fallida en el proceso de anulacion")
                _lstCodValidacion.Add(-16, "Orden de pago no se se encuentra con estado cancelado")
                _lstCodValidacion.Add(-17, "Codigo de Moneda o Importe del pago incorrecto en el proceso de anulacion")
                _lstCodValidacion.Add(-18, "Punto de venta o Terminal invalidos en el proceso de anulacion")
                _lstCodValidacion.Add(-19, "Codigo de convenio invalido en el proceso de anulacion")

                _lstCodValidacion.Add(-21, "Orden de Pago no encontrada en el proceso de extorno o Codigo de servicio invalido")
                _lstCodValidacion.Add(-22, "IdMovimiento no encontrada en el proceso de extorno")
                _lstCodValidacion.Add(-23, "Codigo de Agencia o Codigo Operacion del Banco invalidos en el proceso de extorno")
                _lstCodValidacion.Add(-24, "Fecha de Cancelacion o estado de conciliacion invalidos en el proceso de extorno")
                _lstCodValidacion.Add(-25, "Registro de notificacion del pago fallida en el proceso de extorno")
                _lstCodValidacion.Add(-26, "Codigo de convenio invalido en el proceso de extorno")
                'TIEMPO MAXIMO DE EXTORNO
                _lstCodValidacion.Add(-37, "No se permite extornar se excedio el tiempo limite de extorno")
            End If

            Return _lstCodValidacion
        End Get
    End Property

    Private _listaCodTransaccionBBVA As Dictionary(Of String, String) = Nothing
    Private ReadOnly Property ListaCodTransaccionBBVA() As Dictionary(Of String, String)
        Get
            If (_listaCodTransaccionBBVA Is Nothing) Then
                _listaCodTransaccionBBVA = New Dictionary(Of String, String)

                'Generico (Consulta,Pago,Extorno)
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.TransaccionRealizadaExito, "Transacción realizada con éxito") ' "0001"
                'Consulta
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoSePudoRealizarLaTransaccion, "No se pudo realizar la transacción") ' "3002"
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.EstadoNumeroReferenciaInvalida, "Estado de Número de referencia invalida") '"3013"
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.TramaEnviadaEmpresaInvalida, "Trama enviada a empresa invalida") '"3012"
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NumeroReferenciaNoExiste, "Número de referencia no existe") '"0101"
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoTieneDeudasPendientes, "No tiene deudas Pendientes") '"3009"
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NumeroReferenciaEstadoPagado, "Número de referencia con estado Pagado") '"0106"
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NUmeroReferenciaEstadoExpirado, "Número de referencia con estado expirado") '"0102"
                'Recargas
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.Pago_Realizado, "Pago Realizado") '"1"
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.Pago_no_realizado, "No se Pudo Realizar el Pago") '"2"
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.Extorno_Realizado, "Extorno Realizado") '"3"
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.Extorno_no_Realizado, "No se Pudo Realizar el Extorno") '"4"
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.Cuenta_encontrada, "Cuenta Encontrada") '"5"
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.Cuenta_no_encontrada, "Cuenta no Encontrada") '"6"

                'Pago
                '  _listaCodTransaccionBBVA.Add("0101", "Número de referencia  no existe")
                '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.TramaEnviadaEmpresaInvalida, "Trama enviada a empresa invalida") '"3012"

                'ANULACION SIN EXITO ó TIEMPO MAXIMO DE EXTORNO
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoSePudoRealizarRegistroExtorno, "No se pudo realizar registro de extorno") '"3004"

                ' _listaCodTransaccionBBVA.Add("3012", "Trama enviada a empresa invalida")
                _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoExistePagoParaExtornar, "No Existe Pago para Extornar") '"3014"
            End If
            Return _listaCodTransaccionBBVA
        End Get
    End Property

    Private _listaCodTransaccionBanBif As Dictionary(Of String, String) = Nothing
    Private ReadOnly Property ListaCodTransaccionBanBif() As Dictionary(Of String, String)
        Get
            If (_listaCodTransaccionBanBif Is Nothing) Then
                _listaCodTransaccionBanBif = New Dictionary(Of String, String)

                'Generico (Consulta,Pago,Extorno)
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Proceso_Conforme, "Proceso Conforme")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Deuda_Con_Restricciones, "Deuda con restricciones")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Deuda_No_Valida, "Deuda no Valida")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Error_Proceso_Transaccion, "Error en proceso de transacción")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Limite_Pago_Superado, "Limite de pago superado")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Monto_Pagar_errado, "Monto a pagar errado")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.No_Procede_Extorno_Entidad, "No procede extorno por indicacion de la entidad")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.No_Procede_Pago_Entidad, "No procede pago por indicacion de la entidad")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Sin_Deuda_Pendiente, "Sin deuda pendiente")
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Trama_Invalida, "Trama enviada invalida")

                _listaCodTransaccionBanBif.Add(BanBif.TipoRespuesta.Informacion, "Informacion")
                _listaCodTransaccionBanBif.Add(BanBif.TipoRespuesta.Errores, "Error")
                _listaCodTransaccionBanBif.Add(BanBif.TipoRespuesta.Advertencia_Otros, "Advertencia/Otros")

                'TIEMPO MAXIMO DE EXTORNO
                _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.NoSePermiteExtornar, "Tiempo limite extorno excedido")
            End If
            Return _listaCodTransaccionBanBif
        End Get
    End Property

    Private _listaCodTransaccionKasnet As Dictionary(Of String, String) = Nothing
    Private ReadOnly Property ListaCodTransaccionKasnet() As Dictionary(Of String, String)
        Get
            If (_listaCodTransaccionKasnet Is Nothing) Then
                _listaCodTransaccionKasnet = New Dictionary(Of String, String)

                'Generico (Consulta,Pago,Extorno)
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.TransaccionRealizadaExito, "Transacción realizada con éxito") ' "01"
                'Consulta
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.NoSePudoRealizarTransaccion, "No se pudo realizar la transacción") ' "02"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.TramaEnviadaInvalida, "Trama enviada inválida") '"03"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoPagoNoExiste, "Código de pago no existe") '"04"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoConEstadoPagado, "Código con estado Pagado") '"05"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoConEstadoExpirado, "Código con estado Expirado") '"06"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoConEstadoEliminado, "Código con estado Eliminado") '"07"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoExtornarNoExiste, "Código extornar no existe") '"08"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoExtornarExpirado, "Código extornar expirado") '"09"
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.MontoOMonedaNoCoincide, "Monto o moneda no coincide") '"10"
                'TIEMPO MAXIMO DE EXPIRACION
                _listaCodTransaccionKasnet.Add(kasnet.Mensaje.NoSePermiteExtornar, "Tiempo limite extorno excedido") '"37"
            End If
            Return _listaCodTransaccionKasnet
        End Get
    End Property


    '************************************************************************************************** 
    ' Método          : RegistrarAgenciaBancaria
    ' Descripción     : Registrar una Agencia Bancaria
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Instancia de la Entidad BEAgenciaBancaria Cargada
    ' Parametros_Out  : resultado de la transaccion
    '**************************************************************************************************
    Public Function RegistrarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
        '
        'Utilizamos transaccion para realizar el extorno
        Dim valor As Integer = 0
        Dim options As TransactionOptions = New TransactionOptions()
        options.IsolationLevel = IsolationLevel.ReadCommitted
        'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

        'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)
        Using transaccionscope As New TransactionScope()
            Dim odaAgenciaBancaria As New DAAgenciaBancaria
            valor = odaAgenciaBancaria.RegistrarAgenciaBancaria(obeAgenciaBancaria)
            transaccionscope.Complete()
        End Using
        Return valor

    End Function

    '************************************************************************************************** 
    ' Método          : ActualizarAgenciaBancaria
    ' Descripción     : Actualziacion de una Agencia Bancaria
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Instancia de la Entidad BEAgenciaBancaria Cargada
    ' Parametros_Out  : resultado de la transaccion
    '**************************************************************************************************
    Public Function ActualizarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
        '
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Return odaAgenciaBancaria.ActualizarAgenciaBancaria(obeAgenciaBancaria)
        '
    End Function

    '************************************************************************************************** 
    ' Método          : ConsultarAgenciaBancaria
    ' Descripción     : Consulta de Agencia Bancaria
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Instancia de la Entidad Agencia Bancaria Cargada
    ' Parametros_Out  : Lista de Entidades de tipo BEAgenciaBancaria
    ''**************************************************************************************************
    Public Function ConsultarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As List(Of BusinessEntityBase)
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Dim listresult As List(Of BusinessEntityBase) = odaAgenciaBancaria.ConsultarAgenciaBancaria(obeAgenciaBancaria)
        Return listresult
    End Function

    '************************************************************************************************** 
    ' Método          : ConsultarAgenciaBancariaxCodigo
    ' Descripción     : Consulta de Agencia Bancaria por codigo
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Instancia de la Entidad Agencia Bancaria Cargada
    ' Parametros_Out  : Lista de Entidades de tipo BEAgenciaBancaria
    ''**************************************************************************************************
    Public Function ConsultarAgenciaBancariaxCodigo(ByVal codAgenciaBancaria As String) As BEAgenciaBancaria
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Return odaAgenciaBancaria.ConsultarAgenciaBancariaxCodigo(codAgenciaBancaria)
    End Function

    '************************************************************************************************** 
    ' Método          : ConsultarAgenciaBancariaxId
    ' Descripción     : Consulta de Agencia Bancaria por Id
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 27/12/2008
    ' Parametros_In   : Id Agencia Bancaria
    ' Parametros_Out  : Lista de Entidades de tipo BEAgenciaBancaria
    ''**************************************************************************************************
    Function ConsultarAgenciaBancariaxId(ByVal idAgenciaBancaria As Integer) As BEAgenciaBancaria
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Dim oBEAgenciaBancaria As BEAgenciaBancaria = odaAgenciaBancaria.ConsultarAgenciaBancariaxId(idAgenciaBancaria)
        Return oBEAgenciaBancaria
    End Function

    '************************************************************************************************** 
    ' Método          : DefinirListaOrdenadaAgenciaBancaria
    ' Descripción     : Ordena una lista generica
    ' Autor           : Christian Claros
    ' Fecha/Hora      : 02/01/2009
    ' Parametros_In   : Lista de Entidades de tipo BEAgenciaBancaria
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Sub DefinirListaOrdenadaAgenciaBancaria(ByVal list As System.Collections.Generic.List(Of BEAgenciaBancaria), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "Descripcion"
                list.Sort(New SPE.Entidades.BEAgenciaBancaria.DescAgenciaBancariaBEACComparer())
            Case "Codigo"
                list.Sort(New SPE.Entidades.BEAgenciaBancaria.CodigoAgenciaBancariaBEACComparer())
            Case "Direccion"
                list.Sort(New SPE.Entidades.BEAgenciaBancaria.DireccionAgenciaBancariaBEACComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEAgenciaBancaria.EstadoAgenciaBancariaBEACComparer())

        End Select
    End Sub


    '************************************************************************************************** 
    ' Método          : RegistrarConciliacionBancaria
    ' Descripción     : Registrar Conciliacion Bancaria
    ' Autor           : Alex Paitan Quispe
    ' Fecha/Hora      : 10/01/2009
    ' Parametros_In   : Instancia de la Entidad Conciliacion
    ' Parametros_Out  : Instancia de la Entidad Conciliacion
    ''**************************************************************************************************
    Public Function RegistrarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As BEConciliacion
        Dim IdConciliacion As Integer
        Dim objEmail As New BLEmail()
        Dim objConciliacionBancaria As New BEConciliacion
        Dim objOperacionBancaria As BEOperacionBancaria
        Dim NumeroOperaciones As Integer
        Dim i As Integer
        Try
            Dim options As TransactionOptions = New TransactionOptions()
            options.IsolationLevel = IsolationLevel.ReadCommitted

            Dim odaAgenciaBancaria As New DAAgenciaBancaria()
            IdConciliacion = odaAgenciaBancaria.RegistrarConciliacionBancaria(obeConciliacion)
            If IdConciliacion > 0 Then
                obeConciliacion.IdConciliacion = IdConciliacion
                NumeroOperaciones = obeConciliacion.ListaOperacionesBancarias.Count
                If obeConciliacion.ListaOperacionesBancarias.Count > 1 Then
                    For i = 1 To NumeroOperaciones - 1
                        objOperacionBancaria = New BEOperacionBancaria
                        objOperacionBancaria.IdConciliacion = IdConciliacion
                        objOperacionBancaria.OperacionBancaria = obeConciliacion.ListaOperacionesBancarias.Item(i)

                        Try
                            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                            'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)
                            Using transaccionscope As New TransactionScope()
                                objOperacionBancaria = odaAgenciaBancaria.RegistrarOperacionBancaria(objOperacionBancaria, obeConciliacion.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb) 'wjra se agrego IdOrigenRegistro a pedido de cmiranda
                                If objOperacionBancaria.CodigoOficina.Trim <> "" Then
                                    objEmail.EnviarCorreoGeneracionAgenciaBancaria(objOperacionBancaria.CodigoOficina, ProcesoConciliacion)
                                End If
                                transaccionscope.Complete()
                            End Using
                        Catch ex As Exception
                            'Logger.Write(ex)
                        End Try
                    Next
                    'objOperacionBancaria = Nothing
                End If
                obeConciliacion.OperacionBancaria = ""
                obeConciliacion.NumeroOrdenPago = ""
                obeConciliacion.IdEStado = 0
                obeConciliacion.FechaInicio = Date.Today
                obeConciliacion.FechaFin = Date.Today

                obeConciliacion.ListaOperacionesConciliadas = odaAgenciaBancaria.ConsultarConciliacionBancaria(obeConciliacion)
                obeConciliacion.ListaOperacionesNoConciliadas = odaAgenciaBancaria.ConsultarOperacionesNoConciliadas(obeConciliacion)
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return obeConciliacion
    End Function

    '************************************************************************************************** 
    ' Método          : Consultar conciliación bancaria
    ' Descripción     : Functión que realiza la busqueda de operaciones conciliadas
    ' Autor           : Alex Paitán
    ' Fecha/Hora      : 10/01/2009
    ' Parametros_In   : Instancia de la Entidad Conciliacion
    ' Parametros_Out  : Lista de entidades de operaciones conciliadas
    ''**************************************************************************************************
    Public Function ConsultarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria)
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Dim objListaOperacionesBancarias As List(Of BEOperacionBancaria)
        objListaOperacionesBancarias = odaAgenciaBancaria.ConsultarConciliacionBancaria(obeConciliacion)
        Return objListaOperacionesBancarias
    End Function
#End Region

#Region "Comun"
    '************************************************************************************************** 
    ' Método          : GenerarEstructuraXML
    ' Descripción     : Genera la estructura XML de retorno para el banco
    ' Autor           : Christian Claros
    ' Fecha/Hora      : 02/01/2009
    ' Parametros_In   : string array
    ' Parametros_Out  : Instancia de la Entidad webServiceXResponse
    ''**************************************************************************************************
    Public Function GenerarEstructuraXML(ByVal array() As String) As SPE.Entidades.webServiceXResponse

        Dim WSResponse As New SPE.Entidades.webServiceXResponse()
        WSResponse.webServiceXReturn = New SPE.Entidades.webServiceXResponseWebServiceXReturn()
        WSResponse.webServiceXReturn.stringarray = New SPE.Entidades.webServiceXResponseWebServiceXReturnStringarray
        Dim contador As Int16

        System.Array.Resize(WSResponse.webServiceXReturn.stringarray.Items, array.Length)

        For contador = 0 To array.Length - 1
            If IsNothing(array(contador)) Then
                WSResponse.webServiceXReturn.stringarray.Items(contador) = New Object
            Else
                WSResponse.webServiceXReturn.stringarray.Items(contador) = array(contador)
            End If
        Next

        Return WSResponse

    End Function
    Public Function ExtornarCuentaCommun(ByVal cuenta As BECuentaDineroVirtual, ByVal mov As BEMovimiento) As Integer
        Try
            Dim NotificarRegistroAgenciaOTerminal As Boolean = False
            Dim NotificarRegistroPuntoVenta As Boolean = False
            Dim oDAAgenciaBancaria As New DAAgenciaBancaria
            Dim dv As New DADineroVirtual()

            Using oDAAgenciaBancaria

                Select Case mov.CodigoBanco
                    Case Conciliacion.CodigoBancos.FullCarga
                        'Para FullCarga: Registrar punto de venta y terminal
                        Using oDAPuntoVenta As New DAPuntoVenta
                            Dim AperturaOrigen As Hashtable = oDAPuntoVenta.RegistrarPuntoVentaYTerminalDesdePago(mov.CodigoPuntoVenta, _
                                mov.NumeroSerieTerminal)
                            If AperturaOrigen Is Nothing Then
                                'La apertura origen no se registró correctamente
                                Return 0
                            Else
                                With mov
                                    .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                    .IdTipoOrigenCancelacion = TipoOrigenCancelacion.FullCarga
                                End With
                                NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroTerminal"))
                                NotificarRegistroPuntoVenta = Convert.ToBoolean(AperturaOrigen("RegistroPuntoVenta"))
                            End If
                        End Using
                    Case Conciliacion.CodigoBancos.BBVA, Conciliacion.CodigoBancos.BCP
                        'Para BBVA: Registrar agencia bancaria
                        Dim AperturaOrigen As Hashtable = oDAAgenciaBancaria.RegistrarAgenciaBancariaDesdePago(mov.CodigoBanco, _
                        mov.CodigoAgenciaBancaria)
                        If AperturaOrigen Is Nothing Then
                            'La apertura origen no se registró correctamente
                            Return 0
                        Else
                            With mov
                                .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                .IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                            End With
                            NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroAgenciaBancaria"))
                        End If


                    Case Conciliacion.CodigoBancos.Interbank
                    Case Conciliacion.CodigoBancos.Scotiabank
                    Case Else
                        'Código de banco incorrecto o sin implementar
                        Return 0
                End Select

                'Dim IdTransaccion As Long = dv.RecargarCuenta(cta, Str, mov) 'se cambio por extorno
                Return dv.ExtornarCuenta(cuenta, mov)
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

#End Region

#Region "Integración BCP"

    '************************************************************************************************** 
    ' Método          : AnularOperacionPago
    ' Descripción     : Consulta de Agencia Bancaria
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Instancia de la Entidad Agencia Bancaria Cargada
    ' Parametros_Out  : Lista de Entidades de tipo BEAgenciaBancaria
    ''**************************************************************************************************
    Public Function AnularOperacionPago(ByVal obeMovimiento As BEMovimiento) As Integer
        Dim objBLOrdenPago As New BLOrdenPago
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Dim obeOrdenPago As New BEOrdenPago
        Dim mensaje As Integer
        Dim obeMovimientoRes As BEMovimiento = Nothing
        Dim intFlag = 0
        Try

            'Realizamos las validaciones correspondientes
            With obeOrdenPago
                .NumeroOrdenPago = obeMovimiento.NumeroOrdenPago
                .CodigoOrigenCancelacion = obeMovimiento.CodigoAgenciaBancaria
                .CodigoServicio = obeMovimiento.CodigoServicio
                .CodigoBanco = obeMovimiento.CodigoBanco
            End With

            'Utilizamos transaccion para realizar el extorno
            If (obeMovimiento.CodigoAgenciaBancaria <> "") Then


                If obeMovimiento.NumeroOrdenPago.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP

                    'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
                    'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)
                    mensaje = objBLOrdenPago.ValidarOrdenPagoAgenciaBancaria(obeOrdenPago)
                    If mensaje <> "0" Then
                        Exit Try
                    End If

                    Using transaccionscope As New TransactionScope()

                        'Realizamos el extorno del Movimiento
                        obeMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                        obeMovimientoRes = odaAgenciaBancaria.AnularOperacionPago(obeMovimiento)
                        transaccionscope.Complete()
                        intFlag = 1
                    End Using
                    'Se registra la notificación. UrlError
                    If intFlag = 1 Then
                        Using oblServicionNotificacion As New BLServicioNotificacion()
                            Dim obeOrdenPagon As BEOrdenPago = Nothing
                            Using odaOrdenPago As New DAOrdenPago()
                                obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeMovimiento.NumeroOrdenPago)
                            End Using
                            Dim obeServicio As BEServicio
                            Using odaServicio As New DAServicio()
                                obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
                            End Using

                            If obeServicio.FlgNotificaMovimiento = True Then
                                oblServicionNotificacion.RegistrarServicioNotificacionUrlError(obeOrdenPagon, obeMovimientoRes.IdMovimiento, obeMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion)
                            End If

                        End Using
                        mensaje = BCP.Mensaje.Extorno_Realizado
                    Else
                        Throw New Exception("Sucedio un error al momento de anular")
                    End If
                Else     'Anulando una recarga de dinero

                    Dim dv As New DADineroVirtual()
                    Dim cuenta As New BECuentaDineroVirtual()
                    cuenta.Numero = obeMovimiento.NumeroOrdenPago
                    ' Validar Cuenta
                    cuenta = dv.ConsultarCuentaDineroVirtualUsuarioByNumero(cuenta)

                    If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                        'BCP.Mensaje.Cuenta_no_encontrada()
                        Throw New Exception("Cuenta_no_encontrada")
                    Else

                        'valida que tenga activo el monedero y la cuenta
                        If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                            obeMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                            Dim resultextorno As Integer = ExtornarCuentaCommun(cuenta, obeMovimiento)
                            If resultextorno > 0 Then
                                mensaje = BCP.Mensaje.Extorno_Realizado

                                'Enviar Correo
                                Dim control As New BLEmail
                                control.EnviarCorreoCuentaExtorno(cuenta)

                            Else
                                Throw New Exception("Sucedio un error interno en la BD")
                            End If
                        Else
                            Throw New Exception("La cuenta no esta activa")
                        End If


                    End If
                End If
            Else
                mensaje = BCP.Mensaje.Extorno_no_Realizado
            End If
        Catch ex As Exception
            mensaje = BCP.Mensaje.Extorno_no_Realizado
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return mensaje

    End Function


    '************************************************************************************************** 
    ' Método          : AnularBCP
    ' Descripción     : Anula un pago hecho por BCP  
    ' Parametros_In   : Instancia de la entidad obeMovimiento
    ' Parametros_Out  : Instancia de la entidad BEBCPResponse 
    '**************************************************************************************************
    ' Fecha/Hora Act. : 10/07/2012
    ' Descripción     : Se actualizo el detalle del error , se retiro la validación de codigo de Agencia
    ' Autor           : Jonathan Bastidas
    '**************************************************************************************************
    Public Function AnularBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse

        'Se agrego este detalle de mensaje para especificar mas el motivo de la no anulacion
        Dim detalleMensaje As String = String.Empty

        Dim response As New BEBCPResponse
        Dim odaAgenciaBancaria As New DAAgenciaBancaria
        Dim obeOrdenPago As New BEOrdenPago
        Dim objBLOrdenPago As New BLOrdenPago
        Dim TmpRespuesta(33) As String

        response.Respuesta = TmpRespuesta
        response.Respuesta(33) = 0
        response.Estado = 0

        Dim obeMovimientoRes As New BEMovimiento()

        Try

            'Req. Eliminar validacion de codigo de Agencia Bancaria bcp
            '08/02/2016 - Manuel Rojas.
            'If (obeMovimiento.CodigoAgenciaBancaria <> "") Then
            If obeMovimiento.NumeroOrdenPago.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                With obeOrdenPago
                    .NumeroOrdenPago = obeMovimiento.NumeroOrdenPago
                    .CodigoOrigenCancelacion = obeMovimiento.CodigoAgenciaBancaria
                    .CodigoServicio = obeMovimiento.CodigoServicio
                    .CodigoBanco = obeMovimiento.CodigoBanco
                End With

                'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)
                response.Respuesta(33) = "00" & CType(objBLOrdenPago.ValidarOrdenPagoAgenciaBancaria(obeOrdenPago, detalleMensaje), Integer)
                'Realizamos las validaciones correspondientes
                If response.Respuesta(33) <> "000" Then
                    response.Descripcion1 = "Validación para la anulación del CIP falló"

                    If CType(response.Respuesta(33), Integer) = BCP.Mensaje.Cliente_no_presenta_deuda Then _
                            response.Respuesta(33) = "00" & BCP.Mensaje.Extorno_no_Realizado
                    Exit Try
                End If

                Using transaccionscope As New TransactionScope
                    'Realizamos el extorno del Movimiento
                    obeMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                    obeMovimientoRes = odaAgenciaBancaria.AnularOperacionPago(obeMovimiento)
                    transaccionscope.Complete()
                End Using
                If obeMovimientoRes IsNot Nothing AndAlso obeMovimientoRes.IdMovimiento > 0 Then

                    Dim obeOrdenPagon As BEOrdenPago = Nothing
                    Using odaOrdenPago As New DAOrdenPago()
                        obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeMovimiento.NumeroOrdenPago)
                    End Using

                    Dim obeServicio As BEServicio
                    Using odaServicio As New DAServicio()
                        obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
                    End Using

                    If obeServicio.FlgNotificaMovimiento = True Then
                        'Se registra la notificación. UrlError
                        Using oblServicionNotificacion As New BLServicioNotificacion()

                            If oblServicionNotificacion.RegistrarServicioNotificacionUrlError(obeOrdenPagon, obeMovimientoRes.IdMovimiento, _
                                obeMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then

                                response.Descripcion1 = "Registro de Servicio de Notificacion URL Error, incorrecto"
                                response.Respuesta(33) = "00" & BCP.Mensaje.Extorno_no_Realizado
                                Exit Try
                            End If

                        End Using
                    End If

                    response.Estado = 1
                    response.Descripcion1 = "Transaccion exitosa"
                    response.Respuesta(33) = "00" & BCP.Mensaje.Extorno_Realizado


                Else
                    response.Descripcion1 = "Extorno no realizado - No se pudo realizar el movimiento"
                    response.Respuesta(33) = "00" & BCP.Mensaje.Extorno_no_Realizado
                End If

            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            With response
                .Estado = -1
                .Descripcion2 = ex.Message
                'Agregando el detalle de error:
                .Descripcion1 = .Descripcion1 & " - " & detalleMensaje
            End With
            Throw New FaultException(ex.ToString())
        End Try

        'Agregando el detalle de error:
        response.Descripcion1 = response.Descripcion1 & " - " & detalleMensaje
        Return response

    End Function


    Public Function RegistrarCancelacionOrdenPagoAgenciaBancaria(ByVal obeMovimiento As BEMovimiento) As Integer
        Return RegistrarCancelacionOrdenPagoAgenciaBancariaComun(obeMovimiento, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, Nothing)
    End Function

    '************************************************************************************************** 
    ' Método          : RegistrarCancelacionOrdenPagoAgenciaBancaria
    ' Descripción     : Registro de cancelación de Código de Identificación de Pago por Agencia Bancaria
    ' Autor           : Alex Paitan Quispe
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Instancia de la Entidad Movimiento
    ' Parametros_Out  : Numero entero
    ''**************************************************************************************************
    Public Function RegistrarCancelacionOrdenPagoAgenciaBancariaComun(ByVal obeMovimiento As BEMovimiento, ByVal IdOrigenRegistro As Integer, _
        ByVal FechaCancelacion As Nullable(Of DateTime)) As Integer
        Dim result As Integer = 0
        Dim objEmail As New BLEmail()
        Dim objOperacionBancaria As New BEOperacionBancaria
        Dim bldv As New BLDineroVirtual()
        Dim odaAgenciaBancaria As New DAAgenciaBancaria()
        Try
            If (obeMovimiento.CodigoAgenciaBancaria <> "") Then
                If obeMovimiento.NumeroOrdenPago.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                    'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                    'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)

                    Using transaccionscope As New TransactionScope()


                        objOperacionBancaria = odaAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancariaComun(obeMovimiento, FechaCancelacion)


                        transaccionscope.Complete()
                    End Using
                    If Not objOperacionBancaria Is Nothing Then
                        If objOperacionBancaria.CodigoOficina.Trim <> "" Then
                            objEmail.EnviarCorreoGeneracionAgenciaBancaria(objOperacionBancaria.CodigoOficina, ProcesoCancelacionOP)
                        End If

                        If objOperacionBancaria.NumeroOrdenPago.Trim <> "" Then

                            'Obtener la información completa del CIP
                            Dim obeOrdenPago As BEOrdenPago = Nothing
                            Using odaOrdenPago As New DAOrdenPago()
                                obeOrdenPago = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeMovimiento.NumeroOrdenPago)
                            End Using

                            'objEmail.EnviarCorreoCancelacionOrdenPago(0, objOperacionBancaria.NumeroOrdenPago, objOperacionBancaria.OcultarEmpresa, objOperacionBancaria.DescripcionEmpresa, objOperacionBancaria.DescripcionServicio, objOperacionBancaria.Moneda, objOperacionBancaria.Total.ToString("#,#0.00"), objOperacionBancaria.ConceptoPago, objOperacionBancaria.Email)
                            objEmail.EnviarCorreoCancelacionOrdenPago(obeOrdenPago, objOperacionBancaria.Email)

                            'Registrar a la tabla de notificación para notificar el pago
                            Dim obeServicio As BEServicio
                            Using odaServicio As New DAServicio()
                                obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPago.IdServicio), BEServicio)
                            End Using

                            If obeServicio.FlgNotificaMovimiento = True Then
                                Using oblServicionNotificacion As New BLServicioNotificacion()
                                    oblServicionNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPago, objOperacionBancaria.IdMovimiento, obeMovimiento.IdUsuarioCreacion, IdOrigenRegistro, obeServicio.IdGrupoNotificacion)
                                End Using
                            End If
                        End If
                        result = objOperacionBancaria.IdOrdenPago

                    End If

                Else 'RECARGANDO DINERO A UNA CUENTA

                    Dim dv As New DADineroVirtual()
                    Dim cuenta As New BECuentaDineroVirtual()
                    cuenta.Numero = obeMovimiento.NumeroOrdenPago
                    ' Validar Cuenta
                    cuenta = dv.ConsultarCuentaDineroVirtualUsuarioByNumero(cuenta)

                    If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                        'BCP.Mensaje.Cuenta_no_encontrada()
                        Throw New Exception("Cuenta_no_encontrada")
                    Else
                        If cuenta.EquivalenciaBancaria = obeMovimiento.CodMonedaBanco Then 'obeMovimiento.MonedaAgencia  existe la cuenta en la moneda solicitada

                            'valida que tenga activo el monedero y la cuenta
                            If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                                'Valida si el monto a Recargar es el mismo establecido por el Cliente
                                If cuenta.MontoRecarga = obeMovimiento.Monto Then
                                    'result = dv.RecargarCuenta(cuenta, obeMovimiento.CodigoAgenciaBancaria, New BEMovimiento) 'Aqui
                                    result = bldv.OperacionCuentaGenerico(cuenta, obeMovimiento, "0")
                                    ' BCP.Mensaje.Consulta_Realizada

                                    If result <> -1 Then

                                        If result <> -2 Then
                                            If result > 0 Then
                                                'Enviar Correo
                                                Dim control As New BLEmail
                                                control.EnviarCorreoCuentaRecarga(cuenta)
                                            Else
                                                Throw New Exception("No se ubicó el banco.")
                                            End If
                                        Else
                                            Throw New Exception("El monto ingresado está fuera de los rangos establecidos por el Administrador.")
                                        End If
                                    Else
                                        Throw New Exception("El codigo de servicio BancoMonedero no coincide.")
                                    End If
                                Else
                                    Throw New Exception("El Monto no coincide con el especificado para la cuenta.")
                                End If
                            Else
                                Throw New Exception("La cuenta no esta activa")
                            End If
                        Else
                            Throw New Exception("Moneda no encontrada para la cuenta")
                        End If
                    End If
                End If
            Else
                result = 0
            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)

            With propStrMensaje
                ' cip:xxx|nivel:pagar-bbva-logic|BodyMensajeError;
                .Length = 0
                .Append("cip:" & obeMovimiento.IdOrdenPago & "|nivel:pagar-bcp-logic|" & ex.Message)
            End With
            Throw New Exception(propStrMensaje.ToString())
        End Try
        Return result
    End Function

    ''' <summary>
    '''  pagar orden pago en el bcp
    ''' </summary>
    ''' <param name="obeMovimiento"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PagarBCP(ByVal obeMovimiento As BEMovimiento)

        Dim response As New BEBCPResponse
        Dim objEmail As New BLEmail()
        Dim objOperacionBancaria As New BEOperacionBancaria
        Dim TmpRespuesta(33) As String
        Dim odaAgenciaBancaria As New DAAgenciaBancaria()
        response.Respuesta = TmpRespuesta
        response.Respuesta(33) = 0
        response.Estado = 0

        Dim objMovimiento As New BEMovimiento()

        Try

            If (obeMovimiento.CodigoAgenciaBancaria <> "") Then

                'Dim transOptions As New TransactionOptions()
                'transOptions.IsolationLevel = IsolationLevel.ReadCommitted
                'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
                Using transaccionscope As New TransactionScope()
                    'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)

                    objOperacionBancaria = odaAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria(obeMovimiento)

                    transaccionscope.Complete()

                End Using

                If objOperacionBancaria IsNot Nothing Then
                    'validacion del envio de email por creacion de agencia bancaria
                    'Comentado 28/01/2015 no se notifica el envio de correo al generar agencia
                    'If objOperacionBancaria.CodigoOficina.Trim <> "" Then

                    '    If objEmail.EnviarCorreoGeneracionAgenciaBancaria(objOperacionBancaria.CodigoOficina, _
                    '        ProcesoCancelacionOP) <> 1 Then
                    '        response.Descripcion1 += "Envio de email de generacion de agencia bancaria, incorrecto. "
                    '    End If

                    'End If

                    If objOperacionBancaria.NumeroOrdenPago.Trim <> "" Then

                        'Obtener la información completa del CIP
                        Dim obeOrdenPago As BEOrdenPago = Nothing
                        Using odaOrdenPago As New DAOrdenPago()
                            obeOrdenPago = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeMovimiento.NumeroOrdenPago)
                        End Using

                        If objEmail.EnviarCorreoCancelacionOrdenPago(obeOrdenPago, objOperacionBancaria.Email) <> 1 Then
                            response.Descripcion1 += "Enviar de Correo de Cancelacion de Orden de Pago incorrecto. "
                        End If

                        Dim obeServicio As BEServicio
                        Using odaServicio As New DAServicio()
                            obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPago.IdServicio), BEServicio)
                        End Using

                        'Registrar a la tabla de notificación para notificar el pago
                        If obeServicio.FlgNotificaMovimiento = True Then
                            Using oblServicionNotificacion As New BLServicioNotificacion()
                                If oblServicionNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPago, _
                                    objOperacionBancaria.IdMovimiento, obeMovimiento.IdUsuarioCreacion, _
                                        ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then
                                    response.Descripcion1 += "Registro de Servicio de Notificacion de Url Ok, incorrecta. " 'wjra se agrego IdOrigenRegistro a pedido de cmiranda

                                End If

                            End Using
                        End If

                        response.Estado = 1
                        response.Descripcion1 += "Transaccion exitosa"
                        response.Respuesta(33) = objOperacionBancaria.IdOrdenPago

                    Else
                        response.Descripcion1 = "Numero de Orden de Pago vacio u/o cancelacion de la Orden de Pago incorrecto"
                    End If

                Else
                    response.Descripcion1 = "Cancelacion de Orden de Pago incorrecto"
                End If

            Else
                response.Estado = 0
                response.Descripcion1 += "Codigo de Agencia Bancaria sin valor"
            End If
            ''''''''''''''''''''''''''''//////////////////////////////////////////

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)

            With response
                .Estado = -1
                .Descripcion1 = "Hubo un error en la aplicación."
                .Descripcion2 = ex.ToString()
                'Throw New _3Dev.FW.Excepciones.FWBusinessException(.Mensaje, response)
            End With

        End Try

        Return response

    End Function

    '************************************************************************************************** 
    ' Método          : ConsultarOrdenPagoAgenciaBancaria
    ' Descripción     : Consultar Código de Identificación de Pago por Agencia Bancaria
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 20/02/2009
    ' Parametros_In   : Instancia de la Entidad Código de Identificación de Pago
    ' Parametros_Out  : Instancia de la Entidad Código de Identificación de Pago
    ''**************************************************************************************************
    Public Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String() ' As SPE.Entidades.webServiceXResponse
        Dim odaAgenciaBancaria As New DAAgenciaBancaria()
        Dim objOrdenPago As New BEOrdenPago()
        Dim wsResponse As New SPE.Entidades.webServiceXResponse

        Dim respuesta(33) As String

        If obeOrdenPago.NumeroOrdenPago.Substring(0, 2) <> "99" Then 'BUSCANDO DENTRO DE LOS CIPS
            objOrdenPago = odaAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)
            If Not objOrdenPago Is Nothing Then 'si objOrdenPago  es diferente de nulo significa que se encontro el CIP
                If ((Not objOrdenPago.CipListoParaExpirar) And (objOrdenPago.IdEstado = EstadoOrdenPago.Generada)) Then
                    respuesta(2) = objOrdenPago.ConceptoPago
                    respuesta(3) = objOrdenPago.FechaVencimiento.ToString("yyyy-MM-dd")
                    respuesta(4) = objOrdenPago.NumeroOrdenPago
                    respuesta(5) = objOrdenPago.Total.ToString()
                    respuesta(33) = "00" & BCP.Mensaje.Consulta_Realizada
                Else
                    respuesta(33) = "00" & BCP.Mensaje.Cliente_no_presenta_deuda
                End If
            Else
                respuesta(33) = "00" & BCP.Mensaje.Cliente_no_encontrado
            End If

        Else ' Se busca dentro de las cuentas de dinero virtual
            Dim dv As New DADineroVirtual()
            Dim cuenta As New BECuentaDineroVirtual()
            cuenta.Numero = obeOrdenPago.NumeroOrdenPago
            Dim CdBco As String
            CdBco = Conciliacion.CodigoBancos.BCP
            ' Validar Cuenta
            cuenta = dv.ConsultarCuentaDineroByNumeroWS(cuenta, obeOrdenPago.CodigoServicio, CdBco)
            '----------------------------------------------------------
            If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                'respuesta(33) = "00" & BCP.Mensaje.Cuenta_no_encontrada
                respuesta(33) = "00" & BCP.Mensaje.Cliente_no_encontrado
            Else
                If cuenta.EquivalenciaBancaria = obeOrdenPago.MonedaAgencia Then 'existe la cuenta en la moneda solicitada

                    'valida que tenga activo el monedero y la cuenta
                    If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                        respuesta(2) = "Cuenta Encontrada"
                        respuesta(3) = Today.AddDays(10).ToString("yyyy-MM-dd")
                        respuesta(4) = cuenta.Numero
                        respuesta(5) = cuenta.MontoRecarga
                        respuesta(33) = "00" & BCP.Mensaje.Consulta_Realizada
                    Else
                        'respuesta(33) = "00" & BCP.Mensaje.Cuenta_no_encontrada
                        respuesta(33) = "00" & BCP.Mensaje.Cliente_no_encontrado
                    End If
                Else
                    'no tiene activa la funcion de monedero virtual
                    'respuesta(33) = "00" & BCP.Mensaje.Cuenta_no_encontrada
                    respuesta(33) = "00" & BCP.Mensaje.Cliente_no_encontrado
                End If
            End If
        End If
        Return respuesta
        'Return GenerarEstructuraXML(respuesta)
    End Function


    ''' <summary>
    ''' consultar cip por el bcp
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarBCP(ByVal obeOrdenPago As BEOrdenPago) As BEBCPResponse

        Dim response As New BEBCPResponse
        Dim TmpRespuesta(33) As String

        Dim odaAgenciaBancaria As New DAAgenciaBancaria()
        response.Respuesta = TmpRespuesta
        response.Respuesta(33) = 0

        Dim objOrdenPago As BEOrdenPago

        Try
            response.Estado = 0
            objOrdenPago = odaAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)

            If Not objOrdenPago Is Nothing Then
                If ((Not objOrdenPago.CipListoParaExpirar) And (objOrdenPago.IdEstado = EstadoOrdenPago.Generada)) Then

                    TmpRespuesta(2) = objOrdenPago.ConceptoPago
                    TmpRespuesta(3) = objOrdenPago.FechaVencimiento.ToString("yyyy-MM-dd")
                    TmpRespuesta(4) = objOrdenPago.NumeroOrdenPago
                    TmpRespuesta(5) = objOrdenPago.Total.ToString()

                    '---------------------------------------------------------
                    TmpRespuesta(33) = "00" & BCP.Mensaje.Consulta_Realizada
                    '---------------------------------------------------------

                    response.Estado = 1
                    response.Descripcion1 = "Consulta Realizada con exito"

                Else

                    '--------------------------------------------------------------
                    TmpRespuesta(33) = "00" & BCP.Mensaje.Cliente_no_presenta_deuda
                    '--------------------------------------------------------------   

                    If objOrdenPago.IdEstado = EstadoOrdenPago.Generada Then
                        If objOrdenPago.CipListoParaExpirar Then
                            response.Descripcion1 = "Orden de pago ha expirado"
                        End If
                    Else
                        With objOrdenPago
                            response.Descripcion1 += "Orden de pago con estado " + IIf(.IdEstado = EstadoOrdenPago.Anulada, "Anulado", _
                            IIf(.IdEstado = EstadoOrdenPago.Cancelada, "Cancelado", _
                            IIf(.IdEstado = EstadoOrdenPago.Eliminado, "Eliminado", "Expirado")))
                        End With
                    End If

                End If

            Else
                '----------------------------------------------------------
                TmpRespuesta(33) = "00" & BCP.Mensaje.Cliente_no_encontrado
                '----------------------------------------------------------

                response.Descripcion1 = "Cliente no encontrado"

            End If

            response.Respuesta = TmpRespuesta



        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)

            '----------------------------------------------------------
            TmpRespuesta(33) = "00" & BCP.Mensaje.Cliente_no_encontrado
            '----------------------------------------------------------

            With response
                .Estado = -1
                .Descripcion1 = "Hubo un error en la aplicación"
                .Descripcion2 = ex.ToString()
                .Respuesta = TmpRespuesta
            End With
        End Try
        Return response
    End Function


    '************************************************************************************************** 
    ' Método          : DoDefineOrderedList
    ' Descripción     : Define la clase con la cual se va ordenar para BusinessEntityBase para Servicio
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "NumeroOperacion"
                list.Sort(New SPE.Entidades.BEOperacionBancaria.NumeroOperacionBEACComparer())
            Case "NumeroOrdenPago"
                list.Sort(New SPE.Entidades.BEOperacionBancaria.NumeroOrdenPagoBEACComparer())
            Case "FechaConciliacion"
                list.Sort(New SPE.Entidades.BEOperacionBancaria.FechaConciliacionBEACComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEOperacionBancaria.DescripcionEstadoBEACComparer())
            Case "Observacion"
                list.Sort(New SPE.Entidades.BEOperacionBancaria.ObservacionBEACComparer())

        End Select
    End Sub

#End Region

#Region "Integración BBVA"

    Private Function ObtenerMonedaBanco(ByVal codBanco As String, ByVal codMonedaBanco As Integer) As BEMonedaBanco
        For Each item As BEMonedaBanco In CacheServer.ListaMonedaBanco
            If (item.codBanco = codBanco And item.codMonedaBanco = codMonedaBanco) Then
                Return item
            End If
        Next
        Return Nothing
    End Function


    Public Function ConsultarBBVA(ByVal request As BEBBVAConsultarRequest) As BEBBVAResponse(Of BEBBVAConsultarResponse)

        Dim response As New BEBBVAResponse(Of BEBBVAConsultarResponse)
        response.ObjBBVA = New BEBBVAConsultarResponse()

        Dim obeOrdenPagoReq As New BEOrdenPago
        Dim obeOrdenPagoRes As New BEOrdenPago
        Dim obeConsDet As New BEBBVAConsultarDetalle
        Dim TmpRespuesta(33) As String

        response.ObjBBVA.detalle = New List(Of BEBBVAConsultarDetalle)

        With obeConsDet
            .numeroReferenciaDocumento = ""         ' NRO. REFER. DOCUMENTO
            .importeDeudaDocumento = ""             ' IMPORTE DEUDA DOCUMENTO
            .importeDeudaMinimaDocumento = ""       ' IMPORTE DEUDA MIN. DOCUMENTO
            .fechaVencimientoDocumento = ""         ' FECH. VENCIMIENTO DOCUMENTO
            .fechaEmisionDocumento = ""             ' FECH. EMISION  DOCUMENTO
            .descripcionDocumento = ""              ' DESCRIPCION DOCUMENTO
            .numeroDocumento = ""                   ' NRO. DEL DOCUMENTO
            .indicadorRestriccPago = ""             ' INDICADOR RESTRICCION PAGO
            .indicadorSituacionDocumento = ""       ' INDICADOR SITUACION DOCUMENTO
            .cantidadSubconceptos = ""              ' CANT. SUB-CONCEPTOS

        End With

        response.ObjBBVA.detalle.Add(obeConsDet)

        Try
            'CIP NORMAL
            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBBVA

                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipocliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigocliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim      ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim 'REFER. DEUDA ADICIONAL
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA
                .datoCliente = ""                           ' DATO DEL CLIENTE (opcional)
                .cantidadDocumentos = "00"                   ' CANT. DE DOCUMENTOS "0"

            End With

            response.Estado = 0

            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request

                If .codigoOperacion.Trim = "" OrElse .numeroOperacion.Trim = "" OrElse .codigoBanco.Trim = "" OrElse _
                .codigoConvenio.Trim = "" OrElse .numeroReferenciaDeuda.Trim = "" OrElse .canalOperacion.Trim = "" OrElse _
                .codigoOficina.Trim = "" OrElse .fechaOperacion.Trim = "" OrElse .horaOperacion.Trim = "" Then

                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TramaEnviadaEmpresaInvalida
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

                    response.Mensaje = "Algunos de los campos mandatorios no tienen valor."

                    Return response

                End If

            End With

            '--------------------------------------------------------
            ' 2. INPUT - ORDEN DE PAGO
            '--------------------------------------------------------

            ' 2.1 CIP
            '---------
            obeOrdenPagoReq.NumeroOrdenPago = request.numeroReferenciaDeuda.Trim

            ' 2.2 CODIGO DEL BANCO
            '----------------------
            obeOrdenPagoReq.CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA

            ' 2.2 CODIGO DE CONVENIO
            '----------------------
            obeOrdenPagoReq.CodigoServicio = request.codigoConvenio

            Using odaAgenciaBancaria As New DAAgenciaBancaria()

                '--------------------------------------------------------
                ' 3. CONSULTAR CIP
                '--------------------------------------------------------
                obeOrdenPagoRes = odaAgenciaBancaria.ConsultarCIPDesdeBanco(obeOrdenPagoReq)

                '--------------------------------------------------------
                ' 4. OUTPUT - ORDEN DE PAGO
                '--------------------------------------------------------

                ' 4.1 VALIDAR SI EXISTE ORDEN DE PAGO
                '-------------------------------------
                If obeOrdenPagoRes Is Nothing Then
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NumeroReferenciaNoExiste
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

                    response.Mensaje = "Orden de Pago no existe"

                    Return response
                End If

                '-------------------------------------------------------------------
                ' 5. ESTRUCTURAR DETALLE DEL OBJETO "RESPONSE" CON SU PROPIEDADES
                '-------------------------------------------------------------------

                With response.ObjBBVA

                    .codigoMoneda = obeOrdenPagoRes.codMonedaBanco.Trim   ' COD. DE LA MONEDA
                    .datoCliente = obeOrdenPagoRes.DescripcionCliente.Trim ' dato del cliente

                    .detalle(0).numeroReferenciaDocumento = obeOrdenPagoRes.NumeroOrdenPago          ' NRO. REFER. DOCUMENTO

                    .detalle(0).importeDeudaDocumento = obeOrdenPagoRes.Total.ToString("#00000000000.00").Replace(".", "")       ' IMPORTE DEUDA DOCUMENTO
                    .detalle(0).importeDeudaMinimaDocumento = obeOrdenPagoRes.Total.ToString("#00000000000.00").Replace(".", "") ' IMPORTE DEUDA MIN. DOCUMENTO

                    ' FECH. VENCIMIENTO DOCUMENTO
                    .detalle(0).fechaVencimientoDocumento = obeOrdenPagoRes.FechaVencimiento.ToString("yyyy-MM-dd")
                    ' FECH. EMISION  DOCUMENTO
                    .detalle(0).fechaEmisionDocumento = obeOrdenPagoRes.FechaEmision.ToString("yyyy-MM-dd")
                    ' DESCRIPCION DOCUMENTO
                    .detalle(0).descripcionDocumento = obeOrdenPagoRes.ConceptoPago
                    ' NRO. DEL DOCUMENTO
                    .detalle(0).numeroDocumento = obeOrdenPagoRes.NumeroOrdenPago
                    ' INDICADOR RESTRICCION PAGO
                    .detalle(0).indicadorRestriccPago = ""
                    ' INDICADOR SITUACION DOCUMENTO
                    .detalle(0).indicadorSituacionDocumento = ""
                    ' CANT. SUB-CONCEPTOS
                    .detalle(0).cantidadSubconceptos = "00"

                End With

                'response.ObjBBVA.cantidadDocumentos = "00"   ' CANT. DE DOCUMENTOS "0"
                response.ObjBBVA.detalle(0).numeroDocumento = "000"
                '-------------------------------------------------------------------
                ' 6. CODIGO DE RESULTADO
                '-------------------------------------------------------------------
                If (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Eliminado) Then
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NumeroReferenciaNoExiste
                    response.Mensaje = "Orden de Pago esta eliminado"

                ElseIf (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada) Then
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NumeroReferenciaEstadoPagado
                    response.Mensaje = "Orden de Pago ya esta cancelado"

                ElseIf ((obeOrdenPagoRes.CipListoParaExpirar) OrElse _
                (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NUmeroReferenciaEstadoExpirado
                    response.Mensaje = "Orden de Pago ha expirado"

                Else
                    ' EXITO
                    response.ObjBBVA.cantidadDocumentos = "01"   ' CANT. DE DOCUMENTOS "1"
                    response.ObjBBVA.detalle(0).numeroDocumento = "001"

                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TransaccionRealizadaExito
                    response.Estado = 1
                    response.Mensaje = "Transaccion exitosa"

                End If

                ' MENSAJE RESULTADO
                response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)

            response.Estado = -1
            response.Mensaje = ex.Message
            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

        End Try

        Return response

    End Function

    Public Function PagarBBVA(ByVal request As BEBBVAPagarRequest) As BEBBVAResponse(Of BEBBVAPagarResponse)

        Dim response As New BEBBVAResponse(Of BEBBVAPagarResponse)
        response.ObjBBVA = New BEBBVAPagarResponse()

        Dim obeMovimiento As New BEMovimiento
        Dim resDetalle As BEBBVAPagarDetalle
        Dim IdMovimiento As Integer = 0

        resDetalle = request.detalle(0)

        Try

            '--------------------------------------------------------
            ' 0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBBVA
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim  ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim    'REFER. DEUDA ADICIONAL
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
                .numeroOperacionEmpresa = "0"   ' NRO. DE OPERAC. DE LA EMPRESA
            End With

            response.Estado = 0

            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request

                If .codigoOperacion.Trim = "" OrElse _
                    .numeroOperacion.Trim = "" OrElse _
                    .codigoBanco.Trim = "" OrElse .codigoConvenio.Trim = "" OrElse .cuentaRecaudadora.Trim = "" OrElse _
                    .numeroReferenciaDeuda.Trim = "" OrElse .cantidadDocumentos.Trim = "" OrElse .formaPago.Trim = "" OrElse _
                    .codigoMoneda.Trim = "" OrElse .importeTotalPagado.Trim = "" OrElse _
                    .detalle(0).numeroReferenciaDocumento.Trim = "" OrElse .detalle(0).descripcionDocumento.Trim = "" OrElse _
                    .detalle(0).numeroOperacionBanco.Trim = "" OrElse .detalle(0).importeDeudaPagada.Trim = "" OrElse _
                    .canalOperacion.Trim = "" OrElse .codigoOficina.Trim = "" OrElse .fechaOperacion.Trim = "" OrElse _
                    .horaOperacion.Trim = "" Then

                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TramaEnviadaEmpresaInvalida
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

                    'estado y mensaje para el registro de log                    
                    response.Mensaje = "Algunos de los campos mandatorios no presentan valor."

                    _nlogger.Info(String.Format("NumeroReferencia:{0}, Mensaje:{1}", .numeroReferenciaDeuda, response.Mensaje))


                    Return response

                End If

            End With

            '--------------------------------------------------------
            ' 2. INPUT - MOVIMIENTO
            '--------------------------------------------------------
            With obeMovimiento

                .NumeroOrdenPago = request.numeroReferenciaDeuda.Trim 'Nro. Orden Pago
                .CodMonedaBanco = request.codigoMoneda.Trim 'Codigo Moneda

                .NumeroOperacion = request.numeroOperacion.Trim 'Nro. Operacion del Banco
                .CodigoAgenciaBancaria = request.codigoOficina.Trim ' Cod. Agencia Bancaria
                .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA 'Cod. Banco
                .CodMonedaBanco = request.codigoMoneda.Trim
                .CodigoMedioPago = request.formaPago.Trim
                .CodigoServicio = request.codigoConvenio

                'SE AGREGO CANAL OPERACION
                .CodigoCanal = request.canalOperacion.Trim


                'conversion del monto
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.importeTotalPagado.Trim) / 100 'Importe

            End With

            '--------------------------------------------------------
            ' 3. PROCESO DE PAGO POR BANCO "BBVA"
            '--------------------------------------------------------

            'Dim transOptions As New TransactionOptions()
            'transOptions.IsolationLevel = IsolationLevel.ReadCommitted

            ' Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
            'Using transaccionscope As New TransactionScope()
            ' Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)

            'Using transaccionscope As New TransactionScope() 'Comentado el 06/04

            IdMovimiento = PagarDesdeBancoComun(obeMovimiento, Nothing, Nothing)

            If IdMovimiento > 0 Then

                response.ObjBBVA.codigoResultado = BBVA.Mensaje.TransaccionRealizadaExito
                response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                response.ObjBBVA.numeroOperacionEmpresa = IdMovimiento.ToString

                response.Estado = 1
                response.Mensaje = "Transaccion exitosa"

                'transaccionscope.Complete() 'Comentado el 06/04

            Else

                response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoSePudoRealizarLaTransaccion
                response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

                response.Mensaje = ListaCodValidacion(IdMovimiento)

            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Estado = -1
            response.Mensaje = ex.Message
            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)
        End Try

        Return response

    End Function

    Public Function PagarDesdeBancoComun(ByVal oBEMovimiento As BEMovimiento, ByVal IdOrigenRegistro As Nullable(Of Integer), _
        ByVal FechaCancelacion As Nullable(Of DateTime)) As Integer
        Try

            Dim oBEOrdenPagoReq As New BEOrdenPago
            With oBEOrdenPagoReq
                .NumeroOrdenPago = oBEMovimiento.NumeroOrdenPago
                .CodigoBanco = oBEMovimiento.CodigoBanco
                .CodigoServicio = oBEMovimiento.CodigoServicio
            End With
            _nlogger.Info(String.Format("PagarDesdeBancoComun - NumeroOrdenPago:{0}, CodigoBanco:{1}, CodigoServicio:{2}", oBEOrdenPagoReq.NumeroOrdenPago, oBEOrdenPagoReq.CodigoBanco, oBEOrdenPagoReq.CodigoServicio))
            Using oDAAgenciaBancaria As New DAAgenciaBancaria()
                Dim oBEOrdenPagoRes As BEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPagoReq)
                '.Agregar validacion para indicar q el cip esta en proceso de pago
                If oBEOrdenPagoRes Is Nothing Then
                    'CIP no existe
                    Return -8
                ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Eliminado Then
                    'CIP está eliminado
                    Return -27
                ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Cancelada Then
                    'CIP está cancelado
                    Return -4
                ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Expirado Or _
                    oBEOrdenPagoRes.CipListoParaExpirar Then
                    'CIP ha expirado
                    Return -1
                ElseIf oBEOrdenPagoRes.codMonedaBanco <> oBEMovimiento.CodMonedaBanco Then
                    'Código moneda de entrada es correcto
                    Return -2
                ElseIf oBEOrdenPagoRes.Total <> oBEMovimiento.Monto Then
                    'Monto de entrada es corecto
                    Return -3
                Else
                    'M
                    'Dim objParametros As New ParametrosAgenciaBancariaComun()
                    'objParametros.oBEMovimiento = oBEMovimiento
                    'objParametros.oDAAgenciaBancaria = oDAAgenciaBancaria
                    'Rommel Background
                    'Thread.Sleep(10000)

                    Dim NotificarRegistroAgenciaOTerminal As Boolean = False
                    Dim NotificarRegistroPuntoVenta As Boolean = False
                    Dim intFlag As Integer
                    'Registrar datos de agencia o punto de venta según banco
                    Select Case oBEMovimiento.CodigoBanco
                        'M MoMo
                        Case Conciliacion.CodigoBancos.MoMo
                            'Para MoMo aun esta pendiente
                            Using oDAPuntoVenta As New DAPuntoVenta
                                Dim AperturaOrigen As Hashtable = oDAPuntoVenta.RegistrarPuntoVentaYTerminalDesdePago(oBEMovimiento.CodigoPuntoVenta, _
                                    oBEMovimiento.NumeroSerieTerminal)
                                If AperturaOrigen Is Nothing Then
                                    'La apertura origen no se registró correctamente
                                    Return 0
                                Else
                                    With oBEMovimiento
                                        .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.MoMo
                                    End With
                                    NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroTerminal"))
                                    NotificarRegistroPuntoVenta = Convert.ToBoolean(AperturaOrigen("RegistroPuntoVenta"))
                                    intFlag = 1
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.FullCarga, Conciliacion.CodigoBancos.Kasnet
                            'Para FullCarga: Registrar punto de venta y terminal
                            Using oDAPuntoVenta As New DAPuntoVenta
                                Dim AperturaOrigen As Hashtable = oDAPuntoVenta.RegistrarPuntoVentaYTerminalDesdePago(oBEMovimiento.CodigoPuntoVenta, _
                                    oBEMovimiento.NumeroSerieTerminal)
                                If AperturaOrigen Is Nothing Then
                                    'La apertura origen no se registró correctamente
                                    _nlogger.Error(String.Format("RegistrarPuntoVentaYTerminalDesdePago - NumeroOrdenPago:{0}, Mensaje:{1}", oBEOrdenPagoReq.NumeroOrdenPago, "La apertura origen no se registró correctamente"))
                                    Return 0
                                Else
                                    With oBEMovimiento
                                        .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.FullCarga
                                    End With
                                    NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroTerminal"))
                                    NotificarRegistroPuntoVenta = Convert.ToBoolean(AperturaOrigen("RegistroPuntoVenta"))
                                    intFlag = 1
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.BBVA, Conciliacion.CodigoBancos.Scotiabank, Conciliacion.CodigoBancos.Interbank, Conciliacion.CodigoBancos.WesterUnion, Conciliacion.CodigoBancos.BanBif
                            'Para BBVA: Registrar agencia bancaria
                            Dim AperturaOrigen As Hashtable = oDAAgenciaBancaria.RegistrarAgenciaBancariaDesdePago(oBEMovimiento.CodigoBanco, _
                                oBEMovimiento.CodigoAgenciaBancaria)
                            If AperturaOrigen Is Nothing Then
                                'La apertura origen no se registró correctamente
                                _nlogger.Error(String.Format("RegistrarAgenciaBancariaDesdePago - NumeroOrdenPago:{0}, Mensaje:{1}", oBEOrdenPagoReq.NumeroOrdenPago, "La apertura origen no se registró correctamente"))
                                Return 0
                            Else
                                With oBEMovimiento
                                    .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                    .IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                                End With
                                NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroAgenciaBancaria"))
                                intFlag = 1
                            End If
                        Case Conciliacion.CodigoBancos.Life
                            'Agregamos Tipo Origen cancelacion como Agencia Recaudadora.
                            oBEMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaRecaudadora
                            oBEMovimiento.CodigoMedioPago = MedioPago.Tarjeta
                            Using oDAPuntoVenta As New DAPuntoVenta
                                oBEMovimiento.IdAperturaOrigen = oDAPuntoVenta.ObtenerAperturaOrigenLife(oBEMovimiento.IdTipoOrigenCancelacion)
                            End Using
                        Case Else
                            'Código de banco incorrecto o sin implementar
                            Return 0
                    End Select

                    '1. Comprabamos si se usará la implementación del Fix EXtorno Automatico.
                    Dim UsarFixExtorno As Integer = 0
                    Try
                        Using oDAComun As New DAComun()
                            If oDAComun.HabilitarNuevaVersionExtornoInterno(oBEMovimiento.CodigoBanco, oBEOrdenPagoRes.NumeroOrdenPago) = 1 Then
                                UsarFixExtorno = 1
                            End If
                        End Using
                    Catch ex As Exception
                        _nlogger.Error(ex, ex.Message)
                        UsarFixExtorno = 0
                    End Try


                    'Registrar el pago
                    Dim oBEOperacionBancaria As New BEOperacionBancaria

                    '2. FixExtorno 28-03-14: Actualizar el Cip a estado 26(En Proceso de Pago)
                    If UsarFixExtorno = 1 Then
                        Using oDAOrdenPago As New DAOrdenPago()
                            oDAOrdenPago.RestaurarEstadoPago(oBEMovimiento.NumeroOrdenPago, EstadoOrdenPago.EnProcesoPago)
                        End Using
                        If ConfigurationManager.AppSettings("HabilitarSleep") Then
                            Thread.Sleep(ConfigurationManager.AppSettings("TiempoPago"))
                        End If
                    End If

                    Using transaccionscope As New TransactionScope()
                        oBEOperacionBancaria = oDAAgenciaBancaria.PagarCIPDesdeBancoComun(oBEMovimiento, FechaCancelacion)
                        transaccionscope.Complete()
                    End Using

                    'Validar registro de pago
                    If oBEOperacionBancaria Is Nothing Then
                        '2. FixExtorno 28-03-14: En caso el proceso de pago falle, se cambia el estado a 22(Generado)
                        If UsarFixExtorno = 1 Then
                            Using oDAOrdenPago As New DAOrdenPago()
                                oDAOrdenPago.RestaurarEstadoPago(oBEMovimiento.NumeroOrdenPago, EstadoOrdenPago.Generada)
                            End Using
                        End If
                        Return 0
                    ElseIf oBEOperacionBancaria.NumeroOrdenPago.Trim = "" Or oBEOperacionBancaria.IdMovimiento <= 0 Then
                        '2. FixExtorno 28-03-14: En caso el proceso de pago falle, se cambia el estado a 22(Generado)
                        If UsarFixExtorno = 1 Then
                            Using oDAOrdenPago As New DAOrdenPago()
                                oDAOrdenPago.RestaurarEstadoPago(oBEMovimiento.NumeroOrdenPago, EstadoOrdenPago.Generada)
                            End Using
                        End If
                        Return 0
                    End If

                    'Consultar los datos de la orden de pago
                    Dim oEBOrdenPago As BEOrdenPago = Nothing
                    Using oDAOrdenPago As New DAOrdenPago()
                        oEBOrdenPago = oDAOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(oBEMovimiento.NumeroOrdenPago)
                    End Using

                    'Registrar a la tabla de notificación para notificar el pago
                    If oEBOrdenPago Is Nothing Then
                        Return 0
                    ElseIf oEBOrdenPago.IdEstado = EstadoOrdenPago.Cancelada Then

                        '3. FixExtorno 28-03-14: Validar si hay Extorno interno en cola, 
                        '3.1. En caso exista Extornar el Cip Pagado, no registrar en la tabla ServicioNotificacion y no enviar mails a los usuarios.
                        '3.2. Genero el extorno del cip con tipo de movimiento 31(Extorno Interno), actualizo los intentos de extorno a procesados y retorno idMovimiento 0
                        If UsarFixExtorno = 1 Then
                            Using oDAOrdenPago As New DAOrdenPago()
                                Dim lstIntentosExtorno As New List(Of BEIntentoExtorno)

                                '3.3. Obtengo los intentos de extorno registrado.
                                lstIntentosExtorno = oDAOrdenPago.ObtenerCantidadIntentoExtorno(oEBOrdenPago.IdOrdenPago)
                                If lstIntentosExtorno.Count > 0 Then
                                    Dim i As Integer = 0
                                    For i = 0 To lstIntentosExtorno.Count - 1

                                        '3.4. Valido que la información de los intentos de extorno sea correcta para realizar la operación.
                                        If lstIntentosExtorno(i).IdOrdenPago = oBEMovimiento.NumeroOrdenPago And _
                                           lstIntentosExtorno(i).NumeroOperacion = oBEMovimiento.NumeroOperacion And _
                                           lstIntentosExtorno(i).CodBanco = oBEMovimiento.CodigoBanco And _
                                           lstIntentosExtorno(i).CodServicio = oBEMovimiento.CodigoServicio And _
                                           lstIntentosExtorno(i).CodAgencia = Right(oBEMovimiento.CodigoAgenciaBancaria.Trim, 6).ToString().PadLeft(6, "0") Then
                                            '3.5 Defino que él id del tiop de movimiento para el extorno será 31
                                            oBEMovimiento.IdTipoMovimiento = TipoMovimiento.ExtornoAutomatico
                                            Dim idMoviminentoExtorno As Integer
                                            '3.5. Extorno la Orden de Pago.
                                            idMoviminentoExtorno = ExtornarDesdeBancoComun(oBEMovimiento)
                                            '3.6. Libero las intenciones de extorno registrados.
                                            oDAOrdenPago.ActualizarIntencionExtorno(oEBOrdenPago.IdOrdenPago)
                                            If idMoviminentoExtorno > 0 Then
                                                Return 0
                                            End If

                                        End If
                                    Next
                                End If
                            End Using
                        End If

                        '3.2. En caso no exista un extorna interno en cola, el proceso de pago es correco y registrar en la tabla servicio notificación y enviar mails a los usuario.
                        Dim obeServicio As BEServicio
                        Using odaServicio As New DAServicio()
                            obeServicio = CType(odaServicio.GetRecordByID(oEBOrdenPago.IdServicio), BEServicio)
                        End Using

                        If obeServicio.FlgNotificaMovimiento = True Then
                            Using oBLServicionNotificacion As New BLServicioNotificacion()
                                If IdOrigenRegistro.HasValue = False Then
                                    IdOrigenRegistro = ServicioNotificacion.IdOrigenRegistro.ServicioWeb
                                End If
                                If oBLServicionNotificacion.RegistrarServicioNotificacionUrlOk(oEBOrdenPago, oBEOperacionBancaria.IdMovimiento, _
                                oBEMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then
                                    'La notificación al servicio no se envió correctamente
                                    Return -7
                                End If
                            End Using
                        End If
                    End If

                    '3. FixExtorno 28-03-14: En caso hubo extorno en cola, no deberia notificar al usuario.
                    'Enviar notificación al usuario.
                    Using oBLEmail As New BLEmail()
                        If oBLEmail.EnviarCorreoCancelacionOrdenPago(oEBOrdenPago, oBEOperacionBancaria.Email) <> 1 Then
                            'La notificación al usuario no se envió correctamente
                            Return -6
                        End If
                    End Using

                    'Notificar el registro de Agencia Bancaria o de Terminal según el Banco
                    If NotificarRegistroAgenciaOTerminal Then
                        Select Case oBEMovimiento.CodigoBanco
                            Case Conciliacion.CodigoBancos.FullCarga
                                Using oBLEmail As New BLEmail()
                                    If oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(NotificarRegistroPuntoVenta, _
                                        oBEMovimiento.CodigoPuntoVenta, oBEMovimiento.NumeroSerieTerminal, _
                                        ProcesoCancelacionFC) <> 1 Then
                                        'La notificación al administrador no se envió correctamente
                                        Return -9
                                    End If
                                End Using
                            Case Conciliacion.CodigoBancos.MoMo, Conciliacion.CodigoBancos.Kasnet
                                Using oBLEmail As New BLEmail()
                                    If oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(NotificarRegistroPuntoVenta, _
                                        oBEMovimiento.CodigoPuntoVenta, oBEMovimiento.NumeroSerieTerminal, _
                                        ProcesoCancelacionMoMo) <> 1 Then
                                        'La notificación al administrador no se envió correctamente
                                        Return -9
                                    End If
                                End Using
                            Case Conciliacion.CodigoBancos.BBVA, Conciliacion.CodigoBancos.Scotiabank, Conciliacion.CodigoBancos.Interbank, Conciliacion.CodigoBancos.WesterUnion
                                Using oBLEmail As New BLEmail()
                                    If oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(oBEMovimiento.CodigoAgenciaBancaria, _
                                        ProcesoCancelacionOP, oBEMovimiento.CodigoBanco) <> 1 Then
                                        'La notificación al administrador no se envió correctamente
                                        Return -5
                                    End If
                                End Using
                            Case Else
                                'Código de banco incorrecto o sin implementar
                                Return 0
                        End Select
                    End If
                    'ENDM'

                    Return oBEOperacionBancaria.IdMovimiento
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function PagarDesdeBancoComun_Conciliacion_Diaria(ByVal oBEMovimiento As BEMovimiento, ByVal IdOrigenRegistro As Nullable(Of Integer), _
      ByVal FechaCancelacion As Nullable(Of DateTime)) As Integer
        Try

            Dim oBEOrdenPagoReq As New BEOrdenPago
            With oBEOrdenPagoReq
                .NumeroOrdenPago = oBEMovimiento.NumeroOrdenPago
                .CodigoBanco = oBEMovimiento.CodigoBanco
                .CodigoServicio = oBEMovimiento.CodigoServicio
            End With
            _nlogger.Info(String.Format("PagarDesdeBancoComun - NumeroOrdenPago:{0}, CodigoBanco:{1}, CodigoServicio:{2}", oBEOrdenPagoReq.NumeroOrdenPago, oBEOrdenPagoReq.CodigoBanco, oBEOrdenPagoReq.CodigoServicio))
            Using oDAAgenciaBancaria As New DAAgenciaBancaria()
                Dim oBEOrdenPagoRes As BEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPagoReq)
                '.Agregar validacion para indicar q el cip esta en proceso de pago
                If oBEOrdenPagoRes Is Nothing Then
                    'CIP no existe
                    Return -8
                ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Eliminado Then
                    'CIP está eliminado
                    Return -27
                ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Cancelada Then
                    'CIP está cancelado
                    Return -4
                ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Expirado Or _
                    oBEOrdenPagoRes.CipListoParaExpirar Then
                    'CIP ha expirado
                    Return -1
                ElseIf oBEOrdenPagoRes.codMonedaBanco <> oBEMovimiento.CodMonedaBanco Then
                    'Código moneda de entrada es correcto
                    Return -2
                ElseIf oBEOrdenPagoRes.Total <> oBEMovimiento.Monto Then
                    'Monto de entrada es corecto
                    Return -3
                Else
                    'M
                    'Dim objParametros As New ParametrosAgenciaBancariaComun()
                    'objParametros.oBEMovimiento = oBEMovimiento
                    'objParametros.oDAAgenciaBancaria = oDAAgenciaBancaria
                    'Rommel Background
                    'Thread.Sleep(10000)

                    Dim NotificarRegistroAgenciaOTerminal As Boolean = False
                    Dim NotificarRegistroPuntoVenta As Boolean = False
                    Dim intFlag As Integer
                    'Registrar datos de agencia o punto de venta según banco
                    Select Case oBEMovimiento.CodigoBanco
                        'M MoMo
                        Case Conciliacion.CodigoBancos.MoMo
                            'Para MoMo aun esta pendiente
                            Using oDAPuntoVenta As New DAPuntoVenta
                                Dim AperturaOrigen As Hashtable = oDAPuntoVenta.RegistrarPuntoVentaYTerminalDesdePago(oBEMovimiento.CodigoPuntoVenta, _
                                    oBEMovimiento.NumeroSerieTerminal)
                                If AperturaOrigen Is Nothing Then
                                    'La apertura origen no se registró correctamente
                                    Return 0
                                Else
                                    With oBEMovimiento
                                        .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.MoMo
                                    End With
                                    NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroTerminal"))
                                    NotificarRegistroPuntoVenta = Convert.ToBoolean(AperturaOrigen("RegistroPuntoVenta"))
                                    intFlag = 1
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.FullCarga, Conciliacion.CodigoBancos.Kasnet
                            'Para FullCarga: Registrar punto de venta y terminal
                            Using oDAPuntoVenta As New DAPuntoVenta
                                Dim AperturaOrigen As Hashtable = oDAPuntoVenta.RegistrarPuntoVentaYTerminalDesdePago(oBEMovimiento.CodigoPuntoVenta, _
                                    oBEMovimiento.NumeroSerieTerminal)
                                If AperturaOrigen Is Nothing Then
                                    'La apertura origen no se registró correctamente
                                    _nlogger.Error(String.Format("RegistrarPuntoVentaYTerminalDesdePago - NumeroOrdenPago:{0}, Mensaje:{1}", oBEOrdenPagoReq.NumeroOrdenPago, "La apertura origen no se registró correctamente"))
                                    Return 0
                                Else
                                    With oBEMovimiento
                                        .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.FullCarga
                                    End With
                                    NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroTerminal"))
                                    NotificarRegistroPuntoVenta = Convert.ToBoolean(AperturaOrigen("RegistroPuntoVenta"))
                                    intFlag = 1
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.BBVA, Conciliacion.CodigoBancos.Scotiabank, Conciliacion.CodigoBancos.Interbank, Conciliacion.CodigoBancos.WesterUnion, Conciliacion.CodigoBancos.BanBif
                            'Para BBVA: Registrar agencia bancaria
                            Dim AperturaOrigen As Hashtable = oDAAgenciaBancaria.RegistrarAgenciaBancariaDesdePago(oBEMovimiento.CodigoBanco, _
                                oBEMovimiento.CodigoAgenciaBancaria)
                            If AperturaOrigen Is Nothing Then
                                'La apertura origen no se registró correctamente
                                _nlogger.Error(String.Format("RegistrarAgenciaBancariaDesdePago - NumeroOrdenPago:{0}, Mensaje:{1}", oBEOrdenPagoReq.NumeroOrdenPago, "La apertura origen no se registró correctamente"))
                                Return 0
                            Else
                                With oBEMovimiento
                                    .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                    .IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                                End With
                                NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroAgenciaBancaria"))
                                intFlag = 1
                            End If
                        Case Conciliacion.CodigoBancos.Life
                            'Agregamos Tipo Origen cancelacion como Agencia Recaudadora.
                            oBEMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaRecaudadora
                            oBEMovimiento.CodigoMedioPago = MedioPago.Tarjeta
                            Using oDAPuntoVenta As New DAPuntoVenta
                                oBEMovimiento.IdAperturaOrigen = oDAPuntoVenta.ObtenerAperturaOrigenLife(oBEMovimiento.IdTipoOrigenCancelacion)
                            End Using
                        Case Else
                            'Código de banco incorrecto o sin implementar
                            Return 0
                    End Select

                    '1. Comprabamos si se usará la implementación del Fix EXtorno Automatico.
                    Dim UsarFixExtorno As Integer = 0
                    Try
                        Using oDAComun As New DAComun()
                            If oDAComun.HabilitarNuevaVersionExtornoInterno(oBEMovimiento.CodigoBanco, oBEOrdenPagoRes.NumeroOrdenPago) = 1 Then
                                UsarFixExtorno = 1
                            End If
                        End Using
                    Catch ex As Exception
                        _nlogger.Error(ex, ex.Message)
                        UsarFixExtorno = 0
                    End Try


                    'Registrar el pago
                    Dim oBEOperacionBancaria As New BEOperacionBancaria

                    '2. FixExtorno 28-03-14: Actualizar el Cip a estado 26(En Proceso de Pago)
                    If UsarFixExtorno = 1 Then
                        Using oDAOrdenPago As New DAOrdenPago()
                            oDAOrdenPago.RestaurarEstadoPago(oBEMovimiento.NumeroOrdenPago, EstadoOrdenPago.EnProcesoPago)
                        End Using
                        If ConfigurationManager.AppSettings("HabilitarSleep") Then
                            Thread.Sleep(ConfigurationManager.AppSettings("TiempoPago"))
                        End If
                    End If

                    Using transaccionscope As New TransactionScope()
                        oBEOperacionBancaria = oDAAgenciaBancaria.PagarCIPDesdeBancoComun_Conciliacion_Diaria(oBEMovimiento, FechaCancelacion)
                        transaccionscope.Complete()
                    End Using

                    'Validar registro de pago
                    If oBEOperacionBancaria Is Nothing Then
                        '2. FixExtorno 28-03-14: En caso el proceso de pago falle, se cambia el estado a 22(Generado)
                        If UsarFixExtorno = 1 Then
                            Using oDAOrdenPago As New DAOrdenPago()
                                oDAOrdenPago.RestaurarEstadoPago(oBEMovimiento.NumeroOrdenPago, EstadoOrdenPago.Generada)
                            End Using
                        End If
                        Return 0
                    ElseIf oBEOperacionBancaria.NumeroOrdenPago.Trim = "" Or oBEOperacionBancaria.IdMovimiento <= 0 Then
                        '2. FixExtorno 28-03-14: En caso el proceso de pago falle, se cambia el estado a 22(Generado)
                        If UsarFixExtorno = 1 Then
                            Using oDAOrdenPago As New DAOrdenPago()
                                oDAOrdenPago.RestaurarEstadoPago(oBEMovimiento.NumeroOrdenPago, EstadoOrdenPago.Generada)
                            End Using
                        End If
                        Return 0
                    End If

                    'Consultar los datos de la orden de pago
                    Dim oEBOrdenPago As BEOrdenPago = Nothing
                    Using oDAOrdenPago As New DAOrdenPago()
                        oEBOrdenPago = oDAOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(oBEMovimiento.NumeroOrdenPago)
                    End Using

                    'Registrar a la tabla de notificación para notificar el pago
                    If oEBOrdenPago Is Nothing Then
                        Return 0
                    ElseIf oEBOrdenPago.IdEstado = EstadoOrdenPago.Cancelada Then

                        '3. FixExtorno 28-03-14: Validar si hay Extorno interno en cola, 
                        '3.1. En caso exista Extornar el Cip Pagado, no registrar en la tabla ServicioNotificacion y no enviar mails a los usuarios.
                        '3.2. Genero el extorno del cip con tipo de movimiento 31(Extorno Interno), actualizo los intentos de extorno a procesados y retorno idMovimiento 0
                        If UsarFixExtorno = 1 Then
                            Using oDAOrdenPago As New DAOrdenPago()
                                Dim lstIntentosExtorno As New List(Of BEIntentoExtorno)

                                '3.3. Obtengo los intentos de extorno registrado.
                                lstIntentosExtorno = oDAOrdenPago.ObtenerCantidadIntentoExtorno(oEBOrdenPago.IdOrdenPago)
                                If lstIntentosExtorno.Count > 0 Then
                                    Dim i As Integer = 0
                                    For i = 0 To lstIntentosExtorno.Count - 1

                                        '3.4. Valido que la información de los intentos de extorno sea correcta para realizar la operación.
                                        If lstIntentosExtorno(i).IdOrdenPago = oBEMovimiento.NumeroOrdenPago And _
                                           lstIntentosExtorno(i).NumeroOperacion = oBEMovimiento.NumeroOperacion And _
                                           lstIntentosExtorno(i).CodBanco = oBEMovimiento.CodigoBanco And _
                                           lstIntentosExtorno(i).CodServicio = oBEMovimiento.CodigoServicio And _
                                           lstIntentosExtorno(i).CodAgencia = Right(oBEMovimiento.CodigoAgenciaBancaria.Trim, 6).ToString().PadLeft(6, "0") Then
                                            '3.5 Defino que él id del tiop de movimiento para el extorno será 31
                                            oBEMovimiento.IdTipoMovimiento = TipoMovimiento.ExtornoAutomatico
                                            Dim idMoviminentoExtorno As Integer
                                            '3.5. Extorno la Orden de Pago.
                                            idMoviminentoExtorno = ExtornarDesdeBancoComun(oBEMovimiento)
                                            '3.6. Libero las intenciones de extorno registrados.
                                            oDAOrdenPago.ActualizarIntencionExtorno(oEBOrdenPago.IdOrdenPago)
                                            If idMoviminentoExtorno > 0 Then
                                                Return 0
                                            End If

                                        End If
                                    Next
                                End If
                            End Using
                        End If

                        '3.2. En caso no exista un extorna interno en cola, el proceso de pago es correco y registrar en la tabla servicio notificación y enviar mails a los usuario.
                        Dim obeServicio As BEServicio
                        Using odaServicio As New DAServicio()
                            obeServicio = CType(odaServicio.GetRecordByID(oEBOrdenPago.IdServicio), BEServicio)
                        End Using

                        If obeServicio.FlgNotificaMovimiento = True Then
                            Using oBLServicionNotificacion As New BLServicioNotificacion()
                                If IdOrigenRegistro.HasValue = False Then
                                    IdOrigenRegistro = ServicioNotificacion.IdOrigenRegistro.ServicioWeb
                                End If
                                If oBLServicionNotificacion.RegistrarServicioNotificacionUrlOk(oEBOrdenPago, oBEOperacionBancaria.IdMovimiento, _
                                oBEMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then
                                    'La notificación al servicio no se envió correctamente
                                    Return -7
                                End If
                            End Using
                        End If
                    End If

                    '3. FixExtorno 28-03-14: En caso hubo extorno en cola, no deberia notificar al usuario.
                    'Enviar notificación al usuario.
                    Using oBLEmail As New BLEmail()
                        If oBLEmail.EnviarCorreoCancelacionOrdenPago(oEBOrdenPago, oBEOperacionBancaria.Email) <> 1 Then
                            'La notificación al usuario no se envió correctamente
                            Return -6
                        End If
                    End Using

                    'Notificar el registro de Agencia Bancaria o de Terminal según el Banco
                    If NotificarRegistroAgenciaOTerminal Then
                        Select Case oBEMovimiento.CodigoBanco
                            Case Conciliacion.CodigoBancos.FullCarga
                                Using oBLEmail As New BLEmail()
                                    If oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(NotificarRegistroPuntoVenta, _
                                        oBEMovimiento.CodigoPuntoVenta, oBEMovimiento.NumeroSerieTerminal, _
                                        ProcesoCancelacionFC) <> 1 Then
                                        'La notificación al administrador no se envió correctamente
                                        Return -9
                                    End If
                                End Using
                            Case Conciliacion.CodigoBancos.MoMo, Conciliacion.CodigoBancos.Kasnet
                                Using oBLEmail As New BLEmail()
                                    If oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(NotificarRegistroPuntoVenta, _
                                        oBEMovimiento.CodigoPuntoVenta, oBEMovimiento.NumeroSerieTerminal, _
                                        ProcesoCancelacionMoMo) <> 1 Then
                                        'La notificación al administrador no se envió correctamente
                                        Return -9
                                    End If
                                End Using
                            Case Conciliacion.CodigoBancos.BBVA, Conciliacion.CodigoBancos.Scotiabank, Conciliacion.CodigoBancos.Interbank, Conciliacion.CodigoBancos.WesterUnion
                                Using oBLEmail As New BLEmail()
                                    If oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(oBEMovimiento.CodigoAgenciaBancaria, _
                                        ProcesoCancelacionOP, oBEMovimiento.CodigoBanco) <> 1 Then
                                        'La notificación al administrador no se envió correctamente
                                        Return -5
                                    End If
                                End Using
                            Case Else
                                'Código de banco incorrecto o sin implementar
                                Return 0
                        End Select
                    End If
                    'ENDM'

                    Return oBEOperacionBancaria.IdMovimiento
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Método común para la anulación de un CIP
    ''' </summary>
    ''' <param name="oBEMovimiento">
    ''' Propiedades con valor obligatorio:
    ''' - NumeroOrdenPago
    ''' - NumeroOperacion
    ''' - NumeroOperacionPago
    ''' - CodigoBanco
    ''' - CodMonedaBanco
    ''' - Monto
    ''' -- CodigoMedioPago
    ''' Propiedades con valor condicional:
    ''' - FullCarga: NumeroSerieTerminal y PuntoVenta
    ''' - BBVA: CodigoAgenciaBancaria
    ''' </param>
    ''' <returns>
    '''  > 0: Id del movimiento. Registro de correcto anulación.
    ''' Sino: Error al registrar la anulación.
    ''' </returns>
    ''' <remarks></remarks>
    Public Function AnularDesdeBancoComun(ByVal oBEMovimiento As BEMovimiento) As Integer
        Dim result As Integer = 0
        Try
            Dim IdAnulacion As Integer = 0
            Dim oBEMovimientoRes As BEMovimiento = Nothing
            Dim oBEOrdenPago As New BEOrdenPago
            Select Case oBEMovimiento.CodigoBanco
                Case Conciliacion.CodigoBancos.MoMo
                    'Para MoMo aun esta pendiente
                    oBEMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.MoMo
                    With oBEOrdenPago
                        .NumeroOrdenPago = oBEMovimiento.NumeroOrdenPago
                        .CodigoBanco = oBEMovimiento.CodigoBanco
                        .Total = oBEMovimiento.Monto
                        .codMonedaBanco = oBEMovimiento.CodMonedaBanco
                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.MoMo
                        .CodigoServicio = oBEMovimiento.CodigoServicio
                    End With
                    Using oDAOrdenPago As New DAOrdenPago
                        IdAnulacion = oDAOrdenPago.ValidarOrdenPagoParaAnulacionDesdeMoMo(oBEOrdenPago, _
                         oBEMovimiento.CodigoPuntoVenta, oBEMovimiento.NumeroSerieTerminal, oBEMovimiento.NumeroOperacionPago)
                    End Using


                Case Conciliacion.CodigoBancos.FullCarga
                    oBEMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.FullCarga
                    With oBEOrdenPago
                        .NumeroOrdenPago = oBEMovimiento.NumeroOrdenPago
                        .CodigoBanco = oBEMovimiento.CodigoBanco
                        .Total = oBEMovimiento.Monto
                        .codMonedaBanco = oBEMovimiento.CodMonedaBanco
                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.FullCarga
                        .CodigoServicio = oBEMovimiento.CodigoServicio
                    End With
                    Using oDAOrdenPago As New DAOrdenPago
                        IdAnulacion = oDAOrdenPago.ValidarOrdenPagoParaAnulacionDesdeFullCarga(oBEOrdenPago, _
                         oBEMovimiento.CodigoPuntoVenta, oBEMovimiento.NumeroSerieTerminal, oBEMovimiento.NumeroOperacionPago)
                    End Using
                Case Conciliacion.CodigoBancos.BBVA, Conciliacion.CodigoBancos.Scotiabank, Conciliacion.CodigoBancos.Interbank, Conciliacion.CodigoBancos.WesterUnion
                    oBEMovimiento.IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                    With oBEOrdenPago
                        .NumeroOrdenPago = oBEMovimiento.NumeroOrdenPago
                        .CodigoBanco = oBEMovimiento.CodigoBanco
                        .Total = oBEMovimiento.Monto
                        .codMonedaBanco = oBEMovimiento.CodMonedaBanco
                        .CodigoOrigenCancelacion = oBEMovimiento.CodigoAgenciaBancaria
                        .IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                        .CodigoServicio = oBEMovimiento.CodigoServicio
                    End With
                    Using oDAOrdenPago As New DAOrdenPago
                        IdAnulacion = oDAOrdenPago.ValidarOrdenPagoParaAnulacionDesdeBanco(oBEOrdenPago, oBEMovimiento.NumeroOperacionPago)
                    End Using
                Case Else
                    'Código de banco incorrecto o sin implementar
                    Return 0
            End Select

            If IdAnulacion <= 0 Then
                Return IdAnulacion
            End If

            Using oDAAgenciaBancaria As New DAAgenciaBancaria
                oBEMovimientoRes = oDAAgenciaBancaria.AnularOperacionPago(oBEMovimiento)
            End Using

            If oBEMovimientoRes Is Nothing Then Return 0
            If oBEMovimientoRes.IdMovimiento <= 0 Then Return 0

            Dim oBEOrdenPagoN As BEOrdenPago = Nothing
            Using oDAOrdenPago As New DAOrdenPago()
                oBEOrdenPagoN = oDAOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(oBEMovimiento.NumeroOrdenPago)
            End Using

            Dim obeServicio As BEServicio
            Using odaServicio As New DAServicio()
                obeServicio = CType(odaServicio.GetRecordByID(oBEOrdenPagoN.IdServicio), BEServicio)
            End Using

            If obeServicio.FlgNotificaMovimiento = True Then
                Using oblServicionNotificacion As New BLServicioNotificacion()
                    If oblServicionNotificacion.RegistrarServicioNotificacionUrlError(oBEOrdenPagoN, oBEMovimientoRes.IdMovimiento, _
                        oBEMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then
                        Return -15
                    End If

                End Using
            End If

            result = oBEMovimientoRes.IdMovimiento
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return result

    End Function

    Public Function AnularBBVA(ByVal request As BEBBVAAnularRequest) As BEBBVAResponse(Of BEBBVAAnularResponse)
        Dim response As New BEBBVAResponse(Of BEBBVAAnularResponse)
        Dim obeMovimiento As New BEMovimiento
        Dim IdMovimiento As Integer = 0

        response.ObjBBVA = New BEBBVAAnularResponse

        Try

            '--------------------------------------------------------
            ' 0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBBVA
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim  ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim    'REFER. DEUDA ADICIONAL
                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
            End With

            response.Estado = 0
            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request
                If .codigoOperacion.Trim = "" OrElse _
                    .numeroOperacion.Trim = "" OrElse _
                    .codigoBanco.Trim = "" OrElse .codigoConvenio.Trim = "" OrElse .cuentaRecaudadora.Trim = "" OrElse _
                    .numeroReferenciaDeuda.Trim = "" OrElse .cantidadDocumentos.Trim = "" OrElse .formaPago.Trim = "" OrElse _
                    .codigoMoneda.Trim = "" OrElse .importeTotalPagado.Trim = "" OrElse _
                    .numeroOperacionOriginal = "" OrElse .fechaOperacionOriginal.Trim = "" OrElse _
                    .canalOperacion.Trim = "" OrElse .codigoOficina.Trim = "" OrElse .fechaOperacion.Trim = "" OrElse _
                    .horaOperacion.Trim = "" Then

                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TramaEnviadaEmpresaInvalida
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

                    response.Mensaje = "Algunos de los campos mandatorios no presentan valor."

                    Return response
                End If

            End With


            '--------------------------------------------------------
            ' 2. INPUT - MOVIMIENTO
            '--------------------------------------------------------
            With obeMovimiento
                .NumeroOrdenPago = request.numeroReferenciaDeuda.Trim 'Nro. Orden Pago
                .NumeroOperacion = request.numeroOperacion.Trim 'Nro. Operacion del Banco
                .NumeroOperacionPago = request.numeroOperacionOriginal.Trim 'Nro. Operacion del Banco (del Pago)
                .CodigoAgenciaBancaria = request.codigoOficina.Trim ' Cod. Agencia Bancaria
                .CodigoServicio = request.codigoConvenio
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.importeTotalPagado) / 100 'Importe
                .CodMonedaBanco = request.codigoMoneda.Trim 'Codigo Moneda
                .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA 'Cod. Banco
            End With

            Dim transOptions As New TransactionOptions()
            transOptions.IsolationLevel = IsolationLevel.ReadCommitted
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using oTransactionScope As New TransactionScope(TransactionScopeOption.Required, timeout)

            Using oTransactionScope As New TransactionScope

                IdMovimiento = AnularDesdeBancoComun(obeMovimiento)
                oTransactionScope.Complete()
                If IdMovimiento > 0 Then
                    'ANULACION CORRECTA
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TransaccionRealizadaExito
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.ObjBBVA.numeroOperacionEmpresa = IdMovimiento.ToString

                    response.Estado = 1
                    response.Mensaje = "Transacion exitosa"
                ElseIf IdMovimiento = 0 Then
                    'ANULACION SIN EXITO ó TIEMPO MAXIMO DE EXTORNO
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoSePudoRealizarRegistroExtorno
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.Mensaje = ListaCodValidacion(IdMovimiento)

                ElseIf IdMovimiento = -37 Then
                    'ANULACION FUERA DEL TIEMPO MÁXIMO CONFIGURADO
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoSePudoRealizarRegistroExtorno
                    response.ObjBBVA.mensajeResultado = "No se pudo realizar el registro de extorno"
                    response.Mensaje = ListaCodValidacion(IdMovimiento)
                Else
                    'VALICACION DE LA ANULACION SIN EXITO
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.Mensaje = ListaCodValidacion(IdMovimiento)
                End If

            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Estado = -1
            response.Mensaje = ex.Message
            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)
        End Try

        Return response
    End Function

    Public Function ExtornarBBVA(ByVal request As BEBBVAExtornarRequest) As BEBBVAResponse(Of BEBBVAExtornarResponse)
        Dim response As New BEBBVAResponse(Of BEBBVAExtornarResponse)
        Dim obeMovimiento As New BEMovimiento
        Dim IdMovimiento As Integer = 0

        Try
            response.ObjBBVA = New BEBBVAExtornarResponse
            '--------------------------------------------------------
            ' 0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBBVA
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim  ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim    'REFER. DEUDA ADICIONAL
                .numeroOperacionEmpresa = "0"   ' NRO. DE OPERAC. DE LA EMPRESA
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
            End With

            response.Estado = 0
            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request
                If .codigoOperacion.Trim = "" OrElse _
                    .numeroOperacion.Trim = "" OrElse _
                    .codigoBanco.Trim = "" OrElse .codigoConvenio.Trim = "" OrElse .cuentaRecaudadora.Trim = "" OrElse _
                    .numeroReferenciaDeuda.Trim = "" OrElse .cantidadDocumentos.Trim = "" OrElse .formaPago.Trim = "" OrElse _
                    .codigoMoneda.Trim = "" OrElse .importeTotalPagado.Trim = "" OrElse _
                    .numeroOperacionOriginal = "" OrElse .fechaOperacionOriginal.Trim = "" OrElse _
                    .canalOperacion.Trim = "" OrElse .codigoOficina.Trim = "" OrElse .fechaOperacion.Trim = "" OrElse _
                    .horaOperacion.Trim = "" Then

                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TramaEnviadaEmpresaInvalida
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)

                    response.Mensaje = "Algunos de los campos mandatorios no presentan valor."
                    Return response
                End If
            End With

            '--------------------------------------------------------
            ' 2. INPUT - MOVIMIENTO
            '--------------------------------------------------------
            With obeMovimiento
                .NumeroOrdenPago = request.numeroReferenciaDeuda.Trim 'Nro. Orden Pago
                .NumeroOperacion = request.numeroOperacion.Trim 'Nro. Operacion del Banco
                .NumeroOperacionPago = request.numeroOperacionOriginal.Trim 'Nro. Operacion a Anular
                .CodigoAgenciaBancaria = request.codigoOficina.Trim ' Cod. Agencia Bancaria

                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.importeTotalPagado) / 100 'Importe
                .CodMonedaBanco = request.codigoMoneda.Trim 'Codigo Moneda
                .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA 'Cod. Banco
                .CodigoServicio = request.codigoConvenio
            End With

            If request.numeroReferenciaDeuda.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                '--------------------------------------------------------
                ' 3. PROCESO DE ANULACION DEL BANCO "BBVA"
                '--------------------------------------------------------
                'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
                'Using oTransactionScope As New TransactionScope(TransactionScopeOption.Required, timeout)
                'Using oTransactionScope As New TransactionScope

                IdMovimiento = ExtornarDesdeBancoComun(obeMovimiento)

                If IdMovimiento > 0 Then
                    'EXTORNO CORRECTO
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.TransaccionRealizadaExito
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.ObjBBVA.numeroOperacionEmpresa = IdMovimiento.ToString

                    response.Estado = 1
                    response.Mensaje = "Transacion exitosa"

                    'oTransactionScope.Complete()

                ElseIf IdMovimiento = 0 Then
                    'EXTORNO SIN EXITO
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoSePudoRealizarRegistroExtorno
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.Mensaje = ListaCodValidacion(IdMovimiento) + " en el proceso de extorno"
                    'ElseIf IdMovimiento = -37 Then
                    '    'TIEMPO MAXIMO DE EXTORNO
                    '    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoSePudoRealizarRegistroExtorno
                    '    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    '    response.Mensaje = ListaCodValidacion(IdMovimiento)
                Else
                    'VALIDACION DE EXTORNO SIN EXITO
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.Mensaje = ListaCodValidacion(IdMovimiento)
                End If

                'End Using
            Else     'Anulando una recarga de dinero

                With response.ObjBBVA
                    .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                    .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                    .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                    .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                    .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                    .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                    .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim  ' NRO. REFERENCIA DEUDA
                    .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim    'REFER. DEUDA ADICIONAL
                    .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
                    .numeroOperacionEmpresa = "0"   ' NRO. DE OPERAC. DE LA EMPRESA
                End With
                Dim dv As New DADineroVirtual()
                Dim cuenta As New BERecarga()
                cuenta.xCIPRecarga = request.numeroReferenciaDeuda
                ' Validar Cuenta
                cuenta = dv.ConsultarSolicitudRecargaComun(cuenta.xCIPRecarga)

                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    'Throw New Exception("Cuenta_no_encontrada")
                    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.Mensaje = "Cuenta no encontrada."
                Else

                    'valida que tenga activo el monedero y la cuenta
                    If cuenta.iEstado = 2 Then
                        Dim resultextorno As Integer = dv.AnularRecargaUnibancaComun(cuenta)
                        If resultextorno > 0 Then
                            'response.Mensaje = "Extorno realizado"
                            'response.Respuesta = BCP.Mensaje.Extorno_Realizado
                            ''Enviar Correo                            
                            response.ObjBBVA.codigoResultado = BBVA.Mensaje.TransaccionRealizadaExito
                            response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                            response.ObjBBVA.numeroOperacionEmpresa = resultextorno

                            response.Estado = 1
                            response.Mensaje = "Transaccion exitosa"

                        Else
                            'Throw New Exception("Sucedio un error interno en la BD")
                            response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                            response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                            response.Mensaje = "Sucedio un error interno en BD."
                        End If
                    Else
                        'Throw New Exception("La cuenta no esta activa")
                        response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                        response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                        response.Mensaje = "La cuenta no esta activa."
                    End If
                End If
            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Estado = -1
            response.Mensaje = ex.Message
            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

        End Try

        Return response
    End Function

    Protected Function ExtornarDesdeBancoComun(ByVal obeMovimiento As BEMovimiento) As Integer

        Dim obeOrdenPago As New BEOrdenPago
        Dim result As Integer = 0
        Dim IdAnulacion As Integer = 0
        Dim obeMovimientoRes As BEMovimiento

        Try

            With obeMovimiento
                obeOrdenPago.NumeroOrdenPago = .NumeroOrdenPago 'CIP
                obeOrdenPago.CodigoOrigenCancelacion = .CodigoAgenciaBancaria ' CODIGO AGENCIA
                obeOrdenPago.Total = .Monto
                obeOrdenPago.codMonedaBanco = .CodMonedaBanco
                obeOrdenPago.CodigoBanco = .CodigoBanco
                obeOrdenPago.CodigoServicio = .CodigoServicio

                '--------------------------------------------------------
                ' 1. VALIDAR NroOrdenPago, CodAgencia, NroOpeBanco
                '--------------------------------------------------------
                If obeMovimiento.CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Kasnet Then 'Validacion extorno Kasnet 3r
                    Using odaOrdenPago As New DAOrdenPago()
                        IdAnulacion = odaOrdenPago.ValidarOrdenPagoParaExtornarDesdeKasnet(obeOrdenPago, .CodigoPuntoVenta, .NumeroSerieTerminal, .NumeroOperacionPago)
                    End Using
                Else
                    Using odaOrdenPago As New DAOrdenPago()
                        IdAnulacion = odaOrdenPago.ValidarOrdenPagoParaExtornoDesdeBanco(obeOrdenPago, .NumeroOperacionPago)
                    End Using
                End If


            End With

            ' VALIDACION SIN EXITO
            If IdAnulacion < 0 Then Return IdAnulacion

            '--------------------------------------------------------
            ' 2. REALIZAR ANULACION
            '--------------------------------------------------------
            Using odaAgenciaBancaria As New DAAgenciaBancaria

                'REALIZAMOS LA ANULACION DEL MOVIMIENTO
                Using oTransactionScope As New TransactionScope
                    obeMovimientoRes = odaAgenciaBancaria.ExtornarOperacionPago(obeMovimiento)
                    oTransactionScope.Complete()
                End Using

            End Using

            'VERIFICAR MOVIMIENTO ANULADO
            If obeMovimientoRes Is Nothing Then Return 0

            'VERIFICAR ID MOVIMIENTO
            If obeMovimientoRes.IdMovimiento <= 0 Then Return 0

            Dim obeOrdenPagon As BEOrdenPago = Nothing
            Using odaOrdenPago As New DAOrdenPago()
                obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeMovimiento.NumeroOrdenPago)
            End Using

            'SE REGISTRA LA NOTIFICACIÓN. URLERROR
            Dim obeServicio As BEServicio
            Using odaServicio As New DAServicio()
                obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
            End Using

            If obeServicio.FlgNotificaMovimiento = True Then
                Using oblServicionNotificacion As New BLServicioNotificacion()

                    If oblServicionNotificacion.RegistrarServicioNotificacionUrlError(obeOrdenPagon, obeMovimientoRes.IdMovimiento, _
                                        obeMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then Return (-25)
                End Using
            End If

            result = obeMovimientoRes.IdMovimiento

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return result

    End Function



    ''' <summary>
    ''' Método común para extonar anulación desde banco
    ''' </summary>
    ''' <param name="obeMovimiento"></param>
    ''' <returns>Id del movimiento</returns>
    ''' <remarks></remarks>
    Public Function ExtornarAnulacionDesdeBancoComun(ByVal obeMovimiento As BEMovimiento) As Integer
        Dim result As Integer = 0
        Try
            Dim obeOrdenPago As New BEOrdenPago
            Dim IdAnulacion As Integer = 0
            With obeMovimiento
                obeOrdenPago.NumeroOrdenPago = .NumeroOrdenPago 'CIP
                obeOrdenPago.CodigoOrigenCancelacion = .CodigoAgenciaBancaria ' CODIGO AGENCIA
                obeOrdenPago.Total = .Monto
                obeOrdenPago.codMonedaBanco = .CodMonedaBanco
                obeOrdenPago.CodigoBanco = .CodigoBanco
                obeOrdenPago.CodigoServicio = .CodigoServicio
                If .NumeroOperacion = .NumeroOperacionPago Then
                    Using odaOrdenPago As New DAOrdenPago()
                        IdAnulacion = odaOrdenPago.ValidarOrdenPagoParaExtornoAnulacionDesdeBanco(obeOrdenPago, .NumeroOperacionPago)
                    End Using
                Else
                    IdAnulacion = -33
                End If
            End With

            If IdAnulacion < 0 Then Return IdAnulacion

            Dim oBEMovimientoRes As BEMovimiento

            Using odaAgenciaBancaria As New DAAgenciaBancaria
                oBEMovimientoRes = odaAgenciaBancaria.ExtornarOperacionAnulacion(obeMovimiento)
            End Using

            'VERIFICAR MOVIMIENTO ANULADO
            If oBEMovimientoRes Is Nothing Then Return 0

            'VERIFICAR ID MOVIMIENTO
            If oBEMovimientoRes.IdMovimiento <= 0 Then Return 0

            Dim obeOrdenPagon As BEOrdenPago = Nothing
            Using odaOrdenPago As New DAOrdenPago()
                obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeMovimiento.NumeroOrdenPago)
            End Using

            'SE REGISTRA LA NOTIFICACIÓN.
            Dim obeServicio As BEServicio
            Using odaServicio As New DAServicio()
                obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
            End Using

            If obeServicio.FlgNotificaMovimiento = True Then
                Using oblServicionNotificacion As New BLServicioNotificacion()
                    If oblServicionNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPagon, oBEMovimientoRes.IdMovimiento, _
                                        obeMovimiento.IdUsuarioCreacion, ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion) <= 0 Then
                        Return (-25)
                    End If
                End Using
            End If

            result = oBEMovimientoRes.IdMovimiento
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return result
    End Function
#End Region

#Region "Integración Scotiabank"

    Private _listaCodTransaccionSBK As Dictionary(Of String, String) = Nothing
    Private ReadOnly Property ListaCodTransaccionSBK() As Dictionary(Of String, String)
        Get
            If (_listaCodTransaccionSBK Is Nothing) Then
                'No corregir la tildación ortográfica en los mensajes
                _listaCodTransaccionSBK = New Dictionary(Of String, String)
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.TransaccionExitosa, "Transaccion exitosa")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.TransaccionFallida, "Transaccion fallida")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPNoExiste, "El CIP no existe")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPEliminado, "El CIP esta eliminado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPCancelado, "El CIP esta cancelado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPExpirado, "El CIP esta expirado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CodigoMonedaNoCoincide, "La moneda ingresada no coincide a la del CIP")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.MontoNoCoincide, "El monto ingresado no coincide al del CIP")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaRegistroAgencia, "El registro de la agencia bancaria ha fallado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaUsuario, "La notificacion al usuario ha fallado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaPortal, "La notificacion al servicio ha fallado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.MontoOMonedaNoCoincide, "Monto o moneda ingresada no coincide con el CIP")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide, "Código de servicio no coincide al CIP")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide, "Agencia bancaria o numero de operacion no coincide")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.MovimientoPagoNoExiste, "Movimiento de pago no registrado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.FechaPagoNoActual, "Fecha de pago es distinta a la actual")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPNoExisteONoCancelado, "El CIP no existe o aun no ha sido cancelado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPNoExisteONoCanceladoOMonedaNoCoincide, "CIP no existe o no cancelado o moneda incorrecta.")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CIPNoExisteONoGeneradoOMonedaNoCoincide, "CIP no existe o no anulado o moneda incorrecta.")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.MovimientoAnulacionNoExiste, "Movimiento de anulacion no registrado")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide, "Agencia bancaria o numero de operacion no coincide")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.FechaAnulacionNoActual, "Fecha de anulacion es distinta a la actual")
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.TramaInvalida, "El requerimiento no tiene una trama valida")

                'TIEMPO MAXIMO DE EXPIRACION
                'Modificado
                _listaCodTransaccionSBK.Add(Scotiabank.CodigosRespuestaExtendidos.NoSePermiteExtornar, "El CIP no puede ser extornado")
            End If
            Return _listaCodTransaccionSBK
        End Get
    End Property

    Public Function ConsultarSBK(ByVal request As BESBKConsultarRequest) As BEBKResponse(Of BESBKConsultarResponse)
        Dim response As New BEBKResponse(Of BESBKConsultarResponse)
        response.ObjBEBK = New BESBKConsultarResponse()
        'Estableciendo valores predeterminados
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Scotiabank.IdentificacionTipoMensaje.Respuesta)
            .PrimaryBitMap.EstablecerValor("F22080010E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000018")
            .CodigoProceso.EstablecerValor(request.CodigoProceso)
            .Monto.EstablecerValor(request.Monto)
            .FechaHoraTransaccion.EstablecerValor(request.FechaHoraTransaccion)
            .Trace.EstablecerValor(request.Trace)
            .FechaCaptura.EstablecerValor(request.FechaCaptura)
            .BinAdquiriente.EstablecerValor(request.BinAdquiriente)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            .AuthorizationIDResponse.RellenarConCeros()
            ''.ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.RespuestaOk)
            .TerminalID.EstablecerValor(request.TerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .DatosReservados.EstablecerValor(request.DatosReservados)
            .CabResLongitudCampo.EstablecerValor("860")
            .CabResCodigoFormato.EstablecerValor("02")
            .CabResBinProcesador.RellenarConCeros()
            .CabResBinAcreedor.RellenarConCeros()
            .CabResCodigoProductoServicio.EstablecerValor("REC")
            .CabResAgencia.EstablecerValor(request.CodigoAgenciaRecaudador)
            .CabResTipoIdentificacion.EstablecerValor("01")
            .CabResNumeroIdentificacion.EstablecerValor(request.DatoConsulta)
            ''.CabResNombreDeudor.EstablecerValor("")
            .CabResNumeroServiciosDevueltos.EstablecerValor("01")
            .CabResNumeroOperacionCobranza.RellenarConCeros()
            .CabResIndicadorSiHayDocumentos.EstablecerValor("0")
            .CabResTamanoMaximoBloque.EstablecerValor("1000")
            .CabResPosicionUltimoDocumento.RellenarConEspaciosEnBlanco()
            .CabResPunteroBaseDatos.RellenarConEspaciosEnBlanco()
            .CabResOrigenRespuesta.RellenarConEspaciosEnBlanco()
            ''.CabResCodigoRespuesta.EstablecerValor("")
            .CabResFiller.RellenarConEspaciosEnBlanco()
            ''.DetResCabSerCodigoProductoServicio.EstablecerValor("")
            ''.DetResCabSerMoneda.EstablecerValor("")
            .DetResCabSerEstadoDeudor.EstablecerValor("V")
            .DetResCabSerMensaje1Deudor.RellenarConEspaciosEnBlanco()
            .DetResCabSerMensaje2Deudor.RellenarConEspaciosEnBlanco()
            .DetResCabSerIndicadorCronologia.EstablecerValor("0")
            .DetResCabSerIndicadorPagosVencidos.EstablecerValor("0")
            .DetResCabSerRestriccionPago.EstablecerValor("0")
            .DetResCabSerDocumentosServicio.EstablecerValor("01")
            .DetResCabSerFiller.RellenarConEspaciosEnBlanco()
            ''.DetResDetSerTipoServicio.EstablecerValor("")
            .DetResDetSerNumeroDocumento.EstablecerValor(request.DatoConsulta)
            ''.DetResDetSerReferenciaDeuda.EstablecerValor("")
            ''.DetResDetSerFechaVencimiento.EstablecerValor("")
            ''.DetResDetSerImporteMinimo.EstablecerValor("")
            ''.DetResDetSerImporteTotal.EstablecerValor("")
        End With
        Try
            If request.DatoConsulta.ToString.Trim = "" OrElse request.CodigoProductoServicio.ToString.Trim = "" Then
                With response.ObjBEBK
                    .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.NoActionToken)
                    .CabResCodigoRespuesta.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.TramaInvalida)
                End With
            Else
                Dim oBEOrdenPago As New BEOrdenPago
                With oBEOrdenPago
                    'MODIFICADO
                    '.NumeroOrdenPago = request.DatoConsulta.ToString.Trim
                    'If .NumeroOrdenPago.Length > 14 Then
                    '    .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                    'End If
                    .NumeroOrdenPago = request.DatoConsulta.ToString.Trim
                    Try
                        .NumeroOrdenPago = Convert.ToInt32(.NumeroOrdenPago)
                    Catch ex As Exception
                        If .NumeroOrdenPago.Length > 14 Then
                            .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                        End If
                    End Try

                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                    .CodigoServicio = request.CodigoProductoServicio.ToString.Trim
                End With
                Using oDAAgenciaBancaria As New DAAgenciaBancaria()
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                End Using
                If oBEOrdenPago Is Nothing Then
                    With response.ObjBEBK
                        .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.NoActionToken)
                        .CabResNombreDeudor.RellenarConEspaciosEnBlanco()
                        .CabResCodigoRespuesta.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.CIPNoExiste)
                        .DetResCabSerCodigoProductoServicio.RellenarConEspaciosEnBlanco()
                        .DetResCabSerMoneda.RellenarConEspaciosEnBlanco()
                        '.DetResDetSerTipoServicio.EstablecerValor("")
                        '.DetResDetSerReferenciaDeuda.EstablecerValor("")
                        .DetResDetSerFechaVencimiento.RellenarConCeros()
                        .DetResDetSerImporteMinimo.RellenarConCeros()
                        .DetResDetSerImporteTotal.RellenarConCeros()
                    End With
                Else
                    With response.ObjBEBK
                        ''.ResponseCode.EstablecerValor("")
                        .CabResNombreDeudor.EstablecerValor(oBEOrdenPago.DescripcionCliente, True)
                        ''.CabResCodigoRespuesta.EstablecerValor("")
                        .DetResCabSerCodigoProductoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        .DetResCabSerMoneda.EstablecerValor(oBEOrdenPago.codMonedaBanco)
                        .DetResDetSerTipoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        .DetResDetSerReferenciaDeuda.EstablecerValor(request.DatoConsulta)
                        .DetResDetSerFechaVencimiento.EstablecerValor(oBEOrdenPago.FechaVencimiento, "yyyyMMdd")
                        .DetResDetSerImporteMinimo.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        .DetResDetSerImporteTotal.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                    End With
                    If oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Eliminado Then
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.NoActionToken)
                            .CabResCodigoRespuesta.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.CIPEliminado)
                        End With
                    ElseIf oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada Then
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.NoActionToken)
                            .CabResCodigoRespuesta.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.CIPCancelado)
                        End With
                    ElseIf oBEOrdenPago.CipListoParaExpirar OrElse oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Expirado Then
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.NoActionToken)
                            .CabResCodigoRespuesta.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.CIPExpirado)
                        End With
                    Else
                        'Éxito
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuesta.RespuestaOk)
                            .CabResCodigoRespuesta.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.TransaccionExitosa)
                        End With
                    End If
                End If
            End If
            ListaCodTransaccionSBK.TryGetValue(response.ObjBEBK.CabResCodigoRespuesta.ToString, response.Descripcion1)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
        End Try
        Return response
    End Function

    Public Function CancelarSBK(ByVal request As BESBKCancelarRequest) As BEBKResponse(Of BESBKCancelarResponse)
        Dim response As New BEBKResponse(Of BESBKCancelarResponse)
        response.ObjBEBK = New BESBKCancelarResponse
        Dim IdMovimiento As Long = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Scotiabank.IdentificacionTipoMensaje.Respuesta)
            .PrimaryBitMap.EstablecerValor("F22080010E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000018")
            .CodigoProceso.EstablecerValor(request.CodigoProceso)
            .Monto.EstablecerValor(request.Monto)
            .FechaHoraTransaccion.EstablecerValor(request.FechaHoraTransaccion)
            .Trace.EstablecerValor(request.Trace)
            .FechaCaptura.EstablecerValor(request.FechaCaptura)
            .IdentificacionEmpresa.EstablecerValor(request.BinAdquiriente)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            ''.AuthorizationIDResponse.EstablecerValor("")
            ''.ResponseCode.EstablecerValor("")
            .TerminalID.EstablecerValor(request.TerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .DatosReservados.EstablecerValor(request.DatosReservados)
            .TamanoBloque.EstablecerValor("557")
            .CodigoFormato.EstablecerValor("01")
            .BinProcesador.RellenarConCeros()
            .CodigoAcreedor.RellenarConCeros()
            .CodigoProductoServicio.EstablecerValor("REC")
            .CodigoPlazaRecaudador.EstablecerValor("0000")
            .CodigoAgenciaRecaudador.EstablecerValor(request.CodigoAgenciaRecaudador)
            .TipoDatoPago.EstablecerValor(request.TipoDatoPago)
            .DatoPago.EstablecerValor(request.DatoPago)
            .CodigoCiudad.EstablecerValor(request.CodigoCiudad)
            ''.NumeroOperacionCobranza.EstablecerValor("")
            ''.NumeroOperacionAcreedor.EstablecerValor("")
            .NumeroProductoServicioPagado.EstablecerValor("01")
            .NumeroTotalDocumentosPagados.EstablecerValor("001")
            .Filler.RellenarConEspaciosEnBlanco()
            .OrigenRespuesta.RellenarConEspaciosEnBlanco()
            ''.CodigoRespuestaExtendida.EstablecerValor("")
            ''.DescripcionRespuestaAplicativa.EstablecerValor("")
            ''.NombreDeudor.EstablecerValor("")
            .RUCDeudor.RellenarConEspaciosEnBlanco()
            .RUCAcreedor.RellenarConEspaciosEnBlanco()
            .CodigoZonaDeudor.RellenarConEspaciosEnBlanco()
            .Filler2.RellenarConEspaciosEnBlanco()
            ''.CodigoProductoServicio2.EstablecerValor("")
            ''.DescripcionProductoServicio.EstablecerValor("")
            ''.ImporteTotalProductoServicio.EstablecerValor("")
            .Mensaje1.RellenarConEspaciosEnBlanco()
            .Mensaje2.RellenarConEspaciosEnBlanco()
            .NumeroDocumentos.EstablecerValor("01")
            .Filler3.RellenarConEspaciosEnBlanco()
            '.TipoServicio.EstablecerValor("")
            '.DescripcionDocumento.EstablecerValor("")
            '.NumeroDocumento.EstablecerValor("")
            .PeriodoCotizacion.RellenarConEspaciosEnBlanco()
            .TipoDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            .NumeroDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            ''.FechaEmision.EstablecerValor("")
            ''.FechaVencimiento.EstablecerValor("")
            ''.ImportePagado.EstablecerValor("")
            ''.CodigoConcepto1.EstablecerValor("")
            ''.ImporteConcepto1.EstablecerValor(" ")
            .CodigoConcepto2.RellenarConEspaciosEnBlanco()
            .ImporteConcepto2.RellenarConCeros()
            .CodigoConcepto3.RellenarConEspaciosEnBlanco()
            .ImporteConcepto3.RellenarConCeros()
            .CodigoConcepto4.RellenarConEspaciosEnBlanco()
            .ImporteConcepto4.RellenarConCeros()
            .CodigoConcepto5.RellenarConEspaciosEnBlanco()
            .ImporteConcepto5.RellenarConCeros()
            .IndicadorFacturacion.RellenarConEspaciosEnBlanco()
            .NumeroFactura.RellenarConEspaciosEnBlanco()
            .ReferenciaDeuda.EstablecerValor(request.ReferenciaDeuda)
            .Filler4.RellenarConEspaciosEnBlanco()
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                'MODIFICADO
                '.NumeroOrdenPago = request.DatoPago.ToString.Trim
                'If .NumeroOrdenPago.Length > 14 Then
                '    .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                'End If
                .NumeroOrdenPago = request.DatoPago.ToString.Trim
                Try
                    .NumeroOrdenPago = Convert.ToInt32(.NumeroOrdenPago)
                Catch ex As Exception
                    If .NumeroOrdenPago.Length > 14 Then
                        .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                    End If
                End Try

                .NumeroOperacion = request.Trace.ToString.Trim
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.ImportePagadoDcumento.ToString.Trim) / 100
                .CodigoMedioPago = request.MedioPago.ToString
                .CodigoServicio = request.CodigoServicioPagado.ToString.Trim
                .CodigoAgenciaBancaria = request.CodigoAgenciaRecaudador.ToString.Trim

            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
            'Using Transaccion As New TransactionScope() Comentado el 06/04
            ' Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)

            'Using Transaccion As New TransactionScope() 'Comentado el 06/04
            IdMovimiento = PagarDesdeBancoComun(oBEMovimiento, Nothing, Nothing)
            If IdMovimiento > 0 Then
                Dim oBEOrdenPago As New BEOrdenPago
                With oBEOrdenPago
                    'MODIFICADO
                    '.NumeroOrdenPago = request.DatoPago.ToString.Trim
                    Try
                        .NumeroOrdenPago = Convert.ToInt32(request.DatoPago.ToString.Trim)
                    Catch ex As Exception
                        .NumeroOrdenPago = request.DatoPago.ToString.Trim
                    End Try

                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                    .CodigoServicio = request.CodigoServicioPagado.ToString.Trim
                End With
                Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                End Using
                With response.ObjBEBK
                    .AuthorizationIDResponse.EstablecerValor(IdMovimiento)
                    .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.RespuestaOk)
                    .NumeroOperacionCobranza.EstablecerValor(IdMovimiento)
                    .NumeroOperacionAcreedor.EstablecerValor(IdMovimiento)
                    .CodigoRespuestaExtendida.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.TransaccionExitosa)
                    ''.DescripcionRespuestaAplicativa.EstablecerValor("")
                    .NombreDeudor.EstablecerValor(oBEOrdenPago.DescripcionCliente, True)
                    .CodigoProductoServicio2.EstablecerValor(oBEOrdenPago.CodigoServicio)
                    .DescripcionProductoServicio.EstablecerValor(oBEOrdenPago.DescripcionServicio, True)
                    .ImporteTotalProductoServicio.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                    .TipoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                    .DescripcionDocumento.EstablecerValor(oBEOrdenPago.ConceptoPago, True)
                    .NumeroDocumento.EstablecerValor(oBEOrdenPago.IdCliente)
                    .FechaEmision.EstablecerValor(oBEOrdenPago.FechaEmision, "yyyyMMdd")
                    .FechaVencimiento.EstablecerValor(oBEOrdenPago.FechaVencimiento, "yyyyMMdd")
                    .ImportePagado.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                    .CodigoConcepto1.EstablecerValor("01")
                    .ImporteConcepto1.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                End With
                'Transaccion.Complete() 'Comentado el 06/04
            Else
                With response.ObjBEBK
                    .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.NoActionToken)
                    Select Case IdMovimiento
                        Case 0
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.TransaccionFallida)
                        Case -8
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPNoExiste)
                        Case -1
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPExpirado)
                        Case -27
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPEliminado)
                        Case -4
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPCancelado)
                        Case -2
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoMonedaNoCoincide)
                        Case -3
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.MontoNoCoincide)
                        Case -5
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaRegistroAgencia)
                        Case -6
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaUsuario)
                        Case -7
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                    End Select
                End With
            End If
            response.ObjBEBK.DescripcionRespuestaAplicativa.EstablecerValor(ListaCodTransaccionSBK(response.ObjBEBK.CodigoRespuestaExtendida.ToString))
            ListaCodTransaccionSBK.TryGetValue(response.ObjBEBK.CodigoRespuestaExtendida.ToString, response.Descripcion1)
            'End Using 'Comentado el 06/04
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
        End Try
        Return response
    End Function

    Public Function AnularSBK(ByVal request As BESBKAnularRequest) As BEBKResponse(Of BESBKAnularResponse)
        Dim response As New BEBKResponse(Of BESBKAnularResponse)
        response.ObjBEBK = New BESBKAnularResponse
        Dim IdMovimiento As Integer = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Scotiabank.IdentificacionTipoMensaje.Respuesta)
            .PrimaryBitMap.EstablecerValor("F22080010E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000018")
            .CodigoProceso.EstablecerValor(request.CodigoProceso)
            .Monto.EstablecerValor(request.CodigoProceso)
            .FechaHoraTransaccion.EstablecerValor(request.FechaHoraTransaccion)
            .Trace.EstablecerValor(request.Trace)
            .FechaCaptura.EstablecerValor(request.FechaCaptura)
            .IdentificacionEmpresa.EstablecerValor(request.BinAdquiriente)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            ''.AuthorizationIDResponse.EstablecerValor("")
            ''.ResponseCode.EstablecerValor("")
            .TerminalID.EstablecerValor(request.TerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .DatosReservados.EstablecerValor(request.DatosReservados)
            .LongitudTrama.EstablecerValor("206")
            .CodigoFormato.EstablecerValor("01")
            .BinProcesador.RellenarConEspaciosEnBlanco()
            .CodigoAcreedor.RellenarConEspaciosEnBlanco()
            .CodigoProductoServicio.EstablecerValor("REC")
            .CodigoPlazaRecaudador.RellenarConCeros()
            .CodigoAgenciaRecaudador.EstablecerValor(request.CodigoAgenciaRecaudador)
            .TipoDatoPago.EstablecerValor(request.TipoDatoPago)
            .DatoPago.EstablecerValor(request.DatoPago)
            .CodigoCiudad.EstablecerValor(request.CodigoCiudad)
            ''.NombreCliente.EstablecerValor("")
            .RUCDeudor.RellenarConEspaciosEnBlanco()
            .RUCAcreedor.RellenarConEspaciosEnBlanco()
            ''.NumeroTransaccionesCobrOri.EstablecerValor("")
            ''.NumeroOperacionesOriginalAcreedor.EstablecerValor("")
            .Filler.RellenarConEspaciosEnBlanco()
            .OrigenRespuesta.RellenarConEspaciosEnBlanco()
            ''.CodigoRespuestaExtendida.EstablecerValor("")
            ''.CodigoProductoServicio2.EstablecerValor("")
            ''.DescripcionProductoServicio.EstablecerValor("")
            ''.ImporteProductoServicio.EstablecerValor("")
            .Mensaje1Marketing.RellenarConEspaciosEnBlanco()
            .Mensaje2Marketing.RellenarConEspaciosEnBlanco()
            .NumeroDocumentos.EstablecerValor("01")
            .Filler2.RellenarConEspaciosEnBlanco()
            ''.TipoDocumentoServicio.EstablecerValor("")
            ''.DescripcionDocumento.EstablecerValor("")
            ''.NumeroDocumento.EstablecerValor("")
            .PeriodoCotizacion.RellenarConEspaciosEnBlanco()
            .TipoDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            .NumeroDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            ''.FechaEmision.EstablecerValor("")
            ''.FechaVencimiento.EstablecerValor("")
            ''.ImporteAnuladoDocumento.EstablecerValor("")
            ''.CodigoConcepto1.EstablecerValor("")
            ''.ImporteConcepto1.EstablecerValor("")
            .CodigoConcepto2.RellenarConEspaciosEnBlanco()
            .ImporteConcepto2.RellenarConCeros()
            .CodigoConcepto3.RellenarConEspaciosEnBlanco()
            .ImporteConcepto3.RellenarConCeros()
            .CodigoConcepto4.RellenarConEspaciosEnBlanco()
            .ImporteConcepto4.RellenarConCeros()
            .CodigoConcepto5.RellenarConEspaciosEnBlanco()
            .ImporteConcepto5.RellenarConCeros()
            .IndicadorComprobante.RellenarConEspaciosEnBlanco()
            .NumeroFacturaAnulada.RellenarConEspaciosEnBlanco()
            ''.ReferenciaDeuda.EstablecerValor("")
            .Filler3.RellenarConEspaciosEnBlanco()
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.DatoPago.ToString.Trim

                Try
                    .NumeroOrdenPago = Convert.ToInt32(.NumeroOrdenPago)
                Catch ex As Exception
                    If .NumeroOrdenPago.Length > 14 Then
                        .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                    End If
                End Try

                .NumeroOperacion = request.Trace.ToString.Trim
                .NumeroOperacionPago = request.OriDatEleTrace.ToString.Trim
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString.Trim
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto.ToString.Trim) / 100
                '.CodigoMedioPago = request.me
                .CodigoAgenciaBancaria = request.CodigoAgenciaRecaudador.ToString.Trim
                .CodigoServicio = request.TipoServicio.ToString.Trim
            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            Using Transaccion As New TransactionScope
                IdMovimiento = AnularDesdeBancoComun(oBEMovimiento)
                If IdMovimiento > 0 Then
                    Dim oBEOrdenPago As New BEOrdenPago
                    With oBEOrdenPago

                        Try
                            .NumeroOrdenPago = Convert.ToInt32(request.DatoPago.ToString.Trim)
                        Catch ex As Exception
                            .NumeroOrdenPago = request.DatoPago.ToString.Trim
                        End Try

                        .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                        .CodigoServicio = request.TipoServicio.ToString.Trim
                    End With
                    Using oDAAgenciaBancaria As New DAAgenciaBancaria
                        oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                    End Using
                    With response.ObjBEBK
                        .AuthorizationIDResponse.EstablecerValor(IdMovimiento)
                        .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.RespuestaOk)
                        .NombreCliente.EstablecerValor(oBEOrdenPago.DescripcionCliente, True)
                        .NumeroTransaccionesCobrOri.EstablecerValor(IdMovimiento)
                        .NumeroOperacionesOriginalAcreedor.EstablecerValor(IdMovimiento)
                        .CodigoRespuestaExtendida.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.TransaccionExitosa)
                        .CodigoProductoServicio2.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        .DescripcionProductoServicio.EstablecerValor(oBEOrdenPago.DescripcionServicio, True)
                        .ImporteProductoServicio.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        .TipoDocumentoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        .DescripcionDocumento.EstablecerValor(oBEOrdenPago.ConceptoPago, True)
                        .NumeroDocumento.EstablecerValor(request.DatoPago)
                        .FechaEmision.EstablecerValor(oBEOrdenPago.FechaEmision, "yyyyMMdd")
                        .FechaVencimiento.EstablecerValor(oBEOrdenPago.FechaVencimiento, "yyyyMMdd")
                        .ImporteAnuladoDocumento.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        .CodigoConcepto1.EstablecerValor("01")
                        .ImporteConcepto1.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        .ReferenciaDeuda.EstablecerValor(request.DatoPago)
                    End With
                    Transaccion.Complete()
                Else
                    With response.ObjBEBK
                        .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.NoActionToken)
                        Select Case IdMovimiento
                            Case 0
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.TransaccionFallida)
                            Case -11
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPNoExisteONoCancelado)
                            Case -17
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.MontoOMonedaNoCoincide)
                            Case -19
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide)
                            Case -13
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide)
                            Case -12
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.MovimientoPagoNoExiste)
                            Case -14
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.FechaPagoNoActual)
                            Case -15
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                            Case -37
                                'TIEMPO MAXIMO DE EXPIRACION
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NoSePermiteExtornar)
                        End Select
                        .DescripcionRespuestaAplicativa.EstablecerValor(ListaCodTransaccionSBK(.CodigoRespuestaExtendida.ToString))
                    End With
                End If
                ListaCodTransaccionSBK.TryGetValue(response.ObjBEBK.CodigoRespuestaExtendida.ToString, response.Descripcion1)
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

    Public Function ExtornarCancelacionSBK(ByVal request As BESBKExtornarCancelacionRequest) As BEBKResponse(Of BESBKExtornarCancelacionResponse)
        Dim response As New BEBKResponse(Of BESBKExtornarCancelacionResponse)
        response.ObjBEBK = New BESBKExtornarCancelacionResponse
        Dim IdMovimiento As Long = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Scotiabank.IdentificacionTipoMensaje.RespuestaAutomatica)
            .PrimaryBitMap.EstablecerValor("B22080010E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000018")
            .CodigoProceso.EstablecerValor(request.CodigoProceso)
            .Monto.EstablecerValor(request.Monto)
            .FechaHoraTransaccion.EstablecerValor(request.FechaHoraTransaccion)
            .Trace.EstablecerValor(request.Trace)
            .FechaCaptura.EstablecerValor(request.FechaCaptura)
            .IdentificacionEmpresa.EstablecerValor(request.BinAdquiriente)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            ''.AuthorizationIDResponse.EstablecerValor("")
            ''.ResponseCode.EstablecerValor("")
            .TerminalID.EstablecerValor(request.TerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .DatosReservados.EstablecerValor(request.DatosReservados)
            .TamanoBloque.EstablecerValor("557")
            .CodigoFormato.EstablecerValor("01")
            .BinProcesador.RellenarConCeros()
            .CodigoAcreedor.RellenarConCeros()
            .CodigoProductoServicio.EstablecerValor("REC")
            .CodigoPlazaRecaudador.EstablecerValor("0000")
            .CodigoAgenciaRecaudador.EstablecerValor(request.CodigoAgenciaRecaudador)
            .TipoDatoPago.EstablecerValor(request.TipoDatoPago)
            .DatoPago.EstablecerValor("")
            .CodigoCiudad.EstablecerValor(request.CodigoCiudad)
            ''.NumeroOperacionCobranza.EstablecerValor("")
            ''.NumeroOperacionAcreedor.EstablecerValor("")
            .NumeroProductoServicioPagado.EstablecerValor("01")
            .NumeroTotalDocumentosPagados.EstablecerValor("001")
            .Filler.RellenarConEspaciosEnBlanco()
            .OrigenRespuesta.RellenarConEspaciosEnBlanco()
            ''.CodigoRespuestaExtendida.EstablecerValor("")
            ''.DescripcionRespuestaAplicativa.EstablecerValor("")
            ''.NombreDeudor.EstablecerValor("")
            .RUCDeudor.RellenarConEspaciosEnBlanco()
            .RUCAcreedor.RellenarConEspaciosEnBlanco()
            .CodigoZonaDeudor.RellenarConEspaciosEnBlanco()
            .Filler2.RellenarConEspaciosEnBlanco()
            ''.CodigoProductoServicio2.EstablecerValor("")
            ''.DescripcionProductoServicio.EstablecerValor("")
            ''.ImporteTotalProductoServicio.EstablecerValor("")
            .Mensaje1.RellenarConEspaciosEnBlanco()
            .Mensaje2.RellenarConEspaciosEnBlanco()
            .NumeroDocumentos.EstablecerValor("01")
            .Filler3.RellenarConEspaciosEnBlanco()
            ''.TipoServicio.EstablecerValor("")
            ''.DescripcionDocumento.EstablecerValor("")
            ''.NumeroDocumento.EstablecerValor("")
            .PeriodoCotizacion.EstablecerValor("")
            .TipoDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            .NumeroDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            .FechaEmision.RellenarConEspaciosEnBlanco()
            ''.FechaVencimiento.EstablecerValor("")
            ''.ImportePagado.EstablecerValor("")
            ''.CodigoConcepto1.EstablecerValor("")
            ''.ImporteConcepto1.EstablecerValor("")
            .CodigoConcepto2.RellenarConEspaciosEnBlanco()
            .ImporteConcepto2.RellenarConCeros()
            .CodigoConcepto3.RellenarConEspaciosEnBlanco()
            .ImporteConcepto3.RellenarConCeros()
            .CodigoConcepto4.RellenarConEspaciosEnBlanco()
            .ImporteConcepto4.RellenarConCeros()
            .CodigoConcepto5.RellenarConEspaciosEnBlanco()
            .ImporteConcepto5.RellenarConCeros()
            .IndicadorFacturacion.RellenarConEspaciosEnBlanco()
            .NumeroFactura.RellenarConEspaciosEnBlanco()
            .Filler4.RellenarConEspaciosEnBlanco()
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.DatoPago.ToString.Trim
                'If .NumeroOrdenPago.Length > 14 Then
                '    .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                'End If

                Try
                    .NumeroOrdenPago = Convert.ToInt32(.NumeroOrdenPago)
                Catch ex As Exception
                    If .NumeroOrdenPago.Length > 14 Then
                        .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                    End If
                End Try

                .NumeroOperacion = request.Trace.ToString.Trim
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString.Trim
                .NumeroOperacionPago = request.OriDatEleTrace.ToString.Trim
                .CodigoServicio = request.CodigoServicioPagado.ToString.Trim
                .CodigoAgenciaBancaria = request.CodigoAgenciaRecaudador.ToString.Trim

                .CodigoMedioPago = request.MedioPago.ToString.Trim
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.ImportePagadoDocumento.ToString.Trim) / 100
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
            End With
            ' Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            'Using Transaccion As New TransactionScope
            IdMovimiento = ExtornarDesdeBancoComun(oBEMovimiento)
            If IdMovimiento > 0 Then
                Dim oBEOrdenPago As New BEOrdenPago
                With oBEOrdenPago
                    'agrgado
                    Try
                        .NumeroOrdenPago = Convert.ToInt32(request.DatoPago.ToString.Trim)
                    Catch ex As Exception
                        .NumeroOrdenPago = request.DatoPago.ToString.Trim
                    End Try

                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                    .CodigoServicio = request.CodigoServicioPagado.ToString.Trim
                End With
                Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                End Using
                With response.ObjBEBK
                    .AuthorizationIDResponse.EstablecerValor(IdMovimiento)
                    .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.RespuestaOk)
                    .NumeroOperacionCobranza.EstablecerValor(IdMovimiento)
                    .NumeroOperacionAcreedor.EstablecerValor(IdMovimiento)
                    .CodigoRespuestaExtendida.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.TransaccionExitosa)
                    '.DescripcionRespuestaAplicativa.EstablecerValor("")
                    .NombreDeudor.EstablecerValor(oBEOrdenPago.DescripcionCliente.ToString, True)
                    .CodigoProductoServicio2.EstablecerValor(oBEOrdenPago.CodigoServicio.ToString.Trim)
                    .DescripcionProductoServicio.EstablecerValor(oBEOrdenPago.DescripcionServicio, True)
                    .ImporteTotalProductoServicio.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                    .TipoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio.ToString.Trim)
                    .DescripcionDocumento.EstablecerValor(oBEOrdenPago.ConceptoPago, True)
                    .NumeroDocumento.EstablecerValor(request.DatoPago)
                    .FechaVencimiento.EstablecerValor(oBEOrdenPago.FechaVencimiento, "yyyyMMdd")
                    .ImportePagado.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                    .CodigoConcepto1.EstablecerValor("01")
                    .ImporteConcepto1.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                End With
                'Transaccion.Complete()
            Else
                With response.ObjBEBK
                    .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.NoActionToken)
                    Select Case IdMovimiento
                        Case 0
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.TransaccionFallida)
                        Case -21
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPNoExisteONoCanceladoOMonedaNoCoincide)
                        Case -26
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide)
                        Case -23
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide)
                        Case -22
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.MovimientoPagoNoExiste)
                        Case -24
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.FechaPagoNoActual)
                        Case -25
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                        Case -37
                            'TIEMPO MAXIMO DE EXTORNO
                            .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NoSePermiteExtornar)
                    End Select
                End With
            End If
            response.ObjBEBK.DescripcionRespuestaAplicativa.EstablecerValor(ListaCodTransaccionSBK(response.ObjBEBK.CodigoRespuestaExtendida.ToString))
            ListaCodTransaccionSBK.TryGetValue(response.ObjBEBK.CodigoRespuestaExtendida.ToString, response.Descripcion1)
            'End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

    Public Function ExtornarAnulacionSBK(ByVal request As BESBKExtornarAnulacionRequest) As BEBKResponse(Of BESBKExtornarAnulacionResponse)
        Dim response As New BEBKResponse(Of BESBKExtornarAnulacionResponse)
        response.ObjBEBK = New BESBKExtornarAnulacionResponse
        Dim IdMovimiento As Long = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Scotiabank.IdentificacionTipoMensaje.RespuestaAutomatica)
            .PrimaryBitMap.EstablecerValor("B22080010E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000018")
            .CodigoProceso.EstablecerValor(request.CodigoProceso)
            .Monto.EstablecerValor(request.Monto)
            .FechaHoraTransaccion.EstablecerValor(request.FechaHoraTransaccion)
            .FechaCaptura.EstablecerValor(request.FechaCaptura)
            .IdentificacionEmpresa.EstablecerValor(request.BinAdquiriente)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            ''.AuthorizationIDResponse.EstablecerValor("")
            ''.ResponseCode.EstablecerValor("")
            .TerminalID.EstablecerValor(request.TerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .DatosReservados.EstablecerValor(request.DatosReservados)
            .LongitudTrama.EstablecerValor("548")
            .CodigoFormato.EstablecerValor("01")
            .BinProcesador.RellenarConCeros()
            .CodigoAcreedor.RellenarConCeros()
            .CodigoProductoServicio.EstablecerValor("REC")
            .CodigoPlazaRecaudador.EstablecerValor("0000")
            .CodigoAgenciaRecaudador.EstablecerValor(request.CodigoAgenciaRecaudador)
            .TipoDatoPago.EstablecerValor(request.TipoDatoPago)
            .DatoPago.EstablecerValor(request.DatoPago)
            .CodigoCiudad.EstablecerValor(request.CodigoCiudad)
            ''.NombreCliente.EstablecerValor("")
            .RUCDeudor.RellenarConEspaciosEnBlanco()
            .RUCAcreedor.RellenarConEspaciosEnBlanco()
            ''.NumeroTransaccionesCobrOri.EstablecerValor("")
            ''.NumeroOperacionesOriginalAcreedor.EstablecerValor("")
            .Filler.RellenarConEspaciosEnBlanco()
            .OrigenRespuesta.RellenarConEspaciosEnBlanco()
            ''.CodigoRespuestaExtendida.EstablecerValor("")
            ''.DescripcionRespuestaAplicativa.EstablecerValor("")
            ''.CodigoProductoServicio2.EstablecerValor("")
            ''.DescripcionProductoServicio.EstablecerValor("")
            ''.ImporteProductoServicio.EstablecerValor("")
            .Mensaje1Marketing.RellenarConEspaciosEnBlanco()
            .Mensaje2Marketing.RellenarConEspaciosEnBlanco()
            .NumeroDocumentos.EstablecerValor("01")
            .Filler2.RellenarConEspaciosEnBlanco()
            ''.TipoDocumentoServicio.EstablecerValor("")
            ''.DescripcionDocumento.EstablecerValor("")
            ''.NumeroDocumento.EstablecerValor("")
            .PeriodoCotizacion.RellenarConEspaciosEnBlanco()
            .TipoDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            .NumeroDocumentoIdentidad.RellenarConEspaciosEnBlanco()
            ''.FechaEmision.EstablecerValor("")
            ''.FechaVencimiento.EstablecerValor("")
            ''.ImporteAnuladoDocumento.EstablecerValor("")
            ''.CodigoConcepto1.EstablecerValor("")
            ''.ImporteConcepto1.EstablecerValor("")
            .CodigoConcepto2.RellenarConEspaciosEnBlanco()
            .ImporteConcepto2.RellenarConCeros()
            .CodigoConcepto3.RellenarConEspaciosEnBlanco()
            .ImporteConcepto3.RellenarConCeros()
            .CodigoConcepto4.RellenarConEspaciosEnBlanco()
            .ImporteConcepto4.RellenarConCeros()
            .CodigoConcepto5.RellenarConEspaciosEnBlanco()
            .ImporteConcepto5.RellenarConCeros()
            .IndicadorComprobante.RellenarConEspaciosEnBlanco()
            .NumeroFacturaAnulada.RellenarConEspaciosEnBlanco()
            ''.ReferenciaDeuda.EstablecerValor("")
            .Filler3.RellenarConEspaciosEnBlanco()
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.DatoPago.ToString.Trim
                'If .NumeroOrdenPago.Length > 14 Then
                '    .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                'End If

                Try
                    .NumeroOrdenPago = Convert.ToInt32(.NumeroOrdenPago)
                Catch ex As Exception
                    If .NumeroOrdenPago.Length > 14 Then
                        .NumeroOrdenPago = .NumeroOrdenPago.Substring(0, 14)
                    End If
                End Try


                .NumeroOperacion = request.Trace.ToString.Trim
                .NumeroOperacionPago = request.OriDatEleTrace.ToString.Trim
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto.ToString.Trim) / 100
                '.CodigoMedioPago = request.MedioPago.ToString
                .CodigoServicio = request.TipoServicio.ToString.Trim
                .CodigoAgenciaBancaria = request.CodigoAgenciaRecaudador.ToString.Trim
            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            Using Transaccion As New TransactionScope
                IdMovimiento = ExtornarAnulacionDesdeBancoComun(oBEMovimiento)
                If IdMovimiento > 0 Then
                    Dim oBEOrdenPago As New BEOrdenPago
                    With oBEOrdenPago
                        '.NumeroOrdenPago = request.DatoPago.ToString.Trim

                        'agrgado
                        Try
                            .NumeroOrdenPago = Convert.ToInt32(request.DatoPago.ToString.Trim)
                        Catch ex As Exception
                            .NumeroOrdenPago = request.DatoPago.ToString.Trim
                        End Try

                        .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                        .CodigoServicio = request.TipoServicio.ToString.Trim
                    End With
                    Using oDAAgenciaBancaria As New DAAgenciaBancaria
                        oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                    End Using
                    With response.ObjBEBK
                        .AuthorizationIDResponse.EstablecerValor(IdMovimiento)
                        .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.RespuestaOk)
                        .NombreCliente.EstablecerValor(oBEOrdenPago.DescripcionCliente, True)
                        .NumeroTransaccionesCobrOri.EstablecerValor(IdMovimiento)
                        .NumeroOperacionesOriginalAcreedor.EstablecerValor(IdMovimiento)
                        .CodigoRespuestaExtendida.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Scotiabank.CodigosRespuestaExtendidos.TransaccionExitosa)
                        ''.DescripcionRespuestaAplicativa.EstablecerValor("")
                        .CodigoProductoServicio2.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        .DescripcionProductoServicio.EstablecerValor(oBEOrdenPago.DescripcionServicio, True)
                        .ImporteProductoServicio.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        ''.TipoDocumentoServicio.EstablecerValor("")
                        .DescripcionDocumento.EstablecerValor(oBEOrdenPago.ConceptoPago, True)
                        .NumeroDocumento.EstablecerValor(request.DatoPago)
                        .FechaEmision.EstablecerValor(oBEOrdenPago.FechaEmision, "yyyyMMdd")
                        .FechaVencimiento.EstablecerValor(oBEOrdenPago.FechaVencimiento, "yyyyMMdd")
                        .ImporteAnuladoDocumento.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        .CodigoConcepto1.EstablecerValor("01")
                        .ImporteConcepto1.EstablecerValor(oBEOrdenPago.Total, "#000000000.00", True)
                        .ReferenciaDeuda.EstablecerValor(request.DatoPago)
                    End With
                    Transaccion.Complete()
                Else
                    With response.ObjBEBK
                        .ResponseCode.EstablecerValor(Scotiabank.CodigosRespuesta.NoActionToken)
                        Select Case IdMovimiento
                            Case 0
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.TransaccionFallida)
                            Case -31
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CIPNoExisteONoGeneradoOMonedaNoCoincide)
                            Case -36
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide)
                            Case -32
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.MovimientoAnulacionNoExiste)
                            Case -33
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide)
                            Case -34
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.FechaAnulacionNoActual)
                            Case -25
                                .CodigoRespuestaExtendida.EstablecerValor(Scotiabank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                        End Select
                        .DescripcionRespuestaAplicativa.EstablecerValor(ListaCodTransaccionSBK(.CodigoRespuestaExtendida.ToString))
                    End With
                End If
                ListaCodTransaccionSBK.TryGetValue(response.ObjBEBK.CodigoRespuestaExtendida.ToString, response.Descripcion1)
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

#End Region

#Region "Integración Interbank"

    Private _listaCodTransaccionIBK As Dictionary(Of String, String) = Nothing
    Private ReadOnly Property ListaCodTransaccionIBK() As Dictionary(Of String, String)
        Get
            If (_listaCodTransaccionIBK Is Nothing) Then
                'No corregir la tildación ortográfica en los mensajes
                _listaCodTransaccionIBK = New Dictionary(Of String, String)
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador, "Transaccion aprobada por autorizador.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.CodigoProcesoNoSoportado, "Código de proceso no soportado.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.FechaHoraFueraRangoServicio, "Fecha/hora fuera del rango de servicio.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.CanalNoSoportado, "Canal no soportado.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.BinAdquirienteNoCorrespondeAutorizador, "Bin del adquiriente no corresponde a autorizador.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.BinAdministradorNoCorresponde, "Bin del administrador no corresponde.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.FormatoMensajeInvalido, "Formato de mensaje invalido.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.DocumentoReferenciaNoExiste, "Documento de referencia no existe.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.DocumentoReferenciaVencido, "Documento de referencia vencido.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.DocumentoReferenciaAnulado, "Documento de referencia anulado.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.DocumentoReferenciaBloqueado, "Documento de referencia bloqueado.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.DocumentoReferenciaExcedioTiempoParaAnulacion, "Documento de referencia excedio tiempo para anulacion")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.FormaPagoNoSoportada, "Forma de pago no soportada.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.ImportePagoNoCorresponde, "Importe de pago no corresponde.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.SistemaAutorizadorOcupadoReintenteLuego, "Sistema del autorizador ocupado, reintente luego.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.SistemaAutorizadorFueraServicio, "Sistema del autorizador fuera de servicio.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.TransaccionSospechosaFraude, "Transacción sospechosa de fraude.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.DocumentoReferenciaYaCancelado, "Documento de referencia ya cancelado.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.ProblemaAutenticacionConexion, "Problema con autenticacion de conexion.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.TransaccionDesaprobadaPorAutorizador, "Transaccion desaprobada por autorizador.")
                _listaCodTransaccionIBK.Add(Interbank.CodigosRespuesta.ErrorGeneralSistema, "Error general del sistema.")
            End If
            Return _listaCodTransaccionIBK
        End Get
    End Property

    Private _listaCodValTransaccionIBK As Dictionary(Of String, String)
    Public ReadOnly Property ListaCodValTransaccionIBK() As Dictionary(Of String, String)
        Get
            If _listaCodValTransaccionIBK Is Nothing Then
                _listaCodValTransaccionIBK = New Dictionary(Of String, String)
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.TramaInvalida, "Trama invalida")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.TransaccionFallida, "Transaccion fallida")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPNoExiste, "CIP no existe")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPEliminado, "CIP eliminado")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPCancelado, "CIP cancelado")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPExpirado, "El CIP esta expirado")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CodigoMonedaNoCoincide, "Codigo de moneda no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.MontoNoCoincide, "Monto no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaRegistroAgencia, "Notificacion fallida de agencia")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaUsuario, "Notificacion fallida a usuario")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaPortal, "Notificacion fallida a portal")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.MontoOMonedaNoCoincide, "Monto o moneda no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide, "Codigo de servicio no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide, "Agencia o numero de operacion de pago no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.MovimientoPagoNoExiste, "No se encuentra el registro de pago")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.FechaPagoNoActual, "Fecha de pago no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPNoExisteONoCancelado, "CIP no existe o no cancelado")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPNoExisteONoCanceladoOMonedaNoCoincide, "CIP no existe o no esta cancelado o moneda no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CIPNoExisteONoGeneradoOMonedaNoCoincide, "CIP no existe o no anulado o moneda incorrecta.")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.MovimientoAnulacionNoExiste, "No se encuentra el registro de anulacion")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide, "Agencia o NroOp Vs NroOpPago no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.FechaAnulacionNoActual, "Fecha de anulacion no coincide")
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.RespuestaOk, "PROCESADO OK")
                'TIEMPO MAXIMO DE EXPIRACION
                _listaCodValTransaccionIBK.Add(Interbank.CodigosRespuestaExtendidos.NoSePermiteExtornar, "El CIP no puede ser extornado")
            End If
            Return _listaCodValTransaccionIBK
        End Get
    End Property

    Public Function ConsultarIBK(ByVal request As BEIBKConsultarRequest) As BEBKResponse(Of BEIBKConsultarResponse)
        Dim response As New BEBKResponse(Of BEIBKConsultarResponse)
        response.ObjBEBK = New BEIBKConsultarResponse()
        'Estableciendo valores predeterminados
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Interbank.IdentificacionTipoMensaje.Respuesta)
            .PrimaryBitMap.EstablecerValor("F03804818E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000080")
            .PrimaryAccountNumber.EstablecerValor(request.PrimaryAccountNumber)
            .ProcessingCode.EstablecerValor(request.ProcessingCode)
            '.AmountTransaction.RellenarConCeros()
            .Trace.EstablecerValor(request.Trace)
            .TimeLocalTransaction.EstablecerValor(DateTime.Now.ToString("HHmmss"))
            .DateLocalTransaction.EstablecerValor(DateTime.Now.ToString("ddMMyyyy"))
            .POSEntryMode.EstablecerValor(request.POSEntryMode)
            .POSConditionCode.EstablecerValor(request.POSConditionCode)
            .AcquirerInstitutionIDCode.EstablecerValor(request.AcquirerInstitutionIDCode)
            .ForwardInstitutionIDCode.EstablecerValor(request.ForwardInstitutionIDCode)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            '.ApprovalCode.EstablecerValor()
            '.ResponseCode.EstablecerValor()
            .CardAcceptorTerminalID.EstablecerValor(request.CardAcceptorTerminalID)
            '.TransactionCurrencyCode.EstablecerValor()
            '.LongitudCampo.EstablecerValor()
            '.CIP.EstablecerValor()
            '.CodigoServicio.EstablecerValor()
            '.CodigoRetorno.EstablecerValor()
            '.DescripcionCodigoRetorno.EstablecerValor()
            '.NombreRazonSocial.EstablecerValor()
        End With
        Try
            If request.CIP.ToString.Trim = "" OrElse request.CodigoServicio.ToString.Trim = "" Then
                With response.ObjBEBK
                    .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.FormatoMensajeInvalido)
                    .CodigoRetorno.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuestaExtendidos.TramaInvalida)
                End With
            Else
                Dim oBEOrdenPago As New BEOrdenPago
                With oBEOrdenPago
                    .NumeroOrdenPago = request.CIP.ToString.Trim
                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                    .CodigoServicio = request.CodigoServicio.ToString.Trim
                End With
                Using oDAAgenciaBancaria As New DAAgenciaBancaria()
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                End Using
                If oBEOrdenPago Is Nothing Then
                    With response.ObjBEBK
                        .AmountTransaction.RellenarConCeros()
                        '.ApprovalCode.RellenarConEspaciosEnBlanco()
                        .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))
                        .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.DocumentoReferenciaNoExiste)
                        .TransactionCurrencyCode.RellenarConEspaciosEnBlanco()
                        .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                       .DescripcionCodigoRetorno.Longitud + .NombreRazonSocial.Longitud, "#000", True)
                        .CIP.RellenarConCeros()
                        .CodigoServicio.RellenarConCeros()
                        .CodigoRetorno.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuestaExtendidos.CIPNoExiste)
                        .NombreRazonSocial.RellenarConEspaciosEnBlanco()
                    End With
                Else
                    With response.ObjBEBK
                        .AmountTransaction.EstablecerValor(oBEOrdenPago.Total, "#0000000000.00", True)
                        '.ApprovalCode.RellenarConEspaciosEnBlanco()
                        .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))
                        '.ResponseCode
                        .TransactionCurrencyCode.EstablecerValor(oBEOrdenPago.codMonedaBanco)
                        .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                       .DescripcionCodigoRetorno.Longitud + .NombreRazonSocial.Longitud, "#000", True)
                        .CIP.EstablecerValor(Convert.ToInt64(oBEOrdenPago.NumeroOrdenPago.Trim), "#00000000000000", True)
                        .CodigoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        '.CodigoRetorno
                        '.DescripcionCodigoRetorno
                        .NombreRazonSocial.EstablecerValor(oBEOrdenPago.DescripcionCliente, True)
                        .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                        .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.RespuestaOk)
                    End With
                    If oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Eliminado Then
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.DocumentoReferenciaNoExiste)
                            .CodigoRetorno.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuestaExtendidos.CIPEliminado)
                        End With
                    ElseIf oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada Then
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                            .CodigoRetorno.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuestaExtendidos.CIPCancelado)
                        End With
                    ElseIf oBEOrdenPago.CipListoParaExpirar OrElse oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Expirado Then
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.DocumentoReferenciaVencido)
                            .CodigoRetorno.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuestaExtendidos.CIPExpirado)
                        End With
                    Else
                        'Éxito
                        With response.ObjBEBK
                            .ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                            .CodigoRetorno.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuestaExtendidos.RespuestaOk)
                        End With
                    End If
                End If
            End If
            ListaCodTransaccionIBK.TryGetValue(response.ObjBEBK.ResponseCode.ToString, response.Descripcion1)
            ListaCodValTransaccionIBK.TryGetValue(response.ObjBEBK.CodigoRetorno.ToString, response.Descripcion2)
            response.ObjBEBK.DescripcionCodigoRetorno.EstablecerValor(response.Descripcion2)
            response.ObjBEBK.ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

    Public Function CancelarIBK(ByVal request As BEIBKCancelarRequest) As BEBKResponse(Of BEIBKCancelarResponse)
        Dim response As New BEBKResponse(Of BEIBKCancelarResponse)
        response.ObjBEBK = New BEIBKCancelarResponse()
        Dim IdMovimiento As Long = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Interbank.IdentificacionTipoMensaje.Respuesta)
            .PrimaryBitMap.EstablecerValor("F03804818E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000080")
            .PrimaryAccountNumber.EstablecerValor(request.PrimaryAccountNumber)
            .ProcessingCode.EstablecerValor(request.ProcessingCode)
            .AmountTransaction.EstablecerValor(request.AmountTransaction)
            .Trace.EstablecerValor(request.Trace)
            .TimeLocalTransaction.EstablecerValor(DateTime.Now.ToString("HHmmss"))
            .DateLocalTransaction.EstablecerValor(DateTime.Now.ToString("ddMMyyyy"))
            .POSEntryMode.EstablecerValor(request.POSEntryMode)
            .POSConditionCode.EstablecerValor(request.POSConditionCode)
            .AcquirerInstitutionIDCode.EstablecerValor(request.AcquirerInstitutionIDCode)
            .ForwardInstitutionIDCode.EstablecerValor(request.ForwardInstitutionIDCode)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            '.ApprovalCode.EstablecerValor()
            '.ResponseCode.EstablecerValor()
            .CardAcceptorTerminalID.EstablecerValor(request.CardAcceptorTerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            '.LongitudCampo.EstablecerValor()
            '.CIP.EstablecerValor()
            '.CodigoServicio.EstablecerValor()
            '.CodigoRetorno.EstablecerValor()
            '.DescripcionCodigoRetorno.EstablecerValor()
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.CIP.ToString.Trim
                .NumeroOperacion = request.RetrievalReferenceNumber.ToString.Trim
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.AmountTransaction.ToString.Trim) / 100
                .CodigoMedioPago = request.ProcessingCode.ToString.Substring(2, 4)
                .CodigoServicio = request.CodigoServicio.ToString.Trim
                .CodigoAgenciaBancaria = request.CardAcceptorIDCode.ToString.Trim
                'SE AGREGDO CODIGO CANAL
                .CodigoCanal = request.POSConditionCode.ToString.Trim
            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
            'Using Transaccion As New TransactionScope() 'Comentado el 06/04
            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            'Using Transaccion As New TransactionScope
            IdMovimiento = PagarDesdeBancoComun(oBEMovimiento, Nothing, Nothing)
            If IdMovimiento > 0 Then
                Dim oBEOrdenPago As New BEOrdenPago
                With oBEOrdenPago
                    .NumeroOrdenPago = request.CIP.ToString.Trim
                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                    .CodigoServicio = request.CodigoServicio.ToString.Trim
                End With
                Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                End Using
                With response.ObjBEBK
                    '.ApprovalCode.EstablecerValor(IdMovimiento.ToString)
                    .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))

                    .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                    .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                   .DescripcionCodigoRetorno.Longitud, "#000", True)
                    .CIP.EstablecerValor(oBEOrdenPago.NumeroOrdenPago)
                    .CodigoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                    .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.RespuestaOk)
                    '.DescripcionCodigoRetorno.EstablecerValor()
                End With
                'Transaccion.Complete() 'Comentado el 06/04
            Else
                With response.ObjBEBK
                    '.ApprovalCode.RellenarConCeros()
                    .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))
                    .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionDesaprobadaPorAutorizador)
                    .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                   .DescripcionCodigoRetorno.Longitud, "#000", True)
                    .CIP.RellenarConCeros()
                    .CodigoServicio.RellenarConEspaciosEnBlanco()
                    Select Case IdMovimiento
                        Case 0
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.TransaccionFallida)
                        Case -8
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPNoExiste)
                        Case -1
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPExpirado)
                        Case -27
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPEliminado)
                        Case -4
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPCancelado)
                        Case -2
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoMonedaNoCoincide)
                        Case -3
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.MontoNoCoincide)
                        Case -5
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaRegistroAgencia)
                        Case -6
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaUsuario)
                        Case -7
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                    End Select
                End With
            End If
            ListaCodTransaccionIBK.TryGetValue(response.ObjBEBK.ResponseCode.ToString, response.Descripcion1)
            ListaCodValTransaccionIBK.TryGetValue(response.ObjBEBK.CodigoRetorno.ToString, response.Descripcion2)
            response.ObjBEBK.DescripcionCodigoRetorno.EstablecerValor(response.Descripcion2)
            response.ObjBEBK.ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
            'End Using   'Comentado el 06/04
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

    Public Function AnularIBK(ByVal request As BEIBKAnularRequest) As BEBKResponse(Of BEIBKAnularResponse)
        Dim response As New BEBKResponse(Of BEIBKAnularResponse)
        response.ObjBEBK = New BEIBKAnularResponse()
        Dim IdMovimiento As Integer = 0
        Try
            With response.ObjBEBK
                .MessageTypeIdentification.EstablecerValor(Interbank.IdentificacionTipoMensaje.Respuesta)
                .PrimaryBitMap.EstablecerValor("F03804818E808000")
                .SecondaryBitMap.EstablecerValor("0000000000000080")
                .PrimaryAccountNumber.EstablecerValor(request.PrimaryAccountNumber)
                .ProcessingCode.EstablecerValor(request.ProcessingCode)
                .AmountTransaction.EstablecerValor(request.AmountTransaction)
                .Trace.EstablecerValor(request.Trace)
                .TimeLocalTransaction.EstablecerValor(DateTime.Now.ToString("HHmmss"))
                .DateLocalTransaction.EstablecerValor(DateTime.Now.ToString("ddMMyyyy"))
                .POSEntryMode.EstablecerValor(request.POSEntryMode)
                .POSConditionCode.EstablecerValor(request.POSConditionCode)
                .AcquirerInstitutionIDCode.EstablecerValor(request.AcquirerInstitutionIDCode)
                .ForwardInstitutionIDCode.EstablecerValor(request.ForwardInstitutionIDCode)
                .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
                '.ApprovalCode.EstablecerValor()
                .ResponseCode.EstablecerValor("00")
                .CardAcceptorTerminalID.EstablecerValor(request.CardAcceptorTerminalID)
                .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
                .LongitudCampo.EstablecerValor(request.LongitudCampo)
                .CIP.EstablecerValor(request.CIP)
                .CodigoServicio.EstablecerValor(request.CodigoServicio)
            End With
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.CIP.ToString.Trim
                .NumeroOperacion = request.RetrievalReferenceNumber.ToString.Trim
                .NumeroOperacionPago = request.NumeroOperacionPago.ToString.Trim
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString.Trim
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.AmountTransaction.ToString.Trim) / 100
                .CodigoMedioPago = request.ProcessingCode.ToString.Substring(2, 4)
                .CodigoAgenciaBancaria = request.CardAcceptorIDCode.ToString.Trim
                .CodigoServicio = request.CodigoServicio.ToString.Trim
            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            Using Transaccion As New TransactionScope
                IdMovimiento = AnularDesdeBancoComun(oBEMovimiento)
                If IdMovimiento > 0 Then
                    'Dim oBEOrdenPago As New BEOrdenPago
                    'With oBEOrdenPago
                    '    .NumeroOrdenPago = request.CIP.ToString.Trim
                    '    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                    '    .CodigoServicio = request.CodigoServicio.ToString.Trim
                    'End With
                    'Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    '    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                    'End Using
                    With response.ObjBEBK
                        '.ApprovalCode.EstablecerValor(IdMovimiento)
                        .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))
                        .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                        .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                           .DescripcionCodigoRetorno.Longitud, "#000", True)
                        '.CodigoServicio.EstablecerValor(oBEOrdenPago.CodigoServicio)
                        .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.RespuestaOk)
                    End With
                    Transaccion.Complete()
                Else
                    With response.ObjBEBK
                        .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionDesaprobadaPorAutorizador)
                        Select Case IdMovimiento
                            Case 0
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.TransaccionFallida)
                            Case -11
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPNoExisteONoCancelado)
                            Case -17
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.MontoOMonedaNoCoincide)
                            Case -19
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide)
                            Case -13
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide)
                            Case -12
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.MovimientoPagoNoExiste)
                            Case -14
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.FechaPagoNoActual)
                            Case -15
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                            Case -37
                                'TIEMPO MAXIMO DE EXPIRACION
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NoSePermiteExtornar)
                        End Select
                    End With
                End If
                ListaCodTransaccionIBK.TryGetValue(response.ObjBEBK.ResponseCode.ToString, response.Descripcion1)
                ListaCodValTransaccionIBK.TryGetValue(response.ObjBEBK.CodigoRetorno.ToString, response.Descripcion2)
                response.ObjBEBK.DescripcionCodigoRetorno.EstablecerValor(response.Descripcion2)
                response.ObjBEBK.ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

    Public Function ExtornarCancelacionIBK(ByVal request As BEIBKExtornarCancelacionRequest) As BEBKResponse(Of BEIBKExtornarCancelacionResponse)
        Dim response As New BEBKResponse(Of BEIBKExtornarCancelacionResponse)
        response.ObjBEBK = New BEIBKExtornarCancelacionResponse()
        Dim IdMovimiento As Integer = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Interbank.IdentificacionTipoMensaje.RespuestaAutomatica)
            .PrimaryBitMap.EstablecerValor("F03804818E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000080")
            .PrimaryAccountNumber.EstablecerValor(request.PrimaryAccountNumber)
            .ProcessingCode.EstablecerValor(request.ProcessingCode)
            .AmountTransaction.EstablecerValor(request.AmountTransaction)
            .Trace.EstablecerValor(request.Trace)
            .TimeLocalTransaction.EstablecerValor(DateTime.Now.ToString("HHmmss"))
            .DateLocalTransaction.EstablecerValor(DateTime.Now.ToString("ddMMyyyy"))
            .POSEntryMode.EstablecerValor(request.POSEntryMode)
            .POSConditionCode.EstablecerValor(request.POSConditionCode)
            .AcquirerInstitutionIDCode.EstablecerValor(request.AcquirerInstitutionIDCode)
            .ForwardInstitutionIDCode.EstablecerValor(request.ForwardInstitutionIDCode)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            '.ApprovalCode.EstablecerValor() ->idmovimiento
            .ResponseCode.EstablecerValor("00")
            .CardAcceptorTerminalID.EstablecerValor(request.CardAcceptorTerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .LongitudCampo.EstablecerValor(request.LongitudCampo)
            .CIP.EstablecerValor(request.CIP)
            .CodigoServicio.EstablecerValor(request.CodigoServicio)
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOperacion = request.RetrievalReferenceNumber.ToString.Trim
                .NumeroOperacionPago = request.RetrievalReferenceNumber.ToString.Trim
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString.Trim
                .CodigoServicio = request.CodigoServicio.ToString.Trim
                .CodigoAgenciaBancaria = request.CardAcceptorIDCode.ToString.Trim
                .NumeroOrdenPago = request.CIP.ToString.Trim
                .CodigoMedioPago = request.ProcessingCode.ToString().Substring(2, 4)
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.AmountTransaction.ToString.Trim) / 100
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            'Using Transaccion As New TransactionScope
            IdMovimiento = ExtornarDesdeBancoComun(oBEMovimiento)
            If IdMovimiento > 0 Then
                'Dim oBEOrdenPago As New BEOrdenPago
                'With oBEOrdenPago
                '    .NumeroOrdenPago = request.CIP.ToString.Trim
                '    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                '    .CodigoServicio = request.CodigoServicio.ToString.Trim
                'End With
                'Using oDAAgenciaBancaria As New DAAgenciaBancaria
                '    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                'End Using
                With response.ObjBEBK
                    '.ApprovalCode.EstablecerValor(IdMovimiento)
                    .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))
                    .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                       .DescripcionCodigoRetorno.Longitud, "#000", True)
                    .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                    .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.RespuestaOk)
                End With
                'Transaccion.Complete()
            Else
                With response.ObjBEBK
                    .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionDesaprobadaPorAutorizador)
                    Select Case IdMovimiento
                        Case 0
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.TransaccionFallida)
                        Case -21
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPNoExisteONoCanceladoOMonedaNoCoincide)
                        Case -26
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide)
                        Case -23
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide)
                        Case -22
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.MovimientoPagoNoExiste)
                        Case -24
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.FechaPagoNoActual)
                        Case -25
                            .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                            'Case -37
                            '    'TIEMPO MAXIMO DE EXTORNO
                            '    .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NoSePermiteExtornar)
                    End Select
                End With
            End If
            ListaCodTransaccionIBK.TryGetValue(response.ObjBEBK.ResponseCode.ToString, response.Descripcion1)
            ListaCodValTransaccionIBK.TryGetValue(response.ObjBEBK.CodigoRetorno.ToString, response.Descripcion2)
            response.ObjBEBK.DescripcionCodigoRetorno.EstablecerValor(response.Descripcion2)
            response.ObjBEBK.ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
            'End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

    Public Function ExtornarAnulacionIBK(ByVal request As BEIBKExtornarAnulacionRequest) As BEBKResponse(Of BEIBKExtornarAnulacionResponse)
        Dim response As New BEBKResponse(Of BEIBKExtornarAnulacionResponse)
        response.ObjBEBK = New BEIBKExtornarAnulacionResponse()
        Dim IdMovimiento As Integer = 0
        With response.ObjBEBK
            .MessageTypeIdentification.EstablecerValor(Interbank.IdentificacionTipoMensaje.RespuestaAutomatica)
            .PrimaryBitMap.EstablecerValor("F03804818E808000")
            .SecondaryBitMap.EstablecerValor("0000000000000080")
            .PrimaryAccountNumber.EstablecerValor(request.PrimaryAccountNumber)
            .ProcessingCode.EstablecerValor(request.ProcessingCode)
            .AmountTransaction.EstablecerValor(request.AmountTransaction)
            .Trace.EstablecerValor(request.Trace)
            .TimeLocalTransaction.EstablecerValor(DateTime.Now.ToString("HHmmss"))
            .DateLocalTransaction.EstablecerValor(DateTime.Now.ToString("ddMMyyyy"))
            .POSEntryMode.EstablecerValor(request.POSEntryMode)
            .POSConditionCode.EstablecerValor(request.POSConditionCode)
            .AcquirerInstitutionIDCode.EstablecerValor(request.AcquirerInstitutionIDCode)
            .ForwardInstitutionIDCode.EstablecerValor(request.ForwardInstitutionIDCode)
            .RetrievalReferenceNumber.EstablecerValor(request.RetrievalReferenceNumber)
            '.ApprovalCode.EstablecerValor() ->idmovimiento
            .ResponseCode.EstablecerValor("00")
            .CardAcceptorTerminalID.EstablecerValor(request.CardAcceptorTerminalID)
            .TransactionCurrencyCode.EstablecerValor(request.TransactionCurrencyCode)
            .LongitudCampo.EstablecerValor(request.LongitudCampo)
            .CIP.EstablecerValor(request.CIP)
            .CodigoServicio.EstablecerValor(request.CodigoServicio)
        End With
        Try
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.CIP.ToString.Trim
                .NumeroOperacion = request.RetrievalReferenceNumber.ToString.Trim
                .NumeroOperacionPago = request.NumeroOperacionPago.ToString.Trim
                .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                .CodMonedaBanco = request.TransactionCurrencyCode.ToString
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.AmountTransaction.ToString.Trim) / 100
                '.CodigoMedioPago = request.MedioPago.ToString
                .CodigoServicio = request.CodigoServicio.ToString.Trim
                .CodigoAgenciaBancaria = request.CardAcceptorIDCode.ToString.Trim
            End With
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
            Using Transaccion As New TransactionScope
                IdMovimiento = ExtornarAnulacionDesdeBancoComun(oBEMovimiento)
                If IdMovimiento > 0 Then
                    Dim oBEOrdenPago As New BEOrdenPago
                    With oBEOrdenPago
                        .NumeroOrdenPago = request.CIP.ToString.Trim
                        .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                        .CodigoServicio = request.CodigoServicio.ToString.Trim
                    End With
                    Using oDAAgenciaBancaria As New DAAgenciaBancaria
                        oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                    End Using
                    With response.ObjBEBK
                        .ApprovalCode.EstablecerValor(New Random().Next(999999).ToString().PadLeft(6, "0"))
                        '.ApprovalCode.EstablecerValor(IdMovimiento)
                        .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
                        .LongitudCampo.EstablecerValor(.CIP.Longitud + .CodigoServicio.Longitud + .CodigoRetorno.Longitud +
                                                           .DescripcionCodigoRetorno.Longitud, "#000", True)
                        .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.RespuestaOk)
                    End With
                    Transaccion.Complete()
                Else
                    With response.ObjBEBK
                        .ResponseCode.EstablecerValor(Interbank.CodigosRespuesta.TransaccionDesaprobadaPorAutorizador)
                        Select Case IdMovimiento
                            Case 0
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.TransaccionFallida)
                            Case -31
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CIPNoExisteONoGeneradoOMonedaNoCoincide)
                            Case -36
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoServicioNoCoincide)
                            Case -32
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.MovimientoAnulacionNoExiste)
                            Case -33
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide)
                            Case -34
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.FechaAnulacionNoActual)
                            Case -25
                                .CodigoRetorno.EstablecerValor(Interbank.CodigosRespuestaExtendidos.NotificacionFallidaPortal)
                        End Select
                    End With
                End If
                ListaCodTransaccionIBK.TryGetValue(response.ObjBEBK.ResponseCode.ToString, response.Descripcion1)
                ListaCodValTransaccionIBK.TryGetValue(response.ObjBEBK.CodigoRetorno.ToString, response.Descripcion2)
                response.ObjBEBK.DescripcionCodigoRetorno.EstablecerValor(response.Descripcion2)
                response.ObjBEBK.ResponseCode.EstablecerValor(SPE.EmsambladoComun.ParametrosSistema.Interbank.CodigosRespuesta.TransaccionAprobadaPorAutorizador)
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Descripcion2 = ex.ToString()
            'Throw ex
        End Try
        Return response
    End Function

#End Region

#Region "Integración Wester Union"
    Private _listaCodTransaccionWU As Dictionary(Of Integer, String) = Nothing
    Private ReadOnly Property ListaCodTransaccionWU() As Dictionary(Of Integer, String)
        Get
            If (_listaCodTransaccionWU Is Nothing) Then
                'No corregir la tildación ortográfica en los mensajes
                _listaCodTransaccionWU = New Dictionary(Of Integer, String)
                _listaCodTransaccionWU.Add(WesternUnion.CodError.TransaccionAceptada, "Transacción aceptada.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.ErrorBD, "Error en BD.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.ClaveDuplicada, "Clave duplicada.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.TransaccionYaReversada, "Transacción ya reversada.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.NoExisteDirecta, "No existe la directa.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.OperacionInvalida, "Operación invalida.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.NoExisteRegistro, "No existe registro.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.ClienteNoExiste, "Cliente no existe.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.ErrorValidacionCodigoCliente, "Error de validación del código de cliente.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.ParametrosIncorrectos, "Parámetros incorrectos o faltantes.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.ErrorInternoEntidad, "Error interno de la entidad.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.UsuarioNoHabilitadoParaOperar, "Usuario no habilitado para operar.")
                _listaCodTransaccionWU.Add(WesternUnion.CodError.TransaccionFueraDeHorario, "Transacción fuera de horario.")
                'TIEMPO MAXIMO DE EXPIRACION
                _listaCodTransaccionWU.Add(WesternUnion.CodError.NoSePermiteExtornar, "Tiempo limite extorno excedido")
            End If
            Return _listaCodTransaccionWU
        End Get
    End Property

    Private _listaCodTransaccionWUExtendido As Dictionary(Of String, String)
    Public ReadOnly Property ListaCodTransaccionWUExtendido() As Dictionary(Of String, String)
        Get
            If _listaCodTransaccionWUExtendido Is Nothing Then
                _listaCodTransaccionWUExtendido = New Dictionary(Of String, String)
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.TramaInvalida, "Trama invalida")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.TransaccionFallida, "Transaccion fallida")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPNoExiste, "CIP no existe")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPEliminado, "CIP eliminado")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPCancelado, "CIP cancelado")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPExpirado, "El CIP esta expirado")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CodigoMonedaNoCoincide, "Codigo de moneda no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.MontoNoCoincide, "Monto no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.NotificacionFallidaRegistroAgencia, "Notificacion fallida de agencia")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.NotificacionFallidaUsuario, "Notificacion fallida a usuario")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.NotificacionFallidaPortal, "Notificacion fallida a portal")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.MontoOMonedaNoCoincide, "Monto o moneda no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CodigoServicioNoCoincide, "Codigo de servicio no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide, "Agencia o numero de operacion de pago no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.MovimientoPagoNoExiste, "No se encuentra el registro de pago")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.FechaPagoNoActual, "Fecha de pago no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPNoExisteONoCancelado, "CIP no existe o no cancelado")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPNoExisteONoCanceladoOMonedaNoCoincide, "CIP no existe o moneda no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CIPNoExisteONoGeneradoOMonedaNoCoincide, "CIP no existe o no anulado o moneda incorrecta.")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.MovimientoAnulacionNoExiste, "No se encuentra el registro de anulacion")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CodigoAgenciaBancariaONumeroOperacionAnulacionNoCoincide, "Agencia o numero de operacion de anulacion no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.FechaAnulacionNoActual, "Fecha de anulacion no coincide")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.RespuestaOk, "Respuesta correcta a solicitud")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CodigoPaisErroneo, "El codigo de Pais es erroneo.")
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.CodigoEntidadErroneo, "El codigo de Entidad es erroneo.")
                'TIEMPO MAXIMO DE EXPIRACION
                _listaCodTransaccionWUExtendido.Add(WesternUnion.CodErrorExtendido.NoSePermiteExtornar, "Tiempo limite extorno excedido")
            End If
            Return _listaCodTransaccionWUExtendido
        End Get
    End Property

    Public Function ConsultarWU(ByVal request As BEWUConsultarRequest) As BEWUConsultarResponse
        Dim response As New BEWUConsultarResponse()
        response.ObeLog = New BELog()
        response.ObjBEBK = New ConsultaResp()
        response.ObjBEBK.fields = New List(Of ArrayFieldsQuerie)().ToArray
        response.ObjBEBK.count = 0
        response.ObjBEBK.cob_cliente_nomb = String.Empty
        response.ObjBEBK.seleccion_con_prioridad = 0
        response.ObjBEBK.header = asignarHeaderDefault(request.HdReq)
        response.BarCodeObject = New BEWUBarCode()

        Try
            If request.Codigo_cliente.ToString.Trim = "" Or request.CodigoServicio.ToString.Trim = "" Then
                With response
                    .ObjBEBK.header.codError = WesternUnion.CodError.ParametrosIncorrectos
                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.TramaInvalida
                End With
            Else
                Dim oBEOrdenPago As New BEOrdenPago
                With oBEOrdenPago
                    .NumeroOrdenPago = request.Codigo_cliente.ToString.Trim
                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                    .CodigoServicio = request.CodigoServicio.ToString.Trim
                End With
                Using oDAAgenciaBancaria As New DAAgenciaBancaria()
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                End Using
                If oBEOrdenPago Is Nothing Then
                    With response
                        .ObjBEBK.header.codError = WesternUnion.CodError.ClienteNoExiste
                        .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPNoExiste
                    End With
                Else
                    With response
                        Dim detalles(0) As ArrayFieldsQuerie
                        detalles(0) = New ArrayFieldsQuerie()
                        detalles(0).cob_cobro_tipo = WesternUnion.TipoPago.Total
                        'codEntidad | codPais | codMoneda | CIP | monto | Fecha Expirar YYddd
                        detalles(0).cob_cod_barra = WesternUnion.CodBarra.CodPais & _
                            WesternUnion.CodBarra.CodEntidad & _
                            oBEOrdenPago.codMonedaBanco & _
                            WesternUnion.CodMedioPago.Efectivo & oBEOrdenPago.NumeroOrdenPago.PadLeft(15, "0") & "".PadLeft(20, "0") & _
                            oBEOrdenPago.Total.ToString("0000000#.00").Replace(".", "") & _
                            oBEOrdenPago.FechaVencimiento.ToString("yy") & oBEOrdenPago.FechaVencimiento.DayOfYear.ToString().PadLeft(3, "0")
                        detalles(0).cob_comp_imp = Int32.Parse(oBEOrdenPago.Total.ToString("0000000#.00").Replace(".", ""))
                        detalles(0).cob_estado = WesternUnion.CobroEstado.Disponible
                        detalles(0).cob_prior_gpo = String.Empty
                        detalles(0).cob_prior_nro = String.Empty
                        detalles(0).cob_texto_fe = oBEOrdenPago.ConceptoPago
                        detalles(0).numero_de_orden = 1
                        .ObjBEBK.fields = detalles
                        .ObjBEBK.count = 1
                        .ObjBEBK.cob_cliente_nomb = IIf(String.IsNullOrEmpty(oBEOrdenPago.UsuarioNombre), String.Empty, oBEOrdenPago.UsuarioNombre)
                        .ObjBEBK.seleccion_con_prioridad = 1
                        .BarCodeObject = New BEWUBarCode(detalles(0).cob_cod_barra)
                        .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionAceptada
                        .CodErrorExtendido = WesternUnion.CodErrorExtendido.RespuestaOk
                    End With
                    If oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Eliminado Then
                        With response
                            .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPEliminado
                            .ObjBEBK.header.codError = WesternUnion.CodError.ClienteNoExiste
                            .ObjBEBK.fields(0).cob_estado = WesternUnion.CobroEstado.NoCobrable
                        End With
                    ElseIf oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada Then
                        With response
                            .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPCancelado
                            .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionAceptada
                            .ObjBEBK.fields(0).cob_estado = WesternUnion.CobroEstado.Cobrada
                        End With
                    ElseIf oBEOrdenPago.CipListoParaExpirar OrElse oBEOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Expirado Then
                        With response
                            .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPExpirado
                            .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionFueraDeHorario
                            .ObjBEBK.fields(0).cob_estado = WesternUnion.CobroEstado.NoCobrable
                        End With
                    Else
                        'Éxito
                        With response
                            .CodErrorExtendido = WesternUnion.CodErrorExtendido.RespuestaOk
                            .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionAceptada
                            .ObjBEBK.fields(0).cob_estado = WesternUnion.CobroEstado.Disponible
                        End With
                    End If
                End If
            End If
            ListaCodTransaccionWU.TryGetValue(response.ObjBEBK.header.codError, response.ObeLog.Descripcion)
            ListaCodTransaccionWUExtendido.TryGetValue(response.CodErrorExtendido, response.ObeLog.Descripcion2)
        Catch sqlEx As SqlException
            _nlogger.Error(sqlEx, sqlEx.Message)
            ListaCodTransaccionWU.TryGetValue(WesternUnion.CodError.ErrorBD, response.ObeLog.Descripcion)
            response.ObeLog.Descripcion2 = sqlEx.ToString
            response.ObjBEBK.header.codError = WesternUnion.CodError.ErrorBD
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            ListaCodTransaccionWU.TryGetValue(WesternUnion.CodError.ErrorInternoEntidad, response.ObeLog.Descripcion)
            response.ObeLog.Descripcion2 = ex.ToString
            response.ObjBEBK.header.codError = WesternUnion.CodError.ErrorInternoEntidad
        End Try
        response.ObjBEBK.header.codSeveridad = WesternUnion.CodSeveridad.resolverSeveridad(response.ObjBEBK.header.codError)
        Return response
    End Function
    Public Function CancelarWU(ByVal request As BEWUCancelarRequest) As BEWUCancelarResponse
        Dim response As New BEWUCancelarResponse()
        response.ObeLog = New BELog()
        response.ObjBEBK = New DirectaResp()
        response.ObjBEBK.header = asignarHeaderDefault(request.HdReq)
        response.ObjBEBK.msg = String.Empty
        Dim IdMovimiento As Long = 0
        Try
            If request.BarCodeObject.CodigoPais.Trim <> WesternUnion.CodBarra.CodPais Then
                With response
                    .ObjBEBK.header.codError = WesternUnion.CodError.ParametrosIncorrectos
                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.CodigoPaisErroneo
                End With
            ElseIf request.BarCodeObject.EntidadServicio <> WesternUnion.CodBarra.CodEntidad Then
                With response
                    .ObjBEBK.header.codError = WesternUnion.CodError.ParametrosIncorrectos
                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.CodigoEntidadErroneo
                End With
            Else
                Dim oBEMovimiento As New BEMovimiento
                With oBEMovimiento
                    .NumeroOrdenPago = request.BarCodeObject.CodigoCliente.Trim.PadLeft(15, "0").Substring(1, 14)
                    .NumeroOperacion = request.HdReq.terminal.Trim & request.HdReq.cajero.Trim & _
                        request.HdReq.fechaHora.Value.ToString("yyyyMMddhhmm") & request.HdReq.nroSecuencia.Trim
                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                    .CodMonedaBanco = request.BarCodeObject.Moneda.Trim
                    .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.BarCodeObject.Importe.ToString.Trim) / 100
                    .CodigoMedioPago = request.BarCodeObject.MedioPago.Trim
                    .CodigoServicio = request.CodigoServicio.ToString.Trim
                    .CodigoAgenciaBancaria = request.HdReq.terminal
                End With
                'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
                'Using Transaccion As New TransactionScope 'Comentado el 06/04
                IdMovimiento = PagarDesdeBancoComun(oBEMovimiento, Nothing, Nothing)
                If IdMovimiento > 0 Then
                    Dim oBEOrdenPago As New BEOrdenPago
                    With oBEOrdenPago
                        .NumeroOrdenPago = request.BarCodeObject.CodigoCliente.Trim.PadLeft(15, "0").Substring(1, 14)
                        .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                        .CodigoServicio = request.CodigoServicio.ToString.Trim
                    End With
                    Using oDAAgenciaBancaria As New DAAgenciaBancaria
                        oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                    End Using
                    With response
                        .ObjBEBK.msg = "PagoEfectivo" & "|" & oBEOrdenPago.ConceptoPago
                        .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionAceptada
                        .CodErrorExtendido = WesternUnion.CodErrorExtendido.RespuestaOk
                    End With
                    'Transaccion.Complete() 'Comentado el 06/04
                Else
                    With response
                        .ObjBEBK.header.codError = WesternUnion.CodError.OperacionInvalida
                        Select Case IdMovimiento
                            Case 0
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.TransaccionFallida
                            Case -8
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPNoExiste
                                .ObjBEBK.header.codError = WesternUnion.CodError.ClienteNoExiste
                            Case -1
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPExpirado
                                .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionFueraDeHorario
                            Case -27
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPEliminado
                            Case -4
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPCancelado
                                .ObjBEBK.header.codError = WesternUnion.CodError.ClaveDuplicada
                            Case -2
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.CodigoMonedaNoCoincide
                            Case -3
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.MontoNoCoincide
                            Case -5
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.NotificacionFallidaRegistroAgencia
                            Case -6
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.NotificacionFallidaUsuario
                            Case -7
                                .CodErrorExtendido = WesternUnion.CodErrorExtendido.NotificacionFallidaPortal
                        End Select
                    End With
                End If
                'End Using 'Comentado el 06/04
            End If
            ListaCodTransaccionWU.TryGetValue(response.ObjBEBK.header.codError, response.ObeLog.Descripcion)
            ListaCodTransaccionWUExtendido.TryGetValue(response.CodErrorExtendido, response.ObeLog.Descripcion2)
        Catch sqlEx As SqlException
            _nlogger.Error(sqlEx, sqlEx.Message)
            ListaCodTransaccionWU.TryGetValue(WesternUnion.CodError.ErrorBD, response.ObeLog.Descripcion)
            response.ObeLog.Descripcion2 = sqlEx.ToString
            response.ObjBEBK.header.codError = WesternUnion.CodError.ErrorBD
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            ListaCodTransaccionWU.TryGetValue(WesternUnion.CodError.ErrorInternoEntidad, response.ObeLog.Descripcion)
            response.ObeLog.Descripcion2 = ex.ToString
            response.ObjBEBK.header.codError = WesternUnion.CodError.ErrorInternoEntidad
        End Try
        response.ObjBEBK.header.codSeveridad = WesternUnion.CodSeveridad.resolverSeveridad(response.ObjBEBK.header.codError)
        Return response
    End Function
    Public Function AnularWU(ByVal request As BEWUAnularRequest) As BEWUAnularResponse
        Dim response As New BEWUAnularResponse()
        response.ObeLog = New BELog()
        response.ObjBEBK = New ReversaResp()
        response.ObjBEBK.header = asignarHeaderDefault(request.HdReq)
        response.ObjBEBK.estado = String.Empty 'no se usa
        response.ObjBEBK.operador = String.Empty 'no se usa
        response.ObjBEBK.ticket = String.Empty 'no se usa
        response.CIP = String.Empty

        Dim IdMovimiento As Integer = 0
        Try
            Dim oBEMovimiento As New BEMovimiento
            Dim oBEOrdenPagoOriginal As New BEOrdenPago
            Dim oDAOrdenPago As New DAOrdenPago
            Dim numeroOperacionPago As String

            If request.TerminalOriginal.Contains(";") Then
                Dim TerminaOriginalSplit As String = request.TerminalOriginal.Split(";")(0)
                Dim NumeroOrdenPagoSplit As String = request.TerminalOriginal.Split(";")(1)

                numeroOperacionPago = TerminaOriginalSplit.Trim & request.CajeroOriginal.Trim & _
                   request.FechaHoraOriginal.ToString("yyyyMMddhhmm") & request.NroSecuenciaOriginal.Trim

                'Se consultan los datos de Anulación de acuerdo al numero de Operación, esto porque Wester Union
                'extorna sus pagos de acuerdo al numero de Movimiento Original, mas no por el codigo de consulta

                oBEOrdenPagoOriginal = oDAOrdenPago.COnsultarOrdenPagoPorNumeroMovimientoYCodigoBancoEnBatch(numeroOperacionPago, Conciliacion.CodigoBancos.WesterUnion, Convert.ToInt64(NumeroOrdenPagoSplit.Trim()))

            Else
                numeroOperacionPago = request.TerminalOriginal.Trim & request.CajeroOriginal.Trim & _
                   request.FechaHoraOriginal.ToString("yyyyMMddhhmm") & request.NroSecuenciaOriginal.Trim

                'Se consultan los datos de Anulación de acuerdo al numero de Operación, esto porque Wester Union
                'extorna sus pagos de acuerdo al numero de Movimiento Original, mas no por el codigo de consulta

                oBEOrdenPagoOriginal = oDAOrdenPago.COnsultarOrdenPagoPorNumeroMovimientoYCodigoBanco(numeroOperacionPago, Conciliacion.CodigoBancos.WesterUnion)
            End If

            If oBEOrdenPagoOriginal IsNot Nothing Then
                response.CIP = oBEOrdenPagoOriginal.NumeroOrdenPago
                With oBEMovimiento
                    .NumeroOrdenPago = oBEOrdenPagoOriginal.NumeroOrdenPago
                    .NumeroOperacion = request.HdReq.terminal.Trim & request.HdReq.cajero.Trim & _
                    request.HdReq.fechaHora.Value.ToString("yyyyMMddhhmm") & request.HdReq.nroSecuencia.Trim
                    .NumeroOperacionPago = numeroOperacionPago
                    .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                    .CodMonedaBanco = oBEOrdenPagoOriginal.codMonedaBanco
                    .Monto = parseInt32ToDoubleWU(request.Amount)
                    .CodigoMedioPago = oBEOrdenPagoOriginal.CodigoMedioPago.ToString
                    .CodigoAgenciaBancaria = request.HdReq.terminal.ToString.Trim
                    .CodigoServicio = request.CodigoServicio.ToString.Trim
                End With
                'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                'Using Transaccion As New TransactionScope(TransactionScopeOption.Required, timeout)
                Using Transaccion As New TransactionScope
                    IdMovimiento = AnularDesdeBancoComun(oBEMovimiento)
                    If IdMovimiento > 0 Then
                        Dim oBEOrdenPago As New BEOrdenPago
                        With oBEOrdenPago
                            .NumeroOrdenPago = oBEOrdenPagoOriginal.NumeroOrdenPago.ToString.Trim
                            .CodigoBanco = EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                            .CodigoServicio = request.CodigoServicio.ToString.Trim
                        End With
                        Using oDAAgenciaBancaria As New DAAgenciaBancaria
                            oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPago)
                        End Using
                        With response
                            .CodErrorExtendido = WesternUnion.CodErrorExtendido.RespuestaOk
                            .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionAceptada
                        End With
                        Transaccion.Complete()
                    Else
                        With response
                            .ObjBEBK.header.codError = WesternUnion.CodError.OperacionInvalida
                            Select Case IdMovimiento
                                Case 0
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.TransaccionFallida
                                Case -11
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.CIPNoExisteONoCancelado
                                    .ObjBEBK.header.codError = WesternUnion.CodError.NoExisteRegistro
                                Case -17
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.MontoOMonedaNoCoincide
                                Case -19
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.CodigoServicioNoCoincide
                                Case -13
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.CodigoAgenciaBancariaONumeroOperacionPagoNoCoincide
                                Case -12
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.MovimientoPagoNoExiste
                                    .ObjBEBK.header.codError = WesternUnion.CodError.NoExisteDirecta
                                Case -14
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.FechaPagoNoActual
                                    .ObjBEBK.header.codError = WesternUnion.CodError.TransaccionFueraDeHorario
                                Case -15
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.NotificacionFallidaPortal
                                Case -37
                                    'TIEMPO MAXIMO DE EXPIRACION
                                    .ObjBEBK.header.codError = WesternUnion.CodError.NoSePermiteExtornar
                                    .CodErrorExtendido = WesternUnion.CodErrorExtendido.NoSePermiteExtornar
                            End Select
                        End With
                    End If
                End Using
            Else
                response.ObjBEBK.header.codError = WesternUnion.CodError.NoExisteDirecta
                response.CodErrorExtendido = WesternUnion.CodErrorExtendido.MovimientoPagoNoExiste
            End If
            ListaCodTransaccionWU.TryGetValue(response.ObjBEBK.header.codError, response.ObeLog.Descripcion)
            ListaCodTransaccionWUExtendido.TryGetValue(response.CodErrorExtendido, response.ObeLog.Descripcion2)
        Catch sqlEx As SqlException
            _nlogger.Error(sqlEx, sqlEx.Message)
            ListaCodTransaccionWU.TryGetValue(WesternUnion.CodError.ErrorBD, response.ObeLog.Descripcion)
            response.ObeLog.Descripcion2 = sqlEx.ToString
            response.ObjBEBK.header.codError = WesternUnion.CodError.ErrorBD
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            ListaCodTransaccionWU.TryGetValue(WesternUnion.CodError.ErrorInternoEntidad, response.ObeLog.Descripcion)
            response.ObeLog.Descripcion2 = ex.ToString
            response.ObjBEBK.header.codError = WesternUnion.CodError.ErrorInternoEntidad
        End Try
        response.ObjBEBK.header.codSeveridad = WesternUnion.CodSeveridad.resolverSeveridad(response.ObjBEBK.header.codError)
        Return response
    End Function
    Private Function asignarHeaderDefault(ByVal request As HeaderReq) As HeaderResp
        Dim response As New HeaderResp
        With response
            .algoritmo = request.algoritmo
            .cajero = request.cajero
            .fechaHora = request.fechaHora
            .idMensaje = request.idMensaje
            .marca = request.marca
            .nroSecuencia = request.nroSecuencia
            .terminal = request.terminal
            .version = request.version
            .codError = WesternUnion.CodError.ErrorInternoEntidad
            .codSeveridad = WesternUnion.CodSeveridad.uno
        End With
        Return response
    End Function
    Public Shared Function parseInt32ToDoubleWU(ByVal valor As Int32) As Double
        Dim valorString As String = valor.ToString()
        Dim doubleString As String = String.Empty
        If valorString.Length > 2 Then
            doubleString = valorString.Substring(0, valorString.Length - 2) + "." + valorString.Substring(valorString.Length - 2, 2)
        Else
            doubleString = "0." + valorString.PadLeft(2, "0")
        End If
        Return Double.Parse(doubleString)
    End Function
#End Region

#Region "Integración BanBif"
    Public Function ConsultarBanBif(ByVal request As BEBanBifConsultarRequest) As BEBanBifResponse(Of BEBanBifConsultarResponse)

        Dim response As New BEBanBifResponse(Of BEBanBifConsultarResponse)
        response.ObjBanBif = New BEBanBifConsultarResponse()

        Dim obeOrdenPagoReq As New BEOrdenPago
        Dim obeOrdenPagoRes As New BEOrdenPago
        Dim TmpRespuesta(33) As String
        Dim obeConsDet As New BEBanBifConsultarDetalle
        Dim obeConsDetSub As New BEBanBifConsultarSubConcepto

        response.ObjBanBif.detalle = New List(Of BEBanBifConsultarDetalle)
        With obeConsDet
            .Recibo = ""         ' NRO. REFER. DOCUMENTO
            .SvcId = ""             ' IMPORTE DEUDA DOCUMENTO
            .Descripcion = ""       ' IMPORTE DEUDA MIN. DOCUMENTO
            .FecVct = ""         ' FECH. VENCIMIENTO DOCUMENTO
            .FecEmision = ""             ' FECH. EMISION  DOCUMENTO
            .Total = 0              ' DESCRIPCION DOCUMENTO
            .Ctd = 0                   ' NRO. DEL DOCUMENTO
            .subConceptos = New List(Of BEBanBifConsultarSubConcepto)
        End With
        response.ObjBanBif.detalle.Add(obeConsDet)

        With obeConsDetSub
            .codigoSubconcepto = ""
            .MtoTp = ""
            .Mto = 0
        End With
        response.ObjBanBif.detalle(0).subConceptos.Add(obeConsDetSub)

        Try
            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBanBif
                .RqUID = request.RqUID ' CODIGO 
                .Login = request.Login ' CODIGO 
                .SegRol = request.SegRol
                .ServFec = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")
                .Spcod = request.Spcod
                .PagId = request.PagId
                .Clasificacion = request.Clasificacion  ' CODIGO DE OPERACION
                .Ctd = 1  ' CODIGO DE OPERACION
                .OpnNro = request.OpnNro
            End With

            response.Estado = 0

            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request

                If .RqUID.Trim = "" OrElse .Login.Trim = "" OrElse .SegRol.Trim = "" OrElse _
                .OpnNro.Trim = "" OrElse .OrgCod.Trim = "" OrElse _
                .ClientCod.Trim = "" OrElse .SvcId.Trim = "" OrElse .Spcod.Trim = "" OrElse _
                .PagId.Trim = "" OrElse .Clasificacion.Trim = "" Then

                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Trama_Invalida

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)

                    response.Mensaje = "Algunos de los campos mandatorios no tienen valor."

                    Return response

                End If

            End With

            '--------------------------------------------------------
            ' 2. INPUT - ORDEN DE PAGO
            '--------------------------------------------------------

            ' 2.1 CIP
            '---------
            obeOrdenPagoReq.NumeroOrdenPago = request.PagId.Trim

            ' 2.2 CODIGO DEL BANCO
            '----------------------
            obeOrdenPagoReq.CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BanBif

            ' 2.2 CODIGO DE CONVENIO
            '----------------------
            obeOrdenPagoReq.CodigoServicio = request.SvcId

            Using odaAgenciaBancaria As New DAAgenciaBancaria()

                '--------------------------------------------------------
                ' 3. CONSULTAR CIP
                '--------------------------------------------------------
                obeOrdenPagoRes = odaAgenciaBancaria.ConsultarCIPDesdeBanco(obeOrdenPagoReq)

                '--------------------------------------------------------
                ' 4. OUTPUT - ORDEN DE PAGO
                '--------------------------------------------------------

                ' 4.1 VALIDAR SI EXISTE ORDEN DE PAGO
                '-------------------------------------
                If obeOrdenPagoRes Is Nothing Then

                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Deuda_No_Valida

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                    response.Mensaje = "Orden de Pago no existe"

                    Return response
                End If


                '-------------------------------------------------------------------
                ' 5. ESTRUCTURAR DETALLE DEL OBJETO "RESPONSE" CON SU PROPIEDADES
                '-------------------------------------------------------------------

                With response.ObjBanBif

                    'Recibo 
                    .detalle(0).Recibo = obeOrdenPagoRes.NumeroOrdenPago
                    'Concepto pago
                    .detalle(0).SvcId = request.SvcId
                    'Descripcion
                    .detalle(0).Descripcion = obeOrdenPagoRes.ConceptoPago
                    ' FECH. VENCIMIENTO DOCUMENTO
                    .detalle(0).FecVct = obeOrdenPagoRes.FechaVencimiento.ToString("yyyy-MM-dd")
                    ' FECH. EMISION  DOCUMENTO
                    .detalle(0).FecEmision = obeOrdenPagoRes.FechaEmision.ToString("yyyy-MM-dd")
                    'Codigo Moneda
                    .detalle(0).MonCod = obeOrdenPagoRes.codMonedaBanco.Trim
                    'IMPORTE DEUDA DOCUMENTO
                    .detalle(0).Total = obeOrdenPagoRes.Total.ToString("#00000000000.00")
                    'Cantidad de importes
                    .detalle(0).Ctd = 1
                    'Nombre Deudor
                    .NomCompleto = obeOrdenPagoRes.DescripcionCliente

                    .detalle(0).subConceptos(0).codigoSubconcepto = ""
                    .detalle(0).subConceptos(0).MtoTp = obeOrdenPagoRes.ConceptoPago
                    .detalle(0).subConceptos(0).Mto = obeOrdenPagoRes.Total.ToString("#00000000000.00")
                End With

                '-------------------------------------------------------------------
                ' 6. CODIGO DE RESULTADO
                '-------------------------------------------------------------------
                If (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Eliminado) Then
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Deuda_No_Valida
                    response.Mensaje = "Orden de Pago esta eliminado"

                ElseIf (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada) Then
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Sin_Deuda_Pendiente
                    response.Mensaje = "Orden de Pago ya esta cancelado"

                ElseIf ((obeOrdenPagoRes.CipListoParaExpirar) OrElse _
                (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Limite_Pago_Superado
                    response.Mensaje = "Orden de Pago ha expirado"
                Else
                    ' EXITO
                    response.ObjBanBif.Ctd = 1   ' CANT. DE DOCUMENTOS "1"
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Informacion
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Proceso_Conforme
                    response.Estado = 1
                    response.Mensaje = "Transaccion exitosa"

                End If
                ' MENSAJE RESULTADO
                response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Estado = -1
            response.Mensaje = ex.Message
            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

        End Try

        Return response

    End Function

    Public Function PagarBanBif(ByVal request As BEBanBifPagarRequest) As BEBanBifResponse(Of BEBanBifPagarResponse)

        Dim response As New BEBanBifResponse(Of BEBanBifPagarResponse)
        response.ObjBanBif = New BEBanBifPagarResponse()

        Dim obeMovimiento As New BEMovimiento
        Dim IdMovimiento As Integer = 0

        Try

            '--------------------------------------------------------
            ' 0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBanBif
                .ExtornoTp = request.ExtornoTp.Trim  ' CODIGO DE OPERACION
                .RqUID = request.RqUID.Trim  ' NUMERO DE OPERACION
                .Login = request.Login.Trim          ' CODIGO DE BANCO
                .SegRol = request.SegRol.Trim    ' CODIGO DE CONVENIO
                .ServFec = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")
                .Ctd = request.Ctd.Trim      ' CODIGO DEL CLIENTE
                .SvcId = request.SvcId.Trim  ' NRO. REFERENCIA DEUDA
                .Spcod = request.Spcod.Trim    'REFER. DEUDA ADICIONAL
                .PagId = request.PagId.Trim        ' DATOS DE LA EMPRESA
                .OpnNro = request.OpnNro   ' NRO. DE OPERAC. DE LA EMPRESA
                .Recibo = request.Recibo
                .Clasificacion = request.Clasificacion
            End With

            response.Estado = 0

            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request

                If .Recibo.Trim = "" OrElse _
                    .OpnNro.Trim = "" OrElse .Login.Trim = "" OrElse _
                    .SegRol.Trim = "" OrElse _
                    .OrgCod.Trim = "" OrElse .ClientCod.Trim = "" OrElse _
                    .SvcId.Trim = "" OrElse .Spcod.Trim = "" OrElse _
                    .PagId.Trim = "" OrElse .MedioAbonoCod.Trim = "" OrElse _
                    .Moneda.Trim = "" OrElse .Mto = 0 OrElse Not .ExtornoTp.Equals("P") Then

                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Trama_Invalida

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)

                    response.Mensaje = "Algunos de los campos mandatorios no presentan valor."

                    Return response

                End If

            End With

            '--------------------------------------------------------
            ' 2. INPUT - MOVIMIENTO
            '--------------------------------------------------------
            With obeMovimiento

                .NumeroOrdenPago = request.PagId.Trim 'Nro. Orden Pago
                '.NumeroOrdenPago = request.Recibo.Trim 'Nro. Orden Pago
                .CodMonedaBanco = request.Moneda.Trim 'Codigo Moneda
                .NumeroOperacion = request.OpnNro.Trim 'Nro. Operacion del Banco ConsultarDuda
                .CodigoAgenciaBancaria = request.OrgCod.Trim ' Cod. Agencia Bancaria
                .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BanBif 'Cod. Banco
                .CodMonedaBanco = request.Moneda.Trim
                .CodigoMedioPago = request.MedioAbonoCod.Trim
                .CodigoServicio = request.SvcId
                'Agregado codigocanal
                .CodigoCanal = request.ClientCod
                'conversion del monto
                '.Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Mto) 'Importe
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Mto) 'Importe

            End With

            '--------------------------------------------------------
            ' 3. PROCESO DE PAGO POR BANCO "BBVA"
            '--------------------------------------------------------

            IdMovimiento = PagarDesdeBancoComun(obeMovimiento, Nothing, Nothing)

            If IdMovimiento > 0 Then
                response.ObjBanBif.Moneda = request.Moneda.Trim
                response.ObjBanBif.Mto = request.Mto
                response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Informacion
                response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Proceso_Conforme

                response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)

                response.Estado = 1
                response.Mensaje = "Transaccion exitosa"

                'transaccionscope.Complete() 'Comentado el 06/04
            Else
                If IdMovimiento = -1 Then
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Limite_Pago_Superado
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                    response.Mensaje = ListaCodValidacion(IdMovimiento)

                Else
                    If IdMovimiento = -3 Or IdMovimiento = -2 Then
                        response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Monto_Pagar_errado
                        response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                        response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                        response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    Else
                        If IdMovimiento = -4 Then
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Sin_Deuda_Pendiente
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)

                        Else
                            If IdMovimiento = -8 Then
                                response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Deuda_No_Valida
                                response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                                response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                                response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                response.Mensaje = ListaCodValidacion(IdMovimiento)
                            Else
                                response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.No_Procede_Pago_Entidad
                                response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                                response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                                response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                response.Mensaje = ListaCodValidacion(IdMovimiento)
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)

            response.Estado = -1
            response.Mensaje = ex.Message

            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

        End Try

        Return response

    End Function

    Public Function ExtornarBanBif(ByVal request As BEBanBifExtornoRequest) As BEBanBifResponse(Of BEBanBifExtornoResponse)
        Dim response As New BEBanBifResponse(Of BEBanBifExtornoResponse)
        Dim obeMovimiento As New BEMovimiento
        Dim IdMovimiento As Integer = 0

        Try
            response.ObjBanBif = New BEBanBifExtornoResponse

            '--------------------------------------------------------
            ' 0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response.ObjBanBif
                .ExtornoTp = request.ExtornoTp.Trim  ' CODIGO DE OPERACION
                .RqUID = request.RqUID.Trim  ' NUMERO DE OPERACION
                .Login = request.Login.Trim          ' CODIGO DE BANCO
                .SegRol = request.SegRol.Trim    ' CODIGO DE CONVENIO
                '.ServFec = request.ClientFec.Trim          ' TIPO DE CLIENTE
                .ServFec = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")
                .Ctd = request.Ctd.Trim      ' CODIGO DEL CLIENTE
                .SvcId = request.SvcId.Trim  ' NRO. REFERENCIA DEUDA
                .Spcod = request.Spcod.Trim    'REFER. DEUDA ADICIONAL
                .PagId = request.PagId.Trim        ' DATOS DE LA EMPRESA
                .OpnNro = request.OpnNro   ' NRO. DE OPERAC. DE LA EMPRESA
                .Recibo = request.Recibo
                .Clasificacion = request.Clasificacion
            End With
            response.Estado = 0

            '--------------------------------------------------------
            '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
            '--------------------------------------------------------
            With request
                If .Recibo.Trim = "" OrElse _
                   .OpnNro.Trim = "" OrElse _
                   .OpnNro.Trim = "" OrElse _
                   .Login.Trim = "" OrElse .SegRol.Trim = "" OrElse _
                   .OrgCod.Trim = "" OrElse .ClientCod.Trim = "" OrElse _
                   .SvcId.Trim = "" OrElse .Spcod.Trim = "" OrElse _
                   .PagId.Trim = "" OrElse _
                   .Moneda.Trim = "" OrElse .Mto = 0 OrElse Not (.ExtornoTp.Equals("E") Or .ExtornoTp.Equals("A")) Then

                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Trama_Invalida

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)

                    response.Mensaje = "Algunos de los campos mandatorios no presentan valor."
                    Return response
                End If
            End With

            '--------------------------------------------------------
            ' 2. INPUT - MOVIMIENTO
            '--------------------------------------------------------
            With obeMovimiento

                .NumeroOrdenPago = request.PagId.Trim 'Nro. Orden Pago
                '.NumeroOrdenPago = request.Recibo.Trim 'Nro. Orden Pago
                .NumeroOperacion = request.RqUID.Trim 'Nro. Operacion del Banco
                .NumeroOperacionPago = request.OpnNro.Trim 'Nro. Operacion a Anular
                .CodigoAgenciaBancaria = request.OrgCod.Trim ' Cod. Agencia Bancaria

                '.Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Mto) 'Importe
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Mto)  'Importe
                .CodMonedaBanco = request.Moneda.Trim 'Codigo Moneda
                .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BanBif 'Cod. Banco
                .CodigoServicio = request.SvcId

            End With

            If request.PagId.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                'request.Recibo.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                '--------------------------------------------------------
                ' 3. PROCESO DE ANULACION DEL BANCO "BBVA"
                '--------------------------------------------------------
                'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                'Using oTransactionScope As New TransactionScope(TransactionScopeOption.Required, timeout)

                'Using oTransactionScope As New TransactionScope

                IdMovimiento = ExtornarDesdeBancoComun(obeMovimiento)

                If IdMovimiento > 0 Then
                    'EXTORNO CORRECTO

                    response.ObjBanBif.Moneda = request.Moneda.Trim
                    response.ObjBanBif.Mto = request.Mto
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Informacion
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Proceso_Conforme

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)

                    response.Estado = 1
                    response.Mensaje = "Transaccion exitosa"

                    'oTransactionScope.Complete()

                ElseIf IdMovimiento = 0 Then
                    'EXTORNO SIN EXITO
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Errores
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.No_Procede_Extorno_Entidad

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                    response.Mensaje = ListaCodValidacion(IdMovimiento) + " en el proceso de extorno"

                ElseIf IdMovimiento = -37 Then
                    'TIEMPO MAXIMO DE EXTORNO
                    response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros
                    response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.NoSePermiteExtornar

                    response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                    response.Mensaje = ListaCodValidacion(IdMovimiento)
                Else

                    Dim oBEOrdenPagoReq As New BEOrdenPago
                    With oBEOrdenPagoReq
                        .NumeroOrdenPago = obeMovimiento.NumeroOrdenPago
                        .CodigoBanco = obeMovimiento.CodigoBanco
                        .CodigoServicio = obeMovimiento.CodigoServicio
                    End With
                    Using oDAAgenciaBancaria As New DAAgenciaBancaria()
                        Dim oBEOrdenPagoRes As BEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(oBEOrdenPagoReq)
                        '.Agregar validacion para indicar q el cip esta en proceso de pago
                        If oBEOrdenPagoRes Is Nothing Then
                            'CIP no existe
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Deuda_No_Valida
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)
                        ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Eliminado Then
                            'CIP está eliminado
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Deuda_No_Valida
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)

                        ElseIf oBEOrdenPagoRes.IdEstado = EstadoOrdenPago.Expirado Or _
                            oBEOrdenPagoRes.CipListoParaExpirar Then
                            'CIP ha expirado
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Limite_Pago_Superado
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)
                        ElseIf oBEOrdenPagoRes.codMonedaBanco <> obeMovimiento.CodMonedaBanco Then
                            'Código moneda de entrada es correcto
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Monto_Pagar_errado
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)
                        ElseIf oBEOrdenPagoRes.Total <> obeMovimiento.Monto Then
                            'Monto de entrada es corecto
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.Monto_Pagar_errado
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)
                        Else
                            response.ObjBanBif.ServStatusCod = BanBif.CodigosRespuesta.No_Procede_Pago_Entidad
                            response.ObjBanBif.StatusCod = BanBif.TipoRespuesta.Advertencia_Otros

                            response.ObjBanBif.Severidad = ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                            response.ObjBanBif.StatusDes = ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                            response.Mensaje = ListaCodValidacion(IdMovimiento)
                        End If
                    End Using


                End If
            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            response.Estado = -1
            response.Mensaje = ex.Message
            Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)
        End Try

        Return response
    End Function

#End Region

#Region "Integración Kasnet"

    Public Function ConsultarKasnet(ByVal request As BEKasnetConsultarRequest) As BEKasnetResponse(Of BEKasnetConsultarResponse)

        Dim response As New BEKasnetResponse(Of BEKasnetConsultarResponse)
        response.ObjKasnet = New BEKasnetConsultarResponse()

        Dim obeOrdenPagoReq As New BEOrdenPago
        Dim obeOrdenPagoRes As New BEOrdenPago
        Dim obeConsDet As New BEKasnetConsultarDetalle
        Dim TmpRespuesta(33) As String

        response.ObjKasnet.DetDocumentos = New List(Of BEKasnetConsultarDetalle)

        With obeConsDet
            .NroRecibo = ""             ' Código Identificador de Pago (CIP)
            .TotalPagar = 0            ' El Monto a pagar más la mora aplicada
            .MontoPagar = 0            ' Monto del recibo pendiente a Pagar
            .MoraPagar = 0             ' Cantidad a pagar por el concepto de mora
            .CodMoneda = 0             ' Soles (1) - Dolares (2)
            .MontoTipoCambio = 0       ' Tipo de Cambio de la Moneda
            .MontoConvertido = 0       ' Total a Pagar por Tipo de Cambio
            .FecEmision = ""            ' Fecha de generación del Recibo a pagar
            .FecVencimiento = ""        ' Fecha de Vencimiento del Recibo a pagar
            .ConceptoPago = ""          ' Descripción del recibo a pagar
            .CodConvenio = ""           ' Código que Identifica el producto



        End With

        response.ObjKasnet.DetDocumentos.Add(obeConsDet)

        If request.CodOrdenPago.Trim <> "" Then

            Try
                If request.CodOrdenPago.Substring(0, 2) <> "99" Then


                    '--------------------------------------------------------
                    '0. PARAMETROS DE ENTRADA Y SALIDA
                    '--------------------------------------------------------
                    With response.ObjKasnet

                        .CodPuntoVenta = request.CodPuntoVenta.Trim  ' Código de punto de venta del establecimiento
                        .NroTerminal = request.NroTerminal.Trim  ' Número que identifica al Terminal POS
                        .NroOperacionConsulta = request.NroOperacionConsulta.Trim   ' Número de operación de la consulta generada por el proveedor
                        .CodOrdenPago = request.CodOrdenPago.Trim    ' Código Identificador de Pago (CIP)
                        .CantDocumentos = 1 'Por ahora sera 1 a 1

                    End With

                    response.Estado = 0

                    '--------------------------------------------------------
                    '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
                    '--------------------------------------------------------
                    With request

                        If .CodPuntoVenta.Trim = "" OrElse .NroTerminal.Trim = "" OrElse .NroOperacionConsulta.Trim = "" OrElse _
                        .CodOrdenPago.Trim = "" OrElse .CodConvenio.Trim = "" OrElse .CodCanal = 0 OrElse _
                        .CodUbigeo.Trim = "" OrElse .FecConsulta.Trim = "" Then

                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
                            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                            response.Mensaje = "Algunos de los campos mandatorios no tienen valor."

                            Return response

                        End If

                    End With

                    '--------------------------------------------------------
                    ' 2. INPUT - ORDEN DE PAGO
                    '--------------------------------------------------------

                    ' 2.1 CIP
                    '---------
                    obeOrdenPagoReq.NumeroOrdenPago = request.CodOrdenPago.Trim

                    ' 2.2 CODIGO DEL BANCO
                    '----------------------
                    obeOrdenPagoReq.CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Kasnet

                    ' 2.2 CODIGO DE CONVENIO
                    '----------------------
                    obeOrdenPagoReq.CodigoServicio = request.CodConvenio

                    Using odaAgenciaBancaria As New DAAgenciaBancaria()

                        '--------------------------------------------------------
                        ' 3. CONSULTAR CIP
                        '--------------------------------------------------------
                        obeOrdenPagoRes = odaAgenciaBancaria.ConsultarCIPDesdeBanco(obeOrdenPagoReq)

                        '--------------------------------------------------------
                        ' 4. OUTPUT - ORDEN DE PAGO
                        '--------------------------------------------------------

                        ' 4.1 VALIDAR SI EXISTE ORDEN DE PAGO
                        '-------------------------------------
                        If obeOrdenPagoRes Is Nothing Then
                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoPagoNoExiste
                            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                            response.Mensaje = "Codigo de Pago No Existe"

                            Return response
                        End If

                        '-------------------------------------------------------------------
                        ' 5. ESTRUCTURAR DETALLE DEL OBJETO "RESPONSE" CON SU PROPIEDADES
                        '-------------------------------------------------------------------

                        With response.ObjKasnet

                            .DetDocumentos(0).CodMoneda = CInt(obeOrdenPagoRes.codMonedaBanco.Trim)   ' COD. DE LA MONEDA
                            .UsuarioPagador = obeOrdenPagoRes.DescripcionCliente.Trim ' dato del cliente

                            .CodOrdenPago = obeOrdenPagoRes.NumeroOrdenPago          ' NRO. REFER. DOCUMENTO
                            .FecRespuesta = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss")
                            .DetDocumentos(0).TotalPagar = obeOrdenPagoRes.Total       ' IMPORTE DEUDA DOCUMENTO
                            .DetDocumentos(0).MontoPagar = obeOrdenPagoRes.Total 'ToString("#00000000000.00").Replace(".", "")       ' IMPORTE DEUDA DOCUMENTO
                            '.DetDocumentos(0).Mora = obeOrdenPagoRes.Total.ToString("#00000000000.00").Replace(".", "") ' MORA
                            '.DetDocumentos(0).TipoCambio = obeOrdenPagoRes.Total.ToString("#00000000000.00").Replace(".", "")       ' TIPO CAMBIO
                            '.DetDocumentos(0).ValorConvertido = obeOrdenPagoRes.Total.ToString("#00000000000.00").Replace(".", "")       ' VALOR CONVERTIDO
                            ' FECH. VENCIMIENTO DOCUMENTO
                            .DetDocumentos(0).FecVencimiento = obeOrdenPagoRes.FechaVencimiento.ToString("dd/MM/yyyy HH:MM:ss")
                            ' FECH. EMISION  DOCUMENTO
                            .DetDocumentos(0).FecEmision = obeOrdenPagoRes.FechaEmision.ToString("dd/MM/yyyy HH:MM:ss")
                            ' DESCRIPCION DOCUMENTO
                            .DetDocumentos(0).ConceptoPago = obeOrdenPagoRes.ConceptoPago
                            ' NRO. DEL DOCUMENTO
                            .DetDocumentos(0).NroRecibo = obeOrdenPagoRes.NumeroOrdenPago



                        End With

                        'response.ObjBBVA.cantidadDocumentos = "00"   ' CANT. DE DOCUMENTOS "0"
                        response.ObjKasnet.DetDocumentos(0).CodConvenio = "0001"
                        '-------------------------------------------------------------------
                        ' 6. CODIGO DE RESULTADO
                        '-------------------------------------------------------------------
                        If (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Eliminado) Then
                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoEliminado
                            response.Mensaje = "Codigo con Estado Eliminado"

                        ElseIf (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada) Then
                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoPagado
                            response.Mensaje = "Codigo con Estado Pagado"

                        ElseIf ((obeOrdenPagoRes.CipListoParaExpirar) OrElse _
                        (obeOrdenPagoRes.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoExpirado
                            response.Mensaje = "Codigo con Estado Expirado"

                        Else
                            ' EXITO
                            response.ObjKasnet.CantDocumentos = "01"   ' CANT. DE DOCUMENTOS "1"
                            response.ObjKasnet.DetDocumentos(0).CodConvenio = "0001"

                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.TransaccionRealizadaExito
                            response.Estado = 1
                            response.Mensaje = "Transaccion Realizada con exito"

                        End If

                        ' MENSAJE RESULTADO
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                    End Using
                    'Else ' Se busca dentro de las cuentas de dinero virtual
                    '    Dim dv As New DADineroVirtual()
                    '    Dim cuenta As New BECuentaDineroVirtual()
                    '    Dim CdBco As String
                    '    CdBco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Kasnet
                    '    cuenta.Numero = request.CodOrdenPago.ToString()
                    '    ' Validar Cuenta
                    '    cuenta = dv.ConsultarCuentaDineroByNumeroWS(cuenta, request.codigoConvenio, CdBco)
                    '    '----------------------------------------------------------
                    '    If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    '        'response.ObjBBVA.mensajeResultado = "La cuenta no existe, o el servicio no coincide"
                    '        response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '        response.Mensaje = "La cuenta no existe, o el servicio no coincide."
                    '    Else
                    '        'If cuenta.EquivalenciaBancaria = "00" & request.codigocliente Then 'existe la cuenta en la moneda solicitada
                    '        'Jow Jow
                    '        'valida que tenga activo el monedero y la cuenta
                    '        'If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                    '        If cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                    '            With response.ObjKasnet

                    '                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                    '                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                    '                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                    '                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                    '                .tipoCliente = request.tipocliente.Trim          ' TIPO DE CLIENTE
                    '                .codigoCliente = request.codigocliente.Trim      ' CODIGO DEL CLIENTE
                    '                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim      ' NRO. REFERENCIA DEUDA
                    '                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim 'REFER. DEUDA ADICIONAL
                    '                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
                    '                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA
                    '                .datoCliente = ""                           ' DATO DEL CLIENTE (opcional)
                    '                .cantidadDocumentos = "00"                   ' CANT. DE DOCUMENTOS "0"
                    '                '.codigoMoneda = cuenta.MonedaSimbolo
                    '                Select Case cuenta.EquivalenciaBancaria
                    '                    Case "001"
                    '                        .codigoMoneda = "PEN"
                    '                    Case "002"
                    '                        .codigoMoneda = "USD"
                    '                End Select
                    '                .datoCliente = cuenta.ClienteNombre


                    '                .detalle(0).numeroReferenciaDocumento = request.numeroReferenciaDeuda.Trim
                    '                .detalle(0).importeDeudaDocumento = cuenta.MontoRecarga.ToString("#00000000000.00").Replace(".", "")
                    '                .detalle(0).importeDeudaMinimaDocumento = cuenta.MontoRecarga.ToString("#00000000000.00").Replace(".", "")
                    '                .detalle(0).fechaVencimientoDocumento = cuenta.FechaAprobacion.ToString("yyyy-MM-dd")
                    '                .detalle(0).fechaEmisionDocumento = cuenta.FechaAprobacion.ToString("yyyy-MM-dd")
                    '                .detalle(0).descripcionDocumento = "Cuenta Encontrada"
                    '                .detalle(0).numeroDocumento = "000"
                    '                .detalle(0).indicadorRestriccPago = ""
                    '                .detalle(0).indicadorSituacionDocumento = ""
                    '                .detalle(0).cantidadSubconceptos = "00"

                    '                .CodMensaje = kasnet.Mensaje.TransaccionRealizadaExito
                    '                response.Estado = 1
                    '                response.Mensaje = "Cuenta Encontrada"
                    '                .Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '            End With
                    '        Else
                    '            'response.ObjBBVA.mensajeResultado = "Cuenta no habilitada o usuario no habilitado para dinero virtual"
                    '            response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '            response.Mensaje = "Cuenta no habilitada o usuario no habilitado para dinero virtual."
                    '        End If
                    '    End If
                End If
            Catch ex As Exception
                _nlogger.Error(ex, ex.Message)

                response.Estado = -1
                response.Mensaje = ex.Message
                Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

            End Try

        Else
            response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

            response.Mensaje = "Trama Enviada Invalida"
        End If

        Return response

    End Function

    Public Function PagarKasnet(ByVal request As BEKasnetPagarRequest) As BEKasnetResponse(Of BEKasnetPagarResponse)

        Dim response As New BEKasnetResponse(Of BEKasnetPagarResponse)
        response.ObjKasnet = New BEKasnetPagarResponse()

        Dim obeMovimiento As New BEMovimiento
        Dim resDetalle As BEKasnetPagarDetalle
        Dim IdMovimiento As Integer = 0
        Dim cadena As String = request.FecPago
        Dim FechaPago As String = ""
        Dim Separador() As String = {"/", " "}
        Dim FecPago() As String = cadena.Split(Separador, StringSplitOptions.RemoveEmptyEntries)
        resDetalle = request.DetDocumentos(0)



        If (request.CodOrdenPago.Trim <> "" And FecPago(1) <= 12) Then


            For Each s As String In FecPago
                If (s <> FecPago(3)) Then
                    cadena = s
                    FechaPago = FechaPago + cadena
                End If
            Next

            Try
                If request.CodOrdenPago.Substring(0, 2) <> "99" Then 'BUSCANDO DENTRO DE LOS CIPS


                    '--------------------------------------------------------
                    ' 0. PARAMETROS DE ENTRADA Y SALIDA
                    '--------------------------------------------------------
                    With response.ObjKasnet
                        .CodPuntoVenta = request.CodPuntoVenta.Trim         ' CODIGO DE PUNTO DE VENTA DEL ESTABLECIMIENTO
                        .NroTerminal = request.NroTerminal.Trim             ' NUMERO QUE IDENTIFICA AL TERMINAL POS
                        .NroOperacionPago = request.NroOperacionPago.Trim   ' NUMERO DE OPERACION DEL PAGO GENERADO POR EL PROVEEDOR
                        .CodOrdenPago = request.CodOrdenPago.Trim           ' CODIGO IDENTIFICADOR DE PAGO (CIP)
                        .CantDocumentos = request.CantDocumentos            ' CANTIDAD DE DOCUMENTOS PENDIENTES DE PAGO
                        .FecRespuesta = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss")

                    End With

                    response.Estado = 0

                    '--------------------------------------------------------
                    '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
                    '--------------------------------------------------------
                    With request

                        If .CodPuntoVenta.Trim = "" OrElse _
                            .NroTerminal.Trim = "" OrElse _
                            .NroOperacionPago.Trim = "" OrElse .CodOrdenPago.Trim = "" OrElse .FecPago.Trim = "" OrElse .CodConvenio.Trim = "" OrElse _
                            .CodCanal.ToString.Trim = "" OrElse .CodUbigeo.Trim = "" OrElse .CodMedioPago.ToString.Trim = "" OrElse _
                            .CantDocumentos.ToString.Trim = "" OrElse .CodOrdenPago.Trim = "" OrElse .DetDocumentos(0).NroRecibo.Trim = "" OrElse .DetDocumentos(0).TotalPagar.ToString.Trim = "" _
                            OrElse .DetDocumentos(0).MontoPagar.ToString.Trim = "" OrElse .DetDocumentos(0).MoraPagar.ToString.Trim = "" OrElse .DetDocumentos(0).CodMoneda.ToString.Trim = "" _
                            OrElse .DetDocumentos(0).FecEmision.Trim = "" OrElse .DetDocumentos(0).FecVencimiento.Trim = "" OrElse .DetDocumentos(0).CodConvenio.Trim = "" Then

                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
                            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                            'estado y mensaje para el registro de log                    
                            response.Mensaje = "Algunos de los campos mandatorios no presentan valor."

                            Return response

                        End If

                    End With

                    '--------------------------------------------------------
                    ' 2. INPUT - MOVIMIENTO
                    '--------------------------------------------------------
                    With obeMovimiento

                        .NumeroOrdenPago = request.CodOrdenPago.Trim                  ' Nro. Orden Pago
                        .CodMonedaBanco = request.DetDocumentos(0).CodMoneda.ToString ' MONEDA DEL MONTO A PAGAR
                        .NumeroSerieTerminal = request.NroTerminal.Trim
                        .NumeroOperacion = request.CodPuntoVenta.Trim & request.NroTerminal.Trim & request.NroOperacionPago.Trim & FechaPago 'CDate(request.FecPago).ToString("ddMMyyyy")  ' Nro. DE OPERACION DEL PAGO GENERADO POR EL PROVEEDOR
                        .CodigoPuntoVenta = request.CodPuntoVenta.Trim                ' Cod. DE PUNTO DE VENTA DEL ESTABLECIMIENTO
                        .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Kasnet 'Cod. Banco
                        .CodigoMedioPago = request.CodMedioPago.ToString              ' CODIGO DE MEDIO DE PAGO, FORMA DE PAGO
                        .CodigoServicio = request.CodConvenio                         ' CODIGO QUE IDENTIFICA EL PRODUCTO

                        'conversion del monto
                        .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.DetDocumentos(0).TotalPagar) 'Importe

                    End With

                    '--------------------------------------------------------
                    ' 3. PROCESO DE PAGO POR BANCO "BBVA"
                    '--------------------------------------------------------

                    'Dim transOptions As New TransactionOptions()
                    'transOptions.IsolationLevel = IsolationLevel.ReadCommitted

                    ' Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))
                    'Using transaccionscope As New TransactionScope()
                    ' Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)

                    'Using transaccionscope As New TransactionScope() 'Comentado el 06/04

                    IdMovimiento = PagarDesdeBancoComun(obeMovimiento, Nothing, Nothing)

                    If IdMovimiento > 0 Then

                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.TransaccionRealizadaExito
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.ObjKasnet.CodOperacionEmpresa = IdMovimiento.ToString

                        response.Estado = 1
                        response.Mensaje = "Transaccion exitosa"

                        'transaccionscope.Complete() 'Comentado el 06/04

                    ElseIf (IdMovimiento = -8) Then

                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoPagoNoExiste
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf (IdMovimiento = -27) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoEliminado
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf (IdMovimiento = -4) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoPagado
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf (IdMovimiento = -1) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoExpirado
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)

                    ElseIf (IdMovimiento = -3) Then


                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.MontoOMonedaNoCoincide
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    Else
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)

                    End If

                    'End Using 'Comentado el 06/04
                    'Else 'RECARGANDO DINERO A UNA CUENTA

                    '    With response.ObjKasnet
                    '        .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                    '        .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                    '        .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                    '        .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                    '        .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                    '        .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                    '        .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim  ' NRO. REFERENCIA DEUDA
                    '        .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim    'REFER. DEUDA ADICIONAL
                    '        .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
                    '        .numeroOperacionEmpresa = "0"   ' NRO. DE OPERAC. DE LA EMPRESA
                    '    End With

                    '    With obeMovimiento

                    '        .NumeroOrdenPago = request.numeroReferenciaDeuda.Trim 'Nro. Orden Pago
                    '        .CodMonedaBanco = request.codigoMoneda.Trim 'Codigo Moneda

                    '        .NumeroOperacion = request.numeroOperacion.Trim 'Nro. Operacion del Banco
                    '        .CodigoAgenciaBancaria = request.codigoOficina.Trim ' Cod. Agencia Bancaria
                    '        .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA 'Cod. Banco
                    '        .CodMonedaBanco = request.codigoMoneda.Trim
                    '        .CodigoMedioPago = request.formaPago.Trim
                    '        .CodigoServicio = request.codigoConvenio
                    '        .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.importeTotalPagado.Trim) / 100 'Importe

                    '    End With

                    '    Dim dv As New DADineroVirtual()
                    '    Dim cuenta As New BECuentaDineroVirtual()
                    '    Dim bldv As New BLDineroVirtual()
                    '    cuenta.Numero = request.numeroReferenciaDeuda
                    '    ' Validar Cuenta
                    '    cuenta = dv.ConsultarCuentaDineroVirtualUsuarioByNumero(cuenta)



                    '    If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    '        'BCP.Mensaje.Cuenta_no_encontrada()
                    '        Throw New Exception("Cuenta_no_encontrada")
                    '    Else
                    '        Dim EquiBanc As String = String.Empty
                    '        Select Case request.codigoMoneda
                    '            Case "PEN"
                    '                EquiBanc = "001"
                    '            Case "USD"
                    '                EquiBanc = "002"
                    '        End Select
                    '        If cuenta.EquivalenciaBancaria = EquiBanc Then 'obeMovimiento.MonedaAgencia  existe la cuenta en la moneda solicitada


                    '            'valida que tenga activo el monedero y la cuenta
                    '            If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                    '                'Valida si el monto a Recargar es el mismo establecido por el Cliente

                    '                If cuenta.MontoRecarga = obeMovimiento.Monto Then
                    '                    'Dim IdTransaccion As Long = dv.RecargarCuenta(cuenta, request.codigoConvenio, New BEMovimiento) 'Aqui
                    '                    Dim Respuesta As Long = bldv.OperacionCuentaGenerico(cuenta, obeMovimiento, request.codigoBanco)

                    '                    If Respuesta <> -1 Then
                    '                        If Respuesta <> -2 Then
                    '                            If Respuesta > 0 Then
                    '                                'Enviar Correo
                    '                                Dim control As New BLEmail
                    '                                control.EnviarCorreoCuentaRecarga(cuenta)
                    '                                response.ObjKasnet.CodMensaje = kasnet.Mensaje.TransaccionRealizadaExito
                    '                                response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '                                response.ObjKasnet.CodOperEmpresa = Respuesta

                    '                                response.Estado = 1
                    '                                response.Mensaje = "Transaccion exitosa"
                    '                            Else
                    '                                response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '                                response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '                                response.Mensaje = "No se pudo Realizar la transacción"
                    '                            End If
                    '                        Else
                    '                            'Throw New Exception("El monto ingresado está fuera de los rangos establecidos por el Administrador.")
                    '                            response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '                            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '                            response.Mensaje = "El monto ingresado está fuera de los rangos establecidos por el Administrador."
                    '                        End If
                    '                    Else
                    '                        'Throw New Exception("El codigo de servicio BancoMonedero no coincide.")
                    '                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '                        response.Mensaje = "El codigo de servicio BancoMonedero no coincide."
                    '                    End If
                    '                Else
                    '                    'Throw New Exception("El Monto no coincide con el especificado para la cuenta.")
                    '                    response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '                    response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '                    response.Mensaje = "El Monto no coincide con el especificado para la cuenta."
                    '                End If
                    '            Else
                    '                'Throw New Exception("La cuenta no esta activa")
                    '                response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '                response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '                response.Mensaje = "La cuenta no esta activa."
                    '            End If
                    '        Else
                    '            'Throw New Exception("Moneda no encontrada para la cuenta")
                    '            response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                    '            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                    '            response.Mensaje = "Moneda no encontrada para la cuenta."
                    '        End If
                    '    End If
                End If

            Catch ex As Exception
                _nlogger.Error(ex, ex.Message)

                response.Estado = -1
                response.Mensaje = ex.Message

                Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

            End Try
        Else
            response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

            response.Mensaje = ListaCodValidacion(IdMovimiento)
        End If


        Return response

    End Function

    Public Function ExtornarKasnet(ByVal request As BEKasnetExtornarRequest) As BEKasnetResponse(Of BEKasnetExtornarResponse)
        Dim response As New BEKasnetResponse(Of BEKasnetExtornarResponse)
        Dim obeMovimiento As New BEMovimiento
        Dim IdMovimiento As Integer = 0
        Dim cadena As String = request.FecPago
        Dim FechaPago As String = ""
        Dim Separador() As String = {"/", " "}
        Dim FecPago() As String = cadena.Split(Separador, StringSplitOptions.RemoveEmptyEntries)

        If (FecPago(1) <= 12) Then
            For Each s As String In FecPago
                If (s <> FecPago(3)) Then
                    cadena = s
                    FechaPago = FechaPago + cadena
                End If
            Next

            Try
                response.ObjKasnet = New BEKasnetExtornarResponse

                '--------------------------------------------------------
                ' 0. PARAMETROS DE ENTRADA Y SALIDA
                '--------------------------------------------------------
                With response.ObjKasnet
                    .CodPuntoVenta = request.CodPuntoVenta.Trim             ' CODIGO DE PUNTO DE VENTA DE ESTABLECIMIENTO
                    .NroTerminal = request.NroTerminal.Trim                 ' NUMERO QUE IDENTIFICA AL TERMINAL POS
                    .NroOperacionExtorno = request.NroOperacionExtorno.Trim ' NUMERO DE OPERACION DE EXTORNO GENERADO POR EL PROVEEDOR
                    .CodOrdenPago = request.CodOrdenPago.Trim               ' CODIGO IDENTIFICADOR DE PAGO (CIP)
                    .CantDocumentos = request.CantDocumentos                ' CANTIDAD DE DOCUMENTOS A EXTORNAR
                    .FecRespuesta = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss")
                End With

                response.Estado = 0
                '--------------------------------------------------------
                '1. VERIFICAR QUE LOS CAMPOS MANDATORIOS PRESENTAR VALOR
                '--------------------------------------------------------
                With request

                    If .CodPuntoVenta.Trim = "" OrElse _
                        .NroTerminal.Trim = "" OrElse _
                        .NroOperacionPago.Trim = "" OrElse .NroOperacionExtorno.Trim = "" OrElse .FecPago.Trim = "" OrElse .FecExtorno.Trim = "" OrElse .TipoExtorno.ToString = "" OrElse _
                        .CodOrdenPago.Trim = "" OrElse .CodConvenio.Trim = "" OrElse .CodMoneda.ToString.Trim = "" OrElse _
                        .CodCanal.ToString.Trim = "" OrElse .CodMedioPago.ToString.Trim = "" OrElse _
                        .CantDocumentos.ToString.Trim = "" OrElse .DetDocumentos(0).NroRecibo.Trim = "" OrElse .DetDocumentos(0).TotalPagar.ToString.Trim = "" OrElse _
                        .DetDocumentos(0).MontoPagar.ToString.Trim = "" OrElse .DetDocumentos(0).MoraPagar.ToString.Trim = "" OrElse .DetDocumentos(0).CodMoneda.ToString.Trim = "" OrElse _
                        .DetDocumentos(0).FecEmision.Trim = "" OrElse .DetDocumentos(0).FecVencimiento.Trim = "" OrElse .DetDocumentos(0).CodConvenio.Trim = "" Then

                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = "Algunos de los campos mandatorios no presentan valor."
                        Return response
                    End If
                End With

                '--------------------------------------------------------
                ' 2. INPUT - MOVIMIENTO
                '--------------------------------------------------------
                With obeMovimiento

                    .NumeroOrdenPago = request.CodOrdenPago.Trim            ' Nro. IDENTIFICADOR DE PAGO (CIP)
                    .NumeroOperacion = request.CodPuntoVenta.Trim & request.NroTerminal.Trim & request.NroOperacionExtorno.Trim & FechaPago 'CDate(request.FecPago).ToString("ddMMyyyy")    ' Nro. DE OPERACION DE EXTORNO GENERADO POR EL PROVEEDOR

                    .CodigoPuntoVenta = request.CodPuntoVenta.Trim          ' Cod. DE PUNTO DE VENTA DEL ESTABLECIMIENTO
                    .NumeroOperacionPago = request.CodPuntoVenta.Trim & request.NroTerminal.Trim & request.NroOperacionPago.Trim & FechaPago 'CDate(request.FecPago).ToString("ddMMyyyy") '3R
                    .NumeroSerieTerminal = request.NroTerminal.Trim

                    .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.DetDocumentos(0).TotalPagar) 'Importe
                    .CodMonedaBanco = request.CodMoneda.ToString.Trim 'Codigo Moneda
                    .CodigoBanco = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Kasnet 'Cod. Banco
                    .CodigoServicio = request.CodConvenio 'CODIGO QUE IDENTIFICA EL PRODUCTO

                End With

                If request.CodOrdenPago.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                    '--------------------------------------------------------
                    ' 3. PROCESO DE ANULACION DEL BANCO "BBVA"
                    '--------------------------------------------------------
                    'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

                    'Using oTransactionScope As New TransactionScope(TransactionScopeOption.Required, timeout)

                    'Using oTransactionScope As New TransactionScope

                    IdMovimiento = ExtornarDesdeBancoComun(obeMovimiento)

                    If IdMovimiento > 0 Then
                        'EXTORNO CORRECTO
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.TransaccionRealizadaExito
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.ObjKasnet.CodOperacionEmpresa = IdMovimiento.ToString

                        response.Estado = 1
                        response.Mensaje = "Transacion exitosa"

                        'oTransactionScope.Complete()

                    ElseIf IdMovimiento = 0 Then
                        'EXTORNO SIN EXITO
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.Mensaje = ListaCodValidacion(IdMovimiento) + " en el proceso de extorno"

                    ElseIf (IdMovimiento = -27) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoEliminado
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf (IdMovimiento = -4) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoPagado
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf (IdMovimiento = -1) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoConEstadoExpirado
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

                        response.Mensaje = ListaCodValidacion(IdMovimiento)

                    ElseIf (IdMovimiento = -17) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.MontoOMonedaNoCoincide
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf (IdMovimiento = -13 Or IdMovimiento = -18) Then
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    ElseIf IdMovimiento = -37 Then
                        'TIEMPO MAXIMO DE EXTORNO
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.NoSePermiteExtornar
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    Else
                        'VALIDACION DE EXTORNO SIN EXITO
                        response.ObjKasnet.CodMensaje = kasnet.Mensaje.CodigoExtornarNoExiste
                        response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                        response.Mensaje = ListaCodValidacion(IdMovimiento)
                    End If

                    'End Using
                Else     'Anulando una recarga de dinero

                    With response.ObjKasnet
                        '.codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                        .CodPuntoVenta = request.CodPuntoVenta.Trim                 ' CODIGO DE PUNTO DE VENTA DEL ESTABLECIMIENTO
                        .NroTerminal = request.NroTerminal.Trim                     ' NUMERO QUE IDENTIFICA AL TERMINAL POS
                        .NroOperacionExtorno = request.NroOperacionExtorno.Trim     ' NUMERO DE OPERACION DE EXTORNO GENERADO POR EL PROVEEDOR
                        .CodOrdenPago = request.CodOrdenPago.Trim                   ' CODIGO IDENTIFICADOR DE PAGO (CIP)
                        .CantDocumentos = request.CantDocumentos.ToString.Trim      ' CANTIDAD DE DOCUMENTOS A EXTORNAR
                    End With
                    'Dim dv As New DADineroVirtual()
                    'Dim cuenta As New BECuentaDineroVirtual()
                    'cuenta.Numero = request.numeroReferenciaDeuda
                    '' Validar Cuenta
                    'cuenta = dv.ConsultarCuentaDineroVirtualUsuarioByNumero(cuenta)

                    'If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    '    'Throw New Exception("Cuenta_no_encontrada")
                    '    response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                    '    response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    '    response.Mensaje = "Cuenta no encontrada."
                    'Else

                    '    'valida que tenga activo el monedero y la cuenta
                    '    If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                    '        Dim resultextorno As Integer = ExtornarCuentaCommun(cuenta, obeMovimiento)
                    '        If resultextorno > 0 Then
                    '            'response.Mensaje = "Extorno realizado"
                    '            'response.Respuesta = BCP.Mensaje.Extorno_Realizado
                    '            ''Enviar Correo
                    '            Dim control As New BLEmail
                    '            control.EnviarCorreoCuentaExtorno(cuenta)
                    '            'response.ObjBBVA.mensajeResultado = "Extorno realizado"
                    '            response.ObjBBVA.codigoResultado = BBVA.Mensaje.TransaccionRealizadaExito
                    '            response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    '            response.ObjBBVA.numeroOperacionEmpresa = resultextorno

                    '            response.Estado = 1
                    '            response.Mensaje = "Transaccion exitosa"

                    '        Else
                    '            'Throw New Exception("Sucedio un error interno en la BD")
                    '            response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                    '            response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    '            response.Mensaje = "Sucedio un error interno en BD."
                    '        End If
                    '    Else
                    '        'Throw New Exception("La cuenta no esta activa")
                    '        response.ObjBBVA.codigoResultado = BBVA.Mensaje.NoExistePagoParaExtornar
                    '        response.ObjBBVA.mensajeResultado = ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    '        response.Mensaje = "La cuenta no esta activa."
                    '    End If
                    'End If
                End If

            Catch ex As Exception
                _nlogger.Error(ex, ex.Message)
                response.Estado = -1
                response.Mensaje = ex.Message
                Throw New _3Dev.FW.Excepciones.FWBusinessException(response.Mensaje, response)

            End Try
        Else
            response.ObjKasnet.CodMensaje = kasnet.Mensaje.TramaEnviadaInvalida
            response.ObjKasnet.Mensaje = ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)

            response.Mensaje = ListaCodValidacion(IdMovimiento)
        End If
        Return response
    End Function

#End Region
    'Private Function EsRecarga(NumeroOrdenPago As String) As Boolean

    '    Dim NumeroOrdenPagoTemp As String
    '    If NumeroOrdenPago.Length >= 10 And NumeroOrdenPago.Length <= 14 Then
    '        If NumeroOrdenPago.StartsWith("0") Then
    '            NumeroOrdenPagoTemp = NumeroOrdenPago.TrimStart("0")
    '            If NumeroOrdenPagoTemp.Length >= 10 And NumeroOrdenPagoTemp.Length <= 14 Then
    '                Return True
    '            End If
    '        Else
    '            If NumeroOrdenPago.Length >= 11 And NumeroOrdenPago.Length <= 14 Then
    '                Return True
    '            End If
    '        End If
    '    End If
    '    Return False
    'End Function

End Class
