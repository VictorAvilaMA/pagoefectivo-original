Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Collections.Generic

Public Class BLCaja

    '************************************************************************************************** 
    ' M�todo          : RegistrarCaja
    ' Descripci�n     : Registro de una Caja
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BECaja Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function RegistrarCaja(ByVal obeCaja As BECaja) As Integer
        Dim odaCaja As New DACaja
        Return odaCaja.RegistrarCaja(obeCaja)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ActualizarCaja
    ' Descripci�n     : Actualiza de una Caja
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BECaja Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function ActualizarCaja(ByVal obeCaja As BECaja) As Integer
        Dim odaCaja As New DACaja
        Return odaCaja.ActualizarCaja(obeCaja)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarCaja
    ' Descripci�n     : Consulta de Caja
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : IdAgenciaRecaudadora
    ' Parametros_Out  : Lista de entidades BECaja
    ''**************************************************************************************************
    Public Function ConsultarCaja(ByVal IdAgenciaRecaudadora As Integer) As List(Of BECaja)
        Dim odaCaja As New DACaja
        Return odaCaja.ConsultarCajaPorAgencia(IdAgenciaRecaudadora)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ObtenerAgenciaPorUsuario
    ' Descripci�n     : Consulta de agencia por IdUsuario
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : IdUsuario
    ' Parametros_Out  : Lista de entidades BEAgenciaRecaudadora
    ''**************************************************************************************************
    Public Function ObtenerAgenciaPorUsuario(ByVal IdUsuario As Integer) As BEAgenciaRecaudadora
        Dim odaAgencias As New DACaja
        Return odaAgencias.ObtenerAgenciaPorUsuario(IdUsuario)
    End Function

    '************************************************************************************************** 
    ' M�todo          : OtorgarFechaValuta
    ' Descripci�n     : Otorga una fecha valuya a un determinado agente
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BEFechaValuta Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function OtorgarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer
        Dim odaCaja As New DACaja
        Return odaCaja.OtorgarFechaValuta(obeFechaValuta)
    End Function

End Class
