Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Transactions
Imports _3Dev.FW.Negocio
Imports _3Dev.FW.Entidades.Seguridad
Imports SPE.EmsambladoComun.ParametrosSistema
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class BLCliente
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DACliente()
    End Function

    Public Overrides Function ExecProcedureByParameters(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Try
            Select Case key
                Case "RegisClientConPOP"
                    Return RegistrarClienteConPreOrdenPago(be)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Property oDACliente() As SPE.AccesoDatos.DACliente
        Get
            Return CType(DataAccessObject, SPE.AccesoDatos.DACliente)
        End Get
        Set(ByVal value As SPE.AccesoDatos.DACliente)

        End Set
    End Property

    Private Function CodigoRegistro() As String
        Return Guid.NewGuid.ToString
    End Function

    '************************************************************************************************** 
    ' Método          : RegistrarClienteConPreOrdenPago
    ' Descripción     : Registro de Cliente con una PreOrdenPago
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BusinessEntityBase Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function RegistrarClienteConPreOrdenPago(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Try
            If (oDACliente.GetObjectByParameters("", be)) > 0 Then
                Return -100
            Else
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted
                Using transaccionscope As New TransactionScope()

                    Try
                        Dim becliente As BECliente = CType(be, SPE.Entidades.BECliente)
                        Dim passwordTemp As String
                        If becliente.Password = "" Then
                            passwordTemp = SeguridadComun.GenerarCodigoAleatorio
                            becliente.Password = passwordTemp
                        Else
                            passwordTemp = becliente.Password
                        End If

                        becliente.CodigoRegistro = Me.CodigoRegistro()
                        Dim strpassword As String = becliente.Password
                        becliente.Password = SeguridadComun.EncryptarCodigo(strpassword)

                        Dim val As Integer = MyBase.InsertRecord(be)
                        becliente.IdUsuario = val
                        becliente.IdCliente = val
                        becliente.OrdenPago.IdCliente = val
                        becliente.OrdenPago.IdUsuarioCreacion = becliente.IdUsuario
                        oDACliente.RegistrarPreOrdenRegistroUsuario(becliente.OrdenPago)
                        Dim email As New BLEmail()
                        email.EnviarCorreoConfirmacionUsuario(becliente.IdUsuario, becliente.AliasCliente, strpassword, becliente.CodigoRegistro, becliente.Email)
                        transaccionscope.Complete()
                        Return val
                    Catch ex As Exception
                        Throw ex
                    End Try
                End Using
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function

    '************************************************************************************************** 
    ' Método          : InsertRecord
    ' Descripción     : Registro de Cliente
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BusinessEntityBase Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Return CommonInsertRecord(be, True)
    End Function

    Public Function CommonInsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal SendMailMessage As Boolean, Optional ByVal bcc As String = "") As Integer
        Try
            Dim intResult = 0
            intResult = oDACliente.GetObjectByParameters("", be)
            If intResult > 0 Then
                If intResult = 1 Then
                    Return -100
                Else
                    Return -200
                End If
            Else
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted
                Using transaccionscope As New TransactionScope()

                    Try
                        Dim becliente As BECliente = CType(be, SPE.Entidades.BECliente)
                        Dim passwordTemp As String
                        If becliente.Password = "" Then
                            passwordTemp = SeguridadComun.GenerarCodigoAleatorio
                            becliente.Password = passwordTemp
                        Else
                            passwordTemp = becliente.Password
                        End If

                        becliente.CodigoRegistro = CodigoRegistro()
                        Dim strpassword As String = becliente.Password
                        becliente.Password = SeguridadComun.EncryptarCodigo(strpassword)
                        Dim val As Integer = MyBase.InsertRecord(be)
                        If SendMailMessage Then
                            Using email As New BLEmail()
                                email.EnviarCorreoConfirmacionUsuario(becliente.IdUsuario, becliente.AliasCliente, strpassword, becliente.CodigoRegistro, becliente.Email, bcc)
                            End Using
                        End If
                        transaccionscope.Complete()
                        Return val
                    Catch ex As Exception
                        Throw ex
                    End Try
                End Using
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function

    '************************************************************************************************** 
    ' Método          : ActualizarEstadoUsuario
    ' Descripción     : Actualiza el estado de un usuario
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BEUsuario Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function ActualizarEstadoUsuario(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
        Dim odaCliente As New DACliente
        Return odaCliente.ActualizarEstadoUsuario(obeUsuario)
    End Function

    '************************************************************************************************** 
    ' Método          : CambiarContraseñaUsuario
    ' Descripción     : Cambia la contraseña de un usuario
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BEUsuario Cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function CambiarContraseñaUsuario(ByVal obeUsuario As BEUsuario) As Integer
        Try
            Dim odaCliente As New DACliente
            obeUsuario.Password = SeguridadComun.EncryptarCodigo(obeUsuario.Password)
            obeUsuario.NuevoPassword = SeguridadComun.EncryptarCodigo(obeUsuario.NuevoPassword)
            Return odaCliente.CambiarContraseñaUsuario(obeUsuario)

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    '************************************************************************************************** 
    ' Método          : ValidarEmailUsuarioPorEstado
    ' Descripción     : Valida el email de usuario para que no se vuelva ingresar
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BEUsuario Cargada
    ' Parametros_Out  : Instancia de la Entidad BEUsuario Cargada
    ''**************************************************************************************************
    Public Function ValidarEmailUsuarioPorEstado(ByVal obeUsuario As BEUsuario) As BEUsuario
        Dim options As TransactionOptions = New TransactionOptions()
        Dim objUsuario As SPE.Entidades.BEUsuarioBase = Nothing

        Try
            Dim odaCliente As New DACliente

            Dim password As String = ""
            objUsuario = odaCliente.ValidarEmailUsuarioPorEstado(obeUsuario)
            If objUsuario.IdUsuario = Nothing Or objUsuario.IdUsuario = 0 Then
                objUsuario.DescripcionEstado = "No existe un usuario con ese correo"
            Else
                If objUsuario.IdEstado = EstadoUsuario.Activo Then
                    password = SeguridadComun.UnEncryptarCodigo(objUsuario.Password)
                    Dim email As New BLEmail()
                    email.EnviarCorreoRecuperarPasswordUsuario(objUsuario.Nombres + " " + objUsuario.Apellidos, password, objUsuario.Email)
                    password = ""
                    objUsuario.DescripcionEstado = "Su contraseña ha sido enviada a su correo"
                ElseIf (objUsuario.IdEstado = EstadoUsuario.Bloqueado) Then
                    objUsuario.DescripcionEstado = "No puede recuperar su contraseña por que su cuenta esta bloqueada. Contactátese con el administrador contacto@pagoefectivo.pe"
                ElseIf (objUsuario.IdEstado = EstadoUsuario.Inactivo) Then
                    objUsuario.DescripcionEstado = "No puede recuperar su contraseña por que su cuenta esta inactiva. Contactátese con el administrador contacto@pagoefectivo.pe"
                ElseIf (objUsuario.IdEstado = EstadoUsuario.Pendiente) Then
                    Dim email As New BLEmail()
                    password = SeguridadComun.UnEncryptarCodigo(objUsuario.Password)
                    email.EnviarCorreoConfirmacionUsuario(objUsuario.IdUsuario, objUsuario.Nombres, password, objUsuario.CodigoRegistro, objUsuario.Email)
                    password = ""
                    'objUsuario.DescripcionEstado = "No puede recuperar su contraseña por que su cuenta esta pendiente de registro"
                    objUsuario.DescripcionEstado = "Su cuenta esta pendiente de activación. Se te ha enviado un correo para que puedas activarlo."
                ElseIf (objUsuario.IdEstado = 0) Then
                    objUsuario.DescripcionEstado = "No existe usuario con ese email"
                End If
            End If


            Return objUsuario
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try



    End Function

    '************************************************************************************************** 
    ' Método          : DoDefineOrderedList
    ' Descripción     : Define la clase con la cual se va ordenar para BusinessEntityBase 
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)

        Select Case pSortExpression
            Case "Nombres"
                list.Sort(New SPE.Entidades.BECliente.NombresComparer())
            Case "Email"
                list.Sort(New SPE.Entidades.BECliente.EmailComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BECliente.DescripcionEstadoComparer())
            Case "Apellidos"
                list.Sort(New SPE.Entidades.BECliente.ApellidosComparer())

        End Select
    End Sub

    Public Function GenerarPreordenPagoSuscriptores(ByVal xmlstring As String, ByVal urlservicio As String) As Integer
        Try
            Dim odaCliente As New DACliente

            Return odaCliente.GenerarPreordenPagoSuscriptores(xmlstring, urlservicio)

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarClientePorEmail(ByVal email As Object) As BECliente
        Return oDACliente.ConsultarClientePorEmail(email)
    End Function

    Public Function RegistrarClienteDesdeUsuarioExistente(ByVal obeCliente As BECliente) As BECliente
        Dim obeClienteResult As BECliente = obeCliente
        obeClienteResult = oDACliente.RegistrarClienteDesdeUsuario(obeCliente)
        Return obeClienteResult
    End Function
    Public Function ObtenerClientexIdUsuario(ByVal obeCliente As BECliente) As BECliente
        Dim obCliente As New DACliente
        Return obCliente.ObtenerClientexIdUsuario(obeCliente)
    End Function
    Public Function ValidarTipoyNumeroDocPorEmail(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
        Dim obCliente As New DACliente
        Return obCliente.ValidarTipoyNumeroDocPorEmail(obeUsuario)
    End Function
End Class
