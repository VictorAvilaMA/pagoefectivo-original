
Imports System.Net.Mail
Imports SPE.EmsambladoComun
Imports System.Configuration
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Transactions
Imports System.Xml

Public Class BLComercio
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAComercio
    End Function


    Public Function GetComercioWhitAO() As List(Of BEComercio)
        Return New SPE.AccesoDatos.DAComercio().GetComercioWhitAO()
    End Function



End Class
