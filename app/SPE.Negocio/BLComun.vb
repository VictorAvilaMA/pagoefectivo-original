Imports System
Imports System.Configuration
Imports System.Configuration.Configuration
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun.ParametrosSistema
Imports System.Transactions
Imports SPE.Logging

Public Class BLComun
    Implements IDisposable
    Private ReadOnly _nlogger As ILogger = New Logger()

    '************************************************************************************************** 
    ' M�todo          : ConsultarMoneda
    ' Descripci�n     : Lista de Monedas
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Lista de entidades BEMoneda
    ''**************************************************************************************************
    Public Function ConsultarMoneda() As List(Of BEMoneda)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarMoneda()
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarMonedaPorId
    ' Descripci�n     : Lista de Moneda por IdMoneda
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : parametro
    ' Parametros_Out  : Instancia de entidad BEMoneda cargada
    ''**************************************************************************************************
    Public Function ConsultarMonedaPorId(ByVal parametro As Object) As BEMoneda
        Dim odaComun As New DAComun
        Return odaComun.ConsultarMonedaPorId(parametro)

    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarMedioPago
    ' Descripci�n     : Lista de Medio de Pago
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Lista de entidades BEMedioPago
    ''**************************************************************************************************
    Public Function ConsultarMedioPago() As List(Of BEMedioPago)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarMedioPago()
    End Function
    '<upd Proc.Tesoreria>
    Public Function ConsultarMedioPagoBancoByBanco(be As BEBanco) As List(Of BEMedioPagoBanco)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarMedioPagoBancoByBanco(be)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarTarjeta
    ' Descripci�n     : Lista de Tarjetas
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Lista de entidades BETarjeta
    ''**************************************************************************************************
    Public Function ConsultarTarjeta() As List(Of BETarjeta)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarTarjeta()
    End Function

    '************************************************************************************************** 
    ' M�todo          : RegistrarDetalleTarjeta
    ' Descripci�n     : Reistro de detalle de una tarjeta cuando se recepciona una OP
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de entidad BEDetalleTarjeta cargada
    ' Parametros_Out  : Resultado de la transaccion
    ''**************************************************************************************************
    Public Function RegistrarDetalleTarjeta(ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
        Dim odaComun As New DAComun
        Return odaComun.RegistrarDetalleTarjeta(obeDetalleTarjeta)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarMovimiento
    ' Descripci�n     : Consulta de movimientos de una caja
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de entidad BEMovimiento cargada
    ' Parametros_Out  : Lista de entidades BEMovimiento
    ''**************************************************************************************************
    Public Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As List(Of BEMovimiento)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarMovimiento(obeMovimiento)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarFechaHoraActual
    ' Descripci�n     : Consulta la Fecha y Hora Actual del servidor
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Fecha y Hora
    ''**************************************************************************************************
    Public Function ConsultarFechaHoraActual() As DateTime
        Dim odaComun As New DAComun
        Return odaComun.ConsultarFechaHoraActual()
    End Function


    '************************************************************************************************** 
    ' M�todo          : ConsultarTipoOrigenCancelacion
    ' Descripci�n     : Consulta de tipos de origenes de cancelacion
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 29/12/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Lista de Entidades de Tipo Origen Cancelacion
    ''**************************************************************************************************
    Public Function ConsultarTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarTipoOrigenCancelacion()
    End Function


    '************************************************************************************************** 
    ' M�todo          : ConsultarTipoOrigenCancelacion
    ' Descripci�n     : Consulta de tipos de origenes de cancelacion
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 29/12/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Lista de Entidades de Tipo Origen Cancelacion
    ''**************************************************************************************************
    Public Function ConsultarPasarelaMedioPago() As List(Of BEPasarelaMedioPago)
        Dim odaComun As New DAComun
        Return odaComun.ConsultarPasarelaMedioPago()
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultaNumCorrelativo
    ' Descripci�n     : Consulta el numero correlativo para 
    ' Autor           : Jorge Puente
    ' Fecha/Hora      : 10/12/2009
    ' Parametros_In   : URL del refer
    ' Parametros_Out  : N�mero correlativo para su utilizaci�n en cada uno de los servicios
    ''**************************************************************************************************
    Public Function ConsultaNumCorrelativo(ByVal url As String) As Integer
        Dim odaComun As New DAComun
        Return odaComun.ConsultaNumCorrelativo(url)
    End Function


#Region "Log"

    Public Function RegistrarLog(ByVal obelog As BELog) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Return odaComun.RegistrarLog(obelog)
            End Using
        End If
    End Function

    Public Function ConsultarLog(ByVal obelog As BELog) As List(Of BELog)
        Using odaComun As New DAComun()
            Return odaComun.ConsultarLog(obelog)
        End Using
    End Function


    ' log del bcp            
    Public Function RegistrarLogBCP(ByVal obelog As BELog) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Return odaComun.RegistrarLogBCP(obelog)
            End Using
        End If
    End Function

    Public Function RegistrarLogBCPResquest(ByVal codOperacion As String, ByVal obeBCP As BEBCPRequest) As String
        'Dim parametro As Integer = SPE.EmsambladoComun.ParametrosSistema.TipoRegistroLog.MongoDBSQL 'Pedro de shit
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()

                Select Case codOperacion
                    Case Consultar
                        Return odaComun.RegistrarLogBCPConsReq(obeBCP)
                    Case Cancelar
                        Return odaComun.RegistrarLogBCPCancReq(obeBCP)
                    Case Anular
                        Return odaComun.RegistrarLogBCPAnulReq(obeBCP)
                End Select
            End Using
        End If

        Return String.Empty
    End Function

    Public Function RegistrarLogBCPResponse(ByVal codOperacion As String, ByVal obeBCP As BEBCPResponse) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                obeBCP.DescEstado = IIf(obeBCP.Estado = 1, "Exito", IIf(obeBCP.Estado = 0, "Validacion", "Error"))
                If (obeBCP.Respuesta.Length > 0) Then
                    obeBCP.Codigo = obeBCP.Respuesta(33)
                End If
                Select Case codOperacion
                    Case Consultar
                        Return odaComun.RegistrarLogBCPConsRes(obeBCP)
                    Case Cancelar
                        Return odaComun.RegistrarLogBCPCancRes(obeBCP)
                    Case Anular
                        Return odaComun.RegistrarLogBCPAnulRes(obeBCP)
                End Select
            End Using
        End If
    End Function

    'metodo implementado para los ws de BBVA    
    Public Function RegistrarLogBBVA(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeBBVA As Object) As Int64
        Dim habilitarLog = ConfigurationManager.AppSettings("HabilitarRegistroLog")

        _nlogger.Info(String.Format("RegistrarLogBBVA - Habilitado:{0}, Type:{1}, ObeId:{2}", habilitarLog, type, obeid))

        If (habilitarLog = "1") Then

            Using odaComun As New DAComun()

                Select Case type
                    Case Log.MetodosBBVA.consultar_request
                        'consultar request
                        Return odaComun.RegistrarLogBBVAConsReq(CType(obeBBVA, BEBBVAConsultarRequest))
                    Case Log.MetodosBBVA.consultar_response
                        'consultar response
                        Return odaComun.RegistrarLogBBVAConsRes(obeid, CType(obeBBVA, BEBBVAConsultarResponse), obelog)
                    Case Log.MetodosBBVA.pagar_request
                        'pagar request
                        Return odaComun.RegistrarLogBBVAPagarReq(CType(obeBBVA, BEBBVAPagarRequest))
                    Case Log.MetodosBBVA.pagar_response
                        'pagar response
                        Return odaComun.RegistrarLogBBVAPagarRes(obeid, CType(obeBBVA, BEBBVAPagarResponse), obelog)
                    Case Log.MetodosBBVA.anular_request
                        'anular request
                        Return odaComun.RegistrarLogBBVAExtReq(CType(obeBBVA, BEBBVAAnularRequest))
                    Case Log.MetodosBBVA.anular_response
                        'anular response
                        Return odaComun.RegistrarLogBBVAExtRes(obeid, CType(obeBBVA, BEBBVAAnularResponse), obelog)
                    Case Log.MetodosBBVA.extornar_request
                        'extornar request
                        Return odaComun.RegistrarLogBBVAExtAutoReq(CType(obeBBVA, BEBBVAExtornarRequest))
                    Case Log.MetodosBBVA.extornar_response
                        'extornar response
                        Return odaComun.RegistrarLogBBVAExtAutoRes(obeid, CType(obeBBVA, BEBBVAExtornarResponse), obelog)
                        'Case Log.MetodosBBVA.log_bbva
                        '    'registrar en la tabla LogBBVA
                        '    'RegistrarLog(obelog)
                        '    Return odaComun.RegistrarLogBBVA(CType(obeBBVA, BELog))

                End Select
            End Using

        End If
    End Function
    Public Function RegistrarLogBanBif(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeBanBif As Object) As Int64
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then

            Using odaComun As New DAComun()
                'If (obelog IsNot Nothing) Then
                '    odaComun.RegistrarLog(obelog)
                'End If
                Select Case type
                    Case Log.MetodosBBVA.consultar_request
                        'consultar request
                        Return odaComun.RegistrarLogBanBifConsReq(CType(obeBanBif, BEBanBifConsultarRequest))
                    Case Log.MetodosBBVA.consultar_response
                        'consultar response
                        Return odaComun.RegistrarLogBanBifConsRes(obeid, CType(obeBanBif, BEBanBifConsultarResponse), obelog)
                    Case Log.MetodosBBVA.pagar_request
                        'pagar request
                        Return odaComun.RegistrarLogBanBifPagarReq(CType(obeBanBif, BEBanBifPagarRequest))
                    Case Log.MetodosBBVA.pagar_response
                        'pagar response
                        Return odaComun.RegistrarLogBanBifPagarRes(obeid, CType(obeBanBif, BEBanBifPagarResponse), obelog)
                    Case Log.MetodosBBVA.anular_request
                        'anular request
                        Return odaComun.RegistrarLogBanBifExtornarReq(CType(obeBanBif, BEBanBifExtornoRequest))
                    Case Log.MetodosBBVA.anular_response
                        'anular response
                        Return odaComun.RegistrarLogBanBifExtornarRes(obeid, CType(obeBanBif, BEBanBifExtornoResponse), obelog)
                    Case Log.MetodosBBVA.extornar_request
                        'extornar request
                        Return odaComun.RegistrarLogBanBifExtornarAutomaticoReq(CType(obeBanBif, BEBanBifExtornoRequest))
                    Case Log.MetodosBBVA.extornar_response
                        'extornar response
                        Return odaComun.RegistrarLogBanBifExtornarAutomaticoRes(obeid, CType(obeBanBif, BEBanBifExtornoResponse), obelog)
                        'Case Log.MetodosBBVA.log_bbva
                        '    'registrar en la tabla LogBBVA
                        '    'RegistrarLog(obelog)
                        '    Return odaComun.RegistrarLogBBVA(CType(obeBBVA, BELog))

                End Select
            End Using

        End If
    End Function
    Public Function RegistrarLogFC(ByVal type As String, ByVal obeid As Int64, ByVal obeFC As Object, _
        ByVal oBELog As BELog) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Select Case type
                    Case Log.MetodosFullCarga.consultar_request
                        Return odaComun.RegistrarLogFCConsReq(CType(obeFC, BEFCConsultarRequest))
                    Case Log.MetodosFullCarga.consultar_response
                        Return odaComun.RegistrarLogFCConsRes(obeid, CType(obeFC, BEFCConsultarResponse), oBELog)
                    Case Log.MetodosFullCarga.cancelar_request
                        Return odaComun.RegistrarLogFCCancReq(CType(obeFC, BEFCCancelarRequest))
                    Case Log.MetodosFullCarga.cancelar_response
                        Return odaComun.RegistrarLogFCCancRes(obeid, CType(obeFC, BEFCCancelarResponse), oBELog)
                    Case Log.MetodosFullCarga.anular_request
                        Return odaComun.RegistrarLogFCAnulReq(CType(obeFC, BEFCAnularRequest))
                    Case Log.MetodosFullCarga.anular_response
                        Return odaComun.RegistrarLogFCAnulRes(obeid, CType(obeFC, BEFCAnularResponse), oBELog)
                End Select
            End Using
        End If
    End Function

    Public Function RegistrarLogLF(ByVal type As String, ByVal obeid As Int64, ByVal obeLF As Object, _
        ByVal oBELog As BELog) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Select Case type
                    Case Log.MetodosLife.consultar_request
                        Return odaComun.RegistrarLogLFConsReq(CType(obeLF, BELFConsultarRequest))
                    Case Log.MetodosLife.consultar_response
                        Return odaComun.RegistrarLogLFConsRes(obeid, CType(obeLF, BELFConsultarResponse), oBELog)
                    Case Log.MetodosLife.cancelar_request
                        Return odaComun.RegistrarLogLFCancReq(CType(obeLF, BELFCancelarRequest))
                    Case Log.MetodosLife.cancelar_response
                        Return odaComun.RegistrarLogLFCancRes(obeid, CType(obeLF, BELFCancelarResponse), oBELog)
                    Case Log.MetodosLife.anular_request
                        Return odaComun.RegistrarLogLFAnulReq(CType(obeLF, BELFAnularRequest))
                    Case Log.MetodosLife.anular_response
                        Return odaComun.RegistrarLogLFAnulRes(obeid, CType(obeLF, BELFAnularResponse), oBELog)
                End Select
            End Using
        End If
    End Function

    Public Function RegistrarLogSBK(ByVal type As String, ByVal obeid As Int64, ByVal obeSBK As Object, _
        ByVal oBELog As BELog) As Integer
        Try
            If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
                Using odaComun As New DAComun()
                    Select Case type
                        Case Log.MetodosScotiabank.consultar_request
                            Return odaComun.RegistrarLogSBKConsReq(CType(obeSBK, BESBKConsultarRequest))
                        Case Log.MetodosScotiabank.consultar_response
                            Return odaComun.RegistrarLogSBKConsRes(obeid, CType(obeSBK, BESBKConsultarResponse), oBELog)
                        Case Log.MetodosScotiabank.cancelar_request
                            Return odaComun.RegistrarLogSBKCancReq(CType(obeSBK, BESBKCancelarRequest))
                        Case Log.MetodosScotiabank.cancelar_response
                            Return odaComun.RegistrarLogSBKCancRes(obeid, CType(obeSBK, BESBKCancelarResponse), oBELog)
                        Case Log.MetodosScotiabank.anular_request
                            Return odaComun.RegistrarLogSBKAnulReq(CType(obeSBK, BESBKAnularRequest))
                        Case Log.MetodosScotiabank.anular_response
                            Return odaComun.RegistrarLogSBKAnulRes(obeid, CType(obeSBK, BESBKAnularResponse), oBELog)
                        Case Log.MetodosScotiabank.cancelarauto_request
                            Return odaComun.RegistrarLogSBKExtCancReq(CType(obeSBK, BESBKExtornarCancelacionRequest))
                        Case Log.MetodosScotiabank.cancelarauto_response
                            Return odaComun.RegistrarLogSBKExtCancRes(obeid, CType(obeSBK, BESBKExtornarCancelacionResponse), oBELog)
                        Case Log.MetodosScotiabank.anularauto_request
                            Return odaComun.RegistrarLogSBKExtAnulReq(CType(obeSBK, BESBKExtornarAnulacionRequest))
                        Case Log.MetodosScotiabank.anularauto_response
                            Return odaComun.RegistrarLogSBKExtAnulRes(obeid, CType(obeSBK, BESBKExtornarAnulacionResponse), oBELog)
                    End Select
                End Using
            End If
        Catch ex As Exception
            'Logger.Write(ex)
        End Try

    End Function

    Public Function RegistrarLogIBK(ByVal type As String, ByVal obeid As Int64, ByVal obeIBK As Object, _
    ByVal oBELog As BELog) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Select Case type
                    Case Log.MetodosInterbank.consultar_request
                        Return odaComun.RegistrarLogIBKConsReq(CType(obeIBK, BEIBKConsultarRequest))
                    Case Log.MetodosInterbank.consultar_response
                        Return odaComun.RegistrarLogIBKConsRes(obeid, CType(obeIBK, BEIBKConsultarResponse), oBELog)
                    Case Log.MetodosInterbank.cancelar_request
                        Return odaComun.RegistrarLogIBKCancReq(CType(obeIBK, BEIBKCancelarRequest))
                    Case Log.MetodosInterbank.cancelar_response
                        Return odaComun.RegistrarLogIBKCancRes(obeid, CType(obeIBK, BEIBKCancelarResponse), oBELog)
                    Case Log.MetodosInterbank.anular_request
                        Return odaComun.RegistrarLogIBKAnulReq(CType(obeIBK, BEIBKAnularRequest))
                    Case Log.MetodosInterbank.anular_response
                        Return odaComun.RegistrarLogIBKAnulRes(obeid, CType(obeIBK, BEIBKAnularResponse), oBELog)
                    Case Log.MetodosInterbank.cancelarauto_request
                        Return odaComun.RegistrarLogIBKExtCancReq(CType(obeIBK, BEIBKExtornarCancelacionRequest))
                    Case Log.MetodosInterbank.cancelarauto_response
                        Return odaComun.RegistrarLogIBKExtCancRes(obeid, CType(obeIBK, BEIBKExtornarCancelacionResponse), oBELog)
                    Case Log.MetodosInterbank.anularauto_request
                        Return odaComun.RegistrarLogIBKExtAnulReq(CType(obeIBK, BEIBKExtornarAnulacionRequest))
                    Case Log.MetodosInterbank.anularauto_response
                        Return odaComun.RegistrarLogIBKExtAnulRes(obeid, CType(obeIBK, BEIBKExtornarAnulacionResponse), oBELog)
                End Select
            End Using
        End If
    End Function

    Public Function RegistrarLogWU(ByVal type As String, ByVal obeid As Int64, ByVal obeWU As Object) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Select Case type
                    Case Log.MetodosWesternUnion.consultar_request
                        Return odaComun.RegistrarLogWUConsReq(CType(obeWU, BEWUConsultarRequest))
                    Case Log.MetodosWesternUnion.consultar_response
                        Dim IdLogWUConsRes As Int64 = 0
                        Using transaccionscope As New TransactionScope()
                            IdLogWUConsRes = odaComun.RegistrarLogWUConsRes(obeid, CType(obeWU, BEWUConsultarResponse))
                            odaComun.RegistrarLogWUConsResDet(IdLogWUConsRes, CType(obeWU, BEWUConsultarResponse))
                            transaccionscope.Complete()
                        End Using
                        Return IdLogWUConsRes
                    Case Log.MetodosWesternUnion.cancelar_request
                        Return odaComun.RegistrarLogWUCancReq(CType(obeWU, BEWUCancelarRequest))
                    Case Log.MetodosWesternUnion.cancelar_response
                        Return odaComun.RegistrarLogWUCancRes(obeid, CType(obeWU, BEWUCancelarResponse))
                    Case Log.MetodosWesternUnion.anular_request
                        Return odaComun.RegistrarLogWUAnulReq(CType(obeWU, BEWUAnularRequest))
                    Case Log.MetodosWesternUnion.anular_response
                        Return odaComun.RegistrarLogWUAnulRes(obeid, CType(obeWU, BEWUAnularResponse))
                End Select
            End Using
        End If
    End Function
    'M MoMo

    Public Function RegistrarLogMoMo(ByVal type As String, ByVal obeid As Int64, ByVal obeMoMo As Object, _
       ByVal oBELog As BELog) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Select Case type
                    Case Log.MetodosMoMo.consultar_request
                        Return odaComun.RegistrarLogMoMoConsReq(CType(obeMoMo, BEMoMoConsultarRequest))
                    Case Log.MetodosMoMo.consultar_response
                        Return odaComun.RegistrarLogMoMoConsRes(obeid, CType(obeMoMo, BEMoMoConsultarResponse), oBELog)
                    Case Log.MetodosMoMo.cancelar_request
                        Return odaComun.RegistrarLogMoMoCancReq(CType(obeMoMo, BEMoMoCancelarRequest))
                    Case Log.MetodosMoMo.cancelar_response
                        Return odaComun.RegistrarLogMoMoCancRes(obeid, CType(obeMoMo, BEMoMoCancelarResponse), oBELog)
                    Case Log.MetodosMoMo.anular_request
                        Return odaComun.RegistrarLogMoMoAnulReq(CType(obeMoMo, BEMoMoAnularRequest))
                    Case Log.MetodosMoMo.anular_response
                        Return odaComun.RegistrarLogMoMoAnulRes(obeid, CType(obeMoMo, BEMoMoAnularResponse), oBELog)
                End Select
            End Using
        End If
    End Function

    'metodo implementado para los ws de Kasnet    
    Public Function RegistrarLogKasnet(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeKasnet As Object) As Int64
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then

            Using odaComun As New DAComun()
                'If (obelog IsNot Nothing) Then
                '    odaComun.RegistrarLog(obelog)
                'End If
                Select Case type
                    Case Log.MetodosKasnet.consultar_request
                        'consultar request
                        Return odaComun.RegistrarLogKasnetConsReq(CType(obeKasnet, BEKasnetConsultarRequest))
                    Case Log.MetodosKasnet.consultar_response
                        'consultar response
                        Return odaComun.RegistrarLogKasnetConsRes(obeid, CType(obeKasnet, BEKasnetConsultarResponse), obelog)
                    Case Log.MetodosKasnet.pagar_request
                        'pagar request
                        Return odaComun.RegistrarLogKasnetPagarReq(CType(obeKasnet, BEKasnetPagarRequest))
                    Case Log.MetodosKasnet.pagar_response
                        'pagar response
                        Return odaComun.RegistrarLogKasnetPagarRes(obeid, CType(obeKasnet, BEKasnetPagarResponse), obelog)
                    Case Log.MetodosKasnet.extornar_request
                        'extornar request
                        Return odaComun.RegistrarLogKasnetExtReq(CType(obeKasnet, BEKasnetExtornarRequest))
                    Case Log.MetodosKasnet.extornar_response
                        'extornar response
                        Return odaComun.RegistrarLogKasnetExtRes(obeid, CType(obeKasnet, BEKasnetExtornarResponse), obelog)


                End Select
            End Using

        End If
    End Function
#End Region

#Region "PassLog"

    Public Function RegistrarPassLog(ByVal oPasLog As BELog) As Integer
        Try
            Using oComun As New DAComun
                Return oComun.RegistrarPasLog(oPasLog)
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
        End Try

    End Function

#End Region

#Region "M�todos para Administrar el Cache"
    Public Sub InicializarTodoCache()
        Try
            CacheServer.InicializarTodoCache()
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub
    Public Sub InicializarElementoCache(ByVal nombre As String)
        Try
            CacheServer.InicializarElementoCache(nombre)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub
#End Region

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region "LogNavegacion"
    ' log navegacion
    Public Function RegistrarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Return odaComun.RegistrarLogNavegacion(obelognavegacion)
            End Using
        End If
    End Function

    Public Function ConsultarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As List(Of BELogNavegacion)
        Using odaComun As New DAComun()
            Return odaComun.ConsultarLogNavegacion(obelognavegacion)
        End Using
    End Function

#End Region

    '<add Peru.Com FaseIII>
#Region "Log Redirect"
    Public Function RegistrarLogRedirect(ByVal oBELogRedirect As BELogRedirect) As Integer
        Using odaComun As New DAComun()
            Return odaComun.RegistrarLogRedirect(oBELogRedirect)
        End Using
    End Function
#End Region

#Region "Equipo Registrado X Usuario"
    ' log navegacion
    Public Function RegistrarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Dim Res As New BEParametrosEmailMonedero
            Using odaComun As New DAComun()
                Res = odaComun.RegistrarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
                Res.Monto = CInt(Res.Monto)
                If Res.Monto = 1 Then
                    Dim bl As New BLEmail
                    bl.EnviarCorreoAlRegistrarEquipo(Res)
                End If
                Return Res.Monto
            End Using
        End If
    End Function

    Public Function ValidarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
        Using odaComun As New DAComun()
            Return odaComun.ValidarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
        End Using
    End Function

    Public Function ConsultarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
        Using odaComun As New DAComun()
            Return odaComun.ConsultarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
        End Using
    End Function

    Public Function EliminarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Integer
        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using odaComun As New DAComun()
                Return odaComun.EliminarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
            End Using
        End If
    End Function

#End Region


    Public Function RegistrarTokenCambioCorreo(ByVal be As BEUsuarioBase) As BEUsuarioBase
        '1� Verificamos que el usuario este activo
        Dim odaCliente As New DACliente
        Dim obeUsuario As SPE.Entidades.BEUsuarioBase = Nothing
        Dim objEmail As New BLEmail
        obeUsuario = odaCliente.ValidarEmailUsuarioPorEstado(be)
        Dim password As String = ""

        If obeUsuario.IdUsuario = Nothing Or obeUsuario.IdUsuario = 0 Then
            obeUsuario.DescripcionEstado = "No existe un usuario con ese correo"
        Else
            If obeUsuario.IdEstado = EstadoUsuario.Activo Then
                '2� REGISTRAMOS EL TOKEN EN LA BD
                be.TokenGUIDPassword = Guid.NewGuid().ToString
                obeUsuario = New DAComun().RegistrarTokenCambioCorreo(be)

                '3� ENVIAMOS EL TOKEN GENERADO POR CORREO

                objEmail.EnviarCorreoCambioContrasenia(obeUsuario)
                obeUsuario.DescripcionEstado = "Su contrase�a ha sido enviada a su correo"
            ElseIf (obeUsuario.IdEstado = EstadoUsuario.Bloqueado) Then
                obeUsuario.DescripcionEstado = "No puede recuperar su contrase�a por que su cuenta esta bloqueada. Contact�tese con el administrador contacto@pagoefectivo.pe"
            ElseIf (obeUsuario.IdEstado = EstadoUsuario.Inactivo) Then
                obeUsuario.DescripcionEstado = "No puede recuperar su contrase�a por que su cuenta esta inactiva. Contact�tese con el administrador contacto@pagoefectivo.pe"
            ElseIf (obeUsuario.IdEstado = EstadoUsuario.Pendiente) Then
                Dim email As New BLEmail()
                password = SeguridadComun.UnEncryptarCodigo(obeUsuario.Password)
                email.EnviarCorreoConfirmacionUsuario(obeUsuario.IdUsuario, obeUsuario.Nombres, password, obeUsuario.CodigoRegistro, obeUsuario.Email)
                password = ""
                'objUsuario.DescripcionEstado = "No puede recuperar su contrase�a por que su cuenta esta pendiente de registro"
                obeUsuario.DescripcionEstado = "Su cuenta esta pendiente de activaci�n. Se te ha enviado un correo para que puedas activarlo."
            ElseIf (obeUsuario.IdEstado = 0) Then
                obeUsuario.DescripcionEstado = "No existe usuario con ese email"
            End If
        End If



        ''2� REGISTRAMOS EL TOKEN EN LA BD
        'be.TokenGUIDPassword = Guid.NewGuid().ToString
        'Dim objUsuario As BEUsuarioBase = New DAComun().RegistrarTokenCambioCorreo(be)

        ''3� ENVIAMOS EL TOKEN GENERADO POR CORREO
        'Dim objEmail As New BLEmail
        'objEmail.EnviarCorreoCambioContrasenia(objUsuario)
        ''objUsuario.Token = ""
        Return obeUsuario
    End Function

    Public Function ActualizarContraseniaOlvidada(ByVal be As BEUsuarioBase) As BEUsuarioBase
        '1� REGISTRAMOS EL TOKEN EN LA BD
        be.NuevoPassword = SeguridadComun.EncryptarCodigo(be.NuevoPassword)
        'cmllae
        Dim objUsuario As BEUsuarioBase = New DAComun().ActualizarContraseniaOlvidada(be)

        Dim objUsuarioX As New BEUsuarioBase

        If objUsuario Is Nothing Then
            objUsuario = objUsuarioX
            objUsuario.IdEstado = -1
        End If

        '2� ENVIAMOS EL TOKEN GENERADO POR CORREO
        If objUsuario.IdEstado = 0 Then
            Dim objEmail As New BLEmail
            objEmail.EnviarCorreoCambioContraseniaConfirmacion(objUsuario)
            'objUsuario.Token = ""
        End If

        Return objUsuario
    End Function

    Public Function SuscriptorRegistrar(ByVal oBESuscriptor As SPE.Entidades.BESuscriptor) As String
        Try
            Return New SPE.AccesoDatos.DAComun().SuscriptorRegistrar(oBESuscriptor)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function
    Public Function ConsultarPlantillaTipoVariacion() As List(Of BEParametro)
        Try
            Return New SPE.AccesoDatos.DAComun().ConsultarPlantillaTipoVariacion()
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


#Region "Liquidaciones"


    Public Function RecuperarCipsNoConciliados(ByVal idMoneda As Integer, ByVal Fecha As DateTime) As List(Of BELiquidacion)
        Try
            Return New SPE.AccesoDatos.DAComun().RecuperarCipsNoConciliados(idMoneda, Fecha)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    Public Function RecuperarDepositosBCP(ByVal idMoneda As Integer, ByVal empresas As String) As List(Of BELiquidacion)
        Try
            Return New SPE.AccesoDatos.DAComun().RecuperarDepositosBCP(idMoneda, empresas)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    Public Function RegistrarPeriodo(ByVal oBEPeriodo As BEPeriodoLiquidacion) As Integer
        Dim odaComun As New DAComun
        Return odaComun.RegistrarPeriodo(oBEPeriodo)
    End Function

    Public Function ActualizarPeriodo(ByVal oBEPeriodo As BEPeriodoLiquidacion) As Integer
        Dim odaComun As New DAComun
        Return odaComun.ActualizarPeriodo(oBEPeriodo)
    End Function

    Public Function ConsultarIdDescripcionPeriodo() As List(Of BEPeriodoLiquidacion)
        Using odaParametro As New DAComun
            Return odaParametro.ConsultarIdDescripcionPeriodo()
        End Using
    End Function


    Public Function RecuperarDatosPeriodoLiquidacion(ByVal be As BEPeriodoLiquidacion) As List(Of BEPeriodoLiquidacion)
        Using odaParametro As New DAComun
            Return odaParametro.RecuperarDatosPeriodoLiquidacion(be)
        End Using
    End Function

    Public Function DeletePeriodo(ByVal oBEPeriodo As BEPeriodoLiquidacion) As Integer
        Dim odaComun As New DAComun
        Return odaComun.DeletePeriodo(oBEPeriodo)
    End Function

#End Region


End Class
