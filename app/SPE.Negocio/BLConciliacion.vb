Imports System
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports _3Dev.FW.Excepciones
Imports System.Globalization
Imports System.Configuration
Imports Excel

Public Class BLConciliacion
    Implements IDisposable

    Dim culturaPeru = New CultureInfo(AppSettings("CulturaIdiomaPeru"))

    ''' <summary>
    ''' Retorna clase que implementa los m�todos comunes de acuerdo al c�digo de banco
    ''' </summary>
    ''' <param name="CodigoBanco">C�digo del banco</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearIBLConciliacion(ByVal CodigoBanco As String) As IBLConciliacion
        Select Case CodigoBanco
            Case ParametrosSistema.Conciliacion.CodigoBancos.BCP
                Return New BLConciliacionBCP
            Case ParametrosSistema.Conciliacion.CodigoBancos.BBVA
                Return New BLConciliacionBBVA
            Case ParametrosSistema.Conciliacion.CodigoBancos.FullCarga
                Return New BLConciliacionFullCarga
            Case ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                Return New BLConciliacionSBK
            Case ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                Return New BLConciliacionWU
            Case ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                Return New BLConciliacionIBK
            Case ParametrosSistema.Conciliacion.CodigoBancos.BanBif
                Return New BLConciliacionBanBif
            Case ParametrosSistema.Conciliacion.CodigoBancos.Kasnet
                Return New BLConciliacionKasnet
            Case Else
                Throw New NotImplementedException("Conciliaci�n no implementada para el Banco con c�digo: " + CodigoBanco)
        End Select
    End Function

    ''' <summary>
    ''' Procesamiento de conciliaci�n de archivos
    ''' </summary>
    ''' <param name="request">
    ''' Instancia de objeto BEConciliacionArchivo:
    ''' - CodigoBanco
    ''' - NombreArchivo
    ''' - Bytes
    ''' - Fecha
    ''' - IdTipoConciliacion
    ''' - IdOrigenConciliacion
    ''' - IdUsuarioCreacion
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Function ProcesarArchivoConciliacion(ByVal request As BEConciliacionArchivo) As BEConciliacionResponse
        With request
            .ListaOperacionesConciliadas = New List(Of BEConciliacionDetalle)
            .ListaOperacionesNoConciliadas = New List(Of BEConciliacionDetalle)
        End With
        Dim response As New BEConciliacionResponse
        With response
            .ListaOperacionesConciliadas = New List(Of BEConciliacionDetalle)
            .ListaOperacionesNoConciliadas = New List(Of BEConciliacionDetalle)
        End With
        Try
            Using iBLConciliacion As IBLConciliacion = CrearIBLConciliacion(request.CodigoBanco)
                If Not iBLConciliacion.ValidarFechaProceso(request) Then
                    Throw New FWException("La fecha ingresada no coincide con la del archivo de conciliaci�n.")
                Else
                    Dim CM As String
                    Using Transaccion As New Transactions.TransactionScope
                        Using oDAConciliacion As New DAConciliacion
                            request.IdConciliacionArchivo = oDAConciliacion.RegistrarConciliacionArchivo(request)
                            If request.IdConciliacionArchivo = -1 Then
                                Throw New FWException("Ya existe un archivo con el mismo nombre.")
                            End If
                            Dim RutaCompleta As String = AppSettings("RutaArchivosConciliacion") & _
                                IIf(request.IdOrigenConciliacion = ParametrosSistema.Conciliacion.OrigenConciliacion.Manual, _
                                AppSettings("PrefijoArchivoConciliacionManual"), AppSettings("PrefijoArchivoConciliacionAutomatizada")) & _
                                request.CodigoBanco & "_" & request.NombreArchivo
                            Using strm As New IO.FileStream(RutaCompleta, IO.FileMode.Create)
                                strm.Write(request.Bytes, 0, request.Bytes.Length)
                                strm.Close()
                            End Using
                            Dim Contadores() As String
                            Dim op, rec As Integer
                            Dim Cont As String = iBLConciliacion.RegistrarDetallesArchivoConciliacion(request)
                            Contadores = Cont.Split("|")
                            op = CInt(Contadores(0))
                            rec = CInt(Contadores(1))
                            CM = CStr(Contadores(2))
                            request.IdUsuarioActualizacion = request.IdUsuarioCreacion
                            'Implementar Actualizar Contadores
                            oDAConciliacion.ActualizarCantidadDetallesConciliacionArchivo(request.IdConciliacionArchivo, op, rec)
                            Transaccion.Complete()
                        End Using
                    End Using
                    iBLConciliacion.ProcesarDetallesArchivoConciliacion(request, CM)
                    Dim oDAConAct As New DAConciliacion
                    oDAConAct.ActualizarEstadoConciliacionArchivo(request)
                    With response
                        .BMResultState = _3Dev.FW.Entidades.BMResult.Ok
                        .ListaOperacionesConciliadas = request.ListaOperacionesConciliadas
                        .ListaOperacionesNoConciliadas = request.ListaOperacionesNoConciliadas
                    End With
                End If
            End Using
        Catch ex As FWException
            response.BMResultState = _3Dev.FW.Entidades.BMResult.No
            response.Message = ex.Message
        Catch ex As Exception
            'Logger.Write(ex)
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Error
            response.Message = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.ResultadosConciliacion.ConciliacionErrorMsg
        End Try
        Return response
    End Function







    ''' <summary>
    ''' Consulta conciliaciones de acuerdo a estado y fecha.
    ''' </summary>
    ''' <param name="request">
    ''' Instancia de objeto BEConciliacionRequest:
    ''' - IdEstado
    ''' - FechaInicio
    ''' - FechaFin
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarUltimasConciliaciones(ByVal request As BEConciliacionRequest) As List(Of BEConciliacionResponse)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarUltimasConciliaciones(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Consulta conciliaciones de acuerdo a filtros
    ''' </summary>
    ''' <param name="request">
    ''' Instancia de objeto BEConciliacionEntidad:
    ''' - IdConciliacion
    ''' - IdEstado
    ''' - CodigoBanco
    ''' - FechaInicio
    ''' - FechaFin
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarConciliacionEntidad(ByVal request As BEConciliacionEntidad) As List(Of BEConciliacionEntidad)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarConciliacionEntidad(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Consulta conciliaciones de acuerdo a filtros
    ''' </summary>
    ''' <param name="request">
    ''' Instancia de objeto BEConciliacionArchivo:
    ''' - IdEstado
    ''' - CodigoBanco
    ''' - NombreArchivo
    ''' - IdTipoConciliacion
    ''' - IdOrigenConciliacion
    ''' - FechaInicio
    ''' - FechaFin
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionArchivo)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarConciliacionArchivo(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Consulta conciliaciones de acuerdo a filtros
    ''' </summary>
    ''' <param name="request">
    ''' Instancia de objeto BEConciliacionDetalle:
    ''' - IdEstado
    ''' - CodigoBanco
    ''' - NombreArchivo
    ''' - FechaInicio
    ''' - FechaFin
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarDetalleConciliacion(ByVal request As BEConciliacionDetalle) As List(Of BEConciliacionDetalle)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarDetalleConciliacion(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Sub RecalcularCIPsConciliacion(ByVal IdUsuario As Integer)
        Try
            Using oDAConciliacion As New DAConciliacion
                oDAConciliacion.RecalcularCIPsConciliacion(IdUsuario)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub

    Public Function ConsultarConciliacionAjuste(ByVal request As BEConciliacionAjuste) As List(Of BEConciliacionAjuste)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarConciliacionAjuste(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function



    Public Function ConsultarConciliacionDiaria(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionDiaria)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarConciliacionDiaria(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function




    Public Function ConsultarPreConciliacionDetalle(ByVal request As BEConciliacionArchivo) As List(Of BEPreConciliacionDetalle)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarPreConciliacionDetalle(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function



    Public Function ConsultarConciliacionDetalleAjuste(ByVal CodigoBanco As String, ByVal IdDetConciliacion As Integer) As BEConciliacionDetalleAjuste
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarConciliacionDetalleAjuste(CodigoBanco, IdDetConciliacion)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago)
        Try
            Using oDAConciliacion As New DAConciliacion
                Return oDAConciliacion.ConsultarOrdenesConcArch(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

#Region " PRE - Conciliacion "
    ''' <summary>
    ''' Retorna el IExcelDataReader del archivo de conciliaci�n
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearIExcelDataReader(ByVal request As BEPreConciliacionRequest) As IExcelDataReader
        Dim reader As IExcelDataReader
        Try
            Using _stream As IO.Stream = New IO.MemoryStream(request.Bytes)
                Select Case request.TipoArchivo
                    Case "application/vnd.ms-excel"
                        reader = Excel.ExcelReaderFactory.CreateBinaryReader(_stream)
                    Case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        reader = Excel.ExcelReaderFactory.CreateOpenXmlReader(_stream)
                    Case Else
                        Throw New Exception(String.Format("El archivo '{0}' no es de un tipo permitido. Solo se permite archivos tipo '.xls'", request.NombreArchivoPreCon))
                End Select
            End Using
        Catch ex As Exception
            Throw New Exception(String.Format("El archivo '{0}' no es de un tipo permitido. Solo se permite archivos tipo '.xls'", request.NombreArchivoPreCon))
        End Try
        If Not reader.IsValid Then
            Throw New Exception(String.Format("El archivo '{0}' no es de un tipo permitido. Solo se permite archivos tipo '.xls'", request.NombreArchivoPreCon))
        End If
        Return reader
    End Function

    ''' <summary>
    ''' Retorna clase que implementa los m�todos comunes de acuerdo al c�digo del Banco
    ''' </summary>
    ''' <param name="CodigoBanco">C�digo del Banco</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CrearIBLPreConciliacion(ByVal CodigoBanco As String) As IBLPreConciliacion
        Select Case CodigoBanco
            Case ParametrosSistema.Conciliacion.CodigoBancos.BCP
                Return New BLPreConciliacionBCP
            Case ParametrosSistema.Conciliacion.CodigoBancos.BBVA
                Return New BLPreConciliacionBBVA
            Case Else
                Throw New NotImplementedException("Conciliaci�n no implementada para la Tarjeta con c�digo: " + CodigoBanco)
        End Select
    End Function

    Public Function ProcesarArchivoPreConciliacion(ByVal request As BEPreConciliacionRequest) As BEConciliacionResponse
        With request
            .ListaOperacionesConciliadas = New List(Of BEConciliacionDetalle)
            .ListaOperacionesNoConciliadas = New List(Of BEConciliacionDetalle)
        End With
        Dim response As New BEConciliacionResponse
        With response
            .ListaOperacionesConciliadas = New List(Of BEConciliacionDetalle)
            .ListaOperacionesNoConciliadas = New List(Of BEConciliacionDetalle)
        End With
        Try
            If (request.CodigoBanco = ParametrosSistema.Conciliacion.CodigoBancos.BCP) Then
                Using iBLPreConciliacion As IBLPreConciliacion = CrearIBLPreConciliacion(request.CodigoBanco)

                    Dim ListaDataPreConciliacion As List(Of BEDataPreConciliacion)

                    Dim CantCar As Integer = request.NombreArchivoPreCon.Length()
                    Dim Nombre As String = request.NombreArchivoPreCon.Remove((CantCar - 4), 4)
                    request.NombreArchivoPreCon = Nombre & System.DateTime.Now.ToString("_yyyyMMddhmmss") & ".txt"



                    Using oDAConciliacion As New DAConciliacion
                        If request.NombreArchivoPreCon.Contains("3046") Then
                            request.IdMoneda = 1

                        ElseIf request.NombreArchivoPreCon.Contains("3045") Then
                            request.IdMoneda = 2
                        Else
                            Throw New Exception("El nombre del archivo es Incorrecto.")
                        End If

                        Dim IdPreConArchivo = oDAConciliacion.RegistrarPreConciliacionArchivo(request)
                        If IdPreConArchivo = -2 Then
                            Throw New Exception("No existe el Banco especificado.")
                        ElseIf IdPreConArchivo = -1 Then
                            Throw New Exception("Ya existe un archivo con el mismo nombre.")
                        Else
                            ListaDataPreConciliacion = iBLPreConciliacion.CargarDataConciliacionBCP(request, IdPreConArchivo.ToString)
                            request.Archivo = New BEConciliacionArchivo
                            request.Archivo.IdConciliacionArchivo = IdPreConArchivo
                            'Dim RutaCompleta As String = AppSettings("RutaArchivosPreConciliacion") & _
                            '    request.CodigoBanco & "_" & request.NombreArchivoPreCon
                            'Using strm As New IO.FileStream(RutaCompleta, IO.FileMode.Create)
                            '    strm.Write(request.Bytes, 0, request.Bytes.Length)
                            '    strm.Close()
                            'End Using
                            iBLPreConciliacion.ProcesarArchivoPreConciliacion(request, ListaDataPreConciliacion)
                            oDAConciliacion.ActualizarEstadoArchivoPreConciliacion(request)
                            With response
                                .BMResultState = _3Dev.FW.Entidades.BMResult.Ok
                                .ListaOperacionesConciliadas = request.ListaOperacionesConciliadas
                                .ListaOperacionesNoConciliadas = request.ListaOperacionesNoConciliadas
                            End With
                        End If
                    End Using
                End Using
            Else
                Using iBLPreConciliacion As IBLPreConciliacion = CrearIBLPreConciliacion(request.CodigoBanco)
                    Dim ListaDataPreConciliacion As List(Of BEDataPreConciliacion)
                    Using iExcelDataReader As Excel.IExcelDataReader = CrearIExcelDataReader(request)
                        Dim CantCar As Integer = request.NombreArchivoPreCon.Length()
                        Dim Nombre As String = request.NombreArchivoPreCon.Remove((CantCar - 4), 4)
                        request.NombreArchivoPreCon = Nombre & System.DateTime.Now.ToString("_yyyyMMddhmmss") & ".xls"
                        ListaDataPreConciliacion = iBLPreConciliacion.CargarDataConciliacion(iExcelDataReader)
                        iExcelDataReader.Close()
                    End Using
                    Using oDAConciliacion As New DAConciliacion

                        If ListaDataPreConciliacion(6).Campo0.Trim.ToUpper.Contains("USD") Then
                            request.IdMoneda = 2
                        Else
                            request.IdMoneda = 1
                        End If

                        Dim IdPreConArchivo = oDAConciliacion.RegistrarPreConciliacionArchivo(request)
                        If IdPreConArchivo = -2 Then
                            Throw New Exception("No existe el Banco especificado.")
                        ElseIf IdPreConArchivo = -1 Then
                            Throw New Exception("Ya existe un archivo con el mismo nombre.")
                        Else
                            request.Archivo = New BEConciliacionArchivo
                            request.Archivo.IdConciliacionArchivo = IdPreConArchivo
                            Dim RutaCompleta As String = AppSettings("RutaArchivosPreConciliacion") & _
                                request.CodigoBanco & "_" & request.NombreArchivoPreCon
                            Using strm As New IO.FileStream(RutaCompleta, IO.FileMode.Create)
                                strm.Write(request.Bytes, 0, request.Bytes.Length)
                                strm.Close()
                            End Using
                            iBLPreConciliacion.ProcesarArchivoPreConciliacion(request, ListaDataPreConciliacion)
                            oDAConciliacion.ActualizarEstadoArchivoPreConciliacion(request)
                            With response
                                .BMResultState = _3Dev.FW.Entidades.BMResult.Ok
                                .ListaOperacionesConciliadas = request.ListaOperacionesConciliadas
                                .ListaOperacionesNoConciliadas = request.ListaOperacionesNoConciliadas
                            End With
                        End If
                    End Using
                End Using
            End If

        Catch ex As FWException
            response.BMResultState = _3Dev.FW.Entidades.BMResult.No
            response.Message = ex.Message
        Catch ex As Exception
            'Logger.Write(ex)
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Error
            response.Message = ex.Message.ToString
        End Try
        Return response
    End Function

#End Region

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class