Imports SPE.Entidades

Public Class BLCuenta
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase
    Public Function ConsultarMovimientoCuenta(ByVal be As BEMovimientoCuentaBanco) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim oDA As New SPE.AccesoDatos.DACuenta
        Return oDA.ConsultarMovimientoCuenta(be)
    End Function
    Public Function ConsultarDetMovCuentaPorIdMovCuentaBanco(ByVal be As BEDetMovimientoCuenta) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim oDA As New SPE.AccesoDatos.DACuenta
        Return oDA.ConsultarDetMovCuentaPorIdMovCuentaBanco(be)
    End Function

End Class
