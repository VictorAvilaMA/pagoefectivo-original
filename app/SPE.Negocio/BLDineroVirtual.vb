﻿Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.EmsambladoComun
Imports System.Configuration
Imports SPE.EmsambladoComun.ParametrosSistema
'Imports SPE.EmsambladoComun.ParametrosSistema.WSBancoMensaje.FC

Public Class BLDineroVirtual

#Region "Victor"

    Function RegistrarCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As Integer
        '
        Dim oDADineroVirtual As New DADineroVirtual
        Dim entidad As New BECuentaDineroVirtual
        entidad = oDADineroVirtual.RegistrarCuentaDineroVirtual(oBECuentaDineroVirtual)
        If entidad.ParametrosEmailMonedero.AliasCuenta.Trim() = "" Then
            entidad.ParametrosEmailMonedero.AliasCuenta = "- - - -"
        End If


        If (entidad.ParametrosEmailMonedero.ResultadoInt = 2) Then
            'Enviar Correo
            Dim bl As New BLEmail
            bl.EnviarCorreoSolicitudCreacionCuenta(entidad)
            Return 2
        Else : Return entidad.ParametrosEmailMonedero.ResultadoInt
        End If
        '
    End Function

    Function ActualizarEstadoCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As Integer




        Using Ambito As New TransactionScope()
            Dim oDADineroVirtual As New DADineroVirtual
            Dim result As Integer = oDADineroVirtual.ActualizarEstadoCuentaDineroVirtual(oBECuentaDineroVirtual)
            Dim entidadT As New BECuentaDineroVirtual
            Dim oBody As New BEParametrosEmailMonedero
            entidadT = ConsultarCuentaDineroVirtualUsuarioByCuentaId(oBECuentaDineroVirtual)
            oBody.NombreCliente = entidadT.ClienteNombre.ToString
            oBody.CuentaOrigen = entidadT.Numero.ToString
            oBody.DescripcionMoneda = entidadT.MonedaDescripcion.ToString
            oBody.Monto = entidadT.Saldo
            oBody.FechaSolicitud = entidadT.FechaSolicitud
            oBody.PTo = entidadT.Email
            oBody.Observacion = entidadT.ObservacionBloqueo
            Dim objEmail As New BLEmail()
            Select Case oBECuentaDineroVirtual.IdEstado
                Case SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Inhabilitado
                    objEmail.EnviarCorreoCuentaHabilitadaCliente(oBody) 'MDSEMAIL
                Case SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado
                    objEmail.EnviarCorreoCuentaInhabilitadaCliente(oBody) 'MDSEMAIL
            End Select


            Ambito.Complete()
            Return result
        End Using


    End Function

    Function ConsultarMovimientoXIdCuenta(ByVal entidad As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)
        Return New DADineroVirtual().ConsultarMovimientoXIdCuenta(entidad)
    End Function

    Function ConsultarMovimientoXIdCuentaExportar(ByVal entidad As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)
        Return New DADineroVirtual().ConsultarMovimientoXIdCuentaExportar(entidad)
    End Function

    Function ActualizarMontoRecargaDineroVirtual(ByVal olistaBEMontoRecargaMonedero As List(Of BEMontoRecargaMonedero)) As Integer
        Dim oDADineroVirtual As New DADineroVirtual
        Dim res As Integer
        For Each thisObject As BEMontoRecargaMonedero In olistaBEMontoRecargaMonedero
            res = oDADineroVirtual.ActualizarMontoRecargaDineroVirtual(thisObject)
        Next
        Return res
    End Function

    Function ConsultarMonedaXIdCliente(ByVal entidad As BEMontoRecargaMonedero) As List(Of BEMontoRecargaMonedero)
        Return New DADineroVirtual().ConsultarMonedaXIdCliente(entidad)
    End Function

    Function ConsultarClienteXIdClienteDV(ByVal entidad As BECliente) As BECliente
        Return New DADineroVirtual().ConsultarClienteXIdClienteDV(entidad)
    End Function

    Function ActualizarEstadoHabilitarMonederoClienteDV(ByVal entidad As BECliente) As Integer
        Dim oDADineroVirtual As New DADineroVirtual
        Dim oEmail As New BLEmail
        Dim res As Integer
        res = oDADineroVirtual.ActualizarEstadoHabilitarMonederoClienteDV(entidad)
        Select Case entidad.HabilitarMonedero
            Case 0
                oEmail.EnviarCorreoUsuarioBloqueado(entidad)
            Case 1
                oEmail.EnviarCorreoUsuarioDesbloqueado(entidad)
        End Select
        Return res
    End Function

    Function ConsultarMonedaMonederoVirtual() As List(Of BEMoneda)
        Dim oDADineroVirtual As New DADineroVirtual
        Return oDADineroVirtual.ConsultarMonedaMonederoVirtual()
    End Function



#End Region

#Region "Joe"
    Function ActualizarCuentaDineroVirtual(ByVal entidad As BECuentaDineroVirtual) As Boolean
        Dim oDADineroVirtual As New DADineroVirtual

        'ConsultarCuentaDineroVirtualUsuarioByCuentaId

        Dim result As Boolean
        Using Ambito As New TransactionScope()
            Try
                Dim estado As Integer
                estado = entidad.IdEstado
                '1º REGISTRAMOS LA TRANSACCION EN BD
                result = oDADineroVirtual.ActualizarCuentaDineroVirtual(entidad)
                '2º ENVIO DE EMAIL

                Dim objEmail As New BLEmail()
                Dim objBody As New BEParametrosEmailMonedero
                Dim objCMV As New BECuentaDineroVirtual
                objCMV = ConsultarCuentaDineroVirtualUsuarioByCuentaId(entidad)
                objBody.NombreCliente = objCMV.ClienteNombre
                objBody.NumeroSolicitud = objCMV.Numero
                objBody.DescripcionMoneda = objCMV.MonedaDescripcion
                objBody.FechaSolicitud = objCMV.FechaSolicitud
                objBody.AliasCuenta = objCMV.AliasCuenta
                objBody.PTo = objCMV.Email
                entidad.IdUsuario = objCMV.IdUsuario

                Select Case estado
                    Case SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado
                        Dim dv As New DADineroVirtual

                        If dv.OperacionCuenta(entidad.IdUsuario, "C") Then     'true el cliente ya fue notificado
                            objEmail.EnviarCorreoCuentaAprobadaCliente(objBody)

                        Else
                            objEmail.EnviarCorreoCuentaAprobadaClientePrimeraVez(objBody) ' es la primera vez
                            dv.OperacionCuenta(entidad.IdUsuario, "U")
                        End If
                    Case SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Rechazado
                        objEmail.EnviarCorreoCuentaRechazadaCliente(objBody)
                End Select

                Ambito.Complete()
            Catch ex As Exception
                'Ambito.Dispose()
                Throw ex
            End Try
        End Using


        Return result
    End Function



    Function ConsultarCuentaDineroVirtualUsuarioByNumero(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual
        Return New DADineroVirtual().ConsultarCuentaDineroVirtualUsuarioByNumero(entidad)
    End Function


    Function ConsultarCuentaDineroVirtualUsuarioByCuentaId(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual
        Return New DADineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(entidad)
    End Function

    Function ConsultarCuentaDineroVirtualUsuario(ByVal entidad As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)
        Return New DADineroVirtual().ConsultarCuentaDineroVirtualUsuario(entidad)
    End Function

    Function ConsultarSolicitudRetiroMonedero(ByVal entidad As BESolicitudRetiroDineroVirtual) As List(Of BESolicitudRetiroDineroVirtual)
        Return New DADineroVirtual().ConsultarSolicitudRetiroMonedero(entidad)
    End Function


    Function ConsultarSolicitudRetiroMonederoById(ByVal entidad As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual
        Return New DADineroVirtual().ConsultarSolicitudRetiroMonederoById(entidad)
    End Function

    Function ActualizarSolicitudRetiroDinero(ByVal entidad As BESolicitudRetiroDineroVirtual) As Boolean
        Dim oDADineroVirtual As New DADineroVirtual
        Dim result As Boolean
        Using Ambito As New TransactionScope()
            Try
                'Capturamos el estado con el que llega la solicitud
                Dim estado As Integer = entidad.IdEstado
                '1º REGISTRAMOS LA TRANSACCION EN BD
                result = oDADineroVirtual.ActualizarSolicitudRetiroDinero(entidad)
                '2º ENVIO DE EMAIL

                Dim objEmail As New BLEmail()
                Dim objBody As New BEParametrosEmailMonedero
                Dim objCMV As New BESolicitudRetiroDineroVirtual

                objCMV = ConsultarSolicitudRetiroMonederoById(entidad)
                objBody.NombreCliente = objCMV.ClienteNombre
                objBody.NumeroSolicitud = objCMV.IdSolicitud
                objBody.CuentaOrigen = objCMV.Numero
                objBody.SimboloMoneda = objCMV.MonedaSimbolo

                objBody.ConceptoPago = objCMV.InfoSolicitud 'Monto COncatenado al simbolo de moneda
                objBody.NombreBanco = objCMV.BancoDestinoDescripcion
                objBody.CuentaDestino = objCMV.CuentaBancariaDestino
                objBody.TitularCuentaDestino = objCMV.TitularCuentaBancariaDestino
                objBody.Observacion = objCMV.ObservacionAdministrador
                objBody.FechaSolicitud = objCMV.FechaSolicitud
                objBody.PTo = objCMV.Email

                If result Then
                    Select Case estado
                        Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Aprobado  'Admin 
                            'objEmail.EnviarCorreoAprobacionRetiroCliente(objBody)
                            objEmail.EnviarCorreoAprobacionRetiroTesoreria(objBody)
                        Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Rechazado 'Admin
                            objEmail.EnviarCorreoRechazadoRetiroCliente(objBody)
                        Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Liquidado 'Tesoreria
                            objEmail.EnviarCorreoCuentaLiquidacionAprobadaCliente(objBody)
                        Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Anulado   'Tesoreria
                            objBody.Observacion = objCMV.ObservacionTesoreria
                            objEmail.EnviarCorreoCuentaLiquidacionRechazadaCliente(objBody)
                    End Select
                End If

                Ambito.Complete()
            Catch ex As Exception
                'Ambito.Dispose()
                Throw ex
            End Try
        End Using


        Return result

    End Function

#End Region

#Region "IMPLEMENTADO POR MXRT¡N"
    Function ConsultarCuentaDineroVirtualAdministrador(ByVal entidad As BECuentaDineroVirtual) As List(Of BECuentaDineroVirtual)
        Return New DADineroVirtual().ConsultarCuentaDineroVirtualAdministrador(entidad)
    End Function

    Public Function ObtenerOrdenPagoCompletoPorNroOrdenPago(ByVal nroOrdenPago As Long) As BEOrdenPago
        Return New DADineroVirtual().ObtenerOrdenPagoCompletoPorNroOrdenPago(nroOrdenPago)
    End Function

    Public Function RegistrarTokenMonedero(ByVal token As BETokenDineroVirtual) As BETokenDineroVirtual
        '1º REGISTRAMOS EL TOKEN EN LA BD
        Dim objToken As BETokenDineroVirtual = New DADineroVirtual().RegistrarTokenMonedero(token)

        '2º ENVIAMOS EL TOKEN GENERADO POR CORREO
        Dim objEmail As New BLEmail
        objEmail.EnviarCorreoGeneracionToken(objToken)
        objToken.Token = ""
        Return objToken
    End Function

    Public Function RegistrarPagoCIP(ByVal beMovimiento As BEMovimientoCuenta) As String

        Using Ambito As New TransactionScope()

            '1º REGISTRAMOS LA TRANSACCION EN BD
            Dim oBeMovimiento As BEMovimientoCuenta = New DADineroVirtual().RegistrarPagoCIP(beMovimiento)

            If (Not oBeMovimiento.Observacion.Equals("")) Then 'si la transaccion retorna algun mensaje de error
                Return oBeMovimiento.Observacion
            End If

            '2º REGISTRAMOS LA NOTIFICACION DEL SERVICIO
            Dim obeOrdenPagoCompleto As BEOrdenPago = Nothing
            Using odaOrdenPago1 As New DAOrdenPago()
                obeOrdenPagoCompleto = odaOrdenPago1.ObtenerOrdenPagoCompletoPorNroOrdenPago(beMovimiento.CodigoReferencia)

            End Using

            Dim obeServicio As BEServicio
            Using odaServicio As New DAServicio()
                obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagoCompleto.IdServicio), BEServicio)
            End Using

            If obeServicio.FlgNotificaMovimiento = True Then
                Using oblServicionNotificacion As New BLServicioNotificacion()
                    oblServicionNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPagoCompleto, oBeMovimiento.IdMovimientoAsociado, beMovimiento.IdUsuario, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.Web, obeServicio.IdGrupoNotificacion)
                End Using
            End If

            '3º ENVIO DE EMAIL
            Dim objEmail As New BLEmail()
            'No es DV
            Dim obeCliente As New BECliente
            obeCliente = New DACliente().ConsultarClientePorId(obeOrdenPagoCompleto.IdCliente)
            'Comentar Luego de Probar
            'obeCliente.Email = "victor.avila@perucomsultores.pe"
            objEmail.EnviarCorreoCancelacionOrdenPago(obeOrdenPagoCompleto, obeCliente.Email)

            'De Dinero Virtual
            Dim cuenta As New BECuentaDineroVirtual
            cuenta.IdCuentaDineroVirtual = beMovimiento.IdCuentaVirtual
            cuenta = New DADineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(cuenta)
            obeOrdenPagoCompleto.UsuarioEmail = cuenta.Email
            objEmail.EnviarCorreoCIPPagada(obeOrdenPagoCompleto, oBeMovimiento) 'MDSEMAIL




            Ambito.Complete()
            Return oBeMovimiento.Observacion
        End Using
    End Function

    Public Function RegistrarPagoCIPPortal(ByVal beMovimiento As BEMovimientoCuenta, ByVal oBESolicitudPago As BESolicitudPago) As String
        'Dim result As String
        Dim result As BEMovimientoCuenta
        Using Ambito As New TransactionScope()
            Try

                '1º REGISTRAMOS LA TRANSACCION EN BD
                result = New DADineroVirtual().RegistrarPagoCIPPortal(beMovimiento)
                If (Not result.Observacion.Equals("")) Then 'si la transaccion retorna algun mensaje de error
                    Return result.Observacion
                End If


                '2º REGISTRAMOS LA NOTIFICACION DEL SERVICIO
                'Descomentar Victor

                If ConfigurationManager.AppSettings("Notificar") Then
                    Dim oBLServicioNotificacion As New BLServicioNotificacion
                    oBLServicioNotificacion.RegistrarServicioNotificacionUrlSolicitudPago(oBESolicitudPago, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.Web)
                End If


                '3º ENVIO DE EMAIL
                Dim obeOrdenPagoCompleto As BEOrdenPago = Nothing
                Using odaOrdenPago1 As New DAOrdenPago()
                    obeOrdenPagoCompleto = odaOrdenPago1.ObtenerOrdenPagoCompletoPorNroOrdenPago(beMovimiento.CodigoReferencia)
                End Using

                Dim oFunc As New DADineroVirtual
                Dim oEmail As New BEParametrosEmailMonedero
                oEmail = oFunc.ObtenerSolicitudPagoPortal(beMovimiento)
                oEmail.PTo = oBESolicitudPago.UsuarioEmail 'MDSEMAIL
                Dim objEmail As New BLEmail()
                objEmail.EnviarCorreoCIPPagadaPortal(oEmail, result)

                Ambito.Complete()
            Catch ex As Exception
                'Ambito.Dispose()
                Throw ex
            End Try
        End Using
        Return result.Observacion

    End Function

    Public Function RegistrarSolicitudRetiroDinero(ByVal beSolicitudRetiroDinero As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual
        Dim result As BESolicitudRetiroDineroVirtual
        Using Ambito As New TransactionScope()
            Try

                '1º REGISTRAMOS LA TRANSACCION EN BD
                result = New DADineroVirtual().RegistrarSolicitudRetiroDinero(beSolicitudRetiroDinero)

                '2º ENVIO DE EMAIL

                Dim objEmail As New BLEmail()
                Dim oFunc As New DADineroVirtual
                Dim oBody As BEParametrosEmailMonedero
                oBody = oFunc.ObtenerSolicitudRetiroEmail(beSolicitudRetiroDinero)

                objEmail.EnviarCorreoSolicitudRetiroCliente(oBody, beSolicitudRetiroDinero.Email)
                objEmail.EnviarCorreoSolicitudRetiroAdmin(oBody)

                Ambito.Complete()
            Catch ex As Exception
                'Ambito.Dispose()
                Throw ex
            End Try
        End Using
        Return result
    End Function

    Function ConsultarCuentaDineroVirtualPorNroCuenta(ByVal NroCuenta As String) As BECuentaDineroVirtual
        Return New DADineroVirtual().ConsultarCuentaDineroVirtualPorNroCuenta(NroCuenta)
    End Function

    Public Function RegistrarTransferenciaDinero(ByVal beMovimiento As BEMovimientoCuenta) As String
        Dim da As New DADineroVirtual
        Dim entidad As New BEParametrosEmailMonedero
        entidad = da.RegistrarTransferenciaDinero(beMovimiento)

        If entidad.AliasCuenta = "" Then
            'Enviar Correo
            Dim bl As New BLEmail
            bl.EnviarCorreoAlRegistrarTransferenciaDineroOrigen(entidad)
            bl.EnviarCorreoAlRegistrarTransferenciaDineroDestino(entidad)
        End If
        Return entidad.AliasCuenta
    End Function

    Function ConsultarReglaMovimientoSospechoso() As List(Of BEReglaMovimientoSospechoso)
        Return New DADineroVirtual().ConsultarReglaMovimientoSospechoso()
    End Function

    Function ConsultarRegistroEjecucion(ByVal IdRegla As Integer, ByVal Anio As Integer, ByVal Mes As Integer) As List(Of BERegistroEjecucionRegla)
        Return New DADineroVirtual().ConsultarRegistroEjecucion(IdRegla, Anio, Mes)
    End Function

    Function ConsultarResultadoEjecucionRegla(ByVal entidad As BEResultadoEjecucionRegla) As List(Of BEResultadoEjecucionRegla)
        Return New DADineroVirtual().ConsultarResultadoEjecucionRegla(entidad)
    End Function

    Function ConsultarDetalleResultadoEjecucion(ByVal entidad As BEDetalleResultadoEjecucion) As List(Of BEDetalleResultadoEjecucion)
        Return New DADineroVirtual().ConsultarDetalleResultadoEjecucion(entidad)
    End Function

    Function ConsultarTokenByID(ByVal IdToken As Long) As BETokenDineroVirtual
        Dim daDinerV As New DADineroVirtual
        Return daDinerV.ObtenerTokenMonedero(IdToken)
    End Function



    Function ReenviarEmailToken(ByVal IdToken As Long) As BETokenDineroVirtual
        Dim token As New BETokenDineroVirtual
        Dim daDinerV As New DADineroVirtual
        Dim result As New BETokenDineroVirtual
        token = daDinerV.ObtenerTokenMonedero(IdToken)
        If (token Is Nothing) Then
            result.Token = "El Token es inválido."
            result.IdTokenDineroVirtual = 0
            Return result
        End If
        If Not token.Activo Then
            result = RegistrarTokenMonedero(token)
            result.Token = "El Token ha expirado.Se ha enviado un nuevo Token a su correo"
            Return result
        End If
        Dim objEmail As New BLEmail
        objEmail.EnviarCorreoGeneracionToken(token)
        result.Token = ""
        result.IdTokenDineroVirtual = 0
        Return result
    End Function
#End Region

    'MDS
    Public Function ValidarRegistrarCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual
        Dim obResultado As New DADineroVirtual
        Return obResultado.ValidarRegistrarCuentaDineroVirtual(obecuentadinerovirtual)
    End Function

    Public Function OperacionCuentaGenerico(ByVal cta As BECuentaDineroVirtual, ByVal mov As BEMovimiento, ByVal str As String) As Long
        Try
            Dim NotificarRegistroAgenciaOTerminal As Boolean = False
            Dim NotificarRegistroPuntoVenta As Boolean = False
            Dim oDAAgenciaBancaria As New DAAgenciaBancaria
            Dim dv As New DADineroVirtual()
            Using oDAAgenciaBancaria
                Select Case mov.CodigoBanco
                    Case Conciliacion.CodigoBancos.FullCarga
                        'Para FullCarga: Registrar punto de venta y terminal
                        Using oDAPuntoVenta As New DAPuntoVenta
                            Dim AperturaOrigen As Hashtable = oDAPuntoVenta.RegistrarPuntoVentaYTerminalDesdePago(mov.CodigoPuntoVenta, _
                                mov.NumeroSerieTerminal)
                            If AperturaOrigen Is Nothing Then
                                'La apertura origen no se registró correctamente
                                Return 0
                            Else
                                With mov
                                    .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                    .IdTipoOrigenCancelacion = TipoOrigenCancelacion.FullCarga
                                End With
                                NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroTerminal"))
                                NotificarRegistroPuntoVenta = Convert.ToBoolean(AperturaOrigen("RegistroPuntoVenta"))
                            End If
                        End Using
                    Case Conciliacion.CodigoBancos.BBVA
                        'Para BBVA: Registrar agencia bancaria
                        Dim AperturaOrigen As Hashtable = oDAAgenciaBancaria.RegistrarAgenciaBancariaDesdePago(mov.CodigoBanco, _
                        mov.CodigoAgenciaBancaria)
                        If AperturaOrigen Is Nothing Then
                            'La apertura origen no se registró correctamente
                            Return 0
                        Else
                            With mov
                                .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                .IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                            End With
                            NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroAgenciaBancaria"))
                        End If
                    Case Conciliacion.CodigoBancos.BCP
                        'Para BCP: Registrar agencia bancaria
                        Dim AperturaOrigen As Hashtable = oDAAgenciaBancaria.RegistrarAgenciaBancariaDesdePago(mov.CodigoBanco, _
                        mov.CodigoAgenciaBancaria)
                        If AperturaOrigen Is Nothing Then
                            'La apertura origen no se registró correctamente
                            Return 0
                        Else
                            With mov
                                .IdAperturaOrigen = AperturaOrigen("IdAperturaOrigen")
                                .IdTipoOrigenCancelacion = TipoOrigenCancelacion.AgenciaBancaria
                            End With
                            NotificarRegistroAgenciaOTerminal = Convert.ToBoolean(AperturaOrigen("RegistroAgenciaBancaria"))
                        End If
                    Case Conciliacion.CodigoBancos.Interbank
                    Case Conciliacion.CodigoBancos.Scotiabank
                    Case Else

                        'Código de banco incorrecto o sin implementar
                        Return 0
                End Select
                Dim IdTransaccion As Long = dv.RecargarCuenta(cta, str, mov)
                If NotificarRegistroAgenciaOTerminal Then
                    Select Case mov.CodigoBanco
                        Case Conciliacion.CodigoBancos.FullCarga
                            Using oBLEmail As New BLEmail()
                                If oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(NotificarRegistroPuntoVenta, _
                                    mov.CodigoPuntoVenta, mov.NumeroSerieTerminal, _
                                    ProcesoCancelacionFC) <> 1 Then
                                    'La notificación al administrador no se envió correctamente
                                    Return -9
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.BBVA
                            Using oBLEmail As New BLEmail()
                                If oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(mov.CodigoAgenciaBancaria, _
                                    ProcesoCancelacionOP, mov.CodigoBanco) <> 1 Then
                                    'La notificación al administrador no se envió correctamente
                                    Return -5
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.BCP
                            Using oBLEmail As New BLEmail()
                                If oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(mov.CodigoAgenciaBancaria, _
                                    ProcesoCancelacionOP, mov.CodigoBanco) <> 1 Then
                                    'La notificación al administrador no se envió correctamente
                                    Return -5
                                End If
                            End Using
                        Case Conciliacion.CodigoBancos.Interbank
                        Case Conciliacion.CodigoBancos.Scotiabank

                        Case Else
                            'Código de banco incorrecto o sin implementar
                            Return 0
                    End Select
                End If
                Return IdTransaccion
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Function ObtenerParametroMonederoByID(Idparametro As Integer) As BEParametroMonedero
        Dim obResultado As New DADineroVirtual
        Return obResultado.ObtenerParametroMonederoByID(Idparametro)
    End Function

    Function ConsultaUsuarioByIdCuenta(IdCuenta As Integer) As Integer
        Dim obResultado As New DADineroVirtual
        Return obResultado.ConsultaUsuarioByIdCuenta(IdCuenta)
    End Function

    Function ConsultaEstadoUsuarioByIdUsuario(IdUsuario As Integer) As Integer
        Dim obResultado As New DADineroVirtual
        Return obResultado.ConsultaEstadoUsuarioByIdUsuario(IdUsuario)
    End Function




End Class
