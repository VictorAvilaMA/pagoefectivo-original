Imports System.Linq
Imports System.Net.Mail
Imports SPE.EmsambladoComun
Imports System.Configuration
Imports System.IO
Imports SPE.Entidades
Imports System.Globalization.CultureInfo
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports System.Threading
Imports SPE.AccesoDatos
Imports System.Text.RegularExpressions
Imports SPE.Logging

Public Class BLEmail
    Implements IDisposable
    Private ReadOnly _nlogger As ILogger = New Logger()

    '************************************************************************************************** 
    ' M�todo          : EnviarCorreoAAdministrador
    ' Descripci�n     : Enviar el Correo al Administrador
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : pfrom,psubject,pbody
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarCorreoAAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer
        Dim pto As String = ConfigurationManager.AppSettings("AdministradorEmail")
        Return EnviarEmail(pfrom, pto, psubject, pbody, True)
    End Function

    Public Function EnviarCorreoContactarAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer
        Dim pto As String = ConfigurationManager.AppSettings("ContactarAdministradorEmail")
        Return EnviarEmail(pfrom, pto, psubject, pbody, True)
    End Function

    Public Function EnviarEmailContactenos(ByVal obeMail As SPE.Entidades.BEMailContactenos) As Integer
        With obeMail
            Dim pTo As String = ConfigurationManager.AppSettings("ContactenosEmail")
            Dim pFrom As String = ConfigurationManager.AppSettings("ClienteContactenosEmail")

            Dim sbContenido As New System.Text.StringBuilder()

            sbContenido.Append("Sres. PagoEfectivo: ")
            sbContenido.Append("<br/><br/>Contenido: <br/>" + .Contenido)
            sbContenido.Append("<br/><br/>Atentamente<br/>" + .NombreCompleto)
            sbContenido.Append("<br/><br/>Correo: " + .Email)
            sbContenido.Append("<br/>Tel�fono: " + .Tema)

            Return EnviarEmail(pFrom, pTo, "Cont�ctenos", sbContenido.ToString(), True)
        End With
    End Function

    '************************************************************************************************** 
    ' M�todo          : EnviarCorreoConfirmacionUsuario
    ' Descripci�n     : Enviar el Correo de confirmacion al usuario que se registro
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : userId,nombrecliente,clave, pguid , pto , optional bcc
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarCorreoConfirmacionUsuario(ByVal userId As Integer, ByVal nombrecliente As String, ByVal clave As String, ByVal pguid As String, ByVal pto As String, Optional ByVal bcc As String = "") As Integer
        Try

            Dim pfrom As String = ConfigurationManager.AppSettings("RegistroEmail")
            Dim psubject As String = ConfigurationManager.AppSettings("RegistroTemaEmail")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RegistroEmailPlantillaRuta")
            Dim pbody As String = ""
            Using fs As StreamReader = File.OpenText(rutaplantilla)
                pbody = fs.ReadToEnd()
            End Using
            pbody = pbody.Replace("[Nombre]", nombrecliente)
            pbody = pbody.Replace("[cuenta]", pto)
            pbody = pbody.Replace("[contrasenia]", clave)
            pbody = pbody.Replace("[codigoregistro]", pguid)
            Return EnviarEmailPagoEfectivo(pfrom, pto, psubject, pbody, True, bcc)

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCancelacionOrdenPago(ByVal obeOP As BEOrdenPago, ByVal pto As String) As Integer
        Try
            If ((Not obeOP.ServicioUsaUsuariosAnonimos) And pto <> "") And obeOP.oBEServicio.FlgEmailPagoCIP Then
                obeOP.EmailANotifGeneracion = obeOP.UsuarioEmail
                If (obeOP.ServicioUsaUsuariosAnonimos) Then
                    ' strClienteDescripcion = beOrdenP.UsuarioNombre
                    obeOP.DescripcionCliente = obeOP.UsuarioNombre
                    obeOP.EmailANotifGeneracion = pto
                Else
                    If (ObjectToString(obeOP.DescripcionCliente) <> "") Then
                        'strClienteDescripcion = beOrdenP.DescripcionCliente
                        obeOP.DescripcionCliente = obeOP.DescripcionCliente
                    Else
                        'strClienteDescripcion = beOrdenP.UsuarioNombre
                        obeOP.DescripcionCliente = obeOP.UsuarioNombre
                    End If
                End If
                Using oblPlantilla As New BLPlantilla()
                    Dim valoresDinamicos As New Dictionary(Of String, String)()
                    valoresDinamicos.Add("[UsuarioNombre]", obeOP.DescripcionCliente)
                    valoresDinamicos.Add("[CIP]", obeOP.IdOrdenPago)
                    If (obeOP.OcultarEmpresa) Then
                        valoresDinamicos.Add("[EmpresaContratanteNombre]", obeOP.DescripcionEmpresa)
                    End If
                    valoresDinamicos.Add("[ServicioNombre]", obeOP.DescripcionServicio)
                    valoresDinamicos.Add("[ConceptoPago]", obeOP.ConceptoPago)
                    valoresDinamicos.Add("[Moneda]", obeOP.SimboloMoneda)
                    valoresDinamicos.Add("[Monto]", obeOP.Total.ToString("#,#0.00"))
                    If obeOP.IdMoneda = 1 Then
                        valoresDinamicos.Add("[MonedaNombre]", "Soles")
                    Else
                        valoresDinamicos.Add("[MonedaNombre]", "D�lares")
                    End If

                    Using objcipComun As New CIPComun()
                        valoresDinamicos.Add("[CIPCodigoBarra]", objcipComun.GenerarCodigoBarra(obeOP.NumeroOrdenPago, obeOP.ClaveAPI, obeOP.ClaveSecreta))
                    End Using

                    valoresDinamicos.Add("[idEmpresaContratante]", obeOP.IdEmpresa)
                    valoresDinamicos.Add("[idServicio]", obeOP.IdServicio)

                    Dim strPlantillaTipo As String = ""
                    Select Case obeOP.IdMoneda
                        Case 1
                            strPlantillaTipo = ParametrosSistema.Plantilla.Tipo.EmailCanCIP
                        Case 2
                            strPlantillaTipo = ParametrosSistema.Plantilla.Tipo.EmailCanCIP & "DolarAM"
                    End Select

                    'Correo de copia en caso este configurado por el servicio
                    Dim correoCCAdmin As String = IIf(obeOP.oBEServicio.FlgEmailCCAdmin, obeOP.MailComercio, String.Empty)

                    Dim oBEServicio As New BEServicio
                    oBEServicio = New BLServicio().ObtenerServicioPorIdWS(obeOP.IdServicio)


                    If oBEServicio.PlantillaFormato = 53 Then
                        Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaHtml(New BEGenPlantillaHtmlRequest(obeOP.IdServicio, strPlantillaTipo, valoresDinamicos, obeOP.CodPlantillaTipoVariacion))
                        Dim pfrom As String = resGenHtmlResul.ParamValores("[EmailFrom]")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Dim psubjectFix As String = psubject.Replace("[ServicioNombre]", obeOP.DescripcionServicio)
                        Dim pHtmlFix As String = resGenHtmlResul.Html.Replace("[ServicioNombre]", obeOP.DescripcionServicio)


                        Dim objParametros As New Parametros()
                        objParametros.pfrom = pfrom
                        objParametros.EmailANotifGeneracion = obeOP.EmailANotifGeneracion
                        objParametros.psubjectFix = psubjectFix
                        objParametros.pHtmlFix = pHtmlFix
                        objParametros.pisBodyHtml = True
                        objParametros.correoCCAdmin = correoCCAdmin

                        _nlogger.Info(String.Format("EnviarCorreoCancelacionOrdenPago - From: {0}, To: {1}, Bcc: {2},Subject: {3}, Html: {4}", pfrom, obeOP.EmailANotifGeneracion, correoCCAdmin, psubjectFix, pHtmlFix))
                        Return EnviarEmailPagoEfectivo(pfrom, obeOP.EmailANotifGeneracion, psubjectFix, pHtmlFix, True, correoCCAdmin)
                    Else

                        If (oBEServicio.PlantillaFormato = SPE.EmsambladoComun.ParametrosSistema.PlantillaFormato.Tipo.Estandar) Then
                            strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.ConfPago & "Est"
                        Else
                            strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.ConfPago & "Exp"
                        End If

                        Dim obePlantilla As BEPlantilla = New BLPlantilla().ConsultarPlantillaYParametrosPorTipoYVariacion(oBEServicio.IdServicio, strPlantillaTipo, "st")
                        Dim fileHtml = ObtenerPlantillaS3(obePlantilla.FileS3)

                        If (Not fileHtml = "") Then
                            Dim lstSecciones As List(Of BEPlantillaSeccion) = New BLPlantilla().ObtenerSeccionesPlantillaPorServicio(oBEServicio.IdServicio, obePlantilla.IdPlantillaTipo)
                            For i As Integer = 0 To lstSecciones.Count - 1
                                fileHtml = fileHtml.Replace("[Seccion" + lstSecciones(i).IdSeccion.ToString + "]", lstSecciones(i).Contenido)
                            Next
                            For Each param As KeyValuePair(Of String, String) In valoresDinamicos
                                fileHtml = fileHtml.Replace(param.Key, param.Value)
                            Next

                            For i As Integer = 0 To obePlantilla.ListaParametros.Count - 1
                                If (obePlantilla.ListaParametros(i).IdTipoParametro = 411) Then
                                    fileHtml = fileHtml.Replace(obePlantilla.ListaParametros(i).Etiqueta.ToString, obePlantilla.ListaParametros(i).Valor.ToString)
                                End If
                            Next


                            Dim emailfrom = If(obePlantilla.ListaParametros.Where(Function(x) x.Etiqueta = "[EmailFrom]").Count > 0, obePlantilla.ListaParametros.Where(Function(x) x.Etiqueta = "[EmailFrom]")(0).Valor, ConfigurationManager.AppSettings.[Get]("emailFrom"))
                            Dim emailSubject = ConfigurationManager.AppSettings.[Get]("SubjectCancelacion").ToString()

                            Dim pfrom As String = emailfrom
                            Dim psubject As String = emailSubject
                            Dim psubjectFix As String = psubject.Replace("[ServicioNombre]", obeOP.DescripcionServicio)
                            Dim pHtmlFix As String = fileHtml
                            Dim objParametros As New Parametros()
                            objParametros.pfrom = pfrom
                            objParametros.EmailANotifGeneracion = obeOP.EmailANotifGeneracion
                            objParametros.psubjectFix = psubjectFix
                            objParametros.pHtmlFix = pHtmlFix
                            objParametros.pisBodyHtml = True
                            objParametros.correoCCAdmin = correoCCAdmin

                            Return EnviarEmailPagoEfectivo(pfrom, pto, psubjectFix, pHtmlFix, True, correoCCAdmin)
                        Else
                            Exit Function
                        End If
                    End If

                End Using
            Else
                Return 1
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    Class Parametros
        Private _pfrom As String
        Public Property pfrom() As String
            Get
                Return _pfrom
            End Get
            Set(ByVal value As String)
                _pfrom = value
            End Set
        End Property

        Private _EmailANotifGeneracion As String
        Public Property EmailANotifGeneracion() As String
            Get
                Return _EmailANotifGeneracion
            End Get
            Set(ByVal value As String)
                _EmailANotifGeneracion = value
            End Set
        End Property

        Private _psubjectFix As String
        Public Property psubjectFix() As String
            Get
                Return _psubjectFix
            End Get
            Set(ByVal value As String)
                _psubjectFix = value
            End Set
        End Property

        Private _pHtmlFix As String
        Public Property pHtmlFix() As String
            Get
                Return _pHtmlFix
            End Get
            Set(ByVal value As String)
                _pHtmlFix = value
            End Set
        End Property

        Private _pisBodyHtml As Boolean
        Public Property pisBodyHtml() As Boolean
            Get
                Return _pisBodyHtml
            End Get
            Set(ByVal value As Boolean)
                _pisBodyHtml = value
            End Set
        End Property

        Private _correoCCAdmin As String
        Public Property correoCCAdmin() As String
            Get
                Return _correoCCAdmin
            End Get
            Set(ByVal value As String)
                _correoCCAdmin = value
            End Set
        End Property
    End Class

    Public Sub ThreadProc_EnviarEmail(ByVal stateInfo As Object)
        Try
            Dim oParametros As Parametros = CType(stateInfo, Parametros)
            EnviarEmail(oParametros.pfrom, oParametros.EmailANotifGeneracion, oParametros.psubjectFix, oParametros.pHtmlFix, oParametros.pisBodyHtml, oParametros.correoCCAdmin)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw
        End Try
    End Sub
    Public Function EnviarCorreoCancelacionOrdenPagoPOS(ByVal NumeroOrdenPago As String, ByVal OcultarEmpresa As Boolean, ByVal EmpresaContratante As String, ByVal Servicio As String, ByVal Moneda As String, ByVal Monto As String, ByVal Concepto As String, ByVal pto As String) As Integer
        Try
            Dim pfrom As String = ConfigurationManager.AppSettings("CancelacionOPEmail")
            Dim psubject As String = ConfigurationManager.AppSettings("CancelacionOPTemaEmail")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("CancelacionEmailPlantillaRuta")
            Dim empresaContratanteEtiqueta As String = "Empresa Contratante:"
            Dim fs As StreamReader = File.OpenText(rutaplantilla)
            Dim pbody As String = fs.ReadToEnd()
            If (OcultarEmpresa) Then
                empresaContratanteEtiqueta = ""
                EmpresaContratante = ""
            End If
            pbody = pbody.Replace("[NumeroOrdenPago]", NumeroOrdenPago)
            pbody = pbody.Replace("[EmpresaContratanteEtiqueta]", empresaContratanteEtiqueta)
            pbody = pbody.Replace("[EmpresaContratante]", EmpresaContratante)
            pbody = pbody.Replace("[Servicio]", Servicio)
            pbody = pbody.Replace("[Concepto]", Concepto)
            pbody = pbody.Replace("[Moneda]", Moneda)
            pbody = pbody.Replace("[Monto]", _3Dev.FW.Util.DataUtil.ObjectDecimalToStringFormatMiles(Monto))
            Return EnviarEmailPagoEfectivo(pfrom, pto, psubject, pbody, True)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function

    Private _DiasSemana As Dictionary(Of DayOfWeek, String)
    Public ReadOnly Property DiasSemana() As Dictionary(Of DayOfWeek, String)
        Get
            If (_DiasSemana Is Nothing) Then
                _DiasSemana = New Dictionary(Of DayOfWeek, String)
                _DiasSemana.Add(DayOfWeek.Monday, "Lunes")
                _DiasSemana.Add(DayOfWeek.Tuesday, "Martes")
                _DiasSemana.Add(DayOfWeek.Wednesday, "Mi�rcoles")
                _DiasSemana.Add(DayOfWeek.Thursday, "Jueves")
                _DiasSemana.Add(DayOfWeek.Friday, "Viernes")
                _DiasSemana.Add(DayOfWeek.Saturday, "S�bado")
                _DiasSemana.Add(DayOfWeek.Sunday, "Domingo")
            End If
            Return _DiasSemana
        End Get
    End Property

    Public Function EnviarCorreoGeneracionOrdenPago(ByVal obeOP As BEOrdenPago, ByVal pto As String) As Integer
        Try
            If ((Not obeOP.ServicioUsaUsuariosAnonimos) And pto <> "") And obeOP.oBEServicio.FlgEmailGenCIP Then


                Using oblPlantilla As New BLPlantilla()
                    Dim valoresDinamicos As New Dictionary(Of String, String)()

                    valoresDinamicos.Add("[UsuarioNombre]", obeOP.DescripcionCliente)
                    'valoresDinamicos.Add("[UsuarioApellido]", NumeroOrdenPago)               
                    valoresDinamicos.Add("[CIP]", obeOP.IdOrdenPago)
                    valoresDinamicos.Add("[NroPedido]", obeOP.OrderIdComercio)

                    Dim querySecure As TSHAK.Components.SecureQueryString
                    querySecure = New TSHAK.Components.SecureQueryString(New Byte() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8})
                    querySecure("OP") = obeOP.NumeroOrdenPago

                    Dim querystringSeguro As TSHAK.Components.SecureQueryString
                    querystringSeguro = New TSHAK.Components.SecureQueryString(New Byte() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8}, querySecure.ToString())
                    Try
                        Dim cipObtenido = querystringSeguro("OP").ToString()
                    Catch ex As Exception
                    End Try

                    valoresDinamicos.Add("[CIPEncrypted]", System.Web.HttpUtility.UrlEncode(querySecure.ToString()))

                    If (obeOP.OcultarEmpresa) Then
                        valoresDinamicos.Add("[EmpresaContratanteNombre]", obeOP.DescripcionEmpresa)
                    End If
                    valoresDinamicos.Add("[ServicioNombre]", obeOP.DescripcionServicio)
                    valoresDinamicos.Add("[ConceptoPago]", obeOP.ConceptoPago)
                    valoresDinamicos.Add("[Moneda]", obeOP.SimboloMoneda)
                    valoresDinamicos.Add("[Monto]", obeOP.Total.ToString("#,#0.00"))

                    If obeOP.IdMoneda = 1 Then
                        valoresDinamicos.Add("[MonedaNombre]", "Soles")
                    Else
                        valoresDinamicos.Add("[MonedaNombre]", "D�lares")
                    End If

                    Using objcipComun As New CIPComun()
                        valoresDinamicos.Add("[CIPCodigoBarra]", objcipComun.GenerarCodigoBarra(obeOP.NumeroOrdenPago, obeOP.ClaveAPI, obeOP.ClaveSecreta))
                    End Using

                    'obeOP.FechaVencimiento
                    'Dim strFechaVencimiento As String = String.Format("{0} {1}", CurrentCulture.DateTimeFormat.GetDayName(obeOP.FechaVencimiento.DayOfWeek), obeOP.FechaVencimiento.ToString(CurrentCulture.DateTimeFormat.ShortDatePattern))
                    'Dim strHoraVencimiento As String = obeOP.FechaVencimiento.ToString(CurrentCulture.DateTimeFormat.ShortTimePattern)

                    Dim strFechaVencimiento As String = String.Format("{0} {1}/{2}/{3}", DiasSemana(obeOP.FechaVencimiento.DayOfWeek), _
                                                                      obeOP.FechaVencimiento.Day.ToString("00"), _
                                                                      obeOP.FechaVencimiento.Month.ToString("00"), _
                                                                      obeOP.FechaVencimiento.Year.ToString("0000"))
                    Dim strHoraVencimiento As String = ""
                    If obeOP.FechaVencimiento.Hour < 12 Then
                        ' menor a 12 hrs.
                        strHoraVencimiento = obeOP.FechaVencimiento.Hour.ToString("00") + ":" + _
                             obeOP.FechaVencimiento.Minute.ToString("00") + " a.m."

                    Else
                        ' mayor a 12 hrs.

                        If obeOP.FechaVencimiento.Hour <> 12 Then
                            'diferentes a las 12 hrs.
                            strHoraVencimiento = (obeOP.FechaVencimiento.Hour - 12).ToString("00") + ":" + _
                                              obeOP.FechaVencimiento.Minute.ToString("00") + " p.m."
                        Else
                            'igual a las 12 hrs
                            strHoraVencimiento = "12:" + obeOP.FechaVencimiento.Minute.ToString("00") + " p.m."
                        End If
                    End If
                    valoresDinamicos.Add("[FechaVencimiento]", strFechaVencimiento)
                    valoresDinamicos.Add("[HoraVencimiento]", strHoraVencimiento)
                    valoresDinamicos.Add("[idEmpresaContratante]", obeOP.IdEmpresa)
                    valoresDinamicos.Add("[idServicio]", obeOP.IdServicio)

                    'obeOP.FechaEmision
                    Dim strFechaEmision As String = String.Format("{0} {1}", CurrentCulture.DateTimeFormat.GetDayName(obeOP.FechaEmision.DayOfWeek), obeOP.FechaEmision.ToString(CurrentCulture.DateTimeFormat.ShortDatePattern))
                    Dim strHoraEmision As String = obeOP.FechaEmision.ToString(CurrentCulture.DateTimeFormat.ShortTimePattern)
                    valoresDinamicos.Add("[FechaEmision]", strFechaEmision)
                    valoresDinamicos.Add("[HoraEmision]", strHoraEmision)

                    If obeOP.oBECliente.RecienRegistrado Then
                        Dim valoresDinamicos2 As New Dictionary(Of String, String)()
                        Select Case obeOP.oBEServicio.CodigoServicioVersion
                            Case ParametrosSistema.Servicio.VersionIntegracion.Version2 And obeOP.oBEServicio.FlgEmailGenUsuario
                                If Not obeOP.ServicioUsaUsuariosAnonimos Then
                                    valoresDinamicos2.Add("[Cuenta]", obeOP.oBECliente.Email)
                                    valoresDinamicos2.Add("[CodigoRegistro]", obeOP.oBECliente.CodigoRegistro)
                                    valoresDinamicos2.Add("[Contrasenia]", obeOP.oBECliente.Password)

                                    Dim strPlantTipo As String = ""
                                    Select Case obeOP.IdMoneda
                                        Case 1
                                            strPlantTipo = ParametrosSistema.Plantilla.Tipo.EmailValUsu
                                        Case 2
                                            strPlantTipo = ParametrosSistema.Plantilla.Tipo.EmailValUsu & "DolarAM"
                                    End Select

                                    Dim strSeccionValidacion As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaHtml(New BEGenPlantillaHtmlRequest(obeOP.IdServicio, strPlantTipo, valoresDinamicos2, obeOP.CodPlantillaTipoVariacion))
                                    valoresDinamicos.Add("[SeccionValidacion]", strSeccionValidacion.Html)
                                Else
                                    valoresDinamicos.Add("[SeccionValidacion]", "")
                                End If
                            Case ParametrosSistema.Servicio.VersionIntegracion.Version1
                                valoresDinamicos.Add("[SeccionValidacion]", "")
                        End Select
                    Else
                        valoresDinamicos.Add("[SeccionValidacion]", "")
                    End If

                    Dim strPlantillaTipo As String = ""
                    Select Case obeOP.IdMoneda
                        Case 1
                            strPlantillaTipo = ParametrosSistema.Plantilla.Tipo.EmailGenCIP
                        Case 2
                            strPlantillaTipo = ParametrosSistema.Plantilla.Tipo.EmailGenCIP & "DolarAM"
                    End Select

                    'Si esta configurado la copia de correo se enviara:
                    Dim emailCCAdmin As String = IIf(obeOP.oBEServicio.FlgEmailCCAdmin, obeOP.MailComercio, String.Empty)

                    Dim oBEServicio As New BEServicio
                    oBEServicio = New BLServicio().ObtenerServicioPorIdWS(obeOP.IdServicio)

                    If oBEServicio.PlantillaFormato = 53 Then
                        Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaHtml(New BEGenPlantillaHtmlRequest(obeOP.IdServicio, strPlantillaTipo, valoresDinamicos, obeOP.CodPlantillaTipoVariacion))
                        If ((resGenHtmlResul IsNot Nothing)) Then
                            If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                                Dim pfrom As String = resGenHtmlResul.ParamValores("[EmailFrom]")
                                Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                                Dim psubjectFix As String = psubject.Replace("[ServicioNombre]", obeOP.DescripcionServicio)
                                Dim pHtmlFix As String = resGenHtmlResul.Html.Replace("[ServicioNombre]", obeOP.DescripcionServicio)
                                Dim objParametros As New Parametros()
                                objParametros.pfrom = pfrom
                                objParametros.EmailANotifGeneracion = obeOP.EmailANotifGeneracion
                                objParametros.psubjectFix = psubjectFix
                                objParametros.pHtmlFix = pHtmlFix
                                objParametros.pisBodyHtml = True
                                objParametros.correoCCAdmin = emailCCAdmin
                                'Rommel Background
                                'ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf ThreadProc_EnviarEmail), objParametros)
                                'Thread.Sleep(10000)
                                'Return 1
                                Return EnviarEmailPagoEfectivo(pfrom, pto, psubjectFix, pHtmlFix, True, emailCCAdmin)

                            Else
                                Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                            End If
                        Else
                            Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                        End If
                    Else

                        If (oBEServicio.PlantillaFormato = SPE.EmsambladoComun.ParametrosSistema.PlantillaFormato.Tipo.Estandar) Then
                            strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.GenCip & "Est"
                        Else
                            strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.GenCip & "Exp"
                        End If

                        Dim obePlantilla As BEPlantilla = New BLPlantilla().ConsultarPlantillaYParametrosPorTipoYVariacion(oBEServicio.IdServicio, strPlantillaTipo, "st")
                        Dim fileHtml = ObtenerPlantillaS3(obePlantilla.FileS3)

                        If (Not fileHtml = "") Then
                            Dim lstSecciones As List(Of BEPlantillaSeccion) = New BLPlantilla().ObtenerSeccionesPlantillaPorServicio(oBEServicio.IdServicio, obePlantilla.IdPlantillaTipo)
                            For i As Integer = 0 To lstSecciones.Count - 1
                                fileHtml = fileHtml.Replace("[Seccion" + lstSecciones(i).IdSeccion.ToString + "]", lstSecciones(i).Contenido)
                            Next
                            For Each param As KeyValuePair(Of String, String) In valoresDinamicos
                                fileHtml = fileHtml.Replace(param.Key, param.Value)
                            Next

                            For i As Integer = 0 To obePlantilla.ListaParametros.Count - 1
                                If (obePlantilla.ListaParametros(i).IdTipoParametro = 411) Then
                                    fileHtml = fileHtml.Replace(obePlantilla.ListaParametros(i).Etiqueta.ToString, obePlantilla.ListaParametros(i).Valor.ToString)
                                End If
                            Next


                            Dim emailfrom = If(obePlantilla.ListaParametros.Where(Function(x) x.Etiqueta = "[EmailFrom]").Count > 0, obePlantilla.ListaParametros.Where(Function(x) x.Etiqueta = "[EmailFrom]")(0).Valor, ConfigurationManager.AppSettings.[Get]("emailFrom"))
                            Dim emailSubject = If(obePlantilla.ListaParametros.Where(Function(x) x.Etiqueta = "[Subject]").Count > 0, obePlantilla.ListaParametros.Where(Function(x) x.Etiqueta = "[Subject]")(0).Valor, ConfigurationManager.AppSettings.[Get]("emailSubject"))

                            Dim pfrom As String = emailfrom
                            Dim psubject As String = emailSubject
                            Dim psubjectFix As String = psubject.Replace("[ServicioNombre]", obeOP.DescripcionServicio)
                            Dim pHtmlFix As String = fileHtml
                            Dim objParametros As New Parametros()
                            objParametros.pfrom = pfrom
                            objParametros.EmailANotifGeneracion = obeOP.EmailANotifGeneracion
                            objParametros.psubjectFix = psubjectFix
                            objParametros.pHtmlFix = pHtmlFix
                            objParametros.pisBodyHtml = True
                            objParametros.correoCCAdmin = emailCCAdmin

                            Return EnviarEmailPagoEfectivo(pfrom, pto, psubjectFix, pHtmlFix, True, emailCCAdmin)
                        Else
                            Exit Function
                        End If
                    End If


                End Using
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function

    Public Function ObtenerPlantillaS3(ByVal fileName As String) As String

        Dim s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("AWS_S3_ACCESS_KEY")
        Dim s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("AWS_S3_SECRET_ACCESS_KEY")
        Dim s3Region As String = ConfigurationManager.AppSettings.[Get]("AWS_S3_REGION")
        Dim s3BucketName As String = ConfigurationManager.AppSettings.[Get]("AWS_S3_BUCKET_NAME")
        Dim s3DirectoryPath As String = ConfigurationManager.AppSettings.[Get]("AWS_S3_DIRECTORY_PATH")

        Dim amazonS3 = New SPE.Utilitario.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        If (amazonS3.FileExists(fileName)) Then
            Dim fileStream = amazonS3.ReadStreamFromFile(fileName)
            Dim bytes = fileStream.ToArray()

            Return System.Text.Encoding.UTF8.GetString(bytes)
        Else
            Return "-1"
        End If

    End Function


	'************************************************************************************************** 
    ' M�todo          : EnviarCorreoGeneracionAgenciaBancariaEnConciliacion
    ' Descripci�n     : Envia un correo al cliente informandole que genero una OP
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 12/01/2009
    ' Parametros_In   : CodigoAgenciaBancaria
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarCorreoGeneracionAgenciaBancaria(ByVal CodigoAgenciaBancaria As String, _
                                                            ByVal Proceso As String, _
                                                                  Optional ByVal CodigoBanco As String = "") As Integer
        Try
            Dim pfrom As String = ConfigurationManager.AppSettings("CreacionAgenciaBancariaFromEmail")
            Dim pto As String = ConfigurationManager.AppSettings("CreacionAgenciaBancariaToEmail")
            Dim psubject As String = ConfigurationManager.AppSettings("CreacionAgenciaBancariaTemaEmail")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("CreacionAgenciaBancariaTemaEmailPlantillaRuta")

            'verificar si existe archivo texto, luego devolver CERO
            If Not File.Exists(rutaplantilla) Then Return 0

            Dim fs As StreamReader = File.OpenText(rutaplantilla)
            Dim pbody As String = fs.ReadToEnd()
            pbody = pbody.Replace("[CodigoAgenciaBancaria]", CodigoAgenciaBancaria)
            pbody = pbody.Replace("[FechaCreacion]", Date.Now.ToString)
            pbody = pbody.Replace("[Proceso]", Proceso)

            If CodigoBanco <> "" Then
                pbody = pbody.Replace("[CodigoBanco]", " en el Banco " + CodigoBanco)
            Else
                pbody = pbody.Replace("[CodigoBanco]", "")
            End If

            'devolver UNO en caso de exito

            Return EnviarEmail(pfrom, pto, psubject, pbody, True)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function

    Public Function EnviarCorreoGeneracionPuntoVentaYTerminal(ByVal RegistroPuntoVenta As Boolean, ByVal CodigoPuntoVenta As String, _
        ByVal NumeroSerieTerminal As String, ByVal Proceso As String) As Integer
        Try
            Dim pfrom As String = ConfigurationManager.AppSettings("CreacionPuntoVentaTerminalFromEmail")
            Dim pto As String = ConfigurationManager.AppSettings("CreacionPuntoVentaTerminalToEmail")
            Dim psubject As String = ConfigurationManager.AppSettings("CreacionPuntoVentaTerminalTemaEmail")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("CreacionPuntoVentaTerminalEmailPlantillaRuta")

            If Not File.Exists(rutaplantilla) Then Return 0

            Dim pbody As String = ""
            Using fs As StreamReader = File.OpenText(rutaplantilla)
                pbody = fs.ReadToEnd()
            End Using

            If RegistroPuntoVenta Then
                pbody = pbody.Replace("[PuntoVentaTerminal]", "Punto de Venta y Terminal")
                pbody = pbody.Replace("[PuntoVenta]", "<tr><td colspan=""2"" >Se ha creado un Punto de venta con c&oacute;digo: " + _
                    CodigoPuntoVenta + "</td></tr>")
            Else
                pbody = pbody.Replace("[PuntoVentaTerminal]", "Terminal")
                pbody = pbody.Replace("[PuntoVenta]", "")
            End If
            pbody = pbody.Replace("[NroSerieTerminal]", NumeroSerieTerminal)
            pbody = pbody.Replace("[FechaCreacion]", Date.Now.ToString)
            pbody = pbody.Replace("[Proceso]", Proceso)
            Return EnviarEmail(pfrom, pto, psubject, pbody, True)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function

    '*******************************************************************************************************************************************************************************
    'wjra 14/01/2011
    '************************************************************************************************** 
    ' M�todo          : EnviarCorreoConciliacionFinalizada
    ' Descripci�n     : Envia un correo al administrador de Pago Efectivo al finalizar el Proceso de Conciliacion
    ' Autor           : Walter Rojas
    ' Fecha/Hora      : 14/01/2011
    ' Parametros_In   : fechaProcesoConciliacion
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarCorreoConciliacionFinalizada(ByVal fechaProcesoConciliacion As DateTime, ByVal Proceso As String, ByVal opcorrectas As Integer, ByVal opincorrectas As Integer) As Integer
        Try
            Dim pfrom As String = ConfigurationManager.AppSettings("ProcesoConciliacionFromEmail")
            Dim pto As String = ConfigurationManager.AppSettings("ProcesoConciliacionToEmail")
            Dim psubject As String = ConfigurationManager.AppSettings("ProcesoConciliacionTemaEmail")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("ProcesoConciliacionPlantillaRuta")

            Dim fs As StreamReader = File.OpenText(rutaplantilla)
            Dim pbody As String = fs.ReadToEnd()
            pbody = pbody.Replace("[OperacionesCorrectas]", opcorrectas)
            pbody = pbody.Replace("[OperacionesIncorrectas]", opincorrectas)
            pbody = pbody.Replace("[FechaCreacion]", fechaProcesoConciliacion.ToString("dd/mm/yyyy hh:mm:ss"))

            Return EnviarEmail(pfrom, pto, psubject, pbody, True)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    '*******************************************************************************************************************************************************************************
    '************************************************************************************************** 
    ' M�todo          : EnviarCorreoRecuperarPasswordUsuario
    ' Descripci�n     : Envia un correo al usuario con la nueva contrase�a
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : nombrecliente , clave , pto
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarCorreoRecuperarPasswordUsuario(ByVal nombrecliente As String, ByVal clave As String, ByVal pto As String) As Integer
        Dim pfrom As String = ConfigurationManager.AppSettings("RecuperarPassEmail")
        Dim psubject As String = ConfigurationManager.AppSettings("RecuperarPassTemaEmail")
        Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RecuperarPassEmailPlantillaRuta")
        Dim fs As StreamReader = File.OpenText(rutaplantilla)
        Dim pbody As String = fs.ReadToEnd()
        pbody = pbody.Replace("[Nombre]", nombrecliente)
        pbody = pbody.Replace("[correo]", pto)
        pbody = pbody.Replace("[contrasenia]", clave)

        Return EnviarEmailPagoEfectivo(pfrom, pto, psubject, pbody, True)
    End Function

    '************************************************************************************************** 
    ' M�todo          : EnviarEmail
    ' Descripci�n     : Env�o de correo.
    ' Autor           : Manuel Rojas
    ' Fecha/Hora      : 01/10/2015
    ' Parametros_In   : pfrom , pto , psubject , pbody , pisBodyHtml, optional bcc
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarEmail(ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean, Optional ByVal bcc As String = "") As Integer

        Dim phost As String = ConfigurationManager.AppSettings("ServidorSMTP")
        Dim mailfrom As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailfrom.From = New System.Net.Mail.MailAddress(pfrom)
        Return EnviarEmailGeneral(phost, mailfrom, pto, psubject, pbody, pisBodyHtml, bcc)

    End Function

    Public Function EnviarEmailPagoEfectivo(ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean, Optional ByVal bcc As String = "") As Integer

        Dim phost As String = ConfigurationManager.AppSettings("ServidorSMTP")
        Dim mailfrom As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailfrom.From = New System.Net.Mail.MailAddress(pfrom, "PagoEfectivo")
        Return EnviarEmailGeneral(phost, mailfrom, pto, psubject, pbody, pisBodyHtml, bcc)

    End Function

    Public Function EnviarEmailGeneral(ByVal phost As String, ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean, Optional ByVal bcc As String = "") As Integer

        Dim mailfrom As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailfrom.From = New System.Net.Mail.MailAddress(pfrom)
        Return EnviarEmailGeneral(phost, mailfrom, pto, psubject, pbody, pisBodyHtml, bcc)

    End Function

    '************************************************************************************************** 
    ' M�todo          : EnviarEmailGeneral
    ' Descripci�n     : Env�o de correo
    ' Autor           : Manuel Rojas
    ' Fecha/Hora      : 01/10/2015
    ' Parametros_In   : phost , mail , pto , psubject , pbody , pisBodyHtml, optional bcc
    ' Parametros_Out  : Resultado de transaccion
    ''**************************************************************************************************
    Public Function EnviarEmailGeneral(ByVal phost As String, ByVal mail As MailMessage, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean, Optional ByVal bcc As String = "") As Integer
        'ThreadPool.QueueUserWorkItem(Sub()
        Try
            Dim enabled As String = ConfigurationManager.AppSettings("EnabledEnvioEmail")
            If (_3Dev.FW.Util.DataUtil.ObjectToString(enabled) = "true") Then

                Dim count As Integer
                For Each correo As String In pto.Split(New Char() {",", ";"c})
                    Dim scorreo = correo.Trim().ToLower()
                    If (ValidarMail(scorreo)) Then
                        mail.To.Add(New System.Net.Mail.MailAddress(scorreo))
                        count = count + 1
                    End If
                Next

                If (count < 1) Then
                    Return 1
                End If

                mail.Subject = psubject
                mail.Body = pbody

                mail.IsBodyHtml = pisBodyHtml
                mail.Priority = MailPriority.Normal
                If Not String.IsNullOrEmpty(bcc) Then
                    mail.Bcc.Add(bcc)
                End If
                Dim smtp As New SmtpClient(phost)
                smtp.SendAsync(mail, Nothing)

            End If

        Catch ex As SmtpException
            _nlogger.Error(ex, ex.Message)
            Throw ex
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try

        Return 1
    End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region



#Region "METODOS DE ENV�O DE CORREO PARA MONEDERO ELECTR�NICO"

    Public Function EnviarCorreoGeneracionToken(ByVal entidad As BETokenDineroVirtual) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreCliente]", entidad.ParametrosEmailMonedero.NombreCliente)
                valoresDinamicos.Add("[TipoOperacion]", entidad.ParametrosEmailMonedero.TipoOperacion)
                valoresDinamicos.Add("[Token]", entidad.Token)
                valoresDinamicos.Add("[TiempoExpiracion]", entidad.FechaCaducidad)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.EnviarToken, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("SistemaDineroVirtual")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Dim pto As String = entidad.ParametrosEmailMonedero.EmailCliente
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoSolicitudCreacionCuenta(ByVal entidad As BECuentaDineroVirtual) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreCompletoUsuario]", entidad.ParametrosEmailMonedero.NombreCliente)
                valoresDinamicos.Add("[FechaExpiracion]", entidad.ParametrosEmailMonedero.FechaExpiracion)
                valoresDinamicos.Add("[FechaSolicitud]", Date.Now)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.ParametrosEmailMonedero.DescripcionMoneda)
                valoresDinamicos.Add("[AliasCuenta]", entidad.ParametrosEmailMonedero.AliasCuenta)
                valoresDinamicos.Add("[EmailCliente]", entidad.ParametrosEmailMonedero.EmailCliente)
                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml _
                (ParametrosSistema.EnvioTipoEmail.SolicitudCreacionCuenta, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("SistemaDineroVirtual")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Dim pto As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoAlRegistrarTransferenciaDineroOrigen(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreClienteOrigen]", entidad.TitularCuentaOrigen)
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[MontoComision]", entidad.MontoComision)
                valoresDinamicos.Add("[FechaTransaccion]", DateTime.Now)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[NumeroOperacion]", entidad.NumeroOrdenPago.PadLeft(8, "0"))
                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml _
                (ParametrosSistema.EnvioTipoEmail.TransferenciaDineroOrigen, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("SistemaDineroVirtual")
                        Dim psubject As String = "Transferencia Exitosa"
                        Dim pto As String = entidad.PFrom
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoAlRegistrarTransferenciaDineroDestino(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreClienteDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaOrigen]", entidad.TitularCuentaOrigen)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[FechaTransaccion]", DateTime.Now)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[NumeroOperacion]", entidad.NumeroOrdenPago.PadLeft(8, "0"))
                valoresDinamicos.Add("[ObservacionTransferencia]", entidad.ObservacionTransferencia)
                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml _
                (ParametrosSistema.EnvioTipoEmail.TransferenciaDineroDestino, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("SistemaDineroVirtual")
                        Dim psubject As String = "Transferencia Exitosa"
                        Dim pto As String = entidad.PTo
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function


    Public Function EnviarCorreoAlRegistrarEquipo(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NombrePC]", entidad.NombrePC)
                valoresDinamicos.Add("[FechaRegistro]", entidad.FechaRegistro)
                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml _
                (ParametrosSistema.EnvioTipoEmail.EquipoRegistrado, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("SistemaDineroVirtual")
                        Dim psubject As String = "Se ha Registrado un Nuevo Equipo"
                        Dim pto As String = entidad.EmailCliente
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCIPPagada(ByVal entidad As BEOrdenPago, ByVal entidad2 As BEMovimientoCuenta) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()


                valoresDinamicos.Add("[NumeroOrdenPago]", entidad.NumeroOrdenPago)
                valoresDinamicos.Add("[DescripcionEmpresa]", entidad.DescripcionEmpresa)
                valoresDinamicos.Add("[ConceptoPago]", entidad.ConceptoPago)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.Total)
                valoresDinamicos.Add("[NumeroOperacion]", entidad2.NumeroOperacion.PadLeft(8, "0"))
                valoresDinamicos.Add("[FechaMovimiento]", Date.Now)
                valoresDinamicos.Add("[NumeroCuenta]", entidad2.IdCuentaVirtual)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.PagarCip, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Dim pto As String = entidad.UsuarioEmail
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCIPPagadaPortal(ByVal entidad As BEParametrosEmailMonedero, ByVal ente2 As BEMovimientoCuenta) As Integer
        Try
            Using oblPlantilla As New BLPlantilla() 'asd

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()


                'valoresDinamicos.Add("[NumeroOrdenPago]", entidad.NumeroSolicitud)
                valoresDinamicos.Add("[DescripcionEmpresa]", ente2.NombrePortal) 'Razon Social Empresa
                valoresDinamicos.Add("[ConceptoPago]", entidad.ConceptoPago)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[NumeroOperacion]", ente2.NumeroOperacion.PadLeft(8, "0"))
                valoresDinamicos.Add("[FechaMovimiento]", Date.Now)
                valoresDinamicos.Add("[NumeroCuenta]", ente2.IdCuentaVirtual)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.PagarSolicitudPortal, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoSolicitudRetiroCliente(ByVal entidad As BEParametrosEmailMonedero, ByVal pto As String) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.SolicitudRetiroCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoSolicitudRetiroAdmin(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.SolicitudRetiroAdmin, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, ConfigurationManager.AppSettings("AdminEmailMonedero"), psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCuentaAprobadaCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                If entidad.AliasCuenta.Trim() = "" Then
                    entidad.AliasCuenta = "- - - -"
                End If

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[AliasCuenta]", entidad.AliasCuenta)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaAprobadaCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function


    Public Function EnviarCorreoCuentaAprobadaClientePrimeraVez(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                If entidad.AliasCuenta.Trim() = "" Then
                    entidad.AliasCuenta = "- - - -"
                End If

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[AliasCuenta]", entidad.AliasCuenta)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaAprobadaClientePrimeraVez, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function


    Public Function EnviarCorreoCuentaRechazadaCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                If entidad.AliasCuenta.Trim() = "" Then
                    entidad.AliasCuenta = "- - - -"
                End If

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[AliasCuenta]", entidad.AliasCuenta)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)


                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaRechazadaCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoAprobacionRetiroCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.ConceptoPago)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.AprobacionRetiroCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoAprobacionRetiroTesoreria(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.ConceptoPago)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)


                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.AprobacionRetiroTesoreria, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        'MDSEMAIL

                        Return EnviarEmail(pfrom, ConfigurationManager.AppSettings("AdminTesoreroMonedero"), psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoRechazadoRetiroCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                If entidad.Observacion.Trim() = "" Then
                    entidad.Observacion = "- - - -"
                End If

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[SimboloMoneda]", entidad.SimboloMoneda)
                valoresDinamicos.Add("[Monto]", entidad.ConceptoPago)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[Observacion]", entidad.Observacion)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.RechazadaRetiroCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCuentaLiquidacionAprobadaCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[Monto]", entidad.ConceptoPago)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaLiquidacionAprobada, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCuentaLiquidacionRechazadaCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                If entidad.Observacion.Trim() = "" Then
                    entidad.Observacion = "- - - -"
                End If
                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[NumeroSolicitud]", entidad.NumeroSolicitud.PadLeft(8, "0"))
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[Monto]", entidad.ConceptoPago)
                valoresDinamicos.Add("[NombreBanco]", entidad.NombreBanco)
                valoresDinamicos.Add("[CuentaDestino]", entidad.CuentaDestino)
                valoresDinamicos.Add("[TitularCuentaDestino]", entidad.TitularCuentaDestino)
                valoresDinamicos.Add("[Observacion]", entidad.Observacion)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaLiquidacionRechazada, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCuentaHabilitadaCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaHabilitadaCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    Public Function EnviarCorreoCuentaInhabilitadaCliente(ByVal entidad As BEParametrosEmailMonedero) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombreCliente)
                valoresDinamicos.Add("[CuentaOrigen]", entidad.CuentaOrigen)
                valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaSolicitud]", entidad.FechaSolicitud)
                valoresDinamicos.Add("[Monto]", entidad.Monto)
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaInhabilitadaCliente, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.PTo, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCuentaRecarga(ByVal entidad As BECuentaDineroVirtual) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.ClienteNombre)
                valoresDinamicos.Add("[NumeroCuenta]", entidad.Numero)
                ' valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaRegistro]", Date.Now)
                valoresDinamicos.Add("[Monto]", entidad.MonedaSimbolo + " " + entidad.MontoRecarga.ToString())
                valoresDinamicos.Add("[FechaOperacion]", Date.Now)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaRecarga, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.Email, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoCuentaExtorno(ByVal entidad As BECuentaDineroVirtual) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.ClienteNombre)
                valoresDinamicos.Add("[NumeroCuenta]", entidad.Numero)
                ' valoresDinamicos.Add("[DescripcionMoneda]", entidad.DescripcionMoneda)
                valoresDinamicos.Add("[FechaRegistro]", Date.Now)
                valoresDinamicos.Add("[Monto]", entidad.MonedaSimbolo + " " + entidad.MontoRecarga.ToString())

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.CuentaExtorno, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.Email, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoUsuarioBloqueado(ByVal entidad As BECliente) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombresApellidos)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.UsuarioBloqueado, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.Email, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    Public Function EnviarCorreoUsuarioDesbloqueado(ByVal entidad As BECliente) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()

                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()

                'Obtenemos los datos que necesitamos enviando la entidad q llega como parametro
                Dim objBE As New BEParametrosEmailMonedero

                valoresDinamicos.Add("[NombreCliente]", entidad.NombresApellidos)

                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valors de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml(ParametrosSistema.EnvioTipoEmail.UsuarioDesbloqueado, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))

                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("AdminEmailMonedero")
                        ' Cambiar Subject!
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Return EnviarEmail(pfrom, entidad.Email, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

#End Region
    Public Function EnviarCorreoCambioContrasenia(ByVal entidad As BEUsuarioBase) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreCliente]", entidad.Nombres)
                valoresDinamicos.Add("[Token]", entidad.TokenGUIDPassword)
                valoresDinamicos.Add("[FechaExpiracion]", entidad.FechaCreacionTokenGUID)


                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml _
                (ParametrosSistema.EnvioTipoEmail.CambiarContrasenia, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("RecuperarPassEmail")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Dim pto As String = entidad.Email
                        Return EnviarEmailPagoEfectivo(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    Public Function EnviarCorreoCambioContraseniaConfirmacion(ByVal entidad As BEUsuarioBase) As Integer
        Try
            Using oblPlantilla As New BLPlantilla()
                '1� Creamos los par�metros Din�micos que se reemplazar�n en el html
                Dim valoresDinamicos As New Dictionary(Of String, String)()
                valoresDinamicos.Add("[NombreCliente]", entidad.Nombres)
                valoresDinamicos.Add("[FechaExpiracion]", entidad.FechaActualizacion)


                '2� Obtenemos la plantilla del correo especificando el tipo de correo y  enviandole los valores 
                'de los parametros a setear a la plantilla
                Dim resGenHtmlResul As BEGenPlantillaHtmlResponse = oblPlantilla.GenerarPlantillaEmailHtml _
                (ParametrosSistema.EnvioTipoEmail.CambiarContraseniaConfirmacion, New BEGenPlantillaHtmlRequest(0, "", valoresDinamicos, ParametrosSistema.Plantilla.TipoVariacion.Standar))
                '3� Validamos los resultados y se procede a enviar el email
                If ((resGenHtmlResul IsNot Nothing)) Then
                    If (resGenHtmlResul.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
                        Dim pfrom As String = ConfigurationManager.AppSettings("RecuperarPassEmail")
                        Dim psubject As String = resGenHtmlResul.ParamValores("[Subject]")
                        Dim pto As String = entidad.Email
                        Return EnviarEmailPagoEfectivo(pfrom, pto, psubject, resGenHtmlResul.Html, True)
                    Else
                        Throw New Exception(String.Format("GenerarPlantillaHtml:{0}", resGenHtmlResul.Message))
                    End If
                Else
                    Throw New Exception(String.Format("GenerarPlantillaHtml: el objecto resGenHtmlResul es nulo"))
                End If
            End Using
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    'jeffersonmendoza
    Public Function EnviarEmailContactenosEmpresa(ByVal obeMail As SPE.Entidades.BEMailContactenosEmpresa) As Integer
        With obeMail
            Dim pTo As String = ConfigurationManager.AppSettings("ContactenosEmail")
            Dim pFrom As String = ConfigurationManager.AppSettings("ClienteContactenosEmail")

            Dim sbContenido As New System.Text.StringBuilder()

            sbContenido.Append("Sres. PagoEfectivo: ")
            sbContenido.Append("<br/><br/>Raz�n Social: <br/>" + .RazonSocial)
            sbContenido.Append("<br/><br/>RUC: <br/>" + .RUC)
            sbContenido.Append("<br/><br/>Rubro: <br/>" + .Rubro)
            sbContenido.Append("<br/><br/>Tel�fono: <br/>" + .TelefonoEmpresa)
            sbContenido.Append("<br/><br/>P�gina Web: <br/>" + .PaginaWeb)
            sbContenido.Append("<br/><br/>Contenido: <br/>" + .Contenido)
            sbContenido.Append("<br/><br/>Atentamente<br/>" + .NombreCompleto)
            sbContenido.Append("<br/><br/>Correo: " + .Email)
            sbContenido.Append("<br/>Tel�fono: " + .Tema)

            Return EnviarEmail(pFrom, pTo, "Cont�ctenos", sbContenido.ToString(), True)
        End With
    End Function

    Public Function EnviarCorreoSuscriptor(ByVal EmailSuscriptor As String) As Integer
        Try

            Dim CorreoDesde As String = ConfigurationManager.AppSettings("CorreoDesdeSuscriptor")
            Dim AsuntoCorreo As String = ConfigurationManager.AppSettings("AsuntoCorreo")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("CorreoSuscriptorPlantilla")
            Dim pbody As String = ""
            Using fs As StreamReader = File.OpenText(rutaplantilla)
                pbody = fs.ReadToEnd()
            End Using
            'Return EnviarEmail(CorreoDesde, pto, AsuntoCorreo, pbody, True)
            Dim phost As String = ConfigurationManager.AppSettings("ServidorSMTP")
            'Return EnviarEmailGeneral(phost, pfrom, pto, psubject, pbody, pisBodyHtml)

            Dim enabled As String = ConfigurationManager.AppSettings("EnabledEnvioEmail")
            If (_3Dev.FW.Util.DataUtil.ObjectToString(enabled) = "true") Then
                Dim correoPrueba As String = ConfigurationManager.AppSettings("CorreoPrueba")
                Dim mail As New MailMessage(CorreoDesde, EmailSuscriptor, AsuntoCorreo, pbody)
                mail.IsBodyHtml = True
                mail.Priority = MailPriority.Normal
                Dim smtp As New SmtpClient(phost)
                smtp.Send(mail)
            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function EnviarCorreoDevolucionGenerado(ByVal oBEDevolucion As BEDevolucion, ByVal emailsAdminCip As String) As Integer
        Try

            With oBEDevolucion
                Dim pTo As String = .EmailRepresentante
                Dim pFrom As String = ConfigurationManager.AppSettings("EmailDevolucion")
                Dim sbContenido As New System.Text.StringBuilder()
                sbContenido.Append("Estimado(a): ")
                sbContenido.Append("<br/><br/>Le informamos que su solicitud de devoluci�n N� " + .IdDevolucion.ToString())
                sbContenido.Append(", con los datos mostrados a continuaci�n ha sido registrado.")
                sbContenido.Append("<br/><br/><b>N� Solicitud: </b>")
                sbContenido.Append(.IdDevolucion.ToString())
                sbContenido.Append("<br/><b>Estado Solicitud: </b>")
                sbContenido.Append("Solicitado")
                sbContenido.Append("<br/><b>CIP: </b>")
                sbContenido.Append(.NroCIP.ToString())
                sbContenido.Append("<br/><b>Monto: </b>")
                sbContenido.Append(.Monto.ToString())
                sbContenido.Append("<br/><b>Servicio: </b>")
                sbContenido.Append(.ServicioDescripcion.ToString())
                sbContenido.Append("<br/><b>Estado: </b>")
                sbContenido.Append(.EstadoCipDescripcion.ToString())
                sbContenido.Append("<br/><b>DNI: </b>")
                sbContenido.Append(.ClienteNroDNI.ToString())
                sbContenido.Append("<br/><b>Nombres del Cliente: </b>")
                sbContenido.Append(.ClienteNombres.ToString())
                sbContenido.Append("<br/><b>Apellidos del Cliente: </b>")
                sbContenido.Append(.ClienteApellidos.ToString())
                sbContenido.Append("<br/><b>Direccion del Cliente: </b>")
                sbContenido.Append(.ClienteDireccion.ToString())
                sbContenido.Append("<br/><b>Fecha Registro: </b>")
                sbContenido.Append(.FechaCreacion.ToString("dd/MM/yyyy"))
                sbContenido.Append("<br/><b>Comentarios: </b>")
                sbContenido.Append(.Observaciones.ToString())
                sbContenido.Append("<br/><br/>Gracias")
                sbContenido.Append("<br/>Equipo de PagoEfectivo")

                Return EnviarEmail(pFrom, pTo, " PagoEfectivo - Solicitud de devolucion #Nro. " + .IdDevolucion.ToString(), sbContenido.ToString(), True, emailsAdminCip)
            End With
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    Public Function EnviarCorreoDevolucionRealizado(ByVal oBEDevolucion As BEDevolucion, ByVal emailsAdminCip As String) As Integer
        Try
            With oBEDevolucion
                Dim pTo As String = .EmailRepresentante
                Dim pFrom As String = ConfigurationManager.AppSettings("EmailDevolucion")
                Dim sbContenido As New System.Text.StringBuilder()
                sbContenido.Append("Estimado(a): ")
                sbContenido.Append("<br/><br/>Le informamos que su solicitud de devoluci�n N� " + .IdDevolucion.ToString())
                sbContenido.Append(", con los datos mostrados a continuaci�n ha sido atendido.")
                sbContenido.Append("<br/><br/><b>N� Solicitud: </b>")
                sbContenido.Append(.IdDevolucion.ToString())
                sbContenido.Append("<br/><b>Estado Solicitud: </b>")
                sbContenido.Append(.DescripcionEstadoCliente.ToString())
                sbContenido.Append("<br/><b>CIP: </b>")
                sbContenido.Append(.NroCIP.ToString())
                sbContenido.Append("<br/><b>Monto: </b>")
                sbContenido.Append(.Monto.ToString())
                sbContenido.Append("<br/><b>Servicio: </b>")
                sbContenido.Append(.ServicioDescripcion.ToString())
                sbContenido.Append("<br/><b>Estado: </b>")
                sbContenido.Append(.EstadoCipDescripcion.ToString())
                sbContenido.Append("<br/><b>DNI: </b>")
                sbContenido.Append(.ClienteNroDNI.ToString())
                sbContenido.Append("<br/><b>Nombres del Cliente: </b>")
                sbContenido.Append(.ClienteNombres.ToString())
                sbContenido.Append("<br/><b>Apellidos del Cliente: </b>")
                sbContenido.Append(.ClienteApellidos.ToString())
                sbContenido.Append("<br/><b>Direccion del Cliente: </b>")
                sbContenido.Append(.ClienteDireccion.ToString())
                sbContenido.Append("<br/><b>Fecha Registro: </b>")
                sbContenido.Append(.FechaCreacion.ToString("dd/MM/yyyy"))
                sbContenido.Append("<br/><b>Comentarios: </b>")
                sbContenido.Append(.Observaciones.ToString())
                sbContenido.Append("<br/><br/>Gracias")
                sbContenido.Append("<br/>Equipo de PagoEfectivo")

                Return EnviarEmail(pFrom, pTo, " PagoEfectivo - Solicitud de devolucion #Nro. " + .IdDevolucion.ToString(), sbContenido.ToString(), True, emailsAdminCip)
            End With
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

#Region "ToGenerado"
    Public Function EnviarEmailCambioDeEstadoCipDeExpAGen(ByVal oBEOrdenPago As SPE.Entidades.BEOrdenPago) As Integer
        Try
            Dim pTo As String = ConfigurationManager.AppSettings("DestinatariosCambioDeEstadoCip")
            Dim pFrom As String = ConfigurationManager.AppSettings("AdministradorEmail")
            Dim rutaplantilla As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("CambioEstadoCipRutaPlantillaEmail")

            Dim fs As StreamReader = File.OpenText(rutaplantilla)
            Dim pbody As String = fs.ReadToEnd()
            pbody = pbody.Replace("[CIP]", oBEOrdenPago.NumeroOrdenPago)
            pbody = pbody.Replace("[Portal]", 165)
            pbody = pbody.Replace("[Moneda]", oBEOrdenPago.SimboloMoneda)
            pbody = pbody.Replace("[Monto]", oBEOrdenPago.Total)
            pbody = pbody.Replace("[Motivo]", oBEOrdenPago.MotivoDeExpAGen)
            pbody = pbody.Replace("[Usuario]", oBEOrdenPago.IdUsuarioActualizacion)
            pbody = pbody.Replace("[FechaCambio]", Date.Now.ToString("dd/MM/yyyy hh:mm:ss"))

            Return EnviarEmail(pFrom, pTo, "Cambio de Estado de CIP", pbody, True)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
#End Region

    Private Function ValidarMail(sMail As String) As Boolean
        ' retorna true o false
        Return Regex.IsMatch(sMail, "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$")
    End Function

End Class
