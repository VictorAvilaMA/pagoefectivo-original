Imports System
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades
Imports SPE.AccesoDatos

Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports System.Transactions

Public Class BLEmpresaContratante
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAEmpresaContratante
    End Function
    Public ReadOnly Property DAEmpresaContratante() As DAEmpresaContratante
        Get
            Return CType(DataAccessObject, DAEmpresaContratante)
        End Get
    End Property

    Public Function OcultarEmpresa(ByVal idservicio As Integer) As BEOcultarEmpresa
        Return CType(DataAccessObject, DAEmpresaContratante).OcultarEmpresa(idservicio)
    End Function
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "Codigo"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.CodigoComparer())
            Case "DescripcionOrigenCancelacion"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.DescripcionOrigenCancelacionComparer())
            Case "RazonSocial"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.RazonSocialComparer())
            Case "Contacto"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.ContactoComparer())
            Case "Ruc"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.RucComparer())
            Case "Telefono"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.TelefonoComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEEmpresaContratante.DescripcionEstadoComparer())
        End Select

    End Sub
    Public Overrides Function DeleteEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As BusinessMessageBase = Nothing
        Dim cantServiciosAsociados As Integer = 0
        Dim cantRepresentantesAsociados As Integer = 0
        Dim idEmpresaContratante As Integer = ObjectToInt32(request.EntityId)
        Try
            Using odaServicio As New DAServicio()
                cantServiciosAsociados = odaServicio.CantidadServiciosPorIdEmpresaContratante(idEmpresaContratante)
            End Using
            Using odaRepresentante As New DARepresentante()
                cantRepresentantesAsociados = odaRepresentante.CantidadRepresentantesPorIdEmpresaContratante(idEmpresaContratante)
            End Using
            If ((cantServiciosAsociados = 0) And (cantRepresentantesAsociados = 0)) Then
                response = MyBase.DeleteEntity(request)
            Else
                response = New BusinessMessageBase()
                response.BMResultState = BMResult.No
                Dim strResponse As New System.Text.StringBuilder()
                strResponse.Append("No se puede eliminar la empresa contratante por los siguientes motivos:")
                strResponse.Append("  ")
                If (cantServiciosAsociados > 0) Then
                    strResponse.Append(String.Format(" Existen {0} servicio(s) asociados.", cantServiciosAsociados))
                End If
                If (cantRepresentantesAsociados > 0) Then
                    'If (strResponse.Length > 0) Then
                    '    strResponse.Append(".")
                    'End If
                    strResponse.Append(String.Format(" Existen {0} representante(s) asociados.", cantRepresentantesAsociados))
                End If
                response.Message = strResponse.ToString()
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return response
    End Function

    Private Const MSJRegistroEmpresaSatisfactorio As String = "La empresa contratante {0} se ha registrado satisfactoriamente."
    Private Const MSJActualizacionEmpresaSatisfactorio As String = "La empresa contratante {0} se ha actualizado satisfactoriamente."
    Private Const MSJEmpresaCodigoAGuardarYaExiste As String = "No se pudo guardar porque el c�digo de empresa contratante ya existe."

    Public Overrides Function SaveEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        'Validar que el C�digo Empresa Contratante no se duplique
        'Validar que el RUC tenga el Formato Correcto
        '
        Dim obeEmpresaContrante As BEEmpresaContratante = request.Entity
        Dim response As New BusinessMessageBase()
        Try
            Dim cantEmpContr As Integer = DAEmpresaContratante.ConsultarCantidadEmpresaContratantePorCodigo(obeEmpresaContrante.Codigo)
            If (((obeEmpresaContrante.IdEmpresaContratante <= 0) And (cantEmpContr >= 1)) Or ((obeEmpresaContrante.IdEmpresaContratante > 0) And (cantEmpContr > 1))) Then
                response.BMResultState = BMResult.No
                response.Message = MSJEmpresaCodigoAGuardarYaExiste
                Return response
            End If

            If (obeEmpresaContrante.IdEmpresaContratante <= 0) Then
                DAEmpresaContratante.InsertRecord(obeEmpresaContrante)
                response.BMResultState = BMResult.Ok
                response.Message = String.Format(MSJRegistroEmpresaSatisfactorio, obeEmpresaContrante.RazonSocial)
                response.Entity = DAEmpresaContratante.GetRecordByID(obeEmpresaContrante.IdEmpresaContratante)
            Else
                DAEmpresaContratante.UpdateRecord(obeEmpresaContrante)
                response.BMResultState = BMResult.Ok
                response.Message = String.Format(MSJActualizacionEmpresaSatisfactorio, obeEmpresaContrante.RazonSocial)
                response.Entity = DAEmpresaContratante.GetRecordByID(obeEmpresaContrante.IdEmpresaContratante)
            End If

        Catch ex As Exception
            response.BMResultState = BMResult.Error
            response.Message = ex.Message
        End Try
        Return response
    End Function

    Public Function RegistrarTransaccionesaLiquidar(ByVal be As BETransferencia) As Integer
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Dim result As Integer = 0
        Try

            Using transaccionscope As New TransactionScope()
                result = oTransferencia.RegistrarTransaccionesaLiquidar(be)
                transaccionscope.Complete()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function
    Public Function ConsultarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Return oTransferencia.ConsultarTransaccionesaLiquidar(be)
    End Function
    Public Function ObtenerFechaInicioTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Return oTransferencia.ObtenerFechaInicioTransaccionesaLiquidar(be)
    End Function
    Public Function ObtenerFechaInicioTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Return oTransferencia.ObtenerFechaInicioTransaccionesaLiquidarLote(be)
    End Function

    Private Function MasCercano(ByVal valor As String) As Integer
        Dim cadena As String() = valor.Split(",")
        Dim Diferencia As Integer = 31
        Dim Dia As Integer = 31
        For Each Val As String In cadena
            If Math.Abs(CInt(DateTime.Now.Day) - CInt(Val)) <= Diferencia Then
                Diferencia = Math.Abs(CInt(DateTime.Now.Day) - CInt(Val))
                Dia = CInt(Val)
            End If
        Next
        Return Dia
    End Function

    Public Function ObtenerFechaFinTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Dim oFechaHasta As New DateTime
        Dim oBEEmpresaPeriodo As BEPeriodoLiquidacion = oTransferencia.ObtenerFechaFinTransaccionesaLiquidarLote(be)
        If oBEEmpresaPeriodo IsNot Nothing Then
            If oBEEmpresaPeriodo.IdPeriodo = EmsambladoComun.ParametrosSistema.TipoPeriodo.Diaria Then
                oFechaHasta = DateTime.Now
            ElseIf oBEEmpresaPeriodo.DiasLiquidacion <> "" Then
                Dim counterDays As Integer = 0
                For index As Integer = DateTime.Now.DayOfWeek To DateTime.Now.DayOfWeek - 7 Step -1
                    If oBEEmpresaPeriodo.DiasLiquidacion(IIf(index < 0, index + 7, index)) = "1" Then
                        oFechaHasta = DateTime.Now.AddDays(-counterDays)
                        Exit For
                    End If
                    counterDays += 1
                Next
            ElseIf oBEEmpresaPeriodo.FechasFin <> "" Then
                If DateTime.Now.Day >= MasCercano(oBEEmpresaPeriodo.FechasFin) Then
                    oFechaHasta = DateTime.Now.AddDays(-(DateTime.Now.Day - MasCercano(oBEEmpresaPeriodo.FechasFin)))
                Else
                    Dim mesAnterior As DateTime = DateTime.Now.AddMonths(-1)
                    Dim diasMesAnt As Integer = DateTime.DaysInMonth(mesAnterior.Year, mesAnterior.Month)
                    If diasMesAnt < MasCercano(oBEEmpresaPeriodo.FechasFin) Then
                        oFechaHasta = mesAnterior.AddDays(diasMesAnt - DateTime.Now.Day)
                    End If
                    oFechaHasta = mesAnterior.AddDays(MasCercano(oBEEmpresaPeriodo.FechasFin) - DateTime.Now.Day)
                End If
            End If
        Else
            oFechaHasta = DateTime.Now
        End If
        Return oFechaHasta
    End Function
    Public Function ObtenerFechaFinTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Dim oFechaHasta As New DateTime
        Dim oBEEmpresaPeriodo As BEEmpresaPeriodo = oTransferencia.ObtenerFechaFinTransaccionesaLiquidar(be)
        If oBEEmpresaPeriodo IsNot Nothing Then
            Select Case oBEEmpresaPeriodo.IdTipoPeriodo
                Case EmsambladoComun.ParametrosSistema.TipoPeriodo.Diaria
                    oFechaHasta = DateTime.Now
                Case EmsambladoComun.ParametrosSistema.TipoPeriodo.Semanal
                    Dim counterDays As Integer = 0
                    For index As Integer = DateTime.Now.DayOfWeek To DateTime.Now.DayOfWeek - 7 Step -1
                        If oBEEmpresaPeriodo.ValorSemanal(IIf(index < 0, index + 7, index)) = "1" Then
                            oFechaHasta = DateTime.Now.AddDays(-counterDays)
                            Exit For
                        End If
                        counterDays += 1
                    Next
                Case EmsambladoComun.ParametrosSistema.TipoPeriodo.Mensual
                    If DateTime.Now.Day >= oBEEmpresaPeriodo.ValorMensual Then
                        oFechaHasta = DateTime.Now.AddDays(-(DateTime.Now.Day - oBEEmpresaPeriodo.ValorMensual))
                    Else
                        Dim mesAnterior As DateTime = DateTime.Now.AddMonths(-1)
                        Dim diasMesAnt As Integer = DateTime.DaysInMonth(mesAnterior.Year, mesAnterior.Month)
                        If diasMesAnt < oBEEmpresaPeriodo.ValorMensual Then
                            oBEEmpresaPeriodo.ValorMensual = diasMesAnt
                        End If
                        oFechaHasta = mesAnterior.AddDays(oBEEmpresaPeriodo.ValorMensual - DateTime.Now.Day)
                    End If
                Case Else
                    oFechaHasta = DateTime.Now
            End Select
        Else
            oFechaHasta = DateTime.Now
        End If
        Return oFechaHasta
    End Function
    Public Function ActualizarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Dim result As Integer = 0
        Try

            Using transaccionscope As New TransactionScope()
                result = oTransferencia.ActualizarTransaccionesaLiquidar(be)
                transaccionscope.Complete()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function
    Public Function ObtenerTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BETransferencia
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Return oTransferencia.ObtenerTransaccionesaLiquidar(be)
    End Function
    Public Function ValidarPendientesaConciliar(ByVal be As BETransferencia) As Integer
        Dim oTransferencia As New SPE.AccesoDatos.DAEmpresaContratante
        Dim result As Integer = 0
        result = oTransferencia.ValidarPendientesaConciliar(be)
        Return result
    End Function
#Region "Configuraci�n de cuenta y banco de empresas"
    Public Function InsertarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim oDAEmpresaContratante As New DAEmpresaContratante
        Return oDAEmpresaContratante.InsertarCuentaEmpresa(be)
    End Function
    Public Function ActualizarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim oDAEmpresaContratante As New DAEmpresaContratante
        Return oDAEmpresaContratante.ActualizarCuentaEmpresa(be)
    End Function
    Public Function ConsultarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of BusinessEntityBase)
        Dim oDAEmpresaContratante As New DAEmpresaContratante
        Return oDAEmpresaContratante.ConsultarCuentaEmpresa(be)
    End Function
    Public Function ConsultarCuentaEmpresaPorId(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim oDAEmpresaContratante As New DAEmpresaContratante
        Return oDAEmpresaContratante.ConsultarCuentaEmpresaPorId(id)
    End Function
#End Region
#Region "LiquidacionLotes"
    Public Function RecuperarEmpresasLiquidacion(ByVal FechaDe As Date, ByVal FechaAl As Date, ByVal idPeriodoLiquidacion As Integer, ByVal idMoneda As Integer) As System.Collections.Generic.List(Of BEEmpresaContratante)
        Dim oDAEmpresaContratante As New DAEmpresaContratante
        Return oDAEmpresaContratante.RecuperarEmpresasLiquidacion(FechaDe, FechaAl, idPeriodoLiquidacion, idMoneda)
    End Function
#End Region

End Class
