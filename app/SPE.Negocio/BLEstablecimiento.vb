Imports System.Net.Mail
Imports SPE.EmsambladoComun
Imports System.Configuration
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Transactions
Imports System.Xml

Public Class BLEstablecimiento
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAEstablecimiento
    End Function

    '************************************************************************************************** 
    ' M�todo          : ObtenerOperadores
    ' Descripci�n     : Devuelve la lista de todos los operadores
    ' Autor           : Christian Villanueva Nicho
    ' Fecha/Hora      : 28/12/2008
    ' Parametros_In   : Ninguno
    ' Parametros_Out  : Lista de entidades del tipo BEOperador
    ''*************************************************************************************************
    Public Function ObtenerOperadores() As List(Of BEOperador)
        Try
            Dim odaEstablecimiento As New DAEstablecimiento
            Return odaEstablecimiento.ObtenerOperadores()
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarOrdenPago
    ' Descripci�n     : Consulta de C�digo de Identificaci�n de Pago
    ' Autor           : Christian Villanueva Nicho
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Nro del Establecimiento y Nro de el C�digo de Identificaci�n de Pago
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function ConsultarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
        Try
            Dim obeEstablecimiento As New DAEstablecimiento
            Return obeEstablecimiento.ConsultarOrdenPago(obeParametroPOS)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function

    '************************************************************************************************** 
    ' M�todo          : CancelarOrdenPago
    ' Descripci�n     : Cancelar C�digo de Identificaci�n de Pago
    ' Autor           : Christian Villanueva Nicho
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Nro del Establecimiento, Nro de operacion,Nro de C�digo de Identificaci�n de Pago, Monto, Moneda, Medio de Pago
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function CancelarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim obeOrdenPago As New DAOrdenPago()
        Dim objOrdenPago As New BEOrdenPago()
        Dim options As TransactionOptions = New TransactionOptions()
        Dim resultado As String = ""
        Dim objEmail As New BLEmail()
        Dim documentoXML As XmlDocument
        Dim obeOPPOSResultado As BEOPPOSResultado = Nothing

        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()
                obeOPPOSResultado = obeEstablecimiento.CancelarOrdenPago(obeParametroPOS)
                resultado = obeOPPOSResultado.XMLRespuesta
                'Se registra la notificaci�n. UrlError
                Using oblServicionNotificacion As New BLServicioNotificacion()
                    Dim obeOrdenPagon As BEOrdenPago = Nothing
                    Using odaOrdenPago As New DAOrdenPago()
                        obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeParametroPOS.NroOrdenPago)
                    End Using
                    Dim obeServicio As BEServicio
                    Using odaServicio As New DAServicio()
                        obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
                    End Using

                    If obeServicio.FlgNotificaMovimiento = True Then
                        oblServicionNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPagon, obeOPPOSResultado.IdMovimiento, obeOrdenPagon.IdUsuarioCreacion, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion)
                    End If
                End Using

                transaccionscope.Complete()
            End Using

            documentoXML = New XmlDocument
            documentoXML.LoadXml(resultado)

            objOrdenPago.IdOrdenPago = Convert.ToInt32(obeParametroPOS.NroOrdenPago)
            objOrdenPago = obeOrdenPago.ConsultarOrdenPagoPorIdPOS(objOrdenPago)

            If objOrdenPago.IdOrdenPago > 0 And documentoXML.DocumentElement.FirstChild.InnerText = SPE.EmsambladoComun.ParametrosSistema.CodigoMensajePOS.PagoRealizado Then

                With objOrdenPago
                    .NumeroOrdenPago = objOrdenPago.NumeroOrdenPago
                    .DescripcionEmpresa = objOrdenPago.DescripcionEmpresa
                    .DescripcionServicio = objOrdenPago.DescripcionServicio
                    .SimboloMoneda = objOrdenPago.SimboloMoneda
                    .Total = objOrdenPago.Total
                    .ConceptoPago = objOrdenPago.ConceptoPago
                    .DescripcionCliente = objOrdenPago.DescripcionCliente
                End With

                objEmail.EnviarCorreoCancelacionOrdenPagoPOS(objOrdenPago.NumeroOrdenPago, objOrdenPago.OcultarEmpresa, _
                objOrdenPago.DescripcionEmpresa, _
                objOrdenPago.DescripcionServicio, _
                objOrdenPago.SimboloMoneda, _
                objOrdenPago.Total, _
                objOrdenPago.ConceptoPago, _
                objOrdenPago.DescripcionCliente)

            End If


        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : Anular OrdenPago
    ' Descripci�n     : Consulta de C�digo de Identificaci�n de Pago
    ' Autor           : Karen Figueroa Gonzales
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Nro del Establecimiento y Nro de el C�digo de Identificaci�n de Pago
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function AnularOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim options As TransactionOptions = New TransactionOptions()
        Dim resultado As String = ""
        Dim obeOPPOSResultado As BEOPPOSResultado = Nothing
        Try

            options.IsolationLevel = IsolationLevel.ReadCommitted

            Using transaccionscope As New TransactionScope()
                obeOPPOSResultado = obeEstablecimiento.AnularOrdenPago(obeParametroPOS)
                resultado = obeOPPOSResultado.XMLRespuesta

                'Se registra la notificaci�n. UrlError
                Using oblServicionNotificacion As New BLServicioNotificacion()
                    Dim obeOrdenPagon As BEOrdenPago = Nothing
                    Using odaOrdenPago As New DAOrdenPago()
                        obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeParametroPOS.NroOrdenPago)
                    End Using
                    Dim obeServicio As BEServicio
                    Using odaServicio As New DAServicio()
                        obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
                    End Using

                    If obeServicio.FlgNotificaMovimiento = True Then
                        oblServicionNotificacion.RegistrarServicioNotificacionUrlError(obeOrdenPagon, obeOPPOSResultado.IdMovimiento, obeOrdenPagon.IdUsuarioCreacion, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.ServicioWeb, obeServicio.IdGrupoNotificacion)
                    End If
                End Using

                transaccionscope.Complete()
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja
    ' Descripci�n     : Aperturar Caja de Establecimiento
    ' Autor           : Karen Figueroa Gonzales
    ' Fecha/Hora      : 26/12/2008
    ' Parametros_In   : Nro del Establecimiento, Codigo Moneda, Monto 
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function AperturarCaja(ByVal obeParametroPOS As BEParametroPOS) As String

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim options As TransactionOptions = New TransactionOptions()
        Dim resultado As String = ""
        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()

                resultado = obeEstablecimiento.AperturarCaja(obeParametroPOS)
                transaccionscope.Complete()

            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function


    '************************************************************************************************** 
    ' M�todo          : Cerrar Caja
    ' Descripci�n     : Cerrar Caja de Establecimiento
    ' Autor           : Karen Figueroa Gonzales
    ' Fecha/Hora      : 12/01/2009
    ' Parametros_In   : Nro de Operacion, C�digo de establecimiento, Medio de Pago, Moneda, Monto 
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function CerrarCaja(ByVal obeParametroPOS As BEParametroPOS) As String

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim options As TransactionOptions = New TransactionOptions()
        Dim resultado As String = ""
        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()

                Dim documentoXML As XmlDocument
                resultado = obeEstablecimiento.CerrarCaja(obeParametroPOS)
                documentoXML = New XmlDocument
                documentoXML.LoadXml(resultado)

                'Verificamos si el cierre se ha realizado correctamente para confirmar la transaccion
                If documentoXML.DocumentElement.FirstChild.InnerText = SPE.EmsambladoComun.ParametrosSistema.CodigoMensajePOS.CierreRealizado Then
                    transaccionscope.Complete()
                Else
                    transaccionscope.Dispose()
                End If

            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function


    Public Function CerrarAperturarCaja() As String

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim options As TransactionOptions = New TransactionOptions()
        Dim resultado As String = ""
        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()

                resultado = obeEstablecimiento.CerrarAperturarCaja()
                'Verificamos si el cierre se ha realizado correctamente para confirmar la transaccion
                If resultado = SPE.EmsambladoComun.ParametrosSistema.CodigoMensajePOS.CierreRealizado Then
                    transaccionscope.Complete()
                Else
                    transaccionscope.Dispose()
                End If

            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja
    ' Descripci�n     : Aperturar Caja de Establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 17/02/2009
    ' Parametros_In   : Nro del Establecimiento, Codigo Moneda, Monto 
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function AperturarCaja() As String

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim options As TransactionOptions = New TransactionOptions()
        Dim resultado As String = ""
        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()

                resultado = obeEstablecimiento.AperturarCaja()
                transaccionscope.Complete()

            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja
    ' Descripci�n     : Aperturar Caja de Establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : Nro del Establecimiento, Codigo Moneda, Monto 
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function GetLiquidacionPendientexIdEstablecimiento(ByVal objEstablecimiento As BEEstablecimiento) As List(Of BEEstablecimiento)

        Dim obeEstablecimiento As New DAEstablecimiento
        'Dim options As TransactionOptions = New TransactionOptions()
        Dim objBEEstablecimiento As List(Of BEEstablecimiento)
        Try
            'options.IsolationLevel = IsolationLevel.ReadCommitted
            'Using transaccionscope As New TransactionScope()

            objBEEstablecimiento = obeEstablecimiento.GetLiquidacionPendientexIdEstablecimiento(objEstablecimiento)
            'transaccionscope.Complete()

            'End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return objBEEstablecimiento
    End Function

    '************************************************************************************************** 
    ' M�todo          : Aperturar Caja
    ' Descripci�n     : Aperturar Caja de Establecimiento
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : Nro del Establecimiento, Codigo Moneda, Monto 
    ' Parametros_Out  : Texto en un string que contiene el xml. 
    ''*************************************************************************************************
    Public Function GetPendientesCancelacion(ByVal objComercio As BEComercio) As List(Of BEEstablecimiento)

        Dim obeEstablecimiento As New DAEstablecimiento
        'Dim options As TransactionOptions = New TransactionOptions()
        Dim objBEEstablecimiento As List(Of BEEstablecimiento)
        Try
            'options.IsolationLevel = IsolationLevel.ReadCommitted
            'Using transaccionscope As New TransactionScope()

            objBEEstablecimiento = obeEstablecimiento.GetPendientesCancelacion(objComercio)
            'transaccionscope.Complete()

            'End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return objBEEstablecimiento
    End Function


    '************************************************************************************************** 
    ' M�todo          : LiquidarBloque
    ' Descripci�n     : LiquidarBloque
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : BEAperturaOrigen 
    ' Parametros_Out  : 
    ''*************************************************************************************************
    Public Function LiquidarBloque(ByVal objAperturaOrigen As List(Of BEAperturaOrigen)) As Integer

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim options As TransactionOptions = New TransactionOptions()
        Dim cadena As String = ""

        Dim result As Integer
        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()


                'For Each oBEAperturaOrigen As BEAperturaOrigen In objAperturaOrigen
                '    cadena = cadena + "''" + oBEAperturaOrigen.idEstablecimiento.ToString() + "'',"
                'Next
                'If (cadena.Substring(cadena.Length - 3, 3) = "'',") Then
                '    cadena = cadena.Substring(0, cadena.Length - 1)
                'End If
                'result = obeEstablecimiento.LiquidarBloque("'" + cadena + "'")
                For Each oBEAperturaOrigen As BEAperturaOrigen In objAperturaOrigen
                    cadena = cadena + "" + oBEAperturaOrigen.idEstablecimiento.ToString() + ","
                Next
                If (cadena.Substring(cadena.Length - 1, 1) = ",") Then
                    cadena = cadena.Substring(0, cadena.Length - 1)
                End If
                result = obeEstablecimiento.LiquidarBloque(cadena)
                transaccionscope.Complete()
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return 1
    End Function
    '************************************************************************************************** 
    ' M�todo          : LiquidarBloque
    ' Descripci�n     : LiquidarBloque
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 23/02/2009
    ' Parametros_In   : BEAperturaOrigen 
    ' Parametros_Out  : 
    ''*************************************************************************************************
    Public Function ConsultarCajasLiquidadas(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEMovimiento)

        Dim obeEstablecimiento As New DAEstablecimiento
        Dim oListMovimiento As List(Of BEMovimiento)

        Dim options As TransactionOptions = New TransactionOptions()
        Dim cadena As String = String.Empty


        Try
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()


                oListMovimiento = obeEstablecimiento.ConsultarCajasLiquidadas(obeOrdenPago)
                transaccionscope.Complete()

            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return oListMovimiento
    End Function

    'Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)

    '    Select Case pSortExpression
    '        Case "SerieTerminal"
    '            list.Sort(New SPE.Entidades.BEEstablecimiento.SerieTerminalComparer())
    '        Case "RazonSocial"
    '            list.Sort(New SPE.Entidades.BEEstablecimiento.RazonSocialComparer())
    '        Case "Contacto"
    '            list.Sort(New SPE.Entidades.BEEstablecimiento.ContactoComparer())
    '        Case "Ruc"
    '            list.Sort(New SPE.Entidades.BEEstablecimiento.RUCComparer())
    '        Case "Telefono"
    '            list.Sort(New SPE.Entidades.BEEstablecimiento.TelefonoComparer())
    '        Case "DescripcionEstado"
    '            list.Sort(New SPE.Entidades.BEEstablecimiento.EstadoComparer())

    '    End Select

    'End Sub

End Class
