Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.IO
Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Public Class BLFTPArchivo
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase


    '************************************************************************************************** 
    ' M�todo          : GetConstructorDataAccessObject
    ' Descripci�n     : Obtiene el objeto data access que utilizara la clase base
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 08/01/2009
    ' Parametros_In   : 
    ' Parametros_Out  : clase DataAccessMaintenanceBase
    '**************************************************************************************************
    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAFTPArchivo
    End Function


    '************************************************************************************************** 
    ' M�todo          : ObtenerFTPArchivoPorId
    ' Descripci�n     : Obtiene archivos FTP por id
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 12/01/2009
    ' Parametros_In   : Id Archivo, Expresion de ordenamiento, Forma de ordenamiento
    ' Parametros_Out  : Instancia de la entidad BEFTPArchivo
    '**************************************************************************************************
    Function ObtenerFTPArchivoPorId(ByVal id As Object, ByVal OrderBy As String, ByVal IsAccending As Boolean) As BEFTPArchivo
        Dim odaDAFTPArchivo As New DAFTPArchivo
        Dim obeFTPArchivo As BEFTPArchivo
        obeFTPArchivo = odaDAFTPArchivo.ObtenerFTPArchivoPorId(id)


        DefinirListaOrdenadaFTPDetalleArchivo(obeFTPArchivo.DetalleArchivoFTP, OrderBy)
        If Not (IsAccending) Then
            obeFTPArchivo.DetalleArchivoFTP.Reverse()
        End If

        Return obeFTPArchivo

    End Function


    '************************************************************************************************** 
    ' M�todo          : DoDefineOrderedList
    ' Descripci�n     : Ordena la lista de archivos de acuerdo a la expresion
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 12/01/2009
    ' Parametros_In   : Lista de entidad BusinessEntityBase
    ' Parametros_Out  : Lista de entidad BusinessEntityBase ordenada
    '**************************************************************************************************
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        MyBase.DoDefineOrderedList(list, pSortExpression)
        Select Case pSortExpression
            Case "Servicio"
                list.Sort(New SPE.Entidades.BEFTPArchivo.ServicioComparer())
            Case "NombreArchivo"
                list.Sort(New SPE.Entidades.BEFTPArchivo.NombreArchivoComparer())
            Case "FechaGenerado"
                list.Sort(New SPE.Entidades.BEFTPArchivo.FechaGeneracionComparer())
            Case "TipoArchivo"
                list.Sort(New SPE.Entidades.BEFTPArchivo.TipoArchivoComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEFTPArchivo.DescripcionEstadoComparer())

        End Select
    End Sub


    '************************************************************************************************** 
    ' M�todo          : DefinirListaOrdenadaFTPDetalleArchivo
    ' Descripci�n     : Ordena la lista de detalle de archivos FTP
    ' Autor           : Christian Claros Lindo
    ' Fecha/Hora      : 13/01/2009
    ' Parametros_In   : Lista de entidad BEDetalleArchivoFTP
    ' Parametros_Out  : Lista de entidad BEDetalleArchivoFTP ordenada
    '**************************************************************************************************
    Public Sub DefinirListaOrdenadaFTPDetalleArchivo(ByVal list As List(Of BEDetalleArchivoFTP), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "IdUsuario"
                list.Sort(New SPE.Entidades.BEDetalleArchivoFTP.IdUsuarioComparer())
            Case "OrdenIdComercio"
                list.Sort(New SPE.Entidades.BEDetalleArchivoFTP.OrdenIdComercioComparer())
            Case "TipoPago"
                list.Sort(New SPE.Entidades.BEDetalleArchivoFTP.TipoPagoComparer())
            Case "FechaPago "
                list.Sort(New SPE.Entidades.BEDetalleArchivoFTP.FechaPagoComparer())
            Case "Moneda"
                list.Sort(New SPE.Entidades.BEDetalleArchivoFTP.MonedaComparer())
            Case "Monto"
                list.Sort(New SPE.Entidades.BEDetalleArchivoFTP.MontoComparer())

        End Select
    End Sub


    Public ReadOnly Property DAFTPArchivo() As DAFTPArchivo
        Get
            Return DataAccessObject
        End Get

    End Property


    Function ProcesarArchivoFTPParaNotificar() As List(Of BEServicio)
        Try
            '-- Registrar 
            Dim options As TransactionOptions = New TransactionOptions()
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()
                DAFTPArchivo.RegistrarArchivosOrdenPagoParaNotificar()
                transaccionscope.Complete()
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
        End Try

        Dim listaServicios As List(Of BEServicio) = Nothing
        '-- Consultar Servicios


        listaServicios = DAFTPArchivo.ConsultarServiciosParaNotificacionArchivoFTP()
        For Each obeservicio As BEServicio In listaServicios
            ' consultar cabecera 
            Try
                Dim listaArchivos As List(Of BEFTPArchivo) = DAFTPArchivo.ConsultarFTPArchivoCabecera(obeservicio.IdServicio)
                For Each obeFTPArchivo As BEFTPArchivo In listaArchivos

                    Try
                        ' consultar archivos detalle


                        Dim listaArchivosDetalle As List(Of BEDetalleArchivoFTP) = DAFTPArchivo.ConsultarDetalleArchivoPorIdFTPArchivo(obeFTPArchivo.IdFtpArchivo)
                        obeFTPArchivo.DetalleArchivoFTP = listaArchivosDetalle
                        Dim options1 As TransactionOptions = New TransactionOptions()
                        options1.IsolationLevel = IsolationLevel.ReadCommitted
                        Using transaccionscope As New TransactionScope()

                            GenerarArchivoGenerico(obeFTPArchivo)
                            DAFTPArchivo.ActualizarFTPArchivoEstado(obeFTPArchivo.IdFtpArchivo, SPE.EmsambladoComun.ParametrosSistema.ArchivoFTPEstado.Generado)

                            transaccionscope.Complete()
                        End Using

                    Catch ex As Exception
                        'Logger.Write(ex)
                    End Try
                Next
                obeservicio.FTPArchivos = listaArchivos

            Catch ex As Exception
                'Logger.Write(ex)
            End Try
        Next


        Return listaServicios
    End Function
    Public Function GenerarArchivoGenerico(ByVal be As BEFTPArchivo) As Integer
        'poner if si existe el archivo si existe eliminar
        If (be.IdTipoArchivo = SPE.EmsambladoComun.ParametrosSistema.ArchivoFTPTipoArchivo.Pago) Then
            GenerarArchivoPagadas(be)
        Else
            GenerarArchivoExpiradas(be)
        End If

    End Function
    Public Function GenerarArchivoPagadas(ByVal be As BEFTPArchivo) As Integer
        Dim Path As String
        Path = ObtenerDirectorio(be.IdServicio) + "\" + be.NombreArchivo
        If System.IO.File.Exists(Path) Then
            Kill(Path)
        End If
        Using fs As StreamWriter = File.AppendText(Path)
            fs.Flush()
            For Each bed As BEDetalleArchivoFTP In be.DetalleArchivoFTP
                Dim fecha As String = DameFechaFormato(bed.FechaPago.Day) + "/" + DameFechaFormato(bed.FechaPago.Month) + "/" + DameFechaFormato(bed.FechaPago.Year)
                fs.WriteLine(bed.IdUsuario.ToString() + "|" + bed.TipoPago.ToString() + "|" + fecha + "|" + bed.Monto.ToString())
            Next
        End Using
        be.ArchivoBinario = LeerArchivoEnBinario(ObtenerDirectorio(be.IdServicio) + "\" + be.NombreArchivo)
        Return 1
    End Function
    Private Function DameFechaFormato(ByVal a As Integer) As String
        Dim stra As String = a.ToString()
        If (stra.Length = 1) Then
            Return "0" + stra
        Else
            Return stra
        End If
    End Function


    Public Function GenerarArchivoExpiradas(ByVal be As BEFTPArchivo) As Integer
        Dim strHabilitar As String = "I"
        Dim Path As String
        Path = ObtenerDirectorio(be.IdServicio) + "\" + be.NombreArchivo
        If System.IO.File.Exists(Path) Then
            Kill(Path)
        End If
        Using fs As StreamWriter = File.AppendText(Path)
            fs.Flush()
            For Each bed As BEDetalleArchivoFTP In be.DetalleArchivoFTP
                fs.WriteLine(bed.IdUsuario.ToString() + "|" + strHabilitar.ToString())
            Next
        End Using
        be.ArchivoBinario = LeerArchivoEnBinario(ObtenerDirectorio(be.IdServicio) + "\" + be.NombreArchivo)
        Return 1
    End Function
    Private Function LeerArchivoEnBinario(ByVal ruta As String) As Byte()
        Dim bytes As Byte()
        Using sstream As Stream = File.OpenRead(ruta)
            Dim bufin As BufferedStream = New BufferedStream(sstream)
            bytes = New Byte(sstream.Length) {}
            bufin.Read(bytes, 0, sstream.Length)
        End Using
        Return bytes
    End Function

    Private Function RutaArchivo() As String
        Return System.Configuration.ConfigurationManager.AppSettings("RutaArchivosFTP")
    End Function
    Private Function ObtenerDirectorio(ByVal nom As String) As String
        Dim directorionombre As String = RutaArchivo() + "\" + nom
        If Not System.IO.Directory.Exists(directorionombre) Then
            System.IO.Directory.CreateDirectory(directorionombre)
        End If
        Return directorionombre
    End Function
End Class
