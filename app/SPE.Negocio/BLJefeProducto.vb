Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports _3Dev.FW.Negocio
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.AccesoDatos

Public Class BLJefeProducto
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAJefeProducto
    End Function

    Property odaJefeProducto() As SPE.AccesoDatos.DAJefeProducto
        Get
            Return CType(DataAccessObject, SPE.AccesoDatos.DAJefeProducto)
        End Get
        Set(ByVal value As SPE.AccesoDatos.DAJefeProducto)

        End Set
    End Property

    Private Function CodigoRegistro() As String
        Return Guid.NewGuid.ToString()
    End Function

    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim resultado As Integer
        Try
            If odaJefeProducto.GetObjectByParameters("", be) > 0 Then
                Return SPE.EmsambladoComun.ParametrosSistema.ExisteEmail.Existe
            Else
                Dim options As TransactionOptions = New TransactionOptions()
                Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)
                Dim EncodePwd, Pwd As String

                EncodePwd = SeguridadComun.EncryptarCodigo(obeJefeProducto.Password.Trim)
                options.IsolationLevel = IsolationLevel.ReadCommitted

                Using TransactionScope As New TransactionScope()

                    obeJefeProducto.CodigoRegistro = CodigoRegistro()
                    Pwd = obeJefeProducto.Password
                    obeJefeProducto.Password = EncodePwd
                    resultado = MyBase.InsertRecord(be)
                    obeJefeProducto.IdJefeProducto = resultado
                    RegistraOrigenCancelacion(obeJefeProducto.ListaTipoOrigenCancelacion, obeJefeProducto.IdJefeProducto)

                    Dim Email As New BLEmail
                    Email.EnviarCorreoConfirmacionUsuario(obeJefeProducto.IdUsuario, _
                    obeJefeProducto.Nombres, Pwd, obeJefeProducto.CodigoRegistro, _
                    obeJefeProducto.Email)
                    TransactionScope.Complete()
                    resultado = 1
                End Using

            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

        Return resultado

    End Function

    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim resultado As Integer = 0

        Try
            Dim options As TransactionOptions = New TransactionOptions()
            Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)
            options.IsolationLevel = IsolationLevel.ReadCommitted
            Using transaccionscope As New TransactionScope()
                resultado = MyBase.UpdateRecord(be)
                RegistraOrigenCancelacion(obeJefeProducto.ListaTipoOrigenCancelacion, obeJefeProducto.IdJefeProducto)
                transaccionscope.Complete()
            End Using

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado

    End Function

    Private Sub RegistraOrigenCancelacion(ByVal ListOrigenCancelacion As List(Of SPE.Entidades.BETipoOrigenCancelacion), ByVal idJefeProducto As Integer)

        Dim NroOrigenes, Contador As Integer
        Dim IsSelect As Boolean = False
        Dim odaJefeProducto As New SPE.AccesoDatos.DAJefeProducto
        Dim obeTipoOrigenCancelacion As New SPE.Entidades.BETipoOrigenCancelacion
        Try
            Contador = 0
            NroOrigenes = ListOrigenCancelacion.Count
            For Each obeTipoOrigenCancelacion In ListOrigenCancelacion
                If obeTipoOrigenCancelacion.IdEstado = 1 Then IsSelect = True
                odaJefeProducto.RegistraJefeProductoTipoOrigenCancelacion(idJefeProducto, obeTipoOrigenCancelacion.IdTipoOrigenCancelacion, _
                IsSelect)
                IsSelect = False
            Next

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Sub

End Class
