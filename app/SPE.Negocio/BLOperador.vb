Imports System.Net.Mail
Imports SPE.EmsambladoComun
Imports System.Configuration
Imports System.IO
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Transactions


Public Class BLOperador
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAOperador
    End Function
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        MyBase.DoDefineOrderedList(list, pSortExpression)
        Select Case pSortExpression
            Case "Codigo"
                list.Sort(New SPE.Entidades.BEOperador.CodigoComparer())
            Case "Descripcion"
                list.Sort(New SPE.Entidades.BEOperador.DescripcionComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEOperador.DescripcionEstadoComparer())

        End Select
    End Sub
    'Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
    '    Return MyBase.InsertRecord(be)

    '    'If MyBase.InsertRecord(be) = 0 Then
    '    '    Return 0
    '    'ElseIf MyBase.InsertRecord(be) = 1 Then
    '    '    Return 1
    '    'End If
    'End Function
    Property odaOperador() As SPE.AccesoDatos.DAOperador
        Get
            Return CType(DataAccessObject, SPE.AccesoDatos.DAOperador)
        End Get
        Set(ByVal value As SPE.AccesoDatos.DAOperador)

        End Set
    End Property
End Class
