Imports System
Imports System.Threading
Imports System.Transactions
Imports System.Collections.Generic
Imports System.Text.RegularExpressions

Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Utilitario
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Criptography
Imports System.Xml
Imports _3Dev.FW.Entidades
Imports SPE.AccesoDatos.Seguridad
Imports System.Configuration
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization.Json
Imports MongoDB.Bson
Imports SPE.Logging


Public Class BLOrdenPago
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase
    Private ReadOnly _nlogger As ILogger = New Logger()
    Dim culturaPeru = New CultureInfo(AppSettings("CulturaIdiomaPeru"))


#Region "Metodos Base"
    Private Function DAOrdenPago() As DAOrdenPago
        Return CType(DataAccessObject, SPE.AccesoDatos.DAOrdenPago)
    End Function

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New DAOrdenPago()
    End Function

#End Region

#Region "Metodos de Consulta"
    Public Overrides Function GetListByParametersOrderedBy(ByVal key As String, ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal orderedBy As String, ByVal isAccending As Boolean) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Select Case key
            Case "ConsultPagoServicios"
                Return ConsultarPagosServicios(be, orderedBy, isAccending)

                'agregado formulario------------------>
            Case "ConsultPagoServiciosFormulario"
                Return ConsultarPagosServiciosFormulario(be, orderedBy, isAccending)

        End Select
        Return MyBase.GetListByParametersOrderedBy(key, be, orderedBy, isAccending)
    End Function
    Public Function ConsultarOrdenPagoPorOrdenIdComercio(ByVal OrdenIdComercio As String, ByVal IdServicio As Integer) As BEOPXServClienCons
        Dim OdaOrdenPago As New SPE.AccesoDatos.DAOrdenPago
        Return OdaOrdenPago.ConsultarOrdenPagoPorOrdenIdComercio(OrdenIdComercio, IdServicio)
    End Function
    Public Function ConsultarOrdenPagoPorNumeroOrdenPago(ByVal NumeroOrdenPago As String) As Integer
        Dim OdaOrdenPago As New SPE.AccesoDatos.DAOrdenPago

        Return OdaOrdenPago.ConsultarOrdenPagoPorNumeroOrdenPago(NumeroOrdenPago)
    End Function

    Public Function ConsultarPagosServicios(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal orderedBy As String, ByVal isAccending As Boolean) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim listresult As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = DAOrdenPago().ConsultarPagosServicios(be)
        'DefinirOrdenListaPagoServicio(listresult, orderedBy)
        'Upd <PeruCom FaseIII>
        If (be.PageSize = 0) Then 'sin Paginacion
            If Not (isAccending) Then
                listresult.Reverse()
            End If
        End If

        Return listresult

    End Function


    'nuevo consultar formulario---------------------------->
    Public Function ConsultarPagosServiciosFormulario(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal orderedBy As String, ByVal isAccending As Boolean) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim listresult As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = DAOrdenPago().ConsultarPagosServiciosFormulario(be)
        'DefinirOrdenListaPagoServicio(listresult, orderedBy)
        'Upd <PeruCom FaseIII>
        If (be.PageSize = 0) Then 'sin Paginacion
            If Not (isAccending) Then
                listresult.Reverse()
            End If
        End If

        Return listresult

    End Function


    ''nuevo formlario consultar activo
    Public Function ConsutarFormularioActivo(ByVal IdServicio As Integer) As Boolean
        Return DAOrdenPago().ConsutarFormularioActivo(IdServicio)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarOrdenesPagoListaParaExpiradas
    ' Descripci�n     : Consulta las ordenes pago expiradas
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   :  
    ' Parametros_Out  : Instancia de la entidad BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOrdenesPagoListaParaExpiradas() As List(Of BEOrdenPago)
        Return DAOrdenPago().ConsultarOrdenesPagoListaParaExpiradas()
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarOrdenPagoRecepcionar
    ' Descripci�n     : Consulta de el C�digo de Identificaci�n de Pago para una recepcion por una Agencia
    ' Autor           : Kleber Granados
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   :  
    ' Parametros_Out  : Instancia de la entidad BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOrdenPagoRecepcionar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Return DAOrdenPago().ConsultarOrdenPagoRecepcionar(obeOrdenPago)
    End Function


    '************************************************************************************************** 
    ' M�todo          : ConsultarOrdenPagoPendientes
    ' Descripci�n     : Consulta de c�digos de Identificaci�n de Pago pendientes
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago,orderedBy,isAccending
    ' Parametros_Out  : Lista de entidades BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOrdenPagoPendientes(ByVal obeOrdePago As SPE.Entidades.BEOrdenPago, ByVal orderedBy As String, ByVal isAccending As Boolean) As List(Of BEOrdenPago)
        Return DAOrdenPago().ConsultarOrdenPagoPendientes(obeOrdePago)
        ''''EL ORDENAMIENTO Y LA PAGINACION se hace A Nivel de BD, Los datos de la paginacion y de el Ordenamiento se pasan por ell objeto obeOrdePago
        ''''IMPLEMENTADO EN LA FASE III
        'Dim listresult As List(Of BEOrdenPago) = DAOrdenPago().ConsultarOrdenPagoPendientes(obeOrdePago)
        'DefinirListaOrdenadaOrdenPago(listresult, orderedBy)
        'If Not (isAccending) Then
        '    listresult.Reverse()
        'End If
        'Return listresult
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarOrdenPagoPorId
    ' Descripci�n     : Consulta de c�digos de Identificaci�n de Pago pendientes
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : Instancia de la entidad BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOrdenPagoPorId(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim odaOrdenPago As New DAOrdenPago
        Return odaOrdenPago.ConsultarOrdenPagoPorId(obeOrdenPago)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarDetalleOrdenPago
    ' Descripci�n     : Consulta de detalle de C�digo de Identificaci�n de Pago
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : Instancia de la entidad BEDetalleOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarDetalleOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As BEDetalleOrdenPago
        Dim odaDetalleOP As New DAOrdenPago
        Return odaDetalleOP.ConsultarDetalleOrdenPago(obeOrdenPago)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarDetalleOrdenPago
    ' Descripci�n     : Consulta de c�digos de Identificaci�n de Pago
    ' Autor           : Kleber Granados
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : Lista de entidades BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
        Dim listresult As List(Of BEOrdenPago) = DAOrdenPago().ConsultarOrdenPago(obeOrdenPago)
        'DefinirListaOrdenadaOrdenPago(listresult, obeOrdenPago.OrderBy)
        'If Not (obeOrdenPago.IsAccending) Then
        '    listresult.Reverse()
        'End If
        Return listresult
    End Function

    '************************************************************************************************** 
    ' M�todo          : DoDefineOrderedList
    ' Descripci�n     : Define la clase con la cual se va ordenar para BusinessEntityBase 
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "FechaEmision"
                list.Sort(New SPE.Entidades.BEOrdenPago.FechaEmisionComparer())
            Case "FechaCancelacion"
                list.Sort(New SPE.Entidades.BEOrdenPago.FechaCancelacionComparer())
            Case "DescripcionEmpresa"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionEmpresaComparer())
            Case "DescripcionServicio"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionServicioComparer())
            Case "NumeroOrdenPago"
                list.Sort(New SPE.Entidades.BEOrdenPago.NumeroOrdenPagoComparer())
            Case "SimboloMoneda"
                list.Sort(New SPE.Entidades.BEOrdenPago.SimboloMonedaComparer())
            Case "Total"
                list.Sort(New SPE.Entidades.BEOrdenPago.TotalComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionEstadoComparer())
            Case "ClienteEmailReducido"
                list.Sort(New SPE.Entidades.BEOrdenPago.ClienteEmailReducidoComparer())
            Case "ClienteEmail"
                list.Sort(New SPE.Entidades.BEOrdenPago.ClienteEmailComparer())
            Case "DescripcionCliente"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionClienteComparer())

        End Select
    End Sub

    '************************************************************************************************** 
    ' M�todo          : DoDefineOrderedList
    ' Descripci�n     : Define la clase con la cual se va ordenar para BEOrdenPago
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Sub DefinirListaOrdenadaOrdenPago(ByVal list As System.Collections.Generic.List(Of BEOrdenPago), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "FechaEmision"
                list.Sort(New SPE.Entidades.BEOrdenPago.FechaEmisionBEOPComparer())
            Case "FechaCancelacion"
                list.Sort(New SPE.Entidades.BEOrdenPago.FechaCancelacionBEOPComparer())
            Case "ConceptoPago"
                list.Sort(New SPE.Entidades.BEOrdenPago.ConceptoPagoBEOPComparer())
            Case "DescripcionServicio"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionServicioBEOPComparer())
            Case "NumeroOrdenPago"
                list.Sort(New SPE.Entidades.BEOrdenPago.NumeroOrdenPagoBEOPComparer())
            Case "SimboloMoneda"
                list.Sort(New SPE.Entidades.BEOrdenPago.SimboloMonedaBEOPComparer())
            Case "Total"
                list.Sort(New SPE.Entidades.BEOrdenPago.TotalBEOPComparer())
            Case "DescripcionCliente"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionClienteBEOPComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEOrdenPago.DescripcionEstadoBEOPComparer())
        End Select
    End Sub


    '************************************************************************************************** 
    ' M�todo          : DoDefineOrderedList
    ' Descripci�n     : Define la clase con la cual se va ordenar para BusinessEntityBase para Servicio
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Sub DefinirOrdenListaPagoServicio(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "DescripcionServicio"
                list.Sort(New SPE.Entidades.BEPagoServicio.DescripcionServicioComparer())
            Case "DescripcionAgencia"
                list.Sort(New SPE.Entidades.BEPagoServicio.DescripcionAgenciaComparer())
            Case "DescripcionCliente"
                list.Sort(New SPE.Entidades.BEPagoServicio.DescripcionClienteComparer())
            Case "FechaEmision"
                list.Sort(New SPE.Entidades.BEPagoServicio.FechaEmisionComparer())
            Case "FechaCancelacion"
                list.Sort(New SPE.Entidades.BEPagoServicio.FechaCancelacionComparer())
            Case "NumeroOrdenPago"
                list.Sort(New SPE.Entidades.BEPagoServicio.NumeroOrdenPagoComparer())
            Case "SimboloMoneda"
                list.Sort(New SPE.Entidades.BEPagoServicio.SimboloMonedaComparer())
            Case "Total"
                list.Sort(New SPE.Entidades.BEPagoServicio.TotalComparer())
        End Select
    End Sub

    Public Function ConsultarOrdenPagoNotificar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Return DAOrdenPago().ConsultarOrdenPagoNotificar(obeOrdenPago)

    End Function

#End Region


#Region "Operaciones"
    'INI PAUL 29/08/2018
    Public Sub DeleteCipHtml(fileName As String)

        Dim s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("AccessKeyCipHtml")
        Dim s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("SecretKeyCipHtml")
        Dim s3Region As String = ConfigurationManager.AppSettings.[Get]("AWS_S3_REGION")
        Dim s3BucketName As String = ConfigurationManager.AppSettings.[Get]("BucketCipHtml")

        Dim amazonS3 As AmazonS3 = New SPE.Utilitario.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, String.Empty)

        _nlogger.Trace("Se intenta eliminar archivo: " + fileName + " en bucket: " + s3BucketName)
        amazonS3.DeleteCipHtml(s3BucketName, fileName)
    End Sub

    '************************************************************************************************** 
    ' M�todo          : RealizarProcesoExpiracionOrdenPago
    ' Descripci�n     : Proceso para verificar cuando expira una C�digo de Identificaci�n de Pago
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : 
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function RealizarProcesoExpiracionOrdenPago() As Integer
        Try
            Dim lista As List(Of BEOrdenPago) = ConsultarOrdenesPagoListaParaExpiradas()

            For i As Integer = 0 To lista.Count - 1
                ExpirarOrdenPago(lista(i))
            Next

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return 1
    End Function

    '************************************************************************************************** 
    ' M�todo          : ExpirarOrdenPago
    ' Descripci�n     : Actualiza el C�digo de Identificaci�n de Pago cuando expira
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago cargada
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function ExpirarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim result As Integer = 0
        Try
            Dim options As TransactionOptions = New TransactionOptions()
            options.IsolationLevel = IsolationLevel.ReadCommitted
            'Dim timeout As TimeSpan = TimeSpan.FromSeconds(ConfigurationManager.AppSettings("TimeoutTransaccion"))

            'Using transaccionscope As New TransactionScope(TransactionScopeOption.Required, timeout)
            Using transaccionscope As New TransactionScope()
                result = DAOrdenPago().ExpirarOrdenPago(obeOrdenPago)

                ''result=1 significa que no debe enviar notificacion de expiracion
                ''result=0 significa que si debe enviar notificacion de expiracion
                If result = 0 Then 'Pablo
                    'Se registra la notificaci�n. UrlError
                    Using oblServicionNotificacion As New BLServicioNotificacion()
                        Dim obeOrdenPagon As BEOrdenPago = Nothing
                        Using odaOrdenPago As New DAOrdenPago()
                            obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeOrdenPago.NumeroOrdenPago)
                            If Not obeOrdenPagon.Token Is Nothing Then DeleteCipHtml(obeOrdenPagon.Token & ".html") 'INI PAUL 04092018
                        End Using
                        Dim obeServicio As BEServicio
                        Using odaServicio As New DAServicio()
                            obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
                        End Using

                        If obeServicio.FlgNotificaExpiracion = True Then
                            oblServicionNotificacion.RegistrarServicioNotificacionUrlError(obeOrdenPagon, 0, obeOrdenPagon.IdUsuarioCreacion, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.ProcesoExpiraci�n, obeServicio.IdGrupoNotificacion)
                        End If
                    End Using
                End If
                transaccionscope.Complete()
            End Using

        Catch ex As Exception
            _nlogger.Trace("ExpirarOrdenPago-Error : " + ex.Message)
            'Logger.Write(ex)
            'Throw ex
        End Try
        Return result
    End Function

    '************************************************************************************************** 
    ' M�todo          : Recepcionar una Orden de Pago en una Agencia Recaudadora
    ' Descripci�n     : Actualiza el estado de la op a cancelada
    ' Autor           : Kleber Granados
    ' Fecha/Hora      : 16/12/2008
    ' Parametros_In   : Instancia de la entidad BEMovimiento,BEOrdenPago,BEDetalleTarjeta cargada.
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function ProcesoRecepcionarOrdenPago(ByVal obeMovimiento As BEMovimiento, ByVal obeOrdenPago As BEOrdenPago, ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
        '
        Dim result As Integer = 0
        Dim options As TransactionOptions = New TransactionOptions()
        options.IsolationLevel = IsolationLevel.ReadCommitted
        Dim odaOrdenPago As New DAOrdenPago()
        Dim odaAgencia As New DAAgenciaRecaudadora()
        Dim odaComun As New DAComun()
        Dim objEmail As New BLEmail()
        Dim idmovimiento As Integer = 0
        Try

            ' Se Evalua si se utiliza fecha valuta o no.
            Using oblAgenciaRecaudadora As New BLAgenciaRecaudadora()
                Dim obeAgenteCaja As New BEAgenteCaja
                obeAgenteCaja.IdUsuarioCreacion = obeMovimiento.IdUsuarioCreacion
                obeAgenteCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenteCaja.Aperturado
                Dim listCajaAperturada As List(Of BEAgenteCaja) = oblAgenciaRecaudadora.ConsultarAgenteCajaPorIdUsuarioIdEstado(obeAgenteCaja)

                If (listCajaAperturada.Count > 0) Then
                    Dim obeCajaAperturada As BEAgenteCaja = listCajaAperturada(0)
                    If (obeCajaAperturada.EstadoFechaValuta = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.EnUso) Then
                        obeMovimiento.FechaMovimiento = ObjectToDateTime(obeCajaAperturada.FechaApertura.ToShortDateString())
                    Else
                        Using oblConsultaComun As New BLComun()
                            obeMovimiento.FechaMovimiento = oblConsultaComun.ConsultarFechaHoraActual()
                        End Using
                    End If
                Else
                    Throw New Exception("obeAgenteCaja Aperturada no existe")
                End If

            End Using

            obeOrdenPago.FechaCancelacion = obeMovimiento.FechaMovimiento

            Using transaccionscope As New TransactionScope()

                '
                'REGISTRAR MOVIMIENTO
                idmovimiento = odaAgencia.RegistrarMovimiento(obeMovimiento)
                '
                'ACTUALIZAR C�digo de Identificaci�n de Pago
                odaOrdenPago.ActualizarOrdenPagoRecepcionar(obeOrdenPago)

                '
                'GUARDAR DETALLE TAREJETA
                If obeDetalleTarjeta.IdTarjeta <> 0 Then
                    obeDetalleTarjeta.IdMovimiento = idmovimiento
                    odaComun.RegistrarDetalleTarjeta(obeDetalleTarjeta)
                End If
                '

                '
                transaccionscope.Complete()                '

            End Using
            'M
            If Not obeOrdenPago.NumeroOrdenPago = String.Empty Then
                'Obtener la informaci�n completa del CIP
                Dim obeOrdenPagoCompleto As BEOrdenPago = Nothing
                Using odaOrdenPago1 As New DAOrdenPago()
                    obeOrdenPagoCompleto = odaOrdenPago1.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeOrdenPago.NumeroOrdenPago)
                End Using

                'ENVIO DE CORREO
                'objEmail.EnviarCorreoCancelacionOrdenPago(0, obeOrdenPago.NumeroOrdenPago, obeOrdenPago.OcultarEmpresa, obeOrdenPago.DescripcionEmpresa, obeOrdenPago.DescripcionServicio, obeOrdenPago.SimboloMoneda, obeOrdenPago.Total.ToString("#,#0.00"), obeOrdenPago.ConceptoPago, obeOrdenPago.DescripcionCliente.ToString)

                objEmail.EnviarCorreoCancelacionOrdenPago(obeOrdenPagoCompleto, obeOrdenPago.DescripcionCliente)

                'Registrar a la tabla de notificaci�n para notificar el pago
                Dim obeServicio As BEServicio
                Using odaServicio As New DAServicio()
                    obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagoCompleto.IdServicio), BEServicio)
                End Using

                If obeServicio.FlgNotificaMovimiento = True Then
                    Using oblServicionNotificacion As New BLServicioNotificacion()
                        oblServicionNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPagoCompleto, idmovimiento, obeMovimiento.IdUsuarioCreacion, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.Web, obeServicio.IdGrupoNotificacion)
                    End Using
                End If
            End If

            Return 1
        Catch ex As Exception
            'Logger.Write(ex)
            Return 0
        End Try
        '
    End Function

    '************************************************************************************************** 
    ' M�todo          : GenerarOrdenPago
    ' Descripci�n     : Registra la generacion de una C�digo de Identificaci�n de Pago por un cliente registrado
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago cargada,Lista de entidades BEDetalleOrdenPago 
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function GenerarOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
        Dim result As Integer = 0
        Try
            Using transaccionscope As New TransactionScope()
                result = DAOrdenPago().GenerarOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
                transaccionscope.Complete()
            End Using
            If Not (result = 0) Then
                Dim beOrdenP As New BEOrdenPago()
                beOrdenP.IdOrdenPago = result
                beOrdenP = DAOrdenPago().ConsultarOrdenPagoPorId(beOrdenP)
                Dim objEmail As New BLEmail()
                objEmail.EnviarCorreoGeneracionOrdenPago(beOrdenP, obeOrdenPago.DescripcionCliente)
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function
    '************************************************************************************************** 
    ' M�todo          : GenerarOrdenPago
    ' Descripci�n     : Registra la generacion de una C�digo de Identificaci�n de Pago por un cliente registrado
    ' Autor           : Enrique Rojas
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago cargada,Lista de entidades BEDetalleOrdenPago 
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function GenerarOrdenPagoXML(ByRef obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
        Dim result As Integer = 0
        Try
            Using transaccionscope As New TransactionScope()
                result = DAOrdenPago().GenerarOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
                transaccionscope.Complete()
            End Using
            If Not (result = 0) Then
                Dim beOrdenP As New BEOrdenPago()
                beOrdenP.IdOrdenPago = result
                beOrdenP = DAOrdenPago().ConsultarOrdenPagoPorId(beOrdenP)
                Dim objEmail As New BLEmail()
                Dim emailto As String = obeOrdenPago.EmailANotifGeneracion
                Dim strClienteDescripcion As String = ""
                If (obeOrdenPago.ServicioUsaUsuariosAnonimos) Then
                    ' strClienteDescripcion = beOrdenP.UsuarioNombre
                    beOrdenP.DescripcionCliente = beOrdenP.UsuarioNombre
                Else
                    If (ObjectToString(beOrdenP.DescripcionCliente) <> "") Then
                        'strClienteDescripcion = beOrdenP.DescripcionCliente
                        beOrdenP.DescripcionCliente = beOrdenP.DescripcionCliente
                    Else
                        'strClienteDescripcion = beOrdenP.UsuarioNombre
                        beOrdenP.DescripcionCliente = beOrdenP.UsuarioNombre
                    End If
                End If
                With beOrdenP
                    .oBECliente = obeOrdenPago.oBECliente
                    .oBEServicio = obeOrdenPago.oBEServicio
                End With
                'objEmail.EnviarCorreoGeneracionOrdenPago(beOrdenP.NumeroOrdenPago, beOrdenP.OcultarEmpresa, strClienteDescripcion, beOrdenP.DescripcionEmpresa, beOrdenP.DescripcionServicio, beOrdenP.SimboloMoneda, beOrdenP.Total.ToString("#,#0.00"), beOrdenP.ConceptoPago, emailto)
                'Se enviara si esta configurado el envio de correo
                If obeOrdenPago.oBEServicio.FlgEmailGenCIP Then
                    objEmail.EnviarCorreoGeneracionOrdenPago(beOrdenP, emailto)
                End If
                obeOrdenPago = beOrdenP
            End If

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result

    End Function


    '************************************************************************************************** 
    ' M�todo          : GenerarOrdenPago
    ' Descripci�n     : Registra la generacion de una C�digo de Identificaci�n de Pago por un cliente no registrado
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago cargada,Lista de entidades BEDetalleOrdenPago 
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function GenerarPreOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
        Return DAOrdenPago().GenerarPreOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
    End Function


    '************************************************************************************************** 
    ' M�todo          : ActualizarOrdenPago
    ' Descripci�n     : Actualiza el estado de el C�digo de Identificaci�n de Pago
    ' Autor           : Kleber Granados
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : Resultado de la transaccion
    ''**************************************************************************************************
    Public Function ActualizarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        '
        If obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Anulada Then
            'LA OPERACION SE ACTUALIZA CUANDO SE REGISTRA EL MOVIMIENTO
            Return 1
        Else
            Dim odaOrdenPago As New DAOrdenPago
            Return odaOrdenPago.ActualizarOrdenPago(obeOrdenPago)
        End If
        '
    End Function

    Public Function AnularOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal obeMovimiento As BEMovimiento) As Integer
        Try

            Using odaAgenciRecaudadora As New DAAgenciaRecaudadora

                Dim obeAgenteCaja As New BEAgenteCaja
                obeAgenteCaja.IdUsuarioCreacion = obeMovimiento.IdUsuarioCreacion
                obeAgenteCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenteCaja.Aperturado
                Dim listCajaAperturada As List(Of BEAgenteCaja) = odaAgenciRecaudadora.ConsultarAgenteCajaPorIdUsuarioIdEstado(obeAgenteCaja)

                If (listCajaAperturada.Count > 0) Then
                    Dim obeCajaAperturada As BEAgenteCaja = listCajaAperturada(0)
                    If (obeCajaAperturada.EstadoFechaValuta = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.EnUso) Then
                        obeMovimiento.FechaMovimiento = ObjectToDateTime(obeCajaAperturada.FechaApertura.ToShortDateString())
                    Else
                        Using oblConsultaComun As New BLComun()
                            obeMovimiento.FechaMovimiento = oblConsultaComun.ConsultarFechaHoraActual()
                        End Using
                    End If
                Else
                    Throw New Exception("obeAgenteCaja Aperturada no existe")
                End If

                'Utilizamos transaccion para realizar el extorno
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted

                obeMovimiento.IdTipoMovimiento = SPE.EmsambladoComun.ParametrosSistema.TipoMovimiento.AnularOP

                Using transaccionscope As New TransactionScope()
                    obeMovimiento.IdMovimiento = odaAgenciRecaudadora.RegistrarMovimiento(obeMovimiento)
                    obeMovimiento.NumeroOrdenPago = FormatearNumeroOrdenPago(obeMovimiento.IdOrdenPago)
                    obeOrdenPago.IdOrdenPago = obeMovimiento.IdOrdenPago
                    obeOrdenPago.NumeroOrdenPago = FormatearNumeroOrdenPago(obeMovimiento.IdOrdenPago)
                    obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Anulada
                    'obeOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
                    ActualizarOrdenPago(obeOrdenPago)

                    'SOLO PARA NOTIFICAR EL ESTADO DE LA C�digo de Identificaci�n de Pago
                    'Se registra la notificaci�n. UrlError
                    Using oblServicionNotificacion As New BLServicioNotificacion()
                        Dim obeOrdenPagon As BEOrdenPago = Nothing
                        Using odaOrdenPago As New DAOrdenPago()
                            obeOrdenPagon = odaOrdenPago.ObtenerOrdenPagoCompletoPorNroOrdenPago(obeOrdenPago.NumeroOrdenPago)
                        End Using

                        Dim obeServicio As BEServicio
                        Using odaServicio As New DAServicio()
                            obeServicio = CType(odaServicio.GetRecordByID(obeOrdenPagon.IdServicio), BEServicio)
                        End Using

                        If obeServicio.FlgNotificaMovimiento = True Then
                            oblServicionNotificacion.RegistrarServicioNotificacionUrlError(obeOrdenPagon, obeMovimiento.IdMovimiento, obeMovimiento.IdUsuarioCreacion, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.Web, obeServicio.IdGrupoNotificacion)
                        End If
                    End Using
                    transaccionscope.Complete()
                End Using

            End Using
            Return 1
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try

    End Function

    ''' <summary>
    ''' dj.- validacion de una Orden de Pago para su anulacion
    ''' en el mismo dia, en la misma agencia
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidarOrdenPagoParaAnulacion(ByVal obeMovimiento As BEMovimiento) As Integer

        'Dim intCodMensaje As Integer = 0

        Try
            Using obeOrdenPago As New BEOrdenPago

                With obeMovimiento
                    obeOrdenPago.NumeroOrdenPago = .NumeroOrdenPago 'CIP
                    obeOrdenPago.CodigoOrigenCancelacion = .CodigoAgenciaBancaria ' CODIGO AGENCIA

                    Return DAOrdenPago.ValidarOrdenPagoParaAnulacionDesdeBanco(obeOrdenPago, .NumeroOperacion)

                End With
            End Using

        Catch ex As Exception

            'Logger.Write(ex)
            Throw ex
        End Try

        'Return intCodMensaje

    End Function


    '************************************************************************************************** 
    ' M�todo          : ValidarOrdenPagoAgenciaBancaria
    ' Descripci�n     : Validar C�digo de Identificaci�n de Pago para el proceso de anulacion de operacion de pago
    ' Autor           : Christian Claros    
    ' Fecha/Hora      : 23/12/2008
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : string
    ''**************************************************************************************************

    Public Function ValidarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago, Optional ByRef detalle As String = "") As String

        Dim delimitador As New Char
        Dim validar() As String
        delimitador = "-"

        validar = DAOrdenPago().ValidarOrdenPagoAgenciaBancaria(obeOrdenPago).Split(delimitador)

        If validar(0) = "EX" Then 'Verifica OP exista
            If validar(1) = "EX" Then 'Verifica que OP este cancelada
                If validar(2) = "EX" Then 'Verifica que la Agencia Bancaria en la que se cancel� sea la misma en la que se extorna
                    If validar(3) = "EX" Then 'Verifica la fecha del extorno
                        If validar(4) = "EX" Then 'Verifica tiempo de extorno
                            If validar(5) = "EX" Then
                                Return "0"
                            Else
                                detalle = WSBancoMensaje.BCP.DetalleMensaje.CodigoServicioInvalida
                                Return WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
                            End If
                        Else
                            detalle = WSBancoMensaje.BCP.DetalleMensaje.TiempoExtornoInvalida
                            Return WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
                        End If
                    Else
                        detalle = WSBancoMensaje.BCP.DetalleMensaje.FechaExtornoInvalida
                        Return WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
                    End If
                Else
                    detalle = WSBancoMensaje.BCP.DetalleMensaje.AgenciaBancariaInvalida
                    Return WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
                End If
            Else
                detalle = WSBancoMensaje.BCP.DetalleMensaje.OrdenPagoNoCancelada
                Return WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
            End If
        Else
            detalle = WSBancoMensaje.BCP.DetalleMensaje.NoSeEncontroOrdenPago
            Return WSBancoMensaje.BCP.Mensaje.Cliente_no_presenta_deuda
        End If
        Return "0"
    End Function
#End Region


#Region "Generar CIP"
    Function FormatearNumeroOrdenPago(ByVal numeroOrden As Integer) As String
        Return Right("00000000000000" + numeroOrden.ToString, 14)
    End Function

    Public Function GenerarOrdenPagoXML(ByRef obeOrdenPago As BEOrdenPago) As Integer
        Return GenerarOrdenPagoXML(obeOrdenPago, obeOrdenPago.DetallesOrdenPago)
    End Function

    Public Function GenerarCIP(ByVal request As BEGenCIPRequest) As BEGenCIPResponse
        Dim response As New BEGenCIPResponse()
        Dim obeOPXServClienCons As BEOPXServClienCons = Nothing
        obeOPXServClienCons = ConsultarOrdenPagoPorOrdenIdComercio(request.OrdenPago.OrderIdComercio, request.OrdenPago.IdServicio)
        If (obeOPXServClienCons Is Nothing) Then
            Using odaComun As New DAComun()
                request.OrdenPago.FechaEmision = odaComun.ConsultarFechaHoraActual()
            End Using
            request.OrdenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
            Dim resultado As Integer = GenerarOrdenPagoXML(request.OrdenPago, request.OrdenPago.DetallesOrdenPago)
            If (resultado > 0) Then
                response.NumeroOrdenPago = FormatearNumeroOrdenPago(resultado)
                response.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGeneracionOk
                response.Message = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGeneracionOKMsq
                response.FechaEmision = request.OrdenPago.FechaEmision
                response.TiempoExpiracion = request.OrdenPago.TiempoExpiracion
                'response.State = "1"
                'response.Message = "El c�digo de identificaci�n de pago (CIP) se ha generado satisfactoriamente."
            End If
        Else
            If (request.OrdenPago.ServicioUsaUsuariosAnonimos) Then
                response.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnterior
                response.Message = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorMsq
                response.NumeroOrdenPago = obeOPXServClienCons.NumeroOrdenPago
                response.IdOrdenPago = obeOPXServClienCons.IdOrdenPago
            Else
                If (ObjectToInt(obeOPXServClienCons.IdUsuario) = ObjectToInt(request.OrdenPago.IdUsuarioCreacion)) Then
                    'response.State = "-1"
                    'response.Message = "El c�digo de identificaci�n de pago (CIP) ya se ha generado anteriormente."
                    response.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnterior
                    response.Message = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorMsq
                    response.NumeroOrdenPago = obeOPXServClienCons.NumeroOrdenPago
                    response.IdOrdenPago = obeOPXServClienCons.IdOrdenPago
                Else
                    response.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorOtroUsuario
                    'response.State = "-2"
                    'response.Message = "El c�digo de identificaci�n de pago (CIP) para esta operaci�n ya se ha generado anteriormente por otro usuario."
                    response.Message = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorOtroUsuarioMsq
                End If
            End If
        End If
        Return response
    End Function

    Public Function ObtenerClienteParaWS(ByRef obeOrdenPago As BEOrdenPago, ByVal obeServicio As BEServicio) As BECliente
        Dim obeCliente As BECliente = Nothing

        If (obeServicio.UsaUsuariosAnonimos) Then
            'obtiene el cliente usuario anonimo
            Using odaCliente As New DACliente()
                obeCliente = odaCliente.ObtenerUsuarioAnonimo()
                obeOrdenPago.IdCliente = obeCliente.IdCliente
                obeOrdenPago.IdUsuarioCreacion = obeCliente.IdUsuario
            End Using
        Else
            Using oblCliente As New BLCliente()
                ' Consulta si existe un cliente con por email xxx
                obeCliente = oblCliente.ConsultarClientePorEmail(obeOrdenPago.UsuarioEmail)
                If (obeCliente Is Nothing) Then
                    'como no existe cliente, se procede a registrarlo como registro basico ws
                    obeCliente = New BECliente()
                    obeCliente.Email = obeOrdenPago.UsuarioEmail
                    obeCliente.AliasCliente = obeOrdenPago.UsuarioAlias
                    obeCliente.Nombres = obeOrdenPago.UsuarioNombre
                    obeCliente.Apellidos = obeOrdenPago.UsuarioApellidos
                    obeCliente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroBasicoWS
                    obeCliente.FechaNacimiento = DateTime.MinValue

                    'Se agrega un correo de copia si esta configurado en el servicio
                    Dim EmailComercio As String = IIf(obeServicio.FlgEmailCCAdmin, obeOrdenPago.MailComercio, String.Empty)

                    If obeServicio.CodigoServicioVersion = ParametrosSistema.Servicio.VersionIntegracion.Version2 _
                        Or Not obeServicio.FlgEmailGenUsuario Then
                        obeCliente.IdUsuario = oblCliente.CommonInsertRecord(obeCliente, False, EmailComercio)
                    Else
                        obeCliente.IdUsuario = oblCliente.CommonInsertRecord(obeCliente, True, EmailComercio)
                    End If

                    Dim tmpCodigoRegistro As String = obeCliente.CodigoRegistro
                    Dim tmpPassword As String = obeCliente.Password
                    obeCliente = oblCliente.ConsultarClientePorEmail(obeOrdenPago.UsuarioEmail)
                    With obeCliente
                        .RecienRegistrado = True
                        .CodigoRegistro = tmpCodigoRegistro
                        .Password = SeguridadComun.UnEncryptarCodigo(tmpPassword)
                    End With
                Else
                    'existe usuario pero no como cliente, por esto se requiere registrarlo como cliente.
                    If (ObjectToInt32(obeCliente.IdCliente) = 0) Then
                        obeCliente = oblCliente.RegistrarClienteDesdeUsuarioExistente(obeCliente)
                    End If
                End If
            End Using
        End If
        'retorna el cliente obtenido, creado o actualizado.
        Return obeCliente
    End Function

    Public Function WSGenerarCIP(ByVal request As BEWSGenCIPRequest) As BEWSGenCIPResponse
        Dim response As New BEWSGenCIPResponse()
        response.InformacionCIP = ""
        response.CIP = ""
        Try
            Dim fechaInicio As DateTimeOffset = DateTimeOffset.Now
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                'validar la Api y Clave del Servicio
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CAPI)
                If (obeServicio Is Nothing) Then
                    response.Estado = "0"
                    response.Mensaje = "La API no est� asignada a ningun servicio."
                    Return response
                ElseIf (obeServicio.ClaveSecreta <> request.CClave) Then
                    response.Estado = "0"
                    response.Mensaje = "La clave del servicio es incorrecto."
                    Return response
                End If
            End Using
            Using traductor As New BLTraductorContratoServ(request.CAPI, request.Xml, True, SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService)
                'validar el formato del xml
                If Not (traductor.ValidarFormatoXML()) Then
                    response.Estado = "0"
                    response.Mensaje = "El formato del xml es incorrecto."
                End If
                ''cargar los datos del ContratoXML para luego capturar los datos del XML
                Dim obeOrdenPago As BEOrdenPago = traductor.DesEnsamblarTramaEnBEOrdenPago()
                obeOrdenPago.IdServicio = obeServicio.IdServicio
                obeOrdenPago.IdTipoNotificacion = obeServicio.IdTipoNotificacion
                obeOrdenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
                obeOrdenPago.TramaSolicitud = traductor.ObtenerXMLContenidoDesencriptado()
                obeOrdenPago.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService
                obeOrdenPago.ServicioUsaUsuariosAnonimos = obeServicio.UsaUsuariosAnonimos

                Using odaAdmComun As New DAComun()
                    obeOrdenPago.FechaEmision = odaAdmComun.ConsultarFechaHoraActual()
                End Using

                If (obeOrdenPago.UsuarioEmail <> request.Email) Then
                    response.Estado = "0"
                    response.Mensaje = "No se pudo generar el C�digo de Identificaci�n de Pago. Los emails enviados no pueden ser diferentes."
                    Return response
                End If

                'Tareas #319
                If (obeOrdenPago.FechaAExpirar = Date.MinValue) Then
                    obeOrdenPago.TiempoExpiracion = obeServicio.TiempoExpiracion
                Else
                    Dim totalSegundos As Integer = obeOrdenPago.FechaAExpirar.Subtract(obeOrdenPago.FechaEmision).TotalSeconds
                    'calcular en horas
                    obeOrdenPago.TiempoExpiracion = ((totalSegundos / 60) / 60)
                End If

                If obeOrdenPago.TiempoExpiracion <= 0 Then
                    response.Estado = "0"
                    response.Mensaje = "No se pudo generar el C�digo de Identificaci�n de Pago. La fecha a expirar no puede ser menor a la actual."
                    Return response
                End If

                'Cambio 3r
                If obeOrdenPago.Total < 0 Then
                    response.Estado = "0"
                    response.Mensaje = "No se pudo generar el C�digo de Identificaci�n de Pago. El monto es inv�lido."
                    Return response
                End If

                Using odaComun As New DAComun()
                    Dim obeMoneda As BEMoneda = odaComun.ConsultarMonedaPorId(obeOrdenPago.IdMoneda)
                    If (obeMoneda Is Nothing) Then
                        response.Estado = "0"
                        response.Mensaje = "No se pudo generar el C�digo de Identificaci�n de Pago. El c�digo de moneda recibido no es el correcto."
                        Return response
                    End If
                End Using

                'El sistema valida si el email asociado a la solicitud est� registrado
                Dim obeCliente As BECliente = ObtenerClienteParaWS(obeOrdenPago, obeServicio)
                If (obeCliente Is Nothing) Then
                    response.Estado = "0"
                    response.Mensaje = "No se pudo Generar el C�digo de Identificaci�n de Pago. Error al obtener el cliente."
                Else
                    Dim obeOPXServClienCons As BEOPXServClienCons = ConsultarOrdenPagoPorOrdenIdComercio(obeOrdenPago.OrderIdComercio, obeOrdenPago.IdServicio)
                    If obeOPXServClienCons Is Nothing Then
                        'generar el pago y enviar por email
                        'obeOrdenPago.EmailANotifGeneracion = obeOrdenPago.UsuarioEmail
                        obeOrdenPago.EmailANotifGeneracion = request.Email
                        'obeOrdenPago.TiempoExpiracion = obeServicio.TiempoExpiracion
                        obeOrdenPago.IdUsuarioCreacion = obeCliente.IdUsuario
                        obeOrdenPago.oBECliente = obeCliente
                        obeOrdenPago.oBEServicio = obeServicio
                        Dim i As Integer
                        Dim SumTot As Decimal = 0
                        Dim ListaDetalleOrdePago As List(Of BEDetalleOrdenPago) = obeOrdenPago.DetallesOrdenPago
                        For i = 0 To ListaDetalleOrdePago.Count - 1
                            SumTot = ListaDetalleOrdePago(i).Importe + SumTot
                        Next

                        If obeOrdenPago.Total = SumTot Then
                            Dim idCIP As Int64 = GenerarOrdenPagoXML(obeOrdenPago)
                            If (idCIP > 0) Then
                                'construir el BEGenCIPResponse y devolver la respuesta
                                response.Estado = "1"
                                response.Mensaje = String.Format("Se ha Generado el C�digo de Identificaci�n de Pago  {0} .", obeOrdenPago.NumeroOrdenPago)
                                response.CIP = obeOrdenPago.NumeroOrdenPago
                                Using oblContratoRespuesta As New BLContratoRespuesta(obeOrdenPago.TiempoExpiracion, obeOrdenPago.FechaEmision)
                                    response.InformacionCIP = SPE.Negocio.BLTraductorContratoServ.EncriptarStr(oblContratoRespuesta.ObtenerXMLString())
                                End Using

                                Dim trackCip As New BETrackCip()
                                trackCip.IdOrdenPago = obeOrdenPago.IdOrdenPago
                                If (obeOrdenPago.IdMoneda = "1") Then
                                    trackCip.Moneda = "PEN"
                                Else
                                    trackCip.Moneda = "USD"
                                End If
                                trackCip.Total = obeOrdenPago.Total.ToString("F")
                                trackCip.CodTransaccion = obeOrdenPago.OrderIdComercio
                                trackCip.FechaCreacion = obeOrdenPago.FechaCreacion.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz")
                                trackCip.TiempoExpiracion = Convert.ToDouble(obeOrdenPago.TiempoExpiracion)
                                trackCip.IdServicio = obeOrdenPago.IdServicio
                                trackCip.CodigoServicio = obeOrdenPago.oBEServicio.Codigo
                                trackCip.NombreServicio = obeOrdenPago.oBEServicio.Nombre
                                trackCip.IdEmpresa = obeOrdenPago.oBEServicio.IdEmpresaContratante
                                trackCip.EmailUsuario = obeOrdenPago.UsuarioEmail
                                trackCip.FechaInicio = fechaInicio.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz")
                                Dim fechaFin As DateTimeOffset = DateTimeOffset.Now
                                trackCip.FechaFin = fechaFin.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz")
                                trackCip.TiempoGeneracion = (fechaFin - fechaInicio).TotalMilliseconds
                                trackCip.Origen = 1
                                Dim stream As New MemoryStream()
                                Dim ser As New DataContractJsonSerializer(trackCip.GetType())
                                ser.WriteObject(stream, trackCip)
                                Dim textoJson As String = System.Text.Encoding.UTF8.GetString(stream.ToArray)
                                _nlogger.Trace(textoJson)
                            Else
                                response.Estado = "0"
                                response.Mensaje = "No se ha podido generar el C�digo de Identificaci�n de Pago"
                            End If
                        Else
                            response.Estado = "0"
                            response.Mensaje = "No se pudo Generar el C�digo de Identificaci�n de Pago. El monto total del CIP no concuerda con la suma del importe de los detalles"
                        End If

                    Else
                        response.Estado = "0"
                        response.CIP = obeOPXServClienCons.NumeroOrdenPago
                        response.Mensaje = String.Format("El C�digo de Identificaci�n de Pago (CIP) ya se ha generado anteriormente para esta operaci�n. CIP: {0} ", obeOPXServClienCons.NumeroOrdenPago)
                        Using oblContratoRespuesta As New BLContratoRespuesta(obeOPXServClienCons.TiempoExpiracion, obeOPXServClienCons.FechaEmision)
                            response.InformacionCIP = SPE.Negocio.BLTraductorContratoServ.EncriptarStr(oblContratoRespuesta.ObtenerXMLString())
                        End Using

                    End If
                End If
            End Using
            Return response
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = "0"
            If response.Mensaje = "" Then
                response.Mensaje = String.Format("Ha ocurrido un error al realizar la validaci�n del contrato.")
            End If
        End Try
        Return response
    End Function
#End Region

    Public Function WSEliminarCIP(ByVal request As BEWSElimCIPRequest) As BEWSElimCIPResponse
        Dim response As New BEWSElimCIPResponse()
        Try
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                'validar la Api y Clave del Servicio
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CAPI)
                If (obeServicio Is Nothing) Then
                    response.Estado = "0"
                    response.Mensaje = "La API no est� asignada a ningun servicio."
                    Return response
                ElseIf (obeServicio.ClaveSecreta <> request.CClave) Then
                    response.Estado = "0"
                    response.Mensaje = "La clave del servicio es incorrecto."
                    Return response
                End If
            End Using
            Dim obeOrdenPago As New BEOrdenPago()
            obeOrdenPago.IdOrdenPago = ObjectToInt64(request.CIP)
            obeOrdenPago.IdOrigenEliminacion = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.OrigenEliminacion.EliminadoDeWS
            Dim resultado As Integer = DAOrdenPago.EliminarOrdenPago(obeOrdenPago)
            If (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Generada)) Then
                response.Estado = "1"
                response.Mensaje = String.Format("El CIP {0} se ha eliminado correctamente.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Cancelada)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("El CIP {0} no se puede eliminar porque ya ha sido cancelado.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("El CIP {0} no se puede eliminar porque ya ha expirado.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Eliminado)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("El CIP {0} ya fue eliminado anteriormente.", request.CIP)
            Else
                response.Estado = "-1"
                response.Mensaje = String.Format("Ha ocurrido un Error no Manejado: El estado del CIP no se ha identificado")
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = "-1"
            response.Mensaje = String.Format("Ha ocurrido un Error no Manejado:{0}", ex.Message)
        End Try
        Return response
    End Function

    Public Function WSActualizarCIP(ByVal request As BEWSActualizaCIPRequest) As BEWSActualizaCIPResponse
        Dim response As New BEWSActualizaCIPResponse()
        Try
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                'validar la Api y Clave del Servicio
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CAPI)
                If (obeServicio Is Nothing) Then
                    response.Estado = "0"
                    response.Mensaje = "La API no est� asignada a ningun servicio."
                    Return response
                ElseIf (obeServicio.ClaveSecreta <> request.CClave) Then
                    response.Estado = "0"
                    response.Mensaje = "La clave del servicio es incorrecto."
                    Return response
                End If
            End Using
            Dim obeOrdenPago As New BEOrdenPago()
            obeOrdenPago.IdOrdenPago = ObjectToInt64(request.CIP)
            obeOrdenPago.FechaAExpirar = request.FechaExpira
            obeOrdenPago.ClaveAPI = request.CAPI
            Dim resultado As Integer = DAOrdenPago.ActualizarFechaExpiraCIP(obeOrdenPago)

            If (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Generada)) Then
                response.Estado = "1"
                response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} se ha modificada correctamente.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Cancelada)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} no se puede modificar porque ya ha sido cancelado.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} no se puede modificar porque ya ha expirado.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Eliminado)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} no se puede modificar porque ya fue eliminado anteriormente.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.ValidaFechaExpiraOrdenPago.Inv�lido)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("La nueva fecha de expiraci�n del CIP {0} debe ser mayor a la fecha actual.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.ValidaFechaExpiraOrdenPago.Excedido)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("La nueva fecha de expiraci�n del CIP {0} no debe excederse a los 10 a�os.", request.CIP)
            ElseIf (resultado = ObjectToInt(ParametrosSistema.ValidaFechaExpiraOrdenPago.claveAPI)) Then
                response.Estado = "0"
                response.Mensaje = String.Format("CIP no existe.", request.CIP)

            Else
                response.Estado = "-1"
                response.Mensaje = String.Format("Ha ocurrido un Error no Manejado: El estado del CIP no se ha identificado")
            End If

        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = "-1"
            response.Mensaje = String.Format("Ha ocurrido un Error no Manejado:{0}", ex.Message)
        End Try
        Return response
    End Function

    Private IdOrdenPago As String
    Private Detalles As List(Of BEDetalleOrdenPago)
    Private DetallesMod1 As List(Of BEDetalleOrdenPagoMod1)

    Public Function WSConsultarCIP(ByVal request As BEWSConsultarCIPRequest) As BEWSConsultarCIPResponse
        Dim response As New BEWSConsultarCIPResponse()
        Try
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CAPI)
                If (obeServicio Is Nothing) Then
                    With response
                        .Estado = "0"
                        .Mensaje = "La API no est� asignada a ningun servicio."
                    End With
                    Return response
                ElseIf (obeServicio.ClaveSecreta <> request.CClave) Then
                    With response
                        .Estado = "0"
                        .Mensaje = "La clave del servicio es incorrecto."
                    End With
                    Return response
                End If
            End Using
            Dim RegexCIP As New Regex("^\s*\d{1,14}\s*$")
            Dim arrayString As String() = request.CIPS.Split(",")
            For Each CIP As String In arrayString
                If Not RegexCIP.IsMatch(CIP) Then
                    With response
                        .Estado = "0"
                        .Mensaje = "Los CIPs no tienen un formato correcto."
                    End With
                    Return response
                End If
            Next
            Dim CIPs As String = String.Join(",", arrayString)
            Dim resultado As List(Of BEWSConsCIP) = DAOrdenPago.WSConsultarCIP(CIPs, obeServicio.IdServicio)
            If resultado.Count = 0 Then
                With response
                    .Estado = "-1"
                    .Mensaje = String.Format("No se encontraron CIPs asociados")
                End With
            Else
                Detalles = DAOrdenPago.WSConsultarCIPDetalle(CIPs, obeServicio.IdServicio)
                resultado.ForEach(AddressOf RecorrerCabecera)
                With response
                    .Estado = "1"
                    .CIPs = resultado
                    .Mensaje = "La consulta de los CIPs se realiz� con �xito"
                    .XML = DataUtil.ObjectToXML(.CIPs)
                End With
            End If
            If String.IsNullOrEmpty(response.InfoResponse) Then
                response.InfoResponse = ""
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            With response
                .Estado = "-1"
                .Mensaje = String.Format("Ha ocurrido un error no controlado: {0}", ex.Message)
            End With
        End Try
        Return response
    End Function

    Public Function WSConsultarCIPv2(ByVal request As BEWSConsultarCIPv2Request) As BEWSConsultarCIPv2Response
        Dim response As New BEWSConsultarCIPv2Response()
        Try
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CAPI)
                If (obeServicio Is Nothing) Then
                    With response
                        .Estado = "0"
                        .Mensaje = "La API no est� asignada a ningun servicio."
                    End With
                    Return response
                ElseIf (obeServicio.ClaveSecreta <> request.CClave) Then
                    With response
                        .Estado = "0"
                        .Mensaje = "La clave del servicio es incorrecto."
                    End With
                    Return response
                End If
            End Using
            Dim RegexCIP As New Regex("^\s*\d{1,14}\s*$")
            Dim arrayString As String() = request.CIPS.Split(",")
            For Each CIP As String In arrayString
                If Not RegexCIP.IsMatch(CIP) Then
                    With response
                        .Estado = "0"
                        .Mensaje = "Los CIPs no tienen un formato correcto."
                    End With
                    Return response
                End If
            Next
            Dim CIPs As String = String.Join(",", arrayString)
            Dim resultado As List(Of BEWSConsCIPv2) = DAOrdenPago.WSConsultarCIPv2(CIPs, obeServicio.IdServicio)
            If resultado.Count = 0 Then
                With response
                    .Estado = "-1"
                    .Mensaje = String.Format("No se encontraron CIPs asociados")
                End With
            Else
                Detalles = DAOrdenPago.WSConsultarCIPDetalle(CIPs, obeServicio.IdServicio)
                resultado.ForEach(AddressOf RecorrerCabecera)
                With response
                    .Estado = "1"
                    .CIPs = resultado
                    .Mensaje = "La consulta de los CIPs se realiz� con �xito"
                    .XML = DataUtil.ObjectToXML(.CIPs)
                End With
            End If
            If String.IsNullOrEmpty(response.InfoResponse) Then
                response.InfoResponse = ""
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            With response
                .Estado = "-1"
                .Mensaje = String.Format("Ha ocurrido un error no controlado: {0}", ex.Message)
            End With
        End Try
        Return response
    End Function

    Public Function WSConsultarCIPConciliados(ByVal request As BEWSConsCIPsConciliadosRequest) As BEWSConsCIPsConciliadosResponse
        Dim response As New BEWSConsCIPsConciliadosResponse()
        Try
            Dim oBEServicio As BEServicio = Nothing
            Using oDAServicio As New DAServicio()
                oBEServicio = oDAServicio.ConsultarServicioPorAPI(request.CAPI)
                If (oBEServicio Is Nothing) Then
                    response.Estado = "0"
                    response.Mensaje = "La API no est� asignada a ning�n servicio."
                    Return response
                ElseIf (oBEServicio.ClaveSecreta <> request.CClave) Then
                    response.Estado = "0"
                    response.Mensaje = "La clave del servicio es incorrecta."
                    Return response
                End If
            End Using
            Try
                Dim sFechaDesde As String = New Regex("\d{4}-\d{2}-\d{2}").Match(request.FechaDesde).ToString()
                Dim sFechaHasta As String = New Regex("\d{4}-\d{2}-\d{2}").Match(request.FechaHasta).ToString()
                Dim FechaDesde As New DateTime(sFechaDesde.Substring(0, 4), sFechaDesde.Substring(5, 2), sFechaDesde.Substring(8, 2))
                Dim FechaHasta As New DateTime(sFechaHasta.Substring(0, 4), sFechaHasta.Substring(5, 2), sFechaHasta.Substring(8, 2))
                Dim resultado As List(Of BEWSConsCIPConciliado) = DAOrdenPago.WSConsultarCIPConciliados(FechaDesde, FechaHasta, _
                    request.Tipo, oBEServicio.IdServicio)
                If resultado.Count = 0 Then
                    response.Estado = "-1"
                    response.Mensaje = "No se encontraron CIPs para este servicio."
                Else
                    Detalles = DAOrdenPago.WSConsultarCIPDetalle(request, oBEServicio.IdServicio)
                    resultado.ForEach(AddressOf RecorrerCabecera)
                    With response
                        .Estado = "1"
                        .CIPs = resultado
                        .Mensaje = "La consulta de los CIPs se realiz� con �xito"
                    End With
                End If
            Catch ex As ArgumentOutOfRangeException
                With response
                    .Estado = "-1"
                    .Mensaje = "Las fechas no tienen el formato correcto."
                End With
                Return response
            End Try
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = "-1"
            response.Mensaje = String.Format("Ha ocurrido un error no controlado: {0}", ex.Message)
        End Try
        Return response
    End Function

    Private Sub RecorrerCabeceraMod1(ByVal CIP As BEWSConsCIPMod1)
        IdOrdenPago = CIP.IdOrdenPago
        CIP.Detalle = AsignarDetalleMod1()
    End Sub
    Private Sub RecorrerCabecera(ByVal CIP As BEWSConsCIPConciliado)
        IdOrdenPago = CIP.IdOrdenPago
        CIP.Detalle = AsignarDetalle()
    End Sub
    Private Sub RecorrerCabecera(ByVal CIP As BEWSConsCIP)
        IdOrdenPago = CIP.IdOrdenPago
        CIP.Detalle = AsignarDetalle()
    End Sub
    Private Sub RecorrerCabecera(ByVal CIP As BEWSConsCIPv2)
        IdOrdenPago = CIP.IdOrdenPago
        CIP.Detalle = AsignarDetalle()
    End Sub

    Private Function AsignarDetalle() As List(Of BEWSConsCIPDetalle)
        Dim Resultado As New List(Of BEWSConsCIPDetalle)
        Dim DetallesCIP As List(Of BEDetalleOrdenPago) = Detalles.FindAll(AddressOf BuscarDetalles)
        For Each ItemDetallesCIP As BEDetalleOrdenPago In DetallesCIP
            Dim oBEWSConsCIPDetalle As New BEWSConsCIPDetalle
            With oBEWSConsCIPDetalle
                .IdDetalleOrdenPago = ItemDetallesCIP.IdDetalleOrdenPago
                .ConceptoPago = ItemDetallesCIP.ConceptoPago
                .Cod_Origen = ItemDetallesCIP.codOrigen
                .Tipo_Origen = ItemDetallesCIP.tipoOrigen
                .Importe = ItemDetallesCIP.Importe
                .Campo1 = ItemDetallesCIP.campo1
                .Campo2 = ItemDetallesCIP.campo2
                .Campo3 = ItemDetallesCIP.campo3
            End With
            Resultado.Add(oBEWSConsCIPDetalle)
        Next
        Return Resultado
    End Function

    Private Function AsignarDetalleMod1() As List(Of BEWSConsCIPDetalleMod1)
        Dim Resultado As New List(Of BEWSConsCIPDetalleMod1)
        Dim DetallesCIP As List(Of BEDetalleOrdenPagoMod1) = DetallesMod1.FindAll(AddressOf BuscarDetallesMod1)
        For Each ItemDetallesCIP As BEDetalleOrdenPagoMod1 In DetallesCIP
            Dim oBEWSConsCIPDetalle As New BEWSConsCIPDetalleMod1
            With oBEWSConsCIPDetalle
                .IdDetalleOrdenPago = ItemDetallesCIP.IdDetalleOrdenPago
                .ConceptoPago = ItemDetallesCIP.ConceptoPago
                .Cod_Origen = ItemDetallesCIP.codOrigen
                .Tipo_Origen = ItemDetallesCIP.tipoOrigen
                .Importe = ItemDetallesCIP.Importe
                .Campo1 = ItemDetallesCIP.campo1
                .Campo2 = ItemDetallesCIP.campo2
                .Campo3 = ItemDetallesCIP.campo3
            End With
            Resultado.Add(oBEWSConsCIPDetalle)
        Next
        Return Resultado
    End Function

    Private Function BuscarDetalles(ByVal DetalleCIP As BEDetalleOrdenPago) As Boolean
        Return (DetalleCIP.IdOrdenPago = IdOrdenPago)
    End Function
    Private Function BuscarDetallesMod1(ByVal DetalleCIP As BEDetalleOrdenPagoMod1) As Boolean
        Return (DetalleCIP.IdOrdenPago = IdOrdenPago)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarOPsByServicio
    ' Descripci�n     : Consulta que lista Ordenes de Pago por Servicio
    ' Autor           : Walter Rojas
    ' Fecha/Hora      : 19/11/2010
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : Lista de entidades BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOPsByServicio(ByVal request As BEPagoServicio) As List(Of BEPagoServicio)
        Dim listresult As List(Of BEPagoServicio) = DAOrdenPago().ConsultarOPsByServicio(request)
        Return listresult
    End Function
    '************************************************************************************************** 
    ' M�todo          : ConsultarOPsByServicio
    ' Descripci�n     : Consulta que lista Ordenes de Pago por CIPs
    ' Autor           : Walter Rojas
    ' Fecha/Hora      : 19/11/2010
    ' Parametros_In   : Instancia de la entidad BEOrdenPago
    ' Parametros_Out  : Lista de entidades BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarOPsByCIPs(ByVal request As BEPagoServicio) As List(Of BEPagoServicio)
        Dim listresult As List(Of BEPagoServicio) = DAOrdenPago().ConsultarOPsByCIPs(request)
        Return listresult
    End Function
    '************************************************************************************************** 
    ' M�todo          : ObtenerOrdenPagCabeceraPorNumeroOrden
    ' Descripci�n     : Consulta que Obtine un CIP por numero
    ' Autor           : PERUCOM - MDIAZ
    ' Fecha/Hora      : 21/10/2011
    ' Parametros_In   : numeroOrden del CIP
    ' Parametros_Out  : entidad BEOrdenPago
    ''**************************************************************************************************
    Public Function ObtenerOrdenPagCabeceraPorNumeroOrden(ByVal numeroOrden As String) As BEOrdenPago
        Dim oOrdenPago As New SPE.AccesoDatos.DAOrdenPago
        Return oOrdenPago.ObtenerOrdenPagCabeceraPorNumeroOrden(numeroOrden)
    End Function
    '************************************************************************************************** 
    ' M�todo          : ConsultarDetalleOrdenPagoPorIdOrdenPago
    ' Descripci�n     : Consulta que Obtine detalle de un CIP por id
    ' Autor           : PERUCOM - MDIAZ
    ' Fecha/Hora      : 21/10/2011
    ' Parametros_In   : ID del CIP
    ' Parametros_Out  : lista de entidades de BEDetalleOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarDetalleOrdenPagoPorIdOrdenPago(ByVal idOrdenPago As Integer) As List(Of BEDetalleOrdenPago)
        Dim oOrdenPago As New SPE.AccesoDatos.DAOrdenPago
        Return oOrdenPago.ConsultarDetalleOrdenPagoPorIdOrdenPago(idOrdenPago)
    End Function
    '************************************************************************************************** 
    ' M�todo          : ConsultarHistoricoMovimientosporIdOrden
    ' Descripci�n     : Consulta que Obtine los movimientos de un CIP por id
    ' Autor           : PERUCOM - MDIAZ
    ' Fecha/Hora      : 21/10/2011
    ' Parametros_In   : ID del CIP
    ' Parametros_Out  : lista de entidades de BEMovimiento
    Public Function ConsultarHistoricoMovimientosporIdOrden(ByVal idOrdenPago As Integer) As List(Of BEMovimiento)
        Dim oOrdenPago As New SPE.AccesoDatos.DAOrdenPago
        Return oOrdenPago.ConsultarHistoricoMovimientosporIdOrden(idOrdenPago)
    End Function

    'Mejora de busqueda por cliente nombre + apellido
    '2013-01-14
    Public Overrides Function SearchByParametersOrderedBy(be As _3Dev.FW.Entidades.BusinessEntityBase, orderedBy As String, isAccending As Boolean) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        'No es necesario porque la p�gina valida que no ingrese tilde
        'Dim bePagoServicio As BEOrdenPago = CType(be, BEOrdenPago)
        'bePagoServicio.UsuarioNombre = DataUtil.RemoveDiacritics(bePagoServicio.UsuarioNombre)
        Dim daOrdenPago As New DAOrdenPago
        Return daOrdenPago.SearchByParameters(be)
    End Function

#Region "Consulta Pagos Web "

    Public Function ObtenerCIPInfoByUrLServicio(ByVal request As BEObtCIPInfoByUrLServRequest) As BEObtCIPInfoByUrLServResponse
        Try
            Dim xmlTramaDesEncriptado As String = ""
            Dim obeResponse As New BEObtCIPInfoByUrLServResponse()
            '
            Using oblServicio As New BLServicio()
                obeResponse.Servicio = oblServicio.ConsultarServicioPorUrl(request.UrlServicio)
            End Using
            If (obeResponse.Servicio IsNot Nothing) Then
                If (obeResponse.Servicio.IdServicio > 0) Then
                    Using oblEmpresaContratante As New BLEmpresaContratante()
                        obeResponse.OcultarEmpresa = oblEmpresaContratante.OcultarEmpresa(obeResponse.Servicio.IdServicio)
                    End Using

                    Using traductor As New BLTraductorContratoServ(obeResponse.Servicio.ClaveAPI, request.Trama, True, request.TipoTrama)

                        ' validar el formato del xml
                        If Not (traductor.ValidarFormatoXML()) Then
                            obeResponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.FormatoContratoXMLIncorrecto
                            obeResponse.Message = "El formato del contrato xml es incorrecto."
                            Return obeResponse
                        End If
                        ''cargar los datos del ContratoXML para luego capturar los datos del XML
                        If (request.CargarTramaSolicitud) Then
                            If (request.TipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Request) Then
                                Dim strArray As String() = request.Trama.Split("?")
                                If (strArray.Length > 1) Then
                                    xmlTramaDesEncriptado = strArray(1)
                                End If
                            Else
                                xmlTramaDesEncriptado = traductor.ObtenerXMLContenidoDesencriptado()
                            End If

                        End If
                        obeResponse.EsquemaTrama = traductor.ObtenerEsquemaTrama()
                        obeResponse.OrdenPago = traductor.DesEnsamblarTramaEnBEOrdenPago()
                        obeResponse.OrdenPago.TiempoExpiracion = obeResponse.Servicio.TiempoExpiracion
                        obeResponse.OrdenPago.ServicioUsaUsuariosAnonimos = obeResponse.Servicio.UsaUsuariosAnonimos
                    End Using
                Else
                    obeResponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.ServicioNoExiste
                    obeResponse.Message = "El servicio no existe."
                    Return obeResponse
                End If
            Else
                obeResponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.ServicioNoExiste
                obeResponse.Message = "El servicio no existe."
                Return obeResponse
            End If
            If (obeResponse.OrdenPago IsNot Nothing) Then
                Using odaComun As New DAComun()
                    obeResponse.Moneda = odaComun.ConsultarMonedaPorId(obeResponse.OrdenPago.IdMoneda)
                    If (obeResponse.Moneda Is Nothing) Then
                        obeResponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.CodigoMonedaIncorrecto
                        obeResponse.Message = "El c�digo de moneda recibido no es el correcto."
                        Return obeResponse
                    End If
                End Using
            End If
            If (obeResponse.OrdenPago.Total < 0) Then
                obeResponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.MontoTotalOPMenorQueCero
                obeResponse.Message = "El monto no puede ser menor o igual a cero. Intente nuevamente."
            End If

            obeResponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.SolicitudSatisfactoria
            If (request.CargarTramaSolicitud) Then
                obeResponse.OrdenPago.TramaSolicitud = xmlTramaDesEncriptado
                obeResponse.OrdenPago.IdTramaTipo = request.TipoTrama
            End If
            Return obeResponse
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


#End Region

#Region "Integracion Servicios v2.0"

    '<add Peru.Com FaseIII>'
    Public Function WSSolicitarPago(ByVal request As BEWSSolicitarRequest) As BEWSSolicitarResponse
        Dim response As New BEWSSolicitarResponse()
        response.Estado = MensajeSolicitud.EstadoNoValido
        Try
            Dim fechaInicio As DateTimeOffset = DateTimeOffset.Now
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.cServ)

                If (obeServicio Is Nothing) Then
                    obeServicio = odaServicio.ConsultarServicioPorCodigo(request.cServ)
                End If
                If (obeServicio Is Nothing) Then
                    response.Mensaje = MensajeSolicitud.ErrorCodigoServicio
                    Return response
                End If
            End Using
            Using traductor As New BLTraductorContratoServV3(obeServicio.ClaveAPI, request.Xml, True, SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService)
                If Not (traductor.ValidarFormatoXML()) Then
                    response.Mensaje = MensajeSolicitud.ErrorFormatoXML
                    Return response
                End If
                Dim XMLDescifrado = traductor.ObtenerXMLContenidoDescifrado()
                If Not (traductor.ValidarFirma(XMLDescifrado, request.CClave, obeServicio.IdServicio)) Then
                    response.Mensaje = MensajeSolicitud.ErrorCCLaveFirma
                    Return response
                End If
                Dim obeSolicituPago As BESolicitudPago = traductor.ObtenerBESolicitudPagoFromXML()
                obeSolicituPago.IdServicio = obeServicio.IdServicio
                obeSolicituPago.TramaSolicitud = XMLDescifrado
                obeSolicituPago.ServicioUsaUsuariosAnonimos = obeServicio.UsaUsuariosAnonimos

                Dim totalSegundos As Integer = 300
                obeSolicituPago.TiempoExpiracion = ((totalSegundos / 60))

                If (obeSolicituPago.FechaExpiracion = Date.MinValue) Then
                    obeSolicituPago.FechaExpiracion = DateTime.Now.AddHours(obeServicio.TiempoExpiracion)
                End If
                Using odaComun As New DAComun()
                    Dim obeMoneda As BEMoneda = odaComun.ConsultarMonedaPorId(obeSolicituPago.IdMoneda)
                    If (obeMoneda Is Nothing) Then
                        response.Mensaje = MensajeSolicitud.ErrorCodigoMoneda
                        Return response
                    End If
                End Using

                Dim obeCliente As BECliente = ObtenerClienteParaWS(obeSolicituPago, obeServicio)
                If (obeCliente Is Nothing) Then
                    response.Mensaje = MensajeSolicitud.ErrorGeneracionCliente
                Else
                    If Not ConsultarSolicitudPagoPorOrdenIdComercio(obeSolicituPago.CodTransaccion, obeSolicituPago.IdServicio) Then
                        'obeSolicituPago.EmailANotifGeneracion = obeSolicituPago.UsuarioEmail
                        obeSolicituPago.IdUsuarioCreacion = obeCliente.IdUsuario
                        obeSolicituPago.IdCliente = obeCliente.IdCliente
                        obeSolicituPago.Idestado = SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pendiente

                        Dim i As Integer
                        Dim SumTot As Decimal = 0
                        Dim ListaDetalleSolicitudPago As List(Of BEDetalleSolicitudPago) = obeSolicituPago.DetallesSolicitudPago
                        For i = 0 To ListaDetalleSolicitudPago.Count - 1
                            SumTot = ListaDetalleSolicitudPago(i).Importe + SumTot
                        Next

                        If obeSolicituPago.Total = SumTot Then
                            Dim idCIP As Int64 = GenerarSolicitudPago(obeSolicituPago)
                            If (idCIP > 0) Then
                                response.Estado = MensajeSolicitud.EstadoOk
                                response.Mensaje = MensajeSolicitud.GeneracionOK(obeSolicituPago.IdSolicitudPago)

                                'response.CIP = obeOrdenPago.NumeroOrdenPago

                                Dim Elements As New Dictionary(Of String, String)
                                Elements.Add("iDResSolPago", obeSolicituPago.IdSolicitudPago)
                                Elements.Add("CodTrans", obeSolicituPago.CodTransaccion)
                                Elements.Add("Token", obeSolicituPago.Token)
                                Elements.Add("Fecha", obeSolicituPago.FechaCreacion)

                                Dim oDASeguridad As New DASeguridad
                                Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(obeSolicituPago.IdServicio)
                                response.Xml = SPE.Negocio.BLContratoRespuesta.DictionaryToXml("ResSolPago", Elements)
                                response.Xml = Encripter.EncryptText(response.Xml.Trim(), pathPublicKey)

                                Dim trackCip As New BETrackCip()
                                trackCip.IdOrdenPago = obeSolicituPago.IdOrdenPago
                                trackCip.IdSolicitudPago = obeSolicituPago.IdSolicitudPago
                                If (obeSolicituPago.IdMoneda = "1") Then
                                    trackCip.Moneda = "PEN"
                                Else
                                    trackCip.Moneda = "USD"
                                End If
                                trackCip.Total = obeSolicituPago.Total.ToString("F")
                                trackCip.CodTransaccion = obeSolicituPago.CodTransaccion
                                trackCip.FechaCreacion = obeSolicituPago.FechaCreacion.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz")
                                trackCip.TiempoExpiracion = (obeSolicituPago.FechaExpiracion - obeSolicituPago.FechaCreacion).TotalHours
                                trackCip.IdServicio = obeSolicituPago.IdServicio
                                trackCip.CodigoServicio = obeSolicituPago.CodServicio
                                trackCip.NombreServicio = obeServicio.Nombre
                                trackCip.IdEmpresa = obeServicio.IdEmpresaContratante
                                trackCip.EmailUsuario = obeSolicituPago.UsuarioEmail
                                trackCip.FechaInicio = fechaInicio.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz")
                                Dim fechaFin As DateTimeOffset = DateTimeOffset.Now
                                trackCip.FechaFin = fechaFin.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz")
                                trackCip.TiempoGeneracion = (fechaFin - fechaInicio).TotalMilliseconds
                                trackCip.Origen = 2
                                Dim stream As New MemoryStream()
                                Dim ser As New DataContractJsonSerializer(trackCip.GetType())
                                ser.WriteObject(stream, trackCip)
                                Dim textoJson As String = System.Text.Encoding.UTF8.GetString(stream.ToArray)
                                _nlogger.Trace(textoJson)
                            Else
                                response.Mensaje = MensajeSolicitud.ErrorGeneracion
                            End If
                        Else
                            response.Mensaje = MensajeSolicitud.ErrorGeneracionMonto
                        End If

                    Else
                        response.Mensaje = MensajeSolicitud.ErrorSolicitudYaGenerada(obeSolicituPago.CodTransaccion)
                    End If
                End If
            End Using
            Return response
        Catch ext As SPE.Exceptions.TramaGetException
            response.Estado = MensajeSolicitud.EstadoNoValido
            response.Mensaje = MensajeSolicitud.ErrorNoManejado(ext.Message)
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = MensajeSolicitud.EstadoException
            response.Mensaje = MensajeSolicitud.ErrorNoManejado(ex.Message)
        End Try
        Return response
    End Function

    Public Function WSConsultarSolicitudPago(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse
        Dim response As New BEWSConsultarSolicitudResponse
        Using oDAOrdenPago As New DAOrdenPago
            Dim oBESolicitudPago As New BESolicitudPago
            Dim oBLTraductorContratoServV3 As New BLTraductorContratoServV3
            Dim oBEServicio As BEServicio
            response.Estado = MensajeSolicitud.EstadoNoValido
            Try
                Using odaServicio As New DAServicio()
                    oBEServicio = odaServicio.ConsultarServicioPorAPI(request.cServ)

                    If (oBEServicio Is Nothing) Then
                        oBEServicio = odaServicio.ConsultarServicioPorCodigo(request.cServ)
                    End If
                    If (oBEServicio Is Nothing) Then
                        response.Mensaje = MensajeSolicitud.ErrorCodigoServicio
                        Return response
                    End If
                End Using
                oBESolicitudPago.IdServicio = oBEServicio.IdServicio

                Dim Traductor As New BLTraductorContratoServV3

                Dim XMLDescifrado As String = Traductor.Descifrar(request.Xml)


                If Not (Traductor.ValidarFirma(XMLDescifrado, request.CClave, oBEServicio.IdServicio)) Then
                    response.Mensaje = MensajeSolicitud.ErrorCCLaveFirma
                    Return response
                End If

                Try
                    oBESolicitudPago.IdSolicitudPago = oBLTraductorContratoServV3.ObtenerIDSolicitudPagoFromXML(XMLDescifrado)
                Catch ex As Exception
                    response.Mensaje = ex.Message()
                    Return response
                End Try
                Dim xmlSolicitud As String = oDAOrdenPago.WSConsultarSolicitudPagoPorIDSolicitudYIDServicio(oBESolicitudPago)

                If Not xmlSolicitud.CompareTo("-1") Then
                    response.Mensaje = MensajeSolicitud.SolicitudNoEncontrada(oBESolicitudPago.IdSolicitudPago)
                Else
                    Dim oDASeguridad As New DASeguridad

                    Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(oBEServicio.IdServicio)
                    If Not String.IsNullOrEmpty(pathPublicKey) Then
                        Try
                            response.Xml = Encripter.EncryptText(xmlSolicitud, pathPublicKey)
                            response.Estado = MensajeSolicitud.EstadoOk
                            response.Mensaje = MensajeSolicitud.SolicitudEncontrada
                        Catch ex As Exception
                            response.Mensaje = MensajeSolicitud.ErrorClavePublica
                            Return response
                        End Try
                    Else
                        response.Mensaje = MensajeSolicitud.ErrorNoTieneKPublica
                        Return response
                    End If
                End If
            Catch ex As Exception
                response.Estado = MensajeSolicitud.EstadoException
                response.Mensaje = MensajeSolicitud.ErrorNoManejado(ex.Message)
            End Try
        End Using
        Return response
    End Function
    Public Function ConsultarSolicitudPagoPorOrdenIdComercio(ByVal IdOrdenPago As String, ByVal IdServicio As Int64) As Boolean
        Dim OdaOrdenPago As New SPE.AccesoDatos.DAOrdenPago
        Return OdaOrdenPago.ConsultarSolicitudPagoPorOrdenIdComercio(IdOrdenPago, IdServicio)
    End Function

    Public Function ObtenerClienteParaWS(ByRef obeSolicitudPago As BESolicitudPago, ByVal obeServicio As BEServicio) As BECliente
        Dim obeCliente As BECliente = Nothing

        If (obeServicio.UsaUsuariosAnonimos) Then
            'obtiene el cliente usuario anonimo
            Using odaCliente As New DACliente()
                obeCliente = odaCliente.ObtenerUsuarioAnonimo()
                obeSolicitudPago.IdCliente = obeCliente.IdCliente
                obeSolicitudPago.IdUsuarioCreacion = obeCliente.IdUsuario
            End Using
        Else
            Using oblCliente As New BLCliente()
                ' Consulta si existe un cliente con por email xxx
                obeCliente = oblCliente.ConsultarClientePorEmail(obeSolicitudPago.UsuarioEmail)
                If (obeCliente Is Nothing) Then
                    'como no existe cliente, se procede a registrarlo como registro basico ws
                    obeCliente = New BECliente()
                    obeCliente.Email = obeSolicitudPago.UsuarioEmail
                    obeCliente.AliasCliente = obeSolicitudPago.UsuarioAlias
                    obeCliente.Nombres = obeSolicitudPago.UsuarioNombre
                    obeCliente.Apellidos = obeSolicitudPago.UsuarioApellidos
                    obeCliente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroBasicoWS
                    obeCliente.FechaNacimiento = DateTime.MinValue

                    Dim EmailComercio As String = IIf(obeServicio.FlgEmailCCAdmin, obeSolicitudPago.MailComercio, String.Empty)

                    If obeServicio.CodigoServicioVersion = ParametrosSistema.Servicio.VersionIntegracion.Version2 _
                        Or Not obeServicio.FlgEmailGenUsuario Then
                        obeCliente.IdUsuario = oblCliente.CommonInsertRecord(obeCliente, False, EmailComercio)
                    Else
                        obeCliente.IdUsuario = oblCliente.CommonInsertRecord(obeCliente, True, EmailComercio)
                    End If
                    Dim tmpCodigoRegistro As String = obeCliente.CodigoRegistro
                    Dim tmpPassword As String = obeCliente.Password
                    obeCliente = oblCliente.ConsultarClientePorEmail(obeSolicitudPago.UsuarioEmail)
                    With obeCliente
                        .RecienRegistrado = True
                        .CodigoRegistro = tmpCodigoRegistro
                        .Password = SeguridadComun.UnEncryptarCodigo(tmpPassword)
                    End With
                Else
                    'existe usuario pero no como cliente, por esto se requiere registrarlo como cliente.
                    If (ObjectToInt32(obeCliente.IdCliente) = 0) Then
                        obeCliente = oblCliente.RegistrarClienteDesdeUsuarioExistente(obeCliente)
                    End If
                End If
            End Using
        End If
        'retorna el cliente obtenido, creado o actualizado.
        Return obeCliente
    End Function
    Public Function GenerarSolicitudPago(ByRef obeSolicitudPago As BESolicitudPago) As Integer
        Dim result As Integer = 0
        Try

            Using transaccionscope As New TransactionScope()
                result = DAOrdenPago().GenerarSolicitudPago(obeSolicitudPago)
                transaccionscope.Complete()
            End Using
            If Not (result = 0) Then
                Dim oBESolicitudPagoGenerado As New BESolicitudPago()
                oBESolicitudPagoGenerado.IdSolicitudPago = result
                oBESolicitudPagoGenerado = DAOrdenPago().ConsultarSolicitudPagoPorId(oBESolicitudPagoGenerado)
                obeSolicitudPago = oBESolicitudPagoGenerado
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result

    End Function

    Public Function ConsultarSolicitudPagoPorToken(ByVal obeSolicitudPago As BESolicitudPago) As BESolicitudPago
        Using oDAOrdenPago As New DAOrdenPago()
            obeSolicitudPago = oDAOrdenPago.ConsultarSolicitudPagoPorToken(obeSolicitudPago)
        End Using
        Return obeSolicitudPago
    End Function
    Public Function ConsultarSolicitudXMLPorID(ByVal obeSolicitud As BESolicitudPago) As String
        Dim SolicitudXML As String
        Using oDAOrdenPago As New DAOrdenPago()
            Try
                SolicitudXML = oDAOrdenPago.WSConsultarSolicitudPago(obeSolicitud)
                Dim oDASeguridad As New DASeguridad
                Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(obeSolicitud.IdServicio)
                SolicitudXML = Encripter.EncryptText(SolicitudXML, pathPublicKey)
            Catch ex As Exception
                SolicitudXML = ""
            End Try
        End Using
        Return SolicitudXML
    End Function
    Public Function GenerarOrdenPagoFromSolicitud(ByVal obeSolicitudPago As BESolicitudPago) As BEOrdenPago
        Dim obeOrdenPago As New BEOrdenPago
        Dim oBLServicioNotificacion As New BLServicioNotificacion
        Dim objEmail As New BLEmail()
        Dim IDOrdenPago As Int64
        Dim emailto As String = String.Empty

        Try
            obeOrdenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
            obeOrdenPago.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService
            Using odaComun As New DAComun()
                obeOrdenPago.FechaEmision = odaComun.ConsultarFechaHoraActual()
            End Using

            Dim oDAOrdenPago As New DAOrdenPago()
            Dim totalSegundos As Integer
            totalSegundos = obeSolicitudPago.FechaExpiracion.Subtract(obeOrdenPago.FechaEmision).TotalSeconds
            obeOrdenPago.TiempoExpiracion = ((totalSegundos / 60) / 60)

            Using transaccionscope As New TransactionScope()
                IDOrdenPago = oDAOrdenPago.GenerarOrdenPagoFromSolicitud(obeSolicitudPago, obeOrdenPago)
                transaccionscope.Complete()
            End Using

            Dim oDAServicio As New DAServicio
            If IDOrdenPago > 0 Then
                obeOrdenPago.IdOrdenPago = IDOrdenPago

                obeOrdenPago = DAOrdenPago().ConsultarOrdenPagoPorId(obeOrdenPago)

                emailto = obeOrdenPago.UsuarioEmail
                If (obeOrdenPago.ServicioUsaUsuariosAnonimos) Then
                    ' strClienteDescripcion = beOrdenP.UsuarioNombre
                    obeOrdenPago.DescripcionCliente = obeOrdenPago.UsuarioNombre
                Else
                    If (ObjectToString(obeOrdenPago.DescripcionCliente) <> "") Then
                        'strClienteDescripcion = beOrdenP.DescripcionCliente
                        obeOrdenPago.DescripcionCliente = obeOrdenPago.DescripcionCliente
                    Else
                        'strClienteDescripcion = beOrdenP.UsuarioNombre
                        obeOrdenPago.DescripcionCliente = obeOrdenPago.UsuarioNombre
                    End If
                End If
                obeOrdenPago.oBEServicio = oDAServicio.GetRecordByID(obeOrdenPago.IdServicio)
                obeOrdenPago.oBECliente = ObtenerClienteParaWS(obeOrdenPago, obeOrdenPago.oBEServicio)
                objEmail.EnviarCorreoGeneracionOrdenPago(obeOrdenPago, emailto)
                'Registro de Notificacion
                If (obeOrdenPago.oBEServicio.FlgNotificaGeneracion = True) Then
                    oBLServicioNotificacion.RegistrarServicioNotificacionUrlOk(obeOrdenPago, 0, obeOrdenPago.IdUsuarioCreacion, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.Web, obeOrdenPago.oBEServicio.IdGrupoNotificacion)
                End If
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return obeOrdenPago
    End Function

    Public Function RealizarProcesoExpiracionSolicitudPago() As Integer
        Try
            Dim oDAOrdenPago As New DAOrdenPago
            Dim oBLServicioNotificacion As New BLServicioNotificacion
            Dim lista As List(Of BESolicitudPago) = oDAOrdenPago.ConsultarSolicitudesPagoListaParaExpiradas()

            For i As Integer = 0 To lista.Count - 1
                Dim oBESolicitudPago As BESolicitudPago = lista(i)
                oDAOrdenPago.ExpirarSolicitudPago(oBESolicitudPago)
                oBLServicioNotificacion.RegistrarServicioNotificacionUrlSolicitudPago(oBESolicitudPago, ParametrosSistema.ServicioNotificacion.IdOrigenRegistro.ProcesoExpiraci�n)
            Next

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return 1
    End Function

    Public Function WSConsultarCIPMod1(ByVal request As BEWSConsultarCIPRequestMod1) As BEWSConsultarCIPResponseMod1
        Dim response As New BEWSConsultarCIPResponseMod1
        Using oDAOrdenPago As New DAOrdenPago

            Dim oBEServicio As BEServicio
            Try
                Dim MetodoBloqueado As Integer = 0
                Using odaServicio As New DAServicio()
                    oBEServicio = odaServicio.ConsultarServicioPorAPI(request.CodServ)

                    If (oBEServicio Is Nothing) Then
                        oBEServicio = odaServicio.ConsultarServicioPorCodigo(request.CodServ)
                    End If
                    If (oBEServicio Is Nothing) Then
                        response.Mensaje = "La API o Codigo no est� asignada a ningun servicio."
                        Return response
                    End If
                    'validaci�n de m�todo bloqueado - Obtiene valor
                    MetodoBloqueado = odaServicio.ValidarMetodoBloqueadoXServicio(oBEServicio.IdServicio, 1)
                    'Fin validaci�n de m�todo bloqueado
                End Using

                Dim Traductor As New BLTraductorContratoServV3
                Dim strCIPS As String = Traductor.Descifrar(request.CIPS)

                If Not (Traductor.ValidarFirma(strCIPS, request.Firma, oBEServicio.IdServicio)) Then
                    response.Mensaje = "Firma Invalida, el CCLave es incorrecto."
                    Return response
                End If

                Dim RegexCIP As New Regex("^\s*\d{1,14}\s*$")
                Dim arrayString As String() = strCIPS.Split(",")
                For i As Int32 = 0 To arrayString.Length - 1
                    arrayString(i) = arrayString(i).Trim()
                    If Not RegexCIP.IsMatch(arrayString(i)) Then
                        With response
                            .Estado = "0"
                            .Mensaje = "Los CIPs no tienen un formato correcto."
                        End With
                        Return response
                    End If
                Next
                'validaci�n de m�todo bloqueado - Valida configuraci�n
                If MetodoBloqueado = 0 Then
                    Dim CIPs As String = String.Join(",", arrayString)
                    Dim resultado As String = DAOrdenPago.WSConsultarCIPMod1(CIPs, oBEServicio.IdServicio)
                    If String.IsNullOrEmpty(resultado) Then
                        With response
                            .Estado = "0"
                            .Mensaje = String.Format("No se encontraron CIPs asociados")
                        End With
                    Else
                        With response
                            Dim oDASeguridad As New DASeguridad
                            Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(oBEServicio.IdServicio)
                            If Not String.IsNullOrEmpty(pathPublicKey) Then
                                Try
                                    .XML = Encripter.EncryptText(resultado, pathPublicKey)
                                    .Estado = "1"
                                    .Mensaje = "La consulta de los CIPs se realiz� con �xito"
                                Catch ex As Exception
                                    response.Mensaje = "Hubo un error al encriptar, verifique que este actualizada la clave publica del servicio en PagoEfectivo"
                                    Return response
                                End Try
                            Else
                                response.Mensaje = "Pago Efectivo no tiene la clave publica del Servicio"
                                Return response
                            End If

                        End With

                    End If
                Else
                    With response
                        .Estado = "0"
                        .Mensaje = "M�todo no encontrado (Method Not Found)"
                    End With
                    Return response
                End If
                'Fin validaci�n de m�todo bloqueado

                If String.IsNullOrEmpty(response.InfoResponse) Then
                    response.InfoResponse = ""
                End If

            Catch ex As Exception
                'Logger.Write(ex)
                With response
                    .Estado = "-1"
                    .Mensaje = String.Format("Ha ocurrido un error no controlado: {0}", ex.Message)
                End With
            End Try
        End Using
        Return response
    End Function

    Public Function WSEliminarCIPMod1(ByVal request As BEWSElimCIPRequestMod1) As BEWSElimCIPResponseMod1
        Dim response As New BEWSElimCIPResponseMod1()
        Try
            Dim obeServicio As BEServicio = Nothing
            Dim MetodoBloqueado As Integer = 0
            Using odaServicio As New DAServicio()
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CodServ)
                response.Estado = "0"
                If (obeServicio Is Nothing) Then
                    obeServicio = odaServicio.ConsultarServicioPorCodigo(request.CodServ)
                End If
                If (obeServicio Is Nothing) Then
                    response.Mensaje = "La API o Codigo no est� asignada a ningun servicio."
                    Return response
                End If
                'validaci�n de m�todo bloqueado - Obtiene valor
                MetodoBloqueado = odaServicio.ValidarMetodoBloqueadoXServicio(obeServicio.IdServicio, 3)
                'Fin validaci�n de m�todo bloqueado
            End Using

            'Desencripto
            Dim Traductor As New BLTraductorContratoServV3
            Dim obeOrdenPago As New BEOrdenPago()
            obeOrdenPago.IdOrdenPago = ObjectToInt64(Traductor.Descifrar(request.CIP))

            If Not (Traductor.ValidarFirma(Traductor.Descifrar(request.CIP), request.Firma, obeServicio.IdServicio)) Then
                response.Mensaje = "Firma Invalida, el CCLave es incorrecto."
                Return response
            End If
            'validaci�n de m�todo bloqueado - Valida configuraci�n
            If MetodoBloqueado = 0 Then
                obeOrdenPago.IdOrigenEliminacion = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.OrigenEliminacion.EliminadoDeWS
                Dim resultado As Integer = DAOrdenPago.EliminarOrdenPago(obeOrdenPago)
                If (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Generada)) Then
                    response.Estado = "1"
                    response.Mensaje = String.Format("El CIP {0} se ha eliminado correctamente.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Cancelada)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("El CIP {0} no se puede eliminar porque ya ha sido cancelado.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("El CIP {0} no se puede eliminar porque ya ha expirado.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Eliminado)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("El CIP {0} ya fue eliminado anteriormente.", obeOrdenPago.IdOrdenPago)
                Else
                    response.Estado = "-1"
                    response.Mensaje = String.Format("Ha ocurrido un Error no Manejado: El estado del CIP no se ha identificado")
                End If
            Else
                With response
                    .Estado = "0"
                    .Mensaje = "M�todo no encontrado (Method Not Found)"
                End With
                Return response
            End If
            'Fin validaci�n de m�todo bloqueado
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = "-1"
            response.Mensaje = String.Format("Ha ocurrido un Error no Manejado:{0}", ex.Message)
        End Try
        Return response
    End Function

    Public Function WSActualizarCIPMod1(ByVal request As BEWSActualizaCIPRequestMod1) As BEWSActualizaCIPResponseMod1
        Dim response As New BEWSActualizaCIPResponseMod1()
        Try

            'Desencripto
            Dim Traductor As New BLTraductorContratoServV3
            Dim obeOrdenPago As New BEOrdenPago()

            Dim obeServicio As BEServicio = Nothing

            Dim MetodoBloqueado As Integer = 0

            obeOrdenPago.IdOrdenPago = ObjectToInt64(Traductor.Descifrar(request.CIP))
            obeOrdenPago.ClaveAPI = request.CodServ
            obeOrdenPago.FechaAExpirar = request.FechaExpira

            Using odaServicio As New DAServicio()
                obeServicio = odaServicio.ConsultarServicioPorAPI(obeOrdenPago.ClaveAPI)
                response.Estado = "0"
                If (obeServicio Is Nothing) Then
                    obeServicio = odaServicio.ConsultarServicioPorCodigo(obeOrdenPago.ClaveAPI)
                End If
                If (obeServicio Is Nothing) Then
                    response.Mensaje = "La API o Codigo no est� asignada a ningun servicio."
                    Return response
                End If
                'validaci�n de m�todo bloqueado - Obtiene valor
                MetodoBloqueado = odaServicio.ValidarMetodoBloqueadoXServicio(obeServicio.IdServicio, 2)
                'Fin validaci�n de m�todo bloqueado
            End Using
            obeOrdenPago.ClaveAPI = obeServicio.ClaveAPI

            If Not (Traductor.ValidarFirma(Traductor.Descifrar(request.CIP), request.Firma, obeServicio.IdServicio)) Then
                response.Mensaje = "Firma Invalida, el CCLave es incorrecto."
                Return response
            End If

            'validaci�n de m�todo bloqueado - Valida configuraci�n
            If MetodoBloqueado = 0 Then
                Dim resultado As Integer = DAOrdenPago.ActualizarFechaExpiraCIP(obeOrdenPago)
                If (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Generada)) Then
                    response.Estado = "1"
                    response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} se ha modificada correctamente.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Cancelada)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} no se puede modificar porque ya ha sido cancelado.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Expirado)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} no se puede modificar porque ya ha expirado.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.EstadoOrdenPago.Eliminado)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("La fecha de expiraci�n del CIP {0} no se puede modificar porque ya fue eliminado anteriormente.", obeOrdenPago.IdOrdenPago)

                ElseIf (resultado = ObjectToInt(ParametrosSistema.ValidaFechaExpiraOrdenPago.Inv�lido)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("La nueva fecha de expiraci�n del CIP {0} debe ser mayor a la fecha actual.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.ValidaFechaExpiraOrdenPago.Excedido)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("La nueva fecha de expiraci�n del CIP {0} no debe excederse a los 10 a�os.", obeOrdenPago.IdOrdenPago)
                ElseIf (resultado = ObjectToInt(ParametrosSistema.ValidaFechaExpiraOrdenPago.claveAPI)) Then
                    response.Estado = "0"
                    response.Mensaje = String.Format("CIP no existe.", obeOrdenPago.IdOrdenPago)

                Else
                    response.Estado = "-1"
                    response.Mensaje = String.Format("Ha ocurrido un Error no Manejado: El estado del CIP no se ha identificado")
                End If
            Else
                With response
                    .Estado = "0"
                    .Mensaje = "M�todo no encontrado (Method Not Found)"
                End With
                Return response
            End If
            'Fin validaci�n de m�todo bloqueado
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = "-1"
            response.Mensaje = String.Format("Ha ocurrido un Error no Manejado:{0}", ex.Message)
        End Try
        Return response
    End Function

#End Region

#Region "Nueva Modalidad 1"
    Public Function WSGenerarCIPMod1(ByVal request As BEWSGenCIPRequestMod1) As BEWSGenCIPResponseMod1
        Dim response As New BEWSGenCIPResponseMod1()
        response.Estado = MensajeSolicitud.EstadoNoValido
        Try
            Dim obeServicio As BEServicio = Nothing
            Using odaServicio As New DAServicio()
                obeServicio = odaServicio.ConsultarServicioPorAPI(request.CodServ)

                If (obeServicio Is Nothing) Then
                    obeServicio = odaServicio.ConsultarServicioPorCodigo(request.CodServ)
                End If
                If (obeServicio Is Nothing) Then
                    response.Mensaje = MensajeSolicitud.ErrorCodigoServicio
                    Return response
                End If
            End Using
            Using traductor As New BLTraductorContratoServV3(obeServicio.ClaveAPI, request.Xml, True, SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService)
                If Not (traductor.ValidarFormatoXML()) Then
                    response.Mensaje = MensajeSolicitud.ErrorFormatoXML
                    Return response
                End If
                Dim XMLDescifrado = traductor.ObtenerXMLContenidoDescifrado()
                If Not (traductor.ValidarFirma(XMLDescifrado, request.Firma, obeServicio.IdServicio)) Then
                    response.Mensaje = MensajeSolicitud.ErrorCCLaveFirma
                    Return response
                End If
                Dim obeSolicituPago As BESolicitudPago = traductor.ObtenerBESolicitudPagoFromXML()
                obeSolicituPago.IdServicio = obeServicio.IdServicio
                obeSolicituPago.TramaSolicitud = XMLDescifrado
                obeSolicituPago.ServicioUsaUsuariosAnonimos = obeServicio.UsaUsuariosAnonimos

                Dim totalSegundos As Integer = 300
                obeSolicituPago.TiempoExpiracion = ((totalSegundos / 60))

                If (obeSolicituPago.FechaExpiracion = Date.MinValue) Then
                    obeSolicituPago.FechaExpiracion = DateTime.Now.AddHours(obeServicio.TiempoExpiracion)
                End If
                Using odaComun As New DAComun()
                    Dim obeMoneda As BEMoneda = odaComun.ConsultarMonedaPorId(obeSolicituPago.IdMoneda)
                    If (obeMoneda Is Nothing) Then
                        response.Mensaje = MensajeSolicitud.ErrorCodigoMoneda
                        Return response
                    End If
                End Using

                Dim obeCliente As BECliente = ObtenerClienteParaWS(obeSolicituPago, obeServicio)
                If (obeCliente Is Nothing) Then
                    response.Mensaje = MensajeSolicitud.ErrorGeneracionCliente
                Else
                    If Not ConsultarSolicitudPagoPorOrdenIdComercio(obeSolicituPago.CodTransaccion, obeSolicituPago.IdServicio) Then
                        'obeSolicituPago.EmailANotifGeneracion = obeSolicituPago.UsuarioEmail
                        obeSolicituPago.IdUsuarioCreacion = obeCliente.IdUsuario
                        obeSolicituPago.IdCliente = obeCliente.IdCliente
                        obeSolicituPago.Idestado = SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pendiente

                        'MODIFICADO-----------------------------------
                        Dim i As Integer
                        Dim SumTot As Decimal = 0
                        Dim ListaDetalleSolicitudPago As List(Of BEDetalleSolicitudPago) = obeSolicituPago.DetallesSolicitudPago
                        If obeSolicituPago.CodigoFormulario = "Formulario" Then
                            For i = 0 To ListaDetalleSolicitudPago.Count - 1
                                SumTot = ListaDetalleSolicitudPago(i).Importe * Convert.ToInt32(ListaDetalleSolicitudPago(i).campo1) + SumTot
                            Next

                        Else
                            For i = 0 To ListaDetalleSolicitudPago.Count - 1
                                SumTot = ListaDetalleSolicitudPago(i).Importe + SumTot
                            Next
                        End If
                        'MODIFICADO-----------------------------------

                        'ANTIGUO--------------------------------------
                        'Dim i As Integer
                        'Dim SumTot As Decimal = 0
                        'Dim ListaDetalleSolicitudPago As List(Of BEDetalleSolicitudPago) = obeSolicituPago.DetallesSolicitudPago
                        'For i = 0 To ListaDetalleSolicitudPago.Count - 1
                        '    SumTot = ListaDetalleSolicitudPago(i).Importe + SumTot
                        'Next
                        'ANTIGUO--------------------------------------


                        Dim beOP As BEOrdenPago = New BEOrdenPago()


                        obeSolicituPago.BEOrdenPago = beOP

                        obeSolicituPago.BEOrdenPago.oBECliente = obeCliente
                        obeSolicituPago.BEOrdenPago.oBEServicio = obeServicio

                        If obeSolicituPago.Total = SumTot Then
                            Dim tramaSolicitud As String = GenerarSolicitudPagoMod1(obeSolicituPago)


                            If Not String.IsNullOrEmpty(tramaSolicitud) Then
                                Dim xml As New XmlDocument
                                xml.InnerXml = tramaSolicitud
                                Dim elSolPago As XmlElement = xml.SelectSingleNode("ConfirSolPago")
                                Dim elCIP As XmlElement = elSolPago.SelectSingleNode("CIP")
                                If elCIP.HasChildNodes() Then
                                    Dim elNumeroOrden As XmlElement = elCIP.SelectSingleNode("NumeroOrdenPago")
                                    response.Mensaje = MensajeSolicitud.GeneracionCIPOK(elNumeroOrden.InnerText)
                                End If

                                Dim oDASeguridad As New DASeguridad
                                Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(obeSolicituPago.IdServicio)
                                response.Estado = MensajeSolicitud.EstadoOk

                                response.Xml = Encripter.EncryptText(tramaSolicitud.Trim(), pathPublicKey)
                            Else
                                response.Mensaje = MensajeSolicitud.ErrorGeneracion
                            End If
                        Else
                            response.Mensaje = MensajeSolicitud.ErrorGeneracionMonto
                        End If

                        'FixSaga 29042014
                    ElseIf New DAComun().HabilitarActualizacionOrdenPago(obeSolicituPago.IdServicio) Then

                        Dim IdSolicitudRegistrado As Integer
                        IdSolicitudRegistrado = New DAComun().ObtenerOrdenSolicitudPorCodTransaccion(obeSolicituPago.IdServicio, obeSolicituPago.CodTransaccion)

                        If IdSolicitudRegistrado <= 0 Then
                            response.Mensaje = MensajeSolicitud.ErrorSolicitudYaGenerada(obeSolicituPago.CodTransaccion)
                            Return response
                        End If

                        obeSolicituPago.IdUsuarioCreacion = obeCliente.IdUsuario
                        obeSolicituPago.IdCliente = obeCliente.IdCliente
                        obeSolicituPago.Idestado = SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pendiente



                        'MODIFICADO-----------------------------------
                        Dim i As Integer
                        Dim SumTot As Decimal = 0
                        Dim ListaDetalleSolicitudPago As List(Of BEDetalleSolicitudPago) = obeSolicituPago.DetallesSolicitudPago
                        If obeSolicituPago.CodigoFormulario = "Formulario" Then
                            For i = 0 To ListaDetalleSolicitudPago.Count - 1
                                SumTot = ListaDetalleSolicitudPago(i).Importe * Convert.ToInt32(ListaDetalleSolicitudPago(i).campo1) + SumTot
                            Next

                        Else
                            For i = 0 To ListaDetalleSolicitudPago.Count - 1
                                SumTot = ListaDetalleSolicitudPago(i).Importe + SumTot
                            Next
                        End If
                        'MODIFICADO-----------------------------------

                        'ANTIGUO--------------------------------------
                        'Dim i As Integer
                        'Dim SumTot As Decimal = 0
                        'Dim ListaDetalleSolicitudPago As List(Of BEDetalleSolicitudPago) = obeSolicituPago.DetallesSolicitudPago
                        'For i = 0 To ListaDetalleSolicitudPago.Count - 1
                        '    SumTot = ListaDetalleSolicitudPago(i).Importe + SumTot
                        'Next
                        'ANTIGUO--------------------------------------

                        Dim beOP As BEOrdenPago = New BEOrdenPago()


                        obeSolicituPago.BEOrdenPago = beOP

                        obeSolicituPago.BEOrdenPago.oBECliente = obeCliente
                        obeSolicituPago.BEOrdenPago.oBEServicio = obeServicio

                        If obeSolicituPago.Total = SumTot Then
                            'FixSaga 29042014
                            obeSolicituPago.IdSolicitudPago = IdSolicitudRegistrado

                            Dim tramaSolicitud As String = ActualizarSolicitudPagoMod1(obeSolicituPago)
                            If Not String.IsNullOrEmpty(tramaSolicitud) Then
                                Dim xml As New XmlDocument
                                xml.InnerXml = tramaSolicitud
                                Dim elSolPago As XmlElement = xml.SelectSingleNode("ConfirSolPago")
                                Dim elCIP As XmlElement = elSolPago.SelectSingleNode("CIP")
                                If elCIP.HasChildNodes() Then
                                    Dim elNumeroOrden As XmlElement = elCIP.SelectSingleNode("NumeroOrdenPago")
                                    response.Mensaje = MensajeSolicitud.GeneracionCIPOK(elNumeroOrden.InnerText)
                                End If

                                Dim oDASeguridad As New DASeguridad
                                Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(obeSolicituPago.IdServicio)
                                response.Estado = MensajeSolicitud.EstadoOk

                                response.Xml = Encripter.EncryptText(tramaSolicitud.Trim(), pathPublicKey)
                            Else
                                response.Mensaje = MensajeSolicitud.ErrorGeneracion
                            End If
                        Else
                            response.Mensaje = MensajeSolicitud.ErrorGeneracionMonto
                        End If

                    Else
                        response.Mensaje = MensajeSolicitud.ErrorSolicitudYaGenerada(obeSolicituPago.CodTransaccion)
                    End If
                End If
            End Using
            Return response
        Catch ext As SPE.Exceptions.TramaGetException
            response.Estado = MensajeSolicitud.EstadoNoValido
            response.Mensaje = MensajeSolicitud.ErrorNoManejado(ext.Message)
        Catch ex As Exception
            'Logger.Write(ex)
            response.Estado = MensajeSolicitud.EstadoException
            response.Mensaje = MensajeSolicitud.ErrorNoManejado(ex.Message)
        End Try
        Return response
    End Function

    'FixSaga 29042014
    Public Function ActualizarSolicitudPagoMod1(ByRef obeSolicitudPago As BESolicitudPago) As String
        Dim result As String = ""
        Dim idSolicitud As Int64 = 0
        Try

            Using transaccionscope As New TransactionScope()
                idSolicitud = DAOrdenPago().ActualizarSolicitudPagoYOrden(obeSolicitudPago.IdSolicitudPago, obeSolicitudPago)
                transaccionscope.Complete()
            End Using
            If idSolicitud > 0 Then
                obeSolicitudPago.IdSolicitudPago = idSolicitud
                result = DAOrdenPago().WSConsultarSolicitudPago(obeSolicitudPago)
            End If

            'se agregar envio de correo electronico'
            Dim beOrdenP As New BEOrdenPago()


            Dim xml As New XmlDocument
            xml.InnerXml = result
            Dim elSolPago As XmlElement = xml.SelectSingleNode("ConfirSolPago")
            Dim strEmailUsuarioXML As String = ""
            Dim strEmailComercioXML As String = ""
            If elSolPago.HasChildNodes() Then
                Dim cipXML As XmlElement = elSolPago.SelectSingleNode("CIP")
                If cipXML.HasChildNodes() Then
                    Dim idOrdenPagoXML As XmlElement = cipXML.SelectSingleNode("IdOrdenPago")
                    Dim EmailUsuarioXML As XmlElement = cipXML.SelectSingleNode("UsuarioEmail")
                    Dim EmailComercioXML As XmlElement = cipXML.SelectSingleNode("MailComercio")
                    beOrdenP.IdOrdenPago = Convert.ToInt64(idOrdenPagoXML.InnerText)
                    strEmailUsuarioXML = EmailUsuarioXML.InnerText
                    strEmailComercioXML = EmailComercioXML.InnerText
                End If
            End If


            obeSolicitudPago.BEOrdenPago.ServicioUsaUsuariosAnonimos = obeSolicitudPago.BEOrdenPago.oBEServicio.UsaUsuariosAnonimos
            beOrdenP = DAOrdenPago().ConsultarOrdenPagoPorId(beOrdenP)
            beOrdenP.MailComercio = strEmailComercioXML
            beOrdenP.ClienteEmail = strEmailUsuarioXML
            beOrdenP.IdTipoNotificacion = obeSolicitudPago.BEOrdenPago.oBEServicio.IdTipoNotificacion
            beOrdenP.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
            beOrdenP.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService
            beOrdenP.ServicioUsaUsuariosAnonimos = obeSolicitudPago.BEOrdenPago.oBEServicio.UsaUsuariosAnonimos

            Using odaAdmComun As New DAComun()
                beOrdenP.FechaEmision = odaAdmComun.ConsultarFechaHoraActual()
            End Using
            If (beOrdenP.FechaAExpirar = Date.MinValue) Then
                beOrdenP.TiempoExpiracion = obeSolicitudPago.BEOrdenPago.oBEServicio.TiempoExpiracion
            Else
                Dim totalSegundos As Integer = obeSolicitudPago.BEOrdenPago.FechaAExpirar.Subtract(obeSolicitudPago.BEOrdenPago.FechaEmision).TotalSeconds
                beOrdenP.TiempoExpiracion = ((totalSegundos / 60) / 60)
            End If

            Dim objEmail As New BLEmail()
            Dim emailto As String = strEmailUsuarioXML
            Dim strClienteDescripcion As String = ""
            If (obeSolicitudPago.BEOrdenPago.ServicioUsaUsuariosAnonimos) Then
                beOrdenP.DescripcionCliente = beOrdenP.UsuarioNombre
            Else
                If (ObjectToString(beOrdenP.DescripcionCliente) <> "") Then
                    beOrdenP.DescripcionCliente = beOrdenP.DescripcionCliente
                Else
                    beOrdenP.DescripcionCliente = beOrdenP.UsuarioNombre
                End If
            End If
            With beOrdenP
                .oBECliente = obeSolicitudPago.BEOrdenPago.oBECliente
                .oBEServicio = obeSolicitudPago.BEOrdenPago.oBEServicio
            End With
            If obeSolicitudPago.BEOrdenPago.oBEServicio.FlgEmailGenCIP Then
                objEmail.EnviarCorreoGeneracionOrdenPago(beOrdenP, emailto)
            End If
            'se agregar envio de correo electronico'

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function

    Public Function GenerarSolicitudPagoMod1(ByRef obeSolicitudPago As BESolicitudPago) As String
        Dim result As String = ""
        Dim idSolicitud As Int64 = 0
        Dim mensaje = String.Empty
        Try
            Using transaccionscope As New TransactionScope()
                idSolicitud = DAOrdenPago().GenerarSolicitudPagoYOrden(obeSolicitudPago)
                transaccionscope.Complete()
            End Using
            If idSolicitud > 0 Then
                obeSolicitudPago.IdSolicitudPago = idSolicitud
                result = DAOrdenPago().WSConsultarSolicitudPago(obeSolicitudPago)
            End If

            'se agregar envio de correo electronico'
            Dim beOrdenP As New BEOrdenPago()


            Dim xml As New XmlDocument
            xml.InnerXml = result
            Dim elSolPago As XmlElement = xml.SelectSingleNode("ConfirSolPago")
            Dim strEmailUsuarioXML As String = ""
            Dim strEmailComercioXML As String = ""
            If elSolPago.HasChildNodes() Then
                Dim cipXML As XmlElement = elSolPago.SelectSingleNode("CIP")
                If cipXML.HasChildNodes() Then
                    Dim idOrdenPagoXML As XmlElement = cipXML.SelectSingleNode("IdOrdenPago")
                    Dim EmailUsuarioXML As XmlElement = cipXML.SelectSingleNode("UsuarioEmail")
                    Dim EmailComercioXML As XmlElement = cipXML.SelectSingleNode("MailComercio")
                    beOrdenP.IdOrdenPago = Convert.ToInt64(idOrdenPagoXML.InnerText)
                    strEmailUsuarioXML = EmailUsuarioXML.InnerText
                    strEmailComercioXML = EmailComercioXML.InnerText
                End If
            End If


            obeSolicitudPago.BEOrdenPago.ServicioUsaUsuariosAnonimos = obeSolicitudPago.BEOrdenPago.oBEServicio.UsaUsuariosAnonimos
            beOrdenP = DAOrdenPago().ConsultarOrdenPagoPorId(beOrdenP)
            beOrdenP.MailComercio = strEmailComercioXML
            beOrdenP.ClienteEmail = strEmailUsuarioXML
            beOrdenP.IdTipoNotificacion = obeSolicitudPago.BEOrdenPago.oBEServicio.IdTipoNotificacion
            beOrdenP.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
            beOrdenP.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService
            beOrdenP.ServicioUsaUsuariosAnonimos = obeSolicitudPago.BEOrdenPago.oBEServicio.UsaUsuariosAnonimos

            Using odaAdmComun As New DAComun()
                beOrdenP.FechaEmision = odaAdmComun.ConsultarFechaHoraActual()
            End Using
            If (beOrdenP.FechaAExpirar = Date.MinValue) Then
                beOrdenP.TiempoExpiracion = obeSolicitudPago.BEOrdenPago.oBEServicio.TiempoExpiracion
            Else
                Dim totalSegundos As Integer = obeSolicitudPago.BEOrdenPago.FechaAExpirar.Subtract(obeSolicitudPago.BEOrdenPago.FechaEmision).TotalSeconds
                beOrdenP.TiempoExpiracion = ((totalSegundos / 60) / 60)
            End If

            Dim objEmail As New BLEmail()
            Dim emailto As String = strEmailUsuarioXML
            Dim strClienteDescripcion As String = ""
            If (obeSolicitudPago.BEOrdenPago.ServicioUsaUsuariosAnonimos) Then
                beOrdenP.DescripcionCliente = beOrdenP.UsuarioNombre
            Else
                If (ObjectToString(beOrdenP.DescripcionCliente) <> "") Then
                    beOrdenP.DescripcionCliente = beOrdenP.DescripcionCliente
                Else
                    beOrdenP.DescripcionCliente = beOrdenP.UsuarioNombre
                End If
            End If
            With beOrdenP
                .oBECliente = obeSolicitudPago.BEOrdenPago.oBECliente
                .oBEServicio = obeSolicitudPago.BEOrdenPago.oBEServicio
            End With
            If obeSolicitudPago.BEOrdenPago.oBEServicio.FlgEmailGenCIP Then

                'Agregago-------------------------------------------
                If obeSolicitudPago.CodigoFormulario = "web" Then
                    objEmail.EnviarCorreoGeneracionOrdenPago(beOrdenP, emailto)
                Else
                    mensaje = "El correo se enviara desde el formulario"
                End If
                'Agregado-------------------------------------------

            End If

            'se agregar envio de correo electronico'

        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function

#End Region

#Region "Devoluciones"
    Public Function RegistrarDevolucion(ByVal oBEDevolucion As BEDevolucion) As BEDevolucion
        Dim oBEDevolucionRetorno As New BEDevolucion()
        'Using transaccionscope As New TransactionScope()
        oBEDevolucionRetorno = New DAOrdenPago().RegistrarDevolucion(oBEDevolucion)
        'Agregar el archivo
        Dim RutaCompletaDevolucion = AppSettings("RutaArchivosDevolucion") + oBEDevolucionRetorno.IdRetorno.ToString() + "_" + oBEDevolucion.NombreArchivoDevolucion
        Using strm As New System.IO.FileStream(RutaCompletaDevolucion, System.IO.FileMode.Create)
            strm.Write(oBEDevolucion.ArchivoDevolucion, 0, oBEDevolucion.ArchivoDevolucion.Length)
            strm.Close()
        End Using 'oDAOrdenPago.ObtenerEmailsAdminCip()
        If oBEDevolucionRetorno.CodigoError = 0 Then
            ThreadPool.QueueUserWorkItem(Sub()
                                             Dim oBLEmail As New BLEmail
                                             Dim oDAOrdenPago As New DAOrdenPago
                                             Dim ListaEmailsAdminCip As String
                                             ListaEmailsAdminCip = ObtenerCorreosTodosAdmin()
                                             oBEDevolucion.DescripcionEstadoAdmin = ParametrosSistema.EstadoDevolucionDescripcion.Solicitado
                                             oBEDevolucion.IdDevolucion = oBEDevolucionRetorno.IdRetorno
                                             oBEDevolucion.FechaCreacion = DateTime.Now
                                             oBLEmail.EnviarCorreoDevolucionGenerado(oBEDevolucion, ListaEmailsAdminCip)
                                         End Sub)
        End If
        'transaccionscope.Complete()

        'End Using
        Return oBEDevolucionRetorno
    End Function


    Private Function ObtenerCorreosTodosAdmin() As String
        Dim oDAOrdenPago As New DAOrdenPago
        Dim oListaEmailsAdminCip() As String
        Dim EmailsTodosEnUno As String = String.Empty
        oListaEmailsAdminCip = oDAOrdenPago.ObtenerEmailsAdminCip()
        For Each Email As String In oListaEmailsAdminCip
            EmailsTodosEnUno = EmailsTodosEnUno + Email + ","
        Next
        EmailsTodosEnUno = EmailsTodosEnUno.Substring(0, EmailsTodosEnUno.Length - 1)
        Return EmailsTodosEnUno
    End Function

    Public Function ObtenerArchivoDevolucion(ByVal Archivo As String) As Byte()
        Dim myWebClient As New WebClient()
        Dim myByteArray As Byte()
        Try
            myByteArray = myWebClient.DownloadData(AppSettings("RutaArchivosDevolucion") + Archivo)
        Catch ex As Exception
            myByteArray = Nothing
        End Try

        Return myByteArray
    End Function

    Public Function ConsultarDevoluciones(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
        Using oDAOrdenPago As New DAOrdenPago()
            Return oDAOrdenPago.ConsultarDevoluciones(oBEDevolucion)
        End Using
    End Function
    Public Function ActualizarDevolucion(ByVal oBEDevolucion As BEDevolucion) As Int64
        Dim respuesta As Integer
        respuesta = New DAOrdenPago().ActualizarDevolucion(oBEDevolucion)
        'If oBEDevolucion.IdEstadoAdmin = ParametrosSistema.EstadoDevolucion.Realizado Then
        If oBEDevolucion.IdEstadoAdmin <> 692 Then
            ThreadPool.QueueUserWorkItem(Sub()
                                             Dim oBLEmail As New BLEmail
                                             'oBEDevolucion.DescripcionEstadoAdmin = ParametrosSistema.EstadoDevolucionDescripcion.Realizado
                                             Dim ListaEmailsAdminCip As String
                                             ListaEmailsAdminCip = ObtenerCorreosTodosAdmin()
                                             oBLEmail.EnviarCorreoDevolucionRealizado(oBEDevolucion, ListaEmailsAdminCip)
                                         End Sub)
            'End If
        End If
        Return respuesta
    End Function
    'DevolucionTelefonica
    Public Function ConsultarCipDevolucion(ByVal NroCip As Int64, ByVal IdUsuario As Int32) As BEOrdenPago
        Using oDAOrdenPago As New DAOrdenPago()
            Return oDAOrdenPago.ConsultarCipDevolucion(NroCip, IdUsuario)
        End Using
    End Function

    Public Function ConsultarDevolucionesExcel(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
        Using oDAOrdenPago As New DAOrdenPago()
            Return oDAOrdenPago.ConsultarDevolucionesExcel(oBEDevolucion)
        End Using
    End Function
#End Region

    Public Function ActualizarCIP(oBEOrdenPago As BEOrdenPago) As Int64
        Dim resultado As Int64 = 0
        Try
            resultado = DAOrdenPago.ActualizarCIP(oBEOrdenPago)
        Catch ex As Exception
            'Logger.Write(ex)
            resultado = -1
        End Try
        Return resultado
    End Function
    Public Function EliminarCIP(oBEOrdenPago As BEOrdenPago) As Int64
        Dim resultado As Int64 = 0
        Try
            resultado = DAOrdenPago.EliminarCIP(oBEOrdenPago)
        Catch ex As Exception
            'Logger.Write(ex)
            resultado = -1
        End Try
        Return resultado
    End Function
    Public Function ConsultarOrdenPagoPorIdOrdenYIdRepresentante(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim resultado As BEOrdenPago = Nothing
        Try
            resultado = DAOrdenPago.ConsultarOrdenPagoPorIdOrdenYIdRepresentante(obeOrdenPago)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function EliminarCIPWeb(oBEOrdenPago As BEOrdenPago) As Int64
        Dim resultado As Int64 = 0
        Try
            oBEOrdenPago.NumeroOrdenPago = TraductorServ.DesencriptarStr(oBEOrdenPago.NumeroOrdenPago)
            resultado = DAOrdenPago.EliminarCIP(oBEOrdenPago)
        Catch ex As Exception
            'Logger.Write(ex)
            resultado = -1
        End Try
        Return resultado
    End Function
#Region "Saga"
    Public Function WSConsultarSolicitudPagov2(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse
        Dim response As New BEWSConsultarSolicitudResponse
        Using oDAOrdenPago As New DAOrdenPago
            Dim oBESolicitudPago As New BESolicitudPago
            Dim oBESolicitudPagoAdic As New BESolicitudPago()
            Dim oBLTraductorContratoServV3 As New BLTraductorContratoServV3
            Dim oBEServicio As BEServicio
            response.Estado = MensajeSolicitud.EstadoNoValido
            Try
                Using odaServicio As New DAServicio()
                    oBEServicio = odaServicio.ConsultarServicioPorAPI(request.cServ)

                    If (oBEServicio Is Nothing) Then
                        oBEServicio = odaServicio.ConsultarServicioPorCodigo(request.cServ)
                    End If
                    If (oBEServicio Is Nothing) Then
                        response.Mensaje = MensajeSolicitud.ErrorCodigoServicio
                        Return response
                    End If
                End Using
                oBESolicitudPago.IdServicio = oBEServicio.IdServicio

                Dim Traductor As New BLTraductorContratoServV3

                Dim XMLDescifrado As String = Traductor.Descifrar(request.Xml)


                If Not (Traductor.ValidarFirma(XMLDescifrado, request.CClave, oBEServicio.IdServicio)) Then
                    response.Mensaje = MensajeSolicitud.ErrorCCLaveFirma
                    Return response
                End If

                Try
                    oBESolicitudPagoAdic = oBLTraductorContratoServV3.ObtenerDatosToConsultaCipFromXML(XMLDescifrado)
                    oBESolicitudPago.CodServicio = oBESolicitudPagoAdic.CodServicio
                    oBESolicitudPago.CodTransaccion = oBESolicitudPagoAdic.CodTransaccion

                Catch ex As Exception
                    response.Mensaje = ex.Message()
                    Return response
                End Try
                Dim xmlSolicitud As String = oDAOrdenPago.WSConsultarSolicitudPagoPorCodServCodTran(oBESolicitudPago)

                If Not xmlSolicitud.CompareTo("-1") Then
                    response.Mensaje = MensajeSolicitud.SolicitudNoEncontrada(oBESolicitudPago.IdSolicitudPago)
                Else
                    Dim oDASeguridad As New DASeguridad

                    Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(oBEServicio.IdServicio)
                    If Not String.IsNullOrEmpty(pathPublicKey) Then
                        Try
                            response.Xml = Encripter.EncryptText(xmlSolicitud, pathPublicKey)
                            response.Estado = MensajeSolicitud.EstadoOk
                            response.Mensaje = MensajeSolicitud.SolicitudEncontrada
                        Catch ex As Exception
                            response.Mensaje = MensajeSolicitud.ErrorClavePublica
                            Return response
                        End Try
                    Else
                        response.Mensaje = MensajeSolicitud.ErrorNoTieneKPublica
                        Return response
                    End If
                End If
            Catch ex As Exception
                response.Estado = MensajeSolicitud.EstadoException
                response.Mensaje = MensajeSolicitud.ErrorNoManejado(ex.Message)
            End Try
        End Using
        Return response
    End Function
#End Region

#Region "ToGenerado"
    Public Function ConsultarOrdenPagoPorIdSoloGenYExp(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
        Dim odaOrdenPago As New DAOrdenPago
        Return odaOrdenPago.ConsultarOrdenPagoPorIdSoloGenYExp(obeOrdenPago)
    End Function

    Public Function ActualizarCIPDeExpAGen(oBEOrdenPago As BEOrdenPago) As Int64
        Dim resultado As Int64 = 0
        Try
            resultado = DAOrdenPago.ActualizarCIPDeExpAGen(oBEOrdenPago)
        Catch ex As Exception
            'Logger.Write(ex)
            resultado = -1
        End Try
        Return resultado
    End Function

    Public Function ExtornarCIP(ByVal nroCIP As String, ByVal tipoExtorno As Integer) As Int64
        Dim resultado As Int64 = 0
        Dim odaOP As New DAOrdenPago
        Try
            resultado = odaOP.ExtornarCIP(nroCIP, tipoExtorno)
        Catch ex As Exception
            resultado = -1
        End Try
        Return resultado
    End Function
#End Region

#Region "Messaging"
    '************************************************************************************************** 
    ' M�todo          : RegisterCellPhoneNumber
    ' Descripci�n     : Registra el n�mero de celular
    ' Autor           : Paul Otiniano
    ' Fecha/Hora      : 08/02/2018
    ' Parametros_In   : Instancia de la entidad BEMobileMessaging 
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function RegisterCellPhoneNumber(ByVal oBEMobileMessaging As BEMobileMessaging) As Integer
        Dim result As Integer = 0
        Dim ordenPago As New DAOrdenPago
        Try
            result = ordenPago.RegisterCellPhoneNumber(oBEMobileMessaging)
        Catch ex As Exception
            result = -1
        End Try
        Return result
    End Function

    '************************************************************************************************** 
    ' M�todo          : ValidateCipExpiration
    ' Descripci�n     : Valida si el CIP est� expirado
    ' Autor           : Paul Otiniano
    ' Fecha/Hora      : 08/02/2018
    ' Parametros_In   : PaymentOrderId 
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    Public Function ValidateCipExpiration(ByVal PaymentOrderId As Int64) As String
        Dim result As String = String.Empty
        Dim ordenPago As New DAOrdenPago

        result = ordenPago.ValidateCipExpiration(PaymentOrderId)

        Return result
    End Function

    ''' <summary>
    ''' Consulta el mensaje de una plantillla sms
    ''' </summary>
    ''' <param name="smsTemplateId">Id de la plantilla</param>
    ''' <returns>Mensaje de la plantilla</returns>
    Public Function GetMessageSms(ByVal smsTemplateId As Integer) As String
        Dim result As String = String.Empty
        Dim �rdenPago As New DAOrdenPago

        result = �rdenPago.GetMessageSms(smsTemplateId)

        Return result
    End Function
#End Region
End Class

