Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos

Public Class BLParametro
    Implements IDisposable

    '
    '************************************************************************************************** 
    ' M�todo          : ConsultarParametroPorCodigoGrupo
    ' Descripci�n     : Consulta de parametros del sistema
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : CodigoGrupo
    ' Parametros_Out  : Lista de entidades BEParametro
    ''**************************************************************************************************
    Public Function ConsultarParametroPorCodigoGrupo(ByVal CodigoGrupo As String) As List(Of BEParametro)
        Using odaParametro As New DAParametro
            Return odaParametro.ConsultarParametroPorCodigoGrupo(CodigoGrupo)
        End Using
    End Function
    Public Function ListarVersionServicio() As List(Of BEParametro)
        Using odaParametro As New DAParametro
            Return odaParametro.ListarVersionServicio()
        End Using
    End Function

    '

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
