Imports System
Imports System.Transactions
Imports System.Collections.Generic
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades
Imports SPE.AccesoDatos

Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Utilitario
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

Public Class BLPasarela
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

#Region "Metodos Base"
    Private Function DAPasarela() As DAPasarela
        Return CType(DataAccessObject, SPE.AccesoDatos.DAPasarela)
    End Function

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New DAPasarela()
    End Function

#End Region

    ''' <summary>
    ''' registrar solicitud en la tabla pasarela
    ''' </summary>
    ''' <param name="CodServicio">Codigo del servicio</param>
    ''' <param name="DtosEncriptados">Datos Encriptados</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GenerarPasarela(ByVal CodServicio As String, ByVal DtosEncriptados As String) As BEGenPasResponse

        Dim response As New BEGenPasResponse
        Dim obeServicio As BEServicio

        Try

            Using odaServicio As New DAServicio
                obeServicio = odaServicio.ConsultarServicioPorCodigo(CodServicio)

                'validar que existe el servicio segun el codigo
                If obeServicio Is Nothing Then
                    response.Estado = "0"
                    response.Mensaje = "No existe servicios con el codigo: " + CodServicio
                    Return response
                End If

                'validar el estado del servicio encontrado
                If obeServicio.IdEstado <> ParametrosSistema.EstadoMantenimiento.Activo Then
                    response.Estado = "0"
                    response.Mensaje = "El servicio " + obeServicio.Nombre + " tiene estado: inactivo"
                    Return response
                End If

            End Using

            Using traductor As New BLTraductorContratoPas(CodServicio, DtosEncriptados)

                'validando formato xml del contrato
                If traductor Is Nothing Then
                    response.Estado = "0"
                    response.Mensaje = "El formato del xml es incorrecto."
                    Return response
                End If

            End Using



        Catch ex As Exception
            Logger.Write(ex.Message)
            response.Estado = -1
            response.Mensaje = String.Format("Ha ocurrido un Error no Manejado: {0}", ex.Message)
        End Try

        Return response

    End Function

End Class
