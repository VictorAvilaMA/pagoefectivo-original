Imports System
Imports System.Transactions
Imports System.Collections.Generic

Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Logging

Public Class BLPlantilla
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase
    Private ReadOnly _nlogger As ILogger = New Logger()

    Public Sub New()

    End Sub

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAPlantilla
    End Function
    Public ReadOnly Property DAPlantilla() As DAPlantilla
        Get
            Return CType(DataAccessObject, DAPlantilla)
        End Get
    End Property

    Public Function ConsultarPlantillaPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Return DAPlantilla.ConsultarPlantillaPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
    End Function

    Public Function ConsultarPlantillaPorTipoYVariacionExiste(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Return DAPlantilla.ConsultarPlantillaPorTipoYVariacionExiste(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
    End Function

    Public Function ConsultarPlantillaYParametrosPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Try
            Dim obePlantilla As BEPlantilla = DAPlantilla.ConsultarPlantillaPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
            If (obePlantilla IsNot Nothing) Then
                obePlantilla.ListaParametros = DAPlantilla.ConsultarPlantillaParametroPorIdPlantilla(obePlantilla.IdPlantilla)
            End If
            Return obePlantilla
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function GenerarPlantillaHtml(ByVal request As BEGenPlantillaHtmlRequest) As BEGenPlantillaHtmlResponse
        Dim response As New BEGenPlantillaHtmlResponse()
        Dim obePlantilla As BEPlantilla = ConsultarPlantillaYParametrosPorTipoYVariacion(request.IdServicio, request.CodPlantillaTipo, request.CodPlantillaTipoVariacion)
        If (obePlantilla IsNot Nothing) Then

            Dim paramValores As IDictionary(Of String, String) = New Dictionary(Of String, String)()
            Dim strHtml As String = obePlantilla.PlantillaHtml

            For Each obeParam As BEPlantillaParametro In obePlantilla.ListaParametros
                Select Case obeParam.IdTipoParametro
                    Case Plantilla.Parametro.Tipo.Estatico
                        strHtml = strHtml.Replace(obeParam.Etiqueta, obeParam.Valor)
                        If Not (paramValores.ContainsKey(obeParam.Etiqueta)) Then
                            paramValores.Add(obeParam.Etiqueta, obeParam.Valor)
                        Else
                            paramValores(obeParam.Etiqueta) = obeParam.Valor
                        End If
                    Case Plantilla.Parametro.Tipo.Dinamico
                        If (request.ValoresDinamicos.ContainsKey(obeParam.FuenteEtiqueta)) Then
                            Dim strtmpvalor As String = request.ValoresDinamicos(obeParam.FuenteEtiqueta)
                            strHtml = strHtml.Replace(obeParam.FuenteEtiqueta, strtmpvalor)
                            If Not (paramValores.ContainsKey(obeParam.FuenteEtiqueta)) Then
                                paramValores.Add(obeParam.FuenteEtiqueta, strtmpvalor)
                            Else
                                paramValores(obeParam.FuenteEtiqueta) = strtmpvalor
                            End If
                        End If
                End Select
            Next

            'response = New BEGenPlantillaHtmlResponse(obePlantilla, paramValores, strHtml)
            response.Html = strHtml
            response.ParamValores = paramValores
            response.Plantilla = obePlantilla
            response.State = "1"
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok
        Else
            response.State = "0"
            response.Message = "La plantilla no existe"
            response.BMResultState = _3Dev.FW.Entidades.BMResult.No
        End If
        Return response
    End Function

    '<add Peru.Com FaseIII>
    Public Function ConsultarPlantillaTipoPorMoneda(idMoneda As Int32) As List(Of BEPlantillaTipo)
        Return DAPlantilla.ConsultarPlantillaTipoPorMoneda(idMoneda)
    End Function
    Public Function ConsultarPlantillaTipoPorMonedaYServicio(ByVal idMoneda As Int32, ByVal idServicio As Int32) As List(Of BEPlantillaTipo)
        Return DAPlantilla.ConsultarPlantillaTipoPorMonedaYServicio(idMoneda, idServicio)
    End Function

    Public Function GenerarPlantillaDesdePlantillaBase(ByVal oBEPlantilla As BEPlantilla) As BEPlantilla
        Return DAPlantilla.GenerarPlantillaDesdePlantillaBase(oBEPlantilla)
    End Function

    Public Function ConsultarPlantillaParametroPorIdPlantilla(ByVal idPlantilla As Int32) As List(Of BEPlantillaParametro)
        Return DAPlantilla.ConsultarPlantillaParametroPorIdPlantilla(idPlantilla)
    End Function
    Public Function ConsultarParametrosFuentes() As List(Of BEPlantillaParametro)
        Return DAPlantilla.ConsultarParametrosFuentes()
    End Function
    Public Function RegistrarPlantillaParametros(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Return DAPlantilla.RegistrarPlantillaParametros(oBEPlantillaParametro)
    End Function
    Public Function ActualizarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Return DAPlantilla.ActualizarPlantillaParametro(oBEPlantillaParametro)
    End Function
    Public Function EliminarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Return DAPlantilla.EliminarPlantillaParametro(oBEPlantillaParametro)
    End Function
    Public Function ActualizarPlantilla(ByVal oBEPlantilla As BEPlantilla) As Integer
        Return DAPlantilla.ActualizarPlantilla(oBEPlantilla)
    End Function

#Region "METODOS IMPLEMENTADOS PARA MONEDERO ELECTR�NICO"

    Public Function ConsultarPlantillaYParametrosEmail(ByVal IdPlantilla As Integer) As BEPlantilla
        Try
            Dim obePlantilla As BEPlantilla = DAPlantilla.ConsultarPlantillaEmailPorId(IdPlantilla)
            If (obePlantilla IsNot Nothing) Then
                obePlantilla.ListaParametros = DAPlantilla.ConsultarPlantillaParametroEmailPorIdPlantilla(obePlantilla.IdPlantilla)
            End If
            Return obePlantilla
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function GenerarPlantillaEmailHtml(ByVal IdTipoEmail As Integer, request As BEGenPlantillaHtmlRequest) As BEGenPlantillaHtmlResponse
        Dim response As New BEGenPlantillaHtmlResponse()
        Dim obePlantilla As BEPlantilla = ConsultarPlantillaYParametrosEmail(IdTipoEmail) ''En IdServicio envimos el Id Plantilla
        If (obePlantilla IsNot Nothing) Then

            Dim paramValores As IDictionary(Of String, String) = New Dictionary(Of String, String)()
            Dim strHtml As String = obePlantilla.PlantillaHtml

            For Each obeParam As BEPlantillaParametro In obePlantilla.ListaParametros
                Select Case obeParam.IdTipoParametro
                    Case Plantilla.Parametro.Tipo.Estatico
                        strHtml = strHtml.Replace(obeParam.Etiqueta, obeParam.Valor)
                        If Not (paramValores.ContainsKey(obeParam.Etiqueta)) Then
                            paramValores.Add(obeParam.Etiqueta, obeParam.Valor)
                        Else
                            paramValores(obeParam.Etiqueta) = obeParam.Valor
                        End If
                    Case Plantilla.Parametro.Tipo.Dinamico
                        If (request.ValoresDinamicos.ContainsKey(obeParam.Etiqueta)) Then
                            Dim strtmpvalor As String = request.ValoresDinamicos(obeParam.Etiqueta)
                            strHtml = strHtml.Replace(obeParam.Etiqueta, strtmpvalor)
                            If Not (paramValores.ContainsKey(obeParam.Etiqueta)) Then
                                paramValores.Add(obeParam.Etiqueta, strtmpvalor)
                            Else
                                paramValores(obeParam.Etiqueta) = strtmpvalor
                            End If
                        End If
                End Select
            Next

            'response = New BEGenPlantillaHtmlResponse(obePlantilla, valoresParams, strHtml)
            response.Html = strHtml
            response.ParamValores = paramValores
            response.Plantilla = obePlantilla
            response.State = "1"
            response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok
        Else
            response.State = "0"
            response.Message = "La plantilla no existe"
            response.BMResultState = _3Dev.FW.Entidades.BMResult.No
        End If
        Return response
    End Function
#End Region

    '************************************************************************************************** 
    ' M�todo          : ConsultarPLantillasXServicioGenPago
    ' Descripci�n     : Consulta las plantillas de los correos correspondientes a un servicio para la pasarela de Pagos
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 29/05/2012
    ' Parametros_In   : idServicio , idMoneda
    ' Parametros_Out  : Cadena HTML descripci�n de bancos
    ''**************************************************************************************************
    Public Function ConsultarPLantillasXServicioGenPago(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal codPlantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String
        Try
            codPlantillaTipo += IIf(idMoneda = 2, "DolarAM", "")
            Dim oBEPLantilla As BEPlantilla = New BLPlantilla().ConsultarPlantillaYParametrosPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
            Dim listBEPLantilla As List(Of BEPlantilla) = New DAPlantilla().ConsultarPLantillasXServicioGenPago(idServicio, idMoneda)
            If oBEPLantilla IsNot Nothing Then
                For i As Integer = 0 To listBEPLantilla.Count - 1
                    Dim key As String = SPE.EmsambladoComun.ParametrosSistema.Plantilla.PreFijoSeccion & listBEPLantilla(i).CodigoPlantillaTipo.Trim()
                    Dim value As String = GenerarPlantillaToHtml(listBEPLantilla(i), valoresDinamicos)
                    oBEPLantilla.PlantillaHtml = oBEPLantilla.PlantillaHtml.Replace("[" & key & "]", value)
                Next
            End If
            Return GenerarPlantillaToHtml(oBEPLantilla, valoresDinamicos)
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    '************************************************************************************************** 
    ' M�todo          : GenerarPlantillaToHtml
    ' Descripci�n     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 29/05/2012
    ' Parametros_In   : BEPlantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    Public Function GenerarPlantillaToHtml(ByVal oBEPlantilla As BEPlantilla, ByVal valoresDinamicos As Dictionary(Of String, String)) As String
        Dim paramValores As IDictionary(Of String, String) = New Dictionary(Of String, String)()
        Dim strHtml As String = oBEPlantilla.PlantillaHtml

        For Each obeParam As BEPlantillaParametro In oBEPlantilla.ListaParametros
            Select Case obeParam.IdTipoParametro
                Case Plantilla.Parametro.Tipo.Estatico
                    strHtml = strHtml.Replace(obeParam.Etiqueta, obeParam.Valor)
                    If Not (paramValores.ContainsKey(obeParam.Etiqueta)) Then
                        paramValores.Add(obeParam.Etiqueta, obeParam.Valor)
                    Else
                        paramValores(obeParam.Etiqueta) = obeParam.Valor
                    End If
                Case Plantilla.Parametro.Tipo.Dinamico
                    Dim strtmpvalor As String = String.Empty
                    If (valoresDinamicos.ContainsKey(obeParam.FuenteEtiqueta)) Then
                        strtmpvalor = valoresDinamicos(obeParam.FuenteEtiqueta)
                    End If
                    strHtml = strHtml.Replace(obeParam.FuenteEtiqueta, strtmpvalor)
                    If Not (paramValores.ContainsKey(obeParam.FuenteEtiqueta)) Then
                        paramValores.Add(obeParam.FuenteEtiqueta, strtmpvalor)
                    Else
                        paramValores(obeParam.FuenteEtiqueta) = strtmpvalor
                    End If
            End Select
        Next
        Return strHtml
    End Function

    '************************************************************************************************** 
    ' M�todo          : GenerarPlantillaToHtmlByTipo
    ' Descripci�n     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados apartir del IdServicio y Codigo de tipo de PLantilla
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 30/05/2012
    ' Parametros_In   : IdServicio, codTipoPLantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    Public Function GenerarPlantillaToHtmlByTipo(ByVal idServicio As Integer, ByVal codPLantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String
        Dim oBEPlantilla As BEPlantilla
        oBEPlantilla = New BLPlantilla().ConsultarPlantillaYParametrosPorTipoYVariacion(idServicio, codPLantillaTipo, codPlantillaTipoVariacion)
        Dim paramValores As IDictionary(Of String, String) = New Dictionary(Of String, String)()
        If oBEPlantilla Is Nothing Then
            Return String.Empty
        End If
        Dim strHtml As String = oBEPlantilla.PlantillaHtml

        For Each obeParam As BEPlantillaParametro In oBEPlantilla.ListaParametros
            Select Case obeParam.IdTipoParametro
                Case Plantilla.Parametro.Tipo.Estatico
                    strHtml = strHtml.Replace(obeParam.Etiqueta, obeParam.Valor)
                    If Not (paramValores.ContainsKey(obeParam.Etiqueta)) Then
                        paramValores.Add(obeParam.Etiqueta, obeParam.Valor)
                    Else
                        paramValores(obeParam.Etiqueta) = obeParam.Valor
                    End If
                Case Plantilla.Parametro.Tipo.Dinamico
                    Dim strtmpvalor As String = String.Empty
                    If (valoresDinamicos.ContainsKey(obeParam.FuenteEtiqueta)) Then
                        strtmpvalor = valoresDinamicos(obeParam.FuenteEtiqueta)
                    End If
                    strHtml = strHtml.Replace(obeParam.FuenteEtiqueta, strtmpvalor)
                    If Not (paramValores.ContainsKey(obeParam.FuenteEtiqueta)) Then
                        paramValores.Add(obeParam.FuenteEtiqueta, strtmpvalor)
                    Else
                        paramValores(obeParam.FuenteEtiqueta) = strtmpvalor
                    End If
            End Select
        Next
        Return strHtml
    End Function
    ''' <summary>
    ''' M�todo que genera el c�digo HTML para mostrarse en el modal/p�gina "�Qu� es PagoEfectivo?" de los portales.
    ''' </summary>
    ''' <param name="idServicio">Id del Servicio</param>
    ''' <param name="valoresDinamicos">Diccionario con valores din�micos</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ConsultarPlantillasXServicioQueEsPE(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codigoPlantillaTipoVariacion As String) As String
        Dim strPlantillaTipo As String = ""
        Select Case idMoneda
            Case 1
                strPlantillaTipo = ParametrosSistema.Plantilla.Tipo.QueEsPE
            Case 2
                strPlantillaTipo = ParametrosSistema.Plantilla.Tipo.QueEsPE & "DolarAM"
        End Select

        Dim Plantilla As BEPlantilla = New BLPlantilla().ConsultarPlantillaYParametrosPorTipoYVariacion(idServicio, strPlantillaTipo, codigoPlantillaTipoVariacion)
        Dim PlantillasBanco As List(Of BEPlantilla) = New DAPlantilla().ConsultarPlantillasXServicioQueEsPE(idServicio, idMoneda)
        For Each PlantillaBanco As BEPlantilla In PlantillasBanco
            Dim key As String = SPE.EmsambladoComun.ParametrosSistema.Plantilla.PreFijoSeccion & PlantillaBanco.CodigoPlantillaTipo.Trim()
            Dim value As String = GenerarPlantillaToHtml(PlantillaBanco, valoresDinamicos)
            Plantilla.PlantillaHtml = Plantilla.PlantillaHtml.Replace("[" & key & "]", value)
        Next
        Return GenerarPlantillaToHtml(Plantilla, New Dictionary(Of String, String))
    End Function

    Public Function ObtenerSeccionesPlantillaPorServicio(ByVal idServicio As Integer, ByVal idPlantillaFormato As Integer) As List(Of BEPlantillaSeccion)
        Return New DAPlantilla().ObtenerSeccionesPlantillaPorServicio(idServicio, idPlantillaFormato)
    End Function

    Public Function ActualizarServicioSeccion(ByVal lstPlantillaSeccion As List(Of BEPlantillaSeccion), ByVal idUsuario As Integer) As Integer
        Return New DAPlantilla().ActualizarServicioSeccion(lstPlantillaSeccion, idUsuario)
    End Function

End Class
