Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun.ParametrosSistema.WSBancoMensaje.FC
Imports System.Text.RegularExpressions
Imports SPE.Logging

Public Class BLPuntoVenta
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase
    Private ReadOnly _nlogger As ILogger = New Logger()

    Private Shared _MensajesFC As Dictionary(Of String, String) = Nothing

    Private Shared ReadOnly Property MensajesFC() As Dictionary(Of String, String)
        Get
            If (_MensajesFC Is Nothing) Then
                _MensajesFC = New Dictionary(Of String, String)
                _MensajesFC.Add(Mensaje.TransaccionRealizadaExito, "Transacci�n realizada con �xito")
                _MensajesFC.Add(Mensaje.NoSePudoRealizarTransaccion, "No se pudo realizar la transacci�n")
                _MensajesFC.Add(Mensaje.TramaEnviadaInvalida, "Trama enviada inv�lida")
                _MensajesFC.Add(Mensaje.CodigoPagoNoExiste, "C�digo de pago no existe")
                _MensajesFC.Add(Mensaje.CodigoConEstadoPagado, "C�digo con estado pagado")
                _MensajesFC.Add(Mensaje.CodigoConEstadoExpirado, "C�digo con estado expirado")
                _MensajesFC.Add(Mensaje.NoSePudoRealizarExtorno, "No se pudo realizar el extorno")
                _MensajesFC.Add(Mensaje.CodigoExtornarNoExiste, "C�digo a extornar no existe")
                _MensajesFC.Add(Mensaje.CodigoExtornarExpirado, "C�digo a extornar ha expirado")
                _MensajesFC.Add(Mensaje.MontoOMonedaNoCoincide, "El monto y moneda recibidos no coinciden con el monto y moneda del CIP")
                '_MensajesFC.Add(Mensaje.CuentaNoExiste, "La cuenta no existe, o el servicio no coincide")
                '_MensajesFC.Add(Mensaje.CuentaNoHabilitada, "Cuenta no habilitada o usuario no habilitado para dinero virtual")
                '_MensajesFC.Add(Mensaje.MonedaNoCoincide, "El tipo de moneda de la cuenta no coincide con la moneda ingresada")
                '_MensajesFC.Add(Mensaje.CuentaExiste, "Cuenta Encontrada")
                '_MensajesFC.Add(Mensaje.Pago_Realizado, "Pago Realizado")
                '_MensajesFC.Add(Mensaje.Pago_no_realizado, "Pago no Realizado")
                '_MensajesFC.Add(Mensaje.Extorno_Realizado, "Extorno Realizado")
                '_MensajesFC.Add(Mensaje.Extorno_no_Realizado, "Extorno no Realizado")
                'TIEMPO MAXIMO DE EXPIRACION
                _MensajesFC.Add(Mensaje.NoSePermiteExtornar, "Tiempo limite extorno excedido")
            End If
            Return _MensajesFC
        End Get
    End Property

    Public Function ConsultarFC(ByVal request As BEFCConsultarRequest) As BEFCResponse(Of BEFCConsultarResponse)
        Dim response As New BEFCResponse(Of BEFCConsultarResponse)
        response.ObjFC = New BEFCConsultarResponse
        'Dim TmpRespuesta(33) As String
        'Dim objOrdenPago As New BEOrdenPago()
        Try
            If request.CIP.Substring(0, 2) <> "99" Then


                If (String.IsNullOrEmpty(request.CodPuntoVenta) Or request.CodPuntoVenta.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroSerieTerminal) Or request.NroSerieTerminal.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroOperacionFullCarga) Or request.NroOperacionFullCarga.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                    With response.ObjFC
                        .CodResultado = Mensaje.TramaEnviadaInvalida
                        .MensajeResultado = MensajesFC(.CodResultado)
                    End With
                    Return response
                End If
                Dim oBEOrdenPago As BEOrdenPago = Nothing
                Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    Dim obeOrdenPagoReq As New BEOrdenPago()
                    With obeOrdenPagoReq
                        .NumeroOrdenPago = request.CIP
                        .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
                        .CodigoServicio = request.CodServicio
                    End With
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(obeOrdenPagoReq)
                End Using
                If oBEOrdenPago Is Nothing Then
                    response.ObjFC.CodResultado = Mensaje.CodigoPagoNoExiste
                ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Eliminado) Then
                    response.ObjFC.CodResultado = Mensaje.CodigoPagoNoExiste
                ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Cancelada) Then
                    response.ObjFC.CodResultado = Mensaje.CodigoConEstadoPagado
                ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Expirado Or _
                    oBEOrdenPago.CipListoParaExpirar) Then
                    response.ObjFC.CodResultado = Mensaje.CodigoConEstadoExpirado
                Else

                    With response.ObjFC
                        .CIP = oBEOrdenPago.NumeroOrdenPago
                        .CodResultado = Mensaje.TransaccionRealizadaExito
                        .Monto = oBEOrdenPago.Total
                        .CodMoneda = oBEOrdenPago.codMonedaBanco
                        .ConceptoPago = oBEOrdenPago.ConceptoPago
                        .FechaVencimiento = oBEOrdenPago.FechaVencimiento.ToString("yyyy-MM-dd")
                        .CodServicio = request.CodServicio
                    End With
                End If
                response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)
            Else ' Se busca dentro de las cuentas de dinero virtual
                Dim dv As New DADineroVirtual()
                Dim cuenta As New BERecarga()

                cuenta.xCIPRecarga = request.CIP.ToString()
                cuenta.xCodBancoPE = Conciliacion.CodigoBancos.FullCarga
                ' Validar Cuenta
                cuenta = dv.ConsultarSolicitudRecargaComun(cuenta.xCIPRecarga)
                '----------------------------------------------------------
                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    response.ObjFC.CIP = "0"
                    'response.ObjFC.CodResultado = Mensaje.CuentaNoExiste
                    response.ObjFC.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                    response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)
                Else

                    'If cuenta.EquivalenciaBancaria = "00" & request.NroSerieTerminal Then 'existe la cuenta en la moneda solicitada
                    If cuenta.iEstado = 1 Then

                        With response.ObjFC
                            .CIP = cuenta.xCIPRecarga
                            '.CodResultado = Mensaje.CuentaExiste
                            .CodResultado = Mensaje.TransaccionRealizadaExito
                            .MensajeResultado = MensajesFC(response.ObjFC.CodResultado)
                            .Monto = cuenta.mMonto
                            .CodMoneda = cuenta.iMoneda
                            .ConceptoPago = "Cuenta Consultada"
                            .FechaVencimiento = cuenta.dCreado.ToString("yyyy-MM-dd")
                            .CodServicio = request.CodServicio
                        End With


                    Else
                        'Cuenta no habilitada o usuario no habilitado para dinero virtual"
                        response.ObjFC.CIP = "0"
                        'response.ObjFC.CodResultado = Mensaje.CuentaNoExiste
                        response.ObjFC.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                        response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)
                    End If
                End If

            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    Public Function CancelarFC(ByVal request As BEFCCancelarRequest) As BEFCResponse(Of BEFCCancelarResponse)
        Dim response As New BEFCResponse(Of BEFCCancelarResponse)
        response.ObjFC = New BEFCCancelarResponse
        Try
            If request.CIP.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                If (String.IsNullOrEmpty(request.CodPuntoVenta) Or request.CodPuntoVenta.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroSerieTerminal) Or request.NroSerieTerminal.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroOperacionFullCarga) Or request.NroOperacionFullCarga.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodMedioPago) Or request.CodMedioPago.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.Monto) Or request.Monto.Trim().Length = 0) Or _
                    Not Regex.IsMatch(request.Monto, "^\d{1,11}\.\d{2}$") Or _
                    (String.IsNullOrEmpty(request.CodMoneda) Or request.CodMoneda.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                    With response.ObjFC
                        .CodResultado = Mensaje.TramaEnviadaInvalida
                        .MensajeResultado = MensajesFC(.CodResultado)
                    End With
                    Return response
                End If
                Dim oBEMovimiento As New BEMovimiento
                With oBEMovimiento
                    .NumeroOrdenPago = request.CIP
                    .CodMonedaBanco = request.CodMoneda
                    .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                    .NumeroOperacion = request.NroOperacionFullCarga
                    .CodigoPuntoVenta = request.CodPuntoVenta
                    .NumeroSerieTerminal = request.NroSerieTerminal
                    .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
                    .CodigoMedioPago = request.CodMedioPago
                    .CodigoServicio = request.CodServicio
                End With
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted
                'Using transaccion As New TransactionScope 'Comentado el 06/04
                Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    Dim IdMovimiento As Integer = New BLAgenciaBancaria().PagarDesdeBancoComun(oBEMovimiento, Nothing, Nothing)
                    If IdMovimiento > 0 Then
                        With response.ObjFC
                            .CodMovimiento = IdMovimiento
                            .CodResultado = Mensaje.TransaccionRealizadaExito
                            .CodServicio = request.CodServicio
                        End With
                        'transaccion.Complete() 'Comentado el 06/04
                    ElseIf IdMovimiento = -8 Then
                        response.ObjFC.CodResultado = Mensaje.CodigoPagoNoExiste
                    ElseIf IdMovimiento = -4 Then
                        response.ObjFC.CodResultado = Mensaje.CodigoConEstadoPagado
                    ElseIf IdMovimiento = -1 Then
                        response.ObjFC.CodResultado = Mensaje.CodigoConEstadoExpirado
                    ElseIf IdMovimiento = -2 Or IdMovimiento = -3 Then
                        response.ObjFC.CodResultado = Mensaje.MontoOMonedaNoCoincide
                    Else
                        response.ObjFC.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                    End If
                End Using
                response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)
                'End Using 'Comentado el 06/04
            Else 'RECARGANDO DINERO A UNA CUENTA

                Dim dv As New DADineroVirtual()
                Dim bldv As New BLDineroVirtual()
                'Dim cuentarequest As New BEMovimientoCuenta()
                Dim oBEMovimiento As New BEMovimiento
                With oBEMovimiento
                    .NumeroOrdenPago = request.CIP
                    .CodMonedaBanco = request.CodMoneda
                    .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                    .NumeroOperacion = request.NroOperacionFullCarga
                    .CodigoPuntoVenta = request.CodPuntoVenta
                    .NumeroSerieTerminal = request.NroSerieTerminal
                    .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
                    .CodigoMedioPago = request.CodMedioPago
                    .CodigoServicio = request.CodServicio
                End With
                Dim cuenta As New BERecarga()
                cuenta.xCIPRecarga = request.CIP
                'Validar Cuenta
                cuenta = dv.ConsultarSolicitudRecargaComun(cuenta.xCIPRecarga)
                cuenta.xNumOperacionEntidad = request.NroOperacionFullCarga

                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    'BCP.Mensaje.Cuenta_no_encontrada()
                    Throw New Exception("Cuenta_no_encontrada")
                Else
                    If cuenta.iMoneda = request.CodMoneda Then 'obeMovimiento.MonedaAgencia  existe la cuenta en la moneda solicitada

                        'valida que tenga activo el monedero y la cuenta
                        If cuenta.iEstado = 1 Then
                            'Valida si el monto a Recargar es el mismo establecido por el Cliente
                            If cuenta.mMonto = oBEMovimiento.Monto Then
                                'Dim IdTransaccion As Long = dv.RecargarCuenta(cuenta, request.CodMedioPago)
                                Dim Respuesta As Long = dv.RecargarCuentaUnibancaComun(cuenta)

                                If Respuesta <> -2 Then
                                    If Respuesta > 0 Then
                                        With response.ObjFC
                                            .CodMovimiento = Respuesta
                                            .CodResultado = Mensaje.TransaccionRealizadaExito
                                            .CodServicio = request.CodServicio
                                        End With
                                        response.ObjFC.MensajeResultado = "Cuenta Recargada"
                                    Else
                                        response.ObjFC.MensajeResultado = "Transaccion Fallida"
                                    End If
                                Else
                                    Throw New Exception("No se pudo registrar la operacion contactese con el Administrador.")
                                End If
                            Else
                                Throw New Exception("El Monto no coincide con el especificado para la cuenta.")
                            End If
                        Else
                            Throw New Exception("La cuenta no esta activa")
                        End If
                    Else
                        Throw New Exception("Moneda no encontrada para la cuenta")
                    End If
                End If
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    Public Function AnularFC(ByVal request As BEFCAnularRequest) As BEFCResponse(Of BEFCAnularResponse)
        Dim response As New BEFCResponse(Of BEFCAnularResponse)
        response.ObjFC = New BEFCAnularResponse
        Dim IdMovimiento As Integer
        Try
            If (String.IsNullOrEmpty(request.CodPuntoVenta) Or request.CodPuntoVenta.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.NroSerieTerminal) Or request.NroSerieTerminal.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.NroOperacionFullCarga) Or request.NroOperacionFullCarga.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.NroOperacionFullCargaPago) Or request.NroOperacionFullCargaPago.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.CodMedioPago) Or request.CodMedioPago.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.Monto) Or request.Monto.Trim().Length = 0) Or _
                  Not Regex.IsMatch(request.Monto, "^\d{1,11}\.\d{2}$") Or _
                  (String.IsNullOrEmpty(request.CodMoneda) Or request.CodMoneda.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                With response.ObjFC
                    .CodResultado = Mensaje.TramaEnviadaInvalida
                    .MensajeResultado = MensajesFC(.CodResultado)
                    .CodServicio = request.CodServicio
                End With
                Return response
            End If
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.CIP
                .NumeroOperacion = request.NroOperacionFullCarga
                .NumeroOperacionPago = request.NroOperacionFullCargaPago
                .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
                .CodMonedaBanco = request.CodMoneda
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                .CodigoPuntoVenta = request.CodPuntoVenta
                .NumeroSerieTerminal = request.NroSerieTerminal
                .MedioPago = request.CodMedioPago
                .CodigoServicio = request.CodServicio
            End With

            If request.CIP.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted
                Using transaccion As New TransactionScope
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        IdMovimiento = oBLAgenciaBancaria.AnularDesdeBancoComun(oBEMovimiento)

                    End Using
                    transaccion.Complete()

                End Using

                If IdMovimiento > 0 Then
                    With response.ObjFC
                        .CodMovimiento = IdMovimiento
                        .CodResultado = Mensaje.TransaccionRealizadaExito
                        .CodServicio = request.CodServicio
                    End With

                ElseIf IdMovimiento = -11 Then
                    response.ObjFC.CodResultado = Mensaje.CodigoExtornarNoExiste
                ElseIf IdMovimiento = -17 Then
                    response.ObjFC.CodResultado = Mensaje.MontoOMonedaNoCoincide
                    'ElseIf IdMovimiento = -8 Then
                    '    response.CodResultado = Mensaje.CodigoExtornarExpirado
                ElseIf IdMovimiento = -37 Then 'TIEMPO MAXIMO DE EXPIRACION
                    response.ObjFC.CodResultado = Mensaje.NoSePermiteExtornar
                Else
                    response.ObjFC.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                End If
                response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)
            Else     'Anulando una recarga de dinero

                Dim dv As New DADineroVirtual()
                Dim cuenta As New BERecarga()
                cuenta.xCIPRecarga = request.CIP
                ' Validar Cuenta
                cuenta = dv.ConsultarSolicitudRecargaComun(cuenta.xCIPRecarga)

                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    'BCP.Mensaje.Cuenta_no_encontrada()
                    response.ObjFC.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                    'Throw New Exception("Cuenta_no_encontrada")
                Else
                    'valida que tenga activo el monedero y la cuenta
                    If cuenta.iEstado = 2 Then
                        Dim Ext As New DADineroVirtual
                        Dim Respuesta As Integer
                        Respuesta = Ext.AnularRecargaUnibancaComun(cuenta)
                        If Respuesta > 0 Then
                            With response.ObjFC
                                .CodMovimiento = Respuesta
                                .CodResultado = Mensaje.TransaccionRealizadaExito
                                .CodServicio = request.CodServicio
                            End With
                            response.ObjFC.MensajeResultado = "Extorno realizado"
                        Else
                            Throw New Exception("Sucedio un error interno en la BD")
                        End If
                    Else
                        Throw New Exception("La cuenta o el usuario no estan activos")
                    End If
                End If
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    'LIFE!
    Public Function ConsultarLF(ByVal request As BELFConsultarRequest) As BELFResponse(Of BELFConsultarResponse)
        Dim response As New BELFResponse(Of BELFConsultarResponse)
        response.ObjFC = New BELFConsultarResponse
        'Dim TmpRespuesta(33) As String
        'Dim objOrdenPago As New BEOrdenPago()
        Try
            If ((String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0)) Then
                With response.ObjFC
                    .CodResultado = Mensaje.TramaEnviadaInvalida
                    .MensajeResultado = MensajesFC(.CodResultado)
                End With
                Return response
            End If
            Dim oBEOrdenPago As BEOrdenPago = Nothing
            Using oDAAgenciaBancaria As New DAAgenciaBancaria
                Dim obeOrdenPagoReq As New BEOrdenPago()
                With obeOrdenPagoReq
                    .NumeroOrdenPago = request.CIP
                    .CodigoBanco = Conciliacion.CodigoBancos.Life
                    .CodigoServicio = request.CodServicio
                End With
                oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(obeOrdenPagoReq)
            End Using
            If oBEOrdenPago Is Nothing Then
                response.ObjFC.CodResultado = Mensaje.CodigoPagoNoExiste
            ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Eliminado) Then
                response.ObjFC.CodResultado = Mensaje.CodigoPagoNoExiste
            ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Cancelada) Then
                response.ObjFC.CodResultado = Mensaje.CodigoConEstadoPagado
            ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Expirado Or _
                oBEOrdenPago.CipListoParaExpirar) Then
                response.ObjFC.CodResultado = Mensaje.CodigoConEstadoExpirado
            Else

                With response.ObjFC
                    .CIP = oBEOrdenPago.NumeroOrdenPago
                    .CodResultado = Mensaje.TransaccionRealizadaExito
                    .Monto = oBEOrdenPago.Total
                    .CodMoneda = oBEOrdenPago.codMonedaBanco
                    .Dni = oBEOrdenPago.Dni
                    .FechaVencimiento = oBEOrdenPago.FechaVencimiento.ToString("yyyy-MM-dd")
                    .CodServicio = request.CodServicio
                End With
            End If
            response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    Public Function CancelarLF(ByVal request As BELFCancelarRequest) As BELFResponse(Of BELFCancelarResponse)
        Dim response As New BELFResponse(Of BELFCancelarResponse)
        response.ObjFC = New BELFCancelarResponse
        Try
            'PAGANDO UN CIP
            If (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.Monto) Or request.Monto.Trim().Length = 0) Or _
                    Not Regex.IsMatch(request.Monto, "^\d{1,11}$") Or _
                    (String.IsNullOrEmpty(request.CodMoneda) Or request.CodMoneda.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                With response.ObjFC
                    .CodResultado = Mensaje.TramaEnviadaInvalida
                    .MensajeResultado = MensajesFC(.CodResultado)
                End With
                Return response
            End If
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.CIP
                .CodMonedaBanco = request.CodMoneda
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                .CodigoBanco = Conciliacion.CodigoBancos.Life
                .CodigoServicio = request.CodServicio
            End With
            Dim options As TransactionOptions = New TransactionOptions()
            options.IsolationLevel = IsolationLevel.ReadCommitted
            'Using transaccion As New TransactionScope 'Comentado el 06/04
            Using oDAAgenciaBancaria As New DAAgenciaBancaria
                Dim IdMovimiento As Integer = New BLAgenciaBancaria().PagarDesdeBancoComun(oBEMovimiento, Nothing, Nothing)
                If IdMovimiento > 0 Then
                    With response.ObjFC
                        .CodMovimiento = IdMovimiento
                        .CodResultado = Mensaje.TransaccionRealizadaExito
                        .CodServicio = request.CodServicio
                    End With
                    'transaccion.Complete() 'Comentado el 06/04
                ElseIf IdMovimiento = -8 Then
                    response.ObjFC.CodResultado = Mensaje.CodigoPagoNoExiste
                ElseIf IdMovimiento = -4 Then
                    response.ObjFC.CodResultado = Mensaje.CodigoConEstadoPagado
                ElseIf IdMovimiento = -1 Then
                    response.ObjFC.CodResultado = Mensaje.CodigoConEstadoExpirado
                ElseIf IdMovimiento = -2 Or IdMovimiento = -3 Then
                    response.ObjFC.CodResultado = Mensaje.MontoOMonedaNoCoincide
                Else
                    response.ObjFC.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                End If
            End Using
            response.ObjFC.MensajeResultado = MensajesFC(response.ObjFC.CodResultado)

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    'M MoMo
    Private Shared _MensajesMoMo As Dictionary(Of String, String) = Nothing

    Private Shared ReadOnly Property MensajesMoMo() As Dictionary(Of String, String)
        Get
            If (_MensajesMoMo Is Nothing) Then
                _MensajesMoMo = New Dictionary(Of String, String)
                _MensajesMoMo.Add(Mensaje.TransaccionRealizadaExito, "Transacci�n realizada con �xito")
                _MensajesMoMo.Add(Mensaje.NoSePudoRealizarTransaccion, "No se pudo realizar la transacci�n")
                _MensajesMoMo.Add(Mensaje.TramaEnviadaInvalida, "Trama enviada inv�lida")
                _MensajesMoMo.Add(Mensaje.CodigoPagoNoExiste, "C�digo de pago no existe")
                _MensajesMoMo.Add(Mensaje.CodigoConEstadoPagado, "C�digo con estado pagado")
                _MensajesMoMo.Add(Mensaje.CodigoConEstadoExpirado, "C�digo con estado expirado")
                _MensajesMoMo.Add(Mensaje.NoSePudoRealizarExtorno, "No se pudo realizar el extorno")
                _MensajesMoMo.Add(Mensaje.CodigoExtornarNoExiste, "C�digo a extornar no existe")
                _MensajesMoMo.Add(Mensaje.CodigoExtornarExpirado, "C�digo a extornar ha expirado")
                _MensajesMoMo.Add(Mensaje.MontoOMonedaNoCoincide, "El monto y moneda recibidos no coinciden con el monto y moneda del CIP")
                '_MensajesMoMo.Add(Mensaje.CuentaNoExiste, "La cuenta no existe, o el servicio no coincide")
                '_MensajesMoMo.Add(Mensaje.CuentaNoHabilitada, "Cuenta no habilitada o usuario no habilitado para dinero virtual")
                '_MensajesMoMo.Add(Mensaje.MonedaNoCoincide, "El tipo de moneda de la cuenta no coincide con la moneda ingresada")
                '_MensajesMoMo.Add(Mensaje.CuentaExiste, "Cuenta Encontrada")
                '_MensajesMoMo.Add(Mensaje.Pago_Realizado, "Pago Realizado")
                '_MensajesMoMo.Add(Mensaje.Pago_no_realizado, "Pago no Realizado")
                '_MensajesMoMo.Add(Mensaje.Extorno_Realizado, "Extorno Realizado")
                '_MensajesMoMo.Add(Mensaje.Extorno_no_Realizado, "Extorno no Realizado")
            End If
            Return _MensajesMoMo
        End Get
    End Property

    Public Function ConsultarMoMo(ByVal request As BEMoMoConsultarRequest) As BEMoMoResponse(Of BEMoMoConsultarResponse)
        Dim response As New BEMoMoResponse(Of BEMoMoConsultarResponse)
        response.ObjMoMo = New BEMoMoConsultarResponse
        Try
            If request.CIP.Substring(0, 2) <> "99" Then
                If (String.IsNullOrEmpty(request.CodPuntoVenta) Or request.CodPuntoVenta.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroSerieTerminal) Or request.NroSerieTerminal.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroOperacionMoMo) Or request.NroOperacionMoMo.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                    With response.ObjMoMo
                        .CodResultado = Mensaje.TramaEnviadaInvalida
                        .MensajeResultado = MensajesMoMo(.CodResultado)
                    End With
                    Return response
                End If
                Dim oBEOrdenPago As BEOrdenPago = Nothing
                Using oDAAgenciaBancaria As New DAAgenciaBancaria
                    Dim obeOrdenPagoReq As New BEOrdenPago()
                    With obeOrdenPagoReq
                        .NumeroOrdenPago = request.CIP
                        .CodigoBanco = Conciliacion.CodigoBancos.MoMo
                        .CodigoServicio = request.CodServicio
                    End With
                    oBEOrdenPago = oDAAgenciaBancaria.ConsultarCIPDesdeBanco(obeOrdenPagoReq)
                End Using
                If oBEOrdenPago Is Nothing Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoPagoNoExiste
                ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Eliminado) Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoPagoNoExiste
                ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Cancelada) Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoConEstadoPagado
                ElseIf (oBEOrdenPago.IdEstado = EstadoOrdenPago.Expirado Or _
                    oBEOrdenPago.CipListoParaExpirar) Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoConEstadoExpirado
                Else
                    With response.ObjMoMo
                        .CIP = oBEOrdenPago.NumeroOrdenPago
                        .CodResultado = Mensaje.TransaccionRealizadaExito
                        .Monto = oBEOrdenPago.Total
                        .CodMoneda = oBEOrdenPago.codMonedaBanco
                        .ConceptoPago = oBEOrdenPago.ConceptoPago
                        .FechaVencimiento = oBEOrdenPago.FechaVencimiento.ToString("yyyy-MM-dd")
                        .CodServicio = request.CodServicio
                    End With
                End If
                response.ObjMoMo.MensajeResultado = MensajesMoMo(response.ObjMoMo.CodResultado)
            Else ' Se busca dentro de las cuentas de dinero virtual
                Dim dv As New DADineroVirtual()
                Dim cuenta As New BECuentaDineroVirtual()
                Dim CdBco As String
                CdBco = Conciliacion.CodigoBancos.MoMo
                cuenta.Numero = request.CIP.ToString()
                ' Validar Cuenta
                cuenta = dv.ConsultarCuentaDineroByNumeroWS(cuenta, request.CodServicio, CdBco)
                '----------------------------------------------------------
                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    response.ObjMoMo.CIP = "0"
                    response.ObjMoMo.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                    response.ObjMoMo.MensajeResultado = MensajesMoMo(response.ObjMoMo.CodResultado)
                Else

                    'If cuenta.EquivalenciaBancaria = "00" & request.NroSerieTerminal Then 'existe la cuenta en la moneda solicitada
                    If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then

                        With response.ObjMoMo
                            .CIP = cuenta.Numero
                            '.CodResultado = Mensaje.CuentaExiste
                            .CodResultado = Mensaje.TransaccionRealizadaExito
                            .MensajeResultado = MensajesFC(response.ObjMoMo.CodResultado)
                            .Monto = cuenta.MontoRecarga
                            .CodMoneda = cuenta.IdMoneda
                            .ConceptoPago = "Cuenta Consultada"
                            .FechaVencimiento = cuenta.FechaAprobacion.ToString("yyyy-MM-dd")
                            .CodServicio = request.CodServicio
                        End With
                    Else
                        'Cuenta no habilitada o usuario no habilitado para dinero virtual"
                        response.ObjMoMo.CIP = "0"
                        response.ObjMoMo.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                        response.ObjMoMo.MensajeResultado = MensajesMoMo(response.ObjMoMo.CodResultado)
                    End If
                End If
            End If

        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    Public Function CancelarMoMo(ByVal request As BEMoMoCancelarRequest) As BEMoMoResponse(Of BEMoMoCancelarResponse)
        Dim response As New BEMoMoResponse(Of BEMoMoCancelarResponse)
        response.ObjMoMo = New BEMoMoCancelarResponse
        Dim IdMovimiento As Integer
        Try
            If request.CIP.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                If (String.IsNullOrEmpty(request.CodPuntoVenta) Or request.CodPuntoVenta.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroSerieTerminal) Or request.NroSerieTerminal.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.NroOperacionMoMo) Or request.NroOperacionMoMo.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodMedioPago) Or request.CodMedioPago.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.Monto) Or request.Monto.Trim().Length = 0) Or _
                    Not Regex.IsMatch(request.Monto, "^\d{1,11}\.\d{2}$") Or _
                    (String.IsNullOrEmpty(request.CodMoneda) Or request.CodMoneda.Trim().Length = 0) Or _
                    (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                    With response.ObjMoMo
                        .CodResultado = Mensaje.TramaEnviadaInvalida
                        .MensajeResultado = MensajesMoMo(.CodResultado)
                    End With
                    Return response
                End If
                Dim oBEMovimiento As New BEMovimiento
                With oBEMovimiento
                    .NumeroOrdenPago = request.CIP
                    .CodMonedaBanco = request.CodMoneda
                    .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                    .NumeroOperacion = request.NroOperacionMoMo
                    .CodigoPuntoVenta = request.CodPuntoVenta
                    .NumeroSerieTerminal = request.NroSerieTerminal
                    .CodigoBanco = Conciliacion.CodigoBancos.MoMo
                    .CodigoMedioPago = request.CodMedioPago
                    .CodigoServicio = request.CodServicio
                End With
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted
                Using transaccion As New TransactionScope
                    Using oDAAgenciaBancaria As New DAAgenciaBancaria
                        IdMovimiento = New BLAgenciaBancaria().PagarDesdeBancoComun(oBEMovimiento, Nothing, Nothing)
                    End Using
                    transaccion.Complete()
                End Using
                If IdMovimiento > 0 Then
                    With response.ObjMoMo
                        .CodMovimiento = IdMovimiento
                        .CodResultado = Mensaje.TransaccionRealizadaExito
                        .CodServicio = request.CodServicio
                    End With

                ElseIf IdMovimiento = -8 Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoPagoNoExiste
                ElseIf IdMovimiento = -4 Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoConEstadoPagado
                ElseIf IdMovimiento = -1 Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoConEstadoExpirado
                ElseIf IdMovimiento = -2 Or IdMovimiento = -3 Then
                    response.ObjMoMo.CodResultado = Mensaje.MontoOMonedaNoCoincide
                Else
                    response.ObjMoMo.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                End If
                response.ObjMoMo.MensajeResultado = MensajesFC(response.ObjMoMo.CodResultado)


            Else 'RECARGANDO DINERO A UNA CUENTA

                Dim dv As New DADineroVirtual()
                Dim bldv As New BLDineroVirtual()
                'Dim cuentarequest As New BEMovimientoCuenta()
                Dim oBEMovimiento As New BEMovimiento
                With oBEMovimiento
                    .NumeroOrdenPago = request.CIP
                    .CodMonedaBanco = request.CodMoneda
                    .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                    .NumeroOperacion = request.NroOperacionMoMo
                    .CodigoPuntoVenta = request.CodPuntoVenta
                    .NumeroSerieTerminal = request.NroSerieTerminal
                    .CodigoBanco = Conciliacion.CodigoBancos.MoMo
                    .CodigoMedioPago = request.CodMedioPago
                    .CodigoServicio = request.CodServicio
                End With
                Dim cuenta As New BECuentaDineroVirtual()
                cuenta.Numero = request.CIP
                'Validar Cuenta
                cuenta = dv.ConsultarCuentaDineroVirtualUsuarioByNumero(cuenta)

                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    'BCP.Mensaje.Cuenta_no_encontrada()
                    Throw New Exception("Cuenta_no_encontrada")
                Else
                    If cuenta.EquivalenciaBancaria = "00" & request.CodMoneda Then 'obeMovimiento.MonedaAgencia  existe la cuenta en la moneda solicitada

                        'valida que tenga activo el monedero y la cuenta
                        If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                            'Valida si el monto a Recargar es el mismo establecido por el Cliente
                            If cuenta.MontoRecarga = oBEMovimiento.Monto Then
                                'Dim IdTransaccion As Long = dv.RecargarCuenta(cuenta, request.CodMedioPago)
                                Dim Respuesta As Long = bldv.OperacionCuentaGenerico(cuenta, oBEMovimiento, request.CodServicio)
                                If Respuesta <> -1 Then
                                    If Respuesta <> -2 Then
                                        If Respuesta > 0 Then
                                            'Enviar Correo
                                            Dim oBLEmail As New BLEmail
                                            oBLEmail.EnviarCorreoCuentaRecarga(cuenta)
                                            With response.ObjMoMo
                                                .CodMovimiento = Respuesta
                                                .CodResultado = Mensaje.TransaccionRealizadaExito
                                                .CodServicio = request.CodServicio
                                            End With
                                            Dim control As New BLEmail
                                            control.EnviarCorreoCuentaRecarga(cuenta)
                                            response.ObjMoMo.MensajeResultado = "Cuenta Recargada"
                                        Else
                                            response.ObjMoMo.MensajeResultado = "Transaccion Fallida"
                                        End If
                                    Else
                                        Throw New Exception("El monto ingresado est� fuera de los rangos establecidos por el Administrador.")
                                    End If
                                Else
                                    Throw New Exception("El codigo de servicio BancoMonedero no coincide.")
                                End If
                            Else
                                Throw New Exception("El Monto no coincide con el especificado para la cuenta.")
                            End If
                        Else
                            Throw New Exception("La cuenta no esta activa")
                        End If
                    Else
                        Throw New Exception("Moneda no encontrada para la cuenta")
                    End If
                End If
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function

    Public Function AnularMoMo(ByVal request As BEMoMoAnularRequest) As BEMoMoResponse(Of BEMoMoAnularResponse)
        Dim response As New BEMoMoResponse(Of BEMoMoAnularResponse)
        response.ObjMoMo = New BEMoMoAnularResponse
        Dim IdMovimiento As Integer
        Try
            If (String.IsNullOrEmpty(request.CodPuntoVenta) Or request.CodPuntoVenta.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.NroSerieTerminal) Or request.NroSerieTerminal.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.NroOperacionMoMo) Or request.NroOperacionMoMo.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.NroOperacionMoMoPago) Or request.NroOperacionMoMoPago.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.CodMedioPago) Or request.CodMedioPago.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.CIP) Or request.CIP.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.Monto) Or request.Monto.Trim().Length = 0) Or _
                  Not Regex.IsMatch(request.Monto, "^\d{1,11}\.\d{2}$") Or _
                  (String.IsNullOrEmpty(request.CodMoneda) Or request.CodMoneda.Trim().Length = 0) Or _
                  (String.IsNullOrEmpty(request.CodServicio) Or request.CodServicio.Trim().Length = 0) Then
                With response.ObjMoMo
                    .CodResultado = Mensaje.TramaEnviadaInvalida
                    .MensajeResultado = MensajesMoMo(.CodResultado)
                    .CodServicio = request.CodServicio
                End With
                Return response
            End If
            Dim oBEMovimiento As New BEMovimiento
            With oBEMovimiento
                .NumeroOrdenPago = request.CIP
                .NumeroOperacion = request.NroOperacionMoMo
                .NumeroOperacionPago = request.NroOperacionMoMoPago
                .CodigoBanco = Conciliacion.CodigoBancos.MoMo
                .CodMonedaBanco = request.CodMoneda
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(request.Monto)
                .CodigoPuntoVenta = request.CodPuntoVenta
                .NumeroSerieTerminal = request.NroSerieTerminal
                .MedioPago = request.CodMedioPago
                .CodigoServicio = request.CodServicio
            End With

            If request.CIP.Substring(0, 2) <> "99" Then 'PAGANDO UN CIP
                Dim options As TransactionOptions = New TransactionOptions()
                options.IsolationLevel = IsolationLevel.ReadCommitted
                Using transaccion As New TransactionScope
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        IdMovimiento = oBLAgenciaBancaria.AnularDesdeBancoComun(oBEMovimiento)

                    End Using
                    transaccion.Complete()
                End Using
                If IdMovimiento > 0 Then
                    With response.ObjMoMo
                        .CodMovimiento = IdMovimiento
                        .CodResultado = Mensaje.TransaccionRealizadaExito
                        .CodServicio = request.CodServicio
                    End With

                ElseIf IdMovimiento = -11 Then
                    response.ObjMoMo.CodResultado = Mensaje.CodigoExtornarNoExiste
                ElseIf IdMovimiento = -17 Then
                    response.ObjMoMo.CodResultado = Mensaje.MontoOMonedaNoCoincide
                    'ElseIf IdMovimiento = -8 Then
                    '    response.CodResultado = Mensaje.CodigoExtornarExpirado
                Else
                    response.ObjMoMo.CodResultado = Mensaje.NoSePudoRealizarTransaccion
                End If
                response.ObjMoMo.MensajeResultado = MensajesFC(response.ObjMoMo.CodResultado)
            Else     'Anulando una recarga de dinero

                Dim dv As New DADineroVirtual()
                Dim cuenta As New BECuentaDineroVirtual()
                cuenta.Numero = request.CIP
                ' Validar Cuenta
                cuenta = dv.ConsultarCuentaDineroVirtualUsuarioByNumero(cuenta)

                If cuenta Is Nothing Then ' si la cuenta es nothing la cuenta no existe
                    'BCP.Mensaje.Cuenta_no_encontrada()
                    Throw New Exception("Cuenta_no_encontrada")
                Else

                    'valida que tenga activo el monedero y la cuenta
                    If cuenta.IsUserMonedero = 1 And cuenta.IdEstado = EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                        Dim Ext As New BLAgenciaBancaria
                        Dim Respuesta As Integer
                        Respuesta = Ext.ExtornarCuentaCommun(cuenta, oBEMovimiento)
                        If Respuesta > 0 Then
                            With response.ObjMoMo
                                .CodMovimiento = Respuesta
                                .CodResultado = Mensaje.TransaccionRealizadaExito
                                .CodServicio = request.CodServicio
                            End With
                            'Enviar Correo
                            Dim control As New BLEmail
                            control.EnviarCorreoCuentaExtorno(cuenta)
                            response.ObjMoMo.MensajeResultado = "Extorno realizado"
                        Else
                            Throw New Exception("Sucedio un error interno en la BD")
                        End If
                    Else
                        Throw New Exception("La cuenta o el usuario no estan activos")
                    End If
                End If
            End If
        Catch ex As Exception
            _nlogger.Error(ex, ex.Message)
            Throw ex
        End Try
        Return response
    End Function
End Class
