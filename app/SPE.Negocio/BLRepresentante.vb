Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.AccesoDatos
Imports SPE.Entidades

Public Class BLRepresentante
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase

        Return New SPE.AccesoDatos.DARepresentante

    End Function

    Property oDaRepresentante() As SPE.AccesoDatos.DARepresentante
        Get
            Return CType(DataAccessObject, SPE.AccesoDatos.DARepresentante)
        End Get
        Set(ByVal value As SPE.AccesoDatos.DARepresentante)

        End Set
    End Property

    Private Function CodigoRegistro() As String

        Return Guid.NewGuid.ToString()

    End Function

    '************************************************************************************************** 
    ' M�todo          : InsertRecord
    ' Descripci�n     : Registro de un Representante
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia cargada de la entidad BusinessEntityBase
    ' Parametros_Out  : resultado de la transaccion
    ''**************************************************************************************************
    ' Usuario Act.    : Jonathan Bastidas - jonthan.bastidas@ec.pe
    ' Descripci�n     : Se quito la validaci�n del usuario existente, es decir si un usuario ya existe
    '                   con otro rol puede registrarse como representante normalmente.
    ''**************************************************************************************************
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer

        Dim val As Integer
        Try
            '<update> 
            'If oDaRepresentante.GetObjectByParameters("", be) > 0 Then
            '    Return SPE.EmsambladoComun.ParametrosSistema.ExisteEmail.Existe
            'Else
            '</update> 
            Dim options As TransactionOptions = New TransactionOptions()
            Dim obeRepresentante As SPE.Entidades.BERepresentante = CType(be, SPE.Entidades.BERepresentante)
            Dim EncodePwd, Pwd As String

            EncodePwd = SeguridadComun.EncryptarCodigo(obeRepresentante.Password.Trim)
            options.IsolationLevel = IsolationLevel.ReadCommitted

            Using transaccionscope As New TransactionScope()
                obeRepresentante.CodigoRegistro = CodigoRegistro()
                Pwd = obeRepresentante.Password
                obeRepresentante.Password = EncodePwd
                val = MyBase.InsertRecord(be)
                If val <> SPE.EmsambladoComun.ParametrosSistema.Representante.ExisteUsuario And val <> SPE.EmsambladoComun.ParametrosSistema.Representante.ExisteRepresentante Then
                    Dim Email As New BLEmail
                    Email.EnviarCorreoConfirmacionUsuario(obeRepresentante.IdUsuario, obeRepresentante.Nombres, Pwd, obeRepresentante.CodigoRegistro, obeRepresentante.Email)
                End If
                transaccionscope.Complete()
                Return val
            End Using
            '<update> 
            'End If
            '</update> 
        Catch ex As Exception
            Logger.Write(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try


    End Function

    '************************************************************************************************** 
    ' M�todo          : DoDefineOrderedList
    ' Descripci�n     : Define la clase con la cual se va ordenar para BusinessEntityBase 
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "Nombres"
                list.Sort(New SPE.Entidades.BERepresentante.NombresComparer())
            Case "Apellidos"
                list.Sort(New SPE.Entidades.BERepresentante.ApellidosComparer())
            Case "Telefono"
                list.Sort(New SPE.Entidades.BERepresentante.TelefonoComparer())
            Case "Email"
                list.Sort(New SPE.Entidades.BERepresentante.EmailComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BERepresentante.DescripcionEstadoComparer())
        End Select
    End Sub
    Public Function ObtenerEmpresaporIDRepresentante(ByVal objRepresentante As BERepresentante) As BERepresentante
        Dim obeRepr As New DARepresentante
        Return obeRepr.ObtenerEmpresaporIDRepresentante(objRepresentante)
    End Function
    Public Function ConsultarEmpresasPorIdUsuario(ByVal be As BEUsuarioBase) As List(Of BEEmpresaContratante)
        Return New DARepresentante().ConsultarEmpresasPorIdUsuario(be)
    End Function
End Class
