Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun.ParametrosSistema

Imports _3Dev.FW.Negocio
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Util.DataUtil


Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.IO

Public Class BLServicio
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Private Const MSJRegistroServicioSatisfactorio As String = "El servicio {0} se ha registrado satisfactoriamente."
    Private Const MSJActualizacionServicioSatisfactorio As String = "El servicio {0} se ha actualizado satisfactoriamente."
    Private Const MSJServicioCodigoAGuardarYaExiste As String = "No se pudo guardar porque el c�digo del servicio ya existe."

    ' prueba prueba


    Public Function ConsultarServicioFormularioUrl(ByVal IdServicio As Integer, ByVal URL As String) As SPE.Entidades.BEServicio
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarServicioFormularioUrl(IdServicio, URL)
    End Function




    Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.DataAccessMaintenanceBase
        Return New SPE.AccesoDatos.DAServicio()
    End Function

    Public ReadOnly Property DAServicio() As DAServicio
        Get
            Return CType(DataAccessObject, DAServicio)
        End Get
    End Property

    '************************************************************************************************** 
    ' M�todo          : InsertRecord
    ' Descripci�n     : Registra un servicio con sus respectivos origenes de cancelacion
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 30/12/2008
    ' Parametros_In   : Entidad Servicio
    ' Parametros_Out  : Parametro de tipo entero
    ''**************************************************************************************************
    Public Overrides Function InsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim obeServicio As New BEServicio
        Dim odaServicio As New AccesoDatos.DAServicio
        Dim val As Integer
        Dim i As Integer
        Dim objTipoOrigenCancelacion As BETipoOrigenCancelacion
        Dim numeroTipoOrigenesCancelacion As Integer
        Dim options As TransactionOptions = New TransactionOptions()
        options.IsolationLevel = IsolationLevel.ReadCommitted
        Using transaccionscope As New TransactionScope()
            val = MyBase.InsertRecord(be)
            obeServicio = CType(be, BEServicio)
            If val > 0 Then
                numeroTipoOrigenesCancelacion = obeServicio.ListaTipoOrigenCancelacion.Count
                For i = 0 To numeroTipoOrigenesCancelacion - 1
                    objTipoOrigenCancelacion = New BETipoOrigenCancelacion
                    objTipoOrigenCancelacion = obeServicio.ListaTipoOrigenCancelacion.Item(i)
                    If objTipoOrigenCancelacion.IdTipoOrigenCancelacion > 0 Then
                        odaServicio.RegistrarServicioTipoOrigenCancelacion(val, objTipoOrigenCancelacion.IdTipoOrigenCancelacion, objTipoOrigenCancelacion.IdEstado, obeServicio.IdUsuarioCreacion)
                    End If
                Next
            End If
            transaccionscope.Complete()
        End Using

        'agregado
        Try
            obeServicio = CType(be, BEServicio)
            If obeServicio.PublicKey.Length > 0 Then
                File.WriteAllBytes(obeServicio.RutaClavePublica, obeServicio.PublicKey)
            End If
            If obeServicio.PublicKey.Length > 0 Then
                File.WriteAllBytes(obeServicio.RutaClavePrivate, obeServicio.PrivateKey)
            End If
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
        'agregado

        Return val
    End Function
    '************************************************************************************************** 
    ' M�todo          : InsertRecord
    ' Descripci�n     : Actualiza un servicio con sus respectivos origenes de cancelacion
    ' Autor           : Alex Paitan
    ' Fecha/Hora      : 30/12/2008
    ' Parametros_In   : Entidad Servicio
    ' Parametros_Out  : Parametro de tipo entero
    ''**************************************************************************************************
    Public Overrides Function UpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim obeServicio As New BEServicio
        Dim odaservicio As New AccesoDatos.DAServicio
        Dim val As Integer
        Dim i As Integer
        Dim objTipoOrigenCancelacion As BETipoOrigenCancelacion
        Dim numeroTipoOrigenesCancelacion As Integer
        Dim options As TransactionOptions = New TransactionOptions()
        options.IsolationLevel = IsolationLevel.ReadCommitted
        Using transaccionscope As New TransactionScope()
            val = MyBase.UpdateRecord(be)
            obeServicio = CType(be, BEServicio)
            If val > 0 Then
                numeroTipoOrigenesCancelacion = obeServicio.ListaTipoOrigenCancelacion.Count
                For i = 0 To numeroTipoOrigenesCancelacion - 1
                    objTipoOrigenCancelacion = New BETipoOrigenCancelacion
                    objTipoOrigenCancelacion = obeServicio.ListaTipoOrigenCancelacion.Item(i)
                    If objTipoOrigenCancelacion.IdTipoOrigenCancelacion > 0 Then
                        odaservicio.RegistrarServicioTipoOrigenCancelacion(val, objTipoOrigenCancelacion.IdTipoOrigenCancelacion, objTipoOrigenCancelacion.IdEstado, obeServicio.IdUsuarioCreacion)
                    End If
                Next
            End If
            transaccionscope.Complete()
        End Using
        Try
            obeServicio = CType(be, BEServicio)
            If obeServicio.PublicKey.Length > 0 Then
                File.WriteAllBytes(obeServicio.RutaClavePublica, obeServicio.PublicKey)
            End If
            If obeServicio.PublicKey.Length > 0 Then
                File.WriteAllBytes(obeServicio.RutaClavePrivate, obeServicio.PrivateKey)
            End If
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
        Return val
    End Function

    'Public Sub ListaOrigenesCancelacion(ByVal ListaTiposOrigenesCancelacion As List(Of BETipoOrigenCancelacion), ByVal IdServicio As Integer, ByVal IdUsuario As Integer)
    '    Dim odaServicio As New AccesoDatos.DAServicio
    '    Dim objTipoOrigenCancelacion As BETipoOrigenCancelacion
    '    Dim i As Integer
    '    For i = 0 To ListaTiposOrigenesCancelacion.Count - 1
    '        objTipoOrigenCancelacion = New BETipoOrigenCancelacion
    '        objTipoOrigenCancelacion = ListaTiposOrigenesCancelacion.Item(i)
    '        If objTipoOrigenCancelacion.IdTipoOrigenCancelacion > 0 Then
    '            odaServicio.re()
    '        End If
    '    Next
    'End Sub
    '************************************************************************************************** 
    ' M�todo          : ConsultarServicioPorUrl
    ' Descripci�n     : Consulta de servicios por Url
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : parametro
    ' Parametros_Out  : Instancia de la Entidad BEServicio cargada
    ''**************************************************************************************************
    Public Function ConsultarServicioPorUrl(ByVal parametro As Object) As SPE.Entidades.BEServicio
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarServicioPorUrl(parametro)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarPagoServicios
    ' Descripci�n     : Consulta de pagos de servicios
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BEOrdenPago cargada
    ' Parametros_Out  : Lista entidades BEOrdenPago
    ''**************************************************************************************************
    Public Function ConsultarPagoServicios(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarPagoServicios(obeOrdenPago)
    End Function

    Public Function ConsultarPagoServiciosNoRepresentante(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarPagoServiciosNoRepresentante(obeOrdenPago)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarServiciosPorIdUsuario
    ' Descripci�n     : Consulta de pagos de servicios por idusuario
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : Instancia de la Entidad BEServicio cargada
    ' Parametros_Out  : Lista entidades BEServicio
    ''**************************************************************************************************
    Public Function ConsultarServiciosPorIdUsuario(ByVal obeServicio As BEServicio) As List(Of BEServicio)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarServiciosPorIdUsuario(obeServicio)
    End Function

    '************************************************************************************************** 
    ' M�todo          : DoDefineOrderedList
    ' Descripci�n     : Define la clase con la cual se va ordenar para BusinessEntityBase 
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : list
    ' Parametros_Out  : 
    ''**************************************************************************************************
    Public Overrides Sub DoDefineOrderedList(ByVal list As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal pSortExpression As String)
        Select Case pSortExpression
            Case "Codigo"
                list.Sort(New SPE.Entidades.BEServicio.CodigoComparer())
            Case "Nombre"
                list.Sort(New SPE.Entidades.BEServicio.NombreComparer())
            Case "NombreEmpresaContrante"
                list.Sort(New SPE.Entidades.BEServicio.NombreEmpresaContranteComparer())
            Case "TiempoExpiracion"
                list.Sort(New SPE.Entidades.BEServicio.TiempoExpiracionComparer())
            Case "DescripcionEstado"
                list.Sort(New SPE.Entidades.BEServicio.DescripcionEstadoComparer())
        End Select
    End Sub


#Region "Configuraci�n de Servicios"
    Public Function ConsultarContratoXMLDeServicios(ByVal codigoAcceso As String) As String
        Return DAServicio.ConsultarContratoXMLDeServicios(codigoAcceso)
    End Function

    Public Function GuardarContratoXMLDeServicios(ByVal codigoAcceso As String, ByVal contratoXml As String) As String
        Return DAServicio.GuardarContratoXMLDeServicios(codigoAcceso, contratoXml)
    End Function
#End Region
    Public Function ConsultarServicioPorAPI(ByVal cAPI As String) As BEServicio
        Return DAServicio.ConsultarServicioPorAPI(cAPI)
    End Function
    Public Function ValidarServicioPorAPIYClave(ByVal cAPI As String, ByVal cClave As String) As Boolean
        Return DAServicio.ValidarServicioPorAPIYClave(cAPI, cClave)
    End Function

    Public Overrides Function DeleteEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim response As BusinessMessageBase = Nothing
        Try
            Dim cantPlantillasAsociados As Integer = 0
            Dim cantTipoOrigenCancelacionAsociados As Integer = 0
            Dim existeCIPAsociados As Boolean = False
            Dim idServicio As Integer = ObjectToInt32(request.EntityId)

            cantTipoOrigenCancelacionAsociados = DAServicio.CantidadTipoOrigenCancelaPorIdServicio(idServicio)
            Using odaCIP As New DAOrdenPago()
                existeCIPAsociados = odaCIP.ExisteCIPsPorIdServicio(idServicio)
            End Using
            Using odaPlantilla As New DAPlantilla()
                cantPlantillasAsociados = odaPlantilla.CantidadPlantillasPorIdServicio(idServicio)
            End Using

            If ((cantTipoOrigenCancelacionAsociados = 0) And (cantPlantillasAsociados = 0) And (Not existeCIPAsociados)) Then
                response = DAServicio.DeleteEntity(request)
            Else
                response = New BusinessMessageBase()
                response.BMResultState = BMResult.No
                Dim strResponse As New System.Text.StringBuilder()
                strResponse.Append("No se puede eliminar el servicio por los siguientes motivos:")
                strResponse.Append("  ")
                If (existeCIPAsociados) Then
                    strResponse.Append(" Existen CIPs asociados.")
                End If
                If (cantTipoOrigenCancelacionAsociados > 0) Then
                    'If (strResponse.Length > 0) Then
                    '    strResponse.Append(";")
                    'End If
                    strResponse.Append(String.Format(" Existen {0} Tipo(s) de cancelaci�n asociados", cantTipoOrigenCancelacionAsociados))
                End If
                If (cantPlantillasAsociados > 0) Then
                    'If (strResponse.Length > 0) Then
                    '    strResponse.Append(";")
                    'End If
                    strResponse.Append(String.Format(" Existen {0} Plantilla(s) asociadas", cantPlantillasAsociados))
                End If
                response.Message = strResponse.ToString()
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return response
    End Function

    Public Overrides Function SaveEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        Dim obeServicio As BEServicio = request.Entity
        Dim response As New BusinessMessageBase()
        Try
            Dim cantServicio As Integer = DAServicio.ConsultarCantidadServiciosPorCodigo(obeServicio.Codigo)
            If (((obeServicio.IdServicio <= 0) And (cantServicio >= 1)) Or ((obeServicio.IdServicio > 0) And (cantServicio > 1))) Then
                response.BMResultState = BMResult.No
                response.Message = MSJServicioCodigoAGuardarYaExiste
                Return response
            End If

            If (obeServicio.IdServicio <= 0) Then
                Me.InsertRecord(obeServicio)
                response.BMResultState = BMResult.Ok
                response.Message = String.Format(MSJRegistroServicioSatisfactorio, obeServicio.Nombre)
                response.Entity = DAServicio.GetRecordByID(obeServicio.IdServicio)
            Else
                Me.UpdateRecord(obeServicio)
                response.BMResultState = BMResult.Ok
                response.Message = String.Format(MSJActualizacionServicioSatisfactorio, obeServicio.Nombre)
                response.Entity = DAServicio.GetRecordByID(obeServicio.IdServicio)
            End If

        Catch ex As Exception
            response.BMResultState = BMResult.Error
            response.Message = ex.Message
        End Try
        Return response
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarServicioxEmpresa
    ' Descripci�n     : Consulta de pagos de servicios por Empresa
    ' Autor           : PERUCOM - MDIAZ
    ' Fecha/Hora      : 14/10/2011
    ' Parametros_In   : Instancia de la Entidad BEServicio cargada
    ' Parametros_Out  : Lista entidades BEServicio
    ''**************************************************************************************************
    Public Function ConsultarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarServicioxEmpresa(be)
    End Function
    '************************************************************************************************** 
    ' M�todo          : ActualizaServicioxEmpresa
    ' Descripci�n     : actualiza pagos de servicios por Empresa
    ' Autor           : PERUCOM - MDIAZ
    ' Fecha/Hora      : 14/10/2011
    ' Parametros_In   : Instancia de la Entidad BEServicio cargada
    ' Parametros_Out  : Lista entidades BEServicio
    ''**************************************************************************************************
    Public Function ActualizaServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ActualizaServicioxEmpresa(be)
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarTransaccionesxServicio
    ' Descripci�n     : Consultar transacciones y comisiones en un d�a (o intervalo de fechas) por servicio
    ' Autor           : PERUCOM - MDIAZ
    ' Fecha/Hora      : 26/10/2011
    ' Parametros_In   : Instancia de la Entidad BEServicio cargada,Rango de fechas
    ' Parametros_Out  : Instancia de la Entidad BEServicio cargada
    ''**************************************************************************************************
    Public Function ConsultarTransaccionesxServicio(ByVal objbeservicio As BEServicio, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarTransaccionesxServicio(objbeservicio, objFechaDel, objFechaAl)
    End Function

    ''**************************************************************************************************
    Public Function ConsultarTransaccionesxServicioLote(ByVal objbeservicio As BEServicio, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarTransaccionesxServicioLote(objbeservicio, objFechaDel, objFechaAl)
    End Function


    '<add Peru.Com FaseIII>
    Public Function UpdateServicioContratoXML(ByVal be As BEServicio) As Integer
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.UpdateServicioContratoXML(be)
    End Function

    'Cuenta Servicio
    Public Function RegistrarAsociacionCuentaServicio(ByVal be As BEServicioBanco) As Long
        Dim oServicio As New SPE.AccesoDatos.DAServicio
        Dim result As Long = 0
        Try
            Using transaccionscope As New TransactionScope()
                result = oServicio.RegistrarAsociacionCuentaServicio(be)
                transaccionscope.Complete()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function
    Public Function ActualizarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim oServicio As New SPE.AccesoDatos.DAServicio
        Dim result As Integer = 0
        Try
            Using transaccionscope As New TransactionScope()
                result = oServicio.ActualizarAsociacionCuentaServicio(be)
                transaccionscope.Complete()
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function
    '<upd Proc.Tesoreria>
    Public Function ConsultarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarAsociacionCuentaServicio(be)
    End Function
    Public Function ConsultarCodigosServiciosPorBanco(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarCodigosServiciosPorBanco(be)
    End Function
    Public Function ObtenerAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BEServicioBanco
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ObtenerAsociacionCuentaServicio(be)
    End Function

    Public Function EliminarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Boolean
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.EliminarAsociacionCuentaServicio(be)
    End Function

    Public Function ObtenerServicioComisionporID(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ObtenerServicioComisionporID(be)
    End Function
    Public Function ConsultarDetalleTransaccionesxServicio(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarDetalleTransaccionesxServicio(objbeservicio)
    End Function
    Public Function ConsultarDetalleTransaccionesxServicioSaga(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaSaga)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarDetalleTransaccionesxServicioSaga(objbeservicio)
    End Function
    Public Function ConsultarDetalleTransaccionesxServicioRipley(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaRipley)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarDetalleTransaccionesxServicioRipley(objbeservicio)
    End Function
    Public Function ConsultarDetalleTransaccionesPendientes(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarDetalleTransaccionesPendientes(objbeservicio)
    End Function
    Public Function RegistrarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.RegistrarServicioxEmpresa(be)
    End Function
    Public Function ConsultarServicioPorIdEmpresa(ByVal be As BEServicio) As List(Of BEServicio)
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarServicioPorIdEmpresa(be)
    End Function
    Function ConsultarPorAPIYClave(ByVal CAPI As String, ByVal CClave As String) As BEServicio
        Dim odaServicio As New SPE.AccesoDatos.DAServicio
        Return odaServicio.ConsultarPorAPIYClave(CAPI, CClave)
    End Function
    Public Function ConsultarComerciosAfiliados(ByVal visibleEnPortada As Boolean, ByVal proximoAfiliado As Boolean) As List(Of BEServicio)
        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.ConsultarComerciosAfiliados(visibleEnPortada, proximoAfiliado)
        End Using
    End Function
    Public Function ObtenerServicioPorIdWS(ByVal id As Integer) As BEServicio
        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.ObtenerServicioPorIdWS(id)
        End Using
    End Function


    Public Function ConsultarServicioPorClasificacion(ByVal oBEServicio As BEServicio) As List(Of BEServicio)

        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.ConsultarServicioPorClasificacion(oBEServicio)
        End Using

    End Function


    Public Function ObtenerServicioInstitucionPorId(ByVal id As Integer) As BEServicio
        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.ObtenerServicioInstitucionPorId(id)
        End Using
    End Function

    Public Function ObtenerServicioMunicipalidadBarrancoPorId(ByVal id As Integer) As BEServicio
        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.ObtenerServicioMunicipalidadBarrancoPorId(id)
        End Using
    End Function


    Public Function RecuperarDatosServicioDesconectado(ByVal id As Integer) As BEServicio
        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.RecuperarDatosServicioDesconectado(id)
        End Using
    End Function

    Public Function RecuperarDatosProductoServicio(ByVal oBEServicio As BEServicioProductoRequest) As List(Of BEServicioProducto)

        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.RecuperarDatosProductoServicio(oBEServicio)
        End Using
    End Function

    Public Function RecuperarDetalleProductoServicio(ByVal oBEServicio As BEServicioProductoRequest) As BEServicioProducto

        Using oDAServicio As New SPE.AccesoDatos.DAServicio
            Return oDAServicio.RecuperarDetalleProductoServicio(oBEServicio)
        End Using
    End Function

    Public Function RegistrarOrderIdComercio(ByVal codigo As String) As Integer
        Dim oServicio As New SPE.AccesoDatos.DAServicio
        Dim result As Integer = 0
        Try
            result = oServicio.RegistrarOrderIdComercio(codigo)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return result
    End Function


End Class

