Public Class BLServicioComun
    Inherits _3Dev.FW.Negocio.BLMaintenanceBase

    Public Function ConsultarOPsConciliadas() As System.Data.DataSet
        Return New SPE.AccesoDatos.DAServicioComun().ConsultarOPsConciliadas()
    End Function
    Public Function ActualizarOPAMigradoRecaudacion(ByVal pidOrdenes As Int64()) As Integer
        Dim listaordenes As String = ""
        For i As Integer = 0 To pidOrdenes.Length - 1
            listaordenes = listaordenes + "," + pidOrdenes(i).ToString()
        Next

        If listaordenes.Substring(0, 1) = "," Then
            listaordenes = listaordenes.Substring(1, listaordenes.Length - 1)
        End If
        Return New SPE.AccesoDatos.DAServicioComun().ActualizarOPAMigradoRecaudacion(listaordenes)
    End Function

End Class
