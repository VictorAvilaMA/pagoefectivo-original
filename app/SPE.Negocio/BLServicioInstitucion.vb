﻿Imports System
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports _3Dev.FW.Excepciones
Imports System.Globalization
Imports System.Configuration
Imports Excel
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLServicioInstitucion

    Implements IDisposable

    Dim culturaPeru = New CultureInfo(AppSettings("CulturaIdiomaPeru"))


    Public Sub RegistrarAlumnos(request)

    End Sub


    Function ValidateEmail(ByVal email As String) As String

        If email.Trim <> "" Then
            Dim emailRegex As New System.Text.RegularExpressions.Regex(
                "^(?<user>[^@]+)@(?<host>.+)$")
            Dim emailMatch As System.Text.RegularExpressions.Match =
               emailRegex.Match(email)
            If emailMatch.Success Then
                Return email
            End If
        End If
        Return System.Configuration.ConfigurationManager.AppSettings("EmailUsuarioInst")
    End Function

   
    'Public Function ProcesarArchivoServicioInstitucion(ByRef oServicioInstitucionRequest As Entidades.BEServicioInstitucionRequest) As BEServicioInstitucion
    '    Dim oBEServicioInstitucion As New BEServicioInstitucion


    '    Dim idErrorActualizarTiempoCarga As Integer = 0
    '    Dim idErrorRegistrarCabecera As Integer = 0
    '    Dim idErrorRegistrarDetalle As Integer = 0

    '    Dim intCantCabArchivo As Integer = 0
    '    Dim intCantDetArchivo As Integer = 0
    '    Dim intCantCabProc As Integer = 0
    '    Dim intCantDetProc As Integer = 0
    '    Dim idError As Integer = 0


    '    Try
    '        Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oServicioInstitucionRequest.Bytes)
    '        Dim intCantidadCargaDiaria As Integer = oServicioInstitucionRequest.NumeroCargaDiaria
    '        'Dim ContOP As Integer = 0
    '        'Dim ContRec As Integer = 0
    '        'Dim CodigoPago As String = ""

    '        'cabecera
    '        Dim strMerchantID As String = ""
    '        Dim strCodigoUsuario As String = ""
    '        Dim strCodigoUsuarioComp As String = ""
    '        Dim strNombreUsuario As String = ""
    '        Dim strApepaterno As String = ""
    '        Dim strApeMaterno As String = ""
    '        Dim strEmail As String = ""

    '        'detalle            
    '        Dim intIdOrden As Integer = 0
    '        Dim strNroDocumento As String = ""
    '        Dim strDescripcionOrden As String = ""
    '        Dim intMoneda As Integer = 0
    '        Dim decImporte As Decimal = 0
    '        Dim dtFechaEmision As DateTime
    '        Dim dtFechaVencimiento As DateTime
    '        Dim decMora As Decimal = 0
    '        Dim intNumeroCargaDiaria As Integer = 0
    '        Dim intTiempo As Integer = 0
    '        Dim intIdOrdenPago As Integer = 0
    '        Dim strCadenaErrores As String = ""
    '        Dim intLineaError As Integer = 1
    '        Dim dtNow As DateTime = Date.Now

    '        Using strTiempo As New IO.StringReader(Texto)
    '            Dim LineaTiempo As String = strTiempo.ReadLine()
    '            Dim cadenaTiempo() As String = LineaTiempo.Split("|")
    '            strMerchantID = cadenaTiempo(1)
    '            Using oDAServicioInstitucion As New DAServicioInstitucion
    '                idErrorActualizarTiempoCarga = oDAServicioInstitucion.ActualizarTiempoCarga(strMerchantID)
    '            End Using
    '            strTiempo.Close()
    '        End Using

    '        Using str As New IO.StringReader(Texto)


    '            Do

    '                Dim Linea As String = str.ReadLine()
    '                Dim cadena() As String = Linea.Split("|")
    '                intLineaError += 1
    '                If Linea.StartsWith("CC") Or Linea.StartsWith("DD") Then
    '                    If Linea.StartsWith("CC") Then
    '                        strMerchantID = cadena(1)
    '                        strCodigoUsuario = cadena(2)
    '                        strCodigoUsuarioComp = cadena(3)
    '                        strNombreUsuario = cadena(4)
    '                        strApepaterno = cadena(5)
    '                        strApeMaterno = cadena(6)
    '                        strEmail = ValidateEmail(cadena(7).ToLower().Replace("ñ", "n"))
    '                        Using oDAServicioInstitucion As New DAServicioInstitucion
    '                            idErrorRegistrarCabecera = oDAServicioInstitucion.RegistrarCabecera(strMerchantID, strCodigoUsuario, strCodigoUsuarioComp, strNombreUsuario, strApepaterno, strApeMaterno, strEmail)
    '                        End Using
    '                        intCantCabArchivo += 1

    '                        If idErrorRegistrarCabecera = 0 Then
    '                            intCantCabProc += 1
    '                        Else
    '                            strCadenaErrores += "o Linea: " + intLineaError.ToString + "</br>"
    '                        End If
    '                    Else
    '                        If Linea.StartsWith("DD") Then
    '                            intIdOrden = Integer.Parse(cadena(4))
    '                            strNroDocumento = cadena(5)
    '                            strDescripcionOrden = cadena(6)
    '                            intMoneda = Integer.Parse(cadena(7))
    '                            decImporte = Decimal.Parse(cadena(8))
    '                            dtFechaEmision = Convert.ToDateTime(cadena(9), culturaPeru)
    '                            dtFechaVencimiento = Convert.ToDateTime(cadena(10), culturaPeru)
    '                            decMora = Decimal.Parse(cadena(11))
    '                            intNumeroCargaDiaria = intCantidadCargaDiaria
    '                            intTiempo = SPE.EmsambladoComun.ParametrosSistema.VigenciaArchivoServicioInstitucion.Vigente
    '                            intIdOrdenPago = 0
    '                            Using oDAServicioInstitucion As New DAServicioInstitucion
    '                                idErrorRegistrarDetalle = oDAServicioInstitucion.RegistrarDetalle(strMerchantID, strCodigoUsuario, strCodigoUsuarioComp, strNroDocumento, intIdOrden, strDescripcionOrden, intMoneda, decImporte, dtFechaEmision, dtFechaVencimiento, decMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, dtNow)
    '                            End Using
    '                            intCantDetArchivo += 1

    '                            If idErrorRegistrarDetalle = 0 Then
    '                                intCantDetProc += 1
    '                            Else
    '                                strCadenaErrores += "<li>Linea: " + intLineaError.ToString + "</li>"
    '                            End If
    '                        End If
    '                    End If
    '                Else
    '                    idError = 1 'el formato del archivo no es el correcto
    '                End If
    '            Loop While str.Peek <> -1
    '            str.Close()
    '        End Using

    '        oBEServicioInstitucion.CantCabArchivo = intCantCabArchivo
    '        oBEServicioInstitucion.CantDetArchivo = intCantDetArchivo
    '        oBEServicioInstitucion.CantCabProc = intCantCabProc
    '        oBEServicioInstitucion.CantDetProc = intCantDetProc
    '        oBEServicioInstitucion.IdError = idError
    '        oBEServicioInstitucion.CadenaErrores = strCadenaErrores
    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        idError = 2 ' Ha ocurrido un error al momento de cargar el archivo
    '    End Try
    '    Return oBEServicioInstitucion
    'End Function





    Public Function IniciarCabecera(ByVal dt As DataTable) As DataTable
        dt.Columns.Add("MerchantID")
        dt.Columns.Add("CodUsuario")
        dt.Columns.Add("CodUsuarioComp")
        dt.Columns.Add("NombreUsuario")
        dt.Columns.Add("ApePatUsuario")
        dt.Columns.Add("ApeMatUsuario")
        dt.Columns.Add("EmailUsuario")
        Return dt
    End Function

    Public Sub CargarCabecera(ByVal dt As DataTable, ByVal strMerchantID As String, ByVal strCodigoUsuario As String, ByVal strCodigoUsuarioComp As String,
                            ByVal strNombreUsuario As String, ByVal strApepaterno As String, ByVal strApeMaterno As String, ByVal strEmail As String)
        dt.Rows.Add(strMerchantID, strCodigoUsuario, strCodigoUsuarioComp, strNombreUsuario, strApepaterno, strApeMaterno, strEmail)
    End Sub

    Public Function IniciarDetalle(ByVal dt As DataTable) As DataTable
        dt.Columns.Add("MerchantID")
        dt.Columns.Add("CodUsuario")
        dt.Columns.Add("IdOrden")
        dt.Columns.Add("NroDocumento")
        dt.Columns.Add("DescripcionOrden")
        dt.Columns.Add("Moneda")
        dt.Columns.Add("Importe")
        dt.Columns.Add("FechaEmision")
        dt.Columns.Add("FechaVencimiento")
        dt.Columns.Add("Mora")
        dt.Columns.Add("NumeroCargaDiaria")
        dt.Columns.Add("Tiempo")
        dt.Columns.Add("IdOrdenPago")
        dt.Columns.Add("CodUsuarioComp")
        Return dt
    End Function



    Public Sub CargarDetalle(ByVal dt As DataTable, ByVal strMerchantID As String, ByVal strCodigoUsuario As String, ByVal intIdOrden As Integer,
                              ByVal strNroDocumento As String, ByVal strDescripcionOrden As String, ByVal intMoneda As Integer,
                              ByVal decImporte As Decimal, ByVal dtFechaEmision As String, ByVal dtFechaVencimiento As String, ByVal decMora As Decimal,
                              ByVal intNumeroCargaDiaria As Integer, ByVal intTiempo As Integer, ByVal intIdOrdenPago As Integer, ByVal strCodUsuarioComp As String)

        dt.Rows.Add(strMerchantID, strCodigoUsuario, intIdOrden, strNroDocumento, strDescripcionOrden, intMoneda, decImporte,
                    dtFechaEmision, dtFechaVencimiento, decMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, strCodUsuarioComp)
    End Sub






    Public Function ProcesarArchivoServicioInstitucion(ByRef oServicioInstitucionRequest As Entidades.BEServicioInstitucionRequest) As BEServicioInstitucion
        Dim oBEServicioInstitucion As New BEServicioInstitucion


        Dim idErrorActualizarTiempoCarga As Integer = 0
        Dim idErrorRegistrarCabecera As Integer = 0
        Dim idErrorRegistrarDetalle As Integer = 0

        Dim intCantCabArchivo As Integer = 0
        Dim intCantDetArchivo As Integer = 0
        Dim intCantCabProc As Integer = 0
        Dim intCantDetProc As Integer = 0
        Dim idError As Integer = 0

        Dim dtCabecera As New DataTable
        Dim dtDetalle As New DataTable
        IniciarCabecera(dtCabecera)
        IniciarDetalle(dtDetalle)

        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oServicioInstitucionRequest.Bytes)
            Dim intCantidadCargaDiaria As Integer = oServicioInstitucionRequest.NumeroCargaDiaria
            'cabecera
            Dim strMerchantID As String = ""
            Dim strCodigoUsuario As String = ""
            Dim strCodigoUsuarioComp As String = ""
            Dim strNombreUsuario As String = ""
            Dim strApepaterno As String = ""
            Dim strApeMaterno As String = ""
            Dim strEmail As String = ""

            'detalle            
            Dim intIdOrden As Integer = 0
            Dim strNroDocumento As String = ""
            Dim strDescripcionOrden As String = ""
            Dim intMoneda As Integer = 0
            Dim decImporte As Decimal = 0
            Dim strFechaEmision As String
            Dim strFechaVencimiento As String
            Dim decMora As Decimal = 0
            Dim intNumeroCargaDiaria As Integer = 0
            Dim intTiempo As Integer = 0
            Dim intIdOrdenPago As Integer = 0
            Dim strCadenaErrores As String = ""
            Dim intLineaError As Integer = 1
            Dim dtNow As DateTime = Date.Now

            'Actualizar la tabla con tiempo = 2

            Using str As New IO.StringReader(Texto)


                Do

                    Dim Linea As String = str.ReadLine()
                    Dim cadena() As String = Linea.Split("|")
                    intLineaError += 1
                    If Linea.StartsWith("CC") Or Linea.StartsWith("DD") Then
                        If Linea.StartsWith("CC") Then
                            If cadena.Length = 8 Then
                                strMerchantID = cadena(1)
                                strCodigoUsuario = cadena(2)
                                strCodigoUsuarioComp = cadena(3)
                                strNombreUsuario = cadena(4)
                                strApepaterno = cadena(5)
                                strApeMaterno = cadena(6)
                                strEmail = ValidateEmail(cadena(7).ToLower().Replace("ñ", "n"))
                                Using oDAServicioInstitucion As New DAServicioInstitucion
                                    CargarCabecera(dtCabecera, strMerchantID, strCodigoUsuario, strCodigoUsuarioComp, strNombreUsuario, strApepaterno, strApeMaterno, strEmail)
                                End Using
                                intCantCabArchivo += 1

                                If idErrorRegistrarCabecera = 0 Then
                                    intCantCabProc += 1
                                Else
                                    strCadenaErrores += "o Linea: " + intLineaError.ToString + "</br>"
                                End If
                            Else
                                strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString + " Cabecera Incorrecta</br>"
                                ActualizarServInstitucion(oBEServicioInstitucion, 0, 0, 0, 0, 0, strCadenaErrores)
                                Return oBEServicioInstitucion
                            End If

                        Else
                            If Linea.StartsWith("DD") Then
                                If cadena.Length = 12 Then
                                    If cadena(1) = strMerchantID And strCodigoUsuario = cadena(2) Then


                                        intIdOrden = Integer.Parse(cadena(4))
                                        strNroDocumento = cadena(5)
                                        strDescripcionOrden = cadena(6)
                                        intMoneda = Integer.Parse(cadena(7))
                                        decImporte = Decimal.Parse(cadena(8))
                                        'dtFechaEmision = Convert.ToDateTime(cadena(9), culturaPeru)
                                        'dtFechaVencimiento = Convert.ToDateTime(cadena(10), culturaPeru)
                                        strFechaEmision = cadena(9).Split("/")(2) + cadena(9).Split("/")(1) + cadena(9).Split("/")(0)
                                        strFechaVencimiento = cadena(10).Split("/")(2) + cadena(10).Split("/")(1) + cadena(10).Split("/")(0)
                                        decMora = Decimal.Parse(cadena(11))
                                        intNumeroCargaDiaria = intCantidadCargaDiaria
                                        intTiempo = SPE.EmsambladoComun.ParametrosSistema.VigenciaArchivoServicioInstitucion.Vigente
                                        intIdOrdenPago = 0
                                        Using oDAServicioInstitucion As New DAServicioInstitucion
                                            CargarDetalle(dtDetalle, strMerchantID, strCodigoUsuario, intIdOrden, strNroDocumento, strDescripcionOrden,
                                                          intMoneda, decImporte, strFechaEmision, strFechaVencimiento, decMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, strCodigoUsuarioComp)
                                        End Using
                                        intCantDetArchivo += 1

                                        If idErrorRegistrarDetalle = 0 Then
                                            intCantDetProc += 1
                                        Else
                                            strCadenaErrores += "<li>Linea: " + intLineaError.ToString + "</li>"
                                        End If

                                    Else
                                        strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString + " El Detalle no pertenece a la Cabecera</br>"
                                        ActualizarServInstitucion(oBEServicioInstitucion, 0, 0, 0, 0, 0, strCadenaErrores)
                                        Return oBEServicioInstitucion
                                    End If
                                Else
                                    strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString + " Detalle Incorrecto</br>"
                                    ActualizarServInstitucion(oBEServicioInstitucion, 0, 0, 0, 0, 0, strCadenaErrores)
                                    Return oBEServicioInstitucion
                                End If
                            End If
                    End If
                    Else
                        idError = 1 'el formato del archivo no es el correcto
                        End If
                Loop While str.Peek <> -1
                str.Close()
            End Using





            Using oDAServicioInstitucion As New DAServicioInstitucion
                idErrorRegistrarDetalle = oDAServicioInstitucion.RegistrarLote(strMerchantID, dtCabecera, dtDetalle)
            End Using

            ActualizarServInstitucion(oBEServicioInstitucion, intCantCabArchivo, intCantDetArchivo, intCantCabProc, intCantDetProc, idError, strCadenaErrores)
        Catch ex As Exception
            'Logger.Write(ex)
            idError = 2 ' Ha ocurrido un error al momento de cargar el archivo
        End Try
        Return oBEServicioInstitucion
    End Function



    Public Sub ActualizarServInstitucion(ByVal oBEServicioInstitucion As BEServicioInstitucion, ByVal intCantCabArchivo as Integer, ByVal intCantDetArchivo as Integer, ByVal intCantCabProc as Integer, 
                                         ByVal intCantDetProc as Integer, ByVal idError as Integer, ByVal strCadenaErrores As String)
        oBEServicioInstitucion.CantCabArchivo = intCantCabArchivo
        oBEServicioInstitucion.CantDetArchivo = intCantDetArchivo
        oBEServicioInstitucion.CantCabProc = intCantCabProc
        oBEServicioInstitucion.CantDetProc = intCantDetProc
        oBEServicioInstitucion.IdError = idError
        oBEServicioInstitucion.CadenaErrores = strCadenaErrores
    End Sub

    Public Function ValidarCarga(ByRef oServicioInstitucionRequest As Entidades.BEServicioInstitucionRequest) As Integer
        Dim intContador As Integer = 0
        Try

            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oServicioInstitucionRequest.Bytes)
            Dim strMerchantID As String = ""
            Dim dtFechaEmision As DateTime = Date.Today

            Using str As New IO.StringReader(Texto)


                Dim Linea As String = str.ReadLine()
                Dim cadena() As String = Linea.Split("|")
                If Linea.StartsWith("CC") Or Linea.StartsWith("DD") Then
                    strMerchantID = cadena(1)
                End If
                str.Close()
            End Using

            Using oDAServicioInstitucion As New DAServicioInstitucion
                intContador = oDAServicioInstitucion.ValidarCarga(strMerchantID, dtFechaEmision)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return intContador
    End Function


    Public Function MostrarResumenArchivoServicioInstitucion(ByRef oServicioInstitucion As Entidades.BEServicioInstitucionRequest) As List(Of Entidades.BEServicioInstitucion)
        Dim response As New List(Of BEServicioInstitucion)
        Return response
    End Function


    Public Function ConsultarDetallePagoServicioInstitucion(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.ConsultarDetallePagoServicioInstitucion(oBEServicioInstitucionRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarInstitucionCIP(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.ConsultarInstitucionCIP(oBEServicioInstitucionRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    'Private Function ActualizarHTNroDocumento(ByVal request As BEServicioInstitucionRequest) As Hashtable
    '    Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(request.Bytes)
    '    Dim htTabla As Hashtable = New Hashtable()
    '    Dim intCont As Integer = 0
    '    Using str As New IO.StringReader(Texto)
    '        Do
    '            Dim Linea As String = str.ReadLine()
    '            Dim cadena() As String = Linea.Split("|")
    '            If cadena.Length = 9 Then
    '                htTabla.Add(cadena(3), intCont)
    '            End If
    '            str.Close()
    '        Loop While str.Peek <> -1
    '        str.Close()
    '    End Using
    '    Return htTabla
    'End Function


    Public Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer
        Dim idError As Integer = 0
        Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(request.Bytes)
        Dim htTabla As Hashtable = New Hashtable()
        Dim intCont As Integer = 0
        Dim cadena() As String
        Try
            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    cadena = Linea.Split("|")
                    If Linea.StartsWith("DD") Then
                        Try
                            htTabla.Add(cadena(5), intCont)
                        Catch ex As Exception
                        End Try
                    End If
                Loop While str.Peek <> -1
                str.Close()
            End Using

            Dim Req As BEServicioInstitucionRequest = request
            Req.MerchantID = cadena(1)
            Req.FechaEmision = DateTime.Today

            Dim ArrayCIPS As New ArrayList
            Dim listServInst As New List(Of BEServicioInstitucion)
            listServInst = ConsultarInstitucionCIP(request)

            If listServInst.Count > 0 Then

                Dim intConteo As Integer = 0
                Do
                    If (htTabla(listServInst(intConteo).NroDocumento) Is Nothing) Then
                        If Not ArrayCIPS.Contains(listServInst(intConteo).IdOrdenPago) Then
                            ArrayCIPS.Add(listServInst(intConteo).IdOrdenPago)
                        End If
                    End If
                    intConteo += 1
                Loop While (intConteo <= listServInst.Count - 1)



                'For Each pde As BEServicioInstitucion In listServInst '''''''' verificar que se traigan todos no solo uno

                '    If (htTabla(pde.NroDocumento) Is Nothing) Then
                '        If Not ArrayCIPS.Contains(pde.IdOrdenPago) Then
                '            ArrayCIPS.Add(pde.IdOrdenPago)
                '        End If
                '    End If

                'Next

                If ArrayCIPS.Count > 0 Then

                    Dim oDAServicioInstitucion As New DAServicioInstitucion


                    Dim oDAOrdenPago As New DAOrdenPago
                    Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
                    oBEServicioInstitucionRequest.IdUsuarioActualizacion = ConfigurationManager.AppSettings("UsuarioActInst")
                    oBEServicioInstitucionRequest.IdOrigenEliminacion = 0

                    Dim ValDoc As Integer
                    For Each ValDoc In ArrayCIPS
                        oBEServicioInstitucionRequest.IdOrdenPago = Convert.ToInt32(ValDoc.ToString())
                        'Dim intCIP As Integer = oDAOrdenPago.EliminarCIP(oBEOrdenPago)
                        Dim intCIP As Integer = oDAServicioInstitucion.EliminarCIPInstitucion(oBEServicioInstitucionRequest)
                    Next
                End If
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Return idError
        End Try
        Return idError
    End Function

    Public Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.ActualizarDocumentoInstitucion(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    Public Function ConsultarArchivosDescarga(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.ConsultarArchivosDescarga(oBEServicioInstitucionRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarDetalleArchivoDescarga(ByVal oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.ConsultarDetalleArchivoDescarga(oBEServicioInstitucionRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarCIPDocumento(ByVal request As BEServicioInstitucionRequest) As Integer
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.ConsultarCIPDocumento(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function DeterminarIdParametro(ByVal request As BEServicioInstitucionRequest) As Integer
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioInstitucion As New DAServicioInstitucion
                Return oDAServicioInstitucion.DeterminarIdParametro(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region


End Class




