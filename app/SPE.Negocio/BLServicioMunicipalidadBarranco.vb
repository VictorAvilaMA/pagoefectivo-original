﻿Imports System
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports _3Dev.FW.Excepciones
Imports System.Globalization
Imports System.Configuration
Imports Excel
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLServicioMunicipalidadBarranco

    Implements IDisposable

    Dim culturaPeru = New CultureInfo(AppSettings("CulturaIdiomaPeru"))


    Public Sub RegistrarAlumnos(request)

    End Sub


    Function ValidateEmail(ByVal email As String) As String

        If email.Trim <> "" Then
            Dim emailRegex As New System.Text.RegularExpressions.Regex(
                "^(?<user>[^@]+)@(?<host>.+)$")
            Dim emailMatch As System.Text.RegularExpressions.Match =
               emailRegex.Match(email)
            If emailMatch.Success Then
                Return email
            End If
        End If
        Return System.Configuration.ConfigurationManager.AppSettings("EmailUsuarioInst")
    End Function

    Public Function IniciarCabecera(ByVal dt As DataTable) As DataTable
        dt.Columns.Add("MerchantID")
        dt.Columns.Add("CodigoActual")
        dt.Columns.Add("CodigoContribuyenteComp")
        dt.Columns.Add("NombreContribuyente")
        dt.Columns.Add("ApellidoPaternoContribuyente")
        dt.Columns.Add("ApellidoMaternoContribuyente")
        dt.Columns.Add("EmailContribuyente")
        Return dt
    End Function

    Public Sub CargarCabecera(ByVal dt As DataTable, ByVal strMerchantID As String, ByVal strCodigoActual As String, ByVal strCodigoContribuyenteComp As String,
                            ByVal strNombreContribuyente As String, ByVal strApellidoPaternoContribuyente As String, ByVal strApellidoMaternoContribuyente As String, ByVal strEmailContribuyente As String)
        dt.Rows.Add(strMerchantID, strCodigoActual, strCodigoContribuyenteComp, strNombreContribuyente, strApellidoPaternoContribuyente, strApellidoMaternoContribuyente, strEmailContribuyente)
    End Sub

    Public Function IniciarDetalle(ByVal dt As DataTable) As DataTable
        dt.Columns.Add("MerchantID")
        dt.Columns.Add("CodigoActual")
        dt.Columns.Add("IdOrden")
        dt.Columns.Add("SerieNroDocumento")
        dt.Columns.Add("DescripcionOrden")
        dt.Columns.Add("ConceptoDocPago")
        dt.Columns.Add("MonedaDocPago")
        dt.Columns.Add("ImporteDocPago")
        dt.Columns.Add("FechaEmision")
        dt.Columns.Add("FechaVencimiento")
        dt.Columns.Add("ImporteMora")
        dt.Columns.Add("NumeroCargaDiaria")
        dt.Columns.Add("Tiempo")
        dt.Columns.Add("IdOrdenPago")
        dt.Columns.Add("CodigoContribuyenteComp")
        Return dt
    End Function

    Public Sub CargarDetalle(ByVal dt As DataTable, ByVal strMerchantID As String, ByVal strCodigoActual As String, ByVal intIdOrden As Integer,
                              ByVal strSerieNroDocumento As String, ByVal strDescripcionOrden As String, ByVal intMonedaDocPago As Integer,
                              ByVal decImporteDocPago As Decimal, ByVal dtFechaEmision As String, ByVal dtFechaVencimiento As String, ByVal decImporteMora As Decimal,
                              ByVal intNumeroCargaDiaria As Integer, ByVal intTiempo As Integer, ByVal intIdOrdenPago As Integer, ByVal strCodigoContribuyenteComp As String)
        Dim row As DataRow = dt.NewRow
        row("MerchantID") = strMerchantID
        row("CodigoActual") = strCodigoActual
        row("CodigoContribuyenteComp") = strCodigoContribuyenteComp
        row("IdOrden") = intIdOrden
        row("SerieNroDocumento") = strSerieNroDocumento
        row("DescripcionOrden") = strDescripcionOrden
        row("ConceptoDocPago") = strDescripcionOrden
        row("MonedaDocPago") = intMonedaDocPago
        row("ImporteDocPago") = decImporteDocPago
        row("FechaEmision") = dtFechaEmision
        row("FechaVencimiento") = dtFechaVencimiento
        row("ImporteMora") = decImporteMora
        row("NumeroCargaDiaria") = intNumeroCargaDiaria
        row("Tiempo") = intTiempo
        row("IdOrdenPago") = intIdOrdenPago
        dt.Rows.Add(row)
        'dt.Rows.Add(strMerchantID, strCodigoActual, intIdOrden, strSerieNroDocumento, strDescripcionOrden, intMonedaDocPago, decImporteDocPago,
        '            dtFechaEmision, dtFechaVencimiento, decImporteMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, strCodigoContribuyenteComp)
    End Sub

    Public Function ProcesarArchivoServicioMunicipalidadBarranco(ByRef oServicioMunicipalidadBarrancoRequest As Entidades.BEServicioMunicipalidadBarrancoRequest) As BEServicioMunicipalidadBarranco
        Dim oBEMunicipalidadBarranco As New BEServicioMunicipalidadBarranco

        Dim idErrorActualizarTiempoCarga As Integer = 0
        Dim idErrorRegistrarCabecera As Integer = 0
        Dim idErrorRegistrarDetalle As Integer = 0

        Dim intCantCabArchivo As Integer = 0
        Dim intCantDetArchivo As Integer = 0
        Dim intCantCabProc As Integer = 0
        Dim intCantDetProc As Integer = 0
        Dim idError As Integer = 0

        Dim dtCabecera As New DataTable
        Dim dtDetalle As New DataTable
        IniciarCabecera(dtCabecera)
        IniciarDetalle(dtDetalle)

        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oServicioMunicipalidadBarrancoRequest.Bytes)
            Dim intCantidadCargaDiaria As Integer = oServicioMunicipalidadBarrancoRequest.NumeroCargaDiaria
            'cabecera
            Dim strMerchantID As String = ""
            Dim strCodigoActual As String = ""
            Dim strCodigoContribuyenteComp As String = ""
            Dim strNombreContribuyente As String = ""
            Dim strApellidoPaternoContribuyente As String = ""
            Dim strApellidoMaternoContribuyente As String = ""
            Dim strEmailContribuyente As String = ""

            'detalle            
            Dim intIdOrden As Integer = 0
            Dim strSerieNroDocumento As String = ""
            Dim strDescripcionOrden As String = ""
            Dim intMonedaDocPago As Integer = 0
            Dim decImporteDocPago As Decimal = 0
            Dim strFechaEmision As String
            Dim strFechaVencimiento As String
            Dim decImporteMora As Decimal = 0
            Dim intNumeroCargaDiaria As Integer = 0
            Dim intTiempo As Integer = 0
            Dim intIdOrdenPago As Integer = 0
            Dim strCadenaErrores As String = ""
            Dim intLineaError As Integer = 1
            Dim dtNow As DateTime = Date.Now

            'Actualizar la tabla con tiempo = 2

            Using str As New IO.StringReader(Texto)

                Do

                    Dim Linea As String = str.ReadLine()
                    Dim cadena() As String = Linea.Split("|")
                    intLineaError += 1
                    If Linea.StartsWith("CC") Or Linea.StartsWith("DD") Then
                        If Linea.StartsWith("CC") Then
                            If cadena.Length = 8 Then
                                strMerchantID = cadena(1)
                                strCodigoActual = cadena(2)
                                strCodigoContribuyenteComp = cadena(3)
                                strNombreContribuyente = cadena(4)
                                strApellidoPaternoContribuyente = cadena(5)
                                strApellidoMaternoContribuyente = cadena(6)
                                strEmailContribuyente = ValidateEmail(cadena(7).ToLower().Replace("ñ", "n"))
                                Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                                    CargarCabecera(dtCabecera, strMerchantID, strCodigoActual, strCodigoContribuyenteComp, strNombreContribuyente, strApellidoPaternoContribuyente, strApellidoMaternoContribuyente, strEmailContribuyente)
                                End Using
                                intCantCabArchivo += 1

                                If idErrorRegistrarCabecera = 0 Then
                                    intCantCabProc += 1
                                Else
                                    strCadenaErrores += "o Linea: " + intLineaError.ToString + "</br>"
                                End If
                            Else
                                strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString + " Cabecera Incorrecta</br>"
                                ActualizarServMunicipalidadBarranco(oBEMunicipalidadBarranco, 0, 0, 0, 0, 0, strCadenaErrores)
                                Return oBEMunicipalidadBarranco
                            End If

                        Else
                            If Linea.StartsWith("DD") Then
                                If cadena.Length = 12 Then
                                    If cadena(1) = strMerchantID And strCodigoActual = cadena(2) Then

                                        intIdOrden = Integer.Parse(cadena(4))
                                        strSerieNroDocumento = cadena(5)
                                        strDescripcionOrden = cadena(6)
                                        intMonedaDocPago = Integer.Parse(cadena(7))
                                        decImporteDocPago = Decimal.Parse(cadena(8))
                                        'dtFechaEmision = Convert.ToDateTime(cadena(9), culturaPeru)
                                        'dtFechaVencimiento = Convert.ToDateTime(cadena(10), culturaPeru)
                                        strFechaEmision = cadena(9).Split("/")(2) + cadena(9).Split("/")(1) + cadena(9).Split("/")(0)
                                        strFechaVencimiento = cadena(10).Split("/")(2) + cadena(10).Split("/")(1) + cadena(10).Split("/")(0)
                                        decImporteMora = Decimal.Parse(cadena(11))
                                        intNumeroCargaDiaria = intCantidadCargaDiaria
                                        intTiempo = SPE.EmsambladoComun.ParametrosSistema.VigenciaArchivoServicioInstitucion.Vigente
                                        intIdOrdenPago = 0
                                        Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                                            CargarDetalle(dtDetalle, strMerchantID, strCodigoActual, intIdOrden, strSerieNroDocumento, strDescripcionOrden,
                                                          intMonedaDocPago, decImporteDocPago, strFechaEmision, strFechaVencimiento, decImporteMora, intNumeroCargaDiaria, intTiempo, intIdOrdenPago, strCodigoContribuyenteComp)
                                        End Using
                                        intCantDetArchivo += 1

                                        If idErrorRegistrarDetalle = 0 Then
                                            intCantDetProc += 1
                                        Else
                                            strCadenaErrores += "<li>Linea: " + intLineaError.ToString + "</li>"
                                        End If

                                    Else
                                        strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString + " El Detalle no pertenece a la Cabecera</br>"
                                        ActualizarServMunicipalidadBarranco(oBEMunicipalidadBarranco, 0, 0, 0, 0, 0, strCadenaErrores)
                                        Return oBEMunicipalidadBarranco
                                    End If
                                Else
                                    strCadenaErrores += "o Linea: " + (intLineaError - 1).ToString + " Detalle Incorrecto</br>"
                                    ActualizarServMunicipalidadBarranco(oBEMunicipalidadBarranco, 0, 0, 0, 0, 0, strCadenaErrores)
                                    Return oBEMunicipalidadBarranco
                                End If
                            End If
                        End If
                    Else
                        idError = 1 'el formato del archivo no es el correcto
                    End If
                Loop While str.Peek <> -1
                str.Close()
            End Using

            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                idErrorRegistrarDetalle = oDAServicioMunicipalidadBarranco.RegistrarLoteMunicipalidadBarranco(strMerchantID, dtCabecera, dtDetalle)
            End Using

            ActualizarServMunicipalidadBarranco(oBEMunicipalidadBarranco, intCantCabArchivo, intCantDetArchivo, intCantCabProc, intCantDetProc, idError, strCadenaErrores)
        Catch ex As Exception
            'Logger.Write(ex)
            idError = 2 ' Ha ocurrido un error al momento de cargar el archivo
        End Try
        Return oBEMunicipalidadBarranco
    End Function

    Public Sub ActualizarServMunicipalidadBarranco(ByVal oBEMunicipalidadBarranco As BEServicioMunicipalidadBarranco, ByVal intCantCabArchivo As Integer, ByVal intCantDetArchivo As Integer, ByVal intCantCabProc As Integer,
                                         ByVal intCantDetProc As Integer, ByVal idError As Integer, ByVal strCadenaErrores As String)
        oBEMunicipalidadBarranco.CantCabArchivo = intCantCabArchivo
        oBEMunicipalidadBarranco.CantDetArchivo = intCantDetArchivo
        oBEMunicipalidadBarranco.CantCabProc = intCantCabProc
        oBEMunicipalidadBarranco.CantDetProc = intCantDetProc
        oBEMunicipalidadBarranco.IdError = idError
        oBEMunicipalidadBarranco.CadenaErrores = strCadenaErrores
    End Sub

    Public Function ValidarCargaMunicipalidadBarranco(ByRef oServicioMunicipalidadBarrancoRequest As Entidades.BEServicioMunicipalidadBarrancoRequest) As Integer
        Dim intContador As Integer = 0
        Try

            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oServicioMunicipalidadBarrancoRequest.Bytes)
            Dim strMerchantID As String = ""
            Dim dtFechaEmision As DateTime = Date.Today

            Using str As New IO.StringReader(Texto)

                Dim Linea As String = str.ReadLine()
                Dim cadena() As String = Linea.Split("|")
                If Linea.StartsWith("CC") Or Linea.StartsWith("DD") Then
                    strMerchantID = cadena(1)
                End If
                str.Close()
            End Using

            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                intContador = oDAServicioMunicipalidadBarranco.ValidarCargaMunicipalidadBarranco(strMerchantID, dtFechaEmision)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return intContador
    End Function


    Public Function MostrarResumenArchivoServicioMunicipalidadBarranco(ByRef oServicioMunicipalidadBarranco As Entidades.BEServicioMunicipalidadBarrancoRequest) As List(Of Entidades.BEServicioMunicipalidadBarranco)
        Dim response As New List(Of BEServicioMunicipalidadBarranco)
        Return response
    End Function


    Public Function ConsultarDetallePagoServicioMunicipalidadBarranco(ByVal oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.ConsultarDetallePagoServicioMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarMunicipalidadBarrancoCIP(ByVal oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.ConsultarMunicipalidadBarrancoCIP(oBEServicioMunicipalidadBarrancoRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    'Private Function ActualizarHTNroDocumento(ByVal request As BEServicioInstitucionRequest) As Hashtable
    '    Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(request.Bytes)
    '    Dim htTabla As Hashtable = New Hashtable()
    '    Dim intCont As Integer = 0
    '    Using str As New IO.StringReader(Texto)
    '        Do
    '            Dim Linea As String = str.ReadLine()
    '            Dim cadena() As String = Linea.Split("|")
    '            If cadena.Length = 9 Then
    '                htTabla.Add(cadena(3), intCont)
    '            End If
    '            str.Close()
    '        Loop While str.Peek <> -1
    '        str.Close()
    '    End Using
    '    Return htTabla
    'End Function


    Public Function ActualizarDocumentosCIPMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
        Dim idError As Integer = 0
        Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(request.Bytes)
        Dim htTabla As Hashtable = New Hashtable()
        Dim intCont As Integer = 0
        Dim cadena() As String
        Try
            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    cadena = Linea.Split("|")
                    If Linea.StartsWith("DD") Then
                        Try
                            htTabla.Add(cadena(5), intCont)
                        Catch ex As Exception
                        End Try
                    End If
                Loop While str.Peek <> -1
                str.Close()
            End Using

            Dim Req As BEServicioMunicipalidadBarrancoRequest = request
            Req.MerchantID = cadena(1)
            Req.FechaEmision = DateTime.Today

            Dim ArrayCIPS As New ArrayList
            Dim listServMunicipalidadBarranco As New List(Of BEServicioMunicipalidadBarranco)
            listServMunicipalidadBarranco = ConsultarMunicipalidadBarrancoCIP(request)

            If listServMunicipalidadBarranco.Count > 0 Then

                Dim intConteo As Integer = 0
                Do
                    If (htTabla(listServMunicipalidadBarranco(intConteo).SerieNroDocumento) Is Nothing) Then
                        If Not ArrayCIPS.Contains(listServMunicipalidadBarranco(intConteo).IdOrdenPago) Then
                            ArrayCIPS.Add(listServMunicipalidadBarranco(intConteo).IdOrdenPago)
                        End If
                    End If
                    intConteo += 1
                Loop While (intConteo <= listServMunicipalidadBarranco.Count - 1)



                'For Each pde As BEServicioInstitucion In listServInst '''''''' verificar que se traigan todos no solo uno

                '    If (htTabla(pde.NroDocumento) Is Nothing) Then
                '        If Not ArrayCIPS.Contains(pde.IdOrdenPago) Then
                '            ArrayCIPS.Add(pde.IdOrdenPago)
                '        End If
                '    End If

                'Next

                If ArrayCIPS.Count > 0 Then

                    Dim oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco


                    Dim oDAOrdenPago As New DAOrdenPago
                    Dim oBEServicioMunicipalidadBarrancoRequest As New BEServicioMunicipalidadBarrancoRequest
                    oBEServicioMunicipalidadBarrancoRequest.IdUsuarioActualizacion = ConfigurationManager.AppSettings("UsuarioActMunicipalidadBarranco")
                    oBEServicioMunicipalidadBarrancoRequest.IdOrigenEliminacion = 0

                    Dim ValDoc As Integer
                    For Each ValDoc In ArrayCIPS
                        oBEServicioMunicipalidadBarrancoRequest.IdOrdenPago = Convert.ToInt32(ValDoc.ToString())
                        'Dim intCIP As Integer = oDAOrdenPago.EliminarCIP(oBEOrdenPago)
                        Dim intCIP As Integer = oDAServicioMunicipalidadBarranco.EliminarCIPMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
                    Next
                End If
            End If
        Catch ex As Exception
            'Logger.Write(ex)
            Return idError
        End Try
        Return idError
    End Function

    Public Function ActualizarDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.ActualizarDocumentoMunicipalidadBarranco(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function


    Public Function ConsultarArchivosDescargaMunicipalidadBarranco(ByVal oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.ConsultarArchivosDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarDetalleArchivoDescargaMunicipalidadBarranco(ByVal oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ConsultarCIPDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.ConsultarCIPDocumentoMunicipalidadBarranco(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function DeterminarIdParametroMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
        Try
            Threading.Thread.CurrentThread.CurrentCulture = culturaPeru
            Using oDAServicioMunicipalidadBarranco As New DAServicioMunicipalidadBarranco
                Return oDAServicioMunicipalidadBarranco.DeterminarIdParametroMunicipalidadBarranco(request)
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

#Region " IDisposable Support "

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
