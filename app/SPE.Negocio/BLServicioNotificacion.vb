Imports System
Imports System.Transactions
Imports System.Collections.Generic

Imports SPE.Entidades

Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Utilitario
Imports SPE.Criptography
Imports SPE.Adapters
Imports MongoDB.Driver
Imports SPE.Logging
Imports System.Configuration
Imports System.IO


Public Class BLServicioNotificacion
    Inherits _3Dev.FW.Negocio.BLBase
    Private ReadOnly _logger As ILogger = New Logger()

    Public Sub New()

    End Sub

#Region "Registros de Notificacion"

    '17/11/2010 - wjra - Agregue el parametro : idOrigenRegistro a los Metodos : RegistrarServicioNotificacionUrlOk,RegistrarServicioNotificacionUrlError y RegistrarServicioNotificacionUrlCommun
    Public Function RegistrarServicioNotificacionUrlOk(ByVal obeOrdenPago As BEOrdenPago, ByVal idMovimiento As Int64, ByVal idUsuarioCreacion As Integer, ByVal idOrigenRegistro As Integer, ByVal idGrupoNotificacion As Integer) As Integer
        Return RegistrarServicioNotificacionUrlCommun(obeOrdenPago, obeOrdenPago.UrlOk, SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlOk, idMovimiento, idUsuarioCreacion, idOrigenRegistro, idGrupoNotificacion)
    End Function

    '17/11/2010 - wjra
    Public Function RegistrarServicioNotificacionUrlError(ByVal obeOrdenPago As BEOrdenPago, ByVal idMovimiento As Int64, ByVal idUsuarioCreacion As Integer, ByVal idOrigenRegistro As Integer, ByVal idGrupoNotificacion As Integer) As Integer
        Return RegistrarServicioNotificacionUrlCommun(obeOrdenPago, obeOrdenPago.UrlError, SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlError, idMovimiento, idUsuarioCreacion, idOrigenRegistro, idGrupoNotificacion)
    End Function

    '17/11/2010 - wjra
    '<upd Peru.Com FaseIII> actualizado para generar notificaciones con la nueva version
    Public Function RegistrarServicioNotificacionUrlCommun(ByVal obeOrdenPago As BEOrdenPago, ByVal url As String, ByVal idTipoNotificacion As String, ByVal idMovimiento As Int64, ByVal idUsuarioCreacion As Integer, ByVal idOrigenRegistro As Integer, ByVal idGrupoNotificacion As Integer) As Integer
        Dim IntResult = 0
        Dim urlEstado = True
        Dim obeServNotificacion As New BEServicioNotificacion()
        Try
            _logger.Info(String.Format("RegistrarServicioNotificacionUrlCommun - IdOrdenPago: {0}, ServicioIdTipoNotificacion: {1}, IdMovimiento: {2}, IdUsuarioCreacion: {3}, IdOrigenRegistro:{4}, IdGrupoNotificacion:{5}, VersionOrdenPago:{6}", obeOrdenPago.IdOrdenPago,
                                      obeOrdenPago.ServicioIdTipoNotificacion, idMovimiento, idUsuarioCreacion, idOrigenRegistro, idGrupoNotificacion, obeOrdenPago.VersionOrdenPago))

            If (obeOrdenPago.ServicioIdTipoNotificacion = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoNotificacion.Request) Then

                obeServNotificacion.IdOrdenPago = obeOrdenPago.IdOrdenPago
                obeServNotificacion.IdMovimiento = idMovimiento
                obeServNotificacion.IdUsuarioCreacion = idUsuarioCreacion
                obeServNotificacion.IdOrigenRegistro = idOrigenRegistro
                obeServNotificacion.IdSolicitudPago = 0
                obeServNotificacion.IdGrupoNotificacion = idGrupoNotificacion


                If obeOrdenPago.VersionOrdenPago = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.VersionOrdenPago.Version2 Then
                    Dim oDAServicio As New DAServicio
                    Dim oBEServicio As BEServicio = CType(oDAServicio.GetRecordByID(obeOrdenPago.IdServicio), BEServicio)
                    obeServNotificacion.UrlRespuesta = CompletarUrlRespuestaV2(oBEServicio.UrlNotificacion, obeOrdenPago, urlEstado)
                    'obeServNotificacion.UrlRespuesta = "" 
                    'SagaInicio
                    If obeOrdenPago.MerchantID = "SGA" Then
                        obeServNotificacion.IdTipoServNotificacion = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSaga
                        'ElseIf obeOrdenPago.MerchantID = "RPY" Then
                        'obeServNotificacion.IdTipoServNotificacion = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionRipley
                    ElseIf obeOrdenPago.MerchantID = "SDP" Then
                        obeServNotificacion.IdTipoServNotificacion = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSodimac
                    Else
                        obeServNotificacion.IdTipoServNotificacion = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacion
                    End If
                    'SagaFin
                    'obeServNotificacion.IdTipoServNotificacion = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacion
                Else
                    obeServNotificacion.UrlRespuesta = CompletarUrlRespuesta(url, obeOrdenPago, obeServNotificacion.IdTipoServNotificacion, urlEstado)
                    obeServNotificacion.IdTipoServNotificacion = idTipoNotificacion
                End If

                obeServNotificacion.IdEstado = IIf(urlEstado = True, SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Pendiente, SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorRegistro)
                Using odaServNotificacion As New DAServicioNotificacion()

                    IntResult = odaServNotificacion.InsertRecord(obeServNotificacion)
                    ''Mongo
                    'Dim oBEMongoServicioNotificacion As New BEMongoServicioNotificacion(obeServNotificacion)
                    'oBEMongoServicioNotificacion.IdServicioNotificacion = IntResult
                    'MongoDBAdapter.CollectionInsertarGenerico(Of BEMongoServicioNotificacion)(oBEMongoServicioNotificacion, "LogServicioNotificacion", False)
                    ''MongoFin
                    _logger.Info(String.Format("RegistrarServicioNotificacionUrlCommun: {0}", IntResult))
                    Return IntResult
                End Using
            End If
            'Catch MongoEx As MongoException
            '    Using odaServNotificacion As New DAServicioNotificacion()
            '        Dim oBEMongoServicioNotificacion As New BEMongoServicioNotificacion(obeServNotificacion)
            '        odaServNotificacion.InsertLogNotificacionBD(oBEMongoServicioNotificacion)
            '    End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function


    '<add Peru.Com FaseIII> 
    Public Function RegistrarServicioNotificacionUrlSolicitudPago(ByVal oBESolicitudPago As BESolicitudPago, ByVal idOrigenRegistro As Integer) As Integer
        Dim IntResult = 0
        Dim obeServNotificacion As New BEServicioNotificacion()
        Try
            obeServNotificacion.IdOrdenPago = 0
            obeServNotificacion.IdMovimiento = 0
            obeServNotificacion.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Pendiente
            obeServNotificacion.IdUsuarioCreacion = oBESolicitudPago.IdUsuarioCreacion
            obeServNotificacion.IdOrigenRegistro = idOrigenRegistro
            obeServNotificacion.IdSolicitudPago = oBESolicitudPago.IdSolicitudPago
            ''
            Dim oDAServicio As New DAServicio
            Dim oBEServicio As BEServicio = CType(oDAServicio.GetRecordByID(oBESolicitudPago.IdServicio), BEServicio)
            obeServNotificacion.UrlRespuesta = CompletarUrlRespuestaV2Solicitud(oBEServicio.UrlNotificacion, oBESolicitudPago)
            obeServNotificacion.IdTipoServNotificacion = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacion
            ''
            Using odaServNotificacion As New DAServicioNotificacion()

                IntResult = odaServNotificacion.InsertRecord(obeServNotificacion)
                ''Mongo
                'Dim oBEMongoServicioNotificacion As New BEMongoServicioNotificacion(obeServNotificacion)
                'oBEMongoServicioNotificacion.IdServicioNotificacion = IntResult
                'MongoDBAdapter.CollectionInsertarGenerico(Of BEMongoServicioNotificacion)(oBEMongoServicioNotificacion, "LogServicioNotificacion", False)
                ''MongoFin
                Return IntResult
            End Using
            'Catch MongoEx As MongoException
            '    Using odaServNotificacion As New DAServicioNotificacion()
            '        Dim oBEMongoServicioNotificacion As New BEMongoServicioNotificacion(obeServNotificacion)
            '        odaServNotificacion.InsertLogNotificacionBD(oBEMongoServicioNotificacion)
            '    End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try

    End Function
#End Region

#Region "Proceso"

    Public Function ConsultarNotificacionesPendientes() As List(Of BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarNotificacionesPendientes()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarNotificacionesListasANotificar() As BEConsultServNotifResponse
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Dim obeConsultServNotifResponse As New BEConsultServNotifResponse()
                obeConsultServNotifResponse.ServNotificacionPendiente = odaServicioNotificacion.ConsultarNotificacionesPendientes()
                If (CacheServer.EstadoNotificacionErrorActiva()) Then
                    obeConsultServNotifResponse.ServNotificacionErroneas = odaServicioNotificacion.ConsultarServicioNotificacionPorIdEstado(SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado)
                End If
                Return obeConsultServNotifResponse
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ActualizarEstadoNotificacionReenvioSodimac(ByVal obeBEServicioNotificacion As BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ActualizarEstadoNotificacionReenvioSodimac(obeBEServicioNotificacion.IdServicioNotificacion, obeBEServicioNotificacion.IdEstado)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ActualizarEstadoNotificaciones(ByVal listaNotificacionesCorrectas As List(Of BEServicioNotificacion), ByVal listaNotificacionesErroneas As List(Of BEServicioNotificacion)) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                If (listaNotificacionesCorrectas.Count > 0) Then
                    Dim strIdServicioNotificacion As String = ""
                    Dim listIdServNotificacion As New List(Of String)
                    Dim idEstado As Integer = 0
                    Dim strObservacion As String = ""

                    For Each obeServicioNotif As BEServicioNotificacion In listaNotificacionesCorrectas
                        listIdServNotificacion.Add(obeServicioNotif.IdServicioNotificacion)
                        idEstado = obeServicioNotif.IdEstado
                        strObservacion = obeServicioNotif.Observacion
                    Next
                    If (listIdServNotificacion.Count > 0) Then
                        strIdServicioNotificacion = String.Join(",", listIdServNotificacion.ToArray())
                        odaServicioNotificacion.ActualizarEstadoNotificacionMaviso(strIdServicioNotificacion, idEstado, strObservacion)
                    End If
                End If

                If (listaNotificacionesErroneas.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In listaNotificacionesErroneas
                        odaServicioNotificacion.ActualizarEstadoNotificacion(obeServicioNotif)
                    Next
                End If
            End Using

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

#End Region

#Region "Proceso Saga"
    Public Function ConsultarNotificacionesListasANotificarSaga() As BEConsultServNotifResponse
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Dim obeConsultServNotifResponse As New BEConsultServNotifResponse()
                obeConsultServNotifResponse.ServNotificacionPendiente = odaServicioNotificacion.ConsultarNotificacionesPendientesSaga()
                If (CacheServer.EstadoNotificacionErrorActiva()) Then
                    obeConsultServNotifResponse.ServNotificacionErroneas = odaServicioNotificacion.ConsultarServicioNotificacionPorIdEstadoSaga(SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado)
                End If
                Return obeConsultServNotifResponse
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
#End Region

#Region "Proceso Ripley"

    Public Function ConsultarNotificacionesListasANotificarRipley() As BEConsultServNotifResponse
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Dim obeConsultServNotifResponse As New BEConsultServNotifResponse()
                obeConsultServNotifResponse.ServNotificacionPendiente = odaServicioNotificacion.ConsultarNotificacionesPendientesRipley()
                If (CacheServer.EstadoNotificacionErrorActiva()) Then
                    obeConsultServNotifResponse.ServNotificacionErroneas = odaServicioNotificacion.ConsultarServicioNotificacionPorIdEstadoRipley(SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado)
                End If
                Return obeConsultServNotifResponse
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

#End Region

#Region "Proceso Sodimac"
    Public Function ConsultarNotificacionesListasANotificarSodimac() As BEConsultServNotifResponse
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Dim obeConsultServNotifResponse As New BEConsultServNotifResponse()
                obeConsultServNotifResponse.ServNotificacionPendiente = odaServicioNotificacion.ConsultarNotificacionesPendientesSodimac()
                If (CacheServer.EstadoNotificacionErrorActiva()) Then
                    obeConsultServNotifResponse.ServNotificacionErroneas = odaServicioNotificacion.ConsultarServicioNotificacionPorIdEstadoSodimac(SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado)
                End If
                Return obeConsultServNotifResponse
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
#End Region

    Public Function ConsultarServicioNotificacionReenvioSodimac() As List(Of BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarServicioNotificacionReenvioSodimac()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarDatosParaServicioSodimac(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarDatosParaServicioSodimac(IdOrdenPago)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarOrdenesPagoSodimac() As List(Of BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarOrdenesPagoSodimac()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogConsumoWSSodimacRequest(ByVal obeLogWSSodimacRequest As BELogWebServiceSodimacRequest) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.RegistrarLogConsumoWSSodimacRequest(obeLogWSSodimacRequest)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogConsumoWSSodimacResponse(ByVal obeLogWSSodimacResponse As BELogWebServiceSodimacResponse) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.RegistrarLogConsumoWSSodimacResponse(obeLogWSSodimacResponse)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function CompletarUrlRespuesta(ByVal urlrequest As String, ByVal obeOrdenPago As BEOrdenPago, ByVal tipoServNotificacion As Integer, ByRef urlEstado As Boolean) As String
        Dim tramaRequest As String = ""
        Try
            Using odaServicio As New DAServicio()
                Dim obeServicio As BEServicio = odaServicio.ConsultarServicioCabeceraPorId(obeOrdenPago.IdServicio)
                Using traductor As New BLTraductorContratoServ(obeServicio.ClaveAPI, obeOrdenPago)
                    tramaRequest = traductor.EnsamblarTramaQueryString(urlrequest, True)
                End Using
            End Using
        Catch ex As Exception
            tramaRequest = urlrequest
            urlEstado = False
        End Try
        Return tramaRequest
    End Function


    Private Shared Function ReadFileBytesFromS3(ByVal fileName As String) As Byte()
        Dim s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("LLAVES_S3_ACCESS_KEY")
        Dim s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("LLAVES_S3_SECRET_ACCESS_KEY")
        Dim s3Region As String = ConfigurationManager.AppSettings.[Get]("LLAVES_S3_REGION")

        Dim s3BucketName As String = ConfigurationManager.AppSettings.[Get]("LLAVES_S3_BUCKET_NAME")
        Dim s3DirectoryPath As String = ConfigurationManager.AppSettings.[Get]("LLAVES_S3_DIRECTORY_PATH")

        Dim amazonS3 = New SPE.Utilitario.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)
        fileName = fileName.Replace("\\", "\")
        Dim fileNameS3 = Path.GetFileName(fileName)
        Dim fileStream = amazonS3.ReadStreamFromFile(fileNameS3)
        Dim bytes = fileStream.ToArray
        Return bytes
    End Function

#Region "Version 2 - Peru.Com FaseIII"
    Public Function CompletarUrlRespuestaV2(ByVal urlrequest As String, ByVal obeOrdenPago As BEOrdenPago, ByRef urlEstado As Boolean) As String
        Dim resultado As String = ""
        Try
            Using odaServicio As New DAServicio()
                Using oDAOrdenPago As New DAOrdenPago()
                    Dim xmlSolicitud As String = oDAOrdenPago.WSConsultarSolicitudPagoPorIdOrdenPago(obeOrdenPago)
                    Dim oDASeguridad As New DASeguridad
                    Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(obeOrdenPago.IdServicio)

                    '_logger.Trace(String.Format("CompletarUrlRespuestaV2 - IdOrdenPago:{0}, Xml:{1}, PublicKey:{2}", obeOrdenPago.IdOrdenPago, xmlSolicitud, pathPublicKey))

                    'Dim textoCifrado As String = Encripter.EncryptText(xmlSolicitud, pathPublicKey)


                    Dim obeEncripter As New Encripter()
                    obeEncripter.clavePrivada = ReadFileBytesFromS3("SPE_PrivateKey.1pz")
                    Dim publicKey As String = pathPublicKey.Substring(pathPublicKey.Length - 17, 17)
                    obeEncripter.clavePublicaContraparte = ReadFileBytesFromS3(publicKey)
                    Dim textoCifrado As String() = obeEncripter.Cifrar(xmlSolicitud)

                    Dim xmlEncriptado As String = textoCifrado(0) + "|" + textoCifrado(1)


                    '_logger.Trace(String.Format("CompletarUrlRespuestaV2 - IdOrdenPago:{0}, TextoCifrado:{1}", obeOrdenPago.IdOrdenPago, textoCifrado))

                    If (urlrequest <> "") Then
                        'Dim tramaRequest As String = String.Format("{0}={1}&{2}={3}", "data", textoCifrado, "version", obeOrdenPago.VersionOrdenPago)
                        Dim tramaRequest As String = String.Format("{0}={1}&{2}={3}", "data", xmlEncriptado, "version", obeOrdenPago.VersionOrdenPago)
                        Dim oUri As New Uri(urlrequest)
                        '_logger.Trace(String.Format("CompletarUrlRespuestaV2 - IdOrdenPago:{0}, Query:{1}, UrlRequest:{2}, TramaRequest:{3}", obeOrdenPago.IdOrdenPago, oUri.Query, urlrequest, tramaRequest))
                        If oUri.Query = "" Then
                            resultado = String.Format("{0}?{1}", urlrequest, tramaRequest)
                        Else
                            resultado = String.Format("{0}&{1}", urlrequest, tramaRequest)
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception
            urlEstado = False
            resultado = urlrequest
        End Try
        Return resultado
    End Function

    Public Function CompletarUrlRespuestaV2Solicitud(ByVal urlrequest As String, ByVal obeSolicitud As BESolicitudPago) As String
        Dim resultado As String = ""
        Using odaServicio As New DAServicio()
            Using oDAOrdenPago As New DAOrdenPago()
                Dim xmlSolicitud As String = oDAOrdenPago.WSConsultarSolicitudPago(obeSolicitud)
                Dim oDASeguridad As New DASeguridad
                Dim pathPublicKey As String = oDASeguridad.GetKeyPublic(obeSolicitud.IdServicio)
                If (urlrequest <> "") Then
                    Dim tramaRequest As String = String.Format("{0}={1}&{2}={3}", "data", Encripter.EncryptText(xmlSolicitud, pathPublicKey), "version", 2)
                    Dim oUri As New Uri(urlrequest)
                    If oUri.Query = "" Then
                        resultado = String.Format("{0}?{1}", urlrequest, tramaRequest)
                    Else
                        resultado = String.Format("{0}&{1}", urlrequest, tramaRequest)
                    End If
                End If
            End Using
        End Using
        Return resultado
    End Function
#End Region


    Public Function ConsultarServicioNotificacionPorIdEstado(ByVal idEstado As Integer) As List(Of BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarServicioNotificacionPorIdEstado(idEstado)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    'SagaInicio
    Public Function ConsultarDatosParaServicioSaga(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarDatosParaServicioSaga(IdOrdenPago)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarOrdenesPagoSaga() As List(Of BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarOrdenesPagoSaga()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogConsumoWSSagaRequest(ByVal obeLogWSSagaRequest As BELogWebServiceSagaRequest) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.RegistrarLogConsumoWSSagaRequest(obeLogWSSagaRequest)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogConsumoWSSagaResponse(ByVal obeLogWSSagaResponse As BELogWebServiceSagaResponse) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.RegistrarLogConsumoWSSagaResponse(obeLogWSSagaResponse)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    'SagaFin


    'RipleyInicio
    Public Function ConsultarDatosParaServicioRipley(ByVal IdOrdenPago As Int64) As BEServicioNotificacion
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarDatosParaServicioRipley(IdOrdenPago)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function ConsultarOrdenesPagoRipley() As List(Of BEServicioNotificacion)
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.ConsultarOrdenesPagoRipley()
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogConsumoWSRipleyRequest(ByVal obeLogWSRipleyRequest As BELogWebServiceRipleyRequest) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.RegistrarLogConsumoWSRipleyRequest(obeLogWSRipleyRequest)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function

    Public Function RegistrarLogConsumoWSRipleyResponse(ByVal obeLogWSRipleyResponse As BELogWebServiceRipleyResponse) As Integer
        Try
            Using odaServicioNotificacion As New DAServicioNotificacion()
                Return odaServicioNotificacion.RegistrarLogConsumoWSRipleyResponse(obeLogWSRipleyResponse)
            End Using
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
            Throw ex
        End Try
    End Function
    'RipleyFin

End Class
