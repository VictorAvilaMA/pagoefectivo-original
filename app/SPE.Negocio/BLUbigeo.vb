Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos


Public Class BLUbigeo

    '************************************************************************************************** 
    ' M�todo          : ConsultarPais
    ' Descripci�n     : Consulta de Paises
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : 
    ' Parametros_Out  : Lista de entidades BEPais
    ''**************************************************************************************************
    Public Function ConsultarPais() As List(Of BEPais)
        '
        Dim odaUbigeo As New DAUbigeo
        Return odaUbigeo.ConsultarPais()
        '
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarDepartamentoPorIdPais
    ' Descripci�n     : Consulta de departamento por Pais
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : IdPais
    ' Parametros_Out  : Lista de entidades BEDepartamento
    ''**************************************************************************************************
    Public Function ConsultarDepartamentoPorIdPais(ByVal IdPais As Integer) As List(Of BEDepartamento)
        '
        Dim odaUbigeo As New DAUbigeo
        Return odaUbigeo.ConsultarDepartamentoPorIdPais(IdPais)
        '
    End Function

    '************************************************************************************************** 
    ' M�todo          : ConsultarCiudadPorIdDepartamento
    ' Descripci�n     : Consulta de Ciudades por departamento
    ' Autor           : Cesar Miranda
    ' Fecha/Hora      : 20/11/2008
    ' Parametros_In   : IdDepartamento
    ' Parametros_Out  : Lista de entidades BECiudad
    ''**************************************************************************************************
    Public Function ConsultarCiudadPorIdDepartamento(ByVal IdDepartamento As Integer) As List(Of BECiudad)
        '
        Dim odaUbigeo As New DAUbigeo
        Return odaUbigeo.ConsultarCiudadPorIdDepartamento(IdDepartamento)
        '
    End Function

End Class
