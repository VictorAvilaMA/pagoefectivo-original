Imports SPE.Entidades
Imports SPE.AccesoDatos


Public Class CacheServer


    Private Shared _estadoNotificacionErrorActiva As Object = Nothing

    Public Shared Function EstadoNotificacionErrorActiva() As Boolean
        If (_estadoNotificacionErrorActiva Is Nothing) Then
            Using obl As New BLParametro()
                Dim listParametros As List(Of Entidades.BEParametro) = obl.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoActNotifURLError)
                If (listParametros.Count > 0) Then
                    _estadoNotificacionErrorActiva = listParametros(0).Descripcion = "1"
                Else : _estadoNotificacionErrorActiva = False
                End If
            End Using
        End If
        Return _estadoNotificacionErrorActiva
    End Function

    Private Shared _listaMonedaBanco As List(Of BEMonedaBanco) = Nothing
    Public Shared Function ListaMonedaBanco() As List(Of BEMonedaBanco)
        If (_listaMonedaBanco Is Nothing) Then
            Using odaComun As New DAComun()
                _listaMonedaBanco = odaComun.ConsultarMonedaBanco()
            End Using
        End If
        Return _listaMonedaBanco
    End Function

    Public Shared Sub InicializarTodoCache()
        InicializarElementoCache(SPE.EmsambladoComun.ParametrosSistema.GrupoActNotifURLError)
    End Sub

    Public Shared Sub InicializarElementoCache(ByVal nombre As String)
        Select Case nombre
            Case SPE.EmsambladoComun.ParametrosSistema.GrupoActNotifURLError
                _estadoNotificacionErrorActiva = Nothing
            Case SPE.EmsambladoComun.ParametrosSistema.GrupoMonedaBanco
                _estadoNotificacionErrorActiva = Nothing
        End Select
    End Sub

End Class
