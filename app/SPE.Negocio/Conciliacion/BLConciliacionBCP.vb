Imports System
Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLConciliacionBCP
    Inherits BLConciliacionBase
    Implements IBLConciliacion

    Public Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) Implements IBLConciliacion.ProcesarArchivoConciliacion
        'Try
        '    Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        '    Using str As New IO.StringReader(Texto)
        '        Do
        '            Dim Linea As String = str.ReadLine()
        '            If Linea.StartsWith("DD") Then
        '                Dim ConciliacionBCP As BEConciliacionDetalle = Nothing
        '                Using oDAConciliacion As New DAConciliacion
        '                    Dim IdDetConciliacionBCP As Int64 = oDAConciliacion.RegistrarDetConciliacionBCP(oBEConciliacionArchivo.IdConciliacionArchivo, _
        '                        Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
        '                    Try
        '                        Using Transaccion As New TransactionScope
        '                            ConciliacionBCP = oDAConciliacion.ProcesarDetConciliacionBCP(IdDetConciliacionBCP)
        '                            If ConciliacionBCP.CancelarCIP Then
        '                                If ConciliacionBCP.CIP.Substring(0, 2) = "99" Then
        '                                    Dim oDADV As New DADineroVirtual
        '                                    Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
        '                                    oBECuentaDineroVirtual.Numero = ConciliacionBCP.CIP
        '                                    oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
        '                                    Dim oBEMovimiento As New BEMovimiento
        '                                    With oBEMovimiento
        '                                        .CodigoServicio = ConciliacionBCP.CodigoServicio
        '                                        .NumeroOperacion = ConciliacionBCP.NumeroOperacion
        '                                        .IdAperturaOrigen = CType(ConciliacionBCP.CodigoAgenciaBancaria, Integer)
        '                                        .CodigoBanco = Conciliacion.CodigoBancos.BCP
        '                                    End With
        '                                    oBECuentaDineroVirtual.MontoRecarga = ConciliacionBCP.Monto
        '                                    If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ConciliacionBCP.CodigoServicio, oBEMovimiento) > 0 Then
        '                                        oDAConciliacion.ActualizarDetConciliacionBCP(ConciliacionBCP.IdConciliacionDetalle, _
        '                                                ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
        '                                        ConciliacionBCP.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
        '                                        ConciliacionBCP.Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
        '                                        Transaccion.Complete()
        '                                    End If
        '                                Else
        '                                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
        '                                        Dim oBEMovimiento As New BEMovimiento
        '                                        With oBEMovimiento
        '                                            .NumeroOrdenPago = ConciliacionBCP.CIP
        '                                            .CodMonedaBanco = ConciliacionBCP.CodMonedaBanco
        '                                            .Monto = ConciliacionBCP.Monto
        '                                            .NumeroOperacion = ConciliacionBCP.NumeroOperacion
        '                                            .CodigoAgenciaBancaria = ConciliacionBCP.CodigoAgenciaBancaria
        '                                            .CodigoBanco = Conciliacion.CodigoBancos.BCP
        '                                            .CodigoServicio = ConciliacionBCP.CodigoServicio
        '                                        End With
        '                                        Dim FechaCancelacion As New DateTime(Linea.Substring(57, 4), _
        '                                                                             Linea.Substring(61, 2), _
        '                                                                             Linea.Substring(63, 2))
        '                                        If oBLAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancariaComun(oBEMovimiento, _
        '                                            ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
        '                                            oDAConciliacion.ActualizarDetConciliacionBCP(ConciliacionBCP.IdConciliacionDetalle, _
        '                                                ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
        '                                            ConciliacionBCP.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
        '                                            ConciliacionBCP.Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
        '                                            Transaccion.Complete()
        '                                        End If
        '                                    End Using
        '                                End If
        '                            Else
        '                                Transaccion.Complete()
        '                            End If
        '                        End Using
        '                    Catch ex As Exception
        '                        ConciliacionBCP.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
        '                        Logger.Write(ex)
        '                    End Try
        '                End Using
        '                If ConciliacionBCP.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or _
        '                    ConciliacionBCP.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
        '                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ConciliacionBCP)
        '                Else
        '                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ConciliacionBCP)
        '                End If
        '                If ConciliacionBCP.NotificarCreacionAB Then
        '                    Using oBLEmail As New BLEmail()
        '                        oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ConciliacionBCP.CodigoAgenciaBancaria, _
        '                            ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.BCP)
        '                    End Using
        '                End If
        '            End If
        '        Loop While str.Peek <> -1
        '        str.Close()
        '    End Using
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

    Public Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As String Implements IBLConciliacion.RegistrarDetallesArchivoConciliacion
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    If Linea.StartsWith("DD") Then
                        Using oDAConciliacion As New DAConciliacion
                            oDAConciliacion.RegistrarDetConciliacionBCP(oBEConciliacionArchivo.IdConciliacionArchivo, _
                                                                        Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
                        End Using
                        CodigoPago = LTrim(RTrim(Linea.Substring(13, 14)))
                        If CodigoPago.Substring(0, 1) = "9" Then
                            ContRec = ContRec + 1
                        Else
                            ContOP = ContOP + 1
                        End If
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ContOP.ToString & "|" & ContRec.ToString() & "|" & ""
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo, ByRef cm As String) Implements IBLConciliacion.ProcesarDetallesArchivoConciliacion
        Dim ListaDetConciliacionBCP As New List(Of BEConciliacionDetalle)
        Dim ListDetConcXPagar As New List(Of BEConciliacionDetalle)
        Dim ListDetConcNotCreAB As New List(Of BEConciliacionDetalle)

        Dim oDAConciliacion As New DAConciliacion

        Try
            ListaDetConciliacionBCP = oDAConciliacion.ProcesarDetConciliacionBCP(oBEConciliacionArchivo.IdConciliacionArchivo)
            'Pagos por Cancelar.
            Dim a, c, d As Integer
            For a = 0 To ListaDetConciliacionBCP.Count - 1
                If ListaDetConciliacionBCP(a).CancelarCIP Then
                    ListDetConcXPagar.Add(ListaDetConciliacionBCP(a))
                ElseIf ListaDetConciliacionBCP(a).IdEstado = 171 Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListaDetConciliacionBCP(a))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListaDetConciliacionBCP(a))
                End If
                'Para Notificacion
                If ListaDetConciliacionBCP(a).NotificarCreacionAB Then
                    ListDetConcNotCreAB.Add(ListaDetConciliacionBCP(a))
                End If
            Next
            'Cancelar Pagos
            For c = 0 To ListDetConcXPagar.Count - 1
                Try
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        Dim oBEMovimiento As New BEMovimiento
                        If ListDetConcXPagar(c).CancelarCIP Then
                            If ListDetConcXPagar(c).CIP.Substring(0, 2) = "99" Then
                                Dim oDADV As New DADineroVirtual
                                Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
                                oBECuentaDineroVirtual.Numero = ListDetConcXPagar(c).CIP
                                oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
                                With oBEMovimiento
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .IdAperturaOrigen = CType(ListDetConcXPagar(c).CodigoAgenciaBancaria, Integer)
                                    .CodigoBanco = Conciliacion.CodigoBancos.BCP
                                End With
                                oBECuentaDineroVirtual.MontoRecarga = ListDetConcXPagar(c).Monto
                                If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ListDetConcXPagar(c).CodigoServicio, oBEMovimiento) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionBCP(ListDetConcXPagar(c).IdConciliacionDetalle, _
                                            ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
                                End If
                            Else
                                With oBEMovimiento
                                    .NumeroOrdenPago = ListDetConcXPagar(c).CIP
                                    .CodMonedaBanco = ListDetConcXPagar(c).CodMonedaBanco
                                    .Monto = ListDetConcXPagar(c).Monto
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .CodigoAgenciaBancaria = ListDetConcXPagar(c).CodigoAgenciaBancaria
                                    .CodigoBanco = Conciliacion.CodigoBancos.BCP
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                End With
                                Dim FechaTexto As String = ListDetConcXPagar(c).CodigoPuntoVenta
                                Dim FechaCancelacion As New DateTime(FechaTexto.Substring(0, 4), FechaTexto.Substring(4, 2), FechaTexto.Substring(6, 2))
                                If oBLAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancariaComun(oBEMovimiento, ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionBCP(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
                                End If
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
                    'Logger.Write(ex)
                End Try
                If ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListDetConcXPagar(c))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListDetConcXPagar(c))
                End If
            Next
            'Notificar Creacion de AB
            For d = 0 To ListDetConcNotCreAB.Count - 1
                Try
                    Using oBLEmail As New BLEmail()
                        oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ListDetConcNotCreAB(d).CodigoAgenciaBancaria, ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.BCP)
                    End Using
                Catch ex As Exception
                    'Logger.Write(ex)
                End Try
            Next
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub

    Public Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As Boolean Implements IBLConciliacion.ValidarFechaProceso
        Dim TextoArchivo As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        Dim Fecha As New DateTime(TextoArchivo.Substring(14, 4), _
                                  TextoArchivo.Substring(18, 2), _
                                  TextoArchivo.Substring(20, 2))
        Return (oBEConciliacionArchivo.Fecha = Fecha)
    End Function

End Class