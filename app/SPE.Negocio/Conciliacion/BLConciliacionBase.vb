Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun

Public Class BLConciliacionBase
    Implements IDisposable

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
