Imports System
Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLConciliacionFullCarga
    Inherits BLConciliacionBase
    Implements IBLConciliacion

    Public Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) Implements IBLConciliacion.ProcesarArchivoConciliacion
        'Try
        '    Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        '    Dim CodigoMoneda As String = String.Empty
        '    Using str As New IO.StringReader(Texto)
        '        Do
        '            Dim Linea As String = str.ReadLine()
        '            If Linea.StartsWith("01") Then
        '                CodigoMoneda = Linea.Substring(2, 3).Trim()
        '            ElseIf Linea.StartsWith("02") Then
        '                Dim ConciliacionFC As BEConciliacionDetalle = Nothing
        '                Using oDAConciliacion As New DAConciliacion
        '                    Dim IdDetConciliacionFC As Int64 = oDAConciliacion.RegistrarDetConciliacionFC(oBEConciliacionArchivo.IdConciliacionArchivo, _
        '                        Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
        '                    Try
        '                        Using Transaccion As New TransactionScope
        '                            ConciliacionFC = oDAConciliacion.ProcesarDetConciliacionFC(IdDetConciliacionFC, CodigoMoneda)
        '                            If ConciliacionFC.CancelarCIP Then
        '                                If ConciliacionFC.CIP.Substring(0, 2) = "99" Then
        '                                    Dim oDADV As New DADineroVirtual
        '                                    Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
        '                                    oBECuentaDineroVirtual.Numero = ConciliacionFC.CIP
        '                                    oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
        '                                    Dim oBEMovimiento As New BEMovimiento
        '                                    With oBEMovimiento
        '                                        .CodigoServicio = ConciliacionFC.CodigoServicio
        '                                        .NumeroOperacion = ConciliacionFC.NumeroOperacion
        '                                        .IdAperturaOrigen = CType(ConciliacionFC.CodigoMedioPago, Integer)
        '                                        .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
        '                                    End With
        '                                    oBECuentaDineroVirtual.MontoRecarga = ConciliacionFC.Monto
        '                                    If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ConciliacionFC.CodigoServicio, oBEMovimiento) > 0 Then
        '                                        oDAConciliacion.ActualizarDetConciliacionFC(ConciliacionFC.IdConciliacionDetalle, _
        '                                                ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
        '                                        ConciliacionFC.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
        '                                        ConciliacionFC.Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
        '                                        Transaccion.Complete()
        '                                    End If
        '                                Else
        '                                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
        '                                        Dim oBEMovimiento As New BEMovimiento
        '                                        With oBEMovimiento
        '                                            .NumeroOrdenPago = ConciliacionFC.CIP
        '                                            .NumeroOperacion = ConciliacionFC.NumeroOperacion
        '                                            .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
        '                                            .CodMonedaBanco = ConciliacionFC.CodMonedaBanco
        '                                            .Monto = ConciliacionFC.Monto
        '                                            .CodigoMedioPago = ConciliacionFC.CodigoMedioPago
        '                                            .CodigoServicio = ConciliacionFC.CodigoServicio
        '                                            .NumeroSerieTerminal = ConciliacionFC.NumeroSerieTerminal
        '                                            .CodigoPuntoVenta = ConciliacionFC.CodigoPuntoVenta
        '                                        End With
        '                                        Dim FechaCancelacion As New DateTime(Linea.Substring(61, 4), _
        '                                                                             Linea.Substring(65, 2), _
        '                                                                             Linea.Substring(67, 2))
        '                                        If oBLAgenciaBancaria.PagarDesdeBancoComun(oBEMovimiento, _
        '                                            ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
        '                                            oDAConciliacion.ActualizarDetConciliacionFC(ConciliacionFC.IdConciliacionDetalle, _
        '                                                ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
        '                                            ConciliacionFC.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
        '                                            ConciliacionFC.Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
        '                                            Transaccion.Complete()
        '                                        End If
        '                                    End Using
        '                                End If
        '                            Else
        '                                Transaccion.Complete()
        '                            End If
        '                        End Using
        '                    Catch ex As Exception
        '                        ConciliacionFC = New BEConciliacionDetalle()
        '                        ConciliacionFC.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
        '                        Logger.Write(ex)
        '                    End Try
        '                End Using
        '                If ConciliacionFC.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or _
        '                    ConciliacionFC.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
        '                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ConciliacionFC)
        '                Else
        '                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ConciliacionFC)
        '                End If
        '                If ConciliacionFC.NotificarCreacionT Then
        '                    Using oBLEmail As New BLEmail()
        '                        oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(ConciliacionFC.NotificarCreacionPV, _
        '                            ConciliacionFC.CodigoPuntoVenta, ConciliacionFC.NumeroSerieTerminal, ParametrosSistema.ProcesoConciliacion)
        '                    End Using
        '                End If
        '            End If
        '        Loop While str.Peek <> -1
        '        str.Close()
        '    End Using
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

    Public Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As String Implements IBLConciliacion.RegistrarDetallesArchivoConciliacion
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Dim CodigoMoneda As String = String.Empty
            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    If Linea.StartsWith("01") Then
                        CodigoMoneda = Linea.Substring(2, 3).Trim()
                    ElseIf Linea.StartsWith("02") Then
                        Using oDAConciliacion As New DAConciliacion
                            oDAConciliacion.RegistrarDetConciliacionFC(oBEConciliacionArchivo.IdConciliacionArchivo, _
                                Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
                        End Using
                        CodigoPago = LTrim(RTrim(Linea.Substring(2, 14)))
                        If CodigoPago.Substring(0, 1) = "9" Then
                            ContRec = ContRec + 1
                        Else
                            ContOP = ContOP + 1
                        End If
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ContOP.ToString & "|" & ContRec.ToString() & "|" & CodigoMoneda
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As Boolean Implements IBLConciliacion.ValidarFechaProceso
        Dim TextoArchivo As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        Dim Fecha As New DateTime(TextoArchivo.Substring(5, 4), _
                                  TextoArchivo.Substring(9, 2), _
                                  TextoArchivo.Substring(11, 2))
        Return (oBEConciliacionArchivo.Fecha = Fecha)
    End Function

    Public Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo, ByRef cm As String) Implements IBLConciliacion.ProcesarDetallesArchivoConciliacion
        Dim ListaDetConciliacionFC As New List(Of BEConciliacionDetalle)
        Dim ListDetConcXPagar As New List(Of BEConciliacionDetalle)
        Dim ListDetConcNotCrePV As New List(Of BEConciliacionDetalle)

        Dim oDAConciliacion As New DAConciliacion

        Try
            ListaDetConciliacionFC = oDAConciliacion.ProcesarDetConciliacionFC(oBEConciliacionArchivo.IdConciliacionArchivo, cm)
            'Pagos por Cancelar.
            Dim a, c, d As Integer
            For a = 0 To ListaDetConciliacionFC.Count - 1
                If ListaDetConciliacionFC(a).CancelarCIP Then
                    ListDetConcXPagar.Add(ListaDetConciliacionFC(a))
                ElseIf ListaDetConciliacionFC(a).IdEstado = 171 Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListaDetConciliacionFC(a))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListaDetConciliacionFC(a))
                End If
                'Para Notificacion
                If ListaDetConciliacionFC(a).NotificarCreacionT Then
                    ListDetConcNotCrePV.Add(ListaDetConciliacionFC(a))
                End If
            Next
            'Cancelar Pagos
            For c = 0 To ListDetConcXPagar.Count - 1
                Try
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        'Dim oBEMovimiento As New BEMovimiento
                        If ListDetConcXPagar(c).CancelarCIP Then

                            If ListDetConcXPagar(c).CIP.Substring(0, 2) = "99" Then
                                Dim oDADV As New DADineroVirtual
                                Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
                                oBECuentaDineroVirtual.Numero = ListDetConcXPagar(c).CIP
                                oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
                                Dim oBEMovimiento As New BEMovimiento
                                With oBEMovimiento
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .IdAperturaOrigen = CType(ListDetConcXPagar(c).CodigoMedioPago, Integer)
                                    .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
                                End With
                                oBECuentaDineroVirtual.MontoRecarga = ListDetConcXPagar(c).Monto
                                If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ListDetConcXPagar(c).CodigoServicio, oBEMovimiento) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionFC(ListDetConcXPagar(c).IdConciliacionDetalle,ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
                                End If
                            Else
                                'Using oBLAgenciaBancaria As New BLAgenciaBancaria
                                Dim oBEMovimiento As New BEMovimiento
                                With oBEMovimiento
                                    .NumeroOrdenPago = ListDetConcXPagar(c).CIP
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .CodigoBanco = Conciliacion.CodigoBancos.FullCarga
                                    .CodMonedaBanco = ListDetConcXPagar(c).CodMonedaBanco
                                    .Monto = ListDetConcXPagar(c).Monto
                                    .CodigoMedioPago = ListDetConcXPagar(c).CodigoMedioPago
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                    .NumeroSerieTerminal = ListDetConcXPagar(c).NumeroSerieTerminal
                                    .CodigoPuntoVenta = ListDetConcXPagar(c).CodigoPuntoVenta
                                End With
                                Dim FechaTexto As String = ListDetConcXPagar(c).CodigoAgenciaBancaria
                                Dim FechaCancelacion As New DateTime(FechaTexto.Substring(0, 4), FechaTexto.Substring(4, 2), FechaTexto.Substring(6, 2))
                                If oBLAgenciaBancaria.PagarDesdeBancoComun(oBEMovimiento,ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionFC(ListDetConcXPagar(c).IdConciliacionDetalle,ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
                                End If
                                'End Using
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
                    'Logger.Write(ex)
                End Try
                If ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListDetConcXPagar(c))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListDetConcXPagar(c))
                End If
            Next
            'Notificar Creacion de AB
            For d = 0 To ListDetConcNotCrePV.Count - 1
                Try
                    Using oBLEmail As New BLEmail()
                        oBLEmail.EnviarCorreoGeneracionPuntoVentaYTerminal(ListDetConcXPagar(c).NotificarCreacionPV, ListDetConcXPagar(c).CodigoPuntoVenta, ListDetConcXPagar(c).NumeroSerieTerminal, ParametrosSistema.ProcesoConciliacion)
                    End Using
                Catch ex As Exception
                    'Logger.Write(ex)
                End Try
            Next
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub
    
End Class
