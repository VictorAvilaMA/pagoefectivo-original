Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Configuration
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun
Imports SPE.Utilitario
Public Class BLConciliacionIBK
    Inherits BLConciliacionBase
    Implements IBLConciliacion


    Public Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) Implements IBLConciliacion.ProcesarArchivoConciliacion
        'No se utiliza
    End Sub

    Public Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo, ByRef cm As String) Implements IBLConciliacion.ProcesarDetallesArchivoConciliacion
        Dim ListaDetConciliacionIBK As New List(Of BEConciliacionDetalle)
        Dim ListDetConcXPagar As New List(Of BEConciliacionDetalle)
        Dim ListDetConcNotCreAB As New List(Of BEConciliacionDetalle)

        Dim oDAConciliacion As New DAConciliacion

        Try
            ListaDetConciliacionIBK = oDAConciliacion.ProcesarDetConciliacionIBK(oBEConciliacionArchivo.IdConciliacionArchivo)
            'Pagos por Cancelar.
            Dim a, c, d As Integer
            For a = 0 To ListaDetConciliacionIBK.Count - 1
                If ListaDetConciliacionIBK(a).CancelarCIP Then
                    ListDetConcXPagar.Add(ListaDetConciliacionIBK(a))
                ElseIf ListaDetConciliacionIBK(a).IdEstado = 171 Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListaDetConciliacionIBK(a))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListaDetConciliacionIBK(a))
                End If
                'Para Notificacion
                If ListaDetConciliacionIBK(a).NotificarCreacionAB Then
                    ListDetConcNotCreAB.Add(ListaDetConciliacionIBK(a))
                End If
            Next
            'Cancelar Pagos
            For c = 0 To ListDetConcXPagar.Count - 1
                Try
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        Dim oBEMovimiento As New BEMovimiento
                        If ListDetConcXPagar(c).CancelarCIP Then
                            If ListDetConcXPagar(c).CIP.Substring(0, 2) = "99" Then
                                Dim oDADV As New DADineroVirtual
                                Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
                                oBECuentaDineroVirtual.Numero = ListDetConcXPagar(c).CIP
                                oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
                                With oBEMovimiento
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .IdAperturaOrigen = CType(ListDetConcXPagar(c).CodigoAgenciaBancaria, Integer)
                                    .CodigoBanco = Conciliacion.CodigoBancos.Interbank
                                End With
                                oBECuentaDineroVirtual.MontoRecarga = ListDetConcXPagar(c).Monto
                                If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ListDetConcXPagar(c).CodigoServicio, oBEMovimiento) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionIBK(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
                                End If
                            Else
                                With oBEMovimiento
                                    .NumeroOrdenPago = ListDetConcXPagar(c).CIP
                                    .CodMonedaBanco = ListDetConcXPagar(c).CodMonedaBanco
                                    .Monto = ListDetConcXPagar(c).Monto
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .CodigoAgenciaBancaria = ListDetConcXPagar(c).CodigoAgenciaBancaria
                                    .CodigoBanco = Conciliacion.CodigoBancos.Interbank
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                End With
                                Dim FechaTexto As String = ListDetConcXPagar(c).CodigoPuntoVenta
                                Dim FechaCancelacion As New DateTime(FechaTexto.Substring(0, 4), FechaTexto.Substring(4, 2), FechaTexto.Substring(6, 2))
                                If oBLAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancariaComun(oBEMovimiento, ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionIBK(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
                                End If
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
                    'Logger.Write(ex)
                End Try
                If ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListDetConcXPagar(c))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListDetConcXPagar(c))
                End If
            Next
            'Notificar Creacion de AB
            For d = 0 To ListDetConcNotCreAB.Count - 1
                Try
                    Using oBLEmail As New BLEmail()
                        oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ListDetConcNotCreAB(d).CodigoAgenciaBancaria, ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.Interbank)
                    End Using
                Catch ex As Exception
                    'Logger.Write(ex)
                End Try
            Next
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub

    Public Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As String Implements IBLConciliacion.RegistrarDetallesArchivoConciliacion
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Dim Linea As String = ""
            Using str As New IO.StringReader(Texto)
                Do
                    Linea = str.ReadLine()
                    Using oDAConciliacion As New DAConciliacion
                        CodigoPago = LTrim(RTrim(Linea.Substring(9, 20)))
                        If CodigoPago.Substring(0, 1) = "9" Then
                            ContRec = ContRec + 1
                        Else
                            ContOP = ContOP + 1
                        End If
                        oDAConciliacion.RegistrarDetConciliacionIBK(oBEConciliacionArchivo.IdConciliacionArchivo, Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
                    End Using
                Loop While str.Peek <> -1
                str.Close()
                Return ContOP.ToString & "|" & ContRec.ToString() & "|" & ""
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As Boolean Implements IBLConciliacion.ValidarFechaProceso
        'En caso sea variable el nombre en los Caracteres Antes de la Fecha  CAF******.txt
        'En caso suceda este caso si se desea subir el archivo mas de una vez tendria que modificarse delante del nombre.
        'Dim Nombre As String = oBEConciliacionArchivo.NombreArchivo
        'Dim TamNom As Integer = oBEConciliacionArchivo.NombreArchivo.ToString().Length()
        'Dim TamFijoFecYExt As Integer = 10
        'Dim ColFecIni As Integer = TamNom - TamFijoFecYExt
        'Dim Fecha As New DateTime("20" & Nombre.Substring(ColFecIni, 2), _
        '                          Nombre.Substring(ColFecIni + 2, 2), _
        '                          Nombre.Substring(ColFecIni + 4, 2))
        'Return (oBEConciliacionArchivo.Fecha = Fecha)
        Dim Nombre As String = oBEConciliacionArchivo.NombreArchivo
        Dim TamFijoFecYExt As Integer = 9
        Dim Fecha As New DateTime("20" & Nombre.Substring(TamFijoFecYExt, 2), _
                                  Nombre.Substring(TamFijoFecYExt + 2, 2), _
                                  Nombre.Substring(TamFijoFecYExt + 4, 2))
        Return (oBEConciliacionArchivo.Fecha = Fecha)
    End Function
End Class
