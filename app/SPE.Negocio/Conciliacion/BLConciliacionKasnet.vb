﻿Imports System
Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLConciliacionKasnet
    Inherits BLConciliacionBase
    Implements IBLConciliacion


    Public Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) Implements IBLConciliacion.ProcesarArchivoConciliacion
        'Try
        '    Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        '    Dim CodigoMoneda As String = String.Empty
        '    Using str As New IO.StringReader(Texto)
        '        Do
        '            Dim Linea As String = str.ReadLine()
        '            If Linea.StartsWith("01") Then
        '                CodigoMoneda = Linea.Substring(16, 3)
        '            ElseIf Linea.StartsWith("02") Then
        '                Dim ConciliacionBBVA As BEConciliacionDetalle = Nothing
        '                Using oDAConciliacion As New DAConciliacion
        '                    Dim IdDetConciliacionBBVA As Int64 = oDAConciliacion.RegistrarDetConciliacionBBVA(oBEConciliacionArchivo.IdConciliacionArchivo, _
        '                        Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
        '                    Try
        '                        Using Transaccion As New TransactionScope
        '                            ConciliacionBBVA = oDAConciliacion.ProcesarDetConciliacionBBVA(IdDetConciliacionBBVA, CodigoMoneda)
        '                            If ConciliacionBBVA.CancelarCIP Then
        '                                If ConciliacionBBVA.CIP.Substring(0, 2) = "99" Then
        '                                    Dim oDADV As New DADineroVirtual
        '                                    Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
        '                                    oBECuentaDineroVirtual.Numero = ConciliacionBBVA.CIP
        '                                    oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
        '                                    Dim oBEMovimiento As New BEMovimiento
        '                                    With oBEMovimiento
        '                                        .CodigoServicio = ConciliacionBBVA.CodigoServicio
        '                                        .NumeroOperacion = ConciliacionBBVA.NumeroOperacion
        '                                        .IdAperturaOrigen = CType(ConciliacionBBVA.CodigoAgenciaBancaria, Integer)
        '                                        .CodigoBanco = Conciliacion.CodigoBancos.BBVA
        '                                    End With
        '                                    oBECuentaDineroVirtual.MontoRecarga = ConciliacionBBVA.Monto
        '                                    If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ConciliacionBBVA.CodigoServicio, oBEMovimiento) > 0 Then
        '                                        oDAConciliacion.ActualizarDetConciliacionBBVA(ConciliacionBBVA.IdConciliacionDetalle, _
        '                                                ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
        '                                        ConciliacionBBVA.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
        '                                        ConciliacionBBVA.Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
        '                                        Transaccion.Complete()
        '                                    End If
        '                                Else
        '                                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
        '                                        Dim oBEMovimiento As New BEMovimiento
        '                                        With oBEMovimiento
        '                                            .NumeroOrdenPago = ConciliacionBBVA.CIP
        '                                            .NumeroOperacion = ConciliacionBBVA.NumeroOperacion
        '                                            .CodigoBanco = Conciliacion.CodigoBancos.BBVA
        '                                            .CodMonedaBanco = ConciliacionBBVA.CodMonedaBanco
        '                                            .Monto = ConciliacionBBVA.Monto
        '                                            .CodigoMedioPago = ConciliacionBBVA.CodigoMedioPago
        '                                            .CodigoServicio = ConciliacionBBVA.CodigoServicio
        '                                            .CodigoAgenciaBancaria = ConciliacionBBVA.CodigoAgenciaBancaria
        '                                        End With
        '                                        Dim FechaCancelacion As New DateTime(Linea.Substring(135, 4), _
        '                                                                             Linea.Substring(139, 2), _
        '                                                                             Linea.Substring(141, 2))
        '                                        If oBLAgenciaBancaria.PagarDesdeBancoComun(oBEMovimiento, _
        '                                            ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
        '                                            oDAConciliacion.ActualizarDetConciliacionBBVA(ConciliacionBBVA.IdConciliacionDetalle, _
        '                                                ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
        '                                            ConciliacionBBVA.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
        '                                            ConciliacionBBVA.Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
        '                                            Transaccion.Complete()
        '                                        End If
        '                                    End Using
        '                                End If
        '                            Else
        '                                Transaccion.Complete()
        '                            End If
        '                        End Using
        '                    Catch ex As Exception
        '                        ConciliacionBBVA.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
        '                        Logger.Write(ex)
        '                    End Try
        '                End Using
        '                If ConciliacionBBVA.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or _
        '                    ConciliacionBBVA.IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
        '                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ConciliacionBBVA)
        '                Else
        '                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ConciliacionBBVA)
        '                End If
        '                If ConciliacionBBVA.NotificarCreacionAB Then
        '                    Using oBLEmail As New BLEmail()
        '                        oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ConciliacionBBVA.CodigoAgenciaBancaria, _
        '                            ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.BBVA)
        '                    End Using
        '                End If

        '            End If
        '        Loop While str.Peek <> -1
        '        str.Close()
        '    End Using
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

    Public Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As String Implements IBLConciliacion.RegistrarDetallesArchivoConciliacion
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Dim Linea As String = ""
            Using str As New IO.StringReader(Texto)
                Do
                    Linea = str.ReadLine()
                    If Linea.StartsWith("01") Then
                        'Cabecera
                    ElseIf Linea.StartsWith("02") Then
                        Using oDAConciliacion As New DAConciliacion
                            CodigoPago = LTrim(RTrim(Linea.Substring(120, 14)))
                            If CodigoPago.Substring(0, 1) = "9" Then
                                ContRec = ContRec + 1
                            Else
                                ContOP = ContOP + 1
                            End If
                            oDAConciliacion.RegistrarDetConciliacionKasnet(oBEConciliacionArchivo.IdConciliacionArchivo, Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
                        End Using
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ContOP.ToString & "|" & ContRec.ToString() & "|" & ""
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As Boolean Implements IBLConciliacion.ValidarFechaProceso
        Dim TextoArchivo As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        Dim Fecha As New DateTime(TextoArchivo.Substring(67, 4), _
                                  TextoArchivo.Substring(71, 2), _
                                  TextoArchivo.Substring(73, 2))
        Return (oBEConciliacionArchivo.Fecha = Fecha)
    End Function

    Public Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo, ByRef cm As String) Implements IBLConciliacion.ProcesarDetallesArchivoConciliacion
        Dim ListaDetConciliacionKasnet As New List(Of BEConciliacionDetalle)
        Dim ListDetConcXPagar As New List(Of BEConciliacionDetalle)
        Dim ListDetConcNotCreAB As New List(Of BEConciliacionDetalle)

        Dim oDAConciliacion As New DAConciliacion

        Try
            ListaDetConciliacionKasnet = oDAConciliacion.ProcesarDetConciliacionKasnet(oBEConciliacionArchivo.IdConciliacionArchivo)
            'Pagos por Cancelar.
            Dim a, c As Integer
            For a = 0 To ListaDetConciliacionKasnet.Count - 1
                If ListaDetConciliacionKasnet(a).CancelarCIP Then
                    ListDetConcXPagar.Add(ListaDetConciliacionKasnet(a))
                ElseIf ListaDetConciliacionKasnet(a).IdEstado = 171 Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListaDetConciliacionKasnet(a))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListaDetConciliacionKasnet(a))
                End If
                'Para Notificacion
                If ListaDetConciliacionKasnet(a).NotificarCreacionAB Then
                    ListDetConcNotCreAB.Add(ListaDetConciliacionKasnet(a))
                End If
            Next
            'Cancelar Pagos
            For c = 0 To ListDetConcXPagar.Count - 1
                Try
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        'Dim oBEMovimiento As New BEMovimiento
                        If ListDetConcXPagar(c).CancelarCIP Then

                            If ListDetConcXPagar(c).CIP.Substring(0, 2) = "99" Then
                                'Dim oDADV As New DADineroVirtual
                                'Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
                                'oBECuentaDineroVirtual.Numero = ListDetConcXPagar(c).CIP
                                'oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
                                'Dim oBEMovimiento As New BEMovimiento
                                'With oBEMovimiento
                                '    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                '    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                '    .IdAperturaOrigen = CType(ListDetConcXPagar(c).CodigoAgenciaBancaria, Integer)
                                '    .CodigoBanco = Conciliacion.CodigoBancos.BanBif
                                'End With
                                'oBECuentaDineroVirtual.MontoRecarga = ListDetConcXPagar(c).Monto
                                'If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ListDetConcXPagar(c).CodigoServicio, oBEMovimiento) > 0 Then
                                '    oDAConciliacion.ActualizarDetConciliacionBBVA(ListDetConcXPagar(c).IdConciliacionDetalle, _
                                '            ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                '    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                '    ListDetConcXPagar(c).Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
                                'End If
                            Else
                                'Using oBLAgenciaBancaria As New BLAgenciaBancaria
                                Dim oBEMovimiento As New BEMovimiento
                                With oBEMovimiento
                                    .NumeroOrdenPago = ListDetConcXPagar(c).CIP
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .CodigoBanco = Conciliacion.CodigoBancos.Kasnet
                                    .CodMonedaBanco = ListDetConcXPagar(c).CodMonedaBanco
                                    .Monto = ListDetConcXPagar(c).Monto
                                    .CodigoMedioPago = ListDetConcXPagar(c).CodigoMedioPago
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                    .CodigoAgenciaBancaria = ListDetConcXPagar(c).CodigoAgenciaBancaria
                                End With
                                Dim FechaTexto As String = ListDetConcXPagar(c).CodigoPuntoVenta
                                Dim FechaCancelacion As New DateTime(FechaTexto.Substring(0, 4), FechaTexto.Substring(4, 2), FechaTexto.Substring(6, 2))
                                If oBLAgenciaBancaria.PagarDesdeBancoComun(oBEMovimiento, ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionBIF(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
                                End If
                                'End Using
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
                    'Logger.Write(ex)
                End Try
                If ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListDetConcXPagar(c))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListDetConcXPagar(c))
                End If
            Next
            'Notificar Creacion de AB
            'For d = 0 To ListDetConcNotCreAB.Count - 1
            '    Try
            '        Using oBLEmail As New BLEmail()
            '            oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ListDetConcNotCreAB(d).CodigoAgenciaBancaria, ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.BanBif)
            '        End Using
            '    Catch ex As Exception
            '        Logger.Write(ex)
            '    End Try
            'Next
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub
End Class