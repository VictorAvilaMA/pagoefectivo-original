Imports System
Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLConciliacionSBK
    Inherits BLConciliacionBase
    Implements IBLConciliacion


    Public Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) Implements IBLConciliacion.ProcesarArchivoConciliacion
        'No se esta utilizando
        'Try
        '    ProcesarDetallesArchivoConciliacion(oBEConciliacionArchivo)
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

    Public Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As String Implements IBLConciliacion.RegistrarDetallesArchivoConciliacion
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    If Linea.StartsWith("D") Then
                        Using oDAConciliacion As New DAConciliacion
                            oDAConciliacion.RegistrarDetConciliacionSBK(oBEConciliacionArchivo.IdConciliacionArchivo, _
                                                                        Linea, oBEConciliacionArchivo.IdUsuarioCreacion)
                        End Using
                        CodigoPago = LTrim(RTrim(Linea.Substring(18, 14)))
                        If CodigoPago.Substring(0, 1) = "9" Then
                            ContRec = ContRec + 1
                        Else
                            ContOP = ContOP + 1
                        End If
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ContOP.ToString & "|" & ContRec.ToString() & "|" & ""
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo, ByRef cm As String) Implements IBLConciliacion.ProcesarDetallesArchivoConciliacion
        Dim ListaDetConciliacionSBK As New List(Of BEConciliacionDetalle)
        Dim ListDetConcXPagar As New List(Of BEConciliacionDetalle)
        Dim ListDetConcNotCreAB As New List(Of BEConciliacionDetalle)

        Dim oDAConciliacion As New DAConciliacion
        'Dim ConciliacionSBK As BEConciliacionDetalle = Nothing
        Try
            ListaDetConciliacionSBK = oDAConciliacion.ProcesarDetConciliacionSBK(oBEConciliacionArchivo.IdConciliacionArchivo)
            'CIP's por Cancelar.
            Dim a, b, c, d As Integer
            For a = 0 To ListaDetConciliacionSBK.Count - 1
                If ListaDetConciliacionSBK(a).CancelarCIP Then
                    ListDetConcXPagar.Add(ListaDetConciliacionSBK(a))
                ElseIf ListaDetConciliacionSBK(a).IdEstado = 171 Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListaDetConciliacionSBK(a))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListaDetConciliacionSBK(a))
                End If
            Next
            'CIP's Notifican Creacio de AB
            For b = 0 To ListaDetConciliacionSBK.Count - 1
                If ListaDetConciliacionSBK(b).NotificarCreacionAB Then
                    ListDetConcNotCreAB.Add(ListaDetConciliacionSBK(b))
                End If
            Next
            'Cancelar CIP's
            For c = 0 To ListDetConcXPagar.Count - 1
                Try
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        Dim oBEMovimiento As New BEMovimiento
                        With oBEMovimiento
                            .NumeroOrdenPago = ListDetConcXPagar(c).CIP
                            .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                            .CodigoBanco = Conciliacion.CodigoBancos.Scotiabank
                            .CodMonedaBanco = ListDetConcXPagar(c).CodMonedaBanco
                            .Monto = ListDetConcXPagar(c).Monto
                            .CodigoMedioPago = ListDetConcXPagar(c).CodigoMedioPago
                            .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                            .CodigoAgenciaBancaria = ListDetConcXPagar(c).CodigoAgenciaBancaria
                        End With
                        Dim FechaTexto As String = ListDetConcXPagar(c).CodigoPuntoVenta
                        Dim FechaCancelacion As New DateTime(FechaTexto.Substring(0, 4), FechaTexto.Substring(4, 2), FechaTexto.Substring(6, 2))
                        If oBLAgenciaBancaria.PagarDesdeBancoComun_Conciliacion_Diaria(oBEMovimiento, ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
                            oDAConciliacion.ActualizarDetConciliacionSBK(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                            ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                            ListDetConcXPagar(c).Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
                        End If
                    End Using
                Catch ex As Exception
                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
                    'Logger.Write(ex)
                End Try
                If ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListDetConcXPagar(c))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListDetConcXPagar(c))
                End If
            Next
            'Notificar Creacion de AB
            For d = 0 To ListDetConcNotCreAB.Count - 1
                Try
                    Using oBLEmail As New BLEmail()
                        oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ListDetConcNotCreAB(d).CodigoAgenciaBancaria, ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank)
                    End Using
                Catch ex As Exception
                    'Logger.Write(ex)
                End Try
            Next
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub

    Public Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As Boolean Implements IBLConciliacion.ValidarFechaProceso
        Dim TextoArchivo As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        Dim Fecha As New DateTime(TextoArchivo.Substring(52, 4), _
                                  TextoArchivo.Substring(56, 2), _
                                  TextoArchivo.Substring(58, 2))
        Return (oBEConciliacionArchivo.Fecha = Fecha)
    End Function

End Class