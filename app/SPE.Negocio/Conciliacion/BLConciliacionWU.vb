﻿Imports System
Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

Public Class BLConciliacionWU
    Inherits BLConciliacionBase
    Implements IBLConciliacion

    Public Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) Implements IBLConciliacion.ProcesarArchivoConciliacion
        'Con el nuevo proceso ya no se usa
    End Sub

    Public Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo, ByRef cm As String) Implements IBLConciliacion.ProcesarDetallesArchivoConciliacion
        Dim ListaDetConciliacionWU As New List(Of BEConciliacionDetalle)
        Dim ListDetConcXPagar As New List(Of BEConciliacionDetalle)
        Dim ListDetConcNotCreAB As New List(Of BEConciliacionDetalle)

        Dim oDAConciliacion As New DAConciliacion

        Try
            ListaDetConciliacionWU = oDAConciliacion.ProcesarDetConciliacionWU(oBEConciliacionArchivo.IdConciliacionArchivo)
            'Pagos por Cancelar.
            Dim a, c, d As Integer
            For a = 0 To ListaDetConciliacionWU.Count - 1
                If ListaDetConciliacionWU(a).CancelarCIP Then
                    ListDetConcXPagar.Add(ListaDetConciliacionWU(a))
                ElseIf ListaDetConciliacionWU(a).IdEstado = 171 Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListaDetConciliacionWU(a))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListaDetConciliacionWU(a))
                End If
                'Para Notificacion
                If ListaDetConciliacionWU(a).NotificarCreacionAB Then
                    ListDetConcNotCreAB.Add(ListaDetConciliacionWU(a))
                End If
            Next
            'Cancelar Pagos
            For c = 0 To ListDetConcXPagar.Count - 1
                Try
                    Using oBLAgenciaBancaria As New BLAgenciaBancaria
                        Dim oBEMovimiento As New BEMovimiento
                        If ListDetConcXPagar(c).CancelarCIP Then
                            If ListDetConcXPagar(c).CIP.Substring(0, 2) = "99" Then
                                Dim oDADV As New DADineroVirtual
                                Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
                                oBECuentaDineroVirtual.Numero = ListDetConcXPagar(c).CIP
                                oBECuentaDineroVirtual = oDADV.ConsultarCuentaDineroVirtualUsuarioByNumero(oBECuentaDineroVirtual)
                                With oBEMovimiento
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .IdAperturaOrigen = CType(ListDetConcXPagar(c).CodigoAgenciaBancaria, Integer)
                                    .CodigoBanco = Conciliacion.CodigoBancos.WesterUnion
                                End With
                                oBECuentaDineroVirtual.MontoRecarga = ListDetConcXPagar(c).Monto
                                If oDADV.RecargarCuenta(oBECuentaDineroVirtual, ListDetConcXPagar(c).CodigoServicio, oBEMovimiento) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionWU(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. La recarga tuvo que realizarse en el proceso de conciliación."
                                End If
                            Else
                                With oBEMovimiento
                                    .NumeroOrdenPago = ListDetConcXPagar(c).CIP
                                    .CodMonedaBanco = ListDetConcXPagar(c).CodMonedaBanco
                                    .Monto = ListDetConcXPagar(c).Monto
                                    .NumeroOperacion = ListDetConcXPagar(c).NumeroOperacion
                                    .CodigoAgenciaBancaria = ListDetConcXPagar(c).CodigoAgenciaBancaria
                                    .CodigoBanco = Conciliacion.CodigoBancos.WesterUnion
                                    .CodigoServicio = ListDetConcXPagar(c).CodigoServicio
                                End With
                                Dim FechaTexto As String = ListDetConcXPagar(c).CodigoPuntoVenta
                                Dim FechaCancelacion As New DateTime(FechaTexto.Substring(0, 4), FechaTexto.Substring(4, 2), FechaTexto.Substring(6, 2))
                                If oBLAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancariaComun(oBEMovimiento, ServicioNotificacion.IdOrigenRegistro.ProcesoConciliacion, FechaCancelacion) > 0 Then
                                    oDAConciliacion.ActualizarDetConciliacionWU(ListDetConcXPagar(c).IdConciliacionDetalle, ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado)
                                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado
                                    ListDetConcXPagar(c).Observacion = "Conciliación correcta. El CIP tuvo que ser cancelado en el proceso de conciliación."
                                End If
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
                    'Logger.Write(ex)
                End Try
                If ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.Conciliado Or ListDetConcXPagar(c).IdEstado = ParametrosSistema.Conciliacion.EstadosConciliacion.ConciliadoModificado Then
                    oBEConciliacionArchivo.ListaOperacionesConciliadas.Add(ListDetConcXPagar(c))
                Else
                    oBEConciliacionArchivo.ListaOperacionesNoConciliadas.Add(ListDetConcXPagar(c))
                End If
            Next
            'Notificar Creacion de AB
            For d = 0 To ListDetConcNotCreAB.Count - 1
                Try
                    Using oBLEmail As New BLEmail()
                        oBLEmail.EnviarCorreoGeneracionAgenciaBancaria(ListDetConcNotCreAB(d).CodigoAgenciaBancaria, ProcesoConciliacion, ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion)
                    End Using
                Catch ex As Exception
                    'Logger.Write(ex)
                End Try
            Next
        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub

    Public Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As String Implements IBLConciliacion.RegistrarDetallesArchivoConciliacion
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Dim Linea As String = ""
            Using str As New IO.StringReader(Texto)
                Dim Linea1 As String = ""
                Dim Linea2 As String = ""
                Dim Linea3 As String = ""
                Do
                    Linea = str.ReadLine()
                    If Linea.StartsWith("5") Then
                        Linea1 = Linea
                        CodigoPago = LTrim(RTrim(Linea1.Substring(25, 27)))
                        If CodigoPago.Substring(0, 1) = "9" Then
                            ContRec = ContRec + 1
                        Else
                            ContOP = ContOP + 1
                        End If
                    ElseIf Linea.StartsWith("6") Then
                        Linea2 = Linea
                    ElseIf Linea.StartsWith("7") Then
                        Linea3 = Linea
                        Using oDAConciliacion As New DAConciliacion
                            oDAConciliacion.RegistrarDetConciliacionWU(oBEConciliacionArchivo.IdConciliacionArchivo, Linea1, Linea2, Linea3, oBEConciliacionArchivo.IdUsuarioCreacion)
                        End Using
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ContOP.ToString & "|" & ContRec.ToString() & "|" & ""
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As Entidades.BEConciliacionArchivo) As Boolean Implements IBLConciliacion.ValidarFechaProceso
        Dim TextoArchivo As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEConciliacionArchivo.Bytes)
        Dim Fecha As New DateTime(TextoArchivo.Substring(1, 4), _
                                  TextoArchivo.Substring(5, 2), _
                                  TextoArchivo.Substring(7, 2))
        Return (oBEConciliacionArchivo.Fecha = Fecha)
    End Function
End Class
