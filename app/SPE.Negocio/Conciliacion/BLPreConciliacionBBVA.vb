﻿Imports System
Imports System.Transactions
Imports System.Linq
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema
Imports System.Text.RegularExpressions

Public Class BLPreConciliacionBBVA
    Inherits BLConciliacionBase
    Implements IBLPreConciliacion

    Public Function CargarDataConciliacion(Reader As Excel.IExcelDataReader) As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion) Implements IBLPreConciliacion.CargarDataConciliacion
        Dim ListaDataPreConciliacion As New List(Of BEDataPreConciliacion)
        While Reader.Read
            Dim DataConciliacion As New BEDataPreConciliacion
            With DataConciliacion
                .Campo0 = Convert.ToString(Reader(0))
                .Campo1 = Convert.ToString(Reader(1))
                .Campo2 = Convert.ToString(Reader(2))
                .Campo3 = Convert.ToString(Reader(3))
                .Campo4 = Convert.ToString(Reader(4))
                .Campo5 = Convert.ToString(Reader(5))
                .Campo6 = Convert.ToString(Reader(6))
            End With
            ListaDataPreConciliacion.Add(DataConciliacion)
        End While
        Return ListaDataPreConciliacion
    End Function

    Public Function CargarDataConciliacionBCP(ByRef oBEPreConciliacionRequest As Entidades.BEPreConciliacionRequest, ByRef strIdPreConciliacion As String) As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion) Implements IBLPreConciliacion.CargarDataConciliacionBCP
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEPreConciliacionRequest.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Dim ListaDataPreConciliacion As New List(Of BEDataPreConciliacion)

            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    If Linea.StartsWith("DD") Then

                        Dim DataConciliacion As New BEDataPreConciliacion
                        Dim LineaMonto = Linea.Substring(74, 15).TrimStart("0"c).ToString
                        Dim strSigno = IIf(Linea.Substring(197, 1) = "E", "-", "")
                        With DataConciliacion
                            .Campo0 = strIdPreConciliacion
                            .Campo1 = Linea.Substring(14, 14)
                            .Campo2 = strSigno + LineaMonto.Substring(0, LineaMonto.ToString.Length - 3) + "." + LineaMonto.Substring(LineaMonto.ToString.Length - 2, 2)
                            .Campo3 = Linea.Substring(52, 6)
                            .Campo4 = Linea.Substring(218, 8)
                            .Campo5 = ""
                            .Campo6 = ""
                        End With
                        ListaDataPreConciliacion.Add(DataConciliacion)
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ListaDataPreConciliacion
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function

    Public Function ProcesarArchivoPreConciliacion(ByRef request As Entidades.BEPreConciliacionRequest, ListaDataPreConciliacion As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion)) As System.Collections.Generic.List(Of Entidades.BEConciliacionDetalle) Implements IBLPreConciliacion.ProcesarArchivoPreConciliacion
        ListaDataPreConciliacion.Reverse()
        Dim ListaBEConciliacionDetalle As List(Of BEConciliacionDetalle)
        Using oDAConciliacion As New DAConciliacion
            Try
                oDAConciliacion.RegistrarDetPreConciliacionBBVA(request.Archivo.IdConciliacionArchivo, ListaDataPreConciliacion)
                ListaBEConciliacionDetalle = oDAConciliacion.ProcesarDetArchivoPreConciliacion(request)
                ''''''''''''''''
                Dim a, b As Integer
                Dim TotalListExtornos, TotalListPagos As Integer
                Dim ListaExtornos As New List(Of BEConciliacionDetalle)
                Dim ListaPagos As New List(Of BEConciliacionDetalle)
                For a = 0 To ListaBEConciliacionDetalle.Count - 1
                    If ListaBEConciliacionDetalle(a).IdEstado = 693 Then
                        ListaExtornos.Add(ListaBEConciliacionDetalle(a))
                    Else
                        ListaPagos.Add(ListaBEConciliacionDetalle(a))
                    End If
                Next
                ListaBEConciliacionDetalle = ListaPagos
                For b = 0 To ListaBEConciliacionDetalle.Count - 1
                    TotalListPagos = ListaBEConciliacionDetalle.Where(Function(be As BEConciliacionDetalle) be.CIP = ListaBEConciliacionDetalle(b).CIP And be.NumeroSerieTerminal = ListaBEConciliacionDetalle(b).NumeroSerieTerminal).ToList().Count()
                    TotalListExtornos = ListaExtornos.Where(Function(be As BEConciliacionDetalle) be.Monto = ListaBEConciliacionDetalle(b).Monto And be.NumeroSerieTerminal = ListaBEConciliacionDetalle(b).NumeroSerieTerminal).ToList().Count()
                    If ((TotalListPagos - TotalListExtornos) = 0) Then
                        ListaBEConciliacionDetalle(b).IdEstado = 692
                        ListaBEConciliacionDetalle(b).Observacion = "El Codigo de Pago ha sido extornado, verificar en BD."
                    End If
                Next
                ListaBEConciliacionDetalle.Reverse() 'Para asegurar el primer resultado
                ''''''''''''''''
                Dim c, d As Integer
                Dim ContRep As Integer
                Dim ListaFinal As New List(Of BEConciliacionDetalle)
                For c = 0 To ListaBEConciliacionDetalle.Count - 1
                    ContRep = ListaFinal.Where(Function(be As BEConciliacionDetalle) be.CIP = ListaBEConciliacionDetalle(c).CIP).ToList().Count()
                    If ContRep = 0 Then
                        ListaFinal.Add(ListaBEConciliacionDetalle(c))
                    End If
                Next
                For d = 0 To ListaFinal.Count - 1
                    If ListaFinal(d).IdEstado = 691 Then
                        request.ListaOperacionesConciliadas.Add(ListaFinal(d))
                    Else
                        request.ListaOperacionesNoConciliadas.Add(ListaFinal(d))
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return ListaBEConciliacionDetalle
    End Function
End Class
