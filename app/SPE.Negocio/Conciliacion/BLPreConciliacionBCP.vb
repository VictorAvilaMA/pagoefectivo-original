﻿Imports System
Imports System.Transactions
Imports System.Linq
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema
Imports System.Text.RegularExpressions

Public Class BLPreConciliacionBCP
    Inherits BLConciliacionBase
    Implements IBLPreConciliacion

    Public Function CargarDataConciliacion(Reader As Excel.IExcelDataReader) As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion) Implements IBLPreConciliacion.CargarDataConciliacion
        Dim ListaDataPreConciliacion As New List(Of BEDataPreConciliacion)
        While Reader.Read
            Dim DataConciliacion As New BEDataPreConciliacion
            With DataConciliacion
                .Campo0 = Convert.ToString(Reader(0))
                .Campo1 = Convert.ToString(Reader(1))
                .Campo2 = Convert.ToString(Reader(2))
                .Campo3 = Convert.ToString(Reader(3))
                .Campo4 = Convert.ToString(Reader(4))
                .Campo5 = Convert.ToString(Reader(5))
                .Campo6 = Convert.ToString(Reader(6))
            End With
            ListaDataPreConciliacion.Add(DataConciliacion)
        End While
        Return ListaDataPreConciliacion
    End Function

    Public Function CargarDataConciliacionBCP(ByRef oBEPreConciliacionRequest As Entidades.BEPreConciliacionRequest, ByRef strIdPreConciliacion As String) As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion) Implements IBLPreConciliacion.CargarDataConciliacionBCP
        Try
            Dim Texto As String = _3Dev.FW.Util.DataUtil.BytesToString(oBEPreConciliacionRequest.Bytes)
            Dim ContOP As Integer = 0
            Dim ContRec As Integer = 0
            Dim CodigoPago As String = ""
            Dim ListaDataPreConciliacion As New List(Of BEDataPreConciliacion)

            Using str As New IO.StringReader(Texto)
                Do
                    Dim Linea As String = str.ReadLine()
                    If Linea.StartsWith("DD") Then
                        Dim DataConciliacion As New BEDataPreConciliacion
                        Dim LineaMonto = Linea.Substring(73, 15).TrimStart("0"c).ToString
                        Dim strSigno = IIf(Linea.Substring(196, 1) = "E", "-", "")
                        With DataConciliacion
                            .Campo0 = strIdPreConciliacion
                            .Campo1 = Linea.Substring(13, 14)
                            .Campo2 = strSigno + LineaMonto.Substring(0, LineaMonto.ToString.Length - 2) + "." + LineaMonto.Substring(LineaMonto.ToString.Length - 2, 2)
                            .Campo3 = Linea.Substring(117, 6)
                            .Campo4 = Linea.Substring(217, 8)
                            .Campo5 = IIf(Linea.Substring(196, 1) = "E", "692", "691")
                            .Campo6 = IIf(Linea.Substring(196, 1) = "E", "El Codigo de Pago aparece como no pagado, pero el Banco indica lo contrario, verificar en BD", "Validacion Correcta.")
                        End With
                        ListaDataPreConciliacion.Add(DataConciliacion)
                    End If
                Loop While str.Peek <> -1
                str.Close()
                Return ListaDataPreConciliacion
            End Using
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
    End Function



    Public Function ProcesarArchivoPreConciliacion(ByRef request As Entidades.BEPreConciliacionRequest, ListaDataPreConciliacion As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion)) As System.Collections.Generic.List(Of Entidades.BEConciliacionDetalle) Implements IBLPreConciliacion.ProcesarArchivoPreConciliacion
        'Dim RowIniDetalle = 8 'Para excel BCP
        'Dim CantTotalReg As Integer = ListaDataPreConciliacion.Count()
        'Dim CantRowDetalle = CantTotalReg - 8


        Dim oBEAgenciaBancaria As New Entidades.BEOperador()
        Dim Numero As Int32
        Numero = 1 + Convert.ToInt32(oBEAgenciaBancaria.Codigo)
        'Dim ValorPrueba As Long
        'ValorPrueba = "Jeffer"



        ListaDataPreConciliacion.Reverse()
        Dim ListaBEConciliacionDetalle As List(Of BEConciliacionDetalle)
        Using oDAConciliacion As New DAConciliacion
            Try
                oDAConciliacion.RegistrarDetPreConciliacionBCP(request.Archivo.IdConciliacionArchivo, ListaDataPreConciliacion)
                ListaBEConciliacionDetalle = oDAConciliacion.ProcesarDetArchivoPreConciliacion(request)
                ''''''''''''''''
                Dim a As Integer
                Dim ListaExtornos As New List(Of BEConciliacionDetalle)
                Dim ListaPagos As New List(Of BEConciliacionDetalle)
                For a = 0 To ListaBEConciliacionDetalle.Count - 1
                    If ListaBEConciliacionDetalle(a).IdEstado = 693 Then
                        ListaExtornos.Add(ListaBEConciliacionDetalle(a))
                    Else
                        ListaPagos.Add(ListaBEConciliacionDetalle(a))
                    End If
                Next
                'ListaBEConciliacionDetalle = ListaPagos
                'For b = 0 To ListaBEConciliacionDetalle.Count - 1
                '    TotalListPagos = ListaBEConciliacionDetalle.Where(Function(be As BEConciliacionDetalle) be.CIP = ListaBEConciliacionDetalle(b).CIP And be.NumeroSerieTerminal = ListaBEConciliacionDetalle(b).NumeroSerieTerminal).ToList().Count()
                '    'For e = 0 To ListaExtornos.Count - 1
                '    TotalListExtornos = ListaExtornos.Where(Function(be As BEConciliacionDetalle) be.Monto = ListaBEConciliacionDetalle(b).Monto And be.NumeroSerieTerminal = ListaBEConciliacionDetalle(b).NumeroSerieTerminal).ToList().Count()
                '    'Next
                '    If ((TotalListPagos - TotalListExtornos) = 0) Then
                '        ListaBEConciliacionDetalle(b).IdEstado = 692
                '        ListaBEConciliacionDetalle(b).Observacion = "El Codigo de Pago ha sido extornado, verificar en BD."
                '    End If
                'Next
                'ListaBEConciliacionDetalle.Reverse() 'Para asegurar el primer resultado
                ''''''''''''''''
                Dim c, d As Integer
                Dim ContRep As Integer
                Dim ListaFinal As New List(Of BEConciliacionDetalle)
                For c = 0 To ListaBEConciliacionDetalle.Count - 1
                    ContRep = ListaFinal.Where(Function(be As BEConciliacionDetalle) be.CIP = ListaBEConciliacionDetalle(c).CIP).ToList().Count()
                    If ContRep = 0 Then
                        ListaFinal.Add(ListaBEConciliacionDetalle(c))
                    End If
                Next
                For d = 0 To ListaFinal.Count - 1
                    If ListaFinal(d).IdEstado = 691 Then
                        request.ListaOperacionesConciliadas.Add(ListaFinal(d))
                    Else
                        request.ListaOperacionesNoConciliadas.Add(ListaFinal(d))
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Using
        Return ListaBEConciliacionDetalle
    End Function

    'Public Function CargarConciliacionArchivo(DataConciliacion As System.Collections.Generic.List(Of Entidades.BEDataPreConciliacion)) As Entidades.BEConciliacionArchivo Implements IBLPreConciliacion.CargarConciliacionArchivo
    '    Dim ConciliacionArchivo As New BEConciliacionArchivo
    '    Try
    '        Dim CoincidenciasLote As CaptureCollection = New Regex("\d+").Match(DataConciliacion(0).Campo0).Captures
    '        If CoincidenciasLote Is Nothing Then
    '            Throw New Exception()
    '        ElseIf Not CoincidenciasLote.Count = 1 Then
    '            Throw New Exception()
    '        Else
    '            ConciliacionArchivo.Lote = CoincidenciasLote(0).Value
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception("El archivo no presenta número de Lote.")
    '    End Try
    '    Return ConciliacionArchivo
    'End Function    
End Class
