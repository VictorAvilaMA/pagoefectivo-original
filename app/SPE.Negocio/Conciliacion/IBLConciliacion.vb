Imports SPE.Entidades

Public Interface IBLConciliacion
    Inherits IDisposable

    ''' <summary>
    ''' Procesar archivo de conciliación
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">
    ''' Objeto BEConciliacionArchivo
    ''' </param>
    ''' <remarks></remarks>
    Sub ProcesarArchivoConciliacion(ByRef oBEConciliacionArchivo As BEConciliacionArchivo)

    ''' <summary>
    ''' Valida la fecha del proceso ingresada con la del archivo de conciliación
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">
    ''' Objeto BEConciliacionArchivo
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function ValidarFechaProceso(ByVal oBEConciliacionArchivo As BEConciliacionArchivo) As Boolean

    ''' <summary>
    ''' Registra los detalles del archivo de conciliación
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">Objeto BEConciliacionArchivo</param>
    ''' <remarks></remarks>
    Function RegistrarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As BEConciliacionArchivo) As String

    ''' <summary>
    ''' Procesa los detalles del archivo de conciliación
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">Objeto BEConciliacionArchivo</param>
    ''' <remarks></remarks>
    Sub ProcesarDetallesArchivoConciliacion(ByRef oBEConciliacionArchivo As BEConciliacionArchivo, ByRef cm As String)

End Interface