﻿Imports SPE.Entidades
Public Interface IBLPreConciliacion
    Inherits IDisposable

    ''' <summary>
    ''' Cargar datos de IExcelDataReader a instancia de BEDataConciliacion
    ''' </summary>
    ''' <param name="Reader">Objeto IExcelDataReader</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function CargarDataConciliacion(ByVal Reader As Excel.IExcelDataReader) As List(Of BEDataPreConciliacion)

    ' ''' <summary>
    ' ''' Carga los datos de la cabecera del archivo de conciliación
    ' ''' </summary>
    ' ''' <param name="DataConciliacion">Data del archivo enviado</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Function CargarConciliacionArchivo(ByVal DataConciliacion As List(Of BEDataPreConciliacion)) As BEConciliacionArchivo

    Function CargarDataConciliacionBCP(ByRef oBEPreConciliacionRequest As Entidades.BEPreConciliacionRequest, ByRef strIdPreConciliacion As String) As List(Of BEDataPreConciliacion)

    ' ''' <summary>
    ' ''' Carga los datos de la cabecera del archivo de preconciliación
    ' ''' </summary>
    ' ''' <param name="DataConciliacion">Data del archivo enviado</param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Function CargarConciliacionArchivo(ByVal DataConciliacion As List(Of BEDataPreConciliacion)) As BEConciliacionArchivo

    ''' <summary>
    ''' Procesar archivo de conciliación
    ''' </summary>
    ''' <param name="oBEConciliacionArchivo">Objeto BEConciliacionArchivo</param>
    ''' <param name="ListaDataPreConciliacion">Data del archivo enviado</param>
    ''' <remarks></remarks>
    Function ProcesarArchivoPreConciliacion(ByRef oBEConciliacionArchivo As BEPreConciliacionRequest, ByVal ListaDataPreConciliacion As List(Of BEDataPreConciliacion)) As List(Of BEConciliacionDetalle)

End Interface
