Imports System.Collections.Generic
Imports System.Xml

Public Class BLContratoRespuesta
    Implements IDisposable

    Dim _xmlDoc As Xml.XmlDocument = Nothing
    Dim _xmlInformacionCIP As XmlNode = Nothing
    Public Sub New(ByVal tiempoExpiracion As Decimal, ByVal fechaEmision As DateTime)
        _xmlDoc = New XmlDocument()

        _xmlInformacionCIP = _xmlDoc.CreateElement("InformacionCIP")
        _xmlDoc.AppendChild(_xmlInformacionCIP)

        Dim nodotExpiracion As XmlNode = _xmlDoc.CreateElement("TiempoExpiracion")
        nodotExpiracion.InnerText = tiempoExpiracion
        _xmlInformacionCIP.AppendChild(nodotExpiracion)

        Dim nodoFechaEmision As XmlNode = _xmlDoc.CreateElement("FechaEmision")
        'nodoFechaEmision.InnerText = fechaEmision.ToString("dd/MM/yyyy HH:mm:ss")
        nodoFechaEmision.InnerText = String.Format("{0}/{1}/{2} {3}:{4}:{5}", fechaEmision.Day.ToString("00"), fechaEmision.Month.ToString("00"), fechaEmision.Year, fechaEmision.Hour.ToString("00"), fechaEmision.Minute.ToString("00"), fechaEmision.Second.ToString("00")) '  fechaEmision.ToString("dd/MM/yyyy HH:mm:ss")
        _xmlInformacionCIP.AppendChild(nodoFechaEmision)

    End Sub
    Public Function ObtenerDocumento() As XmlDocument
        Return _xmlDoc
    End Function

    Public Function ObtenerXMLString() As String
        Using sw As New IO.StringWriter
            Using xw As XmlTextWriter = New XmlTextWriter(sw)
                _xmlDoc.WriteTo(xw)
            End Using
            Return sw.ToString()
        End Using
    End Function


    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
                If (_xmlDoc IsNot Nothing) Then
                    _xmlDoc = Nothing
                End If
                If (_xmlInformacionCIP IsNot Nothing) Then
                    _xmlInformacionCIP = Nothing
                End If
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#Region "Integracion Servicios V2"
    Public Shared Function DictionaryToXml(ByVal MainNode As String, ByVal Elements As Dictionary(Of String, String)) As String
        Dim xmlDoc = New XmlDocument()
        Dim xmlNode As XmlNode = xmlDoc.CreateElement(MainNode)

        For Each Element As KeyValuePair(Of String, String) In Elements
            Dim nodo As XmlNode = xmlDoc.CreateElement(Element.Key)
            nodo.InnerText = Element.Value
            xmlNode.AppendChild(nodo)
        Next

        xmlDoc.AppendChild(xmlNode)
        Using sw As New IO.StringWriter
            Using xw As XmlTextWriter = New XmlTextWriter(sw)
                xmlDoc.WriteTo(xw)
            End Using
            Return sw.ToString()
        End Using
    End Function
#End Region

End Class
