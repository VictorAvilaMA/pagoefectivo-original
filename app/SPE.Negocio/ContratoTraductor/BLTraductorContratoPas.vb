Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Xml

Imports _3Dev.FW.Negocio
Imports _3Dev.FW.Util.DataUtil

Public Class BLTraductorContratoPas
    Inherits _3Dev.FW.Negocio.BLBase

    Dim _xmlStrContrato As String = Nothing     ' formato de estructura del contrato
    Dim _xmlStrContenido As String = Nothing    ' contenido encriptado
    Dim _codigoApi As String = Nothing          ' codigo API
    Dim _codigoPas As String = Nothing          ' codigo de la pasarela-servicio

    Public Sub New()

    End Sub

    Public Sub New(ByVal codPas As String, ByVal xmlContenido As String)

        _codigoPas = codPas
        _xmlStrContenido = xmlContenido

    End Sub

    Private Function getContratoPasarelaXML() As EsquemaTrama

        Dim pXmlDocument As New XmlDocument
        Dim nsmgr As XmlNamespaceManager
        Dim nodoPasarela As XmlNode
        Dim root As XmlElement
        Dim pXNodo As XmlNode

        Dim oTramaNodo As EsquemaTramaNodo

        Dim oEsquemaTrama As EsquemaTrama
        Dim oEsquemaTramaNodo As Dictionary(Of String, EsquemaTramaNodo)

        Dim campos As String() = Nothing

        Try

            'VALIDAR CODIGO API
            If _codigoPas Is Nothing Then
                Throw New Exception("No se ha ingreado el codigo del servicio-pasarela")
            End If

            'obteniendo formato de contrato del XML
            Using oDAPasarela As New DAPasarela
                _xmlStrContrato = oDAPasarela.ConsultarContratoXMLPasarela()
            End Using

            'cargar el XML
            pXmlDocument.LoadXml(_xmlStrContrato)

            'agregar name space
            nsmgr = New XmlNamespaceManager(pXmlDocument.NameTable)
            nsmgr.AddNamespace("bk", "urn:contratoXML")

            ' obtener xml elemento
            root = pXmlDocument.DocumentElement

            ' obtener nodo del servicio - pasarela
            nodoPasarela = root.SelectSingleNode("descendant::Servicio[@api='" + _codigoPas + "']", nsmgr)

            If (nodoPasarela Is Nothing) Then
                Throw New Exception("El contrato asociado al Codigo del Servicio - Pasarela No Existe.")
            End If

            'array de campos segun la estructura
            campos = New String() {EsquemaTrama.PCClave, EsquemaTrama.EOrdenId.ToLower, _
                                   EsquemaTrama.PTipoTarjeta, EsquemaTrama.PMonedaId, _
                                    EsquemaTrama.EMonto.ToLower, EsquemaTrama.EUsuarioID.ToLower, _
                                    EsquemaTrama.EUrlOk.ToLower, EsquemaTrama.EUrlError.ToLower, _
                                    EsquemaTrama.PEmail, EsquemaTrama.PNombres, EsquemaTrama.PApellidos}

            'nodo encontrado proceder a obtener los datos de su estructura
            oEsquemaTrama = New EsquemaTrama

            With oEsquemaTrama
                'codigo
                .CodigoApi = nodoPasarela.Attributes(EsquemaTrama.ACodigo).Value
                'descripcion
                .Descripcion = nodoPasarela.Attributes(EsquemaTrama.ADescripcion).Value

            End With

            oEsquemaTramaNodo = New Dictionary(Of String, EsquemaTramaNodo)

            For Each campo As String In campos

                pXNodo = nodoPasarela.SelectSingleNode(campo)

                If pXNodo Is Nothing Then Continue For

                oTramaNodo = New EsquemaTramaNodo

                With oTramaNodo
                    .NodoId = pXNodo.Name
                    .Nombre = pXNodo.Attributes("nombre").Value
                    .Requerido = pXNodo.Attributes("requerido").Value
                    .ValorPorDefecto = pXNodo.Attributes("porDefecto").Value

                    oEsquemaTramaNodo.Add(campo, oTramaNodo)

                End With

            Next

            oEsquemaTrama.EsquemaNodo = oEsquemaTramaNodo

            'validar nro de campos
            If campos.Length <> oEsquemaTramaNodo.Count Then
                Throw New Exception("No tiene la cantidad de Elementos Necesarios")
            End If

            Return oEsquemaTrama

        Catch ex As Exception
            Throw New Exception(String.Format("El Contrato XML no tiene el formato correcto. Error: {0}", ex.Message))
        End Try

    End Function

    Public Sub DesamblarTramaEnBEPasarela(ByVal oEsquemaTrama As EsquemaTrama)

        Dim oPasarela As New BEPasarela
        Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = oEsquemaTrama.EsquemaNodo
        Dim StrXmlTmpContenido As String

        Try

            'desencriptando la trama
            Using libx As New Utilitario.ClsLibreriax
                StrXmlTmpContenido = libx.Decrypt(_xmlStrContenido, libx.fEncriptaKey)
            End Using

        Catch ex As Exception
        End Try
    End Sub

    Private Function ObtenerValorBEOrdenPagoDeXMLContenido(ByVal id As String, _
                                                            ByVal pxmlContenido As XmlDocument, _
                                                            ByVal xpath As String, _
                                                            ByVal _esquemaTrama As EsquemaTrama) As String

        Dim result As String = ""
        If (_esquemaTrama Is Nothing) Then
            Throw New Exception("BLTraductorContratoServ.AsignarValorBEOrdenPagoDeXMLContenido Error: Primero debe ejecutar el Metodo ValidarFormatoXML()")
        End If
        Dim nodoServicio As XmlNode
        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(pxmlContenido.NameTable)
        If Not nsmgr.HasNamespace("bk") Then
            nsmgr.AddNamespace("bk", "urn:ServicioXML")
        End If
        Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = _esquemaTrama.EsquemaNodo
        If (_esquemaNodo.ContainsKey(id)) Then
            Dim objnodo As EsquemaTramaNodo = _esquemaNodo(id)
            Dim root As XmlElement = pxmlContenido.DocumentElement

            nodoServicio = root.SelectSingleNode(xpath + objnodo.Nombre, nsmgr)
            If (nodoServicio Is Nothing) Then
                Dim tmpxpath As String = System.Configuration.ConfigurationManager.AppSettings("XMLContratoRaizOpc")
                nodoServicio = root.SelectSingleNode(tmpxpath + objnodo.Nombre, nsmgr)
            End If


            If (nodoServicio IsNot Nothing) Then
                result = nodoServicio.InnerText
            End If
            If (result = "") Then
                result = objnodo.ValorPorDefecto
            End If
            If (objnodo.Requerido And result = "") Then
                Throw New Exception(String.Format("El Campo {0} es requerido en la trama XML. ", objnodo.Nombre))
            End If
        End If

        Return result
    End Function

End Class
