Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Xml

Imports _3Dev.FW.Negocio
Imports _3Dev.FW.Util.DataUtil

Public Class BLTraductorContratoServ
    Inherits _3Dev.FW.Negocio.BLBase

    'Public Enum TipoEnsamblaje
    '    QueryString
    '    Xml
    'End Enum

    Dim _xmlstrContratoServicio As String = Nothing
    Dim _xmlstrContenido As String = Nothing
    Dim _codigoApi As String = Nothing
    Dim _esquemaTrama As EsquemaTrama = Nothing
    Dim _xmlEncriptado As Boolean = Nothing
    Dim _idTipoTrama As Integer = Nothing
    Dim _obeOrdenPago As BEOrdenPago = Nothing

    Public Sub New(ByVal codigoAPI As String, ByVal obeOrdenPago As BEOrdenPago)
        _codigoApi = codigoAPI
        _obeOrdenPago = obeOrdenPago
    End Sub
    Public Sub New(ByVal codigoAPI As String, ByVal xmlContenido As String, ByVal xmlEncriptado As Boolean, ByVal idTipoTrama As Integer)
        _xmlstrContenido = xmlContenido
        _codigoApi = codigoAPI
        _xmlEncriptado = xmlEncriptado
        _idTipoTrama = idTipoTrama
    End Sub

    Private Function CrearXMLDocument(ByVal _xmlContratoServicio As String) As XmlDocument
        Dim _xmlDocument As New XmlDocument()
        _xmlDocument.LoadXml(_xmlContratoServicio)
        Return _xmlDocument
    End Function

    Private Function ObtenerContratoServicioXMLPorAPI(ByVal codAPI) As EsquemaTrama
        Try

            Using odaServicio As New DAServicio()
                Dim xmlTramaServRutaCodAcceso As String = System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRutaCodAcceso")
                _xmlstrContratoServicio = odaServicio.ConsultarContratoXMLDeServicios(xmlTramaServRutaCodAcceso)
            End Using
            Dim _xmlDocument As XmlDocument = CrearXMLDocument(_xmlstrContratoServicio)
            'Dim rootnode As String = "/Servicios/Servicio"

            Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(_xmlDocument.NameTable)
            nsmgr.AddNamespace("bk", "urn:contratoXML")

            'Select the book node with the matching attribute value.
            Dim nodoServicio As XmlNode
            Dim root As XmlElement = _xmlDocument.DocumentElement
            nodoServicio = root.SelectSingleNode("descendant::Servicio[@api='" + codAPI + "']", nsmgr)
            If (nodoServicio Is Nothing) Then
                Throw New Exception("El contrato asociado al Codigo API del Servicio No Existe.")
            Else
                Dim campos As String() = Nothing
                If ((_idTipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Post) Or (_idTipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService)) Then
                    campos = New String() {EsquemaTrama.EDatosEnc, EsquemaTrama.EMerchantId, EsquemaTrama.EOrdenId, EsquemaTrama.EUrlOk, EsquemaTrama.EUrlError, EsquemaTrama.EMailCom, EsquemaTrama.EOrdenDesc, EsquemaTrama.EMoneda, EsquemaTrama.EMonto, EsquemaTrama.EFechaAExpirar, EsquemaTrama.EUsuarioID, EsquemaTrama.EDataAdicional, EsquemaTrama.EUsuarioNombre, EsquemaTrama.EUsuarioApellidos, EsquemaTrama.EUsuarioLocalidad, EsquemaTrama.EUsuarioDomicilio, EsquemaTrama.EUsuarioProvincia, EsquemaTrama.EUsuarioPais, EsquemaTrama.EUsuarioAlias, EsquemaTrama.EUsuarioEmail, _
                                                           EsquemaTrama.EDetalles, EsquemaTrama.EDetalles_Detalle, EsquemaTrama.EDetalles_Detalle_CodigoOrigen, EsquemaTrama.EDetalles_Detalle_TipoOrigen, EsquemaTrama.EDetalles_Detalle_ConceptoPago, EsquemaTrama.EDetalles_Detalle_Importe, EsquemaTrama.EDetalles_Detalle_Campo1, EsquemaTrama.EDetalles_Detalle_Campo2, EsquemaTrama.EDetalles_Detalle_Campo3}
                Else
                    campos = New String() {EsquemaTrama.EDatosEnc, EsquemaTrama.EMerchantId, EsquemaTrama.EOrdenId, EsquemaTrama.EUrlOk, EsquemaTrama.EUrlError, EsquemaTrama.EMailCom, EsquemaTrama.EOrdenDesc, EsquemaTrama.EMoneda, EsquemaTrama.EMonto, EsquemaTrama.EUsuarioID, EsquemaTrama.EDataAdicional, EsquemaTrama.EUsuarioNombre, EsquemaTrama.EUsuarioApellidos, EsquemaTrama.EUsuarioLocalidad, EsquemaTrama.EUsuarioDomicilio, EsquemaTrama.EUsuarioProvincia, EsquemaTrama.EUsuarioPais, EsquemaTrama.EUsuarioAlias, EsquemaTrama.EUsuarioEmail}
                End If

                Dim objEsquemaTrama As New EsquemaTrama()
                'objEsquemaTrama.Codigo = nodoServicio.Attributes(EsquemaTrama.ACodigo).Value
                objEsquemaTrama.CodigoApi = nodoServicio.Attributes(EsquemaTrama.AAPI).Value
                objEsquemaTrama.UsaEncriptacion = nodoServicio.Attributes(EsquemaTrama.AUsaEncriptacion).Value = "true"
                objEsquemaTrama.TipoRecepcion = nodoServicio.Attributes(EsquemaTrama.ATipoRecepcion).Value
                objEsquemaTrama.URL = nodoServicio.Attributes(EsquemaTrama.AUrl).Value
                objEsquemaTrama.Descripcion = nodoServicio.Attributes(EsquemaTrama.ADescripcion).Value
                objEsquemaTrama.Home = nodoServicio.Attributes(EsquemaTrama.AHome).Value

                Dim objEsquemaTramaNodo As New Dictionary(Of String, EsquemaTramaNodo)

                For Each campo As String In campos
                    Dim pxNode As XmlNode = nodoServicio.SelectSingleNode(campo)
                    If (pxNode IsNot Nothing) Then
                        Dim objNodo As New EsquemaTramaNodo()
                        objNodo.NodoId = pxNode.Name
                        objNodo.Nombre = pxNode.Attributes("nombre").Value
                        objNodo.Requerido = pxNode.Attributes("requerido").Value = "true"
                        objNodo.ValorPorDefecto = pxNode.Attributes("porDefecto").Value
                        objEsquemaTramaNodo.Add(campo, objNodo)
                    End If
                Next
                objEsquemaTrama.EsquemaNodo = objEsquemaTramaNodo
                If (campos.Length = objEsquemaTramaNodo.Count) Then
                    Return objEsquemaTrama
                Else
                    Throw New Exception("No tiene la cantidad de Elementos Necesarios")
                End If

            End If
            ' basado en el codigoAPI consulta el ContratoXML
        Catch ex As Exception
            Throw New Exception(String.Format("El Contrato XML no tiene el formato correcto. Error:{0}", ex.Message))
        End Try

    End Function

    Public Function ValidarFormatoXML() As Boolean
        _esquemaTrama = ObtenerContratoServicioXMLPorAPI(_CodigoApi)
        'valida si el xmlcontenido tiene como campos todos los que estan en el contrato xml.
        Return True
    End Function
    Public Function ObtenerEsquemaTrama() As EsquemaTrama
        Return _esquemaTrama
    End Function

    Private Function ObtenerValorBEOrdenPagoDeXMLContenido(ByVal id As String, ByVal pxmlContenido As XmlDocument, ByVal xpath As String) As String
        Dim result As String = ""
        If (_esquemaTrama Is Nothing) Then
            Throw New Exception("BLTraductorContratoServ.AsignarValorBEOrdenPagoDeXMLContenido Error: Primero debe ejecutar el Metodo ValidarFormatoXML()")
        End If
        Dim nodoServicio As XmlNode
        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(pxmlContenido.NameTable)
        If Not nsmgr.HasNamespace("bk") Then
            nsmgr.AddNamespace("bk", "urn:ServicioXML")
        End If
        Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = _esquemaTrama.EsquemaNodo
        If (_esquemaNodo.ContainsKey(id)) Then
            Dim objnodo As EsquemaTramaNodo = _esquemaNodo(id)
            Dim root As XmlElement = pxmlContenido.DocumentElement

            nodoServicio = root.SelectSingleNode(xpath + objnodo.Nombre, nsmgr)
            If (nodoServicio Is Nothing) Then
                Dim tmpxpath As String = System.Configuration.ConfigurationManager.AppSettings("XMLContratoRaizOpc")
                nodoServicio = root.SelectSingleNode(tmpxpath + objnodo.Nombre, nsmgr)
            End If


            If (nodoServicio IsNot Nothing) Then
                result = nodoServicio.InnerText
            End If
            If (result = "") Then
                result = objnodo.ValorPorDefecto
            End If
            If (objnodo.Requerido And result = "") Then
                Throw New Exception(String.Format("El Campo {0} es requerido en la trama XML. ", objnodo.Nombre))
            End If
        End If

        Return result
    End Function

    Private Function ObtenerValorBEDetalleOrdenPagoDeXMLContenido(ByVal id As String, ByVal pxmlNodo As XmlNode) As String
        Dim result As String = ""
        Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = _esquemaTrama.EsquemaNodo
        If (_esquemaNodo.ContainsKey(id)) Then
            Dim objnodo As EsquemaTramaNodo = _esquemaNodo(id)
            Dim nodo As XmlNode = pxmlNodo.SelectSingleNode(objnodo.Nombre)
            If (nodo IsNot Nothing) Then
                result = nodo.InnerText
            End If
            If (result = "") Then
                result = objnodo.ValorPorDefecto
            End If
            'If (objnodo.Requerido And result = "") Then
            If (objnodo.Requerido And (nodo Is Nothing)) Then
                Throw New Exception(String.Format("El Campo {0} es requerido en la trama XML. ", objnodo.Nombre))
            End If
        End If
        Return result
    End Function

    Private Function ObtenerValorBEOrdenPagoDeUrlString(ByVal id As String, ByVal pstrContenido As String) As String
        Dim result As String = ""
        Dim _tmpesquemaNodo As EsquemaTramaNodo = _esquemaTrama.EsquemaNodo(id)
        Dim arrayStr As String() = pstrContenido.Split("|")
        For Each item As String In arrayStr
            Dim strCodigo As String() = item.Split("=")
            If (strCodigo.Length > 0) Then
                If (strCodigo(0) = _tmpesquemaNodo.Nombre) Then
                    result = strCodigo(1)
                    Exit For
                End If
            End If
        Next
        If (result = "") Then
            result = _tmpesquemaNodo.ValorPorDefecto
        End If

        If (_tmpesquemaNodo.Requerido And result = "") Then
            Throw New Exception(String.Format("El Campo {0} es requerido en la trama. ", _tmpesquemaNodo.Nombre))
        End If
        Return result
    End Function

    Public Function ObtenerXMLContenidoDesencriptado() As String
        Return DesencriptarStr(_xmlstrContenido)
    End Function
    Public Function DesEnsamblarTramaEnBEOrdenPago() As BEOrdenPago
        Try
            Dim obeOrdenPago As New BEOrdenPago()
            Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = _esquemaTrama.EsquemaNodo
            If ((_idTipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService) Or (_idTipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Post)) Then

                Dim strtmpxmlcontenido As String = _xmlstrContenido

                'desencriptar trama
                If (_xmlEncriptado) Then
                    strtmpxmlcontenido = DesencriptarStr(strtmpxmlcontenido)
                End If

                Dim xmlContenido As XmlDocument = CrearXMLDocument(strtmpxmlcontenido)
                'EsquemaTrama.EDatosEnc, 
                Dim xprefpath As String = "/OrdenPago/"

                'EsquemaTrama.EMerchantId, 
                obeOrdenPago.MerchantID = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMerchantId, xmlContenido, xprefpath)

                'EsquemaTrama.EOrdenId
                obeOrdenPago.OrderIdComercio = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EOrdenId, xmlContenido, xprefpath)

                'EsquemaTrama.EUrlOk
                obeOrdenPago.UrlOk = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUrlOk, xmlContenido, xprefpath)

                'EsquemaTrama.EUrlError
                obeOrdenPago.UrlError = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUrlError, xmlContenido, xprefpath)

                ' EsquemaTrama.EMailCom
                obeOrdenPago.MailComercio = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMailCom, xmlContenido, xprefpath)

                'EsquemaTrama.EOrdenDesc
                obeOrdenPago.ConceptoPago = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EOrdenDesc, xmlContenido, xprefpath)

                'EsquemaTrama.EMoneda
                obeOrdenPago.IdMoneda = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMoneda, xmlContenido, xprefpath)

                Try
                    'EsquemaTrama.EFechaAExpirar  'Tareas #319
                    obeOrdenPago.FechaAExpirar = Date.MinValue
                    Dim strfechaAExpirar As String = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EFechaAExpirar, xmlContenido, xprefpath)
                    If (strfechaAExpirar <> "") Then
                        Dim strFechafecha As String = strfechaAExpirar.Split(" ")(0)
                        Dim intDia As Integer = strFechafecha.Split("/")(0)
                        Dim intMes As Integer = strFechafecha.Split("/")(1)
                        Dim intAnio As Integer = strFechafecha.Split("/")(2)
                        Dim strFechahora As String = strfechaAExpirar.Split(" ")(1)
                        Dim intHora As Integer = strFechahora.Split(":")(0)
                        Dim intMin As Integer = strFechahora.Split(":")(1)
                        Dim intSeg As Integer = strFechahora.Split(":")(2)
                        'Dim dtFechaAExpirar As Date = Format("dd/MM/yyyy HH:mm:ss", strfechaAExpirar)
                        Dim dtFechaAExpirar As Date = New Date(intAnio, intMes, intDia, intHora, intMin, intSeg)
                        'If Not IsDate(dtFechaAExpirar) Then
                        '    Throw New Exception("Error al Obtener la Fecha Expiración: Formato incorrecto")
                        'Else
                        obeOrdenPago.FechaAExpirar = dtFechaAExpirar
                        'End If
                    End If
                Catch ex As Exception
                    Throw New Exception("Error al Obtener la Fecha Expiración: Formato incorrecto")
                End Try

                'EsquemaTrama.EMonto
                obeOrdenPago.Total = ObjectToDecimal(ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMonto, xmlContenido, xprefpath))
                If obeOrdenPago.Total = 0.0 Or CType(obeOrdenPago.Total, Object) Is Nothing Then
                    Throw New SPE.Exceptions.TramaGetException("monto")
                End If


                'EsquemaTrama.EUsuarioID
                obeOrdenPago.UsuarioID = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioID, xmlContenido, xprefpath)

                'EsquemaTrama.EDataAdicional
                obeOrdenPago.DataAdicional = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EDataAdicional, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioNombre
                obeOrdenPago.UsuarioNombre = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioNombre, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioApellidos
                obeOrdenPago.UsuarioApellidos = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioApellidos, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioLocalidad, 
                obeOrdenPago.UsuarioLocalidad = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioLocalidad, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioDomicilio, 
                obeOrdenPago.UsuarioDomicilio = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioDomicilio, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioProvincia, 
                obeOrdenPago.UsuarioProvincia = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioProvincia, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioPais, 
                obeOrdenPago.UsuarioPais = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioPais, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioAlias
                obeOrdenPago.UsuarioAlias = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioAlias, xmlContenido, xprefpath)

                'EsquemaTrama.EUsuarioEmail
                obeOrdenPago.UsuarioEmail = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioEmail, xmlContenido, xprefpath)


                If (_esquemaNodo.ContainsKey(EsquemaTrama.EDetalles) And _esquemaNodo.ContainsKey(EsquemaTrama.EDetalles_Detalle)) Then

                    Dim eDetalles As String = _esquemaNodo(EsquemaTrama.EDetalles).Nombre
                    Dim eDetalles_detalle As String = _esquemaNodo(EsquemaTrama.EDetalles_Detalle).Nombre

                    '''' Detalles 
                    'EsquemaTrama.EDetalles, EsquemaTrama.EDetalles_Detalle, 
                    'xprefpath = String.Format("/OrdenPago/{0}/{1}/", EsquemaTrama.EDetalles, EsquemaTrama.EDetalles_Detalle)
                    xprefpath = String.Format("/OrdenPago/{0}/{1}", eDetalles, eDetalles_detalle)

                    '''''
                    obeOrdenPago.DetallesOrdenPago = New List(Of BEDetalleOrdenPago)

                    Dim nodoList As XmlNodeList = Nothing
                    Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(xmlContenido.NameTable)
                    If Not nsmgr.HasNamespace("bk") Then
                        nsmgr.AddNamespace("bk", "urn:ServicioXML")
                    End If
                    Dim root As XmlElement = xmlContenido.DocumentElement
                    nodoList = root.SelectNodes(xprefpath, nsmgr)
                    If (nodoList.Count = 0) Then
                        Dim tmpxpath As String = System.Configuration.ConfigurationManager.AppSettings("XMLContratoRaizOpc")
                        xprefpath = String.Format("{0}{1}/{2}", tmpxpath, eDetalles, eDetalles_detalle)
                        nodoList = root.SelectNodes(xprefpath, nsmgr)
                    End If
                    Dim mtotal As Decimal = 0.0
                    For Each node As XmlNode In nodoList

                        Dim obeDetalle As New BEDetalleOrdenPago()

                        'EsquemaTrama.EDetalles_Detalle_CodigoOrigen, 
                        'obeDetalle.codOrigen = node.SelectSingleNode(_esquemaNodo(EsquemaTrama.EDetalles_Detalle_CodigoOrigen).Nombre).InnerText
                        obeDetalle.codOrigen = ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_CodigoOrigen, node)

                        'EsquemaTrama.EDetalles_Detalle_TipoOrigen, 
                        obeDetalle.tipoOrigen = ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_TipoOrigen, node)

                        'EsquemaTrama.EDetalles_Detalle_ConceptoPago, 
                        obeDetalle.ConceptoPago = ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_ConceptoPago, node)

                        'EsquemaTrama.EDetalles_Detalle_Importe, 
                        obeDetalle.Importe = ObjectToDecimal(ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_Importe, node))

                        'EsquemaTrama.EDetalles_Detalle_Campo1, 
                        obeDetalle.campo1 = ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_Campo1, node)

                        'EsquemaTrama.EDetalles_Detalle_Campo2, 
                        obeDetalle.campo2 = ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_Campo2, node)

                        'EsquemaTrama.EDetalles_Detalle_Campo3}
                        obeDetalle.campo3 = ObtenerValorBEDetalleOrdenPagoDeXMLContenido(EsquemaTrama.EDetalles_Detalle_Campo3, node)
                        mtotal = mtotal + obeDetalle.Importe
                        obeOrdenPago.DetallesOrdenPago.Add(obeDetalle)
                    Next

                    'obeOrdenPago.Total = mtotal

                    If obeOrdenPago.Total = 0.0 Or CType(obeOrdenPago.Total, Object) Is Nothing Then
                        Throw New SPE.Exceptions.TramaGetException(EsquemaTrama.EMonto)
                    End If
                End If

            Else
                'If Not (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EDatosEnc)) Then
                '    Throw New Exception("")
                'End If
                Dim _tmpstrDatosEnc As String = _esquemaTrama.EsquemaNodo(EsquemaTrama.EDatosEnc).Nombre
                ' Caso en el que la trama es QUERYSTRING.
                Dim strtmpcontenido As String = _xmlstrContenido
                Dim arraystrtmpcontenido As String() = _xmlstrContenido.Split(_tmpstrDatosEnc + "=")
                If (arraystrtmpcontenido.Length > 1) Then
                    strtmpcontenido = arraystrtmpcontenido(1).Substring(_tmpstrDatosEnc.Length, arraystrtmpcontenido(1).Length - _tmpstrDatosEnc.Length)
                End If

                'desencriptar trama
                If (_xmlEncriptado) Then
                    strtmpcontenido = DesencriptarStr(strtmpcontenido)
                End If

                'EsquemaTrama.EMerchantId, 
                obeOrdenPago.MerchantID = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EMerchantId, strtmpcontenido)

                'EsquemaTrama.EOrdenId
                obeOrdenPago.OrderIdComercio = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EOrdenId, strtmpcontenido)

                'EsquemaTrama.EUrlOk
                obeOrdenPago.UrlOk = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUrlOk, strtmpcontenido)

                'EsquemaTrama.EUrlError
                obeOrdenPago.UrlError = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUrlError, strtmpcontenido)

                ' EsquemaTrama.EMailCom
                obeOrdenPago.MailComercio = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EMailCom, strtmpcontenido)

                'EsquemaTrama.EOrdenDesc
                obeOrdenPago.ConceptoPago = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EOrdenDesc, strtmpcontenido)

                'EsquemaTrama.EMoneda
                obeOrdenPago.IdMoneda = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EMoneda, strtmpcontenido)

                'EsquemaTrama.EMonto
                Dim monto As String = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EMonto, strtmpcontenido)
                If (monto = "") Then
                    Throw New SPE.Exceptions.TramaGetException("monto")
                End If
                obeOrdenPago.Total = SPE.EmsambladoComun.TraductorServ.ObtenerMontoDeFormatoServicio(monto).ToString()
                If obeOrdenPago.Total = 0.0 Or CType(obeOrdenPago.Total, Object) Is Nothing Then
                    Throw New SPE.Exceptions.TramaGetException("monto")
                End If

                'EsquemaTrama.EUsuarioID
                obeOrdenPago.UsuarioID = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioID, strtmpcontenido)

                'EsquemaTrama.EDataAdicional
                obeOrdenPago.DataAdicional = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EDataAdicional, strtmpcontenido)

                'EsquemaTrama.EUsuarioNombre
                obeOrdenPago.UsuarioNombre = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioNombre, strtmpcontenido)

                'EsquemaTrama.EUsuarioApellidos
                obeOrdenPago.UsuarioApellidos = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioApellidos, strtmpcontenido)

                'EsquemaTrama.EUsuarioLocalidad, 
                obeOrdenPago.UsuarioLocalidad = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioLocalidad, strtmpcontenido)

                'EsquemaTrama.EUsuarioDomicilio, 
                obeOrdenPago.UsuarioDomicilio = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioDomicilio, strtmpcontenido)

                'EsquemaTrama.EUsuarioProvincia, 
                obeOrdenPago.UsuarioProvincia = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioProvincia, strtmpcontenido)

                'EsquemaTrama.EUsuarioPais, 
                obeOrdenPago.UsuarioPais = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioPais, strtmpcontenido)

                'EsquemaTrama.EUsuarioAlias
                obeOrdenPago.UsuarioAlias = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioAlias, strtmpcontenido)

                'EsquemaTrama.EUsuarioEmail
                obeOrdenPago.UsuarioEmail = ObtenerValorBEOrdenPagoDeUrlString(EsquemaTrama.EUsuarioEmail, strtmpcontenido)

                ''Detalle
                obeOrdenPago.DetallesOrdenPago = New List(Of BEDetalleOrdenPago)
                Dim obeDetalle As New BEDetalleOrdenPago()
                obeDetalle.codOrigen = ""
                obeDetalle.tipoOrigen = ""
                obeDetalle.ConceptoPago = obeOrdenPago.ConceptoPago
                obeDetalle.Importe = obeOrdenPago.Total
                obeDetalle.campo1 = ""
                obeDetalle.campo2 = ""
                obeDetalle.campo3 = ""
                obeOrdenPago.DetallesOrdenPago.Add(obeDetalle)
            End If
            Return obeOrdenPago
        Catch ex As Exception
            Throw New Exception("Error al DesEnsamblarTramaEnBEOrdenPago : " + ex.Message)
        End Try
    End Function

    Public Function EnsamblarTramaQueryString(ByVal url As String, ByVal encriptar As Boolean) As String
        Dim resultado As String = ""
        Dim tmpMerchantIDKey As String = ""
        Dim tmpMerchantIDValue As String = ""
        ValidarFormatoXML()

        'Dim tmpMerchantId As String = ""
        If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EMerchantId)) Then
            tmpMerchantIDKey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EMerchantId).Nombre
        End If

        Dim tmpDatosEnc As String = ""
        If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EDatosEnc)) Then
            tmpDatosEnc = _esquemaTrama.EsquemaNodo(EsquemaTrama.EDatosEnc).Nombre
        End If

        'If (ptipoEnsamble = TipoEnsamblaje.Xml) Then
        If ((_obeOrdenPago.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService) Or _
            (_obeOrdenPago.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Post) _
          ) Then
            '_obeOrdenPago.TramaSolicitud


            Dim xmlContenido As XmlDocument = CrearXMLDocument(_obeOrdenPago.TramaSolicitud)
            'EsquemaTrama.EDatosEnc, 
            Dim xprefpath As String = "/OrdenPago/"
            Dim listaresultado As New List(Of String)

            Dim tmpvalue As String = ""
            Dim tmpkey As String = ""

            'MerchantID
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EMerchantId)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EMerchantId).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMerchantId, xmlContenido, xprefpath)
                tmpMerchantIDKey = tmpkey
                tmpMerchantIDValue = tmpvalue
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'OrdenId
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EOrdenId)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EOrdenId).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EOrdenId, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UrlOk
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUrlOk)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUrlOk).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUrlOk, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UrlError
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUrlError)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUrlError).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUrlError, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'MailCom
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EMailCom)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EMailCom).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMailCom, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'OrdenDesc
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EOrdenDesc)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EOrdenDesc).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EOrdenDesc, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'Moneda
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EMoneda)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EMoneda).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMoneda, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'Monto
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EMonto)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EMonto).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EMonto, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioID
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioID)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioID).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioID, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'DataAdicional
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EDataAdicional)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EDataAdicional).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EDataAdicional, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioNombre
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioNombre)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioNombre).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioNombre, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioApellidos
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioApellidos)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioApellidos).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioApellidos, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioLocalidad
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioLocalidad)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioLocalidad).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioLocalidad, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioDomicilio
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioDomicilio)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioDomicilio).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioDomicilio, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioProvincia
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioProvincia)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioProvincia).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioProvincia, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioPais
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioPais)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioPais).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioPais, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioAlias
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioAlias)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioAlias).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioAlias, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If

            'UsuarioEmail
            If (_esquemaTrama.EsquemaNodo.ContainsKey(EsquemaTrama.EUsuarioEmail)) Then
                tmpkey = _esquemaTrama.EsquemaNodo(EsquemaTrama.EUsuarioEmail).Nombre
                tmpvalue = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioEmail, xmlContenido, xprefpath)
                If ((_3Dev.FW.Util.DataUtil.ObjectToString(tmpvalue) <> "") And (_3Dev.FW.Util.DataUtil.ObjectToString(tmpkey) <> "")) Then
                    listaresultado.Add(String.Format("{0}={1}", tmpkey, tmpvalue))
                End If
            End If
            resultado = String.Join("|", listaresultado.ToArray())
            resultado = String.Format("{0}|cip={1}", resultado, _obeOrdenPago.NumeroOrdenPago)

        Else

            Dim tmpDatosEncKey As String = tmpDatosEnc
            Dim tmpArrayTrama As String() = _obeOrdenPago.TramaSolicitud.Split(tmpDatosEncKey)

            Dim tmpTrama As String = ""
            If (tmpArrayTrama.Length > 1) Then
                tmpTrama = tmpArrayTrama(1).Substring(tmpDatosEncKey.Length, tmpArrayTrama(1).Length - tmpDatosEncKey.Length)
            End If

            tmpTrama = DesencriptarStr(tmpTrama)
            resultado = String.Format("{0}|cip={1}", tmpTrama, _obeOrdenPago.NumeroOrdenPago)

        End If

        If (encriptar) Then
            resultado = EncriptarStr(resultado)
        End If

        If (url <> "") Then
            Dim tramaRequest As String = String.Format("{0}={1}&{2}={3}", tmpMerchantIDKey, tmpMerchantIDValue, tmpDatosEnc, resultado)
            Dim oUri As New Uri(url)
            If oUri.Query = "" Then
                resultado = String.Format("{0}?{1}", url, tramaRequest)
            Else
                resultado = String.Format("{0}&{1}", url, tramaRequest)
            End If
            'resultado = String.Format("{0}?{1}", url, tramaRequest)
        End If
        Return resultado
    End Function

    Public Shared Function DesencriptarStr(ByVal str As String) As String
        Dim libx As New Utilitario.ClsLibreriax()
        Return libx.Decrypt(str, libx.fEncriptaKey)
    End Function

    Public Shared Function EncriptarStr(ByVal str As String) As String
        Dim libx As New Utilitario.ClsLibreriax()
        Return libx.Encrypt(str, libx.fEncriptaKey)
    End Function



End Class