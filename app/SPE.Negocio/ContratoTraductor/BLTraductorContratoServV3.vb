﻿Imports System.Transactions
Imports System
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.AccesoDatos
Imports System.Xml
Imports SPE.Criptography

Imports _3Dev.FW.Negocio
Imports _3Dev.FW.Util.DataUtil

Public Class BLTraductorContratoServV3
    Inherits _3Dev.FW.Negocio.BLBase

    Dim _xmlstrContratoServicio As String = Nothing
    Dim _xmlstrContenido As String = Nothing
    Dim _codigoApi As String = Nothing
    Dim _esquemaTrama As EsquemaTramaV2 = Nothing
    Dim _xmlEncriptado As Boolean = Nothing
    Dim _idTipoTrama As Integer = Nothing
    Dim _obeOrdenPago As BEOrdenPago = Nothing

    Dim _telefono As String = ""
    Dim _Anexo As String = ""
    Dim _Celular As String = ""
    Dim _DatoAdicional As String = ""
    Dim _CanalPago As String = ""
    Dim _TipoComprobante As String = ""
    Dim _Ruc As String = ""
    Dim _RazonSocial As String = ""
    Dim _Direcion As String = ""




    Public Sub New()
    End Sub
    Public Sub New(ByVal codigoAPI As String, ByVal xmlContenido As String, ByVal xmlEncriptado As Boolean, ByVal idTipoTrama As Integer)
        _xmlstrContenido = xmlContenido
        _codigoApi = codigoAPI
        _xmlEncriptado = xmlEncriptado
        _idTipoTrama = idTipoTrama
    End Sub
    Private Function CrearXMLDocument(ByVal _xmlContratoServicio As String) As XmlDocument
        Dim _xmlDocument As New XmlDocument()
        _xmlDocument.LoadXml(_xmlContratoServicio)
        Return _xmlDocument
    End Function
    Public Function ValidarFormatoXML() As Boolean
        _esquemaTrama = ObtenerContratoServicioXMLPorAPI(_codigoApi)
        Return True
    End Function
    Public Function Descifrar(ByVal str As String) As String
        Dim DASeguridad As New DASeguridad
        Dim pathPrivateKey As String = DASeguridad.GetKeyPrivateSPE()
        Return Encripter.DecryptText(str, pathPrivateKey)
    End Function
    Public Function ValidarFirma(ByVal textoSinFirmar As String, ByVal textoFirmado As String, ByVal idServicio As Integer) As Boolean
        Dim DASeguridad As New DASeguridad
        Dim pathPublicKey = DASeguridad.GetKeyPublic(idServicio)
        Dim bool As Boolean = Encripter.signerVal(textoSinFirmar, textoFirmado, pathPublicKey)
        Return bool
    End Function
    Public Function ObtenerXMLContenidoDescifrado() As String
        Return Descifrar(_xmlstrContenido)
    End Function

    Private Function ObtenerContratoServicioXMLPorAPI(ByVal codAPI) As EsquemaTramaV2
        Try

            _xmlstrContratoServicio = New DAServicio().GetServicioContratoXML(codAPI)

            Dim nodoServicio As XmlNode
            Dim nodosCampos As XmlNodeList
            Dim _xmlDocument As XmlDocument = CrearXMLDocument(_xmlstrContratoServicio)
            Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(_xmlDocument.NameTable)
            Dim objEsquemaTrama As New EsquemaTramaV2()

            nsmgr.AddNamespace("bk", "urn:contratoXML")
            nodoServicio = _xmlDocument.DocumentElement.SelectSingleNode("/Servicio", nsmgr)
            nodosCampos = nodoServicio.ChildNodes

            objEsquemaTrama.CodigoApi = nodoServicio.Attributes(EsquemaTramaV2.AAPI).Value
            objEsquemaTrama.UsaEncriptacion = nodoServicio.Attributes(EsquemaTramaV2.AUsaEncriptacion).Value = "true"
            objEsquemaTrama.TipoRecepcion = nodoServicio.Attributes(EsquemaTramaV2.ATipoRecepcion).Value
            objEsquemaTrama.URL = nodoServicio.Attributes(EsquemaTramaV2.AUrl).Value
            objEsquemaTrama.Descripcion = nodoServicio.Attributes(EsquemaTramaV2.ADescripcion).Value
            objEsquemaTrama.Home = nodoServicio.Attributes(EsquemaTramaV2.AHome).Value
            objEsquemaTrama.EsquemaNodo = ObtenerEsquemaTramaNodoDeXML(nodosCampos)

            Return objEsquemaTrama

        Catch ex As Exception
            Throw New Exception(String.Format("El Contrato XML no tiene el formato correcto. Error:{0}", ex.Message))
        End Try

    End Function
    Public Function ObtenerEsquemaTramaNodoDeXML(ByVal nodosCampos As XmlNodeList) As Dictionary(Of String, EsquemaTramaNodo)
        Dim objEsquemaTramaNodo As New Dictionary(Of String, EsquemaTramaNodo)
        For i As Integer = 0 To nodosCampos.Count - 1
            Dim nodoCampo As XmlNode = nodosCampos.ItemOf(i)
            Dim objNodo As New EsquemaTramaNodo()
            objNodo.NodosHijos = If(nodoCampo.HasChildNodes, ObtenerEsquemaTramaNodoDeXML(nodoCampo.ChildNodes), Nothing)
            objNodo.NodoId = nodoCampo.Name
            objNodo.Nombre = nodoCampo.Attributes(EsquemaTramaNodo.aNombre).Value
            objNodo.Requerido = nodoCampo.Attributes(EsquemaTramaNodo.aRequerido).Value = "true"
            objNodo.ValorPorDefecto = nodoCampo.Attributes(EsquemaTramaNodo.aPorDefecto).Value
            objEsquemaTramaNodo.Add(nodoCampo.Name, objNodo)
        Next
        Return objEsquemaTramaNodo
    End Function


    Private Function ObtenerValorBEOrdenPagoDeXMLContenido(ByVal id As String, ByVal pxmlContenido As XmlDocument, ByVal xpath As String) As String
        Dim result As String = ""
        If (_esquemaTrama Is Nothing) Then
            Throw New Exception("BLTraductorContratoServ.AsignarValorBEOrdenPagoDeXMLContenido Error: Primero debe ejecutar el Metodo ValidarFormatoXML()")
        End If
        Dim nodoServicio As XmlNode
        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(pxmlContenido.NameTable)
        Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = _esquemaTrama.EsquemaNodo

        If Not nsmgr.HasNamespace("bk") Then
            nsmgr.AddNamespace("bk", "urn:ServicioXML")
        End If

        If (_esquemaNodo.ContainsKey(id)) Then
            Dim objnodo As EsquemaTramaNodo = _esquemaNodo(id)
            Dim root As XmlElement = pxmlContenido.DocumentElement

            nodoServicio = root.SelectSingleNode(xpath + objnodo.Nombre, nsmgr)
            If (nodoServicio Is Nothing) Then
                Dim tmpxpath As String = System.Configuration.ConfigurationManager.AppSettings("XMLContratoRaizOpc")
                nodoServicio = root.SelectSingleNode(tmpxpath + objnodo.Nombre, nsmgr)
            End If

            If (nodoServicio IsNot Nothing) Then
                result = nodoServicio.InnerText
            End If
            If (result = "") Then
                result = objnodo.ValorPorDefecto
            End If
            If (objnodo.Requerido And result = "") Then
                Throw New Exception(String.Format("El Campo {0} es requerido en la trama XML. ", objnodo.Nombre))
            End If
        End If

        Return result
    End Function


    '<add Peru.Com FaseIII>
#Region "Integracion Servicios 2.0"
    Private Function ObtenerValorBEDetalleSolicitudDeXMLContenido(ByVal elementoName As String, ByVal oEsquemaTramaNodo As EsquemaTramaNodo, ByVal oXmlNode As XmlNode) As String
        Dim result As String = ""
        Dim node As XmlNode
        If oEsquemaTramaNodo.TieneHijos Then
            If oEsquemaTramaNodo.NodosHijos.ContainsKey(elementoName) Then
                If oXmlNode.HasChildNodes Then
                    node = oXmlNode.SelectSingleNode(oEsquemaTramaNodo.NodosHijos.Item(elementoName).Nombre)
                    If node IsNot Nothing Then
                        result = node.InnerText
                    Else
                        result = oEsquemaTramaNodo.NodosHijos.Item(elementoName).ValorPorDefecto
                    End If
                    If (oEsquemaTramaNodo.NodosHijos.Item(elementoName).Requerido And result = "") Then
                        Throw New Exception(String.Format("El Campo {0} es requerido en la trama XML. ", oEsquemaTramaNodo.Nombre))
                    End If
                End If
            End If
        End If
        Return result
    End Function
    Public Function ObtenerBESolicitudPagoFromXML() As BESolicitudPago
        Try
            Dim obeSolicitudPago As New BESolicitudPago()
            Dim _esquemaNodo As Dictionary(Of String, EsquemaTramaNodo) = _esquemaTrama.EsquemaNodo

            Dim xmlContenido As XmlDocument = CrearXMLDocument(If(_xmlEncriptado, Descifrar(_xmlstrContenido), _xmlstrContenido))



            'EsquemaTrama.EDatosEnc, 
            Dim xprefpath As String = "/SolPago/"

            'EsquemaTrama.EMoneda
            obeSolicitudPago.IdMoneda = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EIdMoneda, xmlContenido, xprefpath)

            'EsquemaTrama.ETotal
            obeSolicitudPago.Total = ObjectToDecimal(ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.ETotal, xmlContenido, xprefpath))
            If CType(obeSolicitudPago.Total, Object) Is Nothing Then
                Throw New SPE.Exceptions.TramaGetException("El monto es nulo o no tiene el formato numerico")
            ElseIf obeSolicitudPago.Total = 0.0 Then
                Throw New SPE.Exceptions.TramaGetException("El monto no puede ser 0")
            End If

            'EsquemaTrama.EMetodosPago
            obeSolicitudPago.MetodosPago = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EMetodosPago, xmlContenido, xprefpath)

            'EsquemaTrama.ECodServicio
            obeSolicitudPago.CodServicio = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.ECodServicio, xmlContenido, xprefpath)

            'EsquemaTrama.ECodtransaccion
            obeSolicitudPago.CodTransaccion = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.ECodtransaccion, xmlContenido, xprefpath)

            ' EsquemaTrama.EEmailComercio
            obeSolicitudPago.MailComercio = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EEmailComercio, xmlContenido, xprefpath)

            'EsquemaTrama.EOrdenDesc
            obeSolicitudPago.ConceptoPago = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EConceptoPago, xmlContenido, xprefpath)


            'datos adiciaonles-----------------------------------
            '----------------------------------------------------
            '----------------------------------------------------

            If xmlFormulario(xmlContenido.InnerXml) = True Then

                obeSolicitudPago.Telefono = _telefono
                obeSolicitudPago.Anexo = _Anexo
                obeSolicitudPago.Celular = _Celular
                obeSolicitudPago.DatoAdicional = _DatoAdicional
                obeSolicitudPago.CanalPago = _CanalPago
                obeSolicitudPago.TipoComprobante = _TipoComprobante
                obeSolicitudPago.Ruc = _Ruc
                obeSolicitudPago.RazonSocial = _RazonSocial
                obeSolicitudPago.Direccion = _Direcion
                obeSolicitudPago.CodigoFormulario = "Formulario"
            Else
                obeSolicitudPago.Telefono = ""
                obeSolicitudPago.Anexo = ""
                obeSolicitudPago.Celular = ""
                obeSolicitudPago.DatoAdicional = ""
                obeSolicitudPago.CanalPago = ""
                obeSolicitudPago.TipoComprobante = ""
                obeSolicitudPago.Ruc = ""
                obeSolicitudPago.RazonSocial = ""
                obeSolicitudPago.Direccion = ""
                obeSolicitudPago.CodigoFormulario = "web"

            End If



            'datos adiciaonles-----------------------------------
            '----------------------------------------------------
            '----------------------------------------------------

            Try
                'EsquemaTrama.EFechaAExpirar  'Tareas #319
                obeSolicitudPago.FechaExpiracion = Date.MinValue
                Dim strfechaAExpirar As String = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EFechaAExpirar, xmlContenido, xprefpath)
                If (strfechaAExpirar <> "") Then
                    Dim strFechafecha As String = strfechaAExpirar.Split(" ")(0)
                    Dim intDia As Integer = strFechafecha.Split("/")(0)
                    Dim intMes As Integer = strFechafecha.Split("/")(1)
                    Dim intAnio As Integer = strFechafecha.Split("/")(2)
                    Dim strFechahora As String = strfechaAExpirar.Split(" ")(1)
                    Dim intHora As Integer = strFechahora.Split(":")(0)
                    Dim intMin As Integer = strFechahora.Split(":")(1)
                    Dim intSeg As Integer = strFechahora.Split(":")(2)
                    Dim dtFechaAExpirar As Date = New Date(intAnio, intMes, intDia, intHora, intMin, intSeg)
                    obeSolicitudPago.FechaExpiracion = dtFechaAExpirar
                End If

            Catch ex As Exception
                Throw New Exception("Error al Obtener la Fecha Expiración: Formato incorrecto")
            End Try

            'EsquemaTrama.EUsuarioID
            obeSolicitudPago.UsuarioId = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioId, xmlContenido, xprefpath)

            'EsquemaTrama.EDataAdicional
            obeSolicitudPago.DataAdicional = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EDataAdicional, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioNombre
            obeSolicitudPago.UsuarioNombre = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioNombre, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioApellidos
            obeSolicitudPago.UsuarioApellidos = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioApellidos, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioLocalidad, 
            obeSolicitudPago.UsuarioLocalidad = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioLocalidad, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioDomicilio, 
            'obeSolicitudPago.UsuarioDomicilio = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTrama.EUsuarioDomicilio, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioProvincia, 
            obeSolicitudPago.UsuarioProvincia = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioProvincia, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioPais, 
            obeSolicitudPago.UsuarioPais = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioPais, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioAlias
            obeSolicitudPago.UsuarioAlias = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioAlias, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioTipoDoc
            obeSolicitudPago.UsuarioTipoDoc = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioTipoDoc, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioTipoDoc
            obeSolicitudPago.UsuarioNumDoc = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioNumeroDoc, xmlContenido, xprefpath)

            'EsquemaTrama.EUsuarioEmail
            obeSolicitudPago.UsuarioEmail = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EUsuarioEmail, xmlContenido, xprefpath)

            'EsquemaTrama.EConceptoPago
            obeSolicitudPago.ConceptoPago = ObtenerValorBEOrdenPagoDeXMLContenido(EsquemaTramaV2.EConceptoPago, xmlContenido, xprefpath)

            If (_esquemaNodo.ContainsKey(EsquemaTramaV2.EDetalles)) Then
                Dim nodoDetalles As EsquemaTramaNodo
                Dim nodoDetalle As EsquemaTramaNodo
                nodoDetalles = _esquemaNodo.Item(EsquemaTramaV2.EDetalles)

                If (nodoDetalles.TieneHijos) Then

                    If (nodoDetalles.NodosHijos.ContainsKey(EsquemaTramaV2.EDetalles_Detalle)) Then
                        nodoDetalle = nodoDetalles.NodosHijos.Item(EsquemaTramaV2.EDetalles_Detalle)

                        xprefpath = String.Format("/SolPago/{0}/{1}", nodoDetalles.Nombre, nodoDetalle.Nombre)

                        obeSolicitudPago.DetallesSolicitudPago = New List(Of BEDetalleSolicitudPago)

                        Dim nodoList As XmlNodeList = Nothing
                        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(xmlContenido.NameTable)
                        If Not nsmgr.HasNamespace("bk") Then
                            nsmgr.AddNamespace("bk", "urn:ServicioXML")
                        End If
                        Dim root As XmlElement = xmlContenido.DocumentElement
                        nodoList = root.SelectNodes(xprefpath, nsmgr)

                        For Each node As XmlNode In nodoList

                            Dim obeDetalle As New BEDetalleSolicitudPago()

                            'EsquemaTrama.EDetalles_Detalle_CodigoOrigen, 
                            'obeDetalle.codOrigen = node.SelectSingleNode(_esquemaNodo(EsquemaTrama.EDetalles_Detalle_CodigoOrigen).Nombre).InnerText
                            obeDetalle.codOrigen = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_CodOrigen, nodoDetalle, node)

                            'EsquemaTrama.EDetalles_Detalle_TipoOrigen, 
                            obeDetalle.tipoOrigen = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_TipoOrigen, nodoDetalle, node)

                            'EsquemaTrama.EDetalles_Detalle_ConceptoPago, 
                            obeDetalle.ConceptoPago = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_ConceptoPago, nodoDetalle, node)

                            'EsquemaTrama.EDetalles_Detalle_Importe, 
                            obeDetalle.Importe = ObjectToDecimal(ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_Importe, nodoDetalle, node))

                            'EsquemaTrama.EDetalles_Detalle_Campo1, 
                            obeDetalle.campo1 = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_Campo1, nodoDetalle, node)

                            'EsquemaTrama.EDetalles_Detalle_Campo2, 
                            obeDetalle.campo2 = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_Campo2, nodoDetalle, node)

                            'EsquemaTrama.EDetalles_Detalle_Campo3}
                            obeDetalle.campo3 = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EDetalles_Detalle_Campo3, nodoDetalle, node)

                            obeSolicitudPago.DetallesSolicitudPago.Add(obeDetalle)

                            'mtotal = mtotal + obeDetalle.Importe
                        Next

                        'obeSolicitudPago.Total = mtotal

                        'If obeSolicitudPago.Total = 0.0 Or CType(obeSolicitudPago.Total, Object) Is Nothing Then
                        '    Throw New SPE.Exceptions.TramaGetException(EsquemaTramaV2.ETotal)
                        'End If
                    End If
                End If
            End If
            If (_esquemaNodo.ContainsKey(EsquemaTramaV2.EParamsURL)) Then
                Dim nodoParametrosURL As EsquemaTramaNodo
                Dim nodoParametroURL As EsquemaTramaNodo
                nodoParametrosURL = _esquemaNodo.Item(EsquemaTramaV2.EParamsURL)

                If (nodoParametrosURL.TieneHijos) Then

                    If (nodoParametrosURL.NodosHijos.ContainsKey(EsquemaTramaV2.EParamsURL_ParamURL)) Then
                        nodoParametroURL = nodoParametrosURL.NodosHijos.Item(EsquemaTramaV2.EParamsURL_ParamURL)

                        xprefpath = String.Format("/SolPago/{0}/{1}", nodoParametrosURL.Nombre, nodoParametroURL.Nombre)

                        obeSolicitudPago.ParametrosURL = New List(Of BEParametroURL)

                        Dim nodoList As XmlNodeList = Nothing
                        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(xmlContenido.NameTable)
                        If Not nsmgr.HasNamespace("bk") Then
                            nsmgr.AddNamespace("bk", "urn:ServicioXML")
                        End If
                        Dim root As XmlElement = xmlContenido.DocumentElement
                        nodoList = root.SelectNodes(xprefpath, nsmgr)
                        For Each node As XmlNode In nodoList

                            Dim obeDetalle As New BEParametroURL()

                            'EsquemaTrama.EParamsURL_ParamURL_Nombre, 
                            obeDetalle.Nombre = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EParamsURL_ParamURL_Nombre, nodoParametroURL, node)

                            'EsquemaTrama.EParamsURL_ParamURL_Valor, 
                            obeDetalle.Valor = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EParamsURL_ParamURL_Valor, nodoParametroURL, node)
                            obeSolicitudPago.ParametrosURL.Add(obeDetalle)
                        Next
                    End If
                End If
            End If
            If (_esquemaNodo.ContainsKey(EsquemaTramaV2.EParamsEmail)) Then
                Dim nodoParametrosEmail As EsquemaTramaNodo
                Dim nodoParametroEmail As EsquemaTramaNodo
                nodoParametrosEmail = _esquemaNodo.Item(EsquemaTramaV2.EParamsEmail)

                If (nodoParametrosEmail.TieneHijos) Then

                    If (nodoParametrosEmail.NodosHijos.ContainsKey(EsquemaTramaV2.EParamsEmail_ParamEmail)) Then
                        nodoParametroEmail = nodoParametrosEmail.NodosHijos.Item(EsquemaTramaV2.EParamsEmail_ParamEmail)

                        xprefpath = String.Format("/SolPago/{0}/{1}", nodoParametrosEmail.Nombre, nodoParametroEmail.Nombre)

                        obeSolicitudPago.ParametrosEmail = New List(Of BEParametroEmail)

                        Dim nodoList As XmlNodeList = Nothing
                        Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(xmlContenido.NameTable)
                        If Not nsmgr.HasNamespace("bk") Then
                            nsmgr.AddNamespace("bk", "urn:ServicioXML")
                        End If
                        Dim root As XmlElement = xmlContenido.DocumentElement
                        nodoList = root.SelectNodes(xprefpath, nsmgr)
                        For Each node As XmlNode In nodoList

                            Dim obeDetalle As New BEParametroEmail()

                            'EsquemaTrama.EParamsURL_ParamURL_Nombre, 
                            obeDetalle.Nombre = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EParamsEmail_ParamEmail_Nombre, nodoParametroEmail, node)

                            'EsquemaTrama.EParamsURL_ParamURL_Valor, 
                            obeDetalle.Valor = ObtenerValorBEDetalleSolicitudDeXMLContenido(EsquemaTramaV2.EParamsEmail_ParamEmail_Valor, nodoParametroEmail, node)
                            obeSolicitudPago.ParametrosEmail.Add(obeDetalle)
                        Next
                    End If
                End If
            End If
            Return obeSolicitudPago
        Catch ex As Exception
            Throw New Exception("Error al DesEnsamblarTramaEnBEOrdenPago : " + ex.Message)
        End Try
    End Function
    Public Function ObtenerIDSolicitudPagoFromXML(ByVal xml As String) As Int64
        Try

            Dim idSolicitud As Int64 = 0
            Dim nodoConsulta As XmlNode
            Dim _xmlDocument As XmlDocument = CrearXMLDocument(xml)
            Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(_xmlDocument.NameTable)
            Dim objEsquemaTrama As New EsquemaTramaV2()

            nsmgr.AddNamespace("bk", "urn:contratoXML")
            nodoConsulta = _xmlDocument.DocumentElement.SelectSingleNode("/ConsultarPago", nsmgr)
            If (nodoConsulta.HasChildNodes) Then
                Return Convert.ToInt64(nodoConsulta.Item("idResSolPago").InnerText)
            End If
            Return idSolicitud
        Catch ex As Exception
            Throw New Exception(String.Format("No se pudo obtener el campo 'idResSolPago' de la peticion, Error:{0}", ex.Message))
        End Try
    End Function
#End Region
#Region "SagaInicio"
    Public Function ObtenerDatosToConsultaCipFromXML(ByVal xml As String) As BESolicitudPago
        Try
            Dim oBESolicitudPago As New BESolicitudPago()
            Dim idSolicitud As Int64 = 0
            Dim nodoConsulta As XmlNode
            Dim _xmlDocument As XmlDocument = CrearXMLDocument(xml)
            Dim nsmgr As XmlNamespaceManager = New XmlNamespaceManager(_xmlDocument.NameTable)
            Dim objEsquemaTrama As New EsquemaTramaV2()

            nsmgr.AddNamespace("bk", "urn:contratoXML")
            nodoConsulta = _xmlDocument.DocumentElement.SelectSingleNode("/ConsultarPago", nsmgr)
            If (nodoConsulta.HasChildNodes) Then
                oBESolicitudPago.CodServicio = Convert.ToString(nodoConsulta.Item("CodServicio").InnerText)
                oBESolicitudPago.CodTransaccion = Convert.ToString(nodoConsulta.Item("CodTransaccion").InnerText)
            End If
            Return oBESolicitudPago
        Catch ex As Exception
            Throw New Exception(String.Format("No se pudo obtener el campo 'idResSolPago' de la peticion, Error:{0}", ex.Message))
        End Try
    End Function
#End Region 'SagaFin



    'validarxmlformulario-------------------------
    '---------------------------------------------

    Public Function xmlFormulario(ByVal xmldatos As String) As Boolean

        Dim resultado As Boolean = True
        Dim xmltest As New XmlDocument()
        xmltest.LoadXml(xmldatos)

        Try

            Dim listaDatosAdicionales As XmlNodeList = xmltest.SelectNodes("SolPago")
            Dim Datos As XmlNode


            For i As Integer = 0 To listaDatosAdicionales.Count - 1
                Datos = listaDatosAdicionales.Item(i)

                If Datos.OuterXml.Contains("CodServicioFormulario") = True Then

                    _telefono = Datos.SelectSingleNode("Telefono").InnerText
                    _Anexo = Datos.SelectSingleNode("Anexo").InnerText
                    _Celular = Datos.SelectSingleNode("Celular").InnerText
                    _DatoAdicional = Datos.SelectSingleNode("DatoAdicional").InnerText
                    _CanalPago = Datos.SelectSingleNode("CanalPago").InnerText
                    _TipoComprobante = Datos.SelectSingleNode("TipoComprobante").InnerText
                    _Ruc = Datos.SelectSingleNode("Ruc").InnerText
                    _RazonSocial = Datos.SelectSingleNode("RazonSocial").InnerText
                    _Direcion = Datos.SelectSingleNode("Direccion").InnerText
                    resultado = True

                Else
                    _telefono = ""
                    _Anexo = ""
                    _Celular = ""
                    _DatoAdicional = ""
                    _CanalPago = ""
                    _TipoComprobante = ""
                    _Ruc = ""
                    _RazonSocial = ""
                    _Direcion = ""
                    resultado = False
                End If

            Next

        Catch ex As Exception
            resultado = False
        End Try

        Return resultado

    End Function


End Class
