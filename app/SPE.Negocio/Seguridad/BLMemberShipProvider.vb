Namespace Seguridad

    Public Class BLMemberShipProvider
        Inherits _3Dev.FW.Negocio.Seguridad.BLCustomMembershipProvider
        Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.Seguridad.CustomMembershipProvider
            Return New SPE.AccesoDatos.Seguridad.DAMemberShipProvider()
        End Function
    End Class

End Namespace
