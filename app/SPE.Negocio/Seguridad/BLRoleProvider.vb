Namespace Seguridad


    Public Class BLRoleProvider
        Inherits _3Dev.FW.Negocio.Seguridad.BLSqlRoleProvider
        Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.Seguridad.DASqlRoleProvider
            Return New SPE.AccesoDatos.Seguridad.DARoleProvider()
        End Function
    End Class
End Namespace