Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Configuration
Imports SPE.AccesoDatos
Imports SPE.Criptography



Public Class BLSeguridad

    Public Sub New()

    End Sub

    Public Function ValidarServicioClaveAPI(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean
        Dim resultado As Boolean = False
        Try
            resultado = (New AccesoDatos.DASeguridad()).ValidarServicioClaveAPI(claveAPI, claveSecreta)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    '<add Peru.Com FaseIII>
#Region "ModuloSeguridadEncriptacion"
    'Public Function GenerateKeyPrivatePublic(ByVal codEntidad As String) As Boolean
    '    Dim encripter As Encripter = New Encripter()
    '    Dim keyPrivate As Byte() = encripter.GenerarKeyPriv(codEntidad)
    '    Dim keyPublic As Byte() = encripter.GenerarKeyPub(codEntidad)
    '    Dim DASeguridad As DASeguridad = New DASeguridad()
    '    Dim resultado As Boolean = False
    '    Try
    '        resultado = DASeguridad.SaveKeyPublic(codEntidad, keyPublic)
    '        resultado = DASeguridad.SaveKeyPrivate(codEntidad, keyPrivate)
    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        Throw ex
    '    End Try
    '    Return resultado

    'End Function
    Public Function GenerateKeyPrivatePublicSPE() As Boolean

        Dim RutaClavePublica As String = ConfigurationManager.AppSettings("RutaClavePublica") & "_" & DateTime.Now.ToString("yyyyMMdd") & "_" & DateTime.Now.Hour.ToString() & DateTime.Now.Minute.ToString() & ".1pz"
        Dim RutaClavePrivada As String = ConfigurationManager.AppSettings("RutaClavePrivada") & "_" & DateTime.Now.ToString("yyyyMMdd") & "_" & DateTime.Now.Hour.ToString() & DateTime.Now.Minute.ToString() & ".1pz"

        'Encripter.GenerateAndSaveKeys(RutaClavePublica, RutaClavePrivada)
        Dim DASeguridad As DASeguridad = New DASeguridad()
        Dim resultado As Boolean = True
        Try
            'resultado = DASeguridad.SaveKeyPublicSPE(RutaClavePublica)
            'resultado = DASeguridad.SaveKeyPrivateSPE(RutaClavePrivada)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado

    End Function

    Public Function GetPublicKeySPE() As String
        Dim DASeguridad As DASeguridad = New DASeguridad()

        Dim resultado As String
        Try
            resultado = DASeguridad.GetKeyPublicSPE()
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function GetPrivateKeySPE() As String
        Dim DASeguridad As DASeguridad = New DASeguridad()

        Dim resultado As String
        Try
            resultado = DASeguridad.GetKeyPrivateSPE()
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    Public Function SaveKeyPublic(ByVal idServicio As Int32, ByVal keyPublic As String) As Boolean
        Dim resultado As Boolean = False
        Dim DASeguridad As New DASeguridad()
        Try
            resultado = DASeguridad.SaveKeyPublic(idServicio, keyPublic)
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado
    End Function
    Public Function GetPublicKey(ByVal codServicioCodigoApi As String) As String
        Dim DASeguridad As DASeguridad = New DASeguridad()

        Dim resultado As String
        Try
            resultado = DASeguridad.GetKeyPublic(codServicioCodigoApi)
        Catch ex As Exception
            'Logger.Write(ex)
            Throw ex
        End Try
        Return resultado
    End Function

    'Public Function CifrarMensaje(ByVal mensajeACifrar As String, ByVal codEntidadContraparte As String) As String()
    '    Dim encripter As Encripter = New Encripter()
    '    Dim DASeguridad As DASeguridad = New DASeguridad()
    '    encripter.clavePublicaContraparte = GetPublicKey(codEntidadContraparte)

    '    Dim resultado As String()
    '    Try
    '        resultado = encripter.Cifrar(mensajeACifrar)
    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        Throw ex
    '    End Try
    '    Return resultado
    'End Function
    'Public Function DesCifrarMensaje(ByVal mensajeADescifrar As String, ByVal keyCifrado As String, ByVal codEntidad As String) As String
    '    Dim encripter As Encripter = New Encripter()
    '    Dim DASeguridad As DASeguridad = New DASeguridad()
    '    encripter.clavePrivada = GetPrivateKey(codEntidad)

    '    Dim resultado As String
    '    Try
    '        resultado = encripter.DesCifrar(mensajeADescifrar, keyCifrado)
    '    Catch ex As Exception
    '        Logger.Write(ex)
    '        Throw ex
    '    End Try
    '    Return resultado
    'End Function
#End Region

End Class
