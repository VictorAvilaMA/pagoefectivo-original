Namespace Seguridad


    Public Class BLSiteMapProvider
        Inherits _3Dev.FW.Negocio.Seguridad.BLCustomSiteMapProvider
        Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.Seguridad.DACustomSiteMapProvider
            Return New SPE.AccesoDatos.Seguridad.DASiteMapProvider()
        End Function
    End Class
End Namespace