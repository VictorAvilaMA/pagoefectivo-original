Imports System.Transactions
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports SPE.AccesoDatos.Seguridad
Imports SPE.AccesoDatos

Namespace Seguridad


    Public Class BLUsuario
        Inherits _3Dev.FW.Negocio.Seguridad.BLUsuario
        Public Overrides Function GetConstructorDataAccessObject() As _3Dev.FW.AccesoDatos.Seguridad.DAUsuario
            Return New SPE.AccesoDatos.Seguridad.DAUsuario()
        End Function

        Private ReadOnly Property DAUsuario() As DAUsuario
            Get
                Return CType(Me.DataAccessProviderObject, DAUsuario)
            End Get
        End Property

        Private Function CodigoRegistro() As String

            Return Guid.NewGuid.ToString()

        End Function


        Public Overrides Function RegistrarUsuario(ByVal Usuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer

            Dim val As Integer
            Try
                Dim obeUsuario As SPE.Entidades.BEUsuarioBase = CType(Usuario, SPE.Entidades.BEUsuarioBase)

                If DAUsuario.ValidarEmailUsuario(obeUsuario) Then
                    Return SPE.EmsambladoComun.ParametrosSistema.ExisteEmail.Existe
                Else
                    Dim options As TransactionOptions = New TransactionOptions()
                    Dim EncodePwd, Pwd As String
                    obeUsuario.Password = Guid.NewGuid.ToString.Substring(1, 6)
                    EncodePwd = SeguridadComun.EncryptarCodigo(obeUsuario.Password.Trim)
                    options.IsolationLevel = IsolationLevel.ReadCommitted
                    Using transaccionscope As New TransactionScope()
                        obeUsuario.CodigoRegistro = CodigoRegistro()
                        Pwd = obeUsuario.Password
                        obeUsuario.Password = EncodePwd
                        val = DAUsuario.RegistrarUsuario(obeUsuario)
                        Dim Email As New BLEmail
                        Email.EnviarCorreoConfirmacionUsuario(obeUsuario.IdUsuario, _
                        obeUsuario.Nombres, Pwd, obeUsuario.CodigoRegistro, _
                        obeUsuario.Email)
                        transaccionscope.Complete()
                        Return val

                    End Using

                End If

            Catch ex As Exception
                'Logger.Write(ex)
                Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
            End Try
        End Function


    End Class





End Namespace