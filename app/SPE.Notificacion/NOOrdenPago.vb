Imports System.Configuration
Imports System.Collections
Imports SPE.Entidades
Imports System.IO
Imports System.Net
Public Class NOOrdenPago

    'Dim XmlTramaServRuta As String = System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRuta")
    Dim XmlTramaServRuta As String = System.Configuration.ConfigurationManager.AppSettings("targetPath")
    Dim Notifica As String = System.Configuration.ConfigurationManager.AppSettings("FlagNotificacion")

    Private oIOrdenPago As SPE.EmsambladoComun.IOrdenPago
    Dim str As String = ""

    Public Sub New(ByVal oorenpago As SPE.EmsambladoComun.IOrdenPago)
        oIOrdenPago = oorenpago
    End Sub
    Public Function NotificarRecepcionOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim result As Integer = 0
        If (Notifica = "1") Then
            obeOrdenPago = oIOrdenPago.ConsultarOrdenPagoNotificar(obeOrdenPago)
            If (obeOrdenPago.IdTipoNotificacion = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoNotificacion.Request) Then
                result = QSNotificarRecepcionOrdenPago(obeOrdenPago, SPE.EmsambladoComun.ParametrosSistema.ProcesoNotificacion.ProcesarRequest)
            Else
                result = 0
            End If
        Else
            Return 1
        End If
    End Function
    Public Function NotificarAnulacionOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim result As Integer = 0
        If (Notifica = "1") Then
            obeOrdenPago = oIOrdenPago.ConsultarOrdenPagoNotificar(obeOrdenPago)
            If (obeOrdenPago.IdTipoNotificacion = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoNotificacion.Request) Then
                result = QSNotificarAnulacionOrdenPago(obeOrdenPago, SPE.EmsambladoComun.ParametrosSistema.ProcesoNotificacion.ProcesarRequest)
            Else
                result = 0
            End If

        Else
            Return 1
        End If
    End Function
    Public Function NotificarExpiracionOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        Dim result As Integer = 0
        If (Notifica = "1") Then
            obeOrdenPago = oIOrdenPago.ConsultarOrdenPagoNotificar(obeOrdenPago)
            If (obeOrdenPago.IdTipoNotificacion = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoNotificacion.Request) Then
                result = QSNotificarExpiracionOrdenPago(obeOrdenPago)
            Else
                result = 0
            End If

        Else
            Return 1
        End If
    End Function

    Private Function QSNotificarRecepcionOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal opcion As String) As Integer

        obeOrdenPago.XmlTramaServRuta = XmlTramaServRuta
        If opcion = SPE.EmsambladoComun.ParametrosSistema.ProcesoNotificacion.ProcesarRequest Then
            str = SPE.Utilitario.UtilTraductorServ.BuildUrlNotificacion(obeOrdenPago, obeOrdenPago.UrlOk)
            Return RealizarNotificacionQueryStrings(str, "ProcesarRequestUrlCancelacionOP")
        Else
            str = obeOrdenPago.UrlOk
            Return RealizarNotificacionQueryStrings(str, "VerificarRequestUrlCancelacionOP")
        End If


    End Function
    Private Function QSNotificarAnulacionOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal opcion As String) As Integer
        'If (Notifica = "1") Then
        'obeOrdenPago = oIOrdenPago.ConsultarOrdenPagoNotificar(obeOrdenPago)
        obeOrdenPago.XmlTramaServRuta = XmlTramaServRuta
        If opcion = SPE.EmsambladoComun.ParametrosSistema.ProcesoNotificacion.ProcesarRequest Then
            str = SPE.Utilitario.UtilTraductorServ.BuildUrlNotificacion(obeOrdenPago, obeOrdenPago.UrlError)
            Return RealizarNotificacionQueryStrings(str, "ProcesarRequestUrlAnulacionOP")
        Else
            str = obeOrdenPago.UrlError
            Return RealizarNotificacionQueryStrings(str, "VerificarRequestUrlAnulacionOP")
        End If

        'Else
        'Return 1
        'End If
    End Function
    Private Function QSNotificarExpiracionOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
        'If (Notifica = "1") Then
        'obeOrdenPago = oIOrdenPago.ConsultarOrdenPagoNotificar(obeOrdenPago)
        obeOrdenPago.XmlTramaServRuta = XmlTramaServRuta
        str = SPE.Utilitario.UtilTraductorServ.BuildUrlNotificacion(obeOrdenPago, obeOrdenPago.UrlError)
        Return RealizarNotificacionQueryStrings(str, "ExpiracionOP")
        'Else
        'Return 1
        'End If
    End Function

    Public Function RealizarNotificacionQueryStrings(ByVal url As String, ByVal strProceso As String) As Integer

        Dim r As HttpWebResponse = Nothing
        Try
            ' Send the HTTP request
            Dim g As HttpWebRequest = CType(WebRequest.Create(url), _
              HttpWebRequest)
            r = CType(g.GetResponse, HttpWebResponse)
            ' Log the response to a text file
            Dim path As String = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings("logNotificacionRuta")
            Using tw As TextWriter = New StreamWriter(path, True)
                '
                tw.WriteLine(DateTime.Now.ToString + " for " + url + ": " + r.StatusCode.ToString + " - " + strProceso)
                ' Close the HTTP response
            End Using
            '
        Catch ex As Exception
            Throw ex
            Return 0
        Finally
            r.Close()
        End Try
        '
        Return 1
        '
    End Function
   

End Class
