Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports System.Text

Imports SPE.Utilitario.UtilSagaConnect

Public Class CServicioNotificacion
    Implements IDisposable

    '<upd Peru.Com FaseIII> Se modifico la logica de notificacion para la nueva version
    Public Function RealizarNotificacionesPendientes(ByVal oListaPendientes As List(Of BEServicioNotificacion)) As Integer
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                Dim oListaRespuestaBien, oListaRespuestaError As New List(Of BEServicioNotificacion)
                'SagaInicio
                Dim oBEServicioNotificacionParaSaga As New BEServicioNotificacion()
                Dim IntRespServicioSaga As Integer
                Dim objToServiceRs As New confirmarPagoOCResponse
                'SagaFin
                If (oListaPendientes.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In oListaPendientes
                        Try
                            Dim procesodesc As String = String.Format("idOrdenPago={0}|idMovimiento={1}|UrlRespuesta={2}|", obeServicioNotif.IdOrdenPago, obeServicioNotif.IdMovimiento, obeServicioNotif.UrlRespuesta)

                            Select Case obeServicioNotif.IdTipoServNotificacion
                                    'SagaInicio
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSaga
                                    'If obeServicioNotif.IdTipoMovimiento = 34 Then
                                    oBEServicioNotificacionParaSaga = oiServicioNotificacion.ConsultarDatosParaServicioSaga(obeServicioNotif.IdOrdenPago)
                                    oBEServicioNotificacionParaSaga.IdOrdenPago = obeServicioNotif.IdOrdenPago
                                    objToServiceRs = RealizarConsumoServicioSaga(oBEServicioNotificacionParaSaga)
                                    'End If
                                    If objToServiceRs.CodigoRespuesta = 0 Then
                                        obeServicioNotif.Observacion = "Notificación Satisfactoria"
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Notificado
                                        oListaRespuestaBien.Add(obeServicioNotif)
                                    Else
                                        obeServicioNotif.Observacion = String.Format(objToServiceRs.GlosaRespuesta)
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                                        oListaRespuestaError.Add(obeServicioNotif)
                                    End If
                                    'SagaFin
                            End Select
                        Catch ex As Exception
                            obeServicioNotif.Observacion = String.Format("Error :{0}", ex.Message)
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                            oListaRespuestaError.Add(obeServicioNotif)
                        End Try

                    Next
                    oiServicioNotificacion.ActualizarEstadoNotificaciones(oListaRespuestaBien, oListaRespuestaError)
                End If
            End Using
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex.Message)
            Throw ex
        End Try
        Return 1
    End Function

    Dim cantidadMaxregistros As Integer = 20

    '<upd Peru.Com FaseIII> Se modifico la logica de notificacion para la nueva version
    '<upd Peru.Com FaseIII> Se optimizo para el envio de paketes fraccionados en bloques de 20
    Public Function RealizarReNotificacionesErroneas(ByVal oListaPendientes As List(Of BEServicioNotificacion)) As Integer
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())

                Dim oListaRespuestaBien, oListaRespuestaError As New List(Of BEServicioNotificacion)
                Dim cantidadregistros As Integer = 0
                Dim Iterador As Integer = 0

                'SagaInicio
                Dim oBEServicioNotificacionParaSaga As New BEServicioNotificacion()
                Dim IntRespServicioSaga As Integer
                Dim objToServiceRs As New confirmarPagoOCResponse
                'SagaFin

                If (oListaPendientes.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In oListaPendientes
                        cantidadregistros += 1
                        Iterador += 1
                        Try
                            Dim procesodesc As String = String.Format("idOrdenPago={0}|idMovimiento={1}|UrlRespuesta={2}|", obeServicioNotif.IdOrdenPago, obeServicioNotif.IdMovimiento, obeServicioNotif.UrlRespuesta)
                            Select Case obeServicioNotif.IdTipoServNotificacion
                                'SagaInicio
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSaga
                                    'If obeServicioNotif.IdTipoMovimiento = 34 Then
                                    oBEServicioNotificacionParaSaga = oiServicioNotificacion.ConsultarDatosParaServicioSaga(obeServicioNotif.IdOrdenPago)
                                    oBEServicioNotificacionParaSaga.IdOrdenPago = obeServicioNotif.IdOrdenPago
                                    objToServiceRs = RealizarConsumoServicioSaga(oBEServicioNotificacionParaSaga)
                                    'End If
                                    If objToServiceRs.CodigoRespuesta = 0 Then
                                        obeServicioNotif.Observacion = "Notificación Satisfactoria"
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Notificado
                                        oListaRespuestaBien.Add(obeServicioNotif)
                                    Else
                                        obeServicioNotif.Observacion = String.Format(objToServiceRs.GlosaRespuesta)
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                                        oListaRespuestaError.Add(obeServicioNotif)
                                    End If
                                    'SagaFin
                            End Select
                        Catch ex As Exception
                            obeServicioNotif.Observacion = String.Format("Error :{0}", ex.Message)
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                            oListaRespuestaError.Add(obeServicioNotif)
                        End Try
                        If (cantidadMaxregistros = cantidadregistros Or Iterador = oListaPendientes.Count) Then
                            oiServicioNotificacion.ActualizarEstadoNotificaciones(oListaRespuestaBien, oListaRespuestaError)
                            oListaRespuestaBien.Clear()
                            oListaRespuestaError.Clear()
                            cantidadregistros = 0
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex.Message)
            Throw ex
        End Try
        Return 1
    End Function

    Public Shared Sub Write(m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToLongTimeString() & "   >>>>" & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub


    Public Function RealizaNotificacion() As Integer
        Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
            Dim obeConsultServNotifResponse As BEConsultServNotifResponse = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarNotificacionesListasANotificarSaga()
            RealizarNotificacionesPendientes(obeConsultServNotifResponse.ServNotificacionPendiente)
            RealizarReNotificacionesErroneas(obeConsultServNotifResponse.ServNotificacionErroneas)
        End Using
    End Function

    Private Function RndConvertBase64(texto As String) As String
        Dim plainTextBytes = System.Text.Encoding.UTF8.GetBytes(texto)
        Return System.Convert.ToBase64String(plainTextBytes)
    End Function
    'SagaInicio
    Public Function RealizarConsumoServicioSaga(ByVal oBEServicioNotificacion As BEServicioNotificacion) As confirmarPagoOCResponse
        'Dim oConfirmarPagoOCService As New ConfirmarPagoOCService 'Referencio el ws

        Dim Conexionsss As New ProxyBase(Of IServicioNotificacion)
        Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
        Dim idLogSagaRequest As Integer
        Dim obeLogSagaRequest As New BELogWebServiceSagaRequest
        Dim obeLogSagaResponse As New BELogWebServiceSagaResponse

        '1. Registro el Log Request de Saga
        obeLogSagaRequest.OrdenPago = oBEServicioNotificacion.IdOrdenPago
        obeLogSagaRequest.OrderIdSaga = oBEServicioNotificacion.OrderIdSaga
        obeLogSagaRequest.FechaPago = oBEServicioNotificacion.FechaPagoSaga
        obeLogSagaRequest.FechaContable = oBEServicioNotificacion.FechaContableSaga
        obeLogSagaRequest.MontoCancelado = oBEServicioNotificacion.MontoCanceladoSaga
        obeLogSagaRequest.CajaSaga = oBEServicioNotificacion.CajaSaga
        obeLogSagaRequest.WebServiceSagaURL = ConfigurationManager.AppSettings("urlSagaWS")




        Dim wsRequest As New SagaConnect
        Dim confirmarRequest As New confirmarPagoOCRequest
        Dim confirmarResponse As New confirmarPagoOCResponse

        Try
            confirmarRequest.urlWS = ConfigurationManager.AppSettings("urlSagaWS")
            confirmarRequest.usuario = ConfigurationManager.AppSettings("userSagaWS")
            confirmarRequest.password = ConfigurationManager.AppSettings("pwdSagaWS")

            confirmarRequest.orderId = oBEServicioNotificacion.OrderIdSaga '"2014051902"
            confirmarRequest.fechaPago = String.Format("{0:s}.0Z", oBEServicioNotificacion.FechaPagoSaga) 'String.Format("{0:s}.0Z", DateTime.Now)
            confirmarRequest.fechaContable = String.Format("{0:yyyy-MM-dd}", oBEServicioNotificacion.FechaContableSaga) 'String.Format("{0:yyyy-MM-dd}", DateTime.Now)
            confirmarRequest.montoCancelado = oBEServicioNotificacion.MontoCanceladoSaga '"35.43"
            confirmarRequest.caja = oBEServicioNotificacion.CajaSaga
            confirmarRequest.canal = oBEServicioNotificacion.Canal
            confirmarRequest.codigoAutorizacion = Right(oBEServicioNotificacion.CodigoAutorizacionServipagSaga, 8)

            'Modificacion M
            'Parametros por Defecto
            confirmarRequest.random = RndConvertBase64("pe" + DateTime.Now.ToString("MMddyyyyHHmmssffffff"))
            'confirmarRq.codigoAutorizacion = "123"
            'confirmarRq.caja = "7"
            'confirmarRq.canal = "Caja"

            'Construccion de Trama
            Dim oRequest As New StringBuilder()
            oRequest.Append("<soapenv:Envelope xmlns:cli='http://mdwcorp.falabella.com/common/schema/clientservice' xmlns:oas='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:req='http://mdwcorp.falabella.com/OSB/schema/CORP/CORP/PurchaseOrder/ConfirmarPago/Req-v2013.06' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>")
            oRequest.Append("<soapenv:Header>")
            oRequest.Append("<cli:ClientService>")
            oRequest.Append("<cli:country>PE</cli:country>")
            oRequest.Append("<cli:commerce>Falabella</cli:commerce>")
            oRequest.Append("<cli:channel>Presencial</cli:channel>")
            oRequest.Append("<cli:storeId>1</cli:storeId>")
            oRequest.Append("</cli:ClientService>")

            oRequest.Append("<oas:Security soapenv:mustUnderstand='1'>")
            oRequest.Append("<wsse:UsernameToken wsu:Id='UsernameToken-13' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>")
            oRequest.AppendFormat("<wsse:Username>{0}</wsse:Username>", confirmarRequest.usuario)
            oRequest.AppendFormat("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>{0}</wsse:Password>", confirmarRequest.password)
            oRequest.AppendFormat("<wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>{0}</wsse:Nonce>", confirmarRequest.random)
            oRequest.AppendFormat("<wsu:Created>{0}</wsu:Created>", [String].Format("{0:s}.0Z", DateTime.Now))
            oRequest.Append("</wsse:UsernameToken>")

            oRequest.Append("</oas:Security>")
            oRequest.Append("</soapenv:Header>")
            oRequest.Append("<soapenv:Body>")
            oRequest.Append("<req:confirmarPagoOCRequest>")
            oRequest.AppendFormat("<req:orderId>{0}</req:orderId>", confirmarRequest.orderId)
            oRequest.AppendFormat("<req:fechaPago>{0}</req:fechaPago>", confirmarRequest.fechaPago)
            oRequest.AppendFormat("<req:fechaContable>{0}</req:fechaContable>", confirmarRequest.fechaContable)
            oRequest.AppendFormat("<req:montoCancelado>{0}</req:montoCancelado>", confirmarRequest.montoCancelado)
            oRequest.AppendFormat("<req:canal>{0}</req:canal>", confirmarRequest.canal)
            oRequest.AppendFormat("<req:caja>{0}</req:caja>", confirmarRequest.caja)
            oRequest.AppendFormat("<req:codigoAutorizacion>{0}</req:codigoAutorizacion>", confirmarRequest.codigoAutorizacion)
            oRequest.Append("</req:confirmarPagoOCRequest>")
            oRequest.Append("</soapenv:Body>")
            oRequest.Append("</soapenv:Envelope>")

            obeLogSagaRequest.TramaXML = oRequest.ToString
            confirmarRequest.TramaXML = oRequest.ToString
            idLogSagaRequest = oiServicioNotificacion.RegistrarLogConsumoWSSagaRequest(obeLogSagaRequest)
            'End Modif M
            confirmarResponse = wsRequest.ConfirmarPago(confirmarRequest)

        Catch ex As Exception
            Throw ex
            confirmarResponse.codigoRespuesta = 0
            confirmarResponse.glosaRespuesta = ex.Message
            obeLogSagaResponse.MensajeError = ex.Message
        End Try


        '2. Registro el Log Request de Saga
        obeLogSagaResponse.LogIDRequest = idLogSagaRequest
        obeLogSagaResponse.OrdenPago = oBEServicioNotificacion.IdOrdenPago
        obeLogSagaResponse.OrderIdSaga = oBEServicioNotificacion.OrderIdSaga
        obeLogSagaResponse.CodigoRespuesta = confirmarResponse.codigoRespuesta
        obeLogSagaResponse.GlosaRespuesta = confirmarResponse.glosaRespuesta
        obeLogSagaResponse.WebServiceSagaURL = ConfigurationManager.AppSettings("urlSagaWS")
        obeLogSagaResponse.Xml = confirmarResponse.xml
        oiServicioNotificacion.RegistrarLogConsumoWSSagaResponse(obeLogSagaResponse)

        Return confirmarResponse
    End Function
    'SagaFin

#Region "Logica Request Generica "
    Public Function RealizarNotificacionQueryStrings(ByVal url As String, ByVal strProceso As String) As Integer
        Try
            Dim g As HttpWebRequest = CType(WebRequest.Create(url),  _
              HttpWebRequest)
            Using r As HttpWebResponse = CType(g.GetResponse, HttpWebResponse)
                g.Timeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("NotificacionRequestTimeout"))
                Dim path As String = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings("logNotificacionRuta")
                If Not System.IO.File.Exists(path) Then
                    Using file As FileStream = System.IO.File.Create(path)
                    End Using
                End If
                Using tw As TextWriter = New StreamWriter(path, True)
                    tw.WriteLine(DateTime.Now.ToString + " for " + url + ": " + r.StatusCode.ToString + " - " + strProceso)
                End Using
            End Using
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
        '
        Return 1
        '
    End Function
#End Region
#Region "Logica Request-Post Generica "
    Public Function RealizarNotificacionPost(ByVal url As String, ByVal strProceso As String) As Integer

        Try

            Dim uri As New Uri(url)
            Dim pathURL As String = uri.GetLeftPart(UriPartial.Path)
            Dim qryURL As String = String.Empty
            If uri.AbsoluteUri.Split("?").Length > 1 Then
                qryURL = uri.AbsoluteUri.Split("?")(1)
            End If
            pathURL = uri.GetLeftPart(UriPartial.Path)
            Dim request As HttpWebRequest = CType(WebRequest.Create(pathURL), HttpWebRequest)
            Dim data As Byte() = Encoding.ASCII.GetBytes(qryURL)
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = data.Length
            Dim newStream As Stream = request.GetRequestStream()
            newStream.Write(data, 0, data.Length)
            newStream.Close()
            Using r As HttpWebResponse = CType(request.GetResponse, HttpWebResponse)
                request.Timeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("NotificacionRequestTimeout"))
                Dim path As String = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings("logNotificacionRuta")
                If Not System.IO.File.Exists(path) Then
                    Using file As FileStream = System.IO.File.Create(path)
                    End Using
                End If
                Using tw As TextWriter = New StreamWriter(path, True)
                    tw.WriteLine(DateTime.Now.ToString + " for " + pathURL + ": " + r.StatusCode.ToString + " - " + strProceso)
                End Using
            End Using
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
        Return 1
    End Function
#End Region




    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region



End Class

