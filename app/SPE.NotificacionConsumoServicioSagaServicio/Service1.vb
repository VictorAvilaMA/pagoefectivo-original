﻿Imports System.ComponentModel
Imports SPE.NotificacionConsumoServicioSagaServicio
Imports System.Configuration
Imports System.Globalization
Imports SPE.NotificacionConsumoServicioSagaConsola

Public Class Service1
    Dim bw As New BackgroundWorker

    Private myTimerEnvioSaga As System.Timers.Timer

    Sub mensaje(s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub
    Public Sub New()
        InitializeComponent()
        '' This call is required by the Windows Form Designer.
        'InitializeComponent()
        'AddHandler bw.DoWork, AddressOf bw_DoWork
        'AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged
        'AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
        '' Add code here to start your service. This method should set things
        '' in motion so your service can do its work.
        'Try
        '    bw.RunWorkerAsync()
        'Catch ex As Exception
        '    CServicioNotificacionSFTP.Write(ex.ToString)
        '    EventLog.WriteEntry("No se puedo iniciar el Servicio de Noficiacion - Pago Efectivo: " + ex.Message)
        'End Try
    End Sub
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            myTimerEnvioSaga = New System.Timers.Timer()
            myTimerEnvioSaga.Interval = Convert.ToDouble(ConfigurationManager.AppSettings("Intervalo"))
            AddHandler myTimerEnvioSaga.Elapsed, New System.Timers.ElapsedEventHandler(AddressOf myTimerEnvioSaga_Elapsed)

            myTimerEnvioSaga.Enabled = True
        Catch ex As Exception
            EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
            CServicioNotificacion.Write(ex.ToString)
        End Try
    End Sub

    Private Sub myTimerEnvioSaga_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs)
        Try
            'Detiene el Timer
            myTimerEnvioSaga.Enabled = False

            Dim proxy As New CServicioNotificacion
            proxy.RealizaNotificacion()

            myTimerEnvioSaga.Enabled = True
        Catch ex As Exception
            EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
            CServicioNotificacion.Write(ex.ToString)
            myTimerEnvioSaga.Enabled = True
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub bw_DoWork(sender As Object, e As DoWorkEventArgs)
        'While True
        '    Try
        '        Dim proxy As New CServicioNotificacionSFTP
        '        proxy.EnvioXML()
        '        Dim tiempomiliseg As Long = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("Intervalo"))
        '        Threading.Thread.Sleep(tiempomiliseg)
        '    Catch ex As Exception
        '        EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
        '        CServicioNotificacionSFTP.Write(ex.ToString)
        '    End Try
        'End While
    End Sub
    Private Sub bw_ProgressChanged(sender As Object, e As ProgressChangedEventArgs)
        'Throw New NotImplementedException
    End Sub
    Private Sub bw_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        'Throw New NotImplementedException
    End Sub
End Class
