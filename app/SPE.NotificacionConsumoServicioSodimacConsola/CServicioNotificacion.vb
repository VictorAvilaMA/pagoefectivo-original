Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports System.Text

Imports SPE.Utilitario.UtilSodimacConnect

Public Class CServicioNotificacion
    Implements IDisposable

    '<upd Peru.Com FaseIII> Se modifico la logica de notificacion para la nueva version
    Public Function RealizarNotificacionesPendientes(ByVal oListaPendientes As List(Of BEServicioNotificacion)) As Integer
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                Dim oListaRespuestaBien, oListaRespuestaError As New List(Of BEServicioNotificacion)
                'SodimacInicio
                Dim oBEServicioNotificacionParaSodimac As New BEServicioNotificacion()
                Dim IntRespServicioSodimac As Integer
                Dim objToServiceRs As New confirmarPagoOCResponse
                'SodimacFin
                If (oListaPendientes.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In oListaPendientes
                        Try
                            Dim procesodesc As String = String.Format("idOrdenPago={0}|idMovimiento={1}|UrlRespuesta={2}|", obeServicioNotif.IdOrdenPago, obeServicioNotif.IdMovimiento, obeServicioNotif.UrlRespuesta)

                            Select Case obeServicioNotif.IdTipoServNotificacion
                                'SodimacInicio
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSodimac
                                    'If obeServicioNotif.IdTipoMovimiento = 34 Then
                                    oBEServicioNotificacionParaSodimac = oiServicioNotificacion.ConsultarDatosParaServicioSodimac(obeServicioNotif.IdOrdenPago)
                                    oBEServicioNotificacionParaSodimac.IdOrdenPago = obeServicioNotif.IdOrdenPago
                                    objToServiceRs = RealizarConsumoServicioSodimac(oBEServicioNotificacionParaSodimac)
                                    'End If
                                    If objToServiceRs.codigoRespuesta = 0 Then
                                        obeServicioNotif.Observacion = "Notificaci�n Satisfactoria"
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Notificado
                                        oListaRespuestaBien.Add(obeServicioNotif)
                                    Else
                                        obeServicioNotif.Observacion = String.Format(objToServiceRs.glosaRespuesta)
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                                        oListaRespuestaError.Add(obeServicioNotif)
                                    End If
                                    'SodimacFin
                            End Select
                        Catch ex As Exception
                            obeServicioNotif.Observacion = String.Format("Error :{0}", ex.Message)
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                            oListaRespuestaError.Add(obeServicioNotif)
                        End Try

                    Next
                    oiServicioNotificacion.ActualizarEstadoNotificaciones(oListaRespuestaBien, oListaRespuestaError)
                End If
            End Using
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex.Message)
            Throw ex
        End Try
        Return 1
    End Function

    Dim cantidadMaxregistros As Integer = 20

    '<upd Peru.Com FaseIII> Se modifico la logica de notificacion para la nueva version
    '<upd Peru.Com FaseIII> Se optimizo para el envio de paketes fraccionados en bloques de 20
    Public Function RealizarReNotificacionesErroneas(ByVal oListaPendientes As List(Of BEServicioNotificacion)) As Integer
        Try
            RealizarReenvioNotificacionErroneasEstado284()
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())

                Dim oListaRespuestaBien, oListaRespuestaError As New List(Of BEServicioNotificacion)
                Dim cantidadregistros As Integer = 0
                Dim Iterador As Integer = 0

                'SodimacInicio
                Dim oBEServicioNotificacionParaSodimac As New BEServicioNotificacion()
                Dim IntRespServicioSodimac As Integer
                Dim objToServiceRs As New confirmarPagoOCResponse
                'SodimacFin

                If (oListaPendientes.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In oListaPendientes
                        cantidadregistros += 1
                        Iterador += 1
                        Try
                            Dim procesodesc As String = String.Format("idOrdenPago={0}|idMovimiento={1}|UrlRespuesta={2}|", obeServicioNotif.IdOrdenPago, obeServicioNotif.IdMovimiento, obeServicioNotif.UrlRespuesta)
                            Select Case obeServicioNotif.IdTipoServNotificacion
                                'SodimacInicio
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSodimac
                                    'If obeServicioNotif.IdTipoMovimiento = 34 Then
                                    oBEServicioNotificacionParaSodimac = oiServicioNotificacion.ConsultarDatosParaServicioSodimac(obeServicioNotif.IdOrdenPago)
                                    oBEServicioNotificacionParaSodimac.IdOrdenPago = obeServicioNotif.IdOrdenPago
                                    objToServiceRs = RealizarConsumoServicioSodimac(oBEServicioNotificacionParaSodimac)
                                    'End If
                                    If objToServiceRs.codigoRespuesta = 0 Then
                                        obeServicioNotif.Observacion = "Notificaci�n Satisfactoria"
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Notificado
                                        oListaRespuestaBien.Add(obeServicioNotif)
                                    Else
                                        obeServicioNotif.Observacion = String.Format(objToServiceRs.glosaRespuesta)
                                        obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                                        oListaRespuestaError.Add(obeServicioNotif)
                                    End If
                                    'SodimacFin
                            End Select
                        Catch ex As Exception
                            obeServicioNotif.Observacion = String.Format("Error :{0}", ex.Message)
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                            oListaRespuestaError.Add(obeServicioNotif)
                        End Try
                        If (cantidadMaxregistros = cantidadregistros Or Iterador = oListaPendientes.Count) Then
                            oiServicioNotificacion.ActualizarEstadoNotificaciones(oListaRespuestaBien, oListaRespuestaError)
                            oListaRespuestaBien.Clear()
                            oListaRespuestaError.Clear()
                            cantidadregistros = 0
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex.Message)
            Throw ex
        End Try
        Return 1
    End Function

    Private Sub RealizarReenvioNotificacionErroneasEstado284()
        Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
            Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            Dim oBEServicioNotificacionReenvioSodimac As New List(Of BEServicioNotificacion)
            oBEServicioNotificacionReenvioSodimac = oiServicioNotificacion.ConsultarServicioNotificacionReenvioSodimac()

            For Each obeServicioNotif As BEServicioNotificacion In oBEServicioNotificacionReenvioSodimac
                obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorIntentoNotificado
                oiServicioNotificacion.ActualizarEstadoNotificacionReenvioSodimac(obeServicioNotif)
            Next
        End Using

    End Sub

    Public Shared Sub Write(ByVal m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToLongTimeString() & "   >>>>" & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' Para detectar llamadas redundantes

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: eliminar estado administrado (objetos administrados).
            End If

            ' TODO: liberar recursos no administrados (objetos no administrados) e invalidar Finalize() below.
            ' TODO: Establecer campos grandes como Null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: invalidar Finalize() s�lo si la instrucci�n Dispose(ByVal disposing As Boolean) anterior tiene c�digo para liberar recursos no administrados.
    'Protected Overrides Sub Finalize()
    '    ' No cambie este c�digo. Ponga el c�digo de limpieza en la instrucci�n Dispose(ByVal disposing As Boolean) anterior.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' Visual Basic agreg� este c�digo para implementar correctamente el modelo descartable.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' No cambie este c�digo. Coloque el c�digo de limpieza en Dispose (ByVal que se dispone como Boolean).
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Function RealizaNotificacion() As Integer
        Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
            Dim obeConsultServNotifResponse As BEConsultServNotifResponse = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarNotificacionesListasANotificarSodimac()
            RealizarNotificacionesPendientes(obeConsultServNotifResponse.ServNotificacionPendiente)
            RealizarReNotificacionesErroneas(obeConsultServNotifResponse.ServNotificacionErroneas)
        End Using
    End Function

    Private Function RndConvertBase64(ByVal texto As String) As String
        Dim plainTextBytes = System.Text.Encoding.UTF8.GetBytes(texto)
        Return System.Convert.ToBase64String(plainTextBytes)
    End Function

    Public Function RealizarConsumoServicioSodimac(ByVal oBEServicioNotificacion As BEServicioNotificacion) As confirmarPagoOCResponse
        'Dim oConfirmarPagoOCService As New ConfirmarPagoOCService 'Referencio el ws

        Dim Conexionsss As New ProxyBase(Of IServicioNotificacion)
        Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
        Dim idLogSodimacRequest As Integer
        Dim obeLogSodimacRequest As New BELogWebServiceSodimacRequest
        Dim obeLogSodimacResponse As New BELogWebServiceSodimacResponse

        '1. Registro el Log Request de Sodimac
        obeLogSodimacRequest.OrdenPago = oBEServicioNotificacion.IdOrdenPago
        obeLogSodimacRequest.OrderIdSodimac = oBEServicioNotificacion.OrderIdSodimac
        obeLogSodimacRequest.FechaPago = oBEServicioNotificacion.FechaPagoSodimac
        obeLogSodimacRequest.FechaContable = oBEServicioNotificacion.FechaContableSodimac
        obeLogSodimacRequest.MontoCancelado = oBEServicioNotificacion.MontoCanceladoSodimac
        obeLogSodimacRequest.CajaSodimac = oBEServicioNotificacion.CajaSodimac
        obeLogSodimacRequest.WebServiceSodimacURL = ConfigurationManager.AppSettings("Url_Sodimac")

        Dim wsRequest As New SodimacConnect
        Dim confirmarRequest As New confirmarPagoOCRequest
        Dim confirmarResponse As New confirmarPagoOCResponse

        Try
            confirmarRequest.urlWS = ConfigurationManager.AppSettings("Url_Sodimac")
            confirmarRequest.usuario = ConfigurationManager.AppSettings("Usuario")
            confirmarRequest.password = ConfigurationManager.AppSettings("Password")

            confirmarRequest.orderId = oBEServicioNotificacion.OrderIdSodimac '"2014051902"
            confirmarRequest.fechaPago = String.Format("{0:s}.0Z", oBEServicioNotificacion.FechaPagoSodimac) 'String.Format("{0:s}.0Z", DateTime.Now)
            confirmarRequest.fechaContable = String.Format("{0:yyyy-MM-dd}", oBEServicioNotificacion.FechaContableSodimac) 'String.Format("{0:yyyy-MM-dd}", DateTime.Now)
            confirmarRequest.montoCancelado = oBEServicioNotificacion.MontoCanceladoSodimac '"35.43"
            confirmarRequest.caja = oBEServicioNotificacion.CajaSodimac
            confirmarRequest.canal = oBEServicioNotificacion.Canal
            confirmarRequest.codigoAutorizacion = Right(oBEServicioNotificacion.CodigoAutorizacionServipagSodimac, 8)

            confirmarRequest.random = RndConvertBase64("pe" + DateTime.Now.ToString("MMddyyyyHHmmssffffff"))

            'Construccion de Trama
            Dim oRequest As New StringBuilder()
            Dim random As String = RndConvertBase64("pe" + DateTime.Now.ToString("MMddyyyyHHmmssffffff"))
            oRequest.Append("<soapenv:Envelope xmlns:cli='http://mdwcorp.falabella.com/common/schema/clientservice' xmlns:oas='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:req='http://mdwcorp.falabella.com/OSB/schema/CORP/CORP/PurchaseOrder/ConfirmarPago/Req-v2013.06' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>")
            oRequest.Append("<soapenv:Header>")
            oRequest.Append("<cli:ClientService>")
            oRequest.Append("<cli:country>" + ConfigurationManager.AppSettings("Country").ToString() + "</cli:country>")
            oRequest.Append("<cli:commerce>" + ConfigurationManager.AppSettings("Commerce").ToString() + "</cli:commerce>")
            oRequest.Append("<cli:channel>" + ConfigurationManager.AppSettings("Channel").ToString() + "</cli:channel>")
            oRequest.Append("<cli:storeId>" + ConfigurationManager.AppSettings("StoreId").ToString() + "</cli:storeId>")
            oRequest.Append("</cli:ClientService>")
            oRequest.Append("<oas:Security soapenv:mustUnderstand='1'>")
            oRequest.Append("<wsse:UsernameToken wsu:Id='UsernameToken-13' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>")
            oRequest.AppendFormat("<wsse:Username>{0}</wsse:Username>", ConfigurationManager.AppSettings("Usuario").ToString())
            oRequest.AppendFormat("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>{0}</wsse:Password>", ConfigurationManager.AppSettings("Password").ToString())
            oRequest.AppendFormat("<wsse:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>{0}</wsse:Nonce>", random)
            oRequest.AppendFormat("<wsu:Created>{0}</wsu:Created>", String.Format("{0:s}.0Z", DateTime.Now))
            oRequest.Append("</wsse:UsernameToken>")
            oRequest.Append("</oas:Security>")
            oRequest.Append("</soapenv:Header>")
            oRequest.Append("<soapenv:Body>")
            oRequest.Append("<req:confirmarPagoOCRequest>")
            oRequest.AppendFormat("<req:orderId>{0}</req:orderId>", confirmarRequest.orderId)
            oRequest.AppendFormat("<req:fechaPago>{0}</req:fechaPago>", confirmarRequest.fechaPago)
            oRequest.AppendFormat("<req:fechaContable>{0}</req:fechaContable>", confirmarRequest.fechaContable)
            oRequest.AppendFormat("<req:montoCancelado>{0}</req:montoCancelado>", confirmarRequest.montoCancelado)
            oRequest.AppendFormat("<req:canal>{0}</req:canal>", confirmarRequest.canal)
            oRequest.AppendFormat("<req:caja>{0}</req:caja>", confirmarRequest.caja)
            oRequest.AppendFormat("<req:codigoAutorizacion>{0}</req:codigoAutorizacion>", confirmarRequest.codigoAutorizacion)
            oRequest.Append("</req:confirmarPagoOCRequest>")
            oRequest.Append("</soapenv:Body>")
            oRequest.Append("</soapenv:Envelope>")

            obeLogSodimacRequest.TramaXML = oRequest.ToString
            confirmarRequest.TramaXML = oRequest.ToString
            idLogSodimacRequest = oiServicioNotificacion.RegistrarLogConsumoWSSodimacRequest(obeLogSodimacRequest)
            'End Modif M
            confirmarResponse = wsRequest.ConfirmarPago(confirmarRequest)

            ''''''''''''''''''''''''''''''REGISTRAR TRAMA EN NOTEPAD DISCO D''''''''''''''''''''''''''''''''''''
            'Dim sw As New IO.StreamWriter("D:\\Trama.txt", True)
            'sw.WriteLine("------------------------------Inicio de Trama------------------------------")
            'sw.WriteLine(oRequest.ToString)
            'sw.WriteLine("-------------------------------Fin de Trama--------------------------------")
            'sw.Close()
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Catch ex As Exception
            ''''''''''''''''''''''''''''''REGISTRAR EXCEPTION EN NOTEPAD DISCO D''''''''''''''''''''''''''''''''''''
            'Dim sw As New IO.StreamWriter("D:\\Exception.txt", True)
            'sw.WriteLine("------------------------------Inicio de Excepcion------------------------------")
            'sw.WriteLine(ex.Message)
            'sw.WriteLine("-------------------------------Fin de Excepcion--------------------------------")
            'sw.Close()
            ''''''''''''''''''''''''''''''REGISTRAR EXCEPTION EN NOTEPAD DISCO D''''''''''''''''''''''''''''''''''''

            confirmarResponse.codigoRespuesta = 0
            confirmarResponse.glosaRespuesta = ex.Message
            obeLogSodimacResponse.MensajeError = ex.Message

            'Throw ex
        End Try

        '2. Registro el Log Request de Sodimac
        obeLogSodimacResponse.LogIDRequest = idLogSodimacRequest
        obeLogSodimacResponse.OrdenPago = oBEServicioNotificacion.IdOrdenPago
        obeLogSodimacResponse.OrderIdSodimac = oBEServicioNotificacion.OrderIdSodimac
        obeLogSodimacResponse.CodigoRespuesta = confirmarResponse.codigoRespuesta
        obeLogSodimacResponse.GlosaRespuesta = confirmarResponse.glosaRespuesta
        obeLogSodimacResponse.WebServiceSodimacURL = ConfigurationManager.AppSettings("Url_Sodimac")
        obeLogSodimacResponse.Xml = confirmarResponse.xml
        oiServicioNotificacion.RegistrarLogConsumoWSSodimacResponse(obeLogSodimacResponse)

        Return confirmarResponse
    End Function

End Class

