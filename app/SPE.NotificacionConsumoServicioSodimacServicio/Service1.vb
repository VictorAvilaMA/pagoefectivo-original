﻿Imports System.ComponentModel
Imports SPE.NotificacionConsumoServicioSodimacServicio
Imports System.Configuration
Imports System.Globalization
Imports SPE.NotificacionConsumoServicioSodimacConsola

Public Class Service1

    Private myTimerEnvioSodimac As System.Timers.Timer

    Sub mensaje(ByVal s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            myTimerEnvioSodimac = New System.Timers.Timer()
            myTimerEnvioSodimac.Interval = Convert.ToDouble(ConfigurationManager.AppSettings("Intervalo"))
            AddHandler myTimerEnvioSodimac.Elapsed, New System.Timers.ElapsedEventHandler(AddressOf myTimerEnvioSodimac_Elapsed)

            myTimerEnvioSodimac.Enabled = True
        Catch ex As Exception
            EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
            CServicioNotificacion.Write(ex.ToString)
        End Try
    End Sub

    Private Sub myTimerEnvioSodimac_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Try
            'Detiene el Timer
            myTimerEnvioSodimac.Enabled = False

            Dim proxy As New CServicioNotificacion
            proxy.RealizaNotificacion()

            myTimerEnvioSodimac.Enabled = True
        Catch ex As Exception
            EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
            CServicioNotificacion.Write(ex.ToString)
            myTimerEnvioSodimac.Enabled = True
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        'While True
        '    Try
        '        Dim proxy As New CServicioNotificacionSFTP
        '        proxy.EnvioXML()
        '        Dim tiempomiliseg As Long = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("Intervalo"))
        '        Threading.Thread.Sleep(tiempomiliseg)
        '    Catch ex As Exception
        '        EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
        '        CServicioNotificacionSFTP.Write(ex.ToString)
        '    End Try
        'End While
    End Sub
    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        'Throw New NotImplementedException
    End Sub
    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        'Throw New NotImplementedException
    End Sub

End Class
