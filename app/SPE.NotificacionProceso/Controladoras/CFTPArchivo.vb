Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports System.IO
Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Public Class CFTPArchivo


    Public Function ProcesarArchivoFTPParaNotificarPagadas() As Integer
        Dim listServicios As List(Of BEServicio) = CType(SPE.NotificacionProceso.RemoteServices.Instance, SPE.NotificacionProceso.RemoteServices).IFTPArchivo.ProcesarArchivoFTPParaNotificar()

        For Each beServicio As BEServicio In listServicios
            Try
                For Each bearchivo As BEFTPArchivo In beServicio.FTPArchivos
                    Try
                        ArchivoUtil.RealizarNotificacionArchivoFTP(bearchivo, beServicio.UrlFTP, beServicio.Usuario, beServicio.Password)
                    Catch ex As Exception
                        'Logger.Write(ex)
                    End Try
                Next
            Catch ex As Exception
                'Logger.Write(ex)
            End Try
        Next
    End Function
    'Public Function ProcesarArchivoFTPParaNotificarPagadas() As Integer

    '    Dim ruta As String = RutaArchivo()
    '    Dim listftpArchivos As List(Of BEFTPArchivo) = CType(SPE.NotificacionProceso.RemoteServices.Instance, SPE.NotificacionProceso.RemoteServices).IFTPArchivo.ProcesarArchivoFTPParaNotificar()
    '    For Each be As BEFTPArchivo In listftpArchivos
    '        Try
    '            GenerarArchivoPagadas(be)
    '            ArchivoUtil.RealizarNotificacionArchivoFTP(be)
    '        Catch ex As Exception
    '            'System.Diagnostics.EventLog.WriteEntry("Pago Efectio - Error Al enviar Archivo FTP :" + ex.Message)
    '        End Try

    '    Next
    '    Return 1
    'End Function



    'Public Function ProcesarArchivoFTPParaNotificarExpiradas() As Integer

    '    Dim ruta As String = RutaArchivo()
    '    Dim listftpArchivos As List(Of BEFTPArchivo) = CType(SPE.NotificacionProceso.RemoteServices.Instance, SPE.NotificacionProceso.RemoteServices).IFTPArchivo.ProcesarArchivoFTPParaNotificar()
    '    For Each be As BEFTPArchivo In listftpArchivos
    '        Try
    '            GenerarArchivoExpiradas(be)
    '            ArchivoUtil.RealizarNotificacionArchivoFTP(be)
    '        Catch ex As Exception
    '            System.Diagnostics.EventLog.WriteEntry("Pago Efectio - Error Al enviar Archivo FTP :" + ex.Message)
    '        End Try

    '    Next
    '    Return 1
    'End Function



End Class
