﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SPE.NotificacionProceso")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("3dev Business & Consulting")> 
<Assembly: AssemblyProduct("SPE.NotificacionProceso")> 
<Assembly: AssemblyCopyright("Copyright © 3dev Business & Consulting 2009")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b4dd4693-1486-49cc-ae51-3f1b88ae5e8d")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
