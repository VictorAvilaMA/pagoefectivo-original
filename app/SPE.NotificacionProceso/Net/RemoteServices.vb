Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Public Class RemoteServices
    Inherits _3Dev.FW.Web.RemoteServices

    Private _IFTPArchivo As SPE.EmsambladoComun.IComun

    Public Property IFTPArchivo() As SPE.EmsambladoComun.IFTPArchivo
        Get
            Return _IFTPArchivo
        End Get
        Set(ByVal value As SPE.EmsambladoComun.IFTPArchivo)
            _IFTPArchivo = value
        End Set
    End Property


    Public Overrides Sub DoSetupNetworkEnviroment()
        MyBase.DoSetupNetworkEnviroment()

        Me.IFTPArchivo = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IFTPArchivo), Me.ServerUrl + "/" + NombreServiciosConocidos.IFTPArchivo), SPE.EmsambladoComun.IFTPArchivo)
    End Sub

End Class
