Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Module NotificacionProceso

    Private oCFTPArchivo As CFTPArchivo = New CFTPArchivo

    Sub Main()
        Try
            Console.ReadLine()
            DoStartRemotingServices()
            ProcesarArchivoFTPParaNotificar()
            '  Console.ReadLine()

        Catch ex As Exception
            'Logger.Write(ex)
        End Try
    End Sub
    Private Sub DoStartRemotingServices()
        Try
            SPE.NotificacionProceso.RemoteServices.Instance = New SPE.NotificacionProceso.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try


        If Not EstablishConnection() Then
            Return
        End If
    End Sub
    Function EstablishConnection() As Boolean
        Return SPE.NotificacionProceso.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function

    Public Sub ProcesarArchivoFTPParaNotificar()
        oCFTPArchivo.ProcesarArchivoFTPParaNotificarPagadas()
    End Sub

End Module
