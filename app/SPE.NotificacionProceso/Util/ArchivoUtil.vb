Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports System.IO
Imports System.Net

Public Class ArchivoUtil

    Public Shared Function CrearDirectorioServicio(ByVal ruta As String, ByVal nombredirectorio As String) As String


        Return ""
    End Function


    Public Shared Function RealizarNotificacionArchivoFTP(ByVal beftpArchivo As BEFTPArchivo, ByVal urlftp As String, ByVal login As String, ByVal password As String) As Integer

        'Dim X As MemoryStream = New MemoryStream()
        'Dim rutaArchivo = System.Configuration.ConfigurationManager.AppSettings.Get("RutaArchivosFTP") + "/" + beftpArchivo.NombreArchivo
        Dim UsePassive As Boolean = System.Configuration.ConfigurationManager.AppSettings("UsePassive").ToUpper() = "true".ToUpper()
        Using fs As New MemoryStream(beftpArchivo.ArchivoBinario)
            Dim url As String = Path.Combine(urlftp, Path.GetFileName(beftpArchivo.NombreArchivo))
            Dim ftp As FtpWebRequest = FtpWebRequest.Create(url)
            ftp.Credentials = New NetworkCredential(login, password)
            ftp.KeepAlive = False
            ftp.Method = WebRequestMethods.Ftp.UploadFile
            ftp.UseBinary = True
            ftp.ContentLength = fs.Length
            ftp.Proxy = Nothing
            ftp.UsePassive = UsePassive

            fs.Position = 0

            Dim buffLength As Integer = 2048
            Dim buff() As Byte = New Byte(buffLength) {}
            Dim contentlen As Integer

            Using strm As Stream = ftp.GetRequestStream()
                contentlen = fs.Read(buff, 0, buffLength)
                While contentlen <> 0
                    strm.Write(buff, 0, contentlen)
                    contentlen = fs.Read(buff, 0, buffLength)
                End While
            End Using

        End Using
        Return 1

    End Function

    'Public Shared Function RealizarNotificacionArchivoFTP(ByVal beftpArchivo As BEFTPArchivo, ByVal urlftp As String, ByVal login As String, ByVal password As String) As Integer

    '    Dim rutaArchivo = System.Configuration.ConfigurationManager.AppSettings.Get("RutaArchivosFTP") + "/" + beftpArchivo.NombreArchivo
    '    'beftpArchivo.ArchivoBinario
    '    Using fs As New FileStream(rutaArchivo, FileMode.Open, FileAccess.Read, FileShare.Read)
    '        Dim url As String = Path.Combine(urlftp, Path.GetFileName(rutaArchivo))
    '        Dim ftp As FtpWebRequest = FtpWebRequest.Create(url)
    '        ftp.Credentials = New NetworkCredential(login, password)
    '        ftp.KeepAlive = False
    '        ftp.Method = WebRequestMethods.Ftp.UploadFile
    '        ftp.UseBinary = True
    '        ftp.ContentLength = fs.Length
    '        ftp.Proxy = Nothing
    '        fs.Position = 0
    '        Dim buffLength As Integer = 2048
    '        Dim buff() As Byte = New Byte(buffLength) {}
    '        Dim contentlen As Integer

    '        Using strm As Stream = ftp.GetRequestStream()
    '            contentlen = fs.Read(buff, 0, buffLength)
    '            While contentlen <> 0
    '                strm.Write(buff, 0, contentlen)
    '                contentlen = fs.Read(buff, 0, buffLength)
    '            End While
    '        End Using

    '    End Using
    '    Return 1

    'End Function
End Class
