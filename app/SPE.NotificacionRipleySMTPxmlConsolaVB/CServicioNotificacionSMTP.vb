﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades

Imports System.Text
Imports System.Xml
Imports tss = Tamir.SharpSsh
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun
Imports System.Net.Mail
Imports System.Net.Mime

Public Class CServicioNotificacionSMTP
    Implements IDisposable

    Public Sub EnvioXMLForRipleyTxt()
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oIServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())

                Dim xAsunto As String = ConfigurationManager.AppSettings.Get("Asunto")

                Dim xFromAddress As String = ConfigurationManager.AppSettings.Get("FromAddress")
                Dim xToAddressRipley As String = ConfigurationManager.AppSettings.Get("ToAddressRipley")
                Dim xUserID As String = ConfigurationManager.AppSettings.Get("UserID")
                Dim xPassword As String = ConfigurationManager.AppSettings.Get("Password")
                Dim xReplyTo As String = ConfigurationManager.AppSettings.Get("ReplyTo")
                Dim xSMTPClient As String = ConfigurationManager.AppSettings.Get("SMTPClient")
                Dim iSMTPPort As String = Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort"))
                Dim lEnableSSL As Boolean
                If (ConfigurationManager.AppSettings.Get("EnableSSL").ToUpper() = "YES") Then
                    lEnableSSL = True
                Else
                    lEnableSSL = False
                End If

                Dim oListaBEServicioNotificacion As New List(Of BEServicioNotificacion)()
                oListaBEServicioNotificacion = oIServicioNotificacion.ConsultarOrdenesPagoRipley()


                If oListaBEServicioNotificacion.Count > 0 Then

                    Dim FechaFRipley As Date = Date.Now
                    'PE+C+AÑO+MES+DIA+HORA+MINUTOS+SEGUNDOS Ejemplo (PEC20150213136787)
                    Dim NombreArchivoXML As String = "PEC" + FechaFRipley.ToString("yyyyMMddHHmmss") + ".txt"

                    Dim memoryStream = New MemoryStream()
                    Dim stream_reader As StreamWriter = New StreamWriter(memoryStream)


                    For Each RegistroOP As BEServicioNotificacion In oListaBEServicioNotificacion
                        stream_reader.Write(RegistroOP.FechaPagoRipley.ToString("ddMMyyyyHHmmss")) 'FechaPago_
                        stream_reader.Write(RegistroOP.NumeroOrdenDePago) 'CIP_
                        stream_reader.Write("000" + RegistroOP.OrderIdRipley) 'CodTransaccion_
                        stream_reader.Write(LlenarCeros(RegistroOP.MontoCanceladoRipley)) 'Monto_
                        stream_reader.Write(RegistroOP.CajaRipley) 'Banco_
                        stream_reader.Write(System.Environment.NewLine)
                    Next RegistroOP

                    stream_reader.Flush()
                    memoryStream.Position = 0

                    Dim mail As MailMessage = New MailMessage()
                    mail.From = New MailAddress(xUserID)
                    mail.To.Add(xToAddressRipley)

                    If (Not String.IsNullOrEmpty(xReplyTo) And xReplyTo.IndexOf("@") > -1) Then
                        Dim emails As String() = xReplyTo.Split(",")
                        For Each email As String In emails
                            'mail.ReplyTo = New MailAddress(email, email, System.Text.ASCIIEncoding.ASCII)
                            mail.To.Add(New MailAddress(email))
                        Next
                    End If
                    mail.Subject = xAsunto
                    mail.IsBodyHtml = False
                    mail.Body = "Email Conciliacion Ripley " + NombreArchivoXML

                    Dim attach As New Attachment(memoryStream, New ContentType(MediaTypeNames.Text.Plain))
                    attach.ContentDisposition.FileName = NombreArchivoXML
                    mail.Attachments.Add(attach)


                    Dim smtp As SmtpClient = New SmtpClient("smtp.gmail.com", 587)
                    smtp.EnableSsl = True
                    smtp.UseDefaultCredentials = False
                    smtp.Credentials = New NetworkCredential(xUserID, xPassword)
                    smtp.Send(mail)
                End If
            End Using
        Catch ex As Exception
            Throw ex
            Logger.Write(ex.Message)
        End Try

    End Sub

    'Public Sub EnvioXMLForRipleyXml()
    '    Try
    '        Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
    '            Dim oIServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())


    '            Dim xFromAddress As String = ConfigurationManager.AppSettings.Get("FromAddress")
    '            Dim xToAddressRipley As String = ConfigurationManager.AppSettings.Get("ToAddressRipley")
    '            Dim xUserID As String = ConfigurationManager.AppSettings.Get("UserID")
    '            Dim xPassword As String = ConfigurationManager.AppSettings.Get("Password")
    '            Dim xReplyTo As String = ConfigurationManager.AppSettings.Get("ReplyTo")
    '            Dim xSMTPClient As String = ConfigurationManager.AppSettings.Get("SMTPClient")
    '            Dim iSMTPPort As String = Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort"))
    '            Dim lEnableSSL As Boolean
    '            If (ConfigurationManager.AppSettings.Get("EnableSSL").ToUpper() = "YES") Then
    '                lEnableSSL = True
    '            Else
    '                lEnableSSL = False
    '            End If

    '            Dim oListaBEServicioNotificacion As New List(Of BEServicioNotificacion)()
    '            oListaBEServicioNotificacion = oIServicioNotificacion.ConsultarOrdenesPagoRipley()


    '            If oListaBEServicioNotificacion.Count > 0 Then

    '                Dim FechaFSaga As Date = Date.Now
    '                Dim NombreArchivoXML As String = FechaFSaga.ToString("ddMMyyyy") + "-" + FechaFSaga.ToString("HHmm") + ".xml"

    '                Dim utf8WithoutBom As New System.Text.UTF8Encoding(False)
    '                'Using sink As New StreamWriter(NombreArchivoXML, False, utf8WithoutBom)



    '                Dim writer As New XmlTextWriter(NombreArchivoXML, utf8WithoutBom)
    '                writer.WriteStartDocument(True)
    '                writer.Formatting = Formatting.Indented
    '                writer.Indentation = 2
    '                writer.WriteStartElement("Envelope")
    '                writer.WriteStartElement("Header")
    '                writer.WriteStartElement("ClientService")

    '                writer.WriteStartElement("country")
    '                writer.WriteString("PE")
    '                writer.WriteEndElement()
    '                writer.WriteStartElement("commerce")
    '                writer.WriteString("Falabella")
    '                writer.WriteEndElement()
    '                writer.WriteStartElement("channel")
    '                writer.WriteString("B2C")
    '                writer.WriteEndElement()
    '                writer.WriteStartElement("storeId")
    '                writer.WriteString("1")
    '                writer.WriteEndElement()

    '                writer.WriteEndElement()
    '                writer.WriteEndElement()
    '                writer.WriteStartElement("Body")
    '                For Each RegistroOP As BEServicioNotificacion In oListaBEServicioNotificacion
    '                    createNodeForRipley(RegistroOP.OrderIdSaga, RegistroOP.FechaPagoSaga, RegistroOP.FechaContableSaga, RegistroOP.MontoCanceladoSaga, RegistroOP.Canal, RegistroOP.CajaSaga, RegistroOP.CodigoAutorizacionServipagSaga, writer)
    '                Next RegistroOP
    '                writer.WriteEndElement()
    '                writer.WriteEndElement()
    '                writer.WriteEndDocument()
    '                'writer.Flush()
    '                writer.Close()

    '                Dim fromFile As [String] = NombreArchivoXML

    '                'Dim sftpClient As tss.SshTransferProtocolBase
    '                'sftpClient = New tss.Sftp(hostServer, userName)
    '                'sftpClient.Password = password
    '                'sftpClient.Connect(port)
    '                'sftpClient.Put(fromFile, toFile)

    '                Dim mail As MailMessage = New MailMessage()
    '                mail.From = New MailAddress("smartcontact.support@gmail.com")
    '                mail.To.Add("edu@outlook.com.pe")

    '                If (Not String.IsNullOrEmpty(xReplyTo) And xReplyTo.IndexOf("@") > -1) Then
    '                    mail.ReplyTo = New MailAddress(xReplyTo, xReplyTo, System.Text.ASCIIEncoding.ASCII)
    '                End If

    '                mail.Subject = "Email Conciliacion Ripley " + NombreArchivoXML
    '                mail.IsBodyHtml = False
    '                mail.Body = "Email Conciliacion Ripley " + NombreArchivoXML
    '                mail.Attachments.Add(New Attachment(NombreArchivoXML, MediaTypeNames.Application.Octet))
    '                'mail.Attachments.Add(New Attachment(NombreArchivoXML, NombreArchivoXML, "text/xml"))
    '                Dim smtp As SmtpClient = New SmtpClient("smtp.gmail.com", 587)
    '                smtp.EnableSsl = True
    '                smtp.UseDefaultCredentials = False
    '                smtp.Credentials = New NetworkCredential("smartcontact.support@gmail.com", "**smartcontact**")
    '                smtp.Send(mail)

    '            End If
    '        End Using
    '    Catch ex As Exception
    '        Throw ex
    '        Logger.Write(ex.Message)
    '    End Try

    'End Sub

    Private Sub createNodeForRipley(ByVal pOrderId As String, ByVal pFechaPago As DateTime, ByVal pFechaContable As DateTime, ByVal pMontoCancelado As Decimal, ByVal pCanal As String, ByVal pCaja As String, ByVal pcodigoAutorizacionServipag As String, ByVal writer As XmlTextWriter)

        writer.WriteStartElement("confirmarPagoOCRequest")
        writer.WriteStartElement("orderId")
        writer.WriteString(pOrderId)
        writer.WriteEndElement()
        writer.WriteStartElement("fechaPago")
        writer.WriteString(pFechaPago.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.0Z'"))
        writer.WriteEndElement()
        writer.WriteStartElement("fechaContable")
        writer.WriteString(pFechaContable.ToString("yyyy'-'MM'-'dd"))
        writer.WriteEndElement()
        writer.WriteStartElement("montoCancelado")
        writer.WriteString(pMontoCancelado.ToString())
        writer.WriteEndElement()
        writer.WriteStartElement("canal")
        writer.WriteString(pCanal)
        writer.WriteEndElement()
        writer.WriteStartElement("caja")
        writer.WriteString(pCaja)
        writer.WriteEndElement()
        writer.WriteStartElement("codigoAutorizacion")
        writer.WriteString(pcodigoAutorizacionServipag)
        writer.WriteEndElement()
        writer.WriteEndElement()
    End Sub


    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Shared Sub Write(ByVal m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToLongTimeString() & "   >>>>" & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub

    Private Function LlenarCeros(monto As Decimal) As String
        Dim decString As String = monto.ToString().Replace(".", "")
        Return decString.ToString.PadLeft(18, "0")
    End Function

End Class