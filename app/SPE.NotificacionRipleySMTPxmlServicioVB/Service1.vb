﻿Imports System.ComponentModel
Imports SPE.NotificacionRipleySMTPxmlConsolaVB
Imports System.Configuration
Imports System.Globalization

Imports System.IO
Imports System.Threading

Public Class Service1
    Public t As New Timers.Timer

    Sub mensaje(ByVal s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Dim currentTime As DateTime = DateTime.Now
            Dim intervalToElapse As Integer = 0

            Dim scheduleTime As DateTime = Convert.ToDateTime(ConfigurationManager.AppSettings("HoraInicio"))

            If (currentTime <= scheduleTime) Then
                intervalToElapse = Convert.ToInt16(scheduleTime.Subtract(currentTime).TotalSeconds)
            Else
                intervalToElapse = Convert.ToInt16(scheduleTime.AddDays(1).Subtract(currentTime).TotalSeconds)
            End If
            t = New System.Timers.Timer(intervalToElapse * 1000)
            t.AutoReset = True
            AddHandler t.Elapsed, AddressOf timer_Elapsed
            t.Start()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub timer_Elapsed(ByVal sender As Object, ByVal e As Timers.ElapsedEventArgs)
        Try
            'Dim sw As New IO.StreamWriter("D:\\ServiceLog.txt", True)
            'sw.WriteLine("Hola el xml se envio a las " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))
            'sw.Close()
            Dim proxy As New CServicioNotificacionSMTP
            proxy.EnvioXMLForRipleyTxt()
            'Dim mensaje = proxy.EnvioXML()

            'Dim sw As New IO.StreamWriter("D:\\ServiceLog.txt", True)
            'sw.WriteLine(mensaje & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))
            'sw.Close()

            t.Interval = Convert.ToDouble(ConfigurationManager.AppSettings("Intervalo"))
        Catch ex As Exception

        End Try
    End Sub
    'Protected Overrides Sub OnStart(ByVal args() As String)
    '    Me.ScheduleService()
    'End Sub

    'Protected Overrides Sub OnStop()
    '    Me.Schedular.Dispose()
    'End Sub

    'Private Schedular As Timer

    'Public Sub ScheduleService()
    '    Try
    '        Schedular = New Timer(New TimerCallback(AddressOf SchedularCallback))

    '        Dim scheduledTime As DateTime = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings("HoraDeEnvio"))
    '        If DateTime.Now > scheduledTime Then
    '            scheduledTime = scheduledTime.AddDays(1)
    '        End If

    '        Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
    '        Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)
    '        Schedular.Change(dueTime, Timeout.Infinite)
    '    Catch ex As Exception
    '        EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
    '        CServicioNotificacionSMTP.Write(ex.ToString)

    '        Using serviceController As New System.ServiceProcess.ServiceController("Servicio WCF - Pago Efectivo Servicio Conciliacion Ripley")
    '            serviceController.[Stop]()
    '        End Using

    '    End Try
    'End Sub

    'Private Sub SchedularCallback(e As Object)
    '    Using proxy As New CServicioNotificacionSMTP
    '        proxy.EnvioXMLForRipleyTxt()
    '    End Using
    '    Me.ScheduleService()
    'End Sub

    'Private Sub WriteToFile(text As String)
    '    Dim path As String = "D:\ServiceLog.txt"
    '    Using writer As New StreamWriter(path, True)
    '        writer.WriteLine(String.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")))
    '        writer.Close()
    '    End Using
    'End Sub
End Class