Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Public Class RemoteServices
    Inherits _3Dev.FW.Web.RemoteServices

    Private _IServicioNotificacion As SPE.EmsambladoComun.IServicioNotificacion

    Public Property IServicioNotificacion() As SPE.EmsambladoComun.IServicioNotificacion
        Get
            Return _IServicioNotificacion
        End Get
        Set(ByVal value As SPE.EmsambladoComun.IServicioNotificacion)
            _IServicioNotificacion = value
        End Set
    End Property


    Public Overrides Sub DoSetupNetworkEnviroment()
        MyBase.DoSetupNetworkEnviroment()

        Me._IServicioNotificacion = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IServicioNotificacion), Me.ServerUrl + "/" + NombreServiciosConocidos.IServicioNotificacion), SPE.EmsambladoComun.IServicioNotificacion)
    End Sub

End Class
