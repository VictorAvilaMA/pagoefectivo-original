Imports System.Timers
Imports System.IO
Imports System.Net
Imports System.Configuration
Imports SPE.Entidades


Public Class NotificaServRequest

    Private t As Timer = Nothing
    Private oCServicioNotificacion As CServicioNotificacion = Nothing
    'Public Shared Sub Main()

    'End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        t = New Timer(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("Intervalo")))
        AddHandler t.Elapsed, New System.Timers.ElapsedEventHandler(AddressOf Me.t_Elapsed)

    End Sub

    Function EstablishConnection() As Boolean
        'Return SPE.NotificacionServicio.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function

    Public Sub DoStartRemotingServices()
        Try
            'SPE.NotificacionServicio.RemoteServices.Instance = New SPE.NotificacionServicio.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try


        If Not EstablishConnection() Then
            Return
        End If

    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            DoStartRemotingServices()
            oCServicioNotificacion = New CServicioNotificacion()
            t.Start()

        Catch ex As Exception
            EventLog.WriteEntry("No se puedo iniciar el Servicio Verifica Expiracion OP - Pago Efectivo: " + ex.Message)
        End Try

    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        t.Stop()
    End Sub

    Protected Overrides Sub OnContinue()
        MyBase.OnContinue()
        t.Start()
    End Sub

    Protected Overrides Sub OnPause()
        MyBase.OnPause()
        t.Stop()
    End Sub

    Protected Overrides Sub OnShutdown()
        t.Stop()
    End Sub

    Protected Sub t_Elapsed(ByVal sender As System.Object, _
        ByVal e As System.Timers.ElapsedEventArgs)

        RealizaNotificacion()

    End Sub

    Public Sub RealizaNotificacion()
        oCServicioNotificacion.RealizaNotificacion()
    End Sub

    Protected Overrides Sub Finalize()
        'SPE.NotificacionServicio.RemoteServices.Instance = Nothing
        MyBase.Finalize()
        If (oCServicioNotificacion IsNot Nothing) Then
            oCServicioNotificacion.Dispose()
        End If

    End Sub

End Class
