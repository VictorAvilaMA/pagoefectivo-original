Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.EmsambladoComun
Imports System.Text

Public Class CServicioNotificacion
    Implements IDisposable

    '<upd Peru.Com FaseIII> Se modifico la logica de notificacion para la nueva version
    Public Function RealizarNotificacionesPendientes(ByVal oListaPendientes As List(Of BEServicioNotificacion)) As Integer
        'Write("RealizarNotificacionesPendientes")
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                Dim oListaRespuestaBien, oListaRespuestaError As New List(Of BEServicioNotificacion)
                If (oListaPendientes.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In oListaPendientes
                        Try
                            Dim procesodesc As String = String.Format("idOrdenPago={0}|idMovimiento={1}|UrlRespuesta={2}|", obeServicioNotif.IdOrdenPago, obeServicioNotif.IdMovimiento, obeServicioNotif.UrlRespuesta)
                            'Write("uno mas pendiente " & procesodesc)
                            Select Case obeServicioNotif.IdTipoServNotificacion
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlOk
                                    'Write("OK")
                                    procesodesc &= "Proceso=UrlOk"
                                    RealizarNotificacionQueryStrings(obeServicioNotif.UrlRespuesta, procesodesc)
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlError
                                    procesodesc &= "Proceso=UrlError"
                                    RealizarNotificacionQueryStrings(obeServicioNotif.UrlRespuesta, procesodesc)
                                    'Write("Error")
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacion
                                    procesodesc &= "Proceso=UrNotificacion"
                                    RealizarNotificacionPost(obeServicioNotif.UrlRespuesta, procesodesc)
                                    'Write("URLNotificacion")
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSaga
                                    procesodesc &= "Proceso=UrNotificacionSagaNoTomadoEnCuenta"
                            End Select
                            'MDS
                            'Filtro Mientras no sea de SAGA que notifique 
                            If SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacionSaga <> obeServicioNotif.IdTipoServNotificacion Then
                                obeServicioNotif.Observacion = "Notificación Satisfactoria"
                                obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Notificado
                                oListaRespuestaBien.Add(obeServicioNotif)
                            End If
                        Catch ex As Exception
                            obeServicioNotif.Observacion = String.Format("Error :{0}", ex.Message)
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                            oListaRespuestaError.Add(obeServicioNotif)
                        End Try

                    Next
                    oiServicioNotificacion.ActualizarEstadoNotificaciones(oListaRespuestaBien, oListaRespuestaError)
                End If
            End Using
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex.Message)
            Throw ex
        End Try
        Return 1
    End Function


    Dim cantidadMaxregistros As Integer = 20

    '<upd Peru.Com FaseIII> Se modifico la logica de notificacion para la nueva version
    '<upd Peru.Com FaseIII> Se optimizo para el envio de paketes fraccionados en bloques de 20
    Public Function RealizarReNotificacionesErroneas(ByVal oListaPendientes As List(Of BEServicioNotificacion)) As Integer
        'Write("RealizarReNotificacionesErroneas")
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oiServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())

                Dim oListaRespuestaBien, oListaRespuestaError As New List(Of BEServicioNotificacion)
                Dim cantidadregistros As Integer = 0

                If (oListaPendientes.Count > 0) Then
                    For Each obeServicioNotif As BEServicioNotificacion In oListaPendientes
                        cantidadregistros += 1

                        Try
                            Dim procesodesc As String = String.Format("idOrdenPago={0}|idMovimiento={1}|UrlRespuesta={2}|", obeServicioNotif.IdOrdenPago, obeServicioNotif.IdMovimiento, obeServicioNotif.UrlRespuesta)
                            'Write("uno mas error " & procesodesc)
                            Select Case obeServicioNotif.IdTipoServNotificacion
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlOk
                                    procesodesc &= "Proceso=UrlOk"
                                    RealizarNotificacionQueryStrings(obeServicioNotif.UrlRespuesta, procesodesc)
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlError
                                    procesodesc &= "Proceso=UrlError"
                                    RealizarNotificacionQueryStrings(obeServicioNotif.UrlRespuesta, procesodesc)
                                Case SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Tipo.UrlNotificacion
                                    procesodesc &= "Proceso=UrlNotificacion"
                                    RealizarNotificacionPost(obeServicioNotif.UrlRespuesta, procesodesc)
                            End Select

                            obeServicioNotif.Observacion = "Notificación Satisfactoria"
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.Notificado
                            oListaRespuestaBien.Add(obeServicioNotif)
                        Catch ex As Exception
                            obeServicioNotif.Observacion = String.Format("Error :{0}", ex.Message)
                            obeServicioNotif.IdEstado = SPE.EmsambladoComun.ParametrosSistema.ServicioNotificacion.Estado.ErrorNotificado
                            oListaRespuestaError.Add(obeServicioNotif)
                        End Try


                        If (cantidadMaxregistros = cantidadregistros) Then
                            'Write("Sending")
                            oiServicioNotificacion.ActualizarEstadoNotificaciones(oListaRespuestaBien, oListaRespuestaError)
                            oListaRespuestaBien.Clear()
                            oListaRespuestaError.Clear()
                            cantidadregistros = 0
                        End If
                    Next
                End If
            End Using
        Catch ex As Exception
            Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex.Message)
            Throw ex
        End Try
        Return 1
    End Function

    Public Shared Sub Write(m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToLongTimeString() & "   >>>> " & m & Environment.NewLine)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub


    Public Function RealizaNotificacion() As Integer
        Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
            Dim obeConsultServNotifResponse As BEConsultServNotifResponse = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarNotificacionesListasANotificar()
            RealizarNotificacionesPendientes(obeConsultServNotifResponse.ServNotificacionPendiente)
            RealizarReNotificacionesErroneas(obeConsultServNotifResponse.ServNotificacionErroneas)
        End Using
    End Function


#Region "Logica Request Generica "
    Public Function RealizarNotificacionQueryStrings(ByVal url As String, ByVal strProceso As String) As Integer
        Try
            Dim g As HttpWebRequest = CType(WebRequest.Create(url),  _
              HttpWebRequest)
            Using r As HttpWebResponse = CType(g.GetResponse, HttpWebResponse)
                g.Timeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("NotificacionRequestTimeout"))
                Dim path As String = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings("logNotificacionRuta")
                If Not System.IO.File.Exists(path) Then
                    Using file As FileStream = System.IO.File.Create(path)
                    End Using
                End If
                Using tw As TextWriter = New StreamWriter(path, True)
                    tw.WriteLine(DateTime.Now.ToString + " for " + url + ": " + r.StatusCode.ToString + " - " + strProceso)
                End Using
            End Using
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
        '
        Return 1
        '
    End Function
#End Region
#Region "Logica Request-Post Generica "
    Public Function RealizarNotificacionPost(ByVal url As String, ByVal strProceso As String) As Integer

        Try

            Dim uri As New Uri(url)
            Dim pathURL As String = uri.GetLeftPart(UriPartial.Path)
            Dim qryURL As String = uri.GetLeftPart(UriPartial.Query)
            pathURL = uri.GetLeftPart(UriPartial.Path)
            Dim request As HttpWebRequest = CType(WebRequest.Create(pathURL), HttpWebRequest)
            Dim data As Byte() = Encoding.ASCII.GetBytes(qryURL)
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = data.Length
            Dim newStream As Stream = request.GetRequestStream()
            newStream.Write(data, 0, data.Length)
            newStream.Close()
            Using r As HttpWebResponse = CType(request.GetResponse, HttpWebResponse)
                request.Timeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("NotificacionRequestTimeout"))
                Dim path As String = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings("logNotificacionRuta")
                If Not System.IO.File.Exists(path) Then
                    Using file As FileStream = System.IO.File.Create(path)
                    End Using
                End If
                Using tw As TextWriter = New StreamWriter(path, True)
                    tw.WriteLine(DateTime.Now.ToString + " for " + pathURL + ": " + r.StatusCode.ToString + " - " + strProceso)
                End Using
            End Using
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
        Return 1
    End Function
#End Region




    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Sub New()

    End Sub
End Class
