﻿<System.ComponentModel.RunInstaller(True)> Partial Class Installer1
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.serviceInstaller1 = New System.ServiceProcess.ServiceInstaller()
        Me.serviceProcessInstaller1 = New System.ServiceProcess.ServiceProcessInstaller()
        '
        'serviceInstaller1
        '
        Me.serviceInstaller1.Description = "Realiza los procesos de nofiticacion de Pago Efectivo"
        Me.serviceInstaller1.DisplayName = "Pago Efectivo - Servicio de Notificacion"
        Me.serviceInstaller1.ServiceName = "SPE2.ServicioNotificacion"
        '
        'serviceProcessInstaller1
        '
        Me.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.serviceProcessInstaller1.Password = Nothing
        Me.serviceProcessInstaller1.Username = Nothing
        '
        'Installer1
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.serviceInstaller1, Me.serviceProcessInstaller1})

    End Sub
    Private WithEvents serviceProcessInstaller1 As System.ServiceProcess.ServiceProcessInstaller
    Private WithEvents serviceInstaller1 As System.ServiceProcess.ServiceInstaller

End Class
