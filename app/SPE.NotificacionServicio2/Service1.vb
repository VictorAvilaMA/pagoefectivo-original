﻿Imports System.ComponentModel
Imports SPE.NotificacionConsola

Public Class Service1
    Dim bw As New BackgroundWorker
    Sub mensaje(s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub
    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            bw.RunWorkerAsync()
        Catch ex As Exception
            CServicioNotificacion.Write(ex.ToString)
            EventLog.WriteEntry("No se puedo iniciar el Servicio de Noficiacion - Pago Efectivo: " + ex.Message)
        End Try
    End Sub
    Private Sub bw_DoWork(sender As Object, e As DoWorkEventArgs)
        While True
            Try
                Dim proxy As New CServicioNotificacion
                proxy.RealizaNotificacion()
                Dim tiempomiliseg As Long = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("Intervalo"))
                Threading.Thread.Sleep(tiempomiliseg)
            Catch ex As Exception
                EventLog.WriteEntry("El servicio que estaba operativo se cayo: " + ex.Message)
                CServicioNotificacion.Write(ex.ToString)
            End Try
        End While
    End Sub
    Private Sub bw_ProgressChanged(sender As Object, e As ProgressChangedEventArgs)
        'Throw New NotImplementedException
    End Sub
    Private Sub bw_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        'Throw New NotImplementedException
    End Sub
    Protected Overrides Sub OnStart(ByVal args() As String)
    End Sub
    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub
    Protected Overrides Sub OnContinue()
        MyBase.OnContinue()
    End Sub
    Protected Overrides Sub OnPause()
        MyBase.OnPause()
    End Sub
    Protected Overrides Sub OnShutdown()
        bw.Dispose()
    End Sub
    Protected Overrides Sub Finalize()
        'SPE.NotificacionServicio.RemoteServices.Instance = Nothing
        bw.Dispose()
    End Sub
End Class
