﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades

Imports System.Text
Imports System.Xml
'Imports SPE.EmsambladoComun
Imports tss = Tamir.SharpSsh
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Public Class CServicioNotificacionSFTP
    Implements IDisposable
    Public Function EnvioXML()
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oIServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                Dim hostServer As [String] = ConfigurationManager.AppSettings("hostSftp") '"192.168.65.145"
                'ConfigurationManager.AppSettings["host"];
                Dim userName As [String] = ConfigurationManager.AppSettings("userNameSFTP") '"pagoefectivo"
                'ConfigurationManager.AppSettings["userName"];
                Dim password As [String] = ConfigurationManager.AppSettings("passwordSFTP") '"too0wo9UpoahoL"
                'ConfigurationManager.AppSettings["password"];
                Dim port As Int32 = Int32.Parse(ConfigurationManager.AppSettings("portSFTP")) '22
                'Int32.Parse(ConfigurationManager.AppSettings["port"]);

                Dim oListaBEServicioNotificacion As New List(Of BEServicioNotificacion)()
                oListaBEServicioNotificacion = oIServicioNotificacion.ConsultarOrdenesPagoSaga()
                'crear el xml

                If oListaBEServicioNotificacion.Count > 0 Then
                    Dim FechaFSaga As Date = Date.Now
                    Dim NombreArchivoXML As String = FechaFSaga.ToString("ddMMyyyy") + "-" + FechaFSaga.ToString("HHmm") + ".xml"

                    Dim utf8WithoutBom As New System.Text.UTF8Encoding(False)
                    'Using sink As New StreamWriter(NombreArchivoXML, False, utf8WithoutBom)



                    Dim writer As New XmlTextWriter(NombreArchivoXML, utf8WithoutBom)
                    writer.WriteStartDocument(True)
                    writer.Formatting = Formatting.Indented
                    writer.Indentation = 2
                    writer.WriteStartElement("Envelope")
                    writer.WriteStartElement("Header")
                    writer.WriteStartElement("ClientService")

                    writer.WriteStartElement("country")
                    writer.WriteString("PE")
                    writer.WriteEndElement()
                    writer.WriteStartElement("commerce")
                    writer.WriteString("Falabella")
                    writer.WriteEndElement()
                    writer.WriteStartElement("channel")
                    writer.WriteString("B2C")
                    writer.WriteEndElement()
                    writer.WriteStartElement("storeId")
                    writer.WriteString("1")
                    writer.WriteEndElement()

                    writer.WriteEndElement()
                    writer.WriteEndElement()
                    writer.WriteStartElement("Body")
                    For Each RegistroOP As BEServicioNotificacion In oListaBEServicioNotificacion
                        createNode(RegistroOP.OrderIdSaga, RegistroOP.FechaPagoSaga, RegistroOP.FechaContableSaga, RegistroOP.MontoCanceladoSaga, RegistroOP.Canal, RegistroOP.CajaSaga, RegistroOP.CodigoAutorizacionServipagSaga, writer)
                    Next RegistroOP
                    writer.WriteEndElement()
                    writer.WriteEndElement()
                    writer.WriteEndDocument()
                    'writer.Flush()
                    writer.Close()

                    'Dim result As String
                    'Dim utf8noBOM As Encoding = New UTF8Encoding(False)
                    'Dim settings As New XmlWriterSettings()
                    'settings.Indent = True
                    'settings.Encoding = utf8noBOM
                    'Using output As New MemoryStream()
                    '    Using writer As XmlWriter = XmlWriter.Create(output, settings)
                    '        writer.WriteStartDocument()
                    '        writer.WriteStartElement("Colors")
                    '        writer.WriteElementString("Color", "RED")
                    '        writer.WriteEndDocument()
                    '    End Using
                    '    result = Encoding.[Default].GetString(output.ToArray())
                    'End Using
                    'Console.WriteLine(result)
                    '^^^^^'


                    ' fin crear el xml

                    'String fromFile = @"E:\ConfirmarPagoOCService.txt";
                    Dim fromFile As [String] = NombreArchivoXML
                    Dim toFile As [String] = ConfigurationManager.AppSettings("RutaCarpetaSftp") ' "/sftp/Saga/"

                    'Create object
                    Dim sftpClient As tss.SshTransferProtocolBase
                    sftpClient = New tss.Sftp(hostServer, userName)
                    sftpClient.Password = password

                    'connect to server
                    'Comente esto pq Brady me llega al mero nabo pelado - Juan Pedro Barriga 962900685
                    'sftpClient.AddIdentityFile(ConfigurationManager.AppSettings("RutaPrivateKey"), ConfigurationManager.AppSettings("PassPhrase")) '"sagaexport", "ssaga") 
                    sftpClient.Connect(port)
                    'sftpClient.Connect();

                    'Console.WriteLine("conectado al servidor")
                    'Console.ReadLine()

                    'subir archivo
                    sftpClient.Put(fromFile, toFile)

                    'Console.WriteLine("archivo publicado")
                    'Console.ReadLine()

                    'close connection
                    sftpClient.Close()
                    'End Using
                End If
            End Using
        Catch ex As Exception
            Throw ex
            Logger.Write(ex.Message)
        End Try

    End Function

    Private Sub createNode(pOrderId As String, pFechaPago As DateTime, pFechaContable As DateTime, pMontoCancelado As Decimal, pCanal As String, pCaja As String, pcodigoAutorizacionServipag As String, writer As XmlTextWriter)
        writer.WriteStartElement("confirmarPagoOCRequest")
        writer.WriteStartElement("orderId")
        writer.WriteString(pOrderId)
        writer.WriteEndElement()
        writer.WriteStartElement("fechaPago")
        writer.WriteString(pFechaPago.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.0Z'"))
        writer.WriteEndElement()
        writer.WriteStartElement("fechaContable")
        writer.WriteString(pFechaContable.ToString("yyyy'-'MM'-'dd"))
        writer.WriteEndElement()
        writer.WriteStartElement("montoCancelado")
        writer.WriteString(pMontoCancelado.ToString())
        writer.WriteEndElement()
        writer.WriteStartElement("canal")
        writer.WriteString(pCanal)
        writer.WriteEndElement()
        writer.WriteStartElement("caja")
        writer.WriteString(pCaja)
        writer.WriteEndElement()
        writer.WriteStartElement("codigoAutorizacion")
        writer.WriteString(pcodigoAutorizacionServipag)
        writer.WriteEndElement()
        writer.WriteEndElement()
    End Sub


    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Shared Sub Write(m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToLongTimeString() & "   >>>>" & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub
End Class
