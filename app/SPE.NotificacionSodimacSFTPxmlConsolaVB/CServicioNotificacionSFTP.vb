﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Imports SPE.Entidades

Imports System.Text
Imports System.Xml
'Imports SPE.EmsambladoComun
Imports tss = Tamir.SharpSsh
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Public Class CServicioNotificacionSFTP
    Implements IDisposable
    Public Function EnvioXML()
        Try
            Using Conexionsss As New ProxyBase(Of IServicioNotificacion)
                Dim oIServicioNotificacion As IServicioNotificacion = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                Dim hostServer As [String] = ConfigurationManager.AppSettings("hostSftp") '"192.168.65.145"
                Dim userName As [String] = ConfigurationManager.AppSettings("userNameSFTP") '"pagoefectivo"
                Dim password As [String] = ConfigurationManager.AppSettings("passwordSFTP") '"too0wo9UpoahoL"
                Dim port As Int32 = Int32.Parse(ConfigurationManager.AppSettings("portSFTP")) '22

                Dim oListaBEServicioNotificacion As New List(Of BEServicioNotificacion)()
                oListaBEServicioNotificacion = oIServicioNotificacion.ConsultarOrdenesPagoSodimac()

                If oListaBEServicioNotificacion.Count > 0 Then
                    Dim FechaFSodimac As Date = Date.Now
                    Dim NombreArchivoXML As String = ConfigurationManager.AppSettings("NombreXMLSodimac") +
                        "_" + FechaFSodimac.ToString("ddMMyyyy") +
                        "-" + FechaFSodimac.ToString("HHmm") +
                        ".xml"

                    Dim utf8WithoutBom As New System.Text.UTF8Encoding(False)

                    Dim writer As New XmlTextWriter(NombreArchivoXML, utf8WithoutBom)
                    writer.WriteStartDocument(True)
                    writer.Formatting = Formatting.Indented
                    writer.Indentation = 2
                    writer.WriteStartElement("Envelope")
                    writer.WriteStartElement("Header")
                    writer.WriteStartElement("ClientService")

                    writer.WriteStartElement("country")
                    writer.WriteString("PE")
                    writer.WriteEndElement()
                    writer.WriteStartElement("commerce")
                    writer.WriteString("Sodimac")
                    writer.WriteEndElement()
                    writer.WriteStartElement("channel")
                    writer.WriteString("B2C")
                    writer.WriteEndElement()
                    writer.WriteStartElement("storeId")
                    writer.WriteString("1")
                    writer.WriteEndElement()

                    writer.WriteEndElement()
                    writer.WriteEndElement()
                    writer.WriteStartElement("Body")
                    For Each RegistroOP As BEServicioNotificacion In oListaBEServicioNotificacion
                        createNode(RegistroOP.OrderIdSodimac, RegistroOP.FechaPagoSodimac, RegistroOP.FechaContableSodimac, RegistroOP.MontoCanceladoSodimac, RegistroOP.Canal, RegistroOP.CajaSodimac, RegistroOP.CodigoAutorizacionServipagSodimac, writer)
                    Next RegistroOP
                    writer.WriteEndElement()
                    writer.WriteEndElement()
                    writer.WriteEndDocument()

                    writer.Close()

                    Dim fromFile As [String] = NombreArchivoXML
                    Dim toFile As [String] = ConfigurationManager.AppSettings("RutaCarpetaSftp") ' "/sftp/Saga/"
                    Dim sftpClient As tss.SshTransferProtocolBase
                    sftpClient = New tss.Sftp(hostServer, userName)
                    sftpClient.Password = password
                    sftpClient.Connect(port)
                    sftpClient.Put(fromFile, toFile)
                    sftpClient.Close()

                    'Return "El XML " & NombreArchivoXML & " se envio a las "
                End If
            End Using
        Catch ex As Exception
            'Return ex.Message.ToString()
            Throw ex
            Logger.Write(ex.Message)
        End Try
    End Function

    Private Sub createNode(pOrderId As String, pFechaPago As DateTime, pFechaContable As DateTime, pMontoCancelado As Decimal, pCanal As String, pCaja As String, pcodigoAutorizacionServipag As String, writer As XmlTextWriter)
        writer.WriteStartElement("confirmarPagoOCRequest")
        writer.WriteStartElement("orderId")
        writer.WriteString(pOrderId)
        writer.WriteEndElement()
        writer.WriteStartElement("fechaPago")
        writer.WriteString(pFechaPago.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.0Z'"))
        writer.WriteEndElement()
        writer.WriteStartElement("fechaContable")
        writer.WriteString(pFechaContable.ToString("yyyy'-'MM'-'dd"))
        writer.WriteEndElement()
        writer.WriteStartElement("montoCancelado")
        writer.WriteString(pMontoCancelado.ToString())
        writer.WriteEndElement()
        writer.WriteStartElement("canal")
        writer.WriteString(pCanal)
        writer.WriteEndElement()
        writer.WriteStartElement("caja")
        writer.WriteString(pCaja)
        writer.WriteEndElement()
        writer.WriteStartElement("codigoAutorizacion")
        writer.WriteString(pcodigoAutorizacionServipag)
        writer.WriteEndElement()
        writer.WriteEndElement()
    End Sub


    Private disposedValue As Boolean = False        ' To detect redundant calls
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    Public Shared Sub Write(m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(m & "   >>>>" & Date.Now.ToLongTimeString())
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub
End Class
