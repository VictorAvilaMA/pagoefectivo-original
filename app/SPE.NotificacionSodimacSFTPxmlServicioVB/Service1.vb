﻿Imports SPE.NotificacionSodimacSFTPxmlConsolaVB
Imports System.Configuration
Imports System.ComponentModel
Imports System.Globalization

Public Class Service1

    Public t As New Timers.Timer

    Sub mensaje(ByVal s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            Dim currentTime As DateTime = DateTime.Now
            Dim intervalToElapse As Integer = 0

            Dim scheduleTime As DateTime = Convert.ToDateTime(ConfigurationManager.AppSettings("HoraInicio"))

            If (currentTime <= scheduleTime) Then
                intervalToElapse = Convert.ToInt16(scheduleTime.Subtract(currentTime).TotalSeconds)
            Else
                intervalToElapse = Convert.ToInt16(scheduleTime.AddDays(1).Subtract(currentTime).TotalSeconds)
            End If
            t = New System.Timers.Timer(intervalToElapse * 1000)
            t.AutoReset = True
            AddHandler t.Elapsed, AddressOf timer_Elapsed
            t.Start()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub timer_Elapsed(ByVal sender As Object, ByVal e As Timers.ElapsedEventArgs)
        Try
            'Dim sw As New IO.StreamWriter("D:\\ServiceLog.txt", True)
            'sw.WriteLine("Hola el xml se envio a las " & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))
            'sw.Close()
            Dim proxy As New CServicioNotificacionSFTP
            proxy.EnvioXML()
            'Dim mensaje = proxy.EnvioXML()

            'Dim sw As New IO.StreamWriter("D:\\ServiceLog.txt", True)
            'sw.WriteLine(mensaje & Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))
            'sw.Close()

            t.Interval = Convert.ToDouble(ConfigurationManager.AppSettings("Intervalo"))
        Catch ex As Exception
            
        End Try
    End Sub

End Class
