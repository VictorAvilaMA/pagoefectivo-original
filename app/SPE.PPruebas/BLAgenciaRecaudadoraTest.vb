﻿'The following code was generated by Microsoft Visual Studio 2005.
'The test owner should check each test for validity.
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System
Imports System.Text
Imports System.Collections.Generic
Imports SPE.Negocio
Imports SPE.Entidades






'''<summary>
'''This is a test class for SPE.Negocio.BLAgenciaRecaudadora and is intended
'''to contain all SPE.Negocio.BLAgenciaRecaudadora Unit Tests
'''</summary>
<TestClass()> _
Public Class BLAgenciaRecaudadoraTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property
#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for RegistrarAgenciaRecaudadora(ByVal SPE.Entidades.BEAgenciaRecaudadora)
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarAgenciaRecaudadoraTest()
        Dim target As BLAgenciaRecaudadora = New BLAgenciaRecaudadora

        Dim obeAgenciaRecaudadora As BEAgenciaRecaudadora = Nothing 'TODO: Initialize to an appropriate value

        Dim expected As Integer
        Dim actual As Integer

        obeAgenciaRecaudadora = New BEAgenciaRecaudadora()
        obeAgenciaRecaudadora.IdAgenciaRecaudadora = 1
        obeAgenciaRecaudadora.IdTipoAgenciaRecaudadora = 61
        obeAgenciaRecaudadora.NombreComercial = "Agencia test"
        obeAgenciaRecaudadora.Contacto = "contacto test"
        obeAgenciaRecaudadora.Telefono = "123456465"
        obeAgenciaRecaudadora.Fax = "12132313"
        obeAgenciaRecaudadora.Direccion = "direccion test"
        obeAgenciaRecaudadora.Email = "email test"
        obeAgenciaRecaudadora.IdEstado = 41
        obeAgenciaRecaudadora.IdCiudad = 1
        obeAgenciaRecaudadora.IdUsuarioCreacion = 1
        obeAgenciaRecaudadora.IdUsuarioActualizacion = 1
        expected = -1
        actual = target.RegistrarAgenciaRecaudadora(obeAgenciaRecaudadora)
        Assert.AreEqual(expected, actual, "SPE.Negocio.BLAgenciaRecaudadora.RegistrarAgenciaRecaudadora did not return the e" & _
                "xpected value.")

    End Sub


    ''''<summary>
    ''''A test for RegistrarAgente(ByVal SPE.Entidades.BEAgente)
    ''''</summary>
    '<TestMethod()> _
    'Public Sub RegistrarAgenteTest()
    '    Dim target As BLAgenciaRecaudadora = New BLAgenciaRecaudadora

    '    Dim obeAgente As BEAgente = Nothing 'TODO: Initialize to an appropriate value

    '    Dim expected As Integer
    '    Dim actual As Integer

    '    actual = target.RegistrarAgente(obeAgente)



    '    Assert.AreEqual(expected, actual, "SPE.Negocio.BLAgenciaRecaudadora.RegistrarAgente did not return the expected valu" & _
    '            "e.")
    '    Assert.Inconclusive("Verify the correctness of this test method.")
    'End Sub
End Class
