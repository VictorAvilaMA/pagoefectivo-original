Public Class CIP

    Public Sub New()
        _listDetalle = New List(Of CIPDetalle)()
    End Sub




    Private _codigoMoneda As String
    Public Property CodigoMoneda() As String
        Get
            Return _codigoMoneda
        End Get
        Set(ByVal value As String)
            _codigoMoneda = value
        End Set
    End Property

    Private _monto As String
    Public ReadOnly Property Monto() As String
        Get
            ReCalcularMontoTotal()
            Return _monto
        End Get
    End Property

    Private _merchantID As String
    Public Property MerchantID() As String
        Get
            Return _merchantID
        End Get
        Set(ByVal value As String)
            _merchantID = value
        End Set
    End Property

    Private _orderIdComercio As String
    Public Property OrderIdComercio() As String
        Get
            Return _orderIdComercio
        End Get
        Set(ByVal value As String)
            _orderIdComercio = value
        End Set
    End Property

    Private _urlOK As String
    Public Property UrlOK() As String
        Get
            Return _urlOK
        End Get
        Set(ByVal value As String)
            _urlOK = value
        End Set
    End Property

    Private _urlError As String
    Public Property UrlError() As String
        Get
            Return _urlError
        End Get
        Set(ByVal value As String)
            _urlError = value
        End Set
    End Property

    Private _mailComercio As String
    Public Property MailComercio() As String
        Get
            Return _mailComercio
        End Get
        Set(ByVal value As String)
            _mailComercio = value
        End Set
    End Property


    Private _usuarioID As String
    Public Property UsuarioID() As String
        Get
            Return _usuarioID
        End Get
        Set(ByVal value As String)
            _usuarioID = value
        End Set
    End Property

    Private _dataAdicional As String
    Public Property DataAdicional() As String
        Get
            Return _dataAdicional
        End Get
        Set(ByVal value As String)
            _dataAdicional = value
        End Set
    End Property

    Private _usuarioNombre As String
    Public Property UsuarioNombre() As String
        Get
            Return _usuarioNombre
        End Get
        Set(ByVal value As String)
            _usuarioNombre = value
        End Set
    End Property

    Private _usuarioApellidos As String
    Public Property UsuarioApellidos() As String
        Get
            Return _usuarioApellidos
        End Get
        Set(ByVal value As String)
            _usuarioApellidos = value
        End Set
    End Property


    Private _usuarioDomicilio As String
    Public Property UsuarioDomicilio() As String
        Get
            Return _usuarioDomicilio
        End Get
        Set(ByVal value As String)
            _usuarioDomicilio = value
        End Set
    End Property
    Private _usuarioLocalidad As String
    Public Property UsuarioLocalidad() As String
        Get
            Return _usuarioLocalidad
        End Get
        Set(ByVal value As String)
            _usuarioLocalidad = value
        End Set
    End Property

    Private _usuarioProvincia As String
    Public Property UsuarioProvincia() As String
        Get
            Return _usuarioProvincia
        End Get
        Set(ByVal value As String)
            _usuarioProvincia = value
        End Set
    End Property


    Private _usuarioPais As String
    Public Property UsuarioPais() As String
        Get
            Return _usuarioPais
        End Get
        Set(ByVal value As String)
            _usuarioPais = value
        End Set
    End Property


    Private _usuarioAlias As String
    Public Property UsuarioAlias() As String
        Get
            Return _usuarioAlias
        End Get
        Set(ByVal value As String)
            _usuarioAlias = value
        End Set
    End Property

    Private _listDetalle As List(Of CIPDetalle)
    Public ReadOnly Property Detalle() As List(Of CIPDetalle)
        Get
            Return _listDetalle
        End Get
    End Property

    Private Sub DoCalcularMontoTotal(ByVal detalle As CIPDetalle)
        _monto = _monto + detalle.Importe
    End Sub
    Private Sub ReCalcularMontoTotal()
        Detalle.ForEach(AddressOf DoCalcularMontoTotal)
    End Sub


End Class
