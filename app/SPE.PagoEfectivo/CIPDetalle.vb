Public Class CIPDetalle

    Public Sub New()

    End Sub

    Public Sub New(ByVal conceptoPago As String, ByVal importe As Decimal)
        _conceptoPago = conceptoPago
        _importe = importe
    End Sub

    Public Sub New(ByVal conceptoPago As String, ByVal importe As Decimal, ByVal tipoOrigen As String, ByVal codigoOrigen As String, ByVal campo1 As String, ByVal campo2 As String, ByVal campo3 As String)
        Me.New(conceptoPago, importe)
        _tipoOrigen = tipoOrigen
        _codigoOrigen = codigoOrigen
        _campo1 = campo1
        _campo2 = campo2
        _campo3 = campo3
    End Sub

    Private _conceptoPago As String
    Public Property ConceptoPago() As String
        Get
            Return _conceptoPago
        End Get
        Set(ByVal value As String)
            _conceptoPago = value
        End Set
    End Property

    Private _importe As Decimal
    Public Property Importe() As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal value As Decimal)
            _importe = value
        End Set
    End Property

    Private _tipoOrigen As String
    Public Property TipoOrigen() As String
        Get
            Return _tipoOrigen
        End Get
        Set(ByVal value As String)
            _tipoOrigen = value
        End Set
    End Property

    Private _codigoOrigen As String
    Public Property CodigoOrigen() As String
        Get
            Return _codigoOrigen
        End Get
        Set(ByVal value As String)
            _codigoOrigen = value
        End Set
    End Property

    Private _campo1 As String
    Public Property Campo1() As String
        Get
            Return _campo1
        End Get
        Set(ByVal value As String)
            _campo1 = value
        End Set
    End Property

    Private _campo2 As String
    Public Property Campo2() As String
        Get
            Return _campo2
        End Get
        Set(ByVal value As String)
            _campo2 = value
        End Set
    End Property

    Private _campo3 As String
    Public Property Campo3() As String
        Get
            Return _campo3
        End Get
        Set(ByVal value As String)
            _campo3 = value
        End Set
    End Property

End Class

