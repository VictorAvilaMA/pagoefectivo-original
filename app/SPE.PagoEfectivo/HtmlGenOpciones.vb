Public Class HtmlGenOpciones
    Public Sub New()
        _buttonCss = ""
        _buttonStyle = ""
        _buttonText = "Generar CIP"
        _buttonWidth = 0
        _buttonHeight = 0
    End Sub
    Private _buttonText As String
    Public Property ButtonText() As String
        Get
            Return _buttonText
        End Get
        Set(ByVal value As String)
            _buttonText = value
        End Set
    End Property

    Private _buttonCss As String
    Public Property ButtonCss() As String
        Get
            Return _buttonCss
        End Get
        Set(ByVal value As String)
            _buttonCss = value
        End Set
    End Property

    Private _buttonStyle As String
    Public Property ButtonStyle() As String
        Get
            Return _buttonStyle
        End Get
        Set(ByVal value As String)
            _buttonStyle = value
        End Set
    End Property

    Private _buttonWidth As Integer
    Public Property ButtonWidth() As Integer
        Get
            Return _buttonWidth
        End Get
        Set(ByVal value As Integer)
            _buttonWidth = value
        End Set
    End Property

    Private _buttonHeight As Integer
    Public Property ButtonHeight() As Integer
        Get
            Return _buttonheight
        End Get
        Set(ByVal value As Integer)
            _buttonHeight = value
        End Set
    End Property


End Class
