Imports System.Text

Public Class PagoEfectivo


    Public Function ValidarConexion(ByVal ClaveApi As String, ByVal ClaveSecreta As String) As Trama
        Dim proxy As New WSPESeguridadProxy.WSPEConexion()
        If (proxy.ValidarAPIConexion(ClaveApi, ClaveSecreta)) Then
            Dim trama As New Trama()
            trama.ClaveAPI = ClaveApi
            trama.ClaveSecreta = ClaveSecreta
            Return trama
        Else
            Return Nothing
        End If
    End Function

    Private Function GenerarTramaEncriptada(ByVal trama As Trama) As String
        Dim strbuilder As New StringBuilder()
        Dim tramaXml As String = ""
        Dim util As New UtilOrdenPagoXML()
        util.AgregarCabecera("", trama.ClaveAPI, trama.ClaveSecreta, "", "", trama.CIP.CodigoMoneda, "", trama.CIP.Monto, trama.CIP.MerchantID, trama.CIP.OrderIdComercio, trama.CIP.UrlOK, trama.CIP.UrlError, trama.CIP.MailComercio, trama.CIP.UsuarioID, trama.CIP.DataAdicional, trama.CIP.UsuarioNombre, trama.CIP.UsuarioApellidos, trama.CIP.UsuarioLocalidad, trama.CIP.UsuarioProvincia, trama.CIP.UsuarioPais, trama.CIP.UsuarioAlias)

        For Each item As CIPDetalle In trama.CIP.Detalle
            util.AgregarDetalle(item.CodigoOrigen, item.TipoOrigen, item.ConceptoPago, item.Importe, item.Campo1, item.Campo2, item.Campo3)
        Next
        Dim libx As New clsLibreriax()
        tramaXml = libx.Encrypt(util.ObtenerXMLString(), libx.fEncriptaKey)
        strbuilder.AppendLine(" <input name=""_SPEParams"" type=""hidden"" id=""_PEParams"" value=" + tramaXml + " />")
        Return strbuilder.ToString()
    End Function

    Public Function GenerarHtmlPagoEfectivo(ByVal tipoHtml As TipoHtmlGenerado, ByVal trama As Trama) As String
        Return GenerarHtmlPagoEfectivo(tipoHtml, trama, Nothing)
    End Function


    Public Function GenerarHtmlPagoEfectivo(ByVal tipoHtml As TipoHtmlGenerado, ByVal trama As Trama, ByVal opciones As HtmlGenOpciones) As String
        Dim strbuilder As New StringBuilder()
        Dim strInputParameter As String = GenerarTramaEncriptada(trama)
        Dim strUrl As String = System.Configuration.ConfigurationManager.AppSettings("SPEConexionUrl")

        Dim buttonCss As String = ""
        Dim buttonStyle As String = ""
        Dim buttonText As String = "Generar CIP"
        If Not opciones Is Nothing Then
            buttonCss = opciones.ButtonCss
            buttonStyle = opciones.ButtonStyle
            buttonText = opciones.ButtonText
        End If
        strbuilder.AppendLine(strInputParameter)
        Select Case tipoHtml
            Case TipoHtmlGenerado.Boton
                strbuilder.AppendLine("  ")
                strbuilder.AppendLine(" <input type=""submit""  class=" + buttonCss + " style=" + buttonStyle + " value=" + buttonText + " onclick=""__IrAPagoEfectivo('" + strUrl + "','100px','520px');"" />")
                strbuilder.AppendLine("  ")

        End Select

        Return strbuilder.ToString()
    End Function


End Class
