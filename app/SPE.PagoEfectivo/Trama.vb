Public Class Trama

    Public _cip As CIP

    Public Sub New()
        _cip = New CIP()

    End Sub

    Private _claveAPI As String
    Public Property ClaveAPI() As String
        Get
            Return _claveAPI
        End Get
        Set(ByVal value As String)
            _claveAPI = value
        End Set
    End Property

    Private _claveSecreta As String
    Public Property ClaveSecreta() As String
        Get
            Return _claveSecreta
        End Get
        Set(ByVal value As String)
            _claveSecreta = value
        End Set
    End Property

    Public ReadOnly Property CIP() As CIP
        Get
            Return _cip
        End Get
    End Property

    Public Sub AgregarUnicaCIP(ByVal conceptoPago As String, ByVal codigoMoneda As String, ByVal importe As Decimal)
        _cip.CodigoMoneda = codigoMoneda
        _cip.Detalle.Add(New CIPDetalle(conceptoPago, importe))
    End Sub
    'merchangeid,moneda,conceptopago,urlok,urlerror,nroorden,importe,orderidcomercio,idcliente)
    Public Sub AgregarUnicaCIP(ByVal conceptoPago As String, ByVal codigoMoneda As String, ByVal importe As Decimal, ByVal merchantID As String, ByVal ordenId As String, ByVal urlOk As String, ByVal urlError As String)
        _cip.MerchantID = merchantID
        _cip.OrderIdComercio = ordenId
        _cip.UrlError = urlError
        _cip.UrlOK = urlOk
        AgregarUnicaCIP(conceptoPago, codigoMoneda, importe)
    End Sub


    Public Sub AgregarCIP(ByVal cIP As CIP)
        _cip = cIP
    End Sub

    'private sub 
    'Private Sub CalcularMontoTotal()
    '    _cip.Detalle.ForEach(addressof 
    'End Sub






End Class
