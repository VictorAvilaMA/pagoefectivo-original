
Imports System.Web
Imports _3Dev.FW.Util
Imports SPE.Entidades




Module AperturaCierreProceso


    Sub Main()
        DoStartRemotingServices()
        Console.WriteLine(CerrarAperturarCaja())
        '  Console.ReadKey()
    End Sub
    Public Function CerrarAperturarCaja() As String
        Dim resultado As String = ""
        Dim CEstablecimiento As New CEstablecimiento
        Dim obeParametroPOS As New BEParametroPOS
        Try
            resultado = CEstablecimiento.CerrarAperturarCaja()
        Catch ex As Exception
            Dim objUtil As New UtilXML
            resultado = objUtil.GenerarXMLErroPOS()
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try

        Return resultado

    End Function

    Private Sub DoStartRemotingServices()
        Try

            SPE.ProcesoAperturaCierre.RemoteServices.Instance = New SPE.ProcesoAperturaCierre.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try


        If Not EstablishConnection() Then
            Return
        End If
    End Sub
    Function EstablishConnection() As Boolean
        Return SPE.ProcesoAperturaCierre.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function
    'Public Function AperturarCaja(ByVal NroOperacion As String, ByVal NroEstablecimiento As String, _
    ' ByVal IdMoneda As Integer, ByVal Monto As Decimal) As String
    '    Dim CEstablecimiento As New CEstablecimiento
    '    Dim obeParametroPOS As New BEParametroPOS
    '    Dim resultado As String = ""

    '    Try
    '        obeParametroPOS.NroOperacion = NroOperacion.ToString()
    '        obeParametroPOS.NroEstablecimiento = NroEstablecimiento.ToString()
    '        obeParametroPOS.IdMoneda = IdMoneda
    '        obeParametroPOS.Monto = Monto
    '        resultado = CEstablecimiento.AperturarCaja(obeParametroPOS)
    '    Catch ex As Exception
    '        Dim objUtil As New UtilXML
    '        resultado = objUtil.GenerarXMLErroPOS()
    '        _3Dev.FW.Web.Log.Logger.LogException(ex)
    '    End Try

    '    Return resultado

    'End Function
    'Public Function AperturarCaja() As String
    '    Dim CEstablecimiento As New CEstablecimiento
    '    Dim obeParametroPOS As New BEParametroPOS
    '    Dim resultado As String = ""

    '    Try
    '        resultado = CEstablecimiento.AperturarCaja(obeParametroPOS)
    '    Catch ex As Exception
    '        Dim objUtil As New UtilXML
    '        resultado = objUtil.GenerarXMLErroPOS()
    '        _3Dev.FW.Web.Log.Logger.LogException(ex)
    '    End Try

    '    Return resultado

    'End Function

  
    'Public Function CerrarCaja(ByVal NroOperacion As String, ByVal NroEstablecimiento As String, _
    ' ByVal MontoEfectivoSoles As Decimal, ByVal MontoEfectivoDolares As Decimal, ByVal MontoTarjetaSoles As Decimal, ByVal MontoTarjetaDolares As Decimal) As String
    '    Dim CEstablecimiento As New CEstablecimiento
    '    Dim obeParametroPOS As New BEParametroPOS
    '    Dim resultado As String = ""

    '    Try
    '        obeParametroPOS.NroOperacion = NroOperacion.ToString()
    '        obeParametroPOS.NroEstablecimiento = NroEstablecimiento.ToString()
    '        obeParametroPOS.MontoEfectivoSoles = MontoEfectivoSoles
    '        obeParametroPOS.MontoEfectivoDolares = MontoEfectivoDolares
    '        obeParametroPOS.MontoTarjetaSoles = MontoTarjetaSoles
    '        obeParametroPOS.MontoTarjetaDolares = MontoTarjetaDolares
    '        resultado = CEstablecimiento.CerrarCaja(obeParametroPOS)
    '    Catch ex As Exception
    '        Dim objUtil As New UtilXML
    '        resultado = objUtil.GenerarXMLErroPOS()
    '        _3Dev.FW.Web.Log.Logger.LogException(ex)
    '    End Try

    '    Return resultado
    'End Function

End Module
