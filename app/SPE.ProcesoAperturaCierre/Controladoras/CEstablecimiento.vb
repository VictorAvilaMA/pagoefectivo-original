Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades




Public Class CEstablecimiento

    Public Function ConsultarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.ConsultarOrdenPago(obeParametroPOS)
    End Function

    Public Function CancelarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.CancelarOrdenPago(obeParametroPOS)
    End Function

    Public Function AnularOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.AnularOrdenPago(obeParametroPOS)
    End Function

    Public Function AperturarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.AperturarCaja(obeParametroPOS)
    End Function
    Public Function AperturarCaja() As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.AperturarCaja()
    End Function
    Public Function CerrarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.CerrarCaja(obeParametroPOS)
    End Function
    Public Function CerrarAperturarCaja() As String
        Return CType(RemoteServices.Instance, RemoteServices).IEstablecimiento.CerrarAperturarCaja()
    End Function
End Class
