Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun


    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices

        Private _IEstablecimiento As IEstablecimiento
        Public Property IEstablecimiento() As IEstablecimiento
            Get
                Return _IEstablecimiento
            End Get
            Set(ByVal value As IEstablecimiento)
                _IEstablecimiento = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()

            MyBase.DoSetupNetworkEnviroment()
            Me.IEstablecimiento = CType(Activator.GetObject(GetType(IEstablecimiento), Me.ServerUrl + "/" + NombreServiciosConocidos.IEstablecimiento), IEstablecimiento)

        End Sub

    End Class



