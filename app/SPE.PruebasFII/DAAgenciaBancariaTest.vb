﻿'The following code was generated by Microsoft Visual Studio 2005.
'The test owner should check each test for validity.
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System
Imports System.Text
Imports System.Collections.Generic
Imports SPE.AccesoDatos
Imports SPE.Entidades



'''<summary>
'''This is a test class for SPE.AccesoDatos.DAAgenciaBancaria and is intended
'''to contain all SPE.AccesoDatos.DAAgenciaBancaria Unit Tests
'''</summary>
<TestClass()> _
Public Class DAAgenciaBancariaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property
#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for ActualizarAgenciaBancaria(ByVal SPE.Entidades.BEAgenciaBancaria)
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarAgenciaBancariaTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim obeAgenciaBancaria As New BEAgenciaBancaria 'TODO: Initialize to an appropriate value

        obeAgenciaBancaria.IdAgenciaBancaria = 12
        obeAgenciaBancaria.IdBanco = 1
        obeAgenciaBancaria.Codigo = "191014"
        obeAgenciaBancaria.Descripcion = "Argentina"
        obeAgenciaBancaria.Direccion = "Av. Argentina 1561"
        obeAgenciaBancaria.IdEstado = 191
        obeAgenciaBancaria.IdUsuarioActualizacion = 1
        obeAgenciaBancaria.IdCiudad = 1


        Dim expected As Integer = -1
        Dim actual As Integer

        actual = target.ActualizarAgenciaBancaria(obeAgenciaBancaria)

        Assert.AreEqual(expected, actual, "SPE.AccesoDatos.DAAgenciaBancaria.ActualizarAgenciaBancaria did not return the ex" & _
                "pected value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for AnularOperacionPago(ByVal SPE.Entidades.BEMovimiento)
    '''</summary>
    <TestMethod()> _
    Public Sub AnularOperacionPagoTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim obeMovimiento As New BEMovimiento 'TODO: Initialize to an appropriate value
        obeMovimiento.CodigoAgenciaBancaria = "191410"
        obeMovimiento.NumeroOrdenPago = "26"

        Dim expected As Integer = -1
        Dim actual As Integer

        actual = target.AnularOperacionPago(obeMovimiento)

        Assert.AreEqual(expected, actual, "SPE.AccesoDatos.DAAgenciaBancaria.AnularOperacionPago did not return the expected" & _
                " value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for ConsultarAgenciaBancaria(ByVal SPE.Entidades.BEAgenciaBancaria)
    '''</summary>
    <TestMethod()> _
    Public Sub ConsultarAgenciaBancariaTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim obeAgenciaBancaria As New BEAgenciaBancaria 'TODO: Initialize to an appropriate value
        Dim cantidadActual, cantidadEsperada As Int32

        cantidadEsperada = 14

        obeAgenciaBancaria.Descripcion = ""
        obeAgenciaBancaria.IdEstado = 0


        cantidadActual = target.ConsultarAgenciaBancaria(obeAgenciaBancaria).Count

        Assert.AreEqual(cantidadEsperada, cantidadActual, "SPE.AccesoDatos.DAAgenciaBancaria.ConsultarAgenciaBancaria did not return the exp" & _
                "ected value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for ConsultarAgenciaBancariaxCodigo(ByVal String)
    '''</summary>
    <TestMethod()> _
    Public Sub ConsultarAgenciaBancariaxCodigoTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim codAgenciaBancaria As String = "191014" 'TODO: Initialize to an appropriate value
        Dim codigoActual As String

        codigoActual = target.ConsultarAgenciaBancariaxCodigo(codAgenciaBancaria).Codigo

        Assert.AreEqual(codAgenciaBancaria, codigoActual, "SPE.AccesoDatos.DAAgenciaBancaria.ConsultarAgenciaBancariaxCodigo did not return " & _
                "the expected value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for ConsultarAgenciaBancariaxId(ByVal Integer)
    '''</summary>
    <TestMethod()> _
    Public Sub ConsultarAgenciaBancariaxIdTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim idAgenciaBancaria As Integer = 16 'TODO: Initialize to an appropriate value
        Dim idActual As Integer


        idActual = target.ConsultarAgenciaBancariaxId(idAgenciaBancaria).IdAgenciaBancaria

        Assert.AreEqual(idAgenciaBancaria, idActual, "SPE.AccesoDatos.DAAgenciaBancaria.ConsultarAgenciaBancariaxId did not return the " & _
                "expected value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for RegistrarAgenciaBancaria(ByVal SPE.Entidades.BEAgenciaBancaria)
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarAgenciaBancariaExisteTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim obeAgenciaBancaria As New BEAgenciaBancaria 'TODO: Initialize to an appropriate value

        'Setear Valores de prueba de registro 
        obeAgenciaBancaria.Codigo = "191014"

        Dim expected As Integer = 1000
        Dim actual As Integer

        actual = target.RegistrarAgenciaBancaria(obeAgenciaBancaria)
        Assert.AreEqual(expected, actual, "SPE.AccesoDatos.DAAgenciaBancaria.RegistrarAgenciaBancaria did not return the exp" & _
        "ected value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for RegistrarAgenciaBancaria(ByVal SPE.Entidades.BEAgenciaBancaria)
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarAgenciaBancariaInsertaTest()
        Dim target As DAAgenciaBancaria = New DAAgenciaBancaria

        Dim obeAgenciaBancaria As New BEAgenciaBancaria 'TODO: Initialize to an appropriate value

        'Setear Valores de prueba de registro 
        obeAgenciaBancaria.Codigo = "000000"
        obeAgenciaBancaria.Descripcion = "Test Agencia bancaria"
        obeAgenciaBancaria.IdCiudad = 1
        obeAgenciaBancaria.IdBanco = 1
        obeAgenciaBancaria.IdEstado = 193


        Dim expected As Integer = -1
        Dim actual As Integer

        actual = target.RegistrarAgenciaBancaria(obeAgenciaBancaria)

        Assert.AreEqual(expected, actual, "SPE.AccesoDatos.DAAgenciaBancaria.RegistrarAgenciaBancaria did not return the exp" & _
                "ected value.")
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub


End Class
