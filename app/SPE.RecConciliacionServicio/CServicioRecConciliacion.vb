﻿Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Collections.Generic
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.EmsambladoComun
Imports SPE.Entidades
Imports System.Text
Imports SPE.EmsambladoComun


Public Class CServicioRecConciliacion
    Implements IDisposable

    Public Function RecalcularConciliaciones() As Integer
        Try
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Dim oiConciliacion As SPE.EmsambladoComun.IConciliacion = Conexionsss.DevolverContrato(New SPE.EmsambladoComun.EntityBaseContractResolver())
                Dim IdUsRec As Integer = ObjectToInt32(System.Configuration.ConfigurationManager.AppSettings("UsRecConciliacion"))
                oiConciliacion.RecalcularCIPsConciliacion(IdUsRec)
                Write("Recalculo Correcto.")
            End Using
        Catch ex As Exception
            'Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(ex)
            Throw ex
        End Try
        Return 1
    End Function

    Public Shared Sub Write(ByVal m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToString("F") & " >>>> " & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub
#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region



End Class
