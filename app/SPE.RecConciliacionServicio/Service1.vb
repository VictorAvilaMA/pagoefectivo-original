﻿Imports System.ComponentModel

Public Class Service1
    Dim bw As New BackgroundWorker

    Sub mensaje(ByVal s As String)
        System.Windows.Forms.MessageBox.Show(s)
    End Sub
    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            bw.RunWorkerAsync()
        Catch ex As Exception
            CServicioRecConciliacion.Write(ex.ToString)
            EventLog.WriteEntry("No se puedo iniciar el Servicio de Recalculo de Conciliacion - Pago Efectivo: " + ex.Message)
        End Try
    End Sub

    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        While True
            Try
                Dim proxy As New CServicioRecConciliacion
                proxy.RecalcularConciliaciones()
                Dim tiempomiliseg As Long = Convert.ToInt64(System.Configuration.ConfigurationManager.AppSettings("Intervalo"))
                Threading.Thread.Sleep(tiempomiliseg)
            Catch ex As Exception
                EventLog.WriteEntry("El serrvicio que estaba operativo se cayo: " + ex.Message)
                CServicioRecConciliacion.Write(ex.ToString)
            End Try
        End While
    End Sub
    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        'Throw New NotImplementedException
    End Sub
    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        'Throw New NotImplementedException
    End Sub
    Protected Overrides Sub OnContinue()
        MyBase.OnContinue()
    End Sub
    Protected Overrides Sub OnPause()
        MyBase.OnPause()
    End Sub
    Protected Overrides Sub OnShutdown()
        bw.Dispose()
    End Sub
    Protected Overrides Sub Finalize()
        'SPE.NotificacionServicio.RemoteServices.Instance = Nothing
        bw.Dispose()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

End Class
