﻿Imports System
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Linq
Imports System.IO

Public Class Service1

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.

        Dim RutaCarpeta As String = ConfigurationManager.AppSettings("RutaCarpeta").ToString().Trim()
        Dim Extension As String = ConfigurationManager.AppSettings("Extension").ToString().Trim()
        Dim TamanioMaximo As Int64 = Convert.ToInt64(ConfigurationManager.AppSettings("TamanioMaximo").ToString())
        RotarLog(RutaCarpeta, Extension, TamanioMaximo)

    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub RotarLog(RutaCarpeta As String, Extension As String, TamanioMaximo As Int64)
        Dim TamanioArchivo As Int64 = 0
        Dim NombreArchivo As String = String.Empty
        Try
            Dim Directorio As New DirectoryInfo(RutaCarpeta)
            If Directorio IsNot Nothing Then
                Dim ListaArchivos As IEnumerable(Of FileInfo) = Directorio.GetFiles(Extension, SearchOption.AllDirectories)
                If ListaArchivos IsNot Nothing Then
                    If ListaArchivos.Count() > 0 Then
                        TamanioArchivo = (From Archivo In ListaArchivos Let Resultado = Archivo.Length Select Resultado).Max()
                        NombreArchivo = (From Archivo In ListaArchivos Where Archivo.Length = TamanioArchivo Select Archivo.Name).FirstOrDefault()
                        If TamanioArchivo > 0 Then
                            If TamanioArchivo >= TamanioMaximo Then
                                Dim RutaArchivo As String = RutaCarpeta + NombreArchivo
                                Dim ExisteArchivo As Boolean = False
                                ExisteArchivo = File.Exists(RutaArchivo)
                                If ExisteArchivo = True Then
                                    Dim Informacion As FileInfo = New FileInfo(RutaArchivo)
                                    If Informacion IsNot Nothing Then
                                        Informacion.Attributes = FileAttributes.Normal
                                        ExisteArchivo = File.Exists(RutaArchivo)
                                        If ExisteArchivo = True Then
                                            File.Delete(RutaArchivo)
                                        End If
                                        ExisteArchivo = File.Exists(RutaArchivo)
                                        If ExisteArchivo = False Then
                                            File.Create(RutaArchivo)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
