﻿Imports System
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Linq
Imports System.IO

Public Class CServicioRotacionLog

    Public Sub RotarLog()
        Console.WriteLine("Sistema Pago Efectivo")
        Console.WriteLine("Inicio Rutina Rotar Log")
        Dim TamanioArchivo As Int64 = 0
        Dim NombreArchivo As String = String.Empty
        Try
            Dim RutaCarpeta As String = ConfigurationManager.AppSettings("RutaCarpeta").ToString().Trim()
            Dim Extension As String = ConfigurationManager.AppSettings("Extension").ToString().Trim()
            Dim TamanioMaximo As Int64 = Convert.ToInt64(ConfigurationManager.AppSettings("TamanioMaximo").ToString())
            Dim Directorio As New DirectoryInfo(RutaCarpeta)
            If Directorio IsNot Nothing Then
                Try
                    Dim ListaArchivos As IEnumerable(Of FileInfo) = Directorio.GetFiles(Extension, SearchOption.AllDirectories)
                    If ListaArchivos IsNot Nothing Then
                        If ListaArchivos.Count() > 0 Then
                            Try
                                TamanioArchivo = (From Archivo In ListaArchivos Let Resultado = Archivo.Length Select Resultado).Max()
                                NombreArchivo = (From Archivo In ListaArchivos Where Archivo.Length = TamanioArchivo Select Archivo.Name).FirstOrDefault()
                                If TamanioArchivo > 0 Then
                                    If TamanioArchivo >= TamanioMaximo Then
                                        Dim RutaArchivo As String = RutaCarpeta + NombreArchivo
                                        Dim ExisteArchivo As Boolean = False
                                        ExisteArchivo = File.Exists(RutaArchivo)
                                        If ExisteArchivo = True Then
                                            Try
                                                Dim Informacion As FileInfo = New FileInfo(RutaArchivo)
                                                If Informacion IsNot Nothing Then
                                                    Informacion.Attributes = FileAttributes.Normal
                                                    ExisteArchivo = File.Exists(RutaArchivo)
                                                    If ExisteArchivo = True Then
                                                        Try
                                                            File.Delete(RutaArchivo)
                                                            Console.WriteLine("Éxito al Eliminar el Archivo : " + RutaArchivo)
                                                        Catch ex As Exception
                                                            Console.WriteLine("Error : " + ex.Message)
                                                        End Try
                                                    End If
                                                    ExisteArchivo = File.Exists(RutaArchivo)
                                                    If ExisteArchivo = False Then
                                                        Try
                                                            File.Create(RutaArchivo)
                                                            Console.WriteLine("Éxito al Crear el Archivo : " + RutaArchivo)
                                                        Catch ex As Exception
                                                            Console.WriteLine("Error : " + ex.Message)
                                                        End Try
                                                    End If
                                                End If
                                            Catch ex As Exception
                                                Console.WriteLine("Error : " + ex.Message)
                                            End Try
                                        End If
                                    Else
                                        Console.WriteLine("No se ha encontrado archivos >= al tamaño máximo : " + TamanioMaximo.ToString() + " bytes")
                                    End If
                                Else
                                    Console.WriteLine("No se ha encontrado archivos >= al tamaño máximo : " + TamanioMaximo.ToString() + " bytes")
                                End If
                            Catch ex As Exception
                                Console.WriteLine("Error : " + ex.Message)
                            End Try
                        Else
                            Console.WriteLine("No se ha encontrado archivos en la ruta " + RutaCarpeta)
                        End If
                    Else
                        Console.WriteLine("No se ha encontrado archivos en la ruta " + RutaCarpeta)
                    End If
                Catch ex As Exception
                    Console.WriteLine("Error : " + ex.Message)
                End Try
            Else
                Console.WriteLine("No se puede explorar el directorio " + RutaCarpeta)
            End If
        Catch ex As Exception
            Console.WriteLine("Error : " + ex.Message)
        End Try
        Console.WriteLine("Fin Rutina Rotar Log")
        Console.ReadLine()
    End Sub

End Class
