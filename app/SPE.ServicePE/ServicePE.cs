﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SPE.ServidorRemoting;
using SPE.ServidorRemoting.Seguridad;
using System.ServiceModel;
using System.ServiceModel.Description;
using SPE.EmsambladoComun;
//using System.Runtime.Serialization.

namespace SPE.ServicePE
{
    partial class ServicePE : ServiceBase
    {
        List<Type> tipoServicios = new List<Type>();
        List<ServiceHost> listaServicios = new List<ServiceHost>();

        public ServicePE()
        {

            #region Agregamos los Servicios a la lista de tipos
            tipoServicios.Add(typeof(EmailServer));
            tipoServicios.Add(typeof(AgenciaBancariaServer));
            tipoServicios.Add(typeof(AgenciaRecaudadoraServer));
            tipoServicios.Add(typeof(BancoServer));
            tipoServicios.Add(typeof(CajaServer));
            tipoServicios.Add(typeof(ClienteServer));
            tipoServicios.Add(typeof(ComercioServer));
            tipoServicios.Add(typeof(ComunServer));
            tipoServicios.Add(typeof(ConciliacionServer));
            tipoServicios.Add(typeof(FTPArchivoServer));
            tipoServicios.Add(typeof(EmpresaContratanteServer));
            tipoServicios.Add(typeof(EstablecimientoServer));
            tipoServicios.Add(typeof(JefeProductoServer));
            tipoServicios.Add(typeof(PasarelaServer));
            tipoServicios.Add(typeof(PlantillaServer));
            tipoServicios.Add(typeof(PuntoVentaServer));
            tipoServicios.Add(typeof(ServicioNotificacionServer));
            tipoServicios.Add(typeof(OperadorServer));
            tipoServicios.Add(typeof(OrdenPagoServer));
            tipoServicios.Add(typeof(ParametroServer));
            tipoServicios.Add(typeof(RepresentanteServer));
            tipoServicios.Add(typeof(ServicioComunServer));
            tipoServicios.Add(typeof(ServicioServer));
            tipoServicios.Add(typeof(UbigeoServer));
            tipoServicios.Add(typeof(SeguridadServer));
            tipoServicios.Add(typeof(DineroVirtualServer));

            tipoServicios.Add(typeof(SPEMemberShipServer));
            tipoServicios.Add(typeof(SPERoleProviderServer));
            tipoServicios.Add(typeof(SPESiteMapServer));
            tipoServicios.Add(typeof(SPEUsuarioServer));
            tipoServicios.Add(typeof(CuentaServer));
            tipoServicios.Add(typeof(ServicioInstitucionServer));
            tipoServicios.Add(typeof(ServicioMunicipalidadBarrancoServer));

            #endregion

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CrearServicios();
        }

        protected override void OnStop()
        {
            foreach (var itemService in listaServicios )
                itemService.Close();
        }

        private  void CrearServicios()
        {

            for (int i = 0; i < tipoServicios.Count; i++)
            {
                ServiceHost myServiceHostComunServer = null;
                Type tipo = tipoServicios[i];
                if (myServiceHostComunServer != null) { myServiceHostComunServer.Close(); }
                myServiceHostComunServer = new ServiceHost(tipo);
                foreach (var operation in myServiceHostComunServer.Description.Endpoints.First().Contract.Operations)
                    operation.Behaviors.Find<DataContractSerializerOperationBehavior>().DataContractResolver = new EntityBaseContractResolver();
                myServiceHostComunServer.Open();
                listaServicios.Add(myServiceHostComunServer);
            }

        }









    }
}
