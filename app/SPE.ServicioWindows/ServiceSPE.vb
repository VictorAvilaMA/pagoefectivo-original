Imports System.Collections
Imports System.ServiceProcess
Imports System.Configuration
Imports SPE.ServidorRemoting
Imports System.Runtime.Remoting.Channels
Imports System.Runtime.Remoting.Channels.Tcp
Imports Belikov.GenuineChannels
Imports Belikov.GenuineChannels.GenuineTcp

Public Class ServiceSPE


    Shared Sub Main()
        Dim ServicesToRun As System.ServiceProcess.ServiceBase()
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ServiceSPE}
        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Private _genuineTcpChannel As GenuineTcpChannel

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            ''
            '' Inicializar los servicios
            'Servicios.StartUpserver()
            '' Configuracion de la red
            'Dim properties As New Hashtable()
            'properties("port") = ConfigurationManager.AppSettings.Get("port")
            'properties("ClosePersistentConnectionAfterInactivity") = ConfigurationManager.AppSettings.Get("ClosePersistentConnectionAfterInactivity")
            'properties("MaxTimeSpanToReconnect") = ConfigurationManager.AppSettings.Get("MaxTimeSpanToReconnect")

            'Dim _genuineTcpChannel As New GenuineTcpChannel(properties, Nothing, Nothing)
            'ChannelServices.RegisterChannel(_genuineTcpChannel, False)
            ''
        Catch ex As Exception
            '
            EventLog.WriteEntry("No se puedo iniciar el Servicio Pago Efectivo: " + ex.Message)
            '
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Dim strCultura As String = ConfigurationManager.AppSettings("Cultura").ToString
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo(strCultura)

    End Sub



End Class
