Imports SPE.EmsambladoComun
Imports SPE.Negocio

Public Class EmailServer
    Inherits MarshalByRefObject
    Implements IEmail

    Public Overrides Function InitializeLifetimeService() As Object

        Return Nothing

    End Function
    Private blemail As BLEmail

    Private Function InstanciaBLEmail() As BLEmail
        If (blemail Is Nothing) Then
            blemail = New BLEmail()
        End If
        Return blemail
    End Function

    Public Function EnviarCorreoAAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer Implements EmsambladoComun.IEmail.EnviarCorreoAAdministrador
        Return InstanciaBLEmail().EnviarCorreoAAdministrador(pfrom, psubject, pbody)
    End Function

    Public Function EnviarCorreoConfirmacionUsuario(ByVal userId As Integer, ByVal nombrecliente As String, ByVal clave As String, ByVal pguid As String, ByVal pto As String) As Integer Implements EmsambladoComun.IEmail.EnviarCorreoConfirmacionUsuario
        Return InstanciaBLEmail().EnviarCorreoConfirmacionUsuario(userId, nombrecliente, clave, pguid, pto)
    End Function

    Public Function EnviarEmail(ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer Implements EmsambladoComun.IEmail.EnviarEmail
        Return InstanciaBLEmail().EnviarEmail(pfrom, pto, psubject, pbody, pisBodyHtml)
    End Function

    Public Function EnviarEmailGeneral(ByVal phost As String, ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer Implements EmsambladoComun.IEmail.EnviarEmailGeneral
        Return InstanciaBLEmail().EnviarEmailGeneral(phost, pfrom, pto, psubject, pbody, pisBodyHtml)
    End Function
End Class
