Imports System
Imports System.Collections.Generic
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports _3Dev.FW.ServidorRemoting
Imports _3Dev.FW.Entidades.Seguridad
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ClienteServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.ICliente


    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLCliente()
    End Function
    Public Function ActualizarEstadoUsuario(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer Implements EmsambladoComun.ICliente.ActualizarEstadoUsuario
        Dim obeCliente As New BLCliente
        Return obeCliente.ActualizarEstadoUsuario(obeUsuario)
    End Function
    '
    Public Function CambiarContraseņaUsuario(ByVal obeUsuario As BEUsuario) As Integer Implements EmsambladoComun.ICliente.CambiarContraseņaUsuario
        Dim obeCliente As New BLCliente
        Return obeCliente.CambiarContraseņaUsuario(obeUsuario)
    End Function
    Public Function ValidarEmailUsuarioPorEstado(ByVal obeUsuario As BEUsuario) As BEUsuario Implements EmsambladoComun.ICliente.ValidarEmailUsuarioPorEstado
        Dim obeCliente As New BLCliente
        Return obeCliente.ValidarEmailUsuarioPorEstado(obeUsuario)
    End Function
    Public Function GenerarPreordenPagoSuscriptores(ByVal xmlstring As String, ByVal urlservicio As String) As Integer Implements EmsambladoComun.ICliente.GenerarPreordenPagoSuscriptores
        Dim obeCliente As New BLCliente
        Return obeCliente.GenerarPreordenPagoSuscriptores(xmlstring, urlservicio)
    End Function

    Public Function ObtenerClientexIdUsuario(ByVal obeCliente As BECliente) As BECliente Implements ICliente.ObtenerClientexIdUsuario
        Dim obCliente As New BLCliente
        Return obCliente.ObtenerClientexIdUsuario(obeCliente)
    End Function

    Public Function ValidarTipoyNumeroDocPorEmail(ByVal obeUsuario As BEUsuario) As Integer Implements EmsambladoComun.ICliente.ValidarTipoyNumeroDocPorEmail
        Dim obCliente As New BLCliente
        Return obCliente.ValidarTipoyNumeroDocPorEmail(obeUsuario)
    End Function

    'Public Function ActualizarEstadoUsuario1(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer Implements EmsambladoComun.ICliente.ActualizarEstadoUsuario

    'End Function
End Class
