Imports System
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio

Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ComercioServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.IComercio



    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLComercio

    End Function


    Public Function GetComercioWithAO() As System.Collections.Generic.List(Of BEComercio) Implements EmsambladoComun.IComercio.GetComercioWithAO
        Return New BLComercio().GetComercioWhitAO()
    End Function


End Class
