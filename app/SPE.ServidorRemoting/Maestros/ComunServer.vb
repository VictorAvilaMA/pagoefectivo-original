Imports System
Imports System.Collections.Generic
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio

Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ComunServer
    Inherits MarshalByRefObject
    Implements IComun


    Public Overrides Function InitializeLifetimeService() As Object

        Return Nothing

    End Function

    Public Function ConsultarMoneda() As List(Of BEMoneda) Implements IComun.ConsultarMoneda
        '
        Dim objComun As New BLComun
        Return objComun.ConsultarMoneda()
        '
    End Function
    Public Function ConsultarMonedaPorId(ByVal parametro As Object) As BEMoneda Implements IComun.ConsultarMonedaPorId
        '
        Dim objComun As New BLComun
        Return objComun.ConsultarMonedaPorId(parametro)
        '
    End Function
    Public Function ConsultarMedioPago() As List(Of BEMedioPago) Implements IComun.ConsultarMedioPago
        '
        Dim objComun As New BLComun
        Return objComun.ConsultarMedioPago()
        '
    End Function
    Public Function ConsultarMedioPagoBancoByBanco(be As BEBanco) As List(Of BEMedioPagoBanco) Implements IComun.ConsultarMedioPagoBancoByBanco
        Dim objComun As New BLComun
        Return objComun.ConsultarMedioPagoBancoByBanco(be)
    End Function
    '<upd Proc.Tesoreria>
    Public Function ConsultarTarjeta() As List(Of BETarjeta) Implements IComun.ConsultarTarjeta
        '
        Dim objComun As New BLComun
        Return objComun.ConsultarTarjeta()
        '
    End Function

    Public Function RegistrarDetalleTarjeta(ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer Implements IComun.RegistrarDetalleTarjeta
        '
        Dim objComun As New BLComun
        Return objComun.RegistrarDetalleTarjeta(obeDetalleTarjeta)
        '
    End Function

    Public Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As List(Of BEMovimiento) Implements IComun.ConsultarMovimiento
        '
        Dim objComun As New BLComun
        Return objComun.ConsultarMovimiento(obeMovimiento)
        '
    End Function
    Public Function ConsultarFechaHoraActual() As DateTime Implements IComun.ConsultarFechaHoraActual
        Dim objComun As New BLComun
        Return objComun.ConsultarFechaHoraActual()
    End Function
    Public Function ConsultarTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion) Implements IComun.ConsultarTipoOrigenCancelacion
        Dim objComun As New BLComun
        Return objComun.ConsultarTipoOrigenCancelacion()
    End Function

    Function ConsultarPasarelaMedioPago() As List(Of BEPasarelaMedioPago) Implements IComun.ConsultarPasarelaMedioPago
        Dim objComun As New BLComun
        Return objComun.ConsultarPasarelaMedioPago()
    End Function
    Public Function ConsultaNumCorrelativo(ByVal url As String) As Integer Implements IComun.ConsultaNumCorrelativo
        Dim objComun As New BLComun
        Return objComun.ConsultaNumCorrelativo(url)
    End Function

#Region "Log"

    Public Function RegistrarLog(ByVal obelog As BELog) As Integer Implements IComun.RegistrarLog
        Using odaComun As New BLComun()
            Return odaComun.RegistrarLog(obelog)
        End Using
    End Function

    Public Function ConsultarLog(ByVal obelog As BELog) As List(Of BELog) Implements IComun.ConsultarLog
        Using oblComun As New BLComun()
            Return oblComun.ConsultarLog(obelog)
        End Using
    End Function

#End Region

#Region "Log BCP"

    Public Function RegistrarLogBCP(ByVal oLog As BELog) As Int64 Implements IComun.RegistrarLogBCP

        Using oComun As New BLComun()
            Return oComun.RegistrarLogBCP(oLog)
        End Using

    End Function

    Public Function RegistrarLogBCPResquest(ByVal codOperacion As String, ByVal obeBCP As BEBCPRequest) As String Implements IComun.RegistrarLogBCPResquest

        Using oComun As New BLComun()
            Return oComun.RegistrarLogBCPResquest(codOperacion, obeBCP)
        End Using

    End Function

    Public Function RegistrarLogBCPResponse(ByVal codOperacion As String, ByVal obeBCP As BEBCPResponse) As Int64 Implements IComun.RegistrarLogBCPResponse

        Using oComun As New BLComun()
            Return oComun.RegistrarLogBCPResponse(codOperacion, obeBCP)
        End Using

    End Function



#End Region

#Region "Log BBVA"

    Public Function RegistrarLogBBVA(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeBBVA As Object) As Int64 Implements IComun.RegistrarLogBBVA
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogBBVA(type, obelog, obeid, obeBBVA)
        End Using
    End Function

#End Region

#Region "Log Kasnet"

    Public Function RegistrarLogKasnet(ByVal type As String, ByVal obelog As BELog, ByVal obeid As Int64, ByVal obeKasnet As Object) As Int64 Implements IComun.RegistrarLogKasnet
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogKasnet(type, obelog, obeid, obeKasnet)
        End Using
    End Function

#End Region

#Region "Log BanBif"

    Public Function RegistrarLogBanBif(type As String, obelog As Entidades.BELog, obeid As Long, obeBBVA As Object) As Long Implements EmsambladoComun.IComun.RegistrarLogBanBif
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogBanBif(type, obelog, obeid, obeBBVA)
        End Using
    End Function

#End Region

#Region "Log FC"

    Public Function RegistrarLogFC(ByVal type As String, ByVal obeid As Int64, ByVal obeFC As Object, _
        ByVal oBELog As BELog) As Int64 Implements IComun.RegistrarLogFC
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogFC(type, obeid, obeFC, oBELog)
        End Using
    End Function

#End Region

#Region "Log Life"

    Public Function RegistrarLogLF(ByVal type As String, ByVal obeid As Int64, ByVal obeLF As Object, _
        ByVal oBELog As BELog) As Int64 Implements IComun.RegistrarLogLF
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogLF(type, obeid, obeLF, oBELog)
        End Using
    End Function

#End Region

#Region "Log MoMo"

    Public Function RegistrarLogMoMo(ByVal type As String, ByVal obeid As Int64, ByVal obeMoMo As Object, _
        ByVal oBELog As BELog) As Int64 Implements IComun.RegistrarLogMoMo
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogMoMo(type, obeid, obeMoMo, oBELog)
        End Using
    End Function

#End Region

#Region "Log Scotiabank"

    Public Function RegistrarLogSBK(ByVal type As String, ByVal obeid As Long, ByVal obeSBK As Object, _
        ByVal oBELog As Entidades.BELog) As Long Implements EmsambladoComun.IComun.RegistrarLogSBK
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogSBK(type, obeid, obeSBK, oBELog)
        End Using
    End Function

#End Region

#Region "Log Interbank"

    Public Function RegistrarLogIBK(ByVal type As String, ByVal obeid As Long, ByVal obeIBK As Object, _
        ByVal oBELog As Entidades.BELog) As Long Implements EmsambladoComun.IComun.RegistrarLogIBK
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogIBK(type, obeid, obeIBK, oBELog)
        End Using
    End Function

#End Region

#Region "Log Western Union"

    Public Function RegistrarLogWU(ByVal type As String, ByVal obeid As Long, ByVal obeIBK As Object) As Long Implements EmsambladoComun.IComun.RegistrarLogWU
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogWU(type, obeid, obeIBK)
        End Using
    End Function

#End Region

#Region "PassLog"

    Public Function RegistrarPasLog(ByVal oPasLog As BELog) As Integer Implements IComun.RegistarPasLog
        Using oComun As New BLComun
            Return oComun.RegistrarPassLog(oPasLog)
        End Using
    End Function

#End Region

#Region "M�todos para Administrar el Cache"
    Public Sub InicializarTodoCache() Implements IComun.InicializarTodoCache
        Using oblCommun As New BLComun
            oblCommun.InicializarTodoCache()
        End Using
    End Sub
    Public Sub InicializarElementoCache(ByVal nombre As String) Implements IComun.InicializarElementoCache
        Using oblCommun As New BLComun
            oblCommun.InicializarElementoCache(nombre)
        End Using
    End Sub

#End Region

#Region "Log Navegacion"
    Public Function RegistrarLogNavegacion(ByVal oLogNavegacion As BELogNavegacion) As Int64 Implements IComun.RegistrarLogNavegacion

        Using oComun As New BLComun()
            Return oComun.RegistrarLogNavegacion(oLogNavegacion)
        End Using

    End Function

    Public Function ConsultarLog(ByVal obelognavegacion As BELogNavegacion) As List(Of BELogNavegacion) Implements IComun.ConsultarLogNavegacion

        Using oblComun As New BLComun()
            Return oblComun.ConsultarLogNavegacion(obelognavegacion)
        End Using
    End Function

#End Region

    '<add Peru.Com FaseIII>
#Region "Log Redirect"
    Public Function RegistrarLogRedirect(ByVal oBELogRedirect As BELogRedirect) As Integer Implements IComun.RegistrarLogRedirect
        Using oblComun As New BLComun()
            Return oblComun.RegistrarLogRedirect(oBELogRedirect)
        End Using
    End Function
#End Region

#Region "Equipo Registrado X Usuario"
    Public Function RegistrarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Int64 Implements IComun.RegistrarEquipoRegistradoXusuario

        Using oComun As New BLComun()
            Return oComun.RegistrarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
        End Using

    End Function

    Public Function ValidarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario) Implements IComun.ValidarEquipoRegistradoXusuario

        Using oblComun As New BLComun()
            Return oblComun.ValidarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
        End Using
    End Function

    Public Function ConsultarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario) Implements IComun.ConsultarEquipoRegistradoXusuario

        Using oblComun As New BLComun()
            Return oblComun.ConsultarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
        End Using
    End Function

    Public Function EliminarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Int64 Implements IComun.EliminarEquipoRegistradoXusuario

        Using oComun As New BLComun()
            Return oComun.EliminarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
        End Using

    End Function

#End Region
    Public Function RegistrarTokenCambioCorreo(ByVal be As BEUsuarioBase) As BEUsuarioBase Implements IComun.RegistrarTokenCambioCorreo
        Dim objComun As New BLComun
        Return objComun.RegistrarTokenCambioCorreo(be)
    End Function
    Public Function ActualizarContraseniaOlvidada(ByVal be As BEUsuarioBase) As BEUsuarioBase Implements IComun.ActualizarContraseniaOlvidada
        Dim objComun As New BLComun
        Return objComun.ActualizarContraseniaOlvidada(be)
    End Function

    Public Function SuscriptorRegistrar(ByVal oBESuscriptor As SPE.Entidades.BESuscriptor) As String Implements EmsambladoComun.IComun.SuscriptorRegistrar
        Return New SPE.Negocio.BLComun().SuscriptorRegistrar(oBESuscriptor)
    End Function

    Public Function ConsultarPlantillaTipoVariacion() As List(Of BEParametro) Implements EmsambladoComun.IComun.ConsultarPlantillaTipoVariacion
        Return New SPE.Negocio.BLComun().ConsultarPlantillaTipoVariacion()
    End Function

    Public Function RecuperarCipsNoConciliados(ByVal idMoneda As Integer, ByVal fecha As DateTime) As List(Of BELiquidacion) Implements EmsambladoComun.IComun.RecuperarCipsNoConciliados
        Return New SPE.Negocio.BLComun().RecuperarCipsNoConciliados(idMoneda, fecha)
    End Function

    Public Function RecuperarDepositosBCP(ByVal idMoneda As Integer, ByVal empresas As String) As List(Of BELiquidacion) Implements EmsambladoComun.IComun.RecuperarDepositosBCP
        Return New SPE.Negocio.BLComun().RecuperarDepositosBCP(idMoneda, empresas)
    End Function


#Region "Liquidaciones"
    Public Function RegistrarPeriodo(ByVal be As BEPeriodoLiquidacion) As Integer Implements IComun.RegistrarPeriodo
        Dim objComun As New BLComun
        Return objComun.RegistrarPeriodo(be)
    End Function

    Public Function DeletePeriodo(ByVal be As BEPeriodoLiquidacion) As Integer Implements IComun.DeletePeriodo
        Dim objComun As New BLComun
        Return objComun.DeletePeriodo(be)
    End Function

    Public Function ActualizarPeriodo(ByVal be As BEPeriodoLiquidacion) As Integer Implements IComun.ActualizarPeriodo
        Dim objComun As New BLComun
        Return objComun.ActualizarPeriodo(be)
    End Function

#End Region

    Public Function ConsultarIdDescripcionPeriodo() As System.Collections.Generic.List(Of Entidades.BEPeriodoLiquidacion) Implements EmsambladoComun.IComun.ConsultarIdDescripcionPeriodo
        Dim objBlComun As New BLComun
        Return objBlComun.ConsultarIdDescripcionPeriodo()
    End Function

    Public Function RecuperarDatosPeriodoLiquidacion(ByVal be As BEPeriodoLiquidacion) As System.Collections.Generic.List(Of Entidades.BEPeriodoLiquidacion) Implements EmsambladoComun.IComun.RecuperarDatosPeriodoLiquidacion
        Dim objBlComun As New BLComun
        Return objBlComun.RecuperarDatosPeriodoLiquidacion(be)
    End Function

End Class
