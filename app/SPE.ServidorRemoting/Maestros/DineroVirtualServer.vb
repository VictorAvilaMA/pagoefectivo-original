﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class DineroVirtualServer
    Implements IDineroVirtual

#Region "Victor"

    Public Function RegistrarCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As Entidades.BECuentaDineroVirtual) As Integer Implements EmsambladoComun.IDineroVirtual.RegistrarCuentaDineroVirtual
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.RegistrarCuentaDineroVirtual(oBECuentaDineroVirtual)
    End Function

    Public Function ActualizarEstadoCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As Entidades.BECuentaDineroVirtual) As Integer Implements EmsambladoComun.IDineroVirtual.ActualizarEstadoCuentaDineroVirtual
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ActualizarEstadoCuentaDineroVirtual(oBECuentaDineroVirtual)
    End Function

    Function ConsultarMovimientoXIdCuenta(ByVal entidad As BEMovimientoCuenta) As System.Collections.Generic.List(Of Entidades.BEMovimientoCuenta) Implements EmsambladoComun.IDineroVirtual.ConsultarMovimientoXIdCuenta
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarMovimientoXIdCuenta(entidad)
    End Function

    Function ConsultarMovimientoXIdCuentaExportar(ByVal entidad As BEMovimientoCuenta) As System.Collections.Generic.List(Of Entidades.BEMovimientoCuenta) Implements EmsambladoComun.IDineroVirtual.ConsultarMovimientoXIdCuentaExportar
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarMovimientoXIdCuentaExportar(entidad)
    End Function

    Public Function ActualizarMontoRecargaDineroVirtual(ByVal olistaBEMontoRecargaMonedero As List(Of BEMontoRecargaMonedero)) As Integer Implements EmsambladoComun.IDineroVirtual.ActualizarMontoRecargaDineroVirtual
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ActualizarMontoRecargaDineroVirtual(olistaBEMontoRecargaMonedero)
    End Function

    Function ConsultarMonedaXIdCliente(ByVal entidad As BEMontoRecargaMonedero) As System.Collections.Generic.List(Of Entidades.BEMontoRecargaMonedero) Implements EmsambladoComun.IDineroVirtual.ConsultarMonedaXIdCliente
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarMonedaXIdCliente(entidad)
    End Function

    Function ConsultarClienteXIdClienteDV(ByVal entidad As BECliente) As BECliente Implements EmsambladoComun.IDineroVirtual.ConsultarClienteXIdClienteDV
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarClienteXIdClienteDV(entidad)
    End Function

    Public Function ActualizarEstadoHabilitarMonederoClienteDV(ByVal oBECliente As Entidades.BECliente) As Integer Implements EmsambladoComun.IDineroVirtual.ActualizarEstadoHabilitarMonederoClienteDV
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ActualizarEstadoHabilitarMonederoClienteDV(oBECliente)
    End Function

    Public Function ConsultarMonedaMonederoVirtual() As List(Of BEMoneda) Implements IDineroVirtual.ConsultarMonedaMonederoVirtual
        '
        Dim objDineroVirtual As New BLDineroVirtual
        Return objDineroVirtual.ConsultarMonedaMonederoVirtual()
        '
    End Function

#End Region


#Region "Joe"

    Function ActualizarCuentaDineroVirtual(ByVal entidad As BECuentaDineroVirtual) As Boolean Implements EmsambladoComun.IDineroVirtual.ActualizarCuentaDineroVirtual
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ActualizarCuentaDineroVirtual(entidad)
    End Function

    Function ConsultarCuentaDineroVirtualUsuarioByCuentaId(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual Implements EmsambladoComun.IDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByCuentaId
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByCuentaId(entidad)
    End Function

    Function ConsultarCuentaDineroVirtualUsuarioByNumero(ByVal entidad As BECuentaDineroVirtual) As BECuentaDineroVirtual Implements EmsambladoComun.IDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(entidad)
    End Function



    Function ConsultarCuentaDineroVirtualUsuario(ByVal entidad As BECuentaDineroVirtual) As System.Collections.Generic.List(Of Entidades.BECuentaDineroVirtual) Implements EmsambladoComun.IDineroVirtual.ConsultarCuentaDineroVirtualUsuario
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarCuentaDineroVirtualUsuario(entidad)
    End Function

    Function ConsultarSolicitudRetiroMonedero(ByVal entidad As BESolicitudRetiroDineroVirtual) As List(Of BESolicitudRetiroDineroVirtual) Implements EmsambladoComun.IDineroVirtual.ConsultarSolicitudRetiroMonedero
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarSolicitudRetiroMonedero(entidad)
    End Function


    Function ConsultarSolicitudRetiroMonederoById(ByVal entidad As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual Implements EmsambladoComun.IDineroVirtual.ConsultarSolicitudRetiroMonederoById
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ConsultarSolicitudRetiroMonederoById(entidad)
    End Function

    Function ActualizarSolicitudRetiroDinero(ByVal entidad As BESolicitudRetiroDineroVirtual) As Boolean Implements EmsambladoComun.IDineroVirtual.ActualizarSolicitudRetiroDinero
        Dim objBLCuentaDineroVirtual As New BLDineroVirtual
        Return objBLCuentaDineroVirtual.ActualizarSolicitudRetiroDinero(entidad)
    End Function




#End Region

#Region "IMPLEMENTADO POR MXRT¡N Patora"
    Public Function ConsultarCuentasVirtualesAdministrador(ByVal oBECuentaDineroVirtual As Entidades.BECuentaDineroVirtual) As System.Collections.Generic.List(Of Entidades.BECuentaDineroVirtual) Implements EmsambladoComun.IDineroVirtual.ConsultarCuentasVirtualesAdministrador
        Return New BLDineroVirtual().ConsultarCuentaDineroVirtualAdministrador(oBECuentaDineroVirtual)
    End Function

    Function ObtenerOrdenPagoCompletoPorNroOrdenPago(ByVal nroOrdenPago As Long) As BEOrdenPago Implements EmsambladoComun.IDineroVirtual.ObtenerOrdenPagoCompletoPorNroOrdenPago
        Return New BLDineroVirtual().ObtenerOrdenPagoCompletoPorNroOrdenPago(nroOrdenPago)
    End Function

    Function RegistrarTokenMonedero(ByVal token As BETokenDineroVirtual) As BETokenDineroVirtual Implements EmsambladoComun.IDineroVirtual.RegistrarTokenMonedero
        Return New BLDineroVirtual().RegistrarTokenMonedero(token)
    End Function

    Public Function RegistrarPagoCIP(ByVal beMovimiento As BEMovimientoCuenta) As String Implements EmsambladoComun.IDineroVirtual.RegistrarPagoCIP
        Return New BLDineroVirtual().RegistrarPagoCIP(beMovimiento)
    End Function

    Function RegistrarPagoCIPPortal(ByVal beMovimiento As BEMovimientoCuenta, ByVal oBESolicitudPago As BESolicitudPago) As String Implements EmsambladoComun.IDineroVirtual.RegistrarPagoCIPPortal
        Return New BLDineroVirtual().RegistrarPagoCIPPortal(beMovimiento, oBESolicitudPago)
    End Function

    Public Function RegistrarSolicitudRetiroDinero(ByVal beSolicitudRetiroDinero As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual Implements EmsambladoComun.IDineroVirtual.RegistrarSolicitudRetiroDinero
        Return New BLDineroVirtual().RegistrarSolicitudRetiroDinero(beSolicitudRetiroDinero)
    End Function

    Function ConsultarCuentaDineroVirtualPorNroCuenta(ByVal NroCuenta As String) As BECuentaDineroVirtual Implements EmsambladoComun.IDineroVirtual.ConsultarCuentaDineroVirtualPorNroCuenta
        Return New BLDineroVirtual().ConsultarCuentaDineroVirtualPorNroCuenta(NroCuenta)
    End Function

    Public Function RegistrarTransferenciaDinero(ByVal beMovimiento As BEMovimientoCuenta) As String Implements EmsambladoComun.IDineroVirtual.RegistrarTransferenciaDinero
        Return New BLDineroVirtual().RegistrarTransferenciaDinero(beMovimiento)
    End Function

    Public Function ConsultarReglaMovimientoSospechoso() As List(Of BEReglaMovimientoSospechoso) Implements EmsambladoComun.IDineroVirtual.ConsultarReglaMovimientoSospechoso
        Return New BLDineroVirtual().ConsultarReglaMovimientoSospechoso()
    End Function

    Public Function ConsultarRegistroEjecucion(ByVal IdRegla As Integer, ByVal Anio As Integer, ByVal Mes As Integer) As List(Of BERegistroEjecucionRegla) Implements EmsambladoComun.IDineroVirtual.ConsultarRegistroEjecucion
        Return New BLDineroVirtual().ConsultarRegistroEjecucion(IdRegla, Anio, Mes)
    End Function

    Function ConsultarResultadoEjecucionRegla(ByVal entidad As BEResultadoEjecucionRegla) As List(Of BEResultadoEjecucionRegla) Implements EmsambladoComun.IDineroVirtual.ConsultarResultadoEjecucionRegla
        Return New BLDineroVirtual().ConsultarResultadoEjecucionRegla(entidad)
    End Function

    Function ConsultarDetalleResultadoEjecucion(ByVal entidad As BEDetalleResultadoEjecucion) As List(Of BEDetalleResultadoEjecucion) Implements EmsambladoComun.IDineroVirtual.ConsultarDetalleResultadoEjecucion
        Return New BLDineroVirtual().ConsultarDetalleResultadoEjecucion(entidad)
    End Function

    Function ReenviarEmailToken(ByVal IdToken As Long) As BETokenDineroVirtual Implements EmsambladoComun.IDineroVirtual.ReenviarEmailToken
        Return New BLDineroVirtual().ReenviarEmailToken(IdToken)
    End Function
	
	Function ConsultarTokenByID(ByVal IdToken As Long) As BETokenDineroVirtual Implements EmsambladoComun.IDineroVirtual.ConsultarTokenByID
        Return New BLDineroVirtual().ConsultarTokenByID(IdToken)
    End Function 
	
#End Region
    'MDS
    Function ValidarRegistrarCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual Implements EmsambladoComun.IDineroVirtual.ValidarRegistrarCuentaDineroVirtual
        Dim obResultado As New BLDineroVirtual
        Return obResultado.ValidarRegistrarCuentaDineroVirtual(obecuentadinerovirtual)
    End Function

    Function ObtenerParametroMonederoByID(ByVal Idparametro As Integer) As BEParametroMonedero Implements EmsambladoComun.IDineroVirtual.ObtenerParametroMonederoByID
        Dim obResultado As New BLDineroVirtual
        Return obResultado.ObtenerParametroMonederoByID(Idparametro)
    End Function


    Function ConsultaUsuarioByIdCuenta(ByVal IdCuenta As Integer) As Integer Implements EmsambladoComun.IDineroVirtual.ConsultaUsuarioByIdCuenta
        Dim obResultado As New BLDineroVirtual
        Return obResultado.ConsultaUsuarioByIdCuenta(IdCuenta)
    End Function


    Function ConsultaEstadoUsuarioByIdUsuario(ByVal IdUsuario As Integer) As Integer Implements EmsambladoComun.IDineroVirtual.ConsultaEstadoUsuarioByIdUsuario
        Dim obResultado As New BLDineroVirtual
        Return obResultado.ConsultaEstadoUsuarioByIdUsuario(IdUsuario)
    End Function

End Class
