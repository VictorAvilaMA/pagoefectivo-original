Imports System
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class EmpresaContratanteServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.IEmpresaContratante

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLEmpresaContratante
    End Function
    Public Function OcultarEmpresa(ByVal idservicio As Integer) As BEOcultarEmpresa Implements EmsambladoComun.IEmpresaContratante.OcultarEmpresa
        Return CType(Me.BusinessLogicObject, BLEmpresaContratante).OcultarEmpresa(idservicio)
    End Function
    Public Function RegistrarTransaccionesaLiquidar(ByVal be As BETransferencia) As Integer Implements EmsambladoComun.IEmpresaContratante.RegistrarTransaccionesaLiquidar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.RegistrarTransaccionesaLiquidar(be)
    End Function
    Public Function ConsultarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements EmsambladoComun.IEmpresaContratante.ConsultarTransaccionesaLiquidar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ConsultarTransaccionesaLiquidar(be)
    End Function
    Public Function ObtenerFechaInicioTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime Implements EmsambladoComun.IEmpresaContratante.ObtenerFechaInicioTransaccionesaLiquidar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ObtenerFechaInicioTransaccionesaLiquidar(be)
    End Function
    Public Function ObtenerFechaInicioTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime Implements EmsambladoComun.IEmpresaContratante.ObtenerFechaInicioTransaccionesaLiquidarLote
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ObtenerFechaInicioTransaccionesaLiquidarLote(be)
    End Function
    Public Function ObtenerFechaFinTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime Implements EmsambladoComun.IEmpresaContratante.ObtenerFechaFinTransaccionesaLiquidar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ObtenerFechaFinTransaccionesaLiquidar(be)
    End Function
    Public Function ObtenerFechaFinTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime Implements EmsambladoComun.IEmpresaContratante.ObtenerFechaFinTransaccionesaLiquidarLote
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ObtenerFechaFinTransaccionesaLiquidarLote(be)
    End Function
    Public Function ActualizarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements EmsambladoComun.IEmpresaContratante.ActualizarTransaccionesaLiquidar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ActualizarTransaccionesaLiquidar(be)
    End Function
    Public Function ObtenerTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BETransferencia Implements EmsambladoComun.IEmpresaContratante.ObtenerTransaccionesaLiquidar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ObtenerTransaccionesaLiquidar(be)
    End Function
    Public Function ValidarPendientesaConciliar(ByVal be As BETransferencia) As Integer Implements EmsambladoComun.IEmpresaContratante.ValidarPendientesaConciliar
        Dim oblTransacciones As New BLEmpresaContratante
        Return oblTransacciones.ValidarPendientesaConciliar(be)
    End Function

#Region "Configuración de cuenta y banco de empresas"
    Public Function InsertarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements EmsambladoComun.IEmpresaContratante.InsertarCuentaEmpresa
        Dim oBLEmpresaContratante As New BLEmpresaContratante
        Return oBLEmpresaContratante.InsertarCuentaEmpresa(be)
    End Function
    Public Function ActualizarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements EmsambladoComun.IEmpresaContratante.ActualizarCuentaEmpresa
        Dim oBLEmpresaContratante As New BLEmpresaContratante
        Return oBLEmpresaContratante.ActualizarCuentaEmpresa(be)
    End Function
    Public Function ConsultarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements EmsambladoComun.IEmpresaContratante.ConsultarCuentaEmpresa
        Dim oBLEmpresaContratante As New BLEmpresaContratante
        Return oBLEmpresaContratante.ConsultarCuentaEmpresa(be)
    End Function
    Public Function ConsultarCuentaEmpresaPorId(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase Implements EmsambladoComun.IEmpresaContratante.ConsultarCuentaEmpresaPorId
        Dim oBLEmpresaContratante As New BLEmpresaContratante
        Return oBLEmpresaContratante.ConsultarCuentaEmpresaPorId(id)
    End Function
#End Region
#Region "LiquidacionLotes"
    Public Function RecuperarEmpresasLiquidacion(ByVal FechaDe As Date, ByVal FechaAl As Date, ByVal idPeriodoLiquidacion As Integer, ByVal idMoneda As Integer) As System.Collections.Generic.List(Of BEEmpresaContratante) Implements EmsambladoComun.IEmpresaContratante.RecuperarEmpresasLiquidacion
        Dim oblEmpresaContratantes As New BLEmpresaContratante
        Return oblEmpresaContratantes.RecuperarEmpresasLiquidacion(FechaDe, FechaAl, idPeriodoLiquidacion, idMoneda)
    End Function
#End Region

End Class
