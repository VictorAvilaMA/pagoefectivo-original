Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class FTPArchivoServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IFTPArchivo

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLFTPArchivo
    End Function
    Function ObtenerFTPArchivoPorId(ByVal id As Object, ByVal OrderBy As String, ByVal IsAccending As Boolean) As BEFTPArchivo Implements IFTPArchivo.ObtenerFTPArchivoPorId
        Dim objBLFTPArchivo As New BLFTPArchivo
        Return objBLFTPArchivo.ObtenerFTPArchivoPorId(id, OrderBy, IsAccending)
    End Function

    Public Function ProcesarArchivoFTPParaNotificar() As System.Collections.Generic.List(Of Entidades.BEServicio) Implements EmsambladoComun.IFTPArchivo.ProcesarArchivoFTPParaNotificar
        Return CType(BusinessLogicObject, SPE.Negocio.BLFTPArchivo).ProcesarArchivoFTPParaNotificar()
    End Function

End Class
