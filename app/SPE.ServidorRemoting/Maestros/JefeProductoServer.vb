Imports SPE.EmsambladoComun
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class JefeProductoServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements EmsambladoComun.IJefeProducto

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLJefeProducto
    End Function

End Class
