Imports System
Imports System.Collections.Generic
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation
Imports _3Dev.FW.Entidades

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class OrdenPagoServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IOrdenPago

    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function

    'formulario----------------------------------------->
    Public Function ConsutarFormularioActivo(ByVal IdServicio As Integer) As Boolean Implements EmsambladoComun.IOrdenPago.ConsutarFormularioActivo
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsutarFormularioActivo(IdServicio)
    End Function

    Public Function ConsultarOrdenPagoPorOrdenIdComercio(ByVal OrdenIdComercio As String, ByVal IdServicio As Integer) As BEOPXServClienCons Implements EmsambladoComun.IOrdenPago.ConsultarOrdenPagoPorOrdenIdComercio
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoPorOrdenIdComercio(OrdenIdComercio, IdServicio)
    End Function
    Public Function ConsultarOrdenPagoPorNumeroOrdenPago(ByVal NumeroOrdenPago As String) As Integer Implements EmsambladoComun.IOrdenPago.ConsultarOrdenPagoPorNumeroOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoPorNumeroOrdenPago(NumeroOrdenPago)
    End Function

    Public Function GenerarOrdenPago(ByVal obeOrdenPago As Entidades.BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer Implements EmsambladoComun.IOrdenPago.GenerarOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.GenerarOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
    End Function

    Public Function GenerarPreOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer Implements EmsambladoComun.IOrdenPago.GenerarPreOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.GenerarPreOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
    End Function

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New BLOrdenPago()
    End Function

    Public Function ConsultarOrdenPagoRecepcionar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago Implements IOrdenPago.ConsultarOrdenPagoRecepcionar
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoRecepcionar(obeOrdenPago)
    End Function

    Public Function ActualizarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer Implements IOrdenPago.ActualizarOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ActualizarOrdenPago(obeOrdenPago)
    End Function
    Public Function ProcesoRecepcionarOrdenPago(ByVal obeMovimiento As BEMovimiento, ByVal obeOrdenPago As BEOrdenPago, ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer Implements IOrdenPago.ProcesoRecepcionarOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ProcesoRecepcionarOrdenPago(obeMovimiento, obeOrdenPago, obeDetalleTarjeta)
    End Function
    Public Function ConsultarOrdenPagoPendientes(ByVal obeOrdePago As SPE.Entidades.BEOrdenPago, ByVal orderedBy As String, ByVal isAccending As Boolean) As List(Of BEOrdenPago) Implements IOrdenPago.ConsultarOrdenPagoPendientes
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoPendientes(obeOrdePago, orderedBy, isAccending)
    End Function
    Public Function ConsultarOrdenPagoPorId(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago Implements IOrdenPago.ConsultarOrdenPagoPorId
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoPorId(obeOrdenPago)
    End Function
    Public Function ConsultarDetalleOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As BEDetalleOrdenPago Implements IOrdenPago.ConsultarDetalleOrdenPago
        Dim oblDetalleOP As New BLOrdenPago
        Return oblDetalleOP.ConsultarDetalleOrdenPago(obeOrdenPago)
    End Function

    Public Function ConsultarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago) Implements IOrdenPago.ConsultarOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPago(obeOrdenPago)
    End Function

    Public Function ExpirarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer Implements IOrdenPago.ExpirarOrdenPago
        Return CType(BusinessLogicObject, SPE.Negocio.BLOrdenPago).ExpirarOrdenPago(obeOrdenPago)
    End Function
    Public Function ConsultarOrdenesPagoListaParaExpiradas() As List(Of BEOrdenPago) Implements IOrdenPago.ConsultarOrdenesPagoListaParaExpiradas
        Return CType(BusinessLogicObject, SPE.Negocio.BLOrdenPago).ConsultarOrdenesPagoListaParaExpiradas()
    End Function

    Public Function RealizarProcesoExpiracionOrdenPago() As Integer Implements EmsambladoComun.IOrdenPago.RealizarProcesoExpiracionOrdenPago
        Return CType(BusinessLogicObject, SPE.Negocio.BLOrdenPago).RealizarProcesoExpiracionOrdenPago()
    End Function
    Public Function ConsultarOrdenPagoNotificar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago Implements EmsambladoComun.IOrdenPago.ConsultarOrdenPagoNotificar
        Return CType(BusinessLogicObject, SPE.Negocio.BLOrdenPago).ConsultarOrdenPagoNotificar(obeOrdenPago)
    End Function



    Public Function ValidarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String Implements EmsambladoComun.IOrdenPago.ValidarOrdenPagoAgenciaBancaria
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ValidarOrdenPagoAgenciaBancaria(obeOrdenPago)
    End Function

    'Public Function GenerarOrdenPagoXML(ByVal obeOrdenPago As Entidades.BEOrdenPago, ByVal ListaDetalleOrdePago As System.Collections.Generic.List(Of Entidades.BEDetalleOrdenPago)) As Integer Implements EmsambladoComun.IOrdenPago.GenerarOrdenPagoXML
    '    Dim oblOrdenPago As New BLOrdenPago
    '    Return oblOrdenPago.GenerarOrdenPagoXML(obeOrdenPago, ListaDetalleOrdePago)
    'End Function

    Public Function AnularOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal obeMovimiento As BEMovimiento) As Integer Implements EmsambladoComun.IOrdenPago.AnularOrdenPago
        Using oblOrdenPago As New BLOrdenPago
            Return oblOrdenPago.AnularOrdenPago(obeOrdenPago, obeMovimiento)
        End Using
    End Function

    Public Function GenerarCIP(ByVal request As BEGenCIPRequest) As BEGenCIPResponse Implements IOrdenPago.GenerarCIP
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.GenerarCIP(request)
        End Using
    End Function

#Region "Metodos Servicios Web"
    Public Function WSGenerarCIP(ByVal request As BEWSGenCIPRequest) As BEWSGenCIPResponse Implements IOrdenPago.WSGenerarCIP
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSGenerarCIP(request)
        End Using
    End Function

    Public Function WSEliminarCIP(ByVal request As BEWSElimCIPRequest) As BEWSElimCIPResponse Implements IOrdenPago.WSEliminarCIP
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSEliminarCIP(request)
        End Using
    End Function

    Public Function WSActualizarCIP(ByVal request As BEWSActualizaCIPRequest) As BEWSActualizaCIPResponse Implements IOrdenPago.WSActualizarCIP
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSActualizarCIP(request)
        End Using
    End Function

    Public Function WSConsultarCIPConciliados(ByVal request As BEWSConsCIPsConciliadosRequest) As BEWSConsCIPsConciliadosResponse Implements IOrdenPago.WSConsultarCIPConciliados
        Using oBLOrdenPago As New BLOrdenPago()
            Return oBLOrdenPago.WSConsultarCIPConciliados(request)
        End Using
    End Function

    Public Function WSConsultarCIP(ByVal request As BEWSConsultarCIPRequest) As BEWSConsultarCIPResponse Implements IOrdenPago.WSConsultarCIP
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSConsultarCIP(request)
        End Using
    End Function

    Public Function WSConsultarCIPv2(ByVal request As BEWSConsultarCIPv2Request) As BEWSConsultarCIPv2Response Implements IOrdenPago.WSConsultarCIPv2
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSConsultarCIPv2(request)
        End Using
    End Function

    '<add Peru.Com FaseIII>
    Public Function WSSolicitarPago(ByVal request As BEWSSolicitarRequest) As BEWSSolicitarResponse Implements IOrdenPago.WSSolicitarPago
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSSolicitarPago(request)
        End Using
    End Function
    Public Function WSConsultarSolicitudPago(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse Implements IOrdenPago.WSConsultarSolicitudPago
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSConsultarSolicitudPago(request)
        End Using
    End Function


#Region "Nueva Modalidad 1"


    Public Function WSConsultarCIPMod1(ByVal request As BEWSConsultarCIPRequestMod1) As BEWSConsultarCIPResponseMod1 Implements IOrdenPago.WSConsultarCIPMod1
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSConsultarCIPMod1(request)
        End Using
    End Function
    Public Function WSEliminarCIPMod1(ByVal request As BEWSElimCIPRequestMod1) As BEWSElimCIPResponseMod1 Implements IOrdenPago.WSEliminarCIPMod1
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSEliminarCIPMod1(request)
        End Using
    End Function
    Public Function WSActualizarCIPMod1(ByVal request As BEWSActualizaCIPRequestMod1) As BEWSActualizaCIPResponseMod1 Implements IOrdenPago.WSActualizarCIPMod1
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSActualizarCIPMod1(request)
        End Using
    End Function
    Public Function WSGenerarCIPMod1(ByVal request As BEWSGenCIPRequestMod1) As BEWSGenCIPResponseMod1 Implements IOrdenPago.WSGenerarCIPMod1
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSGenerarCIPMod1(request)
        End Using
    End Function
#End Region
#End Region

    Public Function ObtenerCIPInfoByUrLServicio(ByVal request As BEObtCIPInfoByUrLServRequest) As BEObtCIPInfoByUrLServResponse Implements EmsambladoComun.IOrdenPago.ObtenerCIPInfoByUrLServicio
        Using oblOrdenPago As New BLOrdenPago
            Return oblOrdenPago.ObtenerCIPInfoByUrLServicio(request)
        End Using
    End Function

    ''19/11/2010 - Walter Rojas
    Public Function ConsultarOPsByServicio(ByVal oBEPagoServicio As BEPagoServicio) As List(Of BEPagoServicio) Implements IOrdenPago.ConsultarOPsByServicio
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOPsByServicio(oBEPagoServicio)
    End Function
    '19/11/2010 - Walter Rojas
    Public Function ConsultarOPsByCIPs(ByVal oBEPagoServicio As BEPagoServicio) As List(Of BEPagoServicio) Implements IOrdenPago.ConsultarOPsByCIPs
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOPsByCIPs(oBEPagoServicio)
    End Function
    Public Function ObtenerOrdenPagCabeceraPorNumeroOrden(ByVal numeroOrden As String) As BEOrdenPago Implements IOrdenPago.ObtenerOrdenPagCabeceraPorNumeroOrden
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ObtenerOrdenPagCabeceraPorNumeroOrden(numeroOrden)
    End Function

    Public Function ConsultarDetalleOrdenPagoPorIdOrdenPago(ByVal idOrdenPago As Integer) As List(Of BEDetalleOrdenPago) Implements IOrdenPago.ConsultarDetalleOrdenPagoPorIdOrdenPago
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarDetalleOrdenPagoPorIdOrdenPago(idOrdenPago)
    End Function

    Public Function ConsultarHistoricoMovimientosporIdOrden(ByVal idOrdenPago As Integer) As List(Of BEMovimiento) Implements IOrdenPago.ConsultarHistoricoMovimientosporIdOrden
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarHistoricoMovimientosporIdOrden(idOrdenPago)
    End Function

    '<add Peru.Com FaseIII>
    Public Function ConsultarSolicitudPagoPorToken(ByVal obeSolicitudPago As BESolicitudPago) As BESolicitudPago Implements IOrdenPago.ConsultarSolicitudPagoPorToken
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarSolicitudPagoPorToken(obeSolicitudPago)
    End Function
    Public Function GenerarOrdenPagoFromSolicitud(ByVal obeSolicitudPago As BESolicitudPago) As BEOrdenPago Implements IOrdenPago.GenerarOrdenPagoFromSolicitud
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.GenerarOrdenPagoFromSolicitud(obeSolicitudPago)
    End Function

    Public Function ConsultarSolicitudXMLPorID(ByVal obeSolicitudPago As BESolicitudPago) As String Implements IOrdenPago.ConsultarSolicitudXMLPorID
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarSolicitudXMLPorID(obeSolicitudPago)
    End Function

    Public Function RealizarProcesoExpiracionSolicitudPago() As Integer Implements EmsambladoComun.IOrdenPago.RealizarProcesoExpiracionSolicitudPago
        Return CType(BusinessLogicObject, SPE.Negocio.BLOrdenPago).RealizarProcesoExpiracionSolicitudPago()
    End Function

#Region "Devoluciones"
    Public Function RegistrarDevolucion(ByVal oBEDevolucion As BEDevolucion) As BEDevolucion Implements IOrdenPago.RegistrarDevolucion
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.RegistrarDevolucion(oBEDevolucion)
    End Function
    Public Function ConsultarDevoluciones(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase) Implements IOrdenPago.ConsultarDevoluciones
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarDevoluciones(oBEDevolucion)
    End Function
    Public Function ActualizarDevolucion(ByVal oBEDevolucion As BEDevolucion) As Int64 Implements IOrdenPago.ActualizarDevolucion
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ActualizarDevolucion(oBEDevolucion)
    End Function
    'DevolucionTelefonica
    Public Function ConsultarCipDevolucion(ByVal NroCip As Int64, ByVal IdUsuario As Int32) As BEOrdenPago Implements IOrdenPago.ConsultarCipDevolucion
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarCipDevolucion(NroCip, IdUsuario)
    End Function

    Public Function ObtenerArchivoDevolucion(ByVal Archivo As String) As Byte() Implements IOrdenPago.ObtenerArchivoDevolucion
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ObtenerArchivoDevolucion(Archivo)
    End Function

    Public Function ConsultarDevolucionesExcel(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase) Implements IOrdenPago.ConsultarDevolucionesExcel
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarDevolucionesExcel(oBEDevolucion)
    End Function

#End Region

    Public Function ActualizarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64 Implements IOrdenPago.ActualizarCIP
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ActualizarCIP(oBEOrdenPago)
    End Function
    Public Function EliminarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64 Implements IOrdenPago.EliminarCIP
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.EliminarCIP(oBEOrdenPago)
    End Function
    Public Function ConsultarOrdenPagoPorIdOrdenYIdRepresentante(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago Implements IOrdenPago.ConsultarOrdenPagoPorIdOrdenYIdRepresentante
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoPorIdOrdenYIdRepresentante(obeOrdenPago)
    End Function
    Public Function EliminarCIPWeb(ByVal oBEOrdenPago As BEOrdenPago) As Int64 Implements IOrdenPago.EliminarCIPWeb
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.EliminarCIPWeb(oBEOrdenPago)
    End Function
#Region "Saga"
    Public Function WSConsultarSolicitudPagov2(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse Implements IOrdenPago.WSConsultarSolicitudPagov2
        Using oblOrdenPago As New BLOrdenPago()
            Return oblOrdenPago.WSConsultarSolicitudPagov2(request)
        End Using
    End Function
#End Region
#Region "ToGenerado"
    Public Function ConsultarOrdenPagoPorIdSoloGenYExp(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago Implements IOrdenPago.ConsultarOrdenPagoPorIdSoloGenYExp
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ConsultarOrdenPagoPorIdSoloGenYExp(obeOrdenPago)
    End Function

    Public Function ActualizarCIPDeExpAGen(ByVal oBEOrdenPago As BEOrdenPago) As Int64 Implements IOrdenPago.ActualizarCIPDeExpAGen
        Dim oblOrdenPago As New BLOrdenPago
        Return oblOrdenPago.ActualizarCIPDeExpAGen(oBEOrdenPago)
    End Function

#End Region

#Region "ExtornarCIP"

    Public Function ExtornarCIP(ByVal nroCIP As String, ByVal tipoExtorno As Integer) As Int64 Implements IOrdenPago.ExtornarCIP
        Dim oblOP As New BLOrdenPago
        Return oblOP.ExtornarCIP(nroCIP, tipoExtorno)
    End Function

#End Region

#Region "Messaging"
    Public Function RegisterCellPhoneNumber(ByVal messaging As Entidades.BEMobileMessaging) As Integer Implements EmsambladoComun.IOrdenPago.RegisterCellPhoneNumber
        Dim ordenPago As New BLOrdenPago
        Return ordenPago.RegisterCellPhoneNumber(messaging)
    End Function

    Public Function ValidateCipExpiration(ByVal paymentOrderId As Long) As String Implements EmsambladoComun.IOrdenPago.ValidateCipExpiration
        Dim ordenPago As New BLOrdenPago
        Return ordenPago.ValidateCipExpiration(paymentOrderId)
    End Function

    Public Function GetMessageSms(smsTemplateId As Integer) As String Implements IOrdenPago.GetMessageSms
        Dim ordenPago As New BLOrdenPago
        Return ordenPago.GetMessageSms(smsTemplateId)
    End Function
#End Region
End Class