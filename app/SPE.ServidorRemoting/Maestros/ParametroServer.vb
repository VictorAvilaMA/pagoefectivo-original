Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ParametroServer
    Inherits MarshalByRefObject
    Implements IParametro
    '
    Public Overrides Function InitializeLifetimeService() As Object

        Return Nothing

    End Function

    Public Function ConsultarParametroPorCodigoGrupo(ByVal CodigoGrupo As String) As List(Of BEParametro) Implements IParametro.ConsultarParametroPorCodigoGrupo
        '
        Dim objParametro As New BLParametro
        Return objParametro.ConsultarParametroPorCodigoGrupo(CodigoGrupo)
        '
    End Function
    '
    Public Function ListarVersionServicio() As List(Of BEParametro) Implements IParametro.ListarVersionServicio
        '
        Dim objParametro As New BLParametro
        Return objParametro.ListarVersionServicio()
        '
    End Function
End Class
