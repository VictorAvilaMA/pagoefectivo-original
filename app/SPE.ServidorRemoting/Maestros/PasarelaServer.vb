Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class PasarelaServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IPasarela

    Public Function GenerarPasarela(ByVal CodServicio As String, ByVal DtosEncriptados As String) As Entidades.BEGenPasResponse _
    Implements EmsambladoComun.IPasarela.GenerarPasarela

        Using oblPasarela As New BLPasarela
            Return oblPasarela.GenerarPasarela(CodServicio, DtosEncriptados)
        End Using

    End Function
End Class
