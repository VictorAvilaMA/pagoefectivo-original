Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ServicioComunServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.IServicioComun


    Public Function ConsultarOPsConciliadas() As System.Data.DataSet Implements EmsambladoComun.IServicioComun.ConsultarOPsConciliadas
        Return New SPE.Negocio.BLServicioComun().ConsultarOPsConciliadas()
    End Function

    Public Function ActualizarOPAMigradoRecaudacion1(ByVal pidOrdenes() As Int64) As Integer Implements EmsambladoComun.IServicioComun.ActualizarOPAMigradoRecaudacion
        Return New SPE.Negocio.BLServicioComun().ActualizarOPAMigradoRecaudacion(pidOrdenes)
    End Function

End Class
