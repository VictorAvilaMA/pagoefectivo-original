﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ServicioInstitucionServer
    Inherits MarshalByRefObject
    Implements IServicioInstitucion


    Public Overrides Function InitializeLifetimeService() As Object
        '
        Return Nothing
        '
    End Function

    Public Function ProcesarArchivoServicioInstitucion(request As Entidades.BEServicioInstitucionRequest) As BEServicioInstitucion Implements EmsambladoComun.IServicioInstitucion.ProcesarArchivoServicioInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ProcesarArchivoServicioInstitucion(request)
        End Using
    End Function


    Public Function ValidarCarga(request As Entidades.BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicioInstitucion.ValidarCarga
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ValidarCarga(request)
        End Using
    End Function


    Public Function MostrarResumenArchivoServicioInstitucion(request As Entidades.BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicioInstitucion.MostrarResumenArchivoServicioInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.MostrarResumenArchivoServicioInstitucion(request)
        End Using
    End Function


    Public Function ConsultarInstitucionCIP(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicioInstitucion.ConsultarInstitucionCIP
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarInstitucionCIP(oBEServicioInstitucionRequest)
        End Using
    End Function

    Public Function ConsultarDetallePagoServicioInstitucion(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicioInstitucion.ConsultarDetallePagoServicioInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarDetallePagoServicioInstitucion(oBEServicioInstitucionRequest)
        End Using
    End Function


    Public Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicioInstitucion.ActualizarDocumentosCIP
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ActualizarDocumentosCIP(request)
        End Using
    End Function

    Public Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicioInstitucion.ActualizarDocumentoInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ActualizarDocumentoInstitucion(request)
        End Using
    End Function

    Public Function ConsultarArchivosDescarga(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicioInstitucion.ConsultarArchivosDescarga
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarArchivosDescarga(oBEServicioInstitucionRequest)
        End Using
    End Function

    Public Function ConsultarDetalleArchivoDescarga(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicioInstitucion.ConsultarDetalleArchivoDescarga
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarDetalleArchivoDescarga(oBEServicioInstitucionRequest)
        End Using
    End Function

End Class
