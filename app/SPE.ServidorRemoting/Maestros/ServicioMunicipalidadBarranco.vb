﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ServicioMunicipalidadBarrancoServer
    Inherits MarshalByRefObject
    Implements IServicioMunicipalidadBarranco


    Public Overrides Function InitializeLifetimeService() As Object
        '
        Return Nothing
        '
    End Function

    Public Function ProcesarArchivoServicioMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As BEServicioMunicipalidadBarranco Implements EmsambladoComun.IServicioMunicipalidadBarranco.ProcesarArchivoServicioMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ProcesarArchivoServicioMunicipalidadBarranco(request)
        End Using
    End Function


    Public Function ValidarCargaMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicioMunicipalidadBarranco.ValidarCargaMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ValidarCargaMunicipalidadBarranco(request)
        End Using
    End Function


    Public Function MostrarResumenArchivoServicioMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicioMunicipalidadBarranco.MostrarResumenArchivoServicioMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.MostrarResumenArchivoServicioMunicipalidadBarranco(request)
        End Using
    End Function


    Public Function ConsultarMunicipalidadBarrancoCIP(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicioMunicipalidadBarranco.ConsultarMunicipalidadBarrancoCIP
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarMunicipalidadBarrancoCIP(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function

    Public Function ConsultarDetallePagoServicioMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicioMunicipalidadBarranco.ConsultarDetallePagoServicioMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarDetallePagoServicioMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function


    Public Function ActualizarDocumentosCIPMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicioMunicipalidadBarranco.ActualizarDocumentosCIPMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ActualizarDocumentosCIPMunicipalidadBarranco(request)
        End Using
    End Function

    Public Function ActualizarDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicioMunicipalidadBarranco.ActualizarDocumentoMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ActualizarDocumentoMunicipalidadBarranco(request)
        End Using
    End Function

    Public Function ConsultarArchivosDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicioMunicipalidadBarranco.ConsultarArchivosDescargaMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarArchivosDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function

    Public Function ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oBEServiciMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicioMunicipalidadBarranco.ConsultarDetalleArchivoDescargaMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oBEServiciMunicipalidadBarrancoRequest)
        End Using
    End Function

End Class
