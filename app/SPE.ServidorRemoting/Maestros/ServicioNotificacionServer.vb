Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ServicioNotificacionServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerBase
    Implements IServicioNotificacion

    'Public Function RealizaNotificacion() As Integer Implements EmsambladoComun.IServicioNotificacion.RealizaNotificacion

    'End Function

    Function ConsultarNotificacionesListasANotificar() As BEConsultServNotifResponse Implements EmsambladoComun.IServicioNotificacion.ConsultarNotificacionesListasANotificar
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarNotificacionesListasANotificar()
        End Using
    End Function

    Function ConsultarNotificacionesPendientes() As List(Of BEServicioNotificacion) Implements EmsambladoComun.IServicioNotificacion.ConsultarNotificacionesPendientes
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarNotificacionesPendientes()
        End Using
    End Function

    Public Function ActualizarEstadoNotificaciones(ByVal listaNotificacionesCorrectas As List(Of BEServicioNotificacion), ByVal listaNotificacionesErroneas As List(Of BEServicioNotificacion)) As Integer Implements EmsambladoComun.IServicioNotificacion.ActualizarEstadoNotificaciones
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ActualizarEstadoNotificaciones(listaNotificacionesCorrectas, listaNotificacionesErroneas)
        End Using
    End Function

    Public Function ConsultarServicioNotificacionPorIdEstado(ByVal idEstado As Integer) As List(Of BEServicioNotificacion) Implements EmsambladoComun.IServicioNotificacion.ConsultarServicioNotificacionPorIdEstado
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarServicioNotificacionPorIdEstado(idEstado)
        End Using
    End Function
    'SagaInicio

    Public Function ConsultarNotificacionesListasANotificarSaga() As Entidades.BEConsultServNotifResponse Implements EmsambladoComun.IServicioNotificacion.ConsultarNotificacionesListasANotificarSaga
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarNotificacionesListasANotificarSaga()
        End Using
    End Function
    Public Function ConsultarDatosParaServicioSaga(ByVal IdOrdenPago As Int64) As BEServicioNotificacion Implements EmsambladoComun.IServicioNotificacion.ConsultarDatosParaServicioSaga
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarDatosParaServicioSaga(IdOrdenPago)
        End Using
    End Function

    Public Function ConsultarOrdenesPagoSaga() As List(Of BEServicioNotificacion) Implements EmsambladoComun.IServicioNotificacion.ConsultarOrdenesPagoSaga
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarOrdenesPagoSaga()
        End Using
    End Function

    Public Function RegistrarLogConsumoWSSagaRequest(ByVal obeLogWSSagaRequest As Entidades.BELogWebServiceSagaRequest) As Integer Implements EmsambladoComun.IServicioNotificacion.RegistrarLogConsumoWSSagaRequest
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.RegistrarLogConsumoWSSagaRequest(obeLogWSSagaRequest)
        End Using
    End Function

    Public Function RegistrarLogConsumoWSSagaResponse(ByVal obeLogWSSagaResponse As Entidades.BELogWebServiceSagaResponse) As Integer Implements EmsambladoComun.IServicioNotificacion.RegistrarLogConsumoWSSagaResponse
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.RegistrarLogConsumoWSSagaResponse(obeLogWSSagaResponse)
        End Using
    End Function


    'SagaFin
    'Ripley

    Public Function ConsultarNotificacionesListasANotificarRipley() As Entidades.BEConsultServNotifResponse Implements EmsambladoComun.IServicioNotificacion.ConsultarNotificacionesListasANotificarRipley
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarNotificacionesListasANotificarRipley()
        End Using
    End Function
    Public Function ConsultarDatosParaServicioRipley(ByVal IdOrdenPago As Int64) As BEServicioNotificacion Implements EmsambladoComun.IServicioNotificacion.ConsultarDatosParaServicioRipley
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarDatosParaServicioRipley(IdOrdenPago)
        End Using
    End Function

    Public Function ConsultarOrdenesPagoRipley() As List(Of BEServicioNotificacion) Implements EmsambladoComun.IServicioNotificacion.ConsultarOrdenesPagoRipley
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarOrdenesPagoRipley()
        End Using
    End Function

    Public Function RegistrarLogConsumoWSRipleyRequest(ByVal obeLogWSRipleyRequest As Entidades.BELogWebServiceRipleyRequest) As Integer Implements EmsambladoComun.IServicioNotificacion.RegistrarLogConsumoWSRipleyRequest
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.RegistrarLogConsumoWSRipleyRequest(obeLogWSRipleyRequest)
        End Using
    End Function

    Public Function RegistrarLogConsumoWSRipleyResponse(ByVal obeLogWSRipleyResponse As Entidades.BELogWebServiceRipleyResponse) As Integer Implements EmsambladoComun.IServicioNotificacion.RegistrarLogConsumoWSRipleyResponse
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.RegistrarLogConsumoWSRipleyResponse(obeLogWSRipleyResponse)
        End Using
    End Function

    'Fin Ripley


    'Sodimac Inicio
    Public Function ActualizarEstadoNotificacionReenvioSodimac(ByVal obeBEServicioNotificacion As BEServicioNotificacion) As Integer Implements EmsambladoComun.IServicioNotificacion.ActualizarEstadoNotificacionReenvioSodimac
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ActualizarEstadoNotificacionReenvioSodimac(obeBEServicioNotificacion)
        End Using
    End Function
    
    Public Function ConsultarServicioNotificacionReenvioSodimac() As List(Of BEServicioNotificacion) Implements EmsambladoComun.IServicioNotificacion.ConsultarServicioNotificacionReenvioSodimac
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarServicioNotificacionReenvioSodimac()
        End Using
    End Function
    
    Public Function ConsultarNotificacionesListasANotificarSodimac() As Entidades.BEConsultServNotifResponse Implements EmsambladoComun.IServicioNotificacion.ConsultarNotificacionesListasANotificarSodimac
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarNotificacionesListasANotificarSodimac()
        End Using
    End Function
    Public Function ConsultarDatosParaServicioSodimac(ByVal IdOrdenPago As Int64) As BEServicioNotificacion Implements EmsambladoComun.IServicioNotificacion.ConsultarDatosParaServicioSodimac
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarDatosParaServicioSodimac(IdOrdenPago)
        End Using
    End Function

    Public Function ConsultarOrdenesPagoSodimac() As List(Of BEServicioNotificacion) Implements EmsambladoComun.IServicioNotificacion.ConsultarOrdenesPagoSodimac
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.ConsultarOrdenesPagoSodimac()
        End Using
    End Function

    Public Function RegistrarLogConsumoWSSodimacRequest(ByVal obeLogWSSodimacRequest As Entidades.BELogWebServiceSodimacRequest) As Integer Implements EmsambladoComun.IServicioNotificacion.RegistrarLogConsumoWSSodimacRequest
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.RegistrarLogConsumoWSSodimacRequest(obeLogWSSodimacRequest)
        End Using
    End Function

    Public Function RegistrarLogConsumoWSSodimacResponse(ByVal obeLogWSSodimacResponse As Entidades.BELogWebServiceSodimacResponse) As Integer Implements EmsambladoComun.IServicioNotificacion.RegistrarLogConsumoWSSodimacResponse
        Using oblServicioNotificacion As New BLServicioNotificacion()
            Return oblServicioNotificacion.RegistrarLogConsumoWSSodimacResponse(obeLogWSSodimacResponse)
        End Using
    End Function
    'Sodimac Fin

End Class
