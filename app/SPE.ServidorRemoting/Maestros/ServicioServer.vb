Imports System
Imports System.Collections.Generic
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports _3Dev.FW.ServidorRemoting
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ServicioServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.IServicio

    Public Function ConsultarServicioFormularioUrl(ByVal IdServicio As Integer, ByVal URL As String) As SPE.Entidades.BEServicio Implements EmsambladoComun.IServicio.ConsultarServicioFormularioUrl
        Dim obeServicio As New BLServicio
        Return obeServicio.ConsultarServicioFormularioUrl(IdServicio, URL)
    End Function

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLServicio()
    End Function

    Public ReadOnly Property BLServicio() As BLServicio
        Get
            Return CType(BusinessLogicObject, BLServicio)
        End Get
    End Property

    Public Function ConsultarServicioPorUrl(ByVal parametro As Object) As Entidades.BEServicio Implements EmsambladoComun.IServicio.ConsultarServicioPorUrl
        Dim obeServicio As New BLServicio
        Return obeServicio.ConsultarServicioPorUrl(parametro)
    End Function

    Public Function ConsultarPagoServicios(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago) Implements IServicio.ConsultarPagoServicios
        Dim obeServicio As New BLServicio
        Return obeServicio.ConsultarPagoServicios(obeOrdenPago)
    End Function

    Public Function ConsultarPagoServiciosNoRepresentante(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago) Implements IServicio.ConsultarPagoServiciosNoRepresentante
        Dim obeServicio As New BLServicio
        Return obeServicio.ConsultarPagoServiciosNoRepresentante(obeOrdenPago)
    End Function

    Public Function ConsultarServiciosPorIdUsuario(ByVal obeServicio As BEServicio) As List(Of BEServicio) Implements IServicio.ConsultarServiciosPorIdUsuario
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarServiciosPorIdUsuario(obeServicio)
    End Function

#Region "Configuración de Servicios"
    Public Function ConsultarContratoXMLDeServicios(ByVal codigoAcceso As String) As String Implements IServicio.ConsultarContratoXMLDeServicios
        Return BLServicio.ConsultarContratoXMLDeServicios(codigoAcceso)
    End Function

    Public Function GuardarContratoXMLDeServicios(ByVal codigoAcceso As String, ByVal contratoXml As String) As String Implements IServicio.GuardarContratoXMLDeServicios
        Return BLServicio.GuardarContratoXMLDeServicios(codigoAcceso, contratoXml)
    End Function

#End Region

    Public Function ConsultarServicioPorAPI(ByVal cAPI As String) As BEServicio Implements IServicio.ConsultarServicioPorAPI
        Return BLServicio.ConsultarServicioPorAPI(cAPI)
    End Function
    Public Function ValidarServicioPorAPIYClave(ByVal cAPI As String, ByVal cClave As String) As Boolean Implements IServicio.ValidarServicioPorAPIYClave
        Return BLServicio.ValidarServicioPorAPIYClave(cAPI, cClave)
    End Function

    Public Function ConsultarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements IServicio.ConsultarServicioxEmpresa
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarServicioxEmpresa(be)
    End Function

    Public Function ActualizaServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements IServicio.ActualizaServicioxEmpresa
        Dim oblServicio As New BLServicio
        Return oblServicio.ActualizaServicioxEmpresa(be)
    End Function
    Public Function ConsultarTransaccionesxServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio Implements IServicio.ConsultarTransaccionesxServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarTransaccionesxServicio(be, objFechaAl, objFechaDel)
    End Function

    Public Function ConsultarTransaccionesxServicioLote(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio Implements IServicio.ConsultarTransaccionesxServicioLote
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarTransaccionesxServicioLote(be, objFechaAl, objFechaDel)
    End Function

    '<add Peru.Com FaseIII>
    Public Function UpdateServicioContratoXML(ByVal oBEServicio As BEServicio) As Boolean Implements EmsambladoComun.IServicio.UpdateServicioContratoXML
        Dim oblServicio As New BLServicio
        Return oblServicio.UpdateServicioContratoXML(oBEServicio)
    End Function
    'Cuenta Banco
    Public Function RegistrarAsociacionCuentaServicio(ByVal be As BEServicioBanco) As Long Implements EmsambladoComun.IServicio.RegistrarAsociacionCuentaServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.RegistrarAsociacionCuentaServicio(be)
    End Function
    Public Function ActualizarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements EmsambladoComun.IServicio.ActualizarAsociacionCuentaServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.ActualizarAsociacionCuentaServicio(be)
    End Function
    Public Function ConsultarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements EmsambladoComun.IServicio.ConsultarAsociacionCuentaServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarAsociacionCuentaServicio(be)
    End Function
    '<upd Proc.Tesoreria>
    Public Function ConsultarCodigosServiciosPorBanco(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements EmsambladoComun.IServicio.ConsultarCodigosServiciosPorBanco
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarCodigosServiciosPorBanco(be)
    End Function
    Public Function ObtenerAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BEServicioBanco Implements EmsambladoComun.IServicio.ObtenerAsociacionCuentaServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.ObtenerAsociacionCuentaServicio(be)
    End Function
    Function EliminarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Boolean Implements EmsambladoComun.IServicio.EliminarAsociacionCuentaServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.EliminarAsociacionCuentaServicio(be)
    End Function
    Public Function ObtenerServicioComisionporID(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase Implements EmsambladoComun.IServicio.ObtenerServicioComisionporID
        Dim oblServicio As New BLServicio
        Return oblServicio.ObtenerServicioComisionporID(be)
    End Function

    Public Function ConsultarDetalleTransaccionesxServicio(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia) Implements EmsambladoComun.IServicio.ConsultarDetalleTransaccionesxServicio
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarDetalleTransaccionesxServicio(objbeservicio)
    End Function

    Public Function ConsultarDetalleTransaccionesxServicioSaga(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaSaga) Implements EmsambladoComun.IServicio.ConsultarDetalleTransaccionesxServicioSaga
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarDetalleTransaccionesxServicioSaga(objbeservicio)
    End Function

    Public Function ConsultarDetalleTransaccionesxServicioRipley(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaRipley) Implements EmsambladoComun.IServicio.ConsultarDetalleTransaccionesxServicioRipley
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarDetalleTransaccionesxServicioRipley(objbeservicio)
    End Function

    Public Function ConsultarDetalleTransaccionesPendientes(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia) Implements EmsambladoComun.IServicio.ConsultarDetalleTransaccionesPendientes
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarDetalleTransaccionesPendientes(objbeservicio)
    End Function
    Public Function RegistrarServicioxEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer Implements IServicio.RegistrarServicioxEmpresa
        Dim oblServicio As New BLServicio
        Return oblServicio.RegistrarServicioxEmpresa(be)
    End Function
    Public Function ConsultarServicioPorIdEmpresa(ByVal be As BEServicio) As List(Of BEServicio) Implements EmsambladoComun.IServicio.ConsultarServicioPorIdEmpresa
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarServicioPorIdEmpresa(be)
    End Function
    Public Function ConsultarPorAPIYClave(ByVal CAPI As String, ByVal CClave As String) As Entidades.BEServicio Implements EmsambladoComun.IServicio.ConsultarPorAPIYClave
        Dim oblServicio As New BLServicio
        Return oblServicio.ConsultarPorAPIYClave(CAPI, CClave)
    End Function
    Public Function ConsultarComerciosAfiliados(ByVal visibleEnPortada As Boolean, ByVal proximoAfiliado As Boolean) As List(Of BEServicio) Implements EmsambladoComun.IServicio.ConsultarComerciosAfiliados
        Using oBLServicio As New BLServicio
            Return oBLServicio.ConsultarComerciosAfiliados(visibleEnPortada, proximoAfiliado)
        End Using
    End Function
    Public Function ObtenerServicioPorIdWS(ByVal id As Integer) As BEServicio Implements EmsambladoComun.IServicio.ObtenerServicioPorIdWS
        Using oBLServicio As New BLServicio
            Return oBLServicio.ObtenerServicioPorIdWS(id)
        End Using
    End Function

    Public Function ConsultarServicioPorClasificacion(oBEServicio As BEServicio) As List(Of BEServicio) Implements EmsambladoComun.IServicio.ConsultarServicioPorClasificacion
        Using oBLServicio As New BLServicio
            Return oBLServicio.ConsultarServicioPorClasificacion(oBEServicio)
        End Using
    End Function
    Public Function ObtenerServicioInstitucionPorId(ByVal id As Integer) As BEServicio Implements EmsambladoComun.IServicio.ObtenerServicioInstitucionPorId
        Using oBLServicio As New BLServicio
            Return oBLServicio.ObtenerServicioInstitucionPorId(id)
        End Using
    End Function


#Region "ServicioInstitucion"
    Public Function ProcesarArchivoServicioInstitucion(request As Entidades.BEServicioInstitucionRequest) As BEServicioInstitucion Implements EmsambladoComun.IServicio.ProcesarArchivoServicioInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ProcesarArchivoServicioInstitucion(request)
        End Using
    End Function


    Public Function ValidarCarga(request As Entidades.BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicio.ValidarCarga
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ValidarCarga(request)
        End Using
    End Function


    Public Function MostrarResumenArchivoServicioInstitucion(request As Entidades.BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicio.MostrarResumenArchivoServicioInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.MostrarResumenArchivoServicioInstitucion(request)
        End Using
    End Function


    Public Function ConsultarInstitucionCIP(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicio.ConsultarInstitucionCIP
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarInstitucionCIP(oBEServicioInstitucionRequest)
        End Using
    End Function

    Public Function ConsultarDetallePagoServicioInstitucion(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicio.ConsultarDetallePagoServicioInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarDetallePagoServicioInstitucion(oBEServicioInstitucionRequest)
        End Using
    End Function


    Public Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicio.ActualizarDocumentosCIP
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ActualizarDocumentosCIP(request)
        End Using
    End Function

    Public Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicio.ActualizarDocumentoInstitucion
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ActualizarDocumentoInstitucion(request)
        End Using
    End Function

    Public Function ConsultarArchivosDescarga(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicio.ConsultarArchivosDescarga
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarArchivosDescarga(oBEServicioInstitucionRequest)
        End Using
    End Function

    Public Function ConsultarDetalleArchivoDescarga(oBEServicioInstitucionRequest As BEServicioInstitucionRequest) As System.Collections.Generic.List(Of Entidades.BEServicioInstitucion) Implements EmsambladoComun.IServicio.ConsultarDetalleArchivoDescarga
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarDetalleArchivoDescarga(oBEServicioInstitucionRequest)
        End Using
    End Function

    Public Function ConsultarCIPDocumento(ByVal request As BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicio.ConsultarCIPDocumento
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.ConsultarCIPDocumento(request)
        End Using
    End Function


    Public Function DeterminarIdParametro(request As Entidades.BEServicioInstitucionRequest) As Integer Implements EmsambladoComun.IServicio.DeterminarIdParametro
        Using oBLServicioInstitucion As New BLServicioInstitucion
            Return oBLServicioInstitucion.DeterminarIdParametro(request)
        End Using
    End Function

#End Region

    Public Function ObtenerServicioMunicipalidadBarrancoPorId(ByVal id As Integer) As BEServicio Implements EmsambladoComun.IServicio.ObtenerServicioMunicipalidadBarrancoPorId
        Using oBLServicio As New BLServicio
            Return oBLServicio.ObtenerServicioMunicipalidadBarrancoPorId(id)
        End Using
    End Function

#Region "MunicipalidadBarranco"
    Public Function ProcesarArchivoServicioMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As BEServicioMunicipalidadBarranco Implements EmsambladoComun.IServicio.ProcesarArchivoServicioMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ProcesarArchivoServicioMunicipalidadBarranco(request)
        End Using
    End Function


    Public Function ValidarCargaMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicio.ValidarCargaMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ValidarCargaMunicipalidadBarranco(request)
        End Using
    End Function


    Public Function MostrarResumenArchivoServicioMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicio.MostrarResumenArchivoServicioMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.MostrarResumenArchivoServicioMunicipalidadBarranco(request)
        End Using
    End Function


    Public Function ConsultarInstitucionCIPMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicio.ConsultarMunicipalidadBarrancoCIP
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarMunicipalidadBarrancoCIP(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function

    Public Function ConsultarDetallePagoServicioMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicio.ConsultarDetallePagoServicioMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarDetallePagoServicioMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function

    Public Function ActualizarDocumentosCIPMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicio.ActualizarDocumentosCIPMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ActualizarDocumentosCIPMunicipalidadBarranco(request)
        End Using
    End Function

    Public Function ActualizarDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicio.ActualizarDocumentoMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ActualizarDocumentoMunicipalidadBarranco(request)
        End Using
    End Function

    Public Function ConsultarArchivosDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicio.ConsultarArchivosDescargaMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarArchivosDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function

    Public Function ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest As BEServicioMunicipalidadBarrancoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioMunicipalidadBarranco) Implements EmsambladoComun.IServicio.ConsultarDetalleArchivoDescargaMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
        End Using
    End Function

    Public Function ConsultarCIPDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicio.ConsultarCIPDocumentoMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.ConsultarCIPDocumentoMunicipalidadBarranco(request)
        End Using
    End Function

    Public Function DeterminarIdParametroMunicipalidadBarranco(request As Entidades.BEServicioMunicipalidadBarrancoRequest) As Integer Implements EmsambladoComun.IServicio.DeterminarIdParametroMunicipalidadBarranco
        Using oBLServicioMunicipalidadBarranco As New BLServicioMunicipalidadBarranco
            Return oBLServicioMunicipalidadBarranco.DeterminarIdParametroMunicipalidadBarranco(request)
        End Using
    End Function

#End Region



#Region "Servicio Desconectado"
    Public Function RecuperarDatosServicioDesconectado(ByVal id As Integer) As BEServicio Implements EmsambladoComun.IServicio.RecuperarDatosServicioDesconectado
        Using oBLServicio As New BLServicio
            Return oBLServicio.RecuperarDatosServicioDesconectado(id)
        End Using
    End Function

    Public Function RegistrarOrderIdComercio(ByVal codigo As String) As Integer Implements EmsambladoComun.IServicio.RegistrarOrderIdComercio
        Dim oblServicio As New BLServicio
        Return oblServicio.RegistrarOrderIdComercio(codigo)
    End Function

#End Region

    Public Function RecuperarDatosProductoServicio(obeServicio As Entidades.BEServicioProductoRequest) As System.Collections.Generic.List(Of Entidades.BEServicioProducto) Implements EmsambladoComun.IServicio.RecuperarDatosProductoServicio
        Using oBLServicio As New BLServicio
            Return oBLServicio.RecuperarDatosProductoServicio(obeServicio)
        End Using
    End Function

    Public Function RecuperarDetalleProductoServicio(obeServicio As Entidades.BEServicioProductoRequest) As BEServicioProducto Implements EmsambladoComun.IServicio.RecuperarDetalleProductoServicio
        Using oBLServicio As New BLServicio
            Return oBLServicio.RecuperarDetalleProductoServicio(obeServicio)
        End Using
    End Function




End Class


