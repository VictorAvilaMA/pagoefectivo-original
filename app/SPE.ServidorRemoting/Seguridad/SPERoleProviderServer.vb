Imports System.ServiceModel.Activation
Namespace Seguridad

    Public Class SPERoleProviderServer
        Inherits _3Dev.FW.ServidorRemoting.Seguridad.SqlRoleProviderServer
        Implements SPE.EmsambladoComun.Seguridad.IRoleProvider
        Public Overrides Function GetConstructorBusinessProviderObject() As _3Dev.FW.Negocio.Seguridad.BLSqlRoleProvider
            Return New SPE.Negocio.Seguridad.BLRoleProvider
        End Function
        Public Overrides Function InitializeLifetimeService() As Object
            Return Nothing
        End Function

    End Class
End Namespace