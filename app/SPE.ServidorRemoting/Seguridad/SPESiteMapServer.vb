Imports System.ServiceModel.Activation

Namespace Seguridad

    Public Class SPESiteMapServer
        Inherits _3Dev.FW.ServidorRemoting.Seguridad.CustomSiteMapServer
        Implements SPE.EmsambladoComun.Seguridad.ISiteMapProvider
        Public Overrides Function GetConstructorBusinessProviderObject() As _3Dev.FW.Negocio.Seguridad.BLCustomSiteMapProvider
            Return New SPE.Negocio.Seguridad.BLSiteMapProvider
        End Function

        Public Overrides Function InitializeLifetimeService() As Object
            Return Nothing
        End Function
    End Class
End Namespace