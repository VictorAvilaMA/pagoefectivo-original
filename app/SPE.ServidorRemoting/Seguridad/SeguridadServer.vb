Imports SPE.Negocio

Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class SeguridadServer
    Inherits MarshalByRefObject
    Implements SPE.EmsambladoComun.ISeguridad



    Public Function ValidarServicioClaveAPI(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean Implements EmsambladoComun.ISeguridad.ValidarServicioClaveAPI
        Return (New BLSeguridad()).ValidarServicioClaveAPI(claveAPI, claveSecreta)
    End Function

    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function


    '<add Peru.Com FaseIII>
    'Public Function CifrarMensaje(ByVal mensajeACifrar As String, ByVal codEntidadContraparte As String) As String() Implements EmsambladoComun.ISeguridad.CifrarMensaje
    '    Return (New BLSeguridad()).CifrarMensaje(mensajeACifrar, codEntidadContraparte)
    'End Function
    'Public Function DesCifrarMensaje(ByVal mensajeADescifrar As String, ByVal keyCifrado As String, ByVal codEntidad As String) As String Implements EmsambladoComun.ISeguridad.DesCifrarMensaje
    '    Return (New BLSeguridad()).DesCifrarMensaje(mensajeADescifrar, keyCifrado, codEntidad)
    'End Function
    Public Function GenerateKeyPrivatePublicSPE() As Boolean Implements EmsambladoComun.ISeguridad.GenerateKeyPrivatePublicSPE
        Return (New BLSeguridad()).GenerateKeyPrivatePublicSPE()
    End Function
    Public Function GetPublicKey(ByVal codServicio As String) As String Implements EmsambladoComun.ISeguridad.GetPublicKey
        Return (New BLSeguridad()).GetPublicKey(codServicio)
    End Function
    Public Function SaveKeyPublic(ByVal idServicio As Int32, ByVal keyPublic As String) As Boolean Implements EmsambladoComun.ISeguridad.SaveKeyPublic
        Return (New BLSeguridad()).SaveKeyPublic(idServicio, keyPublic)
    End Function
    Public Function GetPublicKeySPE() As String Implements EmsambladoComun.ISeguridad.GetPublicKeySPE
        Return (New BLSeguridad()).GetPublicKeySPE()
    End Function
    Public Function GetPrivateKeySPE() As String Implements EmsambladoComun.ISeguridad.GetPrivateKeySPE
        Return (New BLSeguridad()).GetPrivateKeySPE()
    End Function

End Class
