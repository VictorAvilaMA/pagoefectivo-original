Imports System
Imports System.Diagnostics
Imports System.Runtime.Remoting
Imports SPE.EmsambladoComun
Imports _3Dev.FW.ServidorRemoting


Public Class Servicios
    Public Sub New()

    End Sub
    Shared Sub StartUpserver()

        Dim objMemberShipServer As New Seguridad.SPEMemberShipServer
        RemotingServices.Marshal(objMemberShipServer, _3Dev.FW.EmsambladoComun.ServiciosConocidos.CustomMembership)

        Dim objRoleProviderServer As New Seguridad.SPERoleProviderServer
        RemotingServices.Marshal(objRoleProviderServer, _3Dev.FW.EmsambladoComun.ServiciosConocidos.SqlRoleProviderServices)

        Dim objSiteMapServer As New Seguridad.SPESiteMapServer
        RemotingServices.Marshal(objSiteMapServer, _3Dev.FW.EmsambladoComun.ServiciosConocidos.CustomSiteMapProviderServices)

        Dim objUsuarioServer As New Seguridad.SPEUsuarioServer
        'RemotingServices.Marshal(objUsuarioServer, _3Dev.FW.EmsambladoComun.ServiciosConocidos.UsuarioServices)
        RemotingServices.Marshal(objUsuarioServer, NombreServiciosConocidos.IUsuario)
        '
        '
        'Agencia Recaudadora
        Dim objAgenciaRecaudadoraServer As New AgenciaRecaudadoraServer
        RemotingServices.Marshal(objAgenciaRecaudadoraServer, NombreServiciosConocidos.IAgenciaRecaudadora)

        'Cliente
        Dim objCliente As New ClienteServer
        RemotingServices.Marshal(objCliente, NombreServiciosConocidos.ICliente)

        'Empresa
        Dim objEmpresa As New EmpresaContratanteServer
        RemotingServices.Marshal(objEmpresa, NombreServiciosConocidos.IEmpresaContratante)

        'Parametro
        Dim ojbParametro As New ParametroServer
        RemotingServices.Marshal(ojbParametro, NombreServiciosConocidos.IParametro)

        'Servicio
        Dim objServicio As New ServicioServer
        RemotingServices.Marshal(objServicio, NombreServiciosConocidos.IServicio)

        'Ubigeo
        Dim objUbigeo As New UbigeoServer
        RemotingServices.Marshal(objUbigeo, NombreServiciosConocidos.IUbigeo)

        'Representante
        Dim objRepresentante As New RepresentanteServer
        RemotingServices.Marshal(objRepresentante, NombreServiciosConocidos.IRepresentante)

        'Código de Identificación de Pago
        Dim objOrdenPago As New OrdenPagoServer
        RemotingServices.Marshal(objOrdenPago, NombreServiciosConocidos.IOrdenPago)

        'Comun
        Dim objComun As New ComunServer
        RemotingServices.Marshal(objComun, NombreServiciosConocidos.IComun)

        'Caja
        Dim objCaja As New CajaServer
        RemotingServices.Marshal(objCaja, NombreServiciosConocidos.ICaja)

        'Email
        Dim objEmail As New EmailServer
        RemotingServices.Marshal(objEmail, NombreServiciosConocidos.IEmail)

        'FASE II
        '--------------------------------------------------------------------------------------
        'Agencia Bancaria
        Dim objAgenciaBancariaServer As New AgenciaBancariaServer
        RemotingServices.Marshal(objAgenciaBancariaServer, NombreServiciosConocidos.IAgenciaBancaria)

        'Operador
        Dim objOperador As New OperadorServer
        RemotingServices.Marshal(objOperador, NombreServiciosConocidos.IOperador)

        'Establecimiento
        Dim objEstablecimiento As New EstablecimientoServer
        RemotingServices.Marshal(objEstablecimiento, NombreServiciosConocidos.IEstablecimiento)

        'Comercio
        Dim objComercio As New ComercioServer
        RemotingServices.Marshal(objComercio, NombreServiciosConocidos.IComercio)


        'Banco
        Dim objBancoServer As New BancoServer
        RemotingServices.Marshal(objBancoServer, NombreServiciosConocidos.IBanco)

        'JefeProducto
        Dim objJefeProducto As New JefeProductoServer
        RemotingServices.Marshal(objJefeProducto, NombreServiciosConocidos.IJefeProducto)

        'FTPArchivo
        Dim objFTPArchivoServer As New FTPArchivoServer
        RemotingServices.Marshal(objFTPArchivoServer, NombreServiciosConocidos.IFTPArchivo)

        'ServicioComunServer
        Dim objServicioComunServer As New ServicioComunServer()
        RemotingServices.Marshal(objServicioComunServer, NombreServiciosConocidos.IServicioComun)

        'SeguridadServer
        Dim objSeguridadServer As New SeguridadServer()
        RemotingServices.Marshal(objSeguridadServer, NombreServiciosConocidos.ISeguridad)

        'ServicioNotificacionServer
        Dim objServicioNotificacionServer As New ServicioNotificacionServer()
        RemotingServices.Marshal(objServicioNotificacionServer, NombreServiciosConocidos.IServicioNotificacion)

        'PlantillaServer
        Dim objPlantillaServer As New PlantillaServer()
        RemotingServices.Marshal(objPlantillaServer, NombreServiciosConocidos.IPlantilla)

        '**************************************************************************************************************
        'wjra 14/012011 
        Dim objConciliacionServer As New ConciliacionServer()
        RemotingServices.Marshal(objConciliacionServer, NombreServiciosConocidos.IConciliacion)
        '**************************************************************************************************************

        'FullCarga 31/05/2011
        Dim objPuntoVentaServer As New PuntoVentaServer()
        RemotingServices.Marshal(objPuntoVentaServer, NombreServiciosConocidos.IPuntoVenta)
    End Sub

End Class




