﻿Imports System.IO
Imports Amazon
Imports Amazon.S3
Imports Amazon.S3.IO
Imports Amazon.S3.Model
Imports Amazon.S3.Transfer
Imports System.Configuration

Public Class AmazonS3

    Private Shared _s3Client As AmazonS3Client = Nothing

    Private ReadOnly _s3AccessKey As String = ConfigurationManager.AppSettings("FORM_S3_ACCESS_KEY")
    Private ReadOnly _s3SecretAccessKey As String = ConfigurationManager.AppSettings("FORM_S3_SECRET_ACCESS_KEY")
    Private ReadOnly _s3Region As String = ConfigurationManager.AppSettings("FORM_S3_REGION")
    Private ReadOnly _s3BucketName As String = ConfigurationManager.AppSettings("FORM_S3_BUCKET_NAME")
    Private ReadOnly _s3DirectoryPath As String = ConfigurationManager.AppSettings("FORM_S3_DIRECTORY_PATH")

    Public Sub New()
        Dim s3Config As New AmazonS3Config
        s3Config.RegionEndpoint = RegionEndpoint.GetBySystemName(_s3Region)

        _s3Client = New AmazonS3Client(_s3AccessKey, _s3SecretAccessKey, s3Config)
    End Sub

    Public Sub New(s3AccessKey As String, s3SecretAccessKey As String, s3Region As String)
        _s3AccessKey = s3AccessKey
        _s3SecretAccessKey = s3SecretAccessKey
        _s3Region = s3Region

        Dim s3Config As New AmazonS3Config
        s3Config.RegionEndpoint = RegionEndpoint.GetBySystemName(_s3Region)

        _s3Client = New AmazonS3Client(s3AccessKey, s3SecretAccessKey, s3Config)
    End Sub

    Public Sub New(s3AccessKey As String, s3SecretAccessKey As String, s3BucketName As String, s3DirectoryPath As String)
        _s3AccessKey = s3AccessKey
        _s3SecretAccessKey = s3SecretAccessKey
        _s3BucketName = s3BucketName
        _s3DirectoryPath = s3DirectoryPath

        Dim s3Config As New AmazonS3Config
        s3Config.RegionEndpoint = RegionEndpoint.GetBySystemName(_s3Region)

        _s3Client = New AmazonS3Client(s3AccessKey, s3SecretAccessKey, s3Config)
    End Sub

    Public Sub New(s3AccessKey As String, s3SecretAccessKey As String, s3Region As String, s3BucketName As String, s3DirectoryPath As String)
        _s3AccessKey = s3AccessKey
        _s3SecretAccessKey = s3SecretAccessKey
        _s3BucketName = s3BucketName
        _s3DirectoryPath = s3DirectoryPath
        _s3Region = s3Region

        Dim s3Config As New AmazonS3Config
        s3Config.RegionEndpoint = RegionEndpoint.GetBySystemName(_s3Region)

        _s3Client = New AmazonS3Client(s3AccessKey, s3SecretAccessKey, s3Config)
    End Sub

    Public Sub UploadFile(filePath As String, s3BucketName As String, newFileName As String, deleteLocalFileOnSuccess As Boolean)
        'save in s3
        Dim s3PutRequest As New PutObjectRequest()
        's3PutRequest = New PutObjectRequest()
        s3PutRequest.FilePath = filePath
        s3PutRequest.BucketName = s3BucketName
        s3PutRequest.CannedACL = S3CannedACL.PublicRead

        'key - new file name
        If Not String.IsNullOrWhiteSpace(newFileName) Then
            s3PutRequest.Key = newFileName
        End If

        s3PutRequest.Headers.Expires = New DateTime(2020, 1, 1)

        Try
            _s3Client.PutObject(s3PutRequest)

            If deleteLocalFileOnSuccess Then
                'Delete local file
                If File.Exists(filePath) Then
                    File.Delete(filePath)
                End If
            End If
            'handle exceptions
        Catch ex As Exception
        End Try
    End Sub

    Public Function UploadFileFromFilePath(filePath As String, fileNameS3 As String) As Boolean
        Try

            Dim utility As New TransferUtility(_s3Client)
            Dim request As New TransferUtilityUploadRequest()

            If _s3DirectoryPath Is Nothing OrElse _s3DirectoryPath = "" Then
                request.BucketName = _s3BucketName
            Else
                request.BucketName = Convert.ToString(_s3BucketName & Convert.ToString("/")) & _s3DirectoryPath
            End If

            request.Key = fileNameS3
            request.FilePath = filePath
            request.CannedACL = S3CannedACL.PublicReadWrite

            utility.Upload(request)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function UploadFileFromStream(fileStream As Stream, fileNameS3 As String) As Boolean
        Try

            Dim utility As New TransferUtility(_s3Client)
            Dim request As New TransferUtilityUploadRequest()

            If _s3DirectoryPath Is Nothing OrElse _s3DirectoryPath = "" Then
                request.BucketName = _s3BucketName
            Else
                request.BucketName = Convert.ToString(_s3BucketName & Convert.ToString("/")) & _s3DirectoryPath
            End If

            request.Key = fileNameS3
            request.InputStream = fileStream
            request.CannedACL = S3CannedACL.PublicReadWrite

            utility.Upload(request)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ReadStreamFromFile(fileNameS3 As String) As MemoryStream

        Try
            Dim fileStream = New MemoryStream()
            Dim request = New GetObjectRequest()
            request.Key = fileNameS3

            If String.IsNullOrEmpty(_s3DirectoryPath) Then
                request.BucketName = _s3BucketName
            Else
                request.BucketName = Convert.ToString(_s3BucketName & Convert.ToString("/")) & _s3DirectoryPath
            End If

            Dim response = _s3Client.GetObject(request)
            Using result As GetObjectResponse = response
                result.ResponseStream.CopyTo(fileStream)
            End Using

            fileStream.Position = 0

            Return fileStream
        Catch ex As Exception
            Throw New Exception(ex.ToString())
        End Try

    End Function

    Public Function FileExists(fileNameS3 As String) As Boolean

        Dim request As New ListObjectsRequest()
        Try
            request.BucketName = _s3BucketName
            request.Prefix = fileNameS3
            ' or part of the key
            request.MaxKeys = 1
            ' max limit to find objects
            Dim response As ListObjectsResponse = _s3Client.ListObjects(request)
            Return response.S3Objects.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.ToString())
        End Try

    End Function

    Public Function FileInfo(bucketName As String, fileNameS3 As String) As S3FileInfo
        Dim s3FileInfo As New S3FileInfo(_s3Client, bucketName, fileNameS3)
        Return s3FileInfo
    End Function

    'INI PAUL 29/08/2018
    Public Sub DeleteCipHtml(path As String, fileName As String)
        Dim request As New DeleteObjectRequest
        request.BucketName = path
        request.Key = fileName

        Dim response As DeleteObjectResponse = _s3Client.DeleteObject(request)
    End Sub
End Class