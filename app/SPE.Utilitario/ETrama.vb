Public Class ETrama

    Public Sub New()

    End Sub
    Public Sub New(ByVal etiqueta As String)
        _Etiqueta = etiqueta

    End Sub
    Public Sub New(ByVal etiqueta As String, ByVal nodoetiqueta As String)
        _Etiqueta = etiqueta
        _NodoEtiqueta = nodoetiqueta
    End Sub
    Private _Etiqueta As String
    Private _Valor As String
    Private _SeUtiliza As Boolean
    Private _ValorPorDefecto As String
    Private _NodoEtiqueta As String
    Private _Requerido As String


    Public Property NodoEtiqueta() As String
        Get
            Return _NodoEtiqueta
        End Get
        Set(ByVal value As String)
            _NodoEtiqueta = value
        End Set
    End Property

    Public Property Etiqueta() As String
        Get
            Return _Etiqueta
        End Get
        Set(ByVal value As String)
            _Etiqueta = value
        End Set
    End Property
    Public Property Valor() As String
        Get
            Return _Valor
        End Get
        Set(ByVal value As String)
            _Valor = value
        End Set
    End Property
    Public Property SeUtiliza() As Boolean
        Get
            Return _SeUtiliza
        End Get
        Set(ByVal value As Boolean)
            _SeUtiliza = value
        End Set
    End Property
    Public Property ValorPorDefecto() As String
        Get
            Return _ValorPorDefecto
        End Get
        Set(ByVal value As String)
            _ValorPorDefecto = value
        End Set
    End Property

    Public Property Requerido() As String
        Get
            Return _Requerido
        End Get
        Set(ByVal value As String)
            _Requerido = value
        End Set
    End Property
End Class
