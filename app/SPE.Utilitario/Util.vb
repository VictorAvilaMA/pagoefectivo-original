Imports Microsoft.Office.Interop
Imports System.Web
Imports System.Text
Imports System.IO
Public Class Util
    Public Function ExcelToListadeStrings(ByVal rutaArchivo As String, ByVal nombreHoja As String, ByVal colInicial As String, ByVal colFinal As String) As List(Of String)
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim range As Excel.Range
        Dim rCnt As Integer
        Dim cCnt As Integer
        'Dim culturaActual As Globalization.CultureInfo
        'System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        'culturaActual = System.Threading.Thread.CurrentThread.CurrentCulture
        'System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        'System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        xlApp = New Excel.ApplicationClass
        'xlWorkBook = xlApp.Workbooks.Open("c:\demo.xlsx")
        xlWorkBook = xlApp.Workbooks.Open(rutaArchivo) '"C:\Documents and Settings\mirguece\Escritorio\Conciliacion - Pago Efectivo\Copia de DAY_MOVEMENTS1291336041304(1).xls")
        'Dim cultura As String = System.Threading.Thread.CurrentThread.CurrentUICulture.DisplayName
        xlWorkSheet = xlWorkBook.Worksheets(nombreHoja)

        range = xlWorkSheet.UsedRange
        'range = xlWorkSheet.Range("A1:A2")

        Dim lista As New List(Of String)
        'Delimitamos la Celda Inicio y Celda fin a tomar
        Dim celInicio As String = "", celFin As String = "", colFin As String = ""
        For rCnt = 1 To range.Rows.Count
            For cCnt = 1 To range.Columns.Count
                If CType(range.Cells(rCnt, cCnt), Excel.Range).Text = colInicial Then
                    celInicio = ChrW(CType(range.Cells(rCnt, cCnt), Excel.Range).Column + 64) & (CType(range.Cells(rCnt, cCnt), Excel.Range).Row + 1).ToString()
                End If
                If CType(range.Cells(rCnt, cCnt), Excel.Range).Text = colFinal Then
                    colFin = ChrW(CType(range.Cells(rCnt, cCnt), Excel.Range).Column + 64)
                End If
            Next
            If rCnt = range.Rows.Count Then
                celFin = colFin & rCnt
            End If
        Next
        'Recortamos el rango a la data que nos interesa
        range = xlWorkSheet.Range(celInicio & ":" & celFin)
        'Nota se tomara las columnas 1,14 y 16 que corresponden a los datos importantes para la conciliacion: Moneda,NumeroOrdenPago y MontoTotal
        'Importante: A la columna de tama�a 16 se le restara - 1 caracter, pq el tama�o original era 15
        'Dim estcolumnas As String = "2,3,1,7,14,30,8,8,15,15,15,6,7,22,4,12,6,6"
        Dim estcolumnas As String = "2,3,1,7,14,30,8,8,15,15,15,6,7,22,4,12,6,6"
        Dim codMoneda As String = "0"
        For rCnt = 1 To range.Rows.Count
            Dim item As String = ""
            For cCnt = 1 To range.Columns.Count
                If ChrW(cCnt + 64) = "A" Then
                    If CType(range.Cells(rCnt, cCnt), Excel.Range).Text <> "" Then
                        If CType(range.Cells(rCnt, cCnt + 2), Excel.Range).Text.ToString().Contains("EFECTIVO") Then
                            item = item & StrDup(2, "X") & "CodigoSucursal" & codMoneda & StrDup(7, "X") & "NumeroOrdenPago"
                            item = item & StrDup(30, "X") & FormateaFecha(CType(range.Cells(rCnt, cCnt), Excel.Range).Text.ToString()).ToString("yyyyMMdd") & FormateaFecha(CType(range.Cells(rCnt, cCnt), Excel.Range).Text.ToString()).ToString("yyyyMMdd")
                            item = item & "Monto" & StrDup(15, "X") & "Monto"
                            item = item & StrDup(6, "X") & "NumeroOperacion" & StrDup(22, "X") & StrDup(4, "X") & StrDup(12, "X") & StrDup(6, "X") & StrDup(6, "X")
                        End If
                    End If
                End If
                If ChrW(cCnt + 64) = "C" Then
                    If CType(range.Cells(rCnt, cCnt), Excel.Range).Text.ToString().Contains("EFECTIVO") Then
                        If CType(range.Cells(rCnt, cCnt), Excel.Range).Text <> "" Then
                            item = Replace(item, "NumeroOrdenPago", Replace(CType(range.Cells(rCnt, cCnt), Excel.Range).Text, "EFECTIVO", ""))
                        End If
                    End If
                End If
                If ChrW(cCnt + 64) = "D" Then
                    Dim valorCelda As String = ""
                    If rCnt = 1 Then
                        If CType(range.Cells(rCnt, cCnt - 1), Excel.Range).Text.ToString().Contains("EFECTIVO") Then
                            If CType(range.Cells(rCnt, cCnt), Excel.Range).Text <> "" Then
                                item = Replace(item, "Monto", StrDup(15 - Replace(CType(range.Cells(rCnt, cCnt), Excel.Range).Text, ".", "").Length, "0") & Replace(CType(range.Cells(rCnt, cCnt), Excel.Range).Text, ".", ""))
                            End If
                        End If
                    Else
                        If CType(range.Cells(rCnt, cCnt - 1), Excel.Range).Text.ToString().Contains("EFECTIVO") Then
                            If CType(range.Cells(rCnt, cCnt), Excel.Range).Text <> "" Then
                                item = Replace(item, "Monto", StrDup(15 - Replace(CType(range.Cells(rCnt, cCnt), Excel.Range).Text, ".", "").Length, "0") & Replace(CType(range.Cells(rCnt, cCnt), Excel.Range).Text, ".", ""))
                            End If
                        End If
                    End If
                End If
                If ChrW(cCnt + 64) = "E" Then
                    If CType(range.Cells(rCnt, cCnt - 2), Excel.Range).Text.ToString().Contains("EFECTIVO") Then
                        If CType(range.Cells(rCnt, cCnt), Excel.Range).Text <> "" Then
                            item = Replace(item, "CodigoSucursal", CType(range.Cells(rCnt, cCnt), Excel.Range).Text.ToString().Substring(0, 3))
                        End If
                    End If
                End If
                If ChrW(cCnt + 64) = "F" Then
                    If CType(range.Cells(rCnt, cCnt - 3), Excel.Range).Text.ToString().Contains("EFECTIVO") Then
                        If CType(range.Cells(rCnt, cCnt), Excel.Range).Text <> "" Then
                            item = Replace(item, "NumeroOperacion", CType(range.Cells(rCnt, cCnt), Excel.Range).Text)
                        End If
                    End If
                End If
            Next
            If item <> "" Then
                lista.Add(item)
            End If
        Next
        'Dim sb As New System.Text.StringBuilder
        'For Each linea In lista
        '    sb.Append(linea & vbCrLf)
        'Next
        'txtResultado.Text = sb.ToString()
        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)
        'System.Threading.Thread.CurrentThread.CurrentUICulture = culturaActual
        'System.Threading.Thread.CurrentThread.CurrentCulture = culturaActual
        Return lista
    End Function
    Private Function FormateaFecha(ByVal fecha As String) As DateTime
        Dim response As String()
        response = fecha.Split("/")
        Return New DateTime(response(2), response(1), response(0))
    End Function
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Public Function TransformaraArrayBytes(ByVal fuControl As UI.WebControls.FileUpload) As Byte()
        If Not (fuControl.PostedFile Is Nothing) And fuControl.PostedFile.ContentLength > 0 Then
            If fuControl.HasFile Then
                'Get the extension of the uploaded file. 
                Dim extension As String = System.IO.Path.GetExtension(fuControl.FileName)
                ' Get the size in bytes of the file to upload.
                Dim fileSize As Integer = fuControl.PostedFile.ContentLength
                Dim imgFile As HttpPostedFile = fuControl.PostedFile
                Dim bytesImage(fuControl.PostedFile.ContentLength) As Byte
                imgFile.InputStream.Read(bytesImage, 0, fuControl.PostedFile.ContentLength)
                Return bytesImage
            End If
        End If
        Return Nothing
    End Function

    ''' <summary>
    ''' Convierte un Stream en un Arreglo de Bytes
    ''' </summary>
    ''' <param name="strm">Stream a convertir</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function StreamToBytes(ByVal strm As Stream) As Byte()
        Using bRdr As New BinaryReader(strm)
            Return bRdr.ReadBytes(strm.Length)
        End Using
    End Function

    ''' <summary>
    ''' Implementa l�gica de convertir un arreglo de bytes a texto
    ''' </summary>
    ''' <param name="arrByte">arreglo de bytes a convertir</param>
    ''' <param name="ruta">ruta donde ser� escrito el archivo</param>
    ''' <returns>bool indicando si genero o no el archivo</returns>
    ''' <remarks></remarks>
    Public Function BytesToText(ByVal arrByte() As Byte, ByVal ruta As String) As Boolean
        Try
            Dim TextoMemory As New MemoryStream()

            Using archivo_fisico As StreamWriter = File.CreateText(ruta)

                TextoMemory.Write(arrByte, 0, arrByte.Length)
                TextoMemory.Seek(0, SeekOrigin.Begin)

                Using read As StreamReader = New StreamReader(TextoMemory)
                    Dim line As String = Nothing

                    Dim stringtmp As String = read.ReadToEnd()
                    archivo_fisico.Write(stringtmp)

                End Using
                archivo_fisico.Close()
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        Return True
    End Function
    ''' <summary>
    ''' Valida si el dia seleccionado es Domingo se registre como sabado para evitar problemas de con los stored de Conciliacion Automatizada
    ''' </summary>
    ''' <param name="pdfecha">fecha a validar</param>
    ''' <returns>fecha corregida si es que fuera necesario</returns>
    ''' <remarks></remarks>
    Public Function ValidaDiaProcesoConciliacion(ByVal pdfecha As DateTime) As DateTime
        Dim fecCorregida As DateTime
        Dim dia As System.DayOfWeek = pdfecha.DayOfWeek
        fecCorregida = pdfecha
        If dia = DayOfWeek.Sunday Then
            fecCorregida = DateAdd(DateInterval.Day, -1, pdfecha)
        End If
        Return fecCorregida
    End Function
End Class
