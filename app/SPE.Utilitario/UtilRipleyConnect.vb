﻿Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Text
Imports System.Threading.Tasks

Public Class UtilRipleyConnect

    Public Class RipleyConnect

        Public Function RndConvertBase64(ByVal texto As String) As String
            Dim plainTextBytes = System.Text.Encoding.UTF8.GetBytes(texto)
            Return System.Convert.ToBase64String(plainTextBytes)
        End Function

        Private Shared Function ResponseParse(ByVal xmlResponse As String) As confirmarPagoOCResponse

            'Codigo Respuesta
            Dim posCodRespI = xmlResponse.IndexOf("<resp:codigoRespuesta>") + 22
            Dim codResp = xmlResponse.Substring(posCodRespI)
            Dim posCodRespF = codResp.IndexOf("</resp:codigoRespuesta>")
            Dim codigoRespuesta = xmlResponse.Substring(posCodRespI, posCodRespF)

            'Glosa Respuesta
            Dim posGlosaRespI = xmlResponse.IndexOf("<resp:glosaRespuesta>") + 21
            Dim glosaResp = xmlResponse.Substring(posGlosaRespI)
            Dim posGlosaRespF = glosaResp.IndexOf("</resp:glosaRespuesta>")
            Dim glosaRespuesta = xmlResponse.Substring(posGlosaRespI, posGlosaRespF)

            Dim resp As New confirmarPagoOCResponse()
            resp.codigoRespuesta = codigoRespuesta
            resp.glosaRespuesta = glosaRespuesta

            Return resp

        End Function

        Public Function ConfirmarPago(ByVal confirmarRq As confirmarPagoOCRequest) As confirmarPagoOCResponse
            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function() True
            Dim confirmarRs As New confirmarPagoOCResponse()

            Try
                Dim PostData As [Byte]() = Encoding.UTF8.GetBytes(confirmarRq.TramaXML.ToString())

                Dim req As HttpWebRequest = DirectCast(WebRequest.Create(confirmarRq.urlWS), HttpWebRequest)

                'Definiendo el Header
                req.Headers.Add("SOAPAction", "https://www.ripley.com.pe/webapp/wcs/stores/servlet/RipleyPayPagoEfectivo")
                req.ContentType = "text/xml;charset=utf-8"

                req.KeepAlive = True
                req.ContentLength = PostData.Length
                req.Method = "POST"

                'Capturando el Response
                Dim RequestStream As System.IO.Stream = req.GetRequestStream()
                RequestStream.Write(PostData, 0, PostData.Length)
                RequestStream.Close()
                Dim Reader As System.IO.StreamReader = New StreamReader(req.GetResponse().GetResponseStream())
                Dim ResultHTML As String = Reader.ReadToEnd()
                Reader.Close()
                'Limpio variable
                confirmarRq.random = String.Empty
                'Procesando el Response
                confirmarRs = ResponseParse(ResultHTML)
                confirmarRs.xml = ResultHTML
            Catch e As WebException
                confirmarRs.glosaRespuesta = e.Message
                confirmarRs.codigoRespuesta = -1
                If e.Status = WebExceptionStatus.ProtocolError Then
                    Dim resp As WebResponse = e.Response
                    'Console.WriteLine("Web Exception: " + sr.ReadToEnd());
                    Using sr As New StreamReader(resp.GetResponseStream())
                    End Using
                End If
                'Console.WriteLine("Errrorrrr: " + ex.Message);
            Catch ex As Exception
            End Try

            Return confirmarRs

        End Function

    End Class

    Public Class confirmarPagoOCRequest
        Public Property orderId() As String
            Get
                Return m_orderId
            End Get
            Set(ByVal value As String)
                m_orderId = value
            End Set
        End Property
        Private m_orderId As String
        Public Property fechaPago() As String
            Get
                Return m_fechaPago
            End Get
            Set(ByVal value As String)
                m_fechaPago = value
            End Set
        End Property
        Private m_fechaPago As String
        Public Property fechaContable() As String
            Get
                Return m_fechaContable
            End Get
            Set(ByVal value As String)
                m_fechaContable = value
            End Set
        End Property
        Private m_fechaContable As String
        Public Property montoCancelado() As String
            Get
                Return m_montoCancelado
            End Get
            Set(ByVal value As String)
                m_montoCancelado = value
            End Set
        End Property
        Private m_montoCancelado As String
        Public Property canal() As String
            Get
                Return m_canal
            End Get
            Set(ByVal value As String)
                m_canal = value
            End Set
        End Property
        Private m_canal As String
        Public Property caja() As String
            Get
                Return m_caja
            End Get
            Set(ByVal value As String)
                m_caja = value
            End Set
        End Property
        Private m_caja As String
        Public Property codigoAutorizacion() As String
            Get
                Return m_codigoAutorizacion
            End Get
            Set(ByVal value As String)
                m_codigoAutorizacion = value
            End Set
        End Property
        Private m_codigoAutorizacion As String
        Public Property usuario() As String
            Get
                Return m_usuario
            End Get
            Set(ByVal value As String)
                m_usuario = value
            End Set
        End Property
        Private m_usuario As String
        Public Property password() As String
            Get
                Return m_password
            End Get
            Set(ByVal value As String)
                m_password = value
            End Set
        End Property
        Private m_password As String
        Public Property random() As String
            Get
                Return m_random
            End Get
            Set(ByVal value As String)
                m_random = value
            End Set
        End Property
        Private m_random As String
        Public Property urlWS() As String
            Get
                Return m_urlWS
            End Get
            Set(ByVal value As String)
                m_urlWS = value
            End Set
        End Property
        Private m_urlWS As String

        Public Property TramaXML() As String
            Get
                Return _TramaXML
            End Get
            Set(ByVal value As String)
                _TramaXML = value
            End Set
        End Property
        Private _TramaXML As String

    End Class

    Public Class confirmarPagoOCResponse
        Public Property codigoRespuesta() As String
            Get
                Return m_codigoRespuesta
            End Get
            Set(ByVal value As String)
                m_codigoRespuesta = value
            End Set
        End Property
        Private m_codigoRespuesta As String
        Public Property glosaRespuesta() As String
            Get
                Return m_glosaRespuesta
            End Get
            Set(ByVal value As String)
                m_glosaRespuesta = value
            End Set
        End Property
        Private m_glosaRespuesta As String
        Public Property xml() As String
            Get
                Return m_xml
            End Get
            Set(ByVal value As String)
                m_xml = value
            End Set
        End Property
        Private m_xml As String
        Private _idLogRequest As Long


    End Class
End Class
