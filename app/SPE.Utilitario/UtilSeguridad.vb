Imports _3Dev.FW.AccesoDatos.Seguridad

Public Class UtilSeguridad

    Public Shared Function GenerarCodigoAleatorio() As String
        '
        Return Microsoft.VisualBasic.Left(Guid.NewGuid().ToString, 6).ToUpper()
        '
    End Function

    Public Shared Function GenerarCodigoAleatorioEncryptado() As String
        '
        Return EncryptarCodigo(GenerarCodigoAleatorio())
        '
    End Function

    Public Shared Function EncryptarCodigo(ByVal value As String) As String
        '
        Dim objEncodePass As New CustomMembershipProvider
        Return objEncodePass.EncodePassword(value)
        '
    End Function

    Public Shared Function UnEncryptarCodigo(ByVal value As String) As String
        Dim objUncodePass As New CustomMembershipProvider
        Return objUncodePass.UnEncodePassword(value)
    End Function

End Class
