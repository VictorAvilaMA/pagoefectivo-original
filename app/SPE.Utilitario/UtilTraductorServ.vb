Imports System.Xml
Imports System.Text
Imports System.IO
Imports _3Dev.FW.Util

Public Class UtilTraductorServ
    Implements IDisposable


#Region "Region Variables"
    '
    Public Const EMerchantId As String = "MerchantID"
    Public Const EDatosEnc As String = "DatosEnc"
    Public Const EOrdenId As String = "OrdenId"
    Public Const EUrlOk As String = "UrlOk"
    Public Const EUrlError As String = "UrlError"
    Public Const EMailCom As String = "MailCom"
    Public Const EOrdenDesc As String = "OrdenDesc"
    Public Const EMoneda As String = "Moneda"
    Public Const EHome As String = "Home"
    Public Const EMonto As String = "Monto"
    Public Const EParUtil As String = "parUtil"
    Public Const EUsaEncriptacion As String = "usaencriptacion"
    'control de cambio
    Public Const EUsuarioID As String = "UsuarioID"
    Public Const EDataAdicional As String = "DataAdicional"
    Public Const EUsuarioNombre As String = "UsuarioNombre"
    Public Const EUsuarioApellidos As String = "UsuarioApellidos"
    Public Const EUsuarioLocalidad As String = "UsuarioLocalidad"
    Public Const EUsuarioDomicilio As String = "UsuarioDomicilio"
    Public Const EUsuarioProvincia As String = "UsuarioProvincia"
    Public Const EUsuarioPais As String = "UsuarioPais"
    Public Const EUsuarioAlias As String = "UsuarioAlias"

    Public Const EParIDServicio As String = "parIdServicio"
    Public Const EParIDEmpresa As String = "parIdEmpresa"
    Public Const EParUrlServicio As String = "parUrlServicio"
    'Private EEncriptado As Boolean = True

    Private rutaXML As String = ""
    Private servicioUrl As String = ""
    Private listTrama(18)() As String
    Private listaTrama As List(Of ETrama)

    Private _strUrlAbsoluta As String = ""
    Private _idServicio As String = ""
    Private _idEmpresa As String = ""
    Private _urlServicio As String = ""
    Private _Recepcion As String = ""

    '


#End Region
    Public Enum TipoTraduccion
        Url
        API
    End Enum
    Public Sub New()

    End Sub
    Private _tipoTraduccion As TipoTraduccion = TipoTraduccion.Url

    Public Sub New(ByVal tipo As TipoTraduccion, ByVal strxml As String, ByVal servurl As String, ByVal strUrlAbsoluta As String, ByVal xml As String)
        _tipoTraduccion = tipo

    End Sub

    Public Sub New(ByVal strxml As String, ByVal servurl As String, ByVal strUrlAbsoluta As String)
        _tipoTraduccion = TipoTraduccion.Url
        '
        rutaXML = strxml : servicioUrl = servurl : _strUrlAbsoluta = strUrlAbsoluta : _urlServicio = strUrlAbsoluta
        LeerEstructuraXML(strxml, strUrlAbsoluta)
        LeerTrama("")
        '
    End Sub
    Public Sub New(ByVal strxml As String, ByVal servurl As String, ByVal strUrlAbsoluta As String, ByVal xml As String)
        _tipoTraduccion = TipoTraduccion.Url
        '
        rutaXML = strxml : servicioUrl = servurl : _strUrlAbsoluta = strUrlAbsoluta : _urlServicio = strUrlAbsoluta
        LeerEstructuraXML(strxml, strUrlAbsoluta)
        LeerTrama(xml)
        '
    End Sub

    Public Sub LeerEstructuraXML(ByVal strxml As String, ByVal servicio As String)
        '
        listaTrama = New List(Of ETrama)
        If servicioUrl.IndexOf("parUtil") <> -1 Then
            Dim strurl As String = DesencriptarUrl("parUtil")
            Dim strtempurl As String = strurl.Substring(strurl.IndexOf("parUrlServicio=") + "parUrlServicio".Length + 1)
            Dim jx As Integer = strtempurl.IndexOf("|")
            If (jx < 0) Then jx = strtempurl.Length
            servicio = strtempurl.Substring(0, jx)
        End If

        Dim valUsaEncriptacion As Boolean = False

        Dim xmldoc As New XmlDocument()
        xmldoc.Load(strxml)
        'valUsaEncriptacion = xmldoc.ChildNodes(1).ChildNodes(0).Attributes("usaencriptacion").InnerText
        Dim trama0 As ETrama = New ETrama("parUsaEncriptacion", EUsaEncriptacion)
        ' trama0.Valor = valUsaEncriptacion
        ' listaTrama.Add(trama0)
        For Each node As XmlNode In xmldoc.ChildNodes(1) 'NODO DE "SERVICIOS(1)"

            If node.Attributes("url").InnerText.ToUpper() = servicio.ToUpper() Then
                'valida todos los nodos del contrato
                valUsaEncriptacion = node.Attributes("usaencriptacion").InnerText
                trama0.Valor = valUsaEncriptacion
                listaTrama.Add(trama0)

                If node.Attributes("tipoRecepcion").InnerText.ToUpper() = "GET" Then
                    _Recepcion = "GET"
                ElseIf node.Attributes("tipoRecepcion").InnerText.ToUpper() = "POST" Then
                    _Recepcion = "POST"
                End If

                For Each subnode As XmlNode In node.ChildNodes
                    Dim trama As New ETrama()

                    trama.Etiqueta = subnode.Attributes("nombre").InnerText
                    trama.ValorPorDefecto = subnode.Attributes("default").InnerText
                    trama.Requerido = subnode.Attributes("requerido").InnerText

                    trama.NodoEtiqueta = subnode.LocalName
                    listaTrama.Add(trama)
                    'If (subnode.LocalName = "DatosEnc") Then DatosEnc = trama.Etiqueta
                    'If (subnode.LocalName = "MerchantID") Then MerchantId = trama.Etiqueta
                Next
                listaTrama.Add(New ETrama("parIdServicio", EParIDServicio))
                listaTrama.Add(New ETrama("parIdEmpresa", EParIDEmpresa))
                listaTrama.Add(New ETrama("parUrlServicio", EParUrlServicio))
                Return
            End If
            '
        Next
        'For i As Integer = 0 To listTrama.Length - 1
        '    listTrama(i) = New String(1) {}
        'Next
        '
    End Sub

    Public Shared Function BuildUrlNotificacion(ByVal obeOrdenPago As SPE.Entidades.BEOrdenPago, ByVal parUrl As String) As String
        '
        Dim dburlTemp As New StringBuilder()
        '
        If Not obeOrdenPago Is Nothing AndAlso Not parUrl Is Nothing Then
            '
            Dim objTs As New UtilTraductorServ
            objTs.LeerEstructuraXML(obeOrdenPago.XmlTramaServRuta, obeOrdenPago.Url)
            '
            Dim sbDatosEnc As New StringBuilder()
            sbDatosEnc.Append(objTs.MerchantId) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.MerchantID) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.OrdenId) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.OrderIdComercio) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UrlOk) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UrlOk) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UrlError) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UrlError) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.MailCom) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.MailComercio) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.OrdenDesc) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.ConceptoPago) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.Moneda) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.IdMoneda) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.Monto) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.Total) : sbDatosEnc.Append("|")
            'control de cambio
            sbDatosEnc.Append(objTs.UsuarioID) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioID) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.DataAdicional) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.DataAdicional) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioNombre) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioNombre) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioApellidos) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioApellidos) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioLocalidad) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioLocalidad) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioDomicilio) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioDomicilio) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioProvincia) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioProvincia) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioPais) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioPais) : sbDatosEnc.Append("|")
            sbDatosEnc.Append(objTs.UsuarioAlias) : sbDatosEnc.Append("=") : sbDatosEnc.Append(obeOrdenPago.UsuarioAlias)
            '
            obeOrdenPago.DatosEnc = UtilTraductorServ.EncriptarStr(sbDatosEnc.ToString)
            '
            dburlTemp.Append(parUrl.ToString() & "?")
            dburlTemp.Append(objTs.MerchantId) : dburlTemp.Append("=") : dburlTemp.Append(obeOrdenPago.MerchantID) : dburlTemp.Append("&")
            dburlTemp.Append(objTs.DatosEnc) : dburlTemp.Append("=") : dburlTemp.Append(obeOrdenPago.DatosEnc)
            '
        End If
        '
        Return dburlTemp.ToString
        '
    End Function

    Public Sub LeerTrama(ByVal xml As String)
        '
        If _Recepcion = "GET" And xml = "" Then
            If servicioUrl.IndexOf("parUtil") <> -1 Then

                'If EEncriptado = False Then
                '    DescomponerTrama_WithOutEncryption(servicioUrl)
                'Else

                DescomponerTrama(DesencriptarUrl("parUtil")) ': LeerValoresEnTrama()
                _strUrlAbsoluta = UrlServicioValor
                ' If servicioUrl.IndexOf(DatosEnc) <> -1 Then DescomponerTrama(DesencriptarUrl(DatosEnc)) ': LeerValoresEnTrama()
                'End If

            End If

            If servicioUrl.IndexOf(DatosEnc) <> -1 And UsaEncriptacionValor = "True" Then DescomponerTrama(DesencriptarUrl(DatosEnc)) ': LeerValoresEnTrama()
            If UsaEncriptacionValor = "False" Then DescomponerTrama_WithOutEncryption(servicioUrl)

            '
            'LeerEstructuraXML(rutaXML, _strUrlAbsoluta)
            '
        ElseIf _Recepcion = "POST" And xml <> "" Then
            If servicioUrl.IndexOf("parUtil") <> -1 Then
                _strUrlAbsoluta = UrlServicioValor
            End If

            If UsaEncriptacionValor = "False" Then DescomponerTramaXML(DesencriptarStr(xml))
        End If

        '
    End Sub

    Private Sub DescomponerTrama_WithOutEncryption(ByVal parTrama As String)
        Dim tramaTemp As String = parTrama
        '
        For Each trama As ETrama In listaTrama

            Dim ix As Integer = tramaTemp.ToUpper().IndexOf(trama.Etiqueta.ToUpper() + "=")
            If (ix >= 0) Then
                Dim strtramatmp As String = tramaTemp.Substring(ix + trama.Etiqueta.Length)
                Dim jx As Integer = strtramatmp.IndexOf("&")
                If (jx <= 0) Then jx = strtramatmp.Length

                Dim strvalor As String = strtramatmp.Substring(1, jx - 1)
                If (strvalor <> "") Then trama.Valor = strvalor : trama.SeUtiliza = True Else trama.SeUtiliza = False : trama.Valor = trama.ValorPorDefecto
            Else
                If (trama.Valor = "") Then trama.Valor = trama.ValorPorDefecto
                If (trama.Etiqueta = DatosEnc) And (trama.Valor = "") Then trama.SeUtiliza = True
                If (trama.Etiqueta = EParUrlServicio) And (trama.Valor = "") Then trama.Valor = _urlServicio : trama.SeUtiliza = True

            End If

        Next
    End Sub
    Public Sub DescomponerTramaXML(ByVal xml As String)

        Dim _xmldoc As New XmlDocument()
        Dim nodo As System.Xml.XmlNode
        Dim strvalor As String
        _xmldoc.LoadXml(xml)
        nodo = _xmldoc.SelectSingleNode("ordenPago")
        For Each trama As ETrama In listaTrama
            If trama.Etiqueta.ToUpper() = "EGP_CURRENCY" Then
                strvalor = DataUtil.StringToInt(nodo.SelectSingleNode("IdMoneda").InnerText)
                If (strvalor <> "") Then trama.Valor = strvalor : trama.SeUtiliza = True Else trama.SeUtiliza = False : trama.Valor = trama.ValorPorDefecto
            ElseIf trama.Etiqueta.ToUpper() = "IMPORTE" Then
                strvalor = Convert.ToDecimal(nodo.SelectSingleNode("Total").InnerText)
                If (strvalor <> "") Then trama.Valor = strvalor : trama.SeUtiliza = True Else trama.SeUtiliza = False : trama.Valor = trama.ValorPorDefecto
            ElseIf trama.Etiqueta.ToUpper() = "USUARIOID" Then
                strvalor = nodo.SelectSingleNode("UsuarioId").InnerText
                If (strvalor <> "") Then trama.Valor = strvalor : trama.SeUtiliza = True Else trama.SeUtiliza = False : trama.Valor = trama.ValorPorDefecto
            ElseIf trama.Etiqueta.ToUpper() = "EGP_USUARIONOMBRE" Then
                strvalor = nodo.SelectSingleNode("UsuarioNombre").InnerText
                If (strvalor <> "") Then trama.Valor = strvalor : trama.SeUtiliza = True Else trama.SeUtiliza = False : trama.Valor = trama.ValorPorDefecto

            Else
                If (trama.Valor = "") Then trama.Valor = trama.ValorPorDefecto
                If (trama.Etiqueta = DatosEnc) And (trama.Valor = "") Then trama.SeUtiliza = True
                If (trama.Etiqueta = EParUrlServicio) And (trama.Valor = "") Then trama.Valor = _urlServicio : trama.SeUtiliza = True

            End If

        Next



    End Sub



    Private Sub DescomponerTrama(ByVal parTrama As String)
        '
        Dim tramaTemp As String = parTrama
        '
        For Each trama As ETrama In listaTrama

            Dim ix As Integer = tramaTemp.ToUpper().IndexOf(trama.Etiqueta.ToUpper() + "=")
            If (ix >= 0) Then
                Dim strtramatmp As String = tramaTemp.Substring(ix + trama.Etiqueta.Length)
                Dim jx As Integer = strtramatmp.IndexOf("|")
                If (jx <= 0) Then jx = strtramatmp.Length

                Dim strvalor As String = strtramatmp.Substring(1, jx - 1)
                If (strvalor <> "") Then trama.Valor = strvalor : trama.SeUtiliza = True Else trama.SeUtiliza = False : trama.Valor = trama.ValorPorDefecto
            Else
                If (trama.Valor = "") Then trama.Valor = trama.ValorPorDefecto
                If (trama.Etiqueta = DatosEnc) And (trama.Valor = "") Then trama.SeUtiliza = True
                If (trama.Etiqueta = EParUrlServicio) And (trama.Valor = "") Then trama.Valor = _urlServicio : trama.SeUtiliza = True

            End If

        Next
        '
    End Sub

    Public Function DesencriptarUrl(ByVal strParameter As String) As String
        Dim urltmp As String = ""
        If strParameter = DatosEnc Then
            urltmp = servicioUrl.Substring(servicioUrl.LastIndexOf(strParameter) + strParameter.Length + 1)
            If urltmp.IndexOf("&") <> -1 Then urltmp = urltmp.Substring(0, urltmp.IndexOf("&"))
            DatosEncValor = urltmp
        ElseIf strParameter = "parUtil" Then

            Dim ivr As Integer = servicioUrl.LastIndexOf("&vr")
            If (ivr > 0) Then
                Dim ifin As Integer = ivr - (servicioUrl.LastIndexOf(strParameter) + strParameter.Length + 1)
                urltmp = servicioUrl.Substring(servicioUrl.LastIndexOf(strParameter) + strParameter.Length + 1, ifin)
            Else
                urltmp = servicioUrl.Substring(servicioUrl.LastIndexOf(strParameter) + strParameter.Length + 1)
            End If

        End If
        Return DesencriptarStr(urltmp)
    End Function

    Public Shared Function DesencriptarStr(ByVal str As String) As String
        Dim libx As New ClsLibreriax()
        Return libx.Decrypt(str, libx.fEncriptaKey)
    End Function

    Public Shared Function EncriptarStr(ByVal str As String) As String
        Dim libx As New ClsLibreriax()
        Return libx.Encrypt(str, libx.fEncriptaKey)
    End Function

    Public ReadOnly Property DatosEnc() As String
        Get
            'Return _DatosEnc
            Dim trama As ETrama = DameETrama(EDatosEnc)
            If trama Is Nothing Then
                Return ""
            Else
                Return trama.Etiqueta
            End If
        End Get
    End Property

    Public ReadOnly Property MerchantId() As String
        Get
            'Return _MerchantId
            Return DameETrama(EMerchantId).Etiqueta
        End Get
    End Property

    Public ReadOnly Property OrdenId() As String
        Get
            'Return _OrdenId
            Return DameETrama(EOrdenId).Etiqueta
        End Get
    End Property

    Public ReadOnly Property UrlOk() As String
        Get
            'Return _UrlOk
            Return DameETrama(EUrlOk).Etiqueta
        End Get
    End Property

    Public ReadOnly Property UrlError() As String
        Get
            'Return _UrlError
            Return DameETrama(EUrlError).Etiqueta
        End Get
    End Property

    Public ReadOnly Property MailCom() As String
        Get
            'Return _MailCom
            Return DameETrama(EMailCom).Etiqueta
        End Get
    End Property

    Public ReadOnly Property OrdenDesc() As String
        Get
            'Return _OrderDesc
            Return DameETrama(EOrdenDesc).Etiqueta
        End Get
    End Property

    Public ReadOnly Property Moneda() As String
        Get
            'Return _Moneda
            Return DameETrama(EMoneda).Etiqueta
        End Get
    End Property

    Public ReadOnly Property Monto() As String
        Get
            '  Return _Monto
            Return DameETrama(EMonto).Etiqueta
        End Get
    End Property
    '--Creado por Karen Gordillo
    Public ReadOnly Property Home() As String
        Get

            Return DameETrama(EHome).Etiqueta
        End Get
    End Property

    '---
    Public ReadOnly Property UsuarioID() As String
        Get
            'Return _UsuarioIDValor
            Return DameETrama(EUsuarioID).Etiqueta
        End Get
    End Property

    Public ReadOnly Property DataAdicional() As String
        Get
            'Return _DataAdicionalValor
            Return DameETrama(EDataAdicional).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioNombre() As String
        Get
            '  Return _UsuarioNombreValor
            Return DameETrama(EUsuarioNombre).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioApellidos() As String
        Get
            'Return _UsuarioApellidosValor
            Return DameETrama(EUsuarioApellidos).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioLocalidad() As String
        Get
            'Return _UsuarioLocalidadValor
            Return DameETrama(EUsuarioLocalidad).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioDomicilio() As String
        Get
            'Return _UsuarioDomicilioValor
            Return DameETrama(EUsuarioDomicilio).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioProvincia() As String
        Get
            '  Return _UsuarioProvinciaValor
            Return DameETrama(EUsuarioProvincia).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioPais() As String
        Get
            'Return _UsuarioPaisValor
            Return DameETrama(EUsuarioPais).Etiqueta
        End Get
    End Property
    Public ReadOnly Property UsuarioAlias() As String
        Get
            'Return _UsuarioAliasValor
            Return DameETrama(EUsuarioAlias).Etiqueta
        End Get
    End Property

    Private strnodoetiquetabusq As String
    Public Function EvaluaNombreNodo(ByVal obeNode As ETrama) As Boolean
        Return obeNode.NodoEtiqueta = strnodoetiquetabusq
    End Function
    Dim PredicateEvaluaNombreNodo As New Predicate(Of ETrama)(AddressOf EvaluaNombreNodo)
    '---
    Public ReadOnly Property DameETrama(ByVal str As String) As ETrama
        Get
            strnodoetiquetabusq = str
            Return listaTrama.Find(PredicateEvaluaNombreNodo)
        End Get
    End Property


    Public Property DatosEncValor() As String
        Get
            'Return _DatosEncValor
            Return DameETrama(EDatosEnc).Valor
        End Get
        Set(ByVal value As String)
            DameETrama(EDatosEnc).Valor = value
        End Set
    End Property

    Public ReadOnly Property MerchantIdValor() As String
        Get
            '   Return _MerchantIdValor
            Return DameETrama(EMerchantId).Valor
        End Get
    End Property
    Public ReadOnly Property OrdenIdValor() As String
        Get

            'Return _OrdenIdValor
            Return DameETrama(EOrdenId).Valor
        End Get
    End Property
    Public ReadOnly Property UrlOkValor() As String
        Get
            'Return _UrlOkValor
            Return DameETrama(EUrlOk).Valor
        End Get
    End Property
    Public ReadOnly Property UrlErrorValor() As String
        Get
            'Return _UrlErrorValor
            Return DameETrama(EUrlError).Valor
        End Get
    End Property
    Public ReadOnly Property MailComValor() As String
        Get
            '   Return _MailComValor
            Return DameETrama(EMailCom).Valor
        End Get
    End Property
    Public ReadOnly Property OrdenDescValor() As String
        Get

            '  Return _OrderDescValor
            Return DameETrama(EOrdenDesc).Valor
        End Get
    End Property
    Public ReadOnly Property MonedaValor() As String
        Get
            'Return _MonedaValor
            Return DameETrama(EMoneda).Valor
        End Get
    End Property
    Public ReadOnly Property MontoValor() As String
        Get
            'Return _MontoValor
            Return DameETrama(EMonto).Valor
        End Get
    End Property

    'control de cambio
    Public ReadOnly Property UsuarioIDValor() As String
        Get
            'Return _UsuarioIDValor
            Return DameETrama(EUsuarioID).Valor
        End Get
    End Property
    Public ReadOnly Property DataAdicionalValor() As String
        Get
            'Return _DataAdicionalValor
            Return DameETrama(EDataAdicional).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioNombreValor() As String
        Get
            '  Return _UsuarioNombreValor
            Return DameETrama(EUsuarioNombre).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioApellidosValor() As String
        Get
            'Return _UsuarioApellidosValor
            Return DameETrama(EUsuarioApellidos).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioLocalidadValor() As String
        Get
            'Return _UsuarioLocalidadValor
            Return DameETrama(EUsuarioLocalidad).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioDomicilioValor() As String
        Get
            'Return _UsuarioDomicilioValor
            Return DameETrama(EUsuarioDomicilio).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioProvinciaValor() As String
        Get
            '  Return _UsuarioProvinciaValor
            Return DameETrama(EUsuarioProvincia).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioPaisValor() As String
        Get
            'Return _UsuarioPaisValor
            Return DameETrama(EUsuarioPais).Valor
        End Get
    End Property
    Public ReadOnly Property UsuarioAliasValor() As String
        Get
            'Return _UsuarioAliasValor
            Return DameETrama(EUsuarioAlias).Valor
        End Get
    End Property

    Public ReadOnly Property IdServicioValor() As String
        Get
            Dim trama As ETrama = DameETrama(EParIDServicio)
            If trama Is Nothing Then
                Return ""
            Else
                Return trama.Valor
            End If
        End Get
    End Property

    Public ReadOnly Property IdEmpresaValor() As String
        Get
            Dim trama As ETrama = DameETrama(EParIDEmpresa)
            If trama Is Nothing Then
                Return ""
            Else
                Return trama.Valor
            End If
        End Get
    End Property

    Public ReadOnly Property UrlServicioValor() As String
        Get
            Dim trama As ETrama = DameETrama(EParUrlServicio)
            If trama Is Nothing Then
                Return ""
            Else
                Return trama.Valor
            End If

        End Get
    End Property

    Public ReadOnly Property UsaEncriptacionValor() As String
        Get
            Dim trama As ETrama = DameETrama(EUsaEncriptacion)
            If trama Is Nothing Then
                Return ""
            Else
                Return trama.Valor
            End If
        End Get
    End Property

    'Public Shared Function ObtenerMontoDeFormatoServicio(ByVal val As String) As Decimal
    '    Dim strentero As String = val.Substring(0, 6)
    '    Dim strdecimal As String = val.Substring(6, 2)
    '    Dim decvalor As String = strentero + "." + strdecimal
    '    Return Convert.ToDecimal(decvalor)

    'MODIFICADO 10/11/2009: RECONOCE DE FORMA ADECUADA EL FORMATO DEL MONTO'

    Public Shared Function ObtenerMontoDeFormatoServicio(ByVal monto As String) As Decimal
        'Dim strentero As String = monto.Substring(0, 6)
        'Dim strdecimal As String = monto.Substring(6, 2)
        'Dim decvalor As String = strentero + "." + strdecimal
        'Return Convert.ToDecimal(decvalor)
        Dim monto2 As Integer
        Dim monto3 As Double
        monto2 = Convert.ToInt32(monto)
        monto3 = monto2 / 100
        Return monto3
    End Function

    '----------------------------------------------------------------
    'Creado por Karen Gordillo
    'Public Shared Function GetHome() As String

    '    Dim m_xmld As New XmlDocument()
    '    m_xmld.Load("D:/ContratoServiciosPagoEfectivo/ContratoServicios.xml")
    '    Dim m_node As XmlNode
    '    m_node = m_xmld.SelectSingleNode("/Servicios/servicio_home")
    '    Dim atributo As XmlAttribute = m_node.Attributes("url")
    '    Dim url As String = atributo.InnerText
    '    Return url
    'End Function

    Public ReadOnly Property NodoRequerido(ByVal Nodo As String) As String
        Get
            Return DameETrama(Nodo).Requerido
        End Get
    End Property

    Public Sub ValidarCamposRequeridos()

        For Each trama As ETrama In listaTrama
            If trama.Requerido <> Nothing Then
                If trama.Requerido = True Then
                    If trama.Valor = "" Or trama.Valor = Nothing Then
                        Throw New SPE.Exceptions.TramaGetException(trama.Etiqueta)
                    End If
                End If
            End If
        Next

    End Sub


    Public Shared Sub CrearContratoServicios()

        Dim fileName As String = System.Configuration.ConfigurationManager.AppSettings("fileName")
        Dim sourcePath As String = System.Configuration.ConfigurationManager.AppSettings("sourcePath")
        Dim targetPath As String = System.Configuration.ConfigurationManager.AppSettings("targetPath")
        Dim sourceFile As String = System.IO.Path.Combine(sourcePath, fileName)
        Dim destFile As String = System.IO.Path.Combine(targetPath, fileName)
        If Not System.IO.Directory.Exists(targetPath) Then
            System.IO.Directory.CreateDirectory(targetPath)
        End If
        System.IO.File.Copy(sourceFile, destFile, True)


    End Sub


    '-----------------------------------------------------------------

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free managed resources when explicitly called
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
