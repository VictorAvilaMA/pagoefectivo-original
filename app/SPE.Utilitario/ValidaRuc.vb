Imports System.Xml
Imports System.Text
Imports System.IO
Imports System.Collections.Generic
Public Class ValidaRuc
    'FUNCIONES PARA VALIDAR EL RUC  

    Public Function ValidarRuc(ByVal vRUC As String) As Boolean

        Dim ruc As String = vRUC
        Dim ok As Boolean = (Not (esnulo(ruc) Or Not ContieneSoloCaracteresNumericos(ruc) Or Not eslongrucok(ruc) Or Not valruc(ruc)))
        If (ok = False) Then

            Return False

        Else

            Return True
        End If
    End Function

    Public Function valruc(ByVal valor As String) As Boolean
        valor = trim(valor)

        If (ContieneSoloCaracteresNumericos(valor)) Then

            Dim suma As Integer = 0
            Dim digito As String = String.Empty
            Dim resto As Integer
            If (valor.Length = 8) Then

                For i As Integer = 0 To valor.Length - 2 
                    digito = valor(i)
                    If (i = 0) Then
                        suma += (digito * 2)
                    Else
                        suma += (digito * (valor.Length - i))
                    End If
                Next
                resto = suma Mod 11
                If (resto = 1) Then resto = 11
                If (resto + Convert.ToInt32(valor(valor.Length - 1)) = 11) Then
                    Return True
                End If

            ElseIf (valor.Length = 11) Then

                suma = 0
                Dim x As Integer = 6
                For i As Integer = 0 To valor.Length - 2

                    If (i = 4) Then
                        x = 8
                    End If
                    digito = valor(i)
                    x = x - 1
                    If (i = 0) Then
                        suma += (digito * x)
                    Else
                        suma += (digito * x)
                    End If

                Next
                resto = suma Mod 11
                resto = 11 - resto
                If (resto >= 10) Then
                    resto = resto - 10
                End If

                If (resto = Convert.ToString(valor(valor.Length - 1))) Then
                    Return True
                End If
            End If
        End If

        Return False

    End Function
    Public Function esnulo(ByVal campo As String) As Boolean
        Return (campo = Nothing Or campo = String.Empty)
    End Function

    Public Function eslongrucok(ByVal ruc As String) As Boolean
        Return (ruc.Length = 11)
    End Function
    Public Function trim(ByVal cadena As String) As String
        Dim temp_cadena As String = String.Empty
        Dim len As Integer = cadena.Length
        For i As Integer = 0 To len - 1
            If (cadena(i) <> " ") Then
                temp_cadena += cadena(i)
            End If
        Next
        Return temp_cadena
    End Function

    Public Function ContieneSoloCaracteresNumericos(ByVal targetString As String) As Boolean

        Dim varCharEnumerator As CharEnumerator

        varCharEnumerator = targetString.GetEnumerator()
        While (varCharEnumerator.MoveNext())
            If (Not Char.IsNumber(varCharEnumerator.Current)) Then
                Return False
            End If
        End While

        Return True

    End Function
End Class
