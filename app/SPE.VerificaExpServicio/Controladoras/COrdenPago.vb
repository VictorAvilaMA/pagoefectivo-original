Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun


Public Class COrdenPago


    Function RealizarProcesoExpiracionOrdenPago() As Integer
        'Return SPE.VerificaExpServicio.RemoteServices.Instance.IOrdenPago.RealizarProcesoExpiracionOrdenPago()
        Dim result As Integer
        Using Conexionsss As New ProxyBase(Of IOrdenPago)
            Dim vOrdenPago As IOrdenPago = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            result = vOrdenPago.RealizarProcesoExpiracionOrdenPago
        End Using
        Return result
    End Function
End Class
