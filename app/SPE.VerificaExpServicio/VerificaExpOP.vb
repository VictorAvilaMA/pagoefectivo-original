Imports System.Timers
Imports System.IO
Imports System.Net
Imports System.Configuration
Imports SPE.Entidades
Imports System.ComponentModel


Public Class VerificaExpOP

    Private t As Timer = Nothing

    Public Shared Sub Main()
        'Dim ServicesToRun As System.ServiceProcess.ServiceBase()
        'ServicesToRun = New System.ServiceProcess.ServiceBase() {New VerificaExpOP}
        'System.ServiceProcess.ServiceBase.Run(ServicesToRun)
#If DEBUG Then
        Dim service As New VerificaExpOP()
        service.OnStart(Nothing)
        'Es necesario domir este hilo para que la aplicación
        'no termine y nos permita depurar
        System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite)
#Else
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New VerificaExpOP}
        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
        'Dim ServicesToRun As System.ServiceProcess.ServiceBase()
        'ServicesToRun = New System.ServiceProcess.ServiceBase() {New NotificaServRequest}
        'System.ServiceProcess.ServiceBase.Run(ServicesToRun)
#End If
    End Sub
    Dim oCOrdenPago As COrdenPago
    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        oCOrdenPago = New COrdenPago
        ' Add any initialization after the InitializeComponent() call.
        t = New Timer(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("Intervalo")))
        AddHandler t.Elapsed, New System.Timers.ElapsedEventHandler(AddressOf Me.t_Elapsed)
    End Sub
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            oCOrdenPago = New COrdenPago
            t.Start()
        Catch ex As Exception
            EventLog.WriteEntry("No se puedo iniciar el Servicio Verifica Expiracion OP - Pago Efectivo: " + ex.Message)
        End Try
    End Sub
    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        t.Stop()
    End Sub
    Protected Overrides Sub OnContinue()
        MyBase.OnContinue()
        t.Start()
    End Sub
    Protected Overrides Sub OnPause()
        MyBase.OnPause()
        t.Stop()
    End Sub
    Protected Overrides Sub OnShutdown()
        t.Stop()
    End Sub
    Protected Sub t_Elapsed(ByVal sender As System.Object, _
        ByVal e As System.Timers.ElapsedEventArgs)
        Try
            oCOrdenPago.RealizarProcesoExpiracionOrdenPago()
        Catch ex As Exception
            Write(ex.ToString())
        End Try
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Public Shared Sub Write(m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"
            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(Date.Now.ToLongTimeString() & "   >>>>" & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub
End Class
