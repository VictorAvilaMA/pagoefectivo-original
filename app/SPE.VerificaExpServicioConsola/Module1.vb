Module Module1

    Sub Main()
        Try
            Console.ReadLine()
            DoStartRemotingServices()
            Dim cntrl As New COrdenPago()
            cntrl.RealizarProcesoExpiracionOrdenPago()

        Catch ex As Exception
            Throw ex
        End Try
        
    End Sub
    Private Sub DoStartRemotingServices()
        Try
            SPE.VerificaExpServicioConsola.RemoteServices.Instance = New SPE.VerificaExpServicioConsola.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try


        If Not EstablishConnection() Then
            Return
        End If
    End Sub
    Function EstablishConnection() As Boolean
        Return SPE.VerificaExpServicioConsola.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function


End Module
