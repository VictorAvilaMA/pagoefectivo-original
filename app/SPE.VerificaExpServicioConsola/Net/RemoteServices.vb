Imports System.Configuration
Imports System.Collections
Imports System.Runtime.Remoting
Imports System.Runtime.Remoting.Channels
Imports Belikov.GenuineChannels
Imports Belikov.GenuineChannels.DotNetRemotingLayer
Imports Belikov.GenuineChannels.GenuineTcp
Imports Belikov.GenuineChannels.GenuineHttp
Imports Belikov.GenuineChannels.TransportContext
Imports Belikov.GenuineChannels.Security
Imports Belikov.GenuineChannels.Security.ZeroProofAuthorization
Imports SPE.EmsambladoComun
Public Class RemoteServices

    Private _serverUrl As String
    Private Shared _instance As RemoteServices
    Private _lockForInternalMembers As New Object()

    'Private _TcpChannel As System.Runtime.Remoting.Channels.Tcp.TcpChannel = Nothing


    Private _isRemotingConfigured As Boolean = False
    Private _iTransportContext As ITransportContext
    Private _parsedServerUri As String = ""
    Private _genuineTcpChannel As GenuineTcpChannel
    Private _keyProvider_ZpaClient As KeyProvider_ZpaClient

    Public Property ServerUrl() As String
        Get
            Return _serverUrl
        End Get
        Set(ByVal value As String)
            value = _serverUrl
        End Set
    End Property

    Public Shared Property Instance() As RemoteServices
        Get
            Return RemoteServices._instance

        End Get
        Set(ByVal value As RemoteServices)
            RemoteServices._instance = value
        End Set
    End Property

    Public Property IsRemotingConfigured() As Boolean
        Get
            SyncLock (_lockForInternalMembers)
                Return _isRemotingConfigured
            End SyncLock
        End Get
        Set(ByVal value As Boolean)
            _isRemotingConfigured = value
        End Set
    End Property

    Public Property ITransportContext() As ITransportContext
        Get
            Return _iTransportContext
        End Get
        Set(ByVal value As ITransportContext)
            _iTransportContext = value
        End Set
    End Property
    Public Property IGenuineTcpChannel() As GenuineTcpChannel
        Get
            Return _genuineTcpChannel
        End Get
        Set(ByVal value As GenuineTcpChannel)
            _genuineTcpChannel = value
        End Set
    End Property
    Private _IOrderPago As SPE.EmsambladoComun.IOrdenPago
    Public Property IOrdenPago() As SPE.EmsambladoComun.IOrdenPago
        Get
            Return _IOrderPago
        End Get
        Set(ByVal value As SPE.EmsambladoComun.IOrdenPago)
            _IOrderPago = value
        End Set
    End Property
    'Public Property TcpChannel() As System.Runtime.Remoting.Channels.Tcp.TcpChannel
    '    Get
    '        Return _TcpChannel
    '    End Get
    '    Set(ByVal value As System.Runtime.Remoting.Channels.Tcp.TcpChannel)
    '        _TcpChannel = value
    '    End Set
    'End Property

    Public Property ServerHostInformation() As HostInformation
        Get
            Return ITransportContext.KnownHosts(_parsedServerUri)
        End Get
        Set(ByVal value As HostInformation)

        End Set
    End Property

    Public Property KeyProvider_ZpaClient() As KeyProvider_ZpaClient
        Get
            Return _keyProvider_ZpaClient
        End Get
        Set(ByVal value As KeyProvider_ZpaClient)
            _keyProvider_ZpaClient = value
        End Set
    End Property

    Public Function SetupNetworkEnvironment() As Boolean
        SyncLock (_lockForInternalMembers)
            'Lock(1)
            If Not (Me.IsRemotingConfigured) Then
                ' create the channel
                Dim properties As New Hashtable()
                properties("InvocationTimeout") = ConfigurationManager.AppSettings.Get("InvocationTimeout")
                Me._genuineTcpChannel = New GenuineTcpChannel(properties, Nothing, Nothing)
                ChannelServices.RegisterChannel(Me._genuineTcpChannel, False)

                '// create and register the Zero Proof Authorization Security Key Provider
                Me._keyProvider_ZpaClient = New KeyProvider_ZpaClient(ZpaFeatureFlags.None, Nothing, Nothing)


                '// subscribe to channel events
                'GenuineGlobalEventProvider.GenuineChannelsGlobalEvent += new GenuineChannelsGlobalEventHandler(this.ProcessChannelEvent);
                AddHandler GenuineGlobalEventProvider.GenuineChannelsGlobalEvent, AddressOf ProcessChannelEvent
                Me._isRemotingConfigured = True

            End If


            Dim serverUrl As String = Nothing
            Dim isGtcp As Boolean = True
            If (isGtcp) Then
                Me._iTransportContext = Me._genuineTcpChannel.ITransportContext
            End If

            If (serverUrl = Nothing) Then
                Me._serverUrl = ConfigurationManager.AppSettings.Get("tcpDefaultURL")
            End If

            '' codigo
            Me.IOrdenPago = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IOrdenPago), Me.ServerUrl + "/" + NombreServiciosConocidos.IOrdenPago), SPE.EmsambladoComun.IOrdenPago)


            Try

            Catch ex As Exception
                EventLog.WriteEntry("SPE.VerificaExpServicio", ex.Message)

            End Try
        End SyncLock
        Return True

    End Function

    Private _connectionEstablished As Boolean

    Public Property ConnectionEstablished() As Boolean
        Get
            Return Me._connectionEstablished
        End Get
        Set(ByVal value As Boolean)
            Me._connectionEstablished = value
        End Set
    End Property

    Private _connectionIsReestablished As Boolean
    Public Property ConnectionIsReestablished() As Boolean
        Get
            Return Me._connectionIsReestablished
        End Get
        Set(ByVal value As Boolean)
            Me._connectionIsReestablished = value
        End Set
    End Property

    Public Sub ProcessChannelEvent(ByVal sender As Object, ByVal e As GenuineEventArgs)
        Select Case e.EventType
            Case GenuineEventType.GeneralConnectionEstablished
                Me.ConnectionEstablished = True
                Me.ConnectionIsReestablished = False

            Case GenuineEventType.GeneralConnectionReestablishing
                Me.ConnectionIsReestablished = True


            Case GenuineEventType.GeneralConnectionClosed
                Me.ConnectionEstablished = False
                Me.ConnectionIsReestablished = False

            Case GenuineEventType.GeneralNewSessionDetected
            Case GenuineEventType.GeneralServerRestartDetected
                '// the server has been restarted, it is necessary to reload all content

        End Select

    End Sub



End Class
