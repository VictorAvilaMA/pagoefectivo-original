Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun
Imports _3Dev.FW.Entidades

Namespace SPE.Web


    Public Class CAgenciaBancaria

        Sub New()

        End Sub


        Public Function ConsultarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As List(Of BusinessEntityBase)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaBancaria(obeAgenciaBancaria)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.ConsultarAgenciaBancaria(obeAgenciaBancaria)
        End Function

        'Public Function ActualizarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
        '    Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.ActualizarAgenciaBancaria(obeAgenciaBancaria)
        'End Function


        'Public Function ConsultarAgenciaBancariaxCodigo(ByVal codAgenciaBancaria As String) As BEAgenciaBancaria
        '    Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.ConsultarAgenciaBancariaxCodigo(codAgenciaBancaria)
        'End Function

        Public Function RegistrarCancelacionOrdenPagoAgenciaBancaria(ByVal obeMovimiento As BEMovimiento) As Integer
            Dim result As Integer
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                result = Conexions.DevolverContrato(New EntityBaseContractResolver()).RegistrarCancelacionOrdenPagoAgenciaBancaria(obeMovimiento)
            End Using
            '= CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria(obeMovimiento)
            'If (result > 0) Then
            '    Dim vOrdenPago As IOrdenPago = (CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago)
            '    Dim onoOrdenPago As New NOOrdenPago(vOrdenPago)
            '    Dim objOrdenPago As New BEOrdenPago
            '    objOrdenPago.IdOrdenPago = result
            '    onoOrdenPago.NotificarRecepcionOrdenPago(objOrdenPago)
            'End If
            Return result
        End Function
        Public Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String() 'As SPE.Entidades.webServiceXResponse
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)
        End Function


        Public Function AnularOperacionPago(ByVal obeMovimiento As BEMovimiento) As Integer
            Dim result As Integer
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                result = Conexions.DevolverContrato(New EntityBaseContractResolver()).AnularOperacionPago(obeMovimiento)
            End Using
            'CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.AnularOperacionPago(obeMovimiento)
            If (result = 2) Then result = ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
            Return result
        End Function

        Public Function GenerarEstructuraXML(ByVal array() As String) As webServiceXResponse
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).GenerarEstructuraXML(array)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.GenerarEstructuraXML(array)
        End Function


        ''' <summary>
        ''' dj.- controlodora "consultar BCP"
        ''' </summary>
        ''' <param name="obeOrdenPago"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ConsultarBCP(ByVal obeOrdenPago As BEOrdenPago) As BEBCPResponse
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Dim result As New BEBCPResponse
                Try
                    Dim resolver As New EntityBaseContractResolver()
                    result = Conexions.DevolverContrato(resolver).ConsultarBCP(obeOrdenPago)
                Catch ex As Exception

                End Try

                Return result
            End Using
        End Function

        ''' <summary>
        ''' dj.- controladora "pagar BCP"
        ''' </summary>
        ''' <param name="obeMovimiento"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function PagarBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Dim resultado As New BEBCPResponse
                resultado = Conexions.DevolverContrato(New EntityBaseContractResolver()).PagarBCP(obeMovimiento)
                Return resultado
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.PagarBCP(obeMovimiento)
        End Function

        ''' <summary>
        ''' dj.- controladora "anular BCP"
        ''' </summary>
        ''' <param name="obeMovimiento"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function AnularBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).AnularBCP(obeMovimiento)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.AnularBCP(obeMovimiento)
        End Function


    End Class
End Namespace
