Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Namespace SPE.Web
    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices
        Private _IComun As IComun
        Private _IAgenciaBancaria As IAgenciaBancaria
        Private _IOrdenPago As IOrdenPago


        Public Property IAgenciaBancaria() As IAgenciaBancaria
            Get

                Return _IAgenciaBancaria
            End Get
            Set(ByVal value As IAgenciaBancaria)
                _IAgenciaBancaria = value
            End Set
        End Property

        Public Property IComun() As IComun
            Get
                Return _IComun
            End Get
            Set(ByVal value As IComun)
                _IComun = value
            End Set
        End Property

        Public Property IOrdenPago() As IOrdenPago
            Get
                Return _IOrdenPago
            End Get
            Set(ByVal value As IOrdenPago)
                _IOrdenPago = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()
            MyBase.DoSetupNetworkEnviroment()
            Me.IComun = CType(Activator.GetObject(GetType(IComun), Me.ServerUrl + "/" + NombreServiciosConocidos.IComun), IComun)
            Me.IOrdenPago = CType(Activator.GetObject(GetType(IOrdenPago), Me.ServerUrl + "/" + NombreServiciosConocidos.IOrdenPago), IOrdenPago)
            Me.IAgenciaBancaria = CType(Activator.GetObject(GetType(IAgenciaBancaria), Me.ServerUrl + "/" + NombreServiciosConocidos.IAgenciaBancaria), IAgenciaBancaria)
        End Sub

    End Class

End Namespace