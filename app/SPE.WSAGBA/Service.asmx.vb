Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Collections.Generic
Imports SPE.Entidades
Imports System.Xml
Imports System.IO
Imports _3Dev.FW.Util
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun
Imports System.ComponentModel
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.Data.SqlClient
Imports SPE.Logging
Imports System.Web.Script.Serialization


<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService
    Private ReadOnly _logger As ILogger = New Logger()

    <WebMethod()> _
    Public Function WSAGOperacion(ByVal codigo As String, ByVal codServicio As String, ByVal Moneda As String, ByVal NroOperacion As String, ByVal NroOperacionAnulada As String, _
        ByVal Agencia As String, ByVal NroDocs As String, ByVal NDoc1 As String, ByVal Mdoc1 As String, ByVal NDoc2 As String, ByVal Mdoc2 As String _
        , ByVal NDoc3 As String, ByVal Mdoc3 As String, ByVal NDoc4 As String, ByVal Mdoc4 As String, ByVal NDoc5 As String, ByVal Mdoc5 As String _
        , ByVal NDoc6 As String, ByVal Mdoc6 As String, ByVal NDoc7 As String, ByVal Mdoc7 As String, ByVal NDoc8 As String, ByVal Mdoc8 As String _
        , ByVal NDoc9 As String, ByVal Mdoc9 As String, ByVal NDoc10 As String, ByVal Mdoc10 As String, ByVal Monto As String, ByVal CodOperacion As String) As String 'SPE.Entidades.webServiceXResponse
        ', ByVal NDoc9 As String, ByVal Mdoc9 As Decimal, ByVal NDoc10 As String, ByVal Mdoc10 As Decimal, ByVal Monto As Decimal, ByVal CodOperacion As String) As SPE.Entidades.webServiceXResponse

        Dim obResponse As BEBCPResponse
        Dim ocomun As New CComun
        Dim obRequest As New BEBCPRequest
        Dim idRequest As Integer
        Dim xml As String = ""

        Try
            'Dim oRelic As NewRelic.Api.Agent.NewRelic
            'Dim eventAttributes As New Dictionary(Of String, Object)
            'NewRelic.Api.Agent.NewRelic.RecordCustomEvent('MyCustomEvent', eventAttributes);

            obRequest.Codigo = codigo
            obRequest.CodOperacion = CodOperacion
            obRequest.Moneda = Moneda
            obRequest.NroOperacion = NroOperacion
            obRequest.NroOperacionAnulada = NroOperacionAnulada
            obRequest.Agencia = Agencia
            obRequest.NroDocs = NroDocs
            obRequest.CodServicio = codServicio
            obRequest.NDoc1 = NDoc1 : obRequest.Mdoc1 = Mdoc1
            obRequest.NDoc2 = NDoc2 : obRequest.Mdoc2 = Mdoc2
            obRequest.NDoc3 = NDoc3 : obRequest.Mdoc3 = Mdoc3
            obRequest.NDoc4 = NDoc4 : obRequest.Mdoc4 = Mdoc4
            obRequest.NDoc5 = NDoc5 : obRequest.Mdoc5 = Mdoc5
            obRequest.NDoc6 = NDoc6 : obRequest.Mdoc6 = Mdoc6
            obRequest.NDoc7 = NDoc7 : obRequest.Mdoc7 = Mdoc7
            obRequest.NDoc8 = NDoc8 : obRequest.Mdoc8 = Mdoc8
            obRequest.NDoc9 = NDoc9 : obRequest.Mdoc9 = Mdoc9
            obRequest.NDoc10 = NDoc10 : obRequest.Mdoc10 = Mdoc10
            obRequest.Monto = Monto

           
            'AGREGADO
            Dim json = New JavaScriptSerializer().Serialize(obRequest)
            _logger.Info("REQUEST: " + "ID ORDEN  PAGO: " + codigo + "|" + "OPERACION: " + CodOperacion.ToString() + json.ToString())
           

            idRequest = ocomun.RegistrarLogBCPResquest(CodOperacion, obRequest)

            Select Case (CodOperacion)
                Case Consultar
                    obResponse = consultarBCP(obRequest)
                Case Cancelar
                    obResponse = pagarBCP(obRequest)
                Case Anular
                    obResponse = anularBCP(obRequest)
                Case Else
                    Return Nothing
            End Select

            obResponse.IdRequest = idRequest
            ocomun.RegistrarLogBCPResponse(CodOperacion, obResponse)


            'ANTIGUO
            'Dim a As _3Dev.FW.Util.DataUtil = New _3Dev.FW.Util.DataUtil
            'Return a.GenerarXML(obResponse.Respuesta, False)


            'NUEVO RETURN
            Dim a As _3Dev.FW.Util.DataUtil = New _3Dev.FW.Util.DataUtil
            xml = a.GenerarXML(obResponse.Respuesta, False)
            _logger.Info("RESPONSE: " + "ID ORDEN  PAGO: " + codigo + "|" + "OPERACION : " + CodOperacion.ToString() + xml.ToString())


        Catch ex As Exception
            _logger.Error("ERROR: " + ex.ToString())
        End Try

        Return xml

    End Function

    Private Function AnularOperacion(ByVal codigo As String, ByVal codServicio As String, ByVal Moneda As String, ByVal NroOperacion As String, ByVal NroOperacionAnulada As String, _
    ByVal Agencia As String, ByVal NroDocs As String, ByVal NDoc1 As String, ByVal Mdoc1 As String, ByVal NDoc2 As String, ByVal Mdoc2 As String _
    , ByVal NDoc3 As String, ByVal Mdoc3 As String, ByVal NDoc4 As String, ByVal Mdoc4 As String, ByVal NDoc5 As String, ByVal Mdoc5 As String _
    , ByVal NDoc6 As String, ByVal Mdoc6 As String, ByVal NDoc7 As String, ByVal Mdoc7 As String, ByVal NDoc8 As String, ByVal Mdoc8 As String _
    , ByVal NDoc9 As String, ByVal Mdoc9 As String, ByVal NDoc10 As String, ByVal Mdoc10 As String, ByVal Monto As String) As String 'As SPE.Entidades.webServiceXResponse

        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        Dim oUtil As New DataUtil()
        Dim obeMovimiento As New BEMovimiento
        Dim respuesta(33) As String

        Dim mensaje As String
        'obeMovimiento.NumeroOrdenPago = codigo
        With obeMovimiento
            .NumeroOrdenPago = NDoc1
            .CodigoAgenciaBancaria = Agencia
            .NumeroOperacion = NroOperacion
            .NumeroOperacionPago = NroOperacionAnulada
            .CodigoServicio = codServicio
            .CodigoBanco = Conciliacion.CodigoBancos.BCP
            .Monto = Monto
        End With
        UtilTrace.DoTrace("AnulaCIP", "RecibeParametros", _
            String.Format("NDoc1:{0},Agencia:{1},NroOperacion:{2},NroOperacionAnulada:{3},codServicio:{4}", NDoc1, Agencia, NroOperacion, NroOperacionAnulada, codServicio))
        Try
            mensaje = objCAgenciaBancaria.AnularOperacionPago(obeMovimiento).ToString()
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            UtilTrace.DoTrace("AnulaCIP", "Error", "mensaje:" + ex.Message)
            mensaje = WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
        End Try
        UtilTrace.DoTrace("AnulaCIP", "EnviaParametros", "mensaje:" + mensaje)
        respuesta(33) = "00" & mensaje
        Dim a As _3Dev.FW.Util.DataUtil = New _3Dev.FW.Util.DataUtil
        Return a.GenerarXML(respuesta, False)


    End Function

    Private Function CancelarOrdenPago(ByVal codigo As String, ByVal codServicio As String, ByVal Moneda As String, ByVal NroOperacion As String, _
    ByVal Agencia As String, ByVal NroDocs As String, ByVal NDoc1 As String, ByVal Mdoc1 As String, ByVal NDoc2 As String, ByVal Mdoc2 As String _
    , ByVal NDoc3 As String, ByVal Mdoc3 As String, ByVal NDoc4 As String, ByVal Mdoc4 As String, ByVal NDoc5 As String, ByVal Mdoc5 As String _
    , ByVal NDoc6 As String, ByVal Mdoc6 As String, ByVal NDoc7 As String, ByVal Mdoc7 As String, ByVal NDoc8 As String, ByVal Mdoc8 As String _
    , ByVal NDoc9 As String, ByVal Mdoc9 As String, ByVal NDoc10 As String, ByVal Mdoc10 As String, ByVal Monto As String) As String 'SPE.Entidades.webServiceXResponse
        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        Dim obeMovimiento As New BEMovimiento
        Dim obeOrdenPago As New BEOrdenPago
        Dim outil As New DataUtil()
        Dim mensaje As String
        Dim respuesta(33) As String
        Dim result As Integer
        Try
            With obeMovimiento
                .NumeroOrdenPago = NDoc1
                ' obeMovimiento.IdMoneda = Convert.ToInt32(Moneda)
                .CodMonedaBanco = Moneda
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(Mdoc1)
                .NumeroOperacion = NroOperacion
                .CodigoAgenciaBancaria = Agencia
                .CodigoServicio = codServicio
                .CodigoBanco = Conciliacion.CodigoBancos.BCP
            End With
            UtilTrace.DoTrace("CancelarCIP", "RecibeParametros", _
                String.Format("NDoc1:{0},Moneda:{1},Mdoc1:{2},Agencia:{3},NroOperacion:{4},codServicio:{5}", _
                    NDoc1, Moneda, Mdoc1.ToString(), Agencia, NroOperacion, codServicio))
            result = objCAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria(obeMovimiento)
            If result > 0 Then
                mensaje = ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Pago_Realizado
            Else
                mensaje = ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Pago_no_realizado
            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            UtilTrace.DoTrace("CancelarCIP", "Error", "mensaje:" + ex.Message)
            mensaje = WSBancoMensaje.BCP.Mensaje.Pago_no_realizado
        End Try
        UtilTrace.DoTrace("CancelarCIP", "EnviaParametros", "result:" + result.ToString() + ",mensaje:" + mensaje)
        respuesta(33) = "00" & mensaje
        Dim a As _3Dev.FW.Util.DataUtil = New _3Dev.FW.Util.DataUtil
        Return a.GenerarXML(respuesta, False)
    End Function

    Private Function ConsultarOrdenPago(ByVal codigo As String, ByVal codServicio As String, ByVal Moneda As String, ByVal NroOperacion As String, _
    ByVal Agencia As String, ByVal NroDocs As String, ByVal NDoc1 As String, ByVal Mdoc1 As String, ByVal NDoc2 As String, ByVal Mdoc2 As String _
    , ByVal NDoc3 As String, ByVal Mdoc3 As String, ByVal NDoc4 As String, ByVal Mdoc4 As String, ByVal NDoc5 As String, ByVal Mdoc5 As String _
    , ByVal NDoc6 As String, ByVal Mdoc6 As String, ByVal NDoc7 As String, ByVal Mdoc7 As String, ByVal NDoc8 As String, ByVal Mdoc8 As String _
    , ByVal NDoc9 As String, ByVal Mdoc9 As String, ByVal NDoc10 As String, ByVal Mdoc10 As String, ByVal Monto As String) As String 'SPE.Entidades.webServiceXResponse
        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        Dim obeMovimiento As New BEMovimiento
        Dim obeOrdenPago As New BEOrdenPago
        Dim outil As New DataUtil()
        Dim respuesta(33) As String
        Dim wsResponse As New webServiceXResponse
        Dim a As _3Dev.FW.Util.DataUtil = New _3Dev.FW.Util.DataUtil
        Try
            If codigo <> "" Then
                With obeOrdenPago
                    .NumeroOrdenPago = codigo
                    .MonedaAgencia = Moneda
                    .CodigoBanco = Conciliacion.CodigoBancos.BCP
                    .CodigoServicio = codServicio
                End With
                UtilTrace.DoTrace("ConsultarOrdenPago", "RecibeParametros", _
                    String.Format("NDoc1:{0},Moneda:{1},codServicio:{2}", NDoc1, Moneda, codServicio))
                respuesta = objCAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)
            Else
                respuesta(33) = "00" & ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Cliente_no_encontrado
            End If
            UtilTrace.DoTrace("ConsultarOrdenPago", "EnviaParametros", "Respuesta:" + respuesta(33))
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            respuesta(33) = "00" & ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Cliente_no_encontrado
            UtilTrace.DoTrace("ConsultarOrdenPago", "Error", "Respuesta:" + ex.Message)
        End Try
        Return a.GenerarXML(respuesta, False)
    End Function

    'web service bcp - consultar
    <WebMethod()> _
    Private Function consultarBCP(ByVal obRequest As BEBCPRequest) As BEBCPResponse

        Dim response As New BEBCPResponse()

        Dim TmpRespuesta(33) As String
        response.Respuesta = TmpRespuesta
        response.Respuesta(33) = 0

        response.Estado = -1

        Dim obeOrdenPago As New BEOrdenPago
        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria

        Try

            'validacion de codigo
            If obRequest.Codigo <> "" Then
                'codigo correcto
                With obeOrdenPago
                    .NumeroOrdenPago = obRequest.Codigo
                    .MonedaAgencia = obRequest.Moneda
                    .CodigoBanco = Conciliacion.CodigoBancos.BCP
                    .CodigoServicio = obRequest.CodServicio
                    '.CadenaConexionPagoEfectivoSaldos = ConfigurationManager.ConnectionStrings("SPESQLPagoEfectivoSaldos").ConnectionString
                    '.DNIInvalido1 = ConfigurationManager.AppSettings("DNIInvalido1").ToString
                    '.DNIInvalido2 = ConfigurationManager.AppSettings("DNIInvalido2").ToString
                    .CodigoVoucher = obRequest.NroOperacion
                End With
                'consultar cip
                response = objCAgenciaBancaria.ConsultarBCP(obeOrdenPago)

            Else
                'codigo incorrecto
                response.Respuesta(33) = "00" & ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Cliente_no_encontrado
                response.Estado = 0
                response.Descripcion1 = "Codigo no presenta valor"

            End If

        Catch ex As Exception

            'respuesta al bcp
            response.Respuesta(33) = "00" & ParametrosSistema.WSBancoMensaje.BCP.Mensaje.Cliente_no_encontrado
            response.Estado = -1
            response.Descripcion1 = "Hubo un error en la aplicación"
            response.Descripcion2 = ex.ToString()

            'archivo de log
            ' _3Dev.FW.Web.Log.Logger.LogException(ex)

        End Try

        Return response

    End Function

    'dj.- web service bcp - pagar
    <WebMethod()> _
    Private Function pagarBCP(ByVal obRequest As BEBCPRequest) As BEBCPResponse

        Dim response As New BEBCPResponse
        response.Estado = -1

        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        Dim obeMovimiento As New BEMovimiento

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        'Dim tmpMensaje As String = ""
        Dim tmpRespuesta(33) As String
        response.Respuesta = tmpRespuesta
        response.Respuesta(33) = 0
        'Try

        Try
            With obeMovimiento
                .NumeroOrdenPago = obRequest.Codigo
                .CodMonedaBanco = obRequest.Moneda
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(obRequest.Monto)
                .NumeroOperacion = obRequest.NroOperacion
                .CodigoAgenciaBancaria = obRequest.Agencia
                .CodigoServicio = obRequest.CodServicio
                .CodigoBanco = Conciliacion.CodigoBancos.BCP
                '.CadenaConexionPagoEfectivoSaldos = ConfigurationManager.ConnectionStrings("SPESQLPagoEfectivoSaldos").ConnectionString
                '.DNIInvalido1 = ConfigurationManager.AppSettings("DNIInvalido1").ToString
                '.DNIInvalido2 = ConfigurationManager.AppSettings("DNIInvalido2").ToString
            End With

            'registrar pago de cip por el bcp
            response = objCAgenciaBancaria.PagarBCP(obeMovimiento)

            'respuesta al bcp
            response.Respuesta(33) = "00" & IIf((response.Respuesta(33)) > 0, _
                                        WSBancoMensaje.BCP.Mensaje.Pago_Realizado, _
                                        WSBancoMensaje.BCP.Mensaje.Pago_no_realizado)

            ''objeto log de salida
            'obeLog = New BELog("pagar CIP_BCP-EnviaParametros", _
            '                    "Respuesta:" + tmpRespuesta(33) + ". " + response.Descripcion1, Nothing)

            'obeLog.Parametro1 = "IdLog=" + idLog.ToString
            'obeLog.Parametro2 = "Estado:" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, "Validacion", "Error"))

            'excepcion personalizada
            'Catch ex As _3Dev.FW.Excepciones.FWBusinessException
            'Catch fc As FaultException

            '    tmpRespuesta(33) = "00" & WSBancoMensaje.BCP.Mensaje.Pago_no_realizado
            '    response.Estado = -1
            '    response.Mensaje = "Hubo un error en la aplicación"

            '    obeLog = New BELog("pagar CIP_BCP-EnviaParametros", _
            '                        "Respuesta:" + tmpRespuesta(33) + ". " + response.Mensaje, fc.Reason.ToString())

            '    obeLog.Parametro1 = "IdLog=" + idLog.ToString
            '    obeLog.Parametro2 = "Estado:" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, "Validacion", "Error"))

            'End Try

            ''registrar parametros de salida del bcp
            'UtilTrace.DoTraceBCP(obeLog)

        Catch ex As Exception
            'archivo de log
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'armando respuesta al bcp
            response.Estado = -1
            response.Respuesta(33) = "00" & WSBancoMensaje.BCP.Mensaje.Pago_no_realizado
            response.Descripcion1 = "Hubo un error en la aplicación."
            response.Descripcion2 = ex.ToString()
        End Try

        'Dim a As _3Dev.FW.Util.DataUtil = New _3Dev.FW.Util.DataUtil
        'Return a.GenerarXML(tmpRespuesta, False)
        Return response
    End Function

    'web service bcp - anular
    <WebMethod()> _
    Private Function anularBCP(ByVal obRequest As BEBCPRequest) As BEBCPResponse

        Dim response As New BEBCPResponse
        response.Estado = -1

        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        Dim obeMovimiento As New BEMovimiento

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Dim TmpRespuesta(33) As String
        response.Respuesta = TmpRespuesta
        response.Respuesta(33) = 0

        Try
            With obeMovimiento
                .NumeroOrdenPago = obRequest.NDoc1
                .CodigoAgenciaBancaria = obRequest.Agencia
                .NumeroOperacion = obRequest.NroOperacion
                .NumeroOperacionPago = obRequest.NroOperacionAnulada
                .CodigoServicio = obRequest.CodServicio
                .CodigoBanco = Conciliacion.CodigoBancos.BCP
                .Monto = _3Dev.FW.Util.DataUtil.StrToDecimal(obRequest.Monto)
                '.CadenaConexionPagoEfectivoSaldos = ConfigurationManager.ConnectionStrings("SPESQLPagoEfectivoSaldos").ConnectionString
                '.DNIInvalido1 = ConfigurationManager.AppSettings("DNIInvalido1").ToString
                '.DNIInvalido2 = ConfigurationManager.AppSettings("DNIInvalido2").ToString
            End With

            response = objCAgenciaBancaria.AnularBCP(obeMovimiento)

            response.Respuesta(33) = response.Respuesta(33).ToString

        Catch ex As Exception
            'archivo de log
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            'armando respuesta al bcp
            'If response.Estado = -1 Then TmpRespuesta(33) = "00" & WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
            response.Estado = -1
            TmpRespuesta(33) = "00" & WSBancoMensaje.BCP.Mensaje.Extorno_no_Realizado
            response.Descripcion1 = "Hubo un error en la aplicación."
            response.Descripcion2 = ex.ToString()

        End Try

        Return response
    End Function

End Class