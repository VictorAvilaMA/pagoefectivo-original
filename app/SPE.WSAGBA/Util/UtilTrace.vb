Imports System.Xml
Imports System.Text
Imports System.IO
Imports Microsoft.VisualBasic

Imports SPE.Entidades
Imports SPE.EmsambladoComun

Public Class UtilTrace

    'Public Shared Sub DoTrace(ByVal tipo As String, ByVal accion As String, ByVal log As String, ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String)
    Public Shared Function DoTrace(ByVal origen As String, ByVal accion As String, ByVal log As String) As Int64

        'Dim arrMensajeErr As String() = Nothing
        Dim obeLog As BELog
        Dim idLog As Int64 = 0

        If (ConfigurationManager.AppSettings("HabilitarTraceEventViewer") = "1") Then
            System.Diagnostics.EventLog.WriteEntry("Pago Efectivo - WSBCP", DateTime.Now.ToString + " tipo:" + origen + "| accion:" + accion + "| log:" + log)
        End If

        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using obeCComun As New CComun()

                obeLog = New BELog()

                obeLog.IdTipo = ParametrosSistema.Log.Tipo.idTipoServiciosWebAGBA

                obeLog.Origen = origen + "-" + accion

                obeLog.Descripcion = log

                'arrMensajeErr = log.Split("|")

                'If arrMensajeErr IsNot Nothing Then

                '    If arrMensajeErr.Length = 3 Then

                '        obeLog.Descripcion = "mensaje:" + arrMensajeErr(2).ToString
                '        obeLog.Descripcion2 = arrMensajeErr(0).Replace("mensaje:", "").ToString
                '        obeLog.Parametro1 = arrMensajeErr(1).ToString

                '    End If

                'End If

                idLog = obeCComun.RegistrarLog(obeLog)

            End Using
        End If

        Return idLog

    End Function


    ' dj.- save tabla LOGBCP   

    Public Shared Function DoTraceBCP(ByVal obeLog As BELog) As Int64

        'Dim arrMensajeErr As String() = Nothing
        'Dim obeLog As BELog
        'Dim idLog As Int64 = 0

        With obeLog

            If (ConfigurationManager.AppSettings("HabilitarTraceEventViewer") = "1") Then
                System.Diagnostics.EventLog.WriteEntry("Pago Efectivo - WSBCP", _
                    DateTime.Now.ToString + " tipo:" + .Origen.Split("-")(0).Trim + "| accion:" + _
                    .Origen.Split("-")(1).Replace("-", "").Trim + "| log:" + .Descripcion)
            End If

        End With


        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using obeCComun As New CComun()

                'obeLog = New BELog()

                obeLog.IdTipo = ParametrosSistema.Log.Tipo.idTipoServiciosWebAGBA

                'obeLog.Origen = origen + "-" + IIf(accion = "1", "Exito", IIf(accion = "0", _
                '               "Validacion", "Error"))

                'obeLog.Descripcion = log
                'obeLog.Parametro1 = StrLogRequestBCP

                'arrMensajeErr = log.Split("|")

                'If arrMensajeErr IsNot Nothing Then

                '    If arrMensajeErr.Length = 2 Then

                '        obeLog.Descripcion = "mensaje:" + arrMensajeErr(1).ToString
                '        obeLog.Descripcion2 = arrMensajeErr(0).Replace("mensaje:", "")

                '    End If

                'End If

                Dim idLog As Long = obeCComun.RegistrarLog(obeLog)

                Return idLog

            End Using
        End If

        'Return idLog
        Return 0

    End Function



End Class
