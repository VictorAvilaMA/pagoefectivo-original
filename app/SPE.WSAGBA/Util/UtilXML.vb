Imports Microsoft.VisualBasic
Imports SPE.Entidades

Namespace SPE.Web.Util

    Public Class UtilXML
        Public Sub New()

        End Sub


        Public Shared Function GenerarEstructuraXML(ByVal array() As String) As webServiceXResponse

            Dim WSResponse As New webServiceXResponse()
            WSResponse.webServiceXReturn = New webServiceXResponseWebServiceXReturn()
            WSResponse.webServiceXReturn.stringarray = New webServiceXResponseWebServiceXReturnStringarray
            Dim contador As Int16

            System.Array.Resize(WSResponse.webServiceXReturn.stringarray.Items, array.Length)

            For contador = 0 To array.Length - 1
                If IsNothing(array(contador)) Then
                    WSResponse.webServiceXReturn.stringarray.Items(contador) = New Object
                Else
                    WSResponse.webServiceXReturn.stringarray.Items(contador) = array(contador)
                End If
            Next

            Return WSResponse

        End Function

    End Class

End Namespace