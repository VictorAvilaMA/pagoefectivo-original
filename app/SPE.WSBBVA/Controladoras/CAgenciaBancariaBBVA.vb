Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema.WSBancoMensaje
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web


    Public Class CAgenciaBancariaBBVA
        Implements IDisposable

        Private _listaCodTransaccionBBVA As Dictionary(Of String, String) = Nothing
        Public ReadOnly Property ListaCodTransaccionBBVA() As Dictionary(Of String, String)
            Get
                If (_listaCodTransaccionBBVA Is Nothing) Then
                    _listaCodTransaccionBBVA = New Dictionary(Of String, String)

                    'Generico (Consulta,Pago,Extorno)
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.TransaccionRealizadaExito, "Transacci�n realizada con �xito") ' "0001"

                    'Consulta
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoSePudoRealizarLaTransaccion, "No se pudo realizar la transacci�n") ' "3002"
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.EstadoNumeroReferenciaInvalida, "Estado de N�mero de referencia invalida") '"3013"
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.TramaEnviadaEmpresaInvalida, "Trama enviada a empresa invalida") '"3012"
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NumeroReferenciaNoExiste, "N�mero de referencia no existe") '"0101"
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoTieneDeudasPendientes, "No tiene deudas Pendientes") '"3009"
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NumeroReferenciaEstadoPagado, "N�mero de referencia con estado Pagado") '"0106"
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NUmeroReferenciaEstadoExpirado, "N�mero de referencia con estado expirado") '"0102"

                    'Pago
                    '  _listaCodTransaccionBBVA.Add("0101", "N�mero de referencia  no existe")
                    '_listaCodTransaccionBBVA.Add(BBVA.Mensaje.TramaEnviadaEmpresaInvalida, "Trama enviada a empresa invalida") '"3012"

                    'Extorno
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoSePudoRealizarRegistroExtorno, "No se pudo realizar registro de extorno") '"3004"
                    ' _listaCodTransaccionBBVA.Add("3012", "Trama enviada a empresa invalida")
                    _listaCodTransaccionBBVA.Add(BBVA.Mensaje.NoExistePagoParaExtornar, "No Existe Pago para Extornar") '"3014"
                End If
                Return _listaCodTransaccionBBVA
            End Get
        End Property


        Public Function ConsultarBBVA(ByVal request As BEBBVAConsultarRequest) As BEBBVAResponse(Of BEBBVAConsultarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarBBVA(request)
            End Using
        End Function

        'Public Function PagarBBVA(ByVal request As BEBBVAPagarRequest) As BEBBVAPagarResponse
        '    Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IAgenciaBancaria.PagarBBVA(request)
        'End Function

        Public Function PagarBBVA(ByVal request As BEBBVAPagarRequest) As BEBBVAResponse(Of BEBBVAPagarResponse)

            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).PagarBBVA(request)
            End Using
        End Function

        Public Function AnularBBVA(ByVal request As BEBBVAAnularRequest) As BEBBVAResponse(Of BEBBVAAnularResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).AnularBBVA(request)
            End Using
        End Function

        Public Function ExtornarBBVA(ByVal request As BEBBVAExtornarRequest) As BEBBVAResponse(Of BEBBVAExtornarResponse)

            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarBBVA(request)
            End Using
        End Function

        Public Function ConsultarDataInicial(ByVal request As BEBBVAConsultarRequest) As BEBBVAConsultarResponse

            Dim response As New BEBBVAConsultarResponse
            Dim obeConsDet As New BEBBVAConsultarDetalle

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipocliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigocliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim      ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim 'REFER. DEUDA ADICIONAL
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA
                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA
                .datoCliente = ""                           ' DATO DEL CLIENTE (opcional)
                .cantidadDocumentos = "00"                   ' CANT. DE DOCUMENTOS "0"

                'codigo de resultado para bbva
                .codigoResultado = BBVA.Mensaje.NoSePudoRealizarLaTransaccion

                'mensaje de resultado de bbva
                .mensajeResultado = ListaCodTransaccionBBVA(.codigoResultado)

            End With

            response.detalle = New List(Of BEBBVAConsultarDetalle)

            With obeConsDet
                .numeroReferenciaDocumento = ""         ' NRO. REFER. DOCUMENTO
                .importeDeudaDocumento = ""             ' IMPORTE DEUDA DOCUMENTO
                .importeDeudaMinimaDocumento = ""       ' IMPORTE DEUDA MIN. DOCUMENTO
                .fechaVencimientoDocumento = ""         ' FECH. VENCIMIENTO DOCUMENTO
                .fechaEmisionDocumento = ""             ' FECH. EMISION  DOCUMENTO
                .descripcionDocumento = ""              ' DESCRIPCION DOCUMENTO
                .numeroDocumento = ""                   ' NRO. DEL DOCUMENTO
                .indicadorRestriccPago = ""             ' INDICADOR RESTRICCION PAGO
                .indicadorSituacionDocumento = ""       ' INDICADOR SITUACION DOCUMENTO
                .cantidadSubconceptos = ""              ' CANT. SUB-CONCEPTOS

            End With

            response.detalle.Add(obeConsDet)

            Return response

        End Function

        Public Function PagarDataInicial(ByVal request As BEBBVAPagarRequest) As BEBBVAPagarResponse

            Dim response As New BEBBVAPagarResponse

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim      ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim 'REFER. DEUDA ADICIONAL
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA

                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA

                'codigo de resultado para bbva
                .codigoResultado = BBVA.Mensaje.NoSePudoRealizarLaTransaccion

                'mensaje de resultado de bbva
                .mensajeResultado = ListaCodTransaccionBBVA(.codigoResultado)

            End With

            Return response

        End Function

        Public Function AnularDataInicial(ByVal request As BEBBVAAnularRequest) As BEBBVAAnularResponse

            Dim response As New BEBBVAAnularResponse

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim      ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim 'REFER. DEUDA ADICIONAL
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA

                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA

                'codigo de resultado para bbva
                .codigoResultado = BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

                'mensaje de resultado de bbva
                .mensajeResultado = ListaCodTransaccionBBVA(.codigoResultado)

            End With

            Return response

        End Function

        Public Function ExtornarDataInicial(ByVal request As BEBBVAExtornarRequest) As BEBBVAExtornarResponse

            Dim response As New BEBBVAExtornarResponse

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .codigoOperacion = request.codigoOperacion.Trim  ' CODIGO DE OPERACION
                .numeroOperacion = request.numeroOperacion.Trim  ' NUMERO DE OPERACION
                .codigoBanco = request.codigoBanco.Trim          ' CODIGO DE BANCO
                .codigoConvenio = request.codigoConvenio.Trim    ' CODIGO DE CONVENIO
                .tipoCliente = request.tipoCliente.Trim          ' TIPO DE CLIENTE
                .codigoCliente = request.codigoCliente.Trim      ' CODIGO DEL CLIENTE
                .numeroReferenciaDeuda = request.numeroReferenciaDeuda.Trim      ' NRO. REFERENCIA DEUDA
                .referenciaDeudaAdicional = request.referenciaDeudaAdicional.Trim 'REFER. DEUDA ADICIONAL
                .datosEmpresa = request.datosEmpresa.Trim        ' DATOS DE LA EMPRESA

                .numeroOperacionEmpresa = "0"               ' NRO. DE OPERAC. DE LA EMPRESA

                'codigo de resultado para bbva
                .codigoResultado = BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

                'mensaje de resultado de bbva
                .mensajeResultado = ListaCodTransaccionBBVA(.codigoResultado)

            End With

            Return response

        End Function



        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
