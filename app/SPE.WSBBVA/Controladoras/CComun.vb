Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CComun
        Implements IDisposable

        Public Function RegistrarLog(ByVal obelog As BELog) As Integer
            Using Conexions As New ProxyBase(Of IComun)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).RegistrarLog(obelog)
            End Using
        End Function
        Public Function ConsultarLog(ByVal obelog As BELog) As List(Of BELog)

            Using Conexions As New ProxyBase(Of IComun)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarLog(obelog)
            End Using

        End Function

        'Public Function RegistrarLogBBVA(ByVal origen As String, ByVal accion As String, ByVal log As String, ByVal type As String, ByVal obeid As Int64, ByVal obeBBVA As Object) As Int64
        '    If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
        '        Dim obeLog As New BELog()
        '        obeLog.IdTipo = ParametrosSistema.Log.Tipo.idTipoServiciosWebBBVA
        '        obeLog.Origen = origen + "-" + accion
        '        obeLog.Descripcion = log
        '        Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IComun.RegistrarLogBBVA(type, obeLog, obeid, obeBBVA)
        '    End If
        'End Function

        Public Function RegistrarLogBBVA(ByVal type As String, ByVal obeid As Int64, ByVal obeBBVA As Object, ByVal obeLog As BELog) As Int64
            If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
                Using Conexions As New ProxyBase(Of IComun)
                    Return Conexions.DevolverContrato(New EntityBaseContractResolver()).RegistrarLogBBVA(type, obeLog, obeid, obeBBVA)
                End Using
            End If

        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace