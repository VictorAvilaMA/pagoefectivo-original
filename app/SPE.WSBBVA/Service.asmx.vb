Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO
Imports System.Web.Script.Serialization
Imports SPE.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Exceptions


<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function validarConexion() As String
        Return "OK BBVA"
    End Function

    <WebMethod()> _
    Public Function consultar(ByVal request As BEBBVAConsultarRequest) As BEBBVAConsultarResponse

        Dim response As New BEBBVAResponse(Of BEBBVAConsultarResponse)
        response.ObjBBVA = New BEBBVAConsultarResponse()
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try
            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaBBVA()

                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogBBVA(Log.MetodosBBVA.consultar_request, Nothing, request, obeLog)

                        'consultar CIP
                        response = cntrl.ConsultarBBVA(request)

                        'crear objeto log
                        obeLog = New BELog("consultar CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)

                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException

                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEBBVAResponse(Of BEBBVAConsultarResponse))

                'asignar codigo de resultado
                response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarLaTransaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                End Using

                'crear objeto log
                obeLog = New BELog("consultar CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogBBVA(Log.MetodosBBVA.consultar_response, idLog, response.ObjBBVA, obeLog)
            End Using

        Catch ex As Exception
            'excepcion general

            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjBBVA

            ''codigo de resultado para bbva
            'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarLaTransaccion

            ''mensaje de resultado de bbva
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
            '    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
            'End Using

            If response.Estado = -1 Then
                'codigo de resultado para bbva
                'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarLaTransaccion

                'mensaje de resultado de bbva
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    'response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.ObjBBVA = oCtrlAB.ConsultarDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjBBVA

    End Function

    <WebMethod()> _
    Public Function pagar(ByVal request As BEBBVAPagarRequest) As BEBBVAPagarResponse

        Dim response As New BEBBVAResponse(Of BEBBVAPagarResponse)
        response.ObjBBVA = New BEBBVAPagarResponse()
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try
            UtilTrace.Registrar(String.Format("Numero de Referencia:{0}", request.numeroReferenciaDeuda))
            Dim json = New JavaScriptSerializer().Serialize(request)
            UtilTrace.Registrar(json.ToString())

            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaBBVA()
                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogBBVA(log.MetodosBBVA.pagar_request, Nothing, request, obeLog)

                        'pagar CIP BBVA
                        response = cntrl.PagarBBVA(request)

                        'crear objeto log
                        obeLog = New BELog("pagar CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)
                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException

                UtilTrace.Registrar(ex.Message)

                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de pago
                response = CType(ex.ObjRemoto, BEBBVAResponse(Of BEBBVAPagarResponse))

                'asignar codigo de resultado
                response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarLaTransaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                End Using

                'crear objeto log
                obeLog = New BELog("pagar CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)
            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogBBVA(log.MetodosBBVA.pagar_response, idLog, response.ObjBBVA, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjBBVA

            ''codigo de resultado para bbva
            'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarLaTransaccion

            ''mensaje de resultado de bbva
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
            '    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
            'End Using

            'verificar estado de la consulta

            UtilTrace.Registrar(ex.Message)

            If response.Estado = -1 Then

                'codigo de resultado para bbva
                'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarLaTransaccion

                'mensaje de resultado de bbva
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    'response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.ObjBBVA = oCtrlAB.PagarDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjBBVA

    End Function

    <WebMethod()> _
    Public Function anular(ByVal request As BEBBVAAnularRequest) As BEBBVAAnularResponse
        Dim response As New BEBBVAResponse(Of BEBBVAAnularResponse)
        response.ObjBBVA = New BEBBVAAnularResponse
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try

            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaBBVA()
                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogBBVA(Log.MetodosBBVA.anular_request, Nothing, request, obeLog)
                        'anular cip
                        response = cntrl.AnularBBVA(request)
                        'crear objeto log
                        obeLog = New BELog("anular CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)
                    End Using

                End Using
                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEBBVAResponse(Of BEBBVAAnularResponse))

                'asignar codigo de resultado
                response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                End Using

                'crear objeto log
                obeLog = New BELog("anular CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogBBVA(Log.MetodosBBVA.anular_response, idLog, response.ObjBBVA, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjBBVA

            ''codigo de resultado para bbva
            'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

            ''mensaje de resultado de bbva
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
            '    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
            'End Using

            'verificar estado de la consulta
            If response.Estado = -1 Then

                'codigo de resultado para bbva
                'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

                'mensaje de resultado de bbva
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    'response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                    response.ObjBBVA = oCtrlAB.AnularDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjBBVA

    End Function

    <WebMethod()> _
    Public Function extornar(ByVal request As BEBBVAExtornarRequest) As BEBBVAExtornarResponse
        Dim response As New BEBBVAResponse(Of BEBBVAExtornarResponse)
        response.ObjBBVA = New BEBBVAExtornarResponse
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try

            Try
                Using oCntrlComun As New SPE.Web.CComun
                    Using cntrl As New SPE.Web.CAgenciaBancariaBBVA()
                        'registrar parametros de entrada
                        idLog = oCntrlComun.RegistrarLogBBVA(Log.MetodosBBVA.extornar_request, Nothing, request, obeLog)

                        'extornar CIP
                        response = cntrl.ExtornarBBVA(request)

                        'crear objeto log
                        obeLog = New BELog("extornar CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)

                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEBBVAResponse(Of BEBBVAExtornarResponse))

                'asignar codigo de resultado
                response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.codigoResultado)
                End Using

                'crear objeto log
                obeLog = New BELog("extornar CIP-BBVA-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogBBVA(Log.MetodosBBVA.extornar_response, idLog, response.ObjBBVA, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjBBVA

            ''codigo de resultado para bbva
            'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

            ''mensaje de resultado de bbva
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
            '    response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.mensajeResultado)
            'End Using


            'verificar estado de la consulta
            If response.Estado = (-1) Then

                'codigo de resultado para bbva
                'response.ObjBBVA.codigoResultado = WSBancoMensaje.BBVA.Mensaje.NoSePudoRealizarRegistroExtorno

                'mensaje de resultado de bbva
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBBVA
                    'response.ObjBBVA.mensajeResultado = oCtrlAB.ListaCodTransaccionBBVA(response.ObjBBVA.mensajeResultado)
                    response.ObjBBVA = oCtrlAB.ExtornarDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjBBVA

    End Function

End Class