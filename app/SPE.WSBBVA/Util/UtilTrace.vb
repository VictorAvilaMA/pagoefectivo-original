Imports System.Xml
Imports System.Text
Imports System.IO
Imports Microsoft.VisualBasic

Imports SPE.Entidades
Imports SPE.EmsambladoComun

Public Class UtilTrace

    Private Shared _strRutaLog As String = ConfigurationManager.AppSettings("RutaLog")
    Private Shared ReadOnly StrNombreLog As String = ConfigurationManager.AppSettings("NombreLog")
    Private Shared ReadOnly StrSeRegistra As String = ConfigurationManager.AppSettings("RegistroLog")

    Public Shared Sub Registrar(strMensaje As String)
        Try
            Dim strArchivo As String = String.Empty

            If StrSeRegistra.Equals("1") Then
                _strRutaLog = ObtenerPath(_strRutaLog)
                strArchivo = _strRutaLog + DateTime.Now.ToString("dd-MM-yyyy") + "_" + StrNombreLog

                Using swrLog As StreamWriter = File.AppendText(strArchivo)
                    swrLog.WriteLine(Convert.ToString(DateTime.Now.ToString() + " | ") & strMensaje)
                End Using
            End If
        Catch ex As Exception
            Dim exRegistro As New Exception("UtilTrace.Registrar: Exception - " + ex.Message)

            Using swrErr As StreamWriter = File.AppendText(_strRutaLog + "PE_BBVA_ErrorLog.txt")
                swrErr.WriteLine(Convert.ToString(DateTime.Now.ToString() + " | ") & strMensaje + ex.Message)
            End Using

            Throw (exRegistro)
        End Try
    End Sub

    Public Shared Function ObtenerPath(strRuta As String) As String
        Try
            If Not strRuta.Substring(strRuta.Length - 1, 1).Equals("\") Then
                strRuta += "\"
            End If

            If Not Directory.Exists(strRuta) Then
                Dim exRuta As New Exception("UtilTrace.ObtenerPath: Ruta no existe")
                Throw (exRuta)
            End If
        Catch ex As Exception
            Dim exRuta As New Exception("UtilTrace.ObtenerPath: " + ex.Message)
            Throw (exRuta)
        End Try

        Return strRuta
    End Function

    'Public Shared Sub DoTrace(ByVal tipo As String, ByVal accion As String, ByVal log As String, ByVal param1 As String, ByVal param2 As String, ByVal param3 As String, ByVal param4 As String, ByVal param5 As String)

    'Public Shared Sub DoTrace(ByVal origen As String, ByVal accion As String, ByVal Strlog As String, ByVal StrLogRequestBBVA As String)

    '    Dim arrMensajeErr As String() = Nothing
    '    Dim obeLog As BELog

    '    If (ConfigurationManager.AppSettings("HabilitarTraceEventViewer") = "1") Then
    '        System.Diagnostics.EventLog.WriteEntry("Pago Efectivo - WSBBVA", DateTime.Now.ToString + " tipo:" + origen + "| accion:" + accion + "| log:" + Strlog)
    '    End If

    '    If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then

    '        Using obeCComun As New SPE.Web.CComun()

    '            obeLog = New BELog()

    '            obeLog.IdTipo = ParametrosSistema.Log.Tipo.idTipoServiciosWebBBVA

    '            obeLog.Origen = origen + "-" + IIf(accion = "1", "Exito", IIf(accion = "0", _
    '                            "Validacion", "Error"))

    '            obeLog.Descripcion = Strlog
    '            obeLog.Parametro1 = StrLogRequestBBVA

    '            arrMensajeErr = Strlog.Split("|")

    '            If arrMensajeErr IsNot Nothing Then

    '                If arrMensajeErr.Length = 2 Then

    '                    obeLog.Descripcion = "mensaje:" + arrMensajeErr(1).ToString
    '                    obeLog.Descripcion2 = arrMensajeErr(0).Replace("mensaje:", "")

    '                End If

    '            End If

    '            obeCComun.RegistrarLogBBVA(ParametrosSistema.Log.MetodosBBVA.log_bbva, Nothing, obeLog)

    '        End Using

    '    End If

    'End Sub

End Class
