﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema.WSBancoMensaje
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CAgenciaBancariaBanBif
        Implements IDisposable

        Private _listaCodTransaccionBanBif As Dictionary(Of String, String) = Nothing
        Public ReadOnly Property ListaCodTransaccionBanBif() As Dictionary(Of String, String)
            Get
                If (_listaCodTransaccionBanBif Is Nothing) Then
                    _listaCodTransaccionBanBif = New Dictionary(Of String, String)

                    'Generico (Consulta,Pago,Extorno)
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Proceso_Conforme, "Proceso Conforme")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Deuda_Con_Restricciones, "Deuda con restricciones")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Deuda_No_Valida, "Deuda no Valida")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Error_Proceso_Transaccion, "Error en proceso de transacción")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Limite_Pago_Superado, "Limite de pago superado")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Monto_Pagar_errado, "Monto a pagar errado")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.No_Procede_Extorno_Entidad, "No procede extorno por indicacion de la entidad")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.No_Procede_Pago_Entidad, "No procede pago por indicacion de la entidad")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Sin_Deuda_Pendiente, "Sin deuda pendiente")
                    _listaCodTransaccionBanBif.Add(BanBif.CodigosRespuesta.Trama_Invalida, "Trama enviada invalida")

                End If
                Return _listaCodTransaccionBanBif
            End Get
        End Property

        Public Function ConsultarBanBif(ByVal request As BEBanBifConsultarRequest) As BEBanBifResponse(Of BEBanBifConsultarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarBanBif(request)
            End Using
        End Function

        Public Function PagarBanBif(ByVal request As BEBanBifPagarRequest) As BEBanBifResponse(Of BEBanBifPagarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).PagarBanBif(request)
            End Using
        End Function

        Public Function ExtornarBanBif(ByVal request As BEBanBifExtornoRequest) As BEBanBifResponse(Of BEBanBifExtornoResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarBanBif(request)
            End Using
        End Function

        Public Function ConsultarDataInicial(ByVal request As BEBanBifConsultarRequest) As BEBanBifConsultarResponse

            Dim response As New BEBanBifConsultarResponse
            Dim obeConsDet As New BEBanBifConsultarDetalle
            response.detalle = New List(Of BEBanBifConsultarDetalle)
            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .RqUID = request.RqUID ' CODIGO 
                .Login = request.Login ' CODIGO 
                .SegRol = request.SegRol
                .ServFec = request.ClientFec '
                .Spcod = request.Spcod
                .PagId = request.PagId
                .Clasificacion = request.Clasificacion ' CODIGO DE OPERACION
                .Ctd = 1  ' CODIGO DE OPERACION
                .OpnNro = request.OpnNro

                'codigo de resultado para BanBif
                .StatusCod = BanBif.TipoRespuesta.Errores
                .ServStatusCod = BanBif.CodigosRespuesta.Error_Proceso_Transaccion

                '.Severidad = ListaCodTransaccionBanBif(.StatusCod)
                .StatusDes = ListaCodTransaccionBanBif(.ServStatusCod)

            End With

            response.detalle = New List(Of BEBanBifConsultarDetalle)

            With obeConsDet
                .Recibo = ""         ' NRO. REFER. DOCUMENTO
                .SvcId = ""             ' IMPORTE DEUDA DOCUMENTO
                .Descripcion = ""       ' IMPORTE DEUDA MIN. DOCUMENTO
                .FecVct = ""         ' FECH. VENCIMIENTO DOCUMENTO
                .FecEmision = ""             ' FECH. EMISION  DOCUMENTO
                .Total = 0              ' DESCRIPCION DOCUMENTO
                .Ctd = 0                   ' NRO. DEL DOCUMENTO
                .subConceptos = New List(Of BEBanBifConsultarSubConcepto)
            End With

            response.detalle.Add(obeConsDet)

            Return response

        End Function

        Public Function PagarDataInicial(ByVal request As BEBanBifPagarRequest) As BEBanBifPagarResponse

            Dim response As New BEBanBifPagarResponse

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .ExtornoTp = request.ExtornoTp.Trim
                .RqUID = request.RqUID.Trim
                .Login = request.Login.Trim
                .SegRol = request.SegRol.Trim
                .ServFec = request.ClientFec.Trim
                .Ctd = 1
                .SvcId = request.SvcId.Trim
                .Spcod = request.Spcod.Trim
                .PagId = request.PagId.Trim
                .Recibo = request.Recibo ' NRO. DE OPERAC. DE LA EMPRESA
                .Clasificacion = request.Clasificacion

                .StatusCod = BanBif.TipoRespuesta.Errores
                .ServStatusCod = BanBif.CodigosRespuesta.Error_Proceso_Transaccion

                '.Severidad = ListaCodTransaccionBanBif(.StatusCod)
                .StatusDes = ListaCodTransaccionBanBif(.ServStatusCod)

            End With

            Return response

        End Function

        Public Function ExtornarDataInicial(ByVal request As BEBanBifExtornoRequest) As BEBanBifExtornoResponse

            Dim response As New BEBanBifExtornoResponse

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .ExtornoTp = request.ExtornoTp.Trim
                .RqUID = request.RqUID.Trim
                .Login = request.Login.Trim
                .SegRol = request.SegRol.Trim
                .ServFec = request.ClientFec.Trim
                .Ctd = 1
                .SvcId = request.SvcId.Trim
                .Spcod = request.Spcod.Trim
                .PagId = request.PagId.Trim
                .Recibo = request.Recibo ' NRO. DE OPERAC. DE LA EMPRESA
                .Clasificacion = request.Clasificacion

                .StatusCod = BanBif.TipoRespuesta.Errores
                .ServStatusCod = BanBif.CodigosRespuesta.Error_Proceso_Transaccion

                '.Severidad = ListaCodTransaccionBanBif(.StatusCod)
                '.StatusDes = ListaCodTransaccionBanBif(.ServStatusCod)


            End With

            Return response

        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace