﻿
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Exceptions


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function validarConexion() As String
        Return "BanBif: Conexión correcta."
    End Function

    <WebMethod()> _
    Public Function consultar(ByVal request As BEBanBifConsultarRequest) As BEBanBifConsultarResponse

        Dim response As New BEBanBifResponse(Of BEBanBifConsultarResponse)
        response.ObjBanBif = New BEBanBifConsultarResponse()
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing
        Dim zeros As String = "00000000000000"

        Dim stri As String
        'la logica de recorte de ceros sera aca
        request.PagId = request.PagId.TrimStart("0")
        'If request.PagId.Length <= 20 And request.PagId.Length >= 8 Then
        '    request.PagId = Mid(request.PagId, request.PagId.Length - 7 + 1, 7)
        'End If

        'Select Case request.PagId.Length
        '    Case 20
        '        request.PagId = Mid(request.PagId, 14, 7)
        '    Case 19
        '        request.PagId = Mid(request.PagId, 13, 7)
        '    Case 18
        '        request.PagId = Mid(request.PagId, 12, 7)
        '    Case 17
        '        request.PagId = Mid(request.PagId, 11, 7)
        '    Case 16
        '        request.PagId = Mid(request.PagId, 10, 7)
        '    Case 15
        '        request.PagId = Mid(request.PagId, 9, 7)
        '    Case 14
        '        request.PagId = Mid(request.PagId, 8, 7)
        '    Case 13
        '        request.PagId = Mid(request.PagId, 7, 7)
        '    Case 12
        '        request.PagId = Mid(request.PagId, 6, 7)
        '    Case 11
        '        request.PagId = Mid(request.PagId, 5, 7)
        '    Case 10
        '        request.PagId = Mid(request.PagId, 4, 7)
        '    Case 9
        '        request.PagId = Mid(request.PagId, 3, 7)
        '    Case 8
        '        request.PagId = Mid(request.PagId, 2, 7)
        'End Select

      

        Try
            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaBanBif()

                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogBanBif(Log.MetodosBanBif.consultar_request, Nothing, request, obeLog)

                        'consultar CIP
                        If request.RqUID.Length > 0 And request.RqUID.Length <= 36 And request.Login.Length > 0 And request.Login.Length <= 8 And request.SegRol.Length > 0 And request.SegRol.Length <= 17 And request.OpnNro.Length > 0 And request.OpnNro.Length <= 20 _
                          And request.OrgCod.Length > 0 And request.OrgCod.Length <= 4 And request.ClientCod.Length > 0 And request.ClientCod.Length <= 2 And request.SvcId.Length > 0 And request.SvcId.Length <= 3 And request.Spcod.Length > 0 And request.Spcod.Length <= 9 _
                          And request.PagId.Length > 0 And request.PagId.Length <= 20 And request.Clasificacion.Length > 0 And request.Clasificacion.Length <= 4 Then

                            response = cntrl.ConsultarBanBif(request)
                            response.ObjBanBif.detalle(0).DocRef = request.PagId.ToString
                            response.ObjBanBif.detalle(0).Recibo = request.PagId.ToString
                            stri = response.ObjBanBif.PagId
                            If stri.Length <= 14 Then
                                response.ObjBanBif.PagId = zeros.Substring(0, zeros.Length - stri.Length) + stri
                            ElseIf stri.Length > 14 Then
                                response.ObjBanBif.PagId = stri.Substring(stri.Length - zeros.Length, 14)
                            End If

                            'crear objeto log
                            obeLog = New BELog("consultar CIP-BanBif-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                            "Validacion", "Error")), response.Mensaje, Nothing)
                        Else
                            'obtener objeto remoto de consulta

                            'asignar codigo de resultado
                            response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Advertencia_Otros
                            response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Trama_Invalida
                            response.ObjBanBif.Severidad = "Advertencia/Otros"
                            'asignar mensaje de resultado
                            Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                                'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                                response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                  "Validacion", "Error")), response.Mensaje, Nothing)

                            End Using
                        End If







                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException

                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEBanBifResponse(Of BEBanBifConsultarResponse))

                'asignar codigo de resultado
                response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Errores
                response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Error_Proceso_Transaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                    response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                End Using

                'crear objeto log
                obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                   "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try


            Using oCtrlComun As New SPE.Web.CComun

               'registrar parametros de salida
                oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.consultar_response, idLog, response.ObjBanBif, obeLog)
            End Using

        Catch ex As Exception

            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            If response.Estado = -1 Then
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                    response.ObjBanBif = oCtrlAB.ConsultarDataInicial(request)
                End Using



                Using oCtrlComun As New SPE.Web.CComun


                    'asignar codigo de resultado
                    response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Errores
                    response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Error_Proceso_Transaccion
                    response.ObjBanBif.Severidad = "Errores"
                    'asignar mensaje de resultado
                    Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                        'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                        response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                        obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                          "Validacion", "Error")), response.Mensaje, Nothing)

                    End Using


                    'registrar parametros de entrada
                    idLog = oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.consultar_request, Nothing, request, obeLog)
                    'registrar parametros de salida
                    oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.consultar_response, idLog, response.ObjBanBif, obeLog)
                End Using
            End If

        End Try

        Return response.ObjBanBif

    End Function

    'Public Function PagoExtorno(ByVal request As BEBanBifPagoExtornoRequest) As BEBanBifPagoExtornoResponse
    '    Dim BEPagoExtornoResp As New BEBanBifPagoExtornoResponse
    '    If request.ExtornoTp = "P" Then
    '        BEPagoExtornoResp = pagar(request)
    '    ElseIf request.ExtornoTp = "E" Or request.ExtornoTp = "A" Then
    '        BEPagoExtornoResp = extornar(request)
    '    End If
    '    Return BEPagoExtornoResp
    'End Function

    <WebMethod()> _
    Public Function pagar(ByVal request As BEBanBifPagarRequest) As BEBanBifPagarResponse

        Dim response As New BEBanBifResponse(Of BEBanBifPagarResponse)
        response.ObjBanBif = New BEBanBifPagarResponse()
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing
        Dim zeros As String = "00000000000000"

        Dim stri As String

        request.ExtornoTp = UCase(request.ExtornoTp)
        If request.ExtornoTp.Equals("A") Then

            Dim requestExtornar As New BEBanBifExtornoRequest
            Dim responseExtornar As New BEBanBifExtornoResponse

            requestExtornar.RqUID = request.RqUID
            requestExtornar.Login = request.Login
            requestExtornar.SegRol = request.SegRol
            requestExtornar.ExtornoTp = request.ExtornoTp
            requestExtornar.ClientFec = request.ClientFec
            requestExtornar.OpnNro = request.OpnNro
            requestExtornar.OrgCod = request.OrgCod
            requestExtornar.ClientCod = request.ClientCod
            requestExtornar.Ctd = request.Ctd
            requestExtornar.SvcId = request.SvcId
            requestExtornar.Spcod = request.Spcod
            requestExtornar.PagId = request.PagId
            requestExtornar.Recibo = request.Recibo
            requestExtornar.Clasificacion = request.Clasificacion
            requestExtornar.MedioAbonoCod = request.MedioAbonoCod
            requestExtornar.Moneda = request.Moneda
            requestExtornar.Mto = request.Mto

            responseExtornar = extornar(requestExtornar)

            response.ObjBanBif.RqUID = responseExtornar.RqUID
            response.ObjBanBif.ExtornoTp = responseExtornar.ExtornoTp
            response.ObjBanBif.Login = responseExtornar.Login
            response.ObjBanBif.SegRol = responseExtornar.SegRol
            response.ObjBanBif.ServFec = responseExtornar.ServFec
            response.ObjBanBif.OpnNro = responseExtornar.OpnNro
            response.ObjBanBif.Ctd = responseExtornar.Ctd
            response.ObjBanBif.SvcId = responseExtornar.SvcId
            response.ObjBanBif.Spcod = responseExtornar.Spcod
            stri = responseExtornar.PagId
            If stri.Length <= 14 Then
                response.ObjBanBif.PagId = zeros.Substring(0, zeros.Length - stri.Length) + stri
            ElseIf stri.Length > 14 Then
                response.ObjBanBif.PagId = stri.Substring(stri.Length - zeros.Length, 14)
            End If
            'response.ObjBanBif.PagId = responseExtornar.PagId
            response.ObjBanBif.Recibo = responseExtornar.Recibo
            response.ObjBanBif.Clasificacion = responseExtornar.Clasificacion
            response.ObjBanBif.Moneda = responseExtornar.Moneda
            response.ObjBanBif.Mto = responseExtornar.Mto
            response.ObjBanBif.StatusCod = responseExtornar.StatusCod
            response.ObjBanBif.ServStatusCod = responseExtornar.ServStatusCod
            response.ObjBanBif.Severidad = responseExtornar.Severidad
            response.ObjBanBif.StatusDes = responseExtornar.StatusDes
           


            Return response.ObjBanBif
        End If

        Try

            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaBanBif()
                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogBanBif(Log.MetodosBanBif.pagar_request, Nothing, request, obeLog)

                        request.PagId.TrimEnd()
                        'request.Recibo.TrimEnd()
                        'If request.PagId.Length = 14 Then
                        '    request.PagId = Mid(request.PagId, 8, 14)
                        'ElseIf request.PagId.Length = 9 Then
                        '    request.PagId = Mid(request.PagId, 3, 9)
                        'End If
                        'If request.Recibo.Length = 14 Then
                        '    request.Recibo = Mid(request.Recibo, 8, 14)
                        'ElseIf request.Recibo.Length = 9 Then
                        '    request.Recibo = Mid(request.Recibo, 3, 9)
                        'End If
                        Select Case request.PagId.Length
                            Case 20
                                request.PagId = Mid(request.PagId, 14, 7)
                            Case 19
                                request.PagId = Mid(request.PagId, 13, 7)
                            Case 18
                                request.PagId = Mid(request.PagId, 12, 7)
                            Case 17
                                request.PagId = Mid(request.PagId, 11, 7)
                            Case 16
                                request.PagId = Mid(request.PagId, 10, 7)
                            Case 15
                                request.PagId = Mid(request.PagId, 9, 7)
                            Case 14
                                request.PagId = Mid(request.PagId, 8, 7)
                            Case 13
                                request.PagId = Mid(request.PagId, 7, 7)
                            Case 12
                                request.PagId = Mid(request.PagId, 6, 7)
                            Case 11
                                request.PagId = Mid(request.PagId, 5, 7)
                            Case 10
                                request.PagId = Mid(request.PagId, 4, 7)
                            Case 9
                                request.PagId = Mid(request.PagId, 3, 7)
                            Case 8
                                request.PagId = Mid(request.PagId, 2, 7)
                        End Select

                        Select Case request.Recibo.Length
                            Case 9
                                request.Recibo = Mid(request.Recibo, 3, 7)
                            Case 8
                                request.Recibo = Mid(request.Recibo, 2, 7)
                        End Select

                        request.Recibo = Convert.ToInt64(request.Recibo.ToString).ToString()

                        request.ExtornoTp = UCase(request.ExtornoTp)

                        'Nueva validación - Nro Recibo sin ceros adelante

                        Dim numericCheck As Boolean = IsNumeric(request.Ctd)

                        If (request.RqUID.Length > 0 And request.RqUID.Length <= 36 And request.Login.Length > 0 And request.Login.Length <= 8 And request.SegRol.Length > 0 And request.SegRol.Length <= 17 And request.ExtornoTp.Length > 0 And request.ExtornoTp.Length = 1 And request.ExtornoTp = "P" Or request.ExtornoTp = "p" _
                           And request.PagId.Length > 0 And request.PagId.Length <= 20 _
                           And request.Moneda.Length > 0 And request.Moneda.Length <= 4 And request.MedioAbonoCod.Length > 0 And request.MedioAbonoCod.Length <= 2) Then

                            If (request.Mto.Length > 0 And request.Ctd.Length > 0 And request.Ctd.Length <= 19 And numericCheck = True And request.OpnNro.Length > 0 And request.OpnNro.Length <= 8 And request.Recibo.Length > 0 And request.Recibo.Length <= 9 And request.OrgCod.Length > 0 And request.OrgCod.Length <= 4 And request.ClientCod.Length > 0 And request.ClientCod.Length = 2 And request.SvcId.Length > 0 And request.SvcId.Length = 3 And request.Clasificacion.Length > 0 And request.Clasificacion.Length <= 4 And request.Spcod.Length > 0 And request.Spcod.Length <= 9) Then
                                'pagar CIP Banbif

                                response = cntrl.PagarBanBif(request)
                                stri = response.ObjBanBif.PagId
                                If stri.Length <= 14 Then
                                    response.ObjBanBif.PagId = zeros.Substring(0, zeros.Length - stri.Length) + stri
                                ElseIf stri.Length > 14 Then
                                    response.ObjBanBif.PagId = stri.Substring(stri.Length - zeros.Length, 14)
                                End If
                                'crear objeto log
                                obeLog = New BELog("pagar CIP-Banbif-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                        "Validacion", "Error")), response.Mensaje, Nothing)


                            Else

                                'asignar codigo de resultado
                                response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Advertencia_Otros
                                response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Trama_Invalida
                                response.ObjBanBif.ServFec = String.Empty
                                response.ObjBanBif.Severidad = "Advertencia/Otros"
                                'asignar mensaje de resultado
                                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                                    'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)

                                    response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                    obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                      "Validacion", "Error")), response.Mensaje, Nothing)

                                End Using

                            End If
                        Else

                            'asignar codigo de resultado
                            response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Advertencia_Otros
                            response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Trama_Invalida
                            response.ObjBanBif.ServFec = String.Empty
                            response.ObjBanBif.Severidad = "Advertencia/Otros"
                            'asignar mensaje de resultado
                            Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                                'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)

                                response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                  "Validacion", "Error")), response.Mensaje, Nothing)
                            End Using
                        End If



                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de pago
                response = CType(ex.ObjRemoto, BEBanBifResponse(Of BEBanBifPagarResponse))

                'asignar codigo de resultado
                'asignar codigo de resultado
                response.ObjBanBif.ServFec = Nothing
                response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Errores
                response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Error_Proceso_Transaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                    response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                End Using

                'crear objeto log
                obeLog = New BELog("pagar CIP-BanBif-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)
            End Try


            Using oCtrlComun As New SPE.Web.CComun


                'registrar parametros de salida
                oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.pagar_response, idLog, response.ObjBanBif, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            'verificar estado de la consulta

            If response.Estado = -1 Then
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                    response.ObjBanBif = oCtrlAB.PagarDataInicial(request)
                End Using


                Using oCtrlComun As New SPE.Web.CComun


                    'asignar codigo de resultado
                    response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Errores
                    response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Error_Proceso_Transaccion
                    response.ObjBanBif.ServFec = String.Empty
                    response.ObjBanBif.Severidad = "Errores"
                    'asignar mensaje de resultado
                    Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                        'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)

                        response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                        obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                          "Validacion", "Error")), response.Mensaje, Nothing)
                    End Using

                    'registrar parametros de entrada
                    idLog = oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.pagar_request, Nothing, request, obeLog)

                    'registrar parametros de salida
                    oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.pagar_response, idLog, response.ObjBanBif, obeLog)
                End Using
            End If


        End Try

        Return response.ObjBanBif

    End Function

    <WebMethod()> _
    Public Function extornar(ByVal request As BEBanBifExtornoRequest) As BEBanBifExtornoResponse
        Dim response As New BEBanBifResponse(Of BEBanBifExtornoResponse)
        response.ObjBanBif = New BEBanBifExtornoResponse
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing
        Dim zeros As String = "00000000000000"

        Dim stri As String

        Try

            Try

                Using oCntrlComun As New SPE.Web.CComun
                    Using cntrl As New SPE.Web.CAgenciaBancariaBanBif()
                        'registrar parametros de entrada
                        idLog = oCntrlComun.RegistrarLogBanBif(Log.MetodosBanBif.extornar_request, Nothing, request, obeLog)
                        'request.Recibo.TrimEnd()
                        'If request.Recibo.Length = 14 Then
                        '    request.Recibo = Mid(request.Recibo, 8, 14)
                        'ElseIf request.Recibo.Length = 9 Then
                        '    request.Recibo = Mid(request.Recibo, 3, 9)
                        'End If
                        request.PagId.TrimEnd()
                        'If request.PagId.Length = 14 Then
                        '    request.PagId = Mid(request.PagId, 8, 14)
                        'ElseIf request.PagId.Length = 9 Then
                        '    request.PagId = Mid(request.PagId, 3, 9)
                        'End If

                        Select Case request.PagId.Length
                            Case 20
                                request.PagId = Mid(request.PagId, 14, 7)
                            Case 19
                                request.PagId = Mid(request.PagId, 13, 7)
                            Case 18
                                request.PagId = Mid(request.PagId, 12, 7)
                            Case 17
                                request.PagId = Mid(request.PagId, 11, 7)
                            Case 16
                                request.PagId = Mid(request.PagId, 10, 7)
                            Case 15
                                request.PagId = Mid(request.PagId, 9, 7)
                            Case 14
                                request.PagId = Mid(request.PagId, 8, 7)
                            Case 13
                                request.PagId = Mid(request.PagId, 7, 7)
                            Case 12
                                request.PagId = Mid(request.PagId, 6, 7)
                            Case 11
                                request.PagId = Mid(request.PagId, 5, 7)
                            Case 10
                                request.PagId = Mid(request.PagId, 4, 7)
                            Case 9
                                request.PagId = Mid(request.PagId, 3, 7)
                            Case 8
                                request.PagId = Mid(request.PagId, 2, 7)
                        End Select

                        Select Case request.Recibo.Length
                            Case 9
                                request.Recibo = Mid(request.Recibo, 3, 7)
                            Case 8
                                request.Recibo = Mid(request.Recibo, 2, 7)
                        End Select

                        request.Recibo = Convert.ToInt64(request.Recibo.ToString).ToString()

                        request.ExtornoTp = UCase(request.ExtornoTp)

                        Dim numericCheck As Boolean = IsNumeric(request.Ctd)

                        If (request.RqUID.Length > 0 And request.RqUID.Length <= 36 And request.Login.Length > 0 And request.Login.Length <= 8 And request.SegRol.Length > 0 And request.SegRol.Length <= 17 And request.ExtornoTp.Length > 0 And request.ExtornoTp.Length <= 1 And request.ExtornoTp = "E" Or request.ExtornoTp = "A" _
                            And request.PagId.Length > 0 And request.PagId.Length <= 20 And request.MedioAbonoCod.Length > 0 And request.MedioAbonoCod.Length <= 2 _
                            And request.Moneda.Length > 0 And request.Moneda.Length <= 4) Then

                            If (request.Mto.Length > 0 And request.Ctd.Length > 0 And request.Ctd.Length <= 19 And numericCheck = True And request.OpnNro.Length > 0 And request.OpnNro.Length <= 8 And request.Recibo.Length > 0 And request.Recibo.Length <= 9 And request.OrgCod.Length > 0 And request.OrgCod.Length <= 4 And request.ClientCod.Length > 0 And request.ClientCod.Length <= 2 And request.SvcId.Length > 0 And request.SvcId.Length <= 3 And request.Clasificacion.Length > 0 And request.Clasificacion.Length <= 4 And request.Spcod.Length > 0 And request.Spcod.Length <= 9) Then
                                'extornar CIP

                                response = cntrl.ExtornarBanBif(request)
                                stri = response.ObjBanBif.PagId
                                If stri.Length <= 14 Then
                                    response.ObjBanBif.PagId = zeros.Substring(0, zeros.Length - stri.Length) + stri
                                ElseIf stri.Length > 14 Then
                                    response.ObjBanBif.PagId = stri.Substring(stri.Length - zeros.Length, 14)
                                End If
                                'crear objeto log
                                obeLog = New BELog("extornar CIP-BanBif-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                        "Validacion", "Error")), response.Mensaje, Nothing)
                            Else

                                'asignar codigo de resultado
                                response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Advertencia_Otros
                                response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Trama_Invalida
                                response.ObjBanBif.ServFec = String.Empty
                                response.ObjBanBif.Severidad = "Advertencia/Otros"
                                'asignar mensaje de resultado
                                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif

                                    'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                                    response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                    obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                      "Validacion", "Error")), response.Mensaje, Nothing)

                                End Using
                            End If
                        Else

                            'asignar codigo de resultado
                            response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Advertencia_Otros
                            response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Trama_Invalida
                            response.ObjBanBif.ServFec = String.Empty
                            response.ObjBanBif.Severidad = "Advertencia/Otros"
                            'asignar mensaje de resultado
                            Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                                'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)

                                response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                                obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                  "Validacion", "Error")), response.Mensaje, Nothing)
                            End Using

                        End If




                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEBanBifResponse(Of BEBanBifExtornoResponse))

                'asignar codigo de resultado
                response.ObjBanBif.ServFec = Nothing
                response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Errores
                response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Error_Proceso_Transaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                    response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)
                    response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                End Using

                'crear objeto log
                obeLog = New BELog("extornar CIP-BanBif-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try



            Using oCtrlComun As New SPE.Web.CComun


                'registrar parametros de salida
                oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.extornar_response, idLog, response.ObjBanBif, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            If response.Estado = (-1) Then

                Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif

                    'response.ObjBanBif.mensajeResultado = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.mensajeResultado)
                    response.ObjBanBif = oCtrlAB.ExtornarDataInicial(request)
                End Using

                Using oCtrlComun As New SPE.Web.CComun

                    'asignar codigo de resultado
                    response.ObjBanBif.StatusCod = WSBancoMensaje.BanBif.TipoRespuesta.Errores
                    response.ObjBanBif.ServStatusCod = WSBancoMensaje.BanBif.CodigosRespuesta.Error_Proceso_Transaccion
                    response.ObjBanBif.ServFec = String.Empty
                    response.ObjBanBif.Severidad = "Errores"
                    'asignar mensaje de resultado
                    Using oCtrlAB As New SPE.Web.CAgenciaBancariaBanBif
                        'response.ObjBanBif.Severidad = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.StatusCod)

                        response.ObjBanBif.StatusDes = oCtrlAB.ListaCodTransaccionBanBif(response.ObjBanBif.ServStatusCod)
                        obeLog = New BELog("consultar CIP-BANBIF-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                          "Validacion", "Error")), response.Mensaje, Nothing)
                    End Using


                    'registrar parametros de entrada
                    idLog = oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.extornar_request, Nothing, request, obeLog)

                    'registrar parametros de salida
                    oCtrlComun.RegistrarLogBanBif(Log.MetodosBanBif.extornar_response, idLog, response.ObjBanBif, obeLog)

                End Using

                
            End If

        End Try

        Return response.ObjBanBif

    End Function

End Class
