﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;


namespace SPE.WSEncriptamiento
{
    /// <summary>
    /// Summary description for WSCrypto
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSCrypto : System.Web.Services.WebService
    {
        public string HabilitarLog = ConfigurationManager.AppSettings["HabilitarLog"];
        public string RutaLog = ConfigurationManager.AppSettings["rutaLog"];

        [WebMethod]
        public string EncryptText(string plainText, byte[] publicKey)
        {
            //EscribirLog(plainText);
            //string result = "PublicKey.Length: " + publicKey.Length + " | Value[n-1]: " + publicKey[publicKey.Length - 1];
            //EscribirLog(result);
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePublicaContraparte = publicKey;
            String[] textEncript = encripter.Cifrar(plainText);
            return textEncript[0] + "|" + textEncript[1];
        }

        [WebMethod]
        public string DecryptText(string encryptText, byte[] privateKey)
        {
            //EscribirLog(encryptText);
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePrivada = privateKey;
            String[] textEncript = encryptText.Split('|');
            return encripter.DesCifrar(textEncript[0], textEncript[1]);
        }

        [WebMethod]
        public string Signer(string plainText, byte[] privateKey)
        {
            //EscribirLog(plainText);
            //string result = "PrivateKey.Length: " + privateKey.Length + " | Value[n-1]: " + privateKey[privateKey.Length - 1];
            //EscribirLog(result);
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePrivada = privateKey;
            return encripter.Firmar(plainText);
        }

        public void EscribirLog(string mensaje)
        {
            if (HabilitarLog == "1")
            {
                using (StreamWriter swrErr = File.AppendText(RutaLog))
                {
                    swrErr.WriteLine(DateTime.Now + " | " + mensaje.Trim());
                }
            }
        }

        [WebMethod]
        public bool SignerVal(string plainText, string signerText, byte[] publicKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePublicaContraparte = publicKey;
            return encripter.ValidarFirma(plainText, signerText);
        }

        //[WebMethod]
        //public string encryptTshak(string plainText)
        //{
        //    if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();

        //    if (plainText.Length == 7) plainText = "0000000" + plainText;
        //    if (plainText.Length == 14)
        //    {

        //        TSHAK.Components.SecureQueryString querySecure = default(TSHAK.Components.SecureQueryString);
        //        querySecure = new TSHAK.Components.SecureQueryString(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8 });
        //        querySecure["OP"] = plainText.ToString();

        //        return System.Web.HttpUtility.UrlEncode(querySecure.ToString());
        //    }
        //    else return "Error con el CIP Enviado";
        //}

        //[WebMethod]
        //public string decryptTshak(string querySecure)
        //{
        //    try
        //    {

        //        TSHAK.Components.SecureQueryString qs = default(TSHAK.Components.SecureQueryString);
        //        qs = new TSHAK.Components.SecureQueryString(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8 }, System.Web.HttpUtility.UrlDecode(querySecure));
        //        return qs["OP"].ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        return "CIP inválido";
        //    }

        //}
    }
}
