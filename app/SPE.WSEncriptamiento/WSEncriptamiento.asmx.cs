﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SPE.WSEncriptamiento
{
    /// <summary>
    /// Summary description for WSEncriptamiento
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSEncriptamiento : System.Web.Services.WebService
    {

        [WebMethod]
        public string EncryptText(string plainText, byte[] publicKey)
        {
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePublicaContraparte = publicKey;
            String[] textEncript = encripter.Cifrar(plainText);
            return textEncript[0] + "|" + textEncript[1];
        }

        [WebMethod]
        public string DecryptText(string encryptText, byte[] privateKey)
        {
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePrivada = privateKey;
            String[] textEncript = encryptText.Split('|');
            return encripter.DesCifrar(textEncript[0], textEncript[1]);
        }

        [WebMethod]
        public string Signer(string plainText, byte[] privateKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePrivada = privateKey;
            return encripter.Firmar(plainText);
        }

        [WebMethod]
        public bool SignerVal(string plainText, string signerText, byte[] publicKey)
        {
            if (!String.IsNullOrEmpty(plainText)) plainText = plainText.Trim();
            SPE.Criptography.Encripter encripter = new SPE.Criptography.Encripter();
            encripter.clavePublicaContraparte = publicKey;
            return encripter.ValidarFirma(plainText, signerText);
        }
    }
}
