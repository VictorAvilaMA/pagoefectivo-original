Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Namespace SPE.Web
    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices
        Private _IComun As IComun
        Private _IPuntoVenta As IPuntoVenta

        Public Property IPuntoVenta() As IPuntoVenta
            Get
                Return _IPuntoVenta
            End Get
            Set(ByVal value As IPuntoVenta)
                _IPuntoVenta = value
            End Set
        End Property

        Public Property IComun() As IComun
            Get
                Return _IComun
            End Get
            Set(ByVal value As IComun)
                _IComun = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()
            MyBase.DoSetupNetworkEnviroment()
            Me.IComun = CType(Activator.GetObject(GetType(IComun), Me.ServerUrl + "/" + NombreServiciosConocidos.IComun), IComun)
            Me.IPuntoVenta = CType(Activator.GetObject(GetType(IPuntoVenta), Me.ServerUrl + "/" + NombreServiciosConocidos.IPuntoVenta), IPuntoVenta)
        End Sub

    End Class

End Namespace