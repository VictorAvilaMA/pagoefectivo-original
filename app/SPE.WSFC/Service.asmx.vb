Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService

    '<WebMethod()> _
    Public Function validarConexion() As String
        Return "OK FullCarga"
    End Function

    <WebMethod()> _
    Public Function Consultar(ByVal request As BEFCConsultarRequest) As BEFCConsultarResponse
        Dim response As New BEFCConsultarResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogFC(Log.MetodosFullCarga.consultar_request, Nothing, request, oBELog)
                End Using
                Using cntrlPuntoVenta As New SPE.Web.CPuntoVenta
                    Dim responseG As BEFCResponse(Of BEFCConsultarResponse) = cntrlPuntoVenta.ConsultarFC(request)
                    response = responseG.ObjFC
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                With response
                    .CodResultado = WSBancoMensaje.FC.Mensaje.NoSePudoRealizarTransaccion
                    .MensajeResultado = SPE.Web.CPuntoVenta.MensajeNoSePudoRealizarTransaccion
                End With
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using cntrlComun As New SPE.Web.CComun
                cntrlComun.RegistrarLogFC(Log.MetodosFullCarga.consultar_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            If response.CodResultado IsNot Nothing Then
                Return response
            End If
            With response
                .CodResultado = WSBancoMensaje.FC.Mensaje.NoSePudoRealizarTransaccion
                .MensajeResultado = "No se pudo realizar la transacción"
            End With
            Return response
        End Try
    End Function

    <WebMethod()> _
    Public Function Cancelar(ByVal request As BEFCCancelarRequest) As BEFCCancelarResponse
        Dim response As New BEFCCancelarResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogFC(Log.MetodosFullCarga.cancelar_request, Nothing, request, oBELog)
                End Using
                Using cntrlPuntoVenta As New SPE.Web.CPuntoVenta
                    Dim responseG As BEFCResponse(Of BEFCCancelarResponse) = cntrlPuntoVenta.CancelarFC(request)
                    response = responseG.ObjFC
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                With response
                    .CodResultado = WSBancoMensaje.FC.Mensaje.NoSePudoRealizarTransaccion
                    .MensajeResultado = SPE.Web.CPuntoVenta.MensajeNoSePudoRealizarTransaccion
                End With
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using cntrlComun As New SPE.Web.CComun
                cntrlComun.RegistrarLogFC(Log.MetodosFullCarga.cancelar_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            If response.CodResultado IsNot Nothing Then
                Return response
            End If
            With response
                .CodResultado = WSBancoMensaje.FC.Mensaje.NoSePudoRealizarTransaccion
                .MensajeResultado = "No se pudo realizar la transacción"
            End With
            Return response
        End Try
    End Function

    <WebMethod()> _
    Public Function Anular(ByVal request As BEFCAnularRequest) As BEFCAnularResponse
        Dim response As New BEFCAnularResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogFC(Log.MetodosFullCarga.anular_request, Nothing, request, oBELog)
                End Using
                Using cntrlPuntoVenta As New SPE.Web.CPuntoVenta
                    Dim responseG As BEFCResponse(Of BEFCAnularResponse) = cntrlPuntoVenta.AnularFC(request)
                    response = responseG.ObjFC
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                With response
                    .CodResultado = WSBancoMensaje.FC.Mensaje.NoSePudoRealizarTransaccion
                    .MensajeResultado = SPE.Web.CPuntoVenta.MensajeNoSePudoRealizarTransaccion
                End With
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using cntrlComun As New SPE.Web.CComun
                cntrlComun.RegistrarLogFC(Log.MetodosFullCarga.anular_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            If response.CodResultado IsNot Nothing Then
                Return response
            End If
            With response
                .CodResultado = WSBancoMensaje.FC.Mensaje.NoSePudoRealizarTransaccion
                .MensajeResultado = "No se pudo realizar la transacción"
            End With
            Return response
        End Try
    End Function

End Class