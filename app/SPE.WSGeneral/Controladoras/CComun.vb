Imports SPE.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun
Namespace SPE.Web
    Public Class CComun
        Implements IDisposable


        Public Function RegistrarLog(ByVal obelog As BELog) As Integer
            Using Conexions As New ProxyBase(Of IComun)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).RegistrarLog(obelog)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IComun.RegistrarLog(obelog)
        End Function
        Public Function RegistrarLog(ByVal origen As String, ByVal strDescripcion As String) As Integer
            If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
                Dim obelog As New BELog()
                obelog.IdTipo = Log.Tipo.idTipoServiciosWebGenCIP
                obelog.Origen = origen
                obelog.Descripcion = strDescripcion
                Return RegistrarLog(obelog)
            End If
        End Function

        'Public Function RegistrarLog(ByVal origen As String, ByVal cAPI As String, ByVal cClave As String, ByVal email As String, ByVal password As String, ByVal xml As String) As Integer
        '    If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
        '        Dim obelog As New BELog()
        '        obelog.IdTipo = Log.Tipo.idTipoServiciosWebGenCIP
        '        obelog.Origen = origen
        '        obelog.Descripcion = String.Format("cApi:{0},cClave={1},email={2},xml={3}", cAPI, cClave, email, xml)
        '        Return 1
        '    End If
        'End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace