Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Namespace SPE.Web
    Public Class COrdenPago
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function WSGenerarCIP(ByVal request As BEWSGenCIPRequest) As BEWSGenCIPResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSGenerarCIP(request)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSGenerarCIP(request)
        End Function
        Public Function WSEliminarCIP(ByVal request As BEWSElimCIPRequest) As BEWSElimCIPResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSEliminarCIP(request)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSEliminarCIP(request)
        End Function

        Public Function WSActualizarCIP(ByVal request As BEWSActualizaCIPRequest) As BEWSActualizaCIPResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSActualizarCIP(request)
            End Using
        End Function

        Public Function WSConsultarCIPConciliados(ByVal request As BEWSConsCIPsConciliadosRequest) As BEWSConsCIPsConciliadosResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSConsultarCIPConciliados(request)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIPConciliados(request)
        End Function
        Public Function WSConsultarCIP(ByVal request As BEWSConsultarCIPRequest) As BEWSConsultarCIPResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSConsultarCIP(request)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)
        End Function

        '<add Peru.Com FaseIII>
        Public Function WSSolicitarPago(ByVal request As BEWSSolicitarRequest) As BEWSSolicitarResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSSolicitarPago(request)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)
        End Function
        Public Function WSConsultarSolicitudPago(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse
            Using Conexions As New ProxyBase(Of IOrdenPago)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).WSConsultarSolicitudPago(request)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)
        End Function
    End Class

End Namespace