Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Namespace SPE.Web
    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices

        Private _IServicioComun As IServicioComun
        Private _IOrdenPago As IOrdenPago

        Public Property IServicioComun() As IServicioComun
            Get
                Return _IServicioComun
            End Get
            Set(ByVal value As IServicioComun)
                _IServicioComun = value
            End Set
        End Property

        Public Property IOrdenPago() As IOrdenPago
            Get
                Return _IOrdenPago
            End Get
            Set(ByVal value As IOrdenPago)
                _IOrdenPago = value
            End Set
        End Property

        Private _IComun As IComun
        Public Property IComun() As IComun
            Get
                Return _IComun
            End Get
            Set(ByVal value As IComun)
                _IComun = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()
            MyBase.DoSetupNetworkEnviroment()
            Me.IComun = CType(Activator.GetObject(GetType(IComun), Me.ServerUrl + "/" + NombreServiciosConocidos.IComun), IComun)
            Me.IServicioComun = CType(Activator.GetObject(GetType(IServicioComun), Me.ServerUrl + "/" + NombreServiciosConocidos.IServicioComun), IServicioComun)
            Me.IOrdenPago = CType(Activator.GetObject(GetType(IOrdenPago), Me.ServerUrl + "/" + NombreServiciosConocidos.IOrdenPago), IOrdenPago)
        End Sub
    End Class

End Namespace
