Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.Logging

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WSCIP
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function GenerarCIP(ByVal request As BEWSGenCIPRequest) As BEWSGenCIPResponse
        Dim response As BEWSGenCIPResponse = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Dim log As New Logger()
        Try
            Using ocntrl As New SPE.Web.COrdenPago()

                Using ocntrlComun As New SPE.Web.CComun()
                    'ocntrlComun.RegistrarLog("GenerarCIP-RecibeParametros", String.Format("cApi:{0},cClave={1},email={2},xml={3}", request.CAPI, request.CClave, request.Email, request.Xml))
                    response = ocntrl.WSGenerarCIP(request)
                    'ocntrlComun.RegistrarLog("GenerarCIP-EnviaParametros", String.Format("CIP:{0},estado={1},informacionCIP={2},mensaje={3}", response.CIP, response.Estado, response.InformacionCIP, response.Mensaje))
                End Using
            End Using
            log.Info("Tracking de generacion cip mod 1")
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function EliminarCIP(ByVal request As BEWSElimCIPRequest) As BEWSElimCIPResponse
        Dim response As BEWSElimCIPResponse = Nothing
        Try
            Using ocntrl As New SPE.Web.COrdenPago()
                Using ocntrlComun As New SPE.Web.CComun()
                    ocntrlComun.RegistrarLog("EliminarCIP-RecibeParametros", String.Format("CAPI:{0},CClave={1},CIP={2},InfoRequest={3}", request.CAPI, request.CClave, request.CIP, request.InfoRequest))
                    response = ocntrl.WSEliminarCIP(request)
                    ocntrlComun.RegistrarLog("EliminarCIP-EnviaParametros", String.Format("Estado={0},Mensaje={1},InfoResponse={2}", response.Estado, response.Mensaje, response.InfoResponse))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function ActualizarCIP(ByVal request As BEWSActualizaCIPRequest) As BEWSActualizaCIPResponse
        Dim response As BEWSActualizaCIPResponse = Nothing
        Try
            Using ocntrl As New SPE.Web.COrdenPago()
                Using ocntrlComun As New SPE.Web.CComun()
                    ocntrlComun.RegistrarLog("ActualizarCIP-RecibeParametros", String.Format("CAPI:{0},CClave={1},CIP={2},InfoRequest={3}", request.CAPI, request.CClave, request.CIP, request.InfoRequest))
                    response = ocntrl.WSActualizarCIP(request)
                    ocntrlComun.RegistrarLog("Actualizar-EnviaParametros", String.Format("Estado={0},Mensaje={1},InfoResponse={2}", response.Estado, response.Mensaje, response.InfoResponse))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function ConsultarCIPConciliados(ByVal request As BEWSConsCIPsConciliadosRequest) As BEWSConsCIPsConciliadosResponse
        Dim response As BEWSConsCIPsConciliadosResponse = Nothing
        Try
            Using oCOrdenPago As New SPE.Web.COrdenPago()
                Using oCComun As New SPE.Web.CComun()
                    oCComun.RegistrarLog("ConsultarCIPConciliados-RecibeParametros", _
                        String.Format("cApi:{0},cClave={1},fechaDesde={2},fechaHasta={3},tipo={4},infoRequest={5}", _
                            request.CAPI, request.CClave, request.FechaDesde, request.FechaHasta, request.Tipo, request.InfoRequest))
                    response = oCOrdenPago.WSConsultarCIPConciliados(request)
                    Dim Cips As String
                    If response.CIPs Is Nothing Then
                        Cips = "0"
                    Else : Cips = response.CIPs.Count.ToString
                    End If

                    oCComun.RegistrarLog("ConsultarCIPConciliados-EnviaParametros", String.Format("Estado:{0},Mensaje:{1},InfoResponse:{2},CIPs:{3}", _
                      response.Estado, response.Mensaje, response.InfoResponse, Cips))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function ConsultarCIP(ByVal request As BEWSConsultarCIPRequest) As BEWSConsultarCIPResponse
        Dim response As BEWSConsultarCIPResponse = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Try
            Using oCOrdenPago As New SPE.Web.COrdenPago()
                Using oCComun As New SPE.Web.CComun()
                    oCComun.RegistrarLog("ConsultarCIP-RecibeParametros", _
                        String.Format("cApi:{0};cClave={1};cips={2};infoResquest={3}", _
                            request.CAPI, request.CClave, String.Join(",", request.CIPS), request.InfoRequest))
                    response = oCOrdenPago.WSConsultarCIP(request)
                    oCComun.RegistrarLog("ConsultarCIP-EnviaParametros", String.Format("Estado:{0},Mensaje:{1},InfoResponse:{2},CIPs:{3}", _
                        response.Estado, response.Mensaje, response.InfoResponse, response.CIPs.Count.ToString))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

   '<add Peru.Com FaseIII>
    <WebMethod()> _
    Public Function SolicitarPago(ByVal request As BEWSSolicitarRequest) As BEWSSolicitarResponse
        Dim response As BEWSSolicitarResponse = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Try
            Using ocntrl As New SPE.Web.COrdenPago()
                Using ocntrlComun As New SPE.Web.CComun()
                    'ocntrlComun.RegistrarLog("SolicitarPago-RecibeParametros", String.Format("cServ:{0},cClave={1},xml={2}", request.cServ, request.CClave, request.Xml))
                    response = ocntrl.WSSolicitarPago(request)
                    'ocntrlComun.RegistrarLog("SolicitarPago-EnviaParametros", String.Format("estado={0},mensaje={1},xml={2}", response.Estado, response.Mensaje, response.Xml))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function ConsultarSolicitudPago(ByVal request As BEWSConsultarSolicitudRequest) As BEWSConsultarSolicitudResponse
        Dim response As BEWSConsultarSolicitudResponse = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Try
            Using ocntrl As New SPE.Web.COrdenPago()

                Using ocntrlComun As New SPE.Web.CComun()
                    'ocntrlComun.RegistrarLog("SolicitarPago-RecibeParametros", String.Format("cServ:{0},cClave={1},xml={2}", request.cServ, request.CClave, request.Xml))
                    response = ocntrl.WSConsultarSolicitudPago(request)
                    'ocntrlComun.RegistrarLog("SolicitarPago-EnviaParametros", String.Format("estado={0},mensaje={1},xml={2}", response.Estado, response.Mensaje, response.Xml))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function
End Class