Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WSRecaudacion
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function ConsultarOPsConciliadas() As System.Data.DataSet
        Dim ds As DataSet = Nothing
        Try
            ds = New SPE.Web.CServicioComun().ConsultarOPsConciliadas()
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return ds

    End Function

    <WebMethod()> _
    Public Function ActualizarOPAMigradoRecaudacion(ByVal pidOrdenes As Int64()) As Integer
        Dim resultado As Integer
        Try
            resultado = New SPE.Web.CServicioComun().ActualizarOPAMigradoRecaudacion(pidOrdenes)
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return resultado
    End Function

End Class