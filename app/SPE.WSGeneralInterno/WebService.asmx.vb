﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WebService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function ConsultarCIP(ByVal request As BEWSConsultarCIPv2Request) As BEWSConsultarCIPv2Response
        Dim response As BEWSConsultarCIPv2Response = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Try
            Using oCOrdenPago As New SPE.Web.COrdenPago()
                Using oCComun As New SPE.Web.CComun()
                    oCComun.RegistrarLog("ConsultarCIPv2-RecibeParametros", _
                        String.Format("cApi:{0};cClave={1};cips={2}", _
                            request.CAPI, request.CClave, String.Join(",", request.CIPS)))
                    response = oCOrdenPago.WSConsultarCIPv2(request)
                    oCComun.RegistrarLog("ConsultarCIPv2-EnviaParametros", String.Format("Estado:{0},Mensaje:{1},InfoResponse:{2},CIPs:{3}", _
                        response.Estado, response.Mensaje, response.InfoResponse, response.CIPs.Count.ToString))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

End Class