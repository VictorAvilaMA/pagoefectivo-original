﻿Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Namespace SPE.Api
    Public Class ByteUtil
        Public Shared Function ByteArrayToStringHex(ba As Byte()) As String
            Dim hex As New StringBuilder(ba.Length * 2)
            For Each b As Byte In ba
                hex.AppendFormat("{0:x2}", b)
            Next
            Return hex.ToString().ToUpper()
        End Function

        Public Shared Function StringHexToByteArray(hex As String) As Byte()
            Dim NumberChars As Integer = hex.Length
            Dim bytes As Byte() = New Byte(NumberChars \ 2 - 1) {}
            For i As Integer = 0 To NumberChars - 1 Step 2
                bytes(i \ 2) = Convert.ToByte(hex.Substring(i, 2), 16)
            Next
            Return bytes
        End Function

        Public Shared Function StringToBytes(hex As String) As Byte()
            Return Encoding.GetEncoding("iso-8859-1").GetBytes(hex)
        End Function

        Public Shared Function BytesToString(ba As Byte()) As String
            Return Encoding.GetEncoding("iso-8859-1").GetString(ba)
        End Function

        Public Shared Function ObjectToArrayByte(obj As Object) As Byte()
            Dim oMemoryStream As New MemoryStream()
            Dim oBinaryFormatter As New BinaryFormatter()
            oBinaryFormatter.Serialize(oMemoryStream, obj)
            Return oMemoryStream.ToArray()
        End Function
        Public Shared Function ByteArrayToObject(arrBytes As Byte()) As [Object]
            Dim memStream As New MemoryStream()
            Dim binForm As New BinaryFormatter()
            memStream.Write(arrBytes, 0, arrBytes.Length)
            memStream.Seek(0, SeekOrigin.Begin)
            Dim obj As [Object] = DirectCast(binForm.Deserialize(memStream), [Object])
            Return obj
        End Function
        Public Shared Function ByteArrayToFile(_FileName As String, _ByteArray As Byte()) As Boolean
            Try
                Dim _FileStream As New System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write)
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length)
                _FileStream.Close()

                Return True
            Catch _Exception As Exception
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString())
            End Try
            Return False
        End Function
        Public Shared Function FileToByteArray(_FileName As String) As Byte()
            Dim _Buffer As Byte() = Nothing

            Try
                Dim _FileStream As New System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Dim _BinaryReader As New System.IO.BinaryReader(_FileStream)
                Dim _TotalBytes As Long = New System.IO.FileInfo(_FileName).Length
                _Buffer = _BinaryReader.ReadBytes(CType(_TotalBytes, Int32))
                _FileStream.Close()
                _FileStream.Dispose()
                _BinaryReader.Close()
            Catch _Exception As Exception
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString())
            End Try
            Return _Buffer
        End Function
    End Class
End Namespace