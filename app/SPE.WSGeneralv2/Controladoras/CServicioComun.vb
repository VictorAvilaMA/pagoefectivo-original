Imports Microsoft.VisualBasic
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun


Namespace SPE.Web

    Public Class CServicioComun
        Implements IDisposable

        Public Function ConsultarOPsConciliadas() As System.Data.DataSet
            Using Conexions As New ProxyBase(Of IServicioComun)
                Return Conexions.DevolverContrato().ConsultarOPsConciliadas()
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IServicioComun.ConsultarOPsConciliadas()
        End Function
        Public Function ActualizarOPAMigradoRecaudacion(ByVal pidOrdenes As Int64()) As Integer
            Using Conexions As New ProxyBase(Of IServicioComun)
                Return Conexions.DevolverContrato().ActualizarOPAMigradoRecaudacion(pidOrdenes)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IServicioComun.ActualizarOPAMigradoRecaudacion(pidOrdenes)
        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace

