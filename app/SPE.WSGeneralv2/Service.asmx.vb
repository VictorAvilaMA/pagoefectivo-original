﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Entidades = SPE.Entidades
Imports System.Net
Imports SPE.WSGeneralv2.SPE.Api.Proxys
Imports SPE.WSGeneralv2.SPE.Api
Imports System.IO
Imports SPE.Logging
Imports System.Runtime.Serialization.Json

'Imports SPE.WSGeneralv2.SPE.Api.ByteUtil

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function ConsultarCIPMod1(ByVal request As Entidades.BEWSConsultarCIPRequestMod1) As Entidades.BEWSConsultarCIPResponseMod1
        Dim response As Entidades.BEWSConsultarCIPResponseMod1 = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Try
            Using oCOrdenPago As New SPE.Web.COrdenPago()
                Using oCComun As New SPE.Web.CComun()
                    oCComun.RegistrarLog("ConsultarCIPMod1-RecibeParametros", _
                        String.Format("codServ:{0};cips={1};infoResquest={2}", _
                            request.CodServ, String.Join(",", request.CIPS), request.InfoRequest))
                    response = oCOrdenPago.WSConsultarCIPMod1(request)
                    oCComun.RegistrarLog("ConsultarCIPMod1-EnviaParametros", String.Format("Estado:{0},Mensaje:{1},InfoResponse:{2},XML:{3}", _
                        response.Estado, response.Mensaje, response.InfoResponse, response.XML))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function ActualizarCIPMod1(ByVal request As Entidades.BEWSActualizaCIPRequestMod1) As Entidades.BEWSActualizaCIPResponseMod1
        Dim response As Entidades.BEWSActualizaCIPResponseMod1 = Nothing
        Try
            Using ocntrl As New SPE.Web.COrdenPago()
                Using ocntrlComun As New SPE.Web.CComun()
                    ocntrlComun.RegistrarLog("ActualizarCIPMod1-RecibeParametros", String.Format("codServ:{0},CIP={1},InfoRequest={1},Fecha={3}", request.CodServ, request.CIP, request.InfoRequest, request.FechaExpira))
                    response = ocntrl.WSActualizarCIPMod1(request)
                    ocntrlComun.RegistrarLog("ActualizarCIPMod1-EnviaParametros", String.Format("Estado={0},Mensaje={1},InfoResponse={2}", response.Estado, response.Mensaje, response.InfoResponse))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function EliminarCIPMod1(ByVal request As Entidades.BEWSElimCIPRequestMod1) As Entidades.BEWSElimCIPResponseMod1
        Dim response As Entidades.BEWSElimCIPResponseMod1 = Nothing
        Try
            Using ocntrl As New SPE.Web.COrdenPago()
                Using ocntrlComun As New SPE.Web.CComun()
                    ocntrlComun.RegistrarLog("EliminarCIPMod1-RecibeParametros", String.Format("codServ:{0},CIP={1},InfoRequest={2}", request.CodServ, request.CIP, request.InfoRequest))
                    response = ocntrl.WSEliminarCIPMod1(request)
                    ocntrlComun.RegistrarLog("EliminarCIPMod1-EnviaParametros", String.Format("Estado={0},Mensaje={1},InfoResponse={2}", response.Estado, response.Mensaje, response.InfoResponse))
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function

    <WebMethod()> _
    Public Function GenerarCIPMod1(ByVal request As Entidades.BEWSGenCIPRequestMod1) As Entidades.BEWSGenCIPResponseMod1
        Dim respuesta As Entidades.BEWSGenCIPResponseMod1 = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Dim deserealizador As DataContractJsonSerializer = New DataContractJsonSerializer(GetType(Entidades.BEWSGenCIPResponseMod1))
        Dim log As New Logger()
        Dim url As String = System.Configuration.ConfigurationManager.AppSettings("urlServiceTienda")
        Dim IdRndmCWL As String = GenerarIdRndm()

        Try
            Dim contenido As String = "{" & String.Format("""Identificador"":""{0}"",""CodServ"":""{1}"",""Firma"":""{2}"",""Xml"":""{3}""", IdRndmCWL, request.CodServ, request.Firma, request.Xml) & "}"
            log.Info("GenerarCipMod3-Request: " + contenido)

            Dim httpWebRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
            httpWebRequest.ContentType = "application/json"
            httpWebRequest.Method = "POST"

            Using streamWriter As StreamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                streamWriter.Write(contenido)
                streamWriter.Flush()
            End Using

            Dim httpResponse As HttpWebResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)

            respuesta = deserealizador.ReadObject(httpResponse.GetResponseStream())

            Dim contenidoResponse As String = "{" & String.Format("""Identificador"":""{0}"",""Estado"":""{1}"",""Mensaje"":""{2}"",""Xml"":""{3}""", IdRndmCWL, respuesta.Estado, respuesta.Mensaje, respuesta.Xml) & "}"
            log.Info("GenerarCipMod3-Response: " + contenidoResponse)

        Catch wex As WebException
            respuesta = deserealizador.ReadObject(wex.Response.GetResponseStream())
            log.Error(wex, "GenerarCipMod3-Response:" + wex.Message + String.Format("""Identificador"":""{0}"",", IdRndmCWL))
        Catch ex As Exception
            log.Error(ex, ex.Message)
            respuesta = New Entidades.BEWSGenCIPResponseMod1()
            respuesta.Mensaje = ex.Message
            respuesta.Xml = String.Empty
            respuesta.Estado = -1
        End Try
        Return respuesta
    End Function

#Region "SagaInicio"
    <WebMethod()> _
    Public Function ConsultarSolicitudPagov2(ByVal request As Entidades.BEWSConsultarSolicitudRequest) As Entidades.BEWSConsultarSolicitudResponse
        Dim response As Entidades.BEWSConsultarSolicitudResponse = Nothing
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Dim PublicPathContraparte As String = "C:\\PagoEfectivo2\\Claves\\SPE_PublicKey.1pz"
        Dim PrivatePath As String = "C:\\PagoEfectivo2\\Claves\\SGA_PrivateKey.1pz"
        Try
            Using proxy As New WSCrypto()
                proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
                proxy.Proxy = WebProxy.GetDefaultProxy()
                proxy.Proxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
                request.Xml = request.Xml.Trim()
                request.CClave = proxy.Signer(request.Xml, ByteUtil.FileToByteArray(PrivatePath))
                request.Xml = proxy.EncryptText(request.Xml, ByteUtil.FileToByteArray(PublicPathContraparte))


                Using ocntrl As New SPE.Web.COrdenPago()
                    Using ocntrlComun As New SPE.Web.CComun()
                        'ocntrlComun.RegistrarLog("SolicitarPago-RecibeParametros", String.Format("cServ:{0},cClave={1},xml={2}", request.cServ, request.CClave, request.Xml))
                        response = ocntrl.WSConsultarSolicitudPagov2(request)
                        'ocntrlComun.RegistrarLog("SolicitarPago-EnviaParametros", String.Format("estado={0},mensaje={1},xml={2}", response.Estado, response.Mensaje, response.Xml))
                    End Using
                End Using
                Using proxyCIP = New WSCrypto()
                    'response = proxyCIP.ConsultarSolicitudPago(request)
                    If response IsNot Nothing Then
                        If Not [String].IsNullOrEmpty(response.Xml) Then
                            response.Xml = proxy.DecryptText(response.Xml, ByteUtil.FileToByteArray(PrivatePath))
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return response
    End Function
#End Region

#Region "IdentificadorCWL"

    Private Function GenerarIdRndm() As String
        Dim rnd As Random = New Random()
        Dim numeroSinCotaArbitraria As Integer = rnd.[Next]()
        Return numeroSinCotaArbitraria.ToString()
    End Function

#End Region

End Class
