﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.18408
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by wsdl, Version=4.0.30319.1.
'
Namespace SPE.Api.Proxys
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="WSCryptoSoap", [Namespace]:="http://tempuri.org/")>  _
    Partial Public Class WSCrypto
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private EncryptTextOperationCompleted As System.Threading.SendOrPostCallback
        
        Private DecryptTextOperationCompleted As System.Threading.SendOrPostCallback
        
        Private SignerOperationCompleted As System.Threading.SendOrPostCallback
        
        Private SignerValOperationCompleted As System.Threading.SendOrPostCallback
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = System.Configuration.ConfigurationManager.AppSettings("SPE_WebServiceTest_WSCrypto")
        End Sub
        
        '''<remarks/>
        Public Event EncryptTextCompleted As EncryptTextCompletedEventHandler
        
        '''<remarks/>
        Public Event DecryptTextCompleted As DecryptTextCompletedEventHandler
        
        '''<remarks/>
        Public Event SignerCompleted As SignerCompletedEventHandler
        
        '''<remarks/>
        Public Event SignerValCompleted As SignerValCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/EncryptText", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function EncryptText(ByVal plainText As String, <System.Xml.Serialization.XmlElementAttribute(DataType:="base64Binary")> ByVal publicKey() As Byte) As String
            Dim results() As Object = Me.Invoke("EncryptText", New Object() {plainText, publicKey})
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Function BeginEncryptText(ByVal plainText As String, ByVal publicKey() As Byte, ByVal callback As System.AsyncCallback, ByVal asyncState As Object) As System.IAsyncResult
            Return Me.BeginInvoke("EncryptText", New Object() {plainText, publicKey}, callback, asyncState)
        End Function
        
        '''<remarks/>
        Public Function EndEncryptText(ByVal asyncResult As System.IAsyncResult) As String
            Dim results() As Object = Me.EndInvoke(asyncResult)
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Overloads Sub EncryptTextAsync(ByVal plainText As String, ByVal publicKey() As Byte)
            Me.EncryptTextAsync(plainText, publicKey, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub EncryptTextAsync(ByVal plainText As String, ByVal publicKey() As Byte, ByVal userState As Object)
            If (Me.EncryptTextOperationCompleted Is Nothing) Then
                Me.EncryptTextOperationCompleted = AddressOf Me.OnEncryptTextOperationCompleted
            End If
            Me.InvokeAsync("EncryptText", New Object() {plainText, publicKey}, Me.EncryptTextOperationCompleted, userState)
        End Sub
        
        Private Sub OnEncryptTextOperationCompleted(ByVal arg As Object)
            If (Not (Me.EncryptTextCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent EncryptTextCompleted(Me, New EncryptTextCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/DecryptText", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function DecryptText(ByVal encryptText As String, <System.Xml.Serialization.XmlElementAttribute(DataType:="base64Binary")> ByVal privateKey() As Byte) As String
            Dim results() As Object = Me.Invoke("DecryptText", New Object() {encryptText, privateKey})
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Function BeginDecryptText(ByVal encryptText As String, ByVal privateKey() As Byte, ByVal callback As System.AsyncCallback, ByVal asyncState As Object) As System.IAsyncResult
            Return Me.BeginInvoke("DecryptText", New Object() {encryptText, privateKey}, callback, asyncState)
        End Function
        
        '''<remarks/>
        Public Function EndDecryptText(ByVal asyncResult As System.IAsyncResult) As String
            Dim results() As Object = Me.EndInvoke(asyncResult)
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Overloads Sub DecryptTextAsync(ByVal encryptText As String, ByVal privateKey() As Byte)
            Me.DecryptTextAsync(encryptText, privateKey, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub DecryptTextAsync(ByVal encryptText As String, ByVal privateKey() As Byte, ByVal userState As Object)
            If (Me.DecryptTextOperationCompleted Is Nothing) Then
                Me.DecryptTextOperationCompleted = AddressOf Me.OnDecryptTextOperationCompleted
            End If
            Me.InvokeAsync("DecryptText", New Object() {encryptText, privateKey}, Me.DecryptTextOperationCompleted, userState)
        End Sub
        
        Private Sub OnDecryptTextOperationCompleted(ByVal arg As Object)
            If (Not (Me.DecryptTextCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent DecryptTextCompleted(Me, New DecryptTextCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/Signer", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function Signer(ByVal plainText As String, <System.Xml.Serialization.XmlElementAttribute(DataType:="base64Binary")> ByVal privateKey() As Byte) As String
            Dim results() As Object = Me.Invoke("Signer", New Object() {plainText, privateKey})
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Function BeginSigner(ByVal plainText As String, ByVal privateKey() As Byte, ByVal callback As System.AsyncCallback, ByVal asyncState As Object) As System.IAsyncResult
            Return Me.BeginInvoke("Signer", New Object() {plainText, privateKey}, callback, asyncState)
        End Function
        
        '''<remarks/>
        Public Function EndSigner(ByVal asyncResult As System.IAsyncResult) As String
            Dim results() As Object = Me.EndInvoke(asyncResult)
            Return CType(results(0),String)
        End Function
        
        '''<remarks/>
        Public Overloads Sub SignerAsync(ByVal plainText As String, ByVal privateKey() As Byte)
            Me.SignerAsync(plainText, privateKey, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub SignerAsync(ByVal plainText As String, ByVal privateKey() As Byte, ByVal userState As Object)
            If (Me.SignerOperationCompleted Is Nothing) Then
                Me.SignerOperationCompleted = AddressOf Me.OnSignerOperationCompleted
            End If
            Me.InvokeAsync("Signer", New Object() {plainText, privateKey}, Me.SignerOperationCompleted, userState)
        End Sub
        
        Private Sub OnSignerOperationCompleted(ByVal arg As Object)
            If (Not (Me.SignerCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent SignerCompleted(Me, New SignerCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/SignerVal", RequestNamespace:="http://tempuri.org/", ResponseNamespace:="http://tempuri.org/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function SignerVal(ByVal plainText As String, ByVal signerText As String, <System.Xml.Serialization.XmlElementAttribute(DataType:="base64Binary")> ByVal publicKey() As Byte) As Boolean
            Dim results() As Object = Me.Invoke("SignerVal", New Object() {plainText, signerText, publicKey})
            Return CType(results(0),Boolean)
        End Function
        
        '''<remarks/>
        Public Function BeginSignerVal(ByVal plainText As String, ByVal signerText As String, ByVal publicKey() As Byte, ByVal callback As System.AsyncCallback, ByVal asyncState As Object) As System.IAsyncResult
            Return Me.BeginInvoke("SignerVal", New Object() {plainText, signerText, publicKey}, callback, asyncState)
        End Function
        
        '''<remarks/>
        Public Function EndSignerVal(ByVal asyncResult As System.IAsyncResult) As Boolean
            Dim results() As Object = Me.EndInvoke(asyncResult)
            Return CType(results(0),Boolean)
        End Function
        
        '''<remarks/>
        Public Overloads Sub SignerValAsync(ByVal plainText As String, ByVal signerText As String, ByVal publicKey() As Byte)
            Me.SignerValAsync(plainText, signerText, publicKey, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub SignerValAsync(ByVal plainText As String, ByVal signerText As String, ByVal publicKey() As Byte, ByVal userState As Object)
            If (Me.SignerValOperationCompleted Is Nothing) Then
                Me.SignerValOperationCompleted = AddressOf Me.OnSignerValOperationCompleted
            End If
            Me.InvokeAsync("SignerVal", New Object() {plainText, signerText, publicKey}, Me.SignerValOperationCompleted, userState)
        End Sub
        
        Private Sub OnSignerValOperationCompleted(ByVal arg As Object)
            If (Not (Me.SignerValCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent SignerValCompleted(Me, New SignerValCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")>  _
    Public Delegate Sub EncryptTextCompletedEventHandler(ByVal sender As Object, ByVal e As EncryptTextCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class EncryptTextCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),String)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")>  _
    Public Delegate Sub DecryptTextCompletedEventHandler(ByVal sender As Object, ByVal e As DecryptTextCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class DecryptTextCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),String)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")>  _
    Public Delegate Sub SignerCompletedEventHandler(ByVal sender As Object, ByVal e As SignerCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class SignerCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),String)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1")>  _
    Public Delegate Sub SignerValCompletedEventHandler(ByVal sender As Object, ByVal e As SignerValCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class SignerValCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Boolean
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Boolean)
            End Get
        End Property
    End Class
End Namespace
