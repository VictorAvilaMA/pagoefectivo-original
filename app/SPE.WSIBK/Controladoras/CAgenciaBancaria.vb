Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CAgenciaBancaria
        Implements IDisposable

        Public Function ConsultarIBK(ByVal request As BEIBKConsultarRequest) As BEBKResponse(Of BEIBKConsultarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarIBK(request)
            End Using
        End Function

        Public Function CancelarIBK(ByVal request As BEIBKCancelarRequest) As BEBKResponse(Of BEIBKCancelarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).CancelarIBK(request)
            End Using
        End Function

        Public Function AnularIBK(ByVal request As BEIBKAnularRequest) As BEBKResponse(Of BEIBKAnularResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).AnularIBK(request)
            End Using
        End Function

        Public Function ExtonarCancelacionIBK(ByVal request As BEIBKExtornarCancelacionRequest) As BEBKResponse(Of BEIBKExtornarCancelacionResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarCancelacionIBK(request)
            End Using
        End Function

        Public Function ExtonarAnulacionIBK(ByVal request As BEIBKExtornarAnulacionRequest) As BEBKResponse(Of BEIBKExtornarAnulacionResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarAnulacionIBK(request)
            End Using
        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
