﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService
    Implements IEjecTransaccionInterbankSoapBinding

    <WebMethod(Description:="ejecutarTransaccionInterbank")> _
    <SoapDocumentMethod(Action:="")> _
    Public Function ejecutarTransaccionInterbank(ByVal Input As String) As String Implements IEjecTransaccionInterbankSoapBinding.ejecutarTransaccionInterbank
        Dim response As String = ""
        Try
            Dim IBKrequest As BEBKBaseRequest = ObtenerObjetoRequest(Input, Context.Request.ServerVariables("REMOTE_ADDR"))
            If IBKrequest.EsValido Then
                If IBKrequest.GetType Is GetType(BEIBKConsultarRequest) Then
                    response = Consultar(IBKrequest).ToString
                ElseIf IBKrequest.GetType Is GetType(BEIBKCancelarRequest) Then
                    response = Cancelar(IBKrequest).ToString
                ElseIf IBKrequest.GetType Is GetType(BEIBKAnularRequest) Then
                    response = Anular(IBKrequest).ToString
                ElseIf IBKrequest.GetType Is GetType(BEIBKExtornarCancelacionRequest) Then
                    response = ExtornarCancelacion(IBKrequest).ToString
                ElseIf IBKrequest.GetType Is GetType(BEIBKExtornarAnulacionRequest) Then
                    response = ExtornarAnulacion(IBKrequest).ToString
                End If
            Else
                'TODO
                response = ResolverTramaInvalida(Input, IBKrequest.IP, Nothing)
            End If
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            response = ResolverTramaInvalida(Input, Context.Request.ServerVariables("REMOTE_ADDR"), ex.Message)
            Return response
        End Try
    End Function

    Private Function ResolverTramaInvalida(ByVal Trama As String, ByVal IP As String, ByVal Mensaje As String) As String
        Try
            Dim IdLogRequest As Long = 0
            Try
                IdLogRequest = UtilTrace.DoTrace("WSIBK - Request no válido", Trama, IP, Mensaje)
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
            End Try
            UtilTrace.DoTrace("WSIBK - Response de Request no válido", Trama, IP, IdLogRequest)
            Return Trama
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Return Trama
        End Try
    End Function

    Private Function Consultar(ByVal request As BEIBKConsultarRequest) As BEIBKConsultarResponse
        Dim response As New BEIBKConsultarResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogIBK(Log.MetodosInterbank.consultar_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BEIBKConsultarResponse) = CntrlAgenciaBancaria.ConsultarIBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogIBK(Log.MetodosInterbank.consultar_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function Cancelar(ByVal request As BEIBKCancelarRequest) As BEIBKCancelarResponse
        Dim response As New BEIBKCancelarResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogIBK(Log.MetodosInterbank.cancelar_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BEIBKCancelarResponse) = CntrlAgenciaBancaria.CancelarIBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogIBK(Log.MetodosInterbank.cancelar_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function Anular(ByVal request As BEIBKAnularRequest) As BEIBKAnularResponse
        Dim response As New BEIBKAnularResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogIBK(Log.MetodosInterbank.anular_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BEIBKAnularResponse) = CntrlAgenciaBancaria.AnularIBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogIBK(Log.MetodosInterbank.anular_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function ExtornarCancelacion(ByVal request As BEIBKExtornarCancelacionRequest) As BEIBKExtornarCancelacionResponse
        Dim response As New BEIBKExtornarCancelacionResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogIBK(Log.MetodosInterbank.cancelarauto_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BEIBKExtornarCancelacionResponse) = CntrlAgenciaBancaria.ExtonarCancelacionIBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogIBK(Log.MetodosInterbank.cancelarauto_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function ExtornarAnulacion(ByVal request As BEIBKExtornarAnulacionRequest) As BEIBKExtornarAnulacionResponse
        Dim response As New BEIBKExtornarAnulacionResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogIBK(Log.MetodosInterbank.anularauto_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BEIBKExtornarAnulacionResponse) = CntrlAgenciaBancaria.ExtonarAnulacionIBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogIBK(Log.MetodosInterbank.anularauto_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function ObtenerObjetoRequest(ByVal Trama As String, ByVal IP As String) As BEBKBaseRequest
        Dim SBKrequest As BEBKBaseRequest = Nothing
        Using CntrlComun As New SPE.Web.CComun()
            Try
                Select Case Trama.Substring(55, 2)
                    Case ParametrosSistema.Interbank.CodigoProceso.Consulta
                        SBKrequest = New BEIBKConsultarRequest(Trama)
                    Case ParametrosSistema.Interbank.CodigoProceso.Pago
                        Select Case Trama.Substring(0, 4)
                            Case ParametrosSistema.Interbank.IdentificacionTipoMensaje.Solicitud
                                SBKrequest = New BEIBKCancelarRequest(Trama)
                            Case ParametrosSistema.Interbank.IdentificacionTipoMensaje.SolicitudAutomatica
                                SBKrequest = New BEIBKExtornarCancelacionRequest(Trama)
                        End Select
                    Case ParametrosSistema.Interbank.CodigoProceso.Anulacion
                        Select Case Trama.Substring(0, 4)
                            Case ParametrosSistema.Interbank.IdentificacionTipoMensaje.Solicitud
                                SBKrequest = New BEIBKAnularRequest(Trama)
                            Case ParametrosSistema.Interbank.IdentificacionTipoMensaje.SolicitudAutomatica
                                SBKrequest = New BEIBKExtornarAnulacionRequest(Trama)
                        End Select
                End Select
                If SBKrequest Is Nothing Then
                    SBKrequest = New BEBKBaseRequest(Trama)
                Else
                    SBKrequest.EsValido = True
                End If
            Catch ex As Exception
                SBKrequest = New BEBKBaseRequest(Trama)
            End Try
        End Using
        SBKrequest.IP = IP
        Return SBKrequest
    End Function

End Class