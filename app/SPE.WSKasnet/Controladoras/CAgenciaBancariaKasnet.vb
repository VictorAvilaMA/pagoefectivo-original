﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema.WSBancoMensaje
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CAgenciaBancariaKasnet
        Implements IDisposable

        Private _listaCodTransaccionKasnet As Dictionary(Of String, String) = Nothing
        Public ReadOnly Property ListaCodTransaccionKasnet() As Dictionary(Of String, String)
            Get
                If (_listaCodTransaccionKasnet Is Nothing) Then
                    _listaCodTransaccionKasnet = New Dictionary(Of String, String)

                    'Generico (Consulta,Pago,Extorno)
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.TransaccionRealizadaExito, "Transacción realizada con éxito") ' "01"


                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.NoSePudoRealizarTransaccion, "No se pudo realizar la transacción") ' "02"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.TramaEnviadaInvalida, "Trama enviada inválida") '"03"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoPagoNoExiste, "Código de pago no existe") '"04"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoConEstadoPagado, "Código con estado Pagado") '"05"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoConEstadoExpirado, "Código con estado Expirado") '"06"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoConEstadoEliminado, "Código con estado Eliminado") '"07"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoExtornarNoExiste, "Código extornar no existe") '"08"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.CodigoExtornarExpirado, "Código extornar expirado") '"09"
                    _listaCodTransaccionKasnet.Add(kasnet.Mensaje.MontoOMonedaNoCoincide, "Monto o moneda no coincide") '"10"

                End If
                Return _listaCodTransaccionKasnet
            End Get
        End Property

        Public Function ConsultarKasnet(ByVal request As BEKasnetConsultarRequest) As BEKasnetResponse(Of BEKasnetConsultarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarKasnet(request)
            End Using
        End Function

        Public Function PagarKasnet(ByVal request As BEKasnetPagarRequest) As BEKasnetResponse(Of BEKasnetPagarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).PagarKasnet(request)
            End Using
        End Function

        Public Function ExtornarKasnet(ByVal request As BEKasnetExtornarRequest) As BEKasnetResponse(Of BEKasnetExtornarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarKasnet(request)
            End Using
        End Function

        Public Function ConsultarDataInicial(ByVal request As BEKasnetConsultarRequest) As BEKasnetConsultarResponse

            Dim response As New BEKasnetConsultarResponse
            Dim obeConsDet As New BEKasnetConsultarDetalle
            response.DetDocumentos = New List(Of BEKasnetConsultarDetalle)
            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .CodPuntoVenta = request.CodPuntoVenta.Trim ' CODIGO 
                .NroTerminal = request.NroTerminal.Trim ' CODIGO 
                .NroOperacionConsulta = request.NroOperacionConsulta.Trim
                .CodOrdenPago = request.CodOrdenPago.Trim '


                'codigo de resultado para kasnet
                .CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion

                'mensaje de resultado de kasnet
                .Mensaje = ListaCodTransaccionKasnet(.CodMensaje)

            End With

            response.DetDocumentos = New List(Of BEKasnetConsultarDetalle)

            With obeConsDet
                .NroRecibo = request.CodOrdenPago

            End With

            response.DetDocumentos.Add(obeConsDet)

            Return response

        End Function

        Public Function PagarDataInicial(ByVal request As BEKasnetPagarRequest) As BEKasnetPagarResponse

            Dim response As New BEKasnetPagarResponse
            Dim obePagDet As New BEKasnetPagarDetalle

            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .CodPuntoVenta = request.CodPuntoVenta.Trim
                .NroTerminal = request.NroTerminal.Trim
                .NroOperacionPago = request.NroOperacionPago.Trim
                .CodOrdenPago = request.CodOrdenPago.Trim
                .CantDocumentos = request.CantDocumentos
          
                'codigo de resultado para kasnet
                .CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion

                'mensaje de resultado de kasnet
                .CodMensaje = ListaCodTransaccionKasnet(.CodRespuesta)

            End With

            request.DetDocumentos = New List(Of BEKasnetPagarDetalle)

            With obePagDet
                .NroRecibo = request.CodOrdenPago

            End With

            request.DetDocumentos.Add(obePagDet)

            Return response

        End Function

        Public Function ExtornarDataInicial(ByVal request As BEKasnetExtornarRequest) As BEKasnetExtornarResponse

            Dim response As New BEKasnetExtornarResponse
            Dim obeExtDet As New BEKasnetExtornarDetalle
            '--------------------------------------------------------
            '0. PARAMETROS DE ENTRADA Y SALIDA
            '--------------------------------------------------------
            With response
                .CodPuntoVenta = request.CodPuntoVenta.Trim
                .NroTerminal = request.NroTerminal.Trim
                .NroOperacionExtorno = request.NroOperacionExtorno.Trim
                .CodOrdenPago = request.CodOrdenPago.Trim
                .CantDocumentos = request.CantDocumentos


                'codigo de resultado para kasnet
                .CodMensaje = kasnet.Mensaje.NoSePudoRealizarTransaccion

                'mensaje de resultado de kasnet
                .CodMensaje = ListaCodTransaccionKasnet(.CodRespuesta)


            End With

            request.DetDocumentos = New List(Of BEKasnetExtornarDetalle)

            With obeExtDet
                .NroRecibo = request.CodOrdenPago

            End With

            request.DetDocumentos.Add(obeExtDet)

            Return response

        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace