﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Exceptions

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function validarConexion() As String
        Return "OK Kasnet"
    End Function

    <WebMethod()> _
    Public Function consultar(ByVal request As BEKasnetConsultarRequest) As BEKasnetConsultarResponse

        Dim response As New BEKasnetResponse(Of BEKasnetConsultarResponse)
        response.ObjKasnet = New BEKasnetConsultarResponse()
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try
            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaKasnet()

                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogkasnet(Log.MetodosKasnet.consultar_request, Nothing, request, obeLog)

                        'consultar CIP
                        response = cntrl.ConsultarKasnet(request)

                        'crear objeto log
                        obeLog = New BELog("consultar CIP-Kasnet-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)

                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException

                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEKasnetResponse(Of BEKasnetConsultarResponse))

                'asignar codigo de resultado
                response.ObjKasnet.CodMensaje = WSBancoMensaje.kasnet.Mensaje.NoSePudoRealizarTransaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
                    response.ObjKasnet.Mensaje = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                End Using

                'crear objeto log
                obeLog = New BELog("consultar CIP-Kasnet-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogkasnet(Log.MetodosKasnet.consultar_response, idLog, response.ObjKasnet, obeLog)
            End Using

        Catch ex As Exception
            'excepcion general

            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjKasnet

            ''codigo de resultado para Kasnet
            'response.ObjKasnet.codigoResultado = WSBancoMensaje.Kasnet.Mensaje.NoSePudoRealizarLaTransaccion

            ''mensaje de resultado de Kasnet
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
            '    response.ObjKasnet.mensajeResultado = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.codigoResultado)
            'End Using

            If response.Estado = -1 Then
                'codigo de resultado para Kasnet
                'response.ObjKasnet.codigoResultado = WSBancoMensaje.Kasnet.Mensaje.NoSePudoRealizarLaTransaccion

                'mensaje de resultado de Kasnet
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
                    'response.ObjKasnet.mensajeResultado = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.codigoResultado)
                    response.ObjKasnet = oCtrlAB.ConsultarDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjKasnet

    End Function

    <WebMethod()> _
    Public Function pagar(ByVal request As BEKasnetPagarRequest) As BEKasnetPagarResponse

        Dim response As New BEKasnetResponse(Of BEKasnetPagarResponse)
        response.ObjKasnet = New BEKasnetPagarResponse()
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try

            Try
                Using ocntrlComun As New SPE.Web.CComun()
                    Using cntrl As New SPE.Web.CAgenciaBancariaKasnet()
                        'registrar parametros de entrada
                        idLog = ocntrlComun.RegistrarLogkasnet(Log.MetodosKasnet.pagar_request, Nothing, request, obeLog)

                        'pagar CIP Kasnet
                        response = cntrl.PagarKasnet(request)

                        'crear objeto log
                        obeLog = New BELog("pagar CIP-Kasnet-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)
                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                'log de archivos
                _3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de pago
                response = CType(ex.ObjRemoto, BEKasnetResponse(Of BEKasnetPagarResponse))

                'asignar codigo de resultado
                response.ObjKasnet.CodMensaje = WSBancoMensaje.kasnet.Mensaje.NoSePudoRealizarTransaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
                    response.ObjKasnet.Mensaje = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                End Using

                'crear objeto log
                obeLog = New BELog("pagar CIP-Kasnet-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)
            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogkasnet(Log.MetodosKasnet.pagar_response, idLog, response.ObjKasnet, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjKasnet

            ''codigo de resultado para Kasnet
            'response.ObjKasnet.codigoResultado = WSBancoMensaje.Kasnet.Mensaje.NoSePudoRealizarLaTransaccion

            ''mensaje de resultado de Kasnet
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
            '    response.ObjKasnet.mensajeResultado = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.codigoResultado)
            'End Using

            'verificar estado de la consulta
            If response.Estado = -1 Then

                'codigo de resultado para Kasnet
                'response.ObjKasnet.codigoResultado = WSBancoMensaje.Kasnet.Mensaje.NoSePudoRealizarLaTransaccion

                'mensaje de resultado de Kasnet
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
                    'response.ObjKasnet.mensajeResultado = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.codigoResultado)
                    response.ObjKasnet = oCtrlAB.PagarDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjKasnet

    End Function

    <WebMethod()> _
    Public Function extornar(ByVal request As BEKasnetExtornarRequest) As BEKasnetExtornarResponse
        Dim response As New BEKasnetResponse(Of BEKasnetExtornarResponse)
        response.ObjKasnet = New BEKasnetExtornarResponse
        response.Estado = -1

        Dim idLog As Int64 = 0
        Dim obeLog As BELog = Nothing

        Try

            Try
                Using oCntrlComun As New SPE.Web.CComun
                    Using cntrl As New SPE.Web.CAgenciaBancariaKasnet()
                        'registrar parametros de entrada
                        idLog = oCntrlComun.RegistrarLogkasnet(Log.MetodosKasnet.extornar_request, Nothing, request, obeLog)

                        'extornar CIP
                        response = cntrl.ExtornarKasnet(request)

                        'crear objeto log
                        obeLog = New BELog("extornar CIP-Kasnet-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                                "Validacion", "Error")), response.Mensaje, Nothing)

                    End Using
                End Using

                'excepcion personalizada
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                'log de archivos
                '_3Dev.FW.Web.Log.Logger.LogException(ex)

                'obtener objeto remoto de consulta
                response = CType(ex.ObjRemoto, BEKasnetResponse(Of BEKasnetExtornarResponse))

                'asignar codigo de resultado
                response.ObjKasnet.CodMensaje = WSBancoMensaje.kasnet.Mensaje.NoSePudoRealizarTransaccion

                'asignar mensaje de resultado
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
                    response.ObjKasnet.Mensaje = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.CodMensaje)
                End Using

                'crear objeto log
                obeLog = New BELog("extornar CIP-Kasnet-" + IIf(response.Estado = 1, "Exito", IIf(response.Estado = 0, _
                        "Validacion", "Error")), response.Mensaje, ex.StackTrace.Trim)

            End Try

            'registrar parametros de salida
            Using oCtrlComun As New SPE.Web.CComun
                oCtrlComun.RegistrarLogkasnet(Log.MetodosKasnet.extornar_response, idLog, response.ObjKasnet, obeLog)
            End Using

            'excepcion general
        Catch ex As Exception
            'log de archivos
            '_3Dev.FW.Web.Log.Logger.LogException(ex)

            ''verificar estado de la consulta
            'If response.Estado <> (-1) Then Return response.ObjKasnet

            ''codigo de resultado para Kasnet
            'response.ObjKasnet.codigoResultado = WSBancoMensaje.Kasnet.Mensaje.NoSePudoRealizarRegistroExtorno

            ''mensaje de resultado de Kasnet
            'Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
            '    response.ObjKasnet.mensajeResultado = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.mensajeResultado)
            'End Using


            'verificar estado de la consulta
            If response.Estado = (-1) Then

                'codigo de resultado para Kasnet
                'response.ObjKasnet.codigoResultado = WSBancoMensaje.Kasnet.Mensaje.NoSePudoRealizarRegistroExtorno

                'mensaje de resultado de Kasnet
                Using oCtrlAB As New SPE.Web.CAgenciaBancariaKasnet
                    'response.ObjKasnet.mensajeResultado = oCtrlAB.ListaCodTransaccionKasnet(response.ObjKasnet.mensajeResultado)
                    response.ObjKasnet = oCtrlAB.ExtornarDataInicial(request)
                End Using

            End If

        End Try

        Return response.ObjKasnet

    End Function

End Class