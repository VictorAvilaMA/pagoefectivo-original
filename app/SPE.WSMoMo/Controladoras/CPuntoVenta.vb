Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CPuntoVenta
        Implements IDisposable

        Public Shared MensajeNoSePudoRealizarTransaccion As String = "No se pudo realizar la transacción"

        Public Function ConsultarMoMo(ByVal request As BEMoMoConsultarRequest) As BEMoMoResponse(Of BEMoMoConsultarResponse)
            Using Conexions As New ProxyBase(Of IPuntoVenta)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarMomo(request)
            End Using


        End Function

        Public Function CancelarMoMo(ByVal request As BEMoMoCancelarRequest) As BEMoMoResponse(Of BEMoMoCancelarResponse)
            Using Conexions As New ProxyBase(Of IPuntoVenta)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).CancelarMoMo(request)
            End Using
        End Function

        Public Function AnularMoMo(ByVal request As BEMoMoAnularRequest) As BEMoMoResponse(Of BEMoMoAnularResponse)
            Using Conexions As New ProxyBase(Of IPuntoVenta)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).AnularMoMo(request)
            End Using
        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
