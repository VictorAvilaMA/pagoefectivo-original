Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Public Class CSeguridad

    Public Sub New()

    End Sub

    Public Function ValidarServicioClaveAPI(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean

        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().ValidarServicioClaveAPI(claveAPI, claveSecreta)
        End Using
        'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).ISeguridad.ValidarServicioClaveAPI(claveAPI, claveSecreta)
    End Function

End Class
