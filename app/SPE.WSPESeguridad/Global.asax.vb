Imports System.Web.SessionState
Imports _3Dev.FW.Web.Log

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Function EstablishConnection() As Boolean
        Return SPE.Web.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function


    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)

        Try
            SPE.Web.RemoteServices.Instance = New SPE.Web.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try


        If Not EstablishConnection() Then
            Return
        End If

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Logger.LogException(Me.Server.GetLastError.GetBaseException)
        Logger.LogTransaction("Error de Transacción")

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class