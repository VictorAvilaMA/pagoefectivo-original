Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Namespace SPE.Web
    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices

        Private _ISeguridad As EmsambladoComun.ISeguridad

        Public Property ISeguridad() As EmsambladoComun.ISeguridad
            Get
                Return _ISeguridad
            End Get
            Set(ByVal value As EmsambladoComun.ISeguridad)
                _ISeguridad = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()
            MyBase.DoSetupNetworkEnviroment()
            'Seguridad
            Me.ISeguridad = CType(Activator.GetObject(GetType(EmsambladoComun.ISeguridad), Me.ServerUrl + "/" + NombreServiciosConocidos.ISeguridad), EmsambladoComun.ISeguridad)

        End Sub

    End Class

End Namespace