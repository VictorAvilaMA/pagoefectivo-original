Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WSPEConexion
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function ValidarAPIConexion(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean
      dim resultado as Boolean =False 
        Try
            resultado = (New CSeguridad()).ValidarServicioClaveAPI(claveAPI, claveSecreta)
        Catch ex As Exception
            Throw ex
        End Try
        Return resultado

    End Function

End Class