Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun


Namespace SPE.Web

    Public Class CEstablecimiento

        Public Function ConsultarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
            Using Conexions As New ProxyBase(Of IEstablecimiento)
                Return Conexions.DevolverContrato().ConsultarOrdenPago(obeParametroPOS)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IEstablecimiento.ConsultarOrdenPago(obeParametroPOS)
        End Function

        Public Function CancelarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
            Using Conexions As New ProxyBase(Of IEstablecimiento)
                Return Conexions.DevolverContrato().CancelarOrdenPago(obeParametroPOS)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IEstablecimiento.CancelarOrdenPago(obeParametroPOS)
        End Function

        Public Function AnularOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String
            Using Conexions As New ProxyBase(Of IEstablecimiento)
                Return Conexions.DevolverContrato().AnularOrdenPago(obeParametroPOS)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IEstablecimiento.AnularOrdenPago(obeParametroPOS)
        End Function

        Public Function AperturarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
            Using Conexions As New ProxyBase(Of IEstablecimiento)
                Return Conexions.DevolverContrato().AperturarCaja(obeParametroPOS)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IEstablecimiento.AperturarCaja(obeParametroPOS)
        End Function

        Public Function CerrarCaja(ByVal obeParametroPOS As BEParametroPOS) As String
            Using Conexions As New ProxyBase(Of IEstablecimiento)
                Return Conexions.DevolverContrato().CerrarCaja(obeParametroPOS)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IEstablecimiento.CerrarCaja(obeParametroPOS)
        End Function

    End Class

End Namespace
