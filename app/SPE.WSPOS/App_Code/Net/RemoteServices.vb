Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Namespace SPE.Web

    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices

        Private _IEstablecimiento As SPE.EmsambladoComun.IEstablecimiento
        Public Property IEstablecimiento() As SPE.EmsambladoComun.IEstablecimiento
            Get
                Return _IEstablecimiento
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IEstablecimiento)
                _IEstablecimiento = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()

            MyBase.DoSetupNetworkEnviroment()
            Me.IEstablecimiento = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IEstablecimiento), Me.ServerUrl + "/" + NombreServiciosConocidos.IEstablecimiento), SPE.EmsambladoComun.IEstablecimiento)

        End Sub

    End Class

End Namespace

