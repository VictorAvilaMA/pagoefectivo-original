Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports _3Dev.FW.Util

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
     Inherits System.Web.Services.WebService

    <WebMethod()> _
     Public Function ConsultarOrdenPago(ByVal CodComercio As String _
                                        , ByVal CodEstablecimiento As String _
                                        , ByVal CodTransaccion As Integer _
                                        , ByVal CodOrdenPago As String) As String

        Dim CEstablecimiento As New SPE.Web.CEstablecimiento
        Dim obeParametroPOS As New SPE.Entidades.BEParametroPOS
        Dim Resultado As String = ""

        Try
            obeParametroPOS.NroEstablecimiento = CodEstablecimiento.ToString().Trim
            obeParametroPOS.NroOrdenPago = CodOrdenPago.ToString().Trim
            obeParametroPOS.codComercio = CodComercio.ToString().Trim

            Resultado = CEstablecimiento.ConsultarOrdenPago(obeParametroPOS)

        Catch ex As Exception

            Dim objUtil As New SPE.Web.Util.UtilXML
            Resultado = objUtil.GenerarXMLErroPOS()
            _3Dev.FW.Web.Log.Logger.LogException(ex)

        End Try
        Return Resultado
    End Function

    <WebMethod()> _
    Public Function CancelarOrdenPago(ByVal CodComercio As String, ByVal NroEstablecimiento As String, ByVal NroOperacion As String, _
    ByVal IdMedioPago As Integer, ByVal NroOrdenPago As String, _
    ByVal Monto As Decimal, ByVal IdMoneda As Integer) As String


        Dim CEstablecimiento As New SPE.Web.CEstablecimiento
        Dim obeParametroPOS As New SPE.Entidades.BEParametroPOS
        Dim resultado As String = ""
        Try
            obeParametroPOS.codComercio = CodComercio.ToString()
            obeParametroPOS.NroEstablecimiento = NroEstablecimiento.ToString()
            obeParametroPOS.NroOperacion = NroOperacion.ToString()
            obeParametroPOS.NroOrdenPago = NroOrdenPago.ToString()
            obeParametroPOS.Monto = Monto
            obeParametroPOS.IdMoneda = IdMoneda
            obeParametroPOS.IdMedioPago = IdMedioPago

            resultado = CEstablecimiento.CancelarOrdenPago(obeParametroPOS)

        Catch ex As Exception
            Dim objUtil As New SPE.Web.Util.UtilXML
            resultado = objUtil.GenerarXMLErroPOS()
            _3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try

        Return resultado

    End Function

    <WebMethod()> _
    Public Function AnularOrdenPago(ByVal CodComercio As String, ByVal NroEstablecimiento As String, _
    ByVal NroOperacion As String, ByVal NroOperacionAnular As String, ByVal IdMedioPago As Integer _
    , ByVal NroOrdenPago As String) As String

        Dim CEstablecimiento As New SPE.Web.CEstablecimiento
        Dim obeParametroPOS As New SPE.Entidades.BEParametroPOS
        Dim resultado As String = ""

        Try
            obeParametroPOS.codComercio = CodComercio.ToString()
            obeParametroPOS.NroEstablecimiento = NroEstablecimiento.ToString()
            obeParametroPOS.NroOperacion = NroOperacion.ToString()
            obeParametroPOS.NroOperacionAnular = NroOperacionAnular.ToString()
            obeParametroPOS.IdMedioPago = IdMedioPago
            obeParametroPOS.NroOrdenPago = NroOrdenPago.ToString()

            resultado = CEstablecimiento.AnularOrdenPago(obeParametroPOS)

        Catch ex As Exception
            Dim objUtil As New SPE.Web.Util.UtilXML
            resultado = objUtil.GenerarXMLErroPOS()
            _3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return resultado

    End Function


End Class
