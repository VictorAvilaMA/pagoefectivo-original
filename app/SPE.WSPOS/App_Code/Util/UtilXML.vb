Imports System.Xml
Imports System.IO
Imports Microsoft.VisualBasic

Namespace SPE.Web.Util

    Public Class UtilXML
        Public Sub New()

        End Sub


        Public Function GenerarXMLErroPOS() As String


            Dim retorna As String

            Dim memory_stream As MemoryStream = New MemoryStream()

            Dim xml_text_writer As XmlTextWriter = New XmlTextWriter(memory_stream, System.Text.Encoding.UTF8)

            xml_text_writer.Formatting = Formatting.Indented
            xml_text_writer.Indentation = 4


            xml_text_writer.WriteStartElement("DATOS_TX")

            xml_text_writer.WriteElementString("RESPUESTA", SPE.EmsambladoComun.ParametrosSistema.CodigoMensajeErrorWSPOs.respuestaError)
            xml_text_writer.WriteElementString("MENSAJE", SPE.EmsambladoComun.ParametrosSistema.MensajeErrorWebServicePOS)

            xml_text_writer.WriteEndElement()


            xml_text_writer.Flush()

            'Declaramos un StreamReader para mostrar el resultado.
            Dim stream_reader As StreamReader = New StreamReader(memory_stream)

            memory_stream.Seek(0, SeekOrigin.Begin)
            retorna = stream_reader.ReadToEnd()


            xml_text_writer.Close()
            stream_reader.Close()
            stream_reader.Dispose()

            Return retorna

        End Function
    End Class

End Namespace