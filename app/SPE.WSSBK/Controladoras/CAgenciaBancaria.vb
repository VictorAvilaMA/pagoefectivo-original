Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CAgenciaBancaria
        Implements IDisposable

        Public Function ConsultarSBK(ByVal request As BESBKConsultarRequest) As BEBKResponse(Of BESBKConsultarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ConsultarSBK(request)
            End Using
        End Function

        Public Function CancelarSBK(ByVal request As BESBKCancelarRequest) As BEBKResponse(Of BESBKCancelarResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).CancelarSBK(request)
            End Using
        End Function

        Public Function AnularSBK(ByVal request As BESBKAnularRequest) As BEBKResponse(Of BESBKAnularResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).AnularSBK(request)
            End Using
        End Function

        Public Function ExtonarCancelacionSBK(ByVal request As BESBKExtornarCancelacionRequest) As BEBKResponse(Of BESBKExtornarCancelacionResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarCancelacionSBK(request)
            End Using
        End Function

        Public Function ExtonarAnulacionSBK(ByVal request As BESBKExtornarAnulacionRequest) As BEBKResponse(Of BESBKExtornarAnulacionResponse)
            Using Conexions As New ProxyBase(Of IAgenciaBancaria)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ExtornarAnulacionSBK(request)
            End Using
        End Function

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
