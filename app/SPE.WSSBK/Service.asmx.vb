﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

'<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
'<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
'<ToolboxItem(False)> _

'System.Web.Services.WebServiceBinding(Name:="EjecTransaccionScotiabankSoapBinding", ConformsTo:=WsiProfiles.BasicProfile1_1), _
'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.1432"), _
 System.Web.Services.WebService(Name:="EjecTransaccionScotiabankService", _
     Namespace:="http://scotia.ws.creo.com"), _
 System.Web.Services.WebServiceBindingAttribute(Name:="EjecTransaccionScotiabank", _
     [Namespace]:="http://scotia.ws.creo.com")> _
Public Class Service
    Inherits System.Web.Services.WebService
    Implements IEjecTransaccionScotiabankSoapBinding

    '<WebMethod(Description:="ejecutarTransaccionScotiabank")> _
    '<SoapDocumentMethod(Action:="")> _

    '''<remarks/>
    <System.Web.Services.WebMethod(), _
     System.Web.Services.Protocols.SoapDocumentMethodAttribute("", _
         Binding:="EjecTransaccionScotiabank", _
         RequestElementName:="ejecutarTransaccionScotiabank", RequestNamespace:="http://scotia.ws.creo.com", _
         ResponseElementName:="ejecutarTransaccionScotiabankResponse", ResponseNamespace:="http://scotia.ws.creo.com", _
         Use:=System.Web.Services.Description.SoapBindingUse.Literal, _
         ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)> _
    Public Function ejecutarTransaccionScotiabank(ByVal Input As String) As <System.Xml.Serialization.XmlElementAttribute("ejecutarTransaccionScotiabankReturn")> String Implements IEjecTransaccionScotiabankSoapBinding.ejecutarTransaccionScotiabank
        Dim response As String = ""
        Try
            Dim SBKrequest As BEBKBaseRequest = ObtenerObjetoRequest(Input, Context.Request.ServerVariables("REMOTE_ADDR"))
            If SBKrequest.EsValido Then
                If SBKrequest.GetType Is GetType(BESBKConsultarRequest) Then
                    response = Consultar(SBKrequest).ToString
                ElseIf SBKrequest.GetType Is GetType(BESBKCancelarRequest) Then
                    response = Cancelar(SBKrequest).ToString
                ElseIf SBKrequest.GetType Is GetType(BESBKAnularRequest) Then
                    response = Anular(SBKrequest).ToString
                ElseIf SBKrequest.GetType Is GetType(BESBKExtornarCancelacionRequest) Then
                    response = ExtornarCancelacion(SBKrequest).ToString
                ElseIf SBKrequest.GetType Is GetType(BESBKExtornarAnulacionRequest) Then
                    response = ExtornarAnulacion(SBKrequest).ToString
                End If
            Else
                'TODO
                response = ResolverTramaInvalida(Input, SBKrequest.IP, Nothing)
            End If
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            response = ResolverTramaInvalida(Input, Context.Request.ServerVariables("REMOTE_ADDR"), ex.ToString())
            Return response
        End Try
    End Function

    '<WebMethod()> _
    'Public Function ejecutarTransaccionScotiabank1(ByVal request As ejecutarTransaccionScotiabankRequest) As ejecutarTransaccionScotiabankResponse
    '    Dim response As New ejecutarTransaccionScotiabankResponse(New ejecutarTransaccionScotiabankResponseBody())
    '    Try
    '        Dim SBKrequest As BEBKBaseRequest = ObtenerObjetoRequest(request.Body.Input, Context.Request.ServerVariables("REMOTE_ADDR"))
    '        If SBKrequest.EsValido Then
    '            If SBKrequest.GetType Is GetType(BESBKConsultarRequest) Then
    '                response.Body.ejecutarTransaccionScotiabankReturn = Consultar(SBKrequest).ToString
    '            ElseIf SBKrequest.GetType Is GetType(BESBKCancelarRequest) Then
    '                response.Body.ejecutarTransaccionScotiabankReturn = Cancelar(SBKrequest).ToString
    '            ElseIf SBKrequest.GetType Is GetType(BESBKAnularRequest) Then
    '                response.Body.ejecutarTransaccionScotiabankReturn = Anular(SBKrequest).ToString
    '            ElseIf SBKrequest.GetType Is GetType(BESBKExtornarCancelacionRequest) Then
    '                response.Body.ejecutarTransaccionScotiabankReturn = ExtornarCancelacion(SBKrequest).ToString
    '            ElseIf SBKrequest.GetType Is GetType(BESBKExtornarAnulacionRequest) Then
    '                response.Body.ejecutarTransaccionScotiabankReturn = ExtornarAnulacion(SBKrequest).ToString
    '            End If
    '        Else
    '            'TODO
    '            response.Body.ejecutarTransaccionScotiabankReturn = ResolverTramaInvalida(request.Body.Input, SBKrequest.IP, Nothing)
    '        End If
    '        Return response
    '    Catch ex As Exception
    '        _3Dev.FW.Web.Log.Logger.LogException(ex)
    '        'TODO
    '        response.Body.ejecutarTransaccionScotiabankReturn = ResolverTramaInvalida(request.Body.Input, Context.Request.ServerVariables("REMOTE_ADDR"), ex.Message)
    '        Return response
    '    End Try
    'End Function

    Private Function ResolverTramaInvalida(ByVal Trama As String, ByVal IP As String, ByVal Mensaje As String) As String
        Try
            Dim IdLogRequest As Long = 0
            Try
                IdLogRequest = UtilTrace.DoTrace("WSSBK - Request no válido", Trama, IP, Mensaje)
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
            End Try
            UtilTrace.DoTrace("WSSBK - Response de Request no válido", Trama, IP, IdLogRequest)
            Return Trama
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Return Trama
        End Try
    End Function

    Private Function Consultar(ByVal request As BESBKConsultarRequest) As BESBKConsultarResponse
        Dim response As New BESBKConsultarResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.consultar_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BESBKConsultarResponse) = CntrlAgenciaBancaria.ConsultarSBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.consultar_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw ex
        End Try
    End Function

    Private Function Cancelar(ByVal request As BESBKCancelarRequest) As BESBKCancelarResponse
        Dim response As New BESBKCancelarResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.cancelar_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BESBKCancelarResponse) = CntrlAgenciaBancaria.CancelarSBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                _3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.cancelar_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function Anular(ByVal request As BESBKAnularRequest) As BESBKAnularResponse
        Dim response As New BESBKAnularResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.anular_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BESBKAnularResponse) = CntrlAgenciaBancaria.AnularSBK(request)                 
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                _3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.anular_response, idLog, response, oBELog)
            End Using


            Return response

        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function ExtornarCancelacion(ByVal request As BESBKExtornarCancelacionRequest) As BESBKExtornarCancelacionResponse
        Dim response As New BESBKExtornarCancelacionResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.cancelarauto_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BESBKExtornarCancelacionResponse) = CntrlAgenciaBancaria.ExtonarCancelacionSBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                _3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.cancelarauto_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function ExtornarAnulacion(ByVal request As BESBKExtornarAnulacionRequest) As BESBKExtornarAnulacionResponse
        Dim response As New BESBKExtornarAnulacionResponse
        Dim oBELog As New BELog
        Try
            Dim idLog As Long = 0
            Try
                Using cntrlComun As New SPE.Web.CComun
                    idLog = cntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.anularauto_request, Nothing, request, oBELog)
                End Using
                Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                    Dim responseG As BEBKResponse(Of BESBKExtornarAnulacionResponse) = CntrlAgenciaBancaria.ExtonarAnulacionSBK(request)
                    response = responseG.ObjBEBK
                    With oBELog
                        .Descripcion = responseG.Descripcion1
                        .Descripcion2 = responseG.Descripcion2
                    End With
                End Using
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                'TODO
                oBELog.Descripcion = ex.StackTrace
                oBELog.Descripcion2 = ex.Message
            End Try
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogSBK(Log.MetodosScotiabank.anularauto_response, idLog, response, oBELog)
            End Using
            Return response
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            'TODO
            Return response
        End Try
    End Function

    Private Function ObtenerObjetoRequest(ByVal Trama As String, ByVal IP As String) As BEBKBaseRequest
        Dim SBKrequest As BEBKBaseRequest = Nothing
        Using CntrlComun As New SPE.Web.CComun()
            Try
                Select Case Trama.Substring(54, 6)
                    Case ParametrosSistema.Scotiabank.CodigoProceso.Consulta
                        SBKrequest = New BESBKConsultarRequest(Trama)
                    Case ParametrosSistema.Scotiabank.CodigoProceso.Pago
                        Select Case Trama.Substring(0, 4)
                            Case ParametrosSistema.Scotiabank.IdentificacionTipoMensaje.Solicitud
                                SBKrequest = New BESBKCancelarRequest(Trama)
                            Case ParametrosSistema.Scotiabank.IdentificacionTipoMensaje.SolicitudAutomatica
                                SBKrequest = New BESBKExtornarCancelacionRequest(Trama)
                        End Select
                    Case ParametrosSistema.Scotiabank.CodigoProceso.Anulacion
                        Select Case Trama.Substring(0, 4)
                            Case ParametrosSistema.Scotiabank.IdentificacionTipoMensaje.Solicitud
                                SBKrequest = New BESBKAnularRequest(Trama)
                            Case ParametrosSistema.Scotiabank.IdentificacionTipoMensaje.SolicitudAutomatica
                                SBKrequest = New BESBKExtornarAnulacionRequest(Trama)
                        End Select
                End Select
                If SBKrequest Is Nothing Then
                    SBKrequest = New BEBKBaseRequest(Trama)
                Else
                    SBKrequest.EsValido = True
                End If
            Catch ex As Exception
                SBKrequest = New BEBKBaseRequest(Trama)
            End Try
        End Using
        SBKrequest.IP = IP
        Return SBKrequest
    End Function

End Class