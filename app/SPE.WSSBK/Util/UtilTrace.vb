Imports System.Xml
Imports System.Text
Imports System.IO
Imports Microsoft.VisualBasic

Imports SPE.Entidades
Imports SPE.EmsambladoComun

Public Class UtilTrace

    Public Shared Function DoTrace(ByVal origen As String, ByVal Descripcion As String, _
        ByVal Parametro1 As String, ByVal Parametro2 As String) As Int64

        Dim obeLog As BELog
        Dim idLog As Int64 = 0

        If (ConfigurationManager.AppSettings("HabilitarTraceEventViewer") = "1") Then
            System.Diagnostics.EventLog.WriteEntry("Pago Efectivo - WSSBK", DateTime.Now.ToString + " tipo:" + origen + "| log:" + Descripcion + "ip:" + Parametro1)
        End If

        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using obeCComun As New SPE.Web.CComun()
                obeLog = New BELog()
                With obeLog
                    .IdTipo = ParametrosSistema.Log.Tipo.idTipoServiciosWebSBK
                    .Origen = origen
                    .Descripcion = Descripcion
                    .Parametro1 = Parametro1
                    .Parametro2 = Parametro2
                End With
                idLog = obeCComun.RegistrarLog(obeLog)
            End Using
        End If

        Return idLog

    End Function

End Class
