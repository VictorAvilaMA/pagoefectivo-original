Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CComun
        Implements IDisposable

        Public Function RegistrarLog(ByVal obelog As BELog) As Integer
            Using Conexions As New ProxyBase(Of IComun)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).RegistrarLog(obelog)
            End Using
        End Function

        Public Function RegistrarLogWU(ByVal type As String, ByVal obeid As Int64, ByVal obeWU As Object, _
            ByVal oBELog As BELog) As Int64
            If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
                Using Conexions As New ProxyBase(Of IComun)
                    Return Conexions.DevolverContrato(New EntityBaseContractResolver()).RegistrarLogWU(type, obeid, obeWU)
                End Using
            Else
                Return 0
            End If
        End Function

        Private disposedValue As Boolean = False
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                End If
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
