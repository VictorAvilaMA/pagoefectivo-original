﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports SPE.EmsambladoComun.ParametrosSistema

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.0.30319.1"), _
 System.Web.Services.WebServiceBindingAttribute(Name:="genericoMethodsSoapBinding", [Namespace]:="http://operations.genericows.sibs.com.ar")> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Services.Protocols.SoapDocumentService(RoutingStyle:=SoapServiceRoutingStyle.RequestElement)> _
<ToolboxItem(False)> _
Public Class Service
    Inherits System.Web.Services.WebService
    'Implements IGenericoMethodsSoapBinding
    <System.Web.Services.WebMethodAttribute(), _
     System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace:="http://operations.genericows.sibs.com.ar", _
         ResponseNamespace:="http://operations.genericows.sibs.com.ar", Use:=System.Web.Services.Description.SoapBindingUse.Literal, _
         ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)> _
    Public Function consulta(ByVal hdReq As HeaderReq, ByVal utility As String, ByVal cod_barra As String, ByVal codigo_cliente As String) As <System.Xml.Serialization.XmlElementAttribute("consultaReturn")> ConsultaResp
        Dim request As New BEWUConsultarRequest
        Dim response As New BEWUConsultarResponse
        Dim responseWU As New ConsultaResp
        Dim oBELog As New BELog

        request.HdReq = fixNroSecuencia(hdReq) 'se fixea el Nro de Secuencia para el caso que envie con ceros a la izquierda
        request.Utility = utility
        request.Cod_barra = cod_barra
        request.Codigo_cliente = codigo_cliente
        request.CodigoServicio = System.Configuration.ConfigurationManager.AppSettings("CodigoServicio")

        Try
            Dim idLog As Long = 0
            Using cntrlComun As New SPE.Web.CComun
                idLog = cntrlComun.RegistrarLogWU(Log.MetodosWesternUnion.consultar_request, Nothing, request, oBELog)
            End Using
            Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                response = CntrlAgenciaBancaria.ConsultarWU(request)
                responseWU = response.ObjBEBK
            End Using
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogWU(Log.MetodosWesternUnion.consultar_response, idLog, response, oBELog)
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            responseWU.header = resolverTransacciónFallida(hdReq)
            responseWU.fields = New List(Of ArrayFieldsQuerie)().ToArray()
            response.ObjBEBK = New ConsultaResp()
            response.ObjBEBK.count = 0
            response.ObjBEBK.cob_cliente_nomb = String.Empty
            response.ObjBEBK.seleccion_con_prioridad = 0
        End Try
        Return responseWU
    End Function
    <System.Web.Services.WebMethodAttribute(), _
     System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace:="http://operations.genericows.sibs.com.ar", ResponseNamespace:="http://operations.genericows.sibs.com.ar", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)> _
    Public Function directa(hdReq As HeaderReq, check As Check, utility As String, barCode As String, medioPago As String, creditCard As String, amount As Integer) As DirectaResp
        Dim request As New BEWUCancelarRequest
        Dim response As New BEWUCancelarResponse
        Dim responseWU As New DirectaResp
        Dim oBELog As New BELog

        request.HdReq = fixNroSecuencia(hdReq) 'se fixea el Nro de Secuencia para el caso que envie con ceros a la izquierda
        request.Check = check
        request.Utility = utility
        request.BarCode = barCode
        request.MedioPago = medioPago
        request.CreditCard = creditCard
        request.Amount = UtilTrace.parseInt32ToDoubleWU(amount)
        request.CodigoServicio = System.Configuration.ConfigurationManager.AppSettings("CodigoServicio")

        Try
            Dim idLog As Long = 0
            Using cntrlComun As New SPE.Web.CComun
                idLog = cntrlComun.RegistrarLogWU(Log.MetodosWesternUnion.cancelar_request, Nothing, request, oBELog)
            End Using
            Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                response = CntrlAgenciaBancaria.CancelarWU(request)
                responseWU = response.ObjBEBK
            End Using
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogWU(Log.MetodosWesternUnion.cancelar_response, idLog, response, oBELog)
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            responseWU.header = resolverTransacciónFallida(hdReq)
            responseWU.msg = String.Empty
        End Try
        Return responseWU
    End Function
    <System.Web.Services.WebMethodAttribute(), _
     System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace:="http://operations.genericows.sibs.com.ar", ResponseNamespace:="http://operations.genericows.sibs.com.ar", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)> _
    Public Function reversa(hdReq As HeaderReq, terminalOriginal As String, cajeroOriginal As String, fechaHoraOriginal As Date, nroSecuenciaOriginal As String, tipoReversa As String, utility As String, amount As Integer) As ReversaResp
        Dim request As New BEWUAnularRequest
        Dim response As New BEWUAnularResponse
        Dim responseWU As New ReversaResp
        Dim oBELog As New BELog

        request.HdReq = fixNroSecuencia(hdReq) 'se fixea el Nro de Secuencia para el caso que envie con ceros a la izquierda
        request.TerminalOriginal = terminalOriginal
        request.CajeroOriginal = cajeroOriginal
        request.FechaHoraOriginal = fechaHoraOriginal
        request.NroSecuenciaOriginal = fixNroSecuencia(nroSecuenciaOriginal) 'se fixea el Nro de Secuencia para el caso que envie con ceros a la izquierda
        request.TipoReversa = tipoReversa
        request.Utility = utility
        request.Amount = amount
        request.CodigoServicio = System.Configuration.ConfigurationManager.AppSettings("CodigoServicio")

        Try
            Dim idLog As Long = 0
            Using cntrlComun As New SPE.Web.CComun
                idLog = cntrlComun.RegistrarLogWU(Log.MetodosWesternUnion.anular_request, Nothing, request, oBELog)
            End Using
            Using CntrlAgenciaBancaria As New SPE.Web.CAgenciaBancaria
                response = CntrlAgenciaBancaria.AnularWU(request)
                responseWU = response.ObjBEBK
            End Using
            Using CntrlComun As New SPE.Web.CComun
                CntrlComun.RegistrarLogWU(Log.MetodosWesternUnion.anular_response, idLog, response, oBELog)
            End Using
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            responseWU.header = resolverTransacciónFallida(hdReq)
            responseWU.estado = String.Empty 'no se usa
            responseWU.operador = String.Empty 'no se usa
            responseWU.ticket = String.Empty 'no se usa
        End Try
        Return responseWU
    End Function

    Private Function resolverTransacciónFallida(ByVal request As HeaderReq) As HeaderResp
        Dim response As New HeaderResp
        With response
            .algoritmo = request.algoritmo
            .cajero = request.cajero
            .fechaHora = request.fechaHora
            .idMensaje = request.idMensaje
            .marca = request.marca
            .nroSecuencia = request.nroSecuencia
            .terminal = request.terminal
            .version = request.version
            .codError = WesternUnion.CodError.ErrorInternoEntidad
            .codSeveridad = WesternUnion.CodSeveridad.uno
        End With
        Return response
    End Function
    Private Function fixNroSecuencia(ByVal request As HeaderReq) As HeaderReq
        Try
            If System.Configuration.ConfigurationManager.AppSettings("FixNroSecuencia") = "1" And request IsNot Nothing Then
                Dim valueInt As Integer
                If Integer.TryParse(request.nroSecuencia, valueInt) Then
                    request.nroSecuencia = valueInt.ToString
                End If
            End If
            Return request
        Catch ex As Exception
            Return request
        End Try
    End Function
    Private Function fixNroSecuencia(ByVal nroSecuencia As String) As String
        Try
            If System.Configuration.ConfigurationManager.AppSettings("FixNroSecuencia") = "1" And Not String.IsNullOrEmpty(nroSecuencia) Then
                Dim valueInt As Integer
                If Integer.TryParse(nroSecuencia, valueInt) Then
                    nroSecuencia = valueInt.ToString
                End If
            End If
            Return nroSecuencia
        Catch ex As Exception
            Return nroSecuencia
        End Try
    End Function
End Class