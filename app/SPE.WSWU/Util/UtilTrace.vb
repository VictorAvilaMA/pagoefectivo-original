Imports System.Xml
Imports System.Text
Imports System.IO
Imports Microsoft.VisualBasic

Imports SPE.Entidades
Imports SPE.EmsambladoComun

Public Class UtilTrace

    Public Shared Function DoTrace(ByVal origen As String, ByVal Descripcion As String, ByVal Parametro1 As String, ByVal Parametro2 As String) As Int64

        Dim obeLog As BELog
        Dim idLog As Int64 = 0

        If (ConfigurationManager.AppSettings("HabilitarTraceEventViewer") = "1") Then
            System.Diagnostics.EventLog.WriteEntry("Pago Efectivo - WSWU", DateTime.Now.ToString + " tipo:" + origen + "| log:" + Descripcion + "ip:" + Parametro1)
        End If

        If (ConfigurationManager.AppSettings("HabilitarRegistroLog") = "1") Then
            Using obeCComun As New SPE.Web.CComun()
                obeLog = New BELog()
                With obeLog
                    .IdTipo = ParametrosSistema.Log.Tipo.idTipoServiciosWebWU
                    .Origen = origen
                    .Descripcion = Descripcion
                    .Parametro1 = Parametro1
                    .Parametro2 = Parametro2
                End With
                idLog = obeCComun.RegistrarLog(obeLog)
            End Using
        End If

        Return idLog

    End Function

    Public Shared Function parseInt32ToDoubleWU(ByVal valor As Int32) As Double
        Dim valorString As String = valor.ToString()
        Dim doubleString As String = String.Empty
        If valorString.Length > 2 Then
            doubleString = valorString.Substring(0, valorString.Length - 2) + "." + valorString.Substring(valorString.Length - 2, 2)
        Else
            doubleString = "0." + valorString.PadLeft(2, "0")
        End If
        Return Double.Parse(doubleString)
    End Function
End Class
