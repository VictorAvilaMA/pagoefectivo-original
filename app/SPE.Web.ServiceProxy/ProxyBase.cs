﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Discovery;
using System.Collections;
using System.ServiceModel;

namespace SPE.Web.ServiceProxy
{
    public class ProxyBase<T>:IDisposable
    {
        public ChannelFactory<T> Conexion { get; set; }

        public T DevolverContrato()
        {
            DiscoveryClient discovery = new DiscoveryClient(new UdpDiscoveryEndpoint());
            FindCriteria criterio = new FindCriteria(typeof(T));
            criterio.Duration = TimeSpan.FromSeconds(5);
            FindResponse findresponse = discovery.Find(criterio);

            var direccion = findresponse.Endpoints.First(p => p.Address.Uri.Scheme == "net.tcp").Address;
            Conexion = new ChannelFactory<T>(new NetTcpBinding(), direccion);
            return Conexion.CreateChannel();
        }

        public void Dispose()
        {
            Conexion.Close();
        }
    }
    //public class MyClass
    //{
    //    void sdfsdf()
    //    {
    //        ProxyBase<IOnlineStatus> c;
    //        c.DevolverContrato().
    //    }
    //}

}
