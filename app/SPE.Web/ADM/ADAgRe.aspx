<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ADM/ADAgRe.aspx.vb" Inherits="ADM_ADAgRe" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../UC/UCAudit.ascx" TagName="UCAudit" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/admin.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblTitle" runat="server" Text="Registrar / Actualizar Agencia Recaudadora"></asp:Literal>
    </h2>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional"
        class="conten_pasos3">
        <ContentTemplate>
            <asp:Panel ID="pnlPage" runat="server">
                <h4>
                    1. Informaci&oacute;n general</h4>
                <div id="fisIdAgencia" runat="server">
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Identificador de Agencia: </li>
                        <li class="t2">
                            <asp:Label Style="text-align: left" ID="lblIdgencia" runat="server" CssClass="normal">0</asp:Label>
                        </li>
                    </ul>
                </div>
                <div id="Fieldset1" runat="server">
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Tipo de Agencia: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlTipoAgencia" runat="server" CssClass="neu" Style="float: left">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvTipoAgencia" runat="server" ErrorMessage="Seleccione un Tipo de Agencia"
                                ControlToValidate="ddlTipoAgencia" ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                </div>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Nombre Comercial: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreComercial" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNombreComercial" runat="server" ValidationGroup="IsNullOrEmpty"
                            ErrorMessage="Ingrese el Nombre Comercial" ControlToValidate="txtNombreComercial"
                            ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> R.U.C.: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtRuc" runat="server" CssClass="normal" MaxLength="11"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvRuc" runat="server" ValidationGroup="IsNullOrEmpty"
                            ErrorMessage="Ingrese el Ruc" ControlToValidate="txtRuc" ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rvaRuc" runat="server" ErrorMessage="Ingrese solo n�meros"
                            ControlToValidate="txtRuc" MaximumValue="100000000000" MinimumValue="0" Type="Double"
                            ValidationGroup="IsNullOrEmpty">*</asp:RangeValidator>
                        <asp:CustomValidator ID="cvRUC" runat="server" ClientValidationFunction="ValidarRuc"
                            ControlToValidate="txtRuc" Display="Dynamic" ErrorMessage="Ruc no v�lido" SetFocusOnError="True">*</asp:CustomValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Contacto: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtContacto" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Tel&eacute;fono: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Fax: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> E-mail: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ValidationGroup="IsNullOrEmpty"
                            ErrorMessage="Ingrese un E-mail" ControlToValidate="txtEmail" ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationGroup="IsNullOrEmpty"
                            ErrorMessage="Formato incorrecto de E-mail" ControlToValidate="txtEmail" ToolTip="Formato incorrecto de E-mail"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">**</asp:RegularExpressionValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Direcci&oacute;n: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                </ul>
                <asp:UpdatePanel ID="UpdatePanelUbigeo" runat="server">
                    <ContentTemplate>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Pa&iacute;s: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlPais" runat="server" CssClass="neu" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </li>
                            <li class="t1"><span class="color">>></span> Departamento: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="normal" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                                </asp:DropDownList>
                            </li>
                            <li class="t1"><span class="color">>></span> Ciudad: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="neu" AutoPostBack="True"
                                    Style="float: left">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="Seleccione una Ciudad"
                                    ControlToValidate="ddlCiudad" ValidationGroup="IsNullOrEmpty">*</asp:RequiredFieldValidator>
                            </li>
                        </ul>
                        <div id="fisEstado" runat="server">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Estado: </li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                                    </asp:DropDownList>
                                </li>
                            </ul>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="clear: both">
                </div>
                <asp:ValidationSummary ID="vsmMessage" runat="server" ValidationGroup="IsNullOrEmpty"
                    Style="padding-left: 200px"></asp:ValidationSummary>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtTelefono" runat="server" FilterType="Custom, Numbers"
                    TargetControlID="txtTelefono" ValidChars="-">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtFax" runat="server" FilterType="Custom, Numbers"
                    TargetControlID="txtFax" ValidChars="-">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtContacto" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtContacto">
                </cc1:FilteredTextBoxExtender>
                <div style="clear: both">
                </div>
            </asp:Panel>
            <ul class="datos_cip2">
                <li class="complet" style="height: auto">
                    <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" Visible="False" CssClass="input_azul3">
                    </asp:Button>
                    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" Visible="False" CssClass="input_azul3"
                        OnClientClick="return ConfirmMe();"></asp:Button>
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="input_azul4"
                        OnClientClick="return CancelMe();"></asp:Button>
                    <asp:Label ID="lblTransaccion" runat="server" CssClass="MensajeTransaccion" Width="650px"></asp:Label>
                </li>
            </ul>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRegistrar" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
            <asp:PostBackTrigger ControlID="btnCancelar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
