Imports System.Data
Imports SPE.Entidades
Imports SPE.Utilitario

Partial Class ADM_ADAgRe
    Inherits _3Dev.FW.Web.PageBase

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '
            btnRegistrar.Attributes.Add("OnClick", "return ConfirmMe();") ' agregado el 260509
            Try
                '
                Page.SetFocus(ddlTipoAgencia)
                CargarComboTipoAgencia()
                CargarComboEstado()
                '
                If Page.PreviousPage Is Nothing OrElse Context.Items.Item("IdAgencia") Is Nothing Then
                    CargarProcesoRegistro()
                Else
                    CargarProcesoActualizar(Convert.ToInt32(Context.Items.Item("IdAgencia")))
                End If
                '
            Catch ex As Exception
                '
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
                '
            End Try
            '
        End If
        '
    End Sub

    'REGISTRAR AGENCIA RECAUDADORA
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        '
        Try
            'Validaciones en el servidor agregado el 260509
            Dim a As SPE.Utilitario.ValidaRuc = New SPE.Utilitario.ValidaRuc()
            If (a.ValidarRuc(txtRuc.Text) = False) Then
                'Muestra(mensaje)
                lblTransaccion.Text = "Ruc no Valido"
                Exit Sub

            End If
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            objCAdministrarAgenciaRecaudadora.RegistrarAgenciaRecaudadora(CargarDatosBEAgencia())
            '
            btnRegistrar.Enabled = False
            btnCancelar.Text = "Regresar"
            btnCancelar.OnClientClick = ""
            pnlPage.Enabled = False
            lblTransaccion.Text = "Se registr� satisfactoriamente, la agencia: ''" & txtNombreComercial.Text & "''."
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Sub

    'ACTUALIZAR AGENCIA
    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        '
        Try
            '
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            objCAdministrarAgenciaRecaudadora.ActualizarAgenciaRecaudadora(CargarDatosBEAgencia())
            '
            btnActualizar.Enabled = False
            btnCancelar.Text = "Regresar"
            btnCancelar.OnClientClick = ""
            pnlPage.Enabled = False
            lblTransaccion.Text = "Se actualiz� satisfactoriamente, la agencia: ''" & txtNombreComercial.Text & "''."
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        
    End Sub

    'PROCESO DE REGISTRO
    Private Sub CargarProcesoRegistro()
        '
        lblTitle.Text = "Registrar Agencia Recaudadora"
        fisEstado.Visible = False
        fisIdAgencia.Visible = False
        btnRegistrar.Visible = True
        '
        CargarCombosUbigeo("Pais")
        CargarCombosUbigeo("Departamento")
        CargarCombosUbigeo("Ciudad")
        '
    End Sub

    'PROCESO DE ACTUALIZACION
    Private Sub CargarProcesoActualizar(ByVal IdAgencia As Integer)
        '
        lblTitle.Text = "Actualizar Agencia Recaudadora"
        fisEstado.Visible = True
        fisIdAgencia.Visible = False
        btnActualizar.Visible = True
        btnCancelar.Visible = True
        '
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
        obeAgenciaRecaudadora = objCAdministrarAgenciaRecaudadora.ConsultarAgenciaRecaudadoraPorIdAgencia(IdAgencia)

        lblIdgencia.Text = IdAgencia.ToString
        ddlTipoAgencia.SelectedValue = obeAgenciaRecaudadora.IdTipoAgenciaRecaudadora
        txtNombreComercial.Text = obeAgenciaRecaudadora.NombreComercial
        txtRuc.Text = obeAgenciaRecaudadora.Ruc
        txtContacto.Text = obeAgenciaRecaudadora.Contacto
        txtTelefono.Text = obeAgenciaRecaudadora.Telefono
        txtFax.Text = obeAgenciaRecaudadora.Fax
        txtDireccion.Text = obeAgenciaRecaudadora.Direccion
        txtEmail.Text = obeAgenciaRecaudadora.Email
        ddlEstado.SelectedValue = obeAgenciaRecaudadora.IdEstado
        CargarCombosUbigeo("Pais")
        ddlPais.SelectedValue = obeAgenciaRecaudadora.IdPais
        CargarCombosUbigeo("Departamento")
        ddlDepartamento.SelectedValue = obeAgenciaRecaudadora.IdDepartamento
        CargarCombosUbigeo("Ciudad")
        ddlCiudad.SelectedValue = obeAgenciaRecaudadora.IdCiudad
        '
    End Sub

    'CARGAMOS LOS DATOS DE LA AGENCIA RECAUDADORA
    Private Function CargarDatosBEAgencia() As BEAgenciaRecaudadora
        '
        Try
            '
            Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
            obeAgenciaRecaudadora.IdAgenciaRecaudadora = Convert.ToInt32(lblIdgencia.Text)
            obeAgenciaRecaudadora.IdTipoAgenciaRecaudadora = ddlTipoAgencia.SelectedValue
            obeAgenciaRecaudadora.NombreComercial = txtNombreComercial.Text
            obeAgenciaRecaudadora.Ruc = txtRuc.Text
            obeAgenciaRecaudadora.Contacto = txtContacto.Text
            obeAgenciaRecaudadora.Telefono = txtTelefono.Text
            obeAgenciaRecaudadora.Fax = txtFax.Text
            obeAgenciaRecaudadora.Direccion = txtDireccion.Text
            obeAgenciaRecaudadora.Email = txtEmail.Text
            obeAgenciaRecaudadora.IdEstado = ddlEstado.SelectedValue
            obeAgenciaRecaudadora.IdCiudad = ddlCiudad.SelectedValue
            obeAgenciaRecaudadora.IdUsuarioCreacion = UserInfo.IdUsuario
            obeAgenciaRecaudadora.IdUsuarioActualizacion = UserInfo.IdUsuario
            Return obeAgenciaRecaudadora
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Function

    'FILTRAR DEPARTAMENTO POR PAIS
    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        CargarCombosUbigeo("Departamento")
        CargarCombosUbigeo("Ciudad")
        ScriptManager.SetFocus(ddlPais)
        '
    End Sub

    'FILTRAR CIUDADES POR DEPARTAMENTO
    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        CargarCombosUbigeo("Ciudad")
        ScriptManager.SetFocus(ddlDepartamento)
        '
    End Sub

    'CARGAMOS LOSC COMBOS DE UBIGEO
    Private Sub CargarCombosUbigeo(ByVal opcionCombo As String)
        '
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        '
        Select Case opcionCombo

            Case "Pais"
                ddlPais.DataTextField = "Descripcion" : ddlPais.DataValueField = "IdPais" : ddlPais.DataSource = objCUbigeo.ConsultarPais() : ddlPais.DataBind() : ddlPais.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlPais.SelectedIndex = 0
                '
            Case "Departamento"
                ddlDepartamento.DataTextField = "Descripcion" : ddlDepartamento.DataValueField = "IdDepartamento"
                If ddlPais.SelectedIndex = 0 Then
                    ddlDepartamento.DataSource = Nothing : ddlDepartamento.DataBind() : ddlDepartamento.Items.Clear() : ddlDepartamento.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
                Else
                    ddlDepartamento.DataSource = objCUbigeo.ConsultarDepartamentoPorIdPais(ddlPais.SelectedValue) : ddlDepartamento.DataBind()
                    ddlDepartamento.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlDepartamento.SelectedIndex = 0
                End If
            Case "Ciudad"
                ddlCiudad.DataTextField = "Descripcion" : ddlCiudad.DataValueField = "IdCiudad"
                If ddlDepartamento.SelectedIndex = 0 Then
                    ddlCiudad.DataSource = Nothing : ddlCiudad.DataBind() : ddlCiudad.Items.Clear() : ddlCiudad.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
                Else
                    ddlCiudad.DataSource = objCUbigeo.ConsultarCiudadPorIdDepartamento(ddlDepartamento.SelectedValue) : ddlCiudad.DataBind()
                    ddlCiudad.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlCiudad.SelectedIndex = 0
                End If

        End Select
        '
    End Sub


    'CARGAMOS LOSC COMBOS DE TIPO DE AGENCIA
    Private Sub CargarComboTipoAgencia()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlTipoAgencia.DataTextField = "Descripcion" : ddlTipoAgencia.DataValueField = "Id" : ddlTipoAgencia.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("TAGE") : ddlTipoAgencia.DataBind() : ddlTipoAgencia.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
        '
    End Sub

    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarComboEstado()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESMA") : ddlEstado.DataBind()
        '
    End Sub

    'LIMIAR FORM
    Private Sub LimpiarForm()
        '
        txtNombreComercial.Text = ""
        txtContacto.Text = ""
        txtTelefono.Text = ""
        txtFax.Text = ""
        txtDireccion.Text = ""
        txtEmail.Text = ""
        '
    End Sub

    'CANCELAR OPERACION
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("~/ADM/COAgRe.aspx")
        '
    End Sub

    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)

        Title = "PagoEfectivo - " & lblTitle.Text
    End Sub


    Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
        MyBase.OnInitComplete(e)
        'UCAudit1.InitializeAudit(lblShowAudit)
    End Sub
    
End Class
