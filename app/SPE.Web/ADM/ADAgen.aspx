<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ADM/ADAgen.aspx.vb" Inherits="ADM_ADAgen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar / Actualizar Agente Recaudador"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="UpdatePanelDatos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:Panel ID="pnlPage" runat="server">
                    <asp:Panel ID="pnlDatosUsuario" runat="server" CssClass="inner-col-right">
                        <h4>
                            1. Datos b&aacute;sicos del usuario</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> E-mail: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Ingrese el E-mail"
                                    ValidationGroup="IsNullOrEmpty" ControlToValidate="txtEmail" ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Formato incorrecto de E-mail"
                                    ValidationGroup="IsNullOrEmpty" ControlToValidate="txtEmail" ToolTip="Formato incorrecto de E-mail"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">**</asp:RegularExpressionValidator>
                                <asp:Label ID="lblMensajeEmail" runat="server" Visible="False" ForeColor="Red" Style="width: auto;"></asp:Label>
                            </li>
                        </ul>
                        <div id="fisMensajeContrasena" runat="server" visible="false">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Contrase�a: </li>
                                <li class="t2">
                                    <asp:Label Style="text-align: left" ID="lblDescContrasena" runat="server" Text="**********"
                                        CssClass="normal"></asp:Label>
                                    <asp:CheckBox ID="chbGenerarContrasena" runat="server" Text="Volver a generar" Visible="False">
                                    </asp:CheckBox>
                                </li>
                            </ul>
                        </div>
                        <div id="fisContrasenaAnterior" runat="server" visible="false">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Contrase�a anterior: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtContrasenaAnterior" runat="server" CssClass="normal" MaxLength="100"
                                        TextMode="Password">visible / invisible</asp:TextBox><asp:RequiredFieldValidator
                                            ID="rfvContrasenaAnt" runat="server" ErrorMessage="Ingrese la contrase�a anterior"
                                            ValidationGroup="IsNullOrEmpty" ControlToValidate="txtContrasenaAnterior">*</asp:RequiredFieldValidator>
                                </li>
                            </ul>
                        </div>
                        <div id="fisContrasena" runat="server" visible="false">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Contrase�a: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtContrasena" runat="server" CssClass="normal" MaxLength="100"
                                        TextMode="Password">Autogenerado</asp:TextBox><asp:RequiredFieldValidator ID="rfvContrase�a"
                                            runat="server" ErrorMessage="Ingrese la contrase�a" ValidationGroup="IsNullOrEmpty"
                                            ControlToValidate="txtContrasena">*</asp:RequiredFieldValidator>
                                </li>
                            </ul>
                        </div>
                        <div id="fisRepitaContrasena" runat="server" visible="false">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Repita Contrase�a: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtRepitaContrasena" runat="server" CssClass="normal" MaxLength="100"
                                        TextMode="Password">Autogenerado</asp:TextBox><asp:RequiredFieldValidator ID="rfvRepitaContrasena"
                                            runat="server" ErrorMessage="Repita la contrase�a" ValidationGroup="IsNullOrEmpty"
                                            ControlToValidate="txtRepitaContrasena">*</asp:RequiredFieldValidator>
                                </li>
                                <li class="t1"><span class="color">>></span> Alias: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtAlias" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Ingrese su alias"
                                        ControlToValidate="txtAlias">*</asp:RequiredFieldValidator>
                                </li>
                            </ul>
                        </div>
                    </asp:Panel>
                    <div style="clear: both">
                    </div>
                    <asp:Panel ID="pnlInformacionRegistro" CssClass="inner-col-right" runat="server">
                        <h4>
                            2. Informaci&oacute;n general</h4>
                        <div id="fisIdAgente" runat="server">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Identificador de Agente: </li>
                                <li class="t2">
                                    <asp:Label Style="text-align: left" ID="lblIdAgente" runat="server" Width="232px"
                                        CssClass="normal">0</asp:Label>
                                </li>
                            </ul>
                        </div>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Agencia Recaudadora: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtDescAgenciaRecaudadora" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                                <asp:ImageButton ID="ibtnBuscarAgencia" runat="server" ToolTip="Buscar Agencia" CausesValidation="false"
                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'>
                                </asp:ImageButton>
                                <asp:RequiredFieldValidator ID="rfvAgencia" runat="server" ErrorMessage="Buscar una Agencia Recaudadora"
                                    ValidationGroup="IsNullOrEmpty" ControlToValidate="txtDescAgenciaRecaudadora"
                                    ToolTip="Dato necesario">**</asp:RequiredFieldValidator>
                                <asp:Label ID="lblIdAgenciaRecaudadora" runat="server" Visible="False" CssClass="Etiqueta">0</asp:Label>
                            </li>
                            <li class="t1"><span class="color">>></span> Tipo de Agente: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlTipoAgente" runat="server" CssClass="neu">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvTipoAgente" runat="server" ErrorMessage="Seleccione un Tipo Agente"
                                    ValidationGroup="IsNullOrEmpty" ControlToValidate="ddlTipoAgente">*</asp:RequiredFieldValidator>
                            </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                        <spe:ucUsuarioDatosComun ID="ucUsuarioDatosComun" runat="server"></spe:ucUsuarioDatosComun>
                        <div id="fisEstado" runat="server">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Estado: </li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                                    </asp:DropDownList>
                                </li>
                            </ul>
                        </div>
                        <div style="clear: both">
                        </div>
                        <asp:ValidationSummary ID="vsmError" runat="server" ValidationGroup="IsNullOrEmpty"
                            Style="padding-left: 200px"></asp:ValidationSummary>
                        <cc1:ModalPopupExtender ID="mppAgencia" runat="server" BackgroundCssClass="modalBackground"
                            CancelControlID="imgbtnRegresarHidden" X="300" Y="200" PopupControlID="pnlPopupAgencias"
                            TargetControlID="ibtnBuscarAgencia">
                        </cc1:ModalPopupExtender>
                        <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                            Style="display: none;" CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                        <ul class="datos_cip2">
                            <li class="complet">
                                <asp:Label ID="lblTransaccion" runat="server" Visible="False" Width="650px" CssClass="MensajeTransaccion"
                                    Style="padding-bottom: 5px"></asp:Label>
                                <asp:Button ID="btnRegistrar" CssClass="input_azul3" runat="server" Text="Registrar"
                                    Visible="False" OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty">
                                </asp:Button>
                                <asp:Button ID="btnExportar" CssClass="input_azul3" runat="server" Text="Exportar"
                                    CausesValidation="False" Visible="False"></asp:Button>
                                <asp:Button ID="btnActualizar" CssClass="input_azul3" runat="server" Text="Actualizar"
                                    Visible="False" OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty">
                                </asp:Button>
                                <asp:Button ID="btnCancelar" CssClass="input_azul4" runat="server" Text="Cancelar"
                                    OnClientClick="return CancelMe();"></asp:Button>
                            </li>
                        </ul>
                        <asp:HiddenField ID="hdnIdCliente" runat="server" />
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnRegistrar"></asp:PostBackTrigger>
                <asp:PostBackTrigger ControlID="btnActualizar"></asp:PostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlPopupAgencias" runat="server" Style="display: none; background: white;
            max-height: 400px; min-height: 0px" CssClass="conten_pasos3">
            <div style="float: right">
                <asp:ImageButton ID="imgbtnRegresar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                    CausesValidation="false" runat="server"></asp:ImageButton>
            </div>
            <asp:UpdatePanel ID="upnlListAgencias" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>
                        B&uacute;squeda de Agencias
                    </h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Nombre Comercial: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtBusAgencia" runat="server" CssClass="Texto" Width="253px"></asp:TextBox>
                            <asp:ImageButton ID="ibtnListarAgencia" runat="server" CausesValidation="false" ToolTip="Buscar"
                                ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'>
                            </asp:ImageButton>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <div class="result" style="margin-top: 10px; overflow: auto; height: 300px">
                        <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" Width="580px" BackColor="White"
                            AllowPaging="True" OnRowCommand="gvResultado_RowCommand" CellPadding="3" BorderWidth="1px"
                            BorderStyle="None" BorderColor="#CCCCCC" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="IdAgenciaRecaudadora"></asp:BoundField>
                                <asp:BoundField DataField="NombreComercial" HeaderText="Nombre Comercial" HtmlEncode="False">
                                </asp:BoundField>
                                <asp:BoundField DataField="Contacto" HeaderText="Contacto"></asp:BoundField>
                                <asp:BoundField DataField="Telefono" HeaderText="TelB&eacute;squedafono" Visible="False">
                                </asp:BoundField>
                                <asp:BoundField DataField="Email" HeaderText="E-mail"></asp:BoundField>
                                <asp:TemplateField HeaderText="Seleccionar" HeaderStyle-Width="30">
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                            ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/chk_emp.png" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                         <%--       <asp:CommandField SelectImageUrl="~/Images_Buton/chk_emp.png" SelectText="Seleccionar"
                                    ShowSelectButton="True" HeaderStyle-Width="30" ButtonType="Image"></asp:CommandField>--%>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron Registros
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
</asp:Content>
