Imports SPE.Entidades
Imports System.IO
Partial Class ADM_ADAgen
    Inherits _3Dev.FW.Web.PageBase

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '

            Try
                OcultarDDL(True)
                '
                Page.SetFocus(txtEmail)
                CargarCombos()
                '
                If Page.PreviousPage Is Nothing OrElse Context.Items.Item("IdAgente") Is Nothing Then 'CONSULTAR AGENTE
                    CargarProcesoRegistro()
                    ucUsuarioDatosComun.InicializarVista()
                Else
                    CargarProcesoActualizar(Convert.ToInt32(Context.Items.Item("IdAgente")))
                End If
                '
            Catch ex As Exception
                '
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
                '
            End Try
            
        End If
        '
    End Sub

    'PROCESO DE REGISTRO
    Private Sub CargarProcesoRegistro()
        '
        Title = "PagoEfectivo - Registrar Agente"
        lblProceso.Text = "Registrar Agente"
        txtDescAgenciaRecaudadora.Text = ""
        fisMensajeContrasena.Visible = False
        fisEstado.Visible = False
        fisIdAgente.Visible = False
        btnRegistrar.Visible = True
        '
        'CargarCombosUbigeo("Pais")
        'CargarCombosUbigeo("Departamento")
        'CargarCombosUbigeo("Ciudad")
        '
    End Sub

    'PROCESO DE ACTUALIZACION
    Private Sub CargarProcesoActualizar(ByVal IdAgente As Integer)
        '
        Title = "PagoEfectivo - Actualizar Agente"
        lblProceso.Text = "Actualizar Agente"
        lblDescContrasena.Text = "**********"
        fisEstado.Visible = True
        fisIdAgente.Visible = False
        btnCancelar.Visible = True
        btnActualizar.Visible = True
        '
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgente As New BEAgente
        obeAgente = objCAdministrarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(1, IdAgente)

        txtEmail.ReadOnly = True
        txtEmail.CssClass = "normal"
        txtEmail.Text = obeAgente.Email
        '
        lblIdAgente.Text = IdAgente.ToString()
        txtDescAgenciaRecaudadora.Text = obeAgente.DescAgenciaRecaudadora
        lblIdAgenciaRecaudadora.Text = obeAgente.IdAgenciaRecaudadora
        ddlTipoAgente.SelectedValue = obeAgente.IdTipoAgente
        'txtNombres.Text = obeAgente.Nombres
        'txtApellidos.Text = obeAgente.Apellidos
        'ddlTipoDoc.SelectedValue = obeAgente.IdTipoDocumento
        'txtNumeroDoc.Text = obeAgente.NumeroDocumento
        'txtTelefono.Text = obeAgente.Telefono
        'txtDireccion.Text = obeAgente.Direccion
        'ddlEstado.SelectedValue = obeAgente.IdEstado
        InicializarddlEstado(obeAgente.IdEstado.ToString())
        'If obeAgente.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo Then
        '    ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
        'Else
        '    ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Inactivo
        'End If
        'CargarCombosUbigeo("Pais")
        'ddlPais.SelectedValue = obeAgente.IdPais
        'CargarCombosUbigeo("Departamento")
        'ddlDepartamento.SelectedValue = obeAgente.IdDepartamento
        'CargarCombosUbigeo("Ciudad")
        'ddlCiudad.SelectedValue = obeAgente.IdCiudad
        ucUsuarioDatosComun.RefrescarVista(obeAgente)

    End Sub

    'REGISTRAR AGENTE
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        '
        Try
            '
            'PROCESO DE REGISTRO
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgente As BEAgente = CargarDatosBEAgente()
            obeAgente.IdUsuarioCreacion = UserInfo.IdUsuario
            obeAgente.CodigoRegistro = Guid.NewGuid.ToString
            obeAgente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
            If objCAdministrarAgenciaRecaudadora.RegistrarAgente(obeAgente) = _
                SPE.EmsambladoComun.ParametrosSistema.ExisteEmail.Existe Then
                lblMensajeEmail.Visible = True
                lblMensajeEmail.Text = "E-mail ya registrado."
            Else
                lblMensajeEmail.Visible = False
                lblTransaccion.Visible = True
                btnRegistrar.Enabled = False
                pnlPage.Enabled = False
                btnCancelar.Text = "Regresar"
                btnCancelar.OnClientClick = ""
                lblTransaccion.Text = "Se registr� satisfactoriamente, el(la) agente: ''" & ucUsuarioDatosComun.Nombres.Text & " " & ucUsuarioDatosComun.Apellidos.Text & "''."
            End If
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        
    End Sub

    'ACTUALIZAR AGENTE
    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        '
        Try

            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgente As BEAgente = CargarDatosBEAgente()
            obeAgente.IdUsuarioCreacion = UserInfo.IdUsuario
            obeAgente.IdUsuarioActualizacion = UserInfo.IdUsuario
            obeAgente.CodigoRegistro = Guid.NewGuid.ToString
            objCAdministrarAgenciaRecaudadora.ActualizarAgente(obeAgente)
            '
            lblTransaccion.Visible = True
            btnActualizar.Enabled = False
            btnCancelar.Text = "Regresar"
            btnCancelar.OnClientClick = ""
            pnlPage.Enabled = False
            lblTransaccion.Text = "Se actualiz� satisfactoriamente el(la) agente: ''" & ucUsuarioDatosComun.Nombres.Text & " " & ucUsuarioDatosComun.Apellidos.Text & "''."
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Sub
    Private Sub InicializarddlEstado(ByVal idEstado As String)
        If idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Or idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            ddlEstado.Enabled = False
            ddlEstado.SelectedValue = idEstado
        Else
            ddlEstado.Enabled = True
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente)))
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado)))
            ddlEstado.SelectedValue = idEstado
        End If
    End Sub
    'CARGAMOS LOS DATOS DE LA AGENCIA RECAUDADORA
    Private Function CargarDatosBEAgente() As BEAgente
        '
        Try
            '
            Dim obeAgente As New BEAgente
            obeAgente.Email = txtEmail.Text
            obeAgente.IdAgente = lblIdAgente.Text
            obeAgente.IdAgenciaRecaudadora = Convert.ToInt32(lblIdAgenciaRecaudadora.Text)
            obeAgente.IdTipoAgente = ddlTipoAgente.SelectedValue
            ucUsuarioDatosComun.CargarBEUsuario(obeAgente)
            'obeAgente.Nombres = txtNombres.Text
            'obeAgente.Apellidos = txtApellidos.Text
            'obeAgente.IdTipoDocumento = ddlTipoDoc.SelectedValue
            'obeAgente.NumeroDocumento = txtNumeroDoc.Text
            'obeAgente.Telefono = txtTelefono.Text
            'obeAgente.Direccion = txtDireccion.Text
            'obeAgente.IdCiudad = ddlCiudad.SelectedValue

            If lblIdAgente.Text = "0" Then 'CUANDO SE REGISTRA
                obeAgente.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente
            Else 'CUANDO SE ACTUALIZA
                obeAgente.IdEstado = Convert.ToInt32(ddlEstado.SelectedValue)

            End If
            Return obeAgente
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Function

    ''FILTRAR DEPARTAMENTO POR PAIS
    'Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPais.SelectedIndexChanged
    '    '
    '    CargarCombosUbigeo("Departamento")
    '    CargarCombosUbigeo("Ciudad")
    '    ScriptManager.SetFocus(ddlPais)
    '    OcultarDDL(True)
    '    '
    'End Sub

    ''FILTRAR CIUDADES POR DEPARTAMENTO
    'Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartamento.SelectedIndexChanged
    '    '
    '    CargarCombosUbigeo("Ciudad")
    '    ScriptManager.SetFocus(ddlDepartamento)
    '    '
    'End Sub

    ''CARGAMOS LOSC COMBOS DE UBIGEO
    'Private Sub CargarCombosUbigeo(ByVal opcionCombo As String)
    '    '
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    '
    '    Select Case opcionCombo

    '        Case "Pais"
    '            ddlPais.DataTextField = "Descripcion" : ddlPais.DataValueField = "IdPais" : ddlPais.DataSource = objCUbigeo.ConsultarPais() : ddlPais.DataBind() : ddlPais.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlPais.SelectedIndex = 0
    '            '
    '        Case "Departamento"
    '            ddlDepartamento.DataTextField = "Descripcion" : ddlDepartamento.DataValueField = "IdDepartamento"
    '            If ddlPais.SelectedIndex = 0 Then
    '                ddlDepartamento.DataSource = Nothing : ddlDepartamento.DataBind() : ddlDepartamento.Items.Clear() : ddlDepartamento.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
    '            Else
    '                ddlDepartamento.DataSource = objCUbigeo.ConsultarDepartamentoPorIdPais(ddlPais.SelectedValue) : ddlDepartamento.DataBind()
    '                ddlDepartamento.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlDepartamento.SelectedIndex = 0
    '            End If
    '        Case "Ciudad"
    '            ddlCiudad.DataTextField = "Descripcion" : ddlCiudad.DataValueField = "IdCiudad"
    '            If ddlDepartamento.SelectedIndex = 0 Then
    '                ddlCiudad.DataSource = Nothing : ddlCiudad.DataBind() : ddlCiudad.Items.Clear() : ddlCiudad.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
    '            Else
    '                ddlCiudad.DataSource = objCUbigeo.ConsultarCiudadPorIdDepartamento(ddlDepartamento.SelectedValue) : ddlCiudad.DataBind()
    '                ddlCiudad.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlCiudad.SelectedIndex = 0
    '            End If
    '    End Select
    '    '
    'End Sub

    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESUS") : ddlEstado.DataBind()
        '
        'ddlTipoDoc.DataTextField = "Descripcion" : ddlTipoDoc.DataValueField = "Id" : ddlTipoDoc.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("TDOC") : ddlTipoDoc.DataBind() : ddlTipoDoc.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall, ""))
        '
        ddlTipoAgente.DataTextField = "Descripcion" : ddlTipoAgente.DataValueField = "Id" : ddlTipoAgente.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("TAGT") : ddlTipoAgente.DataBind() : ddlTipoAgente.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
        '
    End Sub

    'LIMIAR FORM
    Protected Sub LimpiarForm()
        '
        txtDescAgenciaRecaudadora.Text = ""
        lblIdAgenciaRecaudadora.Text = "0"
        txtEmail.Text = ""
        txtContrasenaAnterior.Text = ""
        txtContrasena.Text = ""
        txtRepitaContrasena.Text = ""
        'txtNombres.Text = ""
        'txtApellidos.Text = ""
        'txtNumeroDoc.Text = ""
        'txtTelefono.Text = ""
        'txtDireccion.Text = ""
        '
    End Sub

    'Lista de Agencias Recaudadoras
    Protected Sub ibtnListarAgencia_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnListarAgencia.Click
        '
        ListarAgenciaRecaudadora()
        OcultarDDL(False)
        '
    End Sub

    'Listar Agencia Recaudadora
    Private Sub ListarAgenciaRecaudadora()
        '
        Try
            '
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
            '
            obeAgenciaRecaudadora.NombreComercial = txtBusAgencia.Text
            obeAgenciaRecaudadora.Contacto = ""
            obeAgenciaRecaudadora.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
            '
            gvResultado.DataSource = objCAdministrarAgenciaRecaudadora.ConsultarAgenciaRecaudadora(obeAgenciaRecaudadora)
            gvResultado.DataBind()
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        '
    End Sub

    'SELECCIONAMOS LA AGENCIA
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        '
        If Not e Is Nothing Then
            lblIdAgenciaRecaudadora.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument.ToString())).Cells(0).Text
            txtDescAgenciaRecaudadora.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument.ToString())).Cells(1).Text
            OcultarDDL(True)
        End If
        '
    End Sub

    'CANCELAR
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("~/ADM/COAgen.aspx")
        '
    End Sub

    'PAGINACION DE GRILLA
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarAgenciaRecaudadora()
        mppAgencia.Show()
        '
    End Sub

    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        OcultarDDL(True)
        mppAgencia.Hide()
    End Sub

    Private Sub OcultarDDL(ByVal flag As Boolean)
        '    ddlCiudad.Visible = flag
        '    ddlDepartamento.Visible = flag
        '    ddlPais.Visible = flag

    End Sub

    Protected Sub btnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        'generarClaves("SPE")
        'generarClaves("NEO")
    End Sub
    Public Sub generarClaves(ByVal codigoEntidad As String)
        'Dim objCSeguridad As New CSeguridad
        'Dim successfull As Boolean = objCSeguridad.GenerarClavePublicaPrivada(codigoEntidad)

        'Dim keyPublic As Byte() = objCSeguridad.ObtenerClavePublica(codigoEntidad)
        'Dim numBytesToRead As Integer = keyPublic.Length

        'Using fsNew As FileStream = New FileStream("C:\\keyPublic" + codigoEntidad + ".p1z", _
        '        FileMode.Create, FileAccess.Write)
        '    fsNew.Write(keyPublic, 0, numBytesToRead)
        'End Using

        'Dim keyPrivate As Byte() = objCSeguridad.ObtenerClavePrivada(codigoEntidad)
        'numBytesToRead = keyPrivate.Length

        'Using fsNew As FileStream = New FileStream("C:\\keyPrivate" + codigoEntidad + ".p1z", _
        '        FileMode.Create, FileAccess.Write)
        '    fsNew.Write(keyPrivate, 0, numBytesToRead)
        'End Using
    End Sub
End Class
