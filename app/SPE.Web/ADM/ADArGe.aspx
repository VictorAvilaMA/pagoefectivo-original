<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADArGe.aspx.vb" Inherits="ADM_ADArGe" Title="PagoEfectivo - Actualizar Archivos Generados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Actualizar Archivos Generados
    </h2>
    <div class="conten_pasos3">
        <asp:Panel ID="PnlEmpresa" runat="server">
            <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo">
                <h4>
                    1. Informaci&oacute;n general
                </h4>
            </div>
            <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Servicio: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtServicio" runat="server" CssClass="normal" MaxLength="100" ReadOnly="true">
                            </asp:TextBox>
                            <asp:Label ID="lblIdFtpArchivo" runat="server" Text="" Visible="false"></asp:Label>
                        </li>
                        <li class="t1"><span class="color">>></span> Nombre del Archivo: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtNombreArchivo" runat="server" CssClass="normal" MaxLength="100"
                                ReadOnly="true"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">>></span> Fecha Generaci&oacute;n delArchivo:
                        </li>
                        <li class="t2">
                            <asp:TextBox ID="txtFechaGeneracion" runat="server" CssClass="normal" MaxLength="100"
                                ReadOnly="true"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">>></span> Tipo de Archivo: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtTipoArchivo" runat="server" CssClass="normal" MaxLength="15"
                                ReadOnly="true"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">>></span><asp:Label runat="server" ID="lblEstado">Estado:</asp:Label></li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="normal">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Seleccione un estado" Display="Dynamic" ControlToValidate="ddlEstado">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <div style="clear: both;">
        </div>
        <div id="div3" class="result">
            <asp:UpdatePanel ID="upnlArchivos" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div7" class="divContenedorTitulo">
                        <asp:Label ID="lblMsgNroRegistros" runat="server"><h5>Resultados:</h5></asp:Label>
                    </div>
                    <div id="divOrdenesPago" class="divSubContenedorGrilla">
                        <asp:GridView ID="gvDetalleArchivos" runat="server" CssClass="grilla" OnPageIndexChanging="gvDetalleArchivos_PageIndexChanging"
                            AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnSorting="gvDetalleArchivos_Sorting">
                            <Columns>
                                <asp:BoundField DataField="IdUsuario" SortExpression="IdUsuario" HeaderText="IdUsuario">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="OrdenIdComercio" SortExpression="OrdenIdComercio" HeaderText="OrdenIdComercio">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoPago" SortExpression="TipoPago" HeaderText="Tipo de Pago">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaPago" SortExpression="FechaPago" HeaderText="Fecha de Pago">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Moneda" SortExpression="Moneda" HeaderText="Moneda">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Monto" SortExpression="Monto" HeaderText="Monto">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            &nbsp;
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Navy"></asp:Label>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <ul id="fsBotonera" class="datos_cip2">
            <li id="Li1" class="complet">
                <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                    Text="Actualizar" ValidationGroup="ValidaCampos" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                    Text="Cancelar" />
            </li>
        </ul>
        <%--<fieldset id="fsBotonera" class="ContenedorBotonera">
            <asp:Button ID="btnActualizar" runat="server" CssClass="btnActualizar" OnClientClick="return ConfirmMe();"
                Text="Actualizar" ValidationGroup="ValidaCampos" />
            <asp:Button ID="btnCancelar" runat="server" CssClass="btnCancelar" OnClientClick="return CancelMe();"
                Text="Cancelar" />
        </fieldset>--%>
    </div>
</asp:Content>
