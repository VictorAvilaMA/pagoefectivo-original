Imports SPE.Entidades
Imports System.Data
Imports _3Dev.FW.Web.Security
Partial Class ADM_ADArGe
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CFTPArchivo)


    Private Property SortDir() As SortDirection

        Get
            If ViewState("SortDir") Is Nothing Then ViewState("SortDir") = SortDirection.Ascending
            Return CType(ViewState("SortDir"), SortDirection)
        End Get

        Set(ByVal value As SortDirection)
            ViewState("SortDir") = value
        End Set
    End Property

    Private Property SortExpression() As String
        Get
            If ViewState("SortExpression") Is Nothing Then ViewState("SortExpression") = ""
            Return ViewState("SortExpression").ToString()
        End Get

        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Function CreateBEFTPArchivo(ByVal obeFTPArchivo As SPE.Entidades.BEFTPArchivo) As SPE.Entidades.BEFTPArchivo

        obeFTPArchivo = New SPE.Entidades.BEFTPArchivo

        obeFTPArchivo.IdFtpArchivo = lblIdFtpArchivo.Text
        obeFTPArchivo.IdEstado = ddlEstado.SelectedValue

        Return obeFTPArchivo

    End Function

    Public Overrides Sub OnMainLoadInformation()
        Try
            businessEntityObject = ObtenerArchivoFTP(Nothing, Nothing)
            AssignBusinessEntityValuesInLoadingInfo(businessEntityObject)
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            ThrowErrorMessage(DefaultErrorMessage)
        End Try

    End Sub

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim beFTPArchivo As BEFTPArchivo = CreateBEFTPArchivo(be)

        beFTPArchivo.IdFtpArchivo = CInt(lblIdFtpArchivo.Text)
        beFTPArchivo.IdEstado = CInt(ddlEstado.SelectedValue)

        Return beFTPArchivo

    End Function

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase

    '    Return New SPE.Web.CFTPArchivo

    'End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)

        Dim obeFTPArchivo As SPE.Entidades.BEFTPArchivo = CType(be, SPE.Entidades.BEFTPArchivo)

        txtServicio.Text = obeFTPArchivo.Servicio
        txtNombreArchivo.Text = obeFTPArchivo.NombreArchivo
        txtFechaGeneracion.Text = obeFTPArchivo.FechaGenerado
        txtTipoArchivo.Text = obeFTPArchivo.TipoArchivo
        ddlEstado.SelectedValue = obeFTPArchivo.IdEstado

        gvDetalleArchivos.DataSource = obeFTPArchivo.DetalleArchivoFTP
        gvDetalleArchivos.DataBind()

        lblEstado.Visible = True
        ddlEstado.Visible = True

    End Sub

    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADArGe"
        e.URLPageCancelForEdit = "COArGe.aspx"
        e.URLPageCancelForInsert = "COArGe.aspx"

    End Sub

    Private Sub CargarComboEstado()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESAG"), _
        "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Page.SetFocus(ddlEstado)
            CargarComboEstado()
            lblIdFtpArchivo.Text = MaintenanceKeyValue
        End If

        'Page.Title = "PagoEfectivo - " + lblProceso.Text
    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Sub OnAfterInsert()

    End Sub

    Public Overrides Sub OnAfterUpdate()
        btnActualizar.Enabled = False
        Me.ddlEstado.Enabled = False
        btnCancelar.Text = "Regresar"
        btnCancelar.OnClientClick = ""
    End Sub

    Private Sub ControlesMantenimiento(ByVal Habilitar As Boolean)
        OcultarControles(Habilitar)
        btnActualizar.Enabled = Habilitar

    End Sub

    Private Sub OcultarControles(ByVal flag As Boolean)

        ddlEstado.Enabled = flag

    End Sub

    Protected Sub gvDetalleArchivos_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvDetalleArchivos.PageIndex = e.NewPageIndex
        businessEntityObject = ObtenerArchivoFTP(SortExpression, SortDir)
        AssignBusinessEntityValuesInLoadingInfo(businessEntityObject)
    End Sub

    Protected Sub gvDetalleArchivos_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        '
        SortExpression = e.SortExpression
        If (SortDir.Equals(SortDirection.Ascending)) Then
            SortDir = SortDirection.Descending
        Else
            SortDir = SortDirection.Ascending
        End If

        businessEntityObject = ObtenerArchivoFTP(SortExpression, SortDir)
        AssignBusinessEntityValuesInLoadingInfo(businessEntityObject)

        '
    End Sub

    Private Function ObtenerArchivoFTP(ByVal OrderBy As String, ByVal IsAsccending As Boolean) As BEFTPArchivo

        Return CType(ControllerObject, SPE.Web.CFTPArchivo).ObtenerFTPArchivoPorId(lblIdFtpArchivo.Text, OrderBy, IsAsccending)
    End Function

End Class
