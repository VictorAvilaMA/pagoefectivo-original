﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="ADClavSeg.aspx.vb" Inherits="ADM_ADClavSeg" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h1>
        Configuraci&oacute;n de Claves</h1>
    <asp:UpdatePanel ID="upConfiguracionClaves" runat="server" class="dlgrid">
        <contenttemplate>
            <asp:Label ID="lblMsjTransaccion" runat="server" CssClass="MensajeTransaccion"></asp:Label>
            <div class="clear">
            </div>
        </contenttemplate>
    </asp:UpdatePanel>
    <fieldset>
        <div class="inner-col-right">
            <h4>
                Generar claves p&uacute;blica y privada</h4>
            <div id="fsBotonera" runat="server" class="clearfix dlinline btns3">
                <asp:Button ID="btnGenerarClaves" runat="server" CssClass="btnRegenerar" />
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="sep_a14">
        </div>
        <div class="clear">
        </div>
        <div class="inner-col-right">
            <h4>
                Exportar clave P&uacute;blica</h4>
            <div id="Div1" runat="server" class="clearfix dlinline btns3">
                <asp:Button ID="btnExportarClavePublica" runat="server" CssClass="btnExpClave" Style="margin-left: 10px;" />
            </div>
        </div>
    </fieldset>
</asp:Content>
