﻿
Partial Class ADM_ADClavSeg
    Inherits System.Web.UI.Page

    Protected Sub btnGenerarClaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerarClaves.Click
        Dim objCSeguridad As New CSeguridad
        Dim successfull As Boolean = objCSeguridad.GenerateKeyPrivatePublicSPE()
        If successfull Then
            lblMsjTransaccion.Text = "Se generaron las claves públicas correctamente"
        Else
            lblMsjTransaccion.Text = "No se pudo generar la clave pública"
        End If
    End Sub

    Protected Sub btnExportarClavePublica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarClavePublica.Click
        Dim objCSeguridad As New CSeguridad
        Dim Pathkey As String = objCSeguridad.ObtenerClavePublicaSPE()
        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = "text/plain"
        Response.AddHeader("content-disposition", "attachment; filename=" & "ClavePublicaSPE_" & DateTime.Now.ToString("yyyyMMdd") & "_" & DateTime.Now.Hour.ToString() & DateTime.Now.Minute.ToString() & ".1pz")
        Response.WriteFile(Pathkey)
        'Response.BinaryWrite(key)
        Response.Flush()
        Response.[End]()
    End Sub
End Class
