<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ADM/ADClint.aspx.vb" Inherits="ADM_ADClint"
    Title="PagoEfectivo - Cliente"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function AjaxBegin(sender, args) {
//            $(".divLoadingMensaje").show('fast');
//            $('.divLoading').show('blind');
        }
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server">Registrar Cliente</asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pnlDatosUsuario" runat="server">
            <h4>
                Datos de Usuario</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> E-mail: </li>
                <li class="t2">
                    <asp:TextBox ID="txtEmailPrincipal" runat="server" MaxLength="100" CssClass="normal"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailPrincipal"
                        ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailPrincipal"
                        ErrorMessage="Debe ingresar su email">*</asp:RequiredFieldValidator>
                    <asp:Label ID="lblValidaEmail" ForeColor="Red" runat="server" CssClass="MensajeValidacion"
                        Style="width: auto;"></asp:Label>
                </li>
            </ul>
        </asp:Panel>
        <div style="clear: both">
        </div>
        <div id="fisEstado" runat="server">
            <h4>
                Informaci&oacute;n general
            </h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Alias: </li>
                <li class="t2">
                    <asp:TextBox ID="txtAlias" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAlias" runat="server" ErrorMessage="Ingrese su alias"
                        ControlToValidate="txtAlias">*</asp:RequiredFieldValidator>
                </li>
            </ul>
            <asp:UpdatePanel ID="updScrollPosition" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" TargetControlID="txtApellidos"
                        ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' ">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombres" runat="server" TargetControlID="txtNombres"
                        ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' ">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="FTBE_TxtTelefono" runat="server" TargetControlID="txtTelefono"
                        ValidChars="0123456789- ">
                    </cc1:FilteredTextBoxExtender>
                    <ul class="datos_cip2 h0">
                        <li class="t1"><span class="color">>></span> Nombres: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtNombres" runat="server" CssClass="normal" MaxLength="1000"></asp:TextBox>
                            <strong id="strgNombres" runat="server" style="color: darkgray">*</strong>
                            <asp:RequiredFieldValidator ID="rfvNombres" runat="server" ControlToValidate="txtNombres"
                                ErrorMessage="Ingrese su nombre">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                    <ul class="datos_cip2 h0">
                        <li class="t1"><span class="color">>></span> Apellidos: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtApellidos" runat="server" CssClass="normal" ReadOnly="false"
                                MaxLength="100"></asp:TextBox>
                            <strong id="strgApellidos" runat="server" style="color: darkgray">*</strong>
                            <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ControlToValidate="txtApellidos"
                                ErrorMessage="Ingrese su apellido" Style="display: none" EnableTheming="True">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Documento: </li>
                        <li class="t2">
                            <p class="cal">
                                <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="corta2" AutoPostBack="true"
                                    Style="width: 133px">
                                </asp:DropDownList>
                                <asp:Label ID="labelNumero" runat="server" Text="N�:" CssClass="entre"></asp:Label>
                                <asp:TextBox ID="txtNumeroDocumento" runat="server" CssClass="corta" MaxLength="11"></asp:TextBox>
                                <strong id="strgTipoDocumento" runat="server" style="color: darkgray">*</strong>
                                <asp:RequiredFieldValidator ID="rfvTipoDocumento" runat="server" ControlToValidate="ddlTipoDocumento"
                                    ErrorMessage="Seleccione un tipo de documento.">*</asp:RequiredFieldValidator>
                                <strong id="strgNroDocumento" runat="server" style="color: darkgray">*</strong>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNumeroDocumento"
                                    ErrorMessage="Ingrese el n�mero de documento.">*</asp:RequiredFieldValidator>
                            </p>
                        </li>
                    </ul>
                    <div style="clear:both"></div>
                    <ul class="datos_cip2 h0">
                        <li class="t1"><span class="color">>></span>
                            <asp:Label ID="lblGenero" runat="server" Text="G�nero:"></asp:Label>
                        </li>
                        <li class="t2">
                            <p class="cal">
                                <asp:DropDownList ID="ddlGenero" runat="server" CssClass="corta">
                                </asp:DropDownList>
                                <strong id="strgGenero" runat="server" style="color: darkgray">*</strong>
                                <asp:RequiredFieldValidator ID="rfvGenero" runat="server" ControlToValidate="ddlGenero"
                                    ErrorMessage="Seleccione su g�nero.">*</asp:RequiredFieldValidator>
                            </p>
                        </li>
                    </ul>
                    <ul class="datos_cip2" style="width: 700px; clear: both">
                        <li class="t1"><span class="color">>></span> Fecha Nacimiento: </li>
                        <li class="t2" style="width: 500px;">
                            <p class="cal">
                                <asp:DropDownList ID="ddlFechaMes" runat="server" AutoPostBack="true" CssClass="corta2"
                                    OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlFechaDia" runat="server" CssClass="corta2">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlFechaAnio" runat="server" AutoPostBack="true" CssClass="corta2"
                                    OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                                </asp:DropDownList>
                                <strong id="strgFechaNacimiento" runat="server" style="color: darkgray">*</strong>
                                <asp:RequiredFieldValidator ID="rfvFechaDia" runat="server" ControlToValidate="ddlFechaDia"
                                    ErrorMessage="Seleccione el d�a.">*</asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvFechaMes" runat="server" ControlToValidate="ddlFechaMes"
                                    ErrorMessage="Seleccione el mes">*</asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvFechaAnio" runat="server" ControlToValidate="ddlFechaAnio"
                                    ErrorMessage="Seleccione el a�o">*</asp:RequiredFieldValidator>
                            </p>
                        </li>
                    </ul>
                    <ul class="datos_cip2 h0">
                        <li class="t1"><span class="color">>></span> Tel�fono: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                        </li>
                    </ul>
                    <ul class="datos_cip2 f0" style="clear: both">
                        <li class="t1 f0"><span class="color">>></span> Pa�s: </li>
                        <li class="t2 f0">
                            <p class="cal">
                                <asp:DropDownList ID="ddlPais" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                                <strong id="strgPais" runat="server" style="color: darkgray">*</strong>
                            </p>
                        </li>
                    </ul>
                    <ul class="datos_cip2 f0" style="clear: both">
                        <li class="t1 f0"><span class="color">>></span> Departamento: </li>
                        <li class="t2 f0">
                            <p class="cal">
                                <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                                <strong id="strgDepartamento" runat="server" style="color: darkgray">*</strong>
                            </p>
                        </li>
                    </ul>
                    <ul class="datos_cip2 f0" style="clear: both">
                        <li class="t1 f0"><span class="color">>></span> Ciudad: </li>
                        <li class="t2 f0">
                            <p class="cal">
                                <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="corta">
                                </asp:DropDownList>
                                <strong id="strgCiudad" runat="server" style="color: darkgray">*</strong>
                                <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="Seleccione una Ciudad"
                                    ControlToValidate="ddlCiudad">*</asp:RequiredFieldValidator>
                            </p>
                        </li>
                    </ul>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Direcci�n: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        </li>
                    </ul>
                    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> E-mail Alternativo: </li>
                <li class="t2">
                    <asp:TextBox ID="txtEmailAlternativo" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">>></span> Estado: </li>
                <li class="t2">
                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="corta">
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
        <div style="clear: both">
        </div>
        <asp:Panel ID="pnlInformacionRegistro" runat="server">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                Style="padding-left: 200px" />
            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeValidacion" Style="padding-left: 200"></asp:Label>
            <asp:Label ID="lblEstado" runat="server" CssClass="Etiqueta"></asp:Label>
            <asp:HiddenField ID="hdnIdCliente" runat="server" />
        </asp:Panel>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                    Text="Registrar" />
                <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                    Text="Actualizar" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                    Text="Cancelar" />
            </li>
        </ul>
    </div>
</asp:Content>
