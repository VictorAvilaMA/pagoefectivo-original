Imports SPE.Entidades
Imports System.Windows.Forms
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports System.Collections.Generic
Imports _3Dev.FW.Excepciones
Imports System.Globalization

Partial Class ADM_ADClint
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CCliente)

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Function CreateBECliente(ByVal obeCliente As SPE.Entidades.BECliente) As SPE.Entidades.BECliente
        obeCliente = New SPE.Entidades.BECliente
        obeCliente.Email = txtEmailPrincipal.Text
        obeCliente.AliasCliente = txtAlias.Text
        obeCliente.EmailAlternativo = Me.txtEmailAlternativo.Text
        obeCliente.IdUsuarioCreacion = UserInfo.IdUsuario
        obeCliente = CargarBEUsuario(obeCliente)
        Return obeCliente
    End Function
    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeCliente As SPE.Entidades.BECliente = CreateBECliente(be)
        obeCliente.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente
        obeCliente.IdUsuarioCreacion = UserInfo.IdUsuario
        obeCliente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
        Return obeCliente
    End Function
    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeCliente As BECliente = CreateBECliente(be)
        obeCliente.IdEstado = _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue)
        obeCliente.IdUsuarioActualizacion = UserInfo.IdUsuario
        obeCliente.IdCliente = ObjectToInt32(hdnIdCliente.Value)
        Return obeCliente
    End Function
    Private Sub CargarComboEstados()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESUS"), _
        "Descripcion", "Id")
    End Sub
    Private Sub InicializarddlEstado(idEstado As String)
        If idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Or idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            'ddlEstado.Enabled = False
            ddlEstado.Enabled = True
            ddlEstado.SelectedValue = idEstado
        Else
            ddlEstado.Enabled = True
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente)))
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado)))
            ddlEstado.SelectedValue = idEstado
        End If
    End Sub
    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCliente As SPE.Entidades.BECliente = CType(be, SPE.Entidades.BECliente)
        Me.txtEmailPrincipal.Text = obeCliente.Email
        Me.txtAlias.Text = obeCliente.AliasCliente
        Me.txtEmailAlternativo.Text = obeCliente.EmailAlternativo
        Me.lblEstado.Text = obeCliente.IdEstado
        RefrescarVista(obeCliente)
        MyBase.AssignBusinessEntityValuesInLoadingInfo(be)
        Me.txtEmailPrincipal.ReadOnly = True
        hdnIdCliente.Value = obeCliente.IdCliente
        fisEstado.Visible = True
        CargarComboEstados()
        InicializarddlEstado(Me.lblEstado.Text)
        lblProceso.Text = "Actualizar Cliente"
    End Sub
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADClint"
        e.URLPageCancelForEdit = "COClint.aspx"
        e.URLPageCancelForInsert = "COClint.aspx"
    End Sub
    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
        txtEmailPrincipal.CssClass = "normal SoloLectura"
    End Sub
    Public Overrides Sub OnAfterInsert()
        If (valueRegister = -100) Then
            lblValidaEmail.Text = "Este correo est� siendo usado por otro usuario."
            lblMensaje.Text = String.Empty
        Else
            lblMensaje.Text = "Se registr� satisfactoriamente el cliente: ''" + txtNombres.Text + " " + txtApellidos.Text + "''."
            pnlDatosUsuario.Enabled = False
            pnlInformacionRegistro.Enabled = False
            BtnCancel.Text = "Regresar"
            BtnCancel.OnClientClick = ""
            btnRegistrar.Enabled = False
        End If
    End Sub
    Public Overrides Sub OnAfterUpdate()
        HabilitarDiv(False)
        Me.btnActualizar.Enabled = False
        BtnCancel.Text = "Regresar"
        BtnCancel.OnClientClick = ""
        lblMensaje.Text = "Se actualiz� satisfactoriamente el cliente: ''" + txtNombres.Text + " " + txtApellidos.Text + "''."
    End Sub
    Sub HabilitarDiv(ByVal valor As Boolean)
        pnlDatosUsuario.Enabled = valor
        pnlInformacionRegistro.Enabled = valor
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Me.Title = "PagoEfectivo - " + lblProceso.Text
    End Sub
#Region "UCDatosComun"
#Region "Propiedades"
    Protected _flagPreMostrarCamposMandatorios As Boolean
    Public Property FlagPreMostrarCamposMandatorios() As Boolean
        Get
            Return _flagPreMostrarCamposMandatorios
        End Get
        Set(ByVal value As Boolean)
            _flagPreMostrarCamposMandatorios = value
        End Set
    End Property
    Public ReadOnly Property TipoDocumento() As DropDownList
        Get
            Return ddlTipoDocumento
        End Get
    End Property
    Public ReadOnly Property Genero() As DropDownList
        Get
            Return ddlGenero
        End Get
    End Property
    Public ReadOnly Property FechaNacimiento() As UCDropDateBase
        Get
            Return Me.FindControl("ucFechaNacimiento")
        End Get
    End Property
    Public ReadOnly Property Pais() As DropDownList
        Get
            Return ddlPais
        End Get
    End Property
    Public ReadOnly Property Departamento() As DropDownList
        Get
            Return ddlDepartamento
        End Get
    End Property
    Public ReadOnly Property Ciudad() As DropDownList
        Get
            Return ddlCiudad
        End Get
    End Property
#End Region

#Region "Metodos"
    Private _cntrlParametro As SPE.Web.CAdministrarParametro = Nothing
    Private ReadOnly Property CntrlParametro() As SPE.Web.CAdministrarParametro
        Get
            If (_cntrlParametro Is Nothing) Then
                _cntrlParametro = New SPE.Web.CAdministrarParametro()
            End If
            Return _cntrlParametro
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            PreMostrarCamposMandatorios()
            txtNumeroDocumento.MaxLength = 11
            pnlDatosUsuario.Enabled = True
            pnlInformacionRegistro.Enabled = True
            lblEstado.Visible = False

            If (PageManager.IsInserting) Then
                InicializarVista()
                CargarComboEstados()
            End If
        End If
    End Sub
    Private Sub PreMostrarCamposMandatorios()
        strgNombres.Visible = _flagPreMostrarCamposMandatorios
        strgApellidos.Visible = _flagPreMostrarCamposMandatorios
        strgGenero.Visible = _flagPreMostrarCamposMandatorios
        strgNroDocumento.Visible = _flagPreMostrarCamposMandatorios
        strgTipoDocumento.Visible = _flagPreMostrarCamposMandatorios
        strgCiudad.Visible = _flagPreMostrarCamposMandatorios

        strgPais.Visible = _flagPreMostrarCamposMandatorios
        strgDepartamento.Visible = _flagPreMostrarCamposMandatorios
        strgCiudad.Visible = _flagPreMostrarCamposMandatorios
        FlagPreMostrarCamposMandatorios = _flagPreMostrarCamposMandatorios
        'PreMostrarCamposMandatorios()
    End Sub
    Public Sub InicializarVista()
        CargarComboTipoDocumento()
        CargarComboGenero()
        CargarCombosFecha()
        SeleccionarPais(0)
    End Sub
    Private Sub CargarComboTipoDocumento()
        CargarComboTipoDocumento(False)
    End Sub
    Private Sub CargarComboTipoDocumento(ByVal recargar As Boolean)
        If (recargar) Then
            Me.ddlGenero.Items.Clear()
        End If
        If ((Me.ddlGenero.Items.Count = 0)) Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlTipoDocumento, CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoTipoDocumento), "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        End If
    End Sub
    Private Sub CargarComboGenero()
        CargarComboGenero(False)
    End Sub
    Private Sub CargarComboGenero(ByVal recargar As Boolean)
        If (recargar) Then
            Me.ddlGenero.Items.Clear()
        End If
        If ((Me.ddlGenero.Items.Count = 0)) Then
            'lis(CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero))
            Dim lgeneros As List(Of SPE.Entidades.BEParametro) = CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero)
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlGenero, lgeneros, "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        End If
    End Sub
    Private Sub CargarCombosFecha()
        RefrescarVista()
    End Sub
    Public Function CargarBEUsuario(ByVal obeUsuario As SPE.Entidades.BEUsuarioBase) As SPE.Entidades.BEUsuarioBase
        obeUsuario.Nombres = Me.txtNombres.Text
        obeUsuario.Apellidos = Me.txtApellidos.Text
        obeUsuario.IdTipoDocumento = ObjectToInt(Me.ddlTipoDocumento.SelectedValue)
        obeUsuario.NumeroDocumento = Me.txtNumeroDocumento.Text
        obeUsuario.IdCiudad = ObjectToInt(Me.ddlCiudad.SelectedValue)
        obeUsuario.Direccion = Me.txtDireccion.Text
        obeUsuario.Telefono = Me.txtTelefono.Text
        obeUsuario.IdGenero = ObjectToInt(ddlGenero.SelectedValue)
        obeUsuario.FechaNacimiento = DameFechaSeleccionada()
        Return obeUsuario
    End Function
    Public Sub RefrescarVista(ByVal obeUsuario As SPE.Entidades.BEUsuarioBase)
        RefrescarVista(obeUsuario.Nombres, obeUsuario.Apellidos, obeUsuario.IdTipoDocumento, obeUsuario.NumeroDocumento, _
        obeUsuario.IdGenero, obeUsuario.FechaNacimiento, obeUsuario.Telefono, obeUsuario.IdPais, obeUsuario.IdDepartamento, obeUsuario.IdCiudad, _
        obeUsuario.Direccion)
    End Sub
    Public Sub RefrescarVista(ByVal nombre As String, ByVal apellidos As String, ByVal idTipoDocumento As Integer, _
    ByVal nroDocumento As String, ByVal Idgenero As Integer, ByVal fechaNacimiento As DateTime, _
    ByVal telefono As String, ByVal idPais As Integer, ByVal idDepartamento As Integer, _
    ByVal idCiudad As Integer, ByVal direccion As String)
        Me.txtNombres.Text = nombre
        Me.txtApellidos.Text = apellidos
        CargarComboTipoDocumento()
        Me.ddlTipoDocumento.SelectedValue = idTipoDocumento
        Me.txtNumeroDocumento.Text = nroDocumento
        CargarComboGenero()
        Me.ddlGenero.SelectedValue = Idgenero
        Me.SeleccionarPais(idPais)
        Me.SeleccionarDepartamento(idDepartamento)
        Me.SeleccionarCiudad(idCiudad)
        Me.ddlCiudad.SelectedValue = idCiudad
        Me.txtDireccion.Text = direccion
        Me.txtTelefono.Text = telefono
        RefrescarVista(fechaNacimiento)
    End Sub
    Protected Sub ddlTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoDocumento.SelectedIndexChanged
        If ddlTipoDocumento.SelectedValue = "11" Then
            txtNumeroDocumento.MaxLength = 8
            If txtNumeroDocumento.Text.Length > 8 Then
                txtNumeroDocumento.Text = txtNumeroDocumento.Text.Substring(0, 8)
            End If
        Else
            txtNumeroDocumento.MaxLength = 11
        End If
    End Sub
#End Region

#Region "Ubicacion"
    Public Sub SeleccionarPais(ByVal IdPais As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        If IdPais > 0 Then
            Me.ddlPais.SelectedValue = IdPais
        End If
    End Sub
    Public Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlPais.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlPais.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        If IdDepartamento > 0 Then
            Me.ddlDepartamento.SelectedValue = IdDepartamento
        Else
            Me.ddlDepartamento.SelectedIndex = 0
        End If
    End Sub
    Public Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlDepartamento.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlDepartamento.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        If IdCiudad > 0 Then
            Me.ddlCiudad.SelectedValue = IdCiudad
        Else
            Me.ddlCiudad.SelectedIndex = 0
        End If
    End Sub
    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarDepartamento(0)
        SeleccionarCiudad(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlPais)
    End Sub

    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarCiudad(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlDepartamento)
    End Sub
#End Region
#End Region
#Region "UCFecha"

    Protected Sub Initialize(ByVal ddlFechaDias As DropDownList, ByVal ddlFechaMes As DropDownList, ByVal ddlFechaAnio As DropDownList)
        _ddlFechaAnio = ddlFechaAnio
        _ddlFechaMes = ddlFechaMes
        ddlFechaDia = ddlFechaDias
    End Sub

    Public Function DameFechaSeleccionada() As Date
        Return New Date(_ddlFechaAnio.SelectedValue, _ddlFechaMes.SelectedValue, ddlFechaDia.SelectedValue)
    End Function

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
    End Sub

    Private Sub GenerarAnio()

        Dim anio As Integer = Now.Year
        _ddlFechaAnio.Items.Clear()
        _ddlFechaAnio.Items.Add(New ListItem("", ""))

        For i As Integer = 0 To 100
            _ddlFechaAnio.Items.Add(New ListItem(anio - 1, anio - 1))
            anio = anio - 1
        Next

    End Sub

    Private Sub GenerarMeses()
        _ddlFechaMes.Items.Clear()
        _ddlFechaMes.Items.Add(New ListItem("", ""))

        Dim cultureInfo As CultureInfo = cultureInfo.CreateSpecificCulture("es-PE")
        Dim meses As String() = cultureInfo.DateTimeFormat.MonthNames()

        For i As Integer = 0 To 11
            _ddlFechaMes.Items.Add(New ListItem(meses(i), i + 1))
        Next

    End Sub

    Private Sub GenerarDias()
        Dim diatemp As String = ddlFechaDia.SelectedValue
        'Date.
        ddlFechaDia.Items.Clear()
        ddlFechaDia.Items.Add(New ListItem("", ""))
        Dim anio As String = _ddlFechaAnio.SelectedValue
        Dim mes As String = _ddlFechaMes.SelectedValue
        Dim dias As Integer = 31
        If Not ((anio = "") Or (mes = "")) Then
            dias = Date.DaysInMonth(anio, mes)
        End If

        For i As Integer = 1 To dias
            ddlFechaDia.Items.Add(New ListItem(i, i))
        Next

        If Not ddlFechaDia.Items.FindByValue(diatemp) Is Nothing Then
            ddlFechaDia.SelectedValue = diatemp
        End If
    End Sub

    Public Function FechaEsValida() As Boolean
        Dim strfecha As Date
        Try
            strfecha = DameFechaSeleccionada()
        Catch ex As Exception
            Throw New FWBusinessException("El formato de la fecha no es valida", Nothing)
        End Try
        Return True
    End Function
    Public Sub RefrescarVista()
        GenerarAnio()
        GenerarMeses()
        GenerarDias()
    End Sub
    Public Sub RefrescarVista(ByVal fecha As Date)
        Try
            GenerarAnio()
            GenerarMeses()
            _ddlFechaAnio.SelectedValue = fecha.Year
            _ddlFechaMes.SelectedValue = fecha.Month
            GenerarDias()
            ddlFechaDia.SelectedValue = fecha.Day
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub ddlFecha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerarDias()
        Dim drop As DropDownList = CType(sender, DropDownList)
    End Sub
#End Region
End Class
