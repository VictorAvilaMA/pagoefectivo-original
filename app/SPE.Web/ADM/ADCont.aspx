﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" 
    CodeFile="ADCont.aspx.vb" Inherits="ADM_ADCont" Async="true"  ValidateRequest="false"
    Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
     
    <h2>
        Actualizar Contrato XML</h2>
    <div class="conten_pasos3">
        <h4>
            Datos del Servicio</h4>
        <asp:UpdatePanel ID="upFiltros" runat="server">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Empresa: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvEmpresa" runat="server" ValidationGroup="ValidaCampos"
                            ErrorMessage="Debe seleccionar la Empresa." ControlToValidate="ddlEmpresa">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="clsnormal" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="ValidaCampos"
                            ErrorMessage="Debe seleccionar el servicio." ControlToValidate="ddlEmpresa">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul3" Text="Limpiar" />
                        <asp:Label ID="lblMsjEstructuraXML" runat="server" Style="color: Blue; width: 100%"></asp:Label>
                    </li>
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <asp:UpdatePanel ID="upEsctructuraXML" runat="server">
            <ContentTemplate>
                <div id="divEstructuraXML" runat="server" class="datos_cip2" visible="false">
                    <h4>
                        Estructura XML</h4>
                    <div style="clear: both">
                    </div>
                    <asp:TextBox ID="txtEstructuraXML" runat="server" TextMode="MultiLine" Style="width: 95%;
                        height: 500px; margin-left: 15px" Enabled="false"> 
                    </asp:TextBox>
                    <div style="clear: both">
                    </div>
                    <ul class="datos_cip2">
                        <li class="complet">
                            <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" ValidationGroup="ValidaCampos" OnClientClick="return ConfirmMe();"
                                Visible="false" Text="Atualizar" />
                        </li>
                    </ul>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 200px" />

                    <asp:HiddenField ID="hdfIdServicio" runat="server" Value=""></asp:HiddenField>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlServicio" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
