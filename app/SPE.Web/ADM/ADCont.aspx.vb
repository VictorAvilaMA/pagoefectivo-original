﻿Imports SPE.Entidades
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Web
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Web.Security

Partial Class ADM_ADCont
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            InicializarControles()
            If Session("BEServicio") IsNot Nothing Then
                Dim oBEServicio As BEServicio = CType(Session("BEServicio"), BEServicio)
                ddlEmpresa.SelectedValue = oBEServicio.IdEmpresaContratante
                ddlEmpresa_SelectedIndexChanged(Nothing, Nothing)
                ddlServicio.SelectedValue = oBEServicio.IdServicio
                ddlServicio_SelectedIndexChanged(Nothing, Nothing)
                Session.Remove("BEServicio")
                ddlEmpresa.Enabled = False
                ddlServicio.Enabled = False
                btnLimpiar.Visible = False
            End If
        End If
    End Sub

    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        habilitarRegistro(False)
        If ddlEmpresa.SelectedIndex = 0 Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", ":: Seleccione ::")
            Return
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            objbeservicio.NombreEmpresaContrante = ""
            objbeservicio.IdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
            objbeservicio.Nombre = ""
            objbeservicio.IdEstado = 0
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, ocntrlServicio.SearchByParameters(objbeservicio), "Nombre", "IdServicio", ":: Seleccione ::")
        End Using
    End Sub

    Protected Sub ddlServicio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlServicio.SelectedIndexChanged
        lblMsjEstructuraXML.Text = ""
        If ddlServicio.SelectedIndex = 0 Then
            habilitarRegistro(False)
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            If Not ddlServicio.SelectedIndex = 0 Then
                Dim obeServicio As BEServicio = ocntrlServicio.GetRecordByID(ddlServicio.SelectedValue)
                If obeServicio IsNot Nothing Then
                    habilitarRegistro(True)
                    txtEstructuraXML.Text = obeServicio.ContratoXML
                End If
            End If

        End Using
    End Sub



    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Using ocntrlServicio As New CServicio()
            Dim obeServicio As SPE.Entidades.BEServicio = New BEServicio
            obeServicio.IdServicio = ddlServicio.SelectedValue
            obeServicio.ContratoXML = txtEstructuraXML.Text.Trim()
            obeServicio.IdUsuarioActualizacion = UserInfo.IdUsuario
            obeServicio.UsuarioActualizacionNombre = UserInfo.NombreCompleto
            ocntrlServicio.UpdateServicioContratoXML(obeServicio)
        End Using
        divEstructuraXML.Visible = False
        InicializarControles()
        lblMsjEstructuraXML.Text = "Se Actualizo el Contrato XML Satisfactoriamente"

    End Sub
    Protected Sub InicializarControles()
        txtEstructuraXML.Text = ""
        lblMsjEstructuraXML.Text = ""
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", ":: Seleccione")
        Using ocntrlEmpresaContrante As New CEmpresaContratante()
            Dim oBEEmpresaContratante As New BEEmpresaContratante()
            oBEEmpresaContratante.Codigo = ""
            oBEEmpresaContratante.RazonSocial = ""
            oBEEmpresaContratante.RUC = ""
            oBEEmpresaContratante.IdEstado = 41
            oBEEmpresaContratante.Email = ""
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Seleccione::")
            txtEstructuraXML.Enabled = False
        End Using
    End Sub

    Protected Sub habilitarRegistro(ByVal habilitar As Boolean)
        divEstructuraXML.Visible = habilitar
        txtEstructuraXML.Text = If(habilitar, txtEstructuraXML.Text, "")
        txtEstructuraXML.Enabled = habilitar
        btnActualizar.Visible = habilitar
        lblMsjEstructuraXML.Text = String.Empty
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        ddlEmpresa.SelectedIndex = 0
        ddlEmpresa_SelectedIndexChanged(Nothing, Nothing)
        habilitarRegistro(False)
    End Sub
End Class
