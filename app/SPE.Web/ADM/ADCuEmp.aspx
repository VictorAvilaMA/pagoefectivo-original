﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="ADCuEmp.aspx.vb" Inherits="ADM_ADCuEmp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #Error div ul li
        {
            color: Red !important;
        }
        ul.datos_cip2 li select, input.neu
        {
            float: left !important;
        }
        li span[style*="hidden"]
        {
            width: auto !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Cuentas de Deposito a Empresas"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPage" runat="server">
                    <h4>
                        1. Información general</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Empresa Contratante: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEmpresaContratante" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlEmpresaContratante" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar la empresa contratante." ControlToValidate="ddlEmpresaContratante">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Moneda: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar la moneda." ControlToValidate="ddlMoneda">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Banco </li>
                        <li class="t2">
                            <asp:TextBox ID="txtBanco" runat="server" CssClass="neu" MaxLength="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe llenar el banco a Depositar." ControlToValidate="txtBanco">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Tipo de Cuenta: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlTipoCuenta" runat="server" CssClass="neu">
                                <asp:ListItem Value="1">Cuenta Interbancaria</asp:ListItem>
                                <asp:ListItem Value="2">Cuenta Corriente</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar el Tipo de Cuenta." ControlToValidate="ddlTipoCuenta">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Nro de Cuenta: </li>
                        <li id="liComisionFija" runat="server" class="t2">
                            <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="neu" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe llenar la cuenta a Depositar." ControlToValidate="txtNroCuenta">*
                            </asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="ftbNroCuenta" runat="server" TargetControlID="txtNroCuenta"
                                ValidChars="0123456789- ">
                            </cc1:FilteredTextBoxExtender>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvEstado" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar el Estado." ControlToValidate="ddlEstado">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <asp:HiddenField ID="hdfMoneda" runat="server" Value=""></asp:HiddenField>
                        <asp:HiddenField ID="hdfEmpresaContratante" runat="server" Value=""></asp:HiddenField>
                        <asp:HiddenField ID="hdfCuentaEmpresa" runat="server" Value=""></asp:HiddenField>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <div id="Error" class="validateSummary">
                        <asp:ValidationSummary ID="vsmMessage" runat="server" ValidationGroup="IsNullOrEmpty">
                        </asp:ValidationSummary>
                        <ul id="ulMensaje" runat="server" visible="false">
                            <li>
                                <asp:Literal ID="ltlMensaje" runat="server" Text="asd"></asp:Literal></li>
                        </ul>
                        <div style="color: navy; font-weight: bold; padding-bottom: 15px;">
                            <asp:Label ID="lblTransaccion" runat="server" CssClass="MensajeTransaccion" Width="650px">
                            </asp:Label>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <ul id="fsBotonera" runat="server" class="datos_cip2 h0">
                        <li id="botoneraA" runat="server" class="complet">
                            <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Registrar"
                                OnClientClick="return ConfirmMe();" Visible="true"></asp:Button>
                            <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" Text="Actualizar"
                                OnClientClick="return ConfirmMe();" Visible="false"></asp:Button>
                            <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Cancelar"
                                OnClientClick="return confirm('¿Está Ud. seguro que desea cancelar la operación? ');">
                            </asp:Button>
                        </li>
                        <li id="botoneraB" runat="server" visible="false" class="complet">
                            <asp:Button ID="btnRegresar" runat="server" CssClass="input_azul3" Text="Regresar">
                            </asp:Button>
                        </li>
                    </ul>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
