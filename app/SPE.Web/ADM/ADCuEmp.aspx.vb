﻿Imports System.Data
Imports SPE.Entidades
Imports SPE.Web
Imports SPE.EmsambladoComun
Imports System.Collections.Generic

Partial Class ADM_ADCuEmp
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CEmpresaContratante)

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblTransaccion
        End Get
    End Property

    Protected Function loadEntityBECuentaEmpresa() As BECuentaEmpresa
        Dim oBECuentaEmpresa As New BECuentaEmpresa
        With oBECuentaEmpresa
            .IdEmpresaContratante = ddlEmpresaContratante.SelectedValue
            .IdEstado = ddlEstado.SelectedValue
            .IdMoneda = ddlMoneda.SelectedValue
            .Banco = txtBanco.Text
            .NroCuenta = txtNroCuenta.Text
            .IdUsuarioCreacion = UserInfo.IdUsuario
            .IdUsuarioActualizacion = UserInfo.IdUsuario
            .IdTipoCuenta = ddlTipoCuenta.SelectedValue
        End With
        Return oBECuentaEmpresa
    End Function
    Public Overrides Sub OnMainUpdate()
        Dim be As BECuentaEmpresa = loadEntityBECuentaEmpresa()
        With be
            .IdCuentaEmpresa = hdfCuentaEmpresa.Value
            .IdEmpresaContratante = hdfEmpresaContratante.Value
            .IdMoneda = hdfMoneda.Value
            .IdTipoCuenta = ddlTipoCuenta.SelectedValue
        End With
        valueUpdated = CType(ControllerObject, SPE.Web.CEmpresaContratante).ActualizarCuentaEmpresa(be)
        EnabledButtonControls()
    End Sub
    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCuentaEmpresa As SPE.Entidades.BECuentaEmpresa = CType(be, SPE.Entidades.BECuentaEmpresa)
        'txtCodigo.Text = obeServicio.Codigo
        hdfEmpresaContratante.Value = obeCuentaEmpresa.IdEmpresaContratante
        hdfMoneda.Value = obeCuentaEmpresa.IdMoneda
        hdfCuentaEmpresa.Value = obeCuentaEmpresa.IdCuentaEmpresa
        ddlEmpresaContratante.SelectedValue = obeCuentaEmpresa.IdEmpresaContratante
        ddlMoneda.SelectedValue = obeCuentaEmpresa.IdMoneda
        ddlEstado.SelectedValue = obeCuentaEmpresa.IdEstado
        txtBanco.Text = obeCuentaEmpresa.Banco
        ddlTipoCuenta.SelectedValue = obeCuentaEmpresa.IdTipoCuenta
        txtNroCuenta.Text = obeCuentaEmpresa.NroCuenta
        'Activar controles
        ddlEmpresaContratante.Enabled = False
        ddlMoneda.Enabled = False
        ddlEstado.Enabled = True
        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        lblProceso.Text = "Registrar Cuentas de Deposito a Empresas"
    End Sub
    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
        pnlPage.Enabled = True
    End Sub
    Public Overrides Sub OnAfterUpdate()
        'pnlPage.Enabled = False
        btnActualizar.Enabled = False
        BtnCancel.Enabled = False
        ddlEstado.Enabled = False
        txtBanco.Enabled = False
        txtNroCuenta.Enabled = False
        botoneraA.Visible = False
        botoneraB.Visible = True
        ddlTipoCuenta.Enabled = False
    End Sub
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADCuEmp"
        e.URLPageCancelForEdit = "COCuEmp.aspx"
        e.URLPageCancelForInsert = "COCuEmp.aspx"
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            CargarComboEstado()
        End If
    End Sub
    ''CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarComboEstado()
        
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - " + Me.lblProceso.Text
        btnRegresar.Enabled = True
    End Sub
    Public Overrides Function DoUpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Return CType(ControllerObject, SPE.Web.CEmpresaContratante).ActualizarCuentaEmpresa(be)
    End Function


    Public Overrides Sub OnLoadInformation()
        Dim oCEmpresa As New CEmpresaContratante
        Dim obeCuentaEMpresa As BECuentaEmpresa = oCEmpresa.ConsultarCuentaEmpresaPorId(MaintenanceKeyValue)
        AssignBusinessEntityValuesInLoadingInfo(obeCuentaEMpresa)
    End Sub

    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("~/ADM/COCuEmp.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
            Dim objCParametro As New CAdministrarParametro
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
            Using ocntrlEmpresaContrante As New CEmpresaContratante()
                Dim oBEEmpresaContratante As New BEEmpresaContratante()
                oBEEmpresaContratante.IdEstado = 41
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEmpresaContratante, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Seleccione::")
            End Using
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), "Descripcion", "Id")
            ddlEstado.Enabled = False
        End If
    End Sub
    Public Overrides Sub OnMainInsert()
        Dim businessEntityObject As New _3Dev.FW.Entidades.BusinessEntityBase
        Dim beCuentaEmpresa As BECuentaEmpresa = loadEntityBECuentaEmpresa()
        valueRegister = CType(ControllerObject, SPE.Web.CEmpresaContratante).InsertarCuentaEmpresa(beCuentaEmpresa)
        If valueRegister = -1 Then
            ThrowErrorMessage("Ya existe una cuenta asociada a la empresa con la moneda seleccionada")
        Else
            ThrowSuccessfullyMessage("Se registro correctamente la operacion con código: " & valueRegister)
            btnRegistrar.Enabled = False
            BtnCancel.Enabled = False
            ddlEmpresaContratante.Enabled = False
            ddlMoneda.Enabled = False
            txtBanco.Enabled = False
            txtNroCuenta.Enabled = False
            botoneraA.Visible = False
            botoneraB.Visible = True
            ddlTipoCuenta.Enabled = False
        End If
    End Sub
End Class
