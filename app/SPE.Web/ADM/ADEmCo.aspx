<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADEmCo.aspx.vb" Inherits="ADM_ADEmCo" Title="PagoEfectivo - Empresa Contratante" %>

<%@ Register Src="../UC/UCLocation.ascx" TagName="UCLocation" TagPrefix="uc2" %>
<%@ Register Src="../UC/UCAudit.ascx" TagName="UCAudit" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        li span[style*="hidden"]
        {
            width: auto !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblTitle" runat="server" Text="Registrar Empresa Contratante"></asp:Literal>
    </h2>
    <asp:LinkButton ID="lnkShowAudit" Style="float: right" CausesValidation="false" runat="server">[Mostar Auditor�a]</asp:LinkButton>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Panel ID="pnlPage" runat="server">
                    <asp:Panel ID="pnlDatosUsuario" runat="server" CssClass="inner-col-right">
                        <h4>
                            1. Informaci&oacute;n general</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> C&oacute;digo: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCodigo" ControlToValidate="txtCodigo" runat="server"
                                    Text="*" ValidationGroup="ValidaCampos" ErrorMessage="El c&oacute;digo es requerido."></asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">>></span> Raz&oacute;n Social: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRazonSocial"
                                    Display="Dynamic" ErrorMessage="Ingrese la Raz&oacute;n social." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                <asp:Label ID="lblIdEmpresaContrante" runat="server" Text="" Style="display: none"></asp:Label>
                            </li>
                            <li class="t1"><span class="color">>></span> R.U.C. : </li>
                            <li class="t2">
                                <asp:TextBox ID="txtRuc" runat="server" CssClass="normal" MaxLength="11"></asp:TextBox>&nbsp;
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRuc"
                                    Display="Dynamic" ErrorMessage="Ingrese el R.U.C." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rvaRuc" runat="server" ErrorMessage="Ingrese solo n�meros"
                                    MinimumValue="0" MaximumValue="100000000000" ControlToValidate="txtRuc" Type="Double"
                                    ValidationGroup="ValidaCampos">*</asp:RangeValidator>
                                <asp:CustomValidator ID="cvRUC" runat="server" ClientValidationFunction="ValidarRuc"
                                    ControlToValidate="txtRuc" Display="Dynamic" ErrorMessage="R.U.C. no v&aacute;lido"
                                    SetFocusOnError="True" ValidationGroup="ValidaCampos">*</asp:CustomValidator>
                            </li>
                            <li class="t1"><span class="color">>></span> Contacto: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtContacto" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Tel&eacute;fono: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Fax: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtFax" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Correo: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Escriba un email v�lido" ValidationGroup="ValidaCampos" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                            </li>
                            <li class="t1"><span class="color">>></span> Direcci&oacute;n: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Sitio Web: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtSitioWeb" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                            </li>
                        </ul>
                        <uc2:UCLocation ID="ucUbigeo" runat="server" />
                        </div>
                        <div id="divEstado" runat="server" visible="false">
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Estado: </li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlEstado" runat="server" Visible="True" CssClass="normal">
                                    </asp:DropDownList>
                                </li>
                            </ul>
                        </div>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Hora Corte: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtTime" runat="server" CssClass="normal" MaxLength="5">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvHoraCorte" ControlToValidate="txtTime" runat="server"
                                    Text="*" ValidationGroup="ValidaCampos" ErrorMessage="Ingrese Hora de Corte."></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revHoraCorte" runat="server" ErrorMessage="Ingrese Formato 24:00 hrs"
                                    ControlToValidate="txtTime" Text="*" ValidationExpression="(([01][0-9])|(2[0-3])):([0-5][0-9])$"
                                    ValidationGroup="ValidaCampos" Display="Static">
                                </asp:RegularExpressionValidator>
                            </li>
                            <li class="t1"><span class="color">>></span> Periodo de Liquidaci�n: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlPeriodo" runat="server" Visible="True" CssClass="normal">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvPL" runat="server" ErrorMessage="Seleccione un Periodo."
                                  Text="*" ValidationGroup="ValidaCampos" ControlToValidate="ddlPeriodo"></asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">>></span> Ocultar Empresa: </li>
                            <li class="t2">
                                <asp:CheckBox ID="chkOcultarEmpresa" runat="server" class="normal" />
                            </li>
                            <li class="t1"><span class="color">>></span> Adjuntar Imagen: </li>
                            <li class="t2" style="height: auto">
                                <asp:FileUpload ID="ImagenFile" runat="server"></asp:FileUpload>
                                <asp:Label ID="lblUploadMens" runat="server" Width="250px" Text="(Extensi�n v�lida: .gif, .jpg, .jpeg, .png)"
                                    ForeColor="red"></asp:Label>
                            </li>
                            <li class="t1" style="height: auto"></li>
                            <li class="t2" style="height: auto">
                                <img id="imgLogoImagen" alt="" src="" runat="server" class="normal" visible="false" />
                            </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidaCampos"
                            Style="padding-left: 200px" />
                        <asp:Label ID="lblMensaje" runat="server" Style="padding-left: 200px; float: left"></asp:Label>
                        <ul class="datos_cip2">
                            <li class="complet">
                                <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Registrar"
                                    ValidationGroup="ValidaCampos" />
                                <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                                    Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False" />
                                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                                    Text="Cancelar" />
                            </li>
                        </ul>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TxtTelefono" runat="server" FilterType="Custom, Numbers"
                            TargetControlID="txtTelefono" ValidChars="-">
                        </cc1:FilteredTextBoxExtender>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TxtFax" runat="server" FilterType="Custom, Numbers"
                            TargetControlID="txtFax" ValidChars="-">
                        </cc1:FilteredTextBoxExtender>
                        <fieldset id="fisUpload" class="ContenedorEtiquetaTexto" runat="server" visible="false">
                            <asp:Label ID="lblUploadCarga" runat="server" Text="" ForeColor="red"></asp:Label>
                        </fieldset>
                        <div id="fsBotonera" runat="server" class="clearfix dlinline btns3">
                        </div>
                        <asp:UpdatePanel ID="upAudit" runat="server">
                            <ContentTemplate>
                                <uc1:UCAudit ID="ucAudit" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
