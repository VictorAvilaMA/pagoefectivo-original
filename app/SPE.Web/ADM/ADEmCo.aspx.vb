Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Util.DataUtil


Imports SPE.Utilitario
Imports SPE.Entidades

Partial Class ADM_ADEmCo
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CEmpresaContratante)
    'nuevo 

#Region "propiedades"
    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar

        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
#End Region

#Region "metodos base"
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADEmCo"
        'e.URLPageCancelForEdit = "COEmCo.aspx"
        'e.URLPageCancelForInsert = "COEmCo.aspx"
        e.URLPageCancelForSave = "COEmCo.aspx"
        e.UseEntitiesMethod = True
        e.PageTitle = "PagoEfectivo - " + lblTitle.Text
    End Sub
    Protected Overrides Sub MaintenancePage_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.MaintenancePage_Load(sender, e)
        If Not IsPostBack Then
            If (_3Dev.FW.Web.PageManager.IsInserting) Then
                ucUbigeo.InitializeView()
                EnableTextBox(txtCodigo)
                lnkShowAudit.Visible = False
                ucAudit.Visible = False
            ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
                lnkShowAudit.Visible = True
                If (txtCodigo.Text.Trim() = "") Then
                    EnableTextBox(txtCodigo)
                Else
                    DisableTextBox(txtCodigo)
                End If
            End If
        End If
    End Sub

    ''' <summary>
    '''  Se encarga de asignar los valores de la entidad a los controles web luego de traer la informaci�n de la fuente de datos
    ''' </summary>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Overrides Sub AssignEntityMessageResponseValuesInLoadingInfo(ByVal response As _3Dev.FW.Entidades.BusinessMessageBase)
        Dim obeEmpresa As SPE.Entidades.BEEmpresaContratante = CType(response.Entity, SPE.Entidades.BEEmpresaContratante)

        txtCodigo.Text = obeEmpresa.Codigo
        txtRazonSocial.Text = obeEmpresa.RazonSocial
        txtRuc.Text = obeEmpresa.RUC
        txtContacto.Text = obeEmpresa.Contacto
        txtTelefono.Text = obeEmpresa.Telefono
        txtFax.Text = obeEmpresa.Fax
        txtDireccion.Text = obeEmpresa.Direccion
        txtEmail.Text = obeEmpresa.Email
        txtSitioWeb.Text = obeEmpresa.SitioWeb
        ucUbigeo.RefreshView(obeEmpresa)
        CargarComboEstado()
        CargarComboPeriodo()
        ddlEstado.SelectedValue = obeEmpresa.IdEstado
        lblIdEmpresaContrante.Text = obeEmpresa.IdEmpresaContratante
        chkOcultarEmpresa.Checked = obeEmpresa.OcultarEmpresa
        'OCULTAR CONTROLES
        divEstado.Visible = True
        'lblEstado.Visible = True
        ddlEstado.Visible = True
        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        lblTitle.Text = "Actualizar Empresa Contratante"
        imgLogoImagen.Visible = True
        ucAudit.RefreshView(obeEmpresa)

        txtTime.Text = obeEmpresa.HoraCorte
        ddlPeriodo.SelectedValue = obeEmpresa.IdPeriodoLiquidacion.ToString
    End Sub

    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
        imgLogoImagen.Src = "ADEmCoImg.aspx?empresaId=" + CType(loadEntityResponse.Entity, SPE.Entidades.BEEmpresaContratante).IdEmpresaContratante.ToString()
        OcultarImagen()
    End Sub

    Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
        MyBase.OnInitComplete(e)
        ucAudit.InitializeAudit(lnkShowAudit)
    End Sub

    Public Overrides Function AllowToSaveEntity() As Boolean
        Dim vruc As SPE.Utilitario.ValidaRuc = New SPE.Utilitario.ValidaRuc()
        lblMensaje.Text = ""
        lblUploadCarga.Text = ""
        If (vruc.ValidarRuc(txtRuc.Text)) Then
            Return ValidarImagen()
        Else
            ValidarImagen()
            ThrowErrorMessage("Ruc no valido")
            Exit Function
        End If
    End Function

    Public Overrides Function CreateMessageRequestForSaveEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase
        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            Dim obeEmpresa As BEEmpresaContratante = CreateBEEmpresaContratante()
            obeEmpresa.UsuarioCreacionNombre = UserInfo.NombreCompleto
            'agregado
            obeEmpresa.IdEmpresaContratante = StringToInt(If(lblIdEmpresaContrante.Text = "", "0", lblIdEmpresaContrante.Text))
            request.Entity = obeEmpresa
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            Dim obeEmpresa As BEEmpresaContratante = CreateBEEmpresaContratante()
            obeEmpresa.IdEmpresaContratante = StringToInt(lblIdEmpresaContrante.Text)
            obeEmpresa.UsuarioActualizacionNombre = UserInfo.NombreCompleto
            'obeEmpresa.FechaActualizacion = Date.Now
            request.Entity = obeEmpresa
        End If
        Return request
    End Function

    Public Overrides Sub OnAfterSaveEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase, ByVal response As _3Dev.FW.Entidades.BusinessMessageBase)
        MyBase.OnAfterSaveEntity(request, response)
        ucAudit.RefreshView(response.Entity)
        BtnCancel.CssClass = "input_azul3"
    End Sub

    Public Overrides Sub EnabledMaintenanceControls(ByVal enabled As Boolean)
        EnableTextBox(txtCodigo, enabled)
        EnableTextBox(txtRazonSocial, enabled)
        EnableTextBox(txtRuc, enabled)
        EnableTextBox(txtContacto, enabled)
        EnableTextBox(txtTelefono, enabled)
        EnableTextBox(txtFax, enabled)
        EnableTextBox(txtEmail, enabled)
        EnableTextBox(txtDireccion, enabled)
        EnableTextBox(txtSitioWeb, enabled)
        ucUbigeo.Country.Enabled = enabled
        ucUbigeo.Departament.Enabled = enabled
        ucUbigeo.City.Enabled = enabled
        ddlEstado.Enabled = enabled
        ImagenFile.Enabled = enabled
        imgLogoImagen.Disabled = Not enabled
        chkOcultarEmpresa.Enabled = enabled

        EnableTextBox(txtTime, enabled)
        ddlPeriodo.Enabled = enabled
    End Sub
#End Region

#Region "metodos"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Page.SetFocus(txtRazonSocial)
            If (_3Dev.FW.Web.PageManager.IsInserting) Then
                CargarComboEstado()
                CargarComboPeriodo()
                ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
            End If

        End If
    End Sub

    Protected Function CreateBEEmpresaContratante() As SPE.Entidades.BEEmpresaContratante
        Dim obeEmpresa As New SPE.Entidades.BEEmpresaContratante
        obeEmpresa.Codigo = txtCodigo.Text.Trim
        obeEmpresa.RazonSocial = txtRazonSocial.Text.Trim
        obeEmpresa.RUC = txtRuc.Text.Trim
        obeEmpresa.Contacto = txtContacto.Text.Trim
        obeEmpresa.Telefono = txtTelefono.Text
        obeEmpresa.Fax = txtFax.Text
        obeEmpresa.Direccion = txtDireccion.Text
        obeEmpresa.SitioWeb = txtSitioWeb.Text
        obeEmpresa.Email = txtEmail.Text
        obeEmpresa.IdEstado = CInt(ddlEstado.SelectedValue)
        obeEmpresa.IdUsuarioCreacion = UserInfo.IdUsuario
        obeEmpresa.IdUsuarioActualizacion = UserInfo.IdUsuario
        obeEmpresa.IdEstado = ddlEstado.SelectedValue
        obeEmpresa.ImagenLogo = ObtenerImagen()
        obeEmpresa.OcultarEmpresa = chkOcultarEmpresa.Checked
        ucUbigeo.LoadEntityLocation(obeEmpresa)
        obeEmpresa.HoraCorte = txtTime.Text
        obeEmpresa.IdPeriodoLiquidacion = CInt(ddlPeriodo.SelectedValue)
        Return obeEmpresa
    End Function

    Private Sub CargarComboEstado()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
    End Sub

    Private Sub CargarComboPeriodo()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPeriodo, objCParametro.ConsultarParametroPorCodigoGrupo("TPER"), _
        "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)


    End Sub

    Public Function ObtenerImagen() As Byte()
        Return _3Dev.FW.Web.WebUtil.GetImage(ImagenFile)
    End Function

    Private Function ValidarImagen() As Boolean
        Dim result As _3Dev.FW.Web.WebUtil.ValidateImageResult = _3Dev.FW.Web.WebUtil.ValidateImage(ImagenFile)
        If (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedOk) Then
            fisUpload.Visible = False
            Return True
        ElseIf (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedErrorHasNoFile) Then
            fisUpload.Visible = False
            Return True
        ElseIf (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedErrorExtensionInvalid) Then
            lblUploadCarga.Text = "  ** Nota: La imagen no fue cargada porque no tiene una extensi�n v�lida."
            fisUpload.Visible = True
            Return False

        ElseIf (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedErrorWeightLimitExceeded) Then
            lblUploadCarga.Text = "  ** Nota: La imagen no fue cargada porque excede el tama�o limite de 2MB."
            fisUpload.Visible = True
            Return False
        End If
    End Function

    Private Sub OcultarImagen()
        Dim beEmpContratante As BEEmpresaContratante = CType(loadEntityResponse.Entity, SPE.Entidades.BEEmpresaContratante)
        If ((beEmpContratante.ImagenLogo Is Nothing) Or (beEmpContratante.ImagenLogo.Length = 0)) Then
            imgLogoImagen.Visible = False
        Else
            imgLogoImagen.Visible = True
        End If
    End Sub
#End Region

End Class
