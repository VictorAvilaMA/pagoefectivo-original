Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Partial Class ADM_ADEMCoImg
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CEmpresaContratante)

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CEmpresaContratante
    'End Function



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not Context.Request.QueryString("empresaId") Is Nothing Then
                Dim idimagen As Integer = Convert.ToInt32(Context.Request.QueryString("empresaId"))

                Dim cEmpresa As SPE.Web.CEmpresaContratante = CType(ControllerObject, SPE.Web.CEmpresaContratante)
                Dim obeEmpresa As BEEmpresaContratante = cEmpresa.GetRecordByID(idimagen)
                '  obeServicio.LogoImagen

                If (Not obeEmpresa.ImagenLogo Is Nothing) And (obeEmpresa.ImagenLogo.Length > 0) Then
                    Response.ContentType = "image/jpeg"
                    Response.Expires = 0
                    Response.Buffer = True
                    Response.Clear()
                    Response.BinaryWrite(obeEmpresa.ImagenLogo)
                    Response.End()
                End If

            End If




        End If
    End Sub
End Class
