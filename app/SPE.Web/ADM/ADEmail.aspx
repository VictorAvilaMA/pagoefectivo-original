﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="ADEmail.aspx.vb" Inherits="ADM_ADEmail" Async="true" ValidateRequest="false"
    EnableEventValidation="false" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript" src="../Editor/xmlserializer.min.js"></script>
    <script type="text/javascript">

        window.parser = (function (html) {
            var parser = {};

            var addHTMLTagAttributes = function (doc, html) {
                var attributeMatch = /<html((?:\s+[^>]*)?)>/im.exec(html),
            helperDoc = document.implementation.createHTMLDocument(''),
            htmlTagSubstitute,
            i, elementSubstitute, attribute;

                if (!attributeMatch) {
                    return;
                }

                htmlTagSubstitute = '<div' + attributeMatch[1] + '></div>';
                helperDoc.documentElement.innerHTML = htmlTagSubstitute;
                elementSubstitute = helperDoc.querySelector('div');

                for (i = 0; i < elementSubstitute.attributes.length; i++) {
                    attribute = elementSubstitute.attributes[i];
                    doc.documentElement.setAttribute(attribute.name, attribute.value);
                }
            };

            parser.parse = function (html) {
                var doc;
                if ((new DOMParser()).parseFromString('<a></a>', 'text/html')) {
                    doc = (new DOMParser()).parseFromString(html, 'text/html');
                } else {
                    doc = document.implementation.createHTMLDocument('');
                    doc.documentElement.innerHTML = html;

                    addHTMLTagAttributes(doc, html);
                }
                return doc;
            };

            return parser;
        } ());



        function btnDiplayButton(display) {
            var btn = document.getElementById('<%=btnActualizar.ClientID %>');
            btn.style.display = display;
        }


        function plantilla() {

            var info = $('#<%=hdfplantillaeExpresss.ClientID%>').val();
            var tipoPlantilla = $('#<%=hdfTipoPlantilla.ClientID%>').val();
            var framBlock = document.getElementById("fram");
            var innerDoc = framBlock.contentDocument || framBlock.contentWindow.document;

            var plantilla = innerDoc.getElementsByClassName("card-block");
            var botonEditable = innerDoc.getElementsByClassName("btn-codeview")
            botonEditable[0].setAttribute("disabled", true)
            plantilla[0].innerHTML = info;

            if (tipoPlantilla == "53") {
                plantilla[0].setAttribute("contenteditable", "true");
                botonEditable[0].removeAttribute("disabled");
            } else {
                plantilla[0].setAttribute("contenteditable", "false");
                botonEditable[0].setAttribute("disabled", true)
            }
            return true;
        }



        function xml2string(node) {
            if (typeof (XMLSerializer) !== 'undefined') {
                var serializer = new XMLSerializer();
                return serializer.serializeToString(node);
            } else if (node.xml) {
                return node.xml;
            }
        }

        function DoValidation() {
            var framBlock = document.getElementById("fram");
            var innerDoc = framBlock.contentDocument || framBlock.contentWindow.document;
            var templateType = document.getElementById('ctl00_ContentPlaceHolder1_ddlTipoPlantilla').value;
            var plantilla2 = innerDoc.getElementsByClassName("card-block");

            info = plantilla2[0].innerHTML;
            var infoSerializado;
            if (templateType == '48' || templateType == '49') {
                infoSerializado = xml2string(parser.parse(info));
            }
            else {
                infoSerializado = xmlserializer.serializeToString(parser.parse(info));
            }


            $('#<%=hdfplantillaeExpresss.ClientID%>').val(infoSerializado);

            if (valid) {
                __doPostBack('btnActualizar')
            }
        }


        

</script>

    <script type="text/javascript">
        function hideModalPopUp() {
            $("#<%= pnlNuevoParametro.ClientID() %>").hide();
            return;
        }
    </script>
    
    <script type="text/javascript" src="../JScripts/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
//        $('form').submit(function (e) {
//            alert('submit form');
//            for (var instance in CKEDITOR.instances) {
//                if (CKEDITOR.instances.hasOwnProperty(instance)) {
//                    if (instance) {
//                        CKEDITOR.instances[instance].updateElement();
//                        //                        CKEDITOR.remove(instance);
//                    }
//                }
//            }
//        });


        function loadEditor(id, idbutton) {
            if ($('#' + id).length === 0) {
                return;
            }

            deleteEditor(id);
            CKEDITOR.replace(id, { fullPage: true });

            CKEDITOR.instances[id].on('blur', function () {
                CKEDITOR.instances[id].updateElement();
            });
        }

        function deleteEditor(id) {
            var instance = CKEDITOR.instances[id];
            if (instance) {
                CKEDITOR.remove(instance);
            }
        }


    </script>

    <h2>
        Actualizar Plantilla</h2>
    <div class="conten_pasos3" style="min-height: 300px">
        <h4>
            Datos del Servicio</h4>
        <asp:UpdatePanel ID="upFiltros" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdfIdPlantilla" runat="server" Value="" />
                <asp:HiddenField ID="hdfplantillaeExpresss" runat="server" Value="" />
                <asp:HiddenField ID="hdfTipoPlantilla" runat="server" Value="" />

                <p>
           
        </p>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Empresa: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true"
                            Style="float: left">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvEmpresa" runat="server" ValidationGroup="ValidaCampos"
                            ErrorMessage="Debe seleccionar la Empresa." ControlToValidate="ddlEmpresa">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu cmbServicio" Style="float: left"  AutoPostBack="true" >
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvServicio" runat="server" ValidationGroup="ValidaCampos"
                            ErrorMessage="Debe seleccionar el Servicio." ControlToValidate="ddlServicio">*</asp:RequiredFieldValidator>
                    </li>
                    <div runat="server" id="blkOcultar">
                        <li class="t1"><span class="color">>></span> Moneda: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu" Style="float: left" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvIdMoneda" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Debe seleccionar el tipo de Moneda." ControlToValidate="ddlMoneda">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span> Variaci&oacute;n: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlTipoVariacion" runat="server" CssClass="neu" Style="float: left" >
                            </asp:DropDownList>
                        </li>
                    </div>
                    <li class="t1"><span class="color">>></span> Tipo de Plantilla: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoPlantilla" runat="server" CssClass="neu" Style="float: left">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvTipoPlantilla" runat="server" ValidationGroup="ValidaCampos"
                            ErrorMessage="Debe seleccionar el tipo de Plantilla." ControlToValidate="ddlTipoPlantilla">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Descripci&oacute;n: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtPlantillaDesc" runat="server" ReadOnly="true" CssClass="normal"></asp:TextBox>
                    </li>                    
                    <li class="complet" style="height: auto">
                        <asp:Label ID="lblMsjEstructuraHTML" runat="server" Style="width: 100%; height: auto; clear: both"></asp:Label>
                        <%-- OnClientClick="return plantilla()"--%>
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" ValidationGroup="ValidaCampos" 
                            Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 200px" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
    </div>
    <script type="text/javascript">
        var OcultarBloque = function () {

            var blk = document.getElementById("blkOcultar");
            var cmbServicio = document.getElementsByClassName("cmbServicio");

            var value = (cmbServicio[0]).value;
            var formato = (cmbServicio[0].options[cmbServicio[0].selectedIndex]).getAttribute("formato-plantilla")

            if (formato == 53) {
                blk.style.display = "block";
            } else {
                blk.style.display = "none";
            }
        }
    </script>
    <asp:UpdatePanel ID="upEsctructuraHTML" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
        <ContentTemplate>
         <%--   <div id="divEstructuraHTML" runat="server" class="conten_pasos3" visible="false">
                <h4>
                    Estructura HTML</h4>
                <div class="clear">
                </div>
                <HTMLEditor:Editor ID="heEsctructuraEmail" runat="server" AutoFocus="true" Height="500px"
                    Width="100%" NoUnicode="True"  Visible=false />
                    
                <asp:TextBox ID="txtHtmlPlantilla" runat="server" TextMode="MultiLine" Visible="false" ></asp:TextBox>   
                
                <ul class="datos_cip2">
                    <li class="complet" style="height: auto">
                        <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3"   OnClientClick="ConfirmMe();DoValidation()"
                            Text="Actualizar" />
                    </li>
                </ul>
            </div>--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div style="clear: both">
    </div>


   
   <div id="divEstructuraHTML" runat="server" class="conten_pasos3" visible="true">
        <h4>
            Estructura HTML</h4>
        <div class="clear">
        </div>

        <iframe src="../Editor/editor-frame.html" width="100%" height="600px" name="fram" id="fram"></iframe>

        <ul class="datos_cip2">
            <li class="complet" style="height: auto">
                <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3"  style="display:none" OnClientClick="ConfirmMe();DoValidation()"
                    Text="Actualizar" />
            </li>
        </ul>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div id="blkSecciones" runat="server" visible="false"  class="conten_pasos3">
                <h4>Secciones</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Secci&oacute;n 1: </li>
                    <li class="t2">
                        <asp:TextBox runat="server" ID="txtSeccion1" Width="400px"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Secci&oacute;n 2: </li>
                    <li class="t2">
                        <asp:TextBox runat="server" ID="txtSeccion2" Width="400px"></asp:TextBox>
                    </li>
                    <li class="t1" style="height: 95px;"><span class="color">>></span> Secci&oacute;n 3: </li>
                    <li class="t2" style="height: 95px;">
                        <asp:TextBox runat="server" ID="txtSeccion3" Width="400px" TextMode="MultiLine" Height="80px"></asp:TextBox>
                    </li>
                    <li class="t1"" style="height: 95px;"><span class="color">>></span> Secci&oacute;n 4: </li>
                    <li class="t2"" style="height: 95px;">
                        <asp:TextBox runat="server" ID="txtSeccion4" Width="400px" TextMode="MultiLine" Height="80px"></asp:TextBox>
                    </li>
                </ul>
                <div>
                    <ul class="datos_cip2">
                        <li class="complet" style="height: auto">
                            <asp:Button runat="server" ID="btnGrabarSecciones" Text= "Grabar Secciones" CssClass="input_azul3"    />
                        </li>
                    </ul>
                </div>
                <asp:Label ID="lblSeccionesMsj" runat="server" CssClass="MensajeTransaccion" Text=""
                    Style="padding-left: 200px"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divParametrosEmail" runat="server" class="conten_pasos3_2" visible="false">
                <h4>
                    Par&aacute;metros del Email</h4>
                <asp:Label ID="lblEstado" runat="server" CssClass="Etiqueta"></asp:Label>
                <div class="clear">
                </div>
                <div class="result" style="height: auto;">
                    <asp:GridView ID="gvResultado" runat="server" AutoGenerateColumns="False" CssClass="grilla"
                        DataKeyNames="IdPlantillaParametro">
                        <Columns>
                            <asp:TemplateField HeaderText="Codigo" ItemStyle-HorizontalAlign="Center" SortExpression="IdPlantillaParametro">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdPlantillaParametro" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdPlantillaParametro")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FuenteEtiqueta" HeaderText="Fuente" SortExpression="FuenteEtiqueta" />
                            <asp:BoundField DataField="FuenteDescripcion" HeaderText="Descripcion" SortExpression="FuenteDescripcion" />
                            <asp:BoundField DataField="Etiqueta" HeaderText="Etiqueta" SortExpression="Etiqueta" />
                            <asp:BoundField DataField="Valor" HeaderText="Valor" SortExpression="Valor" />
                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtEdit" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                        ToolTip="Actualizar" CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtDelete" runat="server" CommandName="Delete" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/delete.gif" %>'
                                        ToolTip="Eliminar" CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div class="clear">
                </div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul3" Text="Nuevo" />
                        <asp:Button ID="Button1" runat="server" CssClass="input_azul4" Style="display: none"
                            Text="" />
                    </li>
                </ul>
            </div>
            <cc1:ModalPopupExtender ID="mpeNuevoParametro" runat="server" TargetControlID="Button1"
                CancelControlID="imgbtnRegresarHidden" PopupControlID="pnlNuevoParametro" BackgroundCssClass="modalBackground"
                DropShadow="true">
            </cc1:ModalPopupExtender>
            <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                Style="display: none;" CssClass="Hidden" CausesValidation="false" />
            <asp:Panel ID="pnlNuevoParametro" runat="server" CssClass="conten_pasos3" Style="display: none;
                background: white; width: 650px; height: auto; min-height: 0px;">
                <div style="float: right; overflow: hidden;">
                    <asp:ImageButton ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" runat="server"
                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>' CausesValidation="false"></asp:ImageButton>
                </div>
                <asp:HiddenField ID="hdfIdParametroPlantilla" runat="server" Value="" />
                <h4>
                    Datos de Parámetros:</h4>
                <div runat="server" id="divTipoParametro">
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Tipo de Par&aacute;metro:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlTipoParametro" runat="server" CssClass="normal" AutoPostBack="true">
                            </asp:DropDownList>
                        </li>
                    </ul>
                </div>
                <div runat="server" id="divParametro">
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Par&aacute;metro:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlParametros" runat="server" CssClass="normal" AutoPostBack="true">
                            </asp:DropDownList>
                            <li class="t1"><span class="color">>></span> C&oacute;digo:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Etiqueta:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtEtiqueta" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Descripci&oacute;n:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                            </li>
                        </li>
                    </ul>
                </div>
                <div runat="server" id="divValor">
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Valor:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtValor" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        </li>
                    </ul>
                </div>
                <div style="clear: both">
                </div>
                <asp:Label ID="lblMensajeParametros" runat="server" CssClass="MensajeTransaccion"
                    Style="padding-left: 200px"></asp:Label>
                <ul class="datos_cip2">
                    <li class="complet" style="margin-top: 10px">
                        <asp:Button ID="btnActualizarParametro" runat="server" CssClass="input_azul5" OnClientClick="return ConfirmMe();"
                            Text="Actualizar" />
                        <asp:Button ID="btnAgregar" runat="server" CssClass="input_azul5" OnClientClick="return ConfirmMe();"
                            Text="Agregar" />
                        <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="hideModalPopUp()"
                            Text="Cancelar" />
                    </li>
                </ul>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlTipoParametro" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
