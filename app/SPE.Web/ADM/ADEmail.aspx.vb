﻿Imports SPE.Web
Imports System.Linq
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.IO
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Web.Security
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Logging
Imports Amazon.S3.Model
Imports Amazon.S3


Partial Class ADM_ADEmail
    Inherits System.Web.UI.Page

    Private ReadOnly _logger As ILogger = New Logger()

    Private Shared _s3Client As AmazonS3Client = Nothing
    Private ReadOnly s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_ACCESS_KEY")
    Private ReadOnly s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_SECRET_ACCESS_KEY")
    Private ReadOnly s3Region As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_REGION")
    Private ReadOnly s3BucketName As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_BUCKET_NAME")
    Private ReadOnly s3DirectoryPath As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_DIRECTORY_PATH")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'agregado
        'heEsctructuraEmail.Visible = False
 
        If Not Page.IsPostBack Then
            Dim oCntrAdministracionComun As New CAdministrarComun()
            Dim ocntrlEmpresaContrante As New CEmpresaContratante()
            Dim oCntrParametro As New CAdministrarParametro()
            Dim oCntrPlantilla As New CPlantilla()
            Dim oBEEmpresaContratante As New BEEmpresaContratante()
            Dim oCntrComun As New CComun()
            oBEEmpresaContratante.IdEstado = EstadoMantenimiento.Activo

            DropDownlistBinding(ddlMoneda, oCntrAdministracionComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
            DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Seleccione::")
            DropDownlistBinding(ddlTipoPlantilla, New List(Of BEPlantillaTipo), "Descripcion", "IdPlantillaTipo", "::Seleccione::")
            DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Seleccione::")
            DropDownlistBinding(ddlTipoParametro, oCntrParametro.ConsultarParametroPorCodigoGrupo("TPLP"), "Descripcion", "Id")
            DropDownlistBinding(ddlTipoVariacion, oCntrComun.ConsultarPlantillaTipoVariacion(), "Descripcion", "GrupoCodigo")

            Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = oCntrPlantilla.ConsultarParametrosFuentes()
            ViewState("listBEPlantillaParametroFuente") = listBEPlantillaParametro
            DropDownlistBinding(ddlParametros, listBEPlantillaParametro, "FuenteCodigo", "IdPlantillaFuenteParametro")
            'Using ocntrlPlantilla As New CPlantilla()
            '    DropDownlistBinding(ddlTipoPlantilla, ocntrlPlantilla.ConsultarPlantillaTipo, "Descripcion", "IdPlantillaTipo", "::Seleccione::")
            'End Using

            If Session("BEServicio") IsNot Nothing Then
                Dim oBEServicio As BEServicio = CType(Session("BEServicio"), BEServicio)
                ddlEmpresa.SelectedValue = oBEServicio.IdEmpresaContratante
                ddlEmpresa_SelectedIndexChanged(Nothing, Nothing)
                ddlServicio.SelectedValue = oBEServicio.IdServicio
                Session.Remove("BEServicio")
                ddlEmpresa.Enabled = False
                ddlServicio.Enabled = False
                btnLimpiar.Visible = False
            End If
            cargarEstructuraHTML()

            If Not Session("msjEstructuraHTML") Is Nothing Then
                lblMsjEstructuraHTML.Text = Session("msjEstructuraHTML").ToString()
                Session("msjEstructuraHTML") = Nothing
            End If
        End If
    End Sub

    'Protected Sub ddlTipoPlantilla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoPlantilla.SelectedIndexChanged
    '    
    'End Sub

    'Protected Sub ddlServicio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlServicio.SelectedIndexChanged

    'End Sub

    Public Sub cargarEstructuraHTML()
        If ddlServicio.SelectedIndex = 0 Or ddlTipoPlantilla.SelectedIndex = 0 Then
            Return
        End If

        Try
            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Inicia proceso de carga de la plantilla - {0}", ddlTipoPlantilla.SelectedItem.Text))

            Using cntrlPlantilla As New CPlantilla()
                Dim idServicio As Integer = CInt(ddlServicio.SelectedValue)
                Dim codPlantillaTipo As String = ddlTipoPlantilla.SelectedValue

                'Dim idMoneda As Integer = CInt(ddlMoneda.SelectedValue)
                Dim obePlantilla As BEPlantilla = cntrlPlantilla.ConsultarPlantillaPorTipoYVariacionExiste(idServicio, codPlantillaTipo, ddlTipoVariacion.SelectedValue)
                lblMsjEstructuraHTML.Text = ""
                If (obePlantilla Is Nothing) Then
                    obePlantilla = New BEPlantilla()
                    obePlantilla.IdUsuarioActualizacion = UserInfo.IdUsuario
                    obePlantilla.IdServicio = CInt(ddlServicio.SelectedValue)
                    obePlantilla.IdPlantillaTipo = CInt(ddlTipoPlantilla.SelectedValue)
                    obePlantilla.CodigoPlantillaTipoVariacion = ddlTipoVariacion.SelectedValue
                    obePlantilla = cntrlPlantilla.GenerarPlantillaDesdePlantillaBase(obePlantilla)
                    lblMsjEstructuraHTML.Text = "* La plantilla no existía ha sido creada a partir de la plantilla base de: " + obePlantilla.Descripcion
                    _logger.Info(txtCodigo.Text.Trim & ": CARGA PLANTILLA -> " + String.Format("La plantilla no existia ha sido creada apartir de la plantilla base de: " + obePlantilla.Descripcion))
                End If
                If (obePlantilla IsNot Nothing) Then

                    'comentado
                    'heEsctructuraEmail.Content = obePlantilla.PlantillaHtml

                    'plantilla pe la antigua---eliminar
                    hdfplantillaeExpresss.Value = obePlantilla.PlantillaHtml

                    txtPlantillaDesc.Text = obePlantilla.Descripcion
                    hdfIdPlantilla.Value = obePlantilla.IdPlantilla
                    Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = cntrlPlantilla.ConsultarPlantillaParametroPorIdPlantilla(obePlantilla.IdPlantilla)
                    ViewState("listBEPlantillaParametro") = listBEPlantillaParametro
                    gvResultado.DataSource = listBEPlantillaParametro
                    gvResultado.DataBind()
                    divParametrosEmail.Visible = If(listBEPlantillaParametro.Count > 0, True, False)
                    _logger.Info(txtCodigo.Text.Trim & ": CARGA PLANTILLA -> " + String.Format("La plantilla existe y se carga: " + obePlantilla.Descripcion))
                Else
                    lblMsjEstructuraHTML.Text = "* No se encontro la plantilla, no se pudo generar"
                    _logger.Info(txtCodigo.Text.Trim & ": CARGA PLANTILLA -> " + String.Format("No se encontro la plantilla, no se pudo generar - {0}", ddlTipoPlantilla.SelectedItem.Text))
                End If
                UpdatePanel1.Visible = True
                'comentado
                'heEsctructuraEmail.Visible = True
                divEstructuraHTML.Visible = obePlantilla IsNot Nothing
                btnNuevo.Enabled = True

                '' Plantilla desde S3
                'comentado
                'txtHtmlPlantilla.Visible = False

                Dim ocntrlServicio As New CServicio
                Dim codServicio = ocntrlServicio.ObtenerServicioPorID(idServicio)

                'Plantillas de Cobranzas
                Dim codigoPlantillaSolicitudPago = "48" ' "CobSolPago"
                Dim codigoPlantillaSolicitudPagoRecordatorio = "49" ' "CobSolPagoRec"

                Dim list As New List(Of String)
                list.Add(codigoPlantillaSolicitudPago)
                list.Add(codigoPlantillaSolicitudPagoRecordatorio)

                'Plantillas de generacion cip - Microservice
                Dim codigoPlantillaMsEmailGenCip = "50"
                Dim codigoPlantillaMsEmailGenCipEnUs = "51"

                Dim listEmailGenCip As New List(Of String)
                listEmailGenCip.Add(codigoPlantillaMsEmailGenCip)
                listEmailGenCip.Add(codigoPlantillaMsEmailGenCipEnUs)

                'Plantilla de integración pagos - Microservice
                Dim plantillaPaymentsCip As String = "52"
                Dim plantillaPaymentsCipEnUs As String = "53"
                Dim lstPaymentsCip As New List(Of String)
                lstPaymentsCip.Add(plantillaPaymentsCip)
                lstPaymentsCip.Add(plantillaPaymentsCipEnUs)

                Dim s3AccessKey As String = ConfigurationManager.AppSettings("COBRANZA_S3_ACCESS_KEY")
                Dim s3SecretAccessKey As String = ConfigurationManager.AppSettings("COBRANZA_S3_SECRET_ACCESS_KEY")
                Dim s3Region As String = ConfigurationManager.AppSettings("COBRANZA_S3_REGION")
                Dim s3BucketName As String = ConfigurationManager.AppSettings("COBRANZA_S3_BUCKET_NAME")
                Dim s3DirectoryPath As String = ConfigurationManager.AppSettings("COBRANZA_S3_DIRECTORY_PATH")

                If list.Contains(codPlantillaTipo) Then
                    _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Inicia proceso de carga de la plantilla de S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    CrearPlantillaCobranzas(codPlantillaTipo, obePlantilla, codigoPlantillaSolicitudPago, codigoPlantillaSolicitudPagoRecordatorio, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio)
                ElseIf (listEmailGenCip.Contains(codPlantillaTipo)) Then
                    _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Inicia proceso de carga de la plantilla de S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    Dim plantilla As New PlantillaCorreo(codPlantillaTipo, obePlantilla, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio, "Template", "Core/GeneracionCIP", "generacion-de-cip")
                    CrearPlantilla(plantilla)
                    'CrearPlantillaGeneracionCip(codPlantillaTipo, obePlantilla, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio)
                ElseIf (lstPaymentsCip.Contains(codPlantillaTipo)) Then
                    Dim plantilla As New PlantillaCorreo(codPlantillaTipo, obePlantilla, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio, "Template", "Core/Payments", "payments")
                    _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Inicia proceso de carga de la plantilla de S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    CrearPlantilla(Plantilla)
                End If

            End Using

            _logger.Info(txtCodigo.Text.Trim & ": CARGA PLANTILLA -> " + String.Format("Se cargó la plantilla - {0}", ddlTipoPlantilla.SelectedItem.Text))

        Catch ex As Exception
            _logger.Error(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + ex.Message)
        End Try

    End Sub

    Private Sub CrearPlantilla(plantilla As PlantillaCorreo)

        Dim amazonS3 As New AWS.AmazonS3(plantilla.S3AccessKey, plantilla.S3SecretAccessKey, plantilla.S3Region, plantilla.S3BucketName, plantilla.S3DirectoryPath)

        Dim variacion = ddlTipoVariacion.SelectedValue
        Dim moneda = ddlMoneda.SelectedValue

        Dim monedaNombre = ""
        If moneda = "1" Then
            monedaNombre = "-pen"
        ElseIf moneda = "2" Then
            monedaNombre = "-usd"
        ElseIf moneda = "3" Then
            monedaNombre = "-eur"
        ElseIf moneda = "4" Then
            monedaNombre = "-chf"
        ElseIf moneda = "5" Then
            monedaNombre = "-clp"
        End If

        Dim plantillaPayments = plantilla.NombreArchivoPlantillaBase + monedaNombre + ".html"
        Dim plantillaPaymentsEsPe = plantilla.NombreArchivoPlantillaBase + monedaNombre + "-ES-PE.html"
        Dim plantillaPaymentsEnUs = plantilla.NombreArchivoPlantillaBase + monedaNombre + "-EN-US.html"
        Dim plantillaPaymentsFrFr = plantilla.NombreArchivoPlantillaBase + monedaNombre + "-FR-FR.html"

        Dim filenameBase = plantilla.RutaPlantillaBase + "/" + plantillaPayments
        Dim filename = ""

        If variacion = "st" Then
            filename = plantilla.RutaPlantillaDestino + "/" + plantilla.Servicio.Codigo + "/" + plantillaPaymentsEsPe
        ElseIf variacion = "en" Then
            filename = plantilla.RutaPlantillaDestino + "/" + plantilla.Servicio.Codigo + "/" + plantillaPaymentsEnUs
        ElseIf variacion = "fr" Then
            filename = plantilla.RutaPlantillaDestino + "/" + plantilla.Servicio.Codigo + "/" + plantillaPaymentsFrFr
        End If

        Dim fileInfo = amazonS3.FileInfo(plantilla.S3BucketName, filename)
        Dim fileInfoBase = amazonS3.FileInfo(plantilla.S3BucketName, filenameBase)
        Dim divHtmlVisible = False

        If fileInfo.Exists Then
            Dim plantillaHtml = fileInfo.OpenText().ReadToEnd()
            hdfplantillaeExpresss.Value = plantillaHtml.ToString()
            'txtHtmlPlantilla.Visible = True
            'heEsctructuraEmail.Visible = False
            hdfIdPlantilla.Value = plantilla.CodPlantillaTipo
            divHtmlVisible = True
            UpdatePanel1.Visible = True
            divEstructuraHTML.Visible = divHtmlVisible
            divParametrosEmail.Visible = divHtmlVisible
            btnNuevo.Enabled = False

            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> Contenido Plantilla en S3 -> " + Environment.NewLine + plantillaHtml)
            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Se cargo la plantilla de S3- {0}", filename))

            'Dim sScript = "loadEditor('" & txtHtmlPlantilla.ClientID & "','" & btnActualizar.ClientID & "');"
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ckeditor", sScript, True)
        Else
            'Dim fileInfoBase = amazonS3.FileInfo(s3BucketName, filenameBase)
            fileInfoBase.CopyTo(plantilla.S3BucketName, filename, True)
            lblMsjEstructuraHTML.Text = "* La plantilla no existía ha sido creada a partir de la plantilla base de: " + plantilla.Plantilla.Descripcion
            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Se creo la plantilla en S3- {0}", filename))
        End If

        UpdatePanel1.Visible = True
        divEstructuraHTML.Visible = divHtmlVisible
        divParametrosEmail.Visible = divHtmlVisible
    End Sub

    'Private Sub CrearPlantillaGeneracionCip(codPlantillaTipo As String, obePlantilla As BEPlantilla, s3AccessKey As String, s3SecretAccessKey As String, s3Region As String, s3BucketName As String, s3DirectoryPath As String, codServicio As BEServicio)

    '    Dim amazonS3 As New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

    '    Dim variacion = ddlTipoVariacion.SelectedValue
    '    Dim moneda = ddlMoneda.SelectedValue

    '    Dim monedaNombre = ""
    '    If moneda = "1" Then
    '        monedaNombre = "-pen"
    '    ElseIf moneda = "2" Then
    '        monedaNombre = "-usd"
    '    ElseIf moneda = "3" Then
    '        monedaNombre = "-eur"
    '    ElseIf moneda = "4" Then
    '        monedaNombre = "-chf"
    '    ElseIf moneda = "5" Then
    '        monedaNombre = "-clp"
    '    End If

    '    Dim plantillaGeneracionCip = "generacion-de-cip" + monedaNombre + ".html"
    '    Dim plantillaGeneracionCipEsPe = "generacion-de-cip" + monedaNombre + "-ES-PE.html"
    '    Dim plantillaGeneracionCipEnUs = "generacion-de-cip" + monedaNombre + "-EN-US.html"
    '    Dim plantillaGeneracionCipFrFr = "generacion-de-cip" + monedaNombre + "-FR-FR.html"

    '    Dim filenameBase = "Template/" + plantillaGeneracionCip
    '    Dim filename = ""

    '    If variacion = "st" Then
    '        filename = "Core/GeneracionCIP/" + codServicio.Codigo + "/" + plantillaGeneracionCipEsPe
    '    ElseIf variacion = "en" Then
    '        filename = "Core/GeneracionCIP/" + codServicio.Codigo + "/" + plantillaGeneracionCipEnUs
    '    ElseIf variacion = "fr" Then
    '        filename = "Core/GeneracionCIP/" + codServicio.Codigo + "/" + plantillaGeneracionCipFrFr
    '    End If

    '    Dim fileInfo = amazonS3.FileInfo(s3BucketName, filename)
    '    Dim fileInfoBase = amazonS3.FileInfo(s3BucketName, filenameBase)
    '    Dim divHtmlVisible = False

    '    If fileInfo.Exists Then
    '        Dim plantillaHtml = fileInfo.OpenText().ReadToEnd()
    '        txtHtmlPlantilla.Text = plantillaHtml.ToString()
    '        txtHtmlPlantilla.Visible = True
    '        heEsctructuraEmail.Visible = False
    '        hdfIdPlantilla.Value = codPlantillaTipo
    '        divHtmlVisible = True
    '        UpdatePanel1.Visible = True
    '        divEstructuraHTML.Visible = divHtmlVisible
    '        divParametrosEmail.Visible = divHtmlVisible
    '        btnNuevo.Enabled = False

    '        _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> Contenido Plantilla en S3 -> " + Environment.NewLine + plantillaHtml)
    '        _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Se cargo la plantilla de S3- {0}", filename))

    '        Dim sScript = "loadEditor('" & txtHtmlPlantilla.ClientID & "','" & btnActualizar.ClientID & "');"
    '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ckeditor", sScript, True)
    '    Else
    '        'Dim fileInfoBase = amazonS3.FileInfo(s3BucketName, filenameBase)
    '        fileInfoBase.CopyTo(s3BucketName, filename, True)
    '        lblMsjEstructuraHTML.Text = "* La plantilla no existía ha sido creada a partir de la plantilla base de: " + obePlantilla.Descripcion
    '        _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Se creo la plantilla en S3- {0}", filename))
    '    End If

    '    UpdatePanel1.Visible = True
    '    divEstructuraHTML.Visible = divHtmlVisible
    '    divParametrosEmail.Visible = divHtmlVisible
    'End Sub

    Private Sub CrearPlantillaCobranzas(codPlantillaTipo As String, obePlantilla As BEPlantilla, codigoPlantillaSolicitudPago As String, codigoPlantillaSolicitudPagoRecordatorio As String, s3AccessKey As String, s3SecretAccessKey As String, s3Region As String, s3BucketName As String, s3DirectoryPath As String, codServicio As BEServicio)

        Dim amazonS3 As New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        Dim PlantillaSolicitudPago = "solicitud-de-pago.html"
        Dim PlantillaSolicitudPagoRecordatorio = "recordatorio-solicitud-de-pago.html"

        Dim filenameBase = ""
        Dim filename = ""

        If codPlantillaTipo = codigoPlantillaSolicitudPago Then
            filenameBase = "Template/" + PlantillaSolicitudPago
            filename = "SolicitudPago/" + codServicio.Codigo + "/" + PlantillaSolicitudPago
        ElseIf codPlantillaTipo = codigoPlantillaSolicitudPagoRecordatorio Then
            filenameBase = "Template/" + PlantillaSolicitudPagoRecordatorio
            filename = "Recordatorio/" + codServicio.Codigo + "/" + PlantillaSolicitudPagoRecordatorio
        End If

        Dim fileInfo = amazonS3.FileInfo(s3BucketName, filename)
        Dim fileInfoBase = amazonS3.FileInfo(s3BucketName, filenameBase)
        Dim divHtmlVisible = False

        If fileInfo.Exists Then
            Dim plantillaHtml = fileInfo.OpenText().ReadToEnd()
            hdfplantillaeExpresss.Value = plantillaHtml.ToString()
            'txtHtmlPlantilla.Visible = True
            'heEsctructuraEmail.Visible = False
            hdfIdPlantilla.Value = codPlantillaTipo
            divHtmlVisible = True
            btnNuevo.Enabled = False

            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> Contenido Plantilla en S3 -> " + Environment.NewLine + plantillaHtml)
            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Se cargo la plantilla de S3- {0}", filename))

            'Dim sScript = "loadEditor('" & txtHtmlPlantilla.ClientID & "','" & btnActualizar.ClientID & "');"
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ckeditor", sScript, True)
        Else
            'Dim fileInfoBase = amazonS3.FileInfo(s3BucketName, filenameBase)
            fileInfoBase.CopyTo(s3BucketName, filename, True)
            lblMsjEstructuraHTML.Text = "* La plantilla no existía ha sido creada a partir de la plantilla base de: " + obePlantilla.Descripcion
            _logger.Info(txtCodigo.Text.Trim & ": CARGAR PLANTILLA -> " + String.Format("Se creo la plantilla en S3- {0}", filename))
        End If

        UpdatePanel1.Visible = False
        divEstructuraHTML.Visible = divHtmlVisible
        divParametrosEmail.Visible = divHtmlVisible
    End Sub

    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        'comentado
        'heEsctructuraEmail.Content = ""
        lblMsjEstructuraHTML.Text = ""
        If ddlEmpresa.SelectedIndex = 0 Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Seleccione::")
            'comentado
            'heEsctructuraEmail.Content = ""
            Return
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            objbeservicio.IdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
            objbeservicio.IdEstado = EstadoMantenimiento.Activo
            ddlMoneda.SelectedIndex = 0
            ddlTipoPlantilla.SelectedIndex = 0
            txtPlantillaDesc.Text = String.Empty
            hdfplantillaeExpresss.Value = ""
            divParametrosEmail.Visible = False
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: plantilla(); javascript: btnDiplayButton('none'); ", True)
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: btnDiplayButton('none');", True)

            Dim oListServicio = ocntrlServicio.SearchByParameters(objbeservicio)
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, oListServicio, "Nombre", "IdServicio", "::Seleccione::")


            For index As Integer = 0 To oListServicio.Count - 1
                ddlServicio.Items.FindByValue(DirectCast(oListServicio.Item(index), BEServicio).IdServicio.ToString).Attributes("formato-plantilla") = DirectCast(oListServicio.Item(index), BEServicio).PlantillaFormato
                'For item As Integer = 1 To ddlServicio.Items.Count
                '    If ddlServicio.Items(1).Value = "" Then
                '        ddlServicio.Items(1).Value = "0"
                '    End If
                '    If ddlServicio.Items(item).Value = DirectCast(oListServicio.Item(index), BEServicio).IdServicio Then
                '        ddlServicio.Items(item).Attributes("formato-plantilla") = DirectCast(oListServicio.Item(index), BEServicio).PlantillaFormato
                '    End If
                'Next
            Next

        End Using
    End Sub

    Protected Sub ddlMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMoneda.SelectedIndexChanged
        'comentado
        'heEsctructuraEmail.Content = ""
        If ddlMoneda.SelectedIndex = 0 Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoPlantilla, New List(Of BEPlantillaTipo), "Descripcion", "IdPlantillaTipo", "::Seleccione::")
            'comentado
            'heEsctructuraEmail.Content = ""
            Return
        End If
        Using ocntrlPlantilla As New CPlantilla()
            Dim objbePlantilla As New BEPlantillaTipo()
            objbePlantilla.IdMoneda = CInt(ddlMoneda.SelectedValue)
            'objbePlantilla.es = EstadoMantenimiento.Activo
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoPlantilla, ocntrlPlantilla.ConsultarPlantillaTipoPorMonedaYServicio(objbePlantilla.IdMoneda, ddlServicio.SelectedValue), "Descripcion", "IdPlantillaTipo", "::Seleccione::")
        End Using
    End Sub

    Protected Sub ddlServicio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlServicio.SelectedIndexChanged
        lblMsjEstructuraHTML.Text = ""
        Dim obServicio As New BEServicio
        Using ocntrlServicio As New CServicio()
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: btnDiplayButton('none');", True)
            txtDescripcion.Text = ""
            obServicio = ocntrlServicio.ObtenerServicioPorID(ddlServicio.SelectedValue)
            hdfTipoPlantilla.Value = obServicio.PlantillaFormato
            If obServicio.PlantillaFormato = 53 Then
                blkOcultar.Visible = True
                ddlMoneda.SelectedIndex = 0
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoPlantilla, New List(Of BEPlantillaTipo), "Descripcion", "IdPlantillaTipo", "::Seleccione::")
            Else
                blkOcultar.Visible = False

                Using ocntrlPlantilla As New CPlantilla()
                    Dim objbePlantilla As New BEPlantillaTipo()
                    _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoPlantilla, ocntrlPlantilla.ConsultarPlantillaTipoPorMonedaYServicio(0, ddlServicio.SelectedValue), "Descripcion", "IdPlantillaTipo", "::Seleccione::")
                End Using
            End If
        End Using

    End Sub

    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            lblMensajeParametros.Text = String.Empty
            lblSeccionesMsj.Text = String.Empty
            Dim resultado As Integer = 0
            Dim obePlantilla As New BEPlantilla()

            If ((hdfIdPlantilla.Value <> "") And (ddlServicio.SelectedValue <> "") And (ddlTipoPlantilla.SelectedValue <> "")) Then

                _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Inicia proceso de actualización de la plantilla - {0}", ddlTipoPlantilla.SelectedItem.Text))

                Dim idServicio As Integer = CInt(ddlServicio.SelectedValue)
                Dim codPlantillaTipo As String = ddlTipoPlantilla.SelectedValue

                Dim ocntrlServicio As New CServicio
                Dim codServicio = ocntrlServicio.ObtenerServicioPorID(idServicio)

                'Plantillas Cobranzas
                Dim codigoPlantillaSolicitudPago = "48" ' "CobSolPago"
                Dim codigoPlantillaSolicitudPagoRecordatorio = "49" ' "CobSolPagoRec"

                Dim list As New List(Of String)
                list.Add(codigoPlantillaSolicitudPago)
                list.Add(codigoPlantillaSolicitudPagoRecordatorio)

                'Plantillas de generacion cip - Microservice
                Dim codigoPlantillaMsEmailGenCip = "50"
                Dim codigoPlantillaMsEmailGenCipEnUs = "51"

                Dim listEmailGenCip As New List(Of String)
                listEmailGenCip.Add(codigoPlantillaMsEmailGenCip)
                listEmailGenCip.Add(codigoPlantillaMsEmailGenCipEnUs)

                'Plantilla de integración pagos - Microservice
                Dim plantillaPaymentsCip As String = "52"
                Dim plantillaPaymentsCipEnUs As String = "53"
                Dim lstPaymentsCip As New List(Of String)
                lstPaymentsCip.Add(plantillaPaymentsCip)
                lstPaymentsCip.Add(plantillaPaymentsCipEnUs)

                Dim s3AccessKey As String = ConfigurationManager.AppSettings("COBRANZA_S3_ACCESS_KEY")
                Dim s3SecretAccessKey As String = ConfigurationManager.AppSettings("COBRANZA_S3_SECRET_ACCESS_KEY")
                Dim s3Region As String = ConfigurationManager.AppSettings("COBRANZA_S3_REGION")
                Dim s3BucketName As String = ConfigurationManager.AppSettings("COBRANZA_S3_BUCKET_NAME")
                Dim s3DirectoryPath As String = ConfigurationManager.AppSettings("COBRANZA_S3_DIRECTORY_PATH")

                If list.Contains(codPlantillaTipo) Then
                    _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Inicia proceso de actualización de la plantilla en S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    resultado = RegistrarPlantillaCobranzas(codigoPlantillaSolicitudPago, codigoPlantillaSolicitudPagoRecordatorio, codPlantillaTipo, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio)
                ElseIf (listEmailGenCip.Contains(codPlantillaTipo)) Then
                    _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Inicia proceso de actualización de la plantilla en S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    Dim plantilla As New PlantillaCorreo(codPlantillaTipo, obePlantilla, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio, "Template", "Core/GeneracionCIP", "generacion-de-cip")
                    'resultado = RegistrarPlantillaGeneracionCip(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio)
                    resultado = RegistrarPlantilla(Plantilla)
                ElseIf (lstPaymentsCip.Contains(codPlantillaTipo)) Then
                    _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Inicia proceso de actualización de la plantilla en S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    Dim plantilla As New PlantillaCorreo(codPlantillaTipo, obePlantilla, s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath, codServicio, "Template", "Core/Payments", "payments")
                    resultado = RegistrarPlantilla(plantilla)
                Else
                    Using cntrlPlantilla As New CPlantilla()
                        obePlantilla.PlantillaHtml = hdfplantillaeExpresss.Value
                        obePlantilla.IdPlantilla = CInt(hdfIdPlantilla.Value)
                        obePlantilla.IdUsuarioActualizacion = UserInfo.IdUsuario
                        resultado = cntrlPlantilla.ActualizarPlantilla(obePlantilla)
                    End Using
                End If

                If resultado > 0 Then
                    lblMsjEstructuraHTML.Text = String.Format("Se actualizó la plantilla - {0} del tipo {1} ", txtPlantillaDesc.Text, ddlTipoPlantilla.SelectedItem.Text)
                    _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla - {0}", ddlTipoPlantilla.SelectedItem.Text))
                    'limpiarControles()
                Else
                    lblMsjEstructuraHTML.Text = String.Format("No se pudo actualizar la plantilla - {0} del tipo {1} ", txtDescripcion.Text, ddlTipoPlantilla.SelectedValue)
                    _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("No se pudo actualizar la plantilla - {0}", ddlTipoPlantilla.SelectedItem.Text))
                End If
            Else
                lblMsjEstructuraHTML.Text = "Debe seleccionar al menos un servicio y un tipo de Plantilla."
            End If

        Catch ex As Exception
            lblMsjEstructuraHTML.Text = ex.Message
            _logger.Error(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + ex.Message)
        End Try
    End Sub

    Private Function RegistrarPlantilla(plantilla As PlantillaCorreo) As Integer
        Dim resultado As Integer

        Dim amazonS3 As New AWS.AmazonS3(plantilla.S3AccessKey, plantilla.S3SecretAccessKey, plantilla.S3Region, plantilla.S3BucketName, plantilla.S3DirectoryPath)

        Dim variacion = ddlTipoVariacion.SelectedValue
        Dim moneda = ddlMoneda.SelectedValue
        Dim monedaNombre = ""
        If moneda = "1" Then
            monedaNombre = "-pen"
        ElseIf moneda = "2" Then
            monedaNombre = "-usd"
        ElseIf moneda = "3" Then
            monedaNombre = "-eur"
        ElseIf moneda = "4" Then
            monedaNombre = "-chf"
        ElseIf moneda = "5" Then
            monedaNombre = "-clp"
        End If

        Dim plantillaGeneracionCipEsPe = plantilla.NombreArchivoPlantillaBase + monedaNombre + "-ES-PE.html"
        Dim plantillaGeneracionCipEnUs = plantilla.NombreArchivoPlantillaBase + monedaNombre + "-EN-US.html"
        Dim plantillaGeneracionCipFrFr = plantilla.NombreArchivoPlantillaBase + monedaNombre + "-FR-FR.html"

        Dim filename = ""

        If variacion = "st" Then
            filename = plantilla.RutaPlantillaDestino + "/" + plantilla.Servicio.Codigo + "/" + plantillaGeneracionCipEsPe
        ElseIf variacion = "en" Then
            filename = plantilla.RutaPlantillaDestino + "/" + plantilla.Servicio.Codigo + "/" + plantillaGeneracionCipEnUs
        ElseIf variacion = "fr" Then
            filename = plantilla.RutaPlantillaDestino + "/" + plantilla.Servicio.Codigo + "/" + plantillaGeneracionCipFrFr
        End If

        _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> Contenido Plantilla en S3 -> " + Environment.NewLine + hdfplantillaeExpresss.Value)
        Dim bytes = Encoding.UTF8.GetBytes(hdfplantillaeExpresss.Value)
        Dim fs = New MemoryStream(bytes)

        _logger.Debug(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla en S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
        Dim create = amazonS3.UploadFileFromStream(fs, filename)

        If create Then
            resultado = 1
            _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla en S3 - {0}", filename))
        Else
            resultado = 0
            _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("No se pudo actualizar la plantilla en S3 - {0}", filename))
        End If

        'txtHtmlPlantilla.Visible = True
        Session("msjEstructuraHTML") = String.Format("Se actualizó la plantilla: {0}", txtPlantillaDesc.Text)

        'Dim sScript = "loadEditor('" & txtHtmlPlantilla.ClientID & "');"
        'Dim sScript = "deleteEditor('" & txtHtmlPlantilla.ClientID & "');"
        Dim sScript = "location.reload();"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ckeditor", sScript, True)
        Return resultado
    End Function

    'Private Function RegistrarPlantillaGeneracionCip(s3AccessKey As String, s3SecretAccessKey As String, s3Region As String, s3BucketName As String, s3DirectoryPath As String, codServicio As BEServicio) As Integer
    '    Dim resultado As Integer

    '    Dim amazonS3 As New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

    '    Dim variacion = ddlTipoVariacion.SelectedValue
    '    Dim moneda = ddlMoneda.SelectedValue
    '    Dim monedaNombre = ""
    '    If moneda = "1" Then
    '        monedaNombre = "-pen"
    '    ElseIf moneda = "2" Then
    '        monedaNombre = "-usd"
    '    ElseIf moneda = "3" Then
    '        monedaNombre = "-eur"
    '    ElseIf moneda = "4" Then
    '        monedaNombre = "-chf"
    '    ElseIf moneda = "5" Then
    '        monedaNombre = "-clp"
    '    End If

    '    Dim plantillaGeneracionCipEsPe = "generacion-de-cip" + monedaNombre + "-ES-PE.html"
    '    Dim plantillaGeneracionCipEnUs = "generacion-de-cip" + monedaNombre + "-EN-US.html"
    '    Dim plantillaGeneracionCipFrFr = "generacion-de-cip" + monedaNombre + "-FR-FR.html"

    '    Dim filename = ""

    '    If variacion = "st" Then
    '        filename = "Core/GeneracionCIP/" + codServicio.Codigo + "/" + plantillaGeneracionCipEsPe
    '    ElseIf variacion = "en" Then
    '        filename = "Core/GeneracionCIP/" + codServicio.Codigo + "/" + plantillaGeneracionCipEnUs
    '    ElseIf variacion = "fr" Then
    '        filename = "Core/GeneracionCIP/" + codServicio.Codigo + "/" + plantillaGeneracionCipFrFr
    '    End If

    '    _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> Contenido Plantilla en S3 -> " + Environment.NewLine + txtHtmlPlantilla.Text)
    '    Dim bytes = Encoding.UTF8.GetBytes(txtHtmlPlantilla.Text)
    '    Dim fs = New MemoryStream(bytes)

    '    _logger.Debug(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla en S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
    '    Dim create = amazonS3.UploadFileFromStream(fs, filename)

    '    If create Then
    '        resultado = 1
    '        _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla en S3 - {0}", filename))
    '    Else
    '        resultado = 0
    '        _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("No se pudo actualizar la plantilla en S3 - {0}", filename))
    '    End If

    '    txtHtmlPlantilla.Visible = True
    '    Session("msjEstructuraHTML") = String.Format("Se actualizó la plantilla: {0}", txtPlantillaDesc.Text)

    '    'Dim sScript = "loadEditor('" & txtHtmlPlantilla.ClientID & "');"
    '    'Dim sScript = "deleteEditor('" & txtHtmlPlantilla.ClientID & "');"
    '    Dim sScript = "location.reload();"
    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ckeditor", sScript, True)
    '    Return resultado
    'End Function

    Private Function RegistrarPlantillaCobranzas(codigoPlantillaSolicitudPago As String, codigoPlantillaSolicitudPagoRecordatorio As String, codPlantillaTipo As String, s3AccessKey As String, s3SecretAccessKey As String, s3Region As String, s3BucketName As String, s3DirectoryPath As String, codServicio As BEServicio) As Integer
        Dim resultado As Integer

        Dim amazonS3 As New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        Dim PlantillaSolicitudPago = "solicitud-de-pago.html"
        Dim PlantillaSolicitudPagoRecordatorio = "recordatorio-solicitud-de-pago.html"

        Dim filename = ""

        If codPlantillaTipo = codigoPlantillaSolicitudPago Then
            filename = "SolicitudPago/" + codServicio.Codigo + "/" + PlantillaSolicitudPago
        ElseIf codPlantillaTipo = codigoPlantillaSolicitudPagoRecordatorio Then
            filename = "Recordatorio/" + codServicio.Codigo + "/" + PlantillaSolicitudPagoRecordatorio
        End If

        _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> Contenido Plantilla en S3 -> " + Environment.NewLine + hdfplantillaeExpresss.Value)
        Dim bytes = Encoding.UTF8.GetBytes(hdfplantillaeExpresss.Value)
        Dim fs = New MemoryStream(bytes)

        _logger.Debug(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla en S3 - {0}", ddlTipoPlantilla.SelectedItem.Text))
        Dim create = amazonS3.UploadFileFromStream(fs, filename)

        If create Then
            resultado = 1
            _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("Se actualizó la plantilla en S3 - {0}", filename))
        Else
            resultado = 0
            _logger.Info(txtCodigo.Text.Trim & ": ACTUALIZAR -> " + String.Format("No se pudo actualizar la plantilla en S3 - {0}", filename))
        End If

        'txtHtmlPlantilla.Visible = True
        Session("msjEstructuraHTML") = String.Format("Se actualizó la plantilla: {0}", txtPlantillaDesc.Text)

        'Dim sScript = "loadEditor('" & txtHtmlPlantilla.ClientID & "');"
        'Dim sScript = "deleteEditor('" & txtHtmlPlantilla.ClientID & "');"
        Dim sScript = "location.reload();"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ckeditor", sScript, True)
        Return resultado
    End Function

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Edit"
                Dim lblIdParametro As Label = DirectCast(gvResultado.Rows(CInt(e.CommandArgument.ToString())).Controls(0).FindControl("lblIdPlantillaParametro"), Label)
                If ViewState("listBEPlantillaParametro") IsNot Nothing Then
                    Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = DirectCast(ViewState("listBEPlantillaParametro"), List(Of BEPlantillaParametro))
                    Dim oBEPlantillaParametro As BEPlantillaParametro = listBEPlantillaParametro.First(Function(x) x.IdPlantillaParametro = CInt(lblIdParametro.Text))
                    CargarPanelParametro(oBEPlantillaParametro)
                    pnlNuevoParametro.Visible = True
                    mpeNuevoParametro.Show()
                End If
            Case "Delete"
                Dim lblIdParametro As Label = DirectCast(gvResultado.Rows(CInt(e.CommandArgument.ToString())).Controls(0).FindControl("lblIdPlantillaParametro"), Label)
                If ViewState("listBEPlantillaParametro") IsNot Nothing Then
                    Dim oBEPlantillaParametro As BEPlantillaParametro = New BEPlantillaParametro
                    oBEPlantillaParametro.IdPlantillaParametro = CInt(lblIdParametro.Text)
                    EliminarPlantillaParametro(oBEPlantillaParametro)
                End If
        End Select
    End Sub

    Protected Sub CargarPanelParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro)
        hdfIdParametroPlantilla.Value = oBEPlantillaParametro.IdPlantillaParametro
        txtDescripcion.Text = oBEPlantillaParametro.FuenteDescripcion
        ddlTipoParametro.SelectedValue = oBEPlantillaParametro.IdTipoParametro
        If oBEPlantillaParametro.IdTipoParametro = Plantilla.Parametro.Tipo.Estatico Then 'tipo fijo
            SetVisibleTipoParametro(True, True)
            txtEtiqueta.Text = oBEPlantillaParametro.Etiqueta
            txtValor.Text = oBEPlantillaParametro.Valor
        Else
            SetVisibleTipoParametro(False, True)
            txtCodigo.Text = oBEPlantillaParametro.FuenteCodigo
            txtEtiqueta.Text = oBEPlantillaParametro.FuenteEtiqueta
            txtDescripcion.Text = oBEPlantillaParametro.FuenteDescripcion
            ddlParametros.SelectedValue = oBEPlantillaParametro.IdPlantillaFuenteParametro
            txtCodigo.Enabled = False
            txtEtiqueta.Enabled = False
            txtDescripcion.Enabled = False
        End If
        btnAgregar.Visible = False
        btnActualizarParametro.Visible = True
    End Sub

    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        mpeNuevoParametro.Hide()
    End Sub

    Protected Sub ddlParametros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlParametros.SelectedIndexChanged
        Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = CType(ViewState("listBEPlantillaParametroFuente"), List(Of BEPlantillaParametro))
        Dim oBEPlantillaParametro As BEPlantillaParametro = listBEPlantillaParametro.First(Function(x) x.IdPlantillaFuenteParametro = ddlParametros.SelectedValue)
        txtCodigo.Text = oBEPlantillaParametro.FuenteCodigo
        txtEtiqueta.Text = oBEPlantillaParametro.FuenteEtiqueta
        txtDescripcion.Text = oBEPlantillaParametro.FuenteDescripcion
        mpeNuevoParametro.Show()
    End Sub

    Protected Sub SetVisibleTipoParametro(ByVal isStatic As Boolean, ByVal isEdit As Boolean)
        divParametro.Visible = Not isStatic
        divValor.Visible = isStatic
        txtCodigo.Enabled = False
        txtEtiqueta.Enabled = isStatic
        txtDescripcion.Enabled = False

        divTipoParametro.Visible = Not isEdit

        txtCodigo.Text = String.Empty
        txtEtiqueta.Text = String.Empty
        txtDescripcion.Text = String.Empty
        txtValor.Text = String.Empty
        lblSeccionesMsj.Text = String.Empty
    End Sub

    Protected Sub gvResultado_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvResultado.RowEditing
        e.NewEditIndex = -1
    End Sub

    Protected Sub ddlTipoParametro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoParametro.SelectedIndexChanged
        If (ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Estatico) Then 'tipo fijo
            SetVisibleTipoParametro(True, False)
        ElseIf ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Dinamico Then
            SetVisibleTipoParametro(False, False)
            ddlParametros_SelectedIndexChanged(Nothing, Nothing)
        End If
        pnlNuevoParametro.Visible = True
        mpeNuevoParametro.Show()
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Estatico
        ddlParametros_SelectedIndexChanged(Nothing, Nothing)
        SetVisibleTipoParametro(True, False)
        btnActualizarParametro.Visible = False
        btnAgregar.Visible = True
        mpeNuevoParametro.Show()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        mpeNuevoParametro.Hide()
    End Sub
    Protected Sub btnAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim oBEPlantillaParametro As New BEPlantillaParametro
        oBEPlantillaParametro.IdPlantilla = hdfIdPlantilla.Value
        oBEPlantillaParametro.IdPlantillaFuenteParametro = If(ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Dinamico, ddlParametros.SelectedValue, Nothing)
        oBEPlantillaParametro.IdTipoParametro = ddlTipoParametro.SelectedValue
        oBEPlantillaParametro.Etiqueta = If(ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Estatico, txtEtiqueta.Text, Nothing)
        oBEPlantillaParametro.Valor = If(ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Estatico, txtValor.Text, Nothing)

        oBEPlantillaParametro.FuenteCodigo = txtCodigo.Text
        oBEPlantillaParametro.FuenteDescripcion = txtDescripcion.Text
        oBEPlantillaParametro.FuenteEtiqueta = txtEtiqueta.Text

        Dim ctrPlantilla As New CPlantilla
        Dim IdPlantillaParametro As Integer = ctrPlantilla.RegistrarPlantillaParametros(oBEPlantillaParametro)
        If IdPlantillaParametro > 0 Then
            oBEPlantillaParametro.IdPlantillaParametro = IdPlantillaParametro
            Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = CType(ViewState("listBEPlantillaParametro"), List(Of BEPlantillaParametro))
            listBEPlantillaParametro.Add(oBEPlantillaParametro)
            ViewState("listBEPlantillaParametro") = listBEPlantillaParametro
            gvResultado.DataSource = listBEPlantillaParametro
            gvResultado.DataBind()
            lblMensajeParametros.Text = String.Empty
        Else
            lblMensajeParametros.Text = "El parametro ya se encuentra Registrado"
        End If
    End Sub
    Protected Sub btnActualizarParametro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarParametro.Click
        Dim oBEPlantillaParametro As New BEPlantillaParametro
        lblMensajeParametros.Text = String.Empty
        oBEPlantillaParametro.IdPlantillaParametro = hdfIdParametroPlantilla.Value
        oBEPlantillaParametro.IdPlantilla = hdfIdPlantilla.Value
        oBEPlantillaParametro.IdPlantillaFuenteParametro = If(ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Dinamico, ddlParametros.SelectedValue, Nothing)
        oBEPlantillaParametro.IdTipoParametro = ddlTipoParametro.SelectedValue
        oBEPlantillaParametro.Etiqueta = If(ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Estatico, txtEtiqueta.Text, Nothing)
        oBEPlantillaParametro.Valor = If(ddlTipoParametro.SelectedValue = Plantilla.Parametro.Tipo.Estatico, txtValor.Text, Nothing)

        oBEPlantillaParametro.FuenteCodigo = txtCodigo.Text
        oBEPlantillaParametro.FuenteDescripcion = txtDescripcion.Text
        oBEPlantillaParametro.FuenteEtiqueta = txtEtiqueta.Text

        Dim ctrPlantilla As New CPlantilla
        Dim IdPlantillaParametro As Integer = ctrPlantilla.ActualizarPlantillaParametro(oBEPlantillaParametro)
        If IdPlantillaParametro > 0 Then
            Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = CType(ViewState("listBEPlantillaParametro"), List(Of BEPlantillaParametro))
            Dim oBEPlantillaParametroTemp As BEPlantillaParametro = listBEPlantillaParametro.First(Function(x) x.IdPlantillaParametro = oBEPlantillaParametro.IdPlantillaParametro)
            listBEPlantillaParametro.Remove(oBEPlantillaParametroTemp)
            listBEPlantillaParametro.Add(oBEPlantillaParametro)
            ViewState("listBEPlantillaParametro") = listBEPlantillaParametro
            gvResultado.DataSource = listBEPlantillaParametro
            gvResultado.DataBind()
            lblMensajeParametros.Text = String.Empty
        Else
            lblMensajeParametros.Text = "El parametro ya se encuentra Registrado"
        End If
    End Sub
    Protected Sub EliminarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro)
        Dim ctrPlantilla As New CPlantilla
        ctrPlantilla.EliminarPlantillaParametro(oBEPlantillaParametro)
        Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = CType(ViewState("listBEPlantillaParametro"), List(Of BEPlantillaParametro))
        Dim oBEPlantillaParametroTemp As BEPlantillaParametro = listBEPlantillaParametro.First(Function(x) x.IdPlantillaParametro = oBEPlantillaParametro.IdPlantillaParametro)
        listBEPlantillaParametro.Remove(oBEPlantillaParametroTemp)
        ViewState("listBEPlantillaParametro") = listBEPlantillaParametro
        gvResultado.DataSource = listBEPlantillaParametro
        gvResultado.DataBind()
    End Sub

    Protected Sub gvResultado_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvResultado.RowDeleting

    End Sub

    Protected Sub habilitarActualizacion(ByVal habilitar As Boolean)
        divEstructuraHTML.Visible = habilitar
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Dim obServicio As BEServicio

        Dim ocntrlServicio As New CServicio
        Dim ocntrlPlantilla As New CPlantilla
        Dim rutaS3 As String
        obServicio = ocntrlServicio.ObtenerServicioPorID(ddlServicio.SelectedValue)
        lblSeccionesMsj.Text = ""

        If obServicio.PlantillaFormato = 53 Then
            btnActualizar.Visible = True
            blkSecciones.Visible = False
            cargarEstructuraHTML()
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: btnDiplayButton('block'); javascript: plantilla(); ", True)
        Else

            Using cntrlPlantilla As New CPlantilla()
                Dim obePlantilla As BEPlantilla = cntrlPlantilla.ConsultarPlantillaPorTipoYVariacionExiste(ddlServicio.SelectedValue, ddlTipoPlantilla.SelectedValue, "st")
                If (obePlantilla Is Nothing) Then
                    obePlantilla = New BEPlantilla()
                    obePlantilla.IdUsuarioActualizacion = UserInfo.IdUsuario
                    obePlantilla.IdServicio = CInt(ddlServicio.SelectedValue)
                    obePlantilla.IdPlantillaTipo = CInt(ddlTipoPlantilla.SelectedValue)
                    obePlantilla.CodigoPlantillaTipoVariacion = "st"
                    obePlantilla = cntrlPlantilla.GenerarPlantillaDesdePlantillaBase(obePlantilla)
                    'lblMsjEstructuraHTML.Text = "* La plantilla no existía ha sido creada a partir de la plantilla base de: " + obePlantilla.Descripcion
                End If
                txtPlantillaDesc.Text = obePlantilla.Descripcion
                hdfIdPlantilla.Value = obePlantilla.IdPlantilla
                rutaS3 = obePlantilla.FileS3

                Dim listBEPlantillaParametro As List(Of BEPlantillaParametro) = cntrlPlantilla.ConsultarPlantillaParametroPorIdPlantilla(obePlantilla.IdPlantilla)
                ViewState("listBEPlantillaParametro") = listBEPlantillaParametro
                gvResultado.DataSource = listBEPlantillaParametro
                gvResultado.DataBind()
                divParametrosEmail.Visible = If(listBEPlantillaParametro.Count > 0, True, False)

            End Using

            'Cargo plantilla desde S3
            Dim fileHtml = ObtenerPlantillaS3(rutaS3)

            If (fileHtml = "-1") Then
                lblMsjEstructuraHTML.Text = "No se pudo ubicar la plantilla en S3."
                hdfplantillaeExpresss.Value = ""
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: plantilla(); ", True)
                blkSecciones.Visible = False
                divParametrosEmail.Visible = False
                Exit Sub
            End If

            hdfplantillaeExpresss.Value = fileHtml
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: plantilla(); ", True)

            'Cargo Secciones

            If (ddlTipoPlantilla.SelectedValue = 67 Or ddlTipoPlantilla.SelectedValue = 68) Then
                blkSecciones.Visible = False
            Else
                blkSecciones.Visible = True
                txtSeccion1.Text = ""
                txtSeccion2.Text = ""
                txtSeccion3.Text = ""
                txtSeccion4.Text = ""
                Dim lstSecciones As List(Of BEPlantillaSeccion) = ocntrlPlantilla.ObtenerSeccionesPlantillaPorServicio(ddlServicio.SelectedValue, ddlTipoPlantilla.SelectedValue)

                If (lstSecciones.Count > 0) Then

                    For i As Integer = 0 To lstSecciones.Count - 1
                        If (lstSecciones(i).IdSeccion = 1) Then
                            txtSeccion1.Text = lstSecciones(i).Contenido
                        End If
                        If (lstSecciones(i).IdSeccion = 2) Then
                            txtSeccion2.Text = lstSecciones(i).Contenido
                        End If
                        If (lstSecciones(i).IdSeccion = 3) Then
                            txtSeccion3.Text = lstSecciones(i).Contenido
                        End If
                        If (lstSecciones(i).IdSeccion = 4) Then
                            txtSeccion4.Text = lstSecciones(i).Contenido
                        End If
                    Next

                End If
            End If

        End If

    End Sub

    Protected Sub btnGrabarSecciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabarSecciones.Click

        Dim lstSecciones As New List(Of BEPlantillaSeccion)
        Dim obeSeccion As New BEPlantillaSeccion

        obeSeccion.IdSeccion = 1
        obeSeccion.IdServicio = ddlServicio.SelectedValue
        obeSeccion.IdPlantillaFormato = ddlTipoPlantilla.SelectedValue
        obeSeccion.Contenido = txtSeccion1.Text
        lstSecciones.Add(obeSeccion)
        obeSeccion = New BEPlantillaSeccion

        obeSeccion.IdSeccion = 2
        obeSeccion.IdServicio = ddlServicio.SelectedValue
        obeSeccion.IdPlantillaFormato = ddlTipoPlantilla.SelectedValue
        obeSeccion.Contenido = txtSeccion2.Text
        lstSecciones.Add(obeSeccion)
        obeSeccion = New BEPlantillaSeccion

        obeSeccion.IdSeccion = 3
        obeSeccion.IdServicio = ddlServicio.SelectedValue
        obeSeccion.IdPlantillaFormato = ddlTipoPlantilla.SelectedValue
        obeSeccion.Contenido = txtSeccion3.Text
        lstSecciones.Add(obeSeccion)
        obeSeccion = New BEPlantillaSeccion

        obeSeccion.IdSeccion = 4
        obeSeccion.IdServicio = ddlServicio.SelectedValue
        obeSeccion.IdPlantillaFormato = ddlTipoPlantilla.SelectedValue
        obeSeccion.Contenido = txtSeccion4.Text
        lstSecciones.Add(obeSeccion)

        lblSeccionesMsj.Text = "Secciones grabadas correctamente"

        Dim ocntrlPlantilla As New CPlantilla
        Dim result As Integer = ocntrlPlantilla.ActualizarServicioSeccion(lstSecciones, UserInfo.IdUsuario)

    End Sub

    Protected Sub limpiarControles()
        lblMsjEstructuraHTML.Text = ""
        ddlEmpresa.SelectedIndex = 0
        ddlEmpresa_SelectedIndexChanged(Nothing, Nothing)
        ddlMoneda.SelectedIndex = 0
        ddlMoneda_SelectedIndexChanged(Nothing, Nothing)
        ddlTipoPlantilla.SelectedIndex = 0
        lblSeccionesMsj.Text = String.Empty
        txtPlantillaDesc.Text = String.Empty
        divParametrosEmail.Visible = False
        divEstructuraHTML.Visible = False
        lblMensajeParametros.Text = String.Empty
        blkSecciones.Visible = False
        hdfplantillaeExpresss.Value = ""
        ScriptManager.RegisterStartupScript(Me, GetType(Page), "Javascript", "javascript: plantilla(); javascript: btnDiplayButton('none'); ", True)
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        limpiarControles()
    End Sub


    Public Function ObtenerPlantillaS3(ByVal fileName As String) As String

        Dim amazonS3 = New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        If (amazonS3.FileExists(fileName)) Then
            Dim fileStream = amazonS3.ReadStreamFromFile(fileName)
            Dim bytes = fileStream.ToArray()

            Return System.Text.Encoding.UTF8.GetString(bytes)
        Else
            Return "-1"
        End If

    End Function
End Class

Friend Class PlantillaCorreo
    Public ReadOnly CodPlantillaTipo As String
    Public ReadOnly Plantilla As BEPlantilla
    Public ReadOnly S3AccessKey As String
    Public ReadOnly S3SecretAccessKey As String
    Public ReadOnly S3Region As String
    Public ReadOnly S3BucketName As String
    Public ReadOnly S3DirectoryPath As String
    Public ReadOnly Servicio As BEServicio
    Public ReadOnly NombreArchivoPlantillaBase As String
    Public ReadOnly RutaPlantillaBase As String
    Public ReadOnly RutaPlantillaDestino As String

    Sub New(codPlantillaTipo As String, plantilla As BEPlantilla, s3AccessKey As String, s3SecretAccessKey As String, s3Region As String, s3BucketName As String, s3DirectoryPath As String, servicio As BEServicio, rutaPlantillaBase As String, rutaPlantillaDestino As String, nombreArchivoPlantillaBase As String)
        Me.CodPlantillaTipo = codPlantillaTipo
        Me.Plantilla = plantilla
        Me.S3AccessKey = s3AccessKey
        Me.S3SecretAccessKey = s3SecretAccessKey
        Me.S3Region = s3Region
        Me.S3BucketName = s3BucketName
        Me.Servicio = servicio
        Me.RutaPlantillaBase = rutaPlantillaBase
        Me.RutaPlantillaDestino = rutaPlantillaDestino
        Me.NombreArchivoPlantillaBase = NombreArchivoPlantillaBase
        Me.S3DirectoryPath = s3DirectoryPath
    End Sub
End Class