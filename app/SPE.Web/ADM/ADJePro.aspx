<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ADM/ADJePro.aspx.vb" Inherits="ADM_ADJePro"
    Title="PagoEfectivo - Administrar Jefe Producto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Jefe de Producto"></asp:Literal></h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pnlDatosUsuario" runat="server">
            <h4>
                1. Datos del usuario</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> E-mail: </li>
                <li class="t2">
                    <asp:TextBox ID="txtEmailPrincipal" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailPrincipal"
                        ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailPrincipal"
                        ErrorMessage="Debe ingresar su email">*</asp:RequiredFieldValidator>
                    <asp:Label ID="lblIdJefeProducto" CssClass="Hidden" runat="server" Visible="False"></asp:Label>
                </li>
            </ul>
        </asp:Panel>
        <div style="clear: both">
        </div>
        <asp:Panel ID="pnlInformacionRegistro" runat="server">
            <h4>
                2. Informaci&oacute;n general</h4>
            <spe:ucUsuarioDatosComun ID="ucUsuarioDatosComun" runat="server"></spe:ucUsuarioDatosComun>
            <div id="fisEstado" runat="server" visible="false">
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEstado"
                            ErrorMessage="Seleccione el estado">*</asp:RequiredFieldValidator>
                    </li>
                </ul>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlTipoOrigenCancelacion">
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Origenes de Cancelación: </li>
                <li class="t2" style="height: auto; ">
                    <asp:CheckBoxList ID="cklOrigenesCancelacion" runat="server" DataTextField="Descripcion"
                        DataValueField="IdTipoOrigenCancelacion" class="normal checklistsm">
                    </asp:CheckBoxList>
                </li>
            </ul>
        </asp:Panel>
        <div style="clear: both">
        </div>
        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeValidacion" Style="padding-left: 200px"></asp:Label>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 200px" />
        <asp:Panel ID="pnlBotones" runat="server">
            <ul class="datos_cip2">
                <li class="complet">
                    <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" CssClass="input_azul3"
                        OnClientClick="return ConfirmMe();" />
                    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" CssClass="input_azul3"
                        OnClientClick="return ConfirmMe();" />
                    <asp:Button ID="btnCancelar" OnClientClick="return CancelMe();" runat="server" Text="Cancelar"
                        CssClass="input_azul4" />
                </li>
            </ul>
        </asp:Panel>
    </div>
</asp:Content>
