Imports system.Windows.Forms
Imports System.Collections.Generic

Partial Class ADM_ADJePro
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CJefeProducto)


    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Function CreateBEJefeProducto(ByVal obeJefeProducto As SPE.Entidades.BEJefeProducto) As SPE.Entidades.BEJefeProducto

        obeJefeProducto = New SPE.Entidades.BEJefeProducto
        
        obeJefeProducto.Email = Me.txtEmailPrincipal.Text
        obeJefeProducto.Password = Guid.NewGuid.ToString.Substring(1, 6)
        ucUsuarioDatosComun.CargarBEUsuario(obeJefeProducto)
        'obeJefeProducto.Nombres = Me.txtNombres.Text
        'obeJefeProducto.Apellidos = Me.txtApellidos.Text
        'obeJefeProducto.IdPais = Convert.ToInt32(Me.ddlPais.SelectedValue.ToString)
        'obeJefeProducto.IdDepartamento = Convert.ToInt32(Me.ddlDepartamento.SelectedValue.ToString)
        'obeJefeProducto.IdCiudad = Convert.ToInt32(Me.ddlCiudad.SelectedValue.ToString)
        'obeJefeProducto.IdTipoDocumento = Convert.ToInt32(Me.ddlTipoDocumento.SelectedValue.ToString)
        'If obeJefeProducto.IdTipoDocumento > 0 Then
        '    obeJefeProducto.NumeroDocumento = Me.txtNumeroDocumento.Text
        'Else
        '    obeJefeProducto.NumeroDocumento = ""
        'End If
        'obeJefeProducto.Direccion = Me.txtDireccion.Text
        'obeJefeProducto.Telefono = Me.txtTelefono.Text
        obeJefeProducto.IdEstado = ddlEstado.SelectedValue
        obeJefeProducto.ListaTipoOrigenCancelacion = CargarTipoOrigenCancelacion()

        Return obeJefeProducto

    End Function

    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CreateBEJefeProducto(be)
        obeJefeProducto.IdUsuarioCreacion = UserInfo.IdUsuario
        obeJefeProducto.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
        Return obeJefeProducto
    End Function
    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeJefeProducto As SPE.Entidades.BEJefeProducto = CreateBEJefeProducto(be)
        obeJefeProducto.IdJefeProducto = CInt(lblIdJefeProducto.Text)
        obeJefeProducto.IdUsuarioActualizacion = UserInfo.IdUsuario
        Return obeJefeProducto

    End Function

    'Public Overrides Function GetControllerObject() As SPE.Web.CJefeProducto
    '    Return New SPE.Web.CJefeProducto()
    'End Function
    'Public Overrides Function GetControllerObject() As SPE.Web.
    '    Return New SPE.Web.CJefeProducto()
    'End Function

    Public Overrides Function AllowToInsert() As Boolean

        If CklIsSelect() Then
            Return True
        Else
            lblMensaje.Text = "Debe seleccionar por lo menos un tipo de origen de cancelaci�n."
            Return False
        End If

    End Function

    Public Overrides Function AllowToUpdate() As Boolean

        If CklIsSelect() Then
            Return True
        Else
            lblMensaje.Text = "Debe seleccionar por lo menos un tipo de origen de cancelaci�n."
            Return False
        End If

    End Function
    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)

        Dim obejefeProducto As SPE.Entidades.BEJefeProducto = CType(be, SPE.Entidades.BEJefeProducto)
        Dim obeTipoOrigenCancelacion As SPE.Entidades.BETipoOrigenCancelacion
        Me.txtEmailPrincipal.Text = obejefeProducto.Email
        ucUsuarioDatosComun.RefrescarVista(obejefeProducto)
        'ucUsuarioDatosComun.RefrescarVista(obejefeProducto)
        'Me.txtNombres.Text = obejefeProducto.Nombres
        'Me.txtApellidos.Text = obejefeProducto.Apellidos
        'Me.ddlTipoDocumento.SelectedValue = obejefeProducto.IdTipoDocumento
        'Me.txtNumeroDocumento.Text = obejefeProducto.NumeroDocumento
        'Me.ddlPais.SelectedValue = obejefeProducto.IdPais
        'Me.SeleccionarDepartamento(obejefeProducto.IdDepartamento)
        'Me.ddlDepartamento.SelectedValue = obejefeProducto.IdDepartamento
        'Me.SeleccionarCiudad(obejefeProducto.IdCiudad)
        'Me.ddlCiudad.SelectedValue = obejefeProducto.IdCiudad
        'Me.txtTelefono.Text = obejefeProducto.Telefono
        'Me.txtDireccion.Text = obejefeProducto.Direccion
        Me.lblIdJefeProducto.Text = obejefeProducto.IdJefeProducto.ToString()
        ' Me.ddlEstado.SelectedValue = obejefeProducto.IdEstado
        InicializarddlEstado(obejefeProducto.IdEstado.ToString())
        For Each obeTipoOrigenCancelacion In obejefeProducto.ListaTipoOrigenCancelacion()
            cklOrigenesCancelacion.Items.FindByValue(obeTipoOrigenCancelacion.IdTipoOrigenCancelacion.ToString()).Selected = True
        Next

        'Desactivar controles
        Me.txtEmailPrincipal.ReadOnly = True
        Me.txtEmailPrincipal.CssClass = "normal"
        Me.fisEstado.Visible = True
        lblProceso.Text = "Actualizar Jefe de Producto"

    End Sub
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADJePro"
        e.URLPageCancelForEdit = "COJePro.aspx"
        e.URLPageCancelForInsert = "COJePro.aspx"
    End Sub

    'Sub SeleccionarPais(ByVal IdPais As Integer)
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
    '    If IdPais > 0 Then
    '        Me.ddlPais.SelectedValue = IdPais
    '    End If
    'End Sub
    'Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    Dim i As Integer = 0
    '    If ddlPais.SelectedValue = "" Then
    '        i = 0
    '    Else
    '        i = Me.ddlPais.SelectedValue
    '    End If
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '    If IdDepartamento > 0 Then
    '        Me.ddlDepartamento.SelectedValue = IdDepartamento
    '    Else
    '        Me.ddlDepartamento.SelectedIndex = 0
    '    End If
    'End Sub
    'Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    Dim i As Integer = 0
    '    If ddlDepartamento.SelectedValue = "" Then
    '        i = 0
    '    Else
    '        i = Me.ddlDepartamento.SelectedValue
    '    End If
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
    '    If IdCiudad > 0 Then
    '        Me.ddlCiudad.SelectedValue = IdCiudad
    '    Else
    '        Me.ddlCiudad.SelectedIndex = 0
    '    End If
    'End Sub
    'Private Sub CargarComboTipoDocumento()
    '    Dim objCParametro As New SPE.Web.CAdministrarParametro
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlTipoDocumento, objCParametro.ConsultarParametroPorCodigoGrupo("TDOC"), "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)

    'End Sub

    Private Sub CargaCombo()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESUS"), _
        "Descripcion", "Id")
    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub
    Private Sub InicializarddlEstado(ByVal idEstado As String)
        If idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Or idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            ddlEstado.Enabled = False
            ddlEstado.SelectedValue = idEstado
        Else
            ddlEstado.Enabled = True
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente)))
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado)))
            ddlEstado.SelectedValue = idEstado
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Page.SetFocus(txtEmailPrincipal)
            'SeleccionarPais(0)
            'SeleccionarDepartamento(0)
            'SeleccionarCiudad(0)
            'CargarComboTipoDocumento()

            HabilitarDiv(True)
            Me.pnlBotones.Enabled = True
            CargaOrigenCancelacion()
            CargaCombo()
            If (PageManager.IsInserting) Then
                ucUsuarioDatosComun.InicializarVista()
            End If
            ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo
        End If

    End Sub

    Public Overrides Sub OnAfterInsert()
        If (valueRegister = 1000) Then
            lblMensaje.Text = "Este correo est� siendo usado por otro usuario."

        Else

            lblMensaje.Text = "** Se registr� satisfactoriamente el jefe de producto: ''" + ucUsuarioDatosComun.Nombres.Text + " " + ucUsuarioDatosComun.Apellidos.Text + "''."
            HabilitarDiv(False)
            btnRegistrar.Enabled = False
            BtnCancel.Text = "Regresar"
            BtnCancel.OnClientClick = ""


        End If
    End Sub

    Public Overrides Sub OnAfterUpdate()

        HabilitarDiv(False)
        btnActualizar.Enabled = False
        BtnCancel.Text = "Regresar"
        BtnCancel.OnClientClick = ""
        lblMensaje.Text = "** Se actualiz� satisfactoriamente el jefe de producto: ''" + ucUsuarioDatosComun.Nombres.Text + " " + ucUsuarioDatosComun.Apellidos.Text + "''."

    End Sub

    Sub HabilitarDiv(ByVal valor As Boolean)

        pnlDatosUsuario.Enabled = valor
        pnlInformacionRegistro.Enabled = valor
        pnlTipoOrigenCancelacion.Enabled = valor

    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)

        MyBase.OnLoadComplete(e)
        Me.Title = "PagoEfectivo - " + lblProceso.Text

    End Sub


    'Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    SeleccionarDepartamento(0)
    '    SeleccionarCiudad(0)
    '    ScriptManager.SetFocus(ddlPais)
    'End Sub

    'Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    SeleccionarCiudad(0)
    '    ScriptManager.SetFocus(ddlDepartamento)
    'End Sub

    Protected Sub CargaOrigenCancelacion()

        Dim objOrigenCancelacion As New SPE.Web.CComun
        cklOrigenesCancelacion.DataSource = objOrigenCancelacion.ConsultarTipoOrigenCancelacion()
        cklOrigenesCancelacion.DataBind()

    End Sub

    Protected Function CargarTipoOrigenCancelacion() As List(Of SPE.Entidades.BETipoOrigenCancelacion)

        Dim Item As System.Web.UI.WebControls.ListItem
        Dim obeTipoOrigenCancelacion As SPE.Entidades.BETipoOrigenCancelacion
        Dim ListTipoOrigenCancelacion As New List(Of SPE.Entidades.BETipoOrigenCancelacion)

        For Each Item In cklOrigenesCancelacion.Items
            obeTipoOrigenCancelacion = New SPE.Entidades.BETipoOrigenCancelacion
            obeTipoOrigenCancelacion.IdTipoOrigenCancelacion = Item.Value
            If Item.Selected = True Then
                obeTipoOrigenCancelacion.IdEstado = 1
            Else
                obeTipoOrigenCancelacion.IdEstado = 0
            End If
            ListTipoOrigenCancelacion.Add(obeTipoOrigenCancelacion)
        Next
        Return ListTipoOrigenCancelacion
    End Function

    Protected Function CklIsSelect() As Boolean

        Dim Item As System.Web.UI.WebControls.ListItem
        For Each Item In cklOrigenesCancelacion.Items
            If Item.Selected Then Return True
        Next
        Return False
    End Function

    'Private Function ucUsuarioDatosComun() As Object
    '    Throw New NotImplementedException
    'End Function

End Class
