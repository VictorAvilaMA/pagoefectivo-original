<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="ADPlant.aspx.vb" Inherits="ADM_ADPlant" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="Page">
    <h1>
        <asp:ScriptManager id="ScriptManager" runat="server"></asp:ScriptManager>        
        <asp:Label id="lblTitulo" runat="server" Text="Actualizar Plantilla Base"></asp:Label>
        
    </h1>
     <fieldset  class="ContenedorEtiquetaTexto stripe">
        <div id="div11" class="EtiquetaTextoIzquierda" >
            <asp:Label id="lblServicio" runat="server" Text="Servicio:"  CssClass="Etiqueta" ></asp:Label> 
            <asp:DropDownList id="dropServicio" runat="server" CssClass="TextoComboExtraLargo" ></asp:DropDownList>         
            <asp:LinkButton ID="lnkRegresar" runat="server"><< Regresar</asp:LinkButton>
        </div>
     </fieldset> 	          
     <fieldset  class="ContenedorEtiquetaTexto stripe">
        <div id="div1" class="EtiquetaTextoIzquierda" >
            <asp:Label id="lblPlantillaTipo" runat="server" Text="Plantilla Tipo:"  CssClass="Etiqueta" ></asp:Label> 
            <asp:DropDownList id="dropPlantillaTipo" runat="server" CssClass="TextoComboExtraLargo" ></asp:DropDownList>         
        </div>
        <div id="div6" class="EtiquetaTextoIzquierda">
            <asp:Button ID="btnRefrescar" runat="server" Text="Refrescar"  CssClass="Boton" />
        </div>        
     </fieldset> 	          
    <div id="divInfGeneral" class="divContenedor">
      <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" > 
          Plantilla 
      </div>        
      <fieldset  class="ContenedorEtiquetaTexto stripe">
           <div id="div8" class="EtiquetaTextoIzquierda" >
                <asp:Label ID="lblIdPlantillaBase" CssClass="Etiqueta" runat="server" Text="ID:"></asp:Label>
                <asp:TextBox id="txtIdPlantillaBase" runat="server" CssClass="Texto SoloLectura" ReadOnly="true"></asp:TextBox> 
           </div>
      </fieldset>     
      <fieldset  class="ContenedorEtiquetaTexto stripe">
         <div id="div10" class="EtiquetaTextoIzquierda" >
            <asp:Label ID="lblDescripcion" CssClass="Etiqueta" runat="server" Text="Plantilla Desc.:"></asp:Label>
            <asp:TextBox ID="txtDescripcion"  TextMode="SingleLine"  CssClass="TextoLargo" runat="server" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvDescripcion" ControlToValidate="txtDescripcion" runat="server" Text="*" ErrorMessage="La descripci�n es requerida."></asp:RequiredFieldValidator>
         </div>
        <div id="div5" class="EtiquetaTextoIzquierda">
           <asp:Button ID="btnVerParametros" runat="server" Text="Parametros"  CssClass="Boton" />
        </div>         
      </fieldset>            
      <fieldset  class="ContenedorEtiquetaTexto stripe">
         <div id="div7" class="EtiquetaTextoIzquierda" >
            <asp:TextBox ID="txtPlantillaHtml"  TextMode="MultiLine" CssClass="TextoLargo" runat="server" Height="800px" Width="600px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvPlantillaHtml" ControlToValidate="txtPlantillaHtml" runat="server" Text="*" ErrorMessage="La plantilla html es requerida."></asp:RequiredFieldValidator>
         </div>
      </fieldset>      
      <fieldset  class="ContenedorEtiquetaTexto stripe">
         <div id="div3" class="EtiquetaTextoIzquierda" >
            <asp:Label ID="lblEstado" CssClass="Etiqueta" runat="server" Text="Estado:"></asp:Label>
            <asp:TextBox id="txtEstado" runat="server" CssClass="Texto SoloLectura" ReadOnly="true"></asp:TextBox> 
         </div>
      </fieldset>            
      <fieldset  class="ContenedorEtiquetaTexto stripe">
         <div id="div4" class="EtiquetaTextoIzquierda" >
            <asp:Label ID="lblFechaActualizacion" CssClass="Etiqueta" runat="server" Text="Fec. Act.:"></asp:Label>
            <asp:TextBox id="txtFechaActualizacion" runat="server" CssClass="Texto SoloLectura" ReadOnly="true"></asp:TextBox> 
         </div>
      </fieldset>                  
    </div>
  <fieldset id="Fieldset1" class="ContenedorBotonera" >
          <div id="div2" class="EtiquetaTextoIzquierda" >
            <asp:Label ID="lblCodigoAcceso" CssClass="Etiqueta" runat="server" Text="C�digo Acceso:"></asp:Label>
            <asp:TextBox ID="txtCodigoAcceso" CssClass="Texto" runat="server" MaxLength="250" ></asp:TextBox>
        </div> 
     <asp:Label ID="lblPlantillaMensaje" CssClass="MensajeTransaccion"  runat="server" Height="25px" style="padding-top: 0px"></asp:Label>
     <asp:Button ID="btnEditar" runat="server" Text="Editar" CssClass="Boton" />          
     <asp:Button ID="btnActualizar" runat="server" Text="Actualizar"  CssClass="Boton" OnClientClick="return ConfirmMe();" Visible="False" />
     <asp:Button ID="btnVistaPrevia" runat="server" Text="Vista Previa"  CssClass="Boton" />     
     <asp:Button ID="btnCancelar" OnClientClick="return CancelMe();" runat="server" Text="Cancelar"  CssClass="Boton" />     
  </fieldset>
    
</div>

<asp:Panel ID="pnlPopupPlantillaParametros" runat="server"  style="display:none;"  >
    <div id="divPlantilaParametros" class="divContenedor" style="width:600px">
          <div id="div9" class="divContenedorTitulo">
            <div style="float: left">Parametros de la Plantilla 
               <asp:Label ID="lblNombrePlantillaParametro" runat="server" Text=""></asp:Label>
            </div>
           <div style="float: right">
             <asp:ImageButton id="imgbtnRegresar"  ImageUrl="~/img/closeX.GIF"  CausesValidation="false"   runat="server"></asp:ImageButton> 
           </div>
          </div>
          <div>
              <asp:GridView id="gvResultado" runat="server" CssClass="grilla" Width="580px" AutoGenerateColumns="False">
                 <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/images/ok2.jpg" SelectText="Seleccionar" ShowSelectButton="True" HeaderText="Editar" ItemStyle-Width="20px" />
                    <asp:BoundField DataField="IdPlantillaParametroBase"  HeaderText="Id Param." ItemStyle-Width="10px"  />
                    <asp:BoundField DataField="TipoParametroDesc" HeaderText="Tipo Parametro" />
                    <asp:BoundField DataField="Etiqueta" HeaderText="Etiqueta" />
                    <asp:BoundField DataField="Valor" HeaderText="Valor" />
                 </Columns>
                 <HeaderStyle CssClass="cabecera" />
                 <EmptyDataTemplate>
                       No se encontraron registros.
                 </EmptyDataTemplate>
              </asp:GridView> 
         </div>
        <fieldset id="fsBotoneraD" class="ContenedorBotonera" style="width:auto"	>
          <asp:Button ID="btnAceptar" runat="server" OnClientClick="return ConfirmMe();" Text="Aceptar" CssClass="Boton" />
        </fieldset>
    </div>
</asp:Panel>

</asp:Content>

