<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ADM/ADRepr.aspx.vb" Inherits="ADM_ADRepr"
    Title="PagoEfectivo - Administrar Representante" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar  Representante"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pnlPage" runat="server">
            <asp:Panel ID="pnlDatosBasicos" runat="server" CssClass="inner-col-right">
                <h4>
                    1.Informaci&oacute;n general</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> E-mail:
                        <asp:Label ID="lblIdRepresentante" CssClass="Hidden" runat="server" Visible="False">
                        </asp:Label></li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" CssClass="normal" runat="server" MaxLength="100">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe ingresar un email."
                            ControlToValidate="txtEmail" ValidationGroup="ValidaControles">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="Ingrese un email v�lido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="ValidaControles">*</asp:RegularExpressionValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Contrase�a:</li>
                    <li class="t2">
                        <asp:Label ID="Label10" runat="server" Text="**************" CssClass="normal" Style="text-align: left"></asp:Label>
                        <asp:Label ID="lblEstado" CssClass="Hidden" runat="server" Visible="False"></asp:Label>
                    </li>
                </ul>
            </asp:Panel>
        </asp:Panel>
        <div style="clear: both">
        </div>
        <asp:Panel ID="pnlInfPersona" runat="server" CssClass="inner-col-right">
            <h4>
                2.Informaci&oacute;n del represen.</h4>
            <spe:ucUsuarioDatosComun ID="ucUsuarioDatosComun" runat="server"></spe:ucUsuarioDatosComun>
            <div id="fisEstado" runat="server" visible="false">
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> Contrase�a:
                        <asp:Label ID="Label2" CssClass="Hidden" runat="server" Visible="False">
                        </asp:Label></li>
                    <li class="t2">
                        <asp:Label ID="Label5" runat="server" Text="**************" CssClass="normal" Style="text-align: left"></asp:Label>
                        <asp:Label ID="Label6" CssClass="Hidden" runat="server" Visible="False"></asp:Label>
                    </li>
                </ul>
            </div>
        </asp:Panel>
        <div style="clear: both">
        </div>
        <asp:UpdatePanel ID="upInfEmpresas" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlInfEmpresa" runat="server" CssClass="inner-col-right" Style="left: 0px;
                    top: 0px">
                    <h4>
                        3.Informaci&oacute;n de la Empresa</h4>
                    <ul class="datos_cip5 t30">
                        <li class="t1"><span class="color">&gt;&gt;</span> Empresas :</li>
                        <li class="t3">
                            <asp:ListBox ID="lsEmpresas" runat="server" CssClass="lista" Style="margin: 0px;
                                text-align: left;" Rows="5"></asp:ListBox>
                            <asp:ImageButton ID="ibtnBuscarEmpresa" runat="server" 
                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                            ToolTip="Buscar Empresa" Style="padding: 10px; float: left" CausesValidation="false" />
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <%--<ul class="datos_cip2 t0">
                <li class="t1"><span class="color">>></span> Raz�n Social:</li>
                <li class="t2">
                    <asp:TextBox ID="txtRazonSocial" CssClass="normal" runat="server" ReadOnly="True">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Seleccione la Empresa."
                        ControlToValidate="txtRazonSocial" ValidationGroup="ValidaControles">*</asp:RequiredFieldValidator>
                </li>
                <li class="t1"><span class="color">>></span> R.U.C.:</li>
                <li class="t2">
                    <asp:TextBox ID="txtRUC" CssClass="normal" runat="server" ReadOnly="True">
                    </asp:TextBox>
                </li>
            </ul>--%>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ibtnBuscarEmpresa" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                    Style="padding-left: 200px; height: auto" />
                <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion" Style="height: auto;
                    font-weight: bold; padding-left: 210px; float: left"></asp:Label>
                <div style="clear: both">
                </div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" CssClass="input_azul3"
                            OnClientClick="return ConfirmMe();" ValidationGroup="ValidaControles" />
                        <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" CssClass="input_azul3"
                            OnClientClick="return ConfirmMe();" Visible="False" ValidationGroup="ValidaControles" />
                        <asp:Button ID="btnCancelar" OnClientClick="return CancelMe();" runat="server" Text="Cancelar"
                            CssClass="input_azul4" />
                        <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" CssClass="input_azul4" Visible="False" />
                    </li>
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="clear: both">
    </div>
    <asp:UpdatePanel ID="upnlListaEmprContrantes" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc1:ModalPopupExtender ID="mppEmpresaContrante" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="ImageButton1" PopupControlID="pnlPopupEmpresaContrante" TargetControlID="ImageButton2">
            </cc1:ModalPopupExtender>
            <asp:ImageButton ID="ImageButton1" ImageUrl="" CausesValidation="false" Visible="true"
                CssClass="Hidden" runat="server" Style="display: none"></asp:ImageButton>
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
            CssClass="Hidden"
                ToolTip="Buscar Empresa" Style="display: none" />
            <asp:Panel ID="pnlPopupEmpresaContrante" runat="server" Style="display: none; background: white;
                max-height: 500px; min-height: 0px" CssClass="conten_pasos3">
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar" 
                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                    CausesValidation="false" runat="server"></asp:ImageButton>
                </div>
                <h4>
                    Busqueda de Empresas C.</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Raz�n Social: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreComercial" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnBuscarEmpresaContratante" runat="server" Text="Buscar" CssClass="input_azul3"
                            CausesValidation="false"></asp:Button>
                        <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" CssClass="input_azul4"
                            CausesValidation="false" Visible="false"></asp:Button>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <div class="result" style="margin-top: 10px; overflow: auto; height: 300px">
                    <div id="div22" class="EtiquetaTextoIzquierda">
                        <asp:Label ID="lblNombreComercial" runat="server" Text="" CssClass="EtiquetaLargo"></asp:Label>
                    </div>
                    <div>
                        <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" Width="580px" AutoGenerateColumns="False"
                            OnRowCommand="gvResultado_RowCommand" PageSize="10">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIdEmpresaContratante" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdEmpresaContratante") %>'
                                            ToolTip='<%#DataBinder.Eval(Container.DataItem, "IdEmpresaContratante")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEmpresa" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Razon Social" SortExpression="RazonSocial">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRazonSocial" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RazonSocial") %>'
                                            ToolTip='<%#DataBinder.Eval(Container.DataItem, "RazonSocial")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="RUC">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRuc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RUC") %>'
                                            ToolTip='<%#DataBinder.Eval(Container.DataItem, "RUC")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="RUC" HeaderText="RUC"></asp:BoundField>--%>
                                <asp:BoundField DataField="Telefono" HeaderText="Telefono"></asp:BoundField>
                                <asp:BoundField DataField="Email" HeaderText="E-mail"></asp:BoundField>
                                <asp:TemplateField HeaderText="Seleccionar" HeaderStyle-Width="30" >
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                            ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/chk_emp.png" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:CommandField SelectImageUrl="~/Images_Buton/chk_emp.png" SelectText="Selecciionar"
                                    ButtonType="Image" ShowSelectButton="True" HeaderStyle-Width="30"></asp:CommandField>--%>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se encontraron registros. Asegurese que existan empresas contratantes disponibles.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="gvResultado"></asp:PostBackTrigger>
            <asp:PostBackTrigger ControlID="btnAceptar"></asp:PostBackTrigger>
            <%--<asp:AsyncPostBackTrigger ControlID="ibtnBuscarEmpresa" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="ibtnBuscarEmpresa" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
