Imports _3Dev.FW.Web.Security
Imports System.Collections.Generic
Imports SPE.Entidades

Partial Class ADM_ADRepr
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CRepresentante)

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property



    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Private Function CreateBERepresentante(ByVal obeRepresentante As SPE.Entidades.BERepresentante) As SPE.Entidades.BERepresentante

        obeRepresentante = New SPE.Entidades.BERepresentante
        obeRepresentante.Email = txtEmail.Text
        obeRepresentante.Password = Guid.NewGuid.ToString.Substring(1, 6)
        'obeRepresentante.Nombres = txtNombres.Text
        'obeRepresentante.Apellidos = txtApellidos.Text
        'obeRepresentante.Telefono = txtTelefono.Text
        'obeRepresentante.Direccion = txtDireccion.Text
        'obeRepresentante.IdTipoDocumento = ddlTipoDocumento.SelectedValue
        'obeRepresentante.NumeroDocumento = txtNroDocumento.Text
        'obeRepresentante.IdCiudad = CInt(ddlCiudad.SelectedValue)
        ucUsuarioDatosComun.CargarBEUsuario(obeRepresentante)
        obeRepresentante.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente
        Dim listIdsEmpresas As New List(Of String)
        For Each item As ListItem In lsEmpresas.Items
            listIdsEmpresas.Add(item.Value)
        Next
        obeRepresentante.EmIdsEmpresas = listIdsEmpresas
        Return obeRepresentante
    End Function

    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeRepresentante As SPE.Entidades.BERepresentante = CreateBERepresentante(be)
        obeRepresentante.IdUsuarioCreacion = UserInfo.IdUsuario
        obeRepresentante.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
        Return obeRepresentante
    End Function

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeRepresentante As SPE.Entidades.BERepresentante = CreateBERepresentante(be)
        obeRepresentante.IdRepresentante = CInt(lblIdRepresentante.Text)
        obeRepresentante.IdEstado = CInt(ddlEstado.SelectedValue)
        obeRepresentante.IdUsuarioActualizacion = UserInfo.IdUsuario
        Return obeRepresentante
    End Function

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CRepresentante
    'End Function

    Private Sub InicializarddlEstado(ByVal idEstado As String)
        If idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Or idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            ddlEstado.Enabled = False
            ddlEstado.SelectedValue = idEstado
        Else
            ddlEstado.Enabled = True
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente)))
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado)))
            ddlEstado.SelectedValue = idEstado
        End If
    End Sub
    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)

        Dim obeRepresentante As SPE.Entidades.BERepresentante = CType(be, SPE.Entidades.BERepresentante)
        txtEmail.Text = obeRepresentante.Email
        ucUsuarioDatosComun.RefrescarVista(obeRepresentante)
        ' ucUsuarioDatosComun.RefrescarVista(obeRepresentante)
        'txtNombres.Text = obeRepresentante.Nombres
        'txtApellidos.Text = obeRepresentante.Apellidos
        'txtTelefono.Text = obeRepresentante.Telefono
        'txtDireccion.Text = obeRepresentante.Direccion
        'ddlTipoDocumento.SelectedValue = obeRepresentante.IdTipoDocumento
        'txtNroDocumento.Text = obeRepresentante.NumeroDocumento
        'ddlPais.SelectedValue = obeRepresentante.IdPais
        'Me.SeleccionarDepartamento(obeRepresentante.IdDepartamento)
        'ddlDepartamento.SelectedValue = obeRepresentante.IdDepartamento
        'Me.SeleccionarCiudad(obeRepresentante.IdCiudad)
        'ddlCiudad.SelectedValue = obeRepresentante.IdCiudad

        'txtRazonSocial.Text = obeRepresentante.EmRazonSocial.ToString()
        'txtRUC.Text = obeRepresentante.EmRUC.ToString()
        '
        'Dim objCParametro As New SPE.Web.CAdministrarParametro
        'ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESUS") : ddlEstado.DataBind()
        '
        'ddlEstado.SelectedValue = obeRepresentante.IdEstado
        CargarComboEstados()
        InicializarddlEstado(obeRepresentante.IdEstado.ToString())
        lblIdRepresentante.Text = obeRepresentante.IdRepresentante
        lblEstado.Text = obeRepresentante.IdEstado

        'ACTIVAR CONTROLES
        btnActualizar.Visible = True
        btnRegistrar.Visible = False
        fisEstado.Visible = True
        lblProceso.Text = "Actualizar Representante"
        txtEmail.ReadOnly = True
        txtEmail.CssClass = "normal"

        Dim objCRepresentante As New SPE.Web.CRepresentante
        Dim obeUsuarioBase As New SPE.Entidades.BEUsuarioBase
        obeUsuarioBase.IdUsuario = obeRepresentante.IdUsuario
        Dim listEmpresas As List(Of BEEmpresaContratante) = objCRepresentante.ConsultarEmpresasPorIdUsuario(obeUsuarioBase)
        For Each item As BEEmpresaContratante In listEmpresas
            lsEmpresas.Items.Add(New ListItem(item.RazonSocial & " (RUC: " & item.RUC & ")", item.IdEmpresaContratante))
        Next
    End Sub

    Public Overrides Sub OnInsert()
        If lsEmpresas.Items.Count < 1 Then
            lblMensaje.ForeColor = Drawing.Color.Red
            Me.lblMensaje.Text = "No se registr� el representante. Debe seleccionar almenos una empresa."
        Else
            MyBase.OnInsert()
        End If
    End Sub
    Public Overrides Sub OnUpdate()
        If lsEmpresas.Items.Count < 1 Then
            lblMensaje.ForeColor = Drawing.Color.Red
            Me.lblMensaje.Text = "No se actualiz� el representante. Debe seleccionar almenos una empresa."
        Else
            MyBase.OnUpdate()
        End If
    End Sub
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADRepr"
    End Sub

    Public Overrides Sub OnAfterInsert()
        If lsEmpresas.Items.Count < 1 Then
            lblMensaje.ForeColor = Drawing.Color.Red
            Me.lblMensaje.Text = "No se registr� el representante. Debe seleccionar almenos una empresa."
        Else
            Select Case valueRegister
                Case SPE.EmsambladoComun.ParametrosSistema.Representante.ExisteRepresentante
                    lblMensaje.ForeColor = Drawing.Color.Red
                    Me.lblMensaje.Text = "No se registro el(la) representante, ya existe un(a) representante con el mismo correo asigando a alguna empresa."
                Case SPE.EmsambladoComun.ParametrosSistema.Representante.ExisteUsuario
                    ControlesMantenimiento(False)
                    btnCancelar.Text = "Regresar"
                    btnCancelar.Enabled = True
                    btnCancelar.OnClientClick = ""
                    lblMensaje.Text = "Se registr� satisfactoriamente el(la) representante: ''" + ucUsuarioDatosComun.Nombres.Text + " " + ucUsuarioDatosComun.Apellidos.Text + "''." & _
                        " * El usuario ya se encontraba registrado con otro rol, se actualizaron los datos con los proporcionados"
                    lblMensaje.ForeColor = Drawing.Color.DarkBlue
                Case Else
                    ControlesMantenimiento(False)
                    btnCancelar.Text = "Regresar"
                    btnCancelar.Enabled = True
                    btnCancelar.OnClientClick = ""
                    lblMensaje.Text = "Se registr� satisfactoriamente el(la) representante: ''" + ucUsuarioDatosComun.Nombres.Text + " " + ucUsuarioDatosComun.Apellidos.Text + "''." & _
                        " * Se le enviar� un correo al usuario con la contrase�a generada"
                    lblMensaje.ForeColor = Drawing.Color.DarkBlue
            End Select
        End If
        
    End Sub

    'Private Sub CargarCombosUbigeo(ByVal opcionCombo As String)

    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    Select Case opcionCombo
    '        Case "Pais"
    '            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '        Case "Departamento"
    '            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(ddlPais.SelectedValue), _
    '            "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '        Case "Ciudad"
    '            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(ddlDepartamento.SelectedValue), _
    '            "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '    End Select
    'End Sub

    'Sub SeleccionarPais(ByVal IdPais As Integer)
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '    If IdPais > 0 Then
    '        Me.ddlPais.SelectedValue = IdPais
    '    End If
    'End Sub

    'Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    Dim i As Integer = 0
    '    If ddlPais.SelectedValue = "" Then
    '        i = 0
    '    Else
    '        i = Me.ddlPais.SelectedValue
    '    End If
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), _
    '            "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '    If IdDepartamento > 0 Then
    '        Me.ddlDepartamento.SelectedValue = IdDepartamento
    '    Else
    '        Me.ddlDepartamento.SelectedIndex = 0
    '    End If
    'End Sub

    'Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
    '    Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
    '    Dim i As Integer = 0
    '    If ddlDepartamento.SelectedValue = "" Then
    '        i = 0
    '    Else
    '        i = Me.ddlDepartamento.SelectedValue
    '    End If
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), _
    '            "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    '    If IdCiudad > 0 Then
    '        Me.ddlCiudad.SelectedValue = IdCiudad
    '    Else
    '        Me.ddlCiudad.SelectedIndex = 0
    '    End If
    'End Sub

    'Private Sub CargarComboParametro()

    '    Dim objCParametro As New SPE.Web.CAdministrarParametro()
    '    _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoDocumento, objCParametro.ConsultarParametroPorCodigoGrupo("TDOC"), _
    '            "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)

    'End Sub

    'Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPais.SelectedIndexChanged
    '    SeleccionarDepartamento(0)
    '    SeleccionarCiudad(0)
    '    ScriptManager.SetFocus(ddlPais)
    'End Sub

    'Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartamento.SelectedIndexChanged
    '    SeleccionarCiudad(0)
    '    ScriptManager.SetFocus(ddlDepartamento)
    'End Sub
    Private Sub CargarComboEstados()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESUS"), _
        "Descripcion", "Id")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            'SeleccionarPais(0)
            'SeleccionarDepartamento(0)
            'SeleccionarCiudad(0)
            'CargarComboParametro()
            'Page.SetFocus(txtEmail)
            OcultarDDLs(True)
            If (PageManager.IsInserting) Then
                CargarComboEstados()
                ucUsuarioDatosComun.InicializarVista()
            End If
        End If
    End Sub
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Property ocRepresentante() As SPE.Web.CRepresentante
        Get
            Return CType(ControllerObject, SPE.Web.CRepresentante)
        End Get
        Set(ByVal value As SPE.Web.CRepresentante)

        End Set
    End Property

    Public Sub BuscarRepresentantes()
        Dim ocRepresentante As New SPE.Web.CRepresentante()
        Dim obeEmpresa As New SPE.Entidades.BEEmpresaContratante
        obeEmpresa.RazonSocial = txtNombreComercial.Text
        'SPE.Web.Util.UtilPopPupSearchGridView.VisualizarGridView(gvResultado, ocRepresentante.GetListByParameters("", obeEmpresa), Nothing)
        Dim listaEmpresas As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        listaEmpresas = ocRepresentante.GetListByParameters("", obeEmpresa)
        btnAceptar.Visible = listaEmpresas.Count > 0
        gvResultado.DataSource = listaEmpresas
        gvResultado.DataBind()
        OcultarDDLs(True)
        CheckedListaEmpresas()
    End Sub

    Protected Sub btnBuscarEmpresaContratante_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarEmpresaContratante.Click
        BuscarRepresentantes()
        mppEmpresaContrante.Show()
    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim lblRazonSocial As Label = DirectCast(gvResultado.Rows(e.CommandArgument).FindControl("lblRazonSocial"), Label)
        Dim lblRuc As Label = DirectCast(gvResultado.Rows(e.CommandArgument).FindControl("lblRuc"), Label)
        Dim lblIdEmpresaContratante As Label = DirectCast(gvResultado.Rows(e.CommandArgument).FindControl("lblIdEmpresaContratante"), Label)
        lsEmpresas.Items.Add(New ListItem(lblRazonSocial.Text & " (RUC: " & lblRuc.Text & ")", lblIdEmpresaContratante.Text))
        OcultarDDLs(True)
    End Sub

    Public Overrides Sub OnAfterUpdate()
        ControlesMantenimiento(False)
        btnCancelar.Text = "Regresar"
        btnCancelar.Enabled = True
        btnCancelar.OnClientClick = ""
        lblMensaje.Text = "Se actualiz� satisfactoriamente el(la) representante: ''" + ucUsuarioDatosComun.Nombres.Text + " " + ucUsuarioDatosComun.Apellidos.Text + "''."
        lblMensaje.ForeColor = Drawing.Color.DarkBlue
    End Sub

    Private Sub ControlesMantenimiento(ByVal Habilitar As Boolean)
        pnlDatosBasicos.Enabled = Habilitar
        pnlInfEmpresa.Enabled = Habilitar
        pnlInfPersona.Enabled = Habilitar
        btnRegistrar.Enabled = Habilitar
        btnActualizar.Enabled = Habilitar
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Me.Title = "PagoEfectivo - " + lblProceso.Text
    End Sub
    Private Sub OcultarDDLs(ByVal flag As Boolean)
        ucUsuarioDatosComun.Ciudad.Visible = flag
        ucUsuarioDatosComun.Pais.Visible = flag
        ucUsuarioDatosComun.Departamento.Visible = flag
    End Sub
    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        OcultarDDLs(True)
        mppEmpresaContrante.Hide()
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("~/ADM/CORepr.aspx")
    End Sub
    Public Overrides Sub EnabledMaintenanceControls(ByVal bool As Boolean)
        btnCancelar.Enabled = True
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As System.EventArgs) Handles btnAceptar.Click
        Dim listIdEmpresa As New List(Of String)
        For Each item As GridViewRow In gvResultado.Rows
            Dim lblRazonSocial As Label = DirectCast(item.FindControl("lblRazonSocial"), Label)
            Dim lblRuc As Label = DirectCast(item.FindControl("lblRuc"), Label)
            Dim lblIdEmpresaContratante As Label = DirectCast(item.FindControl("lblIdEmpresaContratante"), Label)
            If DirectCast(item.FindControl("chkEmpresa"), CheckBox).Checked Then
                listaEmpresasAdd(lblIdEmpresaContratante.Text, lblRazonSocial.Text, lblRuc.Text)
            Else
                listaEmpresasRemove(lblIdEmpresaContratante.Text, lblRazonSocial.Text, lblRuc.Text)
            End If
        Next
        OcultarDDLs(True)
        'mppEmpresaContrante.Hide()
    End Sub

    Protected Sub ibtnBuscarEmpresa_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ibtnBuscarEmpresa.Click
        CheckedListaEmpresas()
        mppEmpresaContrante.Show()
    End Sub

    Private Sub listaEmpresasRemove(ByVal idEmpresa As Integer, ByVal RazonSocial As String, ByVal Ruc As String)
        If lsEmpresas.Items.FindByValue(idEmpresa) IsNot Nothing Then
            lsEmpresas.Items.Remove(lsEmpresas.Items.FindByValue(idEmpresa))
        End If
    End Sub
    Private Sub listaEmpresasAdd(ByVal idEmpresa As Integer, ByVal RazonSocial As String, ByVal Ruc As String)
        If lsEmpresas.Items.FindByValue(idEmpresa) Is Nothing Then
            lsEmpresas.Items.Add(New ListItem(RazonSocial & " (RUC: " & Ruc & ")", idEmpresa))
        End If
    End Sub
    Private Sub CheckedListaEmpresas()
        For Each item As GridViewRow In gvResultado.Rows
            Dim lblRazonSocial As Label = DirectCast(item.FindControl("lblRazonSocial"), Label)
            Dim lblRuc As Label = DirectCast(item.FindControl("lblRuc"), Label)
            Dim lblIdEmpresaContratante As Label = DirectCast(item.FindControl("lblIdEmpresaContratante"), Label)
            If lsEmpresas.Items.FindByValue(lblIdEmpresaContratante.Text) IsNot Nothing Then
                DirectCast(item.FindControl("chkEmpresa"), CheckBox).Checked = True
            Else
                DirectCast(item.FindControl("chkEmpresa"), CheckBox).Checked = False
            End If
        Next
    End Sub
End Class
