<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADServ.aspx.vb" Inherits="ADM_ADServ" Title="PagoEfectivo - Administrar Servicio"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UC/UCAudit.ascx" TagName="UCAudit" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <style type="text/css">
        ul.datos_cip2 li.t3 span
        {
            height: 24px;
        }
        .input_azul4
        {
        }
    </style>
    <script type="text/javascript">

        function soloLetras(e) {

            document.getElementById('<%=lblUrlFormulario.ClientID%>').innerHTML = '';

            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = "abcdefghijklmnopqrstuvwxyz1234567890-_";
            especiales = [8];

            tecla_especial = false;
            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial)
                return false;
        }

        function onKeyDecimal(e, thix) {
            var keynum = window.event ? window.event.keyCode : e.which;

            var Decimales = 2;

            if (document.getElementById(thix.id).value.indexOf('.') != -1 && keynum == 46)
                return false;
            if (document.getElementById(thix.id).value.length == 0 && keynum == 46)
                return false;

            if ((keynum == 8 || keynum == 48 || keynum == 46))
                return true;
            if (keynum <= 47 || keynum >= 58) return false;

            if (document.getElementById(thix.id).value.indexOf('.') != -1) {
                var valor = document.getElementById(thix.id).value;
                if (valor.substring(valor.indexOf('.')).length == Decimales + 1) return false;
            }

            return /\d/.test(String.fromCharCode(keynum));
        }
    </script>
    <asp:Label ID="lblIdServicio" runat="server" Style="display: none; width: auto;"
        Text=""></asp:Label>
    <asp:UpdatePanel ID="upHeader" runat="server">
        <ContentTemplate>
            <h2>
                <asp:Literal ID="lblTitle" runat="server" Text="Registrar Servicios"></asp:Literal></h2>
            <asp:LinkButton ID="lnkShowAudit" CssClass="der lnkAudit" CausesValidation="false"
                Style="float: right" runat="server">[Mostar Auditor�a]</asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="Page" class="conten_pasos3">
        <h4>
            1. Informaci&oacute;n general</h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="UpdatePanelDatos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
                <ContentTemplate>
                    <li class="t1"><span class="color">>></span> Empresa Contratante:
                        <asp:Label ID="lblIdEmpresaContrante" Visible="false" runat="server" Text=""></asp:Label>
                    </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmpresaContrante" ReadOnly="True" CssClass="normal" runat="server">
                        </asp:TextBox>
                        <asp:ImageButton ID="ibtnBuscarEmpresa" CausesValidation="false" runat="server" CssClass="lupa png_bg"
                            Style="float: left; margin: 10px" ToolTip="Consultar Empresa" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/lupa.png" %>' />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtEmpresaContrante"
                            runat="server" Text="*" ValidationGroup="GrupoValidacion" ErrorMessage="La empresa contratante es requerida.">
                        </asp:RequiredFieldValidator>
                    </li>
                    <cc1:ModalPopupExtender ID="mppEmpresaContrante" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="imgbtnRegresarHidden" X="200" Y="200" PopupControlID="pnlPopupEmpresaContrante"
                        TargetControlID="ibtnBuscarEmpresa">
                    </cc1:ModalPopupExtender>
                    <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                        Style="display: none;" CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="ibtnBuscarEmpresa" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <li class="t1"><span class="color">>></span> C&oacute;digo</li>
            <li class="t2">
                <asp:TextBox ID="txtCodigo" CssClass="normal" MaxLength="3" runat="server">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCodigo" ControlToValidate="txtCodigo" runat="server"
                    Text="*" ValidationGroup="GrupoValidacion" ErrorMessage="El c�digo es requerido.">
                </asp:RequiredFieldValidator>
            </li>
            <li class="t1"><span class="color">>></span> Tiempo Expiraci&oacute;n (horas):</li>
            <li class="t2" style="width: 400px">
                <asp:TextBox ID="txtTiempoExpiracion" CssClass="corta" runat="server" MaxLength="6">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTiempoExpiracion" ControlToValidate="txtTiempoExpiracion"
                    runat="server" ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="El tiempo de expiraci�n es requerido.">
                </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftxtTiempoExpiracion" runat="server" FilterType="Custom,Numbers"
                    TargetControlID="txtTiempoExpiracion" ValidChars=".">
                </cc1:FilteredTextBoxExtender>
            </li>
            <li class="t2" style="width: 400px; padding-left: 200px; height: auto">
                <asp:Label CssClass="msje" ID="lblTiempoExpiracionMsg" runat="server" Text="(Si el tiempo de expiraci�n es cero, las O.P del servicio no expirar�n.)"
                    Style="color: Red; width: auto"></asp:Label>
            </li>
            <li class="t1"><span class="color">>></span>Nombre:</li>
            <li class="t2">
                <asp:TextBox ID="txtNombre" CssClass="normal" MaxLength="100" runat="server">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNombre" ControlToValidate="txtNombre" runat="server"
                    Text="*" ValidationGroup="GrupoValidacion" ErrorMessage="El nombre es requerido.">
                </asp:RequiredFieldValidator>
            </li>
            <li class="t1"><span class="color">>></span>Direc. Web:</li>
            <li class="t2">
                <asp:TextBox ID="txtUrl" CssClass="normal" runat="server" MaxLength="250">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvUrl" ControlToValidate="txtUrl" runat="server"
                    Text="*" ValidationGroup="GrupoValidacion" ErrorMessage="La direcci�n del servicio es requerida.">
                </asp:RequiredFieldValidator>
            </li>
            <li class="t1"><span class="color">>></span>Estado:</li>
            <li class="t2">
                <asp:DropDownList ID="dropEstado" CssClass="neu" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvDropEstado" ControlToValidate="dropEstado" runat="server"
                    ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="El estado es requerido.">
                </asp:RequiredFieldValidator>
            </li>
            <li class="t1"><span class="color">>></span>Versi&oacute;n:</li>
            <li class="t2">
                <asp:DropDownList ID="dropVersion" CssClass="neu" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvVersion" ControlToValidate="dropVersion" runat="server"
                    ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="La version es requerida.">
                </asp:RequiredFieldValidator>
            </li>
            <li class="t1"><span class="color">>></span>Adjuntar Imagen:</li>
            <li class="t2">
                <asp:FileUpload ID="ImagenFile" runat="server" Width="229px" />
            </li>
            <li class="t2" style="padding-left: 200px; height: auto">
                <p class="msjenomargin" style="color: Red">
                    (Extensiones v&aacute;lidas: .gif, .jpg, .jpeg, .png; Tama�o permitido: 225x90)</p>
            </li>
            <li class="t2" style="padding-left: 200px; height: auto">
                <%--nuevo control--%>
                <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/images/ok2.jpg" SelectText="Seleccionar"
                                    ShowSelectButton="True" />--%>
                <%--nuevo control--%>
                <asp:Image ID="Image1" runat="server" />
            </li>
            <li class="t1"><span class="color">>></span>�Pr�ximo afiliado?</li>
            <li class="t2">
                <asp:CheckBox ID="chkProximoAfiliado" runat="server" />
            </li>
            <li class="t1"><span class="color">>></span>�Visible en portada?</li>
            <li class="t2">
                <asp:CheckBox ID="chkVisibleEnPortada" runat="server" />
            </li>


           <%-- agregado PEEEXPRESS--%>
            <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/images/ok2.jpg" SelectText="Seleccionar"
                                    ShowSelectButton="True" />--%>
              <li class="t1"><span class="color">>></span>Plantilla:</li>
            <li class="t2">
                <asp:DropDownList ID="ddlTipoPlantilla" CssClass="neu" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="dropEstado" runat="server"
                    ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="El estado es requerido.">
                </asp:RequiredFieldValidator>
            </li>



           <%--oculto--%>
           <li class="t1" style="display: none"><span class="color">>></span>Adjuntar Imagen:</li>
           <li class="t2" style="width: 400px; display: none">
               <asp:FileUpload ID="fupExpressLogo" runat="server" Width="229px" />
           </li>
           <li class="t2" style="padding-left: 200px; height: auto; display: none">
               <asp:Image ID="ImgExpressLogo" runat="server" Height="16px" />
           </li>
           <li class="t2" style="padding-left: 200px; height: auto; display: none">
               <p class="msjenomargin" style="color: Red">
                   (Extensiones v&aacute;lidas: .png;Tama�o permitido: 300x148px)</p>
           </li>
           <%--oculto--%>

            <%-- agregado PEEEXPRESS--%>
			
        </ul>
        <fieldset id="fisUpload" class="ContenedorEtiquetaTexto" runat="server" visible="false">
            <div id="div9" class="EtiquetaTextoIzquierda">
                <asp:Label ID="lblUploadCarga" runat="server" Text="" Style="color: Red"></asp:Label>
            </div>
        </fieldset>
        <asp:UpdatePanel ID="upAudit" runat="server">
            <ContentTemplate>
                <uc1:UCAudit ID="ucAudit" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="cont_cel2">
            <h6>
                2. Or�genes de cancelaci&oacute;n</h6>
            <ul class="datos_cip2">
                <!--<li class="t3" style="width: auto; height: auto;">-->
                    <asp:CheckBoxList ID="cklOrigenesCancelacion" class="no_check" runat="server" DataTextField="Descripcion"
                        DataValueField="IdTipoOrigenCancelacion">
                    </asp:CheckBoxList>
                <!--</li>-->
            </ul>
        </div>
        <div style="clear: both">
        </div>
        <div class="cont_cel2" style="width: 740px">
            <h6>
                3. Configuraci&oacute;n integraci&oacute;n</h6>
            <asp:Label ID="Label2" CssClass="msje" runat="server" Text="(Los cambios en esta secci�n se aplicar�n al hacer click en el boton Actualizar o Registrar.)"
                Style="color: Red"></asp:Label>
            <asp:UpdatePanel ID="updTipoIntegracion" runat="server">
                <ContentTemplate>
                    <ul class="datos_cip2" style="width: 800px">
                        <li class="t1"><span class="color">>></span>Clave API:</li>
                        <li class="t2" style="width: 600px">
                            <asp:TextBox ID="txtClaveAPI" CssClass="normal" disabled="disabled" ReadOnly="true"
                                runat="server" MaxLength="250"></asp:TextBox>
                            <asp:Button ID="btnGenerarClaveAPI" OnClientClick="return ConfirmaGenClaveAPI();"
                                runat="server" Text="Gen. API" CssClass="input_azul4" OnClick="btnGenerarClaveAPI_Click" />
                        </li>
                        <li class="t1"><span class="color">>></span>Clave Secreta:</li>
                        <li class="t2" style="width: 600px">
                            <asp:TextBox ID="txtClaveSecreta" CssClass="normal izq" disabled="disabled" ReadOnly="true"
                                runat="server" MaxLength="250"></asp:TextBox>
                            <asp:Button ID="btnGenerarClaveSecreta" OnClientClick="return ConfirmaGenClaveSecreta();"
                                runat="server" Text="Re Generar" CssClass="input_azul4" OnClick="btnGenerarClaveSecreta_Click" />
                        </li>
                    </ul>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span>Usuarios An�nimos:</li>
                        <li class="t2">
                            <asp:CheckBox ID="chkUsuarioAnonimo" runat="server" />
                        </li>
                        <li class="t1"><span class="color">>></span>Extorno �nico:</li>
                        <li class="t2">S�<asp:RadioButton ID="rbExtornoUnicoSi" runat="server" AutoPostBack="true" />
                            &nbsp;&nbsp;&nbsp; No<asp:RadioButton ID="rbExtornoUnicoNo" runat="server" AutoPostBack="true" />
                        </li>
                        <li class="t1"><span class="color">>></span>Tipo Integraci�n:</li>
                        <li class="t2">
                            <asp:DropDownList ID="dropTipoIntegracion" runat="server" CausesValidation="false"
                                CssClass="clsform1">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">>></span>Canal de Ventas:</li>
                        <li class="t2">
                            <asp:DropDownList ID="dropCanalVenta" runat="server" CausesValidation="false" CssClass="clsform1"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">>></span>Web Services:</li>
                        <li class="t2">
                            <asp:DropDownList ID="dropWebService" runat="server" CausesValidation="false" CssClass="clsform1">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">>></span>Ip:</li>
                        <li class="t2" style="width: 400px; height: 105px;">
                            <asp:TextBox ID="txtIP" CssClass="corta" runat="server" MaxLength="150" TextMode="MultiLine"
                                Style="width: 400px; height: 100px;">
                            </asp:TextBox>
                        </li>
                        <asp:Label ID="lblMensajeIntegracion" runat="server" CssClass="MensajeValidacion"></asp:Label>
                    </ul>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnGenerarClaveAPI" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnGenerarClaveSecreta" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="cont_cel2">
            <h6>
                4. Configuraci&oacute;n de env&iacute;os de correos</h6>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Env&iacute;ar al crear
                            usuario:</li>
                        <li class="t2" style="width: 200px;">
                            <asp:CheckBox ID="chkNotifCrearUsuario" runat="server" />
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Env&iacute;ar al generar
                            un CIP:</li>
                        <li class="t2" style="width: 200px;">
                            <asp:CheckBox ID="chkNotifGenerarCIP" runat="server" />
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Env&iacute;ar al pagar
                            un CIP:</li>
                        <li class="t2" style="width: 200px;">
                            <asp:CheckBox ID="chkNotifPagarCIP" runat="server" />
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Env&iacute;ar copias
                            al Admin:</li>
                        <li class="t2" style="width: 200px;">
                            <asp:CheckBox ID="chkNotifCopiaAdmin" runat="server" />
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Enviar antes expiraci�n
                            CIP:</li>
                        <li class="t2" style="width: 350px;">
                            <asp:CheckBox ID="chkNotifExpiracionCIP" runat="server" AutoPostBack="True" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="txtTiempoAvisoExpiracion" CssClass="cortaexpiracion" runat="server"
                                MaxLength="4" Enabled="False"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTiempoAvisoExpiracion" CssClass="validacion" ControlToValidate="txtTiempoAvisoExpiracion"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="El tiempo de aviso de expiraci�n es requerido."
                                Enabled="False"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="ftxtTiempoAvisoExpiracion" runat="server" FilterType="Custom,Numbers"
                                Enabled="False" TargetControlID="txtTiempoAvisoExpiracion">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="regExpTiempoAvisoExpiracion" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtTiempoAvisoExpiracion" CssClass="validacion" Text="*" ErrorMessage="El tiempo de aviso de expiraci�n debe ser mayor a cero"
                                Enabled="False"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px"><span class="color">>></span>Enviar al generar un
                            CIP - Cobros:</li>
                        <li class="t2" style="width: 350px">
                            <asp:CheckBox ID="chkEnviarGenerarCipCobros" runat="server" />
                        </li>
                        <li class="t1" style="width: 250px"><span class="color">>></span>Enviar antes de expiraci�n
                            CIP - Cobros:</li>
                        <li class="t2" style="width: 350px">
                            <asp:CheckBox ID="chkEnviarExpiracionCipCobros" runat="server" AutoPostBack="True" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Horas
                            aviso expiraci�n CIP&nbsp;
                            <asp:TextBox ID="txtAvisoExpiracionCobranza" CssClass="cortaexpiracion" runat="server"
                                MaxLength="4" Enabled="False"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvAvisoExpiracionCobranza" CssClass="validacion"
                                ControlToValidate="txtAvisoExpiracionCobranza" runat="server" ValidationGroup="GrupoValidacion"
                                Text="*" 
                                ErrorMessage="Enviar antes de expiraci�n CIP - Cobros es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteAvisoExpiracionCobranza" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtAvisoExpiracionCobranza">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revAvisoExpiracionCobranza" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtAvisoExpiracionCobranza" CssClass="validacion" Text="*"
                                
                                ErrorMessage="Enviar antes de expiraci�n CIP - Cobros debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                    </ul>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="chkNotifExpiracionCIP" EventName="CheckedChanged">
                    </asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="chkEnviarExpiracionCipCobros" EventName="CheckedChanged">
                    </asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="cont_cel2">
            <h6>
                5. Configuraci&oacute;n notificaci&oacute;n</h6>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span>Tipo notificaci�n:</li>
                <li class="t2">
                    <asp:DropDownList ID="ddlTipoNotificacion" runat="server" CausesValidation="false"
                        CssClass="neu" AutoPostBack="true">
                    </asp:DropDownList>
                </li>
                <asp:UpdatePanel ID="updTipoNotificacion" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblMensajeNotificacion" runat="server" CssClass="MensajeValidacion"></asp:Label>
                        <asp:Panel ID="pnlTipoNotificacion" runat="server" Visible="false">
                            <li class="t1"><span class="color">>></span>URL FTP:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtUrlFTP" CssClass="TextoLargo" runat="server" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvUrlFTP" ControlToValidate="txtUrlFTP" runat="server"
                                    ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="La direcci�n del servidor FTP es requerida."></asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">>></span>Usuario:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtUsuarioFTP" CssClass="TextoLargo" runat="server" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtUsuarioFTP"
                                    runat="server" ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="El usuario para el servidor FTP es requerido."></asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">>></span>Password:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtPasswordFTP" CssClass="TextoLargo" runat="server" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtPasswordFTP"
                                    runat="server" ValidationGroup="GrupoValidacion" Text="*" ErrorMessage="El password para el servidor FTP es requerido."></asp:RequiredFieldValidator>
                            </li>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlTipoNotificacion" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <li class="t1"><span class="color">>></span>UrlNotifiacion:</li>
                <li class="t2">
                    <asp:TextBox ID="txtUrlNotificacion" CssClass="normal" runat="server" MaxLength="300"
                        Width="90%" Text=""></asp:TextBox>
                </li>
                <li class="t1"><span class="color">>></span>UrlRedireccionamiento:</li>
                <li class="t2">
                    <asp:TextBox ID="txtUrlRedireccionamiento" CssClass="normal" runat="server" MaxLength="300"
                        Width="90%" Text=""></asp:TextBox>
                </li>
            </ul>
        </div>
        <div id="Div1" class="cont_cel2" runat="server" visible="true">
            <h6>
                6. Configuraci&oacute;n de encriptaci&oacute;n</h6>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span>Ruta Clave P&uacute;blica:</li>
                <li class="t2">
                    <asp:TextBox ID="txtRutaClavePub" CssClass="normal" runat="server" MaxLength="300"
                        Width="90%" Text=""></asp:TextBox>
                </li>
                <li class="t1"><span class="color">>></span>Ruta Clave Privada:</li>
                <li class="t2">
                    <asp:TextBox ID="txtRutaClavePriv" CssClass="normal" runat="server" MaxLength="300"
                        Width="90%" Text=""></asp:TextBox>
                </li>
                <li class="t1"><span class="color">>></span>Adjuntar Clave P&uacute;blica:</li>
                <li class="t2">
                    <asp:FileUpload ID="fuClavePublica" runat="server" Width="250px" />
                </li>
                <li class="t1"><span class="color">>></span>Adjuntar Clave Privada:</li>
                <li class="t2">
                    <asp:FileUpload ID="fuClavePrivada" runat="server" Width="250px" />
                </li>
            </ul>
        </div>
        <div id="divConfEmailCont" runat="server" class="cont_cel2" visible="false" style="min-height: 100px">
            <h6>
                7. Configuraci&oacute;n Email - Contrato</h6>
            <ul class="datos_cip2">
                <li class="complet">
                    <asp:Button ID="btnConfEmail" CssClass="input_azul3" runat="server" OnClientClick="aspnetForm.target ='_blank';"
                        Text="Conf. Email" />
                    <asp:Button ID="btnConfContrato" CssClass="input_azul4" runat="server" OnClientClick="aspnetForm.target ='_blank';"
                        Text="Conf. Contrato" />
                </li>
            </ul>
        </div>
        -
        <asp:UpdatePanel ID="UpdatePanelFormulario" runat="server">
            <ContentTemplate>
                <div class="cont_cel2" style="width: 800px">
                    <h6>
                        8. Configuraci&oacute;n Formulario</h6>
                    <asp:HiddenField ID="hdnFormLogo" runat="server" />
                    <asp:HiddenField ID="hdnLogoResponsive" runat="server" />
                    <asp:HiddenField ID="hdnFormBackground" runat="server" />
                    <asp:HiddenField ID="hdnFormBackgroundResponsive" runat="server" />
                    <ul class="datos_cip2">
                        <!--
                        <li class="t1"><span class="color">>></span>�Formulario activo?</li>
                        <li class="t2">
                            <asp:CheckBox ID="chkFormActivo" runat="server" AutoPostBack="True" />
                        </li>
                        -->
                        <li class="t1"><span class="color">>></span>Nombre URL:</li>
                        <li class="t2" style="width: 400px">
                            <asp:TextBox ID="txtFormNombreUrl" CssClass="corta" runat="server" MaxLength="50"
                                onkeypress="return soloLetras(event)" oncopy="return false;" onpaste="return false;"
                                oncut="return false;" Text="" Width="135px">
                            </asp:TextBox>
                            <asp:Button ID="btnValidarURL" runat="server" Text="Validar URL" Width="120px" CssClass="input_azul4" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <p class="msjenomargin" style="color: Red">
                                https://empresa.pagoefectivo.pe/{Nombre URL}
                            </p>
                            <p class="msjenomargin" style="color: Blue; text-align: left">
                                <asp:Label ID="lblUrlFormulario" runat="server" Text="" Width="100%" Style="text-align: left">
                                </asp:Label>
                            </p>
                        </li>
                        <li class="t1"><span class="color">>></span>Logo:</li>
                        <li class="t2" style="width: 400px">
                            <asp:FileUpload ID="fupFormLogo" runat="server" Width="229px" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <asp:Image ID="ImgLogo" runat="server" Height="16px" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <p class="msjenomargin" style="color: Red">
                                (Extensiones v&aacute;lidas: .png; Dimensiones maximas: 250x90px)</p>
                        </li>
                        <li class="t1"><span class="color">>></span>Logo responsive:</li>
                        <li class="t2">
                            <asp:FileUpload ID="fupFormLogoResponsive" runat="server" Width="229px" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <asp:Image ID="ImgLogoResponsive" runat="server" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <p class="msjenomargin" style="color: Red">
                                (Extensiones v&aacute;lidas: .png; Dimensiones maximas: 132x28px)</p>
                        </li>
                        <li class="t1"><span class="color">>></span>Background desktop:</li>
                        <li class="t2">
                            <asp:FileUpload ID="fupFormBackground" runat="server" Width="229px" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <asp:Image ID="ImgBackground" runat="server" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <p class="msjenomargin" style="color: Red">
                                (Extensiones v&aacute;lidas: .jpg; Dimensiones maximas: 1300x116px)</p>
                        </li>
                        <li class="t1"><span class="color">>></span>Background responsive:</li>
                        <li class="t2">
                            <asp:FileUpload ID="fupFormBackgroundResponsive" runat="server" Width="229px" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <asp:Image ID="ImgBackgroundResponsive" runat="server" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <p class="msjenomargin" style="color: Red">
                                (Extensiones v&aacute;lidas: .jpg; Dimensiones maximas: 320x96px)</p>
                        </li>
                        <li class="t1"><span class="color">>></span>Par�metro �tems M�ximo:</li>
                        <li class="t2" style="width: 400px">
                            <asp:TextBox ID="txtFormItemsMaximo" CssClass="corta" runat="server" MaxLength="2">10</asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtFormItemsMaximo">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtFormItemsMaximo" Type="Integer"
                                MinimumValue="0" MaximumValue="99" Display="None" ErrorMessage="Par�metro �tems M�ximo solo acepta valores entre 0 y 99."
                                runat="server">
                            </asp:RangeValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>Par�metro Cantidad:</li>
                        <li class="t2" style="width: 400px">
                            <asp:TextBox ID="txtFormCantidad" CssClass="corta" runat="server" MaxLength="2">1</asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtFormCantidad">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RangeValidator ID="RangeValidator2" ControlToValidate="txtFormCantidad" Type="Integer"
                                MinimumValue="0" MaximumValue="99" Display="None" ErrorMessage="Par�metro Cantidad solo acepta valores entre 0 y 99."
                                runat="server">
                            </asp:RangeValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>PlaceHolder Dato Adicional:</li>
                        <li class="t2" style="width: 400px; height: 150px;">
                            <asp:TextBox ID="txtFormPlaceHolderDatosAdicionales" CssClass="corta" runat="server"
                                MaxLength="250" TextMode="MultiLine" Style="width: 400px; height: 150px;">
                            </asp:TextBox>
                        </li>
                        <li class="t2" style="padding-left: 10px; height: auto; float: right">
                            <asp:RegularExpressionValidator ID="rgeDatoAdicional" runat="server" Style="float: none"
                                ControlToValidate="txtFormPlaceHolderDatosAdicionales" Display="None" ErrorMessage="Datos Adicionales Invalido"
                                ForeColor="Red" SetFocusOnError="true" ValidationExpression="^[A-Z0-9 a-z.����������������������()*#'\- (/\r?\n/g)]*$">           
                            </asp:RegularExpressionValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>Mensaje Personalizado:</li>
                        <li class="t2" style="width: 400px; height: 150px;">
                            <asp:TextBox ID="txtFormMensajePersonalizado" CssClass="corta" runat="server" MaxLength="250"
                                TextMode="MultiLine" Style="width: 400px; height: 150px;"></asp:TextBox>
                        </li>
                        <li class="t2" style="padding-left: 10px; height: auto; float: right">
                            <asp:RegularExpressionValidator ID="rgeMensajePersnalizado" runat="server" Style="float: none"
                                ControlToValidate="txtFormMensajePersonalizado" Display="None" ErrorMessage="Mensaje Personalizado Invalido"
                                ForeColor="Red" SetFocusOnError="true" ValidationExpression="^[A-Z0-9 a-z.@����������������������()*#'\- (/\r?\n/g)]*$">           
                            </asp:RegularExpressionValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>URL T�rminos y Condiciones:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtFormUrlTerminos" CssClass="normal" runat="server" MaxLength="250"
                                Width="304px" Text=""></asp:TextBox>
                        </li>
                        <li class="t2" style="padding-left: 10px; height: auto; float: right">
                            <asp:RegularExpressionValidator ID="rgeUrlTerminos" runat="server" Style="float: none"
                                ControlToValidate="txtFormUrlTerminos" Display="None" ErrorMessage="La URL de terminos es incorrecto-Ejemplo:http://www.web.com"
                                ForeColor="Red" SetFocusOnError="true" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?">                                                   
                            </asp:RegularExpressionValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>Correo Administrador:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtFormCorreoAdmin" CssClass="normal" runat="server" MaxLength="250"
                                Width="304px" Text=""></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">>></span>Domicilio Completo:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstadoDomicilio" runat="server" CausesValidation="false"
                                CssClass="neu" AutoPostBack="true">
                            </asp:DropDownList>
                        </li>
                        <li class="t2" style="padding-left: 10px; height: auto; float: right">
                            <asp:RegularExpressionValidator ID="rgeCorreo" runat="server" Style="float: none"
                                ControlToValidate="txtFormCorreoAdmin" Display="None" ErrorMessage="El Correo Administrador es Incorrecto"
                                ForeColor="Red" SetFocusOnError="true" ValidationExpression="^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">                                                   
                            </asp:RegularExpressionValidator>
                        </li>
                    </ul>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="chkFormActivo" EventName="CheckedChanged" />
                <asp:AsyncPostBackTrigger ControlID="dropCanalVenta" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanelCobroMasivo" runat="server">
            <ContentTemplate>
                <div class="cont_cel2">
                    <h6>
                        9. Configuraci&oacute;n Cobros Masivos</h6>
                    <asp:HiddenField ID="hdnLogoCobranza" runat="server" />
                    <ul class="datos_cip2" style="width: 650px;">
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Cantidad m&aacute;xima
                            de planillas por mes:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtCanMaxPlaMes" CssClass="corta" runat="server" MaxLength="3">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvCanMaxPlaMes" CssClass="validacion" ControlToValidate="txtCanMaxPlaMes"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Cantidad m�xima de planilla por mes es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteCanMaxPlaMes" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtCanMaxPlaMes">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revCanMaxPlaMes" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtCanMaxPlaMes" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Cantidad m�xima de planilla por mes debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Cantidad de cobros por
                            planilla:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtCanCobPla" CssClass="corta" runat="server" MaxLength="6">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvCanCobPla" CssClass="validacion" ControlToValidate="txtCanCobPla"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Cantidad cobros por planilla es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteCanCobPla" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtCanCobPla">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revCanCobPla" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtCanCobPla" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Cantidad cobros por planilla debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Monto m&iacute;nimo
                            por cobro:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtMonMinCob" CssClass="corta" runat="server" MaxLength="11" onkeypress="return onKeyDecimal(event,this);">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvMonMinCob" CssClass="validacion" ControlToValidate="txtMonMinCob"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�nimo por cobro es requerido."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revMonMinCob" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtMonMinCob" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�nimo por cobro debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Monto m&aacute;ximo
                            por cobro:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtMonMaxCob" CssClass="corta" runat="server" MaxLength="11" onkeypress="return onKeyDecimal(event,this);">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvMonMaxCob" CssClass="validacion" ControlToValidate="txtMonMaxCob"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�ximo por cobro es requerido."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revMonMaxCob" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtMonMaxCob" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�ximo por cobro debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Monto m&iacute;nimo
                            por planilla:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtMonMinPla" CssClass="corta" runat="server" MaxLength="11" onkeypress="return onKeyDecimal(event,this);">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvMonMinPla" CssClass="validacion" ControlToValidate="txtMonMinPla"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�nimo por planilla es requerido."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revMonMinPla" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtMonMinPla" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�nimo por planilla debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Monto m&acirc;ximo
                            por planilla:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtMonMaxPla" CssClass="corta" runat="server" MaxLength="11" onkeypress="return onKeyDecimal(event,this);">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvMonMaxPla" CssClass="validacion" ControlToValidate="txtMonMaxPla"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�ximo por planilla es requerido."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revMonMaxPla" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtMonMaxPla" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Monto m�ximo por planilla debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Vigencia m�nima (d&iacute;as):</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtVigMinDia" CssClass="corta" runat="server" MaxLength="8">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvVigMinDia" CssClass="validacion" ControlToValidate="txtVigMinDia"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�nima (d�as) es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteVigMinDia" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtVigMinDia">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revVigMinDia" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtVigMinDia" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�nima (d�as) debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Vigencia m�nima (horas):</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtVigMinHor" CssClass="corta" runat="server" MaxLength="8">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvVigMinHor" CssClass="validacion" ControlToValidate="txtVigMinHor"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�nima (horas) es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteVigMinHor" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtVigMinHor">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revVigMinHor" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtVigMinHor" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�nima (horas) debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Vigencia m&acirc;xima
                            (d&iacute;as):</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtVigMaxDia" CssClass="corta" runat="server" MaxLength="8">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvVigMaxDia" CssClass="validacion" ControlToValidate="txtVigMaxDia"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�xima (dias) es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteVigMaxDia" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtVigMaxDia">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revVigMaxDia" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtVigMaxDia" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�xima (dias) debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>Vigencia m&acirc;xima
                            (horas):</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtVigMaxHor" CssClass="corta" runat="server" MaxLength="8">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvVigMaxHor" CssClass="validacion" ControlToValidate="txtVigMaxHor"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�xima (horas) es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteVigMaxHor" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtVigMaxHor">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revVigMaxHor" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtVigMaxHor" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Vigencia m�xima (horas) debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px;"><span class="color">>></span>D&iacute;a m&aacute;ximo
                            inicio cobranza:</li>
                        <li class="t2" style="width: 250px;">
                            <asp:TextBox ID="txtDiaMaxIniCob" CssClass="corta" runat="server" MaxLength="8">
                            </asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvDiaMaxIniCob" CssClass="validacion" ControlToValidate="txtDiaMaxIniCob"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: D�a m�ximo inicio cobranza es requerido."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="fteDiaMaxIniCob" runat="server" FilterType="Custom,Numbers"
                                TargetControlID="txtDiaMaxIniCob">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revDiaMaxIniCob" runat="server" ValidationExpression="^([0-9]*[1-9][0-9]*(\.[0-9]+)?|[0]+\.[0-9]*[1-9][0-9]*)$"
                                ControlToValidate="txtDiaMaxIniCob" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: D�a m�ximo inicio cobranza debe ser mayor a cero"></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px"><span class="color">>></span>Env�o de email operador:</li>
                        <li class="t2" style="width: 250px">
                            <asp:CheckBox ID="chkEnvMailOpe" runat="server" AutoPostBack="True" />
                        </li>
                        <li class="t1" style="width: 250px"><span class="color">>></span>Enviar email:</li>
                        <li class="t2" style="height: auto">
                            <asp:TextBox ID="txtMailOperadorCobranza" CssClass="cortaexpiracion" runat="server"
                                TextMode="MultiLine" Width="350px" Height="50px" Enabled="False"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvMailOperadorCobranza" CssClass="validacion" ControlToValidate="txtMailOperadorCobranza"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Enviar mail es requerido."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revMailOperadorCobranza" runat="server" ValidationExpression="^([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}),?)+$"
                                ControlToValidate="txtMailOperadorCobranza" CssClass="validacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Enviar mail no tiene el formato correcto."></asp:RegularExpressionValidator>
                        </li>
                        <li class="t1" style="width: 250px"><span class="color">>></span>Logo de la empresa:</li>
                        <li class="t2" style="width: 400px">
                            <asp:FileUpload ID="fuCargaLogoCobranza" runat="server" Width="229px" />
                            <asp:RequiredFieldValidator ID="rfvCargaLogoCobranza" CssClass="validacion" ControlToValidate="fuCargaLogoCobranza"
                                runat="server" ValidationGroup="GrupoValidacion" Text="*" 
                                ErrorMessage="Cobros Masivos: Logo de la empresa es requerido."></asp:RequiredFieldValidator>
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <asp:Image ID="imgLogoCobranza" runat="server" Height="35px" Width="130px" />
                        </li>
                        <li class="t2" style="padding-left: 200px; height: auto">
                            <p class="msjenomargin" style="color: Red">
                                (Extensiones v&aacute;lidas: .png; Dimensiones maximas: 250x90px)
                        </li>
                    </ul>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="chkEnvMailOpe" EventName="CheckedChanged" />
                <asp:AsyncPostBackTrigger ControlID="dropCanalVenta" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="GrupoValidacion"
            Style="padding-left: 200px; height: auto" runat="server" />
        <ul class="datos_cip2 t0">
            <li class="mensaje" style="padding-bottom: 10px">
                <asp:Label ID="lblMensaje" CssClass="MensajeTransaccion" runat="server" Style="text-align: left"></asp:Label>
            </li>
            <li class="complet">
                <asp:Button ID="btnRegistrar" OnClientClick="return ConfirmMe();" runat="server"
                    Text="Registrar" ValidationGroup="GrupoValidacion" CssClass="input_azul3" />
                <asp:Button ID="btnActualizar" runat="server" OnClientClick="return ConfirmMe();"
                    Text="Actualizar" ValidationGroup="GrupoValidacion" CssClass="input_azul3" />
                <asp:Button ID="btnCancelar" OnClientClick="return CancelMe();" runat="server" Text="Cancelar"
                    CssClass="input_azul4" />
            </li>
        </ul>
        <div style="clear: both">
        </div>
    </div>
    <asp:Panel ID="pnlPopupEmpresaContrante" runat="server" Style="display: none;">
        <div id="divBusquedaRepresentantes" class="divContenedor" style="width: 600px">
            <div id="div8" class="divContenedorTitulo">
                <div style="float: left">
                    Busqueda de Empresas Contrantes</div>
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                        runat="server"></asp:ImageButton>
                </div>
            </div>
            <br />
            <asp:UpdatePanel ID="upnlListaEmprContrantes" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <fieldset class="ContenedorEtiquetaTexto">
                        <div id="div21" class="EtiquetaTextoIzquierda">
                            <asp:Label ID="lblNombreEmpresaContratante" runat="server" Text="Raz�n Social:" CssClass="EtiquetaLargo"></asp:Label>
                            <asp:TextBox ID="txtNombreEmpresaContratante" runat="server" CssClass="TextoLargo"></asp:TextBox>
                            <asp:Button runat="server" ID="btnBuscarEmpresaContratante" CausesValidation="false"
                                OnClick="btnBuscarEmpresaContratante_Click" CssClass="Boton" Text="Buscar"></asp:Button>
                        </div>
                        <div>
                        </div>
                    </fieldset>
                    <div>
                        <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" Width="580px" AutoGenerateColumns="False"
                            AllowPaging="True" OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="IdEmpresaContratante" />
                                <asp:BoundField DataField="Codigo" HeaderText="C&#243;digo" />
                                <asp:BoundField DataField="RazonSocial" HeaderText="Razon Social" />
                                <asp:BoundField DataField="Contacto" HeaderText="Contacto" />
                                <asp:BoundField DataField="Telefono" HeaderText="Telefono" />
                                <asp:BoundField DataField="Email" HeaderText="E-mail" />
                                <asp:TemplateField HeaderText="Ver">
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                            ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/ok2.jpg" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/images/ok2.jpg" SelectText="Seleccionar"
                                    ShowSelectButton="True" />--%>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</asp:Content>
