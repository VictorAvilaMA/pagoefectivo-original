Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.IO
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Drawing.Imaging
Imports Microsoft.VisualStudio.Web.PageInspector.Runtime
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Util.DataUtil


Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Logging
Imports SPE.Web.AWS

Partial Class ADM_ADServ
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CServicio)

    Private ReadOnly _logger As ILogger = New Logger()

    Private _vExpressLogoNombre As String = ""
    Private _vformLogoNombre As String = ""
    Private _vformLogoResponsiveNombre As String = ""
    Private _vformBackgroundNombre As String = ""
    Private _vformBackgroundResponsiveNombre As String = ""
    Dim MontoMaxPlanillaCobranzaValue As String = "0"

    Public _vformUrlImagen As String = ConfigurationManager.AppSettings("FORM_S3_DIRECTORY_WEB")



#Region "Propiedades"
    Private objCParametro As CAdministrarParametro
    Private Function InstanciaParametro() As CAdministrarParametro
        If objCParametro Is Nothing Then
            objCParametro = New CAdministrarParametro()
        End If
        Return objCParametro
    End Function
    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
#End Region

#Region "M�todos base"
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADServ"
        'e.URLPageCancelForEdit = "COServ.aspx"
        'e.URLPageCancelForInsert = "COServ.aspx"
        e.URLPageCancelForSave = "COServ.aspx"
        e.UseEntitiesMethod = True
        e.PageTitle = "PagoEfectivo - " + lblTitle.Text
        CargarTipoOrigenCancelacion()
    End Sub

    Protected Overrides Sub MaintenancePage_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.MaintenancePage_Load(sender, e)

        If Not IsPostBack Then
            If (_3Dev.FW.Web.PageManager.IsInserting) Then
                EnableTextBox(txtCodigo)
                lnkShowAudit.Visible = False
                ucAudit.Visible = False
                rbExtornoUnicoNo.Checked = True
            ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
                If (txtCodigo.Text.Trim() = "") Then
                    EnableTextBox(txtCodigo)
                Else
                    DisableTextBox(txtCodigo)
                End If
                lnkShowAudit.Visible = True
                divConfEmailCont.Visible = True
            End If
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto MaintenancePage_Load")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto MaintenancePage_Load")
        End If
    End Sub

    ''' <summary>
    '''  Se encarga de asignar los valores de la entidad a los controles web luego de traer la informaci�n de la fuente de datos
    ''' </summary>
    ''' <param name="response"></param>
    ''' <remarks></remarks>
    Public Overrides Sub AssignEntityMessageResponseValuesInLoadingInfo(ByVal response As _3Dev.FW.Entidades.BusinessMessageBase)
        Dim obeservicio As SPE.Entidades.BEServicio = CType(response.Entity, SPE.Entidades.BEServicio)
        Dim i As Integer
        Dim j As Integer
        Dim numeroChecks As Integer
        Dim numeroOrigenesCancelacion As Integer
        Dim IdTipoOrigenCancelacion As Integer
        txtCodigo.Text = obeservicio.Codigo
        txtNombre.Text = obeservicio.Nombre
        txtTiempoExpiracion.Text = _3Dev.FW.Util.DataUtil.ObjectDecimalToStringFormatMiles(obeservicio.TiempoExpiracion)
        txtEmpresaContrante.Text = obeservicio.NombreEmpresaContrante
        lblIdEmpresaContrante.Text = obeservicio.IdEmpresaContratante
        dropEstado.SelectedValue = obeservicio.IdEstado
        dropVersion.SelectedValue = obeservicio.CodigoServicioVersion
        lblIdServicio.Text = obeservicio.IdServicio
        txtUrl.Text = obeservicio.Url
        CargarTipoOrigenCancelacion()
        numeroChecks = Me.cklOrigenesCancelacion.Items.Count()
        numeroOrigenesCancelacion = obeservicio.ListaTipoOrigenCancelacion.Count

        ddlTipoNotificacion.SelectedValue = obeservicio.IdTipoNotificacion
        ddlTipoNotificacion_SelectedIndexChanged(Nothing, Nothing)

        txtUrlFTP.Text = obeservicio.UrlFTP
        txtUsuarioFTP.Text = obeservicio.Usuario
        txtPasswordFTP.Text = obeservicio.Password

        txtClaveAPI.Text = obeservicio.ClaveAPI.Trim()
        txtClaveSecreta.Text = obeservicio.ClaveSecreta.Trim()

        txtUrlNotificacion.Text = obeservicio.UrlNotificacion
        txtUrlRedireccionamiento.Text = obeservicio.UrlRedireccionamiento

        chkUsuarioAnonimo.Checked = obeservicio.UsaUsuariosAnonimos
        dropTipoIntegracion.SelectedValue = obeservicio.IdTipoIntegracion

        chkProximoAfiliado.Checked = obeservicio.ProximoAfiliado
        chkVisibleEnPortada.Checked = obeservicio.VisibleEnPortada

        txtRutaClavePriv.Text = obeservicio.RutaClavePrivate
        txtRutaClavePub.Text = obeservicio.RutaClavePublica

        'Configuracion de envios de email
        chkNotifCrearUsuario.Checked = obeservicio.FlgEmailGenUsuario
        chkNotifGenerarCIP.Checked = obeservicio.FlgEmailGenCIP
        chkNotifPagarCIP.Checked = obeservicio.FlgEmailPagoCIP
        chkNotifCopiaAdmin.Checked = obeservicio.FlgEmailCCAdmin
        chkNotifExpiracionCIP.Checked = obeservicio.FlgEmailExpCIP
        txtTiempoAvisoExpiracion.Text = obeservicio.TiempoEmailExpiracion


        'formulario servicio
        'chkFormActivo.Checked = obeservicio.FormularioActivo
        'EnabledMaintenanceControlsForm(obeservicio.FormularioActivo)

        txtFormNombreUrl.Text = obeservicio.FormularioNombreUrl



        'PEEXPRESS
        ddlTipoPlantilla.SelectedValue = obeservicio.TipoPlantilla.ToString()

        'agregado
        If obeservicio.FormularioItemsMaximo.ToString = 0 OrElse String.IsNullOrEmpty(obeservicio.FormularioItemsMaximo.ToString) Then
            txtFormItemsMaximo.Text = "10"
        Else
            txtFormItemsMaximo.Text = obeservicio.FormularioItemsMaximo.ToString
        End If

        If obeservicio.FormularioCantidad.ToString = 0 OrElse String.IsNullOrEmpty(obeservicio.FormularioCantidad.ToString) Then
            txtFormCantidad.Text = "1"
        Else
            txtFormCantidad.Text = obeservicio.FormularioCantidad.ToString
        End If
        'agregado

        txtFormPlaceHolderDatosAdicionales.Text = obeservicio.FormularioPlaceHolderDatosAdicionales
        txtFormMensajePersonalizado.Text = obeservicio.FormularioMensajePersonalizado
        txtFormUrlTerminos.Text = obeservicio.FormularioUrlTerminos
        txtFormCorreoAdmin.Text = obeservicio.FormularioCorreo
        ddlEstadoDomicilio.SelectedValue = Convert.ToInt32(obeservicio.EstadoDireccion.ToString)
        'Convert.ToString(obeservicio.EstadoDireccion.ToString)



        hdnFormLogo.Value = obeservicio.FormularioRutaLogo
        hdnLogoResponsive.Value = obeservicio.FormularioRutaLogoResponsive
        hdnFormBackground.Value = obeservicio.FormularioRutaFondoHeader
        hdnFormBackgroundResponsive.Value = obeservicio.FormularioRutaFondoHeaderResponsive


        'PEEXPRESS
        'hdnExpressLogo.Value = obeservicio.LogoServicio


        'Imagenes de servicio formulario

        If obeservicio.FormularioRutaLogo = "" OrElse String.IsNullOrEmpty(obeservicio.FormularioRutaLogo) Then
            ImgLogo.Visible = False
        Else
            ImgLogo.Visible = True
            ImgLogo.Width = 180
            ImgLogo.Height = 70
            ImgLogo.ImageUrl = _vformUrlImagen + obeservicio.FormularioRutaLogo + "?" + Guid.NewGuid().ToString().Substring(0, 8)
        End If

        If obeservicio.FormularioRutaLogoResponsive = "" OrElse String.IsNullOrEmpty(obeservicio.FormularioRutaLogoResponsive) Then
            ImgLogoResponsive.Visible = False
        Else
            ImgLogoResponsive.Visible = True
            ImgLogoResponsive.Width = 132
            ImgLogoResponsive.Height = 28
            ImgLogoResponsive.ImageUrl = _vformUrlImagen + obeservicio.FormularioRutaLogoResponsive + "?" + Guid.NewGuid().ToString().Substring(0, 8)
        End If

        If obeservicio.FormularioRutaFondoHeader = "" OrElse String.IsNullOrEmpty(obeservicio.FormularioRutaFondoHeader) Then
            ImgBackground.Visible = False
        Else
            ImgBackground.Visible = True
            ImgBackground.Width = 350
            ImgBackground.Height = 70
            ImgBackground.ImageUrl = _vformUrlImagen + obeservicio.FormularioRutaFondoHeader + "?" + Guid.NewGuid().ToString().Substring(0, 8)
        End If

        If obeservicio.FormularioRutaFondoHeaderResponsive = "" OrElse String.IsNullOrEmpty(obeservicio.FormularioRutaFondoHeaderResponsive) Then
            ImgBackgroundResponsive.Visible = False
        Else
            ImgBackgroundResponsive.Visible = True
            ImgBackgroundResponsive.Width = 320
            ImgBackgroundResponsive.Height = 30
            ImgBackgroundResponsive.ImageUrl = _vformUrlImagen + obeservicio.FormularioRutaFondoHeaderResponsive + "?" + Guid.NewGuid().ToString().Substring(0, 8)
        End If




        'iMAGEN PEEXPRESS

        If obeservicio.LogoServicio = "" OrElse String.IsNullOrEmpty(obeservicio.LogoServicio) Then
            ImgExpressLogo.Visible = False
        Else
            ImgExpressLogo.Visible = True
            ImgExpressLogo.Width = 300
            ImgExpressLogo.Height = 148
            ImgExpressLogo.ImageUrl = _vformUrlImagen + obeservicio.LogoServicio + "?" + Guid.NewGuid().ToString().Substring(0, 8)
        End If

        'Imagen del servicio
        Dim base64String As String = Convert.ToBase64String(obeservicio.LogoImagen, 0, obeservicio.LogoImagen.Length)
        If base64String = "" Or String.IsNullOrEmpty(base64String) Then
            Image1.Visible = False
        Else
            Image1.Visible = True
            Image1.ImageUrl = "data:image/jpeg;base64," + base64String
        End If



        For i = 0 To numeroChecks - 1
            For j = 0 To numeroOrigenesCancelacion - 1
                IdTipoOrigenCancelacion = obeservicio.ListaTipoOrigenCancelacion.Item(j).IdTipoOrigenCancelacion
                If IdTipoOrigenCancelacion = cklOrigenesCancelacion.Items(i).Value Then
                    cklOrigenesCancelacion.Items(i).Selected = True
                    Exit For
                Else
                    cklOrigenesCancelacion.Items(i).Selected = False
                End If
            Next
        Next

        'comentado
        'imgLogoImagen.Visible = True
        'agregado
        Image1.Visible = True


        If obeservicio.ExtornoUnico Then
            rbExtornoUnicoSi.Checked = True
            rbExtornoUnicoNo.Checked = False
        Else
            rbExtornoUnicoSi.Checked = False
            rbExtornoUnicoNo.Checked = True
        End If

        'Cobros Masivos - Inicio
        dropCanalVenta.SelectedValue = obeservicio.IdCanalVenta.ToString()
        dropCanalVenta_SelectedIndexChanged(Nothing, Nothing)

        dropWebService.SelectedValue = obeservicio.IdWebService.ToString()
        txtIP.Text = obeservicio.Ip

        chkEnviarGenerarCipCobros.Checked = obeservicio.EnvioGenerarCip
        chkEnviarExpiracionCipCobros.Checked = obeservicio.HorasAvisoExpiracion <> 12 AndAlso obeservicio.HorasAvisoExpiracion <> 0
        chkEnviarExpiracionCipCobros_CheckedChanged(Nothing, Nothing)

        txtAvisoExpiracionCobranza.Text = obeservicio.HorasAvisoExpiracion.ToString()
        txtCanMaxPlaMes.Text = obeservicio.CantidadMaximaPlanillaMes.ToString()
        txtCanCobPla.Text = obeservicio.CantidadCobrosPlanilla.ToString()
        txtMonMinCob.Text = obeservicio.MontoMinimoCobro.ToString()
        txtMonMaxCob.Text = obeservicio.MontoMaximoCobro.ToString()
        txtMonMinPla.Text = obeservicio.MontoMinimoPlanilla.ToString()
        txtMonMaxPla.Text = obeservicio.MontoMaximoPlanilla.ToString()
        txtVigMinDia.Text = obeservicio.VigenciaMinimaDias.ToString()
        txtVigMinHor.Text = obeservicio.VigenciaMinimaHoras.ToString()
        txtVigMaxDia.Text = obeservicio.VigenciaMaximaDias.ToString()
        txtVigMaxHor.Text = obeservicio.VigenciaMaximaHoras.ToString()
        txtDiaMaxIniCob.Text = obeservicio.DiaMaximoInicioCobranza.ToString()

        chkEnvMailOpe.Checked = Not String.IsNullOrEmpty(obeservicio.EmailOperador.Trim())
        chkEnvMailOpe_CheckedChanged(Nothing, Nothing)
        txtMailOperadorCobranza.Text = obeservicio.EmailOperador.Trim()

        If Not String.IsNullOrEmpty(obeservicio.NombreLogo) Then
            hdnLogoCobranza.Value = obeservicio.NombreLogo
            imgLogoCobranza.ImageUrl = ConfigurationManager.AppSettings("COBRANZA_URL_S3_BUCKET_LOGO") & "/" & obeservicio.NombreLogo
            rfvCargaLogoCobranza.Enabled = False
        End If

        _logger.Debug("Valor hdnLogoCobranza [AssignEntityMessageResponseValuesInLoadingInfo]: " + hdnLogoCobranza.Value)


        'Cobros Masivos - Fin

        VisualizarBtnClaves()
        lblTitle.Text = "Actualizar Servicio"
        ucAudit.RefreshView(obeservicio)

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto AssignEntityMessageResponseValuesInLoadingInfo")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto AssignEntityMessageResponseValuesInLoadingInfo")
        End If
    End Sub


    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        WebUtil.DropDownlistBinding(dropEstado, InstanciaParametro.ListarEstados(), "Descripcion", "Id")

        Page.SetFocus(ibtnBuscarEmpresa)

    End Sub


    'comentado
    'Public Overrides Sub OnAfterLoadInformation()
    '    MyBase.OnAfterLoadInformation()
    '    'imgLogoImagen.Src = "ADServImg.aspx?servicioid=" + CType(loadEntityResponse.Entity, SPE.Entidades.BEServicio).IdServicio.ToString()

    '    OcultarImagen()
    'End Sub

    Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
        MyBase.OnInitComplete(e)
        ucAudit.InitializeAudit(lnkShowAudit)
    End Sub

    Public Overrides Function AllowToSaveEntity() As Boolean
        Dim lista, combo As Boolean
        Dim cobroMasivo As String = ConfigurationManager.AppSettings("COBROMASIVO")
        Dim formulario As String = ConfigurationManager.AppSettings("FORMULARIO")
        lblMensaje.Text = ""
        lista = True
        lblMensajeNotificacion.Text = ""
        combo = True

        'formulario
        'If chkFormActivo.Checked = True And txtFormNombreUrl.Text IsNot Nothing Then
        If dropCanalVenta.SelectedValue = formulario And txtFormNombreUrl.Text IsNot Nothing Then

            If ValidarUrlFormulario() = True Then
                lblUrlFormulario.Text = "La Url es Valido"
                lista = True
            Else
                lblUrlFormulario.Text = "La Url No es Valido"
                lblMensaje.CssClass = "MensajeValidacion"
                lblMensaje.Text = "La Url No es Valido"
                lista = False
            End If

        Else
            lista = True
        End If

        If String.IsNullOrEmpty(txtRutaClavePub.Text) Then
            lblMensaje.CssClass = "MensajeValidacion"
            lblMensaje.Text = "La ruta de la clave publica no puede ser vac�a."
            lista = False
        End If
        If String.IsNullOrEmpty(txtRutaClavePriv.Text) Then
            lblMensaje.CssClass = "MensajeValidacion"
            lblMensaje.Text = "La ruta de la clave privada no puede ser vac�a."
            lista = False
        End If
        If ((CklIsSelect() = False) And (dropEstado.SelectedValue = EstadoMantenimiento.Activo)) Then
            lblMensaje.CssClass = "MensajeValidacion"
            lblMensaje.Text = "Debe seleccionar por lo menos un tipo de origen de cancelaci�n."
            lista = False
        End If
        If ((ddlTipoNotificacion.SelectedIndex = 0) And (dropEstado.SelectedValue = EstadoMantenimiento.Activo)) Then
            lblMensajeNotificacion.Text = "Debe seleccionar un tipo de notificaci�n."
            combo = False
        End If
        If ((dropTipoIntegracion.SelectedIndex = 0) And (dropEstado.SelectedValue = EstadoMantenimiento.Activo)) Then
            lblMensajeIntegracion.Text = "Debe seleccionar un Tipo de Integraci�n."
            combo = False
        End If
        If ImagenFile.HasFile Then
            If Not _3Dev.FW.Util.ImageUtil.HasSize(ImagenFile.FileBytes, 225, 90) Then
                lblMensaje.CssClass = "MensajeValidacion"
                lblMensaje.Text = "El tama�o de la imagen debe ser de 225x90 p�xeles."
                lista = False
            End If
        End If

        Page.Validate()

        Dim validationGroup = "GrupoValidacion"
        Dim validarCobroMasivo As Boolean = True

        If dropCanalVenta.SelectedValue = cobroMasivo Then

            MontoMaxPlanillaCobranzaValue = "0"
            Dim objCParametro As New SPE.Web.CAdministrarParametro()
            Dim MontoMaxPlanillaCobranza1 As List(Of BEParametro) = objCParametro.ConsultarParametroPorCodigoGrupo("COBM")
            MontoMaxPlanillaCobranzaValue = MontoMaxPlanillaCobranza1.Item(0).Descripcion.Trim()
            MontoMaxPlanillaCobranza1 = Nothing


            If Decimal.Parse(txtCanMaxPlaMes.Text) > 999D Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Cantidad m�xima de planillas por mes no puede ser mayor que 999", validationGroup)
            End If
            'modificado
            If Decimal.Parse(txtCanCobPla.Text) > 999999D Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Cantidad de cobros por planilla no puede ser mayor que 999999", validationGroup)
            End If

            If Decimal.Parse(txtMonMinCob.Text) > 10000D Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Monto m�nimo por cobro no puede ser mayor que 10,000", validationGroup)
            End If

            If Decimal.Parse(txtMonMaxCob.Text) > 10000D Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Monto m�ximo por cobro no puede ser mayor que 10,000", validationGroup)
            End If

            If Decimal.Parse(txtMonMinPla.Text) > 10000D Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Monto m�nimo por planilla no puede ser mayor que 10,000", validationGroup)
            End If


            'If Decimal.Parse(txtMonMaxPla.Text) > 10000D Then
            '    validarCobroMasivo = False
            '    AddValidationSummaryItem(Me.Page, "Cobros Masivos: Monto m�ximo por planilla no puede ser mayor que 10,000", validationGroup)
            'End If

            '--agregado
            Try
                If (MontoMaxPlanillaCobranzaValue = Nothing Or MontoMaxPlanillaCobranzaValue = "") Then
                    MontoMaxPlanillaCobranzaValue = 0
                End If

                If Decimal.Parse(txtMonMaxPla.Text) > CDec(MontoMaxPlanillaCobranzaValue) Then
                    validarCobroMasivo = False
                    AddValidationSummaryItem(Me.Page, "Cobros Masivos: Monto m�ximo por planilla no puede ser mayor que " + MontoMaxPlanillaCobranzaValue.ToString(), validationGroup)
                End If
            Catch ex As Exception

                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Error en Monto m�ximo por planilla ", validationGroup)
            End Try
            '--agregado


            If Integer.Parse(txtVigMinDia.Text) > 30 Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Vigencia m�nima (d�as) no puede ser mayor que 30", validationGroup)
            End If

            If Integer.Parse(txtVigMinHor.Text) > 23 Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Vigencia m�nima (horas) no puede ser mayor que 23", validationGroup)
            End If

            If Integer.Parse(txtVigMaxDia.Text) > 30 Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Vigencia m�xima (d�as) no puede ser mayor que 30", validationGroup)
            End If

            If Integer.Parse(txtVigMaxHor.Text) > 23 Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Vigencia m�xima (horas) no puede ser mayor que 23", validationGroup)
            End If

            If Integer.Parse(txtDiaMaxIniCob.Text) > 30 Then
                validarCobroMasivo = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: D�as m�ximo inicio de cobranza no puede ser mayor que 30", validationGroup)
            End If
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto AllowToSaveEntity")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto AllowToSaveEntity")
        End If

        Return Page.IsValid And ValidarImagen() And ValidarTiempoExpiracion() And lista And combo And ValidarImagenesFormulario() And validarCobroMasivo And ValidarImagenesCobroMasivo() And Page.IsValid()
    End Function

    Public Overrides Function CreateMessageRequestForSaveEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            Dim obeServicio As BEServicio = CreateBEServicio()
            obeServicio.Codigo = txtCodigo.Text.TrimEnd()
            'AGREGADO
            obeServicio.IdServicio = StringToInt(If(lblIdServicio.Text = "", "0", lblIdServicio.Text))

            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
            obeServicio.UsuarioCreacionNombre = UserInfo.NombreCompleto
            obeServicio.UrlNotificacion = txtUrlNotificacion.Text
            obeServicio.CodigoServicioVersion = dropVersion.SelectedValue
            request.Entity = obeServicio
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            Dim obeServicio As SPE.Entidades.BEServicio = CreateBEServicio()
            obeServicio.Codigo = txtCodigo.Text.TrimEnd()
            obeServicio.IdServicio = StringToInt(lblIdServicio.Text)
            obeServicio.ListaTipoOrigenCancelacion = ListaOrigenesCancelacion()
            obeServicio.IdUsuarioActualizacion = UserInfo.IdUsuario
            obeServicio.UsuarioActualizacionNombre = UserInfo.NombreCompleto
            obeServicio.UrlNotificacion = txtUrlNotificacion.Text
            obeServicio.CodigoServicioVersion = dropVersion.SelectedValue
            request.Entity = obeServicio
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto CreateMessageRequestForSaveEntity")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto CreateMessageRequestForSaveEntity")
        End If

        Return request
    End Function

    Public Overrides Sub OnAfterSaveEntity(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase, ByVal response As _3Dev.FW.Entidades.BusinessMessageBase)

        MyBase.OnAfterSaveEntity(request, response)
        SubirClavesparaNuevoApi()
        ucAudit.RefreshView(response.Entity)
        UtilServicios.LimpiarCacheComerciosAfiliadosHTML()
        If (response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok) Then
            SubirImagenesFormulario()
            SubirClavesFormulario()
            btnCancelar.CssClass = "input_azul3"
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto OnAfterSaveEntity")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto OnAfterSaveEntity")
        End If
    End Sub

    Public Overrides Sub EnabledMaintenanceControls(ByVal enabled As Boolean)
        ibtnBuscarEmpresa.Enabled = enabled
        EnableTextBox(txtEmpresaContrante, enabled)
        EnableTextBox(txtCodigo, enabled)
        EnableTextBox(txtNombre, enabled)
        EnableTextBox(txtTiempoExpiracion, enabled)
        EnableTextBox(txtUrl, enabled)
        EnableTextBox(txtClaveAPI, enabled)
        EnableTextBox(txtClaveSecreta, enabled)
        EnableTextBox(txtUsuarioFTP, enabled)
        EnableTextBox(txtPasswordFTP, enabled)
        txtRutaClavePriv.Enabled = enabled
        txtRutaClavePub.Enabled = enabled
        txtUrlNotificacion.Enabled = enabled
        txtUrlRedireccionamiento.Enabled = enabled
        ImagenFile.Enabled = enabled
        dropEstado.Enabled = enabled
        dropVersion.Enabled = enabled
        dropTipoIntegracion.Enabled = enabled
        cklOrigenesCancelacion.Enabled = enabled
        btnGenerarClaveAPI.Enabled = enabled
        btnGenerarClaveSecreta.Enabled = enabled
        chkUsuarioAnonimo.Enabled = enabled
        ddlTipoNotificacion.Enabled = enabled
        'formulario
        btnValidarURL.Enabled = enabled

        'Cobranza Masivas
        dropCanalVenta.Enabled = enabled
        dropWebService.Enabled = enabled

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto EnabledMaintenanceControls")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto EnabledMaintenanceControls")
        End If
    End Sub


    Public Sub EnabledMaintenanceControlsForm(ByVal enabled As Boolean)
        Dim formulario As String = ConfigurationManager.AppSettings("FORMULARIO")

        'If chkFormActivo.Checked = enabled Then
        If dropCanalVenta.SelectedValue = formulario Then
            txtFormNombreUrl.Enabled = enabled
            txtFormItemsMaximo.Enabled = enabled
            txtFormCantidad.Enabled = enabled
            txtFormPlaceHolderDatosAdicionales.Enabled = enabled
            txtFormMensajePersonalizado.Enabled = enabled
            txtFormUrlTerminos.Enabled = enabled


            fupFormLogo.Enabled = enabled
            fupFormLogoResponsive.Enabled = enabled
            fupFormBackground.Enabled = enabled
            fupFormBackgroundResponsive.Enabled = enabled
            btnValidarURL.Enabled = enabled

        Else

            txtFormNombreUrl.Enabled = enabled
            txtFormItemsMaximo.Enabled = enabled
            txtFormCantidad.Enabled = enabled
            txtFormPlaceHolderDatosAdicionales.Enabled = enabled
            txtFormMensajePersonalizado.Enabled = enabled
            txtFormUrlTerminos.Enabled = enabled


            fupFormLogo.Enabled = enabled
            fupFormLogoResponsive.Enabled = enabled
            fupFormBackground.Enabled = enabled
            fupFormBackgroundResponsive.Enabled = enabled
            btnValidarURL.Enabled = enabled

        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto EnabledMaintenanceControlsForm")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto EnabledMaintenanceControlsForm")
        End If
    End Sub

    Public Sub MaintenanceControlsCharge(ByVal enabled As Boolean)
        'Configuraci�n Envio de Correos
        chkNotifCrearUsuario.Enabled = Not enabled
        chkNotifGenerarCIP.Enabled = Not enabled
        chkNotifPagarCIP.Enabled = True
        chkNotifCopiaAdmin.Enabled = Not enabled

        chkNotifExpiracionCIP.Enabled = Not enabled
        rfvTiempoAvisoExpiracion.Enabled = Not enabled
        ftxtTiempoAvisoExpiracion.Enabled = Not enabled
        regExpTiempoAvisoExpiracion.Enabled = Not enabled

        chkEnviarGenerarCipCobros.Enabled = enabled

        chkEnviarExpiracionCipCobros.Enabled = enabled
        rfvAvisoExpiracionCobranza.Enabled = enabled
        fteAvisoExpiracionCobranza.Enabled = enabled
        revAvisoExpiracionCobranza.Enabled = enabled

        'Cobro Masivo
        If txtCanMaxPlaMes.Enabled <> enabled Then
            txtCanMaxPlaMes.Enabled = enabled
            revCanMaxPlaMes.Enabled = enabled
            rfvCanMaxPlaMes.Enabled = enabled
            fteCanMaxPlaMes.Enabled = enabled

            txtCanCobPla.Enabled = enabled
            fteCanCobPla.Enabled = enabled
            revCanCobPla.Enabled = enabled
            rfvCanCobPla.Enabled = enabled

            txtMonMinCob.Enabled = enabled
            revMonMinCob.Enabled = enabled
            rfvMonMinCob.Enabled = enabled

            txtMonMaxCob.Enabled = enabled
            revMonMaxCob.Enabled = enabled
            rfvMonMaxCob.Enabled = enabled

            txtMonMinPla.Enabled = enabled
            revMonMinPla.Enabled = enabled
            rfvMonMinPla.Enabled = enabled

            txtMonMaxPla.Enabled = enabled
            revMonMaxPla.Enabled = enabled
            rfvMonMaxPla.Enabled = enabled

            txtVigMinDia.Enabled = enabled
            fteVigMinDia.Enabled = enabled
            revVigMinDia.Enabled = enabled
            rfvVigMinDia.Enabled = enabled

            txtVigMinHor.Enabled = enabled
            fteVigMinHor.Enabled = enabled
            revVigMinHor.Enabled = enabled
            rfvVigMinHor.Enabled = enabled

            txtVigMaxDia.Enabled = enabled
            fteVigMaxDia.Enabled = enabled
            revVigMaxDia.Enabled = enabled
            rfvVigMaxDia.Enabled = enabled

            txtVigMaxHor.Enabled = enabled
            fteVigMaxHor.Enabled = enabled
            revVigMaxHor.Enabled = enabled
            rfvVigMaxHor.Enabled = enabled

            txtDiaMaxIniCob.Enabled = enabled
            fteDiaMaxIniCob.Enabled = enabled
            revDiaMaxIniCob.Enabled = enabled
            rfvDiaMaxIniCob.Enabled = enabled

            chkEnvMailOpe.Enabled = enabled
            fuCargaLogoCobranza.Enabled = enabled

            If Not String.IsNullOrEmpty(hdnLogoCobranza.Value) And enabled Then
                rfvCargaLogoCobranza.Enabled = False
            Else
                rfvCargaLogoCobranza.Enabled = enabled
            End If

            imgLogoCobranza.Visible = enabled

            If enabled Then
                chkEnvMailOpe.Checked = Not String.IsNullOrEmpty(txtMailOperadorCobranza.Text.Trim())
                chkEnvMailOpe_CheckedChanged(Nothing, Nothing)
            Else
                'chkEnvMailOpe.Checked = False
                chkEnvMailOpe.Checked = Not String.IsNullOrEmpty(txtMailOperadorCobranza.Text.Trim())
                chkEnvMailOpe.Enabled = False
                txtMailOperadorCobranza.Enabled = False
                revMailOperadorCobranza.Enabled = False
                rfvMailOperadorCobranza.Enabled = False
            End If
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto MaintenanceControlsCharge")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto MaintenanceControlsCharge")
        End If
    End Sub

#End Region

#Region "M�todos"
    ''' <summary>
    ''' Crea la entidad BEServicio con los datos de los controles web.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateBEServicio() As SPE.Entidades.BEServicio
        Dim formulario As String = ConfigurationManager.AppSettings("FORMULARIO")
        Dim cobroMasivo As String = ConfigurationManager.AppSettings("COBROMASIVO")
        Dim obeservicio As New SPE.Entidades.BEServicio()
        obeservicio.Nombre = txtNombre.Text
        obeservicio.TiempoExpiracion = Convert.ToDecimal(txtTiempoExpiracion.Text)
        obeservicio.IdEmpresaContratante = Convert.ToInt32(lblIdEmpresaContrante.Text)
        obeservicio.NombreEmpresaContrante = txtEmpresaContrante.Text
        obeservicio.IdEstado = StringToInt(dropEstado.SelectedValue)
        obeservicio.Url = txtUrl.Text
        obeservicio.CodigoServicioVersion = StringToInt(dropVersion.SelectedValue)
        'AsignarImagen(be)
        Dim BytesImagen As Byte() = ObtenerImagen()
        obeservicio.LogoImagen = BytesImagen
        If BytesImagen IsNot Nothing Then
            Dim RutaImagen As String = Server.MapPath("~/" + ConfigurationManager.AppSettings("RutaImagenesAfiliados"))
            Dim NombreArchivoImagen As String = _3Dev.FW.Util.DataUtil.StringToSlug(txtNombre.Text).ToLower()
            _3Dev.FW.Util.ImageUtil.SaveAsJpeg(BytesImagen, RutaImagen, NombreArchivoImagen + ".jpg")
            _3Dev.FW.Util.ImageUtil.ResizeAndSaveAsJpeg(BytesImagen, RutaImagen, NombreArchivoImagen + "-2.jpg", 182, 98)
        End If

        obeservicio.ProximoAfiliado = chkProximoAfiliado.Checked
        obeservicio.VisibleEnPortada = chkVisibleEnPortada.Checked

        obeservicio.ListaTipoOrigenCancelacion = ListaOrigenesCancelacion()
        obeservicio.IdTipoNotificacion = ObjectToInt32(ddlTipoNotificacion.SelectedValue)
        obeservicio.UrlFTP = txtUrlFTP.Text.Trim()
        obeservicio.Usuario = txtUsuarioFTP.Text.Trim()
        obeservicio.Password = txtPasswordFTP.Text.Trim()

        '*************API***************
        obeservicio.ClaveAPI = txtClaveAPI.Text.Trim()
        obeservicio.ClaveSecreta = txtClaveSecreta.Text.Trim()
        obeservicio.UsaUsuariosAnonimos = chkUsuarioAnonimo.Checked
        obeservicio.IdTipoIntegracion = StringToInt(dropTipoIntegracion.SelectedValue)
        '**********API************'

        '<add Peru.Com FaseIII>
        obeservicio.UrlNotificacion = txtUrlNotificacion.Text
        obeservicio.UrlRedireccionamiento = txtUrlRedireccionamiento.Text

        '<add Mantenimiento de Claves - 2012/12/17>

        obeservicio.RutaClavePublica = txtRutaClavePub.Text
        obeservicio.PublicKey = fuClavePublica.FileBytes

        obeservicio.RutaClavePrivate = txtRutaClavePriv.Text
        obeservicio.PrivateKey = fuClavePrivada.FileBytes
        obeservicio.ExtornoUnico = IIf(rbExtornoUnicoNo.Checked, 0, rbExtornoUnicoSi.Checked)

        'Mantenimiento de envio de correos
        obeservicio.FlgEmailGenUsuario = chkNotifCrearUsuario.Checked
        obeservicio.FlgEmailGenCIP = chkNotifGenerarCIP.Checked
        obeservicio.FlgEmailPagoCIP = chkNotifPagarCIP.Checked
        obeservicio.FlgEmailCCAdmin = chkNotifCopiaAdmin.Checked
        'Email Aviso de Expiraci�n
        obeservicio.FlgEmailExpCIP = chkNotifExpiracionCIP.Checked
        obeservicio.TiempoEmailExpiracion = ObjectToInt32(txtTiempoAvisoExpiracion.Text.Trim())

        'Formulario de Servicio
        'obeservicio.FormularioActivo = chkFormActivo.Checked
        obeservicio.FormularioActivo = dropCanalVenta.SelectedValue = formulario
        obeservicio.FormularioNombreUrl = txtFormNombreUrl.Text


        'PagoEfectivoExpress
        obeservicio.TipoPlantilla = Convert.ToInt32(ddlTipoPlantilla.SelectedValue.ToString())

        If txtFormItemsMaximo.Text = "" Or txtFormItemsMaximo.Text = "0" Then
            obeservicio.FormularioItemsMaximo = 10
        Else
            obeservicio.FormularioItemsMaximo = Convert.ToInt32(txtFormItemsMaximo.Text.ToString())
        End If

        If txtFormCantidad.Text = "" Or txtFormCantidad.Text = "0" Then
            obeservicio.FormularioCantidad = 1
        Else
            obeservicio.FormularioCantidad = Convert.ToInt32(txtFormCantidad.Text.ToString())
        End If

        Dim directoryPath = txtCodigo.Text.Trim & "/"

        obeservicio.FormularioPlaceHolderDatosAdicionales = txtFormPlaceHolderDatosAdicionales.Text
        obeservicio.FormularioMensajePersonalizado = txtFormMensajePersonalizado.Text
        obeservicio.FormularioUrlTerminos = txtFormUrlTerminos.Text
        obeservicio.FormularioCorreo = txtFormCorreoAdmin.Text
        obeservicio.EstadoDireccion = ddlEstadoDomicilio.SelectedValue.ToString



        'PagoEfectivoExpress
        If fupExpressLogo.HasFile Then
            obeservicio.LogoServicio = _vExpressLogoNombre
        Else
            'obeservicio.LogoServicio = hdnExpressLogo.Value().ToString()
        End If
        'PagoEfectivoExpress
		
        If fupFormLogo.HasFile Then
            obeservicio.FormularioRutaLogo = _vformLogoNombre
        Else
            obeservicio.FormularioRutaLogo = hdnFormLogo.Value().ToString()
        End If

        If fupFormLogoResponsive.HasFile Then
            obeservicio.FormularioRutaLogoResponsive = _vformLogoResponsiveNombre
        Else
            obeservicio.FormularioRutaLogoResponsive = hdnLogoResponsive.Value().ToString()
        End If

        If fupFormBackground.HasFile Then
            obeservicio.FormularioRutaFondoHeader = _vformBackgroundNombre
        Else
            obeservicio.FormularioRutaFondoHeader = hdnFormBackground.Value().ToString()
        End If
        If fupFormBackgroundResponsive.HasFile Then
            obeservicio.FormularioRutaFondoHeaderResponsive = _vformBackgroundResponsiveNombre
        Else
            obeservicio.FormularioRutaFondoHeaderResponsive = hdnFormBackgroundResponsive.Value().ToString()
        End If


        'obeservicio.IdCanalVenta = Convert.ToInt32(dropCanalVenta.SelectedValue)
        If String.IsNullOrEmpty(dropCanalVenta.SelectedValue) Then obeservicio.IdCanalVenta = 0 Else obeservicio.IdCanalVenta = Convert.ToInt32(dropCanalVenta.SelectedValue)
        If String.IsNullOrEmpty(dropWebService.SelectedValue) Then obeservicio.IdWebService = 0 Else obeservicio.IdWebService = Convert.ToInt32(dropWebService.SelectedValue)
        obeservicio.Ip = txtIP.Text

        'Cobros Masivos

        If dropCanalVenta.SelectedValue = cobroMasivo Then
            With obeservicio
                .CantidadMaximaPlanillaMes = Convert.ToInt32(txtCanMaxPlaMes.Text)
                .CantidadCobrosPlanilla = Convert.ToInt32(txtCanCobPla.Text)
                .MontoMinimoCobro = Convert.ToDecimal(txtMonMinCob.Text)
                .MontoMaximoCobro = Convert.ToDecimal(txtMonMaxCob.Text)
                .MontoMinimoPlanilla = Convert.ToDecimal(txtMonMinPla.Text)
                .MontoMaximoPlanilla = Convert.ToDecimal(txtMonMaxPla.Text)
                .VigenciaMinimaDias = Convert.ToInt32(txtVigMinDia.Text)
                .VigenciaMaximaDias = Convert.ToInt32(txtVigMaxDia.Text)
                .VigenciaMinimaHoras = Convert.ToInt32(txtVigMinHor.Text)
                .VigenciaMaximaHoras = Convert.ToInt32(txtVigMaxHor.Text)
                .DiaMaximoInicioCobranza = Convert.ToInt32(txtDiaMaxIniCob.Text)
                .EnvioEmailOperador = chkEnvMailOpe.Checked
                .EmailOperador = txtMailOperadorCobranza.Text
                .HorasAvisoExpiracion = Convert.ToInt32(txtAvisoExpiracionCobranza.Text)
                .EnvioGenerarCip = chkEnviarGenerarCipCobros.Checked

                If Not String.IsNullOrEmpty(fuCargaLogoCobranza.FileName) Then
                    .NombreLogo = fuCargaLogoCobranza.FileName.Trim()
                Else
                    .NombreLogo = hdnLogoCobranza.Value().Trim()
                End If
            End With
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto CreateBEServicio")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto CreateBEServicio")
        End If

        Return obeservicio
    End Function

    Private Function RegistrarLogo(ByVal logo As Stream, ByVal nombreLogo As String) As Boolean
        Dim s3AccessKey As String = ConfigurationManager.AppSettings("COBRANZA_S3_ACCESS_KEY")
        Dim s3SecreAccessKey As String = ConfigurationManager.AppSettings("COBRANZA_S3_SECRET_ACCESS_KEY")
        Dim s3BucketLogo As String = ConfigurationManager.AppSettings("COBRANZA_S3_BUCKET_LOGO")
        Dim s3Region As String = ConfigurationManager.AppSettings("COBRANZA_S3_REGION")

        Dim awsS3 As New AmazonS3(s3AccessKey, s3SecreAccessKey, s3Region, s3BucketLogo, s3DirectoryPath:=String.Empty)
        Dim resultado = awsS3.UploadFileFromStream(logo, nombreLogo)
        Return resultado
    End Function

    ''' <summary>
    ''' Metodo para obtener los Origenes de Cancelacion seleccionados
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Function ListaOrigenesCancelacion() As List(Of BETipoOrigenCancelacion)
        Dim ListaOC As New List(Of BETipoOrigenCancelacion)
        Dim i As Integer
        Dim estado As Integer
        For i = 0 To cklOrigenesCancelacion.Items.Count - 1
            Dim objTipoOrigenCancelacion As New BETipoOrigenCancelacion()
            objTipoOrigenCancelacion.IdTipoOrigenCancelacion = cklOrigenesCancelacion.Items(i).Value
            estado = cklOrigenesCancelacion.Items(i).Selected
            If estado Then
                objTipoOrigenCancelacion.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
            Else
                objTipoOrigenCancelacion.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Inactivo
            End If
            ListaOC.Insert(i, objTipoOrigenCancelacion)
            objTipoOrigenCancelacion = Nothing
        Next
        Return ListaOC
    End Function

    ''' <summary>
    ''' Se encarga de obtener la imagen
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerImagen() As Byte()
        Return _3Dev.FW.Web.WebUtil.GetImage(ImagenFile)
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub VisualizarBtnClaves()
        btnGenerarClaveAPI.Visible = txtClaveAPI.Text.Trim() = ""
        btnGenerarClaveSecreta.Visible = txtClaveAPI.Text.Trim() <> ""
    End Sub

    Sub CargarTipoOrigenCancelacion()
        Dim objTipoOrigenCancelacion As New SPE.Web.CAdministrarComun
        cklOrigenesCancelacion.DataSource = objTipoOrigenCancelacion.ConsultarTipoOrigenCancelacion()
        cklOrigenesCancelacion.DataBind()

    End Sub

    Private Sub OcultarImagen()
        Dim beServicio As BEServicio = CType(loadEntityResponse.Entity, SPE.Entidades.BEServicio)
        If ((beServicio.LogoImagen Is Nothing) Or (beServicio.LogoImagen.Length = 0)) Then

            'comentado
            'imgLogoImagen.Visible = False
            'agregado
            Image1.Visible = False

        Else
            'comentado
            'imgLogoImagen.Visible = True
            'agregado
            Image1.Visible = True

        End If
    End Sub

    Public Sub BuscarRepresentantes()
        Dim ocRepresentante As New SPE.Web.CRepresentante()
        Dim obeEmpresa As New SPE.Entidades.BEEmpresaContratante
        obeEmpresa.RazonSocial = txtNombreEmpresaContratante.Text
        obeEmpresa.RUC = ""
        obeEmpresa.Contacto = ""
        gvResultado.DataSource = ocRepresentante.GetListByParameters("EmpresasLibresParaServicio", obeEmpresa)
        gvResultado.DataBind()
        'mppEmpresaContrante.Show()
    End Sub

    Protected Sub btnBuscarEmpresaContratante_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarEmpresaContratante.Click
        BuscarRepresentantes()
    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        If Not e Is Nothing Then
            lblIdEmpresaContrante.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).Text
            'txtPrefijo.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
            txtEmpresaContrante.Text = Server.HtmlDecode(gvResultado.Rows(Convert.ToInt32(e.CommandArgument)).Cells(2).Text)
            mppEmpresaContrante.Hide()
            UpdatePanelDatos.Update()
        End If

    End Sub

    Private Function ValidarTiempoExpiracion() As Boolean
        If IsNumeric(txtTiempoExpiracion.Text) Then
            Return True
        Else
            ThrowErrorMessage("El tiempo de expiraci�n no es un n�mero v�lido.")
            Return False
        End If
    End Function

    Private Function ValidarImagen() As Boolean
        Dim result As _3Dev.FW.Web.WebUtil.ValidateImageResult = _3Dev.FW.Web.WebUtil.ValidateImage(ImagenFile)
        If (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedOk) Then
            fisUpload.Visible = False
            Return True
        ElseIf (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedErrorHasNoFile) Then
            fisUpload.Visible = False
            Return True
        ElseIf (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedErrorExtensionInvalid) Then
            lblUploadCarga.Text = "  ** Nota: La imagen no fue cargada porque no tiene una extensi�n v�lida."
            fisUpload.Visible = True
            Return False

        ElseIf (result = _3Dev.FW.Web.WebUtil.ValidateImageResult.LoadedErrorWeightLimitExceeded) Then
            lblUploadCarga.Text = "  ** Nota: La imagen no fue cargada porque excede el tama�o limite de 2MB."
            fisUpload.Visible = True
            Return False
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagen")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagen")
        End If
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtFormMensajePersonalizado.Attributes("maxlength") = 120
            txtFormPlaceHolderDatosAdicionales.Attributes("maxlength") = 120

            Dim objCParametro As New SPE.Web.CAdministrarParametro()
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoNotificacion, objCParametro.ConsultarParametroPorCodigoGrupo("TNOT"), _
            "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)


            'agregado
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstadoDomicilio, objCParametro.ConsultarParametroPorCodigoGrupo("ECFO"), _
             "Descripcion", "Id")

            ddlEstadoDomicilio.SelectedValue = 3


            'agregado PE EXPRESS
            Dim lstTipoPlantilla As List(Of BEParametro)
            lstTipoPlantilla = objCParametro.ConsultarParametroPorCodigoGrupo("PEEX")

            Dim ordered = From obj In lstTipoPlantilla
                                Order By obj.Id Descending
            lstTipoPlantilla = ordered.ToList()

            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoPlantilla, lstTipoPlantilla, "Descripcion", "Id")


            _3Dev.FW.Web.WebUtil.DropDownlistBinding(dropTipoIntegracion, objCParametro.ConsultarParametroPorCodigoGrupo("TINT"), _
            "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
            WebUtil.DropDownlistBinding(dropVersion, InstanciaParametro.ListarVersionServicio(), "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            Dim tiempoAvisoExpDefault As List(Of BEParametro) = objCParametro.ConsultarParametroPorCodigoGrupo("TEXP")
            txtTiempoAvisoExpiracion.Text = tiempoAvisoExpDefault.Item(0).Descripcion.Trim()

            EnabledMaintenanceControlsForm(False)

            'Cobranza Masivas
            txtMailOperadorCobranza.Attributes("maxlength") = "100"
            txtIP.Attributes("maxlength") = "150"
            hdnLogoCobranza.Value = String.Empty

            _3Dev.FW.Web.WebUtil.DropDownlistBinding(dropCanalVenta, objCParametro.ConsultarParametroPorCodigoGrupo("CAVE"), _
            "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            _3Dev.FW.Web.WebUtil.DropDownlistBinding(dropWebService, objCParametro.ConsultarParametroPorCodigoGrupo("WEBS"), _
            "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            txtAvisoExpiracionCobranza.Text = tiempoAvisoExpDefault.Item(0).Descripcion.Trim()

            MaintenanceControlsCharge(False)
        End If
        ImagenFile.EnableViewState = True



        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto Page_Load")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto Page_Load")
        End If

    End Sub

    Protected Sub ddlTipoNotificacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoNotificacion.SelectedIndexChanged
        If ddlTipoNotificacion.SelectedValue = _3Dev.FW.Util.DataUtil.ObjectToString(SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoNotificacion.ArchivoFTP) Then ' "241"
            pnlTipoNotificacion.Visible = True
        Else
            pnlTipoNotificacion.Visible = False
            txtUrlFTP.Text = ""
            txtUsuarioFTP.Text = ""
            txtPasswordFTP.Text = ""
        End If
    End Sub

    Private Function CklIsSelect() As Boolean
        Dim Item As System.Web.UI.WebControls.ListItem
        For Each Item In cklOrigenesCancelacion.Items
            If Item.Selected Then Return True
        Next
        Return False
    End Function

    Protected Sub btnGenerarClaveAPI_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerarClavesAPI()
    End Sub

    Protected Sub btnGenerarClaveSecreta_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerarClaveSecreta()
    End Sub

    Protected Sub GenerarClavesAPI()
        If txtClaveAPI.Text.Trim() = "" Then
            txtClaveAPI.Text = Guid.NewGuid().ToString()
            If txtClaveSecreta.Text.Trim() = "" Then
                GenerarClaveSecreta()
            End If
            VisualizarBtnClaves()
        End If
    End Sub

    Protected Sub GenerarClaveSecreta()
        txtClaveSecreta.Text = Guid.NewGuid().ToString()
        btnGenerarClaveSecreta.Visible = True
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvResultado.PageIndex = e.NewPageIndex
        BuscarRepresentantes()
        mppEmpresaContrante.Show()
    End Sub

#End Region

    Protected Sub btnConfEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfEmail.Click
        Dim oBEServicio As New BEServicio
        oBEServicio.IdEmpresaContratante = Convert.ToInt64(lblIdEmpresaContrante.Text)
        oBEServicio.IdServicio = StringToInt(lblIdServicio.Text)
        Session("BEServicio") = oBEServicio
        Response.Redirect("~\ADM\ADEmail.aspx")
    End Sub

    Protected Sub btnConfContrato_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfContrato.Click
        Dim oBEServicio As New BEServicio
        oBEServicio.IdEmpresaContratante = Convert.ToInt64(lblIdEmpresaContrante.Text)
        oBEServicio.IdServicio = StringToInt(lblIdServicio.Text)
        Session("BEServicio") = oBEServicio
        Response.Redirect("~\ADM\ADCont.aspx")
    End Sub
    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        mppEmpresaContrante.Hide()
    End Sub

    Protected Sub rbExtornoUnicoSi_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbExtornoUnicoSi.CheckedChanged
        rbExtornoUnicoNo.Checked = False
        rbExtornoUnicoSi.Checked = True
    End Sub

    Protected Sub rbExtornoUnicoNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbExtornoUnicoNo.CheckedChanged
        rbExtornoUnicoNo.Checked = True
        rbExtornoUnicoSi.Checked = False
    End Sub

    Protected Sub chkFormActivo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFormActivo.CheckedChanged

        EnabledMaintenanceControlsForm(chkFormActivo.Checked)

    End Sub

    Protected Sub btnValidarURL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidarURL.Click

        If ValidarUrlFormulario() = True Then
            lblUrlFormulario.Text = "URL VALIDO"
            lblUrlFormulario.ForeColor = Color.Blue
        Else
            lblUrlFormulario.Text = "URL NO VALIDO"
            lblUrlFormulario.ForeColor = Color.Red
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagenesFormulario")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagenesFormulario")
        End If
    End Sub


    Public Function ValidarUrlFormulario() As Boolean
        Dim oservico As New SPE.Web.CServicio()
        Dim resultado As Boolean = True

        If lblIdServicio.Text = "" Then
            lblIdServicio.Text = "0"
        End If

        If (oservico.ConsultarServicioFormularioUrl(Convert.ToInt32(lblIdServicio.Text.ToString()), txtFormNombreUrl.Text)) Is Nothing Then
            resultado = True
        Else
            resultado = False
        End If

        If (_3Dev.FW.Web.PageManager.IsInserting) Then
            _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarUrlFormulario")
        ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
            _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarUrlFormulario")
        End If

        Return resultado
    End Function

    Public Function ValidarImagenesFormulario() As Boolean

        Try

            _logger.Debug("Inicia validacion de imagenes de formulario")

            If txtCodigo.Text.Trim().Length < 3 Then
                Return True
            End If

            Dim logoWith = 250
            Dim logoHeight = 90
            Dim logoSize = 200000

            Dim logoResponsiveWith = 132
            Dim logoResponsiveHeight = 28
            Dim logoResponsiveSize = 200000

            Dim bgWith = 1300
            Dim bgHeight = 116
            Dim bgSize = 200000

            Dim bgResponsiveWith = 320
            Dim bgResponsiveHeight = 96
            Dim bgResponsiveSize = 200000

            Dim directoryPath As String = txtCodigo.Text.Trim()
            Dim formLogoNombre = "logo"
            Dim formLogoResponsiveNombre = "logo-responsive"
            Dim formBackgroundNombre = "bg-header"
            Dim formBackgroundResponsiveNombre = "bg-header-responsive"

            Dim validationGroup = "GrupoValidacion"



            'PEEXPRESS
            Dim logoWithExpress = 300
            Dim logoHeightExpress = 148
            Dim logoSizeExpress = 200000

            Dim ExpressLogoNombre = "logo-plantilla"

            'agregado-----------------------------------------------------------

            Dim fileExt1 As String = System.IO.Path.GetExtension(fupFormLogo.FileName)
            Dim fileExt2 As String = System.IO.Path.GetExtension(fupFormLogoResponsive.FileName)
            Dim fileExt3 As String = System.IO.Path.GetExtension(fupFormBackground.FileName)
            Dim fileExt4 As String = System.IO.Path.GetExtension(fupFormBackgroundResponsive.FileName)

            'PEEXPRESS
            Dim fileExt5 As String = System.IO.Path.GetExtension(fupExpressLogo.FileName)

            Dim fileArray() As String = {".jpg", ".jpeg", ".png"}


            If Not String.IsNullOrEmpty(fileExt1) AndAlso Not fileArray.Any(Function(i) i = fileExt1) Then
                AddValidationSummaryItem(Me.Page, "Formulario: Logo -> Tipo de archivo no valido", validationGroup)
                Return False
            End If

            If Not String.IsNullOrEmpty(fileExt2) AndAlso Not fileArray.Any(Function(i) i = fileExt2) Then
                AddValidationSummaryItem(Me.Page, "Formulario: Logo Responsive -> Tipo de archivo no valido", validationGroup)
                Return False
            End If

            If Not String.IsNullOrEmpty(fileExt3) AndAlso Not fileArray.Any(Function(i) i = fileExt3) Then
                AddValidationSummaryItem(Me.Page, "Formulario: Background -> Tipo de archivo no valido", validationGroup)
                Return False
            End If

            If Not String.IsNullOrEmpty(fileExt4) AndAlso Not fileArray.Any(Function(i) i = fileExt4) Then
                AddValidationSummaryItem(Me.Page, "Formulario: Background Responsive -> Tipo de archivo no valido", validationGroup)
                Return False
            End If


            'PEEXPRESS
            If Not String.IsNullOrEmpty(fileExt5) AndAlso Not fileArray.Any(Function(i) i = fileExt5) Then
                AddValidationSummaryItem(Me.Page, "Logo Integracion Plantilla: -> Tipo de archivo no valido", validationGroup)
                Return False
            End If


            'agregado-----------------------------------------------------------


            If fupFormLogo.HasFile Then
                Dim fileExt As String = Path.GetExtension(fupFormLogo.PostedFile.FileName).ToLower()

                Dim existeError = False

                If Not fileExt = ".png" Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Logo -> Tipo de archivo no valido", validationGroup)
                End If

                Dim imgLogo As New Bitmap(fupFormLogo.FileContent)

                If fupFormLogo.FileContent.Length > logoSize Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Logo -> Tama�o de archivo no valido", validationGroup)
                End If

                If imgLogo.Width > logoWith OrElse imgLogo.Height <> logoHeight Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Logo -> Dimensiones de archivo no validos", validationGroup)
                End If

                If Not existeError Then
                    _vformLogoNombre = String.Format("{0}/{1}{2}", directoryPath, formLogoNombre, fileExt)
                End If
            End If

            If fupFormLogoResponsive.HasFile Then
                Dim fileExt As String = Path.GetExtension(fupFormLogoResponsive.PostedFile.FileName).ToLower()

                Dim existeError = False

                If Not fileExt = ".png" Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Logo Responsive -> Tipo de archivo  no valido", validationGroup)
                End If

                Dim imgLogoResponsive As New Bitmap(fupFormLogoResponsive.FileContent)

                If fupFormLogoResponsive.FileContent.Length > logoResponsiveSize Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Logo Responsive -> Tama�o de archivo no valido", validationGroup)
                End If

                If imgLogoResponsive.Width > logoResponsiveWith OrElse imgLogoResponsive.Height > logoResponsiveHeight Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Logo Responsive -> Dimensiones de archivo no validos", validationGroup)
                End If

                If Not existeError Then
                    _vformLogoResponsiveNombre = String.Format("{0}/{1}{2}", directoryPath, formLogoResponsiveNombre, fileExt)
                End If
            End If

            If fupFormBackground.HasFile Then
                Dim fileExt As String = Path.GetExtension(fupFormBackground.PostedFile.FileName).ToLower()

                Dim existeError = False

                Dim fileExts() As String = {".jpg", ".jpeg"}

                If Not fileExts.Any(Function(i) i = fileExt) Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Background -> Tipo de archivo no valido", validationGroup)
                End If

                Dim imgBackground As New Bitmap(fupFormBackground.FileContent)

                If fupFormBackground.FileContent.Length > bgSize Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Background -> Tama�o de archivo no valido", validationGroup)
                End If

                If imgBackground.Width > bgWith OrElse imgBackground.Height > bgHeight Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Background -> Dimensiones de archivo no validos", validationGroup)
                End If

                If Not existeError Then
                    _vformBackgroundNombre = String.Format("{0}/{1}{2}", directoryPath, formBackgroundNombre, fileExt)
                End If
            End If

            If fupFormBackgroundResponsive.HasFile Then
                Dim fileExt As String = Path.GetExtension(fupFormBackgroundResponsive.PostedFile.FileName).ToLower()

                Dim existeError = False

                Dim fileExts() As String = {".jpg", ".jpeg"}

                If Not fileExts.Any(Function(i) i = fileExt) Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Background Responsive -> Tipo de archivo no valido", validationGroup)
                End If

                Dim imgBackgroundResponsive As New Bitmap(fupFormBackgroundResponsive.FileContent)

                If fupFormBackgroundResponsive.FileContent.Length > bgResponsiveSize Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Background Responsive -> Tama�o de archivo no valido", validationGroup)
                End If

                If imgBackgroundResponsive.Width > bgResponsiveWith OrElse imgBackgroundResponsive.Height > bgResponsiveHeight Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Formulario: Background Responsive -> Dimensiones de archivo no validos", validationGroup)
                End If

                If Not existeError Then
                    _vformBackgroundResponsiveNombre = String.Format("{0}/{1}{2}", directoryPath, formBackgroundResponsiveNombre, fileExt)
                End If
            End If


            'PEEXPRESS
            If fupExpressLogo.HasFile Then
                Dim fileExt As String = Path.GetExtension(fupExpressLogo.PostedFile.FileName).ToLower()

                Dim existeError = False

                If Not fileExt = ".png" Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Logo Integracion Plantilla: -> Tipo de archivo no valido", validationGroup)
                End If

                Dim imgLogo As New Bitmap(fupExpressLogo.FileContent)

                If fupExpressLogo.FileContent.Length > logoSize Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Logo Integracion Plantilla: -> Tama�o de archivo no valido", validationGroup)
                End If

                If imgLogo.Width <> logoWithExpress OrElse imgLogo.Height <> logoHeightExpress Then
                    existeError = True
                    AddValidationSummaryItem(Me.Page, "Logo Integracion Plantilla: -> Dimensiones de archivo no validos", validationGroup)
                End If

                If Not existeError Then
                    _vExpressLogoNombre = String.Format("{0}/{1}{2}", directoryPath, ExpressLogoNombre, fileExt)
                End If
            End If
            'PEEXPRESS


            If (_3Dev.FW.Web.PageManager.IsInserting) Then
                _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagenesFormulario")
            ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
                _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagenesFormulario")
            End If

            Return True
        Catch ex As Exception
            _logger.Error(ex, "Validaci�n de imagenes de formulario fallida")
            Return False
        End Try

    End Function

    Public Function ValidarImagenesCobroMasivo() As Boolean
        Try
            Dim cobroMasivo As String = ConfigurationManager.AppSettings("COBROMASIVO")
            Dim logoWith = 250
            Dim logoHeight = 90
            Dim logoSize = 200000
            Dim validationGroup = "GrupoValidacion"
            Dim existeError = True

            _logger.Debug("Inicia validacion de Logo de cobro masivo")

            If dropCanalVenta.SelectedValue <> cobroMasivo Then Return True

            _logger.Debug("Valor hdnLogoCobranza [ValidarImagenesCobroMasivo]: " + hdnLogoCobranza.Value)
            _logger.Debug("Valor fuCargaLogoCobranza [ValidarImagenesCobroMasivo]: " + fuCargaLogoCobranza.FileName)

            'If _3Dev.FW.Web.PageManager.IsEditing And String.IsNullOrEmpty(fuCargaLogoCobranza.FileName) Then Return True
            If Not String.IsNullOrEmpty(hdnLogoCobranza.Value) And String.IsNullOrEmpty(fuCargaLogoCobranza.FileName) Then Return True

            If Path.GetExtension(fuCargaLogoCobranza.PostedFile.FileName).ToLower() <> ".png" Then
                existeError = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Logo -> Tipo de archivo no valido", validationGroup)
            End If

            Dim imgLogo As New Bitmap(fuCargaLogoCobranza.FileContent)

            If fuCargaLogoCobranza.FileContent.Length > logoSize Then
                existeError = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Logo -> Tama�o de archivo no valido", validationGroup)
            End If

            If imgLogo.Width > logoWith OrElse imgLogo.Height > logoHeight Then
                existeError = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Logo -> Dimensiones de archivo no validos", validationGroup)
            End If

            Dim resultado As Boolean = RegistrarLogo(fuCargaLogoCobranza.FileContent, fuCargaLogoCobranza.FileName)
            If Not resultado Then
                existeError = False
                AddValidationSummaryItem(Me.Page, "Cobros Masivos: Logo Cobranza -> No se puede registrar el logo", "GrupoValidacion")
            End If

            If (_3Dev.FW.Web.PageManager.IsInserting) Then
                _logger.Debug("REGISTRAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagenesCobroMasivo")
            ElseIf (_3Dev.FW.Web.PageManager.IsEditing) Then
                _logger.Debug("ACTUALIZAR -> " & txtCodigo.Text.Trim & ": se ejecuto ValidarImagenesCobroMasivo")
            End If

            Return existeError
        Catch ex As Exception
            _logger.Error(ex, "Validaci�n de imagenes de formulario fallida")
            Return False
        End Try
    End Function

    Protected Sub SubirClavesparaNuevoApi()
        Try
            Dim rutaClavePublica = txtRutaClavePub.Text
            Dim publicKey = fuClavePublica.FileBytes

            Dim rutaClavePrivate = txtRutaClavePriv.Text
            Dim privateKey = fuClavePrivada.FileBytes

            If publicKey.Length > 0 Then
                File.WriteAllBytes(rutaClavePublica, publicKey)
            End If
            If publicKey.Length > 0 Then
                File.WriteAllBytes(rutaClavePrivate, privateKey)
            End If
            _logger.Debug("Se grab� las llaves p�blica y privada correctamente.")
        Catch ex As Exception
            _logger.Error(ex, "Error al intentar grabar llaves p�blica y privada: " + ex.Message)
        End Try
    End Sub

    Protected Sub SubirImagenesFormulario()

        Try

            If txtCodigo.Text.Trim().Length < 3 Then
                Exit Sub
            End If

            Dim amazonS3 As New AWS.AmazonS3

            If fupFormLogo.HasFile Then
                Dim imgLogo As New Bitmap(fupFormLogo.FileContent)
                Dim fileNameLogo = _vformLogoNombre

                Dim streamLogo = New System.IO.MemoryStream()
                imgLogo.Save(streamLogo, ImageFormat.Png)
                streamLogo.Position = 0

                amazonS3.UploadFileFromStream(streamLogo, fileNameLogo)

                _logger.Debug("Se grabo logo de formulario")
            End If

            If fupFormLogoResponsive.HasFile Then
                Dim imgLogoResponsive As New Bitmap(fupFormLogoResponsive.FileContent)
                Dim fileNameLogoResponsive = _vformLogoResponsiveNombre

                Dim streamLogoResponsive = New System.IO.MemoryStream()
                imgLogoResponsive.Save(streamLogoResponsive, ImageFormat.Png)
                streamLogoResponsive.Position = 0

                amazonS3.UploadFileFromStream(streamLogoResponsive, fileNameLogoResponsive)

                _logger.Debug("Se grabo logo responsive de formulario")
            End If

            If fupFormBackground.HasFile Then
                Dim imgBackground As New Bitmap(fupFormBackground.FileContent)
                Dim fileNameBackgroud = _vformBackgroundNombre

                Dim streamBackgroud = New System.IO.MemoryStream()
                imgBackground.Save(streamBackgroud, ImageFormat.Jpeg)
                streamBackgroud.Position = 0

                amazonS3.UploadFileFromStream(streamBackgroud, fileNameBackgroud)

                _logger.Debug("Se grabo background header de formulario")
            End If

            If fupFormBackgroundResponsive.HasFile Then
                Dim imgBackgroundResponsive As New Bitmap(fupFormBackgroundResponsive.FileContent)
                Dim fileNameBackgroudResponsive = _vformBackgroundResponsiveNombre

                Dim streamBackgroudResponsive = New System.IO.MemoryStream()
                imgBackgroundResponsive.Save(streamBackgroudResponsive, ImageFormat.Jpeg)
                streamBackgroudResponsive.Position = 0

                amazonS3.UploadFileFromStream(streamBackgroudResponsive, fileNameBackgroudResponsive)

                _logger.Debug("Se grabo background header responsive de formulario")
            End If


            'PEEXPRESS
            If fupExpressLogo.HasFile Then
                Dim imgBackgroundResponsive As New Bitmap(fupExpressLogo.FileContent)
                Dim fileNameExpress = _vExpressLogoNombre

                Dim streamBackgroudResponsive = New System.IO.MemoryStream()
                imgBackgroundResponsive.Save(streamBackgroudResponsive, ImageFormat.Png)
                streamBackgroudResponsive.Position = 0

                amazonS3.UploadFileFromStream(streamBackgroudResponsive, fileNameExpress)

                _logger.Debug("Se grabo logo Plantilla")
            End If

        Catch ex As Exception
            _logger.Error(ex, "Carga de imagenes de formulario fallida")
        End Try

    End Sub

    Public Sub SubirClavesFormulario()

        Dim codigoServicio = txtCodigo.Text.TrimEnd()

        Try

            Dim s3AccessKey As String = ConfigurationManager.AppSettings("FORM_CLAVES_S3_ACCESS_KEY")
            Dim s3SecretAccessKey As String = ConfigurationManager.AppSettings("FORM_CLAVES_S3_SECRET_ACCESS_KEY")
            Dim s3Region As String = ConfigurationManager.AppSettings("FORM_CLAVES_S3_REGION")
            Dim s3BucketName As String = ConfigurationManager.AppSettings("FORM_CLAVES_S3_BUCKET_NAME")
            Dim s3DirectoryPath As String = ConfigurationManager.AppSettings("FORM_CLAVES_S3_DIRECTORY_PATH")

            Dim amazonS3 As New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

            If fuClavePublica.FileBytes.Length > 0 AndAlso txtRutaClavePub.Text.Trim <> "" Then
                Dim rutaClavePublica = txtRutaClavePub.Text.Replace("\\", "\")
                Dim nombreClavePublica = Path.GetFileName(rutaClavePublica)
                Dim publicKey = New MemoryStream(fuClavePublica.FileBytes)

                amazonS3.UploadFileFromStream(publicKey, nombreClavePublica)

                _logger.Debug(codigoServicio + ": Se grabo clave publica de formulario")
            End If

            If fuClavePrivada.FileBytes.Length > 0 AndAlso txtRutaClavePriv.Text.Trim <> "" Then
                Dim rutaClavePrivate = txtRutaClavePriv.Text.Replace("\\", "\")
                Dim nombreClavePrivada = Path.GetFileName(rutaClavePrivate)
                Dim privateKey = New MemoryStream(fuClavePrivada.FileBytes)

                amazonS3.UploadFileFromStream(privateKey, nombreClavePrivada)

                _logger.Debug(codigoServicio + ": Se grabo clave privada de formulario")
            End If

        Catch ex As Exception
            _logger.Error(ex, codigoServicio + ": Error al intentar cargar claves de formulario a S3")
        End Try

    End Sub

    Public Shared Sub AddValidationSummaryItem(ByVal page As Page, ByVal errorMessage As String, ByVal validationGroup As String)
        Dim validator = New CustomValidator()
        validator.IsValid = False
        validator.ErrorMessage = errorMessage
        validator.ValidationGroup = validationGroup
        page.Validators.Add(validator)
    End Sub

    Protected Sub dropCanalVenta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dropCanalVenta.SelectedIndexChanged
        Dim formulario As String = ConfigurationManager.AppSettings("FORMULARIO")
        Dim cobroMasivo As String = ConfigurationManager.AppSettings("COBROMASIVO")

        If dropCanalVenta.SelectedValue = formulario Then
            EnabledMaintenanceControlsForm(True)
            MaintenanceControlsCharge(False)
            Exit Sub
        End If

        If dropCanalVenta.SelectedValue = cobroMasivo Then
            MaintenanceControlsCharge(True)
            EnabledMaintenanceControlsForm(False)
            Exit Sub
        End If

        EnabledMaintenanceControlsForm(False)
        MaintenanceControlsCharge(False)
    End Sub

    Protected Sub chkEnvMailOpe_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEnvMailOpe.CheckedChanged
        If dropCanalVenta.SelectedValue = ConfigurationManager.AppSettings("COBROMASIVO") Then
            txtMailOperadorCobranza.Enabled = chkEnvMailOpe.Checked
            txtMailOperadorCobranza.Enabled = chkEnvMailOpe.Checked
            revMailOperadorCobranza.Enabled = chkEnvMailOpe.Checked
            rfvMailOperadorCobranza.Enabled = chkEnvMailOpe.Checked
        End If
    End Sub

    Protected Sub chkNotifExpiracionCIP_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNotifExpiracionCIP.CheckedChanged
        txtTiempoAvisoExpiracion.Enabled = chkNotifExpiracionCIP.Checked
        ftxtTiempoAvisoExpiracion.Enabled = chkNotifExpiracionCIP.Checked
        regExpTiempoAvisoExpiracion.Enabled = chkNotifExpiracionCIP.Checked
        rfvTiempoAvisoExpiracion.Enabled = chkNotifExpiracionCIP.Checked
    End Sub

    Protected Sub chkEnviarExpiracionCipCobros_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEnviarExpiracionCipCobros.CheckedChanged
        txtAvisoExpiracionCobranza.Enabled = chkEnviarExpiracionCipCobros.Checked
        rfvAvisoExpiracionCobranza.Enabled = chkEnviarExpiracionCipCobros.Checked
        fteAvisoExpiracionCobranza.Enabled = chkEnviarExpiracionCipCobros.Checked
        revAvisoExpiracionCobranza.Enabled = chkEnviarExpiracionCipCobros.Checked
    End Sub
End Class