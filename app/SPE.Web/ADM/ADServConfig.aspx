<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="ADServConfig.aspx.vb" Inherits="ADM_ADServConfig" title="PagoEfectivo - Actualizar Configuración de Servicios" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Page">
    <h1>
        <asp:ScriptManager id="ScriptManager" runat="server"></asp:ScriptManager>        
        <asp:Label id="lblTitulo" runat="server" Text="Actualizar Configuración de Servicios"></asp:Label>
    </h1>
    <div id="divInfGeneral" class="divContenedor">
      <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" > 
          1. Configuraci&oacute;n del Contrato de Servicios
      </div>
     <fieldset  class="ContenedorEtiquetaTexto stripe">
        <div id="div2" class="EtiquetaTextoIzquierda" >
            <asp:Label ID="Label1" CssClass="EtiquetaLargo" runat="server" Text="Código Acceso:"></asp:Label>
            <asp:TextBox ID="txtCodigoAcceso" CssClass="TextoLargo" runat="server" MaxLength="250" ></asp:TextBox>
        </div>
     </fieldset> 	          
      <fieldset  class="ContenedorEtiquetaTexto stripe">
        <div id="div1" class="EtiquetaTextoIzquierda" >
            <asp:Button ID="btnConsultar" runat="server" Text="Refrescar"  CssClass="Boton" />
        </div>
      </fieldset>
      <fieldset  class="ContenedorEtiquetaTexto stripe">
        <div id="div4" class="EtiquetaTextoIzquierda" >
           <asp:TextBox ID="txtContratoXML" TextMode="MultiLine" CssClass="TextoLargo" runat="server" Height="800px" Width="600px"></asp:TextBox>
        </div>
      </fieldset>
    </div>
    <fieldset id="fsBotonera" class="ContenedorBotonera" style="width:auto"	>
        <asp:Label ID="lblMensaje" CssClass="MensajeTransaccion"  runat="server" Height="25px" style="padding-top: 0px"></asp:Label>
        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="GrupoValidacion" runat="server" />    
        <br />    
        <asp:Button ID="btnActualizar" runat="server"  Text="Actualizar"  ValidationGroup="GrupoValidacion" CssClass="Boton" />
    </fieldset>  
    <br />               
    <br />               
  <div id="div3" class="divContenedor">
      <div id="div5" class="divContenedorTitulo" > 
          2. Configuraci&oacute;n del Emails
      </div>  
      <fieldset  class="ContenedorEtiquetaTexto stripe">
           <div id="div8" class="EtiquetaTextoIzquierda" >
                <asp:Label ID="lblServicio" CssClass="EtiquetaLargo" runat="server" Text="Servicio:"></asp:Label>
                <asp:DropDownList ID="dropServicio" CssClass="TextoComboExtraLargo"  runat="server"></asp:DropDownList>
           </div>
      </fieldset>     
      <fieldset  class="ContenedorEtiquetaTexto stripe">
           <div id="div6" class="EtiquetaTextoIzquierda" >
                <asp:Label ID="lblTipoPlantilla" CssClass="EtiquetaLargo" runat="server" Text="Tipo Plantilla:"></asp:Label>
                <asp:DropDownList ID="dropTipoPlantilla" CssClass="TextoComboExtraLargo"  runat="server">
                    <asp:ListItem Text="EmailGenCIP" Value="EmailGenCIP"></asp:ListItem>
                    <asp:ListItem Text="EmailCanCIP" Value="EmailCanCIP"></asp:ListItem>
                </asp:DropDownList>
           </div>
      </fieldset>
      <fieldset  class="ContenedorEtiquetaTexto stripe">
        <div id="div9" class="EtiquetaTextoIzquierda" >
            <asp:Button ID="btnConsultarPlantilla" runat="server" Text="Cons. Plantilla"  CssClass="Boton" />
        </div>
      </fieldset>     
      <fieldset  class="ContenedorEtiquetaTexto stripe">
         <div id="div10" class="EtiquetaTextoIzquierda" >
            <asp:Label ID="lblPlantillaDescripcion" CssClass="EtiquetaLargo" runat="server" Text="Plantilla Desc.:"></asp:Label>
            <asp:TextBox ID="txtPlantillaDescripcion"  TextMode="SingleLine"  CssClass="TextoLargo SoloLectura" ReadOnly="true" runat="server" ></asp:TextBox>
         </div>
      </fieldset>            
      <fieldset  class="ContenedorEtiquetaTexto stripe">
         <div id="div7" class="EtiquetaTextoIzquierda" >
            <asp:TextBox ID="txtPlantillaHtml"  TextMode="MultiLine" CssClass="TextoLargo "   runat="server" Height="800px" Width="600px"></asp:TextBox>
            <asp:HiddenField ID="hdnIdPlantilla" runat="server"   />
         </div>
      </fieldset>      
  </div>
  <fieldset id="Fieldset1" class="ContenedorBotonera" style="width:auto"	>
        <asp:Label ID="lblPlantillaMensaje" CssClass="MensajeTransaccion"  runat="server" Height="25px" style="padding-top: 0px"></asp:Label>
        
        <asp:Button ID="btnActualizarPlantilla" runat="server"  Text="Act. Plantilla" CssClass="Boton" />           
  </fieldset>

</div>




</asp:Content>

