
Imports SPE.Web
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Util

Partial Class ADM_ADServConfig
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CServicio)

    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Try
            Using oCntrl As New CServicio
                oCntrl.GuardarContratoXMLDeServicios(txtCodigoAcceso.Text, txtContratoXML.Text)
                lblMensaje.Text = "La operaci�n ha sido realizada"
            End Using
        Catch ex As Exception
            lblMensaje.Text = "Error : " + ex.Message
        End Try
    End Sub

    Protected Sub btnConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Try
            Using oCntrl As New CServicio
                txtContratoXML.Text = oCntrl.ConsultarContratoXMLDeServicios(txtCodigoAcceso.Text.TrimEnd())
            End Using
        Catch ex As Exception
            lblMensaje.Text = "Error : " + ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Using ocntrlServicio As New CServicio()
                Dim objbeservicio As New BEServicio()
                objbeservicio.NombreEmpresaContrante = ""
                objbeservicio.Nombre = ""
                objbeservicio.IdEstado = 0
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(dropServicio, ocntrlServicio.SearchByParameters(objbeservicio), "Nombre", "IdServicio")
            End Using
        End If
    End Sub

    Protected Sub btnConsultarPlantilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarPlantilla.Click
        Using cntrlPlantilla As New CPlantilla()
            Dim idServicio As Integer = ObjectToInt(dropServicio.SelectedValue)
            Dim codPlantillaTipo As String = dropTipoPlantilla.SelectedValue
            Dim obePlantilla As BEPlantilla = cntrlPlantilla.ConsultarPlantillaPorTipoYVariacion(idServicio, codPlantillaTipo, SPE.EmsambladoComun.ParametrosSistema.Plantilla.TipoVariacion.Standar)
            If (obePlantilla IsNot Nothing) Then
                txtPlantillaHtml.Text = obePlantilla.PlantillaHtml
                txtPlantillaDescripcion.Text = obePlantilla.Descripcion
                hdnIdPlantilla.Value = obePlantilla.IdPlantilla
            End If
        End Using
    End Sub

    Protected Sub btnActualizarPlantilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarPlantilla.Click
        Try
            If ((hdnIdPlantilla.Value <> "") And (dropServicio.SelectedValue <> "") And (dropTipoPlantilla.SelectedValue <> "")) Then
                Using cntrlPlantilla As New CPlantilla()
                    Dim obePlantilla As New BEPlantilla()
                    obePlantilla.PlantillaHtml = txtPlantillaHtml.Text
                    obePlantilla.IdPlantilla = ObjectToInt(hdnIdPlantilla.Value)
                    obePlantilla.IdUsuarioActualizacion = UserInfo.IdUsuario
                    cntrlPlantilla.UpdateRecord(obePlantilla)
                End Using
                lblPlantillaMensaje.Text = String.Format("Se actualiz� la plantilla - {0} del tipo {1} ", txtPlantillaDescripcion.Text, dropTipoPlantilla.SelectedValue)
            Else
                lblPlantillaMensaje.Text = "Debe seleccionar al menos un servicio y un tipo de Plantilla."
            End If

        Catch ex As Exception
            lblPlantillaMensaje.Text = ex.Message
        End Try

    End Sub
End Class

