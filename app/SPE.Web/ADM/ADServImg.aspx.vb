Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Partial Class ADM_ADServImg
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CServicio)

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CServicio()
    'End Function



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not Context.Request.QueryString("servicioid") Is Nothing Then
                Dim idimagen As Integer = Convert.ToInt32(Context.Request.QueryString("servicioid"))

                Dim cServicio As SPE.Web.CServicio = CType(ControllerObject, SPE.Web.CServicio)
                Dim obeServicio As BEServicio = cServicio.GetRecordByID(idimagen)
                ''  obeServicio.LogoImagen

                If (Not obeServicio.LogoImagen Is Nothing) And (obeServicio.LogoImagen.Length > 0) Then
                    Response.ContentType = "image/jpeg"
                    Response.Expires = 0
                    Response.Buffer = True
                    Response.Clear()
                    Response.BinaryWrite(obeServicio.LogoImagen)
                    Response.End()
                End If

            End If




        End If
    End Sub


End Class
