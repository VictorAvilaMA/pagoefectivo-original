﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    Async="true" CodeFile="AdDepo.aspx.vb" Inherits="ADM_AdDepo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery-ui-1.8.13.custom.min.js"
        type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/ui.dropdownchecklist-1.4-min.js"
        type="text/javascript"></script>
    <style type="text/css">
        ul.datos_cip2.f0.row-servicio
        {
            z-index: 100011;
        }
        /*ul.datos_cip2.f0.row-servicio li.t2.f0
        {
            z-index: 100011;
        }*/
        ul.datos_cip2.f0.row-servicio li.t2.f0 .ui-dropdownchecklist.ui-dropdownchecklist-dropcontainer-wrapper.ui-widget
        {
            z-index: 100011;
            width: 300px !important;
            height: 150px !important;
        }
        ul.datos_cip2.f0.row-servicio li.t2.f0 .ui-dropdownchecklist.ui-dropdownchecklist-dropcontainer-wrapper.ui-widget .ui-dropdownchecklist-dropcontainer.ui-widget-content
        {
            z-index: 100011;
            height: 150px !important;
        }
        #ctl00_ContentPlaceHolder1_upnlListaEmprContrantes
        {
            z-index: 100010;
            position: relative;
        }
        #ddcl-slcServicios, #ddcl-slcMedioPagos
        {
            width: auto;
        }
        .ui-dropdownchecklist-selector
        {
            width: 300px !important;
        }
        .ui-dropdownchecklist-selector, .ui-dropdownchecklist-text
        {
            height: 20px !important;
            text-align: left !important;
            margin-top: 6px !important;
            padding: 0 !important;
            margin: 0 !important;
            line-height: 20px !important; /*position: relative !important;*/
            float: none !important;
            overflow: hidden !important;
            font-family: MS Shell Dlg;
            font-size: 13.3333px !important;
        }
        .ui-dropdownchecklist-dropcontainer-wrapper
        {
            top: 21px !important;
        }
        span.ui-dropdownchecklist-text
        {
            width: 297px !important;
        }
        .ui-dropdownchecklist-item
        {
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Deposito"></asp:Literal>
    </h2>
    <div class="conten_pasos3" style="padding-bottom: 100px">
        <asp:Panel ID="pnlDatosDeposito" runat="server" CssClass="inner-col-right">
            <h4>
                1. Datos del dep&oacute;sito</h4>
            <asp:UpdatePanel ID="updMontoPrecalculado" runat="server">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Fecha deposito: </li>
                        <li class="t2" style="line-height: 1">
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaDeposito" runat="server" CssClass="normal" MaxLength="100"
                                    ValidationGroup="ValidarRegistro">
                                </asp:TextBox>
                                <asp:ImageButton ID="ibtnFechaDeposito" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                <cc1:MaskedEditExtender ID="MaskedEditExtenderDeposito" runat="server" CultureName="es-PE"
                                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaDeposito">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaDeposito"
                                    PopupButtonID="ibtnFechaDeposito" Format="dd/MM/yyyy" PopupPosition="Right">
                                </cc1:CalendarExtender>
                                <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDeposito"
                                    ControlToValidate="txtFechaDeposito" Display="Dynamic" EmptyValueBlurredText="*"
                                    EmptyValueMessage="Fecha deposito es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                    InvalidValueMessage="Fecha Deposito no válida" IsValidEmpty="False" ValidationGroup="ValidarRegistro">
                                </cc1:MaskedEditValidator>
                        </li>
                        <li id="liNroTransDesc" runat="server" class="t1"><span class="color">>></span> Nro
                            Transacci&oacute;n (SBK): </li>
                        <li id="liNroTransVal" runat="server" class="t2">
                            <asp:TextBox ID="txtNroTransaccion" runat="server" CssClass="normal" MaxLength="50"
                                ValidationGroup="ValidarRegistro">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNrotransaccion" runat="server" ErrorMessage="Debe ingresar el Nro. de Transaccion."
                                ControlToValidate="txtNroTransaccion" ValidationGroup="ValidarRegistro">*</asp:RequiredFieldValidator>
                        </li>
                        <li id="liCodOperacionDesc" runat="server" class="t1" visible="false"><span class="color">
                            >></span> Codigo Operaci&oacute;n (FC,WU): </li>
                        <li id="liCodOperacionVal" runat="server" class="t2" visible="false">
                            <asp:TextBox ID="txtCodigoOperacion" runat="server" CssClass="normal" MaxLength="50"
                                ValidationGroup="ValidarRegistro">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvCodigoOperacion" runat="server" ErrorMessage="Debe ingresar el codigo Operacion."
                                ControlToValidate="txtCodigoOperacion" ValidationGroup="ValidarRegistro">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span> Comentarios: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtComentarios" runat="server" CssClass="normal" MaxLength="50"
                                ValidationGroup="ValidarRegistro">
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">>></span> Monto depositado: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtMontoDeposito" runat="server" CssClass="normal" MaxLength="17"
                                ValidationGroup="ValidarRegistro">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftbMontoDeposito" runat="server" TargetControlID="txtMontoDeposito"
                                ValidChars=".0123456789">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="rfvMontoDeposito" runat="server" ErrorMessage="Debe ingresar el Monto de deposito."
                                ControlToValidate="txtMontoDeposito" ValidationGroup="ValidarRegistro">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <ul class="datos_cip2 t0" id="ulMontoPrecalculado" runat="server" visible="false">
                        <li class="t1" style="color: Blue"><span class="color">>></span> Monto Pre calculado:
                        </li>
                        <li class="t2" style="color: Blue">
                            <asp:Literal ID="lblMontoPreCalculado" runat="server"></asp:Literal>
                        </li>
                    </ul>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCancelarOp" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="imgbtnRegresar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnAceptar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlBanco" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <div style="clear: both">
            </div>
            <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" CssClass="Summary_datosCip2">
            </asp:ValidationSummary>
            <div style="clear: both">
            </div>
            <h4>
                2. Busqueda de Arch. Conc.</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Banco: </li>
                <li class="t2">
                    <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu" AutoPostBack="true">
                    </asp:DropDownList>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> Del:</li>
                <li class="t2" style="line-height: 1; z-index: 200;">
                    <p class="input_cal">
                        <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                        <asp:ImageButton CssClass="calendario" ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                        </asp:ImageButton>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                            InvalidValueMessage="Fecha 'Del' no válida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                            EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                            ControlExtender="mdeDel"></cc1:MaskedEditValidator>
                    </p>
                    <span class="entre">al: </span>
                    <p class="input_cal">
                        <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                            Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                            Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:ImageButton CssClass="calendario" ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                        </asp:ImageButton>
                        <cc1:MaskedEditValidator CssClass="izq" ID="MaskedEditValidatorAl" runat="server"
                            IsValidEmpty="False" InvalidValueMessage="Fecha 'Al' no válida" InvalidValueBlurredMessage="*"
                            EmptyValueMessage="Fecha 'Al' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaA"
                            ControlExtender="mdeA">*
                        </cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de búsqueda no es válido"
                            ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                            Type="Date">*</asp:CompareValidator>
                    </p>
                </li>
                <li class="mensaje">
                    <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                </li>
                <li class="complet">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" class="input_azul3" />
                    <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" class="input_azul4"
                        ValidationGroup="ValidarRegistro" OnClientClick="return ConfirmMe();" />
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" class="input_azul4" />
                </li>
            </ul>
            <div style="clear: both">
            </div>
            <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
                Format="dd/MM/yyyy">
            </cc1:CalendarExtender>
            <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
                Format="dd/MM/yyyy">
            </cc1:CalendarExtender>
            <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
                Mask="99/99/9999" CultureName="es-PE">
            </cc1:MaskedEditExtender>
            <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
                Mask="99/99/9999" CultureName="es-PE">
            </cc1:MaskedEditExtender>
            <div style="clear: both;">
            </div>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" class="result" visible="false">
                        <h5 id="h5Resultado" runat="server">
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <div style="clear: both;">
                        </div>
                        <asp:GridView ID="gvResultado" DataKeyNames="IdConciliacionArchivo,IdBanco,CodigoBanco"
                            runat="server" CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False"
                            OnRowCommand="gvResultado_RowCommand" OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="Ver">
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                            ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/ok2.jpg" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/Images_Buton/ok2.jpg" SelectText="Ver"
                                    ShowSelectButton="True" HeaderText="Sel.">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>--%>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIdConciliacionArchivo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdConciliacionArchivo") %>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Fecha" DataFormatString="{0:d}" SortExpression="Fecha"
                                    HeaderText="Fecha">
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NombreArchivo" SortExpression="NombreArchivo" HeaderText="Nombre Archivo">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CantCIPs" HeaderText="Cant.CIPs">
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescTipoConciliacion" SortExpression="DescTipoConciliacion"
                                    HeaderText="Tipo">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescOrigenConciliacion" SortExpression="DescOrigenConciliacion"
                                    HeaderText="Origen">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Observacion" SortExpression="Observacion" HeaderText="Observacion">
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="hdfIdConc" runat="server" />
                    <asp:HiddenField ID="hdfIdConcEnt" runat="server" />
                    <asp:HiddenField ID="hdfCodBanco" runat="server" />
                    <asp:HiddenField ID="hdfIdConciliacionArchivo" runat="server" />
                    <asp:HiddenField ID="hdfIdServicios" runat="server" />
                    <asp:HiddenField ID="hdfServicios" runat="server" />
                    <asp:HiddenField ID="hdfServiciosFormato" runat="server" />
                    <asp:HiddenField ID="hdfIdMedioPagos" runat="server" />
                    <asp:HiddenField ID="hdfMedioPagos" runat="server" />
                    <asp:HiddenField ID="hdfMedioPagosFormato" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnAceptar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnCancelarOp" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="imgbtnRegresar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <div style="clear: both">
    </div>
    <asp:UpdatePanel ID="upnlListaEmprContrantes" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc1:ModalPopupExtender ID="mppOperacionesArchivo" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="ImageButton1" PopupControlID="pnlPopupOrdenesArchivo" TargetControlID="ImageButton2"
                Drag="true" Y="0">
            </cc1:ModalPopupExtender>
            <asp:ImageButton ID="ImageButton1" ImageUrl="" CausesValidation="false" Visible="true"
                CssClass="Hidden" runat="server" Style="display: none"></asp:ImageButton>
            <asp:ImageButton ID="ImageButton2" runat="server" 
            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
            CssClass="Hidden"
                ToolTip="Buscar Empresa" Style="display: none" />
            <asp:Panel ID="pnlPopupOrdenesArchivo" runat="server" Style="display: none; background: white;
                max-height: 500px; min-height: 0px" CssClass="conten_pasos3">
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar"
                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                    CausesValidation="false"
                        runat="server"></asp:ImageButton>
                </div>
                <h4>
                    3. Busqueda de Operaciones</h4>
                <div style="clear: both">
                </div>
                <ul class="datos_cip2 f0 row-servicio">
                    <li class="t1 f0"><span class="color">&gt;&gt; </span>C&oacute;digos de servicio:
                    </li>
                    <li class="t2 f0">
                        <asp:Literal ID="ddlCodigoServicio" runat="server"></asp:Literal>
                    </li>
                    <li id="liMediosPagoDesc" runat="server" class="t1 f0"><span class="color">&gt;&gt;
                    </span>Medios de pago: </li>
                    <li id="liMediosPago" runat="server" class="t2 f0">
                        <asp:Literal ID="ddlMedioPagos" runat="server"></asp:Literal>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <ul id="Ul1" class="datos_cip2">
                    <li id="Li2" class="complet">
                        <asp:Button ID="btnBuscarOp" runat="server" Text="Buscar" class="input_azul5" />
                        <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" class="input_azul4" />
                        <asp:Button ID="btnCancelarOp" runat="server" Text="Cancelar" class="input_azul4" />
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <div class="result" style="margin: 10px; overflow: auto; height: 300px">
                    <h5 id="h1" runat="server">
                        <asp:Literal ID="lblResultadoDetalle" runat="server" Text=""></asp:Literal>
                    </h5>
                    <div style="clear: both;">
                    </div>
                    <asp:GridView ID="gvOperaciones" runat="server" CssClass="grilla" AllowPaging="True"
                        AutoGenerateColumns="False" PageSize="10">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbSeleccionar" runat="server" OnCheckedChanged="cbSeleccionar_CheckedChanged"
                                        Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdOrdenPago" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdOrdenPago") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagSeleccionado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FlagSeleccionado") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaConciliacion" DataFormatString="{0:d}" SortExpression="Fecha"
                                HeaderText="Fecha">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NumeroOrdenPago" SortExpression="CIP" HeaderText="CIP">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaCancelacion" DataFormatString="{0:d}" SortExpression="FechaCancelacion"
                                HeaderText="Fecha Cancelacion">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ArchivoConciliacion" SortExpression="NombreArchivo" HeaderText="Nombre Archivo">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="Estado" HeaderText="Estado">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Total" SortExpression="Total" HeaderText="Monto">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Comision" SortExpression="Comision" HeaderText="Comision">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function cargarMultiselect() {
            $("#slcServicios").dropdownchecklist({ maxDropHeight: 150, width: 216, emptyText: '::Todos::',
                onComplete: function (selector) {
                    var hdfIdServicio = $('#<%= hdfIdServicios.ClientID() %>');
                    hdfIdServicio.val('');
                    for (i = 0; i < selector.options.length; i++) {
                        if (selector.options[i].selected) {
                            hdfIdServicio.val(hdfIdServicio.val() + "," + selector.options[i].text);
                        }
                    }
                }
            });
            $("#slcMedioPagos").dropdownchecklist({ maxDropHeight: 150, width: 216, emptyText: '::Todos::',
                onComplete: function (selector) {
                    var hdfIdMedioPagos = $('#<%= hdfIdMedioPagos.ClientID() %>');
                    hdfIdMedioPagos.val('');
                    for (i = 0; i < selector.options.length; i++) {
                        if (selector.options[i].selected) {
                            hdfIdMedioPagos.val(hdfIdMedioPagos.val() + "," + selector.options[i].value);
                        }
                    }
                }
            });
        }
        $(document).ready(function () {
            cargarMultiselect();
            $('#<%=txtMontoDeposito.ClientID%>').blur(function () {
                if ($('#<%=txtMontoDeposito.ClientID%>').val() != '')
                    $('#<%=txtMontoDeposito.ClientID%>').val(parseFloat($('#<%=txtMontoDeposito.ClientID%>').val()).toFixed(2));
            });
        });
        function AjaxEnd(sender, args) {
            $(".divLoadingMensaje").hide('fast');
            $(".divLoading").hide('fast');
            if ($(".ui-dropdownchecklist").val() == undefined) {
                var selector = $("#slcServicios");
                var hdfIdServicio = $('#<%= hdfIdServicios.ClientID() %>');
                var idsServicios = hdfIdServicio.val().split(",");

                if (selector != undefined && selector.length > 0) {
                    for (i = 0; i < selector[0].options.length; i++) {
                        for (j = 0; j < idsServicios.length; j++) {
                            if (selector[0].options[i].value == idsServicios[j])
                                selector[0].options[i].selected = true;
                        }
                    }
                }

                var selectorMediopago = $("#slcMedioPagos");
                var hdfIdmedioPago = $('#<%= hdfIdMedioPagos.ClientID() %>');
                var idsMediopago = hdfIdmedioPago.val().split(",");

                if (selectorMediopago != undefined && selectorMediopago.length > 0) {
                    for (i = 0; i < selectorMediopago[0].options.length; i++) {
                        for (j = 0; j < idsMediopago.length; j++) {
                            if (selectorMediopago[0].options[i].value == idsMediopago[j]) {
                                selectorMediopago[0].options[i].selected = true;
                            }
                        }
                    }
                }

                cargarMultiselect();
            }
        }
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }
    </script>
</asp:Content>
