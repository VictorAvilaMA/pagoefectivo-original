﻿Imports SPE.Web
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Linq
Imports SPE.EmsambladoComun

Partial Class ADM_AdDepo
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CServicio)
#Region "Propiedades"
    Private objCParametro As CServicio
    Private Function InstanciaParametro() As CServicio
        If objCParametro Is Nothing Then
            objCParametro = New CServicio()
        End If
        Return objCParametro
    End Function
    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
#End Region
#Region "Metodos"
    Public Overrides Sub OnLoadInformation()
        MyBase.OnLoadInformation()
    End Sub
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then
            BtnCancel.Visible = False
            Dim oBEBanco As New BEBanco
            Dim oCntrBanco As New CBanco
            Dim oCntrAdministracionComun As New CAdministrarComun()
            oBEBanco.IdEstado = ParametrosSistema.EstadoBanco.Activo
            'DropDownlistBinding(ddlBanco, oCntrBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
            'Se pidio que de momento funcione solo con Skotiabank y Full Carga
            DropDownlistBinding(ddlBanco, New List(Of BEBanco), "Descripcion", "Codigo")
            ddlBanco.Items.Add(New ListItem("Banco ScotiaBank", ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank))
            ddlBanco.Items.Add(New ListItem("Full Carga", ParametrosSistema.Conciliacion.CodigoBancos.FullCarga))
            ddlBanco.Items.Add(New ListItem("Banco WesternUnion", ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion))
            txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy")
            txtFechaA.Text = DateTime.Now.ToShortDateString()
            txtFechaDeposito.Text = DateTime.Now.ToShortDateString()
            Session.Remove("ListConciliacionArchivo")
        End If
    End Sub
    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeDeposito As New BEDeposito
        obeDeposito.CodigoOperacion = txtCodigoOperacion.Text
        obeDeposito.Comentarios = txtComentarios.Text
        obeDeposito.IdConciliacionArchivo = hdfIdConciliacionArchivo.Value
        obeDeposito.MontoDepositado = ObjectToDecimal(txtMontoDeposito.Text)
        obeDeposito.NroTransaccion = txtNroTransaccion.Text
        obeDeposito.IdUsuarioCreacion = UserInfo.IdUsuario
        obeDeposito.FechaDeposito = ObjectToDateTime(txtFechaDeposito.Text)
        Return obeDeposito
    End Function
    Public Overrides Function DoInsertRecord(be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim oCBanco As New CBanco
        Dim listOrdenPago As New List(Of BEOrdenPago)
        listOrdenPago = IIf(Session("ListDetalleConciliacionOP") Is Nothing, listOrdenPago, DirectCast(Session("ListDetalleConciliacionOP"), List(Of BEOrdenPago)))
        Return oCBanco.RegistrarDeposito(be, listOrdenPago)
    End Function
    Public Overrides Sub OnInsert()
        If String.IsNullOrEmpty(hdfIdConciliacionArchivo.Value) Then
            lblMensaje.CssClass = "MensajeValidacion"
            Me.lblMensaje.Text = "No se ha seleccionado ningun archivo de Conciliación."
        Else
            MyBase.OnInsert()
        End If
    End Sub
    Public Overrides Sub OnAfterInsert()
        BtnInsert.Enabled = False
        btnBuscar.Enabled = False
        BtnCancel.Visible = False
        ddlBanco.Enabled = False
        gvResultado.Enabled = False
        ibtnFechaA.Enabled = False
        ibtnFechaDe.Enabled = False
        ibtnFechaDeposito.Enabled = False
        txtCodigoOperacion.Enabled = False
        txtComentarios.Enabled = False
        txtFechaA.Enabled = False
        txtFechaDe.Enabled = False
        txtFechaDeposito.Enabled = False
        txtMontoDeposito.Enabled = False
        txtNroTransaccion.Enabled = False
    End Sub
#End Region
#Region "Eventos"
    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        Session.Remove("ListConciliacionArchivo")
        gvResultado.PageIndex = 0
        CargarGrilla(CrearEntidad())
    End Sub
    Protected Sub gvResultado_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvResultado.PageIndexChanging
        gvResultado.PageIndex = e.NewPageIndex
        CargarGrilla(CrearEntidad())
        PintarSeleccionado()
    End Sub
    Protected Sub gvResultado_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Try
            If Not e Is Nothing Then
                Dim indice As Integer = Convert.ToInt32(e.CommandArgument)
                Select Case (e.CommandName)
                    Case "Select"
                        hdfIdConciliacionArchivo.Value = gvResultado.DataKeys(indice).Values(0)
                        CargarCodigoServicios(gvResultado.DataKeys(indice).Values(1))
                        CargarMedioPago(gvResultado.DataKeys(indice).Values(2))
                        CargarCodOperacion(gvResultado.DataKeys(indice).Values(2))
                        Session.Remove("ListDetalleConciliacionOP")
                        CargarGrillaDetalle()
                End Select
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub gvOperaciones_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOperaciones.PageIndexChanging
        gvOperaciones.PageIndex = e.NewPageIndex
        ActualizarListaDetalle()
        CargarGrillaDetalle()
    End Sub
    Protected Sub btnBuscarOp_Click(sender As Object, e As System.EventArgs) Handles btnBuscarOp.Click
        Session.Remove("ListDetalleConciliacionOP")
        CargarGrillaDetalle()
        AsignarMontoPrecalculado()
    End Sub
    Protected Sub btnAceptar_Click(sender As Object, e As System.EventArgs) Handles btnAceptar.Click
        ActualizarListaDetalle()
        mppOperacionesArchivo.Hide()
        PintarSeleccionado()
        AsignarMontoPrecalculado()
    End Sub
    Protected Sub cbSeleccionar_CheckedChanged(sender As Object, e As System.EventArgs)
        'mppOperacionesArchivo.Show()
    End Sub
    Protected Sub btnCancelarOp_Click(sender As Object, e As System.EventArgs) Handles btnCancelarOp.Click
        hdfIdConciliacionArchivo.Value = ""
        mppOperacionesArchivo.Hide()
        PintarSeleccionado()
        AsignarMontoPrecalculado()
    End Sub
    Protected Sub imgbtnRegresar_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        hdfIdConciliacionArchivo.Value = ""
        mppOperacionesArchivo.Hide()
        PintarSeleccionado()
        AsignarMontoPrecalculado()
    End Sub
    Protected Sub ddlBanco_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlBanco.SelectedIndexChanged
        liCodOperacionDesc.Visible = False
        liCodOperacionVal.Visible = False
        liNroTransDesc.Visible = False
        liNroTransVal.Visible = False
        Select Case ddlBanco.SelectedValue
            Case ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                liNroTransDesc.Visible = True
                liNroTransVal.Visible = True
            Case ParametrosSistema.Conciliacion.CodigoBancos.FullCarga, ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                liCodOperacionDesc.Visible = True
                liCodOperacionVal.Visible = True
        End Select
    End Sub
#End Region
#Region "Funciones"
    Private Sub CargarGrilla(ByVal request As BEConciliacionArchivo)
        Dim cBanco As New SPE.Web.CBanco
        Dim response As New List(Of BEConciliacionArchivo)
        response = IIf(Session("ListConciliacionArchivo") Is Nothing, cBanco.ConsultarConciliacionArchivo(request), DirectCast(Session("ListConciliacionArchivo"), List(Of BEConciliacionArchivo)))
        gvResultado.DataSource = response
        gvResultado.DataBind()
        lblResultado.Text = "Se encontraron " & response.Count.ToString() & " registros"
        divResult.Visible = True
    End Sub
    Private Sub CargarGrillaDetalle()
        Dim cBanco As New CBanco
        Dim oBEConciliacionDetalle As New BEConciliacionDetalle
        Dim response As New List(Of BEOrdenPago)
        If hdfIdServicios.Value.Trim.StartsWith(",") Then
            hdfIdServicios.Value = hdfIdServicios.Value.Substring(1, hdfIdServicios.Value.Length - 1)
        End If
        If hdfIdMedioPagos.Value.Trim.StartsWith(",") Then
            hdfIdMedioPagos.Value = hdfIdMedioPagos.Value.Substring(1, hdfIdMedioPagos.Value.Length - 1)
        End If
        With oBEConciliacionDetalle
            .IdConciliacionArchivo = hdfIdConciliacionArchivo.Value
            .CodigoServicio = hdfIdServicios.Value
            .CodigoMedioPago = hdfIdMedioPagos.Value
        End With
        response = IIf(Session("ListDetalleConciliacionOP") Is Nothing, cBanco.ConsultarOrdenesConcArch(oBEConciliacionDetalle), DirectCast(Session("ListDetalleConciliacionOP"), List(Of BEOrdenPago)))
        Session("ListDetalleConciliacionOP") = response
        gvOperaciones.DataSource = response
        gvOperaciones.DataBind()
        For Each item As GridViewRow In gvOperaciones.Rows
            DirectCast(item.FindControl("cbSeleccionar"), CheckBox).Checked = DirectCast(item.FindControl("lblFlagSeleccionado"), Label).Text
        Next
        lblResultadoDetalle.Text = "Se encontraron " & response.Count.ToString() & " registros"
        mppOperacionesArchivo.Show()
    End Sub
    Private Function CrearEntidad() As BEConciliacionArchivo
        Dim request As New BEConciliacionArchivo
        With request
            .IdEstado = 0
            .CodigoBanco = IIf(ddlBanco.SelectedValue = "", 0, ddlBanco.SelectedValue)
            .NombreArchivo = String.Empty
            .IdTipoConciliacion = 0
            .IdOrigenConciliacion = 0
            .FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaDe.Text)
            .FechaFin = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaA.Text)
        End With
        Return request
    End Function
    Private Sub ActualizarListaDetalle()
        If Session("ListDetalleConciliacionOP") IsNot Nothing Then
            For Each item As GridViewRow In gvOperaciones.Rows
                Dim lblIdOrdenPago As Label = DirectCast(item.FindControl("lblIdOrdenPago"), Label)
                Dim ListDetalleConciliacionOP As New List(Of BEOrdenPago)
                Dim obeOrdenPago As BEOrdenPago
                ListDetalleConciliacionOP = DirectCast(Session("ListDetalleConciliacionOP"), List(Of BEOrdenPago))
                obeOrdenPago = ListDetalleConciliacionOP.First(Function(be As BEOrdenPago) be.IdOrdenPago = lblIdOrdenPago.Text)
                If obeOrdenPago IsNot Nothing Then
                    obeOrdenPago.FlagSeleccionado = DirectCast(item.FindControl("cbSeleccionar"), CheckBox).Checked
                End If
                Session("ListDetalleConciliacionOP") = ListDetalleConciliacionOP
            Next
        End If
    End Sub
    Private Sub CargarCodigoServicios(ByVal idbanco As Integer)
        Dim obeBanco As New BEBanco
        Dim objCServicio As New SPE.Web.CServicio
        Dim listServicios As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        obeBanco.IdBanco = idbanco
        listServicios = objCServicio.ConsultarCodigosServiciosPorBanco(obeBanco)
        Dim cmbServicios As New StringBuilder("<select id=""slcServicios"" multiple="""" style=""display: none; "">")
        For Each oServicio As BEServicioBanco In listServicios
            cmbServicios.AppendFormat("<option value='{0}'>{1}</option>", oServicio.CodigoServicio, oServicio.CodigoServicio)
        Next
        cmbServicios.Append("</select>")
        ddlCodigoServicio.Text = cmbServicios.ToString()
    End Sub
    Private Sub CargarMedioPago(ByVal codBanco As String)
        Dim obeBanco As New BEBanco
        Dim objCComun As New SPE.Web.CComun
        'Dim listMedioPagos As List(Of BEMedioPagoBanco)
        'obeBanco.IdBanco = idbanco
        'listMedioPagos = objCComun.ConsultarMedioPagoBancoByBanco(obeBanco)
        Dim cmbMedioPagos As New StringBuilder("<select id=""slcMedioPagos"" multiple="""" style=""display: none; "">")
        'For Each oMedioPago As BEMedioPagoBanco In listMedioPagos
        '    cmbMedioPagos.AppendFormat("<option value='{0}'>{1}</option>", oMedioPago.IdMedioPago, oMedioPago.Descripcion)
        'Next
        cmbMedioPagos.AppendFormat("<option value='{0}'>{1}</option>", ParametrosSistema.MedioPagoSBK.MediosVirtuales, ParametrosSistema.MedioPagoSBK.MediosVirtualesDesc)
        cmbMedioPagos.AppendFormat("<option value='{0}'>{1}</option>", ParametrosSistema.MedioPagoSBK.Agencias, ParametrosSistema.MedioPagoSBK.AgenciasDesc)
        cmbMedioPagos.Append("</select>")
        ddlMedioPagos.Text = cmbMedioPagos.ToString()
        liMediosPago.Visible = (codBanco = ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank)
        liMediosPagoDesc.Visible = (codBanco = ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank)
    End Sub
    Private Sub PintarSeleccionado()
        For Each item As GridViewRow In gvResultado.Rows
            If DirectCast(item.FindControl("lblIdConciliacionArchivo"), Label).Text = hdfIdConciliacionArchivo.Value Then
                item.BackColor = Drawing.Color.DarkGray
            Else
                item.BackColor = Drawing.Color.White
            End If
        Next
    End Sub
    Private Sub AsignarMontoPrecalculado()
        If Session("ListDetalleConciliacionOP") IsNot Nothing Then
            If Not String.IsNullOrEmpty(hdfIdConciliacionArchivo.Value) Then
                Dim ListDetalleConciliacionOP As New List(Of BEOrdenPago)
                Dim TotalPreCargado As New Decimal(0)
                ListDetalleConciliacionOP = DirectCast(Session("ListDetalleConciliacionOP"), List(Of BEOrdenPago))
                For Each item As BEOrdenPago In ListDetalleConciliacionOP
                    If (item.FlagSeleccionado) Then
                        TotalPreCargado = TotalPreCargado + (item.Total - item.Comision)
                    End If
                Next
                lblMontoPreCalculado.Text = "S/. " + TotalPreCargado.ToString
                ulMontoPrecalculado.Visible = True
            Else
                ulMontoPrecalculado.Visible = False
            End If
        End If
    End Sub
    Private Sub CargarCodOperacion(ByVal codBanco As String)
        liCodOperacionDesc.Visible = False
        liCodOperacionVal.Visible = False
        liNroTransDesc.Visible = False
        liNroTransVal.Visible = False
        Select Case codBanco
            Case ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                liNroTransDesc.Visible = True
                liNroTransVal.Visible = True
            Case ParametrosSistema.Conciliacion.CodigoBancos.FullCarga, ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                liCodOperacionDesc.Visible = True
                liCodOperacionVal.Visible = True
        End Select
    End Sub
#End Region
End Class
