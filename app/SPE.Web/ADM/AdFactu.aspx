﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="AdFactu.aspx.vb" Inherits="ADM_AdFactu" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Factura"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pnlDatosDeposito" runat="server" CssClass="inner-col-right">
            <h4>
                1. Datos de la Factura</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Fecha de Emisi&oacute;n: </li>
                <li class="t2" style="line-height: 1">
                    <p class="input_cal">
                        <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="normal" MaxLength="100"
                            ValidationGroup="ValidarRegistro">
                        </asp:TextBox>
                        <asp:ImageButton ID="ibtnFechaEmision" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                        </asp:ImageButton>
                        <cc1:MaskedEditExtender ID="meeFechaEmision" runat="server" CultureName="es-PE" MaskType="Date"
                            Mask="99/99/9999" TargetControlID="txtFechaEmision">
                        </cc1:MaskedEditExtender>
                        <cc1:CalendarExtender ID="ceFechaEmision" runat="server" TargetControlID="txtFechaEmision"
                            PopupButtonID="ibtnFechaEmision" Format="dd/MM/yyyy" PopupPosition="Right" Animated="true">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="meFechaEmision" runat="server" ControlExtender="meeFechaEmision"
                            ControlToValidate="txtFechaEmision" Display="Dynamic" EmptyValueBlurredText="*"
                            EmptyValueMessage="Fecha Emision es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                            InvalidValueMessage="Fecha Deposito no válida" IsValidEmpty="False" ValidationGroup="ValidarRegistro">
                        </cc1:MaskedEditValidator>
                    </p>
                </li>
                <li class="t1"><span class="color">>></span> Nro de Factura: </li>
                <li class="t2">
                    <asp:TextBox ID="txtNroFactura" runat="server" CssClass="normal" MaxLength="50" ValidationGroup="ValidarRegistro">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNroFactura" runat="server" ErrorMessage="Debe ingresar el nro. de factura"
                        ControlToValidate="txtNroFactura" ValidationGroup="ValidarRegistro">*</asp:RequiredFieldValidator>
                </li>
                <li class="t1"><span class="color">>></span> Monto Facturado: </li>
                <li class="t2">
                    <asp:TextBox ID="txtMontoFactura" runat="server" CssClass="normal" MaxLength="17"
                        ValidationGroup="ValidarRegistro">
                    </asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftbMontoFactura" runat="server" TargetControlID="txtMontoFactura"
                        ValidChars=".0123456789">
                    </cc1:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="rfvMontoFactura" runat="server" ErrorMessage="Debe ingresar el Monto de la Factura."
                        ControlToValidate="txtMontoFactura" ValidationGroup="ValidarRegistro">*</asp:RequiredFieldValidator>
                </li>
                <li class="t1"><span class="color">>></span> Observaciones: </li>
                <li class="t2">
                    <asp:TextBox ID="txtObservaciones" runat="server" CssClass="normal" MaxLength="50"
                        ValidationGroup="ValidarRegistro">
                    </asp:TextBox>
                </li>
            </ul>
            <div style="clear: both">
            </div>
            <asp:UpdatePanel ID="upMontoPreCalculado" runat="server">
                <ContentTemplate>
                    <ul class="datos_cip2 t0" id="ulMontoPrecalculado" runat="server" visible="false">
                        <li class="t1" style="color: Blue"><span class="color">>></span> Monto Pre calculado:
                        </li>
                        <li class="t2" style="color: Blue">
                            <asp:Literal ID="lblMontoPreCalculado" runat="server"></asp:Literal>
                        </li>
                    </ul>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div style="clear: both">
            </div>
            <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" CssClass="Summary_datosCip2">
            </asp:ValidationSummary>
            <div style="clear: both">
            </div>
            <h4>
                2. Busqueda de depositos</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Banco: </li>
                <li class="t2">
                    <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                    </asp:DropDownList>
                </li>
                <li class="t1"><span class="color">>></span> Cod. Operaci&oacute;n deposito: </li>
                <li class="t2">
                    <asp:TextBox ID="txtCodOpDeposito" runat="server" CssClass="normal" MaxLength="19">
                    </asp:TextBox>
                </li>
                <li class="t1"><span class="color">>></span> Numero de dep&oacute;sito: </li>
                <li class="t2">
                    <asp:TextBox ID="txtNroDeposito" runat="server" CssClass="normal" MaxLength="19">
                    </asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> Del:</li>
                <li class="t2" style="line-height: 1; z-index: 200;">
                    <p class="input_cal">
                        <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                        <asp:ImageButton CssClass="calendario" ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                        </asp:ImageButton>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                            InvalidValueMessage="Fecha 'Del' no válida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                            EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                            ControlExtender="mdeDel"></cc1:MaskedEditValidator>
                    </p>
                    <span class="entre">al: </span>
                    <p class="input_cal">
                        <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                            Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                            Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:ImageButton CssClass="calendario" ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                        </asp:ImageButton>
                        <cc1:MaskedEditValidator CssClass="izq" ID="MaskedEditValidatorAl" runat="server"
                            IsValidEmpty="False" InvalidValueMessage="Fecha 'Al' no válida" InvalidValueBlurredMessage="*"
                            EmptyValueMessage="Fecha 'Al' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaA"
                            ControlExtender="mdeA">*
                        </cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de búsqueda no es válido"
                            ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                            Type="Date">*</asp:CompareValidator>
                    </p>
                </li>
                <li class="mensaje">
                    <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                </li>
                <li class="complet">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" class="input_azul3" />
                    <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" class="input_azul4"
                        ValidationGroup="ValidarRegistro" OnClientClick="return ConfirmMe();" />
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" class="input_azul4" />
                </li>
            </ul>
            <div style="clear: both">
            </div>
            <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
                Format="dd/MM/yyyy">
            </cc1:CalendarExtender>
            <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
                Format="dd/MM/yyyy">
            </cc1:CalendarExtender>
            <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
                Mask="99/99/9999" CultureName="es-PE">
            </cc1:MaskedEditExtender>
            <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
                Mask="99/99/9999" CultureName="es-PE">
            </cc1:MaskedEditExtender>
            <div style="clear: both;">
            </div>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" class="result" visible="false">
                        <h5 id="h5Resultado" runat="server">
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <div style="clear: both;">
                        </div>
                        <asp:GridView ID="gvResultado" DataKeyNames="IdConciliacionArchivo,IdBanco" runat="server"
                            CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False" OnRowCommand="gvResultado_RowCommand"
                            OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFlagSeleccionado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FlagSeleccionado") %>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbSeleccionar" runat="server" AutoPostBack="true" OnCheckedChanged="cbSeleccionar_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIdDeposito" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdDeposito") %>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalComision" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TotalComision") %>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaDeposito" DataFormatString="{0:d}" SortExpression="Fecha"
                                    HeaderText="Fecha Deposito">
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CodigoOperacion" SortExpression="CodigoOperacion" HeaderText="Cod. Operacion">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NroTransaccion" SortExpression="NroTransaccion" HeaderText="Nro. Transacción">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="MontoDepositado" HeaderText="Monto Dep." SortExpression="MontoDepositado">
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Comentarios" SortExpression="Comentarios" HeaderText="Comentarios">
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hdfIdConc" runat="server" />
                        <asp:HiddenField ID="hdfIdConcEnt" runat="server" />
                        <asp:HiddenField ID="hdfCodBanco" runat="server" />
                        <asp:HiddenField ID="hdfIdConciliacionArchivo" runat="server" />
                        <asp:HiddenField ID="hdfIdServicios" runat="server" />
                        <asp:HiddenField ID="hdfServicios" runat="server" />
                        <asp:HiddenField ID="hdfServiciosFormato" runat="server" />
                        <asp:HiddenField ID="hdfIdMedioPagos" runat="server" />
                        <asp:HiddenField ID="hdfMedioPagos" runat="server" />
                        <asp:HiddenField ID="hdfMedioPagosFormato" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txtMontoFactura.ClientID%>').blur(function () {
                if ($('#<%=txtMontoFactura.ClientID%>').val() != '')
                    $('#<%=txtMontoFactura.ClientID%>').val(parseFloat($('#<%=txtMontoFactura.ClientID%>').val()).toFixed(2));
            });
        });
    </script>
</asp:Content>
