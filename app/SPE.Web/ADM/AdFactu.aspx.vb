﻿Imports SPE.Web
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Linq
Imports SPE.EmsambladoComun

Partial Class ADM_AdFactu
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CServicio)
#Region "Propiedades"
    Private objCParametro As CServicio
    Private Function InstanciaParametro() As CServicio
        If objCParametro Is Nothing Then
            objCParametro = New CServicio()
        End If
        Return objCParametro
    End Function
    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
#End Region
#Region "Metodos"
    Public Overrides Sub OnLoadInformation()
        MyBase.OnLoadInformation()
    End Sub
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then
            BtnCancel.Visible = False
            Dim oBEBanco As New BEBanco
            Dim oCntrBanco As New CBanco
            Dim oCntrAdministracionComun As New CAdministrarComun()
            oBEBanco.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
            'DropDownlistBinding(ddlBanco, oCntrBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
            'Se pidio que de momento funcione solo con Skotiabank, Full Carga y Western Union
            DropDownlistBinding(ddlBanco, New List(Of BEBanco), "Descripcion", "IdBanco")
            ddlBanco.Items.Add(New ListItem("Banco ScotiaBank", ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank))
            ddlBanco.Items.Add(New ListItem("Full Carga", ParametrosSistema.Conciliacion.CodigoBancos.FullCarga))
            ddlBanco.Items.Add(New ListItem("Banco WesternUnion", ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion))
            'DropDownlistBinding(ddlMoneda, oCntrAdministracionComun.ConsultarMoneda(), "Descripcion", "IdMoneda", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
            txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy")
            txtFechaA.Text = DateTime.Now.ToShortDateString()
            txtFechaEmision.Text = DateTime.Now.ToShortDateString()
            Session.Remove("ListConciliacionArchivo")
        End If
    End Sub
    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeFactura As New BEFactura
        obeFactura.NroFactura = txtNroFactura.Text
        obeFactura.MontoFactura = ObjectToDecimal(txtMontoFactura.Text)
        obeFactura.FechaEmision = ObjectToDateTime(txtFechaEmision.Text)
        obeFactura.Observacion = txtObservaciones.Text
        obeFactura.IdUsuarioCreacion = UserInfo.IdUsuario
        Return obeFactura
    End Function
    Public Overrides Function DoInsertRecord(be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim oCBanco As New CBanco
        Dim listDeposito As New List(Of BEDeposito)
        listDeposito = IIf(Session("ListDeposito") Is Nothing, listDeposito, DirectCast(Session("ListDeposito"), List(Of BEDeposito)))
        Return oCBanco.RegistrarFactura(be, listDeposito.Where(Function(beDeposito As BEDeposito) beDeposito.FlagSeleccionado = True).ToList())
    End Function
    Public Overrides Sub OnInsert()
        ActualizarDepositos()
        If Session("ListDeposito") Is Nothing Then
            lblMensaje.CssClass = "MensajeValidacion"
            Me.lblMensaje.Text = "No se ha seleccionado ninguna deposito."
        Else
            Dim listDeposito As New List(Of BEDeposito)
            listDeposito = DirectCast(Session("ListDeposito"), List(Of BEDeposito))
            If listDeposito.Where(Function(beDeposito As BEDeposito) beDeposito.FlagSeleccionado = True).Count < 1 Then
                lblMensaje.CssClass = "MensajeValidacion"
                Me.lblMensaje.Text = "No se ha seleccionado ningun deposito."
            Else
                MyBase.OnInsert()
            End If
        End If
    End Sub
    Public Overrides Sub OnAfterInsert()
        BtnInsert.Enabled = False
        btnBuscar.Enabled = False
        BtnCancel.Visible = False
        txtCodOpDeposito.Enabled = False
        txtFechaEmision.Enabled = False
        txtMontoFactura.Enabled = False
        txtNroDeposito.Enabled = False
        txtNroFactura.Enabled = False
        txtObservaciones.Enabled = False
        gvResultado.Enabled = False
        ddlBanco.Enabled = False
        ibtnFechaA.Enabled = False
        ibtnFechaDe.Enabled = False
        ibtnFechaEmision.Enabled = False
    End Sub
#End Region
#Region "Eventos"
    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        Session.Remove("ListDeposito")
        gvResultado.PageIndex = 0
        CargarGrilla(CrearEntidad())
    End Sub
    Protected Sub gvResultado_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvResultado.PageIndexChanging
        gvResultado.PageIndex = e.NewPageIndex
        CargarGrilla(CrearEntidad())
        ActualizarDepositos()
    End Sub
    Protected Sub gvResultado_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
    End Sub
    Protected Sub cbSeleccionar_CheckedChanged(sender As Object, e As System.EventArgs)
        CalcularMontoPrecalculado()
    End Sub
#End Region
#Region "Funciones"
    Private Sub CargarGrilla(ByVal request As BEDeposito)
        Dim cBanco As New SPE.Web.CBanco
        Dim response As New List(Of BEDeposito)
        response = IIf(Session("ListDeposito") Is Nothing, cBanco.ConsultarDepositoRegistroFactura(request), DirectCast(Session("ListDeposito"), List(Of BEDeposito)))
        Session("ListDeposito") = response
        gvResultado.Columns(5).Visible = True
        gvResultado.Columns(6).Visible = True
        gvResultado.DataSource = response
        gvResultado.DataBind()

        Select Case ddlBanco.SelectedValue
            Case ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                gvResultado.Columns(5).Visible = False
            Case ParametrosSistema.Conciliacion.CodigoBancos.FullCarga, ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                gvResultado.Columns(6).Visible = False
        End Select

        For Each item As GridViewRow In gvResultado.Rows
            DirectCast(item.FindControl("cbSeleccionar"), CheckBox).Checked = DirectCast(item.FindControl("lblFlagSeleccionado"), Label).Text
        Next
        lblResultado.Text = "Se encontraron " & response.Count.ToString() & " registros"
        divResult.Visible = True
    End Sub
    Function CrearEntidad() As BEDeposito
        Dim request As New BEDeposito
        With request
            .NroTransaccion = txtNroDeposito.Text
            .CodigoOperacion = txtCodOpDeposito.Text
            .IdBanco = IIf(String.IsNullOrEmpty(ddlBanco.SelectedValue), 0, ddlBanco.SelectedValue)
            .FechaDesde = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaDe.Text)
            .FechaHasta = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaA.Text)
        End With
        Return request
    End Function
    Private Sub ActualizarDepositos()
        If Session("ListDeposito") IsNot Nothing Then
            Dim ListDeposito As New List(Of BEDeposito)
            ListDeposito = DirectCast(Session("ListDeposito"), List(Of BEDeposito))
            For Each item As GridViewRow In gvResultado.Rows
                Dim lblIdDeposito As Label = DirectCast(item.FindControl("lblIdDeposito"), Label)
                Dim obeDeposito As BEDeposito
                obeDeposito = ListDeposito.First(Function(be As BEDeposito) be.IdDeposito = lblIdDeposito.Text)
                If obeDeposito IsNot Nothing Then
                    obeDeposito.FlagSeleccionado = DirectCast(item.FindControl("cbSeleccionar"), CheckBox).Checked
                End If
            Next
            Session("ListDeposito") = ListDeposito
        End If
    End Sub
    Private Sub CalcularMontoPrecalculado()
        Dim totalComision As Decimal = 0
        Dim existChecked As Boolean = False
        lblMontoPreCalculado.Text = ""
        ulMontoPrecalculado.Visible = False
        For Each item As GridViewRow In gvResultado.Rows
            Dim cbSeleccionar As CheckBox = DirectCast(item.FindControl("cbSeleccionar"), CheckBox)
            If cbSeleccionar.Checked = True Then
                Dim lblTotalComision As Label = DirectCast(item.FindControl("lblTotalComision"), Label)
                totalComision += Convert.ToDecimal(lblTotalComision.Text)
                existChecked = True
            End If
        Next
        If existChecked Then
            ulMontoPrecalculado.Visible = True
            lblMontoPreCalculado.Text = "S/ " & totalComision.ToString()
        End If
    End Sub
#End Region
End Class
