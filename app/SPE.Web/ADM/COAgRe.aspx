<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COAgRe.aspx.vb" Inherits="ADM_COAgRe" Title="PagoEfectivo - Consultar Agencia Recaudadora" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Agencia Recaudadora</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanelFil" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombre Comercial:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreComercial" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Contacto: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtContacto" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" PostBackUrl="~/ADM/ADAgRe.aspx" />
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'0123456789& "
                    TargetControlID="txtContacto">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'0123456789& "
                    TargetControlID="txtNombreComercial">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal></h5>
                        <div>
                            &nbsp;<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label></div>
                        <asp:GridView ID="gvResultado" runat="server" BackColor="White" DataKeyNames="IdAgenciaRecaudadora"
                            CssClass="grilla" AllowPaging="True" CellPadding="3" BorderWidth="1px" BorderStyle="None"
                            BorderColor="#CCCCCC" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging"
                            OnRowDataBound="gvResultado_RowDataBound" AllowSorting="True" OnSorting="gvResultado_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnActualizar" runat="server" PostBackUrl="~/ADM/ADAgRe.aspx"
                                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' ToolTip="Actualizar" OnCommand="Actualizar_Command"
                                            CommandArgument='<%# Eval("IdAgenciaRecaudadora") %>' CommandName="Actualizar_Command">
                                        </asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdAgenciaRecaudadora" Visible="False"></asp:BoundField>
                                <asp:BoundField DataField="NombreComercial" HeaderText="Nombre Comercial" SortExpression="NombreComercial">
                                </asp:BoundField>
                                <asp:BoundField DataField="Contacto" HeaderText="Contacto" SortExpression="Contacto">
                                </asp:BoundField>
                                <asp:BoundField DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Email" HeaderText="E-mail" SortExpression="Email">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtndelete" CausesValidation="false" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/borrar.jpg" %>'
                                            ToolTip="Eliminar" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="right" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
