Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class ADM_COAgRe
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)

#Region "atributos"
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property


#End Region

    Public Overrides Sub OnMainSearch()
        ListarAgenciasRecaudadoras()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNombreComercial)
            CargarComboEstado()
        End If
        '
    End Sub

    'BUSQUEDA DE AGENCIA RECAUDADORA
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarAgenciasRecaudadoras()
    End Sub

    'LISTA DE AGENCIAS RECAUDADORAS
    Private Sub ListarAgenciasRecaudadoras()
        '
        Try
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
            Dim listAgencia As New List(Of BEAgenciaRecaudadora)

            obeAgenciaRecaudadora.NombreComercial = txtNombreComercial.Text
            obeAgenciaRecaudadora.Contacto = txtContacto.Text
            If ddlEstado.SelectedIndex = 0 Then
                obeAgenciaRecaudadora.IdEstado = 0
            Else
                obeAgenciaRecaudadora.IdEstado = ddlEstado.SelectedValue
            End If
            '
            obeAgenciaRecaudadora.OrderBy = SortExpression
            obeAgenciaRecaudadora.IsAccending = SortDir
            listAgencia = objCAdministrarAgenciaRecaudadora.ConsultarAgenciaRecaudadora(obeAgenciaRecaudadora)
            gvResultado.DataSource = listAgencia
            gvResultado.DataBind()
            If Not listAgencia Is Nothing Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listAgencia.Count)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If
            lblMensaje.Text = ""
            divResult.Visible = True
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)

        End Try
        '
    End Sub

    'ACTUALIZAR AGENCIA RECAUDADORA
    Protected Sub Actualizar_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Context.Items.Add("IdAgencia", Convert.ToInt32(e.CommandArgument.ToString()))
        '
    End Sub

    'CARGAR COMBO ESTADO
    Private Sub CargarComboEstado()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESMA") : ddlEstado.DataBind() : ddlEstado.Items.Insert(0, "::: Todos :::")
        '
    End Sub

    'LIMPIAR
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        '
        txtNombreComercial.Text = ""
        txtContacto.Text = ""
        ddlEstado.SelectedIndex = 0
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        lblMensaje.Text = ""
        divResult.Visible = False
        '
    End Sub

    'Paginado de Grilla
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarAgenciasRecaudadoras()
        '
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        '
        SortExpression = e.SortExpression
        If (SortDir.Equals(SortDirection.Ascending)) Then
            SortDir = SortDirection.Descending
        Else
            SortDir = SortDirection.Ascending
        End If
        ListarAgenciasRecaudadoras()
        '
    End Sub

    
End Class
