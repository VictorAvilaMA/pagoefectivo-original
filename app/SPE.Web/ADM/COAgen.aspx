<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COAgen.aspx.vb" Inherits="ADM_COAgen" Title="PagoEfectivo - Consultar Agente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Agente</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Tipo Agente:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoAgente" runat="server" CssClass="neu">
                            <asp:ListItem>Agente</asp:ListItem>
                            <asp:ListItem>Supervisor</asp:ListItem>
                            <asp:ListItem>::: Todos :::</asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Agencia: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtAgenciaRecaudadora" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombres: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombres" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Apellidos:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtApellidos" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Tel�fono:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revFono" runat="server" Display="Dynamic" ControlToValidate="txtTelefono"
                            ErrorMessage="Formato de tel�fono no valido" ValidationExpression="\(\d{3}\)\-\d{3}\-\d{7}"></asp:RegularExpressionValidator>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" PostBackUrl="~/ADM/ADAgen.aspx" />
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <asp:ValidationSummary ID="vsmMessage" runat="server" ValidationGroup="IsNullOrEmpty">
                </asp:ValidationSummary>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombres" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtNombres">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtApellidos">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" class="cont_cel" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" runat="server" BackColor="White" CssClass="grilla"
                            OnPageIndexChanging="gvResultado_PageIndexChanging" AutoGenerateColumns="False"
                            BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True"
                            OnRowDataBound="gvResultado_RowDataBound" AllowSorting="True" OnSorting="gvResultado_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnActualizar" runat="server" PostBackUrl="~/ADM/ADAgen.aspx"
                                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' ToolTip="Actualizar" CommandName="Actualizar_Command"
                                            OnCommand="Actualizar_Command" CommandArgument='<%# Eval("IdAgente") %>'></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdAgente" HeaderText="IdAgente" Visible="False"></asp:BoundField>
                                <asp:BoundField DataField="DescAgenciaRecaudadora" HeaderText="Agencia" SortExpression="DescAgenciaRecaudadora">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescTipoAgente" HeaderText="Tipo Agente" SortExpression="DescTipoAgente">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombres" HeaderText="Nombres" SortExpression="Nombres">
                                </asp:BoundField>
                                <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" SortExpression="Apellidos">
                                </asp:BoundField>
                                <asp:BoundField DataField="Telefono" HeaderText="Tel&#233;fono" Visible="False">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Email" HeaderText="E-mail" Visible="False"></asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
