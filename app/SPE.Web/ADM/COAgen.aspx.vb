Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class ADM_COAgen
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtAgenciaRecaudadora)
            CargarCombos()
        End If
        '
    End Sub

    'BUSQUEDA DE AGENTE
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        '
        ListarAgentes()
        '
    End Sub

    'LISTAR AGENTES
    Private Sub ListarAgentes()
        '
        Try
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgente As New BEAgente
            Dim listAgente As New List(Of BEAgente)

            If ddlTipoAgente.SelectedIndex = 0 Then
                obeAgente.IdTipoAgente = 0
            Else
                obeAgente.IdTipoAgente = ddlTipoAgente.SelectedValue
            End If
            obeAgente.DescAgenciaRecaudadora = txtAgenciaRecaudadora.Text
            obeAgente.Nombres = txtNombres.Text
            obeAgente.Apellidos = txtApellidos.Text
            obeAgente.Telefono = txtTelefono.Text

            If ddlEstado.SelectedIndex = 0 Then
                obeAgente.IdEstado = 0
            Else
                obeAgente.IdEstado = Convert.ToInt32(Me.ddlEstado.SelectedValue)

            End If
            '
            obeAgente.OrderBy = SortExpression
            obeAgente.IsAccending = SortDir
            listAgente = objCAdministrarAgenciaRecaudadora.ConsultarAgente(obeAgente)
            gvResultado.DataSource = listAgente
            'objCAdministrarAgenciaRecaudadora.ProbarConexion()
            gvResultado.DataBind()
            If Not listAgente Is Nothing Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listAgente.Count)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If
            divResult.Visible = True
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Sub

    'ACTUALIZAR AGENTE
    Protected Sub Actualizar_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Context.Items.Add("IdAgente", Convert.ToInt32(e.CommandArgument.ToString()))
        '
    End Sub

    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESUS") : ddlEstado.DataBind() : ddlEstado.Items.Insert(0, "::: Todos :::")
        '
        ddlTipoAgente.DataTextField = "Descripcion" : ddlTipoAgente.DataValueField = "Id" : ddlTipoAgente.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("TAGT") : ddlTipoAgente.DataBind() : ddlTipoAgente.Items.Insert(0, "::: Todos :::")
        '
    End Sub

    'LIMPIAR
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        '
        txtAgenciaRecaudadora.Text = ""
        txtNombres.Text = ""
        txtApellidos.Text = ""
        txtTelefono.Text = ""
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        ddlEstado.SelectedIndex = 0
        ddlTipoAgente.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        divResult.Visible = False
        '
    End Sub

    'PAGINADO DE LA GRILLA
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarAgentes()
        '
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        '
        SortExpression = e.SortExpression
        If (SortDir.Equals(SortDirection.Ascending)) Then
            SortDir = SortDirection.Descending
        Else
            SortDir = SortDirection.Ascending
        End If
        ListarAgentes()
        '
    End Sub
End Class
