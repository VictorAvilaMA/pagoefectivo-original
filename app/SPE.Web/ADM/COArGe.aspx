<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COArGe.aspx.vb" Inherits="ADM_COArGe" Title="PagoEfectivo - Consultar Archivos Generados" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Archivos Generados</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> Nombre: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreArchivo" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Tipo de Archivo: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoArchivo" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha de Generaci&oacute;n del:
                    </li>
                    <li class="t2" style="line-height: 1;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDe" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaDe" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Del' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Del' no v�lida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaA" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaA" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Al' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Al' no v�lida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div class="clear">
                            </div>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Rango de fechas no es v�lido"
                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                Type="Date" ValidationGroup="GrupoValidacion">*</asp:CompareValidator>
                        </p>
                    </li>
                </ul>
                <asp:ValidationSummary ID="ValidationSummary" runat="server" ValidationGroup="GrupoValidacion">
                </asp:ValidationSummary>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                    Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaDe">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                    Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaA">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="ibtnFechaDe" TargetControlID="txtFechaDe">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA"
                    TargetControlID="txtFechaA">
                </cc1:CalendarExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtNombreArchivo" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'0123456789.-_ "
                    TargetControlID="txtNombreArchivo">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <ul id="Fieldset2" class="datos_cip2">
            <li id="fsBotonera" class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <asp:Label ID="lblResultado" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <h5 id="div5" runat="server">
                        <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                    </h5>
                    <div>
                        <asp:GridView ID="gvArchivo" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdFtpArchivo" OnPageIndexChanging="gvArchivo_PageIndexChanging"
                            OnRowDataBound="gvArchivo_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Sel.">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgActualizar" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                            PostBackUrl="ADArGe.aspx" ToolTip="Actualizar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdFtpArchivo" HeaderText="IdFtpArchivo" Visible="False" />
                                <asp:BoundField DataField="Servicio" HeaderText="Servicio" SortExpression="Servicio" />
                                <asp:BoundField DataField="NombreArchivo" HeaderText="Nombre Archivo" SortExpression="NombreArchivo" />
                                <asp:BoundField DataField="FechaGenerado" HeaderText="Fecha Generacion" SortExpression="FechaGenerado">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoArchivo" HeaderText="Tipo Archivo" SortExpression="TipoArchivo">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera" />
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
