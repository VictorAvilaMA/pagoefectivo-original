<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COClint.aspx.vb" Inherits="ADM_COClint" Title="PagoEfectivo - Consultar Cliente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Cliente</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> E-mail :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="neu"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombres:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombres" runat="server" CssClass="neu" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Apellidos:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtApellidos" runat="server" CssClass="neu" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado PE:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" />
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="limpiar" />
                    </li>
                </ul>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtApellidos">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombres" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtNombres">
                </cc1:FilteredTextBoxExtender>
                <div style="clear: both">
                </div>
                <div class="result">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divResult" runat="server" class="cont_cel" visible="false">
                                <h5>
                                    <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label></h5>
                                <asp:GridView ID="gvResultado" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="Email" OnPageIndexChanging="gvResultado_PageIndexChanging"
                                    OnRowDataBound="gvResultado_RowDataBound" PageSize="10">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sel." ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                                    PostBackUrl="ADClint.aspx" ToolTip="Actualizar" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Config.">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgConfig" runat="server" CommandName="Configurar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/Configurar.png" %>'
                                                    ToolTip="Configurar" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdCliente")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Email" HeaderText="E-mail" SortExpression="Email" />
                                        <asp:BoundField DataField="Nombres" HeaderText="Nombres" SortExpression="Nombres" />
                                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" SortExpression="Apellidos" />
                                        <asp:BoundField DataField="Telefono" HeaderText="Tel�fono" SortExpression="Telefono"
                                            Visible="False" />
                                        <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" ItemStyle-HorizontalAlign="center" />
                                        <asp:TemplateField HeaderText="Historial de Navegacion">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" CommandName="Navegar" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdUsuario")%>'
                                                    runat="server" Text="Ver"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="cabecera" />
                                    <EmptyDataTemplate>
                                        No se encontraron registros
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
