Imports System.Data
Imports _3Dev.FW.Web.WebUtil
Imports System.Linq

Partial Class ADM_COClint
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CCliente)

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Protected Overrides ReadOnly Property FlagPager As Boolean
        Get
            Return True
        End Get
    End Property

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)

    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeCliente As New SPE.Entidades.BECliente
        obeCliente.Email = txtEmail.Text
        obeCliente.Nombres = txtNombres.Text
        obeCliente.Apellidos = txtApellidos.Text
        If ddlEstado.SelectedIndex = 0 Then
            obeCliente.IdEstado = 0
        Else
            obeCliente.IdEstado = ddlEstado.SelectedValue
        End If
        Return obeCliente
    End Function

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.MaintenanceKey = "ADClint"
        e.MaintenancePageName = "ADClint.aspx"
        e.ExecuteSearchOnFirstLoad = False
    End Sub

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        divResult.Visible = True
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, If(ResultList.Count = 0, 0, ResultList.First().TotalPageNumbers))
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtEmail)
            CargarCombos()
        End If
    End Sub

    Sub CargarCombos()
        Dim CParametro As New SPE.Web.CAdministrarParametro
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, CParametro.ConsultarParametroPorCodigoGrupo("ESUS"), "Descripcion", "Id", "::: Todos :::")
    End Sub

    Sub Limpiar()
        txtEmail.Text = ""
        txtApellidos.Text = ""
        txtNombres.Text = ""
        ddlEstado.SelectedIndex = 0
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        divResult.Visible = False
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
    End Sub





    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub

  

    Public Overrides Sub OnBeforeSearch()

    End Sub




    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Limpiar()
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        'gvResultado.PageIndex = e.NewPageIndex
        '
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvResultado.RowDataBound
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, "IdEstado", SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        ''HistorialNavegacion
    End Sub

    Protected Sub gvResultado_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        VariableTransicion = e.CommandArgument

        If e.CommandName = "Edit" Then
            'Response.Redirect("ADClint.aspx")
        ElseIf e.CommandName = "Configurar" Then
            Response.Redirect("~/DV/HaInAcCuDiVi.aspx")
        ElseIf e.CommandName = "Navegar" Then
            Response.Redirect("~/ADM/COLogNav.aspx")
        End If
    End Sub
End Class
