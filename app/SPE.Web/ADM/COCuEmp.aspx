﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="COCuEmp.aspx.vb" Inherits="ADM_COCuEmp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Cuentas de Deposito a Empresas</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de búsqueda</h4>
        <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Empresa Contratante: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmpresaContratante" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Banco: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtBanco" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Nro Cuenta: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroCuenta" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Moneda: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMoneda" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="dropEstado" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" Text="Buscar" />
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <div class="cont-panel-search">
                    <cc1:FilteredTextBoxExtender ID="FTBE_TxtEmpresaContratante" runat="server" ValidChars="AaÁáBbCcDdEeÉéFfGgHhIiÍíJjKkLlMmNnÑñOoÓóPpQqRrSsTtUuÚúVvWwÜüXxYyZz'&.- "
                        TargetControlID="txtEmpresaContratante">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="FTBE_txtBanco" runat="server" ValidChars="1234567890AaÁáBbCcDdEeÉéFfGgHhIiÍíJjKkLlMmNnÑñOoÓóPpQqRrSsTtUuÚúVvWwÜüXxYyZz'&.- "
                        TargetControlID="txtBanco">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="FTBE_txtNroCuenta" runat="server" ValidChars="1234567890- "
                        TargetControlID="txtNroCuenta">
                    </cc1:FilteredTextBoxExtender>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                            AutoGenerateColumns="False" DataKeyNames="IdCuentaEmpresa" AllowPaging="True"
                            OnRowDataBound="gvResultado_RowDataBound" PageSize="10">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADCuEmp.aspx" runat="server"
                                            ToolTip="Actualizar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NombreEmpresaContratante" SortExpression="NombreEmpresaContratante"
                                    HeaderText="Emp. Contratante"></asp:BoundField>
                                <asp:BoundField DataField="DescripcionMoneda" SortExpression="DescripcionMoneda"
                                    HeaderText="Moneda"></asp:BoundField>
                                <asp:BoundField DataField="Banco" SortExpression="Banco" HeaderText="Banco"></asp:BoundField>
                                <asp:BoundField DataField="NroCuenta" SortExpression="NroCuenta" HeaderText="Nro Cuenta">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                    HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="FechaCreacion" SortExpression="FechaCreacion" HeaderText="Fec.Creacion"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:TemplateField HeaderText="Fec.Actualizacion" SortExpression="FechaActualizacion">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaActualizacion"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
