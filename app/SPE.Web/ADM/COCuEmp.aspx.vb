﻿Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports SPE.EmsambladoComun

Partial Class ADM_COCuEmp
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CEmpresaContratante)

    'prueba prueba

#Region "atributos"
    Private objCParametro As CAdministrarParametro

    Private Function InstanciaParametro() As CAdministrarParametro
        If objCParametro Is Nothing Then
            objCParametro = New CAdministrarParametro()
        End If
        Return objCParametro
    End Function
    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager As Boolean
        Get
            Return True
        End Get
    End Property
#End Region

#Region "métodos base"
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeCuentaEmpresa As New SPE.Entidades.BECuentaEmpresa()
        obeCuentaEmpresa.NombreEmpresaContratante = txtEmpresaContratante.Text.TrimEnd()
        obeCuentaEmpresa.Banco = txtBanco.Text.TrimEnd()
        obeCuentaEmpresa.NroCuenta = txtNroCuenta.Text.TrimEnd()
        obeCuentaEmpresa.IdEstado = _3Dev.FW.Util.DataUtil.StringToInt(dropEstado.SelectedValue)
        obeCuentaEmpresa.IdMoneda = _3Dev.FW.Util.DataUtil.StringToInt(ddlMoneda.SelectedValue)
        Return obeCuentaEmpresa
    End Function
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As GridViewRow)
        key = CInt(gvResultado.DataKeys(e.RowIndex).Values(0))
    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.MaintenanceKey = "ADCuEmp"
        e.MaintenancePageName = "ADCuEmp.aspx"
        e.ExecuteSearchOnFirstLoad = False
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        WebUtil.DropDownlistBinding(dropEstado, InstanciaParametro.ListarEstados(), "Descripcion", "Id", "::: Todos :::")
        WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::: Todos :::")
    End Sub
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If
        divResult.Visible = True
    End Sub

    Public Overrides Sub OnClear()
        MyBase.OnClear()
        txtBanco.Text = ""
        txtEmpresaContratante.Text = ""
        txtNroCuenta.Text = ""
        dropEstado.SelectedIndex = 0
        ddlMoneda.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        divResult.Visible = False
    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim list As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = CType(ControllerObject, SPE.Web.CEmpresaContratante).ConsultarCuentaEmpresa(be)
        Return list
    End Function
#End Region
#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtEmpresaContratante)
        End If
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    End Sub
#End Region

End Class
