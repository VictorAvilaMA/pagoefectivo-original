<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COEmCo.aspx.vb" Inherits="ADM_COEmCo" Title="PagoEfectivo - Consultar Empresa Contratante" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #Error div ul li
        {
            color: Red !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Empresa Contratante</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="10"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Raz&oacute;n Social: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> R.U.C.: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtRUC" runat="server" CssClass="normal" MaxLength="11"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FCtxtRUC" runat="server" ValidChars="0123456789."
                            TargetControlID="txtRUC">
                        </cc1:FilteredTextBoxExtender>
                        <asp:RangeValidator ID="rvaRuc" runat="server" ControlToValidate="txtRuc" ErrorMessage="Ingrese solo n�meros"
                            MaximumValue="100000000000" MinimumValue="0" Type="Double">*</asp:RangeValidator><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRuc" Display="Dynamic"
                                ErrorMessage="Ingrese el R.U.C." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator><asp:CustomValidator
                                    ID="cvRUC" runat="server" ClientValidationFunction="ValidarRuc" ControlToValidate="txtRuc"
                                    Display="Dynamic" ErrorMessage="R.U.C. no v�lido" SetFocusOnError="True">*</asp:CustomValidator>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Contacto:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtContacto" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                    </li>

                    <li class="t1"><span class="color">&gt;&gt;</span> Periodo Liquidaci&oacute;n:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>

                    <li class="t1"><span class="color">&gt;&gt;</span> Correo:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" ValidChars="AaBbCcDdEeFfGgHhIiJjKkLlMmNn��OoPpQqRrSsTtUuVvWw��XxYyZz123456789._-@'"
                            TargetControlID="txtEmail">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlIdEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar"
                            CausesValidation="false" />
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Cancelar" />
                    </li>
                </ul>
                <div id="Error">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </div>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtContacto" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtContacto">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtRazonSocial" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'0123456789&-. "
                    TargetControlID="txtRazonSocial">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Label ID="lblMsgNroRegistros" runat="server" Text=""></asp:Label>
                        </h5>
                        <div id="divlblmensaje" runat="server">
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div id="divgrilla" runat="server">
                            <asp:GridView ID="gvEmpresa" runat="server" AllowPaging="True" AllowSorting="true"
                                AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdEmpresaContratante"
                                OnRowDataBound="gvEmpresa_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel.">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgActualizar" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                                PostBackUrl="ADEmCo.aspx" ToolTip="Actualizar" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Codigo" HeaderText="C�digo" SortExpression="Codigo" />
                                    <asp:BoundField DataField="RazonSocial" HeaderText="Raz�n Social" SortExpression="RazonSocial" />
                                    <asp:BoundField DataField="Email" HeaderText="Correo" SortExpression="Email" />
                                    <asp:BoundField DataField="Contacto" HeaderText="Contacto" SortExpression="Contacto" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C" SortExpression="Ruc">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Telefono" HeaderText="Tel�fono" SortExpression="Telefono">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SitioWeb" HeaderText="Sitio Web" Visible="False" />
                                    <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtndelete" runat="server" CausesValidation="false" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/borrar.jpg" %>'
                                                ToolTip="Eliminar" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="right" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IdEmpresaContratante" HeaderText="IEmpresa" Visible="False" />
                                </Columns>
                                <HeaderStyle CssClass="cabecera" />
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
