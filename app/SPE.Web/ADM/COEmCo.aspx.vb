Imports System.Data
Imports System.Collections.Generic
Imports SPE.Utilitario

Partial Class ADM_COEmCo

    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CEmpresaContratante)


    
#Region "atributos"
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvEmpresa
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
#End Region

#Region "m�todos base"
    Public Overrides Sub OnClear()
        MyBase.OnClear()
        txtCodigo.Text = ""
        txtContacto.Text = ""
        txtEmail.Text = ""
        txtRazonSocial.Text = ""
        txtRUC.Text = ""
        ddlIdEstado.SelectedIndex = 0
        ddlPeriodo.SelectedIndex = 0
        gvEmpresa.DataSource = Nothing
        gvEmpresa.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        divResult.Visible = False
    End Sub

    Public Overrides Function AllowToSearch() As Boolean
        Dim a As SPE.Utilitario.ValidaRuc = New SPE.Utilitario.ValidaRuc()
        lblMensaje.Text = ""
        If (txtRUC.Text = "") Then
            Return True
        Else
            If (a.ValidarRuc(txtRUC.Text)) Then
                Return True
            Else
                ' ValidarImagen()
                lblMensaje.Text = "Ruc no valido"
                gvEmpresa.DataBind()
                lblMsgNroRegistros.Text = ""
                Exit Function
            End If
        End If

    End Function
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        divResult.Visible = True
        divgrilla.Visible = True
        Dim obeEmpresa As New SPE.Entidades.BEEmpresaContratante
        obeEmpresa.Codigo = txtCodigo.Text.TrimEnd()
        obeEmpresa.RazonSocial = txtRazonSocial.Text.TrimEnd()
        obeEmpresa.RUC = txtRUC.Text.TrimEnd()
        obeEmpresa.Contacto = txtContacto.Text.TrimEnd()
        obeEmpresa.Email = txtEmail.Text.TrimEnd()
        If ddlIdEstado.SelectedValue.ToString = "" Then
            obeEmpresa.IdEstado = 0
        Else
            obeEmpresa.IdEstado = ddlIdEstado.SelectedValue
        End If

        If ddlPeriodo.SelectedValue.ToString = "" Then
            obeEmpresa.IdEstado = 0
        Else
            obeEmpresa.IdPeriodoLiquidacion = CInt(ddlPeriodo.SelectedValue)
        End If

        Return obeEmpresa
    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvEmpresa.DataKeys(e.RowIndex).Values(0)
    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.MaintenanceKey = "ADEmCo"
        e.MaintenancePageName = "ADEmCo.aspx"
        e.ExecuteSearchOnFirstLoad = False
    End Sub

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)

            If ResultList.Count = 0 Then
                'divResult.Visible = False
                'divgrilla.Visible = False
                divlblmensaje.Visible = False
            End If

        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
            divlblmensaje.Visible = True
        End If
        divResult.Visible = True
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", "::: Todos :::")

        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", "::: Todos :::")

    End Sub
#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtRazonSocial)
        End If
        '
    End Sub

    Protected Sub gvEmpresa_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "DescripcionEstado", _
        "Inactivo", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub
#End Region

 


    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        divResult.Visible = False
        divgrilla.Visible = False
    End Sub
End Class
