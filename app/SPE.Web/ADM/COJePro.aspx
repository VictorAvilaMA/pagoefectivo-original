<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COJePro.aspx.vb" Inherits="ADM_COJePro" Title="PagoEfectivo - Consultar Jefe de Producto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Jefe de Producto</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombres:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombre" runat="server" MaxLength="100" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Apellidos: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtApellidos" runat="server" MaxLength="100" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Tipo Documento: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoDoc" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> N&uacute;mero Documento:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNumDoc" runat="server" MaxLength="15" CssClass="normal"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_txtNumDoc1" runat="server" FilterType="Numbers"
                            TargetControlID="txtNumDoc">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Email: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="normal"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revMailAlternativo" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="No se permiten tildes ni caracteres extra�os..." ValidationExpression="^[-_a-zA-Z0-9��.@ ]*$" />
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlIdEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" />
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear:both"></div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResultado" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <div>
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div>
                            <asp:GridView ID="gvResultado" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdUsuario" OnRowDataBound="gvResultado_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel.">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                                PostBackUrl="ADJePro.aspx" ToolTip="Actualizar" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Nombres" HeaderText="Nombres" SortExpression="Nombres" />
                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" SortExpression="Apellidos" />
                                    <asp:BoundField DataField="Telefono" HeaderText="Tel�fono" SortExpression="Telefono">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TipoDoc" HeaderText="Tipo Doc." SortExpression="TipoDoc" />
                                    <asp:BoundField DataField="NumeroDocumento" HeaderText="Num. Doc." SortExpression="NumeroDocumento" />
                                    <asp:BoundField DataField="Email" HeaderText="E-mail" SortExpression="Email">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IdUsuario" Visible="False" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="cabecera" />
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
