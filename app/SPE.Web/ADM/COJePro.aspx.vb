Imports System.Data
Imports System.Collections.Generic

Partial Class ADM_COJePro
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CJefeProducto)

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CJefeProducto
    'End Function


    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeJefeProducto As New SPE.Entidades.BEJefeProducto
        obeJefeProducto.Nombres = CStr(txtNombre.Text)
        obeJefeProducto.Apellidos = CStr(txtApellidos.Text)
        obeJefeProducto.Email = CStr(txtEmail.Text)
        If ddlIdEstado.SelectedValue.ToString = "" Then
            obeJefeProducto.IdEstado = 0
        Else
            obeJefeProducto.IdEstado = ddlIdEstado.SelectedValue
        End If
        If ddlTipoDoc.SelectedValue.ToString = String.Empty Then
            obeJefeProducto.IdTipoDocumento = 0
        Else
            obeJefeProducto.IdTipoDocumento = ddlTipoDoc.SelectedValue
        End If
        obeJefeProducto.NumeroDocumento = CStr(txtNumDoc.Text)
        Return obeJefeProducto

    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub


    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADJePro"
        e.MaintenancePageName = "ADJePro.aspx"
        e.ExecuteSearchOnFirstLoad = False

    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        txtNombre.Text = ""
        txtApellidos.Text = ""
        txtEmail.Text = ""
        txtNumDoc.Text = ""
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        ddlTipoDoc.SelectedIndex = 0
        ddlIdEstado.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        divResultado.Visible = False
    End Sub

    Private Sub CargaCombo()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESUS"), _
        "Descripcion", "Id", "::: Todos :::")
    End Sub

    Public Overrides Sub OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If
        divResultado.Visible = True
    End Sub

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNombre)
            CargaCombo()
            CargarComboTipoDocumento()
        End If
        '
    End Sub



    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    End Sub

    Private Sub CargarComboTipoDocumento()
        Dim CntrlParametro As New SPE.Web.CAdministrarParametro
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoDoc, CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoTipoDocumento), "Descripcion", "Id", "::: Todos :::")
    End Sub
End Class
