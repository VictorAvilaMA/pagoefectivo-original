﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COLogNav.aspx.vb" Inherits="ADM_COLogNav" Title="PagoEfectivo - Consultar Log de Navegación" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Log de Navegaci&oacute;n</h2>
    <div class="conten_pasos3">
        <h4>Datos del Cliente</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">>></span>Titular: </li>
            <li class="t2">
                <asp:Literal ID="ltrTitular" runat="server"></asp:Literal>
            </li>
            <li class="t1"><span class="color">>></span>Email: </li>
            <li class="t2">
                <asp:Literal ID="ltrCuenta" runat="server"></asp:Literal>
            </li>
        </ul>
        <div style="clear:both"></div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" Visible="false">
            <ContentTemplate>
                <div class="der cnt-date">
                    <span class="bold"></span><span class="value">
                        <asp:Literal ID="ltrFecha" runat="server"></asp:Literal>
                    </span>
                    <div class="clear">
                    </div>
                    <span class="bold">Hora: </span><span class="value">
                        <asp:Literal ID="ltrHora" runat="server"></asp:Literal>
                    </span>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div class="clear">
        </div>
        <h4>
            Criterios de búsqueda</h4>
        <asp:UpdatePanel ID="UpdatePanelFil" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Desde: </li>
                    <li style="line-height: 1; z-index: 200;" class="t2">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDe" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <span style="color: Red; display: none;" id="ctl00_ContentPlaceHolder1_MaskedEditValidatorDe">
                            </span>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaA" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaDe" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Desde' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Desde' no válida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>
                            <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaA" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Hasta' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Hasta' no válida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Rango de fechas no es válido"
                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                Type="Date" ValidationGroup="GrupoValidacion">*</asp:CompareValidator>
                        </p>
                    </li>
                </ul>
                <div class="clear">
                </div>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                    Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaDe">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                    Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaA">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="ibtnFechaDe" TargetControlID="txtFechaDe">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA"
                    TargetControlID="txtFechaA">
                </cc1:CalendarExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                <asp:Button ID="btnExcel" runat="server" CssClass="input_azul4" Text="Excel" />
            </li>
        </ul>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="true">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal></h5>
                        <div>
                            &nbsp;<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label></div>
                        <asp:GridView ID="gvResultado" runat="server" BackColor="White" DataKeyNames="IdUsuario"
                            CssClass="grilla" AllowPaging="True" CellPadding="3" BorderWidth="1px" BorderStyle="None"
                            BorderColor="#CCCCCC" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging"
                            OnRowDataBound="gvResultado_RowDataBound" AllowSorting="True" PageSize="12">
                            <Columns>
                                <asp:BoundField DataField="IdLogNavegacion" HeaderText="Código" SortExpression="IdLogNavegacion">
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaNavegacion" HeaderText="Fecha Navegaci&oacute;n"
                                    SortExpression="FechaNavegacion"></asp:BoundField>
                                <asp:BoundField DataField="UrlPage" HeaderText="URL Navegación" SortExpression="UrlPage">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Titulo" HeaderText="Nombre de la Página" SortExpression="">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PCName" HeaderText="Nombre de PC" SortExpression="PCName">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="hdnIdCliente" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
