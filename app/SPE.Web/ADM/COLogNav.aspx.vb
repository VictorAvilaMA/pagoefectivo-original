﻿Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Linq
Imports _3Dev.FW.Web
Imports SPE.Web.Seguridad

Partial Class ADM_COLogNav
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CComun)




#Region "atributos"
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Dim listaLogNavegacion As New List(Of BELogNavegacion)

#End Region

    Public Overrides Sub OnMainSearch()
        ListarLogNavegacionXIdUsuario()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = False Then
            hdnIdCliente.Value = VariableTransicion
            'VariableTransicion = Nothing

            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtFechaDe)

            txtFechaDe.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy")
            txtFechaA.Text = DateTime.Now.ToString("dd/MM/yyyy")
            ListarLogNavegacionXIdUsuario()
        End If
        Dim control As New SPEUsuarioClient
        Dim obeus As New BEUsuarioBase
        obeus = control.GetUsuarioById(CInt(hdnIdCliente.Value))

        If Not obeus Is Nothing Then
            ltrTitular.Text = obeus.NombresApellidos.ToString()
            ltrCuenta.Text = obeus.Email.ToString()
        Else
            JSMessageAlertWithRedirect("Alerta: ", "El usuario no tiene información completa o actualizada", "asa", "COClint.aspx")
        End If
        ltrFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")
        ltrHora.Text = DateTime.Now.ToString("hh:mm tt")
    End Sub

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        'hdnIdCliente.Value = obeCliente.IdCliente
        MyBase.AssignParametersToLoadMaintenanceEdit(key, e)
    End Sub


    Public Sub JSMessageAlertWithRedirect(ByVal tipo_alerta As String, ByVal mensaje As String, ByVal key As String, ByVal PageRedirect As String)
        Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", tipo_alerta, mensaje, PageRedirect)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub


    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeLogNavegacion As New BELogNavegacion
        'Dim listaLogNavegacion As New List(Of BELogNavegacion)
        obeLogNavegacion.IdUsuario = hdnIdCliente.Value

        obeLogNavegacion.FechaNavegacionDesde = CType(txtFechaDe.Text, Date)
        obeLogNavegacion.FechaNavegacionHasta = CType(txtFechaA.Text, Date)
        obeLogNavegacion.PropOrder = SortExpression
        obeLogNavegacion.TipoOrder = SortDir
        obeLogNavegacion.PageNumber = gvResultado.PageIndex
        obeLogNavegacion.PageSize = gvResultado.PageSize

        Return obeLogNavegacion
    End Function

    'BUSCAR
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarLogNavegacionXIdUsuario()
        ltrFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")
        ltrHora.Text = DateTime.Now.ToString("hh:mm tt")

    End Sub

    'LISTAR DE AGENCIAS RECAUDADORAS
    Private Sub ListarLogNavegacionXIdUsuario()
        '
        Try
            Dim objCComun As New SPE.Web.CComun

            Dim businessEntityObject As BELogNavegacion
            businessEntityObject = CreateBusinessEntityForSearch()

            'If (FlagPager) Then

            businessEntityObject.PageNumber = PageNumber
            businessEntityObject.PageSize = GridViewResult.PageSize
            businessEntityObject.TipoOrder = IIf(SortDir = SortDirection.Ascending, True, False)
            businessEntityObject.PropOrder = SortExpression
            GridViewResult.PageIndex = PageNumber - 1

            listaLogNavegacion = objCComun.ConsultarLogNavegacion(businessEntityObject)

            If listaLogNavegacion.Count() <> 0 Then


                Dim oObjectDataSource As New ObjectDataSource()

                oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID
                oObjectDataSource.EnablePaging = GridViewResult.AllowPaging
                oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
                oObjectDataSource.SelectMethod = "GetDataGrilla"
                oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
                oObjectDataSource.StartRowIndexParameterName = "filaInicio"
                oObjectDataSource.MaximumRowsParameterName = "maxFilas"
                oObjectDataSource.EnableViewState = False

                AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating

                'gvResultado.DataSource = listaLogNavegacion
                GridViewResult.DataSource = oObjectDataSource
                GridViewResult.DataBind()

            End If

            If listaLogNavegacion.Count() <> 0 Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listaLogNavegacion.First().TotalPageNumbers)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If
            divResult.Visible = True
            lblMensaje.Text = ""
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)

        End Try
        '
    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        e.ObjectInstance = New TotalRegistrosTableAdapter(listaLogNavegacion, listaLogNavegacion.First().TotalPageNumbers)
    End Sub

    'LIMPIAR
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        '
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        lblMensaje.Text = ""
        divResult.Visible = False
        '
    End Sub

    'Paginado de Grilla
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarLogNavegacionXIdUsuario()
        '
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvResultado.RowDataBound
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdUsuario", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

    'Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    '    '
    '    SortExpression = e.SortExpression
    '    If (SortDir.Equals(SortDirection.Ascending)) Then
    '        SortDir = SortDirection.Descending
    '    Else
    '        SortDir = SortDirection.Ascending
    '    End If
    '    ListarLogNavegacionXIdUsuario()
    '    '
    'End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else

            Dim idestado As Integer = 0
            'Response.Redirect(String.Format("~/Reportes/RptLogNavegacion.aspx?idCli={0}&FIni={1}&FFin={2}&PTit={3}&PEm={4}&Pfec={5}&Phr={6}", _
            'hdnIdCliente.Value, txtFechaDe.Text, txtFechaA.Text, ltrTitular.Text, ltrCuenta.Text, ltrFecha.Text, ltrHora.Text))

            Dim objCComun As New SPE.Web.CComun

            Dim obeLogNavegacion As New BELogNavegacion
            obeLogNavegacion.IdUsuario = CInt(hdnIdCliente.Value)
            obeLogNavegacion.FechaNavegacionDesde = CDate(txtFechaDe.Text)
            obeLogNavegacion.FechaNavegacionHasta = CDate(txtFechaA.Text)
            obeLogNavegacion.PageSize = (Int32.MaxValue - 1)
            obeLogNavegacion.PageNumber = 1
            obeLogNavegacion.PropOrder = ""
            obeLogNavegacion.TipoOrder = 0
            Dim lista As New List(Of BELogNavegacion)
            lista = objCComun.ConsultarLogNavegacion(obeLogNavegacion)

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Titular", ltrTitular.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Email", ltrCuenta.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Fecha", ltrFecha.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Hora", ltrHora.Text))
            

            UtilReport.ProcesoExportarGenerico(lista, Page, "EXCEL", "xls", "Log de Navegación - ", parametros, "RptLogNavegacion.rdlc")


        End If
    End Sub
End Class
