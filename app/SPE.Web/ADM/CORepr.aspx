<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="CORepr.aspx.vb" Inherits="ADM_CORepr" Title="PagoEfectivo - Consultar Representante" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Representante</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombres:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombre" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Apellidos: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtApellidos" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlIdEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" />
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombre" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtNombre">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtApellidos">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="upnlRepresentante" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" class="cont_cel" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <div>
                            &nbsp;<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <asp:GridView ID="gvResultado" AllowSorting="true" AllowPaging="True" runat="server"
                            CssClass="grilla" DataKeyNames="IdRepresentante" AutoGenerateColumns="False"
                            OnRowDataBound="gvResultado_RowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButton1" CommandName="Edit" PostBackUrl="ADRepr.aspx" runat="server"
                                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' ToolTip="Actualizar" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Nombres" SortExpression="Nombres" HeaderText="Nombres" />
                                <asp:BoundField DataField="Apellidos" SortExpression="Apellidos" HeaderText="Apellidos" />
                                <asp:BoundField DataField="Telefono" SortExpression="Telefono" HeaderText="Tel&#233;fono"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Email" SortExpression="Email" HeaderText="E-mail" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                    HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="IdRepresentante" Visible="False" />
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
