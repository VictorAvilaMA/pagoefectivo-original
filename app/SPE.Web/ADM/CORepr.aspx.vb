Imports System.Data
Imports System.Collections.Generic

Partial Class ADM_CORepr
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CRepresentante)

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CRepresentante
    'End Function

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeRepresentante As New SPE.Entidades.BERepresentante
        obeRepresentante.Nombres = txtNombre.Text
        obeRepresentante.Apellidos = txtApellidos.Text
        If ddlIdEstado.SelectedValue.ToString = "" Then
            obeRepresentante.IdEstado = 0
        Else

            obeRepresentante.IdEstado = ddlIdEstado.SelectedValue
            
        End If


        Return obeRepresentante

    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub


    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADRepr"
        e.MaintenancePageName = "ADRepr.aspx"
        e.ExecuteSearchOnFirstLoad = False

    End Sub
    
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        txtNombre.Text = ""
        txtApellidos.Text = ""
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        ddlIdEstado.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        divResult.Visible = False
    End Sub
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Sub OnFirstLoadPage()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESUS"), _
        "Descripcion", "Id", "::: Todos :::")

    End Sub

    Public Overrides Sub OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If
        divResult.Visible = True
    End Sub

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNombre)
        End If
        '
    End Sub


    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    End Sub
End Class
