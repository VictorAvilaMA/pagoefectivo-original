<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COServ.aspx.vb" Inherits="ADM_COServ" Title="PagoEfectivo - Consultar Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Servicio</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="6"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Empresa C.: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmpresaContratante" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Servicio: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreServicio" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="dropEstado" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" />
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtNombreServicio" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&.- "
                    TargetControlID="txtNombreServicio">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtEmpresaContratante" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&.- "
                    TargetControlID="txtEmpresaContratante">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                    <h5>
                        <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                    </h5>
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                        AutoGenerateColumns="False" DataKeyNames="IdServicio" AllowPaging="True" OnRowDataBound="gvResultado_RowDataBound">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADServ.aspx" runat="server"
                                        ToolTip="Actualizar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" SortExpression="Codigo" HeaderText="Cod. Serv">
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre" SortExpression="Nombre" HeaderText="Servicio">
                            </asp:BoundField>
                            <asp:BoundField DataField="NombreEmpresaContrante" SortExpression="NombreEmpresaContrante"
                                HeaderText="Emp. Contratante"></asp:BoundField>
                            <asp:BoundField DataField="TiempoExpiracion" SortExpression="TiempoExpiracion" HeaderText="T. Expiraci&#243;n"
                                DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtndelete" CausesValidation="false" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/borrar.jpg" %>'
                                        ToolTip="Eliminar" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="right" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
