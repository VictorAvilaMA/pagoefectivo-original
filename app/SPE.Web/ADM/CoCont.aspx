﻿<%@ Page Title="" Async="true" Language="VB" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="CoCont.aspx.vb" Inherits="ADM_CoCont" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Comisiones por Servicio</h2>
    <div class="inner-col-right">
        <div class="dlgrid">
            <fieldset>
                <h3>
                    Criterios de búsqueda</h3>
                <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="cont-panel-search">
                            <div class="even w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <asp:Label ID="lblCodigo" runat="server" CssClass="EtiquetaLargo" Text="Código:"></asp:Label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="normalCamp" Width="50px" MaxLength="6"></asp:TextBox>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <asp:Label ID="lbl1" runat="server" CssClass="EtiquetaLargo" Text="Empresa Contratante:"></asp:Label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtEmpresaContratante" MaxLength="100" runat="server" CssClass="normalCamp"
                                            Width="150px"></asp:TextBox>
                                    </dd>
                                </dl>
                            </div>
                            <div class="even w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <asp:Label ID="lblNombreServicio" runat="server" CssClass="EtiquetaLargo" Text="Nombre de servicio:"></asp:Label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtNombreServicio" MaxLength="100" runat="server" CssClass="normalCamp"
                                            Width="150px"></asp:TextBox>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <asp:Label ID="lblEstado" runat="server" CssClass="EtiquetaLargo" Text="Estado:"></asp:Label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:DropDownList ID="dropEstado" CssClass="TextoCombo" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </dd>
                                </dl>
                            </div>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtNombreServicio" runat="server" ValidChars="AaÁáBbCcDdEeÉéFfGgHhIiÍíJjKkLlMmNnÑñOoÓóPpQqRrSsTtUuÚúVvWwÜüXxYyZz'&.- "
                                TargetControlID="txtNombreServicio">
                            </cc1:FilteredTextBoxExtender>
                            <cc1:FilteredTextBoxExtender ID="FTBE_TxtEmpresaContratante" runat="server" ValidChars="AaÁáBbCcDdEeÉéFfGgHhIiÍíJjKkLlMmNnÑñOoÓóPpQqRrSsTtUuÚúVvWwÜüXxYyZz'&.- "
                                TargetControlID="txtEmpresaContratante">
                            </cc1:FilteredTextBoxExtender>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <div class="izq btns4">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar" Text="Buscar" />
                    <br />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="btnLimpiar" Text="Limpiar" />
                </div>
            </fieldset>
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h3>
                        <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                    </h3>
                    <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                        AutoGenerateColumns="False" DataKeyNames="IdServicio" AllowPaging="True" OnRowDataBound="gvResultado_RowDataBound">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sel.">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADCont.aspx" runat="server"
                                        ToolTip="Actualizar" ImageUrl="~/images/lupa.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" SortExpression="Codigo" HeaderText="Cod. Serv">
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre" SortExpression="Nombre" HeaderText="Servicio">
                            </asp:BoundField>
                            <asp:BoundField DataField="NombreEmpresaContrante" SortExpression="NombreEmpresaContrante"
                                HeaderText="Emp. Contratante"></asp:BoundField>
                            <asp:BoundField DataField="TiempoExpiracion" SortExpression="TiempoExpiracion" HeaderText="T. Expiraci&#243;n"
                                DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="TipoComision" SortExpression="TipoComision" HeaderText="Tipo Comision"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="MontoComision" SortExpression="MontoComision" HeaderText="Monto de Comision"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
