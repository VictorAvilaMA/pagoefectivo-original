<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRCoAdm.aspx.vb" Inherits="ADM_PRCoAdm" Title="PagoEfectivo - Contactar Administrador" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Page">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <h2>
            Contactar Administrador</h2>
        <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="conten_pasos3">
                    <h4>
                        Ingrese su consulta</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Tema :</li>
                        <li class="t2">
                            <asp:TextBox ID="txtTema" runat="server" CssClass="neu" style="float:left"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtTema" runat="server" ErrorMessage="El tema es requerido."
                                Text="*" ControlToValidate="txtTema"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0123456789qwertyuiopasdfghjkl�zxcvbnmQWERTYUIOPASDFGHJKL�ZXCVBNM,.- "
                                TargetControlID="txtTema">
                            </cc1:FilteredTextBoxExtender>
                        </li>
                        <li class="t1"><span class="color">>></span> Mensaje :</li>
                        <li class="t2" style="height: 285px">
                            <asp:TextBox ID="txtContenido" runat="server" TextMode="MultiLine" CssClass="neu"
                                Style="height: 280px;float:left;overflow-y:auto" cols="40" Rows="20"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftetxtContenido" runat="server" ValidChars="0123456789qwertyuiopasdfghjkl�zxcvbnmQWERTYUIOPASDFGHJKL�ZXCVBNM,.- "
                                TargetControlID="txtContenido">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El contenido es requerido."
                                Text="*" ControlToValidate="txtContenido"></asp:RequiredFieldValidator>
                            <div class="clear">
                            </div>
                        </li>
                        <li class="t1"><span class="color"></span></li>
                        <li class="t2" style="height:auto">
                            <asp:Label ID="lblmsjeContenido" runat="server" ForeColor="#3B3B3B" Font-Bold="true"
                                Width="100%" style="line-height:normal;text-align:left;height:1px;line-height:normal"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="w100"/>
                            <asp:Label ID="lblMensaje" CssClass="MensajeTransaccion" runat="server" Text="" Width="100%" style="float:none;line-height:normal;text-align:left;height:1px;line-height:normal"></asp:Label>
                            
                        </li>
                        <li class="complet">
                            <asp:Button ID="btnEnviar" runat="server" class="input_azul3" Text="Enviar" />
                            <asp:Button ID="btnLimpiar" runat="server" class="input_azul4" CausesValidation="false"
                                Text="Limpiar" />
                        </li>
                        <li class="t1"><span class="color"></span></li>
                        
                        <li class="t1"><span class="color"></span></li>
                        <li class="t2">
                            
                        </li>
                    </ul>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
