Imports System.Data
Imports SPE.Web.Util
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Web.Log

Partial Class ADM_PRCoAdm
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            Page.SetFocus(txtTema)
            txtContenido.Attributes.Add("onkeypress", " ValidarCaracteres2(this, 2000, $('#" + lblmsjeContenido.ClientID + "'));")
            txtContenido.Attributes.Add("onkeyup", " ValidarCaracteres2(this, 2000, $('#" + lblmsjeContenido.ClientID + "'));")
        End If
    End Sub

    Protected Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        Try
            If (txtContenido.Text.Trim() <> "" And txtTema.Text.Trim() <> "") Then
                Dim cemail As New SPE.Web.Util.UtilEmail()
                cemail.EnviarCorreoContactarAdministrador(User.Identity.Name, txtTema.Text, txtContenido.Text)
                lblMensaje.Text = "El mensaje se envi� correctamente al administrador."
            End If
        Catch ex As Exception
            'Logger.LogException(ex)
            'lblMensaje.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
            lblMensaje.Text = New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage
            lblMensaje.CssClass = "MensajeValidacion"

        End Try

    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador, SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolCliente, SPE.EmsambladoComun.ParametrosSistema.RolRepresentante, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        txtContenido.Text = ""
        txtTema.Text = ""
        lblMensaje.Text = ""
    End Sub
End Class
