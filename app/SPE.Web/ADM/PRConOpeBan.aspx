<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="PRConOpeBan.aspx.vb" Inherits="PRConOpeBan" title="Conciliar Agencias Bancarias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="Page">
    <asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>
        <h1>   
            <asp:Label ID="lblProceso" runat="server" Text="Conciliaci�n Agencias Bancarias"></asp:Label>
        </h1>
        
        <asp:Panel ID="PnlEmpresa" runat="server" Cssclass="divContenedor">   
		    <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" >
               1. Subir Archivo
            </div>
            <asp:UpdatePanel id="upnlEmpresa"  runat="server" UpdateMode="Conditional">
                <contenttemplate> 
                    <fieldset class="ContenedorEtiquetaTexto stripe">
                        <div id="div5" class="EtiquetaTextoIzquierda">
                            <asp:Label id="lblAdjuntarImagen" runat="server" Text="Seleccionar Archivo de Agencias Bancarias" CssClass="EtiquetaLargo" Width="142px"></asp:Label> 
                            <asp:FileUpload id="fulArchivoConciliacion" runat="server" ></asp:FileUpload> 
                            <asp:Label ID="lblUploadMens" runat="server" ForeColor="Red" Text="(Extensi�n v�lida: .txt)"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvArchivoConciliacion" ControlToValidate="fulArchivoConciliacion" runat="server" ErrorMessage="Debe seleccionar un archivo v�lido.">*</asp:RequiredFieldValidator>
                        </div>
                    </fieldset> 
                </contenttemplate>
            </asp:UpdatePanel>
        </asp:Panel> 
        <asp:Label ID="lblMensaje" runat="server"></asp:Label>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" /> 
        <fieldset id="fsBotonera" class="ContenedorBotonera" >
            <asp:Button ID="btnRegistrar" runat="server" CssClass="Boton"  Text="Conciliar" ValidationGroup="ValidaCampos" />
            <asp:Button ID="btnActualizar" runat="server" CssClass="Boton"  Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False" />
            <asp:Button ID="btnCancelar" runat="server" CssClass="Boton"   Text="Cancelar" />
         </fieldset>
          
        <asp:Panel ID="pnlConciliadas" runat="server" Cssclass="divContenedor" Visible="false">
            <asp:Label ID="lblOperacionesConciliadas" runat="server" Text="Operaciones conciliadas"></asp:Label>
            <asp:GridView id="gvResultado" AllowSorting="True" runat="server" CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False"   >
                <Columns>
                    <asp:BoundField DataField="NumeroOperacion"  HeaderText="Nro Operacion"></asp:BoundField>
                    <asp:BoundField DataField="NumeroOrdenPago"  HeaderText="Cod. Identif. Pago"  ></asp:BoundField>
                    <asp:BoundField DataField="FechaConciliacion"  HeaderText="Fecha Conciliaci&#243;n">
                        <ItemStyle HorizontalAlign="Right" Width="5px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DescripcionEstado"  HeaderText="Estado Conciliaci�n">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Observacion" HeaderText="Observacion" />
                </Columns>
                <HeaderStyle CssClass="cabecera"></HeaderStyle>
            </asp:GridView>
               
        </asp:Panel>
        <asp:Panel ID="pnlNoConciliadas" runat="server" Cssclass="divContenedor" Visible="false">
            <asp:Label ID="lblOperacionesNoConciliadas" runat="server" Text="Operaciones no conciliadas"></asp:Label>
            <asp:GridView ID="gvResultadoDos" AllowSorting="True" runat="server" CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False" >
                <Columns>
                    <asp:BoundField DataField="CodigoOficina"  HeaderText="Codigo Oficina"></asp:BoundField>
                    <asp:BoundField DataField="NumeroOperacion"  HeaderText="Numero Operacion"  ></asp:BoundField>
                    <asp:BoundField DataField="CodigoDepositante"  HeaderText="Cod. Identif. Pago">
                        <ItemStyle HorizontalAlign="Right" Width="5px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="FechaCancelacion" HeaderText="Fecha de Pago">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                </Columns>
                <HeaderStyle CssClass="cabecera"></HeaderStyle>
            </asp:GridView>

        </asp:Panel>    
</div>
</asp:Content>

