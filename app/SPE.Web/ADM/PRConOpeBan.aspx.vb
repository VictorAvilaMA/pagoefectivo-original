Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Partial Class PRConOpeBan
    Inherits System.Web.UI.Page
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    'Protected Sub gvEmpresa_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    'End Sub

    'Protected Sub gvEmpresa_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    'End Sub

    
    Sub ConciliacionBancaria()

        Dim objControladoraConciliacion As New SPE.Web.CAgenciaBancaria
        Dim objConciliacionBancaria As New BEConciliacion
        ' CargarDatosConciliacion()
        objConciliacionBancaria = CargarDatosConciliacion()
        If Not objConciliacionBancaria Is Nothing Then
            objConciliacionBancaria = objControladoraConciliacion.RegistrarConciliacionBancaria(objConciliacionBancaria)
            If Not objConciliacionBancaria.ListaOperacionesConciliadas Is Nothing Then
                If objConciliacionBancaria.ListaOperacionesConciliadas.Count > 0 Then
                    Me.pnlConciliadas.Visible = True
                    Me.gvResultado.DataSource = objConciliacionBancaria.ListaOperacionesConciliadas
                    Me.gvResultado.DataBind()
                Else
                    Me.pnlConciliadas.Visible = False
                End If
            End If
            If Not objConciliacionBancaria.ListaOperacionesNoConciliadas Is Nothing Then
                If objConciliacionBancaria.ListaOperacionesNoConciliadas.Count > 0 Then
                    Me.pnlNoConciliadas.Visible = True
                    Me.gvResultadoDos.DataSource = objConciliacionBancaria.ListaOperacionesNoConciliadas
                    Me.gvResultadoDos.DataBind()
                Else
                    Me.pnlNoConciliadas.Visible = False
                End If
            End If
        Else
            lblMensaje.Text = "Archivo no valido"
        End If
    End Sub
    Function CargarDatosConciliacion() As BEConciliacion
        Dim objConciliacion As New BEConciliacion

        Dim file As String = Me.fulArchivoConciliacion.FileName
        Dim extension As String = IO.Path.GetExtension(file)
        Dim fullPath As String = fulArchivoConciliacion.PostedFile.FileName
        If fullPath <> "" Then
            Dim sr As New System.IO.StreamReader(fullPath)
            If extension.ToUpper = ".TXT" Then
                objConciliacion.NombreArchivo = Me.fulArchivoConciliacion.FileName.ToString().Trim
                objConciliacion.ListaOperacionesBancarias = ListaOperacionesBancarias(sr)
                sr.Close()
                Return objConciliacion
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function
    Function ListaOperacionesBancarias(ByVal sr As System.IO.StreamReader) As List(Of String)
        Dim objListaOperacionesBancarias As New List(Of String)
        Do While sr.Peek() > 0
            objListaOperacionesBancarias.Add(sr.ReadLine)
        Loop

        Return objListaOperacionesBancarias
    End Function
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        ConciliacionBancaria()
    End Sub
End Class
