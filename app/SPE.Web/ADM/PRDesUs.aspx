<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ADM/PRDesUs.aspx.vb" Inherits="ADM_PRDesUs"
    Title="PagoEfectivo - Desbloquear Usuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="colRight">
        <asp:ScriptManager ID="ScriptManager" runat="server">
        </asp:ScriptManager>
        <h2>
            Desbloquear Usuario</h2>
        <div class="conten_pasos3">
            <h4>
                B�squeda de Usuario</h4>
            <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> E-mail: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="normal"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" 
                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                            ToolTip="Cargar Usuario"
                                Style="float: left; padding: 10px" />
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                ErrorMessage="Formato de Email incorrecto" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                Style="width: auto">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                                Style="width: auto" ErrorMessage="Debe ingresar un e-mail para realizar la b�squeda.">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="width: 100%;
                        padding-left: 200px" />
                    <div style="clear: both">
                    </div>
                    <div id="divInformacionUsuario" runat="server">
                        <h4>
                            Informacion General</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> E-mail: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtMailUsuario" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Nombres: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtNombres" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Apellidos: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtApellidos" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Telefono: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Direcci&oacute;n: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Estado: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtDescripcionEstado" runat="server" CssClass="normal" ReadOnly="True"></asp:TextBox>
                            </li>
                        </ul>
                    </div>
                    <div style="clear: both">
                    </div>
                    <asp:Panel ID="pnlHabilitado" runat="server" class="inner-col-right ">
                        <div class="divContenedor" id="divBloqueo" runat="server">
                            <h4>
                                Estado</h4>
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color"></li>
                                <li class="t2">
                                    <asp:RadioButton ID="rbtDesbloquear" runat="server" Text="Desbloquear" GroupName="Bloqueo" />
                                    <asp:RadioButton ID="rbtBloquear" runat="server" GroupName="Bloqueo" Text="Bloquear" /></fieldset>
                                </li>
                            </ul>
                            <div id="divBotonera" runat="server">
                                <ul class="datos_cip2">
                                    <li class="complet">
                                        <asp:Button ID="btnModificar" runat="server" Text="Actualizar" CssClass="input_azul3"
                                            OnClientClick="return ConfirmMe();" />
                                        <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" CssClass="input_azul4" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </asp:Panel>
                    <div id="divMensaje" runat="server">
                        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
