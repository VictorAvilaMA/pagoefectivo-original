Imports System.Data
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.Web
Partial Class ADM_PRDesUs
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CCliente)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        

        If Not Page.IsPostBack Then
            'Me.divCombo.Visible = False
            Page.SetFocus(txtEmail)
            VisualizarDivs(False)

            Me.rbtBloquear.Visible = False
            Me.rbtDesbloquear.Visible = False
            Me.lblMensaje.Visible = False


        End If
        '
    End Sub
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Sub VisualizarDivs(ByVal estado As Boolean)
        divBotonera.Visible = estado
        divBloqueo.Visible = estado
        divInformacionUsuario.Visible = estado
        divMensaje.Visible = estado
    End Sub
    Sub Habilitado(ByVal valor As Boolean)
        Me.pnlHabilitado.Enabled = valor
        Me.btnModificar.Enabled = valor
    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        ConsultarUsuario()

    End Sub
    Private Sub ConsultarUsuario()
        Dim objCCliente As New SPE.Web.CCliente



        Dim obeUsuario As New _3Dev.FW.Entidades.Seguridad.BEUsuario

        obeUsuario = objCCliente.GetRecordByID(Me.txtEmail.Text)
        If obeUsuario Is Nothing Then

            Me.lblMensaje.CssClass = "MensajeTransaccion"
            lblMensaje.Text = "No se encontraron coincidencias."
            lblMensaje.Visible = True

            VisualizarDivs(False)
            lblMensaje.Visible = True
            Me.divMensaje.Visible = True
            Habilitado(False)

        Else
            divMensaje.Visible = False
            lblMensaje.Visible = False
            Me.txtMailUsuario.Text = obeUsuario.Email
            Me.txtNombres.Text = obeUsuario.Nombres
            Me.txtApellidos.Text = obeUsuario.Apellidos
            Me.txtTelefono.Text = obeUsuario.Telefono
            Me.txtDireccion.Text = obeUsuario.Direccion
            Me.txtDescripcionEstado.Text = obeUsuario.DescripcionEstado

            VisualizarDivs(True)
            Habilitado(True)


            If obeUsuario.DescripcionEstado = "Activo" Then
                Me.rbtBloquear.Visible = True
                Me.rbtDesbloquear.Visible = False
            ElseIf obeUsuario.DescripcionEstado = "Bloqueado" Then
                Me.rbtBloquear.Visible = False
                Me.rbtDesbloquear.Visible = True
            End If
        End If
    End Sub

    Sub EstadoSeleccion(ByVal valor As Boolean)
        Me.rbtBloquear.Checked = valor
        Me.rbtDesbloquear.Checked = valor
    End Sub



    Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Dim obeUsuario As New _3Dev.FW.Entidades.Seguridad.BEUsuario
        Dim resultado As Integer
        Dim objCUsuario As New CCliente
        Dim strEstadoBloqueo As String = ""

        If Me.rbtBloquear.Checked Then

            obeUsuario.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado
        ElseIf Me.rbtDesbloquear.Checked Then

            obeUsuario.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo
        End If

        If obeUsuario.IdEstado > 0 Then
            obeUsuario.Email = Me.txtMailUsuario.Text
            resultado = objCUsuario.ActualizarEstadoUsuario(obeUsuario)

            EstadoSeleccion(False)
            Habilitado(False)
            
            lblMensaje.Text = "El estado del usuario se actualiz� satisfactoriamente."
            lblMensaje.Visible = True
        Else
            lblMensaje.CssClass = "MensajeTransaccion"
            lblMensaje.Text = "El estado del usuario se mantuvo igual."
            lblMensaje.Visible = True

        End If

    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        EstadoSeleccion(True)
        VisualizarDivs(False)
        Me.txtEmail.Text = ""


    End Sub
End Class
