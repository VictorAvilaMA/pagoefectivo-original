<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PgPrl.aspx.vb" Inherits="ADM_PgPrl" Title="PagoEfectivo - P�gina Principal" %>

<%@ OutputCache VaryByParam="None" Duration="10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/base.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/screen.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/fixScreen.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #contenidos #content h3
        {
            margin: 0;
            line-height: 16px;
            text-align: left;
        }
        #header
        {
            padding-left: 0;            
        }
        .colRight .widget-content ul li
        {
            float: left;
        }
    </style>
    <div class="colRight" style="width: 740px; padding-top: 20px">
        <div class="inner-col-right">
            <div class="col-first">
                <div class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Cliente</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="ADClint.aspx">Registrar Cliente</a></li>
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COClint.aspx">Consultar Cliente</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/ic-cliente.jpg" alt="cliente" /></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Agentes</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="ADAgen.aspx">Registrar Agente</a></li>
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COAgen.aspx">Consultar Agente</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/ic-agente.jpg" alt="agente" /></div>
                    </div>
                </div>
            </div>
            <div class="col-last">
                <div class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Agencias recaudadoras</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="ADAgRe.aspx">Registrar Agencia Recaudadora</a></li>
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COAgRe.aspx">Consultar Agencia Recaudadora</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/ic-agencia.jpg" alt="agencia" /></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Empresa contratante</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="ADEmCo.aspx">Registrar Empresa Contratante</a></li>
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COEmCo.aspx">Consultar Empresa Contratante</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/ic-empresa.jpg" alt="empresa" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
