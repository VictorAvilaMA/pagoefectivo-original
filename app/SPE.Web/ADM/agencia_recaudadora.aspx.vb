Imports SPE.EmsambladoComun.ParametrosSistema
Partial Class ADM_agencia_recaudadora
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Context.User Is Nothing Then
            If Context.User.IsInRole(RolAdministrador) Then
                Response.Redirect("~/ADM/PgPrl.aspx")
            ElseIf Context.User.IsInRole(RolJefeProducto) Then
                Response.Redirect("~/JPR/PgPrl.aspx")
            End If
        End If


    End Sub
End Class
