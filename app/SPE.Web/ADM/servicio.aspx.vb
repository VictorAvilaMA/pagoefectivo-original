Imports SPE.EmsambladoComun.ParametrosSistema
Partial Class ADM_servicio
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Context.User Is Nothing Then
            If Context.User.IsInRole(RolAdministrador) Then
                Response.Redirect("~/ADM/PgPrl.aspx")
            End If
        End If


    End Sub
End Class
