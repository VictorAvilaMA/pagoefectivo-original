<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADCaja.aspx.vb" Inherits="ARE_ADCaja" Title="PagoEfectivo - Administraci�n de Cajas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page" class="colRight">
        <h1>
            Administraci&oacute;n de Cajas</h1>
        <div id="div3" class="inner-col-right">
            <div id="divInfGeneral">
                <div class="dlgrid">
                    <asp:UpdatePanel ID="upnlControles" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h4>
                                Registro de Caja</h4>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lbl1" runat="server" Text="Agencia Recaudadora:"></asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="txtAgenciaRecaudadora" runat="server"></asp:Literal></label>
                                    </dd>
                                    <asp:Label ID="lblIdCaja" runat="server" Text="0" Visible="False"></asp:Label>
                                    <asp:Label ID="lblIdAgencia" runat="server" Text="0" Visible="False"></asp:Label>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class=" clearfix dt15 dd60">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="Label4" runat="server" Text="Nombre Caja:">
                                            </asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtNombre" runat="server" class="normalCamp mright10" ValidationGroup="ValidaControles"
                                            MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtNombre"
                                            ValidationGroup="ValidaControles" ErrorMessage="Ingrese el nombre de la Caja"
                                            Text="*"></asp:RequiredFieldValidator>
                                        <asp:Button ID="btnOperacion" runat="server" CssClass="btnRegistrar" ValidationGroup="ValidaControles"
                                            Text="Registrar" />
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd60">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblEstado" runat="server" Text="Estado:" Visible="False">
                                            </asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:DropDownList ID="ddlEstado" runat="server" Visible="False" CssClass="TextoCombo">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblIdEstadoUso" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="lblIdEstado" runat="server" Text="lblIdEstado" CssClass="Hidden"></asp:Label>
                                    </dd>
                                </dl>
                            </div>
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="ValidaControles"
                                runat="server" />
                            <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombre" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz0123456789 "
                                TargetControlID="txtNombre">
                            </cc1:FilteredTextBoxExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="sep_a14">
        </div>
        <div id="div8" class="inner-col-right">
            <div id="divCaja" class="dlgrid">
                <asp:UpdatePanel ID="upnlCaja" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h3>
                            <label>
                                <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                            </label>
                        </h3>
                        <asp:GridView ID="gvCaja" runat="server" BackColor="White" CssClass="result" Width="360px"
                            DataKeyNames="IdEstado" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gvCaja_PageIndexChanging"
                            ShowFooter="False">
                            <Columns>
                                <asp:BoundField DataField="IdCaja">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Estado" HeaderText="Estado">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnEdit" runat="server"
                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/ok2.jpg"%>'
                                        CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>"
                                        CommandName="Select" OnCommand="ibtnEdit_Command"></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se tienen cajas registradas.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnOperacion" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="sep_a14">
    </div>
    <asp:Label ID="lblMensaje" runat="server" CssClass="EtiquetaMensaje"></asp:Label>
    <fieldset id="fsBotonera" class="ContenedorBotonera">
        <asp:Button ID="btnCancelar" runat="server" CssClass="btnCancelar" Text="Cancelar" />
    </fieldset>
    <asp:Label ID="lblVincularPopup" runat="server" CssClass="Hidden"></asp:Label>
    <cc1:ModalPopupExtender ID="mppCaja" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="imgbtnRegresar" X="300" Y="200" PopupControlID="pnlPopupEmpresaContrante"
        TargetControlID="lblVincularPopup">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnlPopupEmpresaContrante" runat="server" Style="display: none; width: 300px;">
        <div id="divBusquedaRepresentantes" class="divContenedorPopPup">
            <div id="div11" class="divContenedorTitulo">
                <div class="divBtnCerrarPopPup">
                    <asp:ImageButton ID="imgbtnRegresar" 
                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/closeX.GIF"%>'
                                    CausesValidation="false"
                        runat="server"></asp:ImageButton>
                </div>
                <asp:Label ID="lblTitulo" runat="server" Width="248px"><b>Actualizar</b></asp:Label>
            </div>
            <asp:UpdatePanel ID="upnlActualizarCaja" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <fieldset class="ContenedorEtiquetaTexto">
                            <div id="Div6" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="Label3" runat="server" Text="Nombre:" CssClass="EtiquetaLargo"></asp:Label>
                                <asp:TextBox ID="txtUpdNombre" runat="server" CssClass="Texto" Width="111px" MaxLength="100"></asp:TextBox>
                            </div>
                        </fieldset>
                        <fieldset class="ContenedorEtiquetaTexto">
                            <div id="Div4" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="Label1" runat="server" Text="Estado:" CssClass="EtiquetaLargo"></asp:Label>
                                <asp:DropDownList ID="ddlUpdEstado" runat="server" CssClass="TextoCombo">
                                </asp:DropDownList>
                            </div>
                        </fieldset>
                        <div style="text-align: center;">
                            <br />
                            <asp:Button ID="btnActualizar" OnClick="btnActualizar_Click" runat="server" Text="Actualizar" style="float: none;"
                                CssClass="btnActualizar" ValidationGroup="ValidaActualizacion"></asp:Button>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtUpdNombre" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'0123456789 "
                                TargetControlID="txtUpdNombre">
                            </cc1:FilteredTextBoxExtender>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvCaja" EventName="RowCommand"></asp:AsyncPostBackTrigger>
                    <asp:PostBackTrigger ControlID="btnActualizar"></asp:PostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUpdNombre"
                ErrorMessage="Ingrese el nombre de la Caja." ValidationGroup="ValidaActualizacion"></asp:RequiredFieldValidator>
        </div>
    </asp:Panel>
</asp:Content>
