Imports System.Collections.Generic
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Web.Security
Imports SPE.Entidades
Partial Class ARE_ADCaja
    Inherits System.Web.UI.Page

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Page.SetFocus(txtNombre)
            CargarAgencia()
            CargarCombos()
            lblIdEstadoUso.Text = SPE.EmsambladoComun.ParametrosSistema.EstadoUsoCaja.Inactivo
            ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
            CargarDataGrid()
        End If

    End Sub

    Private Sub CargarAgencia()

        Dim objCCaja As New SPE.Web.CCaja
        Dim obeAgencia As New SPE.Entidades.BEAgenciaRecaudadora

        obeAgencia = objCCaja.ObtenerAgenciaPorUsuario(UserInfo.IdUsuario)
        txtAgenciaRecaudadora.Text = obeAgencia.NombreComercial
        lblIdAgencia.Text = obeAgencia.IdAgenciaRecaudadora.ToString

    End Sub

    Private Sub CargarDataGrid()
        '
        Dim objCCaja As New SPE.Web.CCaja
        Dim listCaja As New List(Of BECaja)
        listCaja = objCCaja.ConsultarCaja(CInt(lblIdAgencia.Text))
        gvCaja.DataSource = listCaja
        gvCaja.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listCaja.Count)
        '
    End Sub

    Private Sub CargarCombos()

        Dim cCaja As New SPE.Web.CCaja
        Dim cParametros As New SPE.Web.CAdministrarParametro

        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, cParametros.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id")

        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlUpdEstado, cParametros.ConsultarParametroPorCodigoGrupo("ESMA"), _
                "Descripcion", "Id")

    End Sub

    Protected Sub btnOperacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOperacion.Click

        Dim cCaja As New SPE.Web.CCaja
        lblMensaje.Text = ""
        Try

            If ExisteNombreCaja(txtNombre.Text, 0) = False Then
                cCaja.RegistrarCaja(ObtenerEntidad("Registrar"))
                txtNombre.Text = ""
                MostrarMensaje(False, "La operaci�n se realiz� con satisfactoriamente.")
                CargarDataGrid()
            Else
                MostrarMensaje(True, "Ya existe una caja con el nombre: " + txtNombre.Text + ". La operaci�n no se puede realizar. ")
            End If


        Catch ex As Exception
            lblMensaje.Text = New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage
            'Logger.LogException(ex)
        End Try

    End Sub

    Private Function ObtenerEntidad(ByVal operacion As String) As SPE.Entidades.BECaja

        Dim obeCaja As New SPE.Entidades.BECaja

        obeCaja.IdAgenciaRecaudadora = CInt(lblIdAgencia.Text)

        If operacion = "Registrar" Then
            obeCaja.Nombre = txtNombre.Text
            obeCaja.IdEstado = ddlEstado.SelectedValue
        ElseIf operacion = "Actualizar" Then
            obeCaja.IdCaja = CInt(lblIdCaja.Text)
            obeCaja.Nombre = txtUpdNombre.Text
            obeCaja.IdEstado = ddlUpdEstado.SelectedValue
        End If

        obeCaja.IdUsoCaja = CInt(lblIdEstadoUso.Text)
        obeCaja.IdUsuarioCreacion = UserInfo.IdUsuario
        obeCaja.IdUsuarioActualizacion = UserInfo.IdUsuario

        Return obeCaja
    End Function

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("~/ARE/PgPrl.aspx")
    End Sub

    Private Function ObtenerEstadoRegistro(ByVal IdCaja As Integer, ByVal Operacion As String) As Integer
        Dim cCaja As New SPE.Web.CCaja
        Dim IdEstado As Integer

        If (Operacion = "Estado") Then
            For Each Entidad As SPE.Entidades.BECaja In cCaja.ConsultarCaja(CInt(lblIdAgencia.Text))
                If (Entidad.IdCaja = IdCaja) Then IdEstado = Entidad.IdEstado
            Next
        ElseIf Operacion = "EstadoUso" Then
            For Each Entidad As SPE.Entidades.BECaja In cCaja.ConsultarCaja(CInt(lblIdAgencia.Text))
                If (Entidad.IdCaja = IdCaja) Then IdEstado = Entidad.IdUsoCaja
            Next
        End If

        Return IdEstado
    End Function

    Protected Sub ibtnEdit_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)

        If e.CommandName = "Select" Then

            lblTitulo.Text = "Actualizar " + gvCaja.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
            lblIdCaja.Text = gvCaja.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).Text
            txtUpdNombre.Text = gvCaja.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
            lblIdEstado.Text = ObtenerEstadoRegistro(CInt(lblIdCaja.Text), "Estado").ToString
            ddlUpdEstado.SelectedValue = CInt(lblIdEstado.Text)
            lblIdEstadoUso.Text = ObtenerEstadoRegistro(CInt(lblIdCaja.Text), "EstadoUso").ToString

            mppCaja.Show()

        End If

    End Sub

    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click

        Try
            If ExisteNombreCaja(txtUpdNombre.Text, Convert.ToInt32(lblIdCaja.Text)) = False Then


                lblMensaje.Text = ""
                Dim cCaja As New SPE.Web.CCaja
                Dim obeCaja As New SPE.Entidades.BECaja

                cCaja.ActualizarCaja(ObtenerEntidad("Actualizar"))
                MostrarMensaje(False, "La operaci�n se realiz� con �xito.")
                CargarDataGrid()
            Else
                MostrarMensaje(True, "Ya existe una caja con el nombre: " + txtUpdNombre.Text + ". La operaci�n no se pudo realizar.")
            End If
        Catch ex As Exception
            'Logger.LogException(ex)
            MostrarMensaje(True, SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        
        

    End Sub

    Private Function ExisteNombreCaja(ByVal NombreCaja As String, ByVal pos As Integer) As Boolean
        Dim row As GridViewRow
        Dim Existe As Boolean = False
        For Each row In gvCaja.Rows
            If (gvCaja.Rows(row.RowIndex).Cells(1).Text = NombreCaja) And (pos = 0 Or pos <> gvCaja.Rows(row.RowIndex).Cells(0).Text) Then
                Existe = True
            End If
        Next

        Return Existe
    End Function

    Private Sub MostrarMensaje(ByVal esValidacion As Boolean, ByVal msg As String)
        If (esValidacion) Then
            lblMensaje.CssClass = "MensajeValidacion"
        Else
            lblMensaje.CssClass = "MensajeTransaccion"
        End If
        lblMensaje.Text = msg
    End Sub

    Protected Sub gvCaja_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvCaja.PageIndex = e.NewPageIndex
        CargarDataGrid()
        '
    End Sub
End Class
