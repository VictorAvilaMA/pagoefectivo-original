<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCaAc.aspx.vb" Inherits="ARE_COCaAc" Title="PagoEfectivo - Consultar Cajas Activas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page" class="dlgrid">
        <h1>
            Consultar Cajas Activas</h1>
        <h3>
            Lista de cajas activadas por los agentes de la Agencia</h3>
        <fieldset style="visibility: hidden; height: 1px">
        </fieldset>
        <div class="even w100 clearfix dlinline">
            <dl class="even clearfix dt15 dd60">
                <dt class="desc">
                    <label>
                        Nro de Cajas Activas:</label>
                </dt>
                <dd class="camp">
                    <asp:Label ID="lblDescNumeroCajasAbiertas" runat="server" CssClass="msj" Width="250px"
                        Style="text-align: left"></asp:Label>
                </dd>
            </dl>
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="result">
                    <asp:GridView ID="grdResultado" runat="server" BackColor="White" CssClass="grilla"
                        AllowPaging="True" AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None"
                        BorderWidth="1px" CellPadding="3" OnPageIndexChanging="grdResultado_PageIndexChanging"
                        AllowSorting="True" OnSorting="grdResultado_Sorting">
                        <Columns>
                            <asp:BoundField DataField="NombreAgente" HeaderText="Agente" SortExpression="NombreAgente">
                            </asp:BoundField>
                            <asp:BoundField DataField="NombreCaja" HeaderText="Caja" SortExpression="NombreCaja">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaApertura" HeaderText="Fecha Apertura" SortExpression="FechaApertura">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NroOPRecibidas" HeaderText="C.I.P. Recibidas" SortExpression="NroOPRecibidas">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NroOPAnuladas" HeaderText="C.I.P. Anuladas" SortExpression="NroOPAnuladas">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
