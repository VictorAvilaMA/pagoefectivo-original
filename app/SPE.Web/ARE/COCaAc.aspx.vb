Imports System.Data
Imports SPE.Entidades

Partial Class ARE_COCaAc
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '
            ListarCajasctivas()
            '
        End If
    End Sub

    'CARGA DE DATOS
    Private Sub ListarCajasctivas()
        '
        Try
            Dim objCAdministarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgenteCaja As New BEAgenteCaja
            Dim obeAgente As New BEAgente

            obeAgente = objCAdministarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)

            obeAgenteCaja.IdAgenciaRecaudadora = obeAgente.IdAgenciaRecaudadora
            obeAgenteCaja.IdUsuarioCreacion = 0 'TODOS
            obeAgenteCaja.NombreAgente = ""
            obeAgenteCaja.NombreEstado = "Apertura" 'PARA EL TIPO DE FECHA
            obeAgenteCaja.FechaCreacion = Convert.ToDateTime("01/01/1753")
            obeAgenteCaja.FechaActualizacion = Convert.ToDateTime("01/01/9999")
            obeAgenteCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenteCaja.Aperturado
            obeAgenteCaja.OrderBy = SortExpression
            obeAgenteCaja.IsAccending = SortDir
            grdResultado.DataSource = objCAdministarAgenciaRecaudadora.ConsultarCajaAvanzada(obeAgenteCaja)
            grdResultado.DataBind()
            '

            If grdResultado.Rows.Count = 0 Then
                lblDescNumeroCajasAbiertas.Text = "No se encontraron cajas activas."
                lblDescNumeroCajasAbiertas.ForeColor = Drawing.Color.Red
            Else
                lblDescNumeroCajasAbiertas.Text = grdResultado.Rows.Count.ToString
            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try

    End Sub

    'PAGINADO DE LA GRILLA
    Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        grdResultado.PageIndex = e.NewPageIndex
        ListarCajasctivas()
        '
    End Sub

    Protected Sub grdResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        '
        SortExpression = e.SortExpression
        If (SortDir.Equals(SortDirection.Ascending)) Then
            SortDir = SortDirection.Descending
        Else
            SortDir = SortDirection.Ascending
        End If
        ListarCajasctivas()
        '
    End Sub
End Class

