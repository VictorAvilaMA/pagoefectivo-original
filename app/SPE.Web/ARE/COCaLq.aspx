<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCaLq.aspx.vb" Inherits="ARE_COCaLq" Title="PagoEfectivo - Consultar Caja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <div id="Page">
        <h1>
            Consultar Caja</h1>
        <div id="divHistorialOrdenesPago" class="dlgrid">
            <h3>
                Criterios de b�squeda</h3>
            <div id="divCriterios">
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="div8" class="cont-panel-search">
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd60">
                                    <div>
                                        <dt class="desc">
                                            <label>
                                                <asp:Literal ID="lblEstado" runat="server" Text="Estado:"></asp:Literal></label>
                                        </dt>
                                        <dt class="desc">
                                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="clsform1">
                                            </asp:DropDownList>
                                        </dt>
                                    </div>
                                    <div>
                                        <dt class="desc">
                                            <label class="der">
                                                <asp:Literal ID="lblFecha" runat="server" Text="Fecha:"></asp:Literal>
                                            </label>
                                        </dt>
                                        <dt class="desc">
                                            <asp:DropDownList ID="ddlTipoFecha" runat="server" CssClass="clsform1">
                                            </asp:DropDownList>
                                        </dt>
                                    </div>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd70">
                                    <dt class="desc">
                                        <asp:Literal ID="lblFechaDe" runat="server" Text="Del:"></asp:Literal>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtFechaDe" runat="server" CssClass="miniCamp izq"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaDe" CssClass="icon-calendar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/icon-calendar.gif" %>'>
                                        </asp:ImageButton>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                            ControlToValidate="txtFechaDe" ErrorMessage="*" InvalidValueMessage="Formato de fecha 'Del' no v�lido"
                                            IsValidEmpty="False" EmptyValueMessage="Fecha 'Del' es requerida" Display="Dynamic"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>
                                        <div class="der">
                                            <label class="izq">
                                                <asp:Literal ID="Label6" runat="server" Text="Al:"></asp:Literal>
                                            </label>
                                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="miniCamp izq"></asp:TextBox>
                                            <asp:ImageButton ID="ibtnFechaA" CssClass="icon-calendar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/icon-calendar.gif" %>'>
                                            </asp:ImageButton>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                                ControlToValidate="txtFechaA" ErrorMessage="*" InvalidValueMessage="Formato de fecha 'Al' no v�lido"
                                                IsValidEmpty="False" EmptyValueMessage="Fecha 'Al' es requerida" Display="Dynamic"
                                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>
                                            <asp:CompareValidator ID="covFecha" runat="server" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                                Type="Date" ValidationGroup="GrupoValidacion">*</asp:CompareValidator>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                            <asp:Label ID="lblIdAgenciaRecaudadora" runat="server" Visible="False">0</asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" ValidationGroup="GrupoValidacion">
                            </asp:ValidationSummary>
                        </div>
                        <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" MaskType="Date"
                            Mask="99/99/9999" TargetControlID="txtFechaDe" CultureName="es-PE">
                        </cc1:MaskedEditExtender>
                        <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" MaskType="Date" Mask="99/99/9999"
                            TargetControlID="txtFechaA" CultureName="es-PE">
                        </cc1:MaskedEditExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaDe"
                            PopupButtonID="ibtnFechaDe" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaA"
                            PopupButtonID="ibtnFechaA" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div id="Fieldset2" class="izq btns4">
                <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar" Text="Buscar" ValidationGroup="GrupoValidacion">
                </asp:Button>
                <br />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="btnLimpiar" Text="Limpiar" />
            </div>
        </div>
        <div class="clear"></div>
        <div id="div6" class="dlgrid">
            <asp:UpdatePanel runat="server" ID="UpdatePanelResultado" UpdateMode="Conditional">
                <ContentTemplate>
                    <h3>
                        <label>
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </label>
                    </h3>
                    <asp:GridView ID="grdResultado" runat="server" BackColor="White" CssClass="result"
                        AllowPaging="True" AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None"
                        BorderWidth="1px" CellPadding="3" OnPageIndexChanging="grdResultado_PageIndexChanging"
                        OnRowDataBound="grdResultado_RowDataBound" AllowSorting="True" OnSorting="grdResultado_Sorting">
                        <Columns>
                            <asp:BoundField DataField="NombreCaja" HeaderText="Caja" SortExpression="NombreCaja">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaApertura" HeaderText="Fecha Apertura" SortExpression="FechaApertura">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaCierre" HeaderText="Fecha Cierre" DataFormatString="{0:d}"
                                SortExpression="FechaCierre">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NroOPRecibidas" HeaderText="C.I.P. Recibidas" SortExpression="NroOPRecibidas">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NroOPAnuladas" HeaderText="C.I.P. Anuladas" Visible="False"
                                SortExpression="NroOPAnuladas"></asp:BoundField>
                            <asp:BoundField DataField="NombreEstado" HeaderText="Estado" SortExpression="NombreEstado">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnDetalleCaja" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/lupa.gif" %>'
                                        ToolTip="Detalle de Caja" CommandArgument='<%# Eval("IdAgenteCaja") %>' OnCommand="ibtnDetalleCaja_Command"
                                        CommandName="Seleccionar" OnClientClick="javascript:openPRPRDeMoCaReporte();" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                        <FooterStyle Font-Bold="False" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>