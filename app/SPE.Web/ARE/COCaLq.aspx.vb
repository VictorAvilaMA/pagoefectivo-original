Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class ARE_COCaLq
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    'Evento Load de la Pagina
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            '
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(ddlEstado)
            CargarDatos()
            '
        End If
    End Sub

    Public Sub InicializarFecha()
        'FECHAS
        txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaA.Text = Today.ToShortDateString
    End Sub

    'CARGAR COMBOS
    Private Sub CargarDatos()
        '
        InicializarFecha()
        '
        Dim objCAdministrarParametro As New SPE.Web.CAdministrarParametro
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCAdministrarParametro.ConsultarParametroPorCodigoGrupo("ESAC") : ddlEstado.DataBind()
        ddlEstado.Items.RemoveAt(0)
        ddlEstado.Items.Insert(0, "::: Todos :::")
        '
        ddlTipoFecha.Items.Insert(0, "Apertura")
        ddlTipoFecha.Items.Insert(1, "Cierre")
        '
        Dim objCAdministarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgente As New BEAgente
        obeAgente = objCAdministarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        lblIdAgenciaRecaudadora.Text = obeAgente.IdAgenciaRecaudadora.ToString
        '
    End Sub

    'BUSQUEDA DE CAJA
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        '
        If ValidarFecha() = True Then
            '
            ListarCaja()
            '
        End If
        '
    End Sub

    'VALIDAMOS LAS FECHAS
    Private Function ValidarFecha() As Boolean
        '
        If Not IsDate(txtFechaA.Text) OrElse _
            Not IsDate(txtFechaDe.Text) Then
            Return False
        Else
            Return True
        End If
        '
    End Function

    'PAGINADO DE LA GRILLA
    Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        grdResultado.PageIndex = e.NewPageIndex
        ListarCaja()
        '
    End Sub

    'LISTAR CAJAS
    Private Sub ListarCaja()
        '
        Try
            '
            Dim objCAdministarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgenteCaja As New BEAgenteCaja
            Dim listAgenteCaja As New List(Of BEAgenteCaja)

            obeAgenteCaja.IdAgenciaRecaudadora = Convert.ToInt32(lblIdAgenciaRecaudadora.Text)
            If UserInfo.Rol = SPE.EmsambladoComun.ParametrosSistema.RolSupervisor Then
                obeAgenteCaja.IdUsuarioCreacion = 0
            Else
                obeAgenteCaja.IdUsuarioCreacion = UserInfo.IdUsuario
            End If
            obeAgenteCaja.NombreAgente = ""
            obeAgenteCaja.NombreEstado = ddlTipoFecha.SelectedItem.Text 'PARA EL TIPO DE FECHA
            obeAgenteCaja.FechaCreacion = Convert.ToDateTime(txtFechaDe.Text)
            obeAgenteCaja.FechaActualizacion = Convert.ToDateTime(txtFechaA.Text)
            If ddlEstado.SelectedIndex = 0 Then
                obeAgenteCaja.IdEstado = 0
            Else
                obeAgenteCaja.IdEstado = ddlEstado.SelectedValue
            End If
            obeAgenteCaja.OrderBy = SortExpression
            obeAgenteCaja.IsAccending = SortDir
            listAgenteCaja = objCAdministarAgenciaRecaudadora.ConsultarCajaAvanzada(obeAgenteCaja)
            grdResultado.DataSource = listAgenteCaja
            grdResultado.DataBind()
            If Not listAgenteCaja Is Nothing Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listAgenteCaja.Count)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If

            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        '
    End Sub

    'DATABOUND DE LA GRILLA
    Protected Sub grdResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "NombreEstado", _
        "Anulado", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

    'BOTON LIMPIAR
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        '
        ddlEstado.SelectedIndex = 0
        ddlTipoFecha.SelectedIndex = 0
        InicializarFecha()
        grdResultado.DataSource = Nothing
        grdResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        '
    End Sub

    Protected Sub grdResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        '
        SortExpression = e.SortExpression
        If (SortDir.Equals(SortDirection.Ascending)) Then
            SortDir = SortDirection.Descending
        Else
            SortDir = SortDirection.Ascending
        End If
        ListarCaja()
        '
    End Sub

    'Comando de Detalle Caja
    Protected Sub ibtnDetalleCaja_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Session("IdAgenteCaja") = e.CommandArgument.ToString()
        '
    End Sub
End Class



