<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COOrPa.aspx.vb" Inherits="ARE_COOrPa" Title="PagoEfectivo - Consultar C�digo de Identificaci�n de Pago" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <div class="inner-col-right">
        <h2>
            Consultar C&oacute;digo de Identificaci&oacute;n de Pago</h2>
        <div id="div50" class="dlgrid">
            <fieldset>
                <h3>
                    Criterios de b�squeda</h3>
                <div id="divCriteriosBusqueda" class="cont-panel-search">
                    <asp:UpdatePanel ID="UpdatePanelOP" runat="server" UpdateMode="Conditional" class="even w100 clearfix dlinline">
                        <ContentTemplate>
                            <dl class="clearfix dt10 dd60">
                                <dt class="desc">
                                    <label>
                                        C.I.P:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="miniCamp izq" AutoPostBack="True"
                                        OnTextChanged="txtNroOrdenPago_TextChanged"></asp:TextBox>
                                    <label class="izq">
                                        Estado:</label>
                                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="clsform1">
                                    </asp:DropDownList>
                                </dd>
                            </dl>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" runat="server" FilterType="Numbers"
                                TargetControlID="txtNroOrdenPago">
                            </cc1:FilteredTextBoxExtender>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <label>
                                            Cliente:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtCliente" runat="server" CssClass="normalCamp"></asp:TextBox>
                                    </dd>
                                </dl>
                            </div>
                            <div class="even w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <label>
                                            Empresa:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtEmpresa" runat="server" CssClass="normalCamp"></asp:TextBox>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd30">
                                    <dt class="desc">
                                        <label>
                                            Servicio:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtServicio" runat="server" CssClass="normalCamp"></asp:TextBox>
                                    </dd>
                                </dl>
                            </div>
                            <div class="even w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd70">
                                    <dt class="desc">
                                        <label>
                                            Del:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtFechaDe" runat="server" CssClass="miniCamp izq" MaxLength="10"
                                            Width="70px"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/icon-calendar.gif" %>'
                                            class="icon-calendar"></asp:ImageButton>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                            ControlToValidate="txtFechaDe" ErrorMessage="*" InvalidValueMessage="Fecha 'Del' no v�lida"
                                            IsValidEmpty="False" EmptyValueMessage="Fecha 'Del' es requerida" Display="Dynamic"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                                        <label class="izq">
                                            Al:</label>
                                        <asp:TextBox ID="txtFechaA" runat="server" CssClass="miniCamp izq" MaxLength="10"
                                            Width="70px"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/icon-calendar.gif" %>'
                                            class="icon-calendar"></asp:ImageButton>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                            ControlToValidate="txtFechaA" ErrorMessage="*" InvalidValueMessage="Fecha 'Al' no v�lida"
                                            IsValidEmpty="False" EmptyValueMessage="Fecha 'Al' es requerida" Display="Dynamic"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                                        <asp:CompareValidator ID="covFecha" runat="server" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                            ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                            Type="Date">*</asp:CompareValidator>
                                        <asp:ValidationSummary ID="ValidationSummary" runat="server" />
                                    </dd>
                                </d1>
                            </div>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" TargetControlID="txtFechaDe"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" TargetControlID="txtFechaA"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaDe"
                                Format="dd/MM/yyyy" PopupButtonID="ibtnFechaDe">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaA"
                                Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA">
                            </cc1:CalendarExtender>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtCliente" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                                TargetControlID="txtCliente">
                            </cc1:FilteredTextBoxExtender>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtServicio" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&.-_ "
                                TargetControlID="txtServicio">
                            </cc1:FilteredTextBoxExtender>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtEmpresa" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&.-_ "
                                TargetControlID="txtEmpresa">
                            </cc1:FilteredTextBoxExtender>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="izq btns4">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar" Text="Buscar" />
                    <br />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="btnLimpiar" Text="Limpiar" />
                </div>
            </fieldset>
        </div>
        <div id="divOrdenesPago" class="result" style="width: 600px">
            
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h3>
                        <asp:Literal ID="lblResultado" runat="server" Text="" />
                    </h3>
                    <asp:GridView ID="grdResultado" runat="server" BackColor="White" AllowPaging="True"
                        AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" OnRowDataBound="grdResultado_RowDataBound" CssClass="grilla"
                        EnableTheming="True" ShowFooter="False" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="NumeroOrdenPago" HeaderText="N&#250;mero C.I.P." SortExpression="NumeroOrdenPago">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionCliente" HeaderText="Cliente" SortExpression="DescripcionCliente">
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEmpresa" HeaderText="Empresa" Visible="False">
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionServicio" HeaderText="Servicio" Visible="False">
                            </asp:BoundField>
                            <asp:BoundField DataField="SimboloMoneda" HeaderText="" SortExpression="SimboloMoneda">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Total" SortExpression="Total">
                                <ItemTemplate>
                                    <%#SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField Visible="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnSeleccionarReOp" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/ok2.jpg" %>'
                                        PostBackUrl="~/ARE/PRReOrPa.aspx" CommandArgument='<%# Eval("NumeroOrdenPago") %>'
                                        OnCommand="ibtnSeleccionarReOp_Command" CommandName="Seleccionar" ToolTip="Seleccionar">
                                    </asp:ImageButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField Visible="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtnSeleccionarAnOp" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/ok2.jpg" %>'
                                        PostBackUrl="~/ARE/PRAnOrPa.aspx" CommandArgument='<%# Eval("NumeroOrdenPago") %>'
                                        OnCommand="ibtnSeleccionarAnOp_Command" CommandName="Seleccionar" ToolTip="Seleccionar">
                                    </asp:ImageButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="grdResultado" EventName="PageIndexChanging">
                    </asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
