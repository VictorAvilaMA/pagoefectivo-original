Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.EmsambladoComun
Imports _3Dev.FW.Web

Partial Class ARE_COOrPa
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    'LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNroOrdenPago)
            CargarDatos()
            '
            If Not Page.PreviousPage Is Nothing Then
                If PreviousPage.ToString = "ASP.are_prreorpa_aspx" Then
                    grdResultado.Columns(7).Visible = True
                ElseIf PreviousPage.ToString = "ASP.are_pranorpa_aspx" Then
                    grdResultado.Columns(8).Visible = True
                End If
            End If
            '
            Dim MenuPE As Menu = Master.FindControl("MenuPE")

            If Not MenuPE Is Nothing Then
                MenuPE.StaticSelectedStyle.Font.Bold = True
                MenuPE.DynamicSelectedStyle.Font.Bold = True
            End If
            '
        End If
        '
    End Sub

    'CARGA DE DATOS
    Private Sub CargarDatos()

        Dim objParametro As New SPE.Web.CAdministrarParametro

        InicializarFecha()

        'ESTADO Código de Identificación de Pago
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoOrdenPago) : ddlEstado.DataBind() : ddlEstado.Items.Insert(0, "::: TODOS :::")
        txtNroOrdenPago.Focus()

    End Sub
    Private Sub InicializarFecha()
        'FECHAS
        txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaA.Text = Today.ToShortDateString
    End Sub

    'BUSCAR
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        '
        If ValidarFecha() = True Then
            '
            ListarOrdenesDePago()
            '
        End If
        '
    End Sub

    'VALIDAMOS LAS FECHAS
    Private Function ValidarFecha() As Boolean
        '
        If Not IsDate(txtFechaA.Text) OrElse _
            Not IsDate(txtFechaDe.Text) Then
            Return False
        Else
            Return True
        End If
        '
    End Function


    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase


        Dim obeOrdenPago As New BEOrdenPago

        obeOrdenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text
        obeOrdenPago.DescripcionCliente = txtCliente.Text
        obeOrdenPago.DescripcionEmpresa = txtEmpresa.Text
        obeOrdenPago.DescripcionServicio = txtServicio.Text
        '
        If ddlEstado.SelectedIndex = 0 Then
            obeOrdenPago.IdEstado = 0
        Else
            obeOrdenPago.IdEstado = Convert.ToInt32(ddlEstado.SelectedValue)
        End If
        '
        obeOrdenPago.FechaCreacion = Convert.ToDateTime(txtFechaDe.Text)
        obeOrdenPago.FechaActualizacion = Convert.ToDateTime(txtFechaA.Text)
        obeOrdenPago.IdUsuarioCreacion = _3Dev.FW.Web.Security.UserInfo.IdUsuario
        obeOrdenPago.OrderBy = SortExpression
        obeOrdenPago.IsAccending = SortDir

        Return obeOrdenPago
    End Function

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return grdResultado
        End Get
    End Property


    'LISTAR ORDENES DE PAGO
    Dim listOrdenPago As New List(Of BEOrdenPago)

    Private Sub ListarOrdenesDePago()
        '
        Try

            Dim objCadministrarOrdenPago As New SPE.Web.COrdenPago



            'region ini
            Dim businessEntityObject As BEOrdenPago
            businessEntityObject = CreateBusinessEntityForSearch()

            'If (FlagPager) Then

            businessEntityObject.PageNumber = PageNumber
            businessEntityObject.PageSize = GridViewResult.PageSize
            businessEntityObject.TipoOrder = IIf(SortDir = SortDirection.Ascending, True, False)
            businessEntityObject.PropOrder = SortExpression
            GridViewResult.PageIndex = PageNumber - 1

            listOrdenPago = objCadministrarOrdenPago.ConsultarOrdenPago(businessEntityObject)

            Dim oObjectDataSource As New ObjectDataSource()

            oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID
            oObjectDataSource.EnablePaging = GridViewResult.AllowPaging
            oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
            oObjectDataSource.SelectMethod = "GetDataGrilla"
            oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
            oObjectDataSource.StartRowIndexParameterName = "filaInicio"
            oObjectDataSource.MaximumRowsParameterName = "maxFilas"
            oObjectDataSource.EnableViewState = False

            Dim npag As Integer
            AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating

            GridViewResult.DataSource = oObjectDataSource

            If listOrdenPago.Count = 0 Then
                npag = 0
            Else
                npag = listOrdenPago.First().TotalPageNumbers
                GridViewResult.DataBind()
            End If


            If (GridViewResult.Rows.Count = 0) Then
                ThrowWarningMessage(MessageNoRecordsWereFound)
            Else
                ThrowWarningMessage("")
            End If


            ''''''''''''''''''''''''

            'grdResultado.DataSource = listOrdenPago
            'grdResultado.DataBind()
            If Not listOrdenPago Is Nothing Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listOrdenPago.First().TotalPageNumbers)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If

            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        '
    End Sub

    Private Sub oObjectDataSource_ObjectCreating(sender As Object, e As ObjectDataSourceEventArgs)
        e.ObjectInstance = New TotalRegistrosTableAdapter(listOrdenPago, listOrdenPago.First().TotalPageNumbers)
    End Sub

    'LIMPIAR
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        '
        txtNroOrdenPago.Text = ""
        txtCliente.Text = ""
        txtEmpresa.Text = ""
        txtServicio.Text = ""
        grdResultado.DataSource = Nothing
        grdResultado.DataBind()
        ddlEstado.SelectedIndex = 0
        InicializarFecha()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        '
    End Sub

    'PAGINADO DE LA GRILLA
    'Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '
    'grdResultado.PageIndex = e.NewPageIndex
    'ListarOrdenesDePago()
    '
    'End Sub



    'COMANDO DE LA GRILLA
    Protected Sub ibtnSeleccionarAnOp_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Context.Items.Add("NumeroOrdenPago", e.CommandArgument.ToString())
        '
    End Sub

    Protected Sub ibtnSeleccionarReOp_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Context.Items.Add("NumeroOrdenPago", e.CommandArgument.ToString())
        '
    End Sub

    Protected Sub txtNroOrdenPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        If Right("00000000000000" + txtNroOrdenPago.Text, 14) = "00000000000000" Then
            txtNroOrdenPago.Text = ""
        Else
            txtNroOrdenPago.Text = Right("00000000000000" + txtNroOrdenPago.Text, 14)
        End If
        '
    End Sub

    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property

    Protected Sub grdResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "DescripcionEstado", _
        "Anulada", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

    'Protected Sub grdResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    '    '
    '    SortExpression = e.SortExpression
    '    If (SortDir.Equals(SortDirection.Ascending)) Then
    '        SortDir = SortDirection.Descending
    '    Else
    '        SortDir = SortDirection.Ascending
    '    End If
    '    ListarOrdenesDePago()
    '    '
    'End Sub
End Class
