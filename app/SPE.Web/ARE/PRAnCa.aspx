<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRAnCa.aspx.vb" Inherits="ARE_PRAnCa" Title="PagoEfectivo - Anular Caja" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page" class="colRight">
        <h1>
            <asp:Literal ID="lblMensajeTransaccion" runat="server"></asp:Literal>
        </h1>
		<div id="divInfGeneral" class="dlgrid">				
				<div class="inner-col-right">
					<h4>1. Informaci&oacute;n de caja activa</h4>
					<div class="w100 clearfix dlinline">
						<dl class="even clearfix dt15 dd30">
							<dt class="desc">
								<label>
									<asp:Literal ID="lbl1" runat="server" Text="Agente Recaudador:"></asp:Literal></label>
							</dt>
							<dd class="camp">
								<label>
									<asp:Literal ID="lblAgenteRecaudador" runat="server"></asp:Literal></label>
								<label>
									<asp:Literal ID="lblIdAgente" runat="server"></asp:Literal></label>						
							</dd>
							<asp:Label ID="lblIdAgenteCaja" runat="server" Text="" Visible="False"></asp:Label>
						</dl>
					</div>
					<div class="w100 clearfix dlinline">
						<dl class=" clearfix dt15 dd30">
							<dt class="desc">
								<label>
									<asp:Literal ID="LabelCaja" runat="server" Text="Caja:"></asp:Literal></label>
							</dt>
							<dd class="camp">
								<label>
									<asp:Literal ID="lblCaja" runat="server"></asp:Literal></label>	
								<label>
									<asp:Literal ID="lblIdCaja" runat="server"></asp:Literal></label>
							</dd>
						</dl>
					</div>
					<div class="w100 clearfix dlinline">
						<dl class="even clearfix dt15 dd30">
							<dt class="desc">
								<label>
									<asp:Literal ID="Label5" runat="server" Text="Fecha Apertura:"></asp:Literal></label>
							</dt>
							<dd class="camp">
								<label>
									<asp:Literal ID="lblFechaApertura" runat="server"></asp:Literal></label>
							</dd>
						</dl>
					</div>
					<div class="w100 clearfix dlinline">
						<dl class=" clearfix dt15 dd30">
							<dt class="desc">
								<label>
									<asp:Literal ID="lblSaldoInicial" runat="server" Text="Saldo Inicial:"></asp:Literal></label>
							</dt>
							<dd class="camp">
								<label>
									<asp:Literal ID="lblDescSaldoInicial" runat="server"></asp:Literal></label>
							</dd>
						</dl>
					</div>	
				</div>
				<div class="clear">
				</div>
				<div class="sep_a14">
				</div>
				<div class="clear">
				</div>
				<div class="inner-col-right">
					<div id="divSupervisor" class="divContenedor">
						<h4>
                                2. Informaci&oacute;n del supervisor</h4>
						<div class="w100 clearfix dlinline">
							<dl class="even clearfix dt15 dd60">
								<dt class="desc">
									<label>
										<asp:Literal ID="Label1" runat="server" Text="Supervisor:"></asp:Literal></label>
								</dt>
								<dd class="camp">
									<asp:DropDownList ID="ddlSupervisor" runat="server" CssClass="clsnormal">
									</asp:DropDownList>
								</dd>
							</dl>
						</div>
						<div class="w100 clearfix dlinline">
							<dl class=" clearfix dt15 dd30">
								<dt class="desc">
									<label>
										<asp:Literal ID="Label2" runat="server" Text="Contraseña:">
										</asp:Literal></label>
								</dt>
								<dd class="camp">
									<asp:TextBox ID="txtContrasena" runat="server" CssClass="miniCamp" MaxLength="100"
										TextMode="Password" autocomplete="off"></asp:TextBox>
									<asp:RequiredFieldValidator ID="rfvContrasena" runat="server" ControlToValidate="txtContrasena"
										ValidationGroup="IsNullOrEmpty" ErrorMessage="Debe su contraseña">*</asp:RequiredFieldValidator>
								</dd>
							</dl>
						</div>
					</div>
					<div id="divMensaje" runat="server" class="divContenedor" visible="false">
						<asp:Label ID="lblMensaje" runat="server" CssClass="MensajeValidacion"></asp:Label>&nbsp;
						<br />
					</div>
					<div class="clear">
					</div>					
				</div>
			<div class="sep_a14">
			</div>
			<label class="MensajeTransaccion">
				<asp:Literal ID="lblNota" runat="server" Text="Nota: Solo se puede anular caja cuando no haya Códigos de Identificación de Pago recibidas en la caja activa."></asp:Literal>
			</label>					
			<div id="fsBotonera" class="clearfix dlinline btns3" >
				<asp:Button ID="btnAnular" runat="server" Text="Anular" CssClass="btnAnular" Enabled="False"
					ValidationGroup="IsNullOrEmpty" />&nbsp;
				<asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btnCancelar" PostBackUrl="~/ARE/PgPrl.aspx" />
			</div>
		</div>
    </div>
</asp:Content>