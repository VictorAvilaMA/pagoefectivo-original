Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Web.Security
Partial Class ARE_PRAnCa
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of CAdministrarAgenciaRecaudadora)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ObtenerUsuario()
            CargarSupervisor()
        End If
    End Sub

    Sub CargarSupervisor()
        Dim obeAgente As BEAgente
        Dim objCAdministrarAgenciaRecaudadora As New CAdministrarAgenciaRecaudadora
        obeAgente = objCAdministrarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        obeAgente.IdTipoAgente = SPE.EmsambladoComun.ParametrosSistema.TipoAgente.Supervisor
        obeAgente.Nombres = "" : obeAgente.Apellidos = "" : obeAgente.Telefono = ""
        obeAgente.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo
        '
        ddlSupervisor.DataTextField = "NombresApellidos" : ddlSupervisor.DataValueField = "Email" : ddlSupervisor.DataSource = objCAdministrarAgenciaRecaudadora.ConsultarAgente(obeAgente) : ddlSupervisor.DataBind()
    End Sub

    Function ObtenerUsuario() As Integer
        Dim obeAgente As New SPE.Entidades.BEAgente
        Dim objCAgenciaRecaudadora As New CAdministrarAgenciaRecaudadora
   
        Me.btnAnular.Enabled = False

        obeAgente = objCAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        Dim obeAgenteCaja As New SPE.Entidades.BEAgenteCaja

        obeAgenteCaja = objCAgenciaRecaudadora.ConsultaCajaActiva(UserInfo.IdUsuario)
        If obeAgenteCaja.IdAgenteCaja = 0 Then
            lblMensajeTransaccion.Text = "Usted no tiene ninguna caja activa."
        Else

            lblAgenteRecaudador.Text = obeAgenteCaja.NombreAgente

            lblAgenteRecaudador.Text = obeAgenteCaja.NombreAgente
            lblCaja.Text = obeAgenteCaja.NombreCaja
            lblFechaApertura.Text = obeAgenteCaja.FechaApertura.ToString("dd/MM/yyyy")
            lblIdAgenteCaja.Text = obeAgenteCaja.IdAgenteCaja.ToString


            If obeAgenteCaja.NroOPRecibidas > 0 Then
                ' Se ha desactivado este bloque de c�digo debido a que solo se necesita verificar OP para no permitir
                'anular caja, as� se haya otorgado fecha valuta o no

                'If VerificarFechaValuta(obeAgenteCaja) Then
                '    HabilitarAnular(True)
                '    lblMensajeTransaccion.Text = "Proceso de anulaci�n v�lido con fecha valuta"
                'Else

                lblMensajeTransaccion.Text = "Proceso de anulaci�n fue negado, debido a que su caja cuenta con transacciones realizadas."
                HabilitarAnular(False)

                'End If
            Else

                lblMensajeTransaccion.Text = "Anular Caja"
                btnAnular.Enabled = True
                '
                'OBTENER SALDO INICIAL
                CargarSaldoInicial(obeAgenteCaja.IdAgenteCaja)
                '
            End If
        End If
        Return 0
    End Function

    Private Sub CargarSaldoInicial(ByVal idAgenteCaja As Integer)
        '
        Dim listMovimiento As New List(Of BEMovimientoConsulta)
        listMovimiento = GetListMovimientos(idAgenteCaja)
        '
        If listMovimiento.Count > 0 Then
            '
            Dim SaldoInicial As String = ""
            '
            For i As Integer = 0 To listMovimiento.Count - 1
                '
                If listMovimiento(i).DescTipoMovimiento.ToUpper = "SALDO INICIAL" Then
                    If i < listMovimiento.Count - 1 Then
                        SaldoInicial = SaldoInicial & listMovimiento(i).Monto.ToString("#,#0.00") & " " & listMovimiento(i).DescMoneda & ", "
                    Else
                        SaldoInicial = SaldoInicial & listMovimiento(i).Monto.ToString("#,#0.00") & " " & listMovimiento(i).DescMoneda
                    End If
                End If
                '
            Next
            '
            If SaldoInicial = "" Then
                SaldoInicial = "Ud. apertur� la caja sin saldo inicial."
            Else
                SaldoInicial = "Ud. apertur� la caja con: " & SaldoInicial & "."
            End If

            '
            lblDescSaldoInicial.Text = SaldoInicial
            '
        Else
            '
            lblDescSaldoInicial.Text = "Ud. apertur� la caja sin saldo inicial."
            '
        End If
        '
    End Sub

    'OBTENEMOS LA LISTA DE MOVIMIENTOS
    Private Function GetListMovimientos(ByVal idAgenteCaja As Integer) As List(Of BEMovimientoConsulta)
        '
        Dim listMovimiento As New List(Of SPE.Entidades.BEMovimientoConsulta)
        Dim objCAgencia As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgenteCaja As New SPE.Entidades.BEAgenteCaja

        obeAgenteCaja.IdAgenteCaja = idAgenteCaja

        listMovimiento = objCAgencia.ConsultarDetalleMovimientosCaja(obeAgenteCaja)

        Return listMovimiento
        '
    End Function

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        AnularCaja()

    End Sub

    Function VerificarFechaValuta(ByVal obeAgenteCaja As BEAgenteCaja) As Boolean


        Dim objCAgenciaRecaudadoras As New CAdministrarAgenciaRecaudadora
        Dim obeFechaValuta As New BEFechaValuta
        Dim flag As Boolean
        Dim f1 As String
        Dim f2 As String

        obeFechaValuta.IdAgenteCaja = Convert.ToInt32(obeAgenteCaja.IdAgenteCaja)

        obeFechaValuta = objCAgenciaRecaudadoras.ConsultarFechaValutaAgenteCaja(obeFechaValuta)
        f1 = obeAgenteCaja.FechaApertura.ToString("dd/MM/yyyy")
        f2 = obeFechaValuta.Fecha.ToString("dd/MM/yyyy")

        If f1 = f2 Then
            divMensaje.Visible = True
            Me.lblMensaje.Text = "El proceso de anulaci�n se realizar� con fecha valuta otorgada."
            Me.lblMensaje.CssClass = "MensajeTransaccion"
            flag = True
            'Me.btnAnular.Enabled = True
            HabilitarAnular(True)
        Else
            divMensaje.Visible = True
            Me.lblMensaje.Text = "No se puede cerrar su caja."
            Me.lblMensaje.CssClass = "MensajeValidacion"
            flag = False
            HabilitarAnular(False)
            'Me.btnAnular.Enabled = False
        End If


        Return flag
    End Function

    Sub AnularCaja()

        '
        'VALIDAR CONTRASE�A ADMINISTRADOR
        '--------------------------------
        Dim obj As New SPE.Web.Seguridad.SPEMemberShip
        If obj.ValidateUser(ddlSupervisor.SelectedValue, txtContrasena.Text) = False Then
            txtContrasena.Text = ""
            rfvContrasena.Validate()
            divMensaje.Visible = True
            Me.lblMensaje.Text = "La contrase�a del supervisor no es v�lida."
            Me.lblMensaje.CssClass = "MensajeValidacion"
            Exit Sub
        End If

        'ANULAR CAJA
        '--------------------------------
        Dim objCAgenciaRecaudadora As New CAdministrarAgenciaRecaudadora
        Dim obeAgenteCaja As New BEAgenteCaja
        Dim resultado As Integer = 0
        obeAgenteCaja.IdAgenteCaja = Convert.ToInt32(Me.lblIdAgenteCaja.Text)
        obeAgenteCaja.IdUsuarioActualizacion = UserInfo.IdUsuario
        obeAgenteCaja.EmailSupervisorTermino = Me.ddlSupervisor.SelectedValue
        resultado = objCAgenciaRecaudadora.AnularAgenteCaja(obeAgenteCaja)

        HabilitarAnular(False)
        divMensaje.Visible = True
        btnCancelar.Text = "Regresar"
        Me.lblMensaje.Text = "Anulaci�n exitosa."
        Me.lblMensaje.CssClass = "MensajeTransaccion"
        Me.LimpiarCajas()


    End Sub

    Sub HabilitarAnular(ByVal valor As Boolean)
        btnAnular.Enabled = valor
        Me.ddlSupervisor.Enabled = valor
        Me.txtContrasena.Enabled = valor

    End Sub

    Sub LimpiarCajas()
        
    End Sub
End Class
