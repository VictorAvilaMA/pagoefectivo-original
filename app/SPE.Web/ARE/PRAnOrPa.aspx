<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/ARE/PRAnOrPa.aspx.vb" Inherits="ARE_PRAnOrPa"
    Title="PagoEfectivo - Anular C�digo de Identificaci�n de Pago" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ PreviousPageType VirtualPath="~/ARE/COOrPa.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlPage" runat="server" class="inner-col-right">
        <h1>
            <label>
                <asp:Literal ID="lblMensajeAnularOrdenPago" runat="server"></asp:Literal>
            </label>
        </h1>
        <div class="dlgrid">
            <h3>1. B&uacute;squeda por n&uacute;mero</h3>
            <div class="even w100 clearfix dlinline">
                <dl class="clearfix dt15 dd70">
                    <dt class="desc">
                        <label>
                            C.I.P.:</label>
                    </dt>
                    <dd class="camp">
                        &nbsp;<asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="TextoLargo"></asp:TextBox>
                        <asp:ImageButton ID="ibtnBuscar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/lupa.gif" %>' ToolTip="Buscar Orden Pago" />
                        <asp:ImageButton ID="ibtnBusquedaAvanzada" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/documento.jpg" %>'
                            ToolTip="B�squeda Avanzada" />
                        <asp:RequiredFieldValidator ID="rfvMensajeValidacion" runat="server" ErrorMessage=""
                            ControlToValidate="txtValidar">*</asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtValidar" runat="server" Text="-" Width="0px" CssClass="Hidden"></asp:TextBox>
                        <asp:Label ID="lblIdOrdenPago" runat="server" Text="0" Visible="False"></asp:Label>
                        <asp:Label ID="lblIdMoneda" runat="server" Text="0" Visible="False"></asp:Label>
                        <asp:Label ID="lblIdMedioPago" runat="server" Text="0" Visible="False"></asp:Label>
                        <asp:Label ID="lblIdAgenteCaja" runat="server" Text="0" Visible="False"></asp:Label>
                        <asp:Label ID="lblFechaValuta" runat="server" Text="" Visible="False"></asp:Label>
                    </dd>
                </dl>
            </div>
        </div>
        <cc1:FilteredTextBoxExtender ID="FTBE_TxtNroOrdenPago" runat="server" ValidChars="0123456789"
            TargetControlID="txtNroOrdenPago">
        </cc1:FilteredTextBoxExtender>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divDatos2" class="dlgrid" runat="server" visible="false">
                    <h3>
                        2. Detalle de C&oacute;digo de Identificaci&oacute;n de Pago
                    </h3>
                    <div id="divHistorialOrdenesPagoSub" class="dlgrid">
                        <div id="divCriteriosBusqueda">
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Cliente:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblDescCliente" runat="server"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <fieldset id="fisVerEmpresa" class="ContenedorEtiquetaTexto" runat="server">
                                <div class="w100 clearfix dlinline">
                                    <dl class="even clearfix dt15 dd30">
                                        <dt class="desc">
                                            <label>
                                                Empresa:</label>
                                        </dt>
                                        <dd class="camp">
                                            <asp:Image ID="imgEmpresa" runat="server" Visible="False"></asp:Image>
                                            <label>
                                                <asp:Literal ID="lblDescEmpresa" runat="server"></asp:Literal>
                                            </label>
                                        </dd>
                                    </dl>
                                </div>
                            </fieldset>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Servicio:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:Image ID="imgServicio" runat="server" Visible="False"></asp:Image>
                                        <label>
                                            <asp:Literal ID="lblDescServicio" runat="server" Visible="False"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Nro Orden Pago:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblNroOrdenPago" runat="server"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Concepto de Pago:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblConcepto" runat="server"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Total:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label width="30">
                                            <asp:Literal ID="lblSimboloTotal" runat="server"></asp:Literal>
                                        </label>
                                        <label>
                                            <asp:Literal ID="lblTotal" runat="server"></asp:Literal>
                                        </label>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="div5" class="dlgrid">
                    <h3>
                        2. Autorizaci&oacute;n del supervisor</h3>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    Supervisor:</label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList CssClass="clsNormal" ID="ddlSupervisor" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvSupervisor" runat="server" ControlToValidate="ddlSupervisor"
                                    ValidationGroup="IsNullOrEmpty">Supervisor requerido</asp:RequiredFieldValidator>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class="clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    Contrase�a:</label>
                            </dt>
                            <dd class="camp">
                                <asp:TextBox ID="txtContrasena" runat="server" CssClass="miniCamp izq" MaxLength="100"
                                    TextMode="Password" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvContrasena" runat="server" ControlToValidate="txtContrasena"
                                    ValidationGroup="IsNullOrEmpty" ToolTip="Dato necesario">Ingrese una contrase�a v&aacute;lida</asp:RequiredFieldValidator>
                            </dd>
                        </dl>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ibtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnAnularOrdenPago" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanelMensaje" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlMensaje" runat="server" Width="650px" Height="150px">
                <fieldset id="Fieldset2" class="ContenedorBotonera" style="width: 600px">
                    <asp:Label ID="lblTransaccion" runat="server" Visible="False" CssClass="MensajeTransaccion"
                        Width="600px"></asp:Label>
                </fieldset>
                <fieldset id="Fieldset3" class="w100 clearfix dlinline btns2">
                    <asp:Button ID="btnAnularOrdenPago" runat="server" Text="Anular C.I.P." CssClass="btnAnCip"
                        OnClientClick="return ConfirmMe();"></asp:Button>
                    <asp:Button ID="btnCancelar" OnClick="btnCancelar_Click" runat="server" Text="Cancelar"
                        CssClass="btnCancelar" OnClientClick="return CancelMe();"></asp:Button>
                </fieldset>
                <p>Nota: Solo se pueden anular c&oacute;digos de Identificaci&oacute;n de Pago, que se encuentren en el estado de canceladas y se hayan procesado con la caja activa.</p>

                
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ibtnBuscar" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    </asp:Panel>

</asp:Content>
