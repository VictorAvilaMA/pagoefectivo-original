Imports System.Data
Imports spe.Entidades
Imports System.Collections.Generic
Imports System.IO

Partial Class ARE_PRAnOrPa
    Inherits _3Dev.FW.Web.PageBase

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolSupervisor, SPE.EmsambladoComun.ParametrosSistema.RolCajero})
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '
            Page.Form.DefaultButton = ibtnBuscar.UniqueID
            Page.SetFocus(txtNroOrdenPago)
            '
            If VerificarEstadoCaja() = True Then
                If Not Page.PreviousPage Is Nothing AndAlso _
                    Not Context.Items.Item("NumeroOrdenPago") Is Nothing Then 'CONSULTAR Código de Identificación de Pago
                    txtNroOrdenPago.Text = Context.Items.Item("NumeroOrdenPago").ToString
                    BuscarOrdenPago()
                End If
                '
                CargarSupervisor()
                '
            End If
            '
        End If
        '
    End Sub

    'VERIFICAMOS EL ESTADO DE LA CAJA
    Public Function VerificarEstadoCaja() As Boolean
        '
        Dim value As Boolean

        Dim CAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim ValidacionEstadoCaja(3)() As String
        ValidacionEstadoCaja = CAdministrarAgenciaRecaudadora.ValidarEstadoCaja("AnularOrdenPago", UserInfo.IdUsuario)
        '
        If ValidacionEstadoCaja(0)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.Aperturado Then
            '
            If ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaActual Then
                lblIdAgenteCaja.Text = ValidacionEstadoCaja(2)(2).ToString
                value = True
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaValuta Then
                lblIdAgenteCaja.Text = ValidacionEstadoCaja(2)(2).ToString
                lblFechaValuta.Text = ValidacionEstadoCaja(0)(2)
                value = True
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaActual OrElse _
                ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaValuta Then
                pnlPage.Enabled = False
                btnAnularOrdenPago.Enabled = False
                btnCancelar.Enabled = False
                value = False
            End If
            '
            lblMensajeAnularOrdenPago.Text = ValidacionEstadoCaja(1)(1)
            '
        Else
            lblMensajeAnularOrdenPago.Text = ValidacionEstadoCaja(0)(1)
            pnlPage.Enabled = False
            btnAnularOrdenPago.Enabled = False
            btnCancelar.Enabled = False
            value = False
        End If
        '
        Return value
        '
    End Function

    'CARGAR SUPERVISOR
    Private Sub CargarSupervisor()
        '
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgente As New BEAgente

        obeAgente = objCAdministrarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        obeAgente.IdTipoAgente = SPE.EmsambladoComun.ParametrosSistema.TipoAgente.Supervisor
        obeAgente.Nombres = "" : obeAgente.Apellidos = "" : obeAgente.Telefono = ""
        obeAgente.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo

        ddlSupervisor.DataTextField = "NombresApellidos" : ddlSupervisor.DataValueField = "Email" : ddlSupervisor.DataSource = objCAdministrarAgenciaRecaudadora.ConsultarAgente(obeAgente) : ddlSupervisor.DataBind()
        '
    End Sub

    'BUSCAR Código de Identificación de Pago
    Protected Sub ibtnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnBuscar.Click
        '
        BuscarOrdenPago()
        '
    End Sub

    'BUSCAR Código de Identificación de Pago
    Private Sub BuscarOrdenPago()
        '
        '
        'CARGA DE Código de Identificación de Pago
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        Dim objCAdministrarOrdenPago As New SPE.Web.COrdenPago
        Dim obeMovimiento As New BEMovimiento
        Dim obeOrdenPago As New BEOrdenPago

        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text
        obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada
        obeOrdenPago = objCAdministrarOrdenPago.ConsultarOrdenPagoRecepcionar(obeOrdenPago)
        lblIdOrdenPago.Text = obeOrdenPago.IdOrdenPago.ToString
        '
        'TRAER EL MOVIMIENTO DE LA Código de Identificación de Pago RECEPCIONADA 
        '---------------------------------------------------------------
        obeMovimiento.IdOrdenPago = Convert.ToInt32(lblIdOrdenPago.Text)
        obeMovimiento.IdAgenteCaja = Convert.ToInt32(lblIdAgenteCaja.Text)
        obeMovimiento.IdMoneda = 0
        obeMovimiento.IdTipoMovimiento = SPE.EmsambladoComun.ParametrosSistema.TipoMovimiento.RecepcionarOP
        obeMovimiento.IdMedioPago = 0
        Dim listMovimiento As List(Of BEMovimiento) = objCAdministrarComun.ConsultarMovimiento(obeMovimiento)
        If listMovimiento.Count > 0 Then
            lblIdMoneda.Text = listMovimiento.Item(0).IdMoneda.ToString
            lblIdMedioPago.Text = listMovimiento.Item(0).IdMedioPago.ToString
        End If
        '-------------------------------------------------------------------------------------
        '
        'SETEO DE PARAMETROS
        With obeOrdenPago

            If .IdOrdenPago = 0 OrElse listMovimiento.Count = 0 Then 'NO HAY Código de Identificación de Pago EN LA CAJA ACTUAL
                '
                Limpiar()
                divDatos2.Visible = False
                lblTransaccion.Visible = True
                lblTransaccion.Text = "No se encontró el Código de Identificación de Pago."
                '
            Else 'CARGAR DATOS DE Código de Identificación de Pago
                '
                lblTransaccion.Text = ""
                '
                lblTransaccion.Visible = False
                divDatos2.Visible = True
                '
                lblDescCliente.Text = .DescripcionCliente.ToString
                If .LogoServicio.Length = 0 Then
                    lblDescServicio.Text = .DescripcionServicio
                    lblDescServicio.Visible = True
                Else
                    imgServicio.ImageUrl = "~/ADM/ADServImg.aspx?servicioid=" + .IdServicio.ToString
                    imgServicio.AlternateText = .DescripcionCliente.ToString
                    imgServicio.Visible = True
                End If

                If .LogoEmpresa.Length = 0 Then
                    lblDescEmpresa.Text = .DescripcionEmpresa
                    lblDescEmpresa.Visible = True
                Else
                    imgEmpresa.ImageUrl = "~/ADM/ADEmCoImg.aspx?empresaId=" + .IdEmpresa.ToString
                    imgEmpresa.AlternateText = .NumeroOrdenPago.ToString
                    imgEmpresa.Visible = True
                End If

                '
                If .IdServicio > 0 Then

                    Dim objCEmpresaContratante As New SPE.Web.CEmpresaContratante
                    Dim obeOcultarEmpresa As New BEOcultarEmpresa
                    obeOcultarEmpresa = objCEmpresaContratante.OcultarEmpresa(.IdServicio)
                    If Not obeOcultarEmpresa Is Nothing Then
                        If obeOcultarEmpresa.OcultarEmpresa = True Then
                            fisVerEmpresa.Visible = False
                        End If
                    End If

                End If
                '

                lblNroOrdenPago.Text = .NumeroOrdenPago.ToString
                lblConcepto.Text = .ConceptoPago.ToString
                lblSimboloTotal.Text = .SimboloMoneda
                lblTotal.Text = .Total.ToString("#,#0.00")
                '
            End If
        End With
        '
    End Sub

    'LIMPIAR
    Private Sub Limpiar()
        '
        lblIdOrdenPago.Text = "0"
        lblDescCliente.Text = ""
        lblDescServicio.Text = ""
        lblDescServicio.Visible = False
        imgServicio.Visible = False
        lblDescEmpresa.Text = ""
        lblDescEmpresa.Visible = False
        imgEmpresa.Visible = False
        lblNroOrdenPago.Text = ""
        lblConcepto.Text = ""
        lblTotal.Text = ""
        '
    End Sub

    'ANULAR Código de Identificación de Pago
    Protected Sub btnAnularOrdenPago_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnularOrdenPago.Click
        '
        Try
            '
            lblTransaccion.Visible = True
            '
            'VALIDAMOS SI HAY UNA Código de Identificación de Pago
            If lblIdOrdenPago.Text = 0 Then
                '
                'NO HAY Código de Identificación de Pago
                lblTransaccion.Text = "No se tiene detalle para ''Anular Código de Identificación de Pago''"
                '
            Else
                '
                'VALIDAR CONTRASEÑA ADMINISTRADOR
                '--------------------------------
                Dim obj As New SPE.Web.Seguridad.SPEMemberShip
                If obj.ValidateUserAndPass(ddlSupervisor.SelectedValue, txtContrasena.Text) = False Then
                    txtContrasena.Text = ""
                    rfvContrasena.Validate()
                    Exit Sub
                End If
                '
                'Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
                Dim objCAdministrarOrdenPago As New SPE.Web.COrdenPago
                'Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
                Dim obeMovimiento As New BEMovimiento
                Dim obeOrdenPago As New BEOrdenPago

                obeMovimiento.IdOrdenPago = Convert.ToInt32(lblIdOrdenPago.Text)
                obeMovimiento.IdAgenteCaja = Convert.ToInt32(lblIdAgenteCaja.Text)
                'obeMovimiento.IdTipoMovimiento = SPE.EmsambladoComun.ParametrosSistema.TipoMovimiento.AnularOP
                obeMovimiento.IdMoneda = Convert.ToInt32(lblIdMoneda.Text)
                obeMovimiento.IdMedioPago = Convert.ToInt32(lblIdMedioPago.Text)
                obeMovimiento.Monto = Convert.ToDecimal(lblTotal.Text) * -1
                'If lblFechaValuta.Text = "" Then
                '    'obeMovimiento.FechaMovimiento = objCAdministrarComun.ConsultarFechaHoraActual()
                '    obeMovimiento.FechaMovimiento = Date.MinValue
                'Else
                '    obeMovimiento.FechaMovimiento = Convert.ToDateTime(lblFechaValuta.Text)
                'End If
                obeMovimiento.IdUsuarioCreacion = UserInfo.IdUsuario
                'obeMovimiento.IdMovimiento = objCAdministrarAgenciaRecaudadora.RegistrarMovimiento(obeMovimiento)
                '
                'SOLO PARA NOTIFICAR EL ESTADO DE LA Código de Identificación de Pago
                'obeOrdenPago.IdOrdenPago = obeMovimiento.IdOrdenPago
                'obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Anulada
                obeOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
                objCAdministrarOrdenPago.AnularOrdenPago(obeOrdenPago, obeMovimiento)
                '
                divDatos2.Visible = False
                lblTransaccion.Text = "El Código de Identificación de Pago Nro:''" & lblNroOrdenPago.Text & "'' se anuló correctamente."
                Limpiar()
                '
            End If
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            lblTransaccion.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
        End Try

        '
    End Sub

    'CANCELAR
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        Response.Redirect("~/ARE/PgPrl.aspx")
        '
    End Sub

    'BUSQUEDA AVANZADA DE Código de Identificación de Pago
    Protected Sub ibtnBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnBusquedaAvanzada.Click
        '
        Server.Transfer("~/ARE/COOrPa.aspx", True)
        '
    End Sub

End Class
