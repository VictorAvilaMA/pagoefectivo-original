<%@ Page Language="VB" Async="true" EnableEventValidation="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="true" CodeFile="PRApCa.aspx.vb" Inherits="ARE_PRApCa" Title="PagoEfectivo - Aperturar Caja" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h1>
        <asp:Literal ID="lblmensajeApertura" runat="server" Text="Proceso"></asp:Literal>
    </h1>
    <div class="dlgrid">
        <fieldset>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlPage" runat="server">
                        <div id="divInfGeneral" class="inner-col-right">
                            <h4>
                                1. Informaci&oacute;n para la apertura de una caja</h4>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblAgente" runat="server" Text="Agente Recaudador:"></asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblDescAgente" runat="server"></asp:Literal></label>
                                    </dd>
                                    <asp:Label ID="lblIdAgente" runat="server" Visible="False">0</asp:Label>
                                    <asp:Label ID="lblIdAgenteCaja" runat="server" Visible="False">0</asp:Label>
                                    <asp:Label ID="lblIdAgencia" runat="server" Visible="False">0</asp:Label>
                                    <asp:Label ID="lblIdFechaValuta" runat="server" Visible="False">0</asp:Label>
                                    <asp:Label ID="lblFechaValutaApertura" runat="server" Visible="False"></asp:Label>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class=" clearfix dt15 dd60">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblCaja" runat="server" Text="Caja:"></asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:DropDownList ID="ddlCaja" runat="server" CssClass="clsnormal">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvCaja" runat="server" ControlToValidate="ddlCaja"
                                            ValidationGroup="IsNullOrEmpty">No hay cajas disponibles</asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblFechaApertura" runat="server" Text="Fecha Apertura:">
                                            </asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtFechaApertura" runat="server" CssClass="miniCamp" ReadOnly="True"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaApertura" runat="server" ImageUrl="~/Images_Buton/date.gif"
                                            OnClick="ibtnFechaApertura_Click" Visible="False" ToolTip="Utilizar Fecha Valuta" />
                                        <asp:ImageButton ID="ibtnFechaActual" runat="server" ImageUrl="~/Images_Buton/date4.ico"
                                            Visible="False" ToolTip="Utilizar Fecha Actual" OnClick="ibtnFechaActual_Click" />
                                        <div id="Div5" class="EtiquetaTextoIzquierda">
                                            <label>
                                                <asp:Literal ID="lblMensajeFechaValuta" runat="server" Text="Utilizar Fecha Valuta"
                                                    Visible="False">
                                                </asp:Literal></label>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class=" clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblMoneda" runat="server" Text="Moneda:"></asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="clsnormal">
                                        </asp:DropDownList>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblSaldoInicial" runat="server" Text="Saldo inicial:">
                                            </asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtSaldoInicial" runat="server" CssClass="miniCamp" MaxLength="10"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnAgregarSaldo" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/add.jpg"%>'
                                            ToolTip="Agregar Saldo"></asp:ImageButton>
                                        <div id="Div2" class="EtiquetaTextoIzquierda">
                                            <p class="msjenomargin">
                                                <asp:Literal ID="lblMensSaldo" runat="server" Text="N�mero no v�lido" Visible="False">
                                                </asp:Literal></p>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                            <div id="divOrdenesPago" class="result">
                                <asp:GridView ID="grdSaldoInicial" runat="server" BackColor="White" CssClass="grilla"
                                    Width="270px" AutoGenerateColumns="False" CellPadding="3" BorderColor="#CCCCCC"
                                    BorderStyle="None" BorderWidth="1px" OnRowCommand="grdSaldoInicial_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="IdMoneda">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Moneda" HeaderText="Moneda">
                                            <ControlStyle Width="200px"></ControlStyle>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SaldoInicial" HeaderText="Saldo Inicial" DataFormatString="{0:#,#0.00}">
                                            <ControlStyle Width="30px"></ControlStyle>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <controlstyle width="200px"></controlstyle>
                                                <asp:ImageButton ID="ibtnEliminar" runat="server" ToolTip="Eliminar" ImageUrl="~/Images_Buton/b_eliminar.gif"
                                                    CommandName="Eliminar" CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>">
                                                </asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="sep_a14">
                        </div>
                        <div class="clear">
                        </div>
                        <div id="div5" class="inner-col-right">
                            <h4>
                                2. Autorizaci&oacute;n del supervisor</h4>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd60">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblSupervisor" runat="server" Text="Supervisor:"></asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:DropDownList ID="ddlSupervisor" runat="server" CssClass="clsnormal">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvSupervisor" runat="server" ControlToValidate="ddlSupervisor"
                                            ValidationGroup="IsNullOrEmpty">Supervisor requerido</asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class=" clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="lblContrase�a" runat="server" Text="Contrase�a:">
                                            </asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtContrasena" runat="server" CssClass="miniCamp" MaxLength="100"
                                            TextMode="Password" autocomplete="off"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvContrasena" runat="server" ControlToValidate="txtContrasena"
                                            ValidationGroup="IsNullOrEmpty" ToolTip="Dato necesario">Ingrese una contrase�a v�lida</asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:FilteredTextBoxExtender ID="ftxtSaldoInicial" runat="server" FilterType="Custom,Numbers"
                        TargetControlID="txtSaldoInicial" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAperturar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <div class="sep_a14">
            </div>
            <asp:UpdatePanel runat="server" ID="upnlMensaje" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlBotones" runat="server">
                        <asp:Label ID="lblTransaccion" runat="server" Visible="False" CssClass="MensajeTransaccion"></asp:Label>
                        <div id="fsBotonera" class="clearfix dlinline btns3">
                            <asp:Button ID="btnAperturar" runat="server" Text="Aperturar" CssClass="btnAperturar" ValidationGroup="IsNullOrEmpty"
                                OnClientClick="return ConfirmMe();"></asp:Button>
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btnCancelar"></asp:Button>
                        </div>
                    </asp:Panel>
                    <%--            <asp:Panel id="pnlFechaValuta" runat="server" Visible="False" >
                <fieldset  id="fisBotonera2" class="ContenedorBotonera">
                    <asp:Button id="btnFechaValuta" onclick="btnAceptarFechaValuta_Click" runat="server" Text="Utilizar Fecha Valuta" CssClass="BotonLargo" Width="134px" OnClientClick="return confirm('Desea abrir la caja con la fecha valuta? ');"></asp:Button>
                    <br />
                    <asp:Label id="lblFechaValuta" runat="server" ForeColor="Red" CssClass="MensajeValidacion"></asp:Label>
                </fieldset>
            </asp:Panel> --%>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="fisBotonera2" class="clearfix dlinline btns3">
                <asp:Button ID="btnFechaValuta" OnClick="btnAceptarFechaValuta_Click" runat="server"
                    Visible="False" Text="Utilizar Fecha Valuta" CssClass="btnUtiFecVal"
                    OnClientClick="return confirm('Desea abrir la caja con la fecha valuta? ');">
                </asp:Button>
                <br />
                <asp:Label ID="lblFechaValuta" runat="server" Visible="False" ForeColor="Red" CssClass="MensajeValidacion"></asp:Label>
            </div>
        </fieldset>
    </div>
</asp:Content>