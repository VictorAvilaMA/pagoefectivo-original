Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Data

Partial Class ARE_PRApCa
    Inherits _3Dev.FW.Web.PageBase

    Dim dtSaldoInicial As New DataTable

    ''INIT
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '
            Page.SetFocus(ddlCaja)
            VerificarEstadoCaja()
            '
        End If
        '
    End Sub

    Private Sub VisualizaControles(ByVal valor As Boolean)
        btnFechaValuta.Visible = valor
        lblFechaValuta.Visible = valor
    End Sub

    'VERIFICAMOS EL ESTADO DE CAJA
    Private Sub VerificarEstadoCaja()
        '
        Dim CAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim ValidacionEstadoCaja(3)() As String

        ValidacionEstadoCaja = CAdministrarAgenciaRecaudadora.ValidarEstadoCaja("AperturarCaja", UserInfo.IdUsuario)
        '
        If ValidacionEstadoCaja(0)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.Aperturado Then
            '
            If ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaActual Then
                pnlBotones.Enabled = False
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaValuta Then
                pnlBotones.Enabled = False
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaActual OrElse _
                ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaValuta Then
                lblFechaValuta.Text = ValidacionEstadoCaja(2)(1).ToString
                lblIdAgenteCaja.Text = ValidacionEstadoCaja(2)(2).ToString
                If ValidacionEstadoCaja(2)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.NoExiste Then
                    btnFechaValuta.Enabled = False
                    VisualizaControles(True)
                    pnlBotones.Visible = False
                ElseIf ValidacionEstadoCaja(2)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.Disponible Then
                    pnlBotones.Visible = False
                    VisualizaControles(True)
                ElseIf ValidacionEstadoCaja(2)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.DisponibleVencido Then
                    btnFechaValuta.Visible = False
                    pnlBotones.Visible = False
                    VisualizaControles(True)
                ElseIf ValidacionEstadoCaja(2)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.EnUso Then
                    pnlBotones.Visible = False
                    VisualizaControles(True)
                    btnFechaValuta.Visible = False
                End If
            End If
            '
            lblmensajeApertura.Text = ValidacionEstadoCaja(1)(1)
            pnlPage.Enabled = False
            '
        Else
            lblmensajeApertura.Text = ValidacionEstadoCaja(0)(1)
            CargarDatosAgencia()
        End If
        '
    End Sub

    'CARGAR DATOS DE AGENCIA RECAUDADORA
    Private Sub CargarDatosAgencia()
        '
        txtFechaApertura.Text = Date.Today
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        Dim obeFechaValuta As New BEFechaValuta
        Dim obeAgente As New BEAgente
        Dim obeCaja As New BECaja
        '
        'ddlMoneda.DataTextField = "Descripcion" : ddlMoneda.DataValueField = "IdMoneda" : ddlMoneda.DataSource = objCAdministrarComun.ConsultarMoneda() : ddlMoneda.DataBind()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::: Seleccione :::")
        '
        'OBTENEMOS IDAGENCIARECAUDADORA Y DESCAGENCIARECAUDADORA DE ACUERDO AL AGENTE
        obeAgente = objCAdministrarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        lblDescAgente.Text = obeAgente.Nombres & " " & obeAgente.Apellidos
        lblIdAgente.Text = Convert.ToString(obeAgente.IdAgente)
        lblIdAgencia.Text = obeAgente.IdAgenciaRecaudadora
        '
        obeCaja.IdAgenciaRecaudadora = obeAgente.IdAgenciaRecaudadora
        obeCaja.IdUsoCaja = SPE.EmsambladoComun.ParametrosSistema.EstadoUsoCaja.Inactivo
        obeCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
        'ddlCaja.DataTextField = "Nombre" : ddlCaja.DataValueField = "IdCaja" : ddlCaja.DataSource = objCAdministrarAgenciaRecaudadora.ConsultarCaja(obeCaja) : ddlCaja.DataBind()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCaja, objCAdministrarAgenciaRecaudadora.ConsultarCaja(obeCaja), "Nombre", "IdCaja", "::: Seleccione :::")
        If ddlCaja.Items.Count = 0 Then
            rfvCaja.Validate()
        End If
        '
        obeAgente.IdTipoAgente = SPE.EmsambladoComun.ParametrosSistema.TipoAgente.Supervisor
        obeAgente.Nombres = "" : obeAgente.Apellidos = "" : obeAgente.Telefono = ""
        obeAgente.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo
        '
        ddlSupervisor.DataTextField = "NombresApellidos" : ddlSupervisor.DataValueField = "Email" : ddlSupervisor.DataSource = objCAdministrarAgenciaRecaudadora.ConsultarAgente(obeAgente) : ddlSupervisor.DataBind()
        '
        dtSaldoInicial.Columns.Add("IdMoneda") : dtSaldoInicial.Columns.Add("Moneda") : dtSaldoInicial.Columns.Add("SaldoInicial") : Session("dtSaldoInicial") = dtSaldoInicial
        '
        obeFechaValuta.IdTipoFechaValuta = SPE.EmsambladoComun.ParametrosSistema.TipoFechaValuta.Apertura
        obeFechaValuta.IdAgente = Convert.ToInt32(lblIdAgente.Text)
        obeFechaValuta.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.Disponible
        obeFechaValuta = objCAdministrarAgenciaRecaudadora.ConsultarFechaValutaPorAgente(obeFechaValuta)
        '
        If obeFechaValuta.IdFechaValuta <> 0 Then
            lblIdFechaValuta.Text = obeFechaValuta.IdFechaValuta.ToString()
            lblFechaValutaApertura.Text = obeFechaValuta.Fecha.ToShortDateString.ToString()
            ibtnFechaApertura.Visible = True
            lblMensajeFechaValuta.Visible = True
        End If
        '
    End Sub

    'Buscar una Fecha Valuta
    Protected Sub ibtnFechaApertura_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        '
        txtFechaApertura.Text = lblFechaValutaApertura.Text
        ibtnFechaApertura.Visible = False
        ibtnFechaActual.Visible = True
        lblMensajeFechaValuta.Text = "Utilizar Fecha Actual"
        '
    End Sub

    'APERTURAR CAJA
    Protected Sub btnAperturar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAperturar.Click
        '
        Try
            '
            lblTransaccion.Visible = False
            Dim SaldoInicial As String = ""
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
            Dim obeAgenteCaja As New BEAgenteCaja
            Dim obeCaja As New BECaja
            '
            'VALIDAR CONTRASE�A ADMINISTRADOR
            '--------------------------------
            Dim obj As New SPE.Web.Seguridad.SPEMemberShip
            If obj.ValidateUserAndPass(ddlSupervisor.SelectedValue, txtContrasena.Text) = False Then
                txtContrasena.Text = ""
                rfvContrasena.Validate()
                Exit Sub
            End If

            'VALIDAR CAJA
            '------------
            obeCaja.IdAgenciaRecaudadora = Convert.ToInt32(lblIdAgencia.Text)
            obeCaja.IdUsoCaja = SPE.EmsambladoComun.ParametrosSistema.EstadoUsoCaja.Activo
            obeCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
            Dim listCaja As List(Of BECaja) = objCAdministrarAgenciaRecaudadora.ConsultarCaja(obeCaja)
            For i As Integer = 0 To listCaja.Count - 1
                If ddlCaja.SelectedValue = listCaja(i).IdCaja Then
                    lblTransaccion.Visible = True
                    lblTransaccion.Text = "La ''" & ddlCaja.SelectedItem.Text & "'' est� actualmente siendo utilizada. Por favor, seleccione otra caja"
                    Exit Sub
                End If
            Next

            'ASIGNACION
            '----------
            obeAgenteCaja.IdCaja = ddlCaja.SelectedValue
            obeAgenteCaja.IdAgente = Convert.ToUInt32(lblIdAgente.Text)
            'VALIDAMOS SI APERTURO CON UNA FECHA VALUTA
            If ibtnFechaActual.Visible = True Then
                If lblIdFechaValuta.Text <> "0" Then
                    obeAgenteCaja.FechaApertura = Convert.ToDateTime(txtFechaApertura.Text)
                End If
            Else
                obeAgenteCaja.FechaApertura = objCAdministrarComun.ConsultarFechaHoraActual()
            End If
            obeAgenteCaja.EmailSupervisorApertura = ddlSupervisor.SelectedValue
            obeAgenteCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenteCaja.Aperturado
            obeAgenteCaja.IdUsuarioCreacion = UserInfo.IdUsuario
            'TOMAMOS NUMERO DE MOVIMIENTO PARA DESACTIVAR LA CAJA
            obeAgenteCaja.NroOPRecibidas = SPE.EmsambladoComun.ParametrosSistema.EstadoUsoCaja.Activo
            Dim IdAgenteCaja As Integer = objCAdministrarAgenciaRecaudadora.RegistrarAgenteCaja(obeAgenteCaja)
            '
            'VALIDAMOS SI APERTURO CON UNA FECHA VALUTA
            If ibtnFechaActual.Visible = True Then
                If lblIdFechaValuta.Text <> "0" Then
                    ActualizarEstadoFechaValuta("Apertura", IdAgenteCaja)
                End If
            End If
            '
            'VALIDAMOS SI INGRESO SALDO INICIAL
            If grdSaldoInicial.Rows.Count > 0 Then
                '
                Dim obeMovimiento As New BEMovimiento

                'RECORRIDO
                For i As Integer = 0 To grdSaldoInicial.Rows.Count - 1
                    With grdSaldoInicial.Rows(i)
                        obeMovimiento.IdOrdenPago = Nothing
                        obeMovimiento.IdAgenteCaja = IdAgenteCaja
                        obeMovimiento.IdMoneda = Convert.ToInt32(.Cells(0).Text)
                        obeMovimiento.IdTipoMovimiento = SPE.EmsambladoComun.ParametrosSistema.TipoMovimiento.SaldoInicial
                        obeMovimiento.IdMedioPago = SPE.EmsambladoComun.ParametrosSistema.MedioPago.Efectivo
                        obeMovimiento.Monto = Convert.ToDecimal(.Cells(2).Text)
                        obeMovimiento.FechaMovimiento = obeAgenteCaja.FechaApertura
                        obeMovimiento.IdUsuarioCreacion = UserInfo.IdUsuario
                        objCAdministrarAgenciaRecaudadora.RegistrarMovimiento(obeMovimiento)
                        If i < grdSaldoInicial.Rows.Count - 1 Then
                            SaldoInicial = SaldoInicial & .Cells(2).Text & " " & .Cells(1).Text & ", "
                        Else
                            SaldoInicial = SaldoInicial & .Cells(2).Text & " " & .Cells(1).Text
                        End If
                    End With
                Next
                '
                SaldoInicial = ", teniendo un saldo inicial de: " & SaldoInicial & "."
                '
            Else
                '
                If IsNumeric(txtSaldoInicial.Text) Then
                    If CDbl(txtSaldoInicial.Text) > 0 Then
                        SaldoInicial = ", ingresado un saldo inicial de: " + Convert.ToDecimal(txtSaldoInicial.Text).ToString("#,#0.00") + " pero el cual no fue registrado en Caja."
                    End If
                Else
                    SaldoInicial = ", sin tener saldo inicial."
                End If
                '
            End If
            '
            pnlPage.Enabled = False
            btnAperturar.Enabled = False
            btnCancelar.Text = "Regresar"
            lblTransaccion.Visible = True

            lblTransaccion.Text = "Se apertur� la " & ddlCaja.SelectedItem.Text & " el " & obeAgenteCaja.FechaApertura.ToString & SaldoInicial
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            lblTransaccion.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
        End Try
        
    End Sub

    'BOTON DE ACEPTAR FECHA VALUTA
    Protected Sub btnAceptarFechaValuta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFechaValuta.Click
        '
        ActualizarEstadoFechaValuta("Cierre", 0)
        '
        lblFechaValuta.Text = "Se volvi� abrir la caja pendiente de cierre satisfactoriamente."
        btnFechaValuta.Enabled = False
        '
    End Sub

    'ACTUALIZAR ESTADO DE FECHA VALUTA
    Private Sub ActualizarEstadoFechaValuta(ByVal opcion As String, ByVal idAgenteCaja As Integer)
        '
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        '
        Select Case opcion
            Case "Apertura"
                Dim obeFechaValuta As New BEFechaValuta
                obeFechaValuta.IdTipoFechaValuta = SPE.EmsambladoComun.ParametrosSistema.TipoFechaValuta.Apertura
                obeFechaValuta.IdAgente = Convert.ToUInt32(lblIdAgente.Text)
                obeFechaValuta.IdAgenteCaja = idAgenteCaja
                obeFechaValuta.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.EnUso
                obeFechaValuta.IdUsuarioActualizacion = UserInfo.IdUsuario
                objCAgenciaRecaudadora.ActualizarEstadoFechaValutaPorIdAgente(obeFechaValuta)
            Case "Cierre"
                Dim obeAgenteCaja As New SPE.Entidades.BEAgenteCaja
                obeAgenteCaja.IdUsuarioActualizacion = UserInfo.IdUsuario
                obeAgenteCaja.IdAgenteCaja = Convert.ToInt32(lblIdAgenteCaja.Text)
                obeAgenteCaja.EstadoFechaValuta = SPE.EmsambladoComun.ParametrosSistema.EstadoFechaValuta.EnUso
                objCAgenciaRecaudadora.ActualizarEstadoFechaValuta(obeAgenteCaja)
        End Select

        '
    End Sub

    'CANCELAR
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("~/ARE/PgPrl.aspx")
        '
    End Sub

    'AGREGAR SALDO
    Protected Sub ibtnAgregar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnAgregarSaldo.Click
        '
        Try
            If IsNumeric(txtSaldoInicial.Text) Then

                If txtSaldoInicial.Text <> "" AndAlso CDbl(txtSaldoInicial.Text) > 0 Then
                    txtSaldoInicial.Text = Convert.ToDecimal(txtSaldoInicial.Text).ToString("#,#0.00")
                    dtSaldoInicial = CType(Session("dtSaldoInicial"), DataTable)
                    Dim estado As String = "Ingresar"
                    For i As Integer = 0 To dtSaldoInicial.Rows.Count - 1
                        If dtSaldoInicial.Rows(i).Item("Moneda") = ddlMoneda.SelectedItem.Text Then
                            dtSaldoInicial.Rows(i).Item("SaldoInicial") = txtSaldoInicial.Text
                            estado = "Actualizado"
                        End If
                    Next
                    If estado = "Ingresar" Then
                        dtSaldoInicial.Rows.Add(ddlMoneda.SelectedValue, ddlMoneda.SelectedItem.Text, txtSaldoInicial.Text)
                    End If
                    Session("dtSaldoInicial") = dtSaldoInicial
                    grdSaldoInicial.DataSource = dtSaldoInicial
                    grdSaldoInicial.DataBind()
                    lblMensSaldo.Visible = False
                End If
            Else
                lblMensSaldo.Visible = True
            End If
        Catch ex As Exception
            lblMensSaldo.Visible = True
        End Try
        '
    End Sub

    'ELIMINAR SALDO
    Protected Sub grdSaldoInicial_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        '
        Try
            Dim indice As Integer = Convert.ToInt32(e.CommandArgument)

            Select Case (e.CommandName)
                Case "Eliminar"
                    dtSaldoInicial = CType(Session("dtSaldoInicial"), DataTable)
                    dtSaldoInicial.Rows.RemoveAt(indice)
                    Session("dtSaldoInicial") = dtSaldoInicial
                    grdSaldoInicial.DataSource = dtSaldoInicial
                    grdSaldoInicial.DataBind()
                    '
                    'Para reccuperar la fila
                    'Dim row As GridViewRow = grdSaldoInicial.Rows(indice)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        '
    End Sub

    Protected Sub ibtnFechaActual_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        txtFechaApertura.Text = Date.Now.ToShortDateString.ToString()
        ibtnFechaApertura.Visible = True
        ibtnFechaActual.Visible = False
        lblMensajeFechaValuta.Text = "Utilizar Fecha Valuta"
    End Sub
End Class

