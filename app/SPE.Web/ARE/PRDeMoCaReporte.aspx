<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRDeMoCaReporte.aspx.vb" Inherits="ARE_PRDeMoCaReporte" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PagoEfectivo - Reporte de Movimientos de Caja</title>
    <link href="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/default.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</head>
<body style="text-align: center">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
    <div id="divInfGeneral"  style="left: 0px; width: 720px;">
    <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" style="text-align: center;" >
        <strong>
            <asp:Label ID="lblIdAgenteCaja" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Button ID="btnExportar" runat="server" Text="Exportar Excel" CssClass="Boton" />
            <asp:Button ID="btnExportarPDF" runat="server" Text="Exportar PDF" CssClass="Boton" /><br />
            </strong></div>
    </div>
    <div style="text-align: center">
        <asp:UpdatePanel ID="UpdatePanel" runat="server">
            <ContentTemplate>
                <rsweb:ReportViewer ID="RptMovimientosCaja" runat="server" Height="720px" Width="720px" ShowRefreshButton="False" SizeToReportContent="True" Font-Names="Verdana" Font-Size="8pt" ShowExportControls="False">
                    <LocalReport>
                    </LocalReport>
                </rsweb:ReportViewer>
            </ContentTemplate>
                
        </asp:UpdatePanel>
        
        </div>
    </form>
</body>
</html>
