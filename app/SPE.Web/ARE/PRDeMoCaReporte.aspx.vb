Imports SPE.Entidades
Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Imports _3Dev.FW.Web.Security

Partial Class ARE_PRDeMoCaReporte
    Inherits System.Web.UI.Page

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            '
            If Not Session("IdAgenteCaja") Is Nothing Then
                Try
                    CargarReporte(Convert.ToInt32(Session("IdAgenteCaja")))
                    lblIdAgenteCaja.Text = Session("IdAgenteCaja").ToString()
                    Session.Remove("IdAgenteCaja")
                Catch ex As Exception
                    Session.Remove("IdAgenteCaja")
                End Try
            End If
            '
        End If

    End Sub

    'CARGAR REPORTE POR EVENTO LOAD
    Private Sub CargarReporte(ByVal idAgenteCaja As Integer)

        Dim listMovimiento As New List(Of BEMovimientoConsulta)

        listMovimiento = GetListMovimientos(idAgenteCaja)

        If RptMovimientosCaja.LocalReport.DataSources.Count > 0 Then
            RptMovimientosCaja.LocalReport.DataSources.RemoveAt(0)
        End If

        RptMovimientosCaja.LocalReport.DataSources.Add(New ReportDataSource("BEMovimientoConsulta", listMovimiento))
        RptMovimientosCaja.LocalReport.ReportPath = "Reportes/RptDetalleMovimientoCaja.rdlc"

        RptMovimientosCaja.LocalReport.SetParameters(GetParametros(listMovimiento))
        RptMovimientosCaja.LocalReport.Refresh()

    End Sub

    'OBTENEMOS LA LISTA DE MOVIMIENTOS
    Private Function GetListMovimientos(ByVal idAgenteCaja As Integer) As List(Of BEMovimientoConsulta)
        '
        Dim listMovimiento As New List(Of SPE.Entidades.BEMovimientoConsulta)
        Dim objCAgencia As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgenteCaja As New SPE.Entidades.BEAgenteCaja

        obeAgenteCaja.IdAgenteCaja = idAgenteCaja

        listMovimiento = objCAgencia.ConsultarDetalleMovimientosCaja(obeAgenteCaja)

        Return listMovimiento
        '
    End Function

    'OBTENEMOS LOS PARAMETROS
    Private Function GetParametros(ByVal listMovimiento As List(Of BEMovimientoConsulta)) As List(Of ReportParameter)
        '
        Dim paramList As New List(Of ReportParameter)
        Dim param1 As ReportParameter

        If listMovimiento.Count > 0 Then
            param1 = New ReportParameter("parTitulo", "REPORTE DE DETALLE DE MOVIMIENTOS DE CAJA - " + listMovimiento(0).DescAgenciaRecaudadora.ToString())
        Else
            param1 = New ReportParameter("parTitulo", "")
        End If
        '
        paramList.Add(param1)
        '
        Return paramList
        '
    End Function

    'BOTON EXPORTAR REPORTE
    Protected Sub btnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        '
        Dim listMovimiento As New List(Of BEMovimientoConsulta)

        If lblIdAgenteCaja.Text <> "" Then
            '
            listMovimiento = GetListMovimientos(Convert.ToInt32(lblIdAgenteCaja.Text))

            ProcesoExportar("EXCEL", "xls", listMovimiento)
            '
        End If
        '
    End Sub

    Protected Sub btnExportarPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarPDF.Click
        '
        Dim listMovimiento As New List(Of BEMovimientoConsulta)

        If lblIdAgenteCaja.Text <> "" Then
            '
            listMovimiento = GetListMovimientos(Convert.ToInt32(lblIdAgenteCaja.Text))

            ProcesoExportar("PDF", "pdf", listMovimiento)
            '
        End If
        '
    End Sub


    'PROCESO PARA EXPORTAR
    Private Sub ProcesoExportar(ByVal opcion As String, ByVal ext As String, ByVal listMovimiento As List(Of BEMovimientoConsulta))
        '
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")

        Dim localReport As New LocalReport()

        localReport.ReportPath = Server.MapPath("~\Reportes\RptDetalleMovimientoCaja.rdlc")

        localReport.SetParameters(GetParametros(listMovimiento))

        If localReport.DataSources.Count > 0 Then
            localReport.DataSources.RemoveAt(0)
        End If

        localReport.DataSources.Add(New ReportDataSource("BEMovimientoConsulta", listMovimiento))

        ResponseExport(localReport, opcion, ext)
        '
    End Sub

    'RESONSE DE EXPORTACION
    Private Sub ResponseExport(ByVal localReport As LocalReport, ByVal Type As String, ByVal fileExtension As String)
        '
        Dim reportType As String = Type
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim fileNameExtension As String = fileExtension

        Dim deviceInfo As String = _
        "<DeviceInfo>" + _
        "  <OutputFormat>" + reportType + "</OutputFormat>" + _
        "</DeviceInfo>"

        Dim warnings As Warning() = Nothing
        Dim streams As String() = Nothing
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render( _
            reportType, _
            deviceInfo, _
            mimeType, _
            encoding, _
            fileNameExtension, _
            streams, _
            warnings)

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=DetMovimientosCaja_" + Date.Now.ToShortDateString + "." + fileNameExtension)
        Response.BinaryWrite(renderedBytes)
        Response.End()
        '
    End Sub

End Class
