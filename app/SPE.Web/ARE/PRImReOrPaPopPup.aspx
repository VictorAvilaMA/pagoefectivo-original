<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRImReOrPaPopPup.aspx.vb" Inherits="ARE_PRImReOrPaPopPup" %>
<%@ PreviousPageType VirtualPath="~/ARE/PRReOrPa.aspx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title> PagoEfectivo - Imprimir Código de Identificación de Pago</title>
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/default.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <script  type ="text/javascript" language ="javascript"  >
            function OnLoadPrint()
            {
             print();
            }
    </script>
</head>
<body onload="javascript:OnLoadPrint();">
    <form id="form1" runat="server">
        <div id="Page" style="width: 460px; ">
            <div id="div1" class="divContenedorGrilla" style="text-align:center;">
                <fieldset class ="ContenedorEtiquetaTextoPrint">
                            <div id="div6" class="EtiquetaTextoIzquierda">
                            <br />
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                           <div id="div2" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblAgenciaRecaudadora" runat="server" CssClass="EtiquetaLargoCenter" style="text-align: center" ></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div4" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblDireccion" runat="server" CssClass="EtiquetaLargoCenter"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div3" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblUbigeo" runat="server" CssClass="EtiquetaLargoCenter"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div5" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblTelefono" runat="server" CssClass="EtiquetaLargoCenter" style="text-align: center"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div7" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblNumeroOrdenPago" runat="server" CssClass="EtiquetaLargoCenter"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div9" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblGuionHeader" runat="server" CssClass="EtiquetaLargoCenter" Text="------------------------------------------------------------"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div10" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblCliente" runat="server" CssClass="Etiqueta" Text="Cliente:"></asp:Label>
                            </div>
                            <div id="div8" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblDescCliente" runat="server" CssClass="Etiqueta" style="text-align: left" Width="300px"></asp:Label>
                            </div>
                </fieldset>
                <fieldset runat="server" id="fsEmpresaContratante" class="ContenedorEtiquetaTextoPrint">
                            <div id="div17" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblEmpresa" runat="server" CssClass="Etiqueta" Text="Empresa:"></asp:Label>
                            </div>
                            <div id="div18" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblDescEmpresa" runat="server" CssClass="Etiqueta" style="text-align: left" Width="300px"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div19" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblServicio" runat="server" CssClass="Etiqueta" Text="Servicio:"></asp:Label>
                            </div>
                            <div id="div20" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblDescServicio" runat="server" CssClass="Etiqueta" style="text-align: left" Width="300px"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div13" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblConcepto" runat="server" CssClass="Etiqueta" Text="Concepto:"></asp:Label>
                            </div>
                            <div id="div14" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblDescConcepto" runat="server" CssClass="Etiqueta" style="text-align: left" Width="300px"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div15" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblTotal" runat="server" CssClass="Etiqueta" Text="Total:"></asp:Label>
                            </div>
                            <div id="div16" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblDescTotal" runat="server" CssClass="Etiqueta" style="text-align: left" Width="300px"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div11" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblGuionFooter" runat="server" CssClass="EtiquetaLargoCenter" Text="--------------------------------------------------------------"></asp:Label>
                            </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTextoPrint">
                            <div id="div12" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="lblCajero" runat="server" CssClass="EtiquetaLargoCenter"></asp:Label>
                            </div>
                </fieldset>
            </div>
        </div>
    </form>
</body>
</html>

