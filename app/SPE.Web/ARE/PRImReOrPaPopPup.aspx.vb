Imports SPE.Entidades
Partial Class ARE_PRImReOrPaPopPup
    Inherits _3Dev.FW.Web.PageBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            '
            CargarInfoPrint()
            '
        End If
        '
    End Sub

    'CARGAR DATOS
    Private Sub CargarInfoPrint()
        '
        Try
            '
            CargarDatosAgenciaRecaudadora()
            '
            Dim IdOrdenPago As Integer = Convert.ToInt32(Request.QueryString("IdOrdenPago"))
            Dim objCOrdenPago As New SPE.Web.COrdenPago
            Dim obeOrdenPago As New BEOrdenPago
            obeOrdenPago.IdOrdenPago = IdOrdenPago
            obeOrdenPago = objCOrdenPago.ConsultarOrdenPagoPorId(obeOrdenPago)

            fsEmpresaContratante.Visible = Not (obeOrdenPago.OcultarEmpresa = "True")

            With Request
                lblNumeroOrdenPago.Text = "Nro. C.I.P.: " & .QueryString("NumeroOP").ToString() & "   Fecha: " & .QueryString("Fecha").ToString()
                lblCajero.Text = "AGENTE: " & .QueryString("Agente") & "  -  " & .QueryString("Caja")
                lblDescCliente.Text = .QueryString("Cliente")
                lblDescEmpresa.Text = .QueryString("Empresa")
                lblDescServicio.Text = .QueryString("Servicio")
                lblDescConcepto.Text = .QueryString("Concepto")
                lblDescTotal.Text = .QueryString("Total")
            End With
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        '
    End Sub

    'CARGAR DATOS AGENCIA RECAUDADORA
    Private Sub CargarDatosAgenciaRecaudadora()
        '
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
        obeAgenciaRecaudadora = objCAdministrarAgenciaRecaudadora.ConsultarAgenciaRecaudadoraPorIdUsuario(UserInfo.IdUsuario)
        With obeAgenciaRecaudadora
            lblAgenciaRecaudadora.Text = .NombreComercial & " - R.U.C.:" & .Ruc
            lblDireccion.Text = .Direccion
            lblUbigeo.Text = .Ubigeo
            lblTelefono.Text = "Tel�f: " & .Telefono & "  -  E-mail: " & .Email
        End With
        '
    End Sub
End Class
