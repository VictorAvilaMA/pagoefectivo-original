<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRLiCa.aspx.vb" Inherits="ARE_PRLiCa" Title="PagoEfectivo - Liquidar Caja" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page" class="colRight">
        <h1>
            <asp:Literal ID="lblCajaActiva" runat="server" Text="Liquidar Caja"></asp:Literal>
        </h1>
        <div id="divInfGeneral" class="dlgrid">
            <div class="inner-col-right">
                <h4>
                    1. Informaci�n de caja activa</h4>
                <div class="w100 clearfix dlinline">
                    <dl class="even clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="lbl1" runat="server" Text="Agente Recaudador:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label>
                                <asp:Literal ID="txtAgente" runat="server"></asp:Literal></label>
                        </dd>
                        <asp:Label ID="lblIdAgenteCaja" runat="server" CssClass="Hidden" Visible="False"></asp:Label>
                    </dl>
                </div>
                <div class="w100 clearfix dlinline">
                    <dl class=" clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="Label4" runat="server" Text="Caja:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label>
                                <asp:Literal ID="txtCaja" runat="server"></asp:Literal></label>
                        </dd>
                    </dl>
                </div>
                <div class="w100 clearfix dlinline">
                    <dl class="even clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="Label5" runat="server" Text="Fecha Apertura:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label>
                                <asp:Literal ID="lblFechaApertura" runat="server"></asp:Literal></label>
                        </dd>
                    </dl>
                </div>
                <div class="w100 clearfix dlinline">
                    <dl class=" clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="Label7" runat="server" Text="C.I.P. Recibidas:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label>
                                <asp:Literal ID="txtNrotransacciones" runat="server"></asp:Literal></label>
                        </dd>
                    </dl>
                </div>
                <div class="w100 clearfix dlinline">
                    <dl class="even clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="Label11" runat="server" Text="C.I.P. Anuladas:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label>
                                <asp:Literal ID="lblAnuladas" runat="server"></asp:Literal></label>
                        </dd>
                    </dl>
                </div>
                <div class="w100 clearfix dlinline">
                    <dl class=" clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="lblSobrante" runat="server" Text="Sobrante:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label style="color: red;">
                                <asp:Literal ID="lblDescSobrante" runat="server"></asp:Literal></label>
                        </dd>
                    </dl>
                </div>
                <div class="w100 clearfix dlinline">
                    <dl class="even clearfix dt15 dd30">
                        <dt class="desc">
                            <label>
                                <asp:Literal ID="lblFaltante" runat="server" Text="Faltante:"></asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <label style="color: red;">
                                <asp:Literal ID="lblDescFaltante" runat="server"></asp:Literal></label>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="sep_a14">
            </div>
            <div class="inner-col-right">
                <asp:Panel ID="upnlLiquidar" runat="server">
                    <div id="div1" class="divContenedor">
                        <h4>
                            2. Informaci�n necesaria para el cierre de caja</h4>
                        <div class="w100 clearfix dlinline">
                            <dl class="even clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="Label3" runat="server" Text="Fec. Cierre">
                                        </asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtFechaCierre" runat="server" ReadOnly="True" CssClass="Texto SoloLectura"></asp:TextBox>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class=" clearfix dt15 dd60">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="Label1" runat="server" Text="Medio de Pago:"></asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <asp:DropDownList ID="ddlMedioPago" runat="server" CssClass="clsform1">
                                        <asp:ListItem Value="1">Efectivo</asp:ListItem>
                                        <asp:ListItem Value="2">Tarjeta</asp:ListItem>
                                    </asp:DropDownList>
                                    <div>
                                        <label class="izq">
                                            <asp:Literal ID="Label2" runat="server" Text="Moneda:"></asp:Literal>
                                        </label>
                                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="clsform1">
                                            <asp:ListItem Value="1">Soles</asp:ListItem>
                                            <asp:ListItem Value="2">Dolares</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class="even clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="Label6" runat="server" Text="Monto:">
                                        </asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtMonto" runat="server" CssClass="miniCamp" MaxLength="12"></asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtMonto"
                                        ErrorMessage="Ingrese un monto positivo." MaximumValue="999999999999" MinimumValue="1"
                                        Type="Currency" ValidationGroup="ValidarControles">*</asp:RangeValidator>
                                    <asp:ImageButton ID="ibtnAgregar" runat="server" 
                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/add.jpg"%>'
                                        ToolTip="Agregar Monto" ValidationGroup="ValidarControles"></asp:ImageButton>
                                    <div id="Div2" class="EtiquetaTextoIzquierda">
                                        <p class="msjenomargin">
                                            <asp:Literal ID="lblMensSaldo" runat="server" Text="N�mero no v�lido" Visible="False">
                                            </asp:Literal></p>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                        <div id="divOrdenesPago" class="result">
                            <asp:GridView ID="gvMontoCierre" CssClass="grilla" runat="server" AutoGenerateColumns="False"
                                CellPadding="3" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                                Width="447px">
                                <Columns>
                                    <asp:TemplateField HeaderText="Eliminar">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>"
                                                CommandName="Eliminar" ImageUrl="~/Images_Buton/b_eliminar.gif" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="IdMedioPago" DataField="IdMedioPago" Visible="False">
                                        <ControlStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MedioPago" HeaderText="MedioPago">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="IdMoneda" DataField="IdMoneda" Visible="False">
                                        <ControlStyle Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Moneda" HeaderText="Moneda">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Monto" DataField="Monto" DataFormatString="{0:#,#0.00}">
                                        <ControlStyle Width="30px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("url") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="url" HeaderText="URL" Visible="False" />
                                </Columns>
                                <HeaderStyle CssClass="cabecera" />
                            </asp:GridView>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class=" clearfix dt15 dd60">
                                <div>
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="Label9" runat="server" Text="Monto Incorrecto"></asp:Literal></label>
                                    </dt>
                                    <dt class="desc">
                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/aspa.jpg"%>' />
                                    </dt>
                                </div>
                                <div>
                                    <dt class="desc">
                                        <label class="izq">
                                            <asp:Literal ID="Label10" runat="server" Text="Monto Correcto"></asp:Literal>
                                        </label>
                                    </dt>
                                    <dt class="desc">
                                        <asp:Image ID="Image4" runat="server" 
                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/ok2.jpg"%>' />
                                    </dt>
                                </div>
                            </dl>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="sep_a14">
            </div>
            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="ValidarControles" />
            <div id="fsBotonera" class="clearfix dlinline btns3">
                <asp:Button ID="btnLiquidarCaja" runat="server" Text="Liquidar" CssClass="btnLiquidar"
                    OnClientClick="return LiquidarCaja();" CausesValidation="False" ValidationGroup="ValidarControles" />
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btnCancelar" CausesValidation="False" />
            </div>
        </div>
    </div>
</asp:Content>
