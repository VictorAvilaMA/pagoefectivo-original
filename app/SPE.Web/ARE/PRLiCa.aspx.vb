Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports SPE.Entidades
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Web.Security

Partial Class ARE_PRLiCa
    Inherits System.Web.UI.Page

    Dim dtCierre As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            CargaCajaActiva()

            Session("Cierre") = dtCierre
            CargarCombos("Moneda")
            CargarCombos("MedioPago")
            txtFechaCierre.Text = DateTime.Today.ToShortDateString()

            'ESTRUCTURA DE LA TABLA
            dtCierre.Columns.Add("IdMedioPago")
            dtCierre.Columns.Add("MedioPago")
            dtCierre.Columns.Add("IdMoneda")
            dtCierre.Columns.Add("Moneda")
            dtCierre.Columns.Add("Monto")
            dtCierre.Columns.Add("url")
            Session("dtSaldoInicial") = dtCierre


        End If

    End Sub

    Private Sub CargarCombos(ByVal Combo As String)
        Dim objCComun As New SPE.Web.CAdministrarComun
       
        Select Case Combo
            Case "Moneda"
                ddlMoneda.DataSource = objCComun.ConsultarMoneda()
                ddlMoneda.DataTextField = "Descripcion"
                ddlMoneda.DataValueField = "IdMoneda"
                ddlMoneda.DataBind()
            Case "MedioPago"
                ddlMedioPago.DataSource = objCComun.ConsultarMedioPago()
                ddlMedioPago.DataTextField = "Descripcion"
                ddlMedioPago.DataValueField = "IdMedioPago"
                ddlMedioPago.DataBind()
        End Select
    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Protected Sub gvMontoCierre_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMontoCierre.RowCommand
        Try
            Dim indice As Integer = Convert.ToInt32(e.CommandArgument)

            Select Case (e.CommandName)
                Case "Eliminar"
                    dtCierre = Session("Cierre")
                    dtCierre.Rows.RemoveAt(indice)
                    Session("Cierre") = dtCierre
                    gvMontoCierre.DataSource = dtCierre
                    gvMontoCierre.DataBind()

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CargaCajaActiva()

        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeCajaActiva As New SPE.Entidades.BEAgenteCaja
        obeCajaActiva = objCAgenciaRecaudadora.ConsultaCajaActiva(UserInfo.IdUsuario) 'Mandar Id Agente


        txtAgente.Text = obeCajaActiva.NombreAgente
        txtCaja.Text = obeCajaActiva.NombreCaja
        lblFechaApertura.Text = obeCajaActiva.FechaApertura
        txtNrotransacciones.Text = obeCajaActiva.NroOPRecibidas
        lblIdAgenteCaja.Text = obeCajaActiva.IdAgenteCaja.ToString
        lblAnuladas.Text = obeCajaActiva.NroOPAnuladas.ToString

        If IsNumeric(obeCajaActiva.IdAgenteCaja) Then
            If obeCajaActiva.IdAgenteCaja <> 0 Then
                CargarSobranteFaltante(obeCajaActiva.IdAgenteCaja)
            End If
        End If

        If (obeCajaActiva.NombreAgente <> "" And obeCajaActiva.NroOPRecibidas = 0) Then
            lblCajaActiva.Text = "A�n no se han realizado transacciones."
            txtFechaCierre.Text = ""
            upnlLiquidar.Visible = False
            DeshabilitarControles()
        ElseIf (obeCajaActiva.NombreAgente = "") Then
            lblCajaActiva.Text = "No tiene caja activa."
            txtFechaCierre.Text = ""
            upnlLiquidar.Visible = False
            DeshabilitarControles()
        ElseIf obeCajaActiva.Liquidar = False Then
            lblCajaActiva.Text = "No se puede liquidar ya que la fecha de apertura no coincide con la fecha actual."
            DeshabilitarControles()
        End If

    End Sub

    Private Sub CargarSobranteFaltante(ByVal idAgenteCaja As Integer)
        '
        Dim listMovimiento As New List(Of BEMovimientoConsulta)
        listMovimiento = GetListMovimientos(idAgenteCaja)
        '
        If listMovimiento.Count > 0 Then
            '
            Dim Sobrante As String = "" : Dim Faltante As String = ""

            '
            For i As Integer = 0 To listMovimiento.Count - 1
                '
                If listMovimiento(i).DescTipoMovimiento.ToUpper = "SOBRANTE" Then
                    If i < listMovimiento.Count - 1 Then
                        Sobrante = Sobrante & listMovimiento(i).Monto.ToString("#,#0.00") & " " & listMovimiento(i).DescMoneda & ", "
                    Else
                        Sobrante = Sobrante & listMovimiento(i).Monto.ToString("#,#0.00") & " " & listMovimiento(i).DescMoneda
                    End If
                End If
                '
            Next
            '
            For i As Integer = 0 To listMovimiento.Count - 1
                If listMovimiento(i).DescTipoMovimiento.ToUpper = "FALTANTE" Then
                    If i < listMovimiento.Count - 1 Then
                        Faltante = Faltante & listMovimiento(i).Monto.ToString("#,#0.00") & " " & listMovimiento(i).DescMoneda & ", "
                    Else
                        Faltante = Faltante & listMovimiento(i).Monto.ToString("#,#0.00") & " " & listMovimiento(i).DescMoneda
                    End If
                End If
            Next
            '
            If Sobrante = "" Then
                Sobrante = ""
            Else
                Sobrante = "Ud. registr� un sobrante de: " & Sobrante & "; el cual deber� estar incluido en el proceso de Liquidar Caja."
            End If
            '
            If Faltante = "" Then
                Faltante = ""
            Else
                Faltante = "Ud. registr� un faltante de: " & Faltante & "; el cual NO deber� estar incluido en el proceso de Liquidar Caja."
            End If
            '
            lblDescSobrante.Text = Sobrante
            lblDescFaltante.Text = Faltante
            '
        Else
            '
            lblDescSobrante.Text = ""
            lblDescFaltante.Text = ""
            '
        End If
        '
    End Sub

    'OBTENEMOS LA LISTA DE MOVIMIENTOS
    Private Function GetListMovimientos(ByVal idAgenteCaja As Integer) As List(Of BEMovimientoConsulta)
        '
        Dim listMovimiento As New List(Of SPE.Entidades.BEMovimientoConsulta)
        Dim objCAgencia As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgenteCaja As New SPE.Entidades.BEAgenteCaja

        obeAgenteCaja.IdAgenteCaja = idAgenteCaja

        listMovimiento = objCAgencia.ConsultarDetalleMovimientosCaja(obeAgenteCaja)

        Return listMovimiento
        '
    End Function

    Private Sub DeshabilitarControles()

        ddlMedioPago.Enabled = False
        ddlMoneda.Enabled = False
        txtMonto.Enabled = False
        ibtnAgregar.Enabled = False
        btnLiquidarCaja.Enabled = False


    End Sub

    Protected Sub btnLiquidarCaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLiquidarCaja.Click
        ''''
        Dim ListCierre As New Collection(Of SPE.Entidades.BEMovimiento)
        Dim obeCierre As SPE.Entidades.BEMovimiento
        Dim objCAgencia As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora

        dtCierre = Session("Cierre")

        Try
            For Each row As DataRow In dtCierre.Rows

                obeCierre = New SPE.Entidades.BEMovimiento
                obeCierre.IdMedioPago = CInt(row("IdMedioPago").ToString)
                obeCierre.IdMoneda = CInt(row("IdMoneda"))
                obeCierre.Monto = CDbl(row("Monto"))
                ListCierre.Add(obeCierre)

            Next

            If ListCierre.Count > 0 Then

                ListCierre = objCAgencia.ConsultaMontosPorAgenteCaja(CInt(lblIdAgenteCaja.Text), ListCierre) 'Mandar el IdAgente
                CargarResultadoCierre(ListCierre)

                If ListCierre(0).IdTipoMovimiento = SPE.EmsambladoComun.ParametrosSistema.EstadoPuedeLiquidar.Si Then
                    Dim obeLiquidarCaja As New SPE.Entidades.BEAgenteCaja
                    obeLiquidarCaja.IdAgenteCaja = Convert.ToInt32(lblIdAgenteCaja.Text)
                    obeLiquidarCaja.FechaCierre = Convert.ToDateTime(txtFechaCierre.Text)

                    objCAgenciaRecaudadora.LiquidarAgenteCaja(obeLiquidarCaja)
                    btnLiquidarCaja.Enabled = False
                    upnlLiquidar.Enabled = False
                    lblMensaje.CssClass = "MensajeTransaccion"
                    lblMensaje.Text = "La ''" & txtCaja.Text & "'' se liquid� satisfactoriamente - " & Date.Now.ToString & ". "
                Else
                    lblMensaje.CssClass = "MensajeValidacion"
                    lblMensaje.Text = "No se puede liquidar porque los montos no coinciden."
                End If

            Else
                lblMensaje.Text = "Debe ingresar el(los) monto(s)."
            End If


        Catch ex As Exception
            'Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try

        
    End Sub

    Private Sub CargarResultadoCierre(ByVal ListCierre As Collection(Of SPE.Entidades.BEMovimiento))
        Dim dtResultado As New DataSet
        dtCierre = Session("Cierre")

        For Each entidad As SPE.Entidades.BEMovimiento In ListCierre
            If entidad.Monto = 0 Then
                For Each row As DataRow In dtCierre.Rows
                    If CInt(row("IdMedioPago")) = entidad.IdMedioPago And CInt(row("IdMoneda")) = entidad.IdMoneda Then
                        row("url") = "~/Images_Buton/aspa.jpg"
                    End If
                Next
            ElseIf entidad.Monto = 1 Then
                For Each row As DataRow In dtCierre.Rows
                    If CInt(row("IdMedioPago")) = entidad.IdMedioPago And CInt(row("IdMoneda")) = entidad.IdMoneda Then
                        row("url") = "~/Images_Buton/ok2.jpg"
                    End If
                Next
            End If
        Next
        dtCierre.AcceptChanges()
        gvMontoCierre.DataSource = dtCierre
        gvMontoCierre.DataBind()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("~/ARE/PgPrl.aspx")
    End Sub

    Protected Sub ibtnAgregar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnAgregar.Click
        Try
            Dim Existe As Boolean = False
            dtCierre = Session("Cierre")

            For Each row As DataRow In dtCierre.Rows
                If CInt(row("IdMedioPago")) = ddlMedioPago.SelectedValue And CInt(row("IdMoneda") = ddlMoneda.SelectedValue) Then
                    row("Monto") = CDec(txtMonto.Text).ToString("#,#0.00")
                    dtCierre.AcceptChanges()
                    Existe = True
                End If
            Next

            If Existe = False Then
                dtCierre.Rows.Add(ddlMedioPago.SelectedValue, _
                ddlMedioPago.SelectedItem.Text, _
                ddlMoneda.SelectedValue, _
                ddlMoneda.SelectedItem.Text, _
                Convert.ToDecimal(txtMonto.Text).ToString("#,#0.00"), _
                "~/images/clear.gif")
                Session("Cierre") = dtCierre
            End If

            txtMonto.Text = ""
            gvMontoCierre.DataSource = dtCierre
            gvMontoCierre.DataBind()
        Catch ex As Exception
        End Try

    End Sub

End Class
