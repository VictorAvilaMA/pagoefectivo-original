<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRLiCaBl.aspx.vb" Inherits="ARE_PRLiCaBl" Title="PagoEfectivo - Liquidar Caja en Bloque" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Page" class="dlgrid">
        <asp:ScriptManager ID="ScriptManager" runat="server">
        </asp:ScriptManager>
        <h1>
            Liquidar Caja en Bloque</h1>
        <div id="div1" class="inner-col-right" style="left: 0px; top: 0px;">
            <h3>
                <asp:Label ID="lblResultado" runat="server" Text="" />
                <asp:Label ID="lblIdAgencia" runat="server" Text="" Visible="false" />
            </h3>
            <div id="divOrdenesPago" class="result">
                <asp:GridView ID="gvCajasCerradas" CssClass="grilla" runat="server" AutoGenerateColumns="False"
                    CellPadding="3" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                    ShowFooter="False" AllowPaging="True" PageSize="15">
                    <Columns>
                        <asp:BoundField DataField="IdAgenteCaja" HeaderText="ID" Visible="False">
                            <ItemStyle CssClass="Hidden" />
                            <HeaderStyle CssClass="Hidden" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Caja" DataField="NombreCaja" SortExpression="NombreCaja">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Agente" DataField="NombreAgente" SortExpression="NombreAgente">
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Fecha Apertura" DataField="FechaApertura" SortExpression="FechaApertura">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaCierre" HeaderText="Fecha Cierre" SortExpression="FechaCierre">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NroOPRecibidas" HeaderText="C.I.P." SortExpression="NroOPRecibidas">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NroOPAnuladas" HeaderText="Anulados" Visible="False" SortExpression="NroOPAnuladas">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Liquidar">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckbLiquidar" runat="server" CausesValidation="True" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                            <HeaderStyle HorizontalAlign="Center" Width="55px" />
                            <HeaderTemplate>
                                <asp:Label ID="lblLiquidar" runat="server" Text="Liquidar"></asp:Label>
                                <asp:CheckBox ID="ckbLiquidarTodos" AutoPostBack="true" runat="server" CausesValidation="True"
                                    Text="" Width="40px" OnCheckedChanged="ckbLiquidarTodos_CheckedChanged" />
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ibtnVerDetalle" runat="server" ImageUrl="~/images/lupa.gif"
                                    ToolTip="Ver Detalle" CommandArgument='<%# CType(Container,GridViewRow).RowIndex %>'
                                    OnCommand="ibtnVerDetalle_Command" />
                                <asp:Label ID="lblNombreCaja" runat="server" Text='<%# Eval("NombreCaja") %>' Visible="False"
                                    Width="1px"></asp:Label>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("IdAgenteCaja") %>' Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="cabecera" />
                </asp:GridView>
            </div>
        </div>
        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
        <fieldset id="fsBotonera" class="w100 clearfix dlinline btns2" style="width: 455px;">
            <asp:Button ID="btnLiquidar" runat="server" Text="Liquidar Cajas" CssClass="btnLiquidar"
                OnClientClick="return ConfirmLiCaBlo();" />
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btnCancelar" />
        </fieldset>
    </div>
    <cc1:ModalPopupExtender ID="mppDetalleCaja" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="imgbtnRegresar" X="300" Y="200" PopupControlID="pnlPopupEmpresaContrante"
        TargetControlID="btnPopup">
    </cc1:ModalPopupExtender>
    <asp:Button ID="btnPopup" runat="server" BackColor="White" BorderColor="White" BorderStyle="None"
        Enabled="False" Width="1px" />
    <asp:Panel ID="pnlPopupEmpresaContrante" runat="server" Style="display: none;">
        <div id="divBusquedaRepresentantes" class="divContenedor" style="left: 1px; top: 0px;
            text-align: center;">
            <div id="div11" class="divContenedorTitulo">
                <div style="float: left">
                    <b><asp:Literal ID="lblTitulo" runat="server" Text="Resumen"></asp:Literal></b>
                </div>
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar" 
                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/closeX.GIF"%>'
                                    CausesValidation="false"
                        runat="server"></asp:ImageButton>
                </div>
            </div>
            <asp:UpdatePanel ID="upnlListaEmprContrantes" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-left: 10px">
                        &nbsp;
                    </div>
                    <div>
                        <asp:GridView ID="gvResultado" runat="server" AutoGenerateColumns="False" CssClass="grilla">
                            <Columns>
                                <asp:BoundField DataField="MedioPago" HeaderText="MedioPago"></asp:BoundField>
                                <asp:BoundField DataField="Moneda" HeaderText="Moneda">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:#,#0.00}">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</asp:Content>
