<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRLiCaBlReporte.aspx.vb" Inherits="ARE_PRLiCaBlReporte" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PagoEfectivo - Reporte de Cajas Liquidadas</title>
    <link href="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/default.css?<%# System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True" EnableScriptGlobalization="True"></asp:ScriptManager>
    
<div style="width:800px; margin-left:60px; ">
    <div id="divInfGeneral"  >
    
       <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" >
           <strong>REPORTE DE CAJAS LIQUIDADAS</strong>
       </div>
               
       <fieldset  class="ContenedorEtiquetaTexto" >
                        
                      <div id="Div3" class="EtiquetaTextoIzquierda">
                            <asp:Label ID="lblNumOP" runat="server" Text="C.I.P.:&nbsp;" CssClass="EtiquetaLargo" Width="90px" style="text-align: right"></asp:Label>
                          <asp:TextBox ID="txtNumeroOrdenPago" runat="server" CssClass="Texto" Width="100px"></asp:TextBox>
                      </div>
                      <div id="Div7" class="EtiquetaTextoIzquierda" >
                            <asp:Label ID="Label8" runat="server" Text="Fecha Inicio:" CssClass="EtiquetaLargo" Width="45px"></asp:Label>
                          <asp:TextBox ID="txtDel" runat="server" CssClass="Texto" Width="85px"></asp:TextBox>
                          <asp:ImageButton ID="imgDel" runat="server"
                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/date.gif"%>' />
                          <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="mexDel" ControlToValidate="txtDel" ErrorMessage="*" InvalidValueMessage="Fecha de inicio no v�lida" IsValidEmpty="False" EmptyValueMessage="Fecha de inicio es requerida" Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                      </div>
                      <div id="Div8" class="EtiquetaTextoIzquierda" >
                        <asp:Label ID="Label9" runat="server" Text="Fecha Fin:" CssClass="EtiquetaLargo" Width="45px"></asp:Label>  
                          <asp:TextBox ID="txtAl" runat="server" CssClass="Texto" Width="85px"></asp:TextBox>
                        <asp:ImageButton ID="imgAl" runat="server" 
                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/date.gif"%>'/>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="mexAl" ControlToValidate="txtAl" ErrorMessage="*" InvalidValueMessage="Fecha de fin no v�lida" IsValidEmpty="False" EmptyValueMessage="Fecha de fin es requerida" Display="Dynamic" EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                          <asp:CompareValidator ID="cvaAl" runat="server" ControlToCompare="txtAl" ControlToValidate="txtDel" ErrorMessage="Rango de fechas no es v�lido" Operator="LessThanEqual" Type="Date">*</asp:CompareValidator>
                        </div>
                    <div id="Div1" class="EtiquetaTextoIzquierda" >
                                <asp:Button ID="btnVer" runat="server" Text="Ver Reporte" CssClass="btnVerReport" />
                                <br />
                        <asp:UpdatePanel ID="UpdatePanelImagen" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:Button ID="btnExportarExcel" runat="server" CssClass="btnExcel" Text="EXCEL" Enabled="false"/>            
                                <br />
                                <asp:Button id="btnExportarPDF"  runat="server" CssClass="btnPdf" Enabled="false" Text="PDF" />
                                <br />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnVer" EventName="Click" />
                                <asp:PostBackTrigger ControlID="btnExportarExcel" />
                                <asp:PostBackTrigger ControlID="btnExportarPDF" />
                            </Triggers>
                        </asp:UpdatePanel>
                    
                    </div>
       </fieldset>
                
            <cc1:calendarextender id="cexDel" runat="server" format="dd/MM/yyyy" popupbuttonid="imgDel" targetcontrolid="txtDel"></cc1:calendarextender>
            <cc1:calendarextender id="cexAl" runat="server" popupbuttonid="imgAl" targetcontrolid="txtAl" Format="dd/MM/yyyy"></cc1:calendarextender>
            <cc1:MaskedEditExtender ID="mexDel" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="txtDel" CultureName="es-PE"></cc1:MaskedEditExtender>
            <cc1:MaskedEditExtender ID="mexAl" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="txtAl" CultureName="es-PE"></cc1:MaskedEditExtender>
            <asp:ValidationSummary ID="ValidationSummary" runat="server" style="font-size: 11pt" DisplayMode="List" />
        <cc1:FilteredTextBoxExtender ID="FTBE_txtNumeroOrdenPago" runat="server" ValidChars="0123456789" TargetControlID="txtNumeroOrdenPago">
        </cc1:FilteredTextBoxExtender>
    </div>
    <div >
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <rsweb:ReportViewer ID="RptCaja" runat="server" Height="720px" Width="800px" ShowRefreshButton="False" ShowExportControls="False" SizeToReportContent="True"></rsweb:ReportViewer>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnVer" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        
        </div>
     
</div>        
    </form>
</body>
</html>
