Imports SPE.Entidades
Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Imports _3Dev.FW.Web.Security

Partial Class ARE_PRLiCaBlReporte
    Inherits System.Web.UI.Page

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Page.Header.DataBind()
            txtDel.Text = DateTime.Today
            txtAl.Text = DateTime.Today
            CargarReporte(UserInfo.IdUsuario, Convert.ToDateTime(txtDel.Text), Convert.ToDateTime(txtAl.Text))
            '
        End If

    End Sub

    'BOTON VER
    Protected Sub btnVer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVer.Click
        CargarReporte(UserInfo.IdUsuario, Convert.ToDateTime(txtDel.Text), Convert.ToDateTime(txtAl.Text))
    End Sub

    'CARGAR REPORTE
    Private Sub CargarReporte(ByVal IdUsuario As Integer, ByVal FechaIncio As DateTime, ByVal FechaFin As DateTime)

        Dim listBEAgenteCaja As New List(Of BEAgenteCaja)

        listBEAgenteCaja = GetListBEAgenteCaja(IdUsuario, FechaIncio, FechaFin)

        If RptCaja.LocalReport.DataSources.Count > 0 Then
            RptCaja.LocalReport.DataSources.RemoveAt(0)
        End If

        RptCaja.LocalReport.DataSources.Add(New ReportDataSource("BEAgenteCaja", listBEAgenteCaja))
        RptCaja.LocalReport.ReportPath = "Reportes\RptCajasLiquidadas.rdlc"

        RptCaja.LocalReport.SetParameters(GetParametros(listBEAgenteCaja))
        RptCaja.LocalReport.Refresh()

    End Sub

    'IMAGEN PARA EXPORTAR
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        '
        Dim listBEAgenteCaja As New List(Of BEAgenteCaja)
        '
        listBEAgenteCaja = GetListBEAgenteCaja(UserInfo.IdUsuario, Convert.ToDateTime(txtDel.Text), Convert.ToDateTime(txtAl.Text))
        '
        If listBEAgenteCaja.Count > 0 Then
            '
            ProcesoExportar("EXCEL", "xls", listBEAgenteCaja)
            '
        End If
        '
    End Sub

    Protected Sub btnExportarPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarPDF.Click
        '
        Dim listBEAgenteCaja As New List(Of BEAgenteCaja)
        '
        listBEAgenteCaja = GetListBEAgenteCaja(UserInfo.IdUsuario, Convert.ToDateTime(txtDel.Text), Convert.ToDateTime(txtAl.Text))
        '
        If listBEAgenteCaja.Count > 0 Then
            '
            ProcesoExportar("PDF", "pdf", listBEAgenteCaja)
            '
        End If
        '
    End Sub

    'OBTENEMOS LOS PARAMETROS
    Private Function GetParametros(ByVal listBEAgenteCaja As List(Of BEAgenteCaja)) As List(Of ReportParameter)
        '
        Dim paramList As New List(Of ReportParameter)
        Dim param1 As ReportParameter

        If listBEAgenteCaja.Count > 0 Then
            param1 = New ReportParameter("parTitulo", "Reporte de Cajas Liquidadas desde: " & txtDel.Text & " al " & txtAl.Text & " - " & listBEAgenteCaja(0).NombreAgencia.ToUpper())
            btnExportarExcel.Enabled = True
            btnExportarPDF.Enabled = True
        Else
            param1 = New ReportParameter("parTitulo", "")
            btnExportarExcel.Enabled = False
            btnExportarPDF.Enabled = False
        End If
        '
        paramList.Add(param1)
        '
        Return paramList
        '
    End Function

    'OBTENEMOS LA LISTA DE AGENTE CAJA
    Private Function GetListBEAgenteCaja(ByVal IdUsuario As Integer, ByVal FechaIncio As DateTime, ByVal FechaFin As DateTime) As List(Of BEAgenteCaja)
        '
        Dim listBEAgenteCaja As New List(Of SPE.Entidades.BEAgenteCaja)
        Dim objCAgencia As New SPE.Web.CAdministrarAgenciaRecaudadora

        listBEAgenteCaja = objCAgencia.ConsultarCajasLiquidadas(IdUsuario, txtNumeroOrdenPago.Text, FechaIncio, FechaFin)

        Return listBEAgenteCaja
        '
    End Function

    'PROCESO PARA EXPORTAR
    Private Sub ProcesoExportar(ByVal opcion As String, ByVal ext As String, ByVal listBEAgenteCaja As List(Of BEAgenteCaja))
        '
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")

        Dim localReport As New LocalReport()

        localReport.ReportPath = Server.MapPath("~\Reportes\RptCajasLiquidadas.rdlc")

        localReport.SetParameters(GetParametros(listBEAgenteCaja))

        If localReport.DataSources.Count > 0 Then
            localReport.DataSources.RemoveAt(0)
        End If

        localReport.DataSources.Add(New ReportDataSource("BEAgenteCaja", listBEAgenteCaja))

        ResponseExport(localReport, opcion, ext)
        '
    End Sub

    'RESONSE DE EXPORTACION
    Private Sub ResponseExport(ByVal localReport As LocalReport, ByVal Type As String, ByVal fileExtension As String)
        '
        Dim reportType As String = Type
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim fileNameExtension As String = fileExtension

        Dim deviceInfo As String = _
        "<DeviceInfo>" + _
        "  <OutputFormat>" + reportType + "</OutputFormat><PageWidth>11in</PageWidth><PageHeight>8.5in</PageHeight><MarginLeft>1in</MarginLeft><MarginRight>1in</MarginRight>" + _
        "</DeviceInfo>"

        Dim warnings As Warning() = Nothing
        Dim streams As String() = Nothing
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render( _
            reportType, _
            deviceInfo, _
            mimeType, _
            encoding, _
            fileNameExtension, _
            streams, _
            warnings)

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=ReporteCajaLiquidada_" + Date.Now.ToShortDateString + "." + fileNameExtension)
        Response.BinaryWrite(renderedBytes)
        Response.End()
        '
    End Sub

End Class
