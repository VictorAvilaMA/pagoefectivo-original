<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PROtFv.aspx.vb" Inherits="ARE_PROtFv" Title="PagoEfectivo - Otorgar Fecha Valuta"
    Culture='Auto' UICulture='Auto' %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page" class="colRight">
        <h1>
            <asp:Literal ID="lblTitulo" runat="server"></asp:Literal>&nbsp;
        </h1>
        <div id="divHistorialOrdenesPago" class="dlgrid">
            <div class="inner-col-right">
                <h4>
                    Otorgar Fecha Valuta</h4>
                <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="w100 clearfix dlinline">
                            <dl class="even clearfix dt15 dd60">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="lblTipo" runat="server" Text="Tipo Fecha:" __designer:wfdid="w24"></asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <asp:DropDownList ID="ddlTipoFechaValuta" runat="server" CssClass="clsnormal" __designer:wfdid="w25"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlTipoFechaValuta_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class=" clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="labelAgencia" runat="server" Text="Agencia:" __designer:wfdid="w26"></asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <label>
                                        <asp:Literal ID="lblAgencia" runat="server" __designer:wfdid="w27"></asp:Literal></label>
                                    <label>
                                        <asp:Literal ID="lblIdAgenciaRecaudadora" runat="server" __designer:wfdid="w28" Visible="False"></asp:Literal></label>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class="even clearfix dt15 dd60">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="labelAgente" runat="server" Text="Agente:" __designer:wfdid="w29"></asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <asp:DropDownList ID="ddlAgente" runat="server" CssClass="clsnormal" __designer:wfdid="w30"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvAgente" runat="server" __designer:wfdid="w31"
                                        ErrorMessage="Seleccione un Agente" ControlToValidate="ddlAgente">*</asp:RequiredFieldValidator>
                                </dd>
                                <label>
                                    <asp:Literal ID="lblNombreAgente" runat="server" __designer:wfdid="w32" Visible="False"></asp:Literal></label>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class=" clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        <asp:Literal ID="labelFechaApertura" runat="server" Text="Fecha Apertura:" __designer:wfdid="w37"></asp:Literal></label>
                                </dt>
                                <dd class="camp">
                                    <label>
                                        <asp:Literal ID="lblFechaApertura" runat="server" __designer:wfdid="w38"></asp:Literal></label>
                                    <asp:TextBox ID="txtFechaApertura" runat="server" CssClass="miniCamp" Width="90px"
                                        __designer:wfdid="w39"></asp:TextBox>
                                    <asp:ImageButton ID="ibtnFechaValuta" runat="server" __designer:wfdid="w40"
                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/date.gif"%>'>
                                    </asp:ImageButton>
                                </dd>
                            </dl>
                        </div>
                        <div id="fiscaja" runat="server">
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            <asp:Literal ID="labelCaja" runat="server" Text="Caja:" __designer:wfdid="w33"></asp:Literal></label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblCajaPendiente" runat="server" __designer:wfdid="w34"></asp:Literal></label>
                                    </dd>
                                    <asp:Label ID="lblIdCaja" runat="server" Text="" __designer:wfdid="w35" Visible="false"></asp:Label>
                                    <asp:Label ID="lblIdAgenciaCaja" runat="server" __designer:wfdid="w36" Visible="False"></asp:Label>
                                </dl>
                            </div>
                        </div>
                        <cc1:CalendarExtender ID="cexFechaValuta" runat="server" __designer:wfdid="w41" TargetControlID="txtFechaApertura"
                            PopupButtonID="ibtnFechaValuta" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditExtender ID="mexFechaValuta" runat="server" __designer:wfdid="w42"
                            TargetControlID="txtFechaApertura" Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                        </cc1:MaskedEditExtender>
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" __designer:wfdid="w43">
                        </asp:ValidationSummary>
                        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeValidacion" __designer:wfdid="w44"
                            Height="26px"></asp:Label>&nbsp;
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlTipoFechaValuta" EventName="SelectedIndexChanged">
                        </asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="btnOtorgar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
				<div id="fsBotonera" class="clearfix dlinline btns3" >
					<asp:Button ID="btnOtorgar" runat="server" CssClass="btnOtorgar" Text="Otorgar" />
				</div>
            </div>
        </div>
    </div>
</asp:Content>