Imports System.Data
Imports System.Collections.Generic
Imports SPE.Entidades

Partial Class ARE_PROtFv
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            '
            CargarTipoFechaValuta()
            CargarAgenciaRecaudadora()
            MensajeTitulo()

            txtFechaApertura.Text = Date.Now
            ddlTipoFechaValuta.SelectedIndex = 0
            CargarAgenteApertura()
            '
        End If
    End Sub

    Private Sub CargarTipoFechaValuta()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoFechaValuta, objCParametro.ConsultarParametroPorCodigoGrupo("TFEV"), "Descripcion", "Id")
        '
    End Sub

    Private Sub CargarAgenciaRecaudadora()
        '
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgente As New SPE.Entidades.BEAgente
        obeAgente = objCAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        lblAgencia.Text = obeAgente.DescAgenciaRecaudadora
        lblIdAgenciaRecaudadora.Text = obeAgente.IdAgenciaRecaudadora.ToString()
        '
    End Sub

#Region "Region ddlTipoFechaValuta"
    '
    Protected Sub ddlTipoFechaValuta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        If ddlTipoFechaValuta.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.TipoFechaValuta.Apertura Then
            CargarAgenteApertura()
        Else
            CargarAgenteCierre()
        End If
        '
        MensajeLimpio()
        '
    End Sub

    Private Sub CargarAgenteApertura()
        '
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        '
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlAgente, objCAgenciaRecaudadora.ConsultarAgentesConCajaPendientesdeCierre(ddlTipoFechaValuta.SelectedValue, Convert.ToInt32(lblIdAgenciaRecaudadora.Text)), "NombresApellidos", "IdAgente", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        '
        fisCaja.Visible = False
        lblCajaPendiente.Visible = False
        lblFechaApertura.Visible = False
        txtFechaApertura.Visible = True
        ibtnFechaValuta.Visible = True
        '
        ValidarEnabledOtorgar()
        '
    End Sub

    Private Sub CargarAgenteCierre()
        '
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlAgente, objCAgenciaRecaudadora.ConsultarAgentesConCajaPendientesdeCierre(ddlTipoFechaValuta.SelectedValue, Convert.ToInt32(lblIdAgenciaRecaudadora.Text)), "NombresApellidos", "IdAgente", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        '
        fisCaja.Visible = True
        lblCajaPendiente.Visible = True
        lblFechaApertura.Visible = True
        txtFechaApertura.Visible = False
        ibtnFechaValuta.Visible = False
        '
        ValidarEnabledOtorgar()
        '
    End Sub
    '
#End Region

#Region "Region ddlAgente"
    '
    Protected Sub ddlAgente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAgente.SelectedIndexChanged
        '
        If ddlTipoFechaValuta.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.TipoFechaValuta.Apertura Then
            CargarProcesoApertura()
        Else
            CargarProcesoCierre()
        End If
        '
        MensajeLimpio()
        '
    End Sub

    Private Sub CargarProcesoApertura()

    End Sub

    Private Sub CargarProcesoCierre()
        Dim s As String
        s = Convert.ToString(ddlAgente.SelectedValue).Trim
        If s <> "" Then

            Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgenteCaja As New SPE.Entidades.BEAgenteCaja
            obeAgenteCaja = objCAgenciaRecaudadora.ConsultarCajaActivaPorIdAgente(ddlAgente.SelectedValue)
            lblCajaPendiente.Text = obeAgenteCaja.NombreCaja
            lblFechaApertura.Text = obeAgenteCaja.FechaApertura.ToString("dd/MM/yyyy")
            lblIdCaja.Text = obeAgenteCaja.IdCaja.ToString
            lblIdAgenciaCaja.Text = obeAgenteCaja.IdAgenteCaja.ToString
            lblNombreAgente.Text = ddlAgente.SelectedItem.Text

        Else

            LimpiarCajas()
            MensajeLimpio()

        End If
    End Sub
    '
#End Region

#Region "Region Otorgar"
    '
    Protected Sub btnOtorgar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOtorgar.Click
        '
        lblMensaje.Text = ""
        '
        If ddlAgente.SelectedValue = "" Then
            rfvAgente.Validate()
            Exit Sub
        End If
        '
        If ddlTipoFechaValuta.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.TipoFechaValuta.Apertura Then
            If ValidarFecha() = True Then
                OtorgarFechaValutaApertura()
            End If
        Else
            OtorgarFechaValutaCierre()
        End If
        '
    End Sub

    Sub OtorgarFechaValutaApertura()

        Dim obeFechaValuta As New SPE.Entidades.BEFechaValuta
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun

        obeFechaValuta.IdTipoFechaValuta = ddlTipoFechaValuta.SelectedValue
        obeFechaValuta.Fecha = txtFechaApertura.Text
        obeFechaValuta.IdCaja = 0
        obeFechaValuta.IdAgente = ddlAgente.SelectedValue
        obeFechaValuta.IdUsuarioCreacion = UserInfo.IdUsuario
        obeFechaValuta.IdAgenteCaja = 0

        objCAgenciaRecaudadora.RegistrarFechaValuta(obeFechaValuta)
        lblMensaje.CssClass = "MensajeTransaccion"
        lblMensaje.Text = "La fecha valuta : " + txtFechaApertura.Text + " fue otorgada al Agente: " + ddlAgente.SelectedItem.Text

        'FALTA INHABILTAR LAS FECHAS VALUTAS ANTERIORES
        CargarAgenteApertura()
        txtFechaApertura.Text = objCAdministrarComun.ConsultarFechaHoraActual()

    End Sub

    Sub OtorgarFechaValutaCierre()

        Dim obeFechaValuta As New SPE.Entidades.BEFechaValuta
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora

        obeFechaValuta.IdTipoFechaValuta = ddlTipoFechaValuta.SelectedValue
        obeFechaValuta.Fecha = lblFechaApertura.Text
        obeFechaValuta.IdCaja = Convert.ToInt32(lblIdCaja.Text)
        obeFechaValuta.IdAgente = ddlAgente.SelectedValue
        obeFechaValuta.IdUsuarioCreacion = UserInfo.IdUsuario
        obeFechaValuta.IdAgenteCaja = Convert.ToInt32(lblIdAgenciaCaja.Text)
        
        objCAgenciaRecaudadora.RegistrarFechaValuta(obeFechaValuta)
        lblMensaje.CssClass = "MensajeTransaccion"
        lblMensaje.Text = "La fecha valuta: " + lblFechaApertura.Text + " fue otorgada al Agente: " + lblNombreAgente.Text

        LimpiarCajas()
        CargarAgenteCierre()

    End Sub
    '
#End Region

#Region "Region Funciones"

    Sub MensajeLimpio()
        lblMensaje.Text = ""
    End Sub

    Sub MensajeTitulo()
        lblTitulo.Text = "Otorgar Fecha Valuta"
    End Sub

    Sub LimpiarCajas()
        lblCajaPendiente.Text = ""
        lblFechaApertura.Text = ""
    End Sub

    Sub HabilitarBoton(ByVal valor As Boolean)
        btnOtorgar.Enabled = valor
    End Sub

    Sub ValidarEnabledOtorgar()
        If ddlAgente.Items.Count > 0 Then
            HabilitarBoton(True)
        Else
            HabilitarBoton(False)
        End If
    End Sub

    Function ValidarFecha() As Boolean
        If IsDate(txtFechaApertura.Text) Then
            If CDate(txtFechaApertura.Text).ToShortDateString > Date.Now.ToShortDateString Then
                lblMensaje.Text = "Fecha de apertura mayor a la fecha actual."
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
    End Function

#End Region


End Class
