<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRReOrPa.aspx.vb" Inherits="ARE_PRReOrPa" Title="PagoEfectivo - Recepcionar C�digo de Identificaci�n de Pago" %>

<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ PreviousPageType VirtualPath="~/ARE/COOrPa.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/JScripts/Safari3AjaxHack.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <asp:ScriptManager ID="ScriptManager" runat="server">
        <Scripts>
<%--            <asp:ScriptReference Path='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/JScripts/Safari3AjaxHack.js" %>' />--%>
        </Scripts>
    </asp:ScriptManager>
    <asp:Panel ID="pnlPage" runat="server" class="inner-col-right">
        <h1>
            <label>
                <asp:Literal ID="lblMensajeOrdenPago" runat="server"></asp:Literal>
            </label>
        </h1>
        <div class="dlgrid">
            <h3>
                1. B&uacute;squeda por n&uacute;mero</h3>
            <asp:UpdatePanel ID="UpdatePanelBus" runat="server" class="even w100 clearfix dlinline">
                <ContentTemplate>
                    <dl class="clearfix dt15 dd70">
                        <dt class="desc">
                            <label>
                                C.I.P.:</label>
                        </dt>
                        <dd class="camp">
                            &nbsp;<asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="miniCamp izq" OnTextChanged="txtNroOrdenPago_TextChanged"
                                MaxLength="14"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" runat="server" ValidChars="0123456789"
                                TargetControlID="txtNroOrdenPago">
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="ibtnBuscar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/lupa.gif" %>' ToolTip="Buscar Orden Pago" />
                            <asp:ImageButton ID="ibtnBusquedaAvanzada" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Images_Buton/documento.jpg" %>'
                                ToolTip="B�squeda Avanzada" />
                        </dd>
                    </dl>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Label ID="lblIdOrdenPago" runat="server" Text="0" Visible="False"></asp:Label>
            <asp:Label ID="lblIdAgenteCaja" runat="server" Text="0" Visible="False"></asp:Label>
            <asp:Label ID="lblNombreCaja" runat="server" Text="" Visible="False"></asp:Label>
            <asp:Label ID="lblUbigeo" runat="server" Text="" Visible="False"></asp:Label>
            <fieldset id="Fieldset1" class="ContenedorBotonera">
                <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar" Text="Buscar" Visible="False" />
            </fieldset>
            &nbsp;
        </div>
        <asp:UpdatePanel ID="UpdatePanel" runat="server">
            <ContentTemplate>
                <div id="divDatos2" class="dlgrid" runat="server" visible="false">
                    <h3>
                        2. Detalle de ord en de pago
                    </h3>
                    <div id="divHistorialOrdenesPagoSub" class="dlgrid">
                        <div id="divCriteriosBusqueda">
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Cliente:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblDescCliente" runat="server"></asp:Literal>
                                        </label>
                                        <label>
                                            <asp:Literal ID="lblMailCliente" runat="server" Visible="False"></asp:Literal>
                                        </label>
                                        <label>
                                            <asp:Literal ID="lblFechaValuta" runat="server" Visible="False"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <fieldset id="fisVerEmpresa" class="w100 clearfix dlinline" runat="server">
                                <div class="w100 clearfix dlinline">
                                    <dl class="even clearfix dt15 dd30">
                                        <dt class="desc">
                                            <label>
                                                Empresa:</label>
                                        </dt>
                                        <dd class="camp">
                                            <asp:Image ID="imgEmpresa" runat="server" Visible="False"></asp:Image>
                                            <label>
                                                <asp:Literal ID="lblDescEmpresa" runat="server" Visible="False"></asp:Literal>
                                            </label>
                                        </dd>
                                    </dl>
                                </div>
                            </fieldset>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Servicio:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:Image ID="imgServicio" runat="server" Visible="False"></asp:Image>
                                        <label>
                                            <asp:Literal ID="lblDescServicio" runat="server" Visible="False"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            C.I.P.:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblNroOrdenPago" runat="server"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Concepto:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label>
                                            <asp:Literal ID="lblConcepto" runat="server"></asp:Literal>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt15 dd30">
                                    <dt class="desc">
                                        <label>
                                            Total:</label>
                                    </dt>
                                    <dd class="camp">
                                        <label width="30">
                                            <asp:Literal ID="lblSimbolo" runat="server"></asp:Literal>
                                        </label>
                                        <label>
                                            <asp:Literal ID="lblTotal" runat="server"></asp:Literal>
                                        </label>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divDatos3" class="dlgrid" runat="server" visible="false">
                    <h3>
                        3. Modo de pago</h3>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    Moneda:</label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="clsNormal" Enabled="False">
                                </asp:DropDownList>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class="clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    Medio de pago:</label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList ID="ddlMedioPago" runat="server" CssClass="clsNormal" AutoPostBack="True">
                                </asp:DropDownList>
                            </dd>
                        </dl>
                    </div>
                    <fieldset id="fisTarjeta0" class="even w100 clearfix dlinline" runat="server" visible="false">
                        <div id="divTipoTarjeta" class="even w100 clearfix dlinline">
                            <dl class="even clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        Tipo Tarjeta:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:DropDownList ID="ddlTarjeta" runat="server" CssClass="clsNormal" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvTarjeta" runat="server" ErrorMessage="Seleccione un tipo de tarjeta"
                                        ControlToValidate="ddlTarjeta" ValidationGroup="IsNullOrEmpty">*</asp:RequiredFieldValidator>
                                </dd>
                            </dl>
                        </div>
                    </fieldset>
                    <fieldset id="fisTarjeta1" class="w100 clearfix dlinline" runat="server" visible="false">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        Nro tarjeta:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtNroTarjeta" runat="server" CssClass="miniCamp" MaxLength="20"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvNroTarjeta" runat="server" ToolTip="Dato necesario"
                                        ControlToValidate="txtNroTarjeta" ErrorMessage="Ingrese el nro de tarjeta" ValidationGroup="IsNullOrEmpty">*</asp:RequiredFieldValidator>
                                    <asp:Label ID="lblMensNroTar" runat="server" ForeColor="Red" Text="Min 6 d�gitos."
                                        Visible="False"></asp:Label>
                                </dd>
                            </dl>
                        </div>
                        <div class="even w100 clearfix dlinline">
                            <dl class="even clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        Fecha vcmto.:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtFechaVenc" runat="server" CssClass="miniCamp" MaxLength="7"></asp:TextBox>&nbsp;
                                    <asp:Label ID="lblMensFechaVenc" runat="server" ForeColor="Red" Text="Fecha no v�lida."
                                        Visible="False"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfvFechaVenc" runat="server" ToolTip="Dato necesario"
                                        ControlToValidate="txtFechaVenc" ErrorMessage="Ingrese la fecha de vencimiento"
                                        ValidationGroup="IsNullOrEmpty">*</asp:RequiredFieldValidator>
                        </div>
                        </dd> </dl>
                    </fieldset>
                    <fieldset id="fisTarjeta2" class="w100 clearfix dlinline" runat="server" visible="false">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt15 dd30">
                                <dt class="desc">
                                    <label>
                                        Lote:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtLote" runat="server" CssClass="miniCamp" MaxLength="20"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvLote" runat="server" ToolTip="Dato necesario"
                                        ControlToValidate="txtLote" ErrorMessage="Ingrese el lote" ValidationGroup="IsNullOrEmpty">*</asp:RequiredFieldValidator>
                                </dd>
                            </dl>
                        </div>
                    </fieldset>
                    <cc1:MaskedEditExtender ID="MaskedEditExtender" runat="server" Mask="99/9999" TargetControlID="txtFechaVenc"
                        ClearMaskOnLostFocus="False">
                    </cc1:MaskedEditExtender>
                    &nbsp;
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderNumero" runat="server" FilterType="Numbers"
                        TargetControlID="txtNroTarjeta">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtenderLote" runat="server" FilterType="Numbers"
                        TargetControlID="txtLote">
                    </cc1:FilteredTextBoxExtender>
                    <fieldset id="Fieldset2" class="ContenedorBotonera">
                        &nbsp;</fieldset>
                    &nbsp; &nbsp;
                </div>
                <asp:ValidationSummary ID="vsmError" runat="server" ValidationGroup="IsNullOrEmpty">
                </asp:ValidationSummary>
                &nbsp;
                <asp:Label ID="lblTransaccion" runat="server" Visible="False" CssClass="MensajeTransaccion"
                    Width="500px"></asp:Label>
                <fieldset id="Fieldset3" class="ContenedorBotonera" style="width: 400px;">
                    <asp:Button ID="btnRealizarPago" runat="server" Text="Realizar Pago" CssClass="btnReaPago"
                        ValidationGroup="IsNullOrEmpty"></asp:Button>
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btnCancelar"
                        OnClientClick="return CancelMe();"></asp:Button>
                    <asp:Button ID="btnImprimir" runat="server" Text="Excel" CssClass="btnExcel" Enabled="False"
                        Width="0px"></asp:Button>
                </fieldset>
                &nbsp;
                <cc1:ModalPopupExtender ID="mppOrdenPago" runat="server" BackgroundCssClass="modalBackground"
                    OkControlID="mppOrdenPagoOK" CancelControlID="imgbtnRegresarPopPup" PopupControlID="pnlPopup"
                    TargetControlID="btnImprimir">
                </cc1:ModalPopupExtender>
                &nbsp;
                <asp:Button runat="server" ID="mppOrdenPagoOK" Style="display: none;" />
                <asp:Button runat="server" ID="mppOrdenPagoFinish" Style="display: none;" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ibtnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnRealizarPago" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="imgbtnRegresarPopPup" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <cc2:ModalUpdateProgress ID="ModalUpdateProgress1" runat="server" DisplayAfter="0"
            BackgroundCssClass="modalProgressGreyBackground">
            <ProgressTemplate>
                <div style="font-size: 12px; color: Black; width: 170px; font-weight: bold;">
                    Procesando... &nbsp;
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/progress-running.gif" alt="Cargando" />
                </div>
            </ProgressTemplate>
        </cc2:ModalUpdateProgress>
    </asp:Panel>
    <asp:Panel Style="display: none" ID="pnlPopup" runat="server">
        <div id="divPopPup" class="divContenedor" style="left: 0px; top: 0px; text-align: center;
            width: 600px;">
            <div id="divContenedorTituloPopPup" class="divContenedorTitulo">
                <div style="float: left">
                    Informe</div>
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresarPopPup" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/closeX.GIF" %>'
                        CausesValidation="false"></asp:ImageButton>
                </div>
            </div>
            <fieldset class="ContenedorEtiquetaTexto">
                <div id="div14" class="EtiquetaTextoIzquierda">
                    <br />
                    <asp:Label ID="lblMensaje" runat="server" CssClass="Etiqueta" Text="Se cancel� satisfactoriamente el C�digo de Identificaci�n de Pago. �Desea generar la impresi�n?"
                        Style="left: 0px; text-align: center;" Width="400px"></asp:Label>
                </div>
            </fieldset>
            <div id="PagePopPup">
                <asp:Button ID="btnVistaPreviaImpresion" runat="server" CssClass="Boton" Text="Si" />
                <asp:Button ID="btnCerrarPopPup" runat="server" CssClass="Boton" Text="No" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
