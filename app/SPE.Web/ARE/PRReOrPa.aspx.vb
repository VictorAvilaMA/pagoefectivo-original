Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Class ARE_PRReOrPa
    Inherits _3Dev.FW.Web.PageBase

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '          

            btnRealizarPago.Enabled = False
            If VerificarEstadoCaja() = True Then
                If Not Page.PreviousPage Is Nothing AndAlso _
                    Not Context.Items.Item("NumeroOrdenPago") Is Nothing Then 'CONSULTARCódigo de Identificación de Pago
                    txtNroOrdenPago.Text = Context.Items.Item("NumeroOrdenPago").ToString()
                    CargarDatosOrdenPago()
                ElseIf Not Session("NumeroOPPgPr") Is Nothing Then
                    txtNroOrdenPago.Text = Session("NumeroOPPgPr").ToString()
                    CargarDatosOrdenPago()
                    Session.Remove("NumeroOPPgPr")
                End If
            End If
            '
        End If
        '
    End Sub

    'VERIFICAMOS EL ESTADO DE LA CAJA
    Public Function VerificarEstadoCaja() As Boolean
        '
        Dim value As Boolean

        Dim CAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim ValidacionEstadoCaja(3)() As String
        ValidacionEstadoCaja = CAdministrarAgenciaRecaudadora.ValidarEstadoCaja("RecepcionarOrdenPago", UserInfo.IdUsuario)
        '
        If ValidacionEstadoCaja(0)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.Aperturado Then
            '
            If ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaActual Then
                CargarCombos()
                lblIdAgenteCaja.Text = ValidacionEstadoCaja(2)(2)
                lblNombreCaja.Text = ValidacionEstadoCaja(3)(0)
                value = True
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaValuta Then
                CargarCombos()
                lblIdAgenteCaja.Text = ValidacionEstadoCaja(2)(2)
                lblNombreCaja.Text = ValidacionEstadoCaja(3)(0)
                lblFechaValuta.Text = ValidacionEstadoCaja(0)(2)
                value = True
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaActual OrElse _
                ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaValuta Then
                pnlPage.Enabled = False
                value = False
            End If
            '
            lblMensajeOrdenPago.Text = ValidacionEstadoCaja(1)(1)
            '
        Else
            lblMensajeOrdenPago.Text = ValidacionEstadoCaja(0)(1)
            pnlPage.Enabled = False
            value = False
        End If
        '
        Return value
        '
    End Function

    'BOTON DE BUSCAR
    Protected Sub ibtnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnBuscar.Click
        '
        CargarDatosOrdenPago()
        '
    End Sub

    'LISTAR ORDENES DE PAGO
    Private Sub CargarDatosOrdenPago()
        '
        'SETEO DE PARAMETROS
        With TraerOrdenPago()

            If .IdOrdenPago = 0 Then 'NO HAYCódigo de Identificación de Pago
                '
                Limpiar()
                divDatos2.Visible = False
                divDatos3.Visible = False
                lblTransaccion.Visible = True
                lblTransaccion.CssClass = "MensajeTransaccion"
                lblTransaccion.Text = "El Código de Identificación de Pago (CIP) no fue encontrado."
                '
            Else 'CARGAR DATOS DECódigo de Identificación de Pago
                '
                lblTransaccion.Visible = False
                divDatos2.Visible = True
                divDatos3.Visible = True
                '
                lblIdOrdenPago.Text = .IdOrdenPago.ToString
                lblDescCliente.Text = .DescripcionCliente.ToString
                lblMailCliente.Text = .MailComercio.ToString
                lblDescServicio.Text = .DescripcionServicio
                '
                If .LogoServicio.Length = 0 Then
                    lblDescServicio.Visible = True
                Else
                    imgServicio.ImageUrl = "../ADM/ADServImg.aspx?servicioid=" + .IdServicio.ToString
                    imgServicio.AlternateText = .DescripcionServicio
                    imgServicio.Visible = True
                End If
                lblDescEmpresa.Text = .DescripcionEmpresa
                If .LogoEmpresa.Length = 0 Then
                    lblDescEmpresa.Visible = True
                Else
                    imgEmpresa.ImageUrl = "../ADM/ADEmCoImg.aspx?empresaId=" + .IdEmpresa.ToString
                    imgEmpresa.AlternateText = .DescripcionEmpresa
                    imgEmpresa.Visible = True
                End If

                '
                If .IdServicio > 0 Then

                    Dim objCEmpresaContratante As New SPE.Web.CEmpresaContratante
                    Dim obeOcultarEmpresa As New BEOcultarEmpresa
                    obeOcultarEmpresa = objCEmpresaContratante.OcultarEmpresa(.IdServicio)
                    If Not obeOcultarEmpresa Is Nothing Then

                        fisVerEmpresa.Visible = Not obeOcultarEmpresa.OcultarEmpresa
                        imgEmpresa.Visible = Not obeOcultarEmpresa.OcultarImagenEmpresa
                        lblDescEmpresa.Text = obeOcultarEmpresa.EmpresaContratanteDescripcion
                        lblDescEmpresa.Visible = obeOcultarEmpresa.OcultarImagenEmpresa
                        imgServicio.Visible = Not obeOcultarEmpresa.OcultarImagenServicio
                        lblDescServicio.Text = obeOcultarEmpresa.ServicioDescripcion
                        lblDescServicio.Visible = obeOcultarEmpresa.OcultarImagenServicio


                    End If

                End If
                '

                lblNroOrdenPago.Text = .NumeroOrdenPago.ToString
                lblConcepto.Text = .ConceptoPago.ToString
                lblSimbolo.Text = .SimboloMoneda
                lblTotal.Text = .Total.ToString("#,#0.00")
                '
                ddlMoneda.SelectedValue = .IdMoneda
                '
                lblTransaccion.CssClass = "MensajeTransaccion"
                lblTransaccion.Text = ""
                '
                btnRealizarPago.Enabled = True
                If Not (.PermiteOCAgenciaRecaudadora) Then
                    btnRealizarPago.Enabled = False
                    lblTransaccion.Visible = True
                    lblTransaccion.CssClass = "MensajeValidacion"
                    lblTransaccion.Text = "El Código de Identificación de Pago no tiene los permisos para ser cancelada desde una agencia recaudadora."
                Else
                    btnRealizarPago.Enabled = True
                    lblTransaccion.Visible = False
                    lblTransaccion.Text = ""
                End If
            End If
        End With
        '
    End Sub

    'Traer Código de Identificación de Pago
    Private Function TraerOrdenPago() As BEOrdenPago

        'CARGA DE Código de Identificación de Pago
        Dim objCAdministrarOrdenPago As New SPE.Web.COrdenPago
        Dim obeOrdenPago As New BEOrdenPago

        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text
        obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Generada
        obeOrdenPago = objCAdministrarOrdenPago.ConsultarOrdenPagoRecepcionar(obeOrdenPago)


        Return obeOrdenPago

    End Function

    'REALIZAR PAGO DE LA Código de Identificación de Pago
    Protected Sub btnRealizarPago_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRealizarPago.Click
        '
        Try
            lblTransaccion.Visible = True
            '
            'VALIDAMOS SI HAY UNA Código de Identificación de Pago
            If lblIdOrdenPago.Text = 0 Then
                '
                'NO HAY Código de Identificación de Pago
                lblTransaccion.CssClass = "MensajeTransaccion"
                lblTransaccion.Text = "No se encuentra registrada el Código de Identificación de Pago " & txtNroOrdenPago.Text.Trim() & "."
                '
            ElseIf TraerOrdenPago().IdOrdenPago = 0 Then
                '
                'Código de Identificación de Pago DIFERENTE A GENERADA
                lblTransaccion.CssClass = "MensajeTransaccion"
                'lblTransaccion.Text = "El Cod. Identif. Pago: " & txtNroOrdenPago.Text & ", tuvo una operación previa."
                lblTransaccion.Text = "El Cod. Identif. Pago: " & txtNroOrdenPago.Text.Trim() & ", tiene un estado distinto a generada."
                '
            Else
                '

                If fisTarjeta1.Visible = True Then

                    Try

                        'Dim strFechVen As String = ""
                        'Dim strAnio As Integer = _3Dev.FW.Util.DataUtil.ObjectToInt(txtFechaVenc.Text.Split("/")(1))
                        'Dim strFechVenc As Integer = _3Dev.FW.Util.DataUtil.ObjectToInt(txtFechaVenc.Text.Split("/")(0)) + 1
                        Dim dtFechaVen As Date = _3Dev.FW.Util.DataUtil.StringddmmyyyyToDate("01/" + txtFechaVenc.Text)

                        'If Not IsDate("01/" + txtFechaVenc.Text) Then
                        '    lblMensFechaVenc.Visible = True
                        '    Exit Sub
                        'Else

                        If dtFechaVen <= Date.Now Then
                            lblMensFechaVenc.Visible = True
                            Exit Sub
                        Else
                            lblMensFechaVenc.Visible = False
                        End If
                        'End If
                    Catch ex As Exception
                        lblMensFechaVenc.Visible = True
                        Exit Sub
                    End Try



                    If txtNroTarjeta.Text.Length < 6 Then
                        lblMensNroTar.Visible = True
                        Exit Sub
                    Else
                        lblMensNroTar.Visible = False
                    End If
                    '
                End If
                '
                Dim objCAdministrarOrdenPago As New SPE.Web.COrdenPago
                Dim obeDetalleTarjeta As New BEDetalleTarjeta
                Dim obeMovimiento As New BEMovimiento
                Dim obeOrdenPago As New BEOrdenPago

                'CARGAR DATOS DEL MOVIMIENTO
                '---------------------------
                obeMovimiento = CargarMovimiento(obeMovimiento)
                '
                'CARGAR DATOS PARA ACTUALIZAR EL ESTADO DE LA Código de Identificación de Pago
                '----------------------------------------------------------
                obeOrdenPago = CargarOrdenPago(obeOrdenPago, obeMovimiento)
                '
                'CARGAR DATOS DE LA TARJETA
                '--------------------------
                obeDetalleTarjeta = CargarDetalleTarjeta(obeDetalleTarjeta, obeMovimiento)
                '
                '************************************
                'PROCESO DE RECEPCIONAR Código de Identificación de Pago
                '************************************
                If Not objCAdministrarOrdenPago.ProcesoRecepcionarOrdenPago(obeMovimiento, obeOrdenPago, obeDetalleTarjeta) = 1 Then

                    lblTransaccion.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
                    Exit Sub
                End If

                'If objCAdministrarOrdenPago.RequestUrlServicio(obeOrdenPago, _
                '    SPE.EmsambladoComun.ParametrosSistema.TipoNotificacion.Recepcion, _
                '    SPE.EmsambladoComun.ParametrosSistema.ProcesoNotificacion.ValidarRequest) = 1 Then
                '    '
                '    If objCAdministrarOrdenPago.ProcesoRecepcionarOrdenPago(obeMovimiento, obeOrdenPago, obeDetalleTarjeta) = 1 Then
                '        '
                '        objCAdministrarOrdenPago.RequestUrlServicio(obeOrdenPago, _
                '            SPE.EmsambladoComun.ParametrosSistema.TipoNotificacion.Recepcion, _
                '            SPE.EmsambladoComun.ParametrosSistema.ProcesoNotificacion.ProcesarRequest)
                '        '
                '    Else
                '        lblTransaccion.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
                '        Exit Sub
                '    End If
                '    '
                'Else
                '    '
                '    lblTransaccion.Text = "** El servicio no se encuentra disponible para cancelar la Código de Identificación de Pago."
                '    Exit Sub
                '    '
                'End If
                '

                divDatos2.Visible = False
                divDatos3.Visible = False
                lblTransaccion.CssClass = "MensajeTransaccion"
                'btnRealizarPago.Enabled = False



                lblTransaccion.Text = "Se recepcionó correctamente el Código de Identificación de Pago Nro: ''" & lblNroOrdenPago.Text & "''. Se ha enviado un correo electrónico al cliente: " & lblDescCliente.Text & "."
                '
                'GENERAR VOUCHER DE CLIENTE
                mppOrdenPago.Show()
                '
                Limpiar()
                '
            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            lblTransaccion.CssClass = "MensajeValidacion"
            lblTransaccion.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
        End Try
        '
    End Sub

    'CARGAR MOVIMIENTO
    Private Function CargarMovimiento(ByVal obeMovimiento As BEMovimiento) As BEMovimiento
        '
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        '
        obeMovimiento.IdOrdenPago = Convert.ToInt32(lblIdOrdenPago.Text)
        obeMovimiento.IdAgenteCaja = Convert.ToInt32(lblIdAgenteCaja.Text)
        obeMovimiento.IdMoneda = ddlMoneda.SelectedValue
        obeMovimiento.IdTipoMovimiento = SPE.EmsambladoComun.ParametrosSistema.TipoMovimiento.RecepcionarOP
        obeMovimiento.IdMedioPago = ddlMedioPago.SelectedValue
        obeMovimiento.Monto = Convert.ToDecimal(lblTotal.Text)
        obeMovimiento.IdUsuarioCreacion = UserInfo.IdUsuario

        ''Lógica llevada al BL
        'If lblFechaValuta.Text = "" Then
        '    obeMovimiento.FechaMovimiento = objCAdministrarComun.ConsultarFechaHoraActual()
        'Else
        '    obeMovimiento.FechaMovimiento = Convert.ToDateTime(lblFechaValuta.Text)
        'End If

        '
        Return obeMovimiento
        '
    End Function

    'CARGARCódigo de Identificación de Pago
    Private Function CargarOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal obeMovimiento As BEMovimiento) As BEOrdenPago
        '
        obeOrdenPago.IdOrdenPago = obeMovimiento.IdOrdenPago
        obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada
        obeOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario

        'obeOrdenPago.FechaCancelacion = obeMovimiento.FechaMovimiento  Lógica llevada al BL
        obeOrdenPago.DescripcionEmpresa = lblDescEmpresa.Text
        obeOrdenPago.DescripcionServicio = lblDescServicio.Text
        obeOrdenPago.SimboloMoneda = lblSimbolo.Text
        obeOrdenPago.DescripcionCliente = lblMailCliente.Text 'PARA EL CORREO DE CLIENTE
        obeOrdenPago.NumeroOrdenPago = lblNroOrdenPago.Text
        obeOrdenPago.ConceptoPago = lblConcepto.Text
        obeOrdenPago.OcultarEmpresa = Not fisVerEmpresa.Visible
        obeOrdenPago.Total = Convert.ToDecimal(lblTotal.Text)
        '
        Return obeOrdenPago
        '
    End Function

    'CARGAR DETALLE TARJETA
    Private Function CargarDetalleTarjeta(ByVal obeDetalleTarjeta As BEDetalleTarjeta, ByVal obeMovimiento As BEMovimiento) As BEDetalleTarjeta
        '
        If ddlMedioPago.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.MedioPago.Tarjeta Then
            '
            obeDetalleTarjeta.IdTarjeta = Convert.ToInt32(ddlTarjeta.SelectedValue)
            obeDetalleTarjeta.Numero = txtNroTarjeta.Text
            obeDetalleTarjeta.FechaVencimiento = Convert.ToDateTime(txtFechaVenc.Text)
            obeDetalleTarjeta.Lote = txtLote.Text
            obeDetalleTarjeta.Bin = Left(txtNroTarjeta.Text, 4)
            obeDetalleTarjeta.IdMovimiento = obeMovimiento.IdMovimiento
            '
        Else
            '
            obeDetalleTarjeta.IdTarjeta = 0
            '
        End If
        '
        Return obeDetalleTarjeta
        '
    End Function

    'LIMPIAR
    Private Sub Limpiar()
        '
        '   lblIdOrdenPago.Text = "0"
        ddlMedioPago.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.MedioPago.Efectivo
        lblDescServicio.Visible = False
        imgServicio.Visible = False
        lblMensFechaVenc.Visible = False
        lblDescEmpresa.Visible = False
        imgEmpresa.Visible = False
        lblMensNroTar.Visible = False
        fisTarjeta0.Visible = False
        fisTarjeta1.Visible = False
        fisTarjeta2.Visible = False
        txtNroTarjeta.Text = ""
        txtLote.Text = ""
        txtFechaVenc.Text = ""
        btnRealizarPago.Enabled = False
        '
    End Sub

    'CARGAR COMBOS
    Private Sub CargarCombos()
        '
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        '
        ddlMoneda.DataTextField = "Descripcion" : ddlMoneda.DataValueField = "IdMoneda" : ddlMoneda.DataSource = objCAdministrarComun.ConsultarMoneda() : ddlMoneda.DataBind()
        '
        ddlMedioPago.DataTextField = "Descripcion" : ddlMedioPago.DataValueField = "IdMedioPago" : ddlMedioPago.DataSource = objCAdministrarComun.ConsultarMedioPago() : ddlMedioPago.DataBind()
        '
        ddlTarjeta.DataTextField = "Descripcion" : ddlTarjeta.DataValueField = "IdTarjeta" : ddlTarjeta.DataSource = objCAdministrarComun.ConsultarTarjeta() : ddlTarjeta.DataBind() : ddlTarjeta.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
        '
    End Sub

    'MEDIO DE PAGO
    Protected Sub ddlMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMedioPago.SelectedIndexChanged
        '
        If ddlMedioPago.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.MedioPago.Tarjeta Then
            ddlTarjeta.SelectedIndex = 0
            fisTarjeta0.Visible = True
            fisTarjeta1.Visible = True
            fisTarjeta2.Visible = True
        Else
            fisTarjeta0.Visible = False
            fisTarjeta1.Visible = False
            fisTarjeta2.Visible = False
        End If
        '
    End Sub

    'CANCELAR
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("../ARE/PgPrl.aspx")
        '
    End Sub

    'BSUQUEDA AVANZADA DECódigo de Identificación de Pago
    Protected Sub ibtnBusquedaAvanzada_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnBusquedaAvanzada.Click
        '
        Server.Transfer("~/ARE/COOrPa.aspx", True)
        '
    End Sub

    'VISTA PREVIA IMPRESION
    Protected Sub btnVistaPreviaImpresion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVistaPreviaImpresion.Click
        '
        Dim strUrlPopPup As New StringBuilder
        strUrlPopPup.Append("PRImReOrPaPopPup.aspx?NumeroOP=") : strUrlPopPup.Append(lblNroOrdenPago.Text)
        strUrlPopPup.Append("&Fecha=") : strUrlPopPup.Append(Date.Now.ToShortDateString) : strUrlPopPup.Append(" ") : strUrlPopPup.Append(Date.Now.ToShortTimeString)
        strUrlPopPup.Append("&Caja=") : strUrlPopPup.Append(lblNombreCaja.Text)
        strUrlPopPup.Append("&Agente=") : strUrlPopPup.Append(UserInfo.Nombres) : strUrlPopPup.Append(" ") : strUrlPopPup.Append(UserInfo.Apellidos)
        strUrlPopPup.Append("&Cliente=") : strUrlPopPup.Append(lblDescCliente.Text)
        strUrlPopPup.Append("&Empresa=") : strUrlPopPup.Append(lblDescEmpresa.Text)
        strUrlPopPup.Append("&Servicio=") : strUrlPopPup.Append(lblDescServicio.Text)
        strUrlPopPup.Append("&IdOrdenPago=") : strUrlPopPup.Append(lblIdOrdenPago.Text)
        strUrlPopPup.Append("&Concepto=") : strUrlPopPup.Append(lblConcepto.Text)
        strUrlPopPup.Append("&Total=") : strUrlPopPup.Append(lblSimbolo.Text) : strUrlPopPup.Append(" ") : strUrlPopPup.Append(lblTotal.Text)
        Dim popupScript As String = "<script language='JavaScript'>" & _
            "window.open('" & strUrlPopPup.ToString() & "', 'Vista_Previa_Impresión_Orden_De_Pago', " & _
            "'width=400, height=400, scrollbars=no,menubar=no, resizable=no')" & _
            "</script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", popupScript)
        '
    End Sub

    Protected Sub txtNroOrdenPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        If Right("00000000000000" + txtNroOrdenPago.Text, 14) = "00000000000000" Then
            txtNroOrdenPago.Text = ""
        Else
            txtNroOrdenPago.Text = Right("00000000000000" + txtNroOrdenPago.Text, 14)
        End If
        '
    End Sub


End Class
