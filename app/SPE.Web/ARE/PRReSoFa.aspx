<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRReSoFa.aspx.vb" Inherits="ARE_PRReSoFa" Title="PagoEfectivo - Registrar Sobrante / Faltante Caja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="pnlPage" runat="server" class="dlgrid">
                <h1>
                    <asp:Label ID="lblMensajeSoFa" runat="server"></asp:Label>
                </h1>
                <div id="divInfGeneral" class="inner-col-right">
                    <h4>
                        1. Informaci�n de caja activa de un Agente</h4>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd60">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="lbl1" runat="server" Text="Agente Recaudador:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList ID="ddlAgente" runat="server" CssClass="clsnormal" AutoPostBack="True">
                                </asp:DropDownList>
                            </dd>
                            <asp:Label ID="lblIdAgenteCaja" runat="server" Visible="False"></asp:Label>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class=" clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label4" runat="server" Text="Codigo Caja:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <label>
                                    <asp:Literal ID="lblCaja" runat="server"></asp:Literal></label>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label5" runat="server" Text="Fecha Apertura:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <label>
                                    <asp:Literal ID="lblFechaApertura" runat="server"></asp:Literal></label>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class=" clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label7" runat="server" Text="N� Transacciones:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <label>
                                    <asp:Literal ID="lblNroTransacciones" runat="server"></asp:Literal></label>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd30">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label8" runat="server" Text="N� Anuladas:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <label>
                                    <asp:Literal ID="lblNroAnuladas" runat="server"></asp:Literal></label>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="sep_a14">
                </div>
                <div id="divInfGeneral2" class="inner-col-right" runat="server">
                    <h4>
                        2. Ingreso de Importe</h4>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd60">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label3" runat="server" Text="Motivo:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList ID="ddlMotivo" runat="server" CssClass="clsnormal">
                                </asp:DropDownList>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class=" clearfix dt15 dd60">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label1" runat="server" Text="Medio de Pago:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList ID="ddlMedioPago" runat="server" CssClass="clsnormal">
                                </asp:DropDownList>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class="even clearfix dt15 dd60">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label2" runat="server" Text="Moneda:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="clsnormal">
                                </asp:DropDownList>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline">
                        <dl class="clearfix dt15 dd60">
                            <dt class="desc">
                                <label>
                                    <asp:Literal ID="Label6" runat="server" Text="Importe:"></asp:Literal></label>
                            </dt>
                            <dd class="camp">
                                <asp:TextBox ID="txtImporte" runat="server" CssClass="miniCamp" MaxLength="15"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtImporte"
                                    ErrorMessage="Ingrese el monto.">*</asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtImporte"
                                    ErrorMessage="Ingrese un monto positivo." MaximumValue="999999999999" MinimumValue="0"
                                    Type="Currency">*</asp:RangeValidator>
                                <asp:ValidationSummary ID="ValidationSummary" runat="server" />
                            </dd>
                        </dl>
                    </div>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender" runat="server" FilterType="Custom, Numbers"
                        TargetControlID="txtImporte" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </div>
                <div class="clear">
                </div>
                <div class="sep_a14">
                </div>
                <asp:Label ID="lblMensajeValidacion" runat="server" CssClass="MensajeValidacion"
                    Visible="False"></asp:Label>
                <asp:Label ID="lblMensajeConfirmacion" runat="server" CssClass="MensajeTransaccion"
                    Visible="False"></asp:Label>
                <div id="fsBotonera" class="clearfix dlinline btns2">
                    <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" CssClass="btnRegistrar"
                        OnClientClick="return ConfirmReSoFa();" ValidationGroup="ValidarControles" />
                    <asp:Button ID="btnCancelar" OnClientClick="return CancelMe();" runat="server" Text="Cancelar"
                        CssClass="btnCancelar" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
