Imports System.Data
Imports System.Collections.Generic
Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Partial Class ARE_PRReSoFa
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            '
            If VerificarEstadoCaja() = True Then
                CargarAgentesPorAgencia()
                CargarCombos()
                ddlAgente_SelectedIndexChanged(sender, e)
            End If
        End If
        '
    End Sub

    'VERIFICAMOS EL ESTADO DE LA CAJA
    Public Function VerificarEstadoCaja() As Boolean
        '
        Return True
        '
    End Function

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Private Sub CargarAgentesPorAgencia()

        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlAgente, objCAgenciaRecaudadora.ConsultarAgentesPorAgencia(UserInfo.IdUsuario), "NombreAgente", "IdAgenteCaja", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    End Sub

    Protected Sub ddlAgente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAgente.SelectedIndexChanged
        CargarCajaAgente()
    End Sub

    Private Sub CargarCajaAgente()
        If (ddlAgente.SelectedValue <> "") Then
            Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeCaja As BEAgenteCaja = objCAgenciaRecaudadora.ConsultarCajaActivaPorAgente(ddlAgente.SelectedValue)
            lblCaja.Text = obeCaja.NombreCaja.ToString
            lblIdAgenteCaja.Text = obeCaja.IdAgenteCaja.ToString
            lblFechaApertura.Text = obeCaja.FechaApertura.ToShortDateString
            lblNroTransacciones.Text = obeCaja.NroOPRecibidas.ToString
            lblNroAnuladas.Text = obeCaja.NroOPAnuladas.ToString
            MostrarMensaje(False, "")
        Else
            'MostrarMensaje(True, "Seleccione un agente")
        End If
        btnRegistrar.Enabled = (ddlAgente.SelectedValue <> "")
        btnCancelar.Enabled = btnRegistrar.Enabled
        HabilitarIngresoImporte(btnRegistrar.Enabled)

    End Sub

    Private Sub HabilitarIngresoImporte(ByVal flag As Boolean)
        ddlMotivo.Enabled = flag
        ddlMedioPago.Enabled = flag
        ddlMoneda.Enabled = flag
        txtImporte.Enabled = flag
    End Sub


    Private Sub CargarCombos()
        Dim objCComun As New SPE.Web.CAdministrarComun
        Dim objCParametros As New SPE.Web.CAdministrarParametro

        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlMoneda, objCComun.ConsultarMoneda, "Descripcion", "IdMoneda")
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlMedioPago, objCComun.ConsultarMedioPago, "Descripcion", "IdMedioPago")
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlMotivo, objCParametros.ConsultarParametroPorCodigoGrupo("TMON"), "Descripcion", "Id")

    End Sub

    Private Sub IngresaSobranteFaltante()
        Dim objCAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim objMovimiento As New SPE.Entidades.BEMovimiento()

        objMovimiento.IdAgenteCaja = Convert.ToInt32(lblIdAgenteCaja.Text)
        objMovimiento.IdMoneda = ddlMoneda.SelectedValue
        objMovimiento.IdMedioPago = ddlMedioPago.SelectedValue
        objMovimiento.IdTipoMovimiento = ddlMotivo.SelectedValue
        If (ddlMotivo.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.TipoMovimiento.Sobrante) Then
            objMovimiento.Monto = Convert.ToDecimal(txtImporte.Text)
        Else
            objMovimiento.Monto = Convert.ToDecimal(txtImporte.Text)
        End If

        objMovimiento.IdUsuarioCreacion = UserInfo.IdUsuario

        objCAgenciaRecaudadora.RegistrarSobranteFaltante(objMovimiento)
    End Sub

    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        Try
            '
            If txtImporte.Text = "" Then
                RequiredFieldValidator1.Validate()
                Exit Sub
            End If
            If ValidarImporte() Then
                RangeValidator2.Validate()
                Exit Sub
            End If
            '
            IngresaSobranteFaltante()
            txtImporte.Text = ""
            MostrarMensaje(False, "El monto se registr� correctamente.")
        Catch ex As Exception
            MostrarMensaje(True, ex.Message.ToString)
        End Try
    End Sub

    Private Function ValidarImporte() As Boolean
        '
        If Not IsNumeric(txtImporte.Text) Then
            Return False
        End If
        '
    End Function

    Private Sub MostrarMensaje(ByVal esvalidacion As Boolean, ByVal mensaje As String)
        If (esvalidacion) Then
            lblMensajeConfirmacion.Text = ""
            lblMensajeValidacion.Text = mensaje
            lblMensajeValidacion.Visible = True
            lblMensajeConfirmacion.Visible = False
        Else
            lblMensajeConfirmacion.Text = mensaje
            lblMensajeValidacion.Text = ""
            lblMensajeConfirmacion.Visible = True
            lblMensajeValidacion.Visible = False
        End If
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("~/ARE/PgPrl.aspx")
        '
    End Sub
End Class
