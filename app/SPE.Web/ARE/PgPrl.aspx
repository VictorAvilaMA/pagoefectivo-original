<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PgPrl.aspx.vb" Inherits="ARE_PgPrl" Title="P�gina Principal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ OutputCache VaryByParam="None" Duration="5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page"  class="dlgrid">
        <div id="divInfGeneral" class="inner-col-right" style="left: 0px; top: 0px">
            <h3>
                1. Informaci&oacute;n de estado de caja</h3>
            <h2>
                <label>
                    <asp:Literal ID="lblMensajeEstadoCaja" runat="server"></asp:Literal>
                </label>
            </h2>
            <h2>
                <label>
                    <asp:Literal ID="lblNroTransacciones" runat="server"></asp:Literal>
                </label>
            </h2>
            <asp:Button ID="btnAperturarCaja" runat="server" CssClass="Boton" Text="Aperturar Caja"
                Visible="False" />
        </div>
        <div class="sep_a14">
        </div>
        <div id="divRecepcionar" class="inner-col-right" runat="server">
            <h3>
                2. Recepci&oacute;n de C&oacute;digo de Identificaci&oacute;n de Pago</h3>
            
                <asp:UpdatePanel ID="UpdatePanelBus" runat="server" class="even w100 clearfix dlinline">
                    <ContentTemplate>
                        <dl id="div11" class="clearfix dt15 dd60">
                            <dt class="desc">
                                <label>
                                    C.I.P:</label>
                            </dt>
                            <dd class="camp">
                                &nbsp;<asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="normalCamp izq" OnTextChanged="txtNroOrdenPago_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" ValidChars="0123456789" runat="server"
                                    TargetControlID="txtNroOrdenPago">
                                </cc1:FilteredTextBoxExtender>
                                <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar izq" Text="Buscar" />
                            </dd>
                        </dl>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <div style="float: left;">
                    <asp:UpdatePanel ID="UpdatePanelMens" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                        <label class="msje">
                            <asp:Literal ID="Label1" runat="server" Text="El C�digo de Identificaci�n de Pago no fue encontrada."
                                Visible="false"></asp:Literal>
                        </label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            
        </div>
    </div>
</asp:Content>
