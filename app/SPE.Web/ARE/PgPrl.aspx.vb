Imports System.Data
Imports System.Collections.Generic
Imports SPE.Entidades
Imports System.Web

Partial Class ARE_PgPrl
    Inherits _3Dev.FW.Web.PageBase

    'INIT
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    'LOAD
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            '
            'VALIDAMOS SI TIENE ABIERTA UNA CAJA
            VerificarEstadoCaja()
            '
        End If

    End Sub


    Public Sub VerificarEstadoCaja()
        '
        Dim CAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim ValidacionEstadoCaja(3)() As String
        ValidacionEstadoCaja = CAdministrarAgenciaRecaudadora.ValidarEstadoCaja("AperturarCaja", UserInfo.IdUsuario)
        '
        If ValidacionEstadoCaja(0)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.Aperturado Then
            '
            If ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaActual Then
                Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
                lblNroTransacciones.Text = "Nro de C.I.P. Recibidas: " & objCAdministrarAgenciaRecaudadora.ConsultaCajaActiva(UserInfo.IdUsuario).NroOPRecibidas.ToString
                divRecepcionar.Visible = True
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoConFechaValuta Then
                Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
                lblNroTransacciones.Text = "Nro de C.I.P. Recibidas: " & objCAdministrarAgenciaRecaudadora.ConsultaCajaActiva(UserInfo.IdUsuario).NroOPRecibidas.ToString
                divRecepcionar.Visible = True
            ElseIf ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaActual OrElse _
                ValidacionEstadoCaja(1)(0) = SPE.EmsambladoComun.ParametrosSistema.EstadoValidacionCaja.AperturadoPendienteCierrePorFechaValuta Then
                txtNroOrdenPago.Enabled = False
                btnBuscar.Enabled = False
            End If
            '
            lblMensajeEstadoCaja.Text = ValidacionEstadoCaja(1)(1)
            '
        Else
            lblMensajeEstadoCaja.Text = "Usted no tiene caja activa."
            btnAperturarCaja.Visible = True
            divRecepcionar.Visible = False
        End If
        '
    End Sub

    'VERIFICAR CAJA ABIERTA
    Private Function TraerlistCajaAperturada() As List(Of BEAgenteCaja)
        '
        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim obeAgenteCaja As New BEAgenteCaja
        obeAgenteCaja.IdUsuarioCreacion = UserInfo.IdUsuario
        obeAgenteCaja.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenteCaja.Aperturado
        Return objCAdministrarAgenciaRecaudadora.ConsultarAgenteCajaPorIdUsuarioIdEstado(obeAgenteCaja)
        '
    End Function

    Protected Sub btnAperturarCaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAperturarCaja.Click
        Response.Redirect("~/ARE/PRApCa.aspx")
    End Sub

    Protected Sub txtNroOrdenPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        If Right("00000000000000" + txtNroOrdenPago.Text, 14) = "00000000000000" Then
            txtNroOrdenPago.Text = ""
        Else
            txtNroOrdenPago.Text = Right("00000000000000" + txtNroOrdenPago.Text, 14)
        End If
        '
    End Sub

    'Buscamos Código de Identificación de Pago
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        '
        If TraerOrdenPago().IdOrdenPago = 0 Then
            Label1.Visible = True
        Else
            Session("NumeroOPPgPr") = Right("00000000000000" + txtNroOrdenPago.Text, 14)
            Response.Redirect("~/ARE/PRReOrPa.aspx")
        End If
        '
    End Sub

    'Traer Código de Identificación de Pago
    Private Function TraerOrdenPago() As BEOrdenPago

        'CARGA DE Código de Identificación de Pago
        Dim objCAdministrarOrdenPago As New SPE.Web.COrdenPago
        Dim obeOrdenPago As New BEOrdenPago

        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text
        obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Generada
        obeOrdenPago = objCAdministrarOrdenPago.ConsultarOrdenPagoRecepcionar(obeOrdenPago)

        Return obeOrdenPago

    End Function
End Class
