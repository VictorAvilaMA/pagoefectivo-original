Imports SPE.EmsambladoComun.ParametrosSistema
Partial Class ARE_general
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Context.User Is Nothing Then
            If Context.User.IsInRole(RolSupervisor) Then
                Response.Redirect("~/ARE/PgPrl.aspx")
            End If
        End If


    End Sub
End Class
