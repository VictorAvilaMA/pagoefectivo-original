Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Security

Namespace SPE.Web


    Public Class PaginaBase
        Inherits PageBase

        Public Shared Function UserInfoNombreOAlias() As String
            If (UserInfo.Alias = "") Then
                Return UserInfo.Nombres
            Else : Return UserInfo.Alias
            End If
        End Function


        Public Sub JSMessageAlert(tipo_alerta As String, mensaje As String, key As String)
            Dim script As String
            script = String.Format("alert('{0}: {1}');", tipo_alerta, mensaje)
            If Me.Master IsNot Nothing Then
                If Me.Master.AppRelativeVirtualPath.Contains("MasterPagePrincipal") Then
                    If Me.IsPostBack Then
                        script = String.Format("document.getElementById('errores').innerHTML = '{0}: {1}';", tipo_alerta, mensaje)
                        script += "document.getElementById('errores').style.display = 'block';"
                    Else
                        script = "window.onload = function(){"
                        script += String.Format("document.getElementById('errores').innerHTML = '{0}: {1}';", tipo_alerta, mensaje)
                        script += "document.getElementById('errores').style.display = 'block';"
                        script += "}"
                    End If
                End If
            End If
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
        End Sub


        Public Sub JSMessageAlertWithRedirect(tipo_alerta As String, mensaje As String, key As String, PageRedirect As String)
            Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", tipo_alerta, mensaje, PageRedirect)
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
        End Sub

    End Class
End Namespace