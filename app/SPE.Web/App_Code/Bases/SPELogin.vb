Imports System.Data
Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Imports SPE.Utilitario
Imports System.Collections.Generic
Imports System.Web.UI.Page
Imports System.IO
Imports System
Imports System.Web
Imports System.Web.UI.WebControls


Public Class SPELogin

    Private _login As Login
    Private _habilitaRedirect As Boolean


    Public ReadOnly Property Login() As Login
        Get
            Return _login
        End Get
    End Property

    Public Property HabilitaRedirect() As String
        Get
            Return _habilitaRedirect
        End Get
        Set(ByVal value As String)
            _habilitaRedirect = value
        End Set
    End Property



    Public Sub New(ByVal login As Login)
        _login = login
        _habilitaRedirect = False
        AsignarEventos()
    End Sub


    Public Sub AsignarEventos()
        AddHandler _login.LoggedIn, AddressOf Login_LoggedIn
        AddHandler _login.LoginError, AddressOf Login_LoginError
    End Sub

    Public Delegate Sub AfterLoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event AfterLoggedInEventHandler As AfterLoggedIn

    Protected Sub Login_LoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim pathCurrent As String
            'pathCurrent = HttpContext.Current.Server.MapPath("~/")
            'Dim ruta1, ruta2 As String
            'ruta1 = pathCurrent & "img\tempoload"
            'ruta2 = pathCurrent & "img\temposave"
            ''Dim raiz1 As New DirectoryInfo(ruta1)
            'Dim raiz2 As New DirectoryInfo(ruta2)
            ''DeleteFile(raiz1)
            'DeleteFile(raiz2)
            HttpContext.Current.Session.Remove("ImageIni")
            Dim ucliente As New SPE.Web.Seguridad.SPEUsuarioClient()
            LoadUserInfo(ucliente.GetUserInfoByUserName(CType(_login.FindControl("UserName"), TextBox).Text))
            RaiseEvent AfterLoggedInEventHandler(Me, e)
            HttpContext.Current.Session.Remove("SitemapCompleto")
            RegPC()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub RegPC()
        'Using oCComunVal As New SPE.Web.CComun()
        Dim oCComunVal As New SPE.Web.CComun()
        Dim oBEEquipoRegistradoXusuarioVal As New BEEquipoRegistradoXusuario()
        Dim oBEERXUResultado As New List(Of BEEquipoRegistradoXusuario)
        Dim Res As Boolean
        Dim FlagRegPc As Boolean
        Dim oBEEU As New BEEquipoRegistradoXusuario()
        oBEEU = CType(HttpContext.Current.Session("oBEEquipoRegistradoXusuarioVal"), BEEquipoRegistradoXusuario)

        If Not oBEEU Is Nothing Then
            oBEEquipoRegistradoXusuarioVal.Token = oBEEU.Token
            oBEEquipoRegistradoXusuarioVal.Usuario = oBEEU.Usuario
        End If
        oBEERXUResultado = oCComunVal.ValidarEquipoRegistradoXusuario(oBEEquipoRegistradoXusuarioVal)

        Res = oBEERXUResultado(0).Resultado
        FlagRegPc = oBEERXUResultado(0).FlagRegistarPc

        If FlagRegPc Then
            If oBEEquipoRegistradoXusuarioVal.Token.Length > 0 Then
                If Not Res Then
                    HttpContext.Current.Response.Redirect("PrRegDisp.aspx")
                End If
            End If
        End If
        'End Using
    End Sub

    Protected Sub Login_LoginError(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim beUsuario As New SPE.Web.Seguridad.SPEUsuarioClient()
        Dim Estado As Integer
        Dim Pass As String = ""
        Dim email As String = CType(_login.FindControl("UserName"), TextBox).Text
        Dim userInfo As _3Dev.FW.Entidades.Seguridad.BEUserInfo = beUsuario.GetUserInfoByUserName(email)
        Dim LoginErrorDetails As Literal = CType(_login.FindControl("TextoValidacion"), Literal)
        If Not userInfo Is Nothing Then
            Estado = userInfo.IdEstado
        End If
        If (email = "") Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Debe ingresar el Email."
        ElseIf userInfo Is Nothing Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. No existe un usuario con ese Email."
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta ha sido bloqueada. Debe contactarse con el Administrador."
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta est� en estado pendiente debido a que no se ha completado el proceso de registro."
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta ha sido deshabilitada. Debe contactarse con el Administrador."
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Verifique que su email y clave sean correctos."
        Else
            LoginErrorDetails.Text = _login.FailureText
        End If
        'HttpContext.Current.Items("LoginErrorDetails") = LoginErrorDetails.Text
        HttpContext.Current.Session("LoginErrorDetails") = LoginErrorDetails.Text
        If (_habilitaRedirect) Then
            HttpContext.Current.Response.Redirect("~/login.aspx")
        End If
    End Sub

    Protected Sub DeleteFile(ByVal directory As DirectoryInfo)
        For Each f As FileInfo In directory.GetFiles()
            If f.IsReadOnly Then
                f.Attributes = f.Attributes And Not IO.FileAttributes.ReadOnly
            End If
            If f.Extension.ToLower().Equals(".jpg") Then
                File.Delete(f.FullName)
            End If
        Next
    End Sub

End Class
