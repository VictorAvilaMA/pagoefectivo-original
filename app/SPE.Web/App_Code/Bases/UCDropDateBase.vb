Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization

Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Excepciones
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System


Public Class UCDropDateBase
    Inherits System.Web.UI.UserControl

    Public Sub New()
        '_scriptManagerId = "ScriptManager"
    End Sub
#Region "propiedades"
    'Public _scriptManagerId As String
    'Public Property ScriptManagerId() As String
    '    Get
    '        Return _scriptManagerId
    '    End Get
    '    Set(ByVal value As String)
    '        _scriptManagerId = value
    '    End Set
    'End Property

    Public ReadOnly Property FechaDia() As DropDownList
        Get
            Return _ddlFechaDias
        End Get
    End Property

    Public ReadOnly Property FechaMes() As DropDownList
        Get
            Return _ddlFechaMes
        End Get
    End Property

    Public ReadOnly Property FechaAnio() As DropDownList
        Get
            Return _ddlFechaAnio
        End Get
    End Property

#End Region

    Private _ddlFechaAnio As DropDownList
    Private _ddlFechaMes As DropDownList
    Private _ddlFechaDias As DropDownList

    Protected Sub Initialize(ByVal ddlFechaDias As DropDownList, ByVal ddlFechaMes As DropDownList, ByVal ddlFechaAnio As DropDownList)
        _ddlFechaAnio = ddlFechaAnio
        _ddlFechaMes = ddlFechaMes
        _ddlFechaDias = ddlFechaDias
    End Sub

    Public Function DameFechaSeleccionada() As Date
        Return New Date(_ddlFechaAnio.SelectedValue, _ddlFechaMes.SelectedValue, _ddlFechaDias.SelectedValue)
    End Function

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)

        MyBase.OnLoad(e)
        'If Not Page.IsPostBack Then
        '    GenerarAnio()
        '    GenerarMeses()
        '    GenerarDias()
        'End If
    End Sub

    Private Sub GenerarAnio()

        Dim anio As Integer = Now.Year
        _ddlFechaAnio.Items.Clear()
        _ddlFechaAnio.Items.Add(New ListItem("", ""))

        For i As Integer = 0 To 100
            _ddlFechaAnio.Items.Add(New ListItem(anio - 1, anio - 1))
            anio = anio - 1
        Next

    End Sub

    Private Sub GenerarMeses()
        _ddlFechaMes.Items.Clear()
        _ddlFechaMes.Items.Add(New ListItem("", ""))

        Dim cultureInfo As CultureInfo = cultureInfo.CreateSpecificCulture("es-PE")
        Dim meses As String() = cultureInfo.DateTimeFormat.MonthNames()

        For i As Integer = 0 To 11
            _ddlFechaMes.Items.Add(New ListItem(meses(i), i + 1))
        Next

    End Sub

    Private Sub GenerarDias()
        Dim diatemp As String = _ddlFechaDias.SelectedValue
        'Date.
        _ddlFechaDias.Items.Clear()
        _ddlFechaDias.Items.Add(New ListItem("", ""))
        Dim anio As String = _ddlFechaAnio.SelectedValue
        Dim mes As String = _ddlFechaMes.SelectedValue
        Dim dias As Integer = 31
        If Not ((anio = "") Or (mes = "")) Then
            dias = Date.DaysInMonth(anio, mes)
        End If

        For i As Integer = 1 To dias
            _ddlFechaDias.Items.Add(New ListItem(i, i))
        Next

        If Not _ddlFechaDias.Items.FindByValue(diatemp) Is Nothing Then
            _ddlFechaDias.SelectedValue = diatemp
        End If
    End Sub

    Public Function FechaEsValida() As Boolean
        Dim strfecha As Date
        Try
            strfecha = DameFechaSeleccionada()
        Catch ex As Exception
            Throw New FWBusinessException("El formato de la fecha no es valida", Nothing)
        End Try
        Return True
    End Function
    Public Sub RefrescarVista()
        GenerarAnio()
        GenerarMeses()
        GenerarDias()
    End Sub
    Public Sub RefrescarVista(ByVal fecha As Date)
        Try
            GenerarAnio()
            GenerarMeses()
            _ddlFechaAnio.SelectedValue = fecha.Year
            _ddlFechaMes.SelectedValue = fecha.Month
            GenerarDias()
            _ddlFechaDias.SelectedValue = fecha.Day
        Catch ex As Exception
            Throw ex
        End Try
    End Sub




    Protected Sub ddlFecha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        GenerarDias()
        Dim drop As DropDownList = CType(sender, DropDownList)
        ScriptManager.GetCurrent(Me.Page).SetFocus(drop)
    End Sub

End Class
