Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
'Imports _3Dev.FW.Web.Net
Imports _3Dev.FW.EmsambladoComun


Namespace SPE.Web
    Public Class CAdministrarAgenciaRecaudadora
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        'Public ReadOnly Property Conexion() As IAgenciaRecaudadora
        '    Get
        '        Dim a As New ProxyBase(Of IAgenciaRecaudadora)
        '        Return a.DevolverContrato(New EntityBaseContractResolver())
        '    End Get
        'End Property


        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property

        Public Function RegistrarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarAgenciaRecaudadora(obeAgenciaRecaudadora)
            End Using


        End Function

        Public Function ActualizarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarAgenciaRecaudadora(obeAgenciaRecaudadora)
            End Using
        End Function

        Public Function ConsultarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As List(Of BEAgenciaRecaudadora)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaRecaudadora(obeAgenciaRecaudadora)
            End Using
        End Function

        Public Function ConsultarAgenciaRecaudadoraPorIdAgencia(ByVal idAgencia As Integer) As BEAgenciaRecaudadora
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaRecaudadoraPorIdAgencia(idAgencia)
            End Using
        End Function

        Public Function ConsultarAgenciaRecaudadoraPorIdUsuario(ByVal idUsuario As Integer) As BEAgenciaRecaudadora
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaRecaudadoraPorIdUsuario(idUsuario)
            End Using
        End Function

        Public Function ConsultarAgente(ByVal obeAgente As BEAgente) As List(Of BEAgente)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgente(obeAgente)
            End Using
        End Function

        Public Function ConsultarAgentePorIdAgenteOrIdUsuario(ByVal opcion As Integer, ByVal idAgente As Integer) As BEAgente
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgentePorIdAgenteOrIdUsuario(opcion, idAgente)
            End Using
        End Function

        Public Function RegistrarAgente(ByVal obeAgente As BEAgente) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarAgente(obeAgente)
            End Using
        End Function

        Public Function ActualizarAgente(ByVal obeAgente As BEAgente) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarAgente(obeAgente)
            End Using
        End Function

        Public Function ConsultarCaja(ByVal obeCaja As BECaja) As List(Of BECaja)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCaja(obeCaja)
            End Using
        End Function

        Public Function ConsultarCajaAvanzada(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCajaAvanzada(obeAgenteCaja)
            End Using
        End Function

        Public Function RegistrarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarAgenteCaja(obeAgenteCaja)
            End Using
        End Function

        Public Function RegistrarMovimiento(ByVal obeMovimiento As BEMovimiento) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarMovimiento(obeMovimiento)
            End Using
        End Function

        Public Function ConsultarAgenteCajaPorIdUsuarioIdEstado(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenteCajaPorIdUsuarioIdEstado(obeAgenteCaja)
            End Using
        End Function

        Public Function ConsultaCajaActiva(ByVal idAgente As Integer) As Entidades.BEAgenteCaja
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultaCajaActiva(idAgente)
            End Using
        End Function

        Public Function ConsultaMontosPorAgenteCaja(ByVal idAgenteCaja As Integer, ByVal listCierre As Collection(Of BEMovimiento)) As Collection(Of BEMovimiento)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultaMontosPorAgenteCaja(idAgenteCaja, listCierre)
            End Using
        End Function

        Public Function LiquidarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).LiquidarAgenteCaja(obeAgenteCaja)
            End Using
        End Function

        Public Function ConsultarAgentesConCajaPendientesdeCierre(ByVal IdTipoFechaValuta As Integer, ByVal idAgencia As Integer) As List(Of BEAgente)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgentesConCajaPendientesdeCierre(IdTipoFechaValuta, idAgencia)
            End Using
        End Function

        Public Function ConsultarCajaActivaPorIdAgente(ByVal idAgente As Integer) As BEAgenteCaja
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCajaActivaPorIdAgente(idAgente)
            End Using
        End Function

        Public Function RegistrarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarFechaValuta(obeFechaValuta)
            End Using
        End Function

        Public Function ActualizarEstadoFechaValuta(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarEstadoFechaValuta(obeAgenteCaja)
            End Using
        End Function

        Public Function ActualizarEstadoFechaValutaPorIdAgente(ByVal obeFechaValuta As BEFechaValuta) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarEstadoFechaValutaPorIdAgente(obeFechaValuta)
            End Using
        End Function

        Function ConsultarFechaValutaAgenteCaja(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarFechaValutaAgenteCaja(obeFechaValuta)
            End Using
        End Function

        Public Function ConsultarFechaValutaPorAgente(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarFechaValutaPorAgente(obeFechaValuta)
            End Using
        End Function

        Public Function AnularAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).AnularAgenteCaja(obeAgenteCaja)
            End Using
        End Function

        Public Function ConsultarCajasCerradas(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCajasCerradas(obeAgenteCaja)
            End Using
        End Function

        Public Function LiquidarCajaBloque(ByVal obeOrdenPago As BEOrdenPago, ByVal listAgenteCaja As List(Of BEAgenteCaja)) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).LiquidarCajaEnBloque(obeOrdenPago, listAgenteCaja)
            End Using
        End Function

        Public Function ConsultarAgentesPorAgencia(ByVal idSupervisor As Integer) As List(Of BEAgenteCaja)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgentesPorAgencia(idSupervisor)
            End Using
        End Function

        Public Function ConsultarCajaActivaPorAgente(ByVal idAgente As Integer) As Entidades.BEAgenteCaja
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCajaActivaPorAgente(idAgente)
            End Using
        End Function

        Public Function RegistrarSobranteFaltante(ByVal obeMovimiento As Entidades.BEMovimiento) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarSobranteFaltante(obeMovimiento)
            End Using
        End Function

        Public Function ConsultarDetalleCaja(ByVal idAgenteCaja As Integer) As List(Of BEMovimiento)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleDeCaja(idAgenteCaja)
            End Using
        End Function

        Public Function ConsultarCajasLiquidadas(ByVal idUsuario As Integer, ByVal NroOrdenPago As String, ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As List(Of BEAgenteCaja)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCajasLiquidadas(idUsuario, NroOrdenPago, fechaInicio, fechaFin)
            End Using
        End Function

        Public Function ConsultarDetalleMovimientosCaja(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEMovimientoConsulta)
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleMovimientosCaja(obeAgenteCaja)
            End Using
        End Function

        Public Function ValidarEstadoCaja(ByVal form As String, ByVal idUsuario As Integer) As String()()
            Using Conexionsss As New ProxyBase(Of IAgenciaRecaudadora)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarEstadoCaja(form, idUsuario)
            End Using
        End Function

        


    End Class
End Namespace

