Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports System.Web
Imports _3Dev.FW.EmsambladoComun
Imports System

Namespace SPE.Web


    Public Class CAdministrarComun
        Implements IDisposable

        Public Const strConsMoneda As String = "strConsMoneda"
        Public Const strConsMedioPago As String = "strConsMedioPago"
        Public Const strConsTarjeta As String = "strConsTarjeta"
        Public Const strConsDetalleTarjeta As String = "strConsDetalleTarjeta"
        Public Const strConsMovimiento As String = "strConsMovimiento"

        Public Function ConsultarMoneda() As List(Of BEMoneda)
            If (HttpContext.Current.Cache(strConsMoneda) Is Nothing) Then
                Using Conexionsss As New ProxyBase(Of IComun)
                    HttpContext.Current.Cache(strConsMoneda) = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMoneda()
                End Using
            End If
            Return CType(HttpContext.Current.Cache(strConsMoneda), List(Of BEMoneda))
        End Function
        Public Function ConsultarMonedaPorId(ByVal parametro As Object) As BEMoneda
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMonedaPorId(parametro)
            End Using
        End Function
        Public Function ConsultarMedioPago() As List(Of BEMedioPago)
            If (HttpContext.Current.Cache(strConsMedioPago) Is Nothing) Then
                Using Conexionsss As New ProxyBase(Of IComun)
                    HttpContext.Current.Cache(strConsMedioPago) = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMedioPago()
                End Using
            End If
            Return CType(HttpContext.Current.Cache(strConsMedioPago), List(Of BEMedioPago))
        End Function

        Public Function ConsultarTarjeta() As List(Of BETarjeta)
            If (HttpContext.Current.Cache(strConsTarjeta) Is Nothing) Then
                Using Conexionsss As New ProxyBase(Of IComun)
                    HttpContext.Current.Cache(strConsTarjeta) = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTarjeta()
                End Using
            End If
            Return CType(HttpContext.Current.Cache(strConsTarjeta), List(Of BETarjeta))
        End Function

        Public Function RegistrarDetalleTarjeta(ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarDetalleTarjeta(obeDetalleTarjeta)
            End Using
        End Function

        Public Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As List(Of BEMovimiento)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMovimiento(obeMovimiento)
            End Using
        End Function
        Public Function ConsultarFechaHoraActual() As DateTime
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarFechaHoraActual()
            End Using
        End Function
        Function ConsultarTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTipoOrigenCancelacion()
            End Using
        End Function

        Function ConsultarPasarelaMedioPago() As List(Of BEPasarelaMedioPago)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPasarelaMedioPago
            End Using
        End Function

        Public Sub InicializarTodoCache()
            Using Conexionsss As New ProxyBase(Of IComun)
                Conexionsss.DevolverContrato(New EntityBaseContractResolver()).InicializarTodoCache()
            End Using
        End Sub

        Public Sub InicializarElementoCache(ByVal nombre As String)
            Using Conexionsss As New ProxyBase(Of IComun)
                Conexionsss.DevolverContrato(New EntityBaseContractResolver()).InicializarElementoCache(nombre)
            End Using
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace

