Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports System.Web
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class CAdministrarParametro
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function ConsultarParametroPorCodigoGrupo(ByVal CodigoGrupo As String) As List(Of BEParametro)

            If (CodigoGrupo = "COBM") Then

                Using Conexionsss As New ProxyBase(Of IParametro)
                    HttpContext.Current.Cache(CodigoGrupo) = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarParametroPorCodigoGrupo(CodigoGrupo)
                End Using

            Else

                If (HttpContext.Current.Cache(CodigoGrupo) Is Nothing Or CodigoGrupo = "TEXP") Then
                    Using Conexionsss As New ProxyBase(Of IParametro)
                        HttpContext.Current.Cache(CodigoGrupo) = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarParametroPorCodigoGrupo(CodigoGrupo)
                    End Using
                End If

            End If

            Return CType(HttpContext.Current.Cache(CodigoGrupo), List(Of BEParametro))

        End Function
        '
        Public Function ListarEstados() As List(Of BEParametro)
            Return ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoMantenimiento)
        End Function
        Public Function ListarTiposConsultaPagoServicios() As List(Of BEParametro)
            Return ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoTipoConsultaPG)
        End Function
        Public Function ListarVersionServicio() As List(Of BEParametro)
            Using Conexionsss As New ProxyBase(Of IParametro)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ListarVersionServicio
            End Using
        End Function
    End Class
End Namespace

