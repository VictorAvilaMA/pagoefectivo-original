Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports System.Web

Namespace SPE.Web
    Public Class CAdministrarUbigeo
        Public Const strConsPais As String = "strConsPais"
        Public Const strConsDepartamentoPorIdPais As String = "strConsDepartamentoPorIdPais"
        Public Const strConsCiudadPorIdDepartamento As String = "strConsCiudadPorIdDepartamento"

        Public Function ConsultarPais() As List(Of BEPais)
            If (HttpContext.Current.Cache(strConsPais) Is Nothing) Then
                Using Conexionsss As New ProxyBase(Of IUbigeo)
                    HttpContext.Current.Cache(strConsPais) = Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPais
                End Using
            End If
            Return CType(HttpContext.Current.Cache(strConsPais), List(Of BEPais))
        End Function

        Public Function ConsultarDepartamentoPorIdPais(ByVal IdPais As Integer) As List(Of BEDepartamento)
            Using Conexionsss As New ProxyBase(Of IUbigeo)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDepartamentoPorIdPais(IdPais)
            End Using
        End Function

        Public Function ConsultarCiudadPorIdDepartamento(ByVal IdDepartamento As Integer) As List(Of BECiudad)
            Using Conexionsss As New ProxyBase(Of IUbigeo)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCiudadPorIdDepartamento(IdDepartamento)
            End Using
        End Function
        '
    End Class
End Namespace

