Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.Web
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.EmsambladoComun
Imports _3Dev.FW.Entidades

Namespace SPE.Web

    Public Class CAgenciaBancaria
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                    Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                End Using
            End Get
        End Property
        Public ReadOnly Property oCAgenciaBancaria() As SPE.EmsambladoComun.IAgenciaBancaria
            Get
                Return RemoteServicesInterfaceObject
            End Get
        End Property

        Public Function RegistrarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarAgenciaBancaria(obeAgenciaBancaria)
            End Using
        End Function

        Public Function ActualizarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarAgenciaBancaria(obeAgenciaBancaria)
            End Using
        End Function

        Public Function ConsultarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As List(Of BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaBancaria(obeAgenciaBancaria)
            End Using
        End Function

        Public Function ConsultarAgenciaBancariaxCodigo(ByVal codAgenciaBancaria As String) As BEAgenciaBancaria
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaBancariaxCodigo(codAgenciaBancaria)
            End Using
        End Function

        Public Function ConsultarAgenciaBancariaxId(ByVal idAgenciaBancaria As Integer) As BEAgenciaBancaria
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAgenciaBancariaxId(idAgenciaBancaria)
            End Using
        End Function
        Public Function RegistrarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As BEConciliacion
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarConciliacionBancaria(obeConciliacion)
            End Using
        End Function
        Public Function ConsultarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria)
            Using Conexionsss As New ProxyBase(Of IAgenciaBancaria)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarConciliacionBancaria(obeConciliacion)
            End Using
        End Function
    End Class

End Namespace

