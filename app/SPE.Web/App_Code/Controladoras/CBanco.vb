Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CBanco
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function RegistrarBanco(ByVal oBEBanco As BEBanco) As Integer
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarBanco(oBEBanco)
            End Using
        End Function

        Public Function ConsultarBanco(ByVal oBEBanco As BEBanco) As List(Of BEBanco)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarBanco(oBEBanco)
            End Using
        End Function

        Public Function ConsultarCuentaBanco(ByVal oBECuentaBanco As BECuentaBanco) As List(Of BECuentaBanco)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaBanco(oBECuentaBanco)
            End Using
        End Function

        '<upd proc.Tesoreria>
        Public Function RegistrarDeposito(ByVal beDeposito As BEDeposito, ByVal detalleDeposito As List(Of BEOrdenPago)) As Integer
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarDeposito(beDeposito, detalleDeposito)
            End Using
        End Function
        Public Function ConsultarDepositoRegistroFactura(ByVal beDeposito As BEDeposito) As List(Of BEDeposito)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDepositoRegistroFactura(beDeposito)
            End Using
        End Function
        Public Function RegistrarFactura(ByVal beFactura As BEFactura, ByVal detalleDeposito As List(Of BEDeposito)) As Integer
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarFactura(beFactura, detalleDeposito)
            End Using
        End Function
        Public Function ConsultarDeposito(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDeposito(beDeposito)
            End Using
        End Function
        Public Function ConsultarDepositoDetalleByIdDeposito(ByVal request As BEDeposito) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDepositoDetalleByIdDeposito(request)
            End Using
        End Function
        Public Function ConsultarDepositoByIdFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDepositoByIdFactura(beDeposito)
            End Using
        End Function
        Public Function ConsultarFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarFactura(beDeposito)
            End Using
        End Function
        Public Function ConsultarDepositoReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDepositoReporte(beDeposito)
            End Using
        End Function
        Public Function ConsultarFacturaReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarFacturaReporte(beDeposito)
            End Using
        End Function
        Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenesConcArch(request)
            End Using
        End Function
        Public Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionArchivo)
            Using Conexionsss As New ProxyBase(Of IBanco)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarConciliacionArchivo(request)
            End Using
        End Function
    End Class
End Namespace

