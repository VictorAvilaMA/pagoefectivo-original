Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CCaja

        Public Function RegistrarCaja(ByVal obeCaja As BECaja) As Integer
            Using Conexionsss As New ProxyBase(Of ICaja)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarCaja(obeCaja)
            End Using
        End Function

        Public Function ActualizarCaja(ByVal obeCaja As BECaja) As Integer
            Using Conexionsss As New ProxyBase(Of ICaja)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarCaja(obeCaja)
            End Using
        End Function

        Public Function ConsultarCaja(ByVal idAgenciaRecaudadora As Integer) As List(Of BECaja)
            Using Conexionsss As New ProxyBase(Of ICaja)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCaja(idAgenciaRecaudadora)
            End Using
        End Function

        Public Function ObtenerAgenciaPorUsuario(ByVal idUsuario As Integer) As BEAgenciaRecaudadora
            Using Conexionsss As New ProxyBase(Of ICaja)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerAgenciaPorUsuario(idUsuario)
            End Using
        End Function

        Public Function OtorgarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer
            Using Conexionsss As New ProxyBase(Of ICaja)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).OtorgarFechaValuta(obeFechaValuta)
            End Using
        End Function

    End Class

End Namespace

