Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class CCliente
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property

        Public ReadOnly Property oCcliente() As SPE.EmsambladoComun.ICliente
            Get
                Return CType(RemoteServicesInterfaceObject, SPE.EmsambladoComun.ICliente)
            End Get
        End Property
        Public Function ActualizarEstadoUsuario(ByVal obeUsuario As _3Dev.FW.Entidades.Seguridad.BEUsuario) As Integer
            Return CType(RemoteServicesInterfaceObject, SPE.EmsambladoComun.ICliente).ActualizarEstadoUsuario(obeUsuario)
        End Function

        Public Function RegistrarClienteYPreOrdenCompra(ByVal becliente As SPE.Entidades.BECliente) As Integer
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ExecProcedureByParameters("RegisClientConPOP", becliente)
            End Using
        End Function

        Public Function ValidaConfirmacionDeUsuario(ByVal email As String, ByVal guid As String) As BECliente
            Dim obeCliente As New BECliente()
            obeCliente.Email = email
            obeCliente.CodigoRegistro = guid
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetBusinessEntityByParameters("ValidaConfirmacionDeUsuario", obeCliente)
            End Using
        End Function
        Public Function CambiarContraseņaUsuario(ByVal obeUsuario As BEUsuario) As Integer
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).CambiarContraseņaUsuario(obeUsuario)
            End Using
        End Function
        Public Function ValidarEmailUsuarioPorEstado(ByVal obeUsuario As BEUsuario) As BEUsuario
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarEmailUsuarioPorEstado(obeUsuario)
            End Using
        End Function
        Public Function GenerarPreordenPagoSuscriptores(ByVal xmlstring As String, ByVal urlservicio As String) As Integer
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPreordenPagoSuscriptores(xmlstring, urlservicio)
            End Using
        End Function

        Public Function ObtenerClientexIdUsuario(ByVal obeCliente As BECliente) As BECliente
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerClientexIdUsuario(obeCliente)
            End Using
        End Function
        Public Function ValidarTipoyNumeroDocPorEmail(ByVal obeUsuario As BEUsuario) As Integer
            Using Conexionsss As New ProxyBase(Of ICliente)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarTipoyNumeroDocPorEmail(obeUsuario)
            End Using
        End Function
    End Class
End Namespace
