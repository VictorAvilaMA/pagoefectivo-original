Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun


Namespace SPE.Web
    Public Class CComercio
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function GetComercioWithAO() As List(Of BEComercio)
            Using Conexionsss As New ProxyBase(Of IComercio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetComercioWithAO()
            End Using
        End Function
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IComercio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property


    End Class
End Namespace
