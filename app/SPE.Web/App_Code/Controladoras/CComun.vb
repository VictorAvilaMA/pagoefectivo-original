Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports System.Configuration
Imports System

Namespace SPE.Web

    Public Class CComun
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Implements IDisposable

        Public Function ConsultarTipoOrigenCancelacion() As List(Of BETipoOrigenCancelacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTipoOrigenCancelacion()
            End Using
        End Function

        Public Function ConsultarMedioPagoBancoByBanco(ByVal be As BEBanco) As List(Of BEMedioPagoBanco)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMedioPagoBancoByBanco(be)
            End Using
        End Function

        Public Function RegistrarPassLog(ByVal oPassLog As BELog) As Integer

            If (ConfigurationManager.AppSettings("HabilitarRegistroPasLog") = "1") Then
                Using Conexionsss As New ProxyBase(Of IComun)
                    Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistarPasLog(oPassLog)
                End Using
            End If

        End Function



        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

        '#Region " IDisposable Support "
        '        ' This code added by Visual Basic to correctly implement the disposable pattern.
        '        Public Sub Dispose() Implements IDisposable.Dispose
        '            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '            Dispose(True)
        '            GC.SuppressFinalize(Me)
        '        End Sub
        '#End Region

#Region "Log Navegacion"
        Public Function RegistrarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As Integer

            'If (ConfigurationManager.AppSettings("HabilitarRegistroPasLog") = "1") Then
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarLogNavegacion(obelognavegacion)
            End Using
            'End If

        End Function

        Public Function ConsultarLogNavegacion(ByVal obelognavegacion As BELogNavegacion) As List(Of BELogNavegacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarLogNavegacion(obelognavegacion)
            End Using
        End Function

#End Region

#Region "Equipo Registrado X Usuario"

        Public Function RegistrarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
            End Using
        End Function

        Public Function ValidarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
            End Using
        End Function

        Public Function ConsultarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As List(Of BEEquipoRegistradoXusuario)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
            End Using
        End Function

        Public Function EliminarEquipoRegistradoXusuario(ByVal obeequiporegistradoxusuario As BEEquipoRegistradoXusuario) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EliminarEquipoRegistradoXusuario(obeequiporegistradoxusuario)
            End Using
        End Function

#End Region

        '<add Peru.Com FaseIII>
#Region "Log Redirect"
        Public Function RegistrarLogRedirect(ByVal oBELogRedirect As BELogRedirect) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarLogRedirect(oBELogRedirect)
            End Using
        End Function
#End Region


#Region "Log Redirect Institición"
        Public Function RegistrarLogRedirectInst(ByVal oBELogRedirect As BELogRedirect) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarLogRedirect(oBELogRedirect)
            End Using
        End Function
#End Region

        Public Function RegistrarTokenCambioCorreo(ByVal be As BEUsuarioBase) As BEUsuarioBase
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarTokenCambioCorreo(be)
            End Using
        End Function

        Public Function ActualizarContraseniaOlvidada(ByVal be As BEUsuarioBase) As BEUsuarioBase
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarContraseniaOlvidada(be)
            End Using
        End Function

        Public Function SuscriptorRegistrar(ByVal oBESuscriptor As BESuscriptor) As String
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).SuscriptorRegistrar(oBESuscriptor)
            End Using
        End Function

        Public Function ConsultarPlantillaTipoVariacion() As List(Of BEParametro)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaTipoVariacion()
            End Using
        End Function

#Region "Liquidaciones"
        Public Function RecuperarCipsNoConciliados(ByVal idMoneda As Integer, ByVal Fecha As DateTime) As List(Of BELiquidacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarCipsNoConciliados(idMoneda, Fecha)
            End Using
        End Function


        Public Function RegistrarPeriodo(ByVal be As BEPeriodoLiquidacion) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarPeriodo(be)
            End Using
        End Function

        Public Function ActualizarPeriodo(ByVal be As BEPeriodoLiquidacion) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarPeriodo(be)
            End Using
        End Function







        Public Function RecuperarDepositosBCP(ByVal idMoneda As Integer, ByVal empresas As String) As List(Of BELiquidacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarDepositosBCP(idMoneda, empresas)
            End Using
        End Function



        Public Function ConsultarIdDescripcionPeriodo() As List(Of BEPeriodoLiquidacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarIdDescripcionPeriodo()
            End Using
        End Function

        Public Function RecuperarDatosPeriodoLiquidacion(ByVal be As BEPeriodoLiquidacion) As List(Of BEPeriodoLiquidacion)
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarDatosPeriodoLiquidacion(be)
            End Using
        End Function


        Public Function DeletePeriodo(ByVal be As BEPeriodoLiquidacion) As Integer
            Using Conexionsss As New ProxyBase(Of IComun)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).DeletePeriodo(be)
            End Using
        End Function




#End Region

    End Class





End Namespace

