Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports _3Dev.FW.Web
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CConciliacion
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function ConsultarUltimasConciliaciones(ByVal request As BEConciliacionRequest) As List(Of BEConciliacionResponse)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarUltimasConciliaciones(request)
            End Using
        End Function

        Public Function ConsultarConciliacionEntidad(ByVal request As BEConciliacionEntidad) As List(Of BEConciliacionEntidad)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarConciliacionEntidad(request)
            End Using
        End Function

        Public Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionArchivo)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarConciliacionArchivo(request)
            End Using
        End Function

        Public Function ConsultarDetalleConciliacion(ByVal request As BEConciliacionDetalle) As List(Of BEConciliacionDetalle)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleConciliacion(request)
            End Using
        End Function

        Public Function ProcesarArchivoConciliacion(ByVal request As BEConciliacionArchivo) As BEConciliacionResponse
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ProcesarArchivoConciliacion(request)
            End Using
        End Function

        Public Sub RecalcularCIPsConciliacion(ByVal IdUsuario As Integer)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecalcularCIPsConciliacion(IdUsuario)
            End Using
        End Sub
        Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenesConcArch(request)
            End Using
        End Function

        Public Function ProcesarArchivoPreConciliacion(ByVal request As BEPreConciliacionRequest) As BEConciliacionResponse
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ProcesarArchivoPreConciliacion(request)
            End Using
        End Function

        Public Function ConsultarConciliacionDiaria(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionDiaria)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarConciliacionDiaria(request)
            End Using
        End Function

        Public Function ConsultarPreConciliacionDetalle(ByVal request As BEConciliacionArchivo) As List(Of BEPreConciliacionDetalle)
            Using Conexionsss As New ProxyBase(Of IConciliacion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPreConciliacionDetalle(request)
            End Using
        End Function
    End Class

End Namespace
