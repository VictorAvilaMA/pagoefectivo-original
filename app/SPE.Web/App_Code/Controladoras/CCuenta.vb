Imports _3Dev.FW.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.EmsambladoComun
Imports System



Namespace SPE.Web

    Public Class CCuenta
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase
        Implements IDisposable

        Public Function ConsultarMovimientoCuenta(ByVal be As Entidades.BEMovimientoCuentaBanco) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of ICuenta)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMovimientoCuenta(be)
            End Using
        End Function

        Public Function ConsultarDetMovCuentaPorIdMovCuentaBanco(ByVal be As Entidades.BEDetMovimientoCuenta) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of ICuenta)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetMovCuentaPorIdMovCuentaBanco(be)
            End Using
        End Function
    End Class
End Namespace

