﻿Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CDineroVirtual
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

#Region "Victor"
        Public Function RegistrarCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As Integer
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarCuentaDineroVirtual(oBECuentaDineroVirtual)
            End Using
        End Function

        Public Function ActualizarEstadoCuentaDineroVirtual(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As Integer
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarEstadoCuentaDineroVirtual(oBECuentaDineroVirtual)
            End Using
        End Function

        Public Function ConsultarMovimientoXIdCuenta(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMovimientoXIdCuenta(oBEMovimientoCuenta)
            End Using
        End Function

        Public Function ConsultarMovimientoXIdCuentaExportar(ByVal oBEMovimientoCuenta As BEMovimientoCuenta) As List(Of BEMovimientoCuenta)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMovimientoXIdCuentaExportar(oBEMovimientoCuenta)
            End Using
        End Function

        Public Function ActualizarMontoRecargaDineroVirtual(ByVal olistaBEMontoRecargaMonedero As List(Of BEMontoRecargaMonedero)) As Integer
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarMontoRecargaDineroVirtual(olistaBEMontoRecargaMonedero)
            End Using
        End Function

        Public Function ConsultarMonedaXIdCliente(ByVal oBEMontoRecargaMonedero As BEMontoRecargaMonedero) As List(Of BEMontoRecargaMonedero)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMonedaXIdCliente(oBEMontoRecargaMonedero)
            End Using
        End Function

        Public Function ConsultarClienteXIdClienteDV(ByVal entidad As BECliente) As BECliente
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarClienteXIdClienteDV(entidad)
            End Using
        End Function

        Public Function ActualizarEstadoHabilitarMonederoClienteDV(ByVal entidad As BECliente) As Integer
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarEstadoHabilitarMonederoClienteDV(entidad)
            End Using
        End Function

        Public Function ConsultarMonedaMonederoVirtual() As List(Of BEMoneda)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarMonedaMonederoVirtual()
            End Using
        End Function

#End Region


#Region "IMPLEMENTADO POR IOE"
        Public Function ConsultarCuentaDineroVirtualUsuario(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As System.Collections.Generic.List(Of Entidades.BECuentaDineroVirtual)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaDineroVirtualUsuario(oBECuentaDineroVirtual)
            End Using
        End Function

        Public Function ConsultarCuentaDineroVirtualUsuarioByCuentaId(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaDineroVirtualUsuarioByCuentaId(oBECuentaDineroVirtual)
            End Using
        End Function

        Function ConsultarSolicitudRetiroMonedero(ByVal entidad As _3Dev.FW.Entidades.BusinessEntityBase) As List(Of BESolicitudRetiroDineroVirtual)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarSolicitudRetiroMonedero(entidad)
            End Using
        End Function

        Function ConsultarSolicitudRetiroMonederoById(ByVal entidad As _3Dev.FW.Entidades.BusinessEntityBase) As BESolicitudRetiroDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarSolicitudRetiroMonederoById(entidad)
            End Using
        End Function


        Function ActualizarSolicitudRetiroDinero(ByVal entidad As _3Dev.FW.Entidades.BusinessEntityBase) As Boolean
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarSolicitudRetiroDinero(entidad)
            End Using
        End Function



#End Region

#Region "IMPLEMENTADO POR MXRT¡N"
        Public Function ConsultarCuentasVirtualesAdministrador(ByVal oBECuentaDineroVirtual As BECuentaDineroVirtual) As System.Collections.Generic.List(Of Entidades.BECuentaDineroVirtual)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentasVirtualesAdministrador(oBECuentaDineroVirtual)
            End Using
        End Function

        Function ObtenerOrdenPagoCompletoPorNroOrdenPago(ByVal nroOrdenPago As Long) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerOrdenPagoCompletoPorNroOrdenPago(nroOrdenPago)
            End Using
        End Function

        Public Function RegistrarTokenMonedero(ByVal token As BETokenDineroVirtual) As BETokenDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarTokenMonedero(token)
            End Using
        End Function

        Public Function RegistrarPagoCIP(ByVal beMovimiento As BEMovimientoCuenta) As String
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarPagoCIP(beMovimiento)
            End Using
        End Function

        Public Function RegistrarPagoCIPPortal(ByVal beMovimiento As BEMovimientoCuenta, ByVal oBESolicitudPago As BESolicitudPago) As String
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarPagoCIPPortal(beMovimiento, oBESolicitudPago)
            End Using
        End Function

        Public Function RegistrarSolicitudRetiroDinero(ByVal beSolicitudRetiroDinero As BESolicitudRetiroDineroVirtual) As BESolicitudRetiroDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarSolicitudRetiroDinero(beSolicitudRetiroDinero)
            End Using
        End Function

        Function ConsultarCuentaDineroVirtualPorNroCuenta(ByVal NroCuenta As String) As BECuentaDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaDineroVirtualPorNroCuenta(NroCuenta)
            End Using
        End Function

        Public Function RegistrarTransferenciaDinero(ByVal beMovimiento As BEMovimientoCuenta) As String
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarTransferenciaDinero(beMovimiento)
            End Using
        End Function

        Public Function ConsultarReglaMovimientoSospechoso() As List(Of BEReglaMovimientoSospechoso)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarReglaMovimientoSospechoso()
            End Using
        End Function

        Public Function ConsultarRegistroEjecucion(ByVal IdRegla As Integer, ByVal Anio As Integer, ByVal Mes As Integer) As List(Of BERegistroEjecucionRegla)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarRegistroEjecucion(IdRegla, Anio, Mes)
            End Using
        End Function

        Function ConsultarResultadoEjecucionRegla(ByVal entidad As BEResultadoEjecucionRegla) As List(Of BEResultadoEjecucionRegla)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarResultadoEjecucionRegla(entidad)
            End Using
        End Function

        Function ConsultarDetalleResultadoEjecucion(ByVal entidad As BEDetalleResultadoEjecucion) As List(Of BEDetalleResultadoEjecucion)
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleResultadoEjecucion(entidad)
            End Using
        End Function

        Function ReenviarEmailToken(ByVal IdToken As Long) As BETokenDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ReenviarEmailToken(IdToken)
            End Using
        End Function

		Function ConsultarTokenByID(ByVal IdToken As Long) As BETokenDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTokenByID(IdToken)
            End Using
        End Function

#End Region

        Function ActualizarCuentaDineroVirtual(ByVal entidad As BECuentaDineroVirtual) As Boolean
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarCuentaDineroVirtual(entidad)
            End Using
        End Function

        Public Function ValidarRegistrarCuentaDineroVirtual(ByVal obecuentadinerovirtual As BECuentaDineroVirtual) As BECuentaDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarRegistrarCuentaDineroVirtual(obecuentadinerovirtual)
            End Using
        End Function

        Function ConsultarCuentaDineroVirtualUsuarioByNumero(numeroCuenta As Long) As BECuentaDineroVirtual
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Dim c As New BECuentaDineroVirtual
                c.Numero = numeroCuenta
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaDineroVirtualUsuarioByNumero(c)
            End Using

        End Function

        Function ObtenerParametroMonederoByID(IdParametro As Integer) As BEParametroMonedero
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerParametroMonederoByID(IdParametro)
            End Using
        End Function

        Function ConsultaUsuarioByIdCuenta(IdParametro As Long) As Integer
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultaUsuarioByIdCuenta(IdParametro)
            End Using
        End Function

        Function ConsultaEstadoUsuarioByIdUsuario(IdUsuario As Integer) As Integer
            Using Conexionsss As New ProxyBase(Of IDineroVirtual)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultaEstadoUsuarioByIdUsuario(IdUsuario)
            End Using
        End Function

    End Class

End Namespace

