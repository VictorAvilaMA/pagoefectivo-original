Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class CEmpresaContratante

        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
        Public Function OcultarEmpresa(ByVal idservicio As Integer) As BEOcultarEmpresa
            Return CType(RemoteServicesInterfaceObject, IEmpresaContratante).OcultarEmpresa(idservicio)
        End Function
        Public Function RegistrarTransaccionesaLiquidar(ByVal be As BETransferencia) As Integer
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarTransaccionesaLiquidar(be)
            End Using
        End Function

        Public Function ConsultarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTransaccionesaLiquidar(be)
            End Using
        End Function

        Public Function ObtenerFechaInicioTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerFechaInicioTransaccionesaLiquidar(be)
            End Using
        End Function
        Public Function ObtenerFechaInicioTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerFechaInicioTransaccionesaLiquidarLote(be)
            End Using
        End Function
        Public Function ObtenerFechaFinTransaccionesaLiquidar(ByVal be As BETransferencia) As DateTime
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerFechaFinTransaccionesaLiquidar(be)
            End Using
        End Function
        Public Function ObtenerFechaFinTransaccionesaLiquidarLote(ByVal be As BEPeriodoLiquidacion) As DateTime
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerFechaFinTransaccionesaLiquidarLote(be)
            End Using
        End Function

        Public Function ActualizarTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarTransaccionesaLiquidar(be)
            End Using
        End Function
        Public Function ObtenerTransaccionesaLiquidar(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BETransferencia
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerTransaccionesaLiquidar(be)
            End Using
        End Function
        Public Function ValidarPendientesaConciliar(ByVal be As BETransferencia) As Integer
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarPendientesaConciliar(be)
            End Using
        End Function
#Region "Configuración de cuenta y banco de empresas"
        Public Function InsertarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).InsertarCuentaEmpresa(be)
            End Using
        End Function
        Public Function ActualizarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarCuentaEmpresa(be)
            End Using
        End Function
        Public Function ConsultarCuentaEmpresa(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaEmpresa(be)
            End Using
        End Function
        Public Function ConsultarCuentaEmpresaPorId(ByVal id As Object) As _3Dev.FW.Entidades.BusinessEntityBase
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCuentaEmpresaPorId(id)
            End Using
        End Function
#End Region


#Region "Liquidaciones"
        Public Function RecuperarEmpresasLiquidacion(ByVal FechaDe As Date, ByVal FechaAl As Date, ByVal idPeriodoLiquidacion As Integer, ByVal idMoneda As Integer) As System.Collections.Generic.List(Of BEEmpresaContratante)
            Using Conexionsss As New ProxyBase(Of IEmpresaContratante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarEmpresasLiquidacion(FechaDe, FechaAl, idPeriodoLiquidacion, idMoneda)
            End Using
        End Function


#End Region

    End Class

End Namespace
