Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CEstablecimiento
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IEstablecimiento)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property

        Public Function ObtenerOperadores() As List(Of BEOperador)
            Using Conexionsss As New ProxyBase(Of IEstablecimiento)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerOperadores()
            End Using
        End Function


        Public Function GetLiquidacionPendientexIdComercio(ByVal objEstablecimiento As BEEstablecimiento) As List(Of BEEstablecimiento)
            Using Conexionsss As New ProxyBase(Of IEstablecimiento)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetLiquidacionPendientexIdEstablecimiento(objEstablecimiento)
            End Using
        End Function

        Public Function GetPendientesCancelacion(ByVal objComercio As BEComercio) As List(Of BEEstablecimiento)
            Using Conexionsss As New ProxyBase(Of IEstablecimiento)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetPendientesCancelacion(objComercio)
            End Using
        End Function

        Public Function LiquidarBloque(ByVal objAperturaOrigen As List(Of BEAperturaOrigen)) As Integer
            Using Conexionsss As New ProxyBase(Of IEstablecimiento)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).LiquidarBloque(objAperturaOrigen)
            End Using
        End Function

        Public Function ConsultarCajasLiquidadas(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEMovimiento)
            Using Conexionsss As New ProxyBase(Of IEstablecimiento)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCajasLiquidadas(obeOrdenPago)
            End Using
        End Function


    End Class


End Namespace

