Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class CFTPArchivo

        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IFTPArchivo)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property

        Function ObtenerFTPArchivoPorId(ByVal id As Object, ByVal OrderBy As String, ByVal IsAccending As Boolean) As BEFTPArchivo
            Using Conexionsss As New ProxyBase(Of IFTPArchivo)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerFTPArchivoPorId(id, OrderBy, IsAccending)
            End Using
        End Function

    End Class

End Namespace
