Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web

    Public Class CJefeProducto
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IJefeProducto)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
    End Class

End Namespace
