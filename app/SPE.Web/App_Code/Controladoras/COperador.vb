Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class COperador
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IOperador)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
    End Class

End Namespace
