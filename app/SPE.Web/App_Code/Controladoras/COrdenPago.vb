Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.Web
'Imports SPE.Notificacion
Imports System.Transactions
Imports _3Dev.FW.EmsambladoComun
Imports _3Dev.FW.Entidades

Namespace SPE.Web
    Public Class COrdenPago
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property

        'Public Function GenerarOrdenPagoXML(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
        '    Dim result As Integer
        '    Dim vOrdenPago As IOrdenPago = CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago
        '    result = vOrdenPago.GenerarOrdenPagoXML(obeOrdenPago, ListaDetalleOrdePago)
        '    Return result
        'End Function

        Public Function GenerarOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
            Dim result As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Dim vOrdenPago As IOrdenPago = Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                result = vOrdenPago.GenerarOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
            End Using

            Return result
        End Function

        Public Function GenerarCIP(ByVal request As BEGenCIPRequest) As BEGenCIPResponse
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarCIP(request)
            End Using
        End Function

        Public Function GenerarPreOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal ListaDetalleOrdePago As List(Of BEDetalleOrdenPago)) As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPreOrdenPago(obeOrdenPago, ListaDetalleOrdePago)
            End Using
        End Function

        Public Function ConsultarOrdenPagoRecepcionar(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoRecepcionar(obeOrdenPago)
            End Using
        End Function
        'Public Function RequestUrlServicio(ByVal obeOrdenPago As BEOrdenPago, ByVal strProceso As Integer, ByVal opcionProceso As Integer) As Integer
        '    '< -- >
        '    'Dim onoOrdenPago As New NOOrdenPago(CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago)
        '    'Dim result As Integer
        '    ''
        '    'Select Case strProceso
        '    '    Case SPE.EmsambladoComun.ParametrosSistema.TipoNotificacion.Recepcion
        '    '        result = onoOrdenPago.QSNotificarRecepcionOrdenPago(obeOrdenPago, opcionProceso)
        '    '    Case SPE.EmsambladoComun.ParametrosSistema.TipoNotificacion.Anulacion
        '    '        result = onoOrdenPago.QSNotificarAnulacionOrdenPago(obeOrdenPago, opcionProceso)
        '    'End Select

        '    'Return result
        '    '< -- >
        '    Return 1
        '    '
        'End Function
        Public Function AnularOrdenPago(ByVal obeOrdenPago As BEOrdenPago, ByVal obeMovimiento As BEMovimiento) As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).AnularOrdenPago(obeOrdenPago, obeMovimiento)
            End Using
        End Function

        Public Function ActualizarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarOrdenPago(obeOrdenPago)
            End Using
        End Function

        Public Function ProcesoRecepcionarOrdenPago(ByVal obeMovimiento As BEMovimiento, ByVal obeOrdenPago As BEOrdenPago, ByVal obeDetalleTarjeta As BEDetalleTarjeta) As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ProcesoRecepcionarOrdenPago(obeMovimiento, obeOrdenPago, obeDetalleTarjeta)
            End Using
        End Function

        Public Function ConsultarOrdenPagoPendientes(ByVal obeOrdePago As SPE.Entidades.BEOrdenPago, ByVal orderedBy As String, ByVal isAccending As Boolean) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPendientes(obeOrdePago, orderedBy, isAccending)
            End Using
        End Function
        'nuevo
        Public Function ConsultarOrdenPagoPorIdComercio(ByVal OrdenIdComercio As String, ByVal IdServicio As Integer) As BEOPXServClienCons
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPorOrdenIdComercio(OrdenIdComercio, IdServicio)
            End Using
        End Function
        Public Function ConsultarOrdenPagoPorNumeroOrdenPago(ByVal NumeroOrdenPago As String) As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPorNumeroOrdenPago(NumeroOrdenPago)
            End Using
        End Function

        Public Function ConsultarOrdenPagoPorId(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPorId(obeOrdenPago)
            End Using
        End Function

        Public Function ConsultarOrdenPagoPorIdInst(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPorId(obeOrdenPago)
            End Using
        End Function

        Public Function ConsultarDetalleOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As BEDetalleOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleOrdenPago(obeOrdenPago)
            End Using
        End Function

        Function ConsultarOrdenPago(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPago(obeOrdenPago)
            End Using
        End Function

        Public Function ConsultarPagosServicios(ByVal be As SPE.Entidades.BEPagoServicio, ByVal orderedBy As String, ByVal isAccending As Boolean) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetListByParametersOrderedBy("ConsultPagoServicios", be, orderedBy, isAccending)
            End Using
        End Function

        'agregado hector formulario--------------------->
        Public Function ConsultarPagosServiciosFormulario(ByVal be As SPE.Entidades.BEPagoServicio, ByVal orderedBy As String, ByVal isAccending As Boolean) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetListByParametersOrderedBy("ConsultPagoServiciosFormulario", be, orderedBy, isAccending)
            End Using
        End Function

        'formulario--------------------->
        Public Function ConsutarFormularioActivo(ByVal IdServicio As Integer) As Boolean
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsutarFormularioActivo(IdServicio)
            End Using
        End Function




        Public Function ObtenerCIPInfoByUrLServicio(ByVal request As BEObtCIPInfoByUrLServRequest) As BEObtCIPInfoByUrLServResponse
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerCIPInfoByUrLServicio(request)
            End Using
        End Function
        Public Function ObtenerOrdenPagCabeceraPorNumeroOrden(ByVal numeroOrden As String) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerOrdenPagCabeceraPorNumeroOrden(numeroOrden)
            End Using
        End Function
        Public Function ConsultarDetalleOrdenPagoPorIdOrdenPago(ByVal idOrdenPago As Integer) As List(Of BEDetalleOrdenPago)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleOrdenPagoPorIdOrdenPago(idOrdenPago)
            End Using
        End Function
        Public Function ConsultarHistoricoMovimientosporIdOrden(ByVal idOrdenPago As Integer) As List(Of BEMovimiento)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarHistoricoMovimientosporIdOrden(idOrdenPago)
            End Using
        End Function

        Public Function ConsultarSolicitudPagoPorToken(ByVal oBESolicitudPago As BESolicitudPago) As BESolicitudPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarSolicitudPagoPorToken(oBESolicitudPago)
            End Using
        End Function

        Public Function ConsultarSolicitudPagoPorTokenInst(ByVal oBESolicitudPago As BESolicitudPago) As BESolicitudPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarSolicitudPagoPorToken(oBESolicitudPago)
            End Using
        End Function

        Public Function GenerarOrdenPagoFromSolicitud(ByVal oBESolicitudPago As BESolicitudPago) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarOrdenPagoFromSolicitud(oBESolicitudPago)
            End Using
        End Function
        Public Function ConsultarSolicitudXMLPorID(ByVal oBESolicitudPago As BESolicitudPago) As String
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarSolicitudXMLPorID(oBESolicitudPago)
            End Using
        End Function
#Region "Devoluciones"
        Public Function RegistrarDevolucion(ByVal oBEDevolucion As BEDevolucion) As BEDevolucion
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarDevolucion(oBEDevolucion)
            End Using
        End Function
        Public Function ConsultarDevoluciones(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDevoluciones(oBEDevolucion)
            End Using
        End Function
        Public Function ActualizarDevolucion(ByVal oBEDevolucion As BEDevolucion) As Int64
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDevolucion(oBEDevolucion)
            End Using
        End Function
        Public Function ConsultarCipDevolucion(ByVal NroCip As Int64, ByVal IdUsuario As Int32) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCipDevolucion(NroCip, IdUsuario)
            End Using
        End Function
        Public Function ObtenerArchivoDevolucion(ByVal Archivo As String) As Byte()
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerArchivoDevolucion(Archivo)
            End Using
        End Function

        Public Function ConsultarDevolucionesExcel(ByVal oBEDevolucion As BEDevolucion) As List(Of BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDevolucionesExcel(oBEDevolucion)
            End Using
        End Function
#End Region
        Public Function ActualizarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarCIP(oBEOrdenPago)
            End Using
        End Function
        Public Function EliminarCIP(ByVal oBEOrdenPago As BEOrdenPago) As Int64
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EliminarCIP(oBEOrdenPago)
            End Using
        End Function
        Public Function ConsultarOrdenPagoPorIdOrdenYIdRepresentante(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPorIdOrdenYIdRepresentante(obeOrdenPago)
            End Using
        End Function
        Public Function EliminarCIPWeb(ByVal oBEOrdenPago As BEOrdenPago) As Int64
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EliminarCIPWeb(oBEOrdenPago)
            End Using
        End Function

#Region "ToGenetat"

#End Region
        Public Function ConsultarOrdenPagoPorIdSoloGenYExp(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago
            Using Conexionsss As New ProxyBase(Of IOrdenPago)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarOrdenPagoPorIdSoloGenYExp(obeOrdenPago)
            End Using
        End Function

        Public Function ActualizarCIPDeExpAGen(ByVal oBEOrdenPago As BEOrdenPago) As Int64
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarCIPDeExpAGen(oBEOrdenPago)
            End Using
        End Function

        Public Function ExtornarCIP(ByVal nroCIP As String, ByVal tipoExtorno As Integer) As Int64
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ExtornarCIP(nroCIP, tipoExtorno)
            End Using
        End Function

#Region "Messaging"
        Public Function RegisterCellPhoneNumber(ByVal messaging As BEMobileMessaging) As Integer
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegisterCellPhoneNumber(Messaging)
            End Using
        End Function

        Public Function ValidateCipExpiration(ByVal paymentOrderId As Int64) As String
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidateCipExpiration(PaymentOrderId)
            End Using
        End Function

        Public Function GetMessageSms(ByVal smsTemplateId As Integer) As String
            Using Conexionsss As New ProxyBase(Of IOrdenPago)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GetMessageSms(smsTemplateId)
            End Using
        End Function
#End Region
    End Class
End Namespace
