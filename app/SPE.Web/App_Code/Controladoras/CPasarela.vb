Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports System

Namespace SPE.Web
    Public Class CPasarela
        Implements IDisposable

        Public Function GenerarPasarela(ByVal CodServicio As String, ByVal DtosEncriptados As String) As BEGenPasResponse
            Using Conexionsss As New ProxyBase(Of IPasarela)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPasarela(CodServicio, DtosEncriptados)
            End Using
        End Function

#Region "Otros"
        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free managed resources when explicitly called
                End If

                ' TODO: free shared unmanaged resources
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
#End Region

    End Class


End Namespace

