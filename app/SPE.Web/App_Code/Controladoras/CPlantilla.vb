Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports SPE.Entidades
Imports _3Dev.FW.EmsambladoComun
Imports System.Collections.Generic

Public Class CPlantilla
    Inherits _3Dev.FW.Web.ControllerMaintenanceBase

    Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
        Get
            Dim Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
        End Get
    End Property

    '<add Peru.Com FaseIII>
    Public Function ConsultarPlantillaPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
        End Using
    End Function
    Public Function ConsultarPlantillaPorTipoYVariacionExiste(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaPorTipoYVariacionExiste(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
        End Using
    End Function

    Public Function ConsultarPlantillaTipoPorMoneda(ByVal idMoneda As Int32) As List(Of BEPlantillaTipo)
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaTipoPorMoneda(idMoneda)
        End Using
    End Function

    Public Function ConsultarPlantillaTipoPorMonedaYServicio(ByVal idMoneda As Int32, ByVal idServicio As Int32) As List(Of BEPlantillaTipo)
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaTipoPorMonedaYServicio(idMoneda, idServicio)
        End Using
    End Function

    Public Function ConsultarPlantillaParametroPorIdPlantilla(ByVal idPlantilla As Int32) As List(Of BEPlantillaParametro)
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaParametroPorIdPlantilla(idPlantilla)
        End Using
    End Function

    Public Function GenerarPlantillaDesdePlantillaBase(ByVal oBEPlantilla As BEPlantilla) As BEPlantilla
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPlantillaDesdePlantillaBase(oBEPlantilla)
        End Using
    End Function
    Public Function ConsultarParametrosFuentes() As List(Of BEPlantillaParametro)
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarParametrosFuentes()
        End Using
    End Function
    Public Function RegistrarPlantillaParametros(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarPlantillaParametros(oBEPlantillaParametro)
        End Using
    End Function
    Public Function ActualizarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarPlantillaParametro(oBEPlantillaParametro)
        End Using
    End Function
    Public Function EliminarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EliminarPlantillaParametro(oBEPlantillaParametro)
        End Using
    End Function
    Public Function ActualizarPlantilla(ByVal oBEPlantilla As BEPlantilla) As Integer
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarPlantilla(oBEPlantilla)
        End Using
    End Function
    '************************************************************************************************** 
    ' M�todo          : ConsultarPLantillasXServicioGenPago
    ' Descripci�n     : Consulta las plantillas de los correos correspondientes a un servicio para la pasarela de Pagos
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 29/05/2012
    ' Parametros_In   : idServicio , idMoneda
    ' Parametros_Out  : Cadena HTML descripci�n de bancos
    ''**************************************************************************************************
    Public Function ConsultarPLantillasXServicioGenPago(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal codPlantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal CodPlantillaTipoVariacion As String) As String
        Using Conexionsss As New ProxyBase(Of IPlantilla)(TiposServicios.Portales)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPLantillasXServicioGenPago(idServicio, idMoneda, codPlantillaTipo, valoresDinamicos, CodPlantillaTipoVariacion)
        End Using
    End Function
    '************************************************************************************************** 
    ' M�todo          : ConsultarPLantillasXServicioGenPagoInst
    ' Descripci�n     : Consulta las plantillas de los correos correspondientes a un servicio para la pasarela de Pagos desde WCF PE
    ' Autor           : Pedro Monzon
    ' Fecha/Hora      : 04/01/2014
    ' Parametros_In   : idServicio , idMoneda
    ' Parametros_Out  : Cadena HTML descripci�n de bancos
    Public Function ConsultarPLantillasXServicioGenPagoInst(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal codPlantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal CodPlantillaTipoVariacion As String) As String
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPLantillasXServicioGenPago(idServicio, idMoneda, codPlantillaTipo, valoresDinamicos, CodPlantillaTipoVariacion)
        End Using
    End Function
    '************************************************************************************************** 
    ' M�todo          : GenerarPlantillaToHtmlByTipo
    ' Descripci�n     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados apartir del IdServicio y Codigo de tipo de PLantilla
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 30/05/2012
    ' Parametros_In   : IdServicio, codTipoPLantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    Public Function GenerarPlantillaToHtmlByTipo(ByVal idServicio As Integer, ByVal codPLantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal CodPlantillaTipoVariacion As String) As String
        Using Conexionsss As New ProxyBase(Of IPlantilla)(TiposServicios.Portales)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPlantillaToHtmlByTipo(idServicio, codPLantillaTipo, valoresDinamicos, CodPlantillaTipoVariacion)
        End Using
    End Function
    '************************************************************************************************** 
    ' M�todo          : GenerarPlantillaToHtmlByTipoInsti
    ' Descripci�n     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados apartir del IdServicio y Codigo de tipo de PLantilla desde WCF PE
    ' Autor           : Pedro Monzon
    ' Fecha/Hora      : 04/10/2014
    ' Parametros_In   : IdServicio, codTipoPLantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    Public Function GenerarPlantillaToHtmlByTipoInsti(ByVal idServicio As Integer, ByVal codPLantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal CodPlantillaTipoVariacion As String) As String
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPlantillaToHtmlByTipo(idServicio, codPLantillaTipo, valoresDinamicos, CodPlantillaTipoVariacion)
        End Using
    End Function
    '************************************************************************************************** 
    ' M�todo          : GenerarPlantillaToHtmlByTipoInst
    ' Descripci�n     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados apartir del IdServicio y Codigo de tipo de PLantilla desde WCF PE
    ' Autor           : Pedro Monzon
    ' Fecha/Hora      : 02/01/2014
    ' Parametros_In   : IdServicio, codTipoPLantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    Public Function GenerarPlantillaToHtmlByTipoInst(ByVal idServicio As Integer, ByVal codPLantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal CodPlantillaTipoVariacion As String) As String
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GenerarPlantillaToHtmlByTipo(idServicio, codPLantillaTipo, valoresDinamicos, CodPlantillaTipoVariacion)
        End Using
    End Function
    Public Function ConsultarPLantillasXServicioQueEsPE(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal CodPlantillaTipoVariacion As String) As String
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillasXServicioQueEsPE(idServicio, idMoneda, valoresDinamicos, CodPlantillaTipoVariacion)
        End Using
    End Function

    Public Function ObtenerSeccionesPlantillaPorServicio(ByVal idServicio As Integer, ByVal idPlantillaFormato As Integer) As List(Of BEPlantillaSeccion)
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerSeccionesPlantillaPorServicio(idServicio, idPlantillaFormato)
        End Using
    End Function

    Public Function ActualizarServicioSeccion(ByVal lstPlantillaSeccion As List(Of BEPlantillaSeccion), ByVal idUsuario As Integer) As Integer
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarServicioSeccion(lstPlantillaSeccion, idUsuario)
        End Using
    End Function

    Function ConsultarPlantillaYParametrosPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla
        Using Conexionsss As New ProxyBase(Of IPlantilla)
            Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPlantillaYParametrosPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
        End Using
    End Function
	
End Class
