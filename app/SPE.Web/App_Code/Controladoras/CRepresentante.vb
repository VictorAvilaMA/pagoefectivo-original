Imports SPE.EmsambladoComun
Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports _3Dev.FW.EmsambladoComun
Imports SPE.Entidades

Namespace SPE.Web

    Public Class CRepresentante
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IRepresentante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
        Public Function ObtenerEmpresaporIDRepresentante(ByVal objRepresentante As BERepresentante) As BERepresentante
            Using Conexionsss As New ProxyBase(Of IRepresentante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerEmpresaporIDRepresentante(objRepresentante)
            End Using
        End Function
        Public Function ConsultarEmpresasPorIdUsuario(ByVal be As BEUsuarioBase) As List(Of BEEmpresaContratante)
            Using Conexionsss As New ProxyBase(Of IRepresentante)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarEmpresasPorIdUsuario(be)
            End Using
        End Function
    End Class
End Namespace


