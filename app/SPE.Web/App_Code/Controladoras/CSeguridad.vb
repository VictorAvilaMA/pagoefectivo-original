﻿Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Public Class CSeguridad

    Public Sub New()

    End Sub

    Public Function ValidarServicioClaveAPI(ByVal claveAPI As String, ByVal claveSecreta As String) As Boolean

        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().ValidarServicioClaveAPI(claveAPI, claveSecreta)
        End Using
    End Function



    Public Function GuardarClavePublica(ByVal IdServicio As Int32, ByVal key As String) As Boolean
        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().SaveKeyPublic(IdServicio, key)
        End Using
    End Function
    Public Function ObtenerClavePublica(ByVal codEntidad As String) As String
        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().GetPublicKey(codEntidad)
        End Using
    End Function
    Public Function GenerateKeyPrivatePublicSPE() As Boolean

        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().GenerateKeyPrivatePublicSPE()
        End Using
    End Function
    Public Function ObtenerClavePublicaSPE() As String
        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().GetPublicKeySPE()
        End Using
    End Function
    Public Function ObtenerClavePrivadaSPE() As String
        Using Conexions As New ProxyBase(Of ISeguridad)
            Return Conexions.DevolverContrato().GetPrivateKeySPE()
        End Using

    End Function
End Class
