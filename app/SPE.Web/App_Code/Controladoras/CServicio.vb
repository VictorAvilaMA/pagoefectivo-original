Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class CServicio
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function ConsultarServicioFormularioUrl(ByVal IdServicio As Integer, ByVal URL As String) As SPE.Entidades.BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarServicioFormularioUrl(IdServicio, URL)
            End Using
        End Function



        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.IMaintenanceBase
            Get
                Dim Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property

        Public Function ConsultarServicioPorUrl(ByVal parametro As Object) As SPE.Entidades.BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarServicioPorUrl(parametro)
            End Using
        End Function
        Public Function ConsultarPagoServicios(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPagoServicios(obeOrdenPago)
            End Using
        End Function
        Public Function ConsultarPagoServiciosNoRepresentante(ByVal obeOrdenPago As BEOrdenPago) As List(Of BEOrdenPago)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPagoServiciosNoRepresentante(obeOrdenPago)
            End Using
        End Function
        Public Function ConsultarServiciosPorIdUsuario(ByVal obeServicio As BEServicio) As List(Of BEServicio)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarServiciosPorIdUsuario(obeServicio)
            End Using
        End Function

        '----------------------------------------------------------------------------------------------------
        Public Function ConsultarServicios(ByVal obeServicio As BEServicio) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).SearchByParameters(obeServicio)
            End Using
        End Function
        Public Function ConsultarServicioxEmpresa(ByVal obeServicio As BEServicio) As List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarServicioxEmpresa(obeServicio)
            End Using
        End Function
        Public Function ActualizaServicioxEmpresa(ByVal obeServicio As BEServicio) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizaServicioxEmpresa(obeServicio)
            End Using
        End Function
        Public Function ConsultarTransaccionesxServicio(ByVal objbeservicio As BEServicio, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTransaccionesxServicio(objbeservicio, objFechaDel, objFechaAl)
            End Using
        End Function
        Public Function ConsultarTransaccionesxServicioLote(ByVal objbeservicio As BEServicio, ByVal objFechaDel As Date, ByVal objFechaAl As Date) As BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarTransaccionesxServicioLote(objbeservicio, objFechaDel, objFechaAl)
            End Using
        End Function

#Region "Configuracion de Servicios"
        Public Function ConsultarContratoXMLDeServicios(ByVal codigoAcceso As String) As String
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarContratoXMLDeServicios(codigoAcceso)
            End Using
        End Function

        Public Function GuardarContratoXMLDeServicios(ByVal codigoAcceso As String, ByVal contratoXml As String) As String
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).GuardarContratoXMLDeServicios(codigoAcceso, contratoXml)
            End Using
        End Function
#End Region

        Public Function ValidarServicioPorAPIYClave(ByVal cAPI As String, ByVal cClave As String) As Boolean
            Using Conexionsss As New ProxyBase(Of IServicio)(TiposServicios.Portales)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarServicioPorAPIYClave(cAPI, cClave)
            End Using
        End Function

        '<add Peru.Com FaseIII>
        Public Function UpdateServicioContratoXML(ByVal oBEServicio As BEServicio) As Boolean
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).UpdateServicioContratoXML(oBEServicio)
            End Using
        End Function
        'Cuenta Servicio
        Public Function RegistrarAsociacionCuentaServicio(ByVal be As BEServicioBanco) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarAsociacionCuentaServicio(be)
            End Using
        End Function
        Public Function ActualizarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarAsociacionCuentaServicio(be)
            End Using
        End Function
        Public Function ConsultarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarAsociacionCuentaServicio(be)
            End Using
        End Function
        Public Function ObtenerAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As BEServicioBanco
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerAsociacionCuentaServicio(be)
            End Using
        End Function
        Public Function EliminarAsociacionCuentaServicio(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Boolean
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EliminarAsociacionCuentaServicio(be)
            End Using
        End Function
        Public Function ObtenerServicioComisionporID(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerServicioComisionporID(be)
            End Using
        End Function
        Public Function ConsultarDetalleTransaccionesxServicio(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleTransaccionesxServicio(objbeservicio)
            End Using
        End Function
        Public Function ConsultarDetalleTransaccionesxServicioSaga(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaSaga)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleTransaccionesxServicioSaga(objbeservicio)
            End Using
        End Function
        Public Function ConsultarDetalleTransaccionesxServicioRipley(ByVal objbeservicio As BETransferencia) As List(Of BETransferenciaRipley)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleTransaccionesxServicioRipley(objbeservicio)
            End Using
        End Function
        Public Function ConsultarDetalleTransaccionesPendientes(ByVal objbeservicio As BETransferencia) As List(Of BETransferencia)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleTransaccionesPendientes(objbeservicio)
            End Using
        End Function
        Public Function RegistrarServicioxEmpresa(ByVal obeServicio As BEServicio) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarServicioxEmpresa(obeServicio)
            End Using
        End Function
        Public Function ConsultarServicioPorIdEmpresa(ByVal be As BEServicio) As List(Of BEServicio)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarServicioPorIdEmpresa(be)
            End Using
        End Function

        Function ConsultarPorAPIYClave(ByVal capi As String, ByVal cclave As String) As BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarPorAPIYClave(capi, cclave)
            End Using
        End Function

        Public Function ConsultarComerciosAfiliados(ByVal visibleEnPortada As Boolean, ByVal proximoAfiliado As Boolean) As List(Of BEServicio)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarComerciosAfiliados(visibleEnPortada, proximoAfiliado)
            End Using
        End Function
        Public Function ConsultarCodigosServiciosPorBanco(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCodigosServiciosPorBanco(be)
            End Using
        End Function
        Public Function ObtenerServicioPorID(ByVal id As Integer) As BEServicio
            Using Conexions As New ProxyBase(Of IServicio)(TiposServicios.Portales)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ObtenerServicioPorIdWS(id)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)ddd
        End Function

        Public Function ObtenerServicioPorIDInst(ByVal id As Integer) As BEServicio
            Using Conexions As New ProxyBase(Of IServicio)
                Return Conexions.DevolverContrato(New EntityBaseContractResolver()).ObtenerServicioPorIdWS(id)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)ddd
        End Function


        Public Function ConsultarServicioPorClasificacion(ByVal be As BEServicio) As List(Of BEServicio)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarServicioPorClasificacion(be)
            End Using
        End Function

        Public Function ObtenerServicioInstitucionPorId(ByVal id As Integer) As BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ObtenerServicioInstitucionPorId(id)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)ddd
        End Function


#Region "ServicioInstitucion"
        Public Function ProcesarArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As BEServicioInstitucion
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ProcesarArchivoServicioInstitucion(request)
            End Using
        End Function

        Public Function ValidarCarga(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarCarga(request)
            End Using
        End Function


        Public Function MostrarResumenArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).MostrarResumenArchivoServicioInstitucion(request)
            End Using
        End Function


        Public Function ConsultarDetallePagoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetallePagoServicioInstitucion(request)
            End Using
        End Function

        Public Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDocumentosCIP(request)
            End Using
        End Function

        Public Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDocumentoInstitucion(request)
            End Using
        End Function


        Public Function ConsultarCIPDocumento(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCIPDocumento(request)
            End Using
        End Function



        Public Function ConsultarArchivosDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarArchivosDescarga(request)
            End Using
        End Function


        Public Function ConsultarDetalleArchivoDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleArchivoDescarga(request)
            End Using
        End Function


        Public Function DeterminarIdParametro(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).DeterminarIdParametro(request)
            End Using
        End Function

#End Region


#Region "MunicipalidadBarranco"

        Public Function ProcesarArchivoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As BEServicioMunicipalidadBarranco
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ProcesarArchivoServicioMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ValidarCargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarCargaMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function MostrarResumenArchivoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).MostrarResumenArchivoServicioMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ConsultarDetallePagoServicioMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetallePagoServicioMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ActualizarDocumentosCIPMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDocumentosCIPMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ActualizarDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDocumentoMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ConsultarCIPDocumentoMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarCIPDocumentoMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ConsultarArchivosDescargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarArchivosDescargaMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function ConsultarDetalleArchivoDescargaMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As List(Of BEServicioMunicipalidadBarranco)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleArchivoDescargaMunicipalidadBarranco(request)
            End Using
        End Function

        Public Function DeterminarIdParametroMunicipalidadBarranco(ByVal request As BEServicioMunicipalidadBarrancoRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).DeterminarIdParametroMunicipalidadBarranco(request)
            End Using
        End Function

#End Region



#Region "Servicios Desconectados"
        Public Function RecuperarDatosProductoServicio(ByVal obeServProdReq As BEServicioProductoRequest) As List(Of BEServicioProducto)
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarDatosProductoServicio(obeServProdReq)
            End Using
        End Function


        Public Function RecuperarDatosServicioDesconectado(ByVal intID As Integer) As BEServicio
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarDatosServicioDesconectado(intID)
            End Using
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IOrdenPago.WSConsultarCIP(request)ddd
        End Function
        Public Function RecuperarDetalleProductoServicio(ByVal obeServProdReq As BEServicioProductoRequest) As BEServicioProducto
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RecuperarDetalleProductoServicio(obeServProdReq)
            End Using
        End Function

        'Cuenta Servicio
        Public Function RegistrarOrderIdComercio(ByVal codigo As String) As Integer
            Using Conexionsss As New ProxyBase(Of IServicio)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).RegistrarOrderIdComercio(codigo)
            End Using
        End Function

#End Region




    End Class
End Namespace
