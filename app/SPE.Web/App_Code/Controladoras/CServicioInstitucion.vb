Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web
    Public Class CServicioInstitucion
        Inherits _3Dev.FW.Web.ControllerMaintenanceBase

        Public Function ProcesarArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As BEServicioInstitucion
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ProcesarArchivoServicioInstitucion(request)
            End Using
        End Function

        Public Function ValidarCarga(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ValidarCarga(request)
            End Using
        End Function


        Public Function MostrarResumenArchivoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).MostrarResumenArchivoServicioInstitucion(request)
            End Using
        End Function


        Public Function ConsultarDetallePagoServicioInstitucion(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetallePagoServicioInstitucion(request)
            End Using
        End Function

        Public Function ActualizarDocumentosCIP(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDocumentosCIP(request)
            End Using
        End Function

        Public Function ActualizarDocumentoInstitucion(ByVal request As BEServicioInstitucionRequest) As Integer
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ActualizarDocumentoInstitucion(request)
            End Using
        End Function


        Public Function ConsultarArchivosDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarArchivosDescarga(request)
            End Using
        End Function


        Public Function ConsultarDetalleArchivoDescarga(ByVal request As BEServicioInstitucionRequest) As List(Of BEServicioInstitucion)
            Using Conexionsss As New ProxyBase(Of IServicioInstitucion)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).ConsultarDetalleArchivoDescarga(request)
            End Using
        End Function
    End Class
End Namespace
