﻿Imports Microsoft.VisualBasic
Imports NLog

Namespace SPE.Web

    Public Class Logging

        Private Shared ReadOnly Logger As Logger = LogManager.GetCurrentClassLogger()

        Public Shared Sub LogTrace(message As String)
            Logger.Trace(message)
        End Sub

        Public Shared Sub LogDebug(message As String)
            Logger.Debug(message)
        End Sub

        Public Shared Sub LogInfo(message As String)
            Logger.Info(message)
        End Sub

        Public Shared Sub LogWarn(message As String)
            Logger.Warn(message)
        End Sub

        Public Shared Sub LogError(ex As Exception)
            Logger.Error(ex)
        End Sub

        Public Shared Sub LogError(ex As Exception, message As String)
            Logger.Error(ex, message)
        End Sub

        Public Sub LogFatal(ex As Exception, message As String)
            Logger.Fatal(ex, message)
        End Sub

    End Class
End Namespace