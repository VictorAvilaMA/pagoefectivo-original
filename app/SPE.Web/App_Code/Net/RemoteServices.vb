Imports System
Imports System.Data
Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Imports SPE.EmsambladoComun

Namespace SPE.Web

    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices


        Private _IAgenciaBancaria As SPE.EmsambladoComun.IAgenciaBancaria
        Private _IBanco As SPE.EmsambladoComun.IBanco
        Private _IFTPArchivo As SPE.EmsambladoComun.IFTPArchivo



        Private _ISPERoleProvider As SPE.EmsambladoComun.Seguridad.IRoleProvider
        Public Property ISPERoleProvider() As SPE.EmsambladoComun.Seguridad.IRoleProvider
            Get
                Return _ISPERoleProvider
            End Get
            Set(ByVal value As SPE.EmsambladoComun.Seguridad.IRoleProvider)
                _ISPERoleProvider = value
            End Set
        End Property

        Private _ISPEUsuario As SPE.EmsambladoComun.Seguridad.IUSuario
        Public Property ISPEUsuario() As SPE.EmsambladoComun.Seguridad.IUSuario
            Get
                Return _ISPEUsuario
            End Get
            Set(ByVal value As SPE.EmsambladoComun.Seguridad.IUSuario)
                _ISPEUsuario = value
            End Set
        End Property

        Private _ISPESiteMapProvider As SPE.EmsambladoComun.Seguridad.ISiteMapProvider
        Public Property ISPESiteMapProvider() As SPE.EmsambladoComun.Seguridad.ISiteMapProvider
            Get
                Return _ISPESiteMapProvider
            End Get
            Set(ByVal value As SPE.EmsambladoComun.Seguridad.ISiteMapProvider)
                _ISPESiteMapProvider = value
            End Set
        End Property

        Private _ISPEMembershipProvider As SPE.EmsambladoComun.Seguridad.IMemberShipProvider
        Public Property ISPEMembershipProvider() As SPE.EmsambladoComun.Seguridad.IMemberShipProvider
            Get
                Return _ISPEMembershipProvider
            End Get
            Set(ByVal value As SPE.EmsambladoComun.Seguridad.IMemberShipProvider)
                _ISPEMembershipProvider = value
            End Set
        End Property
        '       
        '

        Private _IAgenciaRecaudadora As SPE.EmsambladoComun.IAgenciaRecaudadora

        Public Property IAgenciaRecaudadora() As SPE.EmsambladoComun.IAgenciaRecaudadora
            Get
                Return _IAgenciaRecaudadora
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IAgenciaRecaudadora)
                _IAgenciaRecaudadora = value
            End Set
        End Property
        'wjra 12/01/2010
        Private _IConciliacion As SPE.EmsambladoComun.IConciliacion

        Public Property IConciliacion() As SPE.EmsambladoComun.IConciliacion
            Get
                Return _IConciliacion
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IConciliacion)
                _IConciliacion = value
            End Set
        End Property
        '************************************************************************************

        Private _ICliente As SPE.EmsambladoComun.ICliente
        Public Property ICliente() As SPE.EmsambladoComun.ICliente
            Get
                Return _ICliente
            End Get
            Set(ByVal value As SPE.EmsambladoComun.ICliente)
                _ICliente = value
            End Set
        End Property
        Private _IServicioEmpresa As SPE.EmsambladoComun.IServicio
        Public Property IServicioEmpresa() As SPE.EmsambladoComun.IServicio
            Get
                Return _IServicioEmpresa
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IServicio)
                _IServicioEmpresa = value
            End Set
        End Property

        Private _IEmpresaContratante As SPE.EmsambladoComun.IEmpresaContratante
        Public Property IEmpresaContratante() As SPE.EmsambladoComun.IEmpresaContratante
            Get
                Return _IEmpresaContratante
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IEmpresaContratante)
                _IEmpresaContratante = value
            End Set
        End Property

        Private _IParametro As SPE.EmsambladoComun.IParametro
        Public Property IParametro() As SPE.EmsambladoComun.IParametro
            Get
                Return _IParametro
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IParametro)
                _IParametro = value
            End Set
        End Property

        Private _IUbigeo As SPE.EmsambladoComun.IUbigeo
        Public Property IUbigeo() As SPE.EmsambladoComun.IUbigeo
            Get
                Return _IUbigeo
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IUbigeo)
                _IUbigeo = value
            End Set
        End Property

        Private _IRepresentante As SPE.EmsambladoComun.IRepresentante
        Public Property IRepresentante() As SPE.EmsambladoComun.IRepresentante
            Get
                Return _IRepresentante
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IRepresentante)
                _IRepresentante = value
            End Set
        End Property
        Private _IOrderPago As SPE.EmsambladoComun.IOrdenPago
        Public Property IOrdenPago() As SPE.EmsambladoComun.IOrdenPago
            Get
                Return _IOrderPago
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IOrdenPago)
                _IOrderPago = value
            End Set
        End Property


        Private _IEmail As SPE.EmsambladoComun.IEmail
        Public Property IEmail() As SPE.EmsambladoComun.IEmail
            Get
                Return _IEmail
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IEmail)
                _IEmail = value
            End Set
        End Property

        Private _IComun As SPE.EmsambladoComun.IComun
        Public Property IComun() As SPE.EmsambladoComun.IComun
            Get
                Return _IComun
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IComun)
                _IComun = value
            End Set
        End Property

        Private _ICaja As SPE.EmsambladoComun.ICaja
        Public Property ICaja() As SPE.EmsambladoComun.ICaja
            Get
                Return _ICaja
            End Get
            Set(ByVal value As SPE.EmsambladoComun.ICaja)
                _ICaja = value
            End Set
        End Property

        Private _IEstablecimiento As SPE.EmsambladoComun.IEstablecimiento
        Public Property IEstablecimiento() As SPE.EmsambladoComun.IEstablecimiento
            Get
                Return _IEstablecimiento
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IEstablecimiento)
                _IEstablecimiento = value
            End Set
        End Property


        Private _IComercio As SPE.EmsambladoComun.IComercio
        Public Property IComercio() As SPE.EmsambladoComun.IComercio
            Get
                Return _IComercio
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IComercio)
                _IComercio = value
            End Set
        End Property



        Private _IOperador As SPE.EmsambladoComun.IOperador
        Public Property IOperador() As SPE.EmsambladoComun.IOperador
            Get
                Return _IOperador
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IOperador)
                _IOperador = value
            End Set
        End Property


        Public Property IBanco() As SPE.EmsambladoComun.IBanco
            Get
                Return _IBanco
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IBanco)
                _IBanco = value
            End Set
        End Property


        Public Property IAgenciaBancaria() As SPE.EmsambladoComun.IAgenciaBancaria
            Get
                Return _IAgenciaBancaria
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IAgenciaBancaria)
                _IAgenciaBancaria = value
            End Set
        End Property

        Dim _IJefeProducto As SPE.EmsambladoComun.IJefeProducto
        Public Property IJefeProducto() As SPE.EmsambladoComun.IJefeProducto
            Get
                Return _IJefeProducto
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IJefeProducto)
                _IJefeProducto = value
            End Set
        End Property

        Public Property IFTPArchivo() As SPE.EmsambladoComun.IFTPArchivo
            Get
                Return _IFTPArchivo
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IFTPArchivo)
                _IFTPArchivo = value
            End Set
        End Property

        Private _IPlantilla As SPE.EmsambladoComun.IPlantilla
        Public Property IPlantilla() As SPE.EmsambladoComun.IPlantilla
            Get
                Return _IPlantilla
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IPlantilla)
                _IPlantilla = value
            End Set
        End Property

        Private _IPasarela As SPE.EmsambladoComun.IPasarela
        Public Property IPasarela() As SPE.EmsambladoComun.IPasarela
            Get
                Return _IPasarela
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IPasarela)
                _IPasarela = value
            End Set
        End Property

        'pmonzon 21/12/2013
        Private _IServicioInstitucion As SPE.EmsambladoComun.IServicioInstitucion

        Public Property IServicioInstitucion() As SPE.EmsambladoComun.IServicioInstitucion
            Get
                Return _IServicioInstitucion
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IServicioInstitucion)
                _IServicioInstitucion = value
            End Set
        End Property
        '************************************************************************************


        Public Overrides Sub DoSetupNetworkEnviroment()
            MyBase.DoSetupNetworkEnviroment()
            'Seguridad
            Me.ISPEMembershipProvider = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.Seguridad.IMemberShipProvider), Me.ServerUrl + "/" + _3Dev.FW.EmsambladoComun.ServiciosConocidos.CustomMembership), SPE.EmsambladoComun.Seguridad.IMemberShipProvider)
            Me.ISPERoleProvider = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.Seguridad.IRoleProvider), Me.ServerUrl + "/" + _3Dev.FW.EmsambladoComun.ServiciosConocidos.SqlRoleProviderServices), SPE.EmsambladoComun.Seguridad.IRoleProvider)
            Me.ISPESiteMapProvider = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.Seguridad.ISiteMapProvider), Me.ServerUrl + "/" + _3Dev.FW.EmsambladoComun.ServiciosConocidos.CustomSiteMapProviderServices), SPE.EmsambladoComun.Seguridad.ISiteMapProvider)
            Me.ISPEUsuario = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.Seguridad.IUSuario), Me.ServerUrl + "/" + _3Dev.FW.EmsambladoComun.ServiciosConocidos.UsuarioServices), SPE.EmsambladoComun.Seguridad.IUSuario)
            ''

            Me.IAgenciaRecaudadora = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IAgenciaRecaudadora), Me.ServerUrl + "/" + NombreServiciosConocidos.IAgenciaRecaudadora), SPE.EmsambladoComun.IAgenciaRecaudadora)
            Me.IServicioEmpresa = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IServicio), Me.ServerUrl + "/" + NombreServiciosConocidos.IServicio), SPE.EmsambladoComun.IServicio)
            Me.ICliente = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.ICliente), Me.ServerUrl + "/" + NombreServiciosConocidos.ICliente), SPE.EmsambladoComun.ICliente)
            Me.IEmpresaContratante = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IEmpresaContratante), Me.ServerUrl + "/" + NombreServiciosConocidos.IEmpresaContratante), SPE.EmsambladoComun.IEmpresaContratante)
            Me.IParametro = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IParametro), Me.ServerUrl + "/" + NombreServiciosConocidos.IParametro), SPE.EmsambladoComun.IParametro)
            Me.IUbigeo = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IUbigeo), Me.ServerUrl + "/" + NombreServiciosConocidos.IUbigeo), SPE.EmsambladoComun.IUbigeo)
            Me.IRepresentante = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IRepresentante), Me.ServerUrl + "/" + NombreServiciosConocidos.IRepresentante), SPE.EmsambladoComun.IRepresentante)
            Me.IOrdenPago = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IOrdenPago), Me.ServerUrl + "/" + NombreServiciosConocidos.IOrdenPago), SPE.EmsambladoComun.IOrdenPago)
            Me.IComun = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IComun), Me.ServerUrl + "/" + NombreServiciosConocidos.IComun), SPE.EmsambladoComun.IComun)
            Me.ICaja = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.ICaja), Me.ServerUrl + "/" + NombreServiciosConocidos.ICaja), SPE.EmsambladoComun.ICaja)
            '
            Me.IEmail = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IEmail), Me.ServerUrl + "/" + NombreServiciosConocidos.IEmail), SPE.EmsambladoComun.IEmail)

            'FASE II
            '---------------------------

            Me.IEstablecimiento = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IEstablecimiento), Me.ServerUrl + "/" + NombreServiciosConocidos.IEstablecimiento), SPE.EmsambladoComun.IEstablecimiento)
            Me.IComercio = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IComercio), Me.ServerUrl + "/" + NombreServiciosConocidos.IComercio), SPE.EmsambladoComun.IComercio)
            Me.IOperador = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IOperador), Me.ServerUrl + "/" + NombreServiciosConocidos.IOperador), SPE.EmsambladoComun.IOperador)
            Me.IAgenciaBancaria = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IAgenciaBancaria), Me.ServerUrl + "/" + NombreServiciosConocidos.IAgenciaBancaria), SPE.EmsambladoComun.IAgenciaBancaria)
            Me.IBanco = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IBanco), Me.ServerUrl + "/" + NombreServiciosConocidos.IBanco), SPE.EmsambladoComun.IBanco)
            Me.IJefeProducto = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IJefeProducto), Me.ServerUrl + "/" + NombreServiciosConocidos.IJefeProducto), SPE.EmsambladoComun.IJefeProducto)

            Me.IFTPArchivo = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IFTPArchivo), Me.ServerUrl + "/" + NombreServiciosConocidos.IFTPArchivo), SPE.EmsambladoComun.IFTPArchivo)

            Me.IPlantilla = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IPlantilla), Me.ServerUrl + "/" + NombreServiciosConocidos.IPlantilla), SPE.EmsambladoComun.IPlantilla)
            '*********************************************************************************************************************************************************************************************
            'wjra 14/01/2011
            Me.IConciliacion = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IConciliacion), Me.ServerUrl + "/" + NombreServiciosConocidos.IConciliacion), SPE.EmsambladoComun.IConciliacion)
            '*********************************************************************************************************************************************************************************************
            ' pasarela
            Me.IPasarela = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IPasarela), Me.ServerUrl + "/" + NombreServiciosConocidos.IPasarela), SPE.EmsambladoComun.IPasarela)

            'servicioinstitucion pmonzon 21/12/2013
            Me.IServicioInstitucion = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IServicioInstitucion), Me.ServerUrl + "/" + NombreServiciosConocidos.IServicioInsitucion), SPE.EmsambladoComun.IServicioInstitucion)

        End Sub

    End Class

End Namespace
