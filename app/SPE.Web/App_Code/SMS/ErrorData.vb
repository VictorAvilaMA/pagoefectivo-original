﻿Imports Newtonsoft.Json

Namespace SPE.Web.SMS
    Public Class ErrorData
        <JsonProperty(PropertyName:="code", Required:=Required.Always)>
        Public Property Code As Integer

        <JsonProperty(PropertyName:="message", Required:=Required.Always)>
        Public Property Message As String

        <JsonProperty(PropertyName:="field", Required:=Required.Always)>
        Public Property Field As String
    End Class
End Namespace