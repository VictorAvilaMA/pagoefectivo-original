﻿Imports Microsoft.VisualBasic
Imports Newtonsoft.Json

Namespace SPE.Web.SMS
    Public Class RequestMessageApi
        <JsonProperty(PropertyName:="to", Required:=Required.Always)>
        Public Property CellPhoneNumber As String

        <JsonProperty(PropertyName:="message", Required:=Required.Always)>
        Public Property Message As String
    End Class
End Namespace