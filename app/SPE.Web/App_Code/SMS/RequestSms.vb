﻿Imports Newtonsoft.Json

Namespace SPE.Web.SMS
    Public Class RequestSms
        <JsonProperty(PropertyName:="countrycode", Required:=Required.Always)>
        Public Property CountryCode As String

        <JsonProperty(PropertyName:="cellphonenumber", Required:=Required.Always)>
        Public Property CellPhoneNumber As String

        <JsonProperty(PropertyName:="cip", Required:=Required.Always)>
        Public Property Cip As String
    End Class
End Namespace