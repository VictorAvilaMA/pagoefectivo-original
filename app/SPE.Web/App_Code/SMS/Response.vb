﻿Namespace SPE.Web.SMS
    Public Enum ResponseCodeSms
        Ok = 100
        BadRequest = 111
        InternalError = 166
        Requerid = 112
        InvalidValue = 113
        Expirated = 706
    End Enum

    Public Class ResponseMessageSms
        Public Const Success As String = "Solicitud Exitosa."
        Public Const BadRequest As String = "Solicitud con datos inválidos."
        Public Const InternalError As String = "Error interno del servidor."
        Public Const Requerid As String = "Requerido."
        Public Const InvalidValue As String = "Valor invalido."
        Public Const Expirated As String = "Código con estado expirado."
    End Class
End Namespace