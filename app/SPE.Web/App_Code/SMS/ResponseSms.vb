﻿Imports Newtonsoft.Json

Namespace SPE.Web.SMS
    Public Class ResponseSms
        <JsonProperty(PropertyName:="code", Required:=Required.Always)>
        Public Property Code As Integer

        <JsonProperty(PropertyName:="message", Required:=Required.Always)>
        Public Property Message As String

        <JsonProperty(PropertyName:="data", Required:=Required.Always)>
        Public Property Data As Object
    End Class
End Namespace