Imports Microsoft.VisualBasic
Imports _3Dev.FW.Web
Namespace SPE.Web.Seguridad


    Public Class SPEMemberShip
        Inherits _3Dev.FW.Web.CustomMemberShip
        Public Overrides Function GetConstructorProviderClient() As _3Dev.FW.Web.CustomMembershipProviderClient
            Return New SPEMemberShipProviderClient()
        End Function


    End Class
End Namespace