Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun.Seguridad
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Namespace SPE.Web.Seguridad


    Public Class SPEMemberShipProviderClient
        Inherits _3Dev.FW.Web.CustomMembershipProviderClient
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.Seguridad.ICustomMembershipProvider
            Get
                Dim Conexionsss As New ProxyBase(Of IMemberShipProvider)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
    End Class
End Namespace