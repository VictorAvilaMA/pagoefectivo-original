Imports Microsoft.VisualBasic
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Namespace SPE.Web.Seguridad


    Public Class SPERoleProviderClient
        Inherits _3Dev.FW.Web.SqlRoleProviderClient
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.Seguridad.ISqlRoleProvider

            Get
                'Using Conexionsss As New ProxyBase(Of SPE.EmsambladoComun.Seguridad.IRoleProvider)
                Dim Conexionsss As New ProxyBase(Of SPE.EmsambladoComun.Seguridad.IRoleProvider)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
                'End Using
            End Get
        End Property
    End Class
End Namespace