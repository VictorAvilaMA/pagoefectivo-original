Imports Microsoft.VisualBasic
Imports System.Web

Namespace SPE.Web.Seguridad


    Public Class SPESiteMapProvider
        Inherits _3Dev.FW.Web.CustomSiteMapProvider
        Public Overrides Function GetConstructorProviderClient() As _3Dev.FW.Web.CustomSiteMapProviderClient
            Return New SPESiteMapProviderClient()
        End Function

        Public Sub New()
            Me.IdSistema = SPE.EmsambladoComun.ParametrosSistema.IdSistemaPagoEfectivo
        End Sub
        Public url As String = ""
        Public Function EvaluaSiteMapNode(ByVal obeNode As _3Dev.FW.Entidades.Seguridad.BERolPagina) As Boolean
            Return obeNode.Url = url
        End Function
        Dim PredicateSiteMapNode As New Predicate(Of _3Dev.FW.Entidades.Seguridad.BERolPagina)(AddressOf EvaluaSiteMapNode)


        'Public Overrides Sub OnBeforeToBuildSiteMap()
        '    MyBase.OnBeforeToBuildSiteMap()
        '    If (CType(_3Dev.FW.Web.Security.UserInfo, SPE.Entidades.BEUserInfo).EsJefeProducto) Then
        '        _nodes.Clear()
        '        Clear()
        '    End If
        'End Sub
        Public Overrides Function AllowToAddSiteMapNode(ByVal node As SiteMapNode) As Boolean

            If _3Dev.FW.Web.Security.UserInfo.EsJefeProducto Then
                url = node.Url
                Return Not (_3Dev.FW.Web.Security.UserInfo.PaginasAcceso.Find(PredicateSiteMapNode) Is Nothing)
            ElseIf _3Dev.FW.Web.Security.UserInfo.Rol = "Cliente" And Not _3Dev.FW.Web.Security.UserInfo.FlagRegistrarPc Then
                url = node.Url
                Return Not (_3Dev.FW.Web.Security.UserInfo.PaginasAcceso.Find(PredicateSiteMapNode) Is Nothing)
            ElseIf (_3Dev.FW.Web.Security.UserInfo.Rol = "Cliente" And Not _3Dev.FW.Web.Security.UserInfo.HabilitarMonedero) Then
                url = node.Url
                Return Not (_3Dev.FW.Web.Security.UserInfo.PaginasAcceso.Find(PredicateSiteMapNode) Is Nothing)
            Else
                Return True
            End If



            'If (_3Dev.FW.Web.Security.UserInfo.EsJefeProducto Or Not _3Dev.FW.Web.Security.UserInfo.FlagRegistrarPc) Then
            '    '  Or Not _3Dev.FW.Web.Security.UserInfo.HabilitarMonedero Then
            '    url = node.Url
            '    Return Not (_3Dev.FW.Web.Security.UserInfo.PaginasAcceso.Find(PredicateSiteMapNode) Is Nothing)
            'Else
            '    Return True
            'End If
        End Function
    End Class
End Namespace