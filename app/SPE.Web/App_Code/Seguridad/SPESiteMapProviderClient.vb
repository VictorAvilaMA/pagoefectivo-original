Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun
Imports _3Dev.FW.EmsambladoComun

Namespace SPE.Web.Seguridad


    Public Class SPESiteMapProviderClient
        Inherits _3Dev.FW.Web.CustomSiteMapProviderClient
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.Seguridad.ICustomSiteMapProvider
            Get
                Dim Conexionsss As New ProxyBase(Of SPE.EmsambladoComun.Seguridad.ISiteMapProvider)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
    End Class
End Namespace