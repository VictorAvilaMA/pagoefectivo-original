Imports Microsoft.VisualBasic
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Namespace SPE.Web.Seguridad


    Public Class SPEUsuarioClient
        Inherits _3Dev.FW.Web.UsuarioClient
        Public Overrides ReadOnly Property RemoteServicesInterfaceObject() As _3Dev.FW.EmsambladoComun.Seguridad.IUsuario
            Get
                Dim Conexionsss As New ProxyBase(Of SPE.EmsambladoComun.Seguridad.IUSuario)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver())
            End Get
        End Property
    End Class
End Namespace