﻿Imports Microsoft.VisualBasic

Public Class Util
    
End Class

Public Class MensajeInformacion
    Public Const MensajeOK = "La operacion ser registro correctamente"
    Public Function MensajeOkConOperacion(ByVal NroOPeracion As String) As String
        Return String.Format("La operación nro: {0} se registró correctamente", NroOPeracion)
    End Function
    Public Function MensajeResultadoReportePS(ByVal Servicios As String, ByVal FechaInicio As DateTime, ByVal FechaFin As DateTime, ByVal flag As Integer) As String
        Select Case flag
            Case 0
                Return String.Format("Consulta de Transacciones Pagadas {0} Desde {1} Al {2}", Servicios, FechaInicio.ToShortDateString, FechaFin.ToShortDateString)
            Case 1
                Return String.Format("Consulta de número de transacciones pagadas {0} Desde {1} Al {2}", Servicios, FechaInicio.ToShortDateString, FechaFin.ToShortDateString)
        End Select
    End Function
End Class
Public Class MensajesError
    Public Const MensajeTransaccionPendiente = "No existen Pagos Conciliados en el rango de fechas seleccionado"
    Public Const MensajeTransaccionRango = "Al crear los registros de liquidación, los intervalos de fecha no deben cruzarse"
    Public Const MensajeTransaccionUpdateLiquidado = "Solo puede consultar registros con estado liquidado"
    Public Const MensajeFechaAMayor = "La fecha no puede ser mayor a la fecha actual"
    Public Const MensajeSeleccionados = "No ha seleccionado ninguna empresa para liquidar"
    Public Const MensajeSeleccionadosDet = "No ha seleccionado ninguna empresa para ver su detalle"
End Class
Public Class MensajesValidacion
    Public Const MensajeMonedaReq = "Seleccione el tipo de moneda"
    Public Const MensajeEmpresaReq = "Seleccione la empresa contratante"
    Public Const MensajePeriodoReq = "Seleccione el periodo de liquidación"
    Public Function MensajeResultadoReportePS(ByVal FechaInicio As DateTime, ByVal FechaFin As DateTime) As String
        Return String.Format("No se encontraron registros Desde {0} a {1}", FechaInicio.ToShortDateString, FechaFin.ToShortDateString)
    End Function



End Class

Public Structure T_ReportesExportarNombre
    Public Const ReportesPath As String = "~\Reportes\"
    'public const string ReporteCompPago = "RptBolPagEfect.rdlc";

End Structure

'Tagueo DAX
Public Class URLDAX
    Private _URLTag As String
    Public Property URLTag() As String
        Get
            Return _URLTag
        End Get
        Set(ByVal value As String)
            _URLTag = value
        End Set
    End Property


    Private _NombreDax As String
    Public Property NombreDax() As String
        Get
            Return _NombreDax
        End Get
        Set(ByVal value As String)
            _NombreDax = value
        End Set
    End Property
End Class

Public Class ServicioDAX

    Private _NombreServicio As String
    Public Property NombreServicio() As String
        Get
            Return _NombreServicio
        End Get
        Set(ByVal value As String)
            _NombreServicio = value
        End Set
    End Property

    Private _NombreSiteTag As String
    Public Property NombreSiteTag() As String
        Get
            Return _NombreSiteTag
        End Get
        Set(ByVal value As String)
            _NombreSiteTag = value
        End Set
    End Property

    Private _NombreDax As String
    Public Property NombreDax() As String
        Get
            Return _NombreDax
        End Get
        Set(ByVal value As String)
            _NombreDax = value
        End Set
    End Property
End Class