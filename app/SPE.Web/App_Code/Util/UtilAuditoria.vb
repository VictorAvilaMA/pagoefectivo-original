﻿Imports Microsoft.VisualBasic
Imports System

Public Class UtilAuditoria
    Public Shared Function GetIPAddress(ByVal page As Page) As String
        Try
            Dim hostEntry As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(page.Request.ServerVariables("REMOTE_ADDR"))
            Dim ipAdressArray As System.Net.IPAddress() = hostEntry.AddressList
            Return ipAdressArray(ipAdressArray.Length - 1).ToString()
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Shared Function GetHostName(ByVal page As Page) As String
        Try
            Dim hostEntry As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(page.Request.ServerVariables("REMOTE_ADDR"))
            Return hostEntry.HostName
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
End Class
