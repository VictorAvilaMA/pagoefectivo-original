Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Namespace SPE.Web.Util


    Public Class UtilBarCode

        Public Shared Sub GenerarCodigoBarra(ByVal numero As String)
            Dim barSize As Integer = 40
            Dim FontFamilyName As String
            Dim FontFileName As String
            Dim barCode As New BarCodeClass.Barcode()
            Dim flag As Integer = 0
            FontFamilyName = System.Configuration.ConfigurationManager.AppSettings("BarCodeFontFamily")
            FontFileName = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings("BarCodeFontFile")
            Try
                Dim imgBarcode As System.Byte()
                imgBarcode = barCode.Code39(numero, barSize, False, "", FontFamilyName, FontFileName)
                flag = 1
                HttpContext.Current.Response.ContentType = ""
                HttpContext.Current.Response.Expires = 0
                HttpContext.Current.Response.Buffer = True
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.BinaryWrite(imgBarcode)
                HttpContext.Current.Response.End()
            Catch ex As Exception
                If flag = 0 Then
                    HttpContext.Current.Response.Redirect("../PError404.aspx")
                    HttpContext.Current.Response.End()
                End If
            End Try


        End Sub

    End Class
End Namespace