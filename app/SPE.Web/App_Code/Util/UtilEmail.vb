Imports Microsoft.VisualBasic



Imports System.Net.Mail
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun

Namespace SPE.Web.Util

    Public Class UtilEmail

        Public Sub New()

        End Sub

        Public Function EnviarEmailGeneral(ByVal phost As String, ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarEmailGeneral(phost, pfrom, pto, psubject, pbody, pisBodyHtml)
            End Using
        End Function

        Public Function EnviarEmail(ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarEmail(pfrom, pto, psubject, pbody, pisBodyHtml)
            End Using
        End Function

        Public Function EnviarCorreoConfirmacionUsuario(ByVal userId As Integer, ByVal nombrecliente As String, ByVal clave As String, ByVal pguid As String, ByVal pto As String) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarCorreoConfirmacionUsuario(userId, nombrecliente, clave, pguid, pto)
            End Using

        End Function

        Public Function EnviarCorreoAAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarCorreoAAdministrador(pfrom, psubject, pbody)
            End Using
        End Function

        'agregado
        Public Function EnviarCorreoContactarAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarCorreoContactarAdministrador(pfrom, psubject, pbody)
            End Using
        End Function

        Public Function EnviarEmailContactenos(ByVal obeMail As SPE.Entidades.BEMailContactenos) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarEmailContactenos(obeMail)
            End Using
        End Function
        'jeffersonmendoza
        Public Function EnviarEmailContactenosEmpresa(ByVal obeMail As SPE.Entidades.BEMailContactenosEmpresa) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarEmailContactenosEmpresa(obeMail)
            End Using
        End Function

        Public Function EnviarCorreoSuscriptor(ByVal EmailSuscriptor As String) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarCorreoSuscriptor(EmailSuscriptor)
            End Using
        End Function

#Region "ToGenerado"
        Public Function EnviarEmailCambioDeEstadoCipDeExpAGen(ByVal oBEOrdenPago As SPE.Entidades.BEOrdenPago) As Integer
            Using Conexionsss As New ProxyBase(Of IEmail)
                Return Conexionsss.DevolverContrato(New EntityBaseContractResolver()).EnviarEmailCambioDeEstadoCipDeExpAGen(oBEOrdenPago)
            End Using
        End Function
#End Region

    End Class

End Namespace