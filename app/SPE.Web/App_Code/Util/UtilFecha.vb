Imports Microsoft.VisualBasic
Imports System.Web.UI.WebControls
Imports System.Web.UI

Namespace SPE.Web.Util

    Public Class UtilFecha
        '
        Public Shared Function ValidarFecha(ByVal fecha As String) As Boolean
            If Not IsDate(fecha) Then
                Return False
            Else
                Return True
            End If
        End Function
        '
        Public Shared Function ValidarFecha(ByVal txtFecha1 As TextBox, ByVal txtFecha2 As TextBox) As Boolean
            If Not IsDate(txtFecha1.Text) OrElse _
               Not IsDate(txtFecha2.Text) Then
                Return False
            Else
                Return True
            End If
        End Function
        '
    End Class

End Namespace

