Imports Microsoft.VisualBasic
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System

Namespace SPE.Web.Util

    Public Class UtilFormatGridView
        '
        'BackGroundGridView
        Public Shared Sub BackGroundGridView(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs, ByVal columnaField As String, ByVal descripcionEstado As String, ByVal backColor As String)
            '
            If e.Row.RowType = DataControlRowType.DataRow Then
                'determine the value of the estado field
                Dim estado As String = _
                  Convert.ToString(DataBinder.Eval(e.Row.DataItem, _
                  columnaField))
                If estado = descripcionEstado Then
                    ' color the background of the row yellow
                    e.Row.Style("background-color") = backColor
                End If
                '
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                '
            End If
            '
        End Sub

        'GridViewFooter
        Public Shared Sub ContadorRegistroGridView(ByVal objLabel As Label, ByVal rowCount As Integer)
            '
            If Not (objLabel Is Nothing) Then
                If rowCount = 0 Then
                    objLabel.Text = "Resultados:"
                ElseIf rowCount = 1 Then
                    objLabel.Text = "Resultados: " + rowCount.ToString() + " registro."
                Else
                    objLabel.Text = "Resultados: " + rowCount.ToString() + " registros."
                End If

            End If

            '
        End Sub
        Public Shared Sub ContadorRegistroGridView(ByVal objLabel As Literal, ByVal rowCount As Integer)
            '
            If Not (objLabel Is Nothing) Then
                If rowCount = 0 Then
                    objLabel.Text = "Resultados:"
                ElseIf rowCount = 1 Then
                    objLabel.Text = "Resultados: " + rowCount.ToString() + " registro."
                Else
                    objLabel.Text = "Resultados: " + rowCount.ToString() + " registros."
                End If

            End If

            '
        End Sub
        Public Shared Function ObjectDecimalToStringFormatMiles(ByVal obj As Object) As String
            Return _3Dev.FW.Util.DataUtil.ObjectToDecimal(obj).ToString("#,#0.00")
        End Function

        Public Shared Function DatetimeToStringDate(ByVal obj As Object) As String
            Dim resultado As String = ""
            If (CType(obj, DateTime) = DateTime.MinValue) Then
                Return ""
            Else : Return CType(obj, DateTime).ToShortDateString
            End If
        End Function

        Public Shared Function DatetimeToStringDateandTime(ByVal obj As Object) As String
            Dim resultado As String = ""
            If (CType(obj, DateTime) = DateTime.MinValue) Then
                Return ""
            Else : Return CType(obj, DateTime).ToString
            End If
        End Function

        Public Sub New()

        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub
    End Class




End Namespace

