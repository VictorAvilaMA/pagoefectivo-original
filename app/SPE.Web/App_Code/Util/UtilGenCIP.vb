Imports Microsoft.VisualBasic
Imports SPE.Entidades
Imports System.Text

Namespace SPE.Web.Util




    Public Class UtilGenCIP

        Public Const __UrlTramaPre As String = "__UrlTramaPre"
        Public Const __UrlTrama As String = "__UrlTrama"
        Public Const __UrlObtCIPInfoByUrLServRequest As String = "__UrlObtCIPInfoByUrLServRequest"

        Public Const __XmlTramaPre As String = "__XmlTramaPre"
        Public Const __XmlTrama As String = "__XmlTrama"
        Public Const __XMLObtCIPInfoByUrLServRequest As String = "__XMLObtCIPInfoByUrLServRequest"

        Public Const __UrlServicioPre As String = "__UrlServicioPre"
        Public Const __UrlServicio As String = "__UrlServicio"


        Public Const __CNTUrlTrama As String = "__CNTUrlTrama"
        Public Const __CNTObtCIPInfoByUrLServRequest As String = "__CNTObtCIPInfoByUrLServRequest"
        Public Const __CNTUrlServicio As String = "__CNTUrlServicio"
        Public Const __CNTResponseGenCIP As String = "__CNTResponseGenCIP"


        Public Shared Function GenerarUrlStrParaVerPopupCIP(ByVal obeOrdenPago As BEOrdenPago, ByVal obeOcultarEmpresa As BEOcultarEmpresa, ByVal urlServicio As String) As String
            Dim sbqueryString As New StringBuilder
            sbqueryString.Append("javascript:AbrirPopPup('PrGeOrPaPopPup.aspx?")
            sbqueryString.Append("orderid=") : sbqueryString.Append(obeOrdenPago.OrderIdComercio)
            sbqueryString.Append("&urlok=") : sbqueryString.Append(obeOrdenPago.UrlOk)
            sbqueryString.Append("&urlerror=") : sbqueryString.Append(obeOrdenPago.UrlError)
            sbqueryString.Append("&mailcom=") : sbqueryString.Append(obeOrdenPago.MailComercio)
            sbqueryString.Append("&orderdescripcion=") : sbqueryString.Append(obeOrdenPago.ConceptoPago)
            sbqueryString.Append("&moneda=") : sbqueryString.Append(obeOrdenPago.IdMoneda)
            sbqueryString.Append("&monto=") : sbqueryString.Append(obeOrdenPago.Total)
            ''control de cambio
            sbqueryString.Append("&OcultarEmpresa=") : sbqueryString.Append(obeOcultarEmpresa.OcultarEmpresa)
            sbqueryString.Append("&OcultarImagenEmpresa=") : sbqueryString.Append(obeOcultarEmpresa.OcultarImagenEmpresa)
            sbqueryString.Append("&ECDesc=") : sbqueryString.Append(obeOcultarEmpresa.EmpresaContratanteDescripcion)
            sbqueryString.Append("&OcultarImgEmpresa=") : sbqueryString.Append(obeOcultarEmpresa.OcultarImagenEmpresa)
            sbqueryString.Append("&OcultarImgServicio=") : sbqueryString.Append(obeOcultarEmpresa.OcultarImagenServicio)

            sbqueryString.Append("&idservicio=") : sbqueryString.Append(obeOcultarEmpresa.idServicio)
            sbqueryString.Append("&idempresa=") : sbqueryString.Append(obeOcultarEmpresa.idEmpresaContratante)
            ''
            sbqueryString.Append("&fechaEmision=") : sbqueryString.Append(obeOrdenPago.FechaEmision.ToString)
            ''
            sbqueryString.Append("&numeroOrden=") : sbqueryString.Append(obeOrdenPago.NumeroOrdenPago)
            sbqueryString.Append("&urlServicio=") : sbqueryString.Append(urlServicio)
            sbqueryString.Append("','Vista_Previa_De_Impresion',450,550);")
            Return sbqueryString.ToString()
        End Function
    End Class
End Namespace