﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.IO
Imports System.Net
Imports System


''' <summary>
''' Summary description for UtilReporte
''' </summary>
Public Class UtilManagerFTP
    '
    ' TODO: Add constructor logic here
    '
    Public Sub New()
    End Sub

    'string ftpServerIP = "192.168.1.101", ftpUserID = "webmaster", ftpPassword = "webmaster";


    Public Sub UploadFile(ByVal ArchivoStream As Stream, ByVal _UploadPath As String, ByVal _FTPUser As String, ByVal _FTPPass As String)

        ' Create FtpWebRequest object from the Uri provided
        Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(_UploadPath)), System.Net.FtpWebRequest)

        ' Provide the WebPermission Credintials
        _FtpWebRequest.Credentials = New System.Net.NetworkCredential(_FTPUser, _FTPPass)

        ' By default KeepAlive is true, where the control connection is not closed
        ' after a command is executed.
        _FtpWebRequest.KeepAlive = False

        ' set timeout for 20 seconds
        _FtpWebRequest.Timeout = 20000

        ' Specify the command to be executed.
        _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile
        ' Specify the data transfer type.
        _FtpWebRequest.UseBinary = True

        ' Notify the server about the size of the uploaded file
        _FtpWebRequest.ContentLength = ArchivoStream.Length

        ' The buffer size is set to 2kb
        Dim buffLength As Integer = 2048
        Dim buff(buffLength - 1) As Byte



        Try

            ' Stream to which the file to be upload is written
            Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()

            ' Read from the file stream 2kb at a time
            Dim contentLen As Integer = ArchivoStream.Read(buff, 0, buffLength)

            ' Till Stream content ends
            Do While contentLen <> 0
                ' Write Content from the file stream to the FTP Upload Stream
                _Stream.Write(buff, 0, contentLen)
                contentLen = ArchivoStream.Read(buff, 0, buffLength)
            Loop

            ' Close the file stream and the Request Stream
            _Stream.Close()
            _Stream.Dispose()
            ArchivoStream.Close()
            ArchivoStream.Dispose()

        Catch ex As Exception


        End Try

    End Sub



    Public Function SubirArchivo(ByVal ArchivoStream As Stream, ByVal rutaFTP As String, ByVal usuarioFTP As String, ByVal passwordFTP As String, ByVal nombreArchivo As String) As String
        'DateTime fechaActual = DateTime.Now;
        Dim uri As String = "ftp://" & rutaFTP & "/" & nombreArchivo

        Dim reqFTP As FtpWebRequest

        reqFTP = DirectCast(FtpWebRequest.Create(New Uri(uri)), FtpWebRequest)
        reqFTP.Credentials = New NetworkCredential(usuarioFTP, passwordFTP)


        reqFTP.KeepAlive = False
        reqFTP.Method = WebRequestMethods.Ftp.UploadFile
        reqFTP.UseBinary = True
        reqFTP.ContentLength = ArchivoStream.Length
        reqFTP.UsePassive = True
        Dim buffLength As Integer = 2048
        Dim buff As Byte() = New Byte(buffLength - 1) {}
        Dim contentLen As Integer

        Dim fs As FileStream = ArchivoStream

        Dim strm As Stream = Nothing
        Try
            strm = reqFTP.GetRequestStream()
            contentLen = fs.Read(buff, 0, buffLength)
            While contentLen <> 0
                strm.Write(buff, 0, contentLen)
                contentLen = fs.Read(buff, 0, buffLength)
            End While

            strm.Close()
            fs.Close()
            Return nombreArchivo
        Catch ex As Exception
            strm.Close()
            fs.Close()
            Return Nothing
        End Try



    End Function


    ''' <summary>
    ''' Método para subir Archivo a Servidor FTP
    ''' </summary>
    ''' <param name="rutaArchivoAsubir">Ruta del Archivo local a subir al Servidor FTP</param>
    ''' <param name="rutaFTP">Ruta FTP donde se desea subir el archivo local, formato: 'xxx.xxx.xxx.xxx/carpetaFtp'</param>
    ''' <param name="usuarioFTP">Usuario del Servidor FTP</param>
    ''' <param name="passwordFTP">Password del Servidor FTP</param>
    ''' <returns>Retorna el nombre del Archivo generado en el servidor FTP</returns>
    Public Function SubirArchivo(ByVal rutaArchivoAsubir As String, ByVal rutaFTP As String, ByVal usuarioFTP As String, ByVal passwordFTP As String, ByVal nombreArchivo As String) As String
        'DateTime fechaActual = DateTime.Now;
        Dim fileInf As New FileInfo(rutaArchivoAsubir)
        Dim uri As String = "ftp://" & rutaFTP & "/" & fileInf.Name
        'string nombreArchivo = string.Concat(fileInf.Name.Substring(0, fileInf.Name.Length - fileInf.Extension.Length), "_", fechaActual.ToString("dd-MM-yyyy_hh-mm-ss"))+fileInf.Extension;

        Dim reqFTP As FtpWebRequest

        reqFTP = DirectCast(FtpWebRequest.Create(New Uri("ftp://" & rutaFTP & "/" & nombreArchivo)), FtpWebRequest)
        reqFTP.Credentials = New NetworkCredential(usuarioFTP, passwordFTP)

        reqFTP.KeepAlive = False
        reqFTP.Method = WebRequestMethods.Ftp.UploadFile
        reqFTP.UseBinary = True
        reqFTP.ContentLength = fileInf.Length
        reqFTP.UsePassive = True
        Dim buffLength As Integer = 2048
        Dim buff As Byte() = New Byte(buffLength - 1) {}
        Dim contentLen As Integer

        Dim fs As FileStream = fileInf.OpenRead()
        fileInf = Nothing
        Dim strm As Stream = Nothing
        Try
            strm = reqFTP.GetRequestStream()
            contentLen = fs.Read(buff, 0, buffLength)
            While contentLen <> 0
                strm.Write(buff, 0, contentLen)
                contentLen = fs.Read(buff, 0, buffLength)
            End While

            strm.Close()
            fs.Close()
            Return nombreArchivo
        Catch ex As Exception
            strm.Close()
            fs.Close()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Método para subir Archivo a Servidor FTP
    ''' </summary>
    ''' <param name="rutaArchivoAsubir">Ruta del Archivo local a subir al Servidor FTP</param>
    ''' <param name="rutaFTP">Ruta FTP donde se desea subir el archivo local, formato: 'xxx.xxx.xxx.xxx/carpetaFtp'</param>
    ''' <param name="usuarioFTP">Usuario del Servidor FTP</param>
    ''' <param name="passwordFTP">Password del Servidor FTP</param>
    ''' <returns>Retorna el nombre del Archivo generado en el servidor FTP</returns>
    Public Function SubirArchivo(ByVal rutaArchivoAsubir As String, ByVal nombreFileEnFTP As String, ByVal rutaFTP As String, ByVal usuarioFTP As String, ByVal passwordFTP As String, ByVal band As Boolean) As Object()
        Dim result As Object() = New Object(1) {}
        Dim fechaActual As DateTime = DateTime.Now
        Dim fileInf As New FileInfo(rutaArchivoAsubir)
        Dim uri As String = "ftp://" & rutaFTP & "/" & nombreFileEnFTP
        Dim nombreArchivo As String = nombreFileEnFTP
        Dim reqFTP As FtpWebRequest

        reqFTP = DirectCast(FtpWebRequest.Create(New Uri("ftp://" & rutaFTP & "/" & nombreArchivo)), FtpWebRequest)
        reqFTP.Credentials = New NetworkCredential(usuarioFTP, passwordFTP)

        reqFTP.KeepAlive = False
        reqFTP.Method = WebRequestMethods.Ftp.UploadFile
        reqFTP.UseBinary = True
        reqFTP.ContentLength = fileInf.Length
        reqFTP.UsePassive = True
        result(0) = nombreFileEnFTP
        result(1) = fileInf.Length
        Dim buffLength As Integer = 2048
        Dim buff As Byte() = New Byte(buffLength - 1) {}
        Dim contentLen As Integer

        Dim fs As FileStream = fileInf.OpenRead()
        fileInf = Nothing
        Dim strm As Stream = Nothing
        Try
            strm = reqFTP.GetRequestStream()
            contentLen = fs.Read(buff, 0, buffLength)
            While contentLen <> 0
                strm.Write(buff, 0, contentLen)
                contentLen = fs.Read(buff, 0, buffLength)
            End While

            strm.Close()
            fs.Close()
            Return result
        Catch ex As Exception
            strm.Close()
            fs.Close()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Método para descargar Archivo de Servidor FTP
    ''' </summary>
    ''' <param name="rutaDondeDescargar">Ruta local donde se desea descargar Archivo FTP.</param>
    ''' <param name="nombreArchivoFTP">Nombre del Archivo FTP que se desea descargar.</param>
    ''' <param name="rutaFTP">Ruta FTP donde se desea descargar el archivo, formato: 'xxx.xxx.xxx.xxx/carpetaFtp'</param>
    ''' <param name="usuarioFTP">Usuario del Servidor FTP</param>
    ''' <param name="passwordFTP">Password del Servidor FTP</param>
    Public Sub DescargarArchivo(ByVal rutaDondeDescargar As String, ByVal nombreArchivoFTP As String, ByVal rutaFTP As String, ByVal usuarioFTP As String, ByVal passwordFTP As String)
        Dim reqFTP As FtpWebRequest
        Try
            'rutaDondeDescargar = "C:\Users\asegucal\Desktop\FTPFiles"
            If Not nombreArchivoFTP = String.Empty Then
                Dim outputStream As New FileStream(rutaDondeDescargar & "\" & nombreArchivoFTP, FileMode.Create)

                'reqFTP = DirectCast(FtpWebRequest.Create(New Uri("ftp://" & rutaFTP & "/" & nombreArchivoFTP)), FtpWebRequest)
                reqFTP = DirectCast(FtpWebRequest.Create(New Uri(rutaFTP & nombreArchivoFTP)), FtpWebRequest)
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                reqFTP.UseBinary = True
                reqFTP.UsePassive = True
                reqFTP.Credentials = New NetworkCredential(usuarioFTP, passwordFTP)

                Dim response As FtpWebResponse = DirectCast(reqFTP.GetResponse(), FtpWebResponse)

                Dim ftpStream As Stream = response.GetResponseStream()
                Dim cl As Long = response.ContentLength
                Dim bufferSize As Integer = 2048
                Dim readCount As Integer
                Dim buffer As Byte() = New Byte(bufferSize - 1) {}

                readCount = ftpStream.Read(buffer, 0, bufferSize)
                While readCount > 0
                    outputStream.Write(buffer, 0, readCount)
                    readCount = ftpStream.Read(buffer, 0, bufferSize)
                End While

                ftpStream.Close()
                outputStream.Close()
                response.Close()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    ''' <summary>
    ''' Método para descargar Archivo de Servidor FTP
    ''' </summary>
    ''' <param name="nombreArchivoFTP">Nombre del Archivo FTP que se desea descargar.</param>
    ''' <param name="rutaFTP">Ruta FTP donde se desea descargar el archivo, formato: 'xxx.xxx.xxx.xxx/carpetaFtp'</param>
    ''' <param name="usuarioFTP">Usuario del Servidor FTP</param>
    ''' <param name="passwordFTP">Password del Servidor FTP</param>
    Public Function DescargarArchivo(ByVal nombreArchivoFTP As String, ByVal rutaFTP As String, ByVal usuarioFTP As String, ByVal passwordFTP As String) As Byte()
        Dim outputStream As New MemoryStream()
        Try
            'Dim uri As String = "ftp://"*/ & rutaFTP & "/" & nombreArchivoFTP
            Dim uri As String = rutaFTP & nombreArchivoFTP


            Dim request As FtpWebRequest = DirectCast(FtpWebRequest.Create(New Uri(uri)), FtpWebRequest)
            request.Method = WebRequestMethods.Ftp.DownloadFile
            request.UseBinary = True
            request.UsePassive = True
            request.Credentials = New NetworkCredential(usuarioFTP, passwordFTP)
            Dim response As FtpWebResponse = DirectCast(request.GetResponse(), FtpWebResponse)
            Dim strm As Stream = response.GetResponseStream()
            Dim cl As Long = response.ContentLength
            Dim bufferSize As Integer = 2048
            Dim readCount As Integer
            Dim buffer As Byte() = New Byte(bufferSize - 1) {}
            readCount = strm.Read(buffer, 0, bufferSize)

            While readCount > 0
                outputStream.Write(buffer, 0, readCount)
                readCount = strm.Read(buffer, 0, bufferSize)
            End While
            strm.Close()
            outputStream.Close()
            response.Close()
        Catch ex As WebException
            Console.WriteLine("Ocurrio un error :" & Convert.ToString(ex))
        End Try

        Return outputStream.GetBuffer()
    End Function
End Class