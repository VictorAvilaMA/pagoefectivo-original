Imports Microsoft.VisualBasic
Imports System.Xml
Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Util
Imports System



Public Class UtilOrdenPagoString

    Dim _xmldoc As XmlDocument

    Sub New()
        _xmldoc = New XmlDocument()
    End Sub


    Public Function obtenerStringXML(ByVal xml As String) As BEOrdenPago

        Dim objOrdenPago As BEOrdenPago = New BEOrdenPago
        Dim objDetalleOrden As BEDetalleOrdenPago
        objOrdenPago.DetallesOrdenPago = New List(Of BEDetalleOrdenPago)


        _xmldoc.LoadXml(xml)
        Dim ordenPago As System.Xml.XmlNode = _xmldoc.SelectSingleNode("ordenPago")

        Dim IdOrdenPago As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdOrdenPago")
        objOrdenPago.IdOrdenPago = DataUtil.StringToInt(IdOrdenPago.InnerText)

        '********API************'
        Dim claveAPI As System.Xml.XmlNode = ordenPago.SelectSingleNode("ClaveAPI")
        If Not (claveAPI Is Nothing) Then
            objOrdenPago.ClaveAPI = DataUtil.StringToInt(claveAPI.InnerText)
        Else
            objOrdenPago.ClaveAPI = ""
        End If


        Dim claveSecreta As System.Xml.XmlNode = ordenPago.SelectSingleNode("ClaveSecreta")
        If Not (claveSecreta Is Nothing) Then
            objOrdenPago.ClaveSecreta = DataUtil.StringToInt(claveSecreta.InnerText)
        Else
            objOrdenPago.ClaveSecreta = ""
        End If

        '***********************'

        Dim IdEstado As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdEstado")
        objOrdenPago.IdEstado = DataUtil.StringToInt(IdEstado.InnerText)


        Dim IdServicio As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdServicio")
        objOrdenPago.IdServicio = DataUtil.StringToInt(IdServicio.InnerText)

        Dim IdMoneda As System.Xml.XmlNode = ordenPago.SelectSingleNode("IdMoneda")
        objOrdenPago.IdMoneda = DataUtil.StringToInt(IdMoneda.InnerText)


        Dim NumeroOrdenPago As System.Xml.XmlNode = ordenPago.SelectSingleNode("NumeroOrdenPago")
        objOrdenPago.NumeroOrdenPago = NumeroOrdenPago.InnerText.ToString()

        Dim Total As System.Xml.XmlNode = ordenPago.SelectSingleNode("Total")
        objOrdenPago.Total = Convert.ToDecimal(Total.InnerText)


        Dim MerchantId As System.Xml.XmlNode = ordenPago.SelectSingleNode("MerchantId")
        objOrdenPago.MerchantID = MerchantId.InnerText

        Dim OrdenIdComercio As System.Xml.XmlNode = ordenPago.SelectSingleNode("OrdenIdComercio")
        objOrdenPago.OrderIdComercio = OrdenIdComercio.InnerText


        Dim UrldOk As System.Xml.XmlNode = ordenPago.SelectSingleNode("UrldOk")
        objOrdenPago.UrlOk = UrldOk.InnerText

        Dim UrlError As System.Xml.XmlNode = ordenPago.SelectSingleNode("UrlError")
        objOrdenPago.UrlError = UrlError.InnerText

        Dim MailComercio As System.Xml.XmlNode = ordenPago.SelectSingleNode("MailComercio")
        objOrdenPago.MailComercio = UrlError.InnerText

        Dim UsuarioId As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioId")
        objOrdenPago.UsuarioID = UsuarioId.InnerText

        Dim DataAdicional As System.Xml.XmlNode = ordenPago.SelectSingleNode("DataAdicional")
        objOrdenPago.DataAdicional = UsuarioId.InnerText


        Dim UsuarioNombre As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioNombre")
        objOrdenPago.UsuarioNombre = UsuarioNombre.InnerText

        Dim UsuarioApellidos As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioApellidos")
        objOrdenPago.UsuarioApellidos = UsuarioApellidos.InnerText

        Dim UsuarioLocalidad As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioLocalidad")
        objOrdenPago.UsuarioLocalidad = UsuarioLocalidad.InnerText


        Dim UsuarioProvincia As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioProvincia")
        objOrdenPago.UsuarioProvincia = UsuarioProvincia.InnerText


        Dim UsuarioPais As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioPais")
        objOrdenPago.UsuarioPais = UsuarioPais.InnerText
        Dim UsuarioAlias As System.Xml.XmlNode = ordenPago.SelectSingleNode("UsuarioAlias")
        objOrdenPago.UsuarioAlias = ordenPago.InnerText

        Dim Detalles As System.Xml.XmlNode = ordenPago.SelectSingleNode("Detalles")
        Dim list As XmlNodeList

        list = Detalles.SelectNodes("Detalle")


        For Each Node As XmlNode In list

            objDetalleOrden = New BEDetalleOrdenPago()



            Dim Cod_Origen As System.Xml.XmlNode = Node.SelectSingleNode("Cod_Origen")
            objDetalleOrden.codOrigen = Cod_Origen.InnerText
            Dim TipoOrigen As System.Xml.XmlNode = Node.SelectSingleNode("TipoOrigen")
            objDetalleOrden.tipoOrigen = TipoOrigen.InnerText
            Dim ConceptoPago As System.Xml.XmlNode = Node.SelectSingleNode("ConceptoPago")
            objDetalleOrden.ConceptoPago = ConceptoPago.InnerText
            Dim Importe As System.Xml.XmlNode = Node.SelectSingleNode("Importe")
            objDetalleOrden.Importe = Convert.ToDecimal(Importe.InnerText)
            Dim Campo1 As System.Xml.XmlNode = Node.SelectSingleNode("Campo1")
            objDetalleOrden.campo1 = Campo1.InnerText
            Dim Campo2 As System.Xml.XmlNode = Node.SelectSingleNode("Campo2")
            objDetalleOrden.campo2 = Campo2.InnerText
            Dim Campo3 As System.Xml.XmlNode = Node.SelectSingleNode("Campo3")
            objDetalleOrden.campo3 = Campo3.InnerText
            objOrdenPago.DetallesOrdenPago.Add(objDetalleOrden)


        Next


        Return objOrdenPago


    End Function



End Class
