Imports Microsoft.VisualBasic
Imports System.Xml
Imports System


Public Class UtilOrdenPagoXML

    Dim _xmldoc As XmlDocument
    Dim OrdenPago As XmlNode
    Sub New()
        _xmldoc = New XmlDocument()
    End Sub

    Public Sub AgregarCabecera(ByVal _ordenPago As String, _
    ByVal _idEstado As String, _
    ByVal _idServicio As String, _
    ByVal _idMoneda As String, _
    ByVal _NumeroOrdenPago As String, _
    ByVal _total As String, _
    ByVal _MerchantId As String, _
    ByVal _OrdenIdComercio As String, _
    ByVal _UrldOk As String, _
    ByVal _UrlError As String, _
    ByVal _MailComercio As String, _
    ByVal _UsuarioId As String, _
    ByVal _DataAdicional As String, _
    ByVal _UsuarioNombre As String, _
    ByVal _UsuarioApellidos As String, _
    ByVal _UsuarioLocalidad As String, _
    ByVal _UsuarioProvincia As String, _
    ByVal _UsuarioPais As String, _
    ByVal _UsuarioAlias As String)

        OrdenPago = _xmldoc.CreateElement("ordenPago")
        Dim IdOrdenPago As XmlNode = _xmldoc.CreateElement("IdOrdenPago")
        IdOrdenPago.InnerText = _ordenPago
        OrdenPago.AppendChild(OrdenPago)
        OrdenPago.AppendChild(IdOrdenPago)

        Dim IdEstado As XmlNode = _xmldoc.CreateElement("IdEstado")
        IdEstado.InnerText = _idEstado
        OrdenPago.AppendChild(IdEstado)

        Dim IdServicio As System.Xml.XmlNode = _xmldoc.CreateElement("IdServicio")
        IdServicio.InnerText = _idServicio
        OrdenPago.AppendChild(IdServicio)

        Dim IdMoneda As System.Xml.XmlNode = _xmldoc.CreateElement("IdMoneda")
        IdMoneda.InnerText = _idMoneda
        OrdenPago.AppendChild(IdMoneda)

        Dim NumeroOrdenPago As System.Xml.XmlNode = _xmldoc.CreateElement("NumeroOrdenPago")
        NumeroOrdenPago.InnerText = _NumeroOrdenPago
        OrdenPago.AppendChild(NumeroOrdenPago)

        Dim Total As System.Xml.XmlNode = _xmldoc.CreateElement("Total")
        Total.InnerText = _total
        OrdenPago.AppendChild(Total)

        Dim MerchantId As System.Xml.XmlNode = _xmldoc.CreateElement("MerchantId")
        MerchantId.InnerText = _MerchantId
        OrdenPago.AppendChild(MerchantId)

        Dim OrdenIdComercio As System.Xml.XmlNode = _xmldoc.CreateElement("OrdenIdComercio")
        OrdenIdComercio.InnerText = _OrdenIdComercio
        OrdenPago.AppendChild(OrdenIdComercio)

        Dim UrldOk As System.Xml.XmlNode = _xmldoc.CreateElement("UrldOk")
        UrldOk.InnerText = _UrldOk
        OrdenPago.AppendChild(UrldOk)

        Dim UrlError As System.Xml.XmlNode = _xmldoc.CreateElement("UrlError")
        UrlError.InnerText = _UrlError
        OrdenPago.AppendChild(UrlError)

        Dim MailComercio As System.Xml.XmlNode = _xmldoc.CreateElement("MailComercio")
        MailComercio.InnerText = _MailComercio
        OrdenPago.AppendChild(MailComercio)

        Dim UsuarioId As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioId")
        UsuarioId.InnerText = _UsuarioId
        OrdenPago.AppendChild(UsuarioId)

        Dim DataAdicional As System.Xml.XmlNode = _xmldoc.CreateElement("DataAdicional")
        DataAdicional.InnerText = _DataAdicional
        OrdenPago.AppendChild(DataAdicional)

        Dim UsuarioNombre As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioNombre")
        UsuarioNombre.InnerText = _UsuarioNombre
        OrdenPago.AppendChild(UsuarioNombre)

        Dim UsuarioApellidos As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioApellidos")
        UsuarioApellidos.InnerText = _UsuarioApellidos
        OrdenPago.AppendChild(UsuarioApellidos)

        Dim UsuarioLocalidad As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioLocalidad")
        UsuarioLocalidad.InnerText = _UsuarioLocalidad
        OrdenPago.AppendChild(UsuarioLocalidad)

        Dim UsuarioProvincia As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioProvincia")
        UsuarioProvincia.InnerText = _UsuarioProvincia
        OrdenPago.AppendChild(UsuarioProvincia)

        Dim UsuarioPais As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioPais")
        UsuarioPais.InnerText = _UsuarioPais
        OrdenPago.AppendChild(UsuarioPais)

        Dim UsuarioAlias As System.Xml.XmlNode = _xmldoc.CreateElement("UsuarioAlias")
        UsuarioAlias.InnerText = _UsuarioAlias
        OrdenPago.AppendChild(UsuarioAlias)


    End Sub
    Public Sub AgregarDetalle(ByVal _cod_origen As String _
    , ByVal _TipoOrigen As String _
    , ByVal _ConceptoPago As String _
    , ByVal _Importe As String _
    , ByVal _Campo1 As String _
    , ByVal _Campo2 As String _
    , ByVal _Campo3 As String)

        Dim Detalles As System.Xml.XmlNode = _xmldoc.CreateElement("Detalles")
        OrdenPago.AppendChild(Detalles)

        Dim Detalle As System.Xml.XmlNode = _xmldoc.CreateElement("Detalle")
        Detalles.AppendChild(Detalle)


        Dim Cod_Origen As System.Xml.XmlNode = _xmldoc.CreateElement("Cod_Origen")
        Cod_Origen.InnerText = _cod_origen
        Detalle.AppendChild(Cod_Origen)

        Dim TipoOrigen As System.Xml.XmlNode = _xmldoc.CreateElement("TipoOrigen")
        TipoOrigen.InnerText = _TipoOrigen
        Detalle.AppendChild(TipoOrigen)


        Dim ConceptoPago As System.Xml.XmlNode = _xmldoc.CreateElement("ConceptoPago")
        ConceptoPago.InnerText = _ConceptoPago
        Detalle.AppendChild(ConceptoPago)


        Dim Importe As System.Xml.XmlNode = _xmldoc.CreateElement("Importe")
        Importe.InnerText = _Importe
        Detalle.AppendChild(Importe)


        Dim Campo1 As System.Xml.XmlNode = _xmldoc.CreateElement("Campo1")
        Campo1.InnerText = _Campo1
        Detalle.AppendChild(Campo1)


        Dim Campo2 As System.Xml.XmlNode = _xmldoc.CreateElement("Campo2")
        Campo2.InnerText = _Campo2
        Detalle.AppendChild(Campo2)

        Dim Campo3 As System.Xml.XmlNode = _xmldoc.CreateElement("Campo3")
        Campo3.InnerText = _Campo3
        Detalle.AppendChild(Campo3)
    End Sub
    Public Function ObtenerXMLString() As String

        Dim sw As IO.StringWriter
        Dim xw As XmlTextWriter = New XmlTextWriter(sw)
        _xmldoc.WriteTo(xw)
        Return sw.ToString()


    End Function



End Class
