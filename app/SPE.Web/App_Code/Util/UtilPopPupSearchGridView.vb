Imports Microsoft.VisualBasic
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System.Collections
Imports System.Collections.Generic

Namespace SPE.Web.Util

    Public Class UtilPopPupSearchGridView

        Public Shared Sub VisualizarGridView(ByVal gview As GridView, ByVal listobjects As List(Of _3Dev.FW.Entidades.BusinessEntityBase), ByVal lblMsg As Label)

            'Dim msg As String = "Para la busqueda necesita ingresar al menos un criterio de busqueda."
            Dim msg As String = ""
            If Not (gview Is Nothing And lblMsg Is Nothing) Then
                gview.DataSource = listobjects
                gview.DataBind()
                If (listobjects.Count = 0) Then
                    lblMsg.Text = " No Se Encontraron Registros. " + msg
                Else
                    lblMsg.Text = ""
                End If

            End If

        End Sub

    End Class
End Namespace
