﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports Microsoft.Reporting.WebForms
Imports Microsoft.VisualBasic
Imports System.Web.Hosting
Imports System.IO


Imports System



Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Reflection




''' <summary>
''' Summary description for UtilReport
''' </summary>
Public Class UtilReport
    '
    ' TODO: Add constructor logic here
    '
    Public Sub New()
    End Sub

    Public Shared Function NameReport(nameReport__1 As String) As String
        Return nameReport__1 & DateTime.Today.Day.ToString() & DateTime.Today.Month.ToString() & DateTime.Today.Year.ToString()
    End Function

    Public Shared Sub ProcesoExportarGenerico(Of T)(lista As List(Of T), Page As Page, opcion As String, ext As String, titleReport As String, parametros As List(Of KeyValuePair(Of String, String)), nombreReporte As String)
        Dim server As HttpServerUtility = Page.Server
        Dim response As HttpResponse = Page.Response

        If (lista.Count > 65500) Then
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), " ", "alert('La cantidad de registros supera el máximo permitido para el reporte, reducir el rango de la consulta');", True)
            Return
        End If
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")
        Dim localReport As LocalReport = GetLocalReport()
        localReport.ReportPath = server.MapPath([String].Concat(T_ReportesExportarNombre.ReportesPath, nombreReporte))
        localReport.DataSources.Add(New ReportDataSource(GetType(T).Name.ToString(), lista))

        If parametros IsNot Nothing Then
            If parametros.Count > 0 Then
                localReport.SetParameters(UtilReport.GetParametros(parametros))
            End If
        End If

        UtilReport.ResponseExport(response, localReport, opcion, ext, UtilReport.NameReport(titleReport))
    End Sub

    Public Shared Sub ProcesoExportarGenericoLote(Of T)(lista As List(Of T), Page As Page, opcion As String, ext As String, titleReport As String, parametros As List(Of KeyValuePair(Of String, String)), nombreReporte As String, ByVal strFolderName As String, ByVal strFileName As String)
        Dim server As HttpServerUtility = Page.Server


        If (lista.Count > 65500) Then
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), " ", "alert('La cantidad de registros supera el máximo permitido para el reporte, reducir el rango de la consulta');", True)
            Return
        End If
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")
        Dim localReport As LocalReport = GetLocalReport()
        localReport.ReportPath = server.MapPath([String].Concat(T_ReportesExportarNombre.ReportesPath, nombreReporte))
        localReport.DataSources.Add(New ReportDataSource(GetType(T).Name.ToString(), lista))

        If parametros IsNot Nothing Then
            If parametros.Count > 0 Then
                localReport.SetParameters(UtilReport.GetParametros(parametros))
            End If
        End If



        Dim deviceInfo As String = "<DeviceInfo>" & "  <OutputFormat>" & opcion & "</OutputFormat>" & "</DeviceInfo>"
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim fileNameExtension As String = "xls"
        Dim streamids As String()
        Dim warnings As Warning()
        Dim FilePath As String = strFolderName

        Dim renderedBytes As Byte()
        'generate report as stream of bytes
        renderedBytes = localReport.Render(opcion, deviceInfo, mimeType, encoding, fileNameExtension, streamids, warnings)
        'create file stream in create mode
        Try
            Dim fs As New FileStream(FilePath + strFileName, FileMode.Create)
            'create Excel file
            fs.Write(renderedBytes, 0, renderedBytes.Length)
            fs.Close()
        Catch ex As Exception

        End Try





        'UtilReport.ResponseExportLote(response, localReport, opcion, ext, UtilReport.NameReport(titleReport))
    End Sub

    Public Shared Sub ProcesoExportarGenerico(Of T, Y)(lista As List(Of T), lista2 As List(Of Y), Page As Page, opcion As String, ext As String, titleReport As String, parametros As List(Of KeyValuePair(Of String, String)), nombreReporte As String)
        Dim server As HttpServerUtility = Page.Server
        Dim response As HttpResponse = Page.Response
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")
        Dim localReport As LocalReport = GetLocalReport()
        localReport.ReportPath = server.MapPath([String].Concat(T_ReportesExportarNombre.ReportesPath, nombreReporte))
        localReport.DataSources.Add(New ReportDataSource(GetType(T).Name.ToString(), lista))
        localReport.DataSources.Add(New ReportDataSource(GetType(Y).Name.ToString(), lista2))

        If parametros IsNot Nothing Then
            If parametros.Count > 0 Then
                localReport.SetParameters(UtilReport.GetParametros(parametros))
            End If
        End If

        UtilReport.ResponseExport(response, localReport, opcion, ext, UtilReport.NameReport(titleReport))
    End Sub

    Private Shared Function GetParametros(parametros As List(Of KeyValuePair(Of String, String))) As List(Of ReportParameter)
        Dim paramList As New List(Of ReportParameter)()
        For Each param As KeyValuePair(Of String, String) In parametros
            paramList.Add(New ReportParameter(param.Key, param.Value))
        Next
        Return paramList
    End Function


    'INSTANCIA LOCAL REPORT
    Private Shared Function GetLocalReport() As LocalReport
        Dim localReport As New LocalReport()

        If localReport.DataSources.Count > 0 Then
            localReport.DataSources.RemoveAt(0)
        End If

        Return localReport
    End Function

    'RESONSE DE EXPORTACION
    Public Shared Sub ResponseExport(response As HttpResponse, localReport As LocalReport, Type As String, fileExtension As String, nameReport As String)
        Dim reportType As String = Type
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim fileNameExtension As String = fileExtension

        Dim deviceInfo As String = "<DeviceInfo>" & "  <OutputFormat>" & reportType & "</OutputFormat>" & "</DeviceInfo>"

        Dim warnings As Warning()
        Dim streams As String()
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)

        response.Clear()
        response.ClearHeaders()
        response.SuppressContent = False
        response.ContentType = mimeType
        response.AddHeader("content-disposition", "attachment; filename=" & nameReport & "." & fileNameExtension)
        response.BinaryWrite(renderedBytes)
        response.Flush()
        response.[End]()
    End Sub


    'RESONSE DE EXPORTACION LOTE pmonzon
    Public Shared Sub ResponseExportLote(response As HttpResponse, localReport As LocalReport, Type As String, fileExtension As String, nameReport As String)
        Dim reportType As String = Type
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim fileNameExtension As String = fileExtension
        Dim FilePath As String

        FilePath = "E:\Descargas\"
        'FilePath = System.Web.HttpContext.Current.Server.MapPath("~/Temp")





        Dim deviceInfo As String = "<DeviceInfo>" & "  <OutputFormat>" & reportType & "</OutputFormat>" & "</DeviceInfo>"

        Dim warnings As Warning()
        Dim streams As String()
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)

        response.Clear()
        response.ClearHeaders()
        response.SuppressContent = False
        response.ContentType = mimeType
        response.AddHeader("content-disposition", "attachment; filename=" & nameReport & "." & fileNameExtension)
        response.BinaryWrite(renderedBytes)
        Try
            response.WriteFile(FilePath, True)
        Catch ex As Exception

        End Try

        response.Flush()
        response.[End]()
    End Sub


End Class
