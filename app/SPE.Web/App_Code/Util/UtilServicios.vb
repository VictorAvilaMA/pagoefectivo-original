﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Web.Script.Serialization
Imports SPE.Entidades
Imports System.Configuration
Imports System.Web
Imports System.Text

Public Class UtilServicios

    Public Shared Sub LimpiarCacheComerciosAfiliadosHTML()
        HttpContext.Current.Cache.Remove("ComerciosAfiliados")
    End Sub

    Public Shared Function ConsultarComerciosAfiliadosHTML(ByVal visibleEnPortada As Boolean, ByVal proximoAfiliado As Boolean,
                                                           Optional ByVal numeroFilas As Integer = 4, Optional ByVal vinculable As Boolean = True,
                                                           Optional ByVal anchoLogo As Integer = 225, Optional ByVal altoLogo As Integer = 90) As String
        'LimpiarCacheComerciosAfiliadosHTML()
        Dim Parametros As String = New JavaScriptSerializer().Serialize(New Object() {visibleEnPortada, proximoAfiliado, numeroFilas, vinculable, anchoLogo, altoLogo})
        Dim ComerciosAfiliados As Dictionary(Of String, String) = Nothing
        If HttpContext.Current.Cache("ComerciosAfiliados") IsNot Nothing Then
            ComerciosAfiliados = HttpContext.Current.Cache("ComerciosAfiliados")
            If ComerciosAfiliados.ContainsKey(Parametros) Then
                Return ComerciosAfiliados(Parametros)
            End If
        End If

        Dim HTMLComerciosAfiliados As New StringBuilder("<ul>")
        Dim DireccionLogosComerciosAfiliados As String = ConfigurationManager.AppSettings("RutaImagenesAfiliados").Replace("\", "/")
        Using oCServicio As New SPE.Web.CServicio
            Dim Servicios As List(Of BEServicio) = oCServicio.ConsultarComerciosAfiliados(visibleEnPortada, proximoAfiliado)
            For index = 0 To Servicios.Count - 1
                Dim Servicio As BEServicio = Servicios(index)
                Dim HTMLImagen As String = String.Format("<img alt=""{0}"" title=""{0}"" src=""{1}"" width=""{2}"" height=""{3}"" border=""0"" />",
                    Servicio.Nombre, String.Concat(ConfigurationManager.AppSettings("DominioFile"), "/", DireccionLogosComerciosAfiliados, "/", _3Dev.FW.Util.DataUtil.StringToSlug(Servicio.Nombre).ToLower(), IIf(proximoAfiliado, "-2.jpg?", ".jpg?"), ConfigurationManager.AppSettings("StaticVersion")),
                    anchoLogo, altoLogo)
                Dim FormatoItem As String = "<li class=""{0}"">{3}</li>"
                If vinculable Then
                    FormatoItem = "<li class=""{0}""><a href=""{1}"" target=""_blank"" title=""{2}"">{3}</a></li>"
                End If
                Dim ClaseCSS As String = ""
                If index Mod numeroFilas = 0 Then
                    ClaseCSS = "first"
                End If
                HTMLComerciosAfiliados.AppendFormat(FormatoItem, ClaseCSS, Servicio.Url, Servicio.Nombre, HTMLImagen)
            Next
        End Using
        HTMLComerciosAfiliados.Append("</ul>")

        If ComerciosAfiliados Is Nothing Then
            ComerciosAfiliados = New Dictionary(Of String, String)
        End If
        If HTMLComerciosAfiliados.ToString() = "<ul></ul>" Then
            HTMLComerciosAfiliados.Clear()
        End If
        ComerciosAfiliados.Add(Parametros, HTMLComerciosAfiliados.ToString())
        HttpContext.Current.Cache("ComerciosAfiliados") = ComerciosAfiliados

        Return HTMLComerciosAfiliados.ToString()
    End Function

End Class
