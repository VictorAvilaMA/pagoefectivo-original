﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports _3Dev.FW.Web
Imports SPE.Web.Seguridad
Imports System.Web

'<add Peru.Com FaseIII>
Public Class UtilSiteMap
    Shared Function getSiteMap(ByVal mySMP As SPE.Web.Seguridad.SPESiteMapProvider) As String
        If HttpContext.Current.Session("SitemapCompleto") Is Nothing Then
            Dim oWebUtil As New WebUtil
            Dim sitemapStr As String
            Dim sitemapNode As SiteMapNode = mySMP.BuildSiteMap()

            ''Aca interceptar para no se que!!

            sitemapStr = oWebUtil.ObtenerMenu(sitemapNode)
            HttpContext.Current.Session("SitemapCompleto") = sitemapStr
            Return sitemapStr
        Else
            Return DirectCast(HttpContext.Current.Session("SitemapCompleto"), String)
        End If
        'la session "SitemapCompleto" muere cuendo hace login
    End Function

    Shared Function getSiteMap(ByVal mySMP As SPE.Web.Seguridad.SPESiteMapProvider, ByVal PaginasAcceso As List(Of _3Dev.FW.Entidades.Seguridad.BERolPagina)) As String
        If HttpContext.Current.Session("SitemapCompleto") Is Nothing Then
            Dim oWebUtil As New WebUtil
            Dim sitemapStr As String
            Dim sitemapNode As SiteMapNode = mySMP.BuildSiteMap()

            ''Aca interceptar para no se que!!

            sitemapStr = oWebUtil.ObtenerMenu(sitemapNode, PaginasAcceso)  'aca modificar por la fucking shit
            HttpContext.Current.Session("SitemapCompleto") = sitemapStr
            Return sitemapStr
        Else
            Return DirectCast(HttpContext.Current.Session("SitemapCompleto"), String)
        End If
        'la session "SitemapCompleto" muere cuendo hace login
    End Function

    Shared Function getSiteMapCabezera() As String
        Dim oWebUtil As New WebUtil
        Dim oSPESiteMapProviderClient As New SPESiteMapProviderClient
        Dim urlPagina As String
        Dim cabezeraMenuString As String

        urlPagina = If(HttpContext.Current.Request.Url.Segments.Length > 1, HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 2), "") + HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1)
        cabezeraMenuString = oSPESiteMapProviderClient.GetCabezeraMenu(urlPagina)

        Return oWebUtil.ObtenerMenuCabezera(cabezeraMenuString)

    End Function

End Class
