Imports Microsoft.VisualBasic
Imports SPE.Entidades
Imports System.Web.UI
Imports System.Text
Imports System
Imports System.Collections.Specialized

Namespace SPE.Web.Util

    Public Class UtilUrl

        Public Shared Function DameUrlAppConfig(ByVal key As String)
            Dim result As String = System.Configuration.ConfigurationManager.AppSettings(key)
            If result = "" Then
                result = "#"
            End If
            Return result
        End Function

        '<add Peru.Com FaseIII>
        Public Shared Sub RedirectAndPOST(ByVal page As Page, ByVal destinationUrl As String, ByVal data As NameValueCollection)
            Dim strForm As String = PreparePOSTForm(destinationUrl, data)
            page.Controls.Add(New LiteralControl(strForm))
        End Sub

        Private Shared Function PreparePOSTForm(ByVal url As String, ByVal data As NameValueCollection) As String
            Dim formID As String = "PostForm" & New Random().Next
            Dim strForm As New StringBuilder
            strForm.Append("<form id=""" & formID & """ name=""" & formID & """ action=""" & url & """ method=""POST"">")
            For Each key As String In data
                strForm.Append("<input type=""hidden"" name=""" & key & """ value=""" & data(key) & """>")
            Next
            strForm.Append("</form>")

            Dim strScript As New StringBuilder()
            strScript.Append("<script language='javascript'>")
            strScript.Append("var v" & formID & " = document." & formID & ";")
            strScript.Append("v" & formID & ".submit();")
            strScript.Append("</script>")
            Return strForm.ToString() & strScript.ToString()
        End Function

    End Class

End Namespace
