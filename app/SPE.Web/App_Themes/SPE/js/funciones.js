$(document).ready(function () {
    $('span.cambio,a.cambio').click(function () {
        if ($(this).parent().parent().children('ul').height() == 0) {
            $('li.activa p.contenedor_menu').children('span.cambio').css('background-position', 'left top');
            $('li.activa').children('ul').animate({ height: 0 }, 300);
            $('li.activa').removeClass('activa');
            $(this).parent().parent().addClass('activa');
            $(this).css('background-position', 'left bottom');
            var contando = $(this).parent().parent().children('ul').children('li').size();
            var subir = contando * 42;
            var sumar = subir + 0;
            $(this).parent().parent().children('ul').animate({ height: subir }, 300);
        } else {
            $(this).css('background-position', 'left top');
            $(this).parent().parent().children('ul').animate({ height: 0 }, 300);
        }
    });
    $('.has_submen p').live('click', function () {
        var element = $(this);
        if ($(this).attr('class') == 'cambioHijo')
            element = $(this).parent();
        var activono = element.attr('class');
        if (activono == "noactiva") {
            var contando2 = element.parent().children('ul').children('li').size();
            var subir2 = contando2 * 37;
            var unir = element.parent().parent().children('li').size() * 42;
            var unido = unir + subir2;
            element.parent().parent().find('ul.subsubmenu').animate({ height: 0 }, 250);
            $('.has_submen p').addClass('noactiva');
            element.parent().parent().animate({ height: unido }, 300 );
            element.parent().children('ul').animate({ height: subir2 }, 300);
            element.removeClass('noactiva');
            //element.removeClass('activado');
        } else {
            var unir2 = element.parent().parent().children('li').size() * 42;
            element.parent().children('ul').animate({ height: 0 }, 300);
            element.parent().parent().animate({ height: unir2 }, 300 );
            //element.removeClass('activado');
            element.addClass('noactiva');
        }
    });
    var link = $('#menu_principal').find('a[href*="' + window.location.href.split('?')[0] + '"]');
    if (link.length == 0)
        link = $('#menu_principal').find('a:first(:not([href=""]))');
    if (link.length > 0) {
        link.addClass('bold');
        var li = link.parents('ul#menu_principal>li');
        li.addClass('activa');
        var pContenedor = li.children('.contenedor_menu');
        var contando = pContenedor.parent().children('ul').children('li').size();
        var subir = contando * 42;
        var li2 = link.parents('li.has_submen');
        var subir2 = 0
        if (li2.length > 0) {
            var contando2 = li2.children('ul.subsubmenu').children('li').size();
            subir2 = contando2 * 37;
        }
        pContenedor.children('span.cambio').css('background-position', 'left bottom');
        pContenedor.parent().children('ul').animate({ height: subir + subir2 }, 300);
        if (li2.length > 0) {
            li2.children('ul.subsubmenu').animate({ height: subir2 }, 300);
        }
    }
    if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').length > 0)
        if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').text() != 'P�gina :')
            $('.pagerStyle>td>table>tbody>tr>td:eq(0)').before('<td>P&aacute;gina :</td>');
});
