var handlerCollapse = function(){
  var handler,target,size,body;
  return {
      init : function(_handler){
          var self= this;
          body = $("body");
          handler = $(_handler);
          target = $(handler.attr("data-target"));
          self.resize();
          handler.bind("click.handlerCollapse",function(e){
              e.preventDefault();
              if(target.hasClass("collapse")){
                self.show();
              }else{
                self.hide();  
              }
          });
      },
      show: function(callback){
       target.removeClass("collapse").addClass("full");
       body.removeClass("full");
       callback && callback();
      },
      hide: function(callback){
       size = target.height();
       target.removeClass("expanded").addClass("collapse");
       body.addClass("full");
       callback && callback();
      },
      resize:function(){
       size = target.height();   
       handler.css("height",size);
      },
      getHeight:function(){
        return size;  
      }
  }; 
};
//var show_msje = function(){
//  var _btnTrgr, _this;
//  return{
//      init: function(){
//        _this = this;
//        this._btnTrgr = ".btnPagarCIP";  
//      },
//      bindEvents:function(){
//        this.show_cb();  
//      },
//      show_cb:function(){
//        $(_this._btnTrgr).bind("click.show_cb",function(){
//            
//        }); 
//      }
//  }
//}();
$(function () {
    window.hc = new handlerCollapse();
    hc.init(".btnExpand");
    setTimeout(function () {
        hc.resize();
    }, 100);
    /*accordion*/
    /*$(".menuAcordion").accordion({
    header:"a.head",
    alwaysOpen: false,
    active: false, 
    autoheight: false,
    animated: 'slide'
    });*/
    $(".menuAcordion").treeview({
        collapsed: true,
        animated: "medium",
        persist: "location",
        unique: true,
        toggle: function () {
            hc.resize();
        }
    });
    if ($.browser.msie && $.browser.version != "8.0" && $.fn.corner) {
        $(".widget-content").corner("round 10px").parent().css('padding', '2px').corner("round 10px");
    }
//    if ($.fn.colorbox) {
//        //$('.btnPagarCIP').colorbox({ inline: true, href: "#msje" });
//    }
    /*pasarella*/
    if ($.fn.colorbox) {
//        if ($(".btnConfirmar").length > 0) {
//            $(".btnConfirmar").colorbox({
//                inline: true,
//                href: "#msje",
//                overlayClose: false,
//                escKey: false,
//                onLoad: function () {
//                    $("#cboxClose").hide();
//                }
//            });
//        }
        if ($(".lnk-info").length > 0) {
            $(".lnk-info").colorbox({
                inline: true,
                href: "#help",
                width: 600
            });
        }
    }
});

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function formatearFechaCSharp(fechaCSharp) {
    fechaCSharp = fechaCSharp.replace('/Date(', '').replace(')/', '');
    var fecha = new Date(parseInt(fechaCSharp));
    return (checkTime(fecha.getDate()) + '/' + checkTime(fecha.getMonth() + 1) + '/' + fecha.getFullYear() + ' ' + checkTime(fecha.getHours()) + ':' + checkTime(fecha.getMinutes()));
}
