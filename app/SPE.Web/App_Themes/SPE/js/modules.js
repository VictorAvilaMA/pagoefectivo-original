miniApp.addModule('show-term', (function () {
    var defaults = {},
    dom = {
        btnContent: $('.title-faqs,.title-bank-paid,.tbank-title'),
        btnVer: $('.slip')
    },
	getContentTerm = function (object) {
	    var id = object.attr("href");
	    var content = $(id);
	    return content;
	},
    showTerm = function (comment) {
        dom.btnContent.click(function () {
            var inst = $(".slip", $(this));
            var content = getContentTerm(inst);
            if (inst.hasClass("active")) {
                inst.removeClass("active");
            } else {
                inst.addClass("active");
            }
            content.slideToggle();
            return false;
        });
        dom.btnVer.click(function () {
            var content = getContentTerm($(this));
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");

            } else {
                $(this).addClass("active");
            }
            content.slideToggle();
            return false;
        });
    },
    init = function (st) {
        showTerm();
        $('.slip[href="#faq1"]').parent().trigger("click");
        $('.slip[href="#paid1"]').parent().trigger("click");
    },
    destroy = function (args) {
    };
    return {
        init: init,
        destroy: destroy
    }
} ()));
miniApp.addModule('slide', (function () {
    var defaults = {},
    dom = {
        slider: $('.slide')
    },
	slide = function (object) {
	    dom.slider.slides();
	}
    init = function (st) {
        slide();        
    },
    destroy = function (args) {
    };
    return {
        init: init,
        destroy: destroy
    }
} ()));
miniApp.addModule('login', (function () {
    var defaults = {},
    dom = {
        ingresar: $('.toogle-login'),
        content: $('.desplegable')
    },
	toogle = function (object) {
	    if (dom.content.length) {
	        dom.ingresar.click(function () {
	            if ($(this).hasClass("active")) {
	                dom.ingresar.removeClass("active");
	                dom.content.addClass("hide");
	            } else {
	                dom.ingresar.addClass("active");
	                dom.content.removeClass("hide");
	            }
	        });
	    }
	}
    init = function (st) {
        toogle();
    },
    destroy = function (args) {
    };
    return {
        init: init,
        destroy: destroy
    }
} ()));
/*
* modules initialization
*/
var homeScripts = ['show-term', 'slide', 'login'];

//si estoy en home inicializo todos los modulos para home
miniApp.runModules(homeScripts);

//si estoy en productos inicializo solo 1 modulo pero le puedo pasar parametros
//miniApp.runModule('add-comment', variable);