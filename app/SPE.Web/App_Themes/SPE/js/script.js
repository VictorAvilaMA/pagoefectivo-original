$(document).ready(function () {

    var confValidate = {
        rules: {
            txtNroDocumento: {
                required: true,
                dni: true
            },
            txtNombre: {
                required: true,
                nombre: true,
                maxlength: 70
            },
            txtApellidos: {
                required: true,
                nombre: true,
                maxlength: 70
            },
            txtEmail: {
                required: true,
                email: true,
                maxlength: 70
            },
            txtTelefono: {
                required: true,
                phone: true,
                maxlength: 10,
                minlength: 6
            },
            ddlTipoDocumento: {
                required: true
            },
            ddlProducto: {
                required: false
            }
        },
        messages: {
            txtNroDocumento: {
                required: "Este campo es necesario."
            },
            txtNombre: {
                required: "Este campo es necesario."
            },
            txtApellidos: {
                required: "Este campo es necesario."
            },
            txtEmail: {
                required: "Este campo es necesario.",
                email: "Por favor ingrese un email válido"
            },
            txtTelefono: {
                required: "Este campo es necesario."
            },
            ddlTipoDocumento: {
                required: "Este campo es necesario."
            },
            ddlProducto: {
                required: "Este campo es necesario."
            }
        }
    }


    $('#Form1').validate(confValidate);
    $.validator.addMethod("nombre", function (value, element) { return this.optional(element) || /^[\-\sABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚáéíóú']+$/.test(value); }, "Sólo letras, espacios( ), guiones(-) y apóstrofes(').");
    $.validator.addMethod("dni", function (value, element) { return this.optional(element) || (/^[0-9]+$/.test(value) && value.length === 8); }, 'Ingrese un número de DNI válido');
    $.validator.addMethod("pasaporte", function (value, element) { return this.optional(element) || /^([0-9]{11})$/gi.test(value); }, 'Ingrese un número de Pasaporte válido');
    $.validator.addMethod("phone", function (value, element) { return this.optional(element) || /^([0-9]{6,8}|\#[0-9]{6}|\*[0-9]{6}|\#?[0-9]{9}|[0-9]{3}\*[0-9]{4})$/gi.test(value); }, 'Ingrese un número de teléfono válido');

    $("#ddlTipoDocumento").on('change.ProductoValidacion', function () {

        var valueItem = $('#ddlTipoDocumento').val();


        if (valueItem == 1) {
            $('#txtNroDocumento').attr('maxlength', '8');
            $('#txtNroDocumento').rules('remove', 'pasaporte');
            $('#txtNroDocumento').rules('add', { required: true, dni: true });
            var cant = $('#txtNroDocumento').val().length;
            if (cant > 8) {
                $('#txtNroDocumento').val('');
            }

            $('#txtNroDocumento').val('');
        } else if (valueItem == 2) {
            $('#txtNroDocumento').attr('maxlength', '11');
            $('#txtNroDocumento').rules('remove', 'dni');
            $('#txtNroDocumento').rules('add', { required: true, pasaporte: true });
            var cant = $('#txtNroDocumento').val().length;
            if (cant <= 8) {
                $('#txtNroDocumento').val('');
            }
        }

    });

    $(document).on('change', '#ddlTipoDocumento', function () {
        var valueItem = $(this).val();


        if (valueItem == 1) {
            $('#txtNroDocumento').attr('maxlength', '8');
            $('#txtNroDocumento').rules('remove', 'pasaporte');
            $('#txtNroDocumento').rules('add', { required: true, dni: true });
            var cant = $('#txtNroDocumento').val().length;
            if (cant > 8) {
                $('#txtNroDocumento').val('');
            }
        } else if (valueItem == 2) {
            $('#txtNroDocumento').attr('maxlength', '11');
            $('#txtNroDocumento').rules('remove', 'dni');
            $('#txtNroDocumento').rules('add', { required: true, pasaporte: true });
            var cant = $('#txtNroDocumento').val().length;
            if (cant <= 8) {
                $('#txtNroDocumento').val('');
            }
        }
        //$('#txtNroDocumento').trigger('keyup');
    });


    $('#Form1').removeAttr('novalidate');

    /*
    $(document).on('click', '#Form1', function (e) {
    e.preventDefault();
    });*/

    $(document).on('click', '#btnPagar', function (e) {
        e.preventDefault();
        if ($('#Form1').valid()) {
            $('.blockfix').show();
            //$('#demo-overlay-false .content').waiting({ fixed: true });
            setTimeout(function () {
                $('#Form1').submit();
            }, 5000);
        }
    });


    //$('#Form1').removeAttr('novalidate');

    //    $('#btnPagar').on('click', function (e) {
    //        e.preventDefault();
    //        if ($('#Form1').valid()) {
    //            $('#Form1').submit();
    //        }
    //    });


});