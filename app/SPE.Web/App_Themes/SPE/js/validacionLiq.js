﻿/* validacion de formualrio liquidacion*/
$(function () {
    var $checkList = $(":input[id^=ctl00_ContentPlaceHolder1_gvResultado][type=checkbox]"),
        $txtFecha = $("#ctl00_ContentPlaceHolder1_txtFechasFin"),
        rules = {
            checkList: {
                required: true,
                messages: {
                    required: '*'
                }
            },
            txtFecha: {
                required: true,
                serie: true,
                messages: {
                    required: '*',
                    serie: "Ej. 15,31"
                }
            }
        },
        $form = $('form');
    //private methods
    var _listaCheckeada = function () {
        return $checkList.is(":checked");
    },
       _addValidChecklist = function () {
           $checkList.each(function (i, e) {
               $(e).rules("add", rules.checkList);
           });
       },
       _removeValidChecklist = function () {
           $checkList.each(function (i, e) {
               $(e).rules("remove");
           });
       }
    //validation
    $form.validate({
        rules: {
            "ctl00$ContentPlaceHolder1$txtPeriodoLiquidacion": {
                required: true
            }
        },
        messages: {
            "ctl00$ContentPlaceHolder1$txtPeriodoLiquidacion": {
                required: '*'
            }
        }
    });
    _addValidChecklist();
    $txtFecha.rules("add", rules.txtFecha);

    //events
    $checkList.change(function () {
        if (_listaCheckeada()) {
            $txtFecha.val("");
            //$txtFecha.rules("remove");
            _removeValidChecklist();
        } else {
            $txtFecha.rules("add", rules.txtFecha);
            _addValidChecklist();
        }
    });

    $txtFecha.keyup(function (e) {
        var value = $.trim($txtFecha.val());
        if (value.length) {
            $checkList.prop("checked", false);
            _removeValidChecklist();
            $('#ctl00_ContentPlaceHolder1_lblErrFechasFin').val('');
        } else {
            _addValidChecklist();
        }
    });



});