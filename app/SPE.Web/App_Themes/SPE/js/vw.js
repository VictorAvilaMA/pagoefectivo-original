/* PLAYER */
var qvm = function () {
    var t = this, ios = (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPhone/i)) ? true : false;
    t.draw = function (_p) {
        if (ios) return video_html5(_p);
        else return video_swf(_p);
    }
    t.stop = function () {
        $("a", "#div" + idTag).click();
        return false;
    }
    t.title = function () {
    }
    var video_swf = function (_pr) {
        var _i = "player" + parseInt(Math.random() * 10000), _p = eval("[" + _pr.arg + "]")[0], _h;
        var varAdd = "";
        if (_p.type == "video" && _pr.arg.indexOf('"banner"') < 0 && (typeof tag_vast == "string")) varAdd = (',"banner":"' + tag_vast + '"');
        _pr.arg = encodeURIComponent(_pr.arg.substr(0, _pr.arg.lastIndexOf("}")) + varAdd + '}');
        if (_p.type == "youtube") {
            var autoplay = (typeof (_p.autoplay) == "undefined") ? 1 : (_p.autoplay > 0 ? 1 : 0);
            _h = '<object width="' + _p.width + '" height="' + _p.height + '"><param name="wmode" value="transparent"><param name="movie" value="http://www.youtube.com/v/' + _p.data.src + '&hl=en_US&fs=1&autoplay=' + autoplay + '&showinfo=0&rel=0&enablejsapi=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed wmode="transparent" src="http://www.youtube.com/v/' + _p.data.src + '&hl=en_US&fs=1&autoplay=' + autoplay + '&showinfo=0&rel=0&enablejsapi=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' + _p.width + '" height="' + _p.height + '"></embed></object>'; //+(_p.title?('<div class="legend">'+_p.title+'</div>'):'');
        } else if (_p.type == "audio") {
            _h = '<object width="' + _p.width + '" height="29"><param name="wmode" value="transparent"><param name="allowScriptAccess" value="always" /><param name="movie" value="' + urlBase + '/vid/player/au.swf?v=281078" /><param name="quality" value="high" /><param value="true" name="allowFullScreen"/><param name="flashvars" value="source=' + _pr.arg + '" /><embed wmode="transparent" allowScriptAccess="always" src="' + urlBase + '/vid/player/au.swf?v=281079" quality="high" allowfullscreen="true" type="application/x-shockwave-flash" width="' + _p.width + '" height="29" flashvars="source=' + _pr.arg + '"></embed></object>';
        } else {
            _h = '<object id="' + _i + '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="' + _p.width + '" height="' + _p.height + '"><param name="wmode" value="opaque"><param name="allowScriptAccess" value="always" /><param name="movie" value="' + urlBase + '/vid/player/vw.swf?v=792810" /><param name="quality" value="high" /><param value="true" name="allowFullScreen"/><param name="flashvars" value="source=' + _pr.arg + '" /><embed name="' + _i + '" id="' + _i + '" wmode="opaque" allowScriptAccess="always" src="' + urlBase + '/vid/player/vw.swf?v=792810" quality="high" allowfullscreen="true" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="' + _p.width + '" height="' + _p.height + '" flashvars="source=' + _pr.arg + '"></embed></object>';
            if (!_pr.close) _h = '<div class="viewer_draw" id="div' + _i + '">' + _h + '<div class="legend hide"></div></div>';
            else _h = '<div class="viewer_draw" id="div' + _i + '">' + _h + '<a href="#Cerrar">X</a></div>';
        }
        if (typeof _pr.to != 'object') return _h;
        else {
            $(_pr.to).html('<div id="o' + _i + '"></div>');
            _pr.to = document.getElementById('o' + _i);
            if (document.all && !navigator.userAgent.indexOf('Opera') >= 0) _pr.to.outerHTML = _h;
            else $(_pr.to).replaceWith(_h);
        }
    }
    var video_html5 = function (_pr, _to) {
        var j = eval("[" + _pr.arg + "]")[0];
        var t = $(Array(
		'<div class="vw_body vw_paused">',
			'<div class="vw_video">',
				'<video controls poster="' + path(j.base_ip, j.data.coverpic, j.data.external) + '">',
					'<source src="' + path(j.base_ip, j.data.src, j.data.external) + '"/>',
        /*'<source src="video.ogx"/>',*/
				'</video>',
			'</div>',
			'<div class="vw_options">',
				'<div class="vw_play_pause_big">',
					'<div class="vw_ico"></div>',
				'</div>',
				'<div class="vw_control ">',
					'<div class="vw_play_pause">',
						'<div class="vw_ico"> </div>',
					'</div>',
					'<div class="vw_control_bg" >',
						'<div class="vw_progress">',
							'<div class="vw_progress_load">',
								'<div class="vw_progress_on"> </div>',
							'</div>',
						'</div>',
						'<div class="vw_time"> 00:00</div>',
						'<div class="vw_volume">',
							'<div class="vw_volume_on"> </div>',
						'</div>',
						'<div class="vw_size"> </div>',
					'</div>',
				'</div>',
        /*'<div class="vw_share">',
        '<div class="vw_ico"> </div>',
        '</div>',
        '<div class="vw_mail">',
        '<div class="vw_ico"> </div>',
        '</div>',*/
			'</div>',
		'</div>'
		).join(''));

        if (typeof _pr.to == 'object') {
            $(_pr.to).empty().append(t);
        }

        var vid = $(".vw_video", t);
        var v = $("video", vid).get(0);
        v.controls = false;
        v.autobuffer = true;

        var opt = $(".vw_options", t);
        var ctr = $(".vw_control", t);
        var ppb = $(".vw_play_pause_big", opt);
        var pps = $(".vw_play_pause", opt);
        var tim = $(".vw_time", opt);
        var vol = $(".vw_volume", opt);
        var siz = $(".vw_size", opt);
        var prl = $(".vw_progress_load", opt);

        vid.click(function (e) { play_pause(e) });
        ppb.click(function (e) { play_pause(e) });
        pps.click(function (e) { play_pause(e) });
        siz.click(function (e) { full_size(e) });
        vol.click(function (e) {
            var lp = e.offsetX;
            var lv = lp / vol.width(); lv = lv > 1 ? 1 : lv;
            v.volume = lv;
            $(".vw_volume_on", ctr).css("width", lp + "px");
            return false;
        });
        /*
        $(".vw_progress",opt).click(function(e){
        var t = $(this),
        lp = e.pageX - (t.position().left+16),
        tm = (lp * s.d) / t.width();
        v.currentTime = tm;
        //v.timeupdate();
        });
        */
        var play_pause = function (e) {
            e.preventDefault();
            if (v.paused) {
                t.removeClass('vw_paused');
                v.play();
            } else {
                t.addClass('vw_paused');
                v.pause();
            }
        }
        var full_size = function (e) {
            if (t.hasClass("fullsize")) {
                t.removeClass("fullsize");
                t.attr("style", "").css({ width: j.width + "px", height: j.height + "px" });
                $(v).attr("style", "").css({ width: j.width + "px", height: j.height + "px" });
            } else {
                t.addClass("fullsize");
                if (v.webkitSupportsFullscreen) {
                    v.webkitEnterFullScreen();
                } else {
                    t.css({ position: "absolute", top: 0, left: 0, width: "100%", height: "100%" });
                    $(v).css({ position: "absolute", top: 0, left: 0, width: "100%", height: "100%" });
                }
            }
            resize();
        }
        var resize = function () {
            ctr.css({ top: t.height() - (ctr.height() + 10) });
            var lw = t.width() - (10 + pps.outerWidth() + 20 + 6);
            $(".vw_control_bg", opt).css({ width: lw });
            $(".vw_progress", opt).css({ width: lw - (6 + 6 + tim.outerWidth() + 6 + vol.outerWidth() + 6 + siz.outerWidth()) });
            ppb.css({ top: (t.height() / 2) - (ppb.outerHeight() / 2), left: (t.width() / 2) - (ppb.outerWidth() / 2) });
        }
        var th = 0;
        t.mouseenter(function () {
            clearTimeout(th);
            opt.fadeIn("fast");
        }).mouseleave(function () {
            if (!t.hasClass('vw_paused')) th = setTimeout(function () { opt.fadeOut("fast") }, 100);
        }).addClass("fullsize");
        var s = {};
        addEvent(v, "loadedmetadata", function (e) { s = { d: e.target.duration} });
        addEvent(v, "timeupdate", function (e) {
            var lt = Math.round(e.target.currentTime);
            var ls = "00" + (lt % 60);
            var lm = "00" + (Math.floor(lt / 60));
            tim.html(lm.substring(lm.length - 2) + ':' + ls.substring(ls.length - 2));
            var w100 = $(".vw_progress", opt).width();
            $(".vw_progress_on", opt).css("width", (e.target.currentTime * w100 / s.d) + "px");
        });
        addEvent(v, "progress", function (e) {
            if (e.lengthComputable) { /* firefox */
                var w100 = $(".vw_progress", opt).width();
                prl.css("width", (e.loaded * w100 / e.total) + "px");
            } else if (v.buffered.length) { /* safari */
                var w100 = $(".vw_progress", opt).width();
                var lw = parseInt(((v.buffered.end(0) * w100) / v.duration));
                prl.css("width", lw + "px");
            }
        });

        addEvent(v, "pause", function (e) { ppb.removeClass("paused").removeClass("hide"); pps.removeClass("paused") });
        addEvent(v, "play", function (e) { ppb.addClass("paused").addClass("hide"); pps.addClass("paused") });
        addEvent(v, "ended", function (e) { play_pause(e) });

        full_size();
        if (typeof _pr.to != 'object') return t;
        else {
            if (j.autoplay) pps.click();
        }
    }
    var path = function (base, dir, external) {
        var r = "";
        if (external) {
            r = dir;
        } else if (dir != null && (dir != " " || dir != "")) {
            var a_id = dir.split("/");
            var sector = a_id[0];
            var type = a_id[1];
            var id = a_id[2];
            var extension = a_id[3].substr(0, 3);
            extension = extension == 'flv' ? 'mp4' : extension;
            var filename = "0000000000" + id;
            filename = filename.substring(filename.length - 8);
            var dir_split_file = (filename.substr(0, filename.length - 3)).split('');
            var scheme_dir = dir_split_file.join("/");
            r = base + sector + '/' + id + '.' + extension;
        }
        return r;
    }
    var addEvent = function (elem, event, func) {
        if (elem.addEventListener) elem.addEventListener(event, func, false); // W3C DOM
        else if (elem.attachEvent) return elem.attachEvent("on" + event, func); // IE DOM
        /*else throw 'No es posible añadir evento';*/
    }
};
var vm = new qvm();

function viewer_insert(par) {
    return vm.draw(par);
}

function viewerStop() {
    return vw.stop();
}
function viewerTitle(idTag, txt) {
    //return vw.stop(idTag, txt);
}