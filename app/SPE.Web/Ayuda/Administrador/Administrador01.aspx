<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador01.aspx.vb" Inherits="Ayuda_Administrador_Administrador01" title="Página Principal - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1> P&aacute;gina Principal - Administrador</h1>
    <div id="div1">
        Esta interface muestra iconos con las opciones que el administrador puede gestionar tales como:
    </div>
    
    <br />
    
    <div id="div2">
        •	Clientes
    </div>
    
    <div id="div3">
        •	Agencias Recaudadoras
    </div>
    
    <div id="div4">
        •	Agentes
    </div>
    
    <div id="div5">
        •	Empresas Contratantes
    </div>
    
    <div id="div6">
        •	Representantes</div>
    
    <div id="div7">
        •	Servicio
    </div>
    
    <br />
    
    <div id="div8">
        <img src="../Images/Administrador1-0.JPG" alt="" />
    </div>
    
    <br />
    <div id="div9">
            Al elegir una de las opciones, el sistema redireccionará a la interface de consulta de la opci&oacute;n elegida.
        <br />
        <br />
    </div>
    
</asp:Content>

