<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador02.aspx.vb" Inherits="Ayuda_Administrador_Administrador02" title="Consultar Agencia Recaudadora - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1> Consultar Agencia Recaudadora - Administrador</h1>
    <div id="div1">
        Esta interface realizar b&uacute;squedas de agencias recaudadoras.
    </div>
    
    <br />
    
    <div id="div2">
        Criterios de b&uacute;squeda
    </div>
    
    <br />
    
    <div id="div3">
        �	Nombre Comercial: nombre de la agencia.
    </div>
    
    <div id="div4">
        �	Contacto: contacto de la agencia.
    </div>
    
    <div id="div5">
        �	Estado: estado de la agencia.
    </div>

    <br />
   
        <img src="../Images/Administrador2-0.JPG" alt="" style="width: 687px" />
    <br />
    
    <div id="div6">
        Para realizar la b&uacute;squeda hacer clic en &nbsp;
       <img src="../../images/boton-buscar.JPG" alt="" />
    </div>
    
    <div id="div7">
        <br />
        <br />
        El resultado de la b&uacute;squeda listara las agencias recaudadoras que cumplan con los criterios de b&uacute;squeda.
    </div>
     <img src="../images/Administrador2-1.JPG" alt="" />
        
    <div id="div8">
        &nbsp;</div>
    
    <div id="div9">
        Cada Agencia Recaudadora contara con la opci�n de actualizar sus datos. Para ello hacer clic en el icono
        <img src="../../images/lupa.gif" alt="" />
        de la agencia a actualizar.
    </div>
    
    <div id="div10">
        <img src="../Images/Administrador2-2.JPG" alt="" />
    </div>
    
   
    <div id="div11">
        La interface cuenta con el bot&oacute;n 
        <img src="../../images/boton-nuevo.jpg" alt="" />
        el cual permite crear una nueva agencia. 
        <br />
        <br />
    </div>
    <br />
    <br />
</asp:Content>

