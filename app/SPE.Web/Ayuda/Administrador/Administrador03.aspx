<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador03.aspx.vb" Inherits="Ayuda_Administrador_Administrador03" title="Consultar Agente Recaudador - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Agente - Administrador</h1>
<div>
Esta interface realizar b&uacute;squedas de los agentes recaudadores.
</div>
<br />
<div>
Criterios de b&uacute;squeda
</div>
<br />
<div class="TextoSubtitulo">
•	Tipo Agente: nombre del tipo de agencia.
</div>
<div>
•	Agencia: nombre de la agencia.
</div>
<div>
•	Nombres: nombres del agente recaudador. 
</div>
<div>
•	Apellidos: apellidos del agente recaudador.
</div>
<div>
•	Estado: estado del agente recaudador.
</div>

<div>
    <img src="../Images/Administrador3-0.JPG" alt="" />
</div>

<br />
<div>
Para realizar la b&uacute;squeda hacer clic en
<img src="../../images/boton-buscar.JPG" alt="" />
</div>
<br />
<div>
    <img src="../Images/Administrador3-1.JPG" alt="" />
</div>
<br />
<div>
El resultado de la b&uacute;squeda listara los agentes recaudadores que cumplan con los criterios de b&uacute;squeda, además la opci&oacute;n de actualizar los datos de los agentes. Para ello hacer clic en el icono 
<img src="../../images/lupa.gif" alt="" />
</div>
<br />
<div>

    <img src="../Images/Administrador3-2.JPG" alt="" />

</div>
<br />
<div>
La interface “Consultar Agente Recaudador” cuenta con el bot&oacute;n 
<img src="../../images/boton-nuevo.jpg" alt="" /> el cual permite crear un nuevo agente recaudador.

</div>
<br />
<br />
</asp:Content>

