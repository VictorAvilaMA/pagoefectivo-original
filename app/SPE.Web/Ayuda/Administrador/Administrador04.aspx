<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador04.aspx.vb" Inherits="Ayuda_Administrador_Administrador04" title="Consultar Cliente - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Cliente - Administrador</h1>
<br />
<div>
Esta interface permite realizar consultas de los clientes:
</div>
<br />
<div>
�	Email: correo electr�nico del cliente
</div>
<div>
�	Nombres: nombre del cliente
</div>
<div>
�	Apellidos: apellidos del cliente
</div>
<div>
�	Estado: estado del cliente</div>
<br />
<div>
    <img src="../Images/Administrador4-0.JPG" alt=""/>
</div>
<br />
<div>
Para realizar la b�squeda hacer clic en 
    <img src="../../images/boton-buscar.JPG" alt=""/>
</div>
<br />
<div>
    <img src="../Images/Administrador4-1.JPG" alt=""/>
</div>
<br />
<div>
El resultado de la b�squeda muestra los clientes que coinciden con los criterios seleccionados. 
Cada cliente listado cuenta con la opci�n para poder modificar sus datos. Para realizar la modificaci�n hacer clic en el icono 
<img src="../../images/lupa.gif" alt=""/>
del cliente a modificar.
</div>
<br />
<div>
<img src="../Images/Administrador4-2.JPG" alt=""/>
</div>
<br />
<div>
La interface cuenta con la opci�n para crear un nuevo cliente. Para crear un nuevo cliente hacer clic en el bot�n
<img src="../../images/boton-nuevo.jpg" alt=""/>
</div>
<br />
<br />
</asp:Content>

