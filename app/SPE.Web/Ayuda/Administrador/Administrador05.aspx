<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador05.aspx.vb" Inherits="Ayuda_Administrador_Administrador05" title="Registrar Cliente - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Registrar Cliente - Administrador</h1>
<br />
<div>
La interface permite al Administrador registrar nuevos clientes. 
</div>
<br />
<div>
Los datos necesarios para el registro son:
    <br />
</div>
<div>
�	Email: correo electr�nico del cliente. (Campo &nbsp;obligatorio)
    <br />
    � Alias: alias o sobrenombre del cliente. (Campo no obligatorio)
    <br />
�	Nombres: nombres del cliente. (Campo obligatorio)
</div>
<div>
�	Apellidos: apellidos del cliente. (Campo obligatorio)
</div>
<div>
�	Documento: tipo de documento de identidad
</div>
<div>
�	N�: n�mero del documento de identidad
</div>
<div>
�	Tel�fono: n�mero de tel�fono del cliente
</div>
<div>
�	Email alternativo: correo electr�nico alternativo del cliente
    <br />
�	Direcci�n: direcci�n de residencia del cliente.
</div>
<div>
�	Pa�s: pa�s  de residencia del cliente. (Campo obligatorio)
</div>
<div>
�	Departamento: departamento de residencia del cliente. (Campo obligatorio)
</div>
<div>
�	Ciudad: ciudad de residencia del cliente. (Campo obligatorio)
</div>
<div>
    &nbsp;</div>
<br />
<div>
   <img src="../Images/Administrador5-0.JPG" alt="" /> 
</div>
<br />
<div>
Para registrar al cliente hacer clic en
   <img src="../../images/boton-registar.jpg" alt="" /> 
El sistema verificar� los datos seleccionados e ingresados. Si los datos cumplen con las validaciones se completara el registro de forma exitosa.
</div>
<br />
<br />


</asp:Content>


