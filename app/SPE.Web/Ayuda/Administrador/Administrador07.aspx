<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador07.aspx.vb" Inherits="Ayuda_Administrador_Administrador07" title="Registrar Agencia Recaudadora - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Registrar Agencia Recaudadora - Administrador</h1>
<br />
<div>
La interface permite al Administrador registrar nuevos agencias recaudadoras. 
</div>
<br />
<div>
Los datos necesarios para el registro son:
</div>
<br />
<div>
�	Tipo de Agencia: tipo de agencia. 
</div>
<div>
�	Nombre Comercial: nombre de la agencia. 
</div>
<div>
�	R.U.C.: Registro �nico de Contribuyente de la agencia. 
</div>
<div>
�	Contacto: contacto de la agencia.
</div>
<div>
�	Tel�fono: n�mero de tel�fono de la agencia.
</div>
<div>
�	Fax: n�mero de fax de la agencia.
</div>
<div>
�	E-mail: correo electr�nico de la agencia.
    <br />
�	Direcci�n: direcci�n de residencia de la agencia.
</div>
<div>
�	Pa�s: pa�s de residencia del cliente.
</div>
<div>
�	Departamento: departamento de residencia del cliente. 
</div>
<div>
�	Ciudad: ciudad de residencia del cliente.
</div>
<br />
<div>
<img src="../Images/Administrador6-0.JPG" alt="" /> 
</div>
<br />
<div>
Para registrar la agencia recaudadora hacer clic en
<img src="../../images/boton-registar.jpg" alt="" />. El sistema verificar� los datos seleccionados e ingresados. Si los datos cumplen con las validaciones se completara el registro de forma exitosa.
</div>
<br />
<br />
</asp:Content>

