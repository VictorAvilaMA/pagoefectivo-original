<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador09.aspx.vb" Inherits="Ayuda_Administrador_Administrador09" title="Registrar Empresa Contratante - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Registrar Empresa Contratante - Administrador</h1>
<br />
<div>
La interface permite al Administrador registrar nuevos agentes recaudadores. 
</div>
<br />
<div>
Los datos necesarios para el registro son:
</div>
<br />
<div>
�	Raz�n Social: nombre de la empresa contratante 
</div>
<div>
�	Ruc: Registro �nico de contribuyente de la empresa contratante.
</div>
<div>
�	Contacto: nombre del contacto de la empresa contratante. 
</div>
<div>
�	Tel�fono: n�mero de tel�fono de la empresa contratante.
</div>
<div>
�	Fax: n�mero de fax de la empresa contratante. 
</div>
<div>
�	Email: correo electr�nico de la empresa contratante.
    <br />
    � Direcci�n: direcci�n de la empresa contratante.
</div>
<div>
�	Sitio Web: Url de la empresa contratante 
</div>
<div>
�	Pa�s: pa�s  de residencia de la empresa contratante. 
</div>
<div>
�	Departamento: departamento de residencia de la empresa contratante.
</div>
<div>
�	Ciudad: ciudad de residencia de la empresa contratante.
    <br />
    � Ocultar Empresa: para ocultar la empresa seleccionar con un check.<br />
�	Adjuntar imagen: para seleccionar una imagen hacer clic en el bot�n �Browse� y buscar la imagen a subir.
</div>
<br />
<div>
<img src="../Images/Administrador8-0.JPG" alt="" />  
</div>
<br />
<div>
Para registrar la empresa contratante hacer clic en
<img src="../../images/boton-registar.jpg" alt="" />  
El sistema verificar� los datos seleccionados e ingresados. Si los datos cumplen con las validaciones se completara el registro de forma exitosa.
</div>
<br />
<br />

</asp:Content>

