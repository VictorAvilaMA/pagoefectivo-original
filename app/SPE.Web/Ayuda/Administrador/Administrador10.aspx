<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador10.aspx.vb" Inherits="Ayuda_Administrador_Administrador10" title="Consultar Empresa Contratante - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Empresa Contratante - Administrador</h1>
<br />
<div>
Esta interface realizar b�squedas de las empresas contratantes.
</div>
<br />
<div>
Criterios de b�squeda:
</div>
<br />
<div>
�	Raz�n Social: nombre de la Empresa contratante.
</div>
<div>
�	RUC: registro �nico de contribuyente de la empresa contratante.
</div>
<div>
�	Contacto: nombre del contacto de la empresa contratante. 
</div>
<div>
�	Estado: estado de la empresa contratante.
</div>
<br />
<div>
Para realizar la b�squeda hacer clic en
<img src="../../images/boton-buscar.JPG" alt="" /> 
</div>
<br />
<div>
<img src="../Images/Administrador9-0.JPG" alt="" /> 
</div>
<br />
<div>
El resultado de la b�squeda listara las empresas contratantes que cumplan con los criterios de b�squeda. 
</div>
<br />
<div>
    <img src="../Images/Administrador9-1.JPG" alt="" /> 
</div>
<br />
<div>
Cada empresa contratante contara con la opci�n de actualizar sus datos. Para ello hacer clic en el icono 
<img src="../../images/lupa.gif" alt="" /> de la empresa ha actualizar.
</div>
<br />
<div>
<img src="../Images/Administrador10-0.JPG" alt="" /> 
</div>
<br />
<div>
La interface �Consultar Empresa Contratante� cuenta con el bot�n 
<img src="../../images/boton-nuevo.jpg" alt="" />  el cual permite crear una nueva empresa contratante. 
</div>
<br />
<br />

</asp:Content>

