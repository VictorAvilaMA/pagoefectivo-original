<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador11.aspx.vb" Inherits="Ayuda_Administrador_Administrador11" title="Registrar Representante - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1>Registrar Representante - Administrador</h1>
<br />
<div id="div1">
La interface permite al Administrador registrar nuevos representantes de empresas contratantes. 
</div>
<br />
<div id="div2">
Los datos necesarios para el registro son:
</div>
<br />
<div id="div3">
�	Email: correo electr�nico del representante. 
</div>
<div id="div4">
�	Nombres: nombre de representante.
</div>
<div id="div5">
�	Apellidos: apellidos del representante. 
</div>
<div id="div6">
�	Tel�fono: tel�fono del representante.
</div>
<div id="div7">
�	Documento: Tipo de documento de identidad del representante.
</div>
<div id="div9">
�	N�: numero de documento de identidad.
    <br />
    � Direcci�n: direcci�n del representante.
</div>
<div id="div10">
�	Pa�s: pa�s  de residencia del representante. 
</div>
<div id="div11">
�	Departamento: departamento de residencia del representante.
</div>
<div id="div12">
�	Ciudad: ciudad de residencia del representante.
    <br />
�	Raz�n Social: nombre de la empresa contratante del representante. Se cuenta con una b�squeda para facilitar la selecci�n de la empresa 
    <br />
    contratante, para ello hacer clic en
    <img src="../Images/Administrador11-0.JPG" alt="" />
</div>
<div id="div14">
�	RUC: Registro �nico de Contribuyente de la Empresa contratante.
</div>
<br />
<div id="div15">
<img src="../Images/Administrador11-1.JPG" alt="" />
</div>
<br />
<div id="div16">
Para registrar al representante de una empresa contratante hacer clic en
<img src="../../images/boton-registar.jpg" alt="" />. El sistema verificar� los datos seleccionados e ingresados. Si los datos cumplen con las validaciones se completara el registro de forma exitosa.
</div>
<br />
<div id="div17">
<br />
</div>
</asp:Content>

