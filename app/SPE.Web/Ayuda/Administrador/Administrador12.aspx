<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador12.aspx.vb" Inherits="Ayuda_Administrador_Administrador12" title="Consultar Representante - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Representante - Administrador</h1>
<br />
<div>
Esta interface permite realizar b�squedas de los representantes de las empresas contratantes.
</div>
<br />
<div>
Criterios de b�squeda:
</div>
<br />
<div>
�	Nombres: nombre del representante de la empresa contratante.
</div>
<div>
�	Apellidos: apellidos del representante de la empresa contratante.
</div>
<div>
�	Estado: estado del representante de la empresa contratante.
</div>
<br />
<div>
Para realizar la b�squeda hacer clic en: 
<img src="../../images/boton-buscar.JPG" alt="" />
</div>
<br />
<div>
<img src="../Images/Administrador12-0.JPG" alt="" />
</div>
<br />
<div>
El resultado de la b�squeda listara los representantes de las empresas contratantes que cumplan con los criterios de b�squeda.
</div>
<br />
<div>
    <img src="../Images/Administrador12-1.JPG" alt="" />
</div>
<br />
<div>
Cada representante contara con la opci�n de modificar sus datos. Para ello hacer clic en el icono 
 <img src="../../images/lupa.gif" alt="" /> del representante a actualizar.

</div>
<br />
<div>
<img src="../Images/Administrador12-2.JPG" alt="" />
</div>
<br />
<div>
La interface �Consultar Representante� cuenta con el bot�n 
<img src="../../images/boton-nuevo.jpg" alt="" /> el cual permite crear un nuevo representante. 
</div>
<br />
<br />
</asp:Content>

