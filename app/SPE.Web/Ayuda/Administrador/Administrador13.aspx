<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador13.aspx.vb" Inherits="Ayuda_Administrador_Administrador13" title="Registrar Servicio - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Registrar Servicio - Administrador</h1>
<br />
<div>
La interface permite al Administrador registrar nuevos servicios de empresas contratantes. 
</div>
<br />
<div>
Los datos necesarios para el registro son:
</div>
<br />
<div>
�	Empresa Contratante: nombre de la empresa contratante del servicio. Se cuenta con una b�squeda para facilitar la selecci�n de la empresa contratante, para ello hacer clic en
<img src="../../images/lupa.gif" alt="" />
</div>
<br />
<div>
    <img src="../Images/Administrador13-0.JPG" alt="" />
 </div>
<br />
<div>
�	Nombre: nombre del servicio.
</div>
<div>
�	Tiempo de expiraci�n (horas): intervalo de tiempo de expiraci�n del servicio. 
</div>
<div>
�	Direc. Web: direcci�n web del servicio.
</div>
<div>
�	Adjuntar Imagen: ruta f�sica de la imagen del servicio. Se cuenta con el bot�n 
<img src="../Images/Administrador13-1.JPG" alt="" /> para facilitar la selecci�n de la imagen con su ruta.
</div>
<br />
<div>
    <img src="../Images/Administrador13-2.JPG" alt="" />
</div>
<br />
<div>
Para registrar el servicio de una empresa contratante hacer clic en 
    <img src="../../images/boton-registar.jpg" alt="" />. El sistema verificar� los datos seleccionados e ingresados. Si los datos cumplen con las validaciones se completara el registro de forma exitosa
</div>
<br />
<br />
</asp:Content>

