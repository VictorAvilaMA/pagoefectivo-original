<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador14.aspx.vb" Inherits="Ayuda_Administrador_Administrador14" title="Consultar Servicio - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Servicio - Administrador</h1>
<br />
<div>
Esta interface permite realizar b�squedas de los servicios de las empresas contratantes.
</div>
<br />
<div>
Criterios de b�squeda:
</div>
<br />
<div>
�	Empresa Contratante: nombre de la empresa contratante.
</div>
<div>
�	Servicio Nombre: nombre del servicio de la empresa contratante.
</div>
<div>
�	Estado: estado del servicio de la empresa contratante.
</div>
<br />
<div>
Para realizar la b�squeda hacer clic en
<img src="../../images/boton-buscar.JPG" alt="" />
</div>
<br />
<div>
    <img src="../Images/Administrador14-0.JPG" alt="" />
</div>
<br />
<div>
El resultado de la b�squeda listara los servicios de las empresas contratantes que cumplan con los criterios de b�squeda.
</div>
<br />
<div>
       <img src="../Images/Administrador14-1.JPG" alt="" />
</div>
<br />
<div>
Cada servicio cuenta con la opci�n de actualizar sus datos. Para ello hacer clic en el icono 
       <img src="../../images/lupa.gif" alt="" />del servicio a actualizar.
</div>
<br />
<div>
       <img src="../Images/Administrador14-2.JPG" alt="" />
</div>
<br />
<div>
La interface �Consultar Servicio� cuenta con el bot�n 
<img src="../../images/boton-nuevo.jpg" alt="" /> el cual permite crear un nuevo servicio. 
</div>
<br />
<br />
</asp:Content>

