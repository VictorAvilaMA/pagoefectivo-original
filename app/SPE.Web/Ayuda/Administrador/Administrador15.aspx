<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador15.aspx.vb" Inherits="Ayuda_Administrador_Administrador15" title="Desbloquear Usuario - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Desbloquear Usuario - Administrador</h1>
<br />
<div>
La interface permite al Administrador el bloqueo y desbloque de un usuario del sistema. 
</div>
<br />
<div>
El dato necesario para el desbloqueo es:
</div>
<br />
<div>
�	E-mail: correo electr�nico del usuario. 
</div>
<br />
<div>
    <img src="../Images/Administrador15-0.JPG" alt="" />
</div>
<br />
<div>
Una vez ingresado el correo hacer clic en 
    <img src="../../images/lupa.gif" alt="" />
</div>
<br />
<div>
Se visualizaran los datos del usuario. Adem�s la opci�n para seleccionar el estado que se le quiere actualizar.
</div>
<div>
En el caso de estar boqueado, se visualizara la opci�n de desbloqueo. 
</div>
<div>
De estar desbloqueado se visualizara la opci�n de bloquear.
</div>
<br />
<div>
<img src="../Images/Administrador15-1.JPG" alt="" />
</div>
<br />
<div>
Para actualizar el estado del usuario hacer clic en 
<img src="../../images/boton-actualizar1.JPG" alt="" />
</div>
<br />
<br />
</asp:Content>

