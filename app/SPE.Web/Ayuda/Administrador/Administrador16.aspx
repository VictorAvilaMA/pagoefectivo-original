<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador16.aspx.vb" Inherits="Ayuda_Administrador_Administrador16" title="Consultar Archivos Generados - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Archivos Generados - Administrador</h1>
<br />
<div>
Consultar Archivos Generados permite realizar b�squedas avanzadas de las archivos del cliente.
</div>
<br />
<div>
Los criterios para realizar la b�squeda  son:
</div>
<br />
<div>
�	Servicio: Nombre del servicio de la empresa contratante.
</div>
<div>
�	Nombre: Nombre del archivo 
</div>
<div>
�	Tipo de archivo: Especifica el tipo de archivo , se tiene la posibilidad de seleccionar un tipo o todos.
</div>
<div>
�	Rango de Fechas
</div>
<br />
<div>
Del: Fecha inicio 
</div>
<div>
Al: Fecha fin  
</div>
<br />
<div>
El rango de fechas a tomarse en cuenta depende directamente del estado o tipo de archivo seleccionado. Por ejemplo:
</div>
<br />
<div>
<img src="../Images/Administrador16-0.JPG" alt=""/> 
</div>
</asp:Content>

