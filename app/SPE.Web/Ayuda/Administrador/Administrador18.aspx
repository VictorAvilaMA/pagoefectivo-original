<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Administrador18.aspx.vb" Inherits="Ayuda_Administrador_Administrador18" title="Consultar Jefe de Producto - Administrador" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Jefe de Producto - Administrador</h1>
<br />
<div>
Esta interface permite realizar consultas de los jefes de producto:
</div>
<br />
<div>
�	Email: correo electr�nico del jefe de producto
</div>
<div>
�	Nombres: nombre del jefe de producto
</div>
<div>
�	Apellidos: apellidos del jefe de producto
</div>
<div>
�	Estado: estado del jefe de producto
</div>
<br />
<div>
<img src="../Images/Administrador18-0.JPG" alt="" /> 

</div>
<br />
<div>
Para realizar la b�squeda hacer clic en
<img src="../../images/boton-buscar.JPG" alt="" /> 
</div>
<br />
<div>
    <img src="../Images/Administrador18-1.JPG" alt="" /> 
</div>
<br />
<div>
El resultado de la b�squeda muestra los clientes que coinciden con los criterios seleccionados. 
</div>
<br />

<div>
Cada jefe de producto listado cuenta con la opci�n para poder modificar sus datos. Para realizar la modificaci�n hacer clic en el icono 
    <img src="../../images/lupa.gif" alt="" /> del jefe de producto a modificar.
</div>
<br />
<div>
    <img src="../Images/Administrador18-2.JPG" alt="" /> 
</div>
<br />
<div>
La interface cuenta con la opci�n para crear un nuevo jefe de producto. Para crear un nuevo jefe de producto hacer clic en el bot�n
    <img src="../../images/boton-nuevo.jpg" alt="" /> 
</div>
<br />
<br />
</asp:Content>

