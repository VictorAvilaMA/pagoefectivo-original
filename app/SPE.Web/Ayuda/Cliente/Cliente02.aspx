<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Cliente02.aspx.vb" Inherits="Ayuda_Cliente_Cliente02" title="Actualizar Datos - Cliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1> Actualizar Datos - Cliente</h1>
   <br />
   <div>
   La interface de Actualizar Cliente permite actualizar los datos del cliente, tales como:
   </div>
   <br />
   <div>
   �	Alias: Seud�nimo que el cliente puede utilizar dentro del sistema. Dato obligatorio.
   </div>
   <div>
   �	Nombres: Los nombres del cliente. Dato obligatorio.
   </div>
   <div>
   �	Apellidos: Los apellidos del cliente. Dato obligatorio.
   </div>
   <div>
   �	Documento: Tipo de documento con que el cliente se puede identificar.
   </div>
   <div>
   �	N�: Numero del documento con que se identifica.
   </div>
   <div>
   �	Tel�fono: N�mero telef�nico del cliente.
   </div>
   <div>
   �	Mail Alternativo: Mail alternativo con que cuenta el cliente.
   </div>
   <div>
   �	Pa�s: El pa�s donde reside el cliente. Dato obligatorio.
   </div>
   <div>
   �	Departamento: El departamento donde reside el cliente. Dato obligatorio.
   </div>
   <div>
   �	Ciudad: La ciudad donde reside el cliente. Dato obligatorio.
   </div>
   <div>
   �	Direcci�n: La direcci�n donde reside el cliente
   </div>
   <br />
   <div>
   Para registrar dichos cambios, debe hacer clic en el bot�n 
       <img src="../../images/boton-actualizar1.JPG" alt="" />
    </div>
   <br />
   <div>
   De cumplir con las validaciones correspondientes el sistema confirmara su actualizaci�n. De no cumplir con las validaciones el sistema mostrara una lista con las observaciones del caso.
   </div>
   <br />
   <div>
       <img src="../Images/Cliente2-0.JPG" alt="" />
   </div>
</asp:Content>

