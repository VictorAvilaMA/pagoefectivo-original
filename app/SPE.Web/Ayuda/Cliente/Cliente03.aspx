<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Cliente03.aspx.vb" Inherits="Ayuda_Cliente_Cliente03" title=" Historial de Pagos - Cliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1> Historial de Pagos - Cliente</h1>
   <br />
   <div>
   El Historial de Pagos permite realizar b�squedas avanzadas de las �rdenes de pago del cliente.
   </div>
   <br />
   <div>
   Los criterios para realizar la b�squeda  son:
   </div>
   <br />
   <div>
   �	Cod. Identif. Pago: N�mero que identifica a una orden de pago.
   </div>
   <div>
   �	Estado de Cod. Identif. Pago: estados que el C�digo de Identificaci�n de Pago puede tener, se tiene la posibilidad de  seleccionar un estado o todos.
   </div>
   <div>
   �	Origen de Cancelaci�n: origen donde se cancel� el C�digo de Identificaci�n de Pago.
   </div>
   <div>
   �	Rango de Fechas
   </div>
   <div style="margin-left:20px; margin-top:10px">
   Del: Fecha inicio 
   </div>
   <div style="margin-left:20px; margin-top:5px">
    Al: Fecha fin 
   </div>
   <br />
   <div>
   El rango de fechas a tomarse en cuenta depende directamente del estado seleccionado. Por ejemplo:
   </div>
   <br />
   <div>
   Si se selecciona el estado �Generada�, la b�squeda tomara en cuenta todas las ordenes que fueron creadas en ese intervalo de tiempo.
   </div>
   <br />
   <div>
   De la misma forma si el estado seleccionado es �Cancelada�, la b�squeda tomara en cuenta las �rdenes de pago canceladas en ese intervalo de tiempo.
   </div>
   <br />
   <div>
   Si el estado seleccionado es �Todos�, la b�squeda tomara en cuenta todos los estados de las �rdenes de pago con su respectivo intervalo de tiempo
   </div>
   <br />
   <div>
    <img src="../Images/Cliente3-0.JPG" alt="" /> 
   </div>
   <br />
   <br />
</asp:Content>

