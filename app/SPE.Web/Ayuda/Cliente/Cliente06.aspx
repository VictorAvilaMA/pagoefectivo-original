<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Cliente06.aspx.vb" Inherits="Ayuda_Cliente_Cliente06" title="Generaci�n de C�digo de Identificaci�n de Pago - Cliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1>Generaci�n de C�digo de Identificaci�n de Pago - Cliente</h1>
<br />
<div>Descripci�n </div>
<br />
<div>Se describe el flujo ideal o normal cuando un cliente elije pagar mediante PagoEfectivo. </div>
<br />
<div>1. El cliente una vez teniendo sus productos seleccionados elije la opci�n de pagar mediante PagoEfectivo. </div>
<br />
<div>
    <img src="../Images/Cliente6-0.JPG" alt="" />
    </div>
    <br />
    <div>2. Una vez elegida la opci�n de pagar mediante PagoEfectivo, el sistema mostrara el C�digo de Identificaci�n de Pago que ha de generar el cliente para su respectivo pago. Para ello el cliente debe autenticarse ingresando sus credenciales de PagoEfectivo. </div><br /><div>
    <img src="../Images/Cliente4_1.JPG" alt="" />
    </div>
    <br /><div>3. De no contar con credenciales el cliente, deber� registrarse haciendo clic en
    <img src="../Images/Cliente4_2.JPG" alt="" />
     el sistema mostrara la siguiente interface. </div>
     <br />
     <div>
     <img src="../Images/Cliente4_3.JPG" alt="" />
     </div><br /><div>Datos de usuario </div><br />
     <div>� Email principal: correo electr�nico del cliente. </div>
     <div>� Password: contrase�a del cliente. </div>
     <div>� Repita Password: confirmaci�n de contrase�a del cliente. </div>
     <br />
     <div>Datos personales </div><br /><div>� Alias: seud�nimo del cliente. </div>
     <div>� Nombres: nombres del cliente. </div><div>� Apellidos: apellidos del cliente. </div>
     <div>� Documento: tipo de documento de identidad </div>
     <div>� N�: n�mero del documento de identidad </div>
     <div>� Pa�s: pa�s de residencia del cliente. </div>
     <div>� Departamento: departamento de residencia del cliente. </div>
     <div>� Ciudad: ciudad de residencia del cliente. </div>
     <div>� Direcci�n: direcci�n de residencia del cliente. </div>
     <div>� Tel�fono: n�mero de tel�fono del cliente </div>
     <div>� Email alternativo: correo electr�nico alternativo del cliente </div><br />
     <div>Para proceder con el registro debe leer y aceptar los t�rminos de uso del sistema �PagoEfectivo�. </div>
     <br /><div>Adem�s ingresar el c�digo de verificaci�n de seguridad </div><br /><div>
     <img src="../Images/Cliente4_4.JPG" alt="" />
     </div><br />
    <div>Una vez ingresado y seleccionado los datos necesarios para el registro del cliente hacer clic en 
    <img src="../../images/boton-registar.jpg" alt="" />
     </div>
    <br />
    <div>4. El sistema verificar� el ingreso de los datos. Si los datos cumplen con las validaciones se visualizara la siguiente interface: </div>
    <br /><div>
        <img src="../Images/Cliente4_5.JPG" alt="" />
    </div><br /><div>5. La interface mostrara un mensaje de registro inicial exitoso.
      </div><br /><div>6. Se enviara una notificaci�n al correo electr�nico ingresado por el cliente, explicando los pasos que debe realizar para completar el proceso de su registro. Los datos enviados son: </div>
      <br />
      <div>Email </div>
      <div>Contrase�a </div>
      <div>C�digo de registro </div>
      <br />
      <div>7. En la notificaci�n se visualizara un link al cual direcciona a una interface que valida y completa el registro. </div>
      <br />
      <div>
      <img src="../Images/Cliente5-2.JPG" alt="" />
           
      </div>
      <br /><div>8. Para validar el registro se debe ingresar el �E-mail� y �C�digo Registro� el cual fue enviado a su correo. Hacer clic en 
      <img src="../Images/Cliente5-3.JPG" alt="" />
      para validar y completar el registro. Una vez realizado la validaci�n de manera satisfactoria Uds. deber� autenticarse para continuar con el flujo</div><br /><div>
            <img src="../Images/Cliente4_8.JPG" alt="" />   </div><br /><div>
            <img src="../Images/Cliente4_9.JPG" alt="" />   </div><br /><div>El sistema mostrara una interface con los datos de el C�digo de Identificaci�n de Pago a generar. Para generar la orden hacer clic en <asp:Image runat="server" ID="img12" ImageUrl="~/images/boton-generar.jpg" alt="" /> </div><br /><div>
            <img src="../Images/Cliente4_10.JPG" alt="" />  </div><br /><div>El C�digo de Identificaci�n de Pago generada muestra datos de: </div><br /><div>� La Empresa Contratante </div><div>� Servicio </div><div>� Concepto de Pago </div><div>� Monto </div><div>� El n�mero de el C�digo de Identificaci�n de Pago con su c�digo de barras respectivo. </div><br /><div>Adem�s se cuenta con la opci�n de <asp:Image runat="server" ID="img14" ImageUrl="~/Ayuda/Images/Cliente4_11.JPG" /> . Con dicha impresi�n el cliente se acercara a las agencias recaudadoras para realizar el respectivo pago. 
            
        </div>
        <br />
        <br />
</asp:Content>

