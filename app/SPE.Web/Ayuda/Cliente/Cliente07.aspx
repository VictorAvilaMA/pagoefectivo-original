<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPageBase.master" AutoEventWireup="false" CodeFile="Cliente07.aspx.vb" Inherits="Ayuda_Cliente_Cliente07" title="Registro - Cliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Registro - Cliente</h1>
<br />
<div>
La interface permite el registro de nuevos clientes. 
</div>
<br />
<div>
Datos de usuario
</div>
<br />
<div>
�	Email principal: correo electr�nico del cliente. 
</div>
<div>
�	Password: contrase�a del cliente. 
</div>
<div>
�	Repita Password: confirmaci�n de contrase�a del cliente.
</div>
<br />
<div>
Datos personales
</div>
<br />
<div>
�	Alias: seud�nimo del cliente.
</div>
<div>
�	Nombres: nombres del cliente. 
</div>
<div>
�	Apellidos: apellidos del cliente. 
</div>
<div>
�	Documento: tipo de documento de identidad
</div>
<div>
�	N�: n�mero del documento de identidad
</div>
<div>
�	Pa�s: pa�s  de residencia del cliente. 
</div>
<div>
�	Departamento: departamento de residencia del cliente. 
</div>
<div>
�	Ciudad: ciudad de residencia del cliente. 
</div>
<div>
�	Direcci�n: direcci�n de residencia del cliente.
</div>
<div>
�	Tel�fono: n�mero de tel�fono del cliente
</div>
<div>
�	Email alternativo: correo electr�nico alternativo del cliente
</div>
<br />
<div>
    <img src="../images/Cliente5-0.JPG" alt="" />
</div>
<br />
<div>
Para proceder con el registro debe leer y aceptar los t�rminos de uso del sistema �PagoEfectivo�. 
</div>
<br />
<div>
Adem�s ingresar el c�digo de verificaci�n de seguridad
</div>
<br />
<div>
    <img src="../images/Cliente5-1.JPG" alt="" />
</div>
<br />
<div>
Una vez ingresado y seleccionado los datos necesarios para el registro del cliente hacer clic en
<img src="../../images/boton-registar.jpg" alt="" />
</div>
<br />
<div>
El sistema verificar� el ingreso de los datos. Si los datos cumplen con las validaciones se registrara al cliente y enviara una notificaci�n al correo electr�nico ingresado por el cliente, explicando los pasos que debe realizar para completar el proceso de su registro. Los datos enviados son:
</div>
<br />
<div style="margin-left:20px">
Email
</div>
<div style="margin-left:20px; margin-top:2px">
Contrase�a
</div>
<div style="margin-left:20px; margin-top:2px">
C�digo de registro
</div>
<br />
<div>
En la notificaci�n se visualizara un link al cual direcciona a una interface que valida y completa el registro.
</div>
<br />
<div>
<img src="../images/Cliente5-2.JPG" alt="" />
</div>
<br />
<div>
Para validar el registro se debe ingresar el �E-mail� y �C�digo Registro� el cual fue enviado en la notificaci�n. Hacer clic en  
<img src="../images/Cliente5-3.JPG" alt="" /> para validar y completar el registro.
</div>
<br />
<div>
Una vez realizado la validaci�n de manera satisfactoria ya puede autenticarse e ingresar al sistema.
</div>
<br />
<br />

</asp:Content>

