Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class Default2
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Context.User Is Nothing Then

            If Context.User.IsInRole(RolAdministrador) Then
                Response.Redirect("~/Ayuda/Administrador/Administrador01.aspx")
            ElseIf Context.User.IsInRole(RolCajero) Then
                Response.Redirect("~/Ayuda/Cajero/Cajero01.aspx")
            ElseIf Context.User.IsInRole(RolSupervisor) Then
                Response.Redirect("~/Ayuda/Supervisor/Supervisor01.aspx")
            ElseIf Context.User.IsInRole(RolCliente) Then
                Response.Redirect("~/Ayuda/Cliente/Cliente01.aspx")
            ElseIf Context.User.IsInRole(RolRepresentante) Then
                Response.Redirect("~/Ayuda/Representante/Representante01.aspx")
            ElseIf Context.User.IsInRole(RolJefeProducto) Then
                Response.Redirect("~/Ayuda/JefeProducto/JefeP01.aspx")
            Else
                Response.Redirect("~/SEG/sinautrz.aspx")
            End If


        End If


    End Sub
End Class