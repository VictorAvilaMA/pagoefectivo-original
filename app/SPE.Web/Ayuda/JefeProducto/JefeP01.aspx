<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP01.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP01" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1> P&aacute;gina Principal - Jefe de Producto</h1>
    Esta interface muestra iconos con las opciones que el Jefe de Producto &nbsp;puede gestionar
    tales como:
    <br />
    <div id="div2">
        • Agencia Bancaria</div>
    <div id="div3">
        • Agencias Recaudadoras
    </div>
    <div id="div4">
        • Agentes
    </div>
    <div id="div5">
        • Establecimiento</div>
    <div id="div6">
        • Operadores</div>
    <div id="div7">
        • Conciliaciones<br />
        <img alt="" src="../images/PaginaPrincipaJefeProducto.JPG" style="width: 647px; height: 504px" /><br />
        <br />
        Al elegir una de las opciones, el sistema redireccionará a la interface de consulta
        de la opción elegida.
        <br />
    </div>

</asp:Content>

