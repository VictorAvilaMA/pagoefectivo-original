<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP02.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP02" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Consultar Agente - Jefe de Producto</h1>
    <div>
        Esta interface realizar b�squedas de los agentes recaudadores.
    </div>
    <br />
    <div>
        Criterios de b�squeda
    </div>
    <br />
    <div class="TextoSubtitulo">
        � Tipo Agente: nombre del tipo de agente.
    </div>
    <div>
        � Agencia: nombre de la agencia.
    </div>
    <div>
        � Nombres: nombres del agente recaudador.
    </div>
    <div>
        � Apellidos: apellidos del agente recaudador.
    </div>
    <div>
        � Estado: estado del agente recaudador.
        <br />
        <br />
        <img alt="" src="../images/ConsultarAgenteJefeProducto.JPG" style="width: 812px; height: 503px" /><br />
        <br />
        ara realizar la b�squeda hacer clic en
        <img alt="" src="../../images/boton-buscar.JPG" />
        <br />
        <div>
            <img alt="" src="../Images/Administrador3-1.JPG" />
        </div>
        <br />
        <div>
            El resultado de la b�squeda listara los agentes recaudadores que cumplan con los
            criterios de b�squeda, adem�s la opci�n de actualizar los datos de los agentes.
            Para ello hacer clic en el icono
            <img alt="" src="../../images/lupa.gif" />
        </div>
        <br />
        <div>
            <img alt="" src="../Images/Administrador3-2.JPG" />
        </div>
        <br />
        <div>
            La interface �Consultar Agente Recaudador� cuenta con el bot�n
            <img alt="" src="../../images/boton-nuevo.jpg" />
            el cual permite crear un nuevo agente recaudador.
            <br />
        <br />
        <br />
        <br />
        </div>
    </div>
</asp:Content>

