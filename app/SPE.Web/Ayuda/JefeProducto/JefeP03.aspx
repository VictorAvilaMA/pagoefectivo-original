<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP03.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP03" title="Actualizar Datos - Jefe Producto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1> Registrar Agente - Jefe Producto</h1>
   <br />
   La interface permite al Administrador registrar nuevos agentes recaudadores.
    <br />
    <div>
        Los datos necesarios para el registro son:
    </div>
    <br />
    <div>
        � Email: correo electr�nico del agente recaudador (Campo obligatorio)
    </div>
    <div>
        � Agencia Recaudadora: nombre de la agencia recaudadora, este criterio cuenta con
        una b�squeda avanzada de la agencia, hacer clic en el icono
        <img alt="" src="../../images/lupa.gif" />
        , se visualizara la siguiente ventana:
    </div>
    <br />
    <div>
        <img alt="" src="../Images/Administrador7-0.JPG" />
    </div>
    <br />
    <div>
        Para seleccionar la agencia deseada, hacer clic en el icono
        <img alt="" src="../../images/ok2.jpg" />
    </div>
    � Tipo Agente:tpara seleccionar el tipo de agente. (Campo obligatorio)
    <br />
    <div>
   �	Nombres: nombres del cliente. (Campo obligatorio)
    </div>
    <div>
   �	Apellidos: apellidos del cliente. (Campo obligatorio)
    </div>
    <div>
        � Tipo Doc.: tipo de documento de identidad
    </div>
    <div>
   �	N�: n�mero del documento de identidad
    </div>
    <div>
   �	Tel�fono: n�mero de tel�fono del cliente
    </div>
    <div>
   �	Direcci�n: direcci�n de residencia del cliente.
    </div>
    <div>
   �	Pa�s: pa�s de residencia del cliente. (Campo obligatorio)
    </div>
    <div>
   �	Departamento: departamento de residencia del cliente. (Campo obligatorio)
    </div>
    <div>
   �	Ciudad: ciudad de residencia del cliente. (Campo obligatorio)
    </div>
  <br />
  <div>
      <img src="../images/RegistrarAgenteJefeProducto.JPG" alt="" />
  </div>
  <br />
    Para registrar al agente recaudador hacer clic en
    <img alt="" src="../../images/boton-registar.jpg" />. El sistema verificar� los
    datos seleccionados e ingresados. Si los datos cumplen con las validaciones se completara
    el registro de forma exitosa.
    <br />
    <br />
  <br />
  
</asp:Content>

