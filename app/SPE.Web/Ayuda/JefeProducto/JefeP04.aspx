<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP04.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP04" title="Consultar Establecimiento - Jefe Producto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Consultar Establecimiento - Jefe Producto</h1>
<br />
<div>
La interface de Consultar Establecimiento, permite registrar los datos del Operador con los que trabajar� la aplicaci�n, para ello debe ingresar los siguientes datos:
</div>
<br />
<div>
    <ul>
        <li>C�digo Establecimiento: Identificador del Operador. Dato obligatorio. </li>
        <li>Descripci�n: Nombre con el que se reconoce al establecimiento.&nbsp;</li>
        <li>Operador: Seleccionar el Operador. </li>
        <li>Estado: Estado del Establecimiento. </li>
        <li>Nombre Comercio: Nombre del comercio donde se encuentra ubicado el establecimiento.</li>
    </ul>
</div>
<br />
<div>
<img src="../Images/consultarEstablecimiento.JPG" alt="" />
</div>
<br />
<div>
Una vez ubicado el registro se puede visualizar el detalle del establecimiento pulsando el bot�n
<img src="../../images/lupa.gif" alt="" />
y el sistema mostrar� el detalle de del establecimiento, y brindando la posibilidad de Actualizar los datos del mismo. En caso realice alg�n cambio pulsar el bot�n 
<img src="../../images/boton-actualizar1.JPG" alt="" />, para borrar los datos ingresados 
<img src="../../images/boton-limpiar.JPG" alt="" />
</div>
<br />
<div>
<img src="../Images/ActualizarEstablecimiento.JPG" alt="" />
</div>
<br />
<br />
</asp:Content>

