<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP05.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP05" title="Consultar Operador - Jefe Producto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1> Consultar Operador - Jefe Producto</h1>
   <br />
   <div>
   La interface de Consultar Operador, permite consultar los datos del Operador, para ello debe ingresar los siguientes criterios de b�squeda:
   </div>
   <br />
   <div>
   �	C�digo: Ingresar el c�digo del Operador.
   </div>
   <div>
   �	Nombre: Nombre del Operador. 
   </div>
   <div>
   �	Estado: Seleccionar el estado del Operador.
   </div>
   <div>
   Una vez ingresado los criterios de b�squeda pulsar en el bot�n 
       <img src="../../images/boton-buscar.JPG" alt="" />
    </div>
   <br />
   <div>
   En caso de que el sistema encuentre coincidencias de acuerdo a los criterios de b�squeda se mostrar� un listado de registros. En caso desee visualizar m�s detalle de un determinado Operador, pulsar en la imagen 
       <img src="../../images/lupa.gif" alt="" />
   </div>
   <br />
   <div>
       <img src="../Images/ConsultarOperador.JPG" alt="" />
   </div>
   <br />
   <div>
   El sistema mostrar� la interface de actualizaci�n de datos del Operador, en el cual en caso se considere conveniente podr� actualizar los datos del Operador. Para ello una vez realizado los cambios pulsar el bot�n 
       <img src="../../images/boton-actualizar1.JPG" alt="" />
   </div>
   <br />
   <div>
       <img src="../Images/ActualizarOperador.JPG" alt="" />
   </div>
   <br />
   <br />
</asp:Content>

