<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP06.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP06" title="Registrar Establecimiento - Jefe Producto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1>Registrar Establecimiento - Jefe Producto</h1>
   <br />
   <div>
   La interface de Registrar Establecimiento, permite registrar los datos del Establecimiento, para ello debe ingresar los siguientes datos:
   </div>
   <br />
   <div>
   �	C�digo de Establecimiento: Ingresar el c�digo del establecimiento.
       <br />
       Descripci�n: Nombre con el que se reconocer� al establecimiento.</div>
   <div>
   �	Operador: Seleccionar Operador.
   </div>
   <div>
       Nombre Comercio: Nombre del comercio donde se ubicar� el establecimiento. Este se
       seleccionar� de una lista emergente al hacer clic sobre la lupa.</div>
   <br />
   <div>
   Una vez ingresado los criterios de b�squeda pulsar en el bot�n 
   <img src="../../images/boton-registar.jpg" alt="" />
   
   </div>
   <br />
   <div>
   De cumplir con las validaciones correspondientes el sistema confirmara su registro. De no cumplir con las validaciones el sistema mostrara una lista con las observaciones del caso.
   </div>
   <br />
   <div>
       <img src="../Images/registrarEstablecimiento.JPG" alt="" />
   </div>
   <br />
   <br />
</asp:Content>

