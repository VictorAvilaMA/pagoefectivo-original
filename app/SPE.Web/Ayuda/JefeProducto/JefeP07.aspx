<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP07.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP07aspx" title="Registrar Operador - Jefe Producto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Registrar Operador - Jefe Producto</h1>
<br />
<div>
La interface de Registrar Operador permite registrar los datos del Operador con los que trabajar� la aplicaci�n, para ello debe ingresar los siguientes datos:
</div>
<br />
<div>
�	C�digo: Identificador del Operador. Dato obligatorio.
</div>
<div>
�	Desc.: Nombre del Operador. 
</div>
<br />
<div>
Para registrar dichos cambios, debe hacer clic en el bot�n 
<img src="../../images/boton-registar.jpg" alt="" />
</div>
<br />
<div>
De cumplir con las validaciones correspondientes el sistema confirmara su registro. De no cumplir con las validaciones el sistema mostrara una lista con las observaciones del caso.
</div>
<br />
<div>
<img src="../Images/registrarOperador.JPG" alt="" />
</div>
<br />
<br />
</asp:Content>

