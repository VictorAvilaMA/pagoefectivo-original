<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP09.aspx.vb" Inherits="Ayuda_JefeProducto_Jefe09" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <h1>
        Registrar Comercio - Jefe Producto</h1>
    <br />
    <div>
        Esta interface permite realizar registro de Comercios.</div>
    <br />
    <div>
        <ul>
            <li>&nbsp;C�digo Comercio: C�digo del comercio&nbsp;</li>
            <li>&nbsp;Raz�n Social: Nombre de la raz�n social del cliente&nbsp;</li>
            <li>&nbsp;RUC: N�mero del Ruc del cliente </li>
            <li>&nbsp;Contacto: El nombre del contacto del cliente.</li>
            <li>&nbsp;Tel�fono: N�mero de tel�fono del comercio.</li>
            <li>&nbsp;Direcci�n: Direcci�n exacta del Comercio.</li>
            <li>&nbsp;Ubicaci�n: Estado del cliente Activo, Inactivo</li>
        </ul>
    </div>
    <div>
    </div>

    <br />
    <div>
        Para registrar, debe hacer clic en el bot�n
        <img alt="" src="../../images/boton-registar.jpg" />
    </div>
    <br />
    <div>
        De encontrarse todos los datos debidamente ingresados el sistema registrar�&nbsp;
        el nuevo comercio..</div>
    <br />
    <div>
        <img alt="" src="../Images/registrarcomercio.JPG" />
    </div>
    <br />
    <br />
</asp:Content>

