<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP11.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP11" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <h1> Reporte de Cajas Liquidadas - Jefe Producto</h1>
   <br />
   <div>
   La interface de Actualizar Cliente permite actualizar los datos del cliente, tales como:
   </div>
   <br />
   <div>
       <ul>
           <li>Comercio: Seleccione el nombre del comercio para emitir el reporte.</li>
           <li>Fecha Inicio: Fecha menor de liquidaci�n .</li>
           <li>Fecha Fin: Fecha mayor de liquidaci�n.</li>
           <li>N�mero de C.I.P.: N�mero de C�digo de Identificaci�n de Pago. (Nro obligatorio).</li>
       </ul>
   </div>
   <div>
       &nbsp;</div>
  <br />
  <div>
  Para registrar dichos cambios, debe hacer clic en el bot�n 
  <img src="../../images/boton_verReporte.JPG" alt="" />
    </div>
  <br />
  <div>
  De existir datos en las fechas ingresadas. El sistema mostrar� el reporte como se
      muestra en la imagen.</div>
  <br />
  <div>
      <img src="../Images/ReporteCajasLiquidadas.JPG" alt="" />
  </div>
    Siendo mostrado el reporte con los siguientes campos:<br />
    <br />
    <ul>
        <li>Estab. : C�digo del Establecimiento (Campo agrupado por Fecha).</li>
        <li>Fecha Apertura: Fecha de liquidaci�n menor de el C�digo de Identificaci�n de Pago acumulada.</li>
        <li>Fecha Cierre: Fecha de liquidaci�n mayor &nbsp;de el C�digo de Identificaci�n de Pago acumulada.</li>
        <li>Medio de Pago: Descripci�n del medio de pago.</li>
        <li>Monto: Monto a pagar de la orden de compra generada.</li>
        <li>OP: Nro de Orden de compra generada.</li>
    </ul>
    <br />
  <br />
  <div>
      Puede exportar sus datos a un Formato PDF o Excel.&nbsp;</div>
  <br />
  <br />
</asp:Content>

