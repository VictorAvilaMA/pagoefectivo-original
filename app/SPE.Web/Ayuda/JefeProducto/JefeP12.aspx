<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP12.aspx.vb" Inherits="Ayuda_JefeProducto_Jefe12" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1>
        Liquidar Caja en Bloque - Jefe Producto</h1>
    <br />
    <div>
        Esta interface permite realizar liquidaciones en bloque: 

    </div>
    <br />
    <div>
        �	C�digo Comercio: C�digo del comercio.
    </div>
    <div>
        � Raz�n Social: Nombre de la raz�n social del cliente.
    </div>
    <div>
        �Fecha M�nima: Fecha menor de una caja cerrada.<br />
        Fecha M�xima: Fecha mayor de una caja cerrada.<br />
        # : N�mero de cajas cerradas por comercio.</div>
    <div>
        &nbsp;</div>

    <br />
    <div>
        Para ver el detalle de la caja, debe hacer clic en el bot�n
        <img alt="" src="../../images/lupa.gif" />
    </div>
    <br />
    <br />
    <div>
        <img alt="" src="../Images/liquidarCajaBloque.JPG" />
    </div><div>
        La siguiente ventana nos muestra el detalle por cajas cerradas para ser liquidadas.<br />
        <br />
        � Serie POS: Muestra el c�digo del POS.&nbsp;</div>
    <div>
        �Fecha Apertura: Fecha de apertura de la caja.<br />
        Fecha Cierre: Fecha de cierre de la caja.<br />
        C.I.P.: N�mero de �rdenes de compra dentro de la caja cerrada.<br />
        Liquidar: Caja para seleccionar si se desea cerrar la caja:<br />
        <br />
        Clic sobre la lupa si se desea ver el detalle de la caja cerrada.<br />
        <br />
        Para liquidar clic sobre:
         <img alt="" src="../../images/Boton_liquidar.jpg" />
        <br />
    <br />
    <br />
    </div>
    <div>
    <div>
        <img alt="" src="../Images/liquidarCajaBloque2.JPG" />
    </div>
</asp:Content>

