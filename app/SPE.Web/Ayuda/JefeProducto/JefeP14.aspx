<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP14.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP14" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1>Consultar Agencia Recaudadora - Jefe de Producto</h1> <div id="div1">
        Esta interface realizar b�squedas de agencias recaudadoras.
    </div>
    <br />
    <div id="div2">
        Criterios de b�squeda
    </div>
    <br />
    <div id="div3">
        � Nombre Comercial: nombre de la agencia.
    </div>
    <div id="div4">
        � Contacto: contacto de la agencia.
    </div>
    <div id="div5">
        � Estado: estado de la agencia.
        <br />
        <br />
        <img alt="" src="../images/ConsultarAgenciaRecaudadoraJefeProducto.JPG" style="width: 812px;
            height: 499px" /><br />
        Para realizar la b�squeda hacer clic en &nbsp;
        <img alt="" src="../../images/boton-buscar.JPG" />
        <div id="div7">
            <br />
            <br />
            El resultado de la b�squeda listara las agencias recaudadoras que cumplan con los
            criterios de b�squeda.
        </div>
        <img alt="" src="../images/Administrador2-1.JPG" />
        <div id="div8">
            &nbsp;</div>
        <div id="div9">
            Cada Agencia Recaudadora contara con la opci�n de actualizar sus datos. Para ello
            hacer clic en el icono
            <img alt="" src="../../images/lupa.gif" />
            de la agencia a actualizar.
        </div>
        <div id="div10">
            <img alt="" src="../Images/Administrador2-2.JPG" />
        </div>
        <div id="div11">
            La interface cuenta con el bot�n
            <img alt="" src="../../images/boton-nuevo.jpg" />
            el cual permite crear una nueva agencia.<br />
        <br />
        <br />
        <br />
        <br />
        </div>
    </div>
</asp:Content>

