<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JefeP15.aspx.vb" Inherits="Ayuda_JefeProducto_JefeP15" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1> Consultar Agencia Bancaria - Jefe de Producto</h1><div id="div1">
        Esta interface realizar b�squedas de agencias recaudadoras.
    </div>
    <br />
    <div id="div2">
        Criterios de b�squeda
    </div>
    <br />
    <div id="div3">
        � Codigo: codigo de la agencia.
        <br />
        � Descripcion: descripcion de la agencia.
        <br />
        � Estado: estado de la agencia.
        <br />
        <img alt="" src="../images/ConsultaAgenciaBancariaJefeProducto.JPG" style="width: 761px;
            height: 530px" /><br />
        Para realizar la b�squeda hacer clic en &nbsp;
        <img alt="" src="../../images/boton-buscar.JPG" />
        <br />
        <br />
        El resultado de la b�squeda listara las agencias recaudadoras que cumplan con los
        criterios de b�squeda.
        <br />
        <br />
        <img alt="" src="../images/BusquedaAgenciaBancaria.JPG" /><br />
        <br />
        Cada Agencia Recaudadora contara con la opci�n de actualizar sus datos. Para ello
        hacer clic en el icono
        <img alt="" src="../../images/lupa.gif" />
        de la agencia a actualizar.
        <br />
        <br />
        <img alt="" src="../images/ActualizarAgenciaBancariaJefeProducto.JPG" style="width: 518px;
            height: 438px" /><br />
        La interface cuenta con el bot�n
        <img alt="" src="../../images/boton-nuevo.jpg" />
        el cual permite crear una nueva agencia.<br />
        <br />
        <br />
        <br />
    </div>
    <div id="div4">
        &nbsp;</div>
</asp:Content>

