<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="JeferP16.aspx.vb" Inherits="Ayuda_JefeProducto_JeferP16" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Registrar Agencia Bancaria - Jefe de Producto</h1><br />
    <div>
        La interface permite al Jefe de Producto registrar nuevas Agencias Bancarias&nbsp;</div>
    <br />
    <div>
        Los datos necesarios para el registro son:
        <br />
    </div>
    <div>
        � Nombre: nombres de la agencia bancaria. (Campo obligatorio)
    </div>
    <div>
        � Direcci�n: direcci�n de la agencia bancaria.
        <br />
        � Codigo: codigo de la agencia&nbsp; bancaria. (Campo obligatorio)
        <br />
        � Banco: para seleccionar el banco.
        <br />
        � Pa�s: para seleccionar el pais. (Campo obligatorio)
    </div>
    <div>
        � Departamento: para seleccionar el departamento. (Campo obligatorio)
    </div>
    <div>
        � Ciudad: para seleccionar la ciudad. (Campo obligatorio)
    </div>
    <div>
        &nbsp;</div>
    <br />
    <div>
        <img alt="" src="../images/RegistrarAgenciaBancariaJefeProducto.JPG" style="width: 788px; height: 576px" />
    </div>
    <br />
    <div>
        Para registrar al cliente hacer clic en
        <img alt="" src="../../images/boton-registar.jpg" />
        El sistema verificar� los datos seleccionados e ingresados. Si los datos cumplen
        con las validaciones se completara el registro de forma exitosa.
    </div>
    <br />
    
</asp:Content>

