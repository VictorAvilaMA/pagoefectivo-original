Imports System.Data
Imports System.Web.Security
Imports _3Dev.FW.Web.Security
Imports System.IO
Imports SPE.EmsambladoComun.ParametrosSistema
Partial Class Ayuda_MasterPageHelp01
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New DataSet()


        If (UserInfo.Rol = "Administrador") Then

            ds.ReadXml(Server.MapPath("XMLMenu.xml"))

            Me.MenuPE.Items.Clear()

            For Each rw As DataRow In ds.Tables(0).Rows
                MenuPE.Items.Add(New MenuItem(rw("name"), "", "", rw("url")))
            Next

        ElseIf (UserInfo.Rol = "Cliente") Then
            ds.ReadXml(Server.MapPath("XMLMenuRegistrado.xml"))
            Me.MenuPE.Items.Clear()
            For Each rw As DataRow In ds.Tables(0).Rows

                MenuPE.Items.Add(New MenuItem(rw("name"), "", "", rw("url")))
            Next
        ElseIf (Context.User.IsInRole(RolJefeProducto)) Then
            ds.ReadXml(Server.MapPath("XMLJefeProductoMenu.xml"))
            Me.MenuPE.Items.Clear()

            For Each rw As DataRow In ds.Tables(0).Rows
                MenuPE.Items.Add(New MenuItem(rw("name"), "", "", rw("url")))
            Next

        ElseIf IsDBNull(UserInfo.Rol) Then
            ds.ReadXml(Server.MapPath("XMLMenuRegistrado.xml"))
            Me.MenuPE.Items.Clear()
            For Each rw As DataRow In ds.Tables(0).Rows

                MenuPE.Items.Add(New MenuItem(rw("name"), "", "", rw("url")))
            Next

        End If

    End Sub
    
End Class

