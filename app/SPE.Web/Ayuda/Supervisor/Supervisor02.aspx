<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Supervisor02.aspx.vb" Inherits="Ayuda_Supervisor_Supervisor02" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Aperturar Caja - Supervisor</h1>
   <br />
   <div>
   La interface de Aperturar Caja permite aperturar una caja para un agente recaudador.</div>
   <br />
   <div>
   �	Agente recaudador: Nombre del agente recaudador para la caja<br />
       � Caja: Filtro con las cajas disponibles para el agente<br />
       � Fecha Apertura: Fecha de apertura de la caja<br />
       � Moneda: Tipo de moneda con la que se aperturar� la caja<br />
       � Saldo Inicial: Saldo inicial para la apertura de la caja<br />
       � Supervisor: Nombre del supervisor que autoriza la apertura de la caja.<br />
       �Contrase�a: Contrase�a del supervisor autorizando la apertura de la caja.</div>
   <div>
       &nbsp;</div>
  <br />
  <div>
  Para registrar dichos cambios, debe hacer clic en el bot�n 
  <img src="../../images/buton-aperturar.JPG" alt="" />
    </div>
  <br />
  <div>
  De cumplir con las validaciones correspondientes el sistema confirmara su apertura.
      De no cumplir con las validaciones el sistema mostrar� una lista con las observaciones del caso.
  </div>
  <br />
  <div>
      <img src="../Images/aperturarCaja.JPG" alt="" />
  </div>
  <br />
  <br />
  
</asp:Content>

