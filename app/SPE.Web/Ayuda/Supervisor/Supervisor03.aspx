<%@ Page Language="VB" Async="true" MasterPageFile="~/Ayuda/MasterPageHelp01.master" AutoEventWireup="false" CodeFile="Supervisor03.aspx.vb" Inherits="Ayuda_Supervisor_Supervisor03" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1> Anular Caja- Supervisor</h1>
   <br />
   <div>
   La interface permite anular la caja aperturada.</div>
   <br />
   <div>
   �	Agente Recaudador: Nombre del agente recaudador.<br />
       &nbsp;Caja: N�mero
   </div>
   <div>
   �	Apellidos: Los apellidos del cliente. Dato obligatorio.
   </div>
   <div>
   �	Documento: Tipo de documento con que el cliente se puede identificar.
   </div>
   <div>
   �	N�: Numero del documento con que se identifica.
   </div>
   <div>
   �	Tel�fono: N�mero telef�nico del cliente.
   </div>
   <div>
   �	Mail Alternativo: Mail alternativo con que cuenta el cliente.
   </div>
   <div>
   �	Pa�s: El pa�s donde reside el cliente. Dato obligatorio. 
   </div>
   <div>
   �	Departamento: El departamento donde reside el cliente. Dato obligatorio.
   </div>
   <div>
   �	Ciudad: La ciudad donde reside el cliente. Dato obligatorio.
   </div>
   <div>
   �	Direcci�n: La direcci�n donde reside el cliente
   </div>
  <br />
  <div>
  Para registrar dichos cambios, debe hacer clic en el bot�n 
  <img src="../../images/boton-actualizar1.JPG" alt="" />
    </div>
  <br />
  <div>
  De cumplir con las validaciones correspondientes el sistema confirmara su actualizaci�n. De no cumplir con las validaciones el sistema mostrara una lista con las observaciones del caso.
  </div>
  <br />
  <div>
      <img src="../Images/anularcaja.JPG" alt="" />
  </div>
  <br />
  <div>
  Una vez que se han ingresado los datos solicitados pulsar el bot�n 
    <img src="../../images/boton-actualizar1.JPG" alt="" />. En caso desee borrar los datos que se ingresaron pulsar el bot�n
    <img src="../../images/boton-limpiar.JPG" alt="" />
  </div>
  <br />
  <br />
  
</asp:Content>

