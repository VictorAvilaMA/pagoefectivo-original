﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CIP.aspx.vb" Inherits="CIP" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="head1" runat="server">
    <meta charset="utf-8">
    <title>PagoEfectivo - Transacciones Seguras por Internet en el Perú</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if lt IE 9]>
  <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/js/html5.js"></script>
<![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="perucomsultores.pe" />
    <meta name="viewport" content="width=device-width" />
    <meta name="language" content="es" />
    <meta name="geolocation" content="Peru" />
    <meta name="robots" content="index,follow" />
    <meta property="og:title" content="PagoEfectivo - Transacciones Seguras por Internet en el Perú" />
    <meta property="og:description" content="" />
    <meta property="og:image" content='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/CIPGenerado/pago-efectivo.png" %>' />
    <link href="favicon.ico" rel="icon" type="image/vnd.microsoft.icon" />
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/GenPago.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <%--<link href="css/style.css" rel="stylesheet" type="text/css"/>--%>
</head>
<body>
    <form id="Form1" runat="server">
    <!-- Begin comScore UDM code 1.1104.26 -->
    <script type="text/javascript">
// <![CDATA[
        function comScore(t) { var b = "comScore", o = document, f = o.location, a = "", e = "undefined", g = 2048, s, k, p, h, r = "characterSet", n = "defaultCharset", m = (typeof encodeURIComponent != e ? encodeURIComponent : escape); if (o.cookie.indexOf(b + "=") != -1) { p = o.cookie.split(";"); for (h = 0, f = p.length; h < f; h++) { var q = p[h].indexOf(b + "="); if (q != -1) { a = "&" + unescape(p[h].substring(q + b.length + 1)) } } } t = t + "&ns__t=" + (new Date().getTime()); t = t + "&ns_c=" + (o[r] ? o[r] : (o[n] ? o[n] : "")) + "&c8=" + m(o.title) + a + "&c7=" + m(f && f.href ? f.href : o.URL) + "&c9=" + m(o.referrer); if (t.length > g && t.indexOf("&") > 0) { s = t.substr(0, g - 8).lastIndexOf("&"); t = (t.substring(0, s) + "&ns_cut=" + m(t.substring(s + 1))).substr(0, g) } if (o.images) { k = new Image(); if (typeof ns_p == e) { ns_p = k } k.src = t } else { o.write('<p><' + 'img src="' + t + '" height="1" width="1" alt="*"></p>') } };
        comScore('http' + (document.location.href.charAt(4) == 's' ? 's://sb' : '://b') + '.scorecardresearch.com/p?c1=2&c2=6906602&amp;ns_site=<%=Session("NomSite").ToString()%>&name=<%=Session("NomServDAX").ToString()%>');
// ]]>
    </script>
    <noscript>
        <p>
            <img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6906602&amp; ns_site=<%=Session("NomSite").ToString()%>&amp;name=<%=Session("NomServDAX").ToString()%>"
                height="1" width="1" alt="*"></p>
    </noscript>
    <!-- End comScore UDM code -->
    <div class="hack-shadow">
    </div>
    <header class="cip">
        <div class="content-header">        
    	    <div class="top-head">
                <div class="content-top-head">
        	        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/grupo-comercio.png" width="97" height="16" alt="Grupo El Comercio" title="Grupo El Comercio" border="0" />
                </div>
            </div>
            <div class="head">
                <div class="content-head">
        	        <h1><img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/pago-efectivo.png" width="149" height="60" alt="Pago Efectivo - Transacciones seguras por Internet en el Perú" title="Pago Efectivo - Transacciones seguras por Internet en el Perú">&nbsp;</h1>
                    <h2>Transacciones seguras por Internet en el Perú</h2>
                        <asp:LinkButton CssClass="btn-back" ID="lkbRegresar" runat="server" Text="<< Regresar"></asp:LinkButton>
                </div>         
            </div>
        </div>
               
    </header>
    <div id="wrapper">
        <div class="content-cip">
            <h2>
                Gracias por su preferencia</h2>
            <asp:Literal ID="ltlConfirmGen" runat="server"></asp:Literal>
            <%--<p class="desc">Estimado cliente, confirmamos la operación y procesaremos su orden conforme a lo solicitado. Imprima la orden y acérquese a cualquier centro autorizado para efectuar el pago correspondiente. Para mayor seguridad, una copia de esta operación ha sido enviado a su correo <a href="mailto:sucorreo@dominio.com">sucorreo@dominio.com</a></p>--%>
            <div class="content-company">
                <div class="content-paid">
                    <asp:Image ID="imgServicio" runat="server" Style="border-width: 0px; width: 225px;" />
                    <hr />
                    <p class="number-order">
                        <strong>N°Pedido:</strong>
                        <asp:Literal ID="ltlCodTransaccion" runat="server"></asp:Literal></p>
                    <p>
                        <strong>
                            <asp:Literal ID="ltlNombreServicio" runat="server"></asp:Literal>
                            :
                            <asp:Literal ID="ltlConceptoPago" runat="server"></asp:Literal></strong>.
                        <asp:Literal ID="ltlConceptoPagoAdicional" runat="server"></asp:Literal></p>
                    <hr />
                    <%-- <p>
                        <asp:Literal ID="ltlConceptoPago" runat="server"></asp:Literal>
                    </p>--%>
                    <p class="price">
                        Total a Pagar: <span>
                            <asp:Literal ID="ltlTotal" runat="server"></asp:Literal></span> (incluye IGV)</p>
                    <asp:Image ID="imgCIP" runat="server" AlternateText="Código de barras" />
                    <p class="code">
                        CIP:
                        <asp:Literal ID="ltlCIP" runat="server"></asp:Literal></p>
                </div>
                <p>
                    <asp:Literal ID="ltlAtencion" runat="server"></asp:Literal></p>
            </div>
            <p class="desc2">
                Puede efectuar el pago del aviso en las agencia de:</p>
            <div class="content-terms">
                <div class="terms">
                    <asp:Literal ID="ltSeccionBancos" runat="server"></asp:Literal>
                </div>
                <div class="content-save">
                    <a href="javascript:;" onclick="window.print();" title="Imprimir" class="print"><span
                        class="icon"></span>Imprimir</a> <a href="javascript:;" style="display: none" title="Guardar archivo"
                            class="save"><span class="icon"></span>Guardar archivo</a>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn btn-primary btn-large" style="display: none">
                        Cerrar</button>
                    <%--<button type="submit" id="btnGuardar" runat="server" class="btn btn-primary btn-large">
                        Guardar</button>--%>
                    <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary btn-large" Visible="false"
                        Text="Guardar" />
                </div>
            </div>
        </div>
    </div>
    <footer id="foot1" runat="server">
    	<div id="content-footer">
        	<span class="foot-logo">
            	<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/foot-pago-efectivo.png" width="97" height="40" alt="Pago Efectivo" title="Pago Efectivo" border="0">
            </span>
        	<ul>
            	<li><a href="Default.aspx">Inicio</a></li>
                <li class="break">|</li>
                <li><a href="Personas.aspx">Personas</a></li>
                <li class="break">|</li>
                <li><a href="Empresas.aspx">Empresa</a></li>
                <li class="break">|</li>
                <li><a href="Contactenos.aspx">Contáctenos</a></li>
                <li class="break">|</li>
                <li><a href="http://centraldeayuda.pagoefectivo.pe">Ayuda</a></li>
            </ul>
            <a class="book-complaints" href="javascript:;">Libro de reclamaciones</a>
            <a class="help-center" href="http://centraldeayuda.pagoefectivo.pe">
            	<span class="img"></span>
                <span class="content-help">
                	<span class="title">Central de Ayuda</span>
                	<span class="desc">¿Tienes alguna duda o consulta? estamos para ayudarte</span>
                </span>                
            </a>         
        </div>
        <div class="footer-link">
        	<span>Visite también:</span>
        	<ul>
            	<li class="first"><a target="_blank" title="Diario el Comercio" href="http://elcomercio.pe/?ref=pef">elcomercio.pe</a></li>
                <li><a target="_blank" title="Peru.com" href="http://peru.com/?ref=pef">peru.com</a></li>
                <li><a target="_blank" title="Diario Perú21" href="http://peru21.pe/?ref=pef">peru21.pe</a></li>
                <li><a target="_blank" title="Diario Gestión" href="http://gestion.pe/?ref=pef">gestion.pe</a></li>
                <li><a target="_blank" title="Depor.pe" href="http://depor.pe/?ref=pef">depor.pe</a></li>
                <li><a target="_blank" title="Diario Trome" href="http://trome.pe/?ref=pef">trome.pe</a></li>
                <li><a target="_blank" title="Publimetro" href="http://publimetro.pe/?ref=pef">publimetro.pe</a></li>
                <li><a target="_blank" title="GEC" href="http://gec.pe/?ref=pef">gec.pe</a></li>
                <li><a target="_blank" title="Clasificados.pe" href="http://clasificados.pe/?ref=pef">clasificados.pe</a></li>
                <li><a target="_blank" title="Aptitus" href="http://aptitus.pe/?ref=pef">aptitus.pe</a></li>
                <li><a target="_blank" title="Urbania" href="http://urbania.pe/?ref=pef">urbania.pe</a></li>
                <li><a target="_blank" title="Neoauto" href="http://neoauto.pe/?ref=pef">neoauto.pe</a></li>
                <li class="first"><a target="_blank" title="GuiaGPS" href="http://guiagps.pe/?ref=pef">guiagps.pe</a></li>
                <li><a target="_blank" title="Ofertop" href="http://ofertop.pe/?ref=pef">ofertop.pe</a></li>
                <li><a target="_blank" title="iQuiero" href="http://iquiero.com/?ref=pef">iquiero.com</a></li>
                <li><a target="_blank" title="NuestroMercado" href="http://nuestromercado.pe/?ref=pef">nuestromercado.pe</a></li>
                <li><a target="_blank" title="Club de Suscriptores" href="http://clubsuscriptores.pe/?ref=pef">clubsuscriptores.pe</a></li>
                <li><a target="_blank" title="PeruRed" href="http://perured.pe/?ref=pef">perured.pe</a></li>
                <li><a target="_blank" title="Shopin" href="http://shopin.pe/?ref=pef">shopin.pe</a></li>
            </ul>         
        </div>
        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-1.6.1.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/slides.min.jquery.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/miniApp.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/modules.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    </footer>
    </form>
    <script language="JavaScript1.3" src="https://sb.scorecardresearch.com/c2/6906602/ct.js"></script>
</body>
</html>
