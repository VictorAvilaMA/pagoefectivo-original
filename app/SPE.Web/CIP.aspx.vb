﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Net
Imports System.IO
Imports SPE.Web.Util
Imports System.Threading
Imports System.Collections.Generic
Imports System.Globalization
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports _3Dev.FW.Web.Log

Partial Class CIP
    Inherits System.Web.UI.Page
    Public oBEOrdenPago As New BEOrdenPago()
    Public oBESolicitudPago As New BESolicitudPago()
    Public oBEServicio As New BEServicio()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnGuardar.Visible = False
        If Not IsPostBack Then
            Me.head1.DataBind()
            foot1.DataBind()
            findControlToDataBind(Me.Controls)
            Dim CtrlOrdenPago As New COrdenPago()
            Dim CtrlServicio As New CServicio()
            Dim oBEServicio As New BEServicio()
            ' Dim oBEDatosPDF As New BEDatosPDF()
            Dim RutaRaiz As String = ""


            '1.Consultar por token. Si existe o no.
            If HttpContext.Current.Request.QueryString("token") Is Nothing Then
                Response.Redirect("CentrosPago.aspx")
            Else
                '2.En caso exista obtenerlo y consultar los objetos
                Dim Token As String = HttpContext.Current.Request.QueryString("token")
                Dim oBESolicitudPago As New BESolicitudPago
                oBESolicitudPago.Token = Token
                oBESolicitudPago = CtrlOrdenPago.ConsultarSolicitudPagoPorTokenInst(oBESolicitudPago)

                If oBESolicitudPago Is Nothing Then
                    Session("NomSite") = ""
                    Session("NomServDAX") = ""
                    JSMessageAlertWithRedirect("Validacion", "No se encuentra una solicitud pendiente con el Token: " + Token, "key", "CentrosPago.aspx")

                Else
                    Select Case oBESolicitudPago.Idestado
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.GeneradaCIP
                            oBEOrdenPago.IdOrdenPago = oBESolicitudPago.IdOrdenPago
                            oBEOrdenPago = CtrlOrdenPago.ConsultarOrdenPagoPorId(oBEOrdenPago)
                            'Al obtener ambos objetos mostrarlos
                            If oBEOrdenPago IsNot Nothing And oBESolicitudPago IsNot Nothing Then
                                oBEServicio = CtrlServicio.ObtenerServicioPorID(oBEOrdenPago.IdServicio)
                                'Tagueo DAX - Init
                                Try
                                    Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaServiciosTagDaxReg")
                                    Dim ListServDaxReg As New List(Of ServicioDAX)
                                    Dim Linea As String = ""
                                    Dim Info As String()
                                    'Dim SiteTag As String = ""
                                    'Dim URLTag As String = ""
                                    'Dim NombreDax As String = ""
                                    Dim NombreServicio As String = ""
                                    Dim NombreSiteTag As String = ""
                                    Dim NombreDax As String = ""
                                    Dim lector As New StreamReader(ruta)
                                    Do
                                        Dim ServDaxReg As New ServicioDAX
                                        Linea = lector.ReadLine()
                                        If Not Linea Is Nothing Then
                                            Info = Linea.Split(",")
                                            NombreServicio = Info(0).ToString
                                            NombreSiteTag = Info(1).ToString
                                            NombreDax = Info(2).ToString & oBEOrdenPago.NumeroOrdenPago
                                            ServDaxReg.NombreServicio = NombreServicio
                                            ServDaxReg.NombreSiteTag = NombreSiteTag
                                            ServDaxReg.NombreDax = NombreDax
                                            ListServDaxReg.Add(ServDaxReg)
                                        End If
                                    Loop Until Linea Is Nothing
                                    For i As Integer = 0 To ListServDaxReg.Count - 1
                                        If ListServDaxReg(i).NombreServicio = oBEServicio.Nombre Then
                                            Session("NomSite") = ListServDaxReg(i).NombreSiteTag
                                            Session("NomServDAX") = ListServDaxReg(i).NombreDax
                                            Exit For
                                        Else
                                            Session("NomSite") = ""
                                            Session("NomServDAX") = ""
                                        End If
                                    Next
                                    lector.Close()
                                Catch ex As Exception
                                    'Logger.LogException(ex)
                                End Try
                                'Tagueo DAX - Fin

                                ViewState("oBEOrdenPago") = oBEOrdenPago
                                ViewState("oBESolicitudPago") = oBESolicitudPago
                                ViewState("oBEServicio") = oBEServicio

                                imgServicio.ImageUrl = System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/logopagoefectivo.png"
                                imgServicio.Width = "225"
                                imgServicio.BorderWidth = "0"
                                imgCIP.ImageUrl = "CLI/ADOPImg.aspx?numero=" & oBEOrdenPago.NumeroOrdenPago

                                ltlCIP.Text = oBEOrdenPago.NumeroOrdenPago
                                ltlTotal.Text = IIf(oBEOrdenPago.IdMoneda = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles, "S/. ", "$ ") & oBEOrdenPago.Total
                                ltlNombreServicio.Text = oBEServicio.Nombre
                                ltlCodTransaccion.Text = oBESolicitudPago.CodTransaccion
                                ltlConceptoPago.Text = oBESolicitudPago.ConceptoPago
                                ltlConceptoPagoAdicional.Text = String.Empty
                                For Each item As BEParametroEmail In oBESolicitudPago.ParametrosEmail
                                    If (item.Nombre = "ConceptoPago") Then
                                        ltlConceptoPagoAdicional.Text = item.Valor
                                    End If
                                Next


                                Dim ci As New CultureInfo("Es-Es")
                                Dim diaDeSemana As String = ci.DateTimeFormat.GetDayName(oBEOrdenPago.FechaVencimiento.DayOfWeek)
                                Dim diaNumero As String = oBEOrdenPago.FechaVencimiento.Day.ToString().PadLeft(2, "0")
                                Dim mesNombre As String = ci.DateTimeFormat.GetMonthName(oBEOrdenPago.FechaVencimiento.Month)

                                Dim CtrPlantilla As New CPlantilla()
                                Dim valoresDinamicos As New Dictionary(Of String, String)
                                valoresDinamicos.Add("[CIP]", oBEOrdenPago.NumeroOrdenPago)
                                valoresDinamicos.Add("[FechaVencimiento]", oBEOrdenPago.FechaVencimiento.ToString(" dddd dd") & " de " & oBEOrdenPago.FechaVencimiento.ToString("MMMM "))
                                valoresDinamicos.Add("[HoraVencimiento]", oBEOrdenPago.FechaVencimiento.ToString(" hh:mm tt"))
                                valoresDinamicos.Add("[EmailCLiente]", oBEOrdenPago.UsuarioEmail)
                                ltlAtencion.Text = CtrPlantilla.GenerarPlantillaToHtmlByTipo(oBEOrdenPago.IdServicio, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.VencPago, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
                                ltSeccionBancos.Text = CtrPlantilla.ConsultarPLantillasXServicioGenPago(oBEOrdenPago.IdServicio, oBEOrdenPago.IdMoneda, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoPagoGeneral, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
                                ltlConfirmGen.Text = CtrPlantilla.GenerarPlantillaToHtmlByTipo(oBEOrdenPago.IdServicio, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoConfirm, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
                                oBEOrdenPago = Nothing
                                oBESolicitudPago = Nothing
                            Else
                                Session("NomSite") = ""
                                Session("NomServDAX") = ""
                                Response.Redirect("CentrosPago.aspx")
                            End If

                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Expirada
                            Session("NomSite") = ""
                            Session("NomServDAX") = ""
                            JSMessageAlertWithRedirect("Validacion", "El token: " + Token + " se encuentra expirado", "key", "CentrosPago.aspx")
                            Response.Redirect("CentrosPago.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.ExpiradaCIP
                            Session("NomSite") = ""
                            Session("NomServDAX") = ""
                            JSMessageAlertWithRedirect("Validacion", "El CIP con token: " + Token + " se encuentra expirado", "key", "CentrosPago.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pagada
                            Session("NomSite") = ""
                            Session("NomServDAX") = ""
                            JSMessageAlertWithRedirect("Validacion", "El CIP con Token: " + Token + " se encuentra pagada", "key", "CentrosPago.aspx")
                        Case Else

                    End Select
                End If
            End If
        End If
    End Sub
    Public Sub findControlToDataBind(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls And c.GetType IsNot GetType(GridView) Then
                findControlToDataBind(c.Controls)
            End If
            If c.GetType() Is GetType(ImageButton) Or c.GetType() Is GetType(Button) Or c.GetType() Is GetType(Image) Or c.GetType() Is GetType(LinkButton) Then
                c.DataBind()
            End If
        Next
    End Sub
    Protected Sub lkbRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbRegresar.Click
        Dim url As String = "Default.aspx"
        Dim dataSolicitudEncriptada As String
        Try
            If ViewState("oBEOrdenPago") IsNot Nothing And ViewState("oBESolicitudPago") IsNot Nothing Then
                oBEOrdenPago = CType(ViewState("oBEOrdenPago"), BEOrdenPago)
                oBESolicitudPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago)
                oBEServicio = CType(ViewState("oBEServicio"), BEServicio)
            End If
            url = oBEServicio.UrlRedireccionamiento
            Using oCOrdenPago As New COrdenPago
                dataSolicitudEncriptada = oCOrdenPago.ConsultarSolicitudXMLPorID(oBESolicitudPago)
            End Using
            Dim query As String = String.Format("version={0}&data={1}", 2, dataSolicitudEncriptada)
            Dim oBELogRedirect As New BELogRedirect
            oBELogRedirect.Descripcion = query
            oBELogRedirect.IdTipo = SPE.EmsambladoComun.ParametrosSistema.LogRedirect.Tipo.redirectSaliente
            oBELogRedirect.UrlOrigen = Me.Request.Url.ToString()
            oBELogRedirect.UrlDestino = url
            oBELogRedirect.Parametro1 = UtilAuditoria.GetHostName(Me)
            oBELogRedirect.Parametro2 = UtilAuditoria.GetIPAddress(Me)
            Dim oCComun As New CComun
            oCComun.RegistrarLogRedirect(oBELogRedirect)
            Dim dataToSend As New NameValueCollection()
            dataToSend.Add("version", "2")
            dataToSend.Add("data", dataSolicitudEncriptada)
            UtilUrl.RedirectAndPOST(Me.Page, url, dataToSend)
        Catch exThreadAbord As ThreadAbortException
            Response.Redirect(url)
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'ExportarHTML2PDF()
    End Sub

    Public Sub ExportarHTML2PDF()
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=DemoExportarBigtor.pdf")
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim oStringWriter As New StringWriter()
        'Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)
        'Dim TextoXReemplazar As String = "src=""images"
        ''Dim TextoReemplazo As String

        'Dim url = Request.Url.AbsoluteUri
        'Dim html As String = "" '= ObtenerHtml(url)
        ''TextoReemplazo = ObtenerRaiz() & "img/tempoload/imagen.png"
        ''html = html.Replace(TextoXReemplazar, TextoReemplazo)
        'Dim pathCurrent As String
        'pathCurrent = Server.MapPath("~/")
        'Dim raiz As String
        'raiz = pathCurrent & "img\tempoload\imagen.png"
        ''--------------------------------------------
        'Dim filename As String = "C:\imagen.png"

        'Dim bmpScreenshot As New Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb)

        'Dim gfxScreenshot As Graphics = Graphics.FromImage(bmpScreenshot)

        'gfxScreenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy)

        'bmpScreenshot.Save(raiz, ImageFormat.Png)

        'Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPagCipGeneradoPDF")
        'Dim lector As StreamReader
        'Dim linea As String = ""
        'Try
        '    lector = File.OpenText(ruta)
        '    html = lector.ReadToEnd
        '    lector.Close()
        'Catch ex As Exception
        'End Try
        ''--------------------------------------------
        'oHtmlTextWriter.WriteLine(html)

        'Dim oStringReader As New StringReader(oStringWriter.ToString())
        'Dim oDocumentPDF As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F)
        'Dim oHTMLWorker As New HTMLWorker(oDocumentPDF)
        'PdfWriter.GetInstance(oDocumentPDF, Response.OutputStream)
        'oDocumentPDF.Open()
        'oHTMLWorker.Parse(oStringReader)
        'oDocumentPDF.Close()
        'Response.Write(oDocumentPDF)
        'Response.[End]()
    End Sub

    Public Sub ExportarHTML2PDFV2()
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=DemoExportarBigtor.pdf")
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim oStringWriter As New StringWriter()
        'Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)
        'Dim TextoXReemplazar As String = "src=""images"
        'Dim TextoReemplazo As String

        'Dim url = Request.Url.AbsoluteUri
        'Dim html As String = "" '= ObtenerHtml(url)
        'TextoReemplazo = "src=""" & ObtenerRaiz() & "images"
        ''html = html.Replace(TextoXReemplazar, TextoReemplazo)

        ''--------------------------------------------
        'Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPagCipGeneradoPDF")
        'Dim lector As StreamReader
        'Dim linea As String = ""
        'Try
        '    lector = File.OpenText(ruta)
        '    html = lector.ReadToEnd
        '    lector.Close()
        '    '*----------------------------------------------------------
        '    'ltrHTML.Text = RenderizarUC(UCRuta)
        '    '-----------------------------------------------------------
        'Catch ex As Exception
        'End Try
        ''--------------------------------------------
        'oHtmlTextWriter.WriteLine(html)

        'Dim oStringReader As New StringReader(oStringWriter.ToString())
        'Dim oDocumentPDF As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F)
        'Dim oHTMLWorker As New HTMLWorker(oDocumentPDF)
        'PdfWriter.GetInstance(oDocumentPDF, Response.OutputStream)
        'oDocumentPDF.Open()
        'oHTMLWorker.Parse(oStringReader)
        'oDocumentPDF.Close()
        'Response.Write(oDocumentPDF)
        'Response.[End]()
    End Sub

    Private Function ObtenerHtml(ByVal url As String) As String
        'Dim myWebRequest As HttpWebRequest = DirectCast(HttpWebRequest.Create(url), HttpWebRequest)
        'myWebRequest.Method = "GET"
        '' make request for web page
        'Dim myWebResponse As HttpWebResponse = DirectCast(myWebRequest.GetResponse(), HttpWebResponse)
        'Dim myWebSource As New StreamReader(myWebResponse.GetResponseStream())
        'Dim myPageSource As String = String.Empty
        'myPageSource = myWebSource.ReadToEnd()
        'myWebResponse.Close()
        'Return myPageSource
        Return String.Empty
    End Function

    Private Function ObtenerRaiz() As String
        Dim Ruta As String
        Dim RutaFormateada As String
        Dim NomPag As String
        Dim TamanioNomPag As Integer
        Dim RutaInvertida As String = ""
        Ruta = Request.Url.ToString
        For i As Integer = Ruta.Length - 1 To 0 Step -1
            RutaInvertida = RutaInvertida & Ruta.Substring(i, 1)
        Next
        NomPag = RutaInvertida.Split("/")(0)
        TamanioNomPag = NomPag.Length
        RutaFormateada = Ruta.Substring(0, Ruta.Length - TamanioNomPag)
        Return RutaFormateada
    End Function
    Public Sub JSMessageAlertWithRedirect(tipo_alerta As String, mensaje As String, key As String, PageRedirect As String)
        Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", tipo_alerta, mensaje, PageRedirect)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub
End Class


