﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CIPGenerado.aspx.vb" Inherits="CIPGenerado" EnableViewState="false"%>

<!DOCTYPE html>
<html class="no-js" lang="es">
<head id="head1" runat="server">

    <meta charset="utf-8">
    <title>PagoEfectivo - Transacciones Seguras por Internet en el Perú</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="perucomsultores.pe" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta name="language" content="es" />
    <meta name="geolocation" content="Peru" />
    <meta name="robots" content="index,follow" />
    <meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta property="og:description" content="" />
    <meta property="og:image" content='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/CIPGenerado/logo_pagoefectivo_header.png" %>' />
    <link href="favicon.ico" rel="icon" type="image/vnd.microsoft.icon" />
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/GenPago.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/fonts.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/all.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/layout.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/pagoEfectivo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />    
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/printCIP.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" media="print">
    <%--<link href="css/style.css" rel="stylesheet" type="text/css"/>--%> 
    <!-- Google Tag Manager -->
    <script>        (function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-TL2DDQ');</script>
    <!-- End Google Tag Manager -->
</head>
<body>

    <p>
        <span style="color: rgb(0, 0, 0); font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: -webkit-left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">
        NumeroPedido</span></p>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TL2DDQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <form id="Form1" runat="server">
    <!-- Begin comScore UDM code 1.1104.26 -->
   
    <!-- End comScore UDM code -->
    <div class="hack-shadow">
    </div>
    <header class="cip">
        <div class="content-header">        
    	    <div class="top-head">
                <div class="content-top-head">
        	        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/grupo-comercio.png" width="97" height="16" alt="Grupo El Comercio" title="Grupo El Comercio" border="0" />
                </div>
            </div>
            <div class="head">
                <div class="content-head">
        	        <h1><img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/logo_pagoefectivo_header.png" width="50" height="50" alt="Pago Efectivo - Transacciones seguras por Internet en el Perú" title="Pago Efectivo - Transacciones seguras por Internet en el Perú"/>&nbsp;</h1>
                    <h2>Transacciones seguras por Internet en el Perú</h2>
                        <asp:LinkButton CssClass="btn-back2" ID="lkbRegresar" runat="server" Text="< REGRESAR"></asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="mobile_header">
            
            <asp:LinkButton ID="lkbRegresar2" runat="server"><span></span></asp:LinkButton>
            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/logo_pagoefectivo_header.png" width="40" height="40">
        </div>
              
    </header>

    <!--aqui empieza-->
   
   <asp:Literal ID="ltlConfirmGen" runat="server"></asp:Literal>
            <!--aqui varia -->
              <div class="accordion_affiliated_office">
                <asp:Literal ID="ltSeccionBancos" runat="server"></asp:Literal>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--aqui empieza-->

    <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary btn-large" Visible="false" Text="Guardar" />

    <footer id="foot1" runat="server">
    	<div id="content-footer">
        	<span class="foot-logo">
            	<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/logo_pagoefectivo_foot.png" width="40" height="40" alt="Pago Efectivo" title="Pago Efectivo" border="0">
            </span>
        	<ul>
            	<li><a href="Default.aspx">Inicio</a></li>
                <li class="break">|</li>
                <li><a href="Personas.aspx">Personas</a></li>
                <li class="break">|</li>
                <li><a href="Empresas.aspx">Empresa</a></li>
                <li class="break">|</li>
                <li><a href="Contactenos.aspx">Contáctenos</a></li>
                <li class="break">|</li>
                <li><a href="http://centraldeayuda.pagoefectivo.pe">Ayuda</a></li>
            </ul>
            <!--<a class="book-complaints" href="javascript:;">Libro de reclamaciones</a>-->

            <a class="help-center" target="_blank" href="http://centraldeayuda.pagoefectivo.pe/home">
            	<span class="img"></span>
                <span class="content-help">
                	<span class="title">Central de Ayuda</span>
                	<span class="desc">¿Tienes alguna duda o consulta? estamos para ayudarte</span>
                </span>                
            </a>         
        </div>
        <div class="footer-link">
        	<span>Visite también:</span>
        	<ul>
            	<li class="first"><a target="_blank" title="Diario el Comercio" href="http://elcomercio.pe/?ref=pef">elcomercio.pe</a></li>
                <li><a target="_blank" title="Peru.com" href="http://peru.com/?ref=pef">peru.com</a></li>
                <li><a target="_blank" title="Diario Perú21" href="http://peru21.pe/?ref=pef">peru21.pe</a></li>
                <li><a target="_blank" title="Diario Gestión" href="http://gestion.pe/?ref=pef">gestion.pe</a></li>
                <li><a target="_blank" title="Depor.pe" href="http://depor.pe/?ref=pef">depor.pe</a></li>
                <li><a target="_blank" title="Diario Trome" href="http://trome.pe/?ref=pef">trome.pe</a></li>
                <li><a target="_blank" title="Publimetro" href="http://publimetro.pe/?ref=pef">publimetro.pe</a></li>
                <li><a target="_blank" title="GEC" href="http://gec.pe/?ref=pef">gec.pe</a></li>
                <li><a target="_blank" title="Clasificados.pe" href="http://clasificados.pe/?ref=pef">clasificados.pe</a></li>
                <li><a target="_blank" title="Aptitus" href="http://aptitus.pe/?ref=pef">aptitus.pe</a></li>
                <li><a target="_blank" title="Urbania" href="http://urbania.pe/?ref=pef">urbania.pe</a></li>
                <li><a target="_blank" title="Neoauto" href="http://neoauto.pe/?ref=pef">neoauto.pe</a></li>
                <li class="first"><a target="_blank" title="GuiaGPS" href="http://guiagps.pe/?ref=pef">guiagps.pe</a></li>
                <li><a target="_blank" title="Ofertop" href="http://ofertop.pe/?ref=pef">ofertop.pe</a></li>
                <li><a target="_blank" title="iQuiero" href="http://iquiero.com/?ref=pef">iquiero.com</a></li>
                <li><a target="_blank" title="NuestroMercado" href="http://nuestromercado.pe/?ref=pef">nuestromercado.pe</a></li>
                <li><a target="_blank" title="Club de Suscriptores" href="http://clubsuscriptores.pe/?ref=pef">clubsuscriptores.pe</a></li>
                <li><a target="_blank" title="PeruRed" href="http://perured.pe/?ref=pef">perured.pe</a></li>
                <li><a target="_blank" title="Shopin" href="http://shopin.pe/?ref=pef">shopin.pe</a></li>
            </ul>         
        </div>
        <div class="mobile_footer">
          <ul>
            <li><a href="Default.aspx">Inicio</a></li>
            <li><a href="Personas.aspx">Personas</a></li>
            <li><a href="Empresas.aspx">Empresa</a></li>
            <li><a href="Contactenos.aspx">Contáctenos</a></li>
          </ul>
          <div class="help">
            <a href=""><span></span></a>
          </div>      
        </div>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-1.6.1.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/slides.min.jquery.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/miniApp.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/modules.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script> 
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/accordion.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/JScripts/Messaging/bundle.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'  type="text/javascript"></script>
    </footer>
    </form>


    <%--<script language="JavaScript1.3" src="https://sb.scorecardresearch.com/c2/6906602/ct.js"></script>--%>
</body>
</html>
