﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Net
Imports System.IO
Imports SPE.Web.Util
Imports System.Threading
Imports System.Collections.Generic
Imports System.Globalization
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports _3Dev.FW.Web.Log
Imports Amazon.S3

Partial Class CIPGenerado
    Inherits System.Web.UI.Page
    Public oBEOrdenPago As New BEOrdenPago()
    Public oBESolicitudPago As New BESolicitudPago()
    Public oBEServicio As New BEServicio()

    Private Shared _s3Client As AmazonS3Client = Nothing
    Private ReadOnly s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_ACCESS_KEY")
    Private ReadOnly s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_SECRET_ACCESS_KEY")
    Private ReadOnly s3Region As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_REGION")
    Private ReadOnly s3BucketName As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_BUCKET_NAME")
    Private ReadOnly s3DirectoryPath As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_DIRECTORY_PATH")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnGuardar.Visible = False
        If Not IsPostBack Then
            Me.head1.DataBind()
            foot1.DataBind()
            findControlToDataBind(Me.Controls)
            Dim CtrlOrdenPago As New COrdenPago()
            Dim CtrlServicio As New CServicio()
            Dim oBEServicio As New BEServicio()
            Dim oBEDatosPDF As New BEDatosPDF()
            Dim RutaRaiz As String = ""

            If Session("oBEOrdenPago") IsNot Nothing And Session("oBESolicitudPago") IsNot Nothing Then
                oBEOrdenPago = CType(Session("oBEOrdenPago"), BEOrdenPago)
                oBESolicitudPago = CType(Session("oBESolicitudPago"), BESolicitudPago)
                'oBEServicio = CType(CtrlServicio.GetRecordByID(oBEOrdenPago.IdServicio), BEServicio)
                oBEServicio = CtrlServicio.ObtenerServicioPorID(oBEOrdenPago.IdServicio)
                'Tagueo DAX - Init
                Try
                    Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaServiciosTagDaxReg")
                    Dim ListServDaxReg As New List(Of ServicioDAX)
                    Dim Linea As String = ""
                    Dim Info As String()
                    'Dim SiteTag As String = ""
                    'Dim URLTag As String = ""
                    'Dim NombreDax As String = ""
                    Dim NombreServicio As String = ""
                    Dim NombreSiteTag As String = ""
                    Dim NombreDax As String = ""
                    Dim lector As New StreamReader(ruta)
                    Do
                        Dim ServDaxReg As New ServicioDAX
                        Linea = lector.ReadLine()
                        If Not Linea Is Nothing Then
                            Info = Linea.Split(",")
                            NombreServicio = Info(0).ToString
                            NombreSiteTag = Info(1).ToString
                            NombreDax = Info(2).ToString & oBEOrdenPago.NumeroOrdenPago
                            ServDaxReg.NombreServicio = NombreServicio
                            ServDaxReg.NombreSiteTag = NombreSiteTag
                            ServDaxReg.NombreDax = NombreDax
                            ListServDaxReg.Add(ServDaxReg)
                        End If
                    Loop Until Linea Is Nothing
                    For i As Integer = 0 To ListServDaxReg.Count - 1
                        If ListServDaxReg(i).NombreServicio = oBEServicio.Nombre Then
                            Session("NomSite") = ListServDaxReg(i).NombreSiteTag
                            Session("NomServDAX") = ListServDaxReg(i).NombreDax
                            Exit For
                        Else
                            Session("NomSite") = ""
                            Session("NomServDAX") = ""
                        End If
                    Next
                    lector.Close()
                Catch ex As Exception
                    'Logger.LogException(ex)
                End Try
                'Tagueo DAX - Fin

                ViewState("oBEOrdenPago") = oBEOrdenPago
                ViewState("oBESolicitudPago") = oBESolicitudPago
                ViewState("oBEServicio") = oBEServicio

                'hdkServicio.Value = oBEServicio.IdServicio
                'hdkSolicitud.Value = oBESolicitudPago.IdSolicitudPago

                Session("IdServicio") = oBEServicio.IdServicio
                Session("IdSolicitudPago") = oBESolicitudPago.IdSolicitudPago

                'imgServicio.ImageUrl = System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/logopagoefectivo.png"
                'imgServicio.Width = "225"
                'imgServicio.BorderWidth = "0"
                'imgCIP.ImageUrl = "CLI/ADOPImg.aspx?numero=" & oBEOrdenPago.NumeroOrdenPago

                ' ltlCIP.Text = oBEOrdenPago.NumeroOrdenPago
                'ltlTotal.Text = IIf(oBEOrdenPago.IdMoneda = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles, "S/. ", "$ ") & oBEOrdenPago.Total
                'ltlTotalmovil.Text = ltlTotal.Text
                'ltlNombreServicio.Text = oBEServicio.Nombre
                'NombreServicio2.Text = oBEServicio.Nombre
                'ltlCodTransaccion.Text = oBESolicitudPago.CodTransaccion
                'ltlConceptoPago.Text = oBESolicitudPago.ConceptoPago
                'ltlConceptoPagoAdicional.Text = String.Empty
                'For Each item As BEParametroEmail In oBESolicitudPago.ParametrosEmail
                '    If (item.Nombre = "ConceptoPago") Then
                '        ltlConceptoPagoAdicional.Text = item.Valor
                '    End If
                'Next

                'RutaRaiz = ObtenerRaiz()
                'With oBEDatosPDF
                '    .ImagenServicio = RutaRaiz & "ADM/ADServImg.aspx?servicioid=" & oBEOrdenPago.IdServicio
                '    .ImagenCodBar = RutaRaiz & "CLI/ADOPImg.aspx?numero=" & oBEOrdenPago.NumeroOrdenPago
                '    .NumeroCIP = oBEOrdenPago.NumeroOrdenPago
                '    .Total = IIf(oBEOrdenPago.IdMoneda = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles, "S/. ", "$ ") & oBEOrdenPago.Total
                '    .NombreServicio = oBEServicio.Nombre
                '    .ConceptoPago = oBESolicitudPago.ConceptoPago
                'End With

                'ViewState("oBEServicio")

                Dim strFechaVencimiento As String = String.Format("{0} {1}/{2}/{3}", DiasSemana(oBEOrdenPago.FechaVencimiento.DayOfWeek), _
                                                                      oBEOrdenPago.FechaVencimiento.Day.ToString("00"), _
                                                                      oBEOrdenPago.FechaVencimiento.Month.ToString("00"), _
                                                                      oBEOrdenPago.FechaVencimiento.Year.ToString("0000"))

                Dim ci As New CultureInfo("Es-Es")
                Dim diaDeSemana As String = ci.DateTimeFormat.GetDayName(oBEOrdenPago.FechaVencimiento.DayOfWeek)
                Dim diaNumero As String = oBEOrdenPago.FechaVencimiento.Day.ToString().PadLeft(2, "0")
                Dim mesNombre As String = ci.DateTimeFormat.GetMonthName(oBEOrdenPago.FechaVencimiento.Month)

                Dim CtrPlantilla As New CPlantilla()
                Dim valoresDinamicos As New Dictionary(Of String, String)
                valoresDinamicos.Add("[CIP]", oBEOrdenPago.IdOrdenPago)
                valoresDinamicos.Add("[idServicio]", oBESolicitudPago.IdServicio)
                'valoresDinamicos.Add("[FechaVencimiento]", oBEOrdenPago.FechaVencimiento.ToString(" dddd dd") & " de " & oBEOrdenPago.FechaVencimiento.ToString("MMMM "))
                valoresDinamicos.Add("[FechaVencimiento]", strFechaVencimiento)
                valoresDinamicos.Add("[HoraVencimiento]", oBEOrdenPago.FechaVencimiento.ToString(" hh:mm tt"))
                valoresDinamicos.Add("[EmailCLiente]", oBEOrdenPago.UsuarioEmail)
                valoresDinamicos.Add("[NroPedido]", oBESolicitudPago.CodTransaccion)
                valoresDinamicos.Add("[ServicioNombre]", oBEServicio.Nombre)
                valoresDinamicos.Add("[Moneda]", IIf(oBEOrdenPago.IdMoneda = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles, "S/. ", "$ "))
                valoresDinamicos.Add("[Monto]", oBEOrdenPago.Total)
                valoresDinamicos.Add("[ConceptoPago]", oBESolicitudPago.ConceptoPago)
                valoresDinamicos.Add("[idMoneda]", oBESolicitudPago.IdMoneda.ToString())

                If (oBEOrdenPago.IdMoneda = 1) Then
                    valoresDinamicos.Add("[MostrarBancos]", "display:none;")
                    valoresDinamicos.Add("[MonedaNombre]", "Soles")
                Else
                    valoresDinamicos.Add("[MonedaNombre]", "Dólares")
                    valoresDinamicos.Add("[MostrarBancos]", "")
                End If

                Dim querySecure As TSHAK.Components.SecureQueryString
                querySecure = New TSHAK.Components.SecureQueryString(New Byte() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8})
                querySecure("OP") = oBEOrdenPago.NumeroOrdenPago
                valoresDinamicos.Add("[CIPEncrypted]", System.Web.HttpUtility.UrlEncode(querySecure.ToString()))

                ' ltlAtencion.Text = CtrPlantilla.GenerarPlantillaToHtmlByTipo(oBEOrdenPago.IdServicio, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.VencPago, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
                'ltlAtencionmovil.Text = ltlAtencion.Text


                Dim Seccionheader As String
                Dim TextoXReemplazar As String = "src=""App_Themes"
                Dim TextoReemplazo As String
                Dim Seccionpagos As String

                If oBEServicio.PlantillaFormato = 53 Then

					Seccionpagos = CtrPlantilla.ConsultarPLantillasXServicioGenPago(oBEOrdenPago.IdServicio, oBEOrdenPago.IdMoneda, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoPagoGeneral, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)

					TextoReemplazo = "src=""" & System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") & "/App_Themes"
					'Seccionheader = CtrPlantilla.GenerarPlantillaToHtmlByTipo(oBEOrdenPago.IdServicio, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoConfirm, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
					Seccionheader = CtrPlantilla.ConsultarPLantillasXServicioGenPago(oBEOrdenPago.IdServicio, oBEOrdenPago.IdMoneda, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoConfirm, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
					Seccionheader = Seccionheader.Replace(TextoXReemplazar, TextoReemplazo)
					RutaRaiz = ObtenerRaiz()
					TextoXReemplazar = "[token]"
					TextoReemplazo = "javascript:window.open('" + RutaRaiz + "CIPImpresion.aspx?token=" + oBESolicitudPago.Token + "');"
					Seccionheader = Seccionheader.Replace(TextoXReemplazar, TextoReemplazo)
					ltlConfirmGen.Text = Seccionheader

					Seccionpagos = Seccionpagos.Replace(TextoXReemplazar, TextoReemplazo)
					ltSeccionBancos.Text = Seccionpagos
                Else

                    Dim strPlantillaTipo As String
                    If (oBEServicio.PlantillaFormato = SPE.EmsambladoComun.ParametrosSistema.PlantillaFormato.Tipo.Estandar) Then
                        strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.ConfGen & "Est"
                    Else
                        strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.ConfGen & "Exp"
                    End If

                    Dim obePlantilla = CtrPlantilla.ConsultarPlantillaYParametrosPorTipoYVariacion(oBEOrdenPago.IdServicio, strPlantillaTipo, "st")
                    'Cargo plantilla desde S3
                    Dim fileHtml = ObtenerPlantillaS3(obePlantilla.FileS3)

                    If (Not fileHtml = "") Then
                        Dim lstSecciones As List(Of BEPlantillaSeccion) = CtrPlantilla.ObtenerSeccionesPlantillaPorServicio(oBEServicio.IdServicio, obePlantilla.IdPlantillaTipo)
                        For i As Integer = 0 To lstSecciones.Count - 1
                            fileHtml = fileHtml.Replace("[Seccion" + lstSecciones(i).IdSeccion.ToString + "]", lstSecciones(i).Contenido)
                        Next
                        For Each param As KeyValuePair(Of String, String) In valoresDinamicos
                            fileHtml = fileHtml.Replace(param.Key, param.Value)
                        Next
                        Dim urlToken As String
                        RutaRaiz = ObtenerRaiz()
                        TextoXReemplazar = "[token]"
                        urlToken = "javascript:window.open('" + RutaRaiz + "CIPImpresion.aspx?token=" + oBESolicitudPago.Token + "');"
                        fileHtml = fileHtml.Replace(TextoXReemplazar, urlToken)
                        ltSeccionBancos.Text = fileHtml
                    Else
                        Exit Sub
                    End If

                End If

                'btnImprimir_footer.OnClientClick = "javascript:window.open('" + RutaRaiz + "CIPImpresion.aspx?token=" + oBESolicitudPago.Token + "');"
                Session.Remove("oBEOrdenPago")
                Session.Remove("oBESolicitudPago")
            Else
                Response.Redirect("Default.aspx")
            End If
        End If
    End Sub
    Private _DiasSemana As Dictionary(Of DayOfWeek, String)
    Public ReadOnly Property DiasSemana() As Dictionary(Of DayOfWeek, String)
        Get
            If (_DiasSemana Is Nothing) Then
                _DiasSemana = New Dictionary(Of DayOfWeek, String)
                _DiasSemana.Add(DayOfWeek.Monday, "Lunes")
                _DiasSemana.Add(DayOfWeek.Tuesday, "Martes")
                _DiasSemana.Add(DayOfWeek.Wednesday, "Miércoles")
                _DiasSemana.Add(DayOfWeek.Thursday, "Jueves")
                _DiasSemana.Add(DayOfWeek.Friday, "Viernes")
                _DiasSemana.Add(DayOfWeek.Saturday, "Sábado")
                _DiasSemana.Add(DayOfWeek.Sunday, "Domingo")
            End If
            Return _DiasSemana
        End Get
    End Property
    Public Sub findControlToDataBind(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls And c.GetType IsNot GetType(GridView) Then
                findControlToDataBind(c.Controls)
            End If
            If c.GetType() Is GetType(ImageButton) Or c.GetType() Is GetType(Button) Or c.GetType() Is GetType(Image) Or c.GetType() Is GetType(LinkButton) Then
                c.DataBind()
            End If
        Next
    End Sub
    Protected Sub lkbRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbRegresar.Click
        redirectRegresar()
    End Sub
    Protected Sub lkbRegresar2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbRegresar2.Click
        redirectRegresar()
    End Sub

    Public Function ObtenerPlantillaS3(ByVal fileName As String) As String

        Dim amazonS3 = New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        If (amazonS3.FileExists(fileName)) Then
            Dim fileStream = amazonS3.ReadStreamFromFile(fileName)
            Dim bytes = fileStream.ToArray()

            Return System.Text.Encoding.UTF8.GetString(bytes)
        Else
            Return "-1"
        End If

    End Function

    Private Sub redirectRegresar()
        Dim url As String = "Default.aspx"
        Dim dataSolicitudEncriptada As String

        Try
            If ViewState("oBEOrdenPago") IsNot Nothing And ViewState("oBESolicitudPago") IsNot Nothing Then
                oBEOrdenPago = CType(ViewState("oBEOrdenPago"), BEOrdenPago)
                oBESolicitudPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago)
                oBEServicio = CType(ViewState("oBEServicio"), BEServicio)
            End If

            If (oBEServicio.IdServicio = 0) Then
                oBEServicio = New CServicio().ObtenerServicioPorID(Session("IdServicio"))
                oBESolicitudPago.IdSolicitudPago = Session("IdSolicitudPago")
                oBESolicitudPago.IdServicio = Session("IdServicio")
            End If

            url = oBEServicio.UrlRedireccionamiento

            Using oCOrdenPago As New COrdenPago
                dataSolicitudEncriptada = oCOrdenPago.ConsultarSolicitudXMLPorID(oBESolicitudPago)
            End Using
            Dim query As String = String.Format("version={0}&data={1}", 2, dataSolicitudEncriptada)
            Dim oBELogRedirect As New BELogRedirect
            oBELogRedirect.Descripcion = query
            oBELogRedirect.IdTipo = SPE.EmsambladoComun.ParametrosSistema.LogRedirect.Tipo.redirectSaliente
            oBELogRedirect.UrlOrigen = Me.Request.Url.ToString()
            oBELogRedirect.UrlDestino = url
            oBELogRedirect.Parametro1 = UtilAuditoria.GetHostName(Me)
            oBELogRedirect.Parametro2 = UtilAuditoria.GetIPAddress(Me)
            Dim oCComun As New CComun
            oCComun.RegistrarLogRedirect(oBELogRedirect)
            Dim dataToSend As New NameValueCollection()
            dataToSend.Add("version", "2")
            dataToSend.Add("data", dataSolicitudEncriptada)
            UtilUrl.RedirectAndPOST(Me.Page, url, dataToSend)
        Catch exThreadAbord As ThreadAbortException
            Response.Redirect(url)
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try
    End Sub
    Public Sub ExportarHTML2PDF()
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=DemoExportarBigtor.pdf")
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Dim oStringWriter As New StringWriter()
        'Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)
        'Dim TextoXReemplazar As String = "src=""images"
        ''Dim TextoReemplazo As String

        'Dim url = Request.Url.AbsoluteUri
        'Dim html As String = "" '= ObtenerHtml(url)
        ''TextoReemplazo = ObtenerRaiz() & "img/tempoload/imagen.png"
        ''html = html.Replace(TextoXReemplazar, TextoReemplazo)
        'Dim pathCurrent As String
        'pathCurrent = Server.MapPath("~/")
        'Dim raiz As String
        'raiz = pathCurrent & "img\tempoload\imagen.png"
        ''--------------------------------------------
        'Dim filename As String = "C:\imagen.png"

        'Dim bmpScreenshot As New Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb)

        'Dim gfxScreenshot As Graphics = Graphics.FromImage(bmpScreenshot)

        'gfxScreenshot.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy)

        'bmpScreenshot.Save(raiz, ImageFormat.Png)

        'Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPagCipGeneradoPDF")
        'Dim lector As StreamReader
        'Dim linea As String = ""
        'Try
        '    lector = File.OpenText(ruta)
        '    html = lector.ReadToEnd
        '    lector.Close()
        'Catch ex As Exception
        'End Try
        ''--------------------------------------------
        'oHtmlTextWriter.WriteLine(html)

        'Dim oStringReader As New StringReader(oStringWriter.ToString())
        'Dim oDocumentPDF As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F)
        'Dim oHTMLWorker As New HTMLWorker(oDocumentPDF)
        'PdfWriter.GetInstance(oDocumentPDF, Response.OutputStream)
        'oDocumentPDF.Open()
        'oHTMLWorker.Parse(oStringReader)
        'oDocumentPDF.Close()
        'Response.Write(oDocumentPDF)
        'Response.[End]()
    End Sub


    'Public Sub ExportarHTML2PDFV2()
    '    Response.ContentType = "application/pdf"
    '    Response.AddHeader("content-disposition", "attachment;filename=DemoExportarBigtor.pdf")
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Dim oStringWriter As New StringWriter()
    '    Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)
    '    Dim TextoXReemplazar As String = "src=""images"
    '    Dim TextoReemplazo As String

    '    Dim url = Request.Url.AbsoluteUri
    '    Dim html As String = "" '= ObtenerHtml(url)
    '    TextoReemplazo = "src=""" & ObtenerRaiz() & "images"
    '    'html = html.Replace(TextoXReemplazar, TextoReemplazo)

    '    '--------------------------------------------
    '    Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPagCipGeneradoPDF")
    '    Dim lector As StreamReader
    '    Dim linea As String = ""
    '    Try
    '        lector = File.OpenText(ruta)
    '        html = lector.ReadToEnd
    '        lector.Close()
    '        '*----------------------------------------------------------
    '        'ltrHTML.Text = RenderizarUC(UCRuta)
    '        '-----------------------------------------------------------
    '    Catch ex As Exception
    '    End Try
    '    '--------------------------------------------
    '    oHtmlTextWriter.WriteLine(html)

    '    Dim oStringReader As New StringReader(oStringWriter.ToString())
    '    Dim oDocumentPDF As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F)
    '    Dim oHTMLWorker As New HTMLWorker(oDocumentPDF)
    '    PdfWriter.GetInstance(oDocumentPDF, Response.OutputStream)
    '    oDocumentPDF.Open()
    '    oHTMLWorker.Parse(oStringReader)
    '    oDocumentPDF.Close()
    '    'Response.Write(oDocumentPDF)
    '    'Response.[End]()
    'End Sub

    Private Function ObtenerHtml(ByVal url As String) As String
        'Dim myWebRequest As HttpWebRequest = DirectCast(HttpWebRequest.Create(url), HttpWebRequest)
        'myWebRequest.Method = "GET"
        '' make request for web page
        'Dim myWebResponse As HttpWebResponse = DirectCast(myWebRequest.GetResponse(), HttpWebResponse)
        'Dim myWebSource As New StreamReader(myWebResponse.GetResponseStream())
        'Dim myPageSource As String = String.Empty
        'myPageSource = myWebSource.ReadToEnd()
        'myWebResponse.Close()
        'Return myPageSource
        Return String.Empty
    End Function

    Private Function ObtenerRaiz() As String
        Dim Ruta As String
        Dim RutaFormateada As String
        Dim NomPag As String
        Dim TamanioNomPag As Integer
        Dim RutaInvertida As String = ""
        Ruta = Request.Url.ToString
        For i As Integer = Ruta.Length - 1 To 0 Step -1
            RutaInvertida = RutaInvertida & Ruta.Substring(i, 1)
        Next
        NomPag = RutaInvertida.Split("/")(0)
        TamanioNomPag = NomPag.Length
        RutaFormateada = Ruta.Substring(0, Ruta.Length - TamanioNomPag)
        Return RutaFormateada
    End Function
End Class

Public Class BEDatosPDF
    Public ImagenServicio As String
    Public ImagenCodBar As String
    Public NumeroCIP As String
    Public Total As String
    Public NombreServicio As String
    Public ConceptoPago As String
    Public MensajeAtencion As String
    Public SeccionBancos As String
End Class
