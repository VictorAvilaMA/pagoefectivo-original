﻿Imports SPE.Entidades
Imports SPE.Web
Imports System.Collections.Generic
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.tool.xml
Imports Amazon.S3

Partial Class CIPImpresion
    Inherits PaginaBase

    Private Shared _s3Client As AmazonS3Client = Nothing
    Private ReadOnly s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_ACCESS_KEY")
    Private ReadOnly s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_SECRET_ACCESS_KEY")
    Private ReadOnly s3Region As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_REGION")
    Private ReadOnly s3BucketName As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_BUCKET_NAME")
    Private ReadOnly s3DirectoryPath As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_DIRECTORY_PATH")
    Public oBEOrdenPago As New BEOrdenPago()
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim CtrlServicio As New CServicio()
            Dim oBEServicio As New BEServicio()
            If HttpContext.Current.Request.QueryString("token") Is Nothing Then
                Response.Redirect("Default.aspx")
            Else
                Dim Token As String = HttpContext.Current.Request.QueryString("token")
                Dim oCOrdenPago As New COrdenPago()
                Dim oBESolicitudPago As New BESolicitudPago
                oBESolicitudPago.Token = Token
                oBESolicitudPago = oCOrdenPago.ConsultarSolicitudPagoPorTokenInst(oBESolicitudPago)

                If oBESolicitudPago Is Nothing Then
                    JSMessageAlertWithRedirect("Validacion", "No se encuentra una solicitud pendiente con el Token: " + Token, "key", "Default.aspx")
                Else
                    Select Case oBESolicitudPago.Idestado
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.GeneradaCIP

                            Dim oBEOrdenPago As New BEOrdenPago
                            oBEOrdenPago.IdOrdenPago = oBESolicitudPago.IdOrdenPago
                            oBEOrdenPago.IdServicio = oBESolicitudPago.IdServicio
                            oBEServicio = CtrlServicio.ObtenerServicioPorID(oBEOrdenPago.IdServicio)
                            oBEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorId(oBEOrdenPago)
                            Dim strFechaVencimiento As String = String.Format("{0} {1}/{2}/{3}", DiasSemana(oBEOrdenPago.FechaVencimiento.DayOfWeek), _
                                                                      oBEOrdenPago.FechaVencimiento.Day.ToString("00"), _
                                                                      oBEOrdenPago.FechaVencimiento.Month.ToString("00"), _
                                                                      oBEOrdenPago.FechaVencimiento.Year.ToString("0000"))

                            Dim valoresDinamicos As New Dictionary(Of String, String)
                            valoresDinamicos.Add("[CIP]", oBEOrdenPago.IdOrdenPago)
                            valoresDinamicos.Add("[FechaVencimiento]", strFechaVencimiento)
                            valoresDinamicos.Add("[HoraVencimiento]", oBEOrdenPago.FechaVencimiento.ToString(" hh:mm tt"))
                            valoresDinamicos.Add("[EmailCLiente]", oBEOrdenPago.UsuarioEmail)
                            valoresDinamicos.Add("[Monto]", oBEOrdenPago.Total)
                            valoresDinamicos.Add("[Moneda]", oBEOrdenPago.SimboloMoneda)
                            valoresDinamicos.Add("[ServicioNombre]", oBEServicio.Nombre)
                            valoresDinamicos.Add("[NroPedido]", oBEOrdenPago.OrderIdComercio)
                            valoresDinamicos.Add("[ConceptoPago]", oBEOrdenPago.ConceptoPago)
                            If (oBEOrdenPago.IdMoneda = 1) Then
                                valoresDinamicos.Add("[MostrarBancos]", "display:none;")
                                valoresDinamicos.Add("[MonedaNombre]", "Soles")
                            Else
                                valoresDinamicos.Add("[MonedaNombre]", "Dólares")
                                valoresDinamicos.Add("[MostrarBancos]", "")
                            End If

                            Dim querySecure As TSHAK.Components.SecureQueryString
                            querySecure = New TSHAK.Components.SecureQueryString(New Byte() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 8})
                            querySecure("OP") = oBEOrdenPago.NumeroOrdenPago
                            valoresDinamicos.Add("[CIPEncrypted]", System.Web.HttpUtility.UrlEncode(querySecure.ToString()))
                            ExportarHTML2PDFV3(oBEOrdenPago, valoresDinamicos)
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Expirada
                            JSMessageAlertWithRedirect("Validacion", "El token: " + Token + " se encuentra expirado", "key", "Default.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.ExpiradaCIP
                            JSMessageAlertWithRedirect("Validacion", "El CIP con token: " + Token + " se encuentra expirado", "key", "Default.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pagada
                            JSMessageAlertWithRedirect("Validacion", "El CIP con Token: " + Token + " se encuentra pagada", "key", "Default.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pendiente
                        Case Else
                    End Select
                End If
            End If
        End If
    End Sub

    Public Function ObtenerPlantillaS3(ByVal fileName As String) As String

        Dim amazonS3 = New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        If (amazonS3.FileExists(fileName)) Then
            Dim fileStream = amazonS3.ReadStreamFromFile(fileName)
            Dim bytes = fileStream.ToArray()

            Return System.Text.Encoding.UTF8.GetString(bytes)
        Else
            Return "-1"
        End If

    End Function

    Private _DiasSemana As Dictionary(Of DayOfWeek, String)
    Public ReadOnly Property DiasSemana() As Dictionary(Of DayOfWeek, String)
        Get
            If (_DiasSemana Is Nothing) Then
                _DiasSemana = New Dictionary(Of DayOfWeek, String)
                _DiasSemana.Add(DayOfWeek.Monday, "Lunes")
                _DiasSemana.Add(DayOfWeek.Tuesday, "Martes")
                _DiasSemana.Add(DayOfWeek.Wednesday, "Miércoles")
                _DiasSemana.Add(DayOfWeek.Thursday, "Jueves")
                _DiasSemana.Add(DayOfWeek.Friday, "Viernes")
                _DiasSemana.Add(DayOfWeek.Saturday, "Sábado")
                _DiasSemana.Add(DayOfWeek.Sunday, "Domingo")
            End If
            Return _DiasSemana
        End Get
    End Property
    Public Sub ExportarHTML2PDFV2()
        '    Response.ContentType = "application/pdf"
        '    Response.AddHeader("content-disposition", "attachment;filename=DemoExportarBigtor.pdf")
        '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
        '    Dim oStringWriter As New StringWriter()
        '    Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)
        '    Dim TextoXReemplazar As String = "src=""images"
        '    Dim TextoReemplazo As String

        '    Dim url = Request.Url.AbsoluteUri
        '    Dim html As String = "" '= ObtenerHtml(url)
        '    TextoReemplazo = "src=""" & ObtenerRaiz() & "images"
        '    'html = html.Replace(TextoXReemplazar, TextoReemplazo)

        '    '--------------------------------------------
        '    Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPagCipGeneradoPDF")
        '    Dim lector As StreamReader
        '    Dim linea As String = ""
        '    Try
        '        lector = File.OpenText(ruta)
        '        html = lector.ReadToEnd
        '        lector.Close()
        '        '*----------------------------------------------------------
        '        'ltrHTML.Text = RenderizarUC(UCRuta)
        '        '-----------------------------------------------------------
        '    Catch ex As Exception
        '    End Try

        '    Dim sbHtml As System.Text.StringBuilder = New System.Text.StringBuilder()
        '    sbHtml.Append(html)
        '    Using stream As System.IO.Stream = New System.IO.FileStream(ruta, System.IO.FileMode.OpenOrCreate)
        '        Dim htmlToPdf As New Pdfizer.HtmlToPdfConverter()
        '        htmlToPdf.Open(stream)
        '        htmlToPdf.Run(sbHtml.ToString)
        '        htmlToPdf.Close()
        '    End Using

        '    oHtmlTextWriter.WriteLine(html)
        '    Dim oStringReader As New StringReader(oStringWriter.ToString())
        '    Dim oDocumentPDF As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F)
        '    HttpContext.Current.Response.Clear()

        '    HttpContext.Current.Response.AddHeader("content-disposition",
        'String.Format("attachment; filename={0}", "friendlypdfname.pdf"))


        '    HttpContext.Current.Response.ContentType = "application/pdf"
        '    HttpContext.Current.Response.Write(oDocumentPDF)
        '    HttpContext.Current.Response.End()
    End Sub
    Public Sub ExportarHTML2PDFV3(oBEOrdenPago As BEOrdenPago, valoresDinamicos As Dictionary(Of String, String))
        Dim CtrPlantilla As New CPlantilla()

        Dim html As String = ""
        'html = CtrPlantilla.GenerarPlantillaToHtmlByTipo(oBEOrdenPago.IdServicio, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoImpresion, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)

        Dim CtrlServicio As New CServicio()
        Dim oBEServicio As New BEServicio()
        oBEServicio = CtrlServicio.ObtenerServicioPorID(oBEOrdenPago.IdServicio)

        If oBEServicio.PlantillaFormato = 53 Then
            html = CtrPlantilla.ConsultarPLantillasXServicioGenPago(oBEOrdenPago.IdServicio, oBEOrdenPago.IdMoneda, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoImpresion, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
        Else

            Dim strPlantillaTipo As String
            If (oBEServicio.PlantillaFormato = SPE.EmsambladoComun.ParametrosSistema.PlantillaFormato.Tipo.Estandar) Then
                strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.SecImpr & "Est"
            Else
                strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.SecImpr & "Exp"
            End If

            Dim obePlantilla = CtrPlantilla.ConsultarPlantillaYParametrosPorTipoYVariacion(oBEOrdenPago.IdServicio, strPlantillaTipo, "st")
            'Cargo plantilla desde S3
            Dim fileHtml = ObtenerPlantillaS3(obePlantilla.FileS3)
            If (Not fileHtml = "") Then
                Dim lstSecciones As List(Of BEPlantillaSeccion) = CtrPlantilla.ObtenerSeccionesPlantillaPorServicio(oBEServicio.IdServicio, obePlantilla.IdPlantillaTipo)
                For i As Integer = 0 To lstSecciones.Count - 1
                    fileHtml = fileHtml.Replace("[Seccion" + lstSecciones(i).IdSeccion.ToString + "]", lstSecciones(i).Contenido)
                Next
                For Each param As KeyValuePair(Of String, String) In valoresDinamicos
                    fileHtml = fileHtml.Replace(param.Key, param.Value)
                Next
                For i As Integer = 0 To obePlantilla.ListaParametros.Count - 1
                    If (obePlantilla.ListaParametros(i).IdTipoParametro = 411) Then
                        fileHtml = fileHtml.Replace(obePlantilla.ListaParametros(i).Etiqueta.ToString, obePlantilla.ListaParametros(i).Valor)
                    End If
                Next
                If (oBEOrdenPago.IdMoneda = 1) Then
                    fileHtml = fileHtml.Replace("[MonedaNombre]", "Soles")
                Else
                    fileHtml = fileHtml.Replace("[MonedaNombre]", "Dólares")
                End If
                html = fileHtml
            Else
                JSMessageAlertWithRedirect("Validacion", "No Hay Plantilla configurada para este servicio", "key", "Default.aspx")
            End If
        End If

        If (html = "") Then
            JSMessageAlertWithRedirect("Validacion", "No Hay Plantilla configurada para este servicio", "key", "Default.aspx")
        Else
            Dim oStringWriter As New StringWriter()
            Dim oHtmlTextWriter As New HtmlTextWriter(oStringWriter)
            Dim TextoXReemplazar As String = "src=""App_Themes"
            Dim TextoReemplazo As String
            TextoReemplazo = "src=""" & ObtenerRaiz() & "App_Themes"
            html = html.Replace(TextoXReemplazar, TextoReemplazo)
            oHtmlTextWriter.WriteLine(html)
            Dim oStringReader As New StringReader(oStringWriter.ToString())
            Dim oDocumentPDF As New Document(PageSize.LEGAL, 5.0F, 5.0F, 5.0F, 5.0F)
            'Dim oDocumentPDF As New Document(New Rectangle(800.0F, 800.0F))
            Dim writer = PdfWriter.GetInstance(oDocumentPDF, Response.OutputStream)
            Dim pdfDest As New PdfDestination(PdfDestination.XYZ, 0, oDocumentPDF.PageSize.Height, 0.75F)
            oDocumentPDF.Open()
            oDocumentPDF.NewPage()
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, oDocumentPDF, oStringReader)
            Dim action = PdfAction.GotoLocalPage(1, pdfDest, writer)
            writer.SetOpenAction(action)
            oDocumentPDF.Close()
            Response.ClearHeaders()
            Response.ContentType = "application/pdf"
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.AddHeader("X-UA-Compatible", "IE=EmulateIE8")
            Response.AddHeader("X-UA-Compatible", "IE=Edge,chrome=1")
            Response.AddHeader("X-UA-Compatible", "IE=EmulateIE7")
            Response.Write(oDocumentPDF)
            Response.Flush()
            Response.[End]()
        End If

    End Sub
    Private Function ObtenerRaiz() As String
        Dim Ruta As String
        Dim RutaFormateada As String
        Dim NomPag As String
        Dim TamanioNomPag As Integer
        Dim RutaInvertida As String = ""
        Ruta = Request.Url.ToString
        For i As Integer = Ruta.Length - 1 To 0 Step -1
            RutaInvertida = RutaInvertida & Ruta.Substring(i, 1)
        Next
        NomPag = RutaInvertida.Split("/")(0)
        TamanioNomPag = NomPag.Length
        RutaFormateada = Ruta.Substring(0, Ruta.Length - TamanioNomPag)
        Return RutaFormateada
    End Function
End Class

