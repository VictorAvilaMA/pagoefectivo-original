<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADAcCl.aspx.vb" Inherits="CLI_ADAcCl" Title="PagoEfectivo - Mis Datos" MaintainScrollPositionOnPostback="true"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #Error div ul li
        {
            color: Red !important;
        }
        #ImgError span
        {
            color: Red !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="ltTitulo" runat="server" Text="Mis Datos"></asp:Literal>
    </h2>
    <div class="conten_pasos3" id="content">
        <asp:Panel ID="pnlInfoCambio" runat="server" Visible="false">
            <div class="inner-col-right">
                <div class="w100 clearfix dlinline">
                    <dl class="even clearfix dt15 dd30" style="padding: 10px 0px;">
                        <dt style="width: 98%;" class="desc">Por favor, complete la informaci&oacute;n necesaria
                            para que pueda ingresar al Sistema PagoEfectivo. Esa informaci&oacute;n es necesaria
                            para poderle brindar un mejor servicio.</dt>
                    </dl>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatosUsuario" runat="server">
            <h4>
                Datos de Usuario</h4>
            <ul class="datos_cip2 h0">
                <li class="t1"><span class="color">>></span> E-mail principal : </li>
                <li class="t2">
                    <asp:TextBox ID="txtEmailPrincipal" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlInformacionRegistro" runat="server">
            <h4>
                Datos Personales</h4>
            <%--<ul class="datos_cip2 h0">
                <li class="t1"><span class="color">&gt;&gt;</span>Foto :</li>
                <li class="t2">
                    <asp:FileUpload ID="FileUpload1" runat="server" onchange="javascript:try{submit();}catch(err){}" />
                </li>
            </ul>
            <ul class="datos_cip2 h0">
                <li class="t1"><span class="color"></span></li>
                <li class="t2">
                    <asp:Image ID="ImgAvatar" runat="server" ImageUrl="~/img/avatar/avatar.jpg" Width="120px"
                        Height="120px" ImageAlign="Middle"></asp:Image>
                    <div style="clear: both;"></div>
                    <div id="ImgError">
                        <asp:Label ID="lblimg" runat="server" Visible="false"></asp:Label>
                    </div>
                </li>
            </ul>--%>
            <ul class="datos_cip2 h0" id="filediv">
                <li class="t1"><span class="color">&gt;&gt;</span>Foto :</li>
                <li class="t2">
                    <%--<asp:FileUpload ID="FileUpload1" runat="server" onchange="javascript:try{submit();}catch(err){}" />--%>
                    <div id="filediv">
                        <input type="file" name="file" />
                        <button style="display: none;">
                            Cargar</button>
                        <table id="files">
                        </table>
                    </div>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
            <ul class="datos_cip2 h0">
                <li class="t1"><span class="color"></span></li>
                <li class="t2">
                    <asp:Image ID="ImgAvatar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/avatar/avatar.jpg" %>' Width="120px"
                        Height="120px" ImageAlign="Middle"></asp:Image>                        
                    <div style="clear: both;">
                    </div>
                    <div id="ImgError">
                        <asp:Label ID="lblimg" runat="server" Visible="false"></asp:Label>
                    </div>
                </li>
            </ul>
            <ul class="datos_cip2 h0">
                <li class="t1"><span class="color"></span></li>
                <li class="t2"></li>
            </ul>
            <div style="clear: both;">
            </div>
            <ul class="datos_cip2 h30">
                <li class="t1"><span class="color">&gt;&gt;</span>Alias :</li>
                <li class="t2">
                    <asp:TextBox ID="txtAlias" runat="server" CssClass="normal" MaxLength="80"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAlias" runat="server" ControlToValidate="txtAlias"
                        ErrorMessage="Ingrese un alias">*</asp:RequiredFieldValidator>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
            <spe:ucUsuarioDatosComun ID="ucUsuarioDatosComun" runat="server"></spe:ucUsuarioDatosComun>
            <ul class="datos_cip2 h0">
                <li class="t1"><span class="color">&gt;&gt;</span>Mail Alternativo :</li>
                <li class="t2">
                    <asp:TextBox ID="txtMailAlternativo" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revMailAlternativo" runat="server" ControlToValidate="txtMailAlternativo"
                        ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ForeColor="White">*</asp:RegularExpressionValidator>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
        </asp:Panel>
        <asp:Panel runat="server">
            <h4>
                Configuraci&oacute;n de Seguridad</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Registrar Dispositivo : </li>
                <li class="t2">
                    <asp:RadioButtonList ID="rbtnFRP" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Si" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
        </asp:Panel>
        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeValidacion"></asp:Label>
        <asp:HiddenField ID="hdnIdEstado" runat="server" />
        <asp:HiddenField ID="hdnIdCliente" runat="server" />
        <div id="Error">
            <asp:ValidationSummary ID="vsmError" runat="server" ValidationGroup="IsNullOrEmpty">
            </asp:ValidationSummary>
        </div>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Visible="false"
                    Text="Registrar" OnClientClick="return ConfirmMe();" />
                <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" Text="Actualizar"
                    OnClientClick="return ConfirmMe();" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                    Text="Cancelar" CausesValidation="False" />
            </li>
        </ul>
    </div>
    <asp:HiddenField ID="hdfurlimg" runat="server" />
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Scripts/jquery.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Scripts/jquery-ui.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Scripts/jquery.fileupload.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Scripts/jquery.fileupload-ui.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#aspnetForm').fileUploadUI({
                url: '../FileUpload.ashx',
                method: 'POST',
                uploadTable: $('#files'),
                downloadTable: $('#files'),
                buildUploadRow: function (files, index) {
                    //                    return $('<tr><td>' + files[index].name + '<\/td>' +
                    //                            '<td class="file_upload_progress"><div><\/div><\/td>' +
                    //                            '<td class="file_upload_cancel">' +
                    //                            '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    //                            '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    //                            '<\/button><\/td><\/tr>');
                },
                buildDownloadRow: function (file) {
                    //alert('holass');
                    d = new Date();
                    $('#<%= ImgAvatar.clientID() %>').attr('src', $('#<%= hdfurlimg.ClientID()  %>').val() + '?' + d.getTime());
                }
            });
        });
    </script>
</asp:Content>
