Imports _3Dev.FW.Web
Imports SPE.Entidades
Imports SPE.Web
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.EmsambladoComun
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging

'Imports System.Messaging


Partial Class CLI_ADAcCl
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CCliente)

    Dim DestImage As Bitmap
    Dim SourceToImage As Bitmap
    Dim FileName1 As String
    Dim FileName2 As String
    Dim Name As String
    'Dim Name2 As String
    Public domfile As String = ""

#Region "propiedades"

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return Me.btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return Me.btnRegistrar
        End Get
    End Property

    ''' <summary>
    ''' Metodo que maneja los mesajes de la inteface
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return Me.lblMensaje

        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return Me.btnCancelar
        End Get
    End Property
#End Region

#Region "m�todos overrides"

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CCliente()
    'End Function

    Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
        MyBase.OnInitComplete(e)
        'ucUsuarioDatosComun.FlagPreMostrarCamposMandatorios = True
    End Sub
    Protected Overrides Sub OnPreLoad(ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            'Page.SetFocus(txtEmailPrincipal)
            MaintenanceKeyValue = UserInfo.Email
        End If
        MyBase.OnPreLoad(e)
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)



        MyBase.OnLoad(e)
        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            Dim TieneDataCompleta As Boolean = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).TieneInfoCompleta

            If Not TieneDataCompleta Then
                BtnCancel.Visible = False
                ltTitulo.Text = "Actualice sus datos"
                pnlInfoCambio.Visible = True
            End If
        End If
        Dim EmailAEnc As String = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email
        Dim Clave As String = Encripta(EmailAEnc)

        If Not IsPostBack Then
            If Not SPE.Web.PaginaBase.UserInfo.ImagenAvatar Is Nothing Then
                Me.ImgAvatar.ImageUrl = "~/img/temposave/" & Clave & ".jpg?654321"
            Else
                Me.ImgAvatar.ImageUrl = "~/img/avatar/avatar.jpg"
            End If
        End If

    End Sub

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        lblimg.Visible = False
        Dim obeCliente As SPE.Entidades.BECliente = New SPE.Entidades.BECliente
        obeCliente.Email = Me.txtEmailPrincipal.Text
        obeCliente.AliasCliente = Me.txtAlias.Text
        obeCliente.EmailAlternativo = Me.txtMailAlternativo.Text
        obeCliente.IdEstado = _3Dev.FW.Util.DataUtil.StringToInt(hdnIdEstado.Value)
        obeCliente.IdCliente = _3Dev.FW.Util.DataUtil.StringToInt(hdnIdCliente.Value)

        If CType(ViewState("SeCargo"), Boolean) Then
            obeCliente.ImagenAvatar = CType(ViewState("Image"), Byte())
        Else
            obeCliente.ImagenAvatar = CType(Session("ImageIni"), Byte())
        End If


        obeCliente.FlagRegistrarPc = rbtnFRP.SelectedValue 'ddlFRP.SelectedValue
        ucUsuarioDatosComun.CargarBEUsuario(obeCliente)
        obeCliente.IdUsuarioActualizacion = _3Dev.FW.Web.Security.UserInfo.IdUsuario

        Return obeCliente
    End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCliente As BECliente = CType(be, BECliente)
        Me.txtEmailPrincipal.Text = obeCliente.Email
        Me.rbtnFRP.SelectedValue = obeCliente.FlagRegistrarPc
        Dim ClaveSinMD5 As String
        Dim Clave As String
        ClaveSinMD5 = obeCliente.Email
        Clave = Encripta(ClaveSinMD5)
        Me.txtAlias.Text = obeCliente.AliasCliente
        Me.txtMailAlternativo.Text = obeCliente.EmailAlternativo
        'Aqui Region recycle
        If Not obeCliente.ImagenAvatar Is Nothing Then
            Me.ImgAvatar.ImageUrl = "~/img/temposave/" & Clave & ".jpg"
        Else
            Me.ImgAvatar.ImageUrl = "~/img/temposave/avatar/avatar.jpg"
        End If
        hdnIdEstado.Value = obeCliente.IdEstado
        hdnIdCliente.Value = obeCliente.IdCliente
        ucUsuarioDatosComun.RefrescarVista(obeCliente)
        MyBase.AssignBusinessEntityValuesInLoadingInfo(be)
    End Sub

    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        MyBase.ConfigureMaintenance(e)
        e.MaintenanceKey = ""
        e.URLPageCancelForEdit = "PgPrl.aspx"
    End Sub
    Public Overrides Sub OnAfterUpdate()

        Dim TieneDataCompleta As Boolean
        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            TieneDataCompleta = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).TieneInfoCompleta
        End If

        MyBase.OnAfterUpdate()
        LoadUserInfo((New SPE.Web.Seguridad.SPEUsuarioClient()).GetUserInfoByUserName(UserInfo.Email.Trim()))

        HttpContext.Current.Session.Remove("SitemapCompleto")

        If Not TieneDataCompleta Then
            Response.Redirect("~/Principal.aspx")
        End If

        Dim EmailAEnc As String = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email
        ImgAvatar.ImageUrl = "../img/temposave/" & Encripta(EmailAEnc) & ".jpg?123456"

        'CType(Master.FindControl("ImgAvatar"), ImageButton).ImageUrl = "../img/temposave/" & Encripta(EmailAEnc) & ".jpg?123456"

    End Sub

#End Region

#Region "m�todos de compresi�n"

    'Private Function CreateBECliente(ByVal be As SPE.Entidades.BECliente) As SPE.Entidades.BECliente
    '    be = New SPE.Entidades.BECliente
    '    be.Email = UserInfo.Email
    '    Return be
    'End Function

    Public Shared Function GetEncoderInfo(ByVal mimeType As String) As ImageCodecInfo
        Dim j As Integer
        Dim encoders As ImageCodecInfo()
        encoders = ImageCodecInfo.GetImageEncoders()
        For j = 0 To encoders.Length
            If encoders(j).MimeType = mimeType Then
                Return encoders(j)
            End If
        Next j
        Return Nothing
    End Function

    Public Shared Sub SaveJPGWithCompressionSetting(ByVal image As Image, ByVal szFileName As String, ByVal lCompression As Long)
        Dim eps As EncoderParameters = New EncoderParameters(1)
        eps.Param(0) = New EncoderParameter(Encoder.Quality, lCompression)
        Dim ici As ImageCodecInfo = GetEncoderInfo("image/jpeg")
        Try
            image.Save(szFileName, ici, eps)
        Catch exc As Exception
            'MessageBox.Show(exc, " Atenci�n !", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region


#Region "IMG2BYTE-BYTE2IMG"

    Public Shared Function Image2Bytes(ByVal img As Image) As Byte()
        Dim sTemp As String = Path.GetTempFileName()
        Dim fs As New FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite)
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        '' Cerrarlo y volverlo a abrir
        '' o posicionarlo en el primer byte
        ''fs.Close()
        ''fs = New FileStream(sTemp, FileMode.Open, FileAccess.Read)
        fs.Position = 0
        ''
        Dim imgLength As Integer = CInt(fs.Length)
        Dim bytes(0 To imgLength - 1) As Byte
        fs.Read(bytes, 0, imgLength)
        fs.Close()
        Return bytes
    End Function

    Public Function Bytes2Image(ByVal bytes() As Byte) As Image
        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm
    End Function

#End Region

#Region "Encriptar"

    Function Encripta(ByVal Texto As String) As String
        Dim Clave As String, i As Integer, Pass2 As String
        Dim CAR As String, Codigo As String
        Clave = "#SPE%$"
        Pass2 = ""
        For i = 1 To Len(Texto)
            CAR = Mid(Texto, i, 1)
            Codigo = Mid(Clave, ((i - 1) Mod Len(Clave)) + 1, 1)
            Pass2 = Pass2 & Microsoft.VisualBasic.Strings.Right("0" & Hex(Asc(Codigo) Xor Asc(CAR)), 2)
        Next i
        Encripta = Pass2
    End Function

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Me.FileUpload1.HasFile Then
        '    CargarImagen()
        'End If
        domfile = System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile")
        If Not IsPostBack Then
            Dim EmailAEnc As String = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email
            Page.Form.Enctype = "multipart/form-data"
            hdfurlimg.Value = "../img/temposave/" & Encripta(EmailAEnc) & ".jpg" '"temposave/553A33314A56422539294411146B102248454A3F7E264A49.jpg"
        End If


    End Sub

    'Protected Sub CargarImagen()
    '    Try
    '        If FileUpload1.PostedFile IsNot Nothing AndAlso FileUpload1.PostedFile.FileName <> "" Then

    '            'Generar un bitmap para el origen
    '            Dim SourceImage As Bitmap
    '            SourceImage = New Bitmap(FileUpload1.FileContent)

    '            ' Generar un bitmap para el resultado
    '            SourceToImage = New Bitmap(SourceImage.Width, SourceImage.Height)

    '            ' Generar un objeto Gr�fico para el Bitmap resultante
    '            Dim gr_source As Graphics = Graphics.FromImage(SourceToImage)

    '            ' Copiar la imagen origen al Bitmap destino
    '            gr_source.DrawImage(SourceImage, 0, 0, SourceToImage.Width, SourceToImage.Height)

    '            'Nombre de Archivos anterior
    '            'FileName1 = Path.GetFileNameWithoutExtension(FileUpload1.FileName)
    '            'FileName2 = FileUpload1.FileName
    '            Name = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email
    '            'Name2 = Name1 & ".jpg"
    '            FileName1 = Encripta(Name)
    '            FileName2 = FileName1 & ".jpg"
    '            Dim DG As String = "~\img\temposave\" & FileName2
    '            Dim DGTemporal As String = "~\img\temposave\" & FileName1
    '            '////////////////////////////////////////////////////////////////////////////////////////////////////
    '            'SE COMPRIME ANTES DE GENERAR LA IMAGEN
    '            If IsNothing(ImgAvatar.HasAttributes) = False Then
    '                ImgAvatar.ImageUrl = "~/img/avatar/avatar.jpg"
    '                lblimg.Visible = False
    '            End If
    '            'SourceImage.Dispose()

    '            Dim NeedsHorizontalCrop As Boolean = True
    '            Dim NeedsVerticalCrop As Boolean = False

    '            'Determina si la imagen es Landscape o Portrait
    '            If SourceToImage.Height > SourceToImage.Width Then
    '                NeedsHorizontalCrop = False
    '                NeedsVerticalCrop = True
    '            End If

    '            'Determina si la imagen excede el ancho del PictureBox
    '            If SourceToImage.Width < 200 Then
    '                NeedsHorizontalCrop = False
    '                If SourceToImage.Height > 150 Then
    '                    NeedsVerticalCrop = True
    '                End If
    '            End If

    '            'Calcula el Factor de Ajuste
    '            Dim scale_factor As Single = 1
    '            If SourceToImage.Width > 0 Then
    '                If NeedsHorizontalCrop = True Then
    '                    ' Obtiene el Factor de Ajuste
    '                    scale_factor = 200 / SourceToImage.Width
    '                End If
    '            End If

    '            If SourceToImage.Height > 0 Then
    '                If NeedsVerticalCrop = True Then
    '                    ' Obtiene el Factor de Ajuste
    '                    scale_factor = 150 / SourceToImage.Height
    '                End If
    '            End If

    '            ' Generar un bitmap tmp para el resultado. Ajuste Proporcional
    '            Dim DestTmpImage As New Bitmap(CInt(SourceToImage.Width * scale_factor), CInt(SourceToImage.Height * scale_factor))

    '            ' Generar un objeto Gr�fico para el bitmap tmp resultante
    '            Dim gr_desttmp As Graphics = Graphics.FromImage(DestTmpImage)

    '            ' Copiar la imagen origen al bitmap tmp destino
    '            gr_desttmp.DrawImage(SourceToImage, 0, 0, DestTmpImage.Width, DestTmpImage.Height)

    '            'SourceToImage.Dispose()
    '            'DestTmpImage.Dispose()
    '            'gr_desttmp.Dispose()
    '            'Comprime y Guarda un Archivo Temporal para calcular su peso en Kb

    '            Dim pathCurrent As String
    '            pathCurrent = Server.MapPath("~/")
    '            Dim raiz As String
    '            raiz = pathCurrent & "img\temposave\" & FileName1 & "temp.jpg"

    '            Try
    '                SaveJPGWithCompressionSetting(DestTmpImage, raiz, 80)
    '                'lblmsj.Text = CStr(Session("msj"))
    '                lblimg.Visible = False

    '            Catch exc As Exception
    '                'MessageBox.Show(exc, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            End Try

    '            'ImgAvatar.ImageAlign = "Middle"

    '            'La lectura del nuevo archivo no se puede hacer en forma directa y repetitiva
    '            'ya que est� bloqueado por GDI+ la 1era vez que se lo utiliza,
    '            'por lo tanto resulta necesario resolver en varios pasos. 
    '            'Al efectuar el Dispose() se libera el recurso


    '            DestImage = New Bitmap(pathCurrent & "img\temposave\" & FileName1 & "temp.jpg")

    '            ' Generar un bitmap para el resultado
    '            Dim DestToImage As New Bitmap(DestImage.Width, DestImage.Height)

    '            ' Generar un objeto Grafico para el bitmap resultante
    '            Dim gr_dest As Graphics = Graphics.FromImage(DestToImage)

    '            ' Copiar la imagen origen al bitmap destino
    '            gr_dest.DrawImage(DestImage, 0, 0, DestToImage.Width, DestToImage.Height)

    '            'La almaceno nuevamente en img
    '            DestToImage.Save(Context.Server.MapPath(DG), System.Drawing.Imaging.ImageFormat.Jpeg)

    '            'Muestra imagen comprimida
    '            ImgAvatar.ImageUrl = "~/img/temposave/" & FileName1 & "temp.jpg"

    '            'Pruebas de Reconversion
    '            ViewState("Image") = Image2Bytes(DestImage)
    '            ViewState("SeCargo") = True
    '            'Session("Image") = CType(DestToImage, Image)
    '            'Label2.Text = Session("Image").ToString()

    '            'Liberar recursos
    '            'DestImage.Dispose()
    '            gr_dest.Dispose()
    '            gr_desttmp.Dispose()
    '            lblimg.Visible = False
    '        End If
    '    Catch ex As Exception
    '        lblimg.Visible = True
    '        lblimg.Text = "No se pudo actualizar su Avatar, aseg�rese que el archivo que intenta subir tenga formato de imagen ..."
    '    End Try
    'End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - Mis Datos"
    End Sub

    Public Overrides Function AllowToUpdate() As Boolean
        Dim lista, combo As Boolean
        lblMensaje.Text = ""
        lista = True
        combo = True

        Dim obeCliente As SPE.Entidades.BECliente = New SPE.Entidades.BECliente
        ucUsuarioDatosComun.CargarBEUsuario(obeCliente)

        Dim objCUsuario As New CCliente
        Dim beUsuario As New _3Dev.FW.Entidades.Seguridad.BEUsuario
        beUsuario.IdTipoDocumento = obeCliente.IdTipoDocumento
        beUsuario.Email = Me.txtEmailPrincipal.Text
        beUsuario.NumeroDocumento = obeCliente.NumeroDocumento
        Dim intExisteTipoNumDoc = objCUsuario.ValidarTipoyNumeroDocPorEmail(beUsuario)
        If intExisteTipoNumDoc > 0 Then
            lblMensaje.CssClass = "MensajeValidacion"
            Me.lblMensaje.Text = "El tipo y n�mero de documento est� siendo usado por otro usuario."
            lista = False
        End If

        Page.Validate()
        Return lista
    End Function

End Class
