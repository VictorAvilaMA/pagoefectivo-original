<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="ADAvRe.aspx.vb"
    Inherits="CLI_ADAvRe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <title>Untitled Page</title>
    <link rel="stylesheet" type="text/css" href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/default.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" title="default" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Images_Otros/BannerPagoEfectivo.JPG" />
    </div>
    <div id="div11" class="divSubContenedor">
        <div id="div12" class="divContenedorTitulo">
            Registro Exitoso</div>
        <fieldset class="ContenedorEtiquetaTexto">
            <div id="div3" class="EtiquetaTextoIzquierda">
                <asp:Label ID="Label100" runat="server" CssClass="TextoLargo" Text="Se ha enviado un correo con instrucciones necesarias para completar el registro."
                    Width="495px"></asp:Label>
            </div>
        </fieldset>
        <fieldset class="ContenedorEtiquetaTexto">
            <div id="div1" class="EtiquetaTextoIzquierda">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Login.aspx">Login</asp:HyperLink>
            </div>
        </fieldset>
    </div>
    </form>
</body>
</html>
