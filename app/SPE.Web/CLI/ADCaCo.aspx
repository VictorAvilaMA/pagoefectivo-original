<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADCaCo.aspx.vb" Inherits="CLI_ADCaCo" Title="PagoEfectivo - Cambiar contrase�a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Cambiar Contrase�a</h2>
    <asp:UpdatePanel ID="Ubigeo" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="conten_pasos3">
                <h4>
                    Datos de usuario</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Contrase�a anterior :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtPasswordAnterior" runat="server" CssClass="neu" TextMode="Password"
                            MaxLength="100" style="float:left"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPasswordAnterior"
                            ErrorMessage="Ingrese su anterior contrase�a">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> (*) Nueva Contrase�a :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNuevoPassword" runat="server" CssClass="neu" TextMode="Password"
                            MaxLength="100" style="float:left"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNuevoPassword" runat="server" ControlToValidate="txtNuevoPassword"
                            ErrorMessage="Ingrese su nueva contrase�a">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> (*) Confirmar Contrase�a :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtConfirmarPassword" runat="server" CssClass="neu" TextMode="Password"
                            MaxLength="100" style="float:left"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvConfirmarNuevoPassword" runat="server" ControlToValidate="txtConfirmarPassword"
                            ErrorMessage="Ingrese la confirmacion de la nueva contrase�a">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator CssClass="msje" ID="cvlConfirmarContrase�a" runat="server"
                            ControlToCompare="txtNuevoPassword" ControlToValidate="txtConfirmarPassword"
                            ErrorMessage="La nueva contrase�a no conincide con su verificaci�n" ShowMessageBox="true">*</asp:CompareValidator>
                    </li>
                    <li class="t1"><span class="color"></span></li>
                    <li class="t2" style="height:auto">
                        <asp:Label CssClass="msje" ID="lblMensajeInformativo" Width="100%" runat="server"
                            Text="* M�nimo [Nro] Caracteres" style="line-height:normal;text-align:left;height:1px;line-height:normal"></asp:Label>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="w100" Visible="true"
                        EnableClientScript="true" ShowSummary="true" />
                        <asp:Label ID="lblMensaje" runat="server" Width="100%" style="float:none;line-height:normal;text-align:left;height:1px;line-height:normal"></asp:Label>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Cambiar"
                            OnClientClick="return ConfirmMe();" />
                        <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Limpiar"
                            CausesValidation="False"  />
                    </li>
                    <li class="t1"><span class="color"></span></li>
                    <li class="t2">
                        
                    </li>
                    <li class="t1"><span class="color"></span></li>
                </ul>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCancelar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
