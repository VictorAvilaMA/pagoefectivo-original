Imports SPE.Entidades
Imports SPE.Web
Imports _3Dev.FW.Entidades.Seguridad


Partial Class CLI_ADCaCo
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CCliente)

    Function CreateBECliente(ByVal be As SPE.Entidades.BECliente) As SPE.Entidades.BECliente
        be = New SPE.Entidades.BECliente
        be.Email = UserInfo.Email
        Return be
    End Function

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCliente, SPE.EmsambladoComun.ParametrosSistema.RolRepresentante, SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor, SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        'ValidationSummary1.Visible = True
        If (txtNuevoPassword.Text.Length >= DameMaximoTamanioContrasenia()) Then
            Dim objCCliente As New CCliente
            Dim obeUsuario As New BEUsuario
            obeUsuario.Password = Me.txtPasswordAnterior.Text
            obeUsuario.NuevoPassword = Me.txtNuevoPassword.Text
            obeUsuario.IdUsuario = UserInfo.IdUsuario
            Me.lblMensaje.Text = ""

            Dim resultado As Integer
            resultado = objCCliente.CambiarContraseñaUsuario(obeUsuario)
            If resultado = UserInfo.IdUsuario Then
                ThrowSuccessfullyMessage("Su contraseña se cambio satisfactoriamente.")
                Me.btnRegistrar.Enabled = False
            Else
                ThrowErrorMessage("Su contraseña anterior no es correcta.")
            End If

        Else
            ThrowErrorMessage("La contraseña debe ser como mínimo de  " + DameMaximoTamanioContrasenia().ToString() + " caracteres. ")

        End If

    End Sub
    Sub limpiar()
        Me.txtPasswordAnterior.Text = ""
        Me.txtNuevoPassword.Text = ""
        Me.txtConfirmarPassword.Text = ""
        Me.lblMensaje.Text = ""
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        LimpiarTextos()
    End Sub
    Sub LimpiarTextos()
        Me.txtPasswordAnterior.Text = ""
        Me.txtNuevoPassword.Text = ""
        Me.txtConfirmarPassword.Text = ""
        ' ValidationSummary1.Visible = False
    End Sub
    Public Function DameMaximoTamanioContrasenia() As Integer
        Return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings(SPE.EmsambladoComun.ParametrosSistema.MinCaracteresContrasenia))
    End Function
    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        Page.SetFocus(txtPasswordAnterior)
        lblMensajeInformativo.Text = lblMensajeInformativo.Text.Replace("[Nro]", DameMaximoTamanioContrasenia().ToString)

    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - Cambiar contraseña"
    End Sub

End Class
