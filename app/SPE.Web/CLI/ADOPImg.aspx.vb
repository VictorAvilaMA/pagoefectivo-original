Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Partial Class CLI_ADOPImg
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GenerarCodigo()
    End Sub
    Sub GenerarCodigo()
        Dim numero As String = Request.QueryString("numero")
        SPE.Web.Util.UtilBarCode.GenerarCodigoBarra(numero)
    End Sub
End Class
