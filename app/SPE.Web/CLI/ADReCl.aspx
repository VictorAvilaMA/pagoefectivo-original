<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="ADReCl.aspx.vb" Inherits="CLI_ADReCl" Title="PagoEfectivo - Registro de Cliente"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc2" %>
<%@ Register Src="../UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc3" %>
<%@ Register Assembly="WebControlCaptcha" Namespace="WebControlCaptcha" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="ContentPlaceHolderHeaderJS1" ContentPlaceHolderID="ContentPlaceHolderHeaderJS"
    runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#liInicio a').attr('href', '../Default.aspx');
            $('#liPersonas a').attr('href', '../Personas.aspx');
            $('#liEmpresas a').attr('href', '../Empresas.aspx');
            $('#liCentroPago a').attr('href', '../CentrosPago.aspx');
            $('#liComerciosAfiliados a').attr('href', '../ComerciosAfiliados.aspx');
            $('#liContactenos .dentrope').attr('href', '../contactenos.aspx');
            $('#liContactenos .fuerape').attr('href', 'http://centraldeayuda.pagoefectivo.pe/anonymous_requests/new');
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('html,body').css('background', '#d4a476');
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/reset.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/estilo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <!--[if lte IE 7]><link href="App_Themes/SPE/css/ie7.css" rel="stylesheet" type="text/css" /><![endif]-->
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/fixEstilo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/funciones.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <%--<div id="body_pe">--%>
    <div id="contenidos">
        <%--<div class="registrocont_pe">--%>
        <div id="content">
            <h2>
                Registro Clientes</h2>
            <h3 style="float: left">
                (<span class="required_red">*</span>) Campos Obligatorios</h3>
            <div style="clear: both">
            </div>
            <div class="conten_pasos3">
                <div>
                    <h4>
                        Datos de Usuario</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> E-mail Principal: (
                            <label style="color: red">
                                *
                            </label>
                            ) </li>
                        <li class="t2">
                            <asp:TextBox ID="txtEmailPrincipal" runat="server" CssClass="normal" ReadOnly="false"
                                MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmailPrincipal" runat="server" ControlToValidate="txtEmailPrincipal"
                                ErrorMessage="Debe ingresar un email.">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailPrincipal"
                                ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        </li>
                        <li class="t1"><span class="color">>></span> Password: (
                            <label style="color: red">
                                *
                            </label>
                            ) </li>
                        <li class="t2">
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="normal" TextMode="Password"
                                MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                ErrorMessage="Debe ingresar su password">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span> Repita Password: (
                            <label style="color: red">
                                *
                            </label>
                            ) </li>
                        <li class="t2">
                            <asp:TextBox ID="txtConfirmacionPassword" runat="server" CssClass="normal" TextMode="Password"
                                MaxLength="100"></asp:TextBox>
                            <ul>
                                <li style="width: auto; clear: both;">
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                                        Style="width: auto !important;" ControlToValidate="txtConfirmacionPassword" ErrorMessage="Su password de confirmacion no coincide."></asp:CompareValidator>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <h4 style="margin-top: 25px !important;">
                        Datos Personales</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Alias: (
                            <label style="color: red">
                                *
                            </label>
                            ) </li>
                        <li class="t2">
                            <asp:TextBox ID="txtAlias" runat="server" CssClass="normal" MaxLength="80"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtAlias"
                                ErrorMessage="Ingrese un alias.">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span> Nombres: (
                            <label style="color: red">
                                *
                            </label>
                            ) </li>
                        <li class="t2">
                            <asp:TextBox ID="txtNombres" runat="server" CssClass="normal" MaxLength="1000"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNombres" runat="server" ControlToValidate="txtNombres"
                                ErrorMessage="Ingrese su nombre.">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span> Apellidos: (
                            <label style="color: red">
                                *
                            </label>
                            ) </li>
                        <li class="t2">
                            <asp:TextBox ID="txtApellidos" runat="server" CssClass="normal" ReadOnly="false"
                                MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ControlToValidate="txtApellidos"
                                ErrorMessage="Ingrese su apellido.">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" class="w50  bgf8f8f8 ">
                        <ContentTemplate>
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Documento: (
                                    <label style="color: red">
                                        *
                                    </label>
                                    ) </li>
                                <li class="t2">
                                    <p class="cal">
                                        <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="corta2" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:Label ID="labelNumero" runat="server" Text="N�:" CssClass="entre"></asp:Label>
                                        <span id="strgNroDocumento" runat="server" class="required_red " style="display: none;">
                                            *</span>
                                        <asp:TextBox ID="txtNumeroDocumento" runat="server" CssClass="corta" MaxLength="11"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvTipoDocumento" runat="server" ControlToValidate="ddlTipoDocumento"
                                            ErrorMessage="Seleccione un tipo de documento.">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNumeroDocumento"
                                            ErrorMessage="Ingrese N� de documento.">*</asp:RequiredFieldValidator>
                                    </p>
                                </li>
                            </ul>
                            <div style="clear: both">
                            </div>
                            <ul class="datos_cip2 h0">
                                <li class="t1"><span class="color">>></span> G&eacute;nero: (
                                    <label style="color: red">
                                        *
                                    </label>
                                    ) </li>
                                <li class="t2">
                                    <p class="cal">
                                        <asp:DropDownList ID="ddlGenero" runat="server" CssClass="corta">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvGenero" runat="server" ControlToValidate="ddlGenero"
                                            ErrorMessage="Seleccione su g�nero.">*</asp:RequiredFieldValidator>
                                    </p>
                                </li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="clear: both">
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <ul class="datos_cip2" style="width: 700px; clear: both">
                                <li class="t1"><span class="color">>></span> Fecha de Nacimiento: (
                                    <label style="color: red">
                                        *
                                    </label>
                                    ) </li>
                                <li class="t2" style="width: 500px;">
                                    <p class="cal">
                                        <asp:DropDownList ID="ddlFechaMes" runat="server" CssClass="corta2" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlFechaDia" runat="server" CssClass="corta2">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlFechaAnio" runat="server" AutoPostBack="true" CssClass="corta2"
                                            OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFechaMes" runat="server" ControlToValidate="ddlFechaMes"
                                            ErrorMessage="Seleccione el mes de nacimiento.">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfvFechaDia" runat="server" ControlToValidate="ddlFechaDia"
                                            ErrorMessage="Seleccione el d�a de nacimiento.">*</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="rfvFechaAnio" runat="server" ControlToValidate="ddlFechaAnio"
                                            ErrorMessage="Seleccione la fecha de nacimiento.">*</asp:RequiredFieldValidator>
                                    </p>
                                </li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="clear: both">
                    </div>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Tel&eacute;fono</li>
                        <li class="t2">
                            <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftbTelefono" runat="server" TargetControlID="txtTelefono"
                                ValidChars="0123456789">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RegularExpressionValidator ID="revFono" runat="server" Display="Dynamic" ControlToValidate="txtTelefono"
                                ErrorMessage="*" ValidationExpression="^\d+(\.\d\d)?$">*</asp:RegularExpressionValidator>
                        </li>
                    </ul>
                    <asp:UpdatePanel ID="Ubigeo" runat="server">
                        <ContentTemplate>
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span> Pa&iacute;s: (
                                    <label style="color: red">
                                        *
                                    </label>
                                    ) </li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlPais" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </li>
                                <li class="t1"><span class="color">>></span> Departamento: (
                                    <label style="color: red">
                                        *
                                    </label>
                                    ) </li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </li>
                                <li class="t1"><span class="color">>></span> Ciudad: (
                                    <label style="color: red">
                                        *
                                    </label>
                                    ) </li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="corta">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="Seleccione una Ciudad"
                                        ControlToValidate="ddlCiudad">*</asp:RequiredFieldValidator>
                                </li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="clear: both">
                    </div>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span> Direcci&oacute;n</li>
                        <li class="t2">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">>></span> Mail Alternativo:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtMailAlternativo" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        </li>
                    </ul>
                </div>
                <div class="aceptarmedios" style="padding-left: 100px; padding-right: 100px">
                    <p style="line-height: 20px">
                        <b>Aceptaci&oacute;n de t�rminos y condiciones del sitio</b> Para registrarse en
                        PagoEfectivo debe aceptar condiciones de uso y la pol�tica de privacidad del sitio.
                        <asp:HyperLink ID="HyperLinkTeCoUs" runat="server" Target="_blank" Text="T�rminos de uso"></asp:HyperLink>
                        y
                        <asp:HyperLink ID="HyperLinkPoliticaPriv" runat="server" Target="_blank" Text="Pol�tica de privacidad"></asp:HyperLink></p>
                    <div class="terminosycondi">
                        <label for="conditions">
                            <asp:CheckBox ID="ckbTerminos" runat="server" CssClass="Check" Text="" />
                            Acepto los t�rminos de uso del sitio <span class="required_red ">*</span>
                        </label>
                    </div>
                </div>
                <div class="capcha_pe" style="padding-left: 100px; padding-right: 100px">
                    <h3 class="titulo_regist pattop302" style="text-align: left">
                        Verificaci�n de Seguridad
                    </h3>
                    <asp:UpdatePanel ID="UpdatePanel1" class="captchaContent" runat="server" EnableViewState="False">
                        
                        <ContentTemplate>
                        <cc2:CaptchaControl ID="CaptchaControl1"  runat="server" CaptchaMaxTimeout="3600"
                                Text="Ingrese el codigo mostrado:" CaptchaMinTimeout="0" />
                        </ContentTemplate>
                                                                 
                    </asp:UpdatePanel>
                </div>
                <div class="mensaje_error">
                    <asp:Label ID="lblMensaje" runat="server" CssClass="mensaje_error"></asp:Label>
                    <%--<asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="mensaje_error" />--%>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" Style="padding-left: 200px" />
                </div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btniRegistrar" runat="server" OnClientClick="return ConfirmReCli();"
                            CssClass="input_azul5" Text="Enviar" />
                        <asp:Button ID="btniCancelar" runat="server" OnClientClick="return CancelMe();" CssClass="input_azul4"
                            CausesValidation="False" Text="Cancelar" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- fin body-->
</asp:Content>
