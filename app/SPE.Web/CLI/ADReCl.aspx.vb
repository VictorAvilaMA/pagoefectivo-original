Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Collections
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Excepciones
Imports System.Globalization

Partial Class CLI_ADReCl
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CCliente)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMensaje.Text = ""
        Page.Master.FindControl("dvlnkregistroingreso").Visible = False
        
        If Not Page.IsPostBack Then
            AsignarHyperlinksUrls()
            'Session("Entidad") = Nothing
            '''''''

            'Page.SetFocus(txtEmailPrincipal)

            'SeleccionarPais(0)
            'SeleccionarDepartamento(0)
            'SeleccionarCiudad(0)

            'SeleccionarPais(1)
            'ddlPais_SelectedIndexChanged(sender, e)
            'SeleccionarDepartamento(1)
            'ddlDepartamento_SelectedIndexChanged(sender, e)

            'CargarComboTipoDocumento()
            If Request.QueryString("datosEnc") <> "" Then
                CargarParametros()
            End If
            '
            ' HyperLinkTeCoUs.NavigateUrl = "javascript:openPgTeCoUs();"
            Page.MaintainScrollPositionOnPostBack = True
            InicializarVista()
            PreMostrarCamposMandatorios()
            ' InicializarVista()
            txtNumeroDocumento.MaxLength = 11
            '
        Else
            'If CaptchaControl.ErrorMessage = "El codigo escrito ha expirado600 segundos." Then
            '    CaptchaControl.ErrorMessage = ""
            'End If
        End If
    End Sub

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CCliente()
    'End Function

    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Return CreateBECliente(be)
    End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeCliente As SPE.Entidades.BECliente = CType(be, SPE.Entidades.BECliente)
        Me.txtEmailPrincipal.Text = obeCliente.Email
        Me.txtAlias.Text = obeCliente.AliasCliente
        Me.txtMailAlternativo.Text = obeCliente.EmailAlternativo
        RefrescarVista(obeCliente)
        MyBase.AssignBusinessEntityValuesInLoadingInfo(be)
        'Desactivar controles
        Me.txtEmailPrincipal.ReadOnly = True

    End Sub

    Public Function CreateBECliente(ByVal be As SPE.Entidades.BECliente) As SPE.Entidades.BECliente

        be = New SPE.Entidades.BECliente()
        be.Email = Me.txtEmailPrincipal.Text
        be.Password = Me.txtPassword.Text
        be.AliasCliente = Me.txtAlias.Text
        CargarBEUsuario(be)
        be.EmailAlternativo = Me.txtMailAlternativo.Text
        be.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
        'If (Session("Entidad") <> Nothing And Session("Url") <> Nothing) Then
        'cambio realizado x SSusanibar
        If (Session("EntidadSuscriptores1") <> Nothing And Session("UrlServicio1") <> Nothing) Then
            be.OrdenPago = ObtenerPreOrdenPagoSuscriptores()
        Else
            be.OrdenPago = ObtenerPreOrdenPago()
        End If

        Return be
    End Function

    Public Function ObtenerPreOrdenPago() As BEOrdenPago
        '
        Dim obeOrdenPago As New BEOrdenPago

        'Detalle Orden Pago
        Dim obeDetalle As New BEDetalleOrdenPago
        Dim listaDetalle As New List(Of BEDetalleOrdenPago)
        listaDetalle.Add(obeDetalle)

        'Seteo de Parametros
        'obeOrdenPago.
        obeOrdenPago.MerchantID = ViewState(SPE.EmsambladoComun.TraductorServ.EMerchantId)
        obeOrdenPago.DatosEnc = ViewState(SPE.EmsambladoComun.TraductorServ.EDatosEnc)
        obeOrdenPago.ParUtil = ViewState(SPE.EmsambladoComun.TraductorServ.EParUtil)
        obeOrdenPago.Url = ViewState("Url")
        obeOrdenPago.DetallesOrdenPago = listaDetalle

        Return obeOrdenPago
        '
    End Function

    Public Function ObtenerPreOrdenPagoSuscriptores() As BEOrdenPago
        Dim obeOrdenPago As New BEOrdenPago
        'obeOrdenPago.UrlServicio = Session("Url")
        'obeOrdenPago.XmlString = Session("Entidad").ToString()
        'NUEVO CODIGO PARA PRUEBA ssusanibar@3devnet.com
        '
        obeOrdenPago.UrlServicio = Session("UrlServicio1").ToString()
        obeOrdenPago.XmlString = Session("EntidadSuscriptores1").ToString()
        '

        Return obeOrdenPago
    End Function

    Private Sub CargarParametros()
        '   
        ViewState(SPE.EmsambladoComun.TraductorServ.EDatosEnc) = Request.QueryString("datosEnc") 'DEFAULT
        ViewState(SPE.EmsambladoComun.TraductorServ.EMerchantId) = Request.QueryString("Empresa") 'DEFAULT
        ViewState(SPE.EmsambladoComun.TraductorServ.EParUtil) = Request.QueryString(SPE.EmsambladoComun.TraductorServ.EParUtil)
        ViewState("Url") = Request.Url.OriginalString.Substring(Request.Url.OriginalString.IndexOf("?"))
        '
    End Sub

    Public Overrides Function AllowToInsert() As Boolean
        Page.MaintainScrollPositionOnPostBack = True
        'Validate()
        If Not (Page.IsValid) Then
            Return False
        End If

        Dim maxcaracteres As Integer = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings(SPE.EmsambladoComun.ParametrosSistema.MinCaracteresContrasenia))
        If ckbTerminos.Checked = False Then
            ThrowErrorMessage("Usted debe leer y aceptar los terminos de referencia.")
            ckbTerminos.Focus()
            Return False
        ElseIf txtPassword.Text.Trim.Length < maxcaracteres Then
            ThrowErrorMessage("Usted debe ingresar como m�nimo " + maxcaracteres.ToString() + " caracteres para su contrase�a.")
            txtPassword.Focus()
            Return False

        Else
            lblMensaje.CssClass = "MensajeTransaccion"
            Return True

        End If

    End Function
    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btniCancelar
        End Get

    End Property
    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btniRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Public Overrides Sub OnAfterInsert()
        lblMensaje.Text = ""
        If (valueRegister = -100) Then
            lblMensaje.Text = "Este correo est� siendo usado por otro usuario."
            txtEmailPrincipal.Focus()
        ElseIf (valueRegister = -200) Then
            lblMensaje.Text = "El tipo y n�mero de documento est� siendo usado por otro usuario."
            txtNumeroDocumento.Focus()
        Else
            Session("aliasClienteReg") = txtAlias.Text
            Session("emailClienteReg") = txtEmailPrincipal.Text
            Response.Redirect("ADReCl2.aspx")
        End If

    End Sub


    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        MyBase.ConfigureMaintenance(e)
        e.URLPageCancelForInsert = "~/default.aspx"
    End Sub

    Public Overrides Function DoInsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim idServicio As String = Request.QueryString("datosEnc")
        If (idServicio = "") Then
            Return MyBase.DoInsertRecord(be)
        Else
            Return CType(ControllerObject, SPE.Web.CCliente).RegistrarClienteYPreOrdenCompra(be)
        End If
    End Function

    Private Sub AsignarHyperlinksUrls()
        HyperLinkTeCoUs.NavigateUrl = SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlCondicionesUso
        HyperLinkPoliticaPriv.NavigateUrl = SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlPoliticaPrivacidad
    End Sub
    Public Overrides Sub OnBeforeInsert()
        MyBase.OnBeforeInsert()
        lblMensaje.Text = ""
    End Sub
    Public Overrides Sub OnBeforeUpdate()
        MyBase.OnBeforeUpdate()
        lblMensaje.Text = ""
    End Sub




#Region "Propiedades"

    Protected _flagPreMostrarCamposMandatorios As Boolean
    Public Property FlagPreMostrarCamposMandatorios() As Boolean
        Get
            Return _flagPreMostrarCamposMandatorios
        End Get
        Set(ByVal value As Boolean)
            _flagPreMostrarCamposMandatorios = value
            '            ucFechaNacimiento.FlagPreMostrarCamposMandatorios = value
        End Set
    End Property

    Public ReadOnly Property Nombres() As TextBox
        Get
            Return txtNombres
        End Get
    End Property

    Public ReadOnly Property Apellidos() As TextBox
        Get
            Return txtApellidos
        End Get
    End Property

    Public ReadOnly Property TipoDocumento() As DropDownList
        Get
            Return ddlTipoDocumento
        End Get
    End Property

    Public ReadOnly Property NroDocumento() As TextBox
        Get
            Return txtNumeroDocumento
        End Get
    End Property

    Public ReadOnly Property Genero() As DropDownList
        Get
            Return ddlGenero
        End Get
    End Property

    Public ReadOnly Property FechaNacimiento() As UCDropDateBase
        Get
            Return Me.FindControl("ucFechaNacimiento")
        End Get
    End Property


    Public ReadOnly Property Telefono() As TextBox
        Get
            Return txtTelefono
        End Get
    End Property

    Public ReadOnly Property Pais() As DropDownList
        Get
            Return ddlPais
        End Get
    End Property

    Public ReadOnly Property Departamento() As DropDownList
        Get
            Return ddlDepartamento
        End Get
    End Property

    Public ReadOnly Property Ciudad() As DropDownList
        Get
            Return ddlCiudad
        End Get
    End Property

    Public ReadOnly Property Direccion() As TextBox
        Get
            Return txtDireccion
        End Get
    End Property
#End Region

#Region "Metodos"

    Private _cntrlParametro As SPE.Web.CAdministrarParametro = Nothing
    Private ReadOnly Property CntrlParametro() As SPE.Web.CAdministrarParametro
        Get
            If (_cntrlParametro Is Nothing) Then
                _cntrlParametro = New SPE.Web.CAdministrarParametro()
            End If
            Return _cntrlParametro
        End Get
    End Property


    Private Sub PreMostrarCamposMandatorios()
        'strgNombres.Visible = _flagPreMostrarCamposMandatorios
        'strgApellidos.Visible = _flagPreMostrarCamposMandatorios
        'strgGenero.Visible = _flagPreMostrarCamposMandatorios
        'strgNroDocumento.Visible = _flagPreMostrarCamposMandatorios
        'strgTipoDocumento.Visible = _flagPreMostrarCamposMandatorios
        'strgCiudad.Visible = _flagPreMostrarCamposMandatorios

        'strgPais.Visible = _flagPreMostrarCamposMandatorios
        'strgDepartamento.Visible = _flagPreMostrarCamposMandatorios
        'strgCiudad.Visible = _flagPreMostrarCamposMandatorios
        'FlagPreMostrarCamposMandatorios = _flagPreMostrarCamposMandatorios
        'strgFechaNacimiento.Visible = _flagPreMostrarCamposMandatorios
    End Sub

    Public Sub InicializarVista()
        CargarComboTipoDocumento()
        CargarComboGenero()
        CargarCombosFecha()
        SeleccionarPais(0)
    End Sub

    Private Sub CargarComboTipoDocumento()
        CargarComboTipoDocumento(False)
    End Sub
    Private Sub CargarComboTipoDocumento(ByVal recargar As Boolean)
        If (recargar) Then
            Me.ddlGenero.Items.Clear()
        End If
        If ((Me.ddlGenero.Items.Count = 0)) Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlTipoDocumento, CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoTipoDocumento), "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        End If

    End Sub

    Private Sub CargarComboGenero()
        CargarComboGenero(False)
    End Sub
    Private Sub CargarComboGenero(ByVal recargar As Boolean)
        If (recargar) Then
            Me.ddlGenero.Items.Clear()
        End If
        If ((Me.ddlGenero.Items.Count = 0)) Then
            'lis(CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero))
            Dim lgeneros As List(Of SPE.Entidades.BEParametro) = CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero)
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlGenero, lgeneros, "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        End If

    End Sub

    Private Sub CargarCombosFecha()
        RefrescarVista()
    End Sub

    Public Function CargarBEUsuario(ByVal obeUsuario As SPE.Entidades.BEUsuarioBase) As SPE.Entidades.BEUsuarioBase
        obeUsuario.Nombres = Me.txtNombres.Text
        obeUsuario.Apellidos = Me.txtApellidos.Text
        obeUsuario.IdTipoDocumento = ObjectToInt(Me.ddlTipoDocumento.SelectedValue)
        obeUsuario.NumeroDocumento = Trim(Me.txtNumeroDocumento.Text)
        obeUsuario.IdCiudad = ObjectToInt(Me.ddlCiudad.SelectedValue)
        obeUsuario.Direccion = Me.txtDireccion.Text
        obeUsuario.Telefono = Me.txtTelefono.Text
        obeUsuario.IdGenero = ObjectToInt(ddlGenero.SelectedValue)
        obeUsuario.FechaNacimiento = DameFechaSeleccionada()
        Return obeUsuario
    End Function

    Public Sub RefrescarVista(ByVal obeUsuario As SPE.Entidades.BEUsuarioBase)

        'Me.txtNombres.Text = obeUsuario.Nombres
        'Me.txtApellidos.Text = obeUsuario.Apellidos
        'Me.ddlTipoDocumento.SelectedValue = obeUsuario.IdTipoDocumento
        'Me.txtNumeroDocumento.Text = obeUsuario.NumeroDocumento.ToString
        ''Carga de Ubigeo
        'Me.SeleccionarPais(obeUsuario.IdPais)
        'Me.SeleccionarDepartamento(obeUsuario.IdDepartamento)
        'Me.SeleccionarCiudad(obeUsuario.IdCiudad)
        'Me.ddlCiudad.SelectedValue = obeUsuario.IdCiudad
        'Me.txtDireccion.Text = obeUsuario.Direccion
        'Me.txtTelefono.Text = obeUsuario.Telefono
        RefrescarVista(obeUsuario.Nombres, obeUsuario.Apellidos, obeUsuario.IdTipoDocumento, obeUsuario.NumeroDocumento, _
        obeUsuario.IdGenero, obeUsuario.FechaNacimiento, obeUsuario.Telefono, obeUsuario.IdPais, obeUsuario.IdDepartamento, obeUsuario.IdCiudad, _
        obeUsuario.Direccion)

    End Sub

    Public Sub RefrescarVista(ByVal nombre As String, ByVal apellidos As String, ByVal idTipoDocumento As Integer, _
    ByVal nroDocumento As String, ByVal Idgenero As Integer, ByVal fechaNacimiento As DateTime, _
    ByVal telefono As String, ByVal idPais As Integer, ByVal idDepartamento As Integer, _
    ByVal idCiudad As Integer, ByVal direccion As String)
        Me.txtNombres.Text = nombre
        Me.txtApellidos.Text = apellidos

        CargarComboTipoDocumento()
        Me.ddlTipoDocumento.SelectedValue = idTipoDocumento

        Me.txtNumeroDocumento.Text = nroDocumento

        CargarComboGenero()
        Me.ddlGenero.SelectedValue = Idgenero
        'Carga de Ubigeo
        Me.SeleccionarPais(idPais)
        Me.SeleccionarDepartamento(idDepartamento)
        Me.SeleccionarCiudad(idCiudad)
        Me.ddlCiudad.SelectedValue = idCiudad
        Me.txtDireccion.Text = direccion
        Me.txtTelefono.Text = telefono
        RefrescarVista(fechaNacimiento)
    End Sub

#End Region

#Region "Ubicacion"
    Public Sub SeleccionarPais(ByVal IdPais As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        'Me.ddlPais.DataBind()
        If IdPais > 0 Then
            Me.ddlPais.SelectedValue = IdPais
        End If
    End Sub

    Public Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlPais.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlPais.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        ' Me.ddlDepartamento.DataBind()

        If IdDepartamento > 0 Then
            Me.ddlDepartamento.SelectedValue = IdDepartamento
        Else
            Me.ddlDepartamento.SelectedIndex = 0
        End If
    End Sub

    Public Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlDepartamento.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlDepartamento.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        ' Me.ddlCiudad.DataBind()
        If IdCiudad > 0 Then
            Me.ddlCiudad.SelectedValue = IdCiudad
        Else
            Me.ddlCiudad.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarDepartamento(0)
        SeleccionarCiudad(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlPais)
    End Sub

    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarCiudad(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlDepartamento)
    End Sub

#End Region


    Protected Sub ddlTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoDocumento.SelectedIndexChanged
        If ddlTipoDocumento.SelectedValue = "11" Then
            txtNumeroDocumento.MaxLength = 8
            If txtNumeroDocumento.Text.Length > 8 Then
                txtNumeroDocumento.Text = txtNumeroDocumento.Text.Substring(0, 8)
            End If
        Else
            txtNumeroDocumento.MaxLength = 11
        End If

    End Sub
#Region "propiedades"
    'Public _scriptManagerId As String
    'Public Property ScriptManagerId() As String
    '    Get
    '        Return _scriptManagerId
    '    End Get
    '    Set(ByVal value As String)
    '        _scriptManagerId = value
    '    End Set
    'End Property

    Public ReadOnly Property FechaMes() As DropDownList
        Get
            Return ddlFechaMes
        End Get
    End Property

    Public ReadOnly Property FechaAnio() As DropDownList
        Get
            Return ddlFechaAnio
        End Get
    End Property

#End Region

    Protected Sub Initialize(ByVal ddlFechaDias As DropDownList, ByVal ddlFechaMes As DropDownList, ByVal ddlFechaAnio As DropDownList)
        ddlFechaAnio = ddlFechaAnio
        ddlFechaMes = ddlFechaMes
        ddlFechaDias = ddlFechaDias
    End Sub

    Public Function DameFechaSeleccionada() As Date
        Return New Date(ddlFechaAnio.SelectedValue, ddlFechaMes.SelectedValue, ddlFechaDia.SelectedValue)
    End Function

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)

        MyBase.OnLoad(e)
        'If Not Page.IsPostBack Then
        '    GenerarAnio()
        '    GenerarMeses()
        '    GenerarDias()
        'End If
    End Sub

    Private Sub GenerarAnio()

        Dim anio As Integer = Now.Year
        ddlFechaAnio.Items.Clear()
        ddlFechaAnio.Items.Add(New ListItem("", ""))

        For i As Integer = 0 To 100
            ddlFechaAnio.Items.Add(New ListItem(anio - 1, anio - 1))
            anio = anio - 1
        Next

    End Sub

    Private Sub GenerarMeses()
        ddlFechaMes.Items.Clear()
        ddlFechaMes.Items.Add(New ListItem("", ""))

        Dim cultureInfo As CultureInfo = cultureInfo.CreateSpecificCulture("es-PE")
        Dim meses As String() = cultureInfo.DateTimeFormat.MonthNames()

        For i As Integer = 0 To 11
            ddlFechaMes.Items.Add(New ListItem(meses(i), i + 1))
        Next

    End Sub

    Private Sub GenerarDias()
        Dim diatemp As String = ddlFechaDia.SelectedValue
        'Date.
        ddlFechaDia.Items.Clear()
        ddlFechaDia.Items.Add(New ListItem("", ""))
        Dim anio As String = ddlFechaAnio.SelectedValue
        Dim mes As String = ddlFechaMes.SelectedValue
        Dim dias As Integer = 31
        If Not ((anio = "") Or (mes = "")) Then
            dias = Date.DaysInMonth(anio, mes)
        End If

        For i As Integer = 1 To dias
            ddlFechaDia.Items.Add(New ListItem(i, i))
        Next

        If Not ddlFechaDia.Items.FindByValue(diatemp) Is Nothing Then
            ddlFechaDia.SelectedValue = diatemp
        End If
    End Sub

    Public Function FechaEsValida() As Boolean
        Dim strfecha As Date
        Try
            strfecha = DameFechaSeleccionada()
        Catch ex As Exception
            Throw New FWBusinessException("El formato de la fecha no es valida", Nothing)
        End Try
        Return True
    End Function
    Public Sub RefrescarVista()
        GenerarAnio()
        GenerarMeses()
        GenerarDias()
    End Sub
    Public Sub RefrescarVista(ByVal fecha As Date)
        Try
            GenerarAnio()
            GenerarMeses()
            ddlFechaAnio.SelectedValue = fecha.Year
            ddlFechaMes.SelectedValue = fecha.Month
            GenerarDias()
            ddlFechaDia.SelectedValue = fecha.Day
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlFecha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        GenerarDias()
        Dim drop As DropDownList = CType(sender, DropDownList)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(drop)
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - Registrar Cliente"
        HyperLinkTeCoUs.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") & "/condiciones.aspx"
        HyperLinkPoliticaPriv.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") & "/politicas.aspx"
    End Sub
End Class
