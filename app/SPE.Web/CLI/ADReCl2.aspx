<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="ADReCl2.aspx.vb" Inherits="CLI_ADReCl2" Title="PagoEfectivo - Validar Registro de Usuario" %>

<%@ Register Src="../UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>

<asp:Content ID="ContentPlaceHolderHeaderJS1" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $('#liInicio a').attr('href', '../Default.aspx');
        $('#liPersonas a').attr('href', '../Personas.aspx');
        $('#liEmpresas a').attr('href', '../Empresas.aspx');
        $('#liCentroPago a').attr('href', '../CentrosPago.aspx');
        $('#liComerciosAfiliados a').attr('href', '../ComerciosAfiliados.aspx');
        $('#liContactenos a').attr('href', '../contactenos.aspx');
    });
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">
        <div class="content-welcome">
            <h3>
                Bienvenido, "<asp:Literal ID="lblNombre" runat="server"></asp:Literal>" a PagoEfectivo</h3>
            <p>
                <span class="info"></span>Tu cuenta PagoEfectivo est� casi lista para usar, solo
                tienes que revisar tu correo para activarla.</p>
            <div class="form-actions">
                <button type="button" class="btn btn-primary btn-large" onclick="window.location = '../Default.aspx'">
                    Cerrar</button>
            </div>
        </div>
    </div>
</asp:Content>
