﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COEqReg.aspx.vb" Inherits="CLI_COEqReg" Title="PagoEfectivo - Consultar Equipos Registrados" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Equipos Registrados</h2>
    <div class="conten_pasos3">
        <p class="separacion">
            Usted no recibir&aacute; mensajes de confirmaci&oacute;n cuando ingrese desde estos
            equipos:</p>
        <div class="cont_cel">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        &nbsp;<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label></div>
                    <h5>
                        <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal></h5>
                    <asp:HiddenField ID="hfSortDir" runat="server" />
                    <asp:GridView ID="gvResultado" runat="server" BackColor="White" DataKeyNames="IdEquipoRegistrado"
                        CssClass="grilla" AllowPaging="True" CellPadding="3" BorderWidth="1px" BorderStyle="None"
                        BorderColor="#CCCCCC" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging"
                        OnRowDataBound="gvResultado_RowDataBound" AllowSorting="True" PageSize="12" OnSorting="gvResultado_Sorting">
                        <Columns>
                            <asp:BoundField DataField="PcName" HeaderText="Equipo" SortExpression="PcName"></asp:BoundField>
                            <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha de Registro" SortExpression="FechaRegistro">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Eliminar Equipo">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Eliminar"
                                        OnClientClick="javascript:return confirm('¿Está seguro de eliminar el registro seleccionado?');">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
