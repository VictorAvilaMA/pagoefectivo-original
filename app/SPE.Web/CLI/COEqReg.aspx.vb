﻿Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Linq
Imports _3Dev.FW.Web

Partial Class CLI_COEqReg
    'Inherits System.Web.UI.Page
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CComun)

#Region "atributos"
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Dim listaBEEquipoRegistradoXusuario As New List(Of BEEquipoRegistradoXusuario)

#End Region


    Public Overrides Sub OnMainSearch()
        ListarEquiposRegistradosXUsuario()
    End Sub

    Public Overrides Function AllowToDelete(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As Boolean
        Return False
    End Function


    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeequiporegistradoxusuario As New BEEquipoRegistradoXusuario
        Dim oBEEU As New BEEquipoRegistradoXusuario()
        oBEEU = CType(Session("oBEEquipoRegistradoXusuarioVal"), BEEquipoRegistradoXusuario)

        obeequiporegistradoxusuario.Usuario = oBEEU.Usuario
        obeequiporegistradoxusuario.PropOrder = SortExpression
        obeequiporegistradoxusuario.TipoOrder = SortDir
        obeequiporegistradoxusuario.PageNumber = gvResultado.PageIndex
        obeequiporegistradoxusuario.PageSize = gvResultado.PageSize

        Return obeequiporegistradoxusuario
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        If Not Page.IsPostBack Then
            hfSortDir.Value = 0
        End If
        Try
            ListarEquiposRegistradosXUsuario()
        Catch ex As Exception

        End Try

        '
    End Sub

    Protected Sub gvResultado_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvResultado.RowDeleting

        Dim oCComun As New SPE.Web.CComun
        Dim oBEEquipoRegistradoXusuario As New BEEquipoRegistradoXusuario

        oBEEquipoRegistradoXusuario.IdEquipoRegistrado = CType(e.Keys("IdEquipoRegistrado").ToString(), Long)
        oCComun.EliminarEquipoRegistradoXusuario(oBEEquipoRegistradoXusuario)
        ListarEquiposRegistradosXUsuario()

    End Sub

    Private Sub ListarEquiposRegistradosXUsuario()
        '
        Try
            Dim objCComun As New SPE.Web.CComun

            Dim businessEntityObject As BEEquipoRegistradoXusuario
            businessEntityObject = CreateBusinessEntityForSearch()

            Dim SortDirhf As Integer
            SortDirhf = CInt(hfSortDir.Value)

            businessEntityObject.PageNumber = PageNumber
            businessEntityObject.PageSize = GridViewResult.PageSize
            businessEntityObject.TipoOrder = IIf(SortDirhf = SortDirection.Ascending, True, False)
            businessEntityObject.PropOrder = SortExpression
            GridViewResult.PageIndex = PageNumber - 1

            listaBEEquipoRegistradoXusuario = objCComun.ConsultarEquipoRegistradoXusuario(businessEntityObject)

            If listaBEEquipoRegistradoXusuario.Count() <> 0 Then


                Dim oObjectDataSource As New ObjectDataSource()

                oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID
                oObjectDataSource.EnablePaging = GridViewResult.AllowPaging
                oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
                oObjectDataSource.SelectMethod = "GetDataGrilla"
                oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
                oObjectDataSource.StartRowIndexParameterName = "filaInicio"
                oObjectDataSource.MaximumRowsParameterName = "maxFilas"
                oObjectDataSource.EnableViewState = False

                AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating
                GridViewResult.DataSource = oObjectDataSource
                GridViewResult.DataBind()

            Else
                GridViewResult.DataSource = Nothing
                GridViewResult.DataBind()

            End If

            If listaBEEquipoRegistradoXusuario.Count() <> 0 Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listaBEEquipoRegistradoXusuario.First().TotalPageNumbers)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If
            lblMensaje.Text = ""
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)

        End Try
        '
    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        If listaBEEquipoRegistradoXusuario.Count() <> 0 Then
            e.ObjectInstance = New TotalRegistrosTableAdapter(listaBEEquipoRegistradoXusuario, listaBEEquipoRegistradoXusuario.First().TotalPageNumbers)
        End If
    End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        '
        SortExpression = e.SortExpression
        Dim SortDirhf As Integer
        SortDirhf = CInt(hfSortDir.Value)
        If (SortDirhf.Equals(SortDirection.Ascending)) Then
            SortDirhf = SortDirection.Descending
            hfSortDir.Value = 1
        Else
            SortDirhf = SortDirection.Ascending
            hfSortDir.Value = 0
        End If
        ListarEquiposRegistradosXUsuario()
        '
    End Sub

    'Paginado de Grilla
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarEquiposRegistradosXUsuario()
        '
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvResultado.RowDataBound
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "Usuario", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

End Class
