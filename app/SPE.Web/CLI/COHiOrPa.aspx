<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COHiOrPa.aspx.vb" Inherits="CLI_COHiOrPa" Title="PagoEfectivo - Historial de Pagos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <h2>
        Historial de Pagos
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1" style="display:none"><span class="color">&gt;&gt;</span> Servicio :</li>
                    <li class="t2" style="display:none">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> CIP :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="normal" AutoPostBack="True"
                            OnTextChanged="txtNroOrdenPago_TextChanged"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" runat="server" FilterType="Numbers"
                            TargetControlID="txtNroOrdenPago">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span>Nro Orden Comercio :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtOrderComercio" runat="server" CssClass="normal"  MaxLength="64"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de movimiento del:</li>
                    <li class="t2" style="line-height: 1;z-index:200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDe" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaDe" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Del' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Del' no v�lida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:RequiredFieldValidator runat ="server"  SetFocusOnError="true" Enabled ="true"  ValidationGroup="GrupoValidacion" 
                            ControlToValidate="txtFechaA" Display="None" ErrorMessage="Campo Requerido" Visible="true" 
                                EnableClientScript="true" Text="*"></asp:RequiredFieldValidator>

                            <asp:ImageButton ID="ibtnFechaA" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaA" Display="None" Visible="true" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Al' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div class="clear">
                            </div>
                            <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>
                        </p>
                    </li>
                    <li style="padding-left: 33px; width: auto;">
                        <b>* El reporte mostrar&aacute; la informaci&oacute;n actualizada despu&eacute;s de 10 minutos.</b>
                    </li>
                    <li class="t1"><span class="color"></span></li>
                    <li class="t2">
                        <asp:ValidationSummary ID="ValidationSummary"  DisplayMode="List"   EnableClientScript="true"
                            runat="server" ShowSummary="true" Visible="true" />
                    </li>
                    <li class="complet" style="z-index:10;">
                        <asp:Button ID="btnBuscar" runat="server" CausesValidation="true" CssClass="input_azul5"
                            Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                        <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Text="Excel" />
                    </li>
                </ul>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" TargetControlID="txtFechaDe"
                    Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" TargetControlID="txtFechaA"
                    Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaDe"
                    Format="dd/MM/yyyy" PopupButtonID="ibtnFechaDe">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaA"
                    Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA">
                </cc1:CalendarExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                <asp:PostBackTrigger ControlID="btnExportarExcel" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" class="result"
            style="clear: both">
            <ContentTemplate>
                <div id="divgrilla" visible="false" runat="server" class="cont_cel" style="overflow:auto">
                    <h5>
                        <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                    </h5>
                    <div class="clear">
                    </div>
                    <asp:GridView ID="grdResultado" runat="server" BackColor="White" AllowPaging="True"
                        AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" OnRowDataBound="grdResultado_RowDataBound" CssClass="grilla"
                        EnableTheming="True" AllowSorting="True" PageSize="20" Width="720">
                        <Columns>
                            <asp:BoundField DataField="DescripcionServicio" HeaderText="Servicio" SortExpression="DescripcionServicio">
                                <ItemStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="N&#250;mero C.I.P." ItemStyle-HorizontalAlign="Center"
                                SortExpression="NumeroOrdenPago">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkNumeroOrdenPago" runat="server" CommandName="Detalle" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                        ToolTip="Ver informaci�n" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOrdenPago")%>'
                                        CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                    <asp:HiddenField ID="hdfNumeroOrdenPago" runat="server" Value='<%# Eval("NumeroOrdenPago") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="OrderIdComercio" HeaderText="Nro.Ord.Comercio" SortExpression="OrderIdComercio">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Total" SortExpression="Total" >
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "SimboloMoneda") + " " + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" ForeColor="Red" Font-Bold="True" ></ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Fec.Emision" SortExpression="FechaEmision">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEmision"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fec.Cancelacion" SortExpression="FechaCancelacion">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaCancelacion"))%>
                                </ItemTemplate>
                            </asp:TemplateField>

                           <%-- <asp:TemplateField HeaderText="Fec.Anulada" SortExpression="FechaAnulada">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaAnulada"))%>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Fecha A Expirar" SortExpression="FechaExpirada">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaExpirada"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <%--  <asp:TemplateField HeaderText="Fec.Eliminado" SortExpression="FechaEliminado">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEliminado"))%>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="grdResultado" EventName="PageIndexChanging">
                </asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--    <div class="dlgrid inner-col-right">
        <h3>
            Criterios de b�squeda</h3>
        <div class="cont-panel-search-big" style="width: 520px;">
            <asp:UpdatePanel ID="UpdatePanelOP" runat="server" UpdateMode="Conditional" class="even w100 clearfix dlinline">
                <ContentTemplate>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
      
    </div>--%>
</asp:Content>
