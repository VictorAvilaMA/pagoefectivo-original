Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Web

Partial Class CLI_COHiOrPa
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return grdResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    'add <PeruCom FaseIII>
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Private Property ActualIdEmpresaContratante() As Integer
        Get
            If (ViewState("IdEmpresaContratante") Is Nothing) Then
                ViewState("IdEmpresaContratante") = 0
            End If
            Return ViewState("IdEmpresaContratante")
        End Get
        Set(ByVal value As Integer)
            ViewState("IdEmpresaContratante") = value
        End Set
    End Property


    'Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
    '    Get
    '        Return lblResultado
    '    End Get
    'End Property


    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.ExecuteSearchOnFirstLoad = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (Page.IsPostBack) Then
            'divgrilla.Visible = False
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNroOrdenPago)
            CargarDatos()
            If Session("obeOrdenPagoSearch") IsNot Nothing Then
                CreateSearchOfBusiness(CType(Session("obeOrdenPagoSearch"), BEOrdenPago))
                Session.Remove("obeOrdenPagoSearch")
            End If
        End If
    End Sub

    'CARGA DE DATOS
    Private Sub CargarDatos()
        Using objParametro As New SPE.Web.CAdministrarParametro
            'ESTADO Código de Identificación de Pago
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoOrdenPago), "Descripcion", "Id", "::: TODOS :::")
        End Using
        Using objCAdministrarServicio As New SPE.Web.CServicio
            Dim obeServicio As New BEServicio
            obeServicio.IdUsuarioCreacion = 0
            Dim listBEServicio As List(Of BEServicio) = objCAdministrarServicio.ConsultarServiciosPorIdUsuario(obeServicio)
            If (listBEServicio.Count > 0) Then
                ActualIdEmpresaContratante = listBEServicio(0).IdEmpresaContratante
            End If
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, listBEServicio, "Nombre", "IdServicio", "::: TODOS :::")
        End Using
        InicializarFecha()
        txtNroOrdenPago.Focus()
    End Sub

    Private Sub InicializarFecha()
        'FECHAS
        txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaA.Text = Today.ToShortDateString
    End Sub

    'VALIDAMOS LAS FECHAS
    Private Function ValidarFecha() As Boolean
        If Not IsDate(txtFechaA.Text) OrElse _
            Not IsDate(txtFechaDe.Text) Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Overrides Sub OnClear()

        txtNroOrdenPago.Text = String.Empty
        ddlServicio.SelectedIndex = 0
        grdResultado.DataSource = Nothing
        grdResultado.DataBind()
        ddlEstado.SelectedIndex = 0
        InicializarFecha()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        txtOrderComercio.Text = String.Empty
        MyBase.OnClear()
        divgrilla.Visible = False
    End Sub

    ''LIMPIAR
    'Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click

    'End Sub

    ''PAGINADO DE LA GRILLA
    'Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    grdResultado.PageIndex = e.NewPageIndex
    '    ListarOrdenesDePago()
    'End Sub

    Private Function ObtenerMensajeResultado(ByVal cantidad As Integer) As String
        Dim strResultado As New StringBuilder()
        strResultado.Append(String.Format("Resultados: {0} registro(s) ", cantidad))
        'If (ddlServicio.SelectedIndex > 0) Then
        '    strResultado.Append(String.Format("para el servicio {0} ", ddlServicio.SelectedItem.Text))
        'End If
        'strResultado.AppendLine(String.Format("entre el {0} y {1} ", txtFechaDe.Text.TrimEnd(), txtFechaA.Text.TrimEnd()))
        Return strResultado.ToString()
    End Function
    Public Overrides Function AllowToSearch() As Boolean
        Return ValidarFecha() And MyBase.AllowToSearch()
    End Function
    'BUSCAR
    'Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
    '    '
    '    If ValidarFecha() = True Then
    '        '
    '        ListarOrdenesDePago()
    '        '
    '    End If
    '    '
    'End Sub

    'LISTAR ORDENES DE PAGO

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeOrdenPago As New BEOrdenPago
        Dim oFuncion As New CCliente
        Dim oParametro As New BECliente


        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
        obeOrdenPago.IdEmpresa = 0
        obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
        obeOrdenPago.IdCliente = UserInfo.IdUsuario
        If ddlEstado.SelectedIndex = 0 Then obeOrdenPago.IdEstado = 0 Else obeOrdenPago.IdEstado = StringToInt(ddlEstado.SelectedValue)
        obeOrdenPago.FechaCreacion = StringToDateTime(txtFechaDe.Text)
        obeOrdenPago.FechaActualizacion = StringToDateTime(txtFechaA.Text)
        obeOrdenPago.OrderIdComercio = txtOrderComercio.Text
        obeOrdenPago.OrderBy = SortExpression
        obeOrdenPago.IsAccending = SortDir


        Return obeOrdenPago
    End Function
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            'upd <PeruCom FaseIII>
            lblResultado.Text = ObtenerMensajeResultado(If(ResultList.Count > 0, ResultList.First().TotalPageNumbers, 0))
        Else
            lblResultado.Text = ObtenerMensajeResultado(0)
        End If
        divgrilla.Visible = True
    End Sub


    Protected Sub grdResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdResultado.RowDataBound
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "DescripcionEstado", _
        "Anulada", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)

    End Sub



    Protected Sub txtNroOrdenPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If Right("00000000000000" + txtNroOrdenPago.Text, 14) = "00000000000000" Then
            txtNroOrdenPago.Text = ""
        Else
            txtNroOrdenPago.Text = Right("00000000000000" + txtNroOrdenPago.Text, 14)
        End If

    End Sub
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click

        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim dtfechainicio As DateTime = StringToDateTime(txtFechaDe.Text)
            Dim dtfechafin As DateTime = StringToDateTime(txtFechaA.Text)
            Dim strFechaInicio As String = String.Format("{0}-{1}-{2}", dtfechainicio.Year, dtfechainicio.Month, dtfechainicio.Day)
            Dim strFechaFin As String = String.Format("{0}-{1}-{2}", dtfechafin.Year, dtfechafin.Month, dtfechafin.Day)
            Dim idestado As Integer = 0

            If ddlEstado.SelectedIndex = 0 Then idestado = 0 Else idestado = StringToInt(ddlEstado.SelectedValue)
            'Response.Redirect(String.Format("~/Reportes/RptCOCIPDet.aspx?
            'CIP={0}&IdS={1}&C={2}&E={3}&IdE={4}&FIni={5}&FFin={6}&Com={7}&flag={8}&stt={9}", 
            'txtNroOrdenPago.Text.TrimEnd(), 
            'StringToInt(ddlServicio.SelectedValue), 
            'UserInfo.IdUsuario.ToString, 
            'idestado, 0, strFechaInicio, strFechaFin, txtOrderComercio.Text, 1, ddlEstado.SelectedItem))

            Dim listaOrdenPublicacionBase As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Dim listaOrdenPublicacion As New List(Of BEOrdenPago)
            Using cOrdenPago As New COrdenPago()
                Dim obeOrdenPago As New BEOrdenPago
                obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
                obeOrdenPago.IdEmpresa = 0
                obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
                obeOrdenPago.IdCliente = UserInfo.IdUsuario.ToString
                obeOrdenPago.IdEstado = idestado
                obeOrdenPago.FechaCreacion = StringToDateTime(strFechaInicio)
                obeOrdenPago.FechaActualizacion = StringToDateTime(strFechaFin)
                obeOrdenPago.OrderIdComercio = txtOrderComercio.Text
                listaOrdenPublicacionBase = cOrdenPago.SearchByParameters(obeOrdenPago)
            End Using
            For index = 0 To listaOrdenPublicacionBase.Count - 1
                listaOrdenPublicacion.Add(CType(listaOrdenPublicacionBase(index), BEOrdenPago))
            Next

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", strFechaInicio))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", strFechaFin))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", ddlEstado.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("CIP", IIf(String.IsNullOrEmpty(txtNroOrdenPago.Text), " - ", txtNroOrdenPago.Text)))

            UtilReport.ProcesoExportarGenerico(listaOrdenPublicacion, Page, "EXCEL", "xls", "Consulta CIP - ", parametros, "RptCOCIPDet.rdlc")

        End If

    End Sub
    Protected Sub grdResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdResultado.RowCommand
        Try
            Dim reg As Integer
            Dim NroOP As String
            reg = grdResultado.Rows(e.CommandArgument).RowIndex
            NroOP = CType(grdResultado.Rows(reg).FindControl("hdfNumeroOrdenPago"), HiddenField).Value
            VariableTransicion = NroOP & "|" & "4"
            Session("obeOrdenPagoSearch") = CreateBusinessEntityForSearch()
            Select Case e.CommandName
                Case "Detalle"
                    Response.Redirect("~/ECO/DECIP.aspx")
            End Select
        Catch ex As Exception

        End Try


    End Sub
    Private Sub CreateSearchOfBusiness(ByVal obeOrdenPago As BEOrdenPago)
        txtNroOrdenPago.Text = obeOrdenPago.NumeroOrdenPago
        ActualIdEmpresaContratante = obeOrdenPago.IdEmpresa
        If obeOrdenPago.IdServicio = 0 Then ddlServicio.SelectedIndex = 0 Else ddlServicio.SelectedValue = obeOrdenPago.IdServicio.ToString()
        
        If obeOrdenPago.IdEstado = 0 Then ddlEstado.SelectedIndex = 0 Else ddlEstado.SelectedValue = obeOrdenPago.IdEstado.ToString()
        txtFechaDe.Text = obeOrdenPago.FechaCreacion.ToString("dd/MM/yyyy")
        txtFechaA.Text = obeOrdenPago.FechaActualizacion.ToString("dd/MM/yyyy")
        txtOrderComercio.Text = obeOrdenPago.OrderIdComercio
        SortExpression = obeOrdenPago.OrderBy
        SortDir = obeOrdenPago.IsAccending
        BtnSearch_Click(Nothing, Nothing)
    End Sub
End Class
