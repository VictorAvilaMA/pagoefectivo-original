Imports System.Data
Imports SPE.Web.COrdenPago
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.IO
Imports System.Drawing.Text
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Web.Security
Imports SPE.Utilitario
Imports System.Xml

Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports _3Dev.FW.Web


Partial Class CLI_PRGeOrPa
    Inherits System.Web.UI.Page

    Private WebMessage As WebMessage = Nothing
    Private Const __Respuesta As String = "__Respuesta"
    Private Const __UrlServicio As String = "__UrlServicio"

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        WebMessage = New WebMessage(lblMensaje)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then
                Dim xmlTrama As String = ""
                Dim urlServicio As String = ""

                lnkVolverServicio.Visible = False
                If Not Context.User Is Nothing Then
                    If Context.User.IsInRole(RolCliente) = False Then
                        Response.Redirect("~/Default.aspx")
                    End If
                End If
                xmlTrama = ObjectToString(Session(SPE.Web.Util.UtilGenCIP.__UrlTrama))
                urlServicio = ObjectToString(Session(SPE.Web.Util.UtilGenCIP.__UrlServicio))

                Session(SPE.Web.Util.UtilGenCIP.__UrlTrama) = Nothing
                Session(SPE.Web.Util.UtilGenCIP.__UrlServicio) = Nothing


                If urlServicio <> "" Then
                    Dim oberesponse As BEObtCIPInfoByUrLServResponse = Session(SPE.Web.Util.UtilGenCIP.__XMLObtCIPInfoByUrLServRequest)
                    If (oberesponse Is Nothing) Then
                        Using ocOrdenPago As New SPE.Web.COrdenPago()
                            Dim oberequest As New BEObtCIPInfoByUrLServRequest()
                            oberequest.UrlServicio = urlServicio
                            oberequest.Trama = xmlTrama
                            oberequest.CargarTramaSolicitud = True
                            oberequest.TipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Request
                            oberesponse = ocOrdenPago.ObtenerCIPInfoByUrLServicio(oberequest)
                        End Using
                    End If

                    If (oberesponse IsNot Nothing) Then
                        If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.ServicioNoExiste) Then
                            Response.Redirect("~/ServicioNoEncontrado.aspx")
                        End If

                        If oberesponse.Servicio.IdServicio < 1 OrElse oberesponse.Servicio.IdEmpresaContratante < 1 Then
                            imgEmpresaContratante.ImageUrl = ""
                            imgServicio.ImageUrl = ""
                        Else

                            imgEmpresaContratante.ImageUrl = String.Format("~/ADM/ADEmCoImg.aspx?empresaId={0}", ObjectToString(oberesponse.OcultarEmpresa.idEmpresaContratante))
                            imgServicio.ImageUrl = String.Format("~/ADM/ADServImg.aspx?servicioid={0}", ObjectToString(oberesponse.OcultarEmpresa.idServicio))

                            If Not oberesponse.OcultarEmpresa Is Nothing Then
                                trVerEmpresa.Visible = Not oberesponse.OcultarEmpresa.OcultarEmpresa


                                imgEmpresaContratante.Visible = Not oberesponse.OcultarEmpresa.OcultarImagenEmpresa
                                lblEmpresaContratanteTexto.Text = oberesponse.OcultarEmpresa.EmpresaContratanteDescripcion
                                lblEmpresaContratanteTexto.Visible = oberesponse.OcultarEmpresa.OcultarImagenEmpresa
                                imgServicio.Visible = Not oberesponse.OcultarEmpresa.OcultarImagenServicio
                                lblServicioTexto.Text = oberesponse.OcultarEmpresa.ServicioDescripcion
                                lblServicioTexto.Visible = oberesponse.OcultarEmpresa.OcultarImagenServicio

                            End If
                            If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.CodigoMonedaIncorrecto) Then
                                WebMessage.ShowErrorMessage(oberesponse.Message)
                                Throw New Exception(oberesponse.Message)
                            End If
                            If (oberesponse.OrdenPago IsNot Nothing) Then
                                lblEmpresaContratanteTexto.Text = oberesponse.OrdenPago.DescripcionEmpresa
                                lblMonto.Text = oberesponse.Moneda.Simbolo + "  " + Convert.ToDecimal(oberesponse.OrdenPago.Total).ToString("#,#0.00")
                                lblConcepto.Text = oberesponse.OrdenPago.ConceptoPago
                                ViewState(__Respuesta) = oberesponse
                                ViewState(__UrlServicio) = urlServicio
                                btnImprimir2.Visible = False
                            End If
                        End If
                    End If
                Else
                    Response.Redirect("~/ServicioNoEncontrado.aspx")
                End If
            End If

        Catch ex3 As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex3)
            WebMessage.Initialize(lblMensaje)
            WebMessage.ShowErrorMessage(ex3.Message, False)
        End Try
    End Sub

    Private Sub GenerarOrdenPago()
        Dim obeResGenCIP As BEGenCIPResponse = Nothing
        Try
            Dim obeRespuesta As BEObtCIPInfoByUrLServResponse = ViewState(__Respuesta)
            If (obeRespuesta IsNot Nothing) Then
                Dim esquemaTrama As EsquemaTrama = obeRespuesta.EsquemaTrama
                If (esquemaTrama IsNot Nothing) Then
                    lnkVolverServicio.NavigateUrl = esquemaTrama.Home
                    lnkVolverServicio.Text = String.Format("Volver al Servicio ({0})", obeRespuesta.OcultarEmpresa.ServicioDescripcion)
                    lnkVolverServicio.Visible = True
                End If

                Using oCOrdenPago As New SPE.Web.COrdenPago
                    Dim request As New BEGenCIPRequest()
                    request.OrdenPago = obeRespuesta.OrdenPago
                    request.OrdenPago.IdUsuarioCreacion = UserInfo.IdUsuario
                    request.OrdenPago.EmailANotifGeneracion = UserInfo.Email
                    request.OrdenPago.IdEmpresa = obeRespuesta.OcultarEmpresa.idEmpresaContratante
                    request.OrdenPago.IdServicio = obeRespuesta.OcultarEmpresa.idServicio

                    obeResGenCIP = oCOrdenPago.GenerarCIP(request)
                    If (obeResGenCIP IsNot Nothing) Then
                        obeRespuesta.OrdenPago.IdOrdenPago = obeResGenCIP.IdOrdenPago
                        obeRespuesta.OrdenPago.NumeroOrdenPago = obeResGenCIP.NumeroOrdenPago
                        If (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGeneracionOk) Then
                            btnImprimir2.Attributes("onclick") = SPE.Web.Util.UtilGenCIP.GenerarUrlStrParaVerPopupCIP(request.OrdenPago, obeRespuesta.OcultarEmpresa, ObjectToString(ViewState("__urlServicio")))
                            btnImprimir2.Visible = True
                            btnGenerar.Visible = False
                            imgCodeBar.Src = "ADOPImg.aspx?numero=" + obeResGenCIP.NumeroOrdenPago
                            lblTitulo.Text = "Código de Identificación de Pago generada"
                            lblNumeroOrdenPago.Text = obeResGenCIP.NumeroOrdenPago
                            divGenerarOrdenPago.Visible = True
                            WebMessage.Initialize(lblMensaje)
                            WebMessage.ShowSuccessfullyMessage(obeResGenCIP.Message, False)
                        ElseIf (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnterior) Then
                            Me.lblNumeroOrdenPago.Text = obeResGenCIP.NumeroOrdenPago
                            Me.divGenerarOrdenPago.Visible = True
                            Me.btnGenerar.Visible = False
                            imgCodeBar.Src = "ADOPImg.aspx?numero=" + obeResGenCIP.NumeroOrdenPago
                            lblNumeroOrdenPago.Text = obeResGenCIP.NumeroOrdenPago
                            divGenerarOrdenPago.Visible = True
                            ' datos de query string 
                            Dim sbqueryString As New StringBuilder
                            sbqueryString.Append("javascript:AbrirPopPup('PrImOrPaPopPup.aspx?idOrdenPago=" + DataUtil.IntToString(obeResGenCIP.IdOrdenPago) + "','Vista_Previa_De_Impresion',450,550);")
                            btnImprimir2.Attributes("onclick") = sbqueryString.ToString
                            btnImprimir2.Visible = True
                            ''
                            WebMessage.Initialize(lblMensaje)
                            WebMessage.ShowSuccessfullyMessage(obeResGenCIP.Message, False)
                        ElseIf (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorOtroUsuario) Then
                            WebMessage.Initialize(lblMensaje)
                            WebMessage.ShowErrorMessage(obeResGenCIP.Message, False)
                        End If
                    End If

                End Using

            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            lblMensaje.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
            lblMensaje.CssClass = "MensajeValidacion"
        End Try

    End Sub

    Function FormatearNumeroOrdenPago(ByVal numeroOrden As Integer) As String
        Return Right("00000000000000" + numeroOrden.ToString, 14)
    End Function

    Protected Sub btnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        GenerarOrdenPago()
    End Sub

End Class
