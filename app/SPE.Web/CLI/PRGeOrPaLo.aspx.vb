Imports System.Data
Imports SPE.Entidades
Imports SPE.Utilitario
Imports SPE.EmsambladoComun

Imports _3Dev.FW.Util
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Util.DataUtil

Partial Class CLI_PRGeOrPaLo
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CServicio)

    Private WebMessage As WebMessage = Nothing

    Private Sub ConfigureLoginManager()
        Dim _speLogin As New SPELogin(Login1)
        AddHandler _speLogin.AfterLoggedInEventHandler, AddressOf AfterLoggedIn
    End Sub
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        ConfigureLoginManager()
        WebMessage = New WebMessage(lblMensaje)
    End Sub
    Protected Sub AfterLoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Login1.DestinationPageUrl = "PRGeCIP.aspx?lg=2"
        Catch ex As Exception
            'Logger.LogException(ex)
            WebMessage.ShowErrorMessage(ex, True)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                Dim strTrama As String = Nothing
                Dim urlServicio As String = Nothing
                Dim urlservoptemp As String = System.Configuration.ConfigurationManager.AppSettings("UrlReqServicioGenerarOP")

                If Session(SPE.Web.Util.UtilGenCIP.__UrlTramaPre) Is Nothing Then
                    If (urlservoptemp <> "") Then
                        urlServicio = urlservoptemp
                    Else

                        If ((Request Is Nothing) Or (Request.UrlReferrer Is Nothing) Or (Request.UrlReferrer.AbsoluteUri = Nothing)) Then
                            urlServicio = ""
                            strTrama = ""
                        Else
                            urlServicio = _3Dev.FW.Web.WebUtil.GetAbsoluteUriWithOutQuery(Request.UrlReferrer)
                            strTrama = Request.RawUrl.ToString
                        End If
                    End If
                Else
                    strTrama = Session(SPE.Web.Util.UtilGenCIP.__UrlTramaPre)
                    urlServicio = Session(SPE.Web.Util.UtilGenCIP.__UrlServicioPre)
                End If
                Session(SPE.Web.Util.UtilGenCIP.__UrlTramaPre) = Nothing
                Session(SPE.Web.Util.UtilGenCIP.__UrlServicioPre) = Nothing
                Session(SPE.Web.Util.UtilGenCIP.__UrlTrama) = strTrama
                Session(SPE.Web.Util.UtilGenCIP.__UrlServicio) = urlServicio

                Dim oberesponse As BEObtCIPInfoByUrLServResponse = Nothing
                Using ocOrdenPago As New SPE.Web.COrdenPago()
                    Dim oberequest As New BEObtCIPInfoByUrLServRequest()
                    oberequest.UrlServicio = urlServicio
                    oberequest.Trama = strTrama
                    oberequest.CargarTramaSolicitud = True
                    oberequest.TipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Request
                    oberesponse = ocOrdenPago.ObtenerCIPInfoByUrLServicio(oberequest)
                End Using

                If (oberesponse IsNot Nothing) Then
                    Session(SPE.Web.Util.UtilGenCIP.__XMLObtCIPInfoByUrLServRequest) = oberesponse
                    If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.ServicioNoExiste) Then
                        Response.Redirect("~/ServicioNoEncontrado.aspx")
                    End If

                    If oberesponse.Servicio.IdServicio < 1 OrElse oberesponse.Servicio.IdEmpresaContratante < 1 Then
                        imgEmpresaContratante.ImageUrl = ""
                        imgServicio.ImageUrl = ""
                    Else

                        imgEmpresaContratante.ImageUrl = String.Format("~/ADM/ADEmCoImg.aspx?empresaId={0}", ObjectToString(oberesponse.OcultarEmpresa.idEmpresaContratante))
                        imgServicio.ImageUrl = String.Format("~/ADM/ADServImg.aspx?servicioid={0}", ObjectToString(oberesponse.OcultarEmpresa.idServicio))

                        If Not oberesponse.OcultarEmpresa Is Nothing Then
                            trVerEmpresa.Visible = Not oberesponse.OcultarEmpresa.OcultarEmpresa

                            imgEmpresaContratante.Visible = Not oberesponse.OcultarEmpresa.OcultarImagenEmpresa
                            lblEmpresaContratanteTexto.Text = oberesponse.OcultarEmpresa.EmpresaContratanteDescripcion
                            lblEmpresaContratanteTexto.Visible = oberesponse.OcultarEmpresa.OcultarImagenEmpresa
                            imgServicio.Visible = Not oberesponse.OcultarEmpresa.OcultarImagenServicio
                            lblServicioTexto.Text = oberesponse.OcultarEmpresa.ServicioDescripcion
                            lblServicioTexto.Visible = oberesponse.OcultarEmpresa.OcultarImagenServicio
                        End If
                        If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.CodigoMonedaIncorrecto) Then
                            WebMessage.ShowErrorMessage(oberesponse.Message)
                            Throw New Exception(oberesponse.Message)
                        End If
                        If (oberesponse.OrdenPago IsNot Nothing) Then
                            lblEmpresaContratanteTexto.Text = oberesponse.OcultarEmpresa.EmpresaContratanteDescripcion
                            lblMonto.Text = oberesponse.Moneda.Simbolo + "  " + Convert.ToDecimal(oberesponse.OrdenPago.Total).ToString("#,#0.00")
                            lblConcepto.Text = oberesponse.OrdenPago.ConceptoPago
                        End If
                    End If

                    CType(Login1.FindControl("CreateUserLink"), HyperLink).NavigateUrl = "ADReCl.aspx?datosEnc=" + "1" '+ "&" & sbparametersUrl.ToString

                    If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.MontoTotalOPMenorQueCero) Then
                        'lblMensaje.Text = "El monto no puede ser menor o igual a cero. Intente nuevamente."
                        lblMensaje.Text = oberesponse.Message
                        lblMensaje.CssClass = "MensajeValidacion"

                    Else

                        If (User.Identity.IsAuthenticated) Then
                            'Response.Redirect(Login1.DestinationPageUrl)
                            System.Web.Security.FormsAuthentication.SignOut()
                        End If

                        Dim deValidaRegs As String = HttpContext.Current.Request.QueryString("vr")
                        If (deValidaRegs = "1") Then
                            lblMensaje.Text = "Su cuenta ya ha sido activada. Usted estuvo realizando una Código de Identificación de Pago. Para continuar con la operación, necesita ingresar al sistema con su cuenta."

                            lblMensaje.Visible = True
                        Else
                            lblMensaje.Text = ""
                            lblMensaje.Visible = False

                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Login1.Visible = False
            WebMessage.Initialize(lblMensaje)
            WebMessage.ShowErrorMessage(ex.Message, False)
        End Try

    End Sub

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property


End Class

