<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PgPrl.aspx.vb" Inherits="CLI_PgPrl" Title="PagoEfectivo - Página Principal" %>

<%@ OutputCache VaryByParam="None" Duration="10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner-col-right">
        <div style="width: 740px; float: left;">
            <h2 style="width: 500px; float: left;">
                Mis Pagos Pendientes</h2>
            <h3 style="width: 240px; float: left; margin: 25px 0 0 0 !important;">
                <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal></h3>
        </div>
        <div style="clear: both; height: 1px;">
        </div>
        <div>
        </div>
        <div class="result">
            <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
                EnableScriptGlobalization="True">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView DataKeyNames="IdOrdenPago" ID="gvResultado" CssClass="grilla" runat="server"
                        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="IdOrdenPago" HeaderText="IdOrdenPago" Visible="False">
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Servicio" SortExpression="DescripcionServicio" DataField="DescripcionServicio">
                                <ItemStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Concepto Pago" DataField="ConceptoPago" SortExpression="ConceptoPago"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Fec. Generaci&#243;n." SortExpression="FechaEmision"
                                DataField="FechaEmision" ItemStyle-HorizontalAlign="center">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Nro Orden" SortExpression="NumeroOrdenPago" DataField="NumeroOrdenPago"
                                Visible="false">
                                <ItemStyle Width="40px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="N&#250;mero C.I.P." ItemStyle-HorizontalAlign="Center"
                                SortExpression="NumeroOrdenPago">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkNumeroOrdenPago" runat="server" CommandName="Detalle" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                        ToolTip="Ver información" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOrdenPago")%>'
                                        CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' Font-Bold="False"
                                        Font-Underline="True" />
                                    <%--                                <asp:HiddenField ID="hdfNumeroOrdenPago" runat="server" Value='<%# Eval("NumeroOrdenPago") %>' />
                                    --%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Monto" SortExpression="Total">
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "SimboloMoneda") + " " + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" ForeColor="Red" Font-Bold="True"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Imprimir">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandArgument='<%# Eval("IdOrdenPago") %>'
                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/impresora.png" %>' OnCommand="ImageButton1_Command"
                                        CommandName="Edit" />
                                </ItemTemplate>
                                <ItemStyle Width="5px" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagerStyle" />
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:HiddenField ID="hfSortExpression" runat="server" Value="" />
                    <asp:HiddenField ID="hfSortDireccion" runat="server" Value="1" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        // JScript File
        function AbrirPopPup(url, titulo, width, height) {
            //window.open(url,titulo,"width="+width+", height="+ height + ", scrollbars=no,menubar=no,location=no,resizable=no")
            window.open(url, titulo, "width=" + width + ", height=" + height + ", scrollbars=no,menubar=no,location=no,resizable=no");
        }
    </script>
</asp:Content>
