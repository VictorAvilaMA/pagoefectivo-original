Imports System.Data
Imports System.Collections.Generic
Imports System.Linq

Partial Class CLI_PgPrl
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            CargarOrdenesPendientes()

        End If

    End Sub

    Private _ResultList As New List(Of SPE.Entidades.BEOrdenPago)
    Public Property ResultadoList() As List(Of SPE.Entidades.BEOrdenPago)
        Get
            Return _ResultList
        End Get
        Set(ByVal value As List(Of SPE.Entidades.BEOrdenPago))
            _ResultList = value
        End Set
    End Property



    Sub CargarOrdenesPendientes()
        'Dim obeOrdenPago As New SPE.Entidades.BEOrdenPago
        'obeOrdenPago.IdCliente = UserInfo.IdUsuario
        'Dim objCOrdenPago As New SPE.Web.COrdenPago
        'Dim resultado As List(Of SPE.Entidades.BEOrdenPago) = objCOrdenPago.ConsultarOrdenPagoPendientes(obeOrdenPago, SortExpression, SortDir = SortDirection.Ascending)
        'Me.gvResultado.DataSource = resultado
        'Me.gvResultado.DataBind()
        'SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, resultado.Count)

        Dim obeOrdenPago As New SPE.Entidades.BEOrdenPago
        obeOrdenPago.IdCliente = UserInfo.IdUsuario
        obeOrdenPago.PageSize = gvResultado.PageSize
        obeOrdenPago.PageNumber = gvResultado.PageIndex + 1
        obeOrdenPago.PropOrder = hfSortExpression.Value
        obeOrdenPago.TipoOrder = IIf(hfSortDireccion.Value = "1", True, False)
        Dim objCOrdenPago As New SPE.Web.COrdenPago
        Me.ResultadoList = objCOrdenPago.ConsultarOrdenPagoPendientes(obeOrdenPago, SortExpression, SortDir = SortDirection.Ascending)
        Dim oObjectDataSource As New ObjectDataSource()
        oObjectDataSource.ID = "oObjectDataSource_" + gvResultado.ID
        oObjectDataSource.EnablePaging = gvResultado.AllowPaging
        oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
        oObjectDataSource.SelectMethod = "GetDataGrilla"
        oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
        oObjectDataSource.StartRowIndexParameterName = "filaInicio"
        oObjectDataSource.MaximumRowsParameterName = "maxFilas"
        oObjectDataSource.EnableViewState = False

        AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating
        'If gvResultado.PagerSettings IsNot Nothing Then
        '    gvResultado.PagerSettings.Mode = PagerButtons.NumericFirstLast
        '    gvResultado.PagerSettings.FirstPageText = "Primero"
        '    gvResultado.PagerSettings.LastPageText = "Ultimo"
        'End If
        gvResultado.DataSource = oObjectDataSource
        gvResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, If(ResultadoList.Count = 0, 0, ResultadoList.First().TotalPageNumbers))
    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        Dim TotalGeneral As Int32
        TotalGeneral = If(ResultadoList.Count = 0, 0, ResultadoList.First().TotalPageNumbers)
        e.ObjectInstance = New _3Dev.FW.Web.TotalRegistrosTableAdapter(ResultadoList, TotalGeneral)
    End Sub





    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCliente})
    'End Sub
    'Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
    'End Sub
    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    'End Sub

    Protected Sub ImageButton1_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim idOrdenPago As String = e.CommandArgument.ToString
        'Response.Redirect("PrImOrPaPopPup.aspx?idOrdenPago=" + idOrdenPago)
        Dim queryString As String = "<script>function Abrirpoppup() { window.open('PrImOrPaPopPup.aspx?idOrdenPago=" + idOrdenPago + "','Vista_Previa_De_Impresion','width=600, height=550, scrollbars=no,menubar=no,location=no,resizable=no') } Abrirpoppup(); </script>"
        ScriptManager.RegisterStartupScript(Page, Me.GetType(), "AbrirPopupScript", queryString, False)


    End Sub
    Protected Sub gvResultado_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvResultado.RowEditing
        e.NewEditIndex = -1
    End Sub
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvResultado.PageIndexChanging
        Me.gvResultado.PageIndex = e.NewPageIndex
        CargarOrdenesPendientes()
    End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvResultado.Sorting
        hfSortExpression.Value = e.SortExpression
        If (hfSortDireccion.Value = "0") Then
            hfSortDireccion.Value = "1"
        Else
            hfSortDireccion.Value = "0"
        End If
        CargarOrdenesPendientes()
    End Sub
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, If(ResultList.Count = 0, 0, ResultList.First().TotalPageNumbers))
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        End If
    End Sub
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Public Overrides Sub OnGoToMaintenanceEditPage(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        e.NewEditIndex = -1
    End Sub
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Try
            Dim NroOP As String
            Dim index As Integer
            If Integer.TryParse(e.CommandArgument, index) Then
                NroOP = CType(gvResultado.Rows(index).Cells(4).FindControl("lnkNumeroOrdenPago"), LinkButton).Text

                VariableTransicion = NroOP & "|" & "5"
                Select Case e.CommandName
                    Case "Detalle"
                        Response.Redirect("~/ECO/DECIP.aspx")
                End Select
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class
