<%@ Page Language="VB" Async="true"  MasterPageFile="~/MasterPageBase.master" AutoEventWireup="false" CodeFile="PrGeOrPS.aspx.vb" Inherits="CLI_PrGeOrPS" title="Generar Código de Identificación de Pago" %>
<%@ PreviousPageType VirtualPath="~/CLI/PrGeOrPSLog.aspx"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <div id="Page" >
        <h1 class="tituLoGenerarOrdenPago" >  
            <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label>
         
         </h1>
		<div id="divHistorialOrdenesPago" style="text-align: center"  >
            <table id="genera-orden" style=" width:100%">
                <tr id="trVerEmpresa" class="stripe" runat="server">
                    <td class="item"><span>Empresa:</span></td>
                    <td>
                      <asp:Image ID="imgEmpresaContratante" AlternateText="Empresa Contratante" runat="server" ImageUrl="" Width="100px" />
                      <asp:Label ID="lblEmpresaContratanteTexto" runat="server" Text=""></asp:Label>
                        </td>
                </tr>
                <tr>
                    <td class="item"><strong>Servicio:</strong></td>
                    <td>
                      <asp:Image ID="imgServicio"  runat="server" ImageUrl=""/>
                      <asp:Label ID="lblServicioTexto" runat="server" Text=""></asp:Label>
                        <asp:HyperLink ID="lnkVolverServicio" Visible ="false"  runat="server">Volver al Servicio</asp:HyperLink></td>
                </tr>
               <tr class="stripe">
                     <td class="item"><span>Concepto de Pago:</span>
                     </td>
                     <td class="detalle">
                     <span><asp:Label ID="lblConcepto" runat="server" ></asp:Label></span>
                     </td>
                </tr>
		        <tr>
                    <td class="item" style="border-bottom:0"><span>Monto:</span></td>
                    <td class="detalle" style="border-bottom:0">
                      <span >
                       <asp:Label ID="lblMoneda"  Visible="false" runat="server" ></asp:Label>
                       <asp:Label ID="lblMonto"  runat="server" style="text-align:left;" ></asp:Label>
                      </span>
                    </td>
                </tr>
             </table>
                   <div style="text-align:center;">
                        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                        
                    </div>
            <asp:Label ID="lblMensaje2" runat="server" CssClass="MensajeTransaccion"></asp:Label><br />              
                   <br />              
                  <div  id="DivContenedorBotonera" class="ContenedorBotoneraGenerarOP" style="text-align:center;" >
                     <asp:Button ID="btnGenerar" runat="server" CssClass="Boton" Text="Generar" ></asp:Button>
                     <input id="btnImprimir2" runat="server"  type="button" onclick="" class="Boton" value="Imprimir" />
                  </div>               
                
               <div  runat="server" style="text-align:center;"  id="divGenerarOrdenPago" visible="False">
                  <div >
                       <div id="dvEtiquetaTextoCodigo" >
                           <asp:Label ID="lblOrdenPago" runat="server" CssClass="Etiqueta" Text="Cod. Identif. Pago:"></asp:Label>
                           <asp:Label ID="lblNumeroOrdenPago" runat="server" style="font-size:22px"  ></asp:Label>
                       </div>
                      <br />
                      <img id="imgCodeBar" runat="server" alt="" src="" style="width:500px;" />
                   </div>
                   <div class="ContenedorBotonera" id="Div6" style="text-align:center;" >
                		<input id="btnImprimir" runat="server"  type="button" onclick="" class="Hidden" value="Imprimir" />
                       
                    </div>
               </div>
               
             </div>      
              
           </div>

</asp:Content>

