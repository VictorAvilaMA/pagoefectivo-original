<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPageBase.master" AutoEventWireup="false"
    CodeFile="PrGeOrPSLog.aspx.vb" Inherits="CLI_PrGeOrPSLog" Title="Generar C�digo de Identificaci�n de Pago" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Page1">
        <div>
            <h1>
                Generar C�digo de Identificaci�n de Pago</h1>
            <div>
                <table id="genera-orden" style="width: 100%">
                    <tr id="trVerEmpresa" class="stripe" runat="server">
                        <td class="item">
                            <span>Empresa:</span>
                        </td>
                        <td class="detalle" style="width: 631px">
                            <asp:Image ID="imgEmpresaContratante" runat="server" ImageUrl="" Width="100px" />
                            <asp:Label ID="lblEmpresaContratanteTexto" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="item" style="height: 127px">
                            <span>Servicio:</span>
                        </td>
                        <td class="detalle" style="height: 127px; width: 631px;">
                            <asp:Image ID="imgServicio" runat="server" ImageUrl=""/>
                            <asp:Label ID="lblServicioTexto" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr class="stripe">
                        <td class="item">
                            <span>Concepto de Pago:</span>
                        </td>
                        <td class="detalle" style="width: 631px">
                            <span>
                                <asp:Label ID="lblConcepto" runat="server"></asp:Label></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="item" style="border-bottom: 0">
                            <span>Monto:</span></td>
                        <td class="detalle" style="border-bottom: 0; width: 631px;">
                            <span>
                                <asp:Label ID="lblMoneda" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblMonto" runat="server" Style="text-align: left;"></asp:Label>
                            </span>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr class="stripe">
                        <td class="item" style="font: bold; text-align: center">
                            <span style="font: bold">Detalle:</span></td>
                        <td class="detalle" style="border: 0px; width: 631px;">
                            <asp:GridView ShowHeader="false" CssClass="grilla" ID="gvDetalle" runat="server"
                                AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="tipoOrigen" />
                                    <asp:BoundField DataField="codOrigen" />
                                    <asp:BoundField DataField="Importe" />
                                    <asp:BoundField DataField="campo1" />
                                    <asp:BoundField DataField="campo2" />
                                    <asp:BoundField DataField="campo3" Visible ="false" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div2" style="padding-left: 180px; text-align: center;">
                &nbsp;
            </div>
            <br />
            <div id="Div1" style="padding-left: 180px; text-align: center;">
                <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
            </div>
            <br />
            <div style="padding-left: 200px;">
                <asp:TextBox ID="txtURL" style="visibility:hidden" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtXML"  style="visibility:hidden" runat="server"></asp:TextBox>
                <asp:Login ID="Login1" runat="server" CssClass="Login" LoginButtonText="Ingresa"
                    PasswordLabelText="Clave:" PasswordRequiredErrorMessage="La clave es requerida."
                    TitleText="Identificate" UserNameLabelText="E-mail:" UserNameRequiredErrorMessage="El email es requerido."
                    FailureText="Su intento de acceso no tuvo �xito. Por favor, int�ntalo de nuevo."
                    RememberMeText="Recordarme la pr�xima vez" CreateUserText="Registrarse" DestinationPageUrl="~/CLI/PrGeOrPS.aspx"
                    CreateUserUrl="~/CLI/ADReCl.aspx" PasswordRecoveryText="Recuperar contrase�a"
                    PasswordRecoveryUrl="~/RecuperarPassword.aspx">
                    <CheckBoxStyle CssClass="LoginCheckBoxStyle" />
                    <TextBoxStyle CssClass="LoginTextBox" />
                    <LoginButtonStyle CssClass="LoginButton" />
                    <ValidatorTextStyle CssClass="LoginValidator" />
                    <InstructionTextStyle CssClass="LoginInstruction" />
                    <LabelStyle CssClass="LoginLabel" />
                    <FailureTextStyle CssClass="LoginFailure" />
                    <TitleTextStyle CssClass="LoginTitle" />
                    <HyperLinkStyle CssClass="LoginHyPerLink" />
                    <LayoutTemplate>
                        <div id="login" style="width: 430px;">
                            <div>
                                <h1>
                                    Ingresar</h1>
                            </div>
                            <div class="label-email">
                                <div class="left item clear" style="margin-right: 10px;">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">E-mail:</asp:Label>
                                </div>
                                <div class="left">
                                    <asp:TextBox ID="UserName" runat="server" Width="220"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        CssClass="LoginValidator" ErrorMessage="El email es requerido." ToolTip="El email es requerido."
                                        ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="label-contrasena">
                                <div class="left item clear" style="margin-right: 10px;">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contrase�a:</asp:Label>
                                </div>
                                <div class="left">
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="220"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        CssClass="LoginValidator" ErrorMessage="La clave es requerida." ToolTip="La clave es requerida."
                                        ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="recordar">
                                <asp:CheckBox ID="RememberMe" runat="server" Text="Recordar mi sesi�n" />
                            </div>
                            <div style="width: 440px;">
                                <div class="LoginMensajeValidacion">
                                    <asp:Literal ID="FailureText" runat="server" Visible="false" EnableViewState="True"></asp:Literal>
                                    <asp:Literal ID="TextoValidacion" runat="server" EnableViewState="True"></asp:Literal>
                                </div>
                                <div class="divLoginIngresar">
                                    <asp:Button ID="LoginButton" CommandName="Login" runat="server" ValidationGroup="Login1"
                                        CssClass="BotonLogin" />
                                </div>
                            </div>
                            <div class="links">
                                <p>
                                    <asp:HyperLink ID="CreateUserLink" runat="server" NavigateUrl="~/CLI/ADReCl.aspx">Registrarse</asp:HyperLink>
                                    |
                                    <asp:HyperLink ID="PasswordRecoveryLink" runat="server" NavigateUrl="~/RecuperarPassword.aspx">Recuperar contrase�a</asp:HyperLink>
                                </p>
                            </div>
                        </div>
                    </LayoutTemplate>
                </asp:Login>
            </div>
        </div>
    </div>
    
</asp:Content>
