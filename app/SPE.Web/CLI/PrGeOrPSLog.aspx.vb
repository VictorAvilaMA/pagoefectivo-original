Imports System.Xml

Imports SPE.Entidades
Imports SPE.Utilitario

Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Util.DataUtil

Partial Class CLI_PrGeOrPSLog
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)
    Implements Ixml

    Protected WebMessage As WebMessage
    Private Sub ConfigureLoginManager()
        Dim _speLogin As New SPELogin(Login1)
        AddHandler _speLogin.AfterLoggedInEventHandler, AddressOf AfterLoggedIn
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        ConfigureLoginManager()
        WebMessage = New WebMessage(lblMensaje)
        WebMessage.Clear()
    End Sub


    Public Function GetUrl() As String Implements Ixml.GetUrl
        Return txtURL.Text
    End Function

    Public Function Getxml() As String Implements Ixml.Getxml
        Return txtXML.Text
    End Function

    Protected Sub AfterLoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            'Login1.DestinationPageUrl = "PRGeCIP.aspx?lg=2"
        Catch ex As Exception
            'Logger.LogException(ex)
            WebMessage.ShowErrorMessage(ex, True)
        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then


                Dim xmlTrama As String = Nothing
                Dim urlServicio As String = Nothing
                If Session(SPE.Web.Util.UtilGenCIP.__XmlTramaPre) Is Nothing Then
                    xmlTrama = Request.Form(System.Configuration.ConfigurationManager.AppSettings("stringXml")).ToString()
                    urlServicio = _3Dev.FW.Web.WebUtil.GetAbsoluteUriWithOutQuery(Request.UrlReferrer)
                Else
                    xmlTrama = Session(SPE.Web.Util.UtilGenCIP.__XmlTramaPre)
                    urlServicio = Session(SPE.Web.Util.UtilGenCIP.__UrlServicioPre)
                End If
                Session(SPE.Web.Util.UtilGenCIP.__XmlTramaPre) = Nothing
                Session(SPE.Web.Util.UtilGenCIP.__UrlServicioPre) = Nothing
                Session(SPE.Web.Util.UtilGenCIP.__XmlTrama) = xmlTrama
                Session(SPE.Web.Util.UtilGenCIP.__UrlServicio) = urlServicio

                Dim oberesponse As BEObtCIPInfoByUrLServResponse = Nothing
                Using ocOrdenPago As New SPE.Web.COrdenPago()
                    Dim oberequest As New BEObtCIPInfoByUrLServRequest()
                    oberequest.UrlServicio = urlServicio
                    oberequest.Trama = xmlTrama
                    oberequest.CargarTramaSolicitud = True
                    oberequest.TipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Post
                    oberesponse = ocOrdenPago.ObtenerCIPInfoByUrLServicio(oberequest)
                End Using
                If (oberesponse IsNot Nothing) Then

                    Session(SPE.Web.Util.UtilGenCIP.__XMLObtCIPInfoByUrLServRequest) = oberesponse
                    If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.ServicioNoExiste) Then
                        Response.Redirect("~/ServicioNoEncontrado.aspx")
                    End If

                    If oberesponse.Servicio.IdServicio < 1 OrElse oberesponse.Servicio.IdEmpresaContratante < 1 Then
                        imgEmpresaContratante.ImageUrl = ""
                        imgServicio.ImageUrl = ""
                    Else

                        imgEmpresaContratante.ImageUrl = String.Format("~/ADM/ADEmCoImg.aspx?empresaId={0}", ObjectToString(oberesponse.OcultarEmpresa.idEmpresaContratante))
                        imgServicio.ImageUrl = String.Format("~/ADM/ADServImg.aspx?servicioid={0}", ObjectToString(oberesponse.OcultarEmpresa.idServicio))

                        If Not oberesponse.OcultarEmpresa Is Nothing Then
                            trVerEmpresa.Visible = Not oberesponse.OcultarEmpresa.OcultarEmpresa

                            imgEmpresaContratante.Visible = Not oberesponse.OcultarEmpresa.OcultarImagenEmpresa
                            lblEmpresaContratanteTexto.Text = oberesponse.OcultarEmpresa.EmpresaContratanteDescripcion
                            lblEmpresaContratanteTexto.Visible = oberesponse.OcultarEmpresa.OcultarImagenEmpresa
                            imgServicio.Visible = Not oberesponse.OcultarEmpresa.OcultarImagenServicio
                            lblServicioTexto.Text = oberesponse.OcultarEmpresa.ServicioDescripcion
                            lblServicioTexto.Visible = oberesponse.OcultarEmpresa.OcultarImagenServicio

                        End If
                        If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.CodigoMonedaIncorrecto) Then
                            WebMessage.ShowErrorMessage(oberesponse.Message)
                            Throw New Exception(oberesponse.Message)
                        End If

                        If (oberesponse.OrdenPago IsNot Nothing) Then
                            lblEmpresaContratanteTexto.Text = oberesponse.OcultarEmpresa.EmpresaContratanteDescripcion
                            lblMonto.Text = oberesponse.Moneda.Simbolo + "  " + Convert.ToDecimal(oberesponse.OrdenPago.Total).ToString("#,#0.00")
                            lblConcepto.Text = oberesponse.OrdenPago.ConceptoPago

                            gvDetalle.DataSource = oberesponse.OrdenPago.DetallesOrdenPago
                            gvDetalle.DataBind()

                        End If

                    End If

                    CType(Login1.FindControl("CreateUserLink"), HyperLink).NavigateUrl = "ADReCl.aspx?datosEnc=" + "1" '+ "&" & sbparametersUrl.ToString

                    If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.MontoTotalOPMenorQueCero) Then
                        'lblMensaje.Text = "El monto no puede ser menor o igual a cero. Intente nuevamente."
                        lblMensaje.Text = oberesponse.Message
                        lblMensaje.CssClass = "MensajeValidacion"

                    Else

                        If (User.Identity.IsAuthenticated) Then
                            'Response.Redirect(Login1.DestinationPageUrl)
                            System.Web.Security.FormsAuthentication.SignOut()
                        End If

                        Dim deValidaRegs As String = HttpContext.Current.Request.QueryString("vr")
                        If (deValidaRegs = "1") Then
                            lblMensaje.Text = "Su cuenta ya ha sido activada. Usted estuvo realizando una C�digo de Identificaci�n de Pago. Para continuar con la operaci�n, necesita ingresar al sistema con su cuenta."

                            lblMensaje.Visible = True
                        Else
                            lblMensaje.Text = ""
                            lblMensaje.Visible = False

                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Login1.Visible = False
            lblMensaje.Text = ex.Message ' New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage
            lblMensaje.CssClass = "MensajeValidacion"
        End Try

    End Sub
    'Protected Sub Login1_LoggedIn(ByVal sender As Object, ByVal e As System.EventArgs) Handles Login1.LoggedIn
    '    Dim ucliente As New SPE.Web.Seguridad.SPEUsuarioClient()
    '    LoadUserInfo(ucliente.GetUserInfoByUserName(CType(Login1.FindControl("UserName"), TextBox).Text))
    'End Sub
    'Protected Sub Login1_LoginError(ByVal sender As Object, ByVal e As System.EventArgs) Handles Login1.LoginError
    '    Dim beUsuario As New SPE.Web.Seguridad.SPEUsuarioClient()
    '    Dim Estado As Integer
    '    Dim Pass As String = ""
    '    Dim userInfo As _3Dev.FW.Entidades.Seguridad.BEUserInfo = beUsuario.GetUserInfoByUserName(CType(Login1.FindControl("UserName"), TextBox).Text)
    '    Dim LoginErrorDetails As Literal = CType(Login1.FindControl("TextoValidacion"), Literal)
    '    If Not userInfo Is Nothing Then
    '        Estado = userInfo.IdEstado

    '    End If
    '    If userInfo Is Nothing Then
    '        LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. No existe un usuario con ese Email."
    '    ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
    '        LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta ha sido bloqueada. Debe contactarse con el Administrador."
    '    ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Then
    '        LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta est� en estado pendiente debido a que no se ha completado el proceso de registro."
    '    ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo Then
    '        LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta ha sido deshabilitada. Debe contactarse con el Administrador."
    '    ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo Then
    '        LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Verifique que su email y clave sean correctos."
    '    Else
    '        LoginErrorDetails.Text = Login1.FailureText
    '    End If
    'End Sub

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Private _grid As GridView
    Public Property GetGrid() As GridView
        Get
            Return _grid
        End Get
        Set(ByVal value As GridView)
            _grid = value
        End Set
    End Property
   

End Class
