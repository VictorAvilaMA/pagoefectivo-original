<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PrGeOrPaPopPup.aspx.vb" Inherits="CLI_PrGeOrPaPopPup" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vista Previa de Impresion</title>
    <link rel="stylesheet" type="text/css" href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/default.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" />
</head>
<body onload="javascript:print();" style=" font-size:12px; font-family:Verdana, Arial, Helvetica, sans-serif">

    <form id="form1" runat="server">
          <div style="height:30px;" >
            <div class="divContenedorTitulo" style="width:100px; float:left;" >Código de Identificación de Pago </div>
            <div style="float:right; cursor:pointer; " onclick="javascript:print();">  <img  alt="" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Images_Buton/imprimir.jpg" style="float:left" />
            </div>
          </div>
          <table id="genera-orden" class="mod"  style="margin-left:0px;  text-align:center;">
                <tr id="trVerEmpresa" class="stripe" runat="server">
                    <td class="item"><strong>Empresa:</strong></td>
                    <td>
                      <asp:Image ID="imgEmpresaContratante" AlternateText="Empresa Contratante" runat="server" ImageUrl="" Width="100px" />
                      <asp:Label ID="lblEmpresaContratanteTexto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="item"><strong>Servicio:</strong></td>
                    <td>
                      <asp:Image ID="imgServicio"  runat="server" ImageUrl="" Width="100px" />
                      <asp:Label ID="lblServicioTexto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr class="stripe">
                    <td class="item"><strong>Concepto   de pago:</strong></td>
                    <td><asp:Label ID="lblConcepto" runat="server"  ></asp:Label></td>
                </tr>
		        <tr>
                    <td class="item" style="border-bottom:0!important;"><strong>Monto</strong></td>
                    <td style="border-bottom:0!important;">
                        <asp:Label ID="lblMoneda" Visible="false"  runat="server"  ></asp:Label>
                        <asp:Label ID="lblMonto" runat="server"  ></asp:Label>
                    </td>
                </tr>
          </table>      
               <div  runat="server" style="text-align:center; margin-top:20px;"  id="divGenerarOrdenPago">
                  <div >
                      <asp:Label ID="lblNumeroOrdenPago" runat="server" style="font-size:35px"  ></asp:Label>
                      <img id="imgCodeBar" runat="server" alt="" src="" style="width:400px;" />
                   </div>
               </div>
        <div  runat="server" style="text-align:left; margin-left:35px; margin-top:5px;"  id="div1">
            <div>
                <strong><asp:Label ID="lblNombreCliente" Width="100px" runat="server" Text="Cliente:"></asp:Label></strong>
                <asp:Label ID="lblNombreClienteValor" runat="server" Text=""></asp:Label>
            </div>                        
            <div>
               <strong> <asp:Label ID="lblEmailCliente" Width="100px" runat="server" Text=""></asp:Label></strong>
                <asp:Label ID="lblEmailClienteValor" runat="server" Text=""></asp:Label>
            </div>            
            <div>
               <strong> <asp:Label ID="lblFechaGenerado" Width="100px" runat="server" Text="Fec.Generado:"></asp:Label></strong>
                <asp:Label ID="lblFechaGeneradoValor" runat="server" Text=""></asp:Label>
            </div>
            <div>
              <strong>  <asp:Label ID="lblFechaImpreso" Width="100px" runat="server" Text="Fec. Impresión:"></asp:Label></strong>
                <asp:Label ID="lblFechaImpresoValor" runat="server" Text=""></asp:Label>
            </div>            
        </div>        
               
                
    </form>
</body>
</html>
