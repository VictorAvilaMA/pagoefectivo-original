Imports SPE.Entidades
Partial Class CLI_PrGeOrPaPopPup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            Dim fechaEmision, numeroOrden, idservicio, idempresa, orderId, urlOk, urlError, mailComercio, orderDescripcion, moneda, monto, urlServicio As String
            idservicio = Request.QueryString("idservicio")
            idempresa = Request.QueryString("idempresa")
            orderId = Request.QueryString("orderid")
            urlOk = Request.QueryString("urlok")
            urlError = Request.QueryString("urlerror")
            mailComercio = Request.QueryString("mailcom")
            orderDescripcion = Request.QueryString("orderdescripcion")
            moneda = Request.QueryString("moneda")
            monto = Request.QueryString("monto")
            numeroOrden = Request.QueryString("numeroOrden")
            urlServicio = Request.QueryString("urlServicio")
            fechaEmision = Request.QueryString("fechaEmision")

            trVerEmpresa.Visible = Not (Request.QueryString("OcultarEmpresa") = Boolean.TrueString)
            imgEmpresaContratante.Visible = Not (Request.QueryString("OcultarImagenEmpresa") = Boolean.TrueString)
            lblEmpresaContratanteTexto.Text = Request.QueryString("ECDesc")
            lblEmpresaContratanteTexto.Visible = Request.QueryString("OcultarImgEmpresa") = Boolean.TrueString
            imgServicio.Visible = Not (Request.QueryString("OcultarImgServicio") = Boolean.TrueString)
            lblServicioTexto.Text = Request.QueryString("SEDesc")
            lblServicioTexto.Visible = Request.QueryString("OcultarImgServicio") = Boolean.TrueString

            Dim obeServicio As New BEServicio
            If idservicio = "" Or idempresa = "" Then
                Dim objCServicio As New SPE.Web.CServicio
                obeServicio = objCServicio.ConsultarServicioPorUrl(urlServicio)
            End If
            obeServicio.IdServicio = Convert.ToInt32(idservicio)
            obeServicio.IdEmpresaContratante = Convert.ToInt32(idempresa)

            If obeServicio.IdEmpresaContratante < 1 Or obeServicio.IdServicio < 1 Then
                imgEmpresaContratante.ImageUrl = ""
                imgServicio.ImageUrl = ""
            Else
                imgEmpresaContratante.ImageUrl = "~/ADM/ADEmCoImg.aspx?empresaId=" + obeServicio.IdEmpresaContratante.ToString()
                imgServicio.ImageUrl = "~/ADM/ADServImg.aspx?servicioid=" + obeServicio.IdServicio.ToString()
            End If
            lblConcepto.Text = orderDescripcion
            Dim objCComun As New SPE.Web.CAdministrarComun
            Dim obeMonedas As New SPE.Entidades.BEMoneda
            obeMonedas = objCComun.ConsultarMonedaPorId(moneda)
            lblMonto.Text = obeMonedas.Simbolo + "  " + Convert.ToDecimal(monto).ToString("#,#0.00")

            lblNumeroOrdenPago.Text = numeroOrden
            CargarCodigo()
            '
            Dim cAdmComun As New SPE.Web.CAdministrarComun()
            lblFechaGeneradoValor.Text = fechaEmision
            lblFechaImpresoValor.Text = cAdmComun.ConsultarFechaHoraActual().ToString()
            lblEmailClienteValor.Text = _3Dev.FW.Web.Security.UserInfo.Email
            lblNombreClienteValor.Text = _3Dev.FW.Web.Security.UserInfo.Nombres + " " + _3Dev.FW.Web.Security.UserInfo.Apellidos


        End If



        'End If
    End Sub
    Sub CargarCodigo()
        Me.imgCodeBar.Src = "ADOPImg.aspx?numero=" + lblNumeroOrdenPago.Text
    End Sub
End Class
