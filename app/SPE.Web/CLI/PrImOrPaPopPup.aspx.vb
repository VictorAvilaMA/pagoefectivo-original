Imports SPE.Entidades

Partial Class CLI_PrImOrPaPopPup
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ConstruirOrden()
        End If


    End Sub
    Sub ConstruirOrden()
        Dim idOrdenPago As Integer
        Dim numeroOrden, idservicio, idempresa, moneda, monto As String
        Dim objCOrdenPago As New SPE.Web.COrdenPago
        Dim obeOrdenPago As New BEOrdenPago
        Dim idUsuario As Integer

        idOrdenPago = Convert.ToInt32(Request.QueryString("idOrdenPago"))
        '        idUsuario = Convert.ToInt32(Request.QueryString("idUsuario"))
        obeOrdenPago.IdCliente = idUsuario
        obeOrdenPago.IdOrdenPago = idOrdenPago
        obeOrdenPago = objCOrdenPago.ConsultarOrdenPagoPorId(obeOrdenPago)

        trVerEmpresa.Visible = Not (obeOrdenPago.OcultarEmpresa = "True")
        imgEmpresaContratante.Visible = Not obeOrdenPago.OcultarImagenEmpresa
        lblEmpresaContratanteTexto.Text = obeOrdenPago.DescripcionEmpresa
        lblEmpresaContratanteTexto.Visible = obeOrdenPago.OcultarImagenEmpresa
        imgServicio.Visible = Not obeOrdenPago.OcultarImagenServicio
        lblServicioTexto.Text = obeOrdenPago.DescripcionServicio
        lblServicioTexto.Visible = obeOrdenPago.OcultarImagenServicio
        lblNumeroOrdenPago.Text = obeOrdenPago.NumeroOrdenPago
        lblConcepto.Text = obeOrdenPago.ConceptoPago
        idservicio = obeOrdenPago.IdServicio.ToString()
        idempresa = obeOrdenPago.IdEmpresa.ToString()
        moneda = obeOrdenPago.IdMoneda.ToString

        monto = obeOrdenPago.Total.ToString
        numeroOrden = obeOrdenPago.NumeroOrdenPago
        imgEmpresaContratante.ImageUrl = "~/ADM/ADEmCoImg.aspx?empresaId=" + obeOrdenPago.IdEmpresa.ToString
        imgServicio.ImageUrl = "~/ADM/ADServImg.aspx?servicioid=" + obeOrdenPago.IdServicio.ToString

        '        lblMonto.Text = moneda + "" + Convert.ToDecimal(monto).ToString("F")
        lblMonto.Text = obeOrdenPago.SimboloMoneda + "  " + Convert.ToDecimal(monto).ToString("#,#0.00")
        lblNumeroOrdenPago.Text = numeroOrden
        lblFechaGeneradoValor.Text = obeOrdenPago.FechaEmision.ToString()
        Dim cAdmComun As New SPE.Web.CAdministrarComun()
        lblFechaImpresoValor.Text = cAdmComun.ConsultarFechaHoraActual().ToString()
        Me.imgCodeBar.Src = "ADOPImg.aspx?numero=" + Me.lblNumeroOrdenPago.Text.Trim
        lblEmailClienteValor.Text = _3Dev.FW.Web.Security.UserInfo.Email
        lblNombreClienteValor.Text = _3Dev.FW.Web.Security.UserInfo.Nombres + " " + _3Dev.FW.Web.Security.UserInfo.Apellidos
    End Sub
End Class
