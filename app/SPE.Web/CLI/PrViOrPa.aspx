<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="PrViOrPa.aspx.vb" Inherits="CLI_PrViOrPa" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Page" style="text-align: center;width: 890px "  >
        <h1>Generar Código de Identificación de Pago</h1>
		<div id="divHistorialOrdenesPago" class="divContenedor" style=" ">

           <div id="divHistorialOrdenesPagoSub" class="divSubContenedor2">
             <div id="divCriteriosBusqueda" >
               <div id="divCriterios" class="divSubContenedorCriteriosBusqueda">
                   <fieldset  class="ContenedorEtiquetaTexto">
                    <div id="divNroOrdenPago" class="EtiquetaTextoIzquierda" >
                       <asp:Label ID="lblEmpresa" CssClass="EtiquetaLargo" runat="server" Text="Empresa:"></asp:Label>
                        <asp:Image ID="imgEmpresaContratante" runat="server" ImageUrl="" Width="100px" />
                        
                        
                    </div>
                  </fieldset>
                  <fieldset  class="ContenedorEtiquetaTexto">
                    <div id="div3" class="EtiquetaTextoIzquierda" >
                       <asp:Label ID="lblServicio" CssClass="EtiquetaLargo" runat="server" Text="Servicio:"></asp:Label>
                        <asp:Image ID="imgServicio" runat="server" ImageUrl="" Width="100px" />
                    </div>
                  </fieldset>
                  
                   <fieldset  class="ContenedorEtiquetaTexto">
                    <div id="Div2" class="EtiquetaTextoIzquierda" >
                       <asp:Label ID="lblewqrwe" CssClass="EtiquetaLargo" runat="server" Text="Concepto de Pago:"></asp:Label>
                        <asp:Label ID="lblConcepto" runat="server"  ></asp:Label>
                       
                    </div>       
                  </fieldset>
                  
                  <fieldset  class="ContenedorEtiquetaTexto">
                    <div id="div4" class="EtiquetaTextoIzquierda" >
                       <asp:Label ID="lblasdad" CssClass="EtiquetaLargo" runat="server" Text="Monto:"></asp:Label>
                       <asp:Label ID="lblMoneda"  runat="server"  ></asp:Label>
                        <asp:Label ID="lblMonto" runat="server"  ></asp:Label>
                    </div>
                  </fieldset>
                  <fieldset  class="ContenedorEtiquetaTexto">
                    <div id="div5" class="EtiquetaTextoIzquierda" >
                       <asp:Label ID="lblCliente" CssClass="EtiquetaLargo" runat="server" Text="Cliente:" Visible="False"></asp:Label>
                        <asp:Label ID="Label5" runat="server"  Visible="False"></asp:Label>
                    </div>
                  </fieldset>

                  

                  
               </div>

                  <fieldset class="Hidden">
                  
                      <asp:Label ID="lblIdCliente" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblUrlOk" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblUrlError" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblMailComercio" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblOrderIdComerio" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblIdMoneda" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblIdServicio" runat="server" Visible="False"></asp:Label>
                      <asp:Label ID="lblIdEmpresa" CssClass="Etiqueta" runat="server" Visible="False" ></asp:Label>
                      
                      
                  
                  </fieldset>
                       <asp:ImageButton ID="imgBtnImprimir" ImageUrl="../Images_Buton/imprimir.jpg"  runat="server" />                                             
               <div class="divSubContenedorCriteriosBusqueda" runat="server" id="divGenerarOrdenPago" visible="False">
                  <div style="text-align:center;">
                       <div id="dvEtiquetaTextoCodigo" >
                           <asp:Label ID="lblOrdenPago" runat="server" CssClass="Etiqueta" Text="Cod. Identif. Pago:"></asp:Label>
                           <asp:Label ID="lblNumeroOrdenPago" runat="server"  CssClass="Etiqueta"></asp:Label>
                       </div>
                       <br /> <br />
                       <div id="dvEtiquetaTextoEstado">
                           
                             
                       </div>
                   </div>
                   <div class="ContenedorBotonera" id="Div6" >
                		<input id="btnImprimir" runat="server"  type="button" onclick="" class="Hidden" value="Imprimir" />
                       
                    </div>
               </div>
               
             </div>      
              
           </div>

		</div>
        </div>
</asp:Content>

