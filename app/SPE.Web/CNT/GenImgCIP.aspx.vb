Imports SPE.Web
Imports System.Collections
Imports System.Collections.Generic
Imports SPE.Utilitario
Imports _3Dev.FW.Util.DataUtil

Partial Class CNT_GenImgCIP
    Inherits System.Web.UI.Page

    Const QSCODIGO As String = "codigo"
    'Const QSCAPI As String = "capi"
    'Const QSCCLAVE As String = "cclave"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GenerarCodigo()
    End Sub

    Sub GenerarCodigo()
        Dim codigo As String = Request.QueryString(QSCODIGO)
        If codigo IsNot Nothing Then
            Dim cip As String = Nothing
            Dim capi As String = Nothing
            Dim cclave As String = Nothing
            Using oclsLibreria As New ClsLibreriax()
                codigo = oclsLibreria.Decrypt(codigo, oclsLibreria.fEncriptaKey)
                Dim parametros As New Dictionary(Of String, String)
                Dim listcodigos As String() = codigo.Split("|")
                For Each item As String In listcodigos
                    Dim key As String = item.Split("=")(0)
                    Dim value As String = item.Split("=")(1)
                    parametros.Add(key, value)
                Next
                cip = parametros("cip")
                capi = parametros("capi")
                cclave = parametros("cclave")
            End Using
            Using oCntrlServicio As New CServicio()
                If (oCntrlServicio.ValidarServicioPorAPIYClave(capi, cclave)) Then
                    SPE.Web.Util.UtilBarCode.GenerarCodigoBarra(cip)
                Else
                    Response.Write("El C�digo y Clave no son correctos. No se Puede Generar el C�digo de Barras")
                End If
            End Using
        Else
            Response.Write("No se Puede Generar el C�digo de Barras, parametro codigo esta vac�o")
        End If
        

    End Sub

End Class
