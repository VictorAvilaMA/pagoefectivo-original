Imports _3Dev.FW.Util.DataUtil

Partial Class CNT_GenImgCIP2
    Inherits System.Web.UI.Page

    Const QSCODIGO As String = "codigo"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GenerarCodigo()
    End Sub

    Sub GenerarCodigo()
        Dim codigo As String = ObjectToString(Request.QueryString(QSCODIGO))
        If (codigo <> "") Then
            SPE.Web.Util.UtilBarCode.GenerarCodigoBarra(codigo)
        End If
    End Sub

End Class
