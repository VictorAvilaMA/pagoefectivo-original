<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRGeCIP.aspx.vb"
    Inherits="CNT_PRGeCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PagoEfectivo</title>
    <link id="Link1" rel="stylesheet" runat="server" type="text/css" href="style/peconnect/print.css"
        media="print" />
    <link id="Link2" rel="stylesheet" runat="server" type="text/css" href="style/peconnect/screen.css"
        media="screen" />
    <!--[if IE]>
	    <link rel="stylesheet" runat="server" href="~/style/peconnect/_ie-only.css" type="text/css" media="screen" />
    <![endif]-->
    <script type="text/javascript">
        function OnCloseCommon(msg) {

            if (window.confirm(msg))
                return window.close();
            else
                return false;

        }

        function OnClose() {
            return OnCloseCommon("Est� seguro que desea cerrar la ventana de generaci�n de CIP");
        }

        function OnCancel() {
            return OnCloseCommon("Est� seguro que desea cancelar el proceso de generaci�n de CIP");
        }

        function OnExit() {
            return OnCloseCommon("Est� seguro que desea salir del proceso de generaci�n de CIP");
        }
        function OnLogin() {
            document.getElementById("Login1_LoginButton").click();
            /*  WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;Login1$LoginButton&quot;, &quot;&quot;, true, &quot;Login1&quot;, &quot;&quot;, false, false))*/
        }
        //function OnProccess()
        //    {
        //      document.getElementById("btnFooter2GenerarHidden").click();
        //    }
    </script>
</head>
<body id="body_home" class="login">
    <form id="form1" runat="server" class="dlgrid h100 normalize" defaultfocus="Login1_UserName">
    <%--<asp:HiddenField ID="_SPEParams" runat="server" />--%>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="wrapper">
        <div id="header">
            <h1>
                <asp:Literal ID="ltTitulo" runat="server" Text="Realiza tu pago con PagoEfectivo"></asp:Literal>
            </h1>
        </div>
        <div id="main">
            <div id="content">
                <asp:Panel ID="pnlBody1GePaLo" runat="server">
                    <p class="intro center">
                        Conecta <strong>
                            <asp:Literal ID="ltBody1GePaLoServicioNombre" runat="server"></asp:Literal>
                        </strong>con <strong>PagoEfectivo</strong> para generar tus c�digos de confirmaci�n
                        de pago (CIP) y cancela en efectivo mediante nuestras diversas agencias o bancos
                        afiliados
                    </p>
                    <div id="img_process">
                        <img id="imgBody1GePaLoServicio" height="51" src="~/images/neoauto_thumb.gif" runat="server"
                            class="thumb_servproc" alt="neoauto" />
                    </div>
                    <asp:Login ID="Login1" runat="server">
                        <LayoutTemplate>
                            <fieldset class="dlinline login">
                                <dl class="clearfix w100">
                                    <dt>
                                        <label for="txtemail">
                                            E-mail</label>
                                    </dt>
                                    <dd>
                                        <asp:TextBox ID="UserName" runat="server" CssClass="text" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvEmailMandatorio" runat="server" ControlToValidate="UserName"
                                            ErrorMessage="Debe ingresar un email." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rfvEmailFormato" runat="server" ControlToValidate="UserName"
                                            ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="Login1">*</asp:RegularExpressionValidator>
                                    </dd>
                                </dl>
                                <dl class="clearfix w100">
                                    <dt>
                                        <label for="txtpswd">
                                            Password</label>
                                    </dt>
                                    <dd>
                                        <asp:TextBox ID="Password" runat="server" CssClass="text" MaxLength="15" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="Password"
                                            ErrorMessage="Debe ingresar su password" ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl class="clearfix w100">
                                    <dt>
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="Login1"
                                            Style="display: none;" />
                                    </dt>
                                    <dd>
                                        <div style="color: red;">
                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            <asp:Literal ID="TextoValidacion" runat="server" EnableViewState="True"></asp:Literal>
                                        </div>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                    </dd>
                                </dl>
                            </fieldset>
                        </LayoutTemplate>
                    </asp:Login>
                </asp:Panel>
                <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion" EnableViewState="false"></asp:Label>
                <asp:HiddenField ID="hdnCurrentPanel" runat="server" />
            </div>
        </div>
        <!-- no borrar div push -->
        <div class="push">
        </div>
    </div>
    <div id="footer">
        <asp:Panel ID="pnlFooter1GePaLo" runat="server">
            <button type="button" id="btnFooter1Salir" runat="server" class="close float_right ml"
                title="Salir" onclick="return OnExit();">
                Salir</button>
            <button type="button" class="submit float_right" title="Ingresar" onclick="javascript:OnLogin();">
                Ingresar</button>
            <asp:HyperLink ID="hplinkRegistro" runat="server" NavigateUrl="~/CNT/PRRegCl.aspx"
                CssClass="registrarse" title="Registrate en PagoEfectivo">
                  &raquo; Registrate en <strong>PagoEfectivo</strong>
            </asp:HyperLink>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
