Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration.Configuration

Imports SPE.Web.Seguridad
Imports SPE.Entidades
Imports SPE.Utilitario

Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

Partial Class CNT_PRGeCIP
    Inherits System.Web.UI.Page

#Region "Atributos"
    Protected WebMessage As WebMessage
#End Region

#Region "Eventos y Metodos sobreescritos"
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        ConfigureLoginManager()
        WebMessage = New WebMessage(lblMensaje)
        WebMessage.Clear()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not FirstPostBack Then
            Dim urlServicio As String = Nothing
            Dim strTrama As String = Nothing
            If ((Request Is Nothing) Or (Request.UrlReferrer Is Nothing) Or (Request.UrlReferrer.AbsoluteUri = Nothing)) Then
                urlServicio = ""
                strTrama = ""
            Else
                urlServicio = _3Dev.FW.Web.WebUtil.GetAbsoluteUriWithOutQuery(Request.UrlReferrer)
                strTrama = Request.RawUrl.ToString
            End If
            Session(SPE.Web.Util.UtilGenCIP.__CNTUrlServicio) = urlServicio
            Session(SPE.Web.Util.UtilGenCIP.__CNTUrlTrama) = strTrama

            If User.Identity.IsAuthenticated Then
                System.Web.Security.FormsAuthentication.SignOut()
            End If
            FirstPostBack = True
            MostrarVistaGePaLo1(urlServicio, strTrama)
        End If



        'If Not FirstPostBack Then


        'FirstPostBack = True
        'hdnCurrentPanel.Value = "1"

        'Dim originalSPParams As Object = Request.Form(ConfigurationManager.AppSettings("CNTParams"))
        'If Not originalSPParams Is Nothing Then

        '    _SPEParams.Value = CType(originalSPParams, String)
        'Else
        '_SPEParams.Value = Request.QueryString("")
        'Dim strurlServicio As String

        'End If


        'If Request.QueryString("lg") = "2" Then
        '    hdnCurrentPanel.Value = "2"
        '    GoToPanel("2")
        'Else

        '    If ((Request Is Nothing) Or (Request.UrlReferrer Is Nothing) Or (Request.UrlReferrer.AbsoluteUri = Nothing)) Then
        '        UrlServicio = ""
        '    Else
        '        UrlServicio = _3Dev.FW.Web.WebUtil.GetAbsoluteUriWithOutQuery(Request.UrlReferrer)
        '    End If

        '    If User.Identity.IsAuthenticated Then
        '        System.Web.Security.FormsAuthentication.SignOut()
        '    End If
        '    'GoToPanel(hdnCurrentPanel.Value)
        'End If



        'End If

    End Sub

    Private Sub MostrarVistaGePaLo1(ByVal urlServicio As String, ByVal strTrama As String)
        Try

            Dim oberesponse As BEObtCIPInfoByUrLServResponse = Nothing
            Using ocOrdenPago As New SPE.Web.COrdenPago()
                Dim oberequest As New BEObtCIPInfoByUrLServRequest()
                oberequest.UrlServicio = urlServicio
                oberequest.Trama = strTrama
                oberequest.CargarTramaSolicitud = True
                oberequest.TipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Request
                oberesponse = ocOrdenPago.ObtenerCIPInfoByUrLServicio(oberequest)
            End Using

            If (oberesponse IsNot Nothing) Then
                Session(SPE.Web.Util.UtilGenCIP.__CNTObtCIPInfoByUrLServRequest) = oberesponse
                If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.ServicioNoExiste) Then
                    Response.Redirect("~/ServicioNoEncontrado.aspx")
                End If

                ltTitulo.Text = " Realiza tu pago con PagoEfectivo"
                ltBody1GePaLoServicioNombre.Text = oberesponse.OcultarEmpresa.ServicioDescripcion
                imgBody1GePaLoServicio.Src = "~/ADM/ADServImg.aspx?servicioid=" + ObjectToString(oberesponse.OcultarEmpresa.idServicio)
                hplinkRegistro.NavigateUrl = "~/CNT/PRRegCl.aspx?sn=" + oberesponse.OcultarEmpresa.ServicioDescripcion
                Login1.DestinationPageUrl = "~/CNT/PRGeCIP2.aspx"
            End If


            'ValidarContratoServicios()
            ''CType(Login1.FindControl("UserName"), TextBox).Focus()

            'Dim trans As SPE.EmsambladoComun.TraductorServ = New SPE.EmsambladoComun.TraductorServ(StrRutaServ, Request.RawUrl.ToString, UrlServicio)
            'Dim idservicio As String = ""
            'Dim servicioNombre As String = ""
            'If UrlServicio <> "" Then
            '    Dim obeServicio As New BEServicio
            '    Dim objCServicio As New SPE.Web.CServicio
            '    obeServicio = objCServicio.ConsultarServicioPorUrl(UrlServicio)
            '    servicioNombre = obeServicio.Nombre
            '    If obeServicio Is Nothing Then
            '        Response.Redirect("~/ServicioNoEncontrado.aspx")
            '    Else
            '        idservicio = obeServicio.IdServicio
            '    End If
            '    ActualizarTramaRequest(trans, obeServicio)
            'Else
            '    Response.Redirect("~/ServicioNoEncontrado.aspx")
            'End If

            'ltTitulo.Text = " Realiza tu pago con PagoEfectivo"
            'ltBody1GePaLoServicioNombre.Text = servicioNombre
            'imgBody1GePaLoServicio.Src = "~/ADM/ADServImg.aspx?servicioid=" + idservicio.ToString()
            'MostrarVista("1", True)
            'hplinkRegistro.NavigateUrl = "~/CNT/PRRegCl.aspx?sn=" + servicioNombre

        Catch ex As Exception
            'Logger.LogException(ex)
            WebMessage.ShowErrorMessage(ex, True)
        End Try

    End Sub

    Protected Sub AfterLoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            'Login1.DestinationPageUrl = "PRGeCIP.aspx?lg=2"
        Catch ex As Exception
            'Logger.LogException(ex)
            WebMessage.ShowErrorMessage(ex, True)
        End Try

    End Sub


    'Protected Sub btnFooter2Generar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    GoToPanel("3")
    'End Sub
#End Region
 
    '#Region "Metodos"
    Private Sub ConfigureLoginManager()
        Dim _speLogin As New SPELogin(Login1)
        AddHandler _speLogin.AfterLoggedInEventHandler, AddressOf AfterLoggedIn
    End Sub

    '    Private Sub ValidarContratoServicios()
    '        'Dim strRutaServ As String = ""
    '        Dim strTargetPath As String = ""
    '        Try

    '            strTargetPath = System.Configuration.ConfigurationManager.AppSettings("targetPath")
    '            If Not System.IO.File.Exists(strTargetPath) Then
    '                StrRutaServ = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRuta")
    '                If Not System.IO.File.Exists(StrRutaServ) Then
    '                    Throw New SPE.Exceptions.ContratoNoEncontradoException(System.Configuration.ConfigurationManager.AppSettings("CodErrorContrato"))
    '                End If
    '            Else
    '                StrRutaServ = strTargetPath
    '            End If



    '        Catch exx As SPE.Exceptions.ContratoNoEncontradoException
    '            Logger.LogException(exx)
    '            WebMessage.ShowErrorMessage(exx, True)
    '            Login1.Visible = False
    '            'ThrowErrorMessage(exx.CustomMessage)
    '        End Try
    '    End Sub


    '    Function FormatearNumeroOrdenPago(ByVal numeroOrden As Integer) As String
    '        Return Right("00000000000000" + numeroOrden.ToString, 14)
    '    End Function

    '    Private Sub MostrarGenSuccess3()
    '        Try
    '            Dim objOrdenPago As New SPE.Web.COrdenPago
    '            Dim obeOPXServClienCons As BEOPXServClienCons = objOrdenPago.ConsultarOrdenPagoPorIdComercio(_OrdenPago.OrderIdComercio, _OrdenPago.IdServicio)
    '            'idOrdenPago = objOrdenPago.ConsultarOrdenPagoPorNumeroOrdenPago(respuestas)


    '            If obeOPXServClienCons Is Nothing Then

    '                ltBody3GenSuccessMsgProcesoResultado.Text = "Se ha generado el c�digo de identificaci�n de pago (CIP)"

    '                Dim respuestas As String
    '                Dim idOrdenPago As String = ""
    '                Dim resultado As String
    '                'urlServicio = ViewState("urlServicio")
    '                'idservicio = _OrdenPago.IdServicio

    '                'Dim Servicio As String
    '                'Servicio = Session("Servicio")


    '                resultado = objOrdenPago.GenerarOrdenPago(_OrdenPago, _OrdenPago.DetallesOrdenPago)

    '                Me.lblMensaje.Text = ""
    '                If resultado > 0 Then
    '                    Me.ltBody3GenSuccessNroOrdenPago.Text = FormatearNumeroOrdenPago(resultado)



    '                    'Me.divGenerarOrdenPago.Visible = True
    '                    'Me.btnGenerar.Enabled = False
    '                    ' Me.btnGenerar.Visible = False

    '                    'Dim sbqueryString As New StringBuilder
    '                    'sbqueryString.Append("javascript:AbrirPopPup('PrGeOrPaPopPup.aspx?")
    '                    'sbqueryString.Append("orderid=") : sbqueryString.Append(_OrdenPago.OrderIdComercio)
    '                    'sbqueryString.Append("&urlok=") : sbqueryString.Append(_OrdenPago.UrlOk)
    '                    'sbqueryString.Append("&urlerror=") : sbqueryString.Append(_OrdenPago.UrlError)
    '                    'sbqueryString.Append("&mailcom=") : sbqueryString.Append(_OrdenPago.MailComercio)
    '                    'sbqueryString.Append("&orderdescripcion=") : sbqueryString.Append(_OrdenPago.ConceptoPago)
    '                    'sbqueryString.Append("&moneda=") : sbqueryString.Append(_OrdenPago.IdMoneda)
    '                    'sbqueryString.Append("&monto=") : sbqueryString.Append(_OrdenPago.Total)

    '                    'sbqueryString.Append("&OcultarEmpresa=") : sbqueryString.Append(ViewState("OcultarEmpresa").ToString)
    '                    'sbqueryString.Append("&OcultarImagenEmpresa=") : sbqueryString.Append(ViewState("OcultarImagenEmpresa").ToString)
    '                    'sbqueryString.Append("&ECDesc=") : sbqueryString.Append(ViewState("ECDesc").ToString)
    '                    'sbqueryString.Append("&OcultarImgEmpresa=") : sbqueryString.Append(ViewState("OcultarImgEmpresa").ToString)
    '                    'sbqueryString.Append("&OcultarImgServicio=") : sbqueryString.Append(ViewState("OcultarImgServicio").ToString)
    '                    'sbqueryString.Append("&SEDesc=") : sbqueryString.Append(ViewState("SEDesc").ToString)

    '                    'sbqueryString.Append("&UsuarioPais=") : sbqueryString.Append(ViewState("UsuarioPais").ToString)

    '                    'sbqueryString.Append("&idservicio=") : sbqueryString.Append(_OrdenPago.IdServicio)
    '                    'sbqueryString.Append("&idempresa=") : sbqueryString.Append(_OrdenPago.IdEmpresa)
    '                    ''
    '                    'sbqueryString.Append("&fechaEmision=") : sbqueryString.Append(_OrdenPago.FechaEmision.ToString)
    '                    ''
    '                    'sbqueryString.Append("&numeroOrden=") : sbqueryString.Append(ltBody3GenSuccessNroOrdenPago.Text)
    '                    'sbqueryString.Append("&urlServicio=") : sbqueryString.Append(_OrdenPago.UrlServicio)
    '                    'sbqueryString.Append("','Vista_Previa_De_Impresion',450,550);")

    '                    'btnImprimir2.Attributes("onclick") = sbqueryString.ToString
    '                    'btnImprimir2.Visible = Not btnGenerar.Visible
    '                    'imgCodeBar.Src = "ADOPImg.aspx?numero=" + lblNumeroOrdenPago.Text
    '                    'lblTitulo.Text = "C�digo de Identificaci�n de Pago generada"

    '                    'lblMensaje.Text = "La orden se gener� satisfactoriamente."
    '                    'lblMensaje.CssClass = "MensajeTransaccion"
    '                    MostrarGeneracionSatisfactoria()

    '                Else
    '                    lblMensaje.Text = "El C�digo de Identificaci�n de Pago no se gener� debido a que ya ha sido generado anteriormente."
    '                    lblMensaje.CssClass = "MensajeValidacion"
    '                End If

    '                ltBody3GenSuccessMsgEnvioEmail.Visible = True
    '            Else
    '                ltBody3GenSuccessMsgEnvioEmail.Visible = False
    '                If (ObjectToInt(obeOPXServClienCons.IdUsuario) = ObjectToInt(UserInfo.IdUsuario)) Then
    '                    MostrarGeneracionSatisfactoria()
    '                    ltBody3GenSuccessMsgProcesoResultado.Text = "el c�digo de identificaci�n de pago (CIP) ya se ha generado anteriormente "
    '                    'lblMensaje.Text = "La operaci�n gener� el siguiente C�digo de Identificaci�n de Pago"
    '                    'lblMensaje.CssClass = "MensajeValidacion"
    '                    Me.ltBody3GenSuccessNroOrdenPago.Text = FormatearNumeroOrdenPago(obeOPXServClienCons.NumeroOrdenPago)
    '                    'Me.divGenerarOrdenPago.Visible = True
    '                    'Me.btnGenerar.Enabled = False
    '                    'Me.btnGenerar.Visible = False
    '                    imgBody3GenSuccessCodeBar.Src = "~/CLI/ADOPImg.aspx?numero=" + obeOPXServClienCons.NumeroOrdenPago
    '                    imgBody3GenSuccessCodeBar.Alt = Me.ltBody3GenSuccessNroOrdenPago.Text

    '                Else
    '                    MostrarVista("2", True)
    '                    WebMessage.ShowErrorMessage("El c�digo de identificaci�n de pago (CIP) para esta operaci�n ya se ha generado anteriormente por otro usuario.")

    '                End If




    '                '' datos de query string ojo
    '                'Dim sbqueryString As New StringBuilder
    '                'sbqueryString.Append("javascript:AbrirPopPup('PrImOrPaPopPup.aspx?idOrdenPago=" + idOrdenPago + "','Vista_Previa_De_Impresion',450,550);")
    '                'btnImprimir2.Attributes("onclick") = sbqueryString.ToString
    '                'btnImprimir2.Visible = Not btnGenerar.Visible



    '            End If

    '        Catch ex As Exception
    '            _3Dev.FW.Web.Log.Logger.LogException(ex)
    '            lblMensaje.Text = New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage
    '            lblMensaje.CssClass = "MensajeValidacion"
    '        End Try


    '    End Sub

    '    Private Sub MostrarGeneracionSatisfactoria()
    '        MostrarVista("3", True)
    '        ltTitulo.Text = " La orden se gener� satisfactoriamente"
    '        ltBody3GenSuccessMonto.Text = ltBody2GeCIPMonto.Text
    '        ltBody3GenSuccessMonedaDesc.Text = ltBody2GeCIPMonedaDesc.Text
    '        'ltMonedaDesc.Text = ob
    '        ltBody3GenSuccessServicioNombre.Text = ltBody2GeCIPServicioNombre.Text
    '        ltBody3GenSuccessUsuarioNombre.Text = SPE.Web.PaginaBase.UserInfoNombreOAlias()
    '    End Sub
    '    'Private Function DameMonedaDesc(ByVal idmoneda As String) As String
    '    '    Dim admComun As New SPE.Web.CAdministrarComun()

    '    'End Function

    '    Private Sub MostrarVistaGeCIP2()



    '        ValidarContratoServicios()


    '        Dim trans As SPE.EmsambladoComun.TraductorServ = New SPE.EmsambladoComun.TraductorServ(StrRutaServ, Request.RawUrl.Replace("&lg=2", ""), "")
    '        Dim obeOrdenPago As New BEOrdenPago
    '        Try
    '            obeOrdenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
    '            obeOrdenPago.IdCliente = UserInfo.IdUsuario ''Recuperando el Id de Cliente
    '            obeOrdenPago.IdServicio = trans.IdServicioValor
    '            obeOrdenPago.IdMoneda = _3Dev.FW.Util.DataUtil.StringToInt(trans.MonedaValor)
    '            obeOrdenPago.OrderIdComercio = trans.OrdenIdValor
    '            obeOrdenPago.MerchantID = trans.MerchantIdValor
    '            obeOrdenPago.UrlOk = trans.UrlOkValor
    '            obeOrdenPago.UrlError = trans.UrlErrorValor
    '            obeOrdenPago.MailComercio = trans.MailComValor
    '            If trans.MontoValor <> "" Or trans.MontoValor <> Nothing Then
    '                obeOrdenPago.Total = SPE.EmsambladoComun.TraductorServ.ObtenerMontoDeFormatoServicio(trans.MontoValor)
    '            Else
    '                Throw New SPE.Exceptions.TramaGetException("monto")
    '            End If

    '            obeOrdenPago.UsuarioID = trans.UsuarioIDValor
    '            obeOrdenPago.DataAdicional = trans.DataAdicionalValor
    '            obeOrdenPago.UsuarioNombre = trans.UsuarioNombreValor
    '            obeOrdenPago.UsuarioApellidos = trans.UsuarioApellidosValor
    '            obeOrdenPago.UsuarioLocalidad = trans.UsuarioLocalidadValor
    '            obeOrdenPago.UsuarioDomicilio = trans.UsuarioDomicilioValor
    '            obeOrdenPago.UsuarioProvincia = trans.UsuarioProvinciaValor
    '            obeOrdenPago.UsuarioPais = trans.UsuarioPaisValor
    '            obeOrdenPago.UsuarioAlias = trans.UsuarioAliasValor
    '            obeOrdenPago.DescripcionCliente = UserInfo.Email
    '            Dim cAdmComun As New SPE.Web.CAdministrarComun()
    '            obeOrdenPago.FechaEmision = cAdmComun.ConsultarFechaHoraActual()

    '            'Detalle Orden Pago
    '            Dim obeDetalle As New BEDetalleOrdenPago

    '            obeDetalle.ConceptoPago = trans.OrdenDescValor
    '            obeDetalle.Importe = obeOrdenPago.Total

    '            'Detalle
    '            obeOrdenPago.DetallesOrdenPago = New List(Of BEDetalleOrdenPago)
    '            obeOrdenPago.DetallesOrdenPago.Add(obeDetalle)

    '            _OrdenPago = obeOrdenPago

    '        Catch ex2 As SPE.Exceptions.TramaGetException
    '            _3Dev.FW.Web.Log.Logger.LogException(ex2)
    '            lblMensaje.Text = ex2.Message
    '            lblMensaje.CssClass = "MensajeValidacion"
    '        Catch ex As Exception
    '            Throw New SPE.Exceptions.URLNoEncontradaExcepcion(ex.Message)
    '        End Try
    '        Try
    '            trans.ValidarCamposRequeridos()

    '            Dim obeServicio As New BEServicio
    '            'If trans.IdServicioValor = "" Or trans.IdEmpresaValor = "" Then
    '            Dim objCServicio As New SPE.Web.CServicio
    '            obeServicio = objCServicio.ConsultarServicioPorUrl(trans.UrlServicioValor)
    '            'End If
    '            'obeServicio.IdServicio = Convert.ToInt32(trans.IdServicioValor)
    '            'obeServicio.IdEmpresaContratante = Convert.ToInt32(trans.IdEmpresaValor)


    '            If obeServicio.IdEmpresaContratante < 1 OrElse obeServicio.IdServicio < 1 Then
    '                imgBody2GeCIPServicio.Src = ""
    '            Else
    '                imgBody2GeCIPServicio.Src = "~/ADM/ADServImg.aspx?servicioid=" + obeServicio.IdServicio.ToString()
    '            End If

    '            Dim objCComun As New SPE.Web.CAdministrarComun : Dim obeMonedas As New SPE.Entidades.BEMoneda
    '            obeMonedas = objCComun.ConsultarMonedaPorId(trans.MonedaValor)

    '            ltBody2GeCIPMonto.Text = obeMonedas.Simbolo + "  " + obeOrdenPago.Total.ToString("#,#0.00")
    '            ltBody2GeCIPMonedaDesc.Text = ObjectToString(obeMonedas.Descripcion).ToLower()
    '            ltBody2GeCIPConcepto.Text = trans.OrdenDescValor
    '            ltBody2GeCIPUsuarioNombre.Text = SPE.Web.PaginaBase.UserInfoNombreOAlias()
    '            ltBody2GeCIPServicioNombre.Text = obeServicio.Nombre
    '            ltBody3GenSuccessServicioNombre.Text = obeServicio.Nombre
    '            'aqui va lo de imprimir.

    '            If Not (obeOrdenPago.Total <= 0) Then
    '                btnFooter2Generar.Visible = True
    '            Else
    '                btnFooter2Generar.Visible = False
    '                lblMensaje.Text = "El monto no puede ser menor o igual a cero. Intente nuevamente."
    '                lblMensaje.CssClass = "MensajeValidacion"
    '                ' exit
    '            End If

    '            MostrarVista("2", True)
    '            ltTitulo.Text = " Genera tu c�digo de identificaci�n de pago (CIP)"

    '        Catch ex2 As SPE.Exceptions.TramaGetException
    '            _3Dev.FW.Web.Log.Logger.LogException(ex2)
    '            lblMensaje.Text = ex2.Message
    '            lblMensaje.CssClass = "MensajeValidacion"
    '        Catch ex1 As SPE.Exceptions.URLNoEncontradaExcepcion
    '            _3Dev.FW.Web.Log.Logger.LogException(ex1)
    '            lblMensaje.Text = ex1.Message
    '            lblMensaje.CssClass = "MensajeValidacion"
    '        Catch ex3 As Exception
    '            _3Dev.FW.Web.Log.Logger.LogException(ex3)
    '            lblMensaje.Text = New SPE.Exceptions.GeneralConexionException(ex3.Message).CustomMessage
    '            lblMensaje.CssClass = "MensajeValidacion"

    '        End Try



    '    End Sub
    '    Private Sub MostrarVistaGePaLo1()
    '        Try

    '            ValidarContratoServicios()
    '            'CType(Login1.FindControl("UserName"), TextBox).Focus()

    '            Dim trans As SPE.EmsambladoComun.TraductorServ = New SPE.EmsambladoComun.TraductorServ(StrRutaServ, Request.RawUrl.ToString, UrlServicio)
    '            Dim idservicio As String = ""
    '            Dim servicioNombre As String = ""
    '            If UrlServicio <> "" Then
    '                Dim obeServicio As New BEServicio
    '                Dim objCServicio As New SPE.Web.CServicio
    '                obeServicio = objCServicio.ConsultarServicioPorUrl(UrlServicio)
    '                servicioNombre = obeServicio.Nombre
    '                If obeServicio Is Nothing Then
    '                    Response.Redirect("~/ServicioNoEncontrado.aspx")
    '                Else
    '                    idservicio = obeServicio.IdServicio
    '                End If
    '                ActualizarTramaRequest(trans, obeServicio)
    '            Else
    '                Response.Redirect("~/ServicioNoEncontrado.aspx")
    '            End If

    '            ltTitulo.Text = " Realiza tu pago con PagoEfectivo"
    '            ltBody1GePaLoServicioNombre.Text = servicioNombre
    '            imgBody1GePaLoServicio.Src = "~/ADM/ADServImg.aspx?servicioid=" + idservicio.ToString()
    '            MostrarVista("1", True)
    '            hplinkRegistro.NavigateUrl = "~/CNT/PRRegCl.aspx?sn=" + servicioNombre

    '        Catch ex As Exception
    '            Logger.LogException(ex)
    '            WebMessage.ShowErrorMessage(ex, True)
    '        End Try

    '    End Sub

    '    Private Sub ActualizarTramaRequest(ByVal trans As SPE.EmsambladoComun.TraductorServ, ByVal obeServicio As BEServicio)


    '        Dim sbparUtil As New StringBuilder
    '        Dim objclsLibreriax As New SPE.Utilitario.ClsLibreriax
    '        Dim sbparametersUrl As New StringBuilder

    '        sbparUtil.Append("parIdServicio=") : sbparUtil.Append(obeServicio.IdServicio.ToString) : sbparUtil.Append("|")
    '        sbparUtil.Append("parIdEmpresa=") : sbparUtil.Append(obeServicio.IdEmpresaContratante.ToString) : sbparUtil.Append("|")
    '        sbparUtil.Append("parUsaEncriptacion=") : sbparUtil.Append(trans.UsaEncriptacionValor) : sbparUtil.Append("|")
    '        sbparUtil.Append("parUrlServicio=") : sbparUtil.Append(UrlServicio.ToString)
    '        If (trans.UsaEncriptacionValor) Then
    '            'sbparametersUrl.Append(trans.MerchantId) : sbparametersUrl.Append("=") : sbparametersUrl.Append(MerchantID) : sbparametersUrl.Append("&")
    '            sbparametersUrl.Append(trans.DatosEnc) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.DatosEncValor) : sbparametersUrl.Append("&")
    '        Else
    '            'sbparametersUrl.Append(trans.MerchantId) : sbparametersUrl.Append("=") : sbparametersUrl.Append(MerchantID) : sbparametersUrl.Append("&")
    '            sbparametersUrl.Append(trans.Monto) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.MontoValor) : sbparametersUrl.Append("&")
    '            sbparametersUrl.Append(trans.UsuarioID) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.UsuarioIDValor) : sbparametersUrl.Append("&")
    '            sbparametersUrl.Append(trans.UsuarioAlias) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.UsuarioAliasValor) : sbparametersUrl.Append("&")
    '        End If
    '        sbparametersUrl.Append("parUtil") : sbparametersUrl.Append("=") : sbparametersUrl.Append(objclsLibreriax.Encrypt(sbparUtil.ToString, objclsLibreriax.fEncriptaKey))

    '        Login1.DestinationPageUrl = "PRGeCIP.aspx?" + sbparametersUrl.ToString + "&lg=2"

    '    End Sub

    '    Public Sub OcultarTodosLosPaneles(ByVal flag As Boolean)
    '        MostrarVista("1", Not flag)
    '        MostrarVista("2", Not flag)
    '        MostrarVista("3", Not flag)
    '    End Sub

    '    Public Sub MostrarVista(ByVal panelId As String, ByVal flag As Boolean)
    '        Select Case panelId
    '            Case "1"
    '                pnlBody1GePaLo.Visible = flag
    '                pnlBody1GePaLo.Enabled = flag
    '                pnlFooter1GePaLo.Visible = flag
    '            Case "2"
    '                pnlBody2GeCIP.Visible = flag
    '                pnlBody2GeCIP.Enabled = flag
    '                pnlFooter2GeCIP.Visible = flag
    '            Case "3"
    '                pnlBody3GenSuccess.Visible = flag
    '                pnlBody3GenSuccess.Enabled = flag
    '                pnlFooter3GenSuccess.Visible = flag
    '        End Select
    '    End Sub

    '    Public Sub GoToPanel(ByVal panelid As String)
    '        OcultarTodosLosPaneles(True)
    '        Select Case panelid
    '            Case "1"
    '                hdnCurrentPanel.Value = "1"
    '                MostrarVistaGePaLo1()
    '            Case "2"

    '                hdnCurrentPanel.Value = "2"
    '                MostrarVistaGeCIP2()
    '            Case "3"

    '                hdnCurrentPanel.Value = "3"
    '                MostrarGenSuccess3()
    '        End Select

    '    End Sub

    '#End Region


    '#Region "Propiedades"
    '    Private Property UrlServicio() As String
    '        Get
    '            Return ViewState("UrlServicio")
    '        End Get
    '        Set(ByVal value As String)
    '            ViewState("UrlServicio") = value
    '        End Set
    '    End Property

    '    Private Property StrRutaServ() As String
    '        Get
    '            Return ViewState("StrRutaServ")
    '        End Get
    '        Set(ByVal value As String)
    '            ViewState("StrRutaServ") = value
    '        End Set
    '    End Property

    '    Private Property _OrdenPago() As BEOrdenPago
    '        Get
    '            Return ViewState("_OrdenPago")
    '        End Get
    '        Set(ByVal value As BEOrdenPago)
    '            ViewState("_OrdenPago") = value
    '        End Set
    '    End Property

    Private Property FirstPostBack() As Boolean
        Get
            If ViewState("_FirstPostBack") Is Nothing Then
                ViewState("_FirstPostBack") = False
            End If
            Return ViewState("_FirstPostBack")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_FirstPostBack") = value
        End Set
    End Property
    '#End Region

    'Private Sub AsignarHyperlinksUrls()
    '    hlinkCentrosDeCobro.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlConoceCentrosCobro)
    '    hlinkTerminosYCondicionDeUso.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlTerminosYCondicionesUso)
    'End Sub

End Class
