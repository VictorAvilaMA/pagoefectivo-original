<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRGeCIP2.aspx.vb" Inherits="CNT_PRGeCIP2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PagoEfectivo</title>
    <link id="Link1" rel="stylesheet" runat="server" type="text/css" href="style/peconnect/print.css"  media="print" />
    <link id="Link2" rel="stylesheet" runat="server" type="text/css" href="style/peconnect/screen.css" media="screen"/>    
    <!--[if IE]>
	    <link rel="stylesheet" runat="server" href="~/style/peconnect/_ie-only.css" type="text/css" media="screen" />
    <![endif]-->    
     <script type="text/javascript" >
     
    function OnCloseCommon(msg)
    {
     if(window.confirm(msg))
      return window.close();
     else 
     return false;
    }
         
    function OnCancel()
    {
    return OnCloseCommon("Est� seguro que desea cancelar el proceso de generaci�n de CIP");
    }
    
     </script>    
</head>
<body id="body_home" class="login">
    <form id="form1" runat="server" class="dlgrid h100 normalize">
      <asp:ScriptManager id="ScriptManager" runat="server">
      </asp:ScriptManager>            
      <div id="wrapper">
         <div id="header">

           <h1> 
               <asp:Literal ID="ltTitulo" runat="server" Text="Realiza tu pago con PagoEfectivo"></asp:Literal> 
           </h1>
         </div>
           <div id="main">
              <div id="content" >

                       
                        <asp:Panel ID="pnlBody2GeCIP" runat="server">
                                <h4><strong>Bienvenido, 
                                    <asp:Literal ID="ltBody2GeCIPUsuarioNombre" runat="server"></asp:Literal>!</strong></h4>
                                <p>Acabas de solicitar la generaci�n de tu c�digo de Identificaci�n de Pago para   <strong><asp:Literal ID="ltBody2GeCIPServicioNombre" runat="server"></asp:Literal></strong> con los siguientes detalles:</p>
                                <fieldset class="dlinline loged">
                                <div class="tikibox clearfix mb">
                                  <dl class="clearfix w100">
                                    <dt>
                                      <label>Servicio</label>
                                    </dt>
                                    <dd> <img id="imgBody2GeCIPServicio" height="51" runat="Server"  src="" class="thumb_servproc" alt="Servicio Neoauto" /> </dd>
                                  </dl>
                                  <dl class="clearfix w100">
                                    <dt>
                                      <label>Concepto</label>
                                    </dt>
                                    <dd> 
                                        <asp:Literal ID="ltBody2GeCIPConcepto" runat="server"></asp:Literal>
                                    </dd>
                                  </dl>
                                  <dl class="clearfix w100 plastitem">
                                    <dt>
                                      <label>A pagar</label>
                                    </dt>
                                    <dd> 
                                       <strong class="price"><asp:Literal ID="ltBody2GeCIPMonto" runat="server"></asp:Literal> </strong> <asp:Literal ID="ltBody2GeCIPMonedaDesc" runat="server"></asp:Literal>
                                    </dd>
                                  </dl>
                                </div>
                                </fieldset>
                                <p class="legal lastitem">Al hacer clic en "Generar" accepto los <asp:HyperLink ID="hlinkTerminosYCondicionDeUso" runat ="server" Target="_blank" ToolTip="Leer t�rminos y condiciones">t�rminos y condiciones</asp:HyperLink> de uso de <strong>PagoEfectivo</strong>.</p>
                        </asp:Panel>
                        
                        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion" EnableViewState="false"></asp:Label>

              </div>
           </div>
    	 <!-- no borrar div push -->
         <div class="push"></div>
      </div>
      <div id="footer">
                              
            <asp:Panel ID="pnlFooter2GeCIP" runat="server">
                <button type="button" id="btnFooter2Cancelar" runat="server" class="close float_right ml" title="Cancelar" onclick="return OnCancel();">Cancelar</button>
                <asp:Button ID="btnFooter2Generar" runat="server" Text="Generar"  CssClass="submit float_right" OnClick="btnFooter2Generar_Click"  />
            </asp:Panel>
                       


      </div>
    </form>
</body>
</html>
