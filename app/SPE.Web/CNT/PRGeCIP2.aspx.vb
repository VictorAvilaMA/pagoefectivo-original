Imports SPE.EmsambladoComun.ParametrosSistema

Imports SPE.Web.Seguridad
Imports SPE.Entidades
Imports SPE.Utilitario

Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil

Partial Class CNT_PRGeCIP2
    Inherits System.Web.UI.Page

#Region "Atributos"
    Protected WebMessage As WebMessage
    Private Const __Respuesta As String = "__Respuesta"
    Private Const __UrlServicio As String = "__UrlServicio"
#End Region

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        WebMessage = New WebMessage(lblMensaje)
        WebMessage.Clear()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            AsignarHyperlinksUrls()
            If Not Context.User Is Nothing Then
                If Context.User.IsInRole(RolCliente) = False Then
                    Response.Redirect("~/Default.aspx")
                End If
            End If
            Dim urlServicio As String = Session(SPE.Web.Util.UtilGenCIP.__CNTUrlServicio)
            Dim strTrama As String = Session(SPE.Web.Util.UtilGenCIP.__CNTUrlTrama)
            MostrarVistaGeCIP2(urlServicio, strTrama)

        End If
    End Sub

    Private Sub MostrarVistaGeCIP2(ByVal urlServicio As String, ByVal strTrama As String)
        Try

            Dim oberesponse As BEObtCIPInfoByUrLServResponse = Nothing
            oberesponse = Session(SPE.Web.Util.UtilGenCIP.__CNTObtCIPInfoByUrLServRequest)
            If (oberesponse Is Nothing) Then
                Using ocOrdenPago As New SPE.Web.COrdenPago()
                    Dim oberequest As New BEObtCIPInfoByUrLServRequest()
                    oberequest.UrlServicio = urlServicio
                    oberequest.Trama = strTrama
                    oberequest.CargarTramaSolicitud = True
                    oberequest.TipoTrama = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.Request
                    oberesponse = ocOrdenPago.ObtenerCIPInfoByUrLServicio(oberequest)
                End Using

            End If

            If (oberesponse IsNot Nothing) Then
                Session(SPE.Web.Util.UtilGenCIP.__CNTObtCIPInfoByUrLServRequest) = oberesponse
                If oberesponse.OcultarEmpresa.idEmpresaContratante < 1 OrElse oberesponse.OcultarEmpresa.idServicio < 1 Then
                    imgBody2GeCIPServicio.Src = ""
                Else
                    imgBody2GeCIPServicio.Src = "~/ADM/ADServImg.aspx?servicioid=" + ObjectToString(oberesponse.OcultarEmpresa.idServicio)
                End If
                If (oberesponse.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.CodigoMonedaIncorrecto) Then
                    WebMessage.ShowErrorMessage(oberesponse.Message)
                    Throw New Exception(oberesponse.Message)
                End If
                If (oberesponse.OrdenPago IsNot Nothing) Then

                    ltBody2GeCIPMonto.Text = oberesponse.Moneda.Simbolo + "  " + oberesponse.OrdenPago.Total.ToString("#,#0.00")
                    ltBody2GeCIPMonedaDesc.Text = ObjectToString(oberesponse.Moneda.Descripcion).ToLower()
                    ltBody2GeCIPConcepto.Text = oberesponse.OrdenPago.ConceptoPago
                    ltBody2GeCIPUsuarioNombre.Text = SPE.Web.PaginaBase.UserInfoNombreOAlias()
                    ltBody2GeCIPServicioNombre.Text = oberesponse.OcultarEmpresa.ServicioDescripcion
                    'ltBody3GenSuccessServicioNombre.Text = oberesponse.OcultarEmpresa.ServicioDescripcion
                    'aqui va lo de imprimir.

                    If Not (oberesponse.OrdenPago.Total <= 0) Then
                        btnFooter2Generar.Visible = True
                    Else
                        btnFooter2Generar.Visible = False
                        lblMensaje.Text = "El monto no puede ser menor o igual a cero. Intente nuevamente."
                        lblMensaje.CssClass = "MensajeValidacion"
                        ' exit
                    End If

                    ViewState(__Respuesta) = oberesponse
                    ViewState(__UrlServicio) = urlServicio
                    ltTitulo.Text = " Genera tu código de identificación de pago (CIP)"
                End If
            End If


        Catch ex As Exception
            'Logger.LogException(ex)
            WebMessage.ShowErrorMessage(ex, True)
        End Try
    End Sub
    Protected Sub btnFooter2Generar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerarOrdenPago()
    End Sub

    Private Sub GenerarOrdenPago()
        Dim obeResGenCIP As BEGenCIPResponse = Nothing
        Try
            Dim obeRespuesta As BEObtCIPInfoByUrLServResponse = ViewState(__Respuesta)
            If (obeRespuesta IsNot Nothing) Then
                'Dim esquemaTrama As EsquemaTrama = obeRespuesta.EsquemaTrama
                ''If (esquemaTrama IsNot Nothing) Then
                ''    'lnkVolverServicio.NavigateUrl = esquemaTrama.Home
                ''    'lnkVolverServicio.Text = String.Format("Volver al Servicio ({0})", obeRespuesta.OcultarEmpresa.ServicioDescripcion)
                ''    'lnkVolverServicio.Visible = True
                ''End If

                Using oCOrdenPago As New SPE.Web.COrdenPago
                    Dim request As New BEGenCIPRequest()
                    request.OrdenPago = obeRespuesta.OrdenPago
                    request.OrdenPago.IdUsuarioCreacion = UserInfo.IdUsuario
                    request.OrdenPago.EmailANotifGeneracion = UserInfo.Email
                    request.OrdenPago.IdEmpresa = obeRespuesta.OcultarEmpresa.idEmpresaContratante
                    request.OrdenPago.IdServicio = obeRespuesta.OcultarEmpresa.idServicio
                    request.OrdenPago.TiempoExpiracion = obeRespuesta.Servicio.TiempoExpiracion
                    obeResGenCIP = oCOrdenPago.GenerarCIP(request)
                    If (obeResGenCIP IsNot Nothing) Then



                        If ((obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGeneracionOk) Or (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnterior)) Then
                            Session(SPE.Web.Util.UtilGenCIP.__CNTResponseGenCIP) = obeResGenCIP
                            Response.Redirect("~/CNT/PRGeCIP3.aspx")
                        ElseIf (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorOtroUsuario) Then
                            WebMessage.Initialize(lblMensaje)
                            WebMessage.ShowErrorMessage(obeResGenCIP.Message, False)
                        End If
                    End If

                End Using

            End If
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            lblMensaje.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
            lblMensaje.CssClass = "MensajeValidacion"
        End Try

    

    End Sub

    Private Sub AsignarHyperlinksUrls()
        'hlinkCentrosDeCobro.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlConoceCentrosCobro)
        hlinkTerminosYCondicionDeUso.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlTerminosYCondicionesUso)
    End Sub
End Class
