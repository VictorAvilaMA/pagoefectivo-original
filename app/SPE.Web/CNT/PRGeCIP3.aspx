<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRGeCIP3.aspx.vb" Inherits="CNT_PRGeCIP3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PagoEfectivo</title>
    <link id="Link1" rel="stylesheet" runat="server" type="text/css" href="style/peconnect/print.css" media="print" />
    <link id="Link2" rel="stylesheet" runat="server" type="text/css" href="style/peconnect/screen.css" media="screen"/>    
    <!--[if IE]>
	    <link rel="stylesheet" runat="server" href="~/style/peconnect/_ie-only.css" type="text/css" media="screen" />
    <![endif]-->    
    <script  type="text/javascript" >
    function OnClose()
    {
    return window.close();
    }
    </script>
</head>
<body id="body_home" class="login">
    <form id="form1" runat="server" class="dlgrid h100 normalize">
     
      <div id="wrapper">
         <div id="header">

           <h1> 
               <asp:Literal ID="ltTitulo" runat="server" Text="Realiza tu pago con PagoEfectivo"></asp:Literal> 
           </h1>
         </div>
           <div id="main">
              <div id="content" >
            
                        
                                <h4><strong>Gracias, <asp:Literal ID="ltBody3GenSuccessUsuarioNombre" runat="server"></asp:Literal>!</strong></h4>
                                <p> <asp:Literal ID="ltBody3GenSuccessMsgProcesoResultado" runat="server"> Se ha generado el c�digo de identificaci�n de pago (CIP) </asp:Literal>  <strong></strong> para 
                                    <strong>
                                     <asp:Literal ID="ltBody3GenSuccessServicioNombre" runat="server"></asp:Literal>
                                    </strong>                               
                                </p>
                                <fieldset class="dlinline">
	                            <p class="center codenum black">
	                               <strong>
                                       <asp:Literal ID="ltBody3GenSuccessNroOrdenPago" runat="server"></asp:Literal>
                                    </strong>
                                 </p>
                                <div class="barcode"> 
                                   <img id="imgBody3GenSuccessCodeBar" runat="server" src="images/codebar.gif" alt="00000000000066" /> </div>
	                            <p class="center">Monto a pagar: <strong class="price black"> 
                                    <asp:Literal ID="ltBody3GenSuccessMonto" runat="server"></asp:Literal></strong>
                                    <asp:Literal ID="ltBody3GenSuccessMonedaDesc" runat="server"></asp:Literal></p>
                                </fieldset>
                                <p><asp:Literal ID="ltBody3GenSuccessMsgEnvioEmail" runat="server"> Una copia de la generaci�n del CIP ha sido enviada a su correo  </asp:Literal> </p>
                                <p class="lastitem">Puedes <a href="javascript:window.print()" title="Imprimir">imprimir tu c�digo</a> o ver el listado de <asp:HyperLink ID="hlinkCentrosDeCobro" runat ="server" Target="_blank" ToolTip="Ver centros de cobro">nuestros centros de cobro</asp:HyperLink>.</p>
                        
                        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion" EnableViewState="false"></asp:Label>

                     <asp:HiddenField ID="hdnCurrentPanel" runat="server" />
              </div>
           </div>
    	 <!-- no borrar div push -->
         <div class="push"></div>
      </div>
      <div id="footer">

                
            <asp:Panel ID="pnlFooter3GenSuccess" runat="server">
               <button type="button" id="btnFooter3Cerrar" runat="server" class="submit float_right" title="Cerrar" onclick="return OnClose();">Cerrar</button>
            </asp:Panel>
        
      </div>
    </form>
</body>
</html>
