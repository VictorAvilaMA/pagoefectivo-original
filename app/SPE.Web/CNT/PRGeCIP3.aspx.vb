Imports SPE.Web.Seguridad
Imports SPE.Entidades
Imports SPE.Utilitario

Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil


Partial Class CNT_PRGeCIP3
    Inherits System.Web.UI.Page

    Protected WebMessage As WebMessage
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        WebMessage = New WebMessage(lblMensaje)
        WebMessage.Clear()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            AsignarHyperlinksUrls()
            MostrarGeneracionSatisfactoria()
        End If

    End Sub

    Private Sub MostrarGeneracionSatisfactoria()
        ltBody3GenSuccessMsgEnvioEmail.Visible = False
        Dim obeResGenCIP As BEGenCIPResponse = Session(SPE.Web.Util.UtilGenCIP.__CNTResponseGenCIP)
        Dim obeObtCIPInfoByUrLServResponse As BEObtCIPInfoByUrLServResponse = Session(SPE.Web.Util.UtilGenCIP.__CNTObtCIPInfoByUrLServRequest)
        If (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGeneracionOk) Then

            imgBody3GenSuccessCodeBar.Src = "~/CLI/ADOPImg.aspx?numero=" + obeResGenCIP.NumeroOrdenPago
            imgBody3GenSuccessCodeBar.Alt = Me.ltBody3GenSuccessNroOrdenPago.Text
            ltBody3GenSuccessMsgEnvioEmail.Visible = True
        ElseIf (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnterior) Then
            ltBody3GenSuccessMsgProcesoResultado.Text = "el c�digo de identificaci�n de pago (CIP) ya se ha generado anteriormente "
            Me.ltBody3GenSuccessNroOrdenPago.Text = obeResGenCIP.NumeroOrdenPago
            imgBody3GenSuccessCodeBar.Src = "~/CLI/ADOPImg.aspx?numero=" + obeResGenCIP.NumeroOrdenPago
            imgBody3GenSuccessCodeBar.Alt = Me.ltBody3GenSuccessNroOrdenPago.Text

        ElseIf (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.GenerarCIPEstado.CIPGenAnteriorOtroUsuario) Then
            WebMessage.Initialize(lblMensaje)
            WebMessage.ShowErrorMessage(obeResGenCIP.Message, False)
        End If

        If (obeResGenCIP.State = SPE.EmsambladoComun.ParametrosSistema.OrdenPago.ObtCIPInfoByUrLServEstado.CodigoMonedaIncorrecto) Then
            WebMessage.ShowErrorMessage(obeResGenCIP.Message)
            Throw New Exception(obeResGenCIP.Message)
        End If


        ''MostrarVista("3", True)
        ltTitulo.Text = " La orden se gener� satisfactoriamente"
        ltBody3GenSuccessMonto.Text = obeObtCIPInfoByUrLServResponse.Moneda.Simbolo + "  " + obeObtCIPInfoByUrLServResponse.OrdenPago.Total.ToString("#,#0.00")
        ltBody3GenSuccessMonedaDesc.Text = obeObtCIPInfoByUrLServResponse.Moneda.Descripcion
        ' ''ltMonedaDesc.Text = ob
        ltBody3GenSuccessServicioNombre.Text = obeObtCIPInfoByUrLServResponse.OcultarEmpresa.ServicioDescripcion
        ltBody3GenSuccessUsuarioNombre.Text = SPE.Web.PaginaBase.UserInfoNombreOAlias()
        ltBody3GenSuccessNroOrdenPago.Text = obeResGenCIP.NumeroOrdenPago



        Session.Remove(SPE.Web.Util.UtilGenCIP.__CNTUrlTrama)
        Session.Remove(SPE.Web.Util.UtilGenCIP.__CNTUrlServicio)
        Session.Remove(SPE.Web.Util.UtilGenCIP.__CNTResponseGenCIP)
        Session.Remove(SPE.Web.Util.UtilGenCIP.__CNTObtCIPInfoByUrLServRequest)
    End Sub

    Private Sub AsignarHyperlinksUrls()
        hlinkCentrosDeCobro.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlConoceCentrosCobro)
        'hlinkTerminosYCondicionDeUso.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlTerminosYCondicionesUso)
    End Sub
End Class
