<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PRRegCl.aspx.vb" Inherits="CNT_PRRegCl"
    EnableEventValidation="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../UC/UCDropDate.ascx" TagName="ucDropDate" TagPrefix="spe" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="WebControlCaptcha" Namespace="WebControlCaptcha" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PagoEfectivo - Registrate</title>
    <link rel="stylesheet" type="text/css" runat="server" href='<%# "../style/peconnect/print.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        media="print" />
    <link rel="stylesheet" type="text/css" runat="server" href='<%# "../style/peconnect/screen.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        media="screen" />
    <!--[if IE]>
	    <link rel="stylesheet" href="~/style/peconnect/_ie-only.css"  runat="server" type="text/css" media="screen" />
    <![endif]-->
    <%--<script  type="text/javascript" src="../JScripts/peconnect/jsScript.js"></script>--%>

    <script type="text/javascript">
         function ValidarCaptcha()
         {
            if(document.getElementById('CaptchaControl1').value=="")
            {
                window.alert("Debes ingresar el c�digo de la imagen.")
                return false;
            }else
                return true;
         }
         function OnCloseCommon(msg)
         {            
             if(window.confirm(msg))
              return window.close();
             else 
             return false;           
         }
         function OnClose()
         {
            return OnCloseCommon("Est� seguro que desea cerrar la ventana de generaci�n de CIP");
         }
    </script>

</head>
<body id="body_home">
    <form id="form1" runat="server" class="dlgrid h100 normalize">
        <asp:ScriptManager ID="ScriptManager" runat="server">
        </asp:ScriptManager>
        <div id="wrapper">
            <div id="header">
                <h1>
                    Reg�strate en PagoEfectivo</h1>
            </div>
            <div id="main">
                <div id="content">
                    <!--Contenido-->
                    <asp:UpdatePanel ID="UpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlBodyRegistro1" runat="server">
                                <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" TargetControlID="txtapellidos"
                                    ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' ">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombres" runat="server" TargetControlID="txtnombre"
                                    ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' ">
                                </cc1:FilteredTextBoxExtender>
                                <p>
                                    Reg�strate en <strong>PagoEfectivo</strong> para generar c�digos de pago de <strong>
                                        <asp:Literal ID="ltBodyRegistro1ServicioNombre" runat="server"></asp:Literal></strong>
                                    y otros portales. Es gratis y cualquiera puede unirse.</p>
                                <p>
                                    <small>&raquo; Todos los campos del formulario son obligatorios.</small></p>
                                <fieldset class="dlinline registro">
                                    <dl class="clearfix w100">
                                        <dt>
                                            <label for="txtnombre">
                                                Nombre</label>
                                        </dt>
                                        <dd>
                                            <asp:TextBox ID="txtnombre" runat="server" CssClass="text" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtnombre"
                                                ErrorMessage="Ingrese su nombre.">*</asp:RequiredFieldValidator>
                                        </dd>
                                    </dl>
                                    <dl class="clearfix w100">
                                        <dt>
                                            <label for="txtapellidos">
                                                Apellidos</label>
                                        </dt>
                                        <dd>
                                            <asp:TextBox ID="txtapellidos" runat="server" CssClass="text" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ControlToValidate="txtapellidos"
                                                ErrorMessage="Ingrese su apellido." EnableTheming="True">*</asp:RequiredFieldValidator>
                                        </dd>
                                    </dl>
                                    <dl class="clearfix w100">
                                        <dt>
                                            <label for="txtemail">
                                                E-mail</label>
                                        </dt>
                                        <dd>
                                            <asp:TextBox ID="txtemail" runat="server" CssClass="text" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtemail" runat="server" ControlToValidate="txtemail"
                                                ErrorMessage="Debe ingresar un email.">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="rfvtxtemailvalidaformato" runat="server" ControlToValidate="txtemail"
                                                ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                        </dd>
                                    </dl>
                                    <dl class="clearfix w100">
                                        <dt>
                                            <label for="slcgenero">
                                                Genero</label>
                                        </dt>
                                        <dd>
                                            <asp:DropDownList ID="ddlGenero" runat="server" CssClass="w33">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvGenero" runat="server" ControlToValidate="ddlGenero"
                                                ErrorMessage="Seleccione su g�nero.">*</asp:RequiredFieldValidator>
                                        </dd>
                                    </dl>
                                    <dl class="clearfix w100">
                                        <dt>
                                            <label for="slcdia">
                                                Fecha de nacimiento</label>
                                        </dt>
                                        <dd>
                                            <spe:ucDropDate ID="ucFechaNacimiento" runat="server" />
                                        </dd>
                                    </dl>
                                    <dl class="clearfix w100">
                                        <dt>
                                            <label for="txtpswd">
                                                Contrase�a</label>
                                        </dt>
                                        <dd>
                                            <asp:TextBox ID="txtPassword" runat="server" CssClass="text" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                                ErrorMessage="Debe ingresar su contrase�a">*</asp:RequiredFieldValidator>
                                        </dd>
                                    </dl>
                                </fieldset>
                            </asp:Panel>
                            <asp:Panel ID="pnlBodyRegistro2" runat="server">
                                <p>
                                    Para completar tu registro debes ingresar el c�digo que aparece a continuaci�n:</p>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
                                    <ContentTemplate>
                                        <br />
                                        <cc2:CaptchaControl ID="CaptchaControl1" runat="server" CssClass="text" CaptchaMaxTimeout="100" LayoutStyle="Vertical"  Text="Ingrese el c�digo mostrado:" />
                                        <br />
                                        <div style="float:left;">
                                          <p class="sugesthelp lastitem">�No puedes leerlas? 
                                                <asp:LinkButton ID="lnkBodyRegistro2GenerarNuevoCaptchaImg" runat="server" OnClick="lnkBodyRegistro2GenerarNuevoCaptchaImg_Click" CausesValidation="false" >
                                                    <strong> Prueba con otro codigo!</strong>
                                                </asp:LinkButton>
                                          </p> 
                                        </div>     
                                        <div style="float:left">
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True" EnableViewState="false"
                                            ShowSummary="true"   />                                        
                                        </div>                                      
                                        
                                    </ContentTemplate>
                                    <Triggers>
                                       <asp:AsyncPostBackTrigger ControlID="btnFooterRegistro2Registrarte" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>
                            <asp:Panel ID="pnlBodyRegistro3" runat="server">
                                <h2 class="black">
                                    <strong>�Ya est�s casi registrado!</strong></h2>
                                <p>
                                    �Gracias por unirte a PagoEfectivo! Acabamos de enviarte un mensaje de confirmaci�n
                                    a <strong>
                                        <asp:Literal ID="ltBodyRegistro3UsuarioEmail" runat="server"></asp:Literal>
                                    </strong>.</p>
                                <p>
                                    Recuerda que para completar el proceso de registro es necesario que hagas clic en
                                    el enlace que aparece en dicho mensaje.</p>
                                <p>
                                    Una vez que hayas confirmado y configurado tu cuenta de <strong>PagoEfectivo</strong>,
                                    podr�s generar los c�digos de identificaci�n que necesites para
                                    <strong><asp:Literal ID="ltBodyRegistro3ServicioNombre" runat="server"></asp:Literal> </strong>
                                    o los otros portales.</p>
                                <fieldset class="dlinline ">
                                    <ul class="list_links list_plane">
                                        <li><asp:HyperLink ID="hlinkConoceMasPE" runat ="server" Target="_blank" ToolTip="Conoce m�s de PagoEfectivo">Conoce m�s de PagoEfectivo</asp:HyperLink></li>
                                        <li><asp:HyperLink ID="hlinkConoceCentroCobro" runat ="server" Target="_blank" ToolTip="Conoce nuestros centros de cobro">Conoce nuestros centros de cobro</asp:HyperLink></li>
                                        <li><asp:HyperLink ID="hlinkContactateAgentePE" runat ="server" Target="_blank" ToolTip="Cont�ctate con un Agente PagoEfectivo">Cont�ctate con un Agente PagoEfectivo</asp:HyperLink></li>
                                    </ul>
                                </fieldset>
                            </asp:Panel>
                            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion" EnableViewState="false"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                ShowSummary="False" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnFooterRegistro1Registrarte" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnFooterRegistro2Registrarte" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- no borrar div push -->
            <div class="push">
            </div>
        </div>
        <div id="footer">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlFooterRegistro1" runat="server">
                        <%--<button type="submit" class="submit float_right" title="Registrate">Registrate</button>--%>
                        <asp:Button ID="btnFooterRegistro1Registrarte" CssClass="submit float_right" runat="server"
                            Text="Reg�strate" OnClick="btnFooterRegistro1Registrarte_Click" />
                        <p class="float_left">
                            Al hacer clic en Reg�strate, aceptas haber le�do y estar de acuerdo con las
                            <br />
                            <asp:HyperLink ID="hlinkCondicionDeUso" runat ="server" Target="_blank" ToolTip="Ver condiciones de uso">Condiciones de uso</asp:HyperLink>
                             y la <asp:HyperLink ID="hlinkPoliticaPrivacidad" runat ="server" Target="_blank" ToolTip="ver pol�tica de privacidad.">Pol�tica de privacidad.</asp:HyperLink></p>
                    </asp:Panel>
                    <asp:Panel ID="pnlFooterRegistro2" runat="server">
                        <%--<button type="submit" class="submit float_right" title="Reg�strate">Reg�strate</button>--%>
                        <asp:Button ID="btnFooterRegistro2Registrarte" CssClass="submit float_right" runat="server" Text="Reg�strate" OnClick="btnFooterRegistro2Registrarte_Click" OnClientClick=""  />
                        <asp:LinkButton ID="lnkFooterRegistro2Volver" runat="server" CssClass="registrarse" OnClick="lnkFooterRegistro2Volver_Click"><strong>&laquo; Volver</strong></asp:LinkButton>
                    </asp:Panel>
                    <asp:Panel ID="pnlFooterRegistro3" runat="server">
                        <button type="submit" class="submit float_right" title="Generar" onclick="return OnClose();">
                            Cerrar</button>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:HiddenField ID="hdnCurrentPanel" runat="server" />
    </form>
</body>
</html>
