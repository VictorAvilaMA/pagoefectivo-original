Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log

Imports SPE.Entidades

Partial Class CNT_PRRegCl
    Inherits System.Web.UI.Page

#Region "Atributos"
    Protected WebMessage As WebMessage
#End Region

#Region "Propiedades"
    Private Property _ServicioNombre() As String
        Get

            Return ViewState("_ServicioNombre")
        End Get
        Set(ByVal value As String)
            ViewState("_ServicioNombre") = value
        End Set
    End Property

    Private _cntrlParametro As SPE.Web.CAdministrarParametro = Nothing
    Private ReadOnly Property CntrlParametro() As SPE.Web.CAdministrarParametro
        Get
            If (_cntrlParametro Is Nothing) Then
                _cntrlParametro = New SPE.Web.CAdministrarParametro()
            End If
            Return _cntrlParametro
        End Get
    End Property

#End Region

#Region "Eventos y metodos sobreescritos"


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        WebMessage = New WebMessage(lblMensaje)
        WebMessage.Clear()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Page.Header.DataBind()
            AsignarHyperlinksUrls()
            hdnCurrentPanel.Value = "1"
            _ServicioNombre = Request.QueryString("sn")
            GoToPanel(hdnCurrentPanel.Value)
            Page.MaintainScrollPositionOnPostBack = True
            ucFechaNacimiento.RefrescarVista()
            ucFechaNacimiento.PreMostrarCamposMandatorios()
        End If
    End Sub

    Protected Sub btnFooterRegistro1Registrarte_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If btnFooterRegistro1Registrarte.Visible Then


            Try
                If (ValidarRegistro()) Then
                    _BECliente = CreateBECliente()
                    GoToPanel("2")
                End If
            Catch ex As _3Dev.FW.Excepciones.FWBusinessException
                WebMessage.ShowErrorMessage(ex.Message)
            Catch ex As Exception
                'Logger.LogException(ex)
                WebMessage.ShowErrorMessage(ex, True)
            End Try
        Else
            WebMessage.ShowErrorMessage("El c�digo de seguridad que usted ingres� no es el correcto.")
        End If
    End Sub
    Protected Sub btnFooterRegistro2Registrarte_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Page.Validate()
            If (Page.IsValid) Then
                Dim ccliente As New SPE.Web.CCliente()
                If (ccliente.InsertRecord(_BECliente) = -100) Then
                    WebMessage.ShowErrorMessage(String.Format("El email ""{0}"" est� siendo usado por otro usuario. Haga click en Volver y utilice otro.", txtemail.Text.Trim()))
                    txtemail.Focus()
                Else
                    GoToPanel("3")
                End If
            End If
        Catch ex As _3Dev.FW.Excepciones.FWBusinessException
            WebMessage.ShowErrorMessage(ex.Message)
        Catch ex As Exception
            'Logger.LogException(ex)
            WebMessage.ShowErrorMessage(ex, True)
        End Try
    End Sub
#End Region

#Region "M�todos"

    Private Sub CargarComboGenero()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlGenero, CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero), "Descripcion", "Id", " ")
    End Sub

    Public Sub OcultarTodosLosPaneles(ByVal flag As Boolean)
        MostrarVista("1", Not flag)
        MostrarVista("2", Not flag)
        MostrarVista("3", Not flag)
    End Sub
    Public Sub MostrarVista(ByVal panelId As String, ByVal flag As Boolean)
        Select Case panelId
            Case "1"
                pnlBodyRegistro1.Visible = flag
                'pnlBodyRegistro1.Enabled = flag
                pnlFooterRegistro1.Visible = flag
                rfvPassword.Visible = flag
                'pnlFooterRegistro1.Enabled = flag
            Case "2"
                pnlBodyRegistro2.Visible = flag
                'pnlBodyRegistro2.Enabled = flag
                pnlFooterRegistro2.Visible = flag
                rfvPassword.Visible = False
                'pnlFooterRegistro2.Enabled = flag
            Case "3"
                pnlBodyRegistro3.Visible = flag
                'pnlBodyRegistro3.Enabled = flag
                pnlFooterRegistro3.Visible = flag
                rfvPassword.Visible = False
                'pnlFooterRegistro3.Enabled = flag
        End Select
    End Sub

    Public Sub GoToPanel(ByVal panelid As String)
        OcultarTodosLosPaneles(True)
        Select Case panelid
            Case "1"
                hdnCurrentPanel.Value = "1"
                MostrarRegistro1()
            Case "2"

                hdnCurrentPanel.Value = "2"
                MostrarRegistro2()
            Case "3"

                hdnCurrentPanel.Value = "3"
                MostrarRegistro3()
        End Select

    End Sub

    Public Sub MostrarRegistro1()
        ltBodyRegistro1ServicioNombre.Text = _ServicioNombre
        MostrarVista("1", True)
        CargarComboGenero()
    End Sub
    Public Sub MostrarRegistro2()

        MostrarVista("2", True)
    End Sub
    Public Sub MostrarRegistro3()
        ltBodyRegistro3UsuarioEmail.Text = txtemail.Text
        ltBodyRegistro3ServicioNombre.Text = _ServicioNombre
        MostrarVista("3", True)
    End Sub

    Private Property _BECliente() As BECliente
        Get
            Return ViewState("_BECliente")
        End Get
        Set(ByVal value As BECliente)
            ViewState("_BECliente") = value
        End Set
    End Property

    Public Function CreateBECliente() As SPE.Entidades.BECliente
        Dim beCliente As New SPE.Entidades.BECliente
        beCliente.Email = Me.txtemail.Text
        beCliente.Password = Me.txtPassword.Text
        beCliente.Nombres = Me.txtnombre.Text
        beCliente.Apellidos = Me.txtapellidos.Text
        beCliente.IdGenero = ddlGenero.SelectedValue
        beCliente.FechaNacimiento = ucFechaNacimiento.DameFechaSeleccionada()
        beCliente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroBasico
        Return beCliente
    End Function

    Protected Function ValidarEmail() As Boolean
        Dim vale As New SPE.Web.Seguridad.SPEMemberShip()
        Dim resultado = (New SPE.Web.Seguridad.SPEMemberShip()).ExistUser(txtemail.Text.Trim())
        If (resultado) Then
            WebMessage.ShowErrorMessage(String.Format("El email ""{0}"" est� siendo usado por otro usuario", txtemail.Text.Trim()))
            Return False
        Else
            Return True
        End If

        Return True
    End Function

    Protected Function ValidarRegistro() As Boolean
        Page.MaintainScrollPositionOnPostBack = True
        Validate()
        If (Page.IsValid And ucFechaNacimiento.FechaEsValida() And ValidarEmail()) Then

            Dim maxcaracteres As Integer = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings(SPE.EmsambladoComun.ParametrosSistema.MinCaracteresContrasenia))

            If txtPassword.Text.Trim.Length < maxcaracteres Then
                WebMessage.ShowErrorMessage(String.Format("Usted debe ingresar como m�nimo {0} caracteres para su contrase�a.", maxcaracteres.ToString()))
                'ThrowErrorMessage("Usted debe ingresar como m�nimo " + maxcaracteres.ToString() + " caracteres para su contrase�a.")
                txtPassword.Focus()
                Return False
                'ElseIf Not CaptchaControl1.CaptchaMaxTimeout() Then
                '    ThrowErrorMessage("El c�digo de seguridad que usted ingres� no es el correcto. ")
                '    Return False
                '    'ElseIf IsNumeric(txtTelefono.Text) = False Then
                '    '    ThrowErrorMessage("Ud. debe ingresar valores num�ricos, como n�mero telefonico.")
                '    '    txtTelefono.Focus()
                '    '    Return False
            Else
                Return True
            End If
        End If
        Return False

    End Function

#End Region

    Protected Sub lnkFooterRegistro2Volver_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        GoToPanel("1")
        Me.txtemail.Text = _BECliente.Email
        Me.txtPassword.Text = _BECliente.Password
        Me.txtnombre.Text = _BECliente.Nombres
        Me.txtapellidos.Text = _BECliente.Apellidos
        ddlGenero.SelectedValue = _BECliente.IdGenero
    End Sub
    Protected Sub lnkBodyRegistro2GenerarNuevoCaptchaImg_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Genera Postback ajax que refresca el nuevo captcha.
    End Sub

    Private Sub AsignarHyperlinksUrls()
        hlinkContactateAgentePE.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlContactateAgentePE)
        hlinkConoceCentroCobro.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlConoceCentrosCobro)
        hlinkConoceMasPE.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlConoceMasDePE)
        hlinkCondicionDeUso.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlCondicionesUso)
        hlinkPoliticaPrivacidad.NavigateUrl = SPE.Web.Util.UtilUrl.DameUrlAppConfig(SPE.EmsambladoComun.ParametrosSistema.UrlsApp.UrlPoliticaPrivacidad)
    End Sub

End Class
