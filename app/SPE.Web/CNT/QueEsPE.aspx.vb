﻿Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.Utilitario
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil

Partial Class QueEsPE
    Inherits System.Web.UI.Page

    Const QSCODIGO As String = "codigo"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim codigo As String = Request.QueryString(QSCODIGO)
        If codigo <> "" Then
            Dim moneda As String = Nothing
            Dim capi As String = Nothing
            Dim cclave As String = Nothing
            Try
                Using oclsLibreria As New ClsLibreriax()
                    codigo = oclsLibreria.Decrypt(codigo, oclsLibreria.fEncriptaKey)
                End Using
                Dim parametros As New Dictionary(Of String, String)
                For Each itemscodigo As String In codigo.Split("|")
                    Dim items As String() = itemscodigo.Split("=")
                    parametros.Add(items(0), items(1))
                Next
                moneda = parametros("moneda")
                capi = parametros("capi")
                cclave = parametros("cclave")
            Catch ex As Exception
                ResponderError("El código es incorrecto o no tiene el formato requerido.")
            End Try
            Using CntrlServicio As New CServicio()
                Dim oBEServicio As BEServicio = CntrlServicio.ConsultarPorAPIYClave(capi, cclave)
                If oBEServicio IsNot Nothing Then
                    Using CntrlPlantilla As New CPlantilla()
                        ltrQueEsPE.Text = CntrlPlantilla.ConsultarPLantillasXServicioQueEsPE(oBEServicio.IdServicio, moneda, New Dictionary(Of String, String), SPE.EmsambladoComun.ParametrosSistema.Plantilla.TipoVariacion.Standar)
                    End Using
                Else
                    ResponderError("El código API y la clave no son correctos.")
                End If
            End Using
            Return
        End If
        Response.Redirect("~/Default.aspx")
    End Sub

    Private Sub ResponderError(ByVal Mensaje As String, Optional ByVal Codigo As Integer = 403)
        Response.StatusCode = Codigo
        Response.Write(Mensaje)
        Response.End()
    End Sub

End Class
