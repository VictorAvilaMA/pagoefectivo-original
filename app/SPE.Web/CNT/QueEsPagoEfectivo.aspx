﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="QueEsPagoEfectivo.aspx.vb"
    Inherits="CNT_QueEsPagoEfectivo" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <title>Pago Efectivo - Transacciones Seguras por Internet en el Perú</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if lt IE 9]>
  <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/js/html5.js"></script>
<![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="perucomsultores.pe" />
    <meta name="viewport" content="width=device-width" />
    <meta name="language" content="es" />
    <meta name="geolocation" content="Peru" />
    <meta name="robots" content="index,follow" />
    <meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="images/pago-efectivo.png" />
    <style type="text/css">
        *
        {
            margin: 0;
            padding: 0;
        }
        body
        {
            background: transparent;
        }
        .pagoefectivo-lb
        {
            background: #fff;
            font-family: Arial,Helvetica,Tahoma,trebuchet MS;
            height: auto;
            display: block;
            float: left;
            width: 600px;
        }
        .pagoefectivo-lb .pagoefectivo-container
        {
            height: auto;
            width: 500px;
            display: block;
            float: left;
            margin: 5px;
        }
        .pagoefectivo-lb .pagoefectivo-container h2.title
        {
            color: #000;
            display: block;
            font-size: 20px;
            text-align:center;
            height: 40px;
        }
        .pagoefectivo-lb .pagoefectivo-container h2.title2
        {
            color: #000;
            display: block;
            font-size: 20px;
            text-align:center;
            height: 30px;
            margin-top:20px;
        }
        .pagoefectivo-lb .pagoefectivo-container p.description
        {
            color: #000;
            display: block;
            font-size: 12px;
            height: 45px;
        }
        .pagoefectivo-lb .pagoefectivo-container p.description2
        {
            color: #000;
            display: block;
            font-size: 12px;
            height: 30px;
            text-align:center;
            margin: 0px 0 0px 0;
        }
        .pagoefectivo-lb .pagoefectivo-container .imagen-centros 
        {
			text-align:center;
		}
        .pagoefectivo-lb .pagoefectivo-container .video-player-pagoefectivo
        {
            background: #000;
            display: block;
            height: 300px;
            margin: 0 0 0 22px;
            width: 500px;
        }
        .pagoefectivo-lb .pagoefectivo-container .video-player-pagoefectivo #pagoefectivo-video
        {
            display: block;
            height: 300px;
            width: 500px;
        }
        .pagoefectivo-lb .pagoefectivo-container .means-of-payment
        {
            display: block;
            height: 45px;
            width: 545px;
        }
        .pagoefectivo-lb .pagoefectivo-container .means-of-payment .left-pagoefectivo
        {
            display: block;
            float: left;
            height: 45px;
            width: 100px;
        }
        .pagoefectivo-lb .pagoefectivo-container .means-of-payment .right-pagoefectivo
        {
            color: #666;
            display: block;
            float: left;
            font-size: 11px;
            height: 45px;
            width: 421px;
            word-wrap: break-word;
        }
    </style>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/ResourcesVideoPortada/js/swfobject/swfobject.js?<%# System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
        type="text/javascript"></script>
    <script type="text/javascript">
        function checkVideo() {
            if (!!document.createElement('video').canPlayType) {

                var vidTest = document.createElement("video");
                var oggTest = vidTest.canPlayType('video/ogg; codecs="theora, vorbis"');

                if (!oggTest) {
                    h264Test = vidTest.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
                    if (!h264Test) {
                        return false;
                    } else {
                        if (h264Test == "probably") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    if (oggTest == "probably") {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        if (checkVideo() != true) {
            var params = {},
		flashvars = {},
		baseUrl_ = '<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/';
            //alert(baseUrl_);
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            params.wmode = "opaque";
            params.flashvars = "file=" + baseUrl_ + "video/1.flv&repeat=no&stretching=fill&skin=" + baseUrl_ + "swf/md.swf&autostart=false&bufferlength=1&image=" + baseUrl_ + "video/1.jpg";
            swfobject.embedSWF("" + baseUrl_ + "swf/playertv.swf", "pagoefectivo-video", "650", "390", "9", "" + baseUrl_ + "js/swfobject/expressInstall.swf", flashvars, params);
        }
        //window.onload = function () { document.getElementById('bcp-pe').style.display = 'block' };
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="pagoefectivo-lb">
        <div class="pagoefectivo-container">
           <h2 class="title">¿C&oacute;mo funciona PagoEfectivo?</h2>
            <div class="video-player-pagoefectivo" style="width: 500px; height: auto; border: 0px;">
                <%--<div id="pagoefectivo-video">
                    <object width="500" height="300" type="application/x-shockwave-flash" data="https://pagoefectivo.pe/App_Themes/SPE/ResourcesVideoPortada/swf/playertv.swf"
                        id="mediaplayer" style="visibility: visible;">
                        <param name="allowscriptaccess" value="always">
                        <param name="allowfullscreen" value="true">
                        <param name="wmode" value="opaque">
                        <param name="flashvars" value="file=https://pagoefectivo.pe/App_Themes/SPE/ResourcesVideoPortada/video/1.flv&amp;repeat=no&amp;stretching=fill&amp;skin=https://pagoefectivo.pe/App_Themes/SPE/ResourcesVideoPortada/swf/md.swf&amp;autostart=false&amp;bufferlength=1&amp;image=https://pagoefectivo.pe/App_Themes/SPE/ResourcesVideoPortada/video/1.jpg">
                    </object>
                </div>--%>
                <div id="pagoefectivo-video" style="width: 500px; height: auto; border: 0px;">
                    <!--<video controls="controls" poster="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                        height="300" width="500">-->
		                <!-- Firefox | Chrome -->
		                <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.webm" type='video/webm; codecs="vp8, vorbis"'/>-->
		                <!-- IE9 | iOS -->
		                <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>-->
		                <!-- Android -->
		                <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.ogv" type='video/ogg; codecs="theora, vorbis"'/>-->
		                <!-- Other -->
                        <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.m4v"/>-->
                        <iframe width="500px" height="300px" src="https://www.youtube.com/embed/N1NJabZgUKQ?autoplay=0&loop=1&modestbranding=1&rel=0" frameborder="0" allowfullscreen></iframe>
	                <!--</video>-->
                </div>
            </div>

            <div class="imagen-centros" style="margin-top:20px;margin-left:20px">
                <h2 class="title2">¿Dónde pagar?</h2>
                <p title="Banca por Internet, Agentes corresponsales y Agencias bancarias." class="description2">Banca por Internet, Agentes corresponsales y Agencias bancarias.</p>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/InfoPagos/centros-pago-pagoefectivo.jpg?v=16082013"></div>

            </div>
        </div>
    </form>
</body>
</html>
