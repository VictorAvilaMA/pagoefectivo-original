<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COHConc.aspx.vb" Inherits="COHConc" Title="PagoEfectivo - Consultar Conciliaciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True" AsyncPostBackTimeout="900">
    </asp:ScriptManager>
    <h2>
        Consultar Conciliaciones
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B&uacute;squeda
        </h4>
        <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de C.I.P. del:</li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorFecInicio" runat="server" IsValidEmpty="False"
                                InvalidValueMessage="Fecha Inicio no v�lida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                                EmptyValueMessage="*" EmptyValueBlurredText="*" ControlToValidate="txtFechaInicio"
                                ControlExtender="mdeFechaInicio">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaFin"
                                Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                Text="*"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorFecFin" runat="server" IsValidEmpty="False"
                                InvalidValueMessage="Fecha Fin no v�lida" InvalidValueBlurredMessage="*" EmptyValueMessage="*"
                                EmptyValueBlurredText="*" ControlToValidate="txtFechaFin" ControlExtender="mdeFechaFin">*
                            </cc1:MaskedEditValidator>
                            <div style="clear: both;">
                            </div>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="El Rango de fechas no es v�lido"
                                ControlToValidate="txtFechaFin" Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtFechaInicio">*
                            </asp:CompareValidator>
                        </p>
                    </li>
                </ul>
                <asp:ValidationSummary ID="ValidationSummary" runat="server"></asp:ValidationSummary>
                <cc1:CalendarExtender ID="calFechaInicio" runat="server" TargetControlID="txtFechaInicio"
                    PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="calFechaFin" runat="server" TargetControlID="txtFechaFin"
                    PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:MaskedEditExtender ID="mdeFechaInicio" runat="server" TargetControlID="txtFechaInicio"
                    MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="mdeFechaFin" runat="server" TargetControlID="txtFechaFin"
                    MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                </cc1:MaskedEditExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnRecalcular" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both;">
        </div>
        <ul class="datos_cip2 h25">
            <li id="fsBotonera" class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                <asp:Button ID="btnRecalcular" runat="server" CssClass="input_azul4" Text="Recalcular" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <asp:Label ID="lblResultado" runat="server" ForeColor="Navy"></asp:Label>
                    </div>
                    <h5 id="div5" runat="server">
                        <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label>
                    </h5>
                    <asp:GridView ID="gvResultado" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CssClass="grilla" DataKeyNames="Fecha" OnPageIndexChanging="gvResultado_PageIndexChanging"
                        OnRowCommand="gvResultado_RowCommand" PageSize="10">
                        <Columns>
                            <asp:TemplateField HeaderText="Ver">
                                <ItemTemplate>
                                    <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                        ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/b_consultar.gif" %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width ="10px" />
                            </asp:TemplateField>
                            <%--<asp:CommandField ButtonType="Image" HeaderText="Ver" SelectImageUrl="~/Images_Buton/b_consultar.gif"
                                SelectText="Ver" ShowSelectButton="True">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                            </asp:CommandField>--%>
                            <asp:BoundField DataField="Fecha" DataFormatString="{0:d}" HeaderText="Fecha" SortExpression="Fecha">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescEstado" HeaderText="Estado" SortExpression="DescEstado">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CantCIPS" HeaderText="Cant. CIPs" SortExpression="CantCIPS">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CantCIPSConciliados" HeaderText="Cant. Conc." SortExpression="CantCIPSConciliados">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaInicio" HeaderText="Fecha Inicio" SortExpression="FechaInicio">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <%--<asp:BoundField DataField="FechaFin" HeaderText="Fecha Fin" SortExpression="FechaFin">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>--%>
                            <asp:TemplateField HeaderText="Fecha Fin" SortExpression="FechaFin" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaFin"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Observacion" HeaderText="Observaci�n" SortExpression="Observacion">
                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera" />
                    </asp:GridView>
                    <div style="clear: both;">
                    </div>
                    <div align="center" style="margin-top: 30px; overflow: visible;">
                        <asp:Literal ID="NoReg" runat="server" Visible="false">
                            No se encontraron registros.
                        </asp:Literal></div>
                    <asp:HiddenField ID="_idEstado" runat="server" />
                    <asp:HiddenField ID="_fechaInicio" runat="server" />
                    <asp:HiddenField ID="_fechaFin" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
