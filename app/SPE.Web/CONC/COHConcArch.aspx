<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COHConcArch.aspx.vb" Inherits="COHConcArch"
    Title="PagoEfectivo - Consultar Conciliaciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <h2>
        Consultar Conciliaciones de Archivos</h2>
    <div class="conten_pasos3">
        <fieldset>
            <h4>
                Criterios de b�squeda</h4>
            <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Nombre de Archivo:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtNombreArchivo" runat="server" CssClass="normal"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Tipo :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlIdTipoConciliacion" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Origen :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlIdOrigenConciliacion" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Fecha de C.I.P. del:</li>
                        <li class="t2" style="line-height: 1; z-index: 200;">
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                <cc1:MaskedEditValidator ID="MaskedEditValidatorFecInicio" runat="server" IsValidEmpty="False"
                                    InvalidValueMessage="Fecha Inicio no v�lida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                                    EmptyValueMessage="*" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                                    ControlExtender="mdeFechaInicio">
                                </cc1:MaskedEditValidator>
                            </p>
                            <span class="entre">al: </span>
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                    Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                                    Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                <cc1:MaskedEditValidator ID="MaskedEditValidatorFecFin" runat="server" IsValidEmpty="False"
                                    InvalidValueMessage="Fecha Fin no v�lida" InvalidValueBlurredMessage="*" EmptyValueMessage="*"
                                    EmptyValueBlurredText="*" ControlToValidate="txtFechaA" ControlExtender="mdeFechaFin">*
                                </cc1:MaskedEditValidator>
                                <div style="clear: both;">
                                </div>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="El Rango de fechas no es v�lido"
                                    ControlToValidate="txtFechaA" Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtFechaDe">*
                                </asp:CompareValidator>
                            </p>
                        </li>
                    </ul>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server"></asp:ValidationSummary>
                    <cc1:CalendarExtender ID="calFechaInicio" runat="server" TargetControlID="txtFechaDe"
                        PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="calFechaFin" runat="server" TargetControlID="txtFechaA"
                        PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <cc1:MaskedEditExtender ID="mdeFechaInicio" runat="server" TargetControlID="txtFechaDe"
                        MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                    <cc1:MaskedEditExtender ID="mdeFechaFin" runat="server" TargetControlID="txtFechaA"
                        MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnVolver" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <div style="clear: both;">
            </div>
            <ul class="datos_cip2">
                <li class="complet">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    <asp:Button ID="btnVolver" runat="server" CssClass="input_azul4" Text="Volver" />
                </li>
            </ul>
        </fieldset>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" DataKeyNames="IdConciliacionArchivo,CodigoBanco" runat="server"
                            CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False" OnRowCommand="gvResultado_RowCommand"
                            OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="Ver">
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                            ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/b_consultar.gif" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/Images_Buton/b_consultar.gif"
                                    SelectText="Ver" ShowSelectButton="True" HeaderText="Ver">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>--%>
                                <asp:BoundField DataField="Fecha" DataFormatString="{0:d}" SortExpression="Fecha"
                                    HeaderText="Fecha">
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NombreArchivo" SortExpression="NombreArchivo" HeaderText="Nombre Archivo">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Banco" SortExpression="Banco" HeaderText="Banco">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescEstado" SortExpression="DescEstado" HeaderText="Estado">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CantCIPs" HeaderText="Cant. CIPs">
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CantCIPsConciliados" HeaderText="Cant. Conc.">
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescTipoConciliacion" SortExpression="DescTipoConciliacion"
                                    HeaderText="Tipo">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescOrigenConciliacion" SortExpression="DescOrigenConciliacion"
                                    HeaderText="Origen">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaInicio" SortExpression="FechaInicio" HeaderText="Fecha Inicio">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaFin" SortExpression="FechaFin" HeaderText="Fecha Fin">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Observacion" SortExpression="Observacion" HeaderText="Observacion">
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hdfIdConc" runat="server" />
                        <asp:HiddenField ID="hdfIdConcEnt" runat="server" />
                        <asp:HiddenField ID="hdfCodBanco" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnVolver" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
