Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class COHConcArch
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("Fecha") IsNot Nothing And _
                Request.QueryString("CodBan") IsNot Nothing Then
                Dim request As New BEConciliacionArchivo
                Dim _fecha As DateTime = Convert.ToDateTime(Context.Request.QueryString("Fecha"))
                If Context.Request.QueryString.AllKeys.Length > 0 Then
                    hdfIdConc.Value = Context.Request.QueryString("Fecha").ToString()
                    hdfCodBanco.Value = Context.Request.QueryString("CodBan").ToString()
                End If
                With request
                    .CodigoBanco = hdfCodBanco.Value
                    .NombreArchivo = ""
                    .IdTipoConciliacion = 0
                    .IdOrigenConciliacion = 0
                    .FechaInicio = _fecha
                    .FechaFin = _fecha
                End With
                txtFechaDe.Text = _fecha.ToShortDateString()
                txtFechaA.Text = _fecha.ToShortDateString()
                CargarGrilla(request)
            Else
                txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy")
                txtFechaA.Text = DateTime.Now.ToShortDateString()
            End If
            CargarCombos()
        End If
    End Sub
    Private Sub CargarCombos()
        Dim objCParametros As New SPE.Web.CAdministrarParametro
        Dim objCBanco As New SPE.Web.CBanco
        Dim oBEBanco As New BEBanco
        With oBEBanco
            .Codigo = ""
            .Descripcion = ""
            .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        End With
        WebUtil.DropDownlistBinding(ddlBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", "::: Todos :::")
        WebUtil.DropDownlistBinding(ddlEstado, objCParametros.ConsultarParametroPorCodigoGrupo("ESCO"), "Descripcion", "Id", "::: Todos :::")
        WebUtil.DropDownlistBinding(ddlIdTipoConciliacion, objCParametros.ConsultarParametroPorCodigoGrupo("TCON"), "Descripcion", "Id", "::: Todos :::")
        WebUtil.DropDownlistBinding(ddlIdOrigenConciliacion, objCParametros.ConsultarParametroPorCodigoGrupo("TOCO"), "Descripcion", "Id", "::: Todos :::")
        If Context.Request.QueryString.AllKeys.Length > 0 Then
            ddlBanco.SelectedValue = hdfCodBanco.Value
        End If
    End Sub
    Sub Limpiar()
        ddlBanco.SelectedIndex = 0
        ddlEstado.SelectedIndex = 0
        Me.txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'DateTime.Now.ToShortDateString()
        Me.txtFechaA.Text = DateTime.Now.ToShortDateString()
        Me.lblResultado.Text = ""
        gvResultado.Visible = False
        Me.gvResultado.DataSource = Nothing
        divResult.Visible = False
    End Sub
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        CargarGrilla(CrearEntidad())
        divResult.Visible = True
    End Sub
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Limpiar()
    End Sub
    Private Sub CargarGrilla(ByVal request As BEConciliacionArchivo)
        Dim CConc As New SPE.Web.CConciliacion
        Dim response As New List(Of BEConciliacionArchivo)
        response = CConc.ConsultarConciliacionArchivo(request)
        If response.Count = 0 Then
            gvResultado.Visible = False
            Me.gvResultado.DataSource = Nothing
            Me.lblResultado.Text = "No se encontraron registros"
        Else
            gvResultado.Visible = True
            Me.gvResultado.DataSource = response
            gvResultado.DataBind()
            Me.lblResultado.Text = "Se encontraron " & response.Count.ToString() & " registros"
        End If
    End Sub
    Function CrearEntidad() As BEConciliacionArchivo
        Dim request As New BEConciliacionArchivo
        With request
            .IdEstado = IIf(ddlEstado.SelectedIndex = 0, 0, _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue))
            .CodigoBanco = IIf(ddlBanco.SelectedIndex = 0, 0, ddlBanco.SelectedValue)
            .NombreArchivo = txtNombreArchivo.Text
            .IdTipoConciliacion = IIf(ddlIdTipoConciliacion.SelectedIndex = 0, 0, _3Dev.FW.Util.DataUtil.StringToInt(ddlIdTipoConciliacion.SelectedValue))
            .IdOrigenConciliacion = IIf(ddlIdOrigenConciliacion.SelectedIndex = 0, 0, _3Dev.FW.Util.DataUtil.StringToInt(ddlIdOrigenConciliacion.SelectedValue))
            .FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaDe.Text)
            .FechaFin = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaA.Text)
        End With
        Return request
    End Function
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            If Not e Is Nothing Then
                Dim indice As Integer = Convert.ToInt32(e.CommandArgument)
                Dim idConciliacionArchivo As String, fecConciliacion As String, codigoBanco As String
                Select Case (e.CommandName)
                    Case "Select"
                        idConciliacionArchivo = gvResultado.DataKeys(indice).Values(0).ToString()
                        codigoBanco = gvResultado.DataKeys(indice).Values(1).ToString()
                        fecConciliacion = gvResultado.Rows(indice).Cells(1).Text
                        Response.Redirect("~/CONC/COHDetConcBCP.aspx?IdConcArch=" & idConciliacionArchivo & "&Fecha=" & fecConciliacion & "&CodBan=" & codigoBanco)
                End Select
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnVolver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVolver.Click
        Response.Redirect("~/CONC/COHConcEnt.aspx?IdConc")
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvResultado.PageIndex = e.NewPageIndex
        CargarGrilla(CrearEntidad())
    End Sub

End Class