<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COHConcEnt.aspx.vb" Inherits="COHConcEnt" Title="PagoEfectivo - Consultar Conciliaciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <h2>
        Consultar Conciliaciones con los Bancos</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B&uacute;squeda</h4>
        <div class="dlgrid">
            <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Del:</li>
                        <li class="t2" style="line-height: 1; z-index: 200;">
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:ImageButton CssClass="calendario" ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                                    InvalidValueMessage="Fecha 'Del' no v�lida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                                    EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                                    ControlExtender="mdeDel"></cc1:MaskedEditValidator>
                            </p>
                            <span class="entre">al: </span>
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                    Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                                    Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:ImageButton CssClass="calendario" ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                <cc1:MaskedEditValidator CssClass="izq" ID="MaskedEditValidatorAl" runat="server"
                                    IsValidEmpty="False" InvalidValueMessage="Fecha 'Al' no v�lida" InvalidValueBlurredMessage="*"
                                    EmptyValueMessage="Fecha 'Al' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaA"
                                    ControlExtender="mdeA">*
                                </cc1:MaskedEditValidator>
                                <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                    ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                    Type="Date">*</asp:CompareValidator>
                                <div style="clear: both;">
                                </div>
                            </p>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server"></asp:ValidationSummary>
                    <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
                        Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
                        Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
                        Mask="99/99/9999" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                    <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
                        Mask="99/99/9999" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnVolver" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <ul class="datos_cip2">
                <li id="fsBotonera" class="complet">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    <asp:Button ID="btnVolver" runat="server" CssClass="input_azul4" Text="Volver" />
                </li>
            </ul>
            <div style="clear: both;" />
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" class="result" visible="false">
                        <h5 id="h5Resultado" runat="server">
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <div style="clear: both;">
                        </div>
                        <asp:GridView ID="gvResultado" DataKeyNames="Fecha,CodigoBanco" runat="server" CssClass="grilla"
                            AllowPaging="True" AutoGenerateColumns="False" OnRowCommand="gvResultado_RowCommand"
                            OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/Images_Buton/b_consultar.gif" SelectText="Ver" ShowSelectButton="True" HeaderText="Ver" >
										<ItemStyle HorizontalAlign="Center" Width="10px" />
									</asp:CommandField>--%>
                                <asp:BoundField DataField="Fecha" DataFormatString="{0:d}" SortExpression="Fecha"
                                    HeaderText="Fecha">
                                    <ItemStyle HorizontalAlign="Right" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Banco" SortExpression="Banco" HeaderText="Banco">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescEstado" SortExpression="DescEstado" HeaderText="Estado">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CantCIPS" SortExpression="CantCIPS" HeaderText="Cant. CIPs">
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CantCIPSConciliados" SortExpression="CantCIPSConciliados"
                                    HeaderText="Cant. Conc.">
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaInicio" SortExpression="FechaInicio" HeaderText="Fecha Inicio">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaFin" SortExpression="FechaFin" HeaderText="Fecha Fin">
                                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Observacion" SortExpression="Observacion" HeaderText="Observaci�n">
                                    <ItemStyle HorizontalAlign="Left" Width="100px" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="_codigoBanco" runat="server" />
                        <asp:HiddenField ID="_idEstado" runat="server" />
                        <asp:HiddenField ID="_fechaInicio" runat="server" />
                        <asp:HiddenField ID="_fechaFin" runat="server" />
                        <asp:HiddenField ID="hdfIdConc" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnVolver" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
