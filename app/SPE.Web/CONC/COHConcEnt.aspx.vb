Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class COHConcEnt
    Inherits PaginaBase
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("Fecha") IsNot Nothing Then
                Dim request As New BEConciliacionEntidad
                Dim _fecha As DateTime = Convert.ToDateTime(Context.Request.QueryString("Fecha"))
                If Context.Request.QueryString.AllKeys.Length > 0 Then
                    hdfIdConc.Value = Context.Request.QueryString("Fecha").ToString()
                End If
                With request
                    .CodigoBanco = "0"
                    .FechaInicio = _fecha
                    .FechaFin = _fecha
                End With
                txtFechaDe.Text = _fecha.ToShortDateString
                txtFechaA.Text = _fecha.ToShortDateString
                CargarGrilla(request)
            Else
                txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy")
                txtFechaA.Text = DateTime.Now.ToShortDateString()
            End If
            CargarCombos()
        End If
    End Sub
    Private Sub CargarCombos()
        Dim objCParametros As New SPE.Web.CAdministrarParametro
        Dim objCBanco As New SPE.Web.CBanco
        Dim oBEBanco As New BEBanco
        With oBEBanco
            .Codigo = ""
            .Descripcion = ""
            .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        End With
        WebUtil.DropDownlistBinding(ddlBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", "::: Todos :::")
        WebUtil.DropDownlistBinding(ddlEstado, objCParametros.ConsultarParametroPorCodigoGrupo("ESCO"), "Descripcion", "Id", "::: Todos :::")
    End Sub
    Sub Limpiar()
        ddlBanco.SelectedIndex = 0
        ddlEstado.SelectedIndex = 0
        Me.txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy")
        Me.txtFechaA.Text = DateTime.Now.ToShortDateString()
        Me.lblResultado.Text = ""
        Me.gvResultado.DataSource = Nothing
        divResult.Visible = False
    End Sub
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        _codigoBanco.Value = IIf(ddlBanco.SelectedIndex = 0, 0, ddlBanco.SelectedValue)
        _idEstado.Value = IIf(ddlEstado.SelectedIndex = 0, 0, _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue))
        _fechaInicio.Value = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaDe.Text)
        _fechaFin.Value = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaA.Text)
        CargarGrilla(CrearEntidad())
    End Sub
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Limpiar()
    End Sub
    Private Sub CargarGrilla(ByVal request As BEConciliacionEntidad)
        Dim CConc As New SPE.Web.CConciliacion
        Dim response As List(Of BEConciliacionEntidad) = CConc.ConsultarConciliacionEntidad(request)
        gvResultado.DataSource = response
        gvResultado.DataBind()
        lblResultado.Text = "Se encontraron " & response.Count.ToString() & " registros"
        divResult.Visible = True
    End Sub
    Function CrearEntidad() As BEConciliacionEntidad
        Dim request As New BEConciliacionEntidad
        With request
            .IdConciliacion = 0
            .CodigoBanco = _codigoBanco.Value
            .IdEstado = _idEstado.Value
            .FechaInicio = _fechaInicio.Value
            .FechaFin = _fechaFin.Value
        End With
        Return request
    End Function
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Try
            If Not e Is Nothing Then
                Dim indice As Integer = Convert.ToInt32(e.CommandArgument)
                Dim Fecha As String, codBanco As String
                Select Case (e.CommandName)
                    Case "Select"
                        Fecha = Convert.ToDateTime(gvResultado.DataKeys(indice).Values(0).ToString())
                        codBanco = gvResultado.DataKeys(indice).Values(1).ToString()
                        Response.Redirect("~/CONC/COHConcArch.aspx?Fecha=" & Fecha & "&CodBan=" & codBanco)
                End Select
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnVolver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVolver.Click
        Response.Redirect("~/CONC/COHConc.aspx")
    End Sub
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvResultado.PageIndex = e.NewPageIndex
        CargarGrilla(CrearEntidad())
    End Sub
End Class

