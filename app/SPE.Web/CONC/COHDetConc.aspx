<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COHDetConc.aspx.vb" Inherits="COHDetConc" Title="PagoEfectivo - Consultar Conciliaciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <div id="Page1">
        <h1>
            Consultar Detalle de Conciliaciones</h1>
        <div id="div50">
            <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo">
                Criterios de b�squeda</div>
            <div id="divCriteriosBusqueda">
                <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlIdConciliacionEntidad" runat="server">
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="div8" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblIdConciliacionEntidad" runat="server" CssClass="Etiqueta" Text="IdConciliacionEntidad:"></asp:Label>
                                    <asp:TextBox ID="txtIdConciliacionEntidad" runat="server" CssClass="Texto" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div id="div5" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblFechaConciliacion" runat="server" CssClass="Etiqueta" Text="FechaConciliacion:"></asp:Label>
                                    <asp:TextBox ID="txtFechaConciliacion" runat="server" CssClass="TextoCalendar" ReadOnly="true"></asp:TextBox>
                                </div>
                            </fieldset>
                        </asp:Panel>
                        <div id="divCriterios" class="divSubContenedorCriteriosBusqueda">
                            <fieldset class="ContenedorEtiquetaTexto ">
                                <div id="div3" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblNroOperacion" runat="server" CssClass="Etiqueta" Text="Nro Operaci�n:"></asp:Label>
                                    <asp:TextBox ID="txtNroOperacion" runat="server" CssClass="Texto"></asp:TextBox>
                                </div>
                                <div id="div4" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblNroOrdenPago" runat="server" CssClass="Etiqueta" Text="C.I.P."></asp:Label>
                                    <asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="Texto"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="div6" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblEstado" runat="server" CssClass="Etiqueta" Text="Estado:"></asp:Label>
                                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="TextoComboLargo">
                                    </asp:DropDownList>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto">
                                <div id="div1" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblFechaDe" runat="server" CssClass="Etiqueta" Text="Del :" Width="57px"></asp:Label>
                                    <asp:TextBox ID="txtFechaDe" runat="server" CssClass="TextoCalendar"></asp:TextBox>
                                    <asp:ImageButton ID="ibtnFechaDe" runat="server" ImageUrl="~/Images_Buton/date.gif">
                                    </asp:ImageButton>&nbsp;
                                    <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                                        InvalidValueMessage="Fecha 'Del' no v�lida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                                        EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                                        ControlExtender="mdeDel">
                                    </cc1:MaskedEditValidator>
                                </div>
                                <div id="div7" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblFechaA" runat="server" CssClass="Etiqueta" Text="Al:" Width="36px"></asp:Label>
                                    <asp:TextBox ID="txtFechaA" runat="server" CssClass="TextoCalendar"></asp:TextBox>
                                    <asp:ImageButton ID="ibtnFechaA" runat="server" ImageUrl="~/Images_Buton/date.gif">
                                    </asp:ImageButton>&nbsp;
                                    <cc1:MaskedEditValidator ID="MaskedEditValidatorAl" runat="server" IsValidEmpty="False"
                                        InvalidValueMessage="Fecha 'Al' no v�lida" InvalidValueBlurredMessage="*" EmptyValueMessage="Fecha 'Al' es requerida"
                                        EmptyValueBlurredText="*" ControlToValidate="txtFechaA" ControlExtender="mdeA">*
                                    </cc1:MaskedEditValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="El Rango de fechas no es v�lido"
                                        ControlToValidate="txtFechaA" Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtFechaDe">*
                                    </asp:CompareValidator>
                                </div>
                            </fieldset>
                            <asp:ValidationSummary ID="ValidationSummary" runat="server"></asp:ValidationSummary>
                            <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
                                Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
                                Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                            <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
                                Mask="99/99/9999" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                            <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
                                Mask="99/99/9999" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" ValidChars="0123456789" runat="server"
                                TargetControlID="txtNroOrdenPago">
                            </cc1:FilteredTextBoxExtender>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <fieldset id="Fieldset2" class="ContenedorBotoneraCriteriosBusqueda">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="Boton" Text="Buscar" />
                    <br />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="Boton" Text="Limpiar" />
                </fieldset>
            </div>
        </div>
        &nbsp;&nbsp;
        <div id="divOrdenesPago" class="divSubContenedorGrilla">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div2" class="divContenedorTitulo">
                        <asp:Label ID="lblResultado" runat="server" Text="" /></div>
                    <asp:GridView ID="gvResultado" AllowSorting="True" runat="server" CssClass="grilla"
                        AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="NumeroOperacion" SortExpression="NumeroOperacion" HeaderText="Nro Operacion">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NumeroOrdenPago" SortExpression="NumeroOrdenPago" HeaderText="Cod. Identif. Pago">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaConciliacion" SortExpression="FechaConciliacion"
                                HeaderText="Fecha Conciliaci&#243;n">
                                <ItemStyle HorizontalAlign="Center" Width="5px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                HeaderText="Estado Conciliaci�n">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Observacion" SortExpression="Observacion" HeaderText="Observacion">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
