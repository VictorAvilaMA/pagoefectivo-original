<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COHDetConcBCP.aspx.vb" Inherits="COHDetConcBCP"
    Title="PagoEfectivo - Consultar Conciliaciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <h2>
        Consultar detalle de conciliaciones</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B&uacute;squeda</h4>
        <div class="dlgrid">
            <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Nombre del archivo:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtNombreArchivo" runat="server" CssClass="normal"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> C.I.P:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="normal"></asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Del:</li>
                        <li class="t2" style="line-height: 1; z-index: 200;">
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:ImageButton CssClass="calendario" ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                <cc1:MaskedEditValidator ID="MaskedEditValidatorAl" runat="server" IsValidEmpty="False"
                                    InvalidValueMessage="Fecha 'Al' no v�lida" InvalidValueBlurredMessage="*" EmptyValueMessage="Fecha 'Al' es requerida"
                                    EmptyValueBlurredText="*" ControlToValidate="txtFechaA" ControlExtender="mdeA">*
                                </cc1:MaskedEditValidator>
                            </p>
                            <span class="entre">al: </span>
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:ImageButton CssClass="calendario" ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>' style="float:left">
                                </asp:ImageButton>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                    Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                                    Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                    Text="*" style="width:auto"></asp:RequiredFieldValidator>
                                <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                                    InvalidValueMessage="Fecha 'Del' no v�lida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                                    EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                                    ControlExtender="mdeDel" style="width:auto">
                                </cc1:MaskedEditValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="El Rango de fechas no es v�lido"
                                    ControlToValidate="txtFechaA" Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtFechaDe">*
                                </asp:CompareValidator>
                                <div style="clear: both;">
                                </div>
                            </p>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server"></asp:ValidationSummary>
                    <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
                        Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
                        Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
                        Mask="99/99/9999" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                    <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
                        Mask="99/99/9999" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                    <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" ValidChars="0123456789" runat="server"
                        TargetControlID="txtNroOrdenPago">
                    </cc1:FilteredTextBoxExtender>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnVolver" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <ul class="datos_cip2">
                <li id="fsBotonera" class="complet">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    <asp:Button ID="btnVolver" runat="server" CssClass="input_azul4" Text="Volver" />
                </li>
            </ul>
            <div style="clear: both;" />
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" class="result" visible="false">
                        <h5 id="h5Resultado" runat="server">
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <div style="clear: both;">
                        </div>
                        <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" AllowPaging="True"
                            AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="Fecha" DataFormatString="{0:d}" SortExpression="Fecha"
                                    HeaderText="Fecha">
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CIP" SortExpression="CIP" HeaderText="CIP">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaCancelacion" SortExpression="FechaCancelacion" HeaderText="Fecha Cancelacion">
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NombreArchivo" SortExpression="NombreArchivo" HeaderText="Nombre Archivo">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Banco" SortExpression="Banco" HeaderText="Banco">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Estado" SortExpression="Estado" HeaderText="Estado">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Observacion" SortExpression="Observacion" HeaderText="Observaci&#243;n">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hdfIdConcArch" runat="server" />
                        <asp:HiddenField ID="hdfFecha" runat="server" />
                        <asp:HiddenField ID="hdfCodBanco" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnVolver" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
