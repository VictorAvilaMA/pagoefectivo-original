Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class COHDetConcBCP
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
            If Request.QueryString("IdConcArch") IsNot Nothing And _
                Request.QueryString("CodBan") IsNot Nothing And _
                Request.QueryString("Fecha") IsNot Nothing Then
                Dim request As New BEConciliacionDetalle
                Dim _IdConcArch As Integer = Convert.ToInt32(Page.Request.QueryString("IdConcArch"))
                Dim _fecha As DateTime = Convert.ToDateTime(Page.Request.QueryString("Fecha"))
                Dim _codBanco As String = Convert.ToString(Page.Request.QueryString("CodBan"))
                If Context.Request.QueryString.AllKeys.Length > 1 Then
                    hdfIdConcArch.Value = Context.Request.QueryString("IdConcArch").ToString()
                    hdfFecha.Value = Context.Request.QueryString("Fecha").ToString()
                    hdfCodBanco.Value = Context.Request.QueryString("CodBan").ToString()
                End If
                With request
                    .IdConciliacionArchivo = _IdConcArch
                    .IdEstado = "0"
                    .CodigoBanco = _codBanco
                    .CIP = ""
                    .NombreArchivo = ""
                    .FechaInicio = _fecha
                    .FechaFin = _fecha
                End With
                CargarGrilla(request)
                ddlBanco.SelectedValue = _codBanco
                txtFechaDe.Text = _fecha.ToShortDateString
                txtFechaA.Text = _fecha.ToShortDateString()
            Else
                txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy")
                txtFechaA.Text = DateTime.Now.ToShortDateString()
            End If
        End If
    End Sub
    Private Sub CargarCombos()
        Dim objCParametros As New SPE.Web.CAdministrarParametro
        Dim objCBanco As New SPE.Web.CBanco
        Dim oBEBanco As New BEBanco
        With oBEBanco
            .Codigo = ""
            .Descripcion = ""
            .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        End With
        With ddlBanco
            .DataTextField = "Descripcion"
            .DataValueField = "Codigo"
            .DataSource = objCBanco.ConsultarBanco(oBEBanco)
            .DataBind()
        End With
        WebUtil.DropDownlistBinding(ddlEstado, objCParametros.ConsultarParametroPorCodigoGrupo("ESCO"), "Descripcion", "Id", "::: Todos :::")
    End Sub
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Limpiar()
    End Sub
    Sub Limpiar()
        ddlEstado.SelectedIndex = 0
        Me.txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'DateTime.Now.ToShortDateString()
        Me.txtFechaA.Text = DateTime.Now.ToShortDateString()
        Me.lblResultado.Text = ""
        divResult.Visible = False
        Me.gvResultado.DataSource = Nothing
    End Sub
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        hdfIdConcArch.Value = "0"
        CargarGrilla(CrearEntidad())
    End Sub
    Private Sub CargarGrilla(ByVal request As BEConciliacionDetalle)
        Dim CConc As New SPE.Web.CConciliacion
        Dim response As List(Of BEConciliacionDetalle) = CConc.ConsultarDetalleConciliacion(request)
        divResult.Visible = True
        Me.gvResultado.DataSource = response
        gvResultado.DataBind()
        Me.lblResultado.Text = "Se encontraron " & response.Count.ToString() & " registros"

    End Sub
    Function CrearEntidad() As BEConciliacionDetalle
        Dim request As New BEConciliacionDetalle
        With request
            .IdConciliacionArchivo = hdfIdConcArch.Value
            .IdEstado = IIf(ddlEstado.SelectedIndex = 0, 0, _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue))
            .NumeroOperacion = txtNombreArchivo.Text
            .CIP = txtNroOrdenPago.Text
            .NombreArchivo = txtNombreArchivo.Text
            .CodigoBanco = ddlBanco.SelectedValue
            .FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaDe.Text)
            .FechaFin = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaA.Text)
        End With
        Return request
    End Function
    Protected Sub btnVolver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVolver.Click
        Response.Redirect("~/CONC/COHConcArch.aspx")
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvResultado.PageIndex = e.NewPageIndex
        CargarGrilla(CrearEntidad())
    End Sub

End Class