﻿Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class CONC_PRCPreConciliacion
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub

    Private Sub CargarCombos()
        Dim objCBanco As New SPE.Web.CBanco
        Dim oBEBanco As New BEBanco
        With oBEBanco
            .Codigo = ""
            .Descripcion = ""
            .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        End With
        WebUtil.DropDownlistBinding(ddlBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", "::: Seleccione :::")
    End Sub


    Protected Sub btnProcesar_Click(sender As Object, e As System.EventArgs) Handles btnProcesar.Click
        ConciliacionBancaria()
    End Sub

    Private Sub ConciliacionBancaria()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False
        Me.lblMensaje.Text = ""
        Dim request As BEPreConciliacionRequest = CargarDatosArchivoConciliacion()
        If request IsNot Nothing Then
            Dim objCConciliacion As New SPE.Web.CConciliacion
            Dim response As BEConciliacionResponse = objCConciliacion.ProcesarArchivoPreConciliacion(request)
            If response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok Then
                Dim ListaOperaciones As New List(Of BEConciliacionDetalle)
                With ListaOperaciones
                    .AddRange(response.ListaOperacionesConciliadas)
                    .AddRange(response.ListaOperacionesNoConciliadas)
                End With
                gvResultado.DataSource = ListaOperaciones
                gvResultado.DataBind()
                If ListaOperaciones.Count > 0 Then
                    pnlConciliadas.Visible = True
                    lblMensaje.Text = "Se validaron " + ListaOperaciones.Count.ToString() + " registro(s) enviado(s) en el archivo."
                Else
                    pnlConciliadas.Visible = True
                    lblMensaje.Text = "No se validó ninguno de los registros enviados en el archivo."
                End If
            Else
                Me.lblMensaje.Text = response.Message
            End If
            Me.lblMensaje.Visible = True
        Else
            Ocultar()
        End If
    End Sub

    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub

    Private Function CargarDatosArchivoConciliacion() As BEPreConciliacionRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEPreConciliacionRequest As New BEPreConciliacionRequest
            'Dim CantCar As Integer = fulArchivoConciliacion.FileName.Length()
            'Dim Nombre As String = fulArchivoConciliacion.FileName.Remove((CantCar - 4), 4)
            With oBEPreConciliacionRequest
                .CodigoBanco = ddlBanco.SelectedItem.Value.Trim()
                .NombreArchivoPreCon = fulArchivoConciliacion.FileName 'Nombre & System.DateTime.Now.ToString("_yyyyMMddhmmss") & ".xls"
                .Bytes = fulArchivoConciliacion.FileBytes
                .IdUsuarioCreacion = Security.UserInfo.IdUsuario
                .TipoArchivo = fulArchivoConciliacion.PostedFile.ContentType
            End With
            Return oBEPreConciliacionRequest
        Else
            Return Nothing
        End If
    End Function

    Private Function ValidaDatos() As Boolean
        If Not (ddlBanco.SelectedIndex > 0 And ddlBanco.SelectedItem.Text <> "" And ddlBanco.SelectedItem.Text <> "::: Seleccione :::") Then
            MostrarValidacionFileUpload(True, "Debe seleccionar un banco.")
            ddlBanco.Focus()
            Return False
        End If
        If Not fulArchivoConciliacion.HasFile Then
            MostrarValidacionFileUpload(True, "No se ha seleccionado ningún archivo.")
            fulArchivoConciliacion.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub


    Protected Sub BtnLimpiar_Click(sender As Object, e As System.EventArgs) Handles BtnLimpiar.Click
        Ocultar()
        ddlBanco.SelectedIndex = 0
        lblMensaje.Text = ""
    End Sub
End Class
