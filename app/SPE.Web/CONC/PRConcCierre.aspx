<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="PRConcCierre.aspx.vb" Inherits="PRConcCierre" title="PagoEfectivo - Conciliación Bancaria" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Page">
    <asp:ScriptManager id="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" ></asp:ScriptManager>          
        <h1>            
            <asp:Label ID="lblProceso" runat="server" Text="Conciliación al Cierre"></asp:Label>
        </h1>
        <asp:Panel ID="PnlEmpresa" runat="server" Cssclass="divContenedor"  >
           <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" >
               1. Subir Archivo
           </div>
           <asp:UpdatePanel id="upnlEmpresa"  runat="server" UpdateMode="Conditional">
            <contenttemplate> 
                <fieldset class="ContenedorEtiquetaTexto">
                    <div id="div6" class="EtiquetaTextoIzquierda">
                        <asp:Label id="lblBanco" runat="server" CssClass="EtiquetaLargo" Text="Banco:" Width="142px"></asp:Label> 
                        <asp:DropDownList id="ddlBanco" runat="server" CssClass="TextoComboLargo"></asp:DropDownList> 
                    </div>
                </fieldset> 
                <fieldset class="ContenedorEtiquetaTexto stripe">
                    <div id="div5" class="EtiquetaTextoIzquierda">
                        <asp:Label id="lblAdjuntarImagen" runat="server" Text="Seleccionar Archivo de Agencias Bancarias" 
                            CssClass="EtiquetaLargo" Width="142px">
                        </asp:Label> 
                        <asp:FileUpload id="fulArchivoConciliacion" runat="server" ></asp:FileUpload> 
                        <asp:Label ID="lblUploadMens" runat="server" ForeColor="Red" Text="(Extensión válida: .xls,.xlsx)"></asp:Label>
                        <asp:RequiredFieldValidator ID="rfvArchivoConciliacion" ControlToValidate="fulArchivoConciliacion"
                               runat="server" ErrorMessage="Debe seleccionar un archivo válido.">*</asp:RequiredFieldValidator>
                   </div>
                </fieldset>
                <fieldset class="ContenedorEtiquetaTexto">
                    <div id="div2" class="EtiquetaTextoIzquierda">
                        <asp:Label id="lblFechaConciliacion" runat="server" CssClass="EtiquetaLargo" Text="Fecha Conciliación :" Width="142px"></asp:Label> 
                        <asp:TextBox id="txtFechaConciliacion" runat="server" CssClass="TextoCalendar"></asp:TextBox> 
                        <asp:ImageButton id="ibtnFechaConciliacion" runat="server" ImageUrl="~/Images_Buton/date.gif"></asp:ImageButton>&nbsp; 
                        <cc1:MaskedEditValidator id="MaskedEditValidatorDel" runat="server" IsValidEmpty="False" 
                            InvalidValueMessage="Fecha Conciliación no válida" InvalidValueBlurredMessage="*" ErrorMessage="*" 
                            EmptyValueMessage="Fecha Conciliación es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaConciliacion" 
                            ControlExtender="mdeDel">
                        </cc1:MaskedEditValidator>
                    </div>
                </fieldset>                    
                <fieldset class="ContenedorEtiquetaTexto stripe">
                    <div id="div1" class="EtiquetaTextoIzquierda">
                        <asp:Label id="Label1" runat="server" CssClass="EtiquetaLargo" Text="Conciliación Asociada:" Width="142px"></asp:Label> 
                        <asp:DropDownList id="ddlConciliacionAsociada" runat="server" CssClass="TextoComboLargo" OnSelectedIndexChanged="ddlConciliacionAsociada_SelectedIndexChanged"></asp:DropDownList> 
                        <asp:ImageButton id="ibtnActualizar" runat="server" ImageUrl="~/Images_Buton/update.gif" OnClick="ibtnActualizar_Click"></asp:ImageButton>&nbsp; 
                    </div>
                </fieldset>  
                <cc1:CalendarExtender id="calDel" runat="server" TargetControlID="txtFechaConciliacion" PopupButtonID="ibtnFechaConciliacion" Format="dd/MM/yyyy"></cc1:CalendarExtender>                 
                <cc1:MaskedEditExtender id="mdeDel" runat="server" TargetControlID="txtFechaConciliacion" MaskType="Date" Mask="99/99/9999" CultureName="es-PE"></cc1:MaskedEditExtender>                                 
            </contenttemplate>
           </asp:UpdatePanel>
        </asp:Panel> 
        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>          
        <asp:Panel ID="pnlFileUpLoad" runat="server" Visible="false">
            <asp:Label ID="lblUploadFile" CssClass="MensajeValidacion" runat="server" Text=""></asp:Label>
        </asp:Panel>
        <br />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" /> 
        <fieldset id="fsBotonera" class="ContenedorBotonera" >
            <asp:Button ID="btnRegistrar" runat="server" CssClass="Boton"  Text="Conciliar" ValidationGroup="ValidaCampos" />
            <asp:Button ID="btnActualizar" runat="server" CssClass="Boton"  Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False" />
            <asp:Button ID="btnCancelar" runat="server" CssClass="Boton" OnClientClick="return confirm('Esta seguro que desea cancelar este documento? ');"    Text="Cancelar" />
        </fieldset>    
        <asp:Panel ID="pnlConciliadas" runat="server" Cssclass="divContenedor" Visible="false">
            <asp:Label ID="lblOperacionesConciliadas" runat="server" Text="Operaciones conciliadas" CssClass="divContenedorTitulo"></asp:Label>
            <asp:GridView id="gvResultado" AllowSorting="True" runat="server" CssClass="grilla" AutoGenerateColumns="False"   >
                       <Columns>
                            <asp:BoundField DataField="NumeroOperacion"  HeaderText="Nro Operacion">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NumeroOrdenPago"  HeaderText="Cod. Identif. Pago">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaConciliacion"  HeaderText="Fecha Conciliaci&#243;n">
                                <ItemStyle HorizontalAlign="Center" Width="5px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado"  HeaderText="Estado Conciliaci&#243;n">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Observacion" HeaderText="Observacion" >
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                       </Columns>
                     <HeaderStyle CssClass="cabecera"></HeaderStyle>
            </asp:GridView>               
        </asp:Panel> 
 </div>
</asp:Content>

