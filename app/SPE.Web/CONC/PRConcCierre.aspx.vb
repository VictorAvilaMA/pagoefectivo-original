Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.Utilitario.Util
Partial Class PRConcCierre
    Inherits PaginaBase
#Region "Codigo Nuevo"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub
    Private Sub CargarCombos()
        'Llenamos ddlBancos
        Dim objCBanco As New SPE.Web.CBanco
        Dim obeBanco As New BEBanco
        obeBanco.Codigo = ""
        obeBanco.Descripcion = ""
        obeBanco.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        WebUtil.DropDownlistBinding(ddlBanco, objCBanco.ConsultarBanco(obeBanco), "Descripcion", "Codigo", "::: Seleccione :::")

        CargarUltimasConciliacionesPendientes()
    End Sub
    Private Sub CargarUltimasConciliacionesPendientes()
        'Llenamos DropDownList con las Ultimas Conciliaciones Registradas
        Dim objCConciliacion As New SPE.Web.CConciliacion
        Dim request As New BEConciliacionRequest
        request.IdEstado = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.EstadosConciliacion.NoConciliado
        'request.IdTipoConciliacion = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.TiposConciliacion.AlCierre
        'request.IdOrigenConciliacion = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.OrigenConciliacion.Manual
        request.FechaConciliacion = DateAdd(DateInterval.Year, -5, Date.Now)
        request.FechaFin = DateTime.Now
        'WebUtil.DropDownlistBinding(ddlConciliacionAsociada, objCConciliacion.ConsultarUltimasConciliaciones(request), "UltimasConciliaciones", "IdConciliacion", "::: Seleccione :::")
    End Sub
    Sub ConciliacionBancaria()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False

        Me.lblMensaje.Text = ""
        Dim objCConciliacion As New SPE.Web.CConciliacion
        Dim request As New BEConciliacionRequest
        Dim response As New BEConciliacionResponse
        request = CargarDatosConciliacion()
        If Not request Is Nothing Then
            'response = objCConciliacion.ProcesarConciliacion(request)
            If Not response.ListaOperacionesConciliadas Is Nothing Then
                If response.ListaOperacionesConciliadas.Count > 0 Then
                    Me.pnlConciliadas.Visible = True
                    Me.gvResultado.DataSource = response.ListaOperacionesConciliadas
                    Me.gvResultado.DataBind()
                Else
                    Me.pnlConciliadas.Visible = False
                End If
                Me.lblMensaje.Visible = True
                Me.lblMensaje.Text = "Se conciliaron " + response.ListaOperacionesConciliadas.Count.ToString + " operaciones bancarias."
            Else
                Me.lblMensaje.Visible = True
                Me.lblMensaje.Text = response.Message
            End If
            CargarUltimasConciliacionesPendientes()
            'If response.ListaOperacionesConciliadas Is Nothing And response.ListaOperacionesNoConciliadas Is Nothing Then
            '    'lblMensaje.Visible = True
            '    'lblMensaje.Text = "El archivo ya ha sido conciliado anteriormente."
            '    MostrarValidacionFileUpload(True, "El archivo ya ha sido conciliado anteriormente.")
            'End If
        Else
            '    'lblMensaje.Visible = True
            '    'lblMensaje.Text = "El archivo no es válido para la conciliación."
            '    MostrarValidacionFileUpload(True, "El archivo no es válido para la conciliación.")
            Ocultar()
        End If
    End Sub
    Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub
    Function CargarDatosConciliacion() As BEConciliacionRequest
        Ocultar()
        Dim request As New BEConciliacionRequest
        Dim file As String = Me.fulArchivoConciliacion.FileName
        Dim extension As String = IO.Path.GetExtension(file)
        'Dim sr As New System.IO.StreamReader(fulArchivoConciliacion.PostedFile.InputStream)
        Dim oUtil As New SPE.Utilitario.Util
        If ValidaArchivo() Then
            Dim obeConcEntidad As New BEConciliacionEntidad
            Dim obeConcArchivo As New BEConciliacionArchivo
            Dim lstConcEntidad As New List(Of BEConciliacionEntidad)
            Dim lstConcArchivo As New List(Of BEConciliacionArchivo)
            obeConcArchivo.NombreArchivo = Me.fulArchivoConciliacion.FileName.ToString().Trim
            'FALTA VER SI SE SETEARA ACA EL IdTipoEstructuraArchivo
            obeConcArchivo.IdTipoConciliacion = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.TiposConciliacion.AlCierre
            obeConcArchivo.IdOrigenConciliacion = SPE.EmsambladoComun.ParametrosSistema.Conciliacion.OrigenConciliacion.Manual
            lstConcArchivo.Add(obeConcArchivo)
            obeConcEntidad.ListaConcilacionesArchivo = lstConcArchivo
            obeConcEntidad.CodigoBanco = ddlBanco.SelectedItem.Value.Trim()
            request.FechaConciliacion = oUtil.ValidaDiaProcesoConciliacion(Convert.ToDateTime(txtFechaConciliacion.Text + " " + DateTime.Now.ToLongTimeString()))
            request.IdConciliacion = Convert.ToInt32(ddlConciliacionAsociada.SelectedItem.Value)
            request.IdUsuarioCreacion = Security.UserInfo.IdUsuario
            lstConcEntidad.Add(obeConcEntidad)
            request.ListaConciliacionesEntidad = lstConcEntidad
            'Dim CConc As New SPE.Web.CConciliacion
            'Dim rutaTempArchivo As String = CConc.RutaArchivosTemporales  'ConfigurationManager.AppSettings("RutaArchivoConciliacionCierre") & "temp_" & Date.Now.ToString("ddMMyyyy_HHmmss") & ConfigurationManager.AppSettings("TipoArchivoBCP_ConcCierre").Split(",")(0)
            Dim rutaTempArchivo As String = RutaArchivosTemporales(ddlBanco.SelectedItem.Value.Trim())
            fulArchivoConciliacion.SaveAs(rutaTempArchivo)
            'request.NombreArchivoCierre = rutaTempArchivo 'oUtil.TransformaraArrayBytes(fulArchivoConciliacion)

            'Seteamos la cultura a en-US
            Dim culturaActual As System.Globalization.CultureInfo
            culturaActual = System.Threading.Thread.CurrentThread.CurrentCulture
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(ConfigurationManager.AppSettings.Get("culturaArchivoConc_Cierre"))
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(ConfigurationManager.AppSettings.Get("culturaArchivoConc_Cierre"))

            request.ListaOperacionesArchivo = oUtil.ExcelToListadeStrings(rutaTempArchivo, NombreHojaporBanco(ddlBanco.SelectedItem.Value.Trim()), ConfigurationManager.AppSettings.Get("ColInicial_ConCierre"), ConfigurationManager.AppSettings.Get("ColFinal_ConCierre"))
            System.Threading.Thread.CurrentThread.CurrentUICulture = culturaActual
            System.Threading.Thread.CurrentThread.CurrentCulture = culturaActual
            If System.IO.File.Exists(rutaTempArchivo) Then
                Microsoft.VisualBasic.FileSystem.Kill(rutaTempArchivo)
            End If
            'sr.Close()
            Return request
        Else
            Return Nothing
        End If
        Return Nothing
    End Function
    Private Function NombreHojaporBanco(ByVal codBanco As String) As String
        Dim nombreHoja As String = ""
        Select Case codBanco
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BCP
                nombreHoja = ConfigurationManager.AppSettings.Get("NombreHojaConcCierreBCP")
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA
                nombreHoja = ConfigurationManager.AppSettings.Get("NombreHojaConcCierreBBVA")
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                nombreHoja = ConfigurationManager.AppSettings.Get("NombreHojaConcCierreIBK")
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                nombreHoja = ConfigurationManager.AppSettings.Get("NombreHojaConcCierreSBK")
        End Select
        Return nombreHoja
    End Function
    Public Function RutaArchivosTemporales(ByVal codBanco As String) As String
        Dim tipoArchivo As String = ""
        Select Case codBanco
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BCP
                tipoArchivo = ConfigurationManager.AppSettings("TipoArchivoBCP_ConcCierre").Split(",")(0)
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.BBVA
                tipoArchivo = ConfigurationManager.AppSettings("TipoArchivoBBVA_ConcCierre").Split(",")(0)
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Interbank
                tipoArchivo = ConfigurationManager.AppSettings("TipoArchivoIBK_ConcCierre").Split(",")(0)
            Case SPE.EmsambladoComun.ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                tipoArchivo = ConfigurationManager.AppSettings("TipoArchivoSBK_ConcCierre").Split(",")(0)
        End Select
        Return ConfigurationManager.AppSettings("RutaArchivoConciliacionCierre") & "temp_" & Date.Now.ToString("ddMMyyyy_HHmmss") & tipoArchivo
    End Function
    Private Function ValidaArchivo() As Boolean
        Dim respuesta As Boolean = False
        Dim file As String = Me.fulArchivoConciliacion.FileName
        Dim extension As String = IO.Path.GetExtension(file)
        Dim sr As New System.IO.StreamReader(fulArchivoConciliacion.PostedFile.InputStream)
        If Not fulArchivoConciliacion.PostedFile Is Nothing And fulArchivoConciliacion.PostedFile.ContentLength > 0 Then
            If extension.ToUpper = ".XLS" Or extension.ToUpper = ".XLSX" Then
                If fulArchivoConciliacion.HasFile Then
                    respuesta = True
                Else
                    MostrarValidacionFileUpload(True, "No se a seleccionado ningun archivo.")
                    fulArchivoConciliacion.Focus()
                    respuesta = False
                    Return respuesta
                End If
            Else
                MostrarValidacionFileUpload(True, "El tipo de archivo para la conciliación debe ser excel.")
                fulArchivoConciliacion.Focus()
                respuesta = False
                Return respuesta
            End If
            If ddlBanco.SelectedIndex > 0 And ddlBanco.SelectedItem.Text <> "" And ddlBanco.SelectedItem.Text <> "::: Seleccione :::" Then
                respuesta = True
            Else
                MostrarValidacionFileUpload(True, "Debe seleccionar un banco.")
                ddlBanco.Focus()
                respuesta = False
                Return respuesta
            End If
            'Validamos que se haya especificado una Fecha para la Conciliacion
            If txtFechaConciliacion.Text.Trim <> "" And txtFechaConciliacion.Text.Length > 0 Then
                respuesta = True
            Else
                MostrarValidacionFileUpload(True, "Debe especificar una Fecha de Conciliación.")
                txtFechaConciliacion.Focus()
                respuesta = False
                Return respuesta
            End If
            'Validamos que se tiene que especificar el IdConciliacion con el que se va a relacionar el archivo a procesar
            If ddlConciliacionAsociada.SelectedIndex > 0 And ddlConciliacionAsociada.SelectedItem.Text <> "" And ddlConciliacionAsociada.SelectedItem.Text <> "::: Seleccione :::" Then
                respuesta = True
            Else
                MostrarValidacionFileUpload(True, "Debe asociar el archivo a una de las Conciliaciones Pendientes.")
                ddlConciliacionAsociada.Focus()
                respuesta = False
                Return respuesta
            End If
        Else
            MostrarValidacionFileUpload(True, "No se a seleccionado ningun archivo.")
            fulArchivoConciliacion.Focus()
            respuesta = False
            Return respuesta
        End If
        Return respuesta
    End Function
    Function ListaOperacionesBancarias(ByVal sr As System.IO.StreamReader) As List(Of String)
        Dim objListaOperacionesBancarias As New List(Of String)
        Do While sr.Peek() > 0
            objListaOperacionesBancarias.Add(sr.ReadLine)
        Loop
        Return objListaOperacionesBancarias
    End Function
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        ConciliacionBancaria()
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Ocultar()
        Response.Redirect("~/CONC/COHConc.aspx")
    End Sub
    Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub
    Protected Sub ddlConciliacionAsociada_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlConciliacionAsociada.SelectedIndex > 0 And ddlConciliacionAsociada.SelectedItem.Text <> "" And ddlConciliacionAsociada.SelectedItem.Text <> "::: Seleccione :::" Then
            'txtFechaConciliacion.Text = ddlConciliacionAsociada.SelectedItem.Text.Split("-")(1)
        End If
    End Sub
    Protected Sub ibtnActualizar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        rfvArchivoConciliacion.Enabled = False
        MaskedEditValidatorDel.Enabled = False
        CargarUltimasConciliacionesPendientes()
        rfvArchivoConciliacion.Enabled = True
        MaskedEditValidatorDel.Enabled = True
    End Sub
#End Region
End Class