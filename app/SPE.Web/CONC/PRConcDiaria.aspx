<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRConcDiaria.aspx.vb" Inherits="PRConcDiaria"
    Title="PagoEfectivo - Conciliación Bancaria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Conciliaci&oacute;n Diaria
    </h2>
    <div class="conten_pasos3">
        <div method="post" action="">
            <fieldset>
                <h4>
                    1. Subir Archivo</h4>
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                                </asp:DropDownList>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span> Archivo de conciliaci&oacute;n :</li>
                            <li class="t2">
                                <asp:FileUpload ID="fulArchivoConciliacion" runat="server">
                                </asp:FileUpload>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span> Fecha de conciliaci&oacute;n :</li>
                             <li class="t2" style="line-height: 1">
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaConciliacion" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:ImageButton ID="ibtnFechaConciliacion" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                </p>
                            </li>
                            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                            <asp:Panel ID="pnlFileUpLoad" runat="server" Visible="false">
                                <asp:Label ID="lblUploadFile" CssClass="MensajeValidacion" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </asp:Panel>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaConciliacion"
                                PopupButtonID="ibtnFechaConciliacion" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtFechaConciliacion"
                                MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </ul>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="clear:both;"></div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Conciliar"
                            ValidationGroup="ValidaCampos" />
                        <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
            </fieldset>
        </div>
        <div>
            <asp:Panel ID="pnlConciliadas" runat="server" CssClass="divContenedor" Visible="false">
                <%--<asp:Label ID="lblOperacionesConciliadas" runat="server" Text="Operaciones conciliadas" CssClass="divContenedorTitulo"></asp:Label>--%>
                <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="CIP" HeaderText="Cod. Identif. Pago">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NumeroOperacion" HeaderText="Nro Operación">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Observacion" HeaderText="Observación">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
