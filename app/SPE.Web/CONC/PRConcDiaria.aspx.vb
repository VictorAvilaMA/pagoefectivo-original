Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class PRConcDiaria
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub

    Private Sub CargarCombos()
        Dim objCBanco As New SPE.Web.CBanco
        Dim oBEBanco As New BEBanco
        With oBEBanco
            .Codigo = ""
            .Descripcion = ""
            .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        End With
        WebUtil.DropDownlistBinding(ddlBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", "::: Seleccione :::")
    End Sub

    Private Sub ConciliacionBancaria()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False
        Me.lblMensaje.Text = ""
        Dim request As BEConciliacionArchivo = CargarDatosArchivoConciliacion()
        If request IsNot Nothing Then
            Dim objCConciliacion As New SPE.Web.CConciliacion
            Dim response As BEConciliacionResponse = objCConciliacion.ProcesarArchivoConciliacion(request)
            If response.BMResultState = _3Dev.FW.Entidades.BMResult.Ok Then
                Dim ListaOperaciones As New List(Of BEConciliacionDetalle)
                With ListaOperaciones
                    .AddRange(response.ListaOperacionesConciliadas)
                    .AddRange(response.ListaOperacionesNoConciliadas)
                End With
                gvResultado.DataSource = ListaOperaciones
                gvResultado.DataBind()
                If response.ListaOperacionesConciliadas.Count > 0 Then
                    pnlConciliadas.Visible = True
                    lblMensaje.Text = "Se conciliaron " + response.ListaOperacionesConciliadas.Count.ToString + " de " + _
                        (response.ListaOperacionesConciliadas.Count + response.ListaOperacionesNoConciliadas.Count).ToString + _
                        " registro(s) enviado(s) en el archivo."
                Else
                    pnlConciliadas.Visible = True
                    'lblMensaje.Text = "No se concili� ninguno de los " + _
                    '    (response.ListaOperacionesConciliadas.Count + response.ListaOperacionesNoConciliadas.Count).ToString + _
                    '    " registros enviados en el archivo."
                    lblMensaje.Text = "No se concili� ninguno de los registros enviados en el archivo."
                End If
            Else
                Me.lblMensaje.Text = response.Message
            End If
            Me.lblMensaje.Visible = True
        Else
            Ocultar()
        End If
    End Sub

    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub

    Private Function CargarDatosArchivoConciliacion() As BEConciliacionArchivo
        Ocultar()
        If ValidaDatos() Then
            Dim oBEConciliacionArchivo As New BEConciliacionArchivo
            With oBEConciliacionArchivo
                .CodigoBanco = ddlBanco.SelectedItem.Value.Trim()
                .NombreArchivo = fulArchivoConciliacion.FileName
                .Bytes = fulArchivoConciliacion.FileBytes
                .Fecha = Convert.ToDateTime(txtFechaConciliacion.Text)
                .IdTipoConciliacion = Conciliacion.TiposConciliacion.Diaria
                .IdOrigenConciliacion = Conciliacion.OrigenConciliacion.Manual
                .IdUsuarioCreacion = Security.UserInfo.IdUsuario
            End With
            Return oBEConciliacionArchivo
        Else
            Return Nothing
        End If
    End Function

    Private Function ValidaDatos() As Boolean
        If Not (ddlBanco.SelectedIndex > 0 And ddlBanco.SelectedItem.Text <> "" And ddlBanco.SelectedItem.Text <> "::: Seleccione :::") Then
            MostrarValidacionFileUpload(True, "Debe seleccionar un banco.")
            ddlBanco.Focus()
            Return False
        End If
        If Not fulArchivoConciliacion.HasFile Then
            MostrarValidacionFileUpload(True, "No se ha seleccionado ning�n archivo.")
            fulArchivoConciliacion.Focus()
            Return False
        End If
        If txtFechaConciliacion.Text.Trim = "" And txtFechaConciliacion.Text.Length = 0 Then
            MostrarValidacionFileUpload(True, "Debe especificar una fecha de conciliaci�n.")
            txtFechaConciliacion.Focus()
            Return False
        End If
        Try
            Convert.ToDateTime(txtFechaConciliacion.Text)
        Catch ex As Exception
            MostrarValidacionFileUpload(True, "Debe especificar una fecha de conciliaci�n v�lida.")
            txtFechaConciliacion.Focus()
            Return False
        End Try
        Return True
    End Function

    Private Function ListaOperacionesBancarias(ByVal sr As System.IO.StreamReader) As List(Of String)
        Dim objListaOperacionesBancarias As New List(Of String)
        Do While sr.Peek() > 0
            objListaOperacionesBancarias.Add(sr.ReadLine)
        Loop
        Return objListaOperacionesBancarias
    End Function

    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        ConciliacionBancaria()
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Ocultar()
        ddlBanco.SelectedIndex = 0
        txtFechaConciliacion.Text = ""
    End Sub

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub

End Class