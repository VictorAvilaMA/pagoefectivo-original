﻿<%@ Page Language="VB" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRConcDiariaCons.aspx.vb" Inherits="PRConcDiariaCons"
    Title="PagoEfectivo - Conciliación Bancaria" %>




<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consulta Conciliaci&oacute;n Diaria
    </h2>
    <div class="conten_pasos3">
        <div method="post" action="">
            <fieldset>
                <h4>
                    1. Realizar Consulta</h4>
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                                </asp:DropDownList>
                            </li>
                            
                            <li class="t1"><span class="color">&gt;&gt;</span> Fecha Inicial :</li>
                             <li class="t2" style="line-height: 1">
                            <p class="input_cal">
                                <asp:TextBox ID="txtFechaConciliacion" runat="server" CssClass="corta"></asp:TextBox>
                                <asp:ImageButton ID="ibtnFechaConciliacion" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                </asp:ImageButton>
                                </p>
                            </li>
                            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                            <asp:Panel ID="pnlFileUpLoad" runat="server" Visible="false">
                                <asp:Label ID="lblUploadFile" CssClass="MensajeValidacion" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </asp:Panel>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaConciliacion"
                                PopupButtonID="ibtnFechaConciliacion" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtFechaConciliacion"
                                MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                            </cc1:MaskedEditExtender>



                            

                        </ul>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="clear:both;"></div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Buscar"
                            ValidationGroup="ValidaCampos" />
                        <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>

                    <li class="t2">
                            <asp:LinkButton ID="lnkVerDetalle" runat="server" Visible="false">Ver Detalle</asp:LinkButton>
                    </li>
                </ul>
            </fieldset>
        </div>
        <div>
            <asp:Panel ID="pnlConciliadas" runat="server" CssClass="divContenedor" Visible="False">
                <%--<asp:Label ID="lblOperacionesConciliadas" runat="server" Text="Operaciones conciliadas" CssClass="divContenedorTitulo"></asp:Label>--%>
                <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" 
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="IdOrdenPago" HeaderText="CIP">
                        </asp:BoundField>
                        <asp:BoundField DataField="OrderIdComercio" HeaderText="Orden Id Comercio">
                        </asp:BoundField>
                        <asp:BoundField DataField="Moneda" HeaderText="Moneda">
                        </asp:BoundField>
                        <asp:BoundField DataField="Banco" HeaderText="Banco" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado" />
                        <asp:BoundField DataField="Servicio" HeaderText="Servicio" />
                        <asp:BoundField DataField="Total" HeaderText="Total" />
                        <asp:BoundField DataField="FechaCancelacion" HeaderText="Fecha Cancelación" />
                        <asp:BoundField DataField="UsuarioEmail" HeaderText="UsuarioEmail" />
                        <asp:BoundField DataField="Observacion" HeaderText="Observación" />
                    </Columns>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
