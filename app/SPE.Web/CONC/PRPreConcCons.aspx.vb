﻿Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema
Imports Microsoft.Reporting.WebForms
Imports System.Data
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing.Printing
Imports System.Drawing.Imaging
Imports System.Linq

Partial Class PRPreConcCons
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub


    Private Sub CargarCombos()
        Dim objCBanco As New SPE.Web.CBanco
        Dim oBEBanco As New BEBanco
        With oBEBanco
            .Codigo = ""
            .Descripcion = ""
            .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
        End With
        WebUtil.DropDownlistBinding(ddlBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "Codigo", "::: Todos :::")
    End Sub


    Private Sub ConsultaPreConciliacionBancaria()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False
        Me.lblMensaje.Text = ""
        Dim request As BEConciliacionArchivo = CargarDatosArchivoConciliacion()
        If request IsNot Nothing Then
            Dim objCConciliacion As New SPE.Web.CConciliacion

            Dim response As List(Of BEPreConciliacionDetalle) = objCConciliacion.ConsultarPreConciliacionDetalle(request)

            If response.Count = 0 Then
                gvResultado.Visible = False
                Me.gvResultado.DataSource = Nothing
                Me.lnkVerDetalle.Visible = False
                lblMensaje.Text = "No se encontraron Registros."
            Else
                Me.pnlConciliadas.Visible = True
                Me.gvResultado.Visible = True
                Me.gvResultado.DataSource = response
                Me.lnkVerDetalle.Visible = True
                gvResultado.DataBind()

                If response.Count() = 1 Then
                    Me.lblMensaje.Text = "Resultados: 1 registro."
                ElseIf response.Count() > 1 Then
                    Me.lblMensaje.Text = "Resultados: " & response.Count.ToString() & " registros."
                End If
            End If




        Else
            Ocultar()
        End If
    End Sub


    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub


    Private Function CargarDatosArchivoConciliacion() As BEConciliacionArchivo
        Ocultar()

        Dim oBEConciliacionArchivo As New BEConciliacionArchivo
        With oBEConciliacionArchivo
            .CodigoBanco = ddlBanco.SelectedItem.Value.Trim()
        End With
        Return oBEConciliacionArchivo
        
    End Function


    

    Private Function ListaOperacionesBancarias(ByVal sr As System.IO.StreamReader) As List(Of String)
        Dim objListaOperacionesBancarias As New List(Of String)
        Do While sr.Peek() > 0
            objListaOperacionesBancarias.Add(sr.ReadLine)
        Loop
        Return objListaOperacionesBancarias
    End Function

    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        ConsultaPreConciliacionBancaria()
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        Ocultar()
        ddlBanco.SelectedIndex = 0
        lnkVerDetalle.Visible = False
    End Sub

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub

    Protected Sub lnkVerDetalle_Click(sender As Object, e As System.EventArgs) Handles lnkVerDetalle.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            'ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            'Response.Redirect(String.Format("~/Reportes/RptDetalleTransaccionPendiente.aspx?idEm={0}&idMo={1}&Fini={2}&Ffin={3}", _
            '                                ddlEmpresa.SelectedValue, ddlMoneda.SelectedValue, txtFechaA.Text.TrimEnd, txtFechaDe.Text.TrimEnd))
            ExportarExcel(ddlBanco.SelectedItem.ToString())
        End If 'asd
    End Sub

    Protected Sub ExportarExcel(ByVal strBanco As String)
        Dim request As BEConciliacionArchivo = CargarDatosArchivoConciliacion()
        If request IsNot Nothing Then
            Dim objCConciliacion As New SPE.Web.CConciliacion
            Dim listResponse As List(Of BEPreConciliacionDetalle) = objCConciliacion.ConsultarPreConciliacionDetalle(request)
            Dim moment As System.DateTime = System.DateTime.Now
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Banco", strBanco.Replace(":", "")))
            parametros.Add(New KeyValuePair(Of String, String)("FechaInicio", DateTime.Today.ToShortDateString()))
            UtilReport.ProcesoExportarGenerico(listResponse, Page, "EXCEL", "xls", "Detalle de Pre - Conciliación", parametros, "RptDetallePreconciliacion.rdlc")
        End If
    End Sub


    Protected Sub gvResultado_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvResultado.PageIndexChanging
        gvResultado.PageIndex = e.NewPageIndex
        ConsultaPreConciliacionBancaria()
    End Sub
End Class
