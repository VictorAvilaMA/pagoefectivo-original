<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="CambiarPassword.aspx.vb" Inherits="CambiarPassword" Title="PagoEfectivo - Cambiar Contrase�a" %>

<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/estructura_pagoefectivo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/letras_pagoefectivo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/gridandforms.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/accordian-src.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <%--	<script type="text/javascript" src="jscripts/validate.js"></script>--%>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/pagoefectivo.utils.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/pagoefectivo.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/jquery.simplyscroll-1.0.4.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/jquery.simplyscroll-1.0.4.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/accordian.pack.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <!--[if IE 6]>
	    <script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	    <script type="text/javascript">
	    (function($) {
		    $(document).ready(function(){
			    try {	
				    DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			    }catch(e){}});
		    })(jQuery);	
	    </script>
	    <![endif]-->

        <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/reset.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/estilo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <!--[if lte IE 7]>
    <link href="App_Themes/SPE/css/ie7.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/fixEstilo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3

                });
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Cambiar de Contrase�a</h2>
    <asp:UpdatePanel ID="Ubigeo" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="conten_pasos3">
                <h4>
                    Datos de usuario</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> (*) Nueva Contrase�a :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNuevoPassword" runat="server" CssClass="neu" TextMode="Password"
                            MaxLength="100" Style="float: left"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNuevoPassword" runat="server" ControlToValidate="txtNuevoPassword"
                            ErrorMessage="Ingrese su nueva contrase�a" ValidationGroup="Contrasenia">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> (*) Confirmar Contrase�a :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtConfirmarPassword" runat="server" CssClass="neu" TextMode="Password"
                            MaxLength="100" Style="float: left"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvConfirmarNuevoPassword" runat="server" ControlToValidate="txtConfirmarPassword"
                            ErrorMessage="Ingrese la confirmacion de la nueva contrase�a" ValidationGroup="Contrasenia">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator CssClass="msje" ID="cvlConfirmarContrase�a" runat="server"
                            ControlToCompare="txtNuevoPassword" ControlToValidate="txtConfirmarPassword"
                            ErrorMessage="La nueva contrase�a no conincide con su verificaci�n" ShowMessageBox="true" ValidationGroup="Contrasenia">*</asp:CompareValidator>
                    </li>
                    <li class="t1"><span class="color"></span></li>
                    <li class="t2" style="height: auto">
                        <asp:Label CssClass="msje" ID="lblMensajeInformativo" Width="100%" runat="server"
                            Text="* M�nimo [Nro] Caracteres" Style="line-height: normal; text-align: left;
                            height: 1px; line-height: normal"></asp:Label>
                        <asp:ValidationSummary ID="ValidationSummaryx" runat="server" CssClass="w100" Visible="true"
                        EnableClientScript="true" ShowSummary="true" ValidationGroup="Contrasenia"/>
                        <asp:Label ID="lblMensaje" runat="server" Width="100%" Style="float: none; line-height: normal;
                            text-align: left; height: 1px; line-height: normal"></asp:Label>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Cambiar" ValidationGroup="Contrasenia"/>
                        <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Limpiar"
                            CausesValidation="False" />
                    </li>
                </ul>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCancelar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
