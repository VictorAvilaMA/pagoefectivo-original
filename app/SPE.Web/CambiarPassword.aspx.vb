
Partial Class CambiarPassword
    Inherits System.Web.UI.Page

   


    Sub LimpiarTextos()
        Me.txtNuevoPassword.Text = ""
        Me.txtConfirmarPassword.Text = ""
        ' ValidationSummary1.Visible = False
        lblMensaje.Text = String.Empty
    End Sub


    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        LimpiarTextos()
    End Sub

    
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        Dim obeUsuario As New SPE.Entidades.BEUsuarioBase
        If (txtNuevoPassword.Text.Length >= DameMaximoTamanioContrasenia()) Then
            If (txtNuevoPassword.Text.Equals(txtConfirmarPassword.Text)) Then
                obeUsuario.TokenGUIDPassword = CStr(Request.QueryString("Token"))
                obeUsuario.NuevoPassword = CStr(txtNuevoPassword.Text)
                Dim fnc As New SPE.Web.CComun
                Dim be As New SPE.Entidades.BEUsuarioBase
                be = fnc.ActualizarContraseniaOlvidada(obeUsuario)
                If be.IdEstado = 0 Then
                    Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", "Confirmacion", "La nueva contrase�a se cambio correctamente", "Login.aspx")
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Mensaje", script, True)
                Else
                    Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", "Confirmacion", "El token de correo ha caducado o es incorrecto, vuelva a ingresar su correo", "RecuperarPassword.aspx")
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Mensaje", script, True)

                End If

            Else
                lblMensaje.Text = "La nueva contrase�a no conincide con su verificaci�n"
            End If
        Else
            lblMensaje.Text = "La contrase�a debe ser como m�nimo de  " + DameMaximoTamanioContrasenia().ToString() + " caracteres. "
        End If
        

    End Sub

    Protected Sub CambiarPassword_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lblMensajeInformativo.Text = lblMensajeInformativo.Text.Replace("[Nro]", DameMaximoTamanioContrasenia().ToString)
        End If
    End Sub
    Public Function DameMaximoTamanioContrasenia() As Integer
        Return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings(SPE.EmsambladoComun.ParametrosSistema.MinCaracteresContrasenia))
    End Function
End Class
