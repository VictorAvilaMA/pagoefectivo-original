﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="CentrosPago.aspx.vb" Inherits="CentrosPago" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ulMenu li').removeClass('active');
            $('#liCentroPago').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="wrapper">
        <div id="container">
            <div class="content">
                <h2 class="title-seccion">
                    <span>&nbsp;</span> Centros de Pago Autorizados</h2>
                <ul class="nav-paid-center">
                    <li><a href="#ibk">Banco Interbank</a></li>
                    <li><a href="#bif">Banco BanBif</a></li>
                    <li><a href="#kas">Agente Kasnet</a></li>
                    <li><a href="#bbva">Banco BBVA Continental</a></li>
                    <li><a href="#bcp">Banco de Crédito</a></li>
                    <li><a href="#sc">Banco Scotiabank</a></li>
                    <li><a href="#wu">Western Union</a></li>
                    <li><a href="#fc">Full Carga</a></li>
                </ul>

                <div class="paid-center">
                    <a name="ibk"></a>
                    <h3 class="title-paid-center">
                        Interbank</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/interbank.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="Interbank" title="Interbank" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                Si eres cliente del Interbank</h4>
                            <p>
                                <u>V&iacute;a internet Interbank, seguir los siguientes pasos</u>:</p>
                            <ul>
                                <li>Ingrese a su cuenta y selecciona la opci&oacute;n <strong>Pago de Recibos - Diversas Empresas</strong>.</li>
                                <li>Seleccione la cuenta de Cargo y en Empresa seleccionar <strong>PAGOEFECTIVO</strong>.</li>
                                <li>Ingrese el siguiente <strong>c&oacute;digo de Pago:[CIP]</strong>, confirme su transacci&oacute;n y <strong>Listo!</strong></li>
                            </ul>
                        </div>
                        <div class="two-columns">
                            <h4>
                                Si no eres cliente Interbank</h4>
                            <p>
                                <u>En un Agente Interbank</u>:</p>
                            <ul>
                                <li>Indique que va realizar un pago a la empresa <strong>PagoEfectivo</strong> y brinda el siguiente c&oacute;digo 2735001.</li>
                                <li>Indique el <strong>C&oacute;digo de Pago: [CIP]</strong>, el importe a pagar y la moneda.</li>
                                <li>Verificar que el voucher indique <strong>PAGOEFECTIVO</strong>.</li>
                            </ul>
                            <p>
                                <u>En Tiendas y Money Market de Interbank</u>:</p>
                            <ul>
                                <li>Indique que va realizar un pago de recaudo a <strong>PAGOEFECTIVO</strong>.</li>
                                <li>Indique el <strong>C&oacute;digo de Pago: [CIP]</strong>, el importe a pagar y la moneda.</li>
                                <li>Verificar que el voucher indique <strong>PAGOEFECTIVO</strong>.</li>
                            </ul>
                            <p>
                                <u>En Cajeros Global Net con tarjeta</u>:</p>
                            <ul>
                                <li>Seleccione Pagos/Pagos de recibo/Pagos Varios/Varios.</li>
                                <li><strong>Digite el c&oacute;digo: 35001</strong> y siga las instrucciones del Cajero Autom&aacute;tico (*).</li>
                             </ul>
                             <p>
                                <u>En Cajeros sin Tarjeta - Global Net Plus</u>:</p>
                            <ul>
                                <li>Presione cualquier bot&oacute;n de la pantalla del cajero.</li>
                                <li>Seleccione Pagos de recibos/Otros/Otros.</li>
                                <li><strong>Digite el c&oacute;digo: 35001</strong> y siga las instrucciones del Cajero Autom&aacute;tico (*)</li>
                            </ul>
                            <p>
                            (*)Verificar que el voucher indique Empresa: <strong>PAGOEFECTIVO</strong>.</p>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="bif"></a>
                    <h3 class="title-paid-center">
                        BanBif</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/banbif.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="BanBif" title="BanBif" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                Banca por Internet del BANBIF:</h4>
                            <ul>
                                <li>Ingrese a su cuenta.</li>
                                <li>Seleccione la opci&oacute;n <strong>Pago de servicios\ Un nuevo Pago</strong>.</li>
                                <li>Realice la b&uacute;squeda por Empresa escribiendo <strong>PAGOEFECTIVO</strong> y seleccione la empresa Pago.</li>
                                <li>Ingrese el siguiente C&oacute;digo de Pago: <strong>[CIP]</strong>, confirme su transacci&oacute;n y Listo.</li>
                            </ul>
                        </div>
                        <div class="two-columns">
                            <h4>
                                En una agencia BanBif:</h4>
                            
                            <ul>
                                <li>Indique que desea realizar un pago de Recaudo a <strong>PAGOEFECTIVO</strong>.</li>
                                <li>Indique el C&oacute;digo de Pago: <strong>[CIP]</strong>, el importe a pagar y la moneda.</li>
                                <li>Verificar que el voucher indique <strong>PAGOEFECTIVO</strong>.</li>
                            </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="kas"></a>
                    <h3 class="title-paid-center">
                        Agente Kasnet</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/logo_kasnet.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="Agente Kasnet" title="Agente Kasnet" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                En un Agente Kasnet:</h4>
                            <%--<p>
                                <u>Ac&eacute;rcate a cualquier Establecimiento (Bodegas, Farmacias, Cabinas, Locutorios,
                                    Etc) que tenga el Logo de PagoEfectivo y/o FullCarga</u></p>--%>
                            <ul>
                                <li>Indique que va realizar un pago de recaudo a <strong>PagoEfectivo MN</strong>.</li>
                                <li>Indique el <strong>C&oacute;digo de Pago [CIP]</strong>y el importe a pagar.</li>
                                <li>Verificar que el voucher indique <strong>PAGO DE RECAUDO: PAGOEFECTIVO MN</strong>.</li>
                            </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="bbva"></a>
                    <h3 class="title-paid-center">
                        Banco BBVA Continental</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/bbva.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="Banco BBVA Continental" title="Banco BBVA Continental"
                            border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                Si eres cliente del BBVA</h4>
                                <p>
                                <u>V&iacute;a internet BBVA Continental, seguir los siguientes pasos</u>:</p>
                            <ul>
                                <li>Ingrese a su cuenta, seleccione<strong>Pago de Servicios, De instituciones y Emp.</strong>.</li>
                                <li>Realice la b&uacute;squeda de los servicios escribiendo <strong>PagoEfectivo</strong>. Luego seleccione <strong>PAGOEFECTIVO</strong>.</li>
                                <li>Ingrese el <strong>c&oacute;digo de Pago: [CIP]</strong>, confirme su transacci&oacute;n y <strong>Listo</strong>.</li>
                            </ul>
                        </div>
                        <div class="two-columns">
                            <h4>
                                Si no eres cliente del BBVA</h4>
                            <p>
                                <u>En una Agencia del BBVA / Agente Express</u>:</p>
                            <ul>
                                <li>Indique que va a realizar un <strong>pago de recaudo a PAGOEFECTIVO</strong>.</li>
                                <li>Indique la moneda, el <strong>C&oacute;digo de Pago: [CIP]</strong> y el el importe.</li>
                                <li>Verificar que el voucher indique <strong>EMISORA: PAGOEFECTIVO MN o PAGOEFECTIVO ME</strong>.</li>
                            </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="bcp"></a>
                    <h3 class="title-paid-center">
                        Banco de Cr&eacute;dito</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/bcp.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="Banco de Crédito" title="Banco de Crédito" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                Si eres cliente del BCP</h4>
                                <p>
                                <u>V&iacute;a internet BCP, seguir los siguientes pasos</u>:</p>
                            <ul>
                                <li>Ingrese a tu cuenta y seleccione la opci&oacute;n Pago de servicios, Empresas Diversas.</li>
                                <li>Seleccione la empresa <strong>PagoEfectivo</strong> y el servicio <strong>PagoEfectivo - Soles</strong>.</li>
                                <li>Ingrese el siguiente <strong>C&oacute;digo de Pago: [CIP]</strong>, confirme su transacci&oacute;n y <strong>Listo</strong>.</li>
                            </ul>
                        </div>
                        <div class="two-columns">
                            <h4>
                                Si no eres cliente del BCP</h4>
                            <p>
                                A. <u>En una Agencia BCP</u>:</p>
                            <ul>
                                <li>Indique que va a realizar un <strong>pago a la empresa PAGOEFECTIVO</strong>.</li>
                                <li>Indique el <strong>C&oacute;digo de Pago: [CIP]</strong>, el importe a pagar <strong>y la moneda</strong>.</li>
                                <li>Verificar que el voucher indique <strong>Empresa Afiliada: PAGOEFECTIVO</strong>.</li>
                            </ul>
                            <p style="margin-top: 8px;">
                                B. <u>En un Agente BCP</u>:</p>
                            <ul>
                                <li>Indique que va a realizar un pago a la empresa <strong>PagoEfectivo</strong> y brinde el siguiente <strong>c&oacute;digo 02186</strong>.</li>
                                <li>Indique el <strong>C&oacute;digo de Pago: [CIP]</strong>, el importe a pagar <strong>y la moneda</strong>.</li>
                                <li>Verificar que el voucher indique <strong>EMPRESA: PAGOEFECTIVO</strong>.</li>
                            </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="sc"></a>
                    <h3 class="title-paid-center">
                        Scotiabank</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/scotiabank.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="Scotiabank" title="Scotiabank" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                Si eres cliente de Scotiabank</h4>
                                <p>
                                <u>V&iacute;a internet Scotiabank, seguir los siguientes pasos</u>:</p>
                            
                            <ul>
                                <li>Ingrese a su cuenta y seleccione <strong>Pagos – Otras Instituciones, Otros</strong>.</li>
                                <li>Seleccionar la Instituci&oacute;n seg&uacute;n servicio: <strong>PAGOEFECTIVO (SOLES)</strong>.</li>
                                <li>Ingrese el siguiente <strong>C&oacute;digo de Pago: [CIP]</strong>, confirme su transacci&oacute;n <strong>y Listo.</strong>.</li>
                            </ul>
                        </div>
                        <div class="two-columns">
                            <h4>
                                Si no eres cliente de Scotiabank</h4>
                            <p>
                                <u>En una Agencia de Scotiabank o Cajero Express</u>:</p>
                            <ul>
                                <li>Indique que desea realizar un pago a la empresa <strong>PagoEfectivo</strong>.</li>
                                <li>Indicar el <strong>C&oacute;digo de Pago: [CIP]</strong>, el importe a pagar <strong>y la moneda</strong>.</li>
                                <li>Verificar que el voucher indique <strong>PAGOEFECTIVO</strong>.</li>
                            </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="wu"></a>
                    <h3 class="title-paid-center">
                        Western Union</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/western-union.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="Western Union" title="Western Union" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                En una Agencia de Pago de Servicios Western Union:</h4>
                            <%--<p>
                                <u>Ac&eacute;rcate a cualquier Establecimiento (Bodegas, Farmacias, Cabinas, Locutorios,
                                    Etc) que tenga el Logo de PagoEfectivo y/o FullCarga</u></p>--%>
                            <ul>
                                <li>Indique que va a realizar un pago de servicio a la empresa <strong>PagoEfectivo</strong>.</li>
                                <li>Indique el <strong>C&oacute;digo de Pago:[CIP] el importe a pagar y la moneda</strong>.</li>
                                <li>Verificar que el voucher indique <strong>Entidad: PagoEfectivo</strong>.</li>
                           </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="paid-center">
                    <a name="fc"></a>
                    <h3 class="title-paid-center">
                        Full Carga</h3>
                    <div class="img-paid-center">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/centros-pagos/full-carga.jpg?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
                            width="300" height="130" alt="FullCarga" title="FullCarga" border="0">
                    </div>
                    <div class="content-paid-center">
                        <div class="two-columns">
                            <h4>
                                En un Establecimiento autorizado:</h4>
                            
                            <ul>
                                <li>Ac&eacute;rquese a cualquier Establecimiento Bodega, Farmacias, Cabina o Locutorio
                                     que tenga el Logo de <strong>PagoEfectivo y/o FullCarga</strong>.</li>
                                <li>Indique que va a realizar un <strong>pago de Servicio a la empresa PagoEfectivo</strong>.</li>
                                <li>Elija el servicio <strong>PagoEfectivo-Soles</strong>.</li>
                                <li>Indique el <strong>c&oacute;digo de Pago: [CIP]</strong>.</li>
                            </ul>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
