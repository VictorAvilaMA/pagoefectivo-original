﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="ComerciosAfiliados.aspx.vb" Inherits="ComerciosAfiliados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ulMenu li').removeClass('active');
            $('#liComerciosAfiliados').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">
        <asp:Panel ID="pnlComerciosAfiliados" runat="server">
            <h2 class="title-seccion">
                <span>&nbsp;</span> Comercios Afiliados</h2>
            <div class="content-affiliates">
                <asp:Literal ID="ltlComerciosAfiliados" runat="server"></asp:Literal>
                <div class="clear">
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlProximosAfiliados" runat="server">
            <h2 class="title-seccion">
                <span>&nbsp;</span> Próximos Afiliados</h2>
            <div class="next-afiliates">
                <asp:Literal ID="ltlProximosAfiliados" runat="server"></asp:Literal>
                <div class="clear">
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
</asp:Content>
