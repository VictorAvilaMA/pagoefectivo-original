﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="ADDev.aspx.vb" Inherits="DEV_ADDev" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <style type="text/css">
        li span[style*="hidden"]
        {
            width: 0px !important;
        }
    </style>
    <h2>
        Registrar una Devoluci&oacute;n de dinero
    </h2>
    <asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="conten_pasos3">
                <div style="clear: both">
                </div>
                <h4>
                    1. Datos del CIP</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span>Nro. de CIP: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroCIP" runat="server" CssClass="normal" MaxLength="14"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbNroCIP" runat="server" TargetControlID="txtNroCIP"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="rfvNroCIP" runat="server" ErrorMessage="Debe ingresar el número del CIP."
                            ControlToValidate="txtNroCIP" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                        <asp:ImageButton ID="ibtnBuscarCIP" runat="server" ImageUrl="~/images/lupa.png" />
                    </li>
                    <li class="t1"><span class="color">>></span>Concepto de Pago: </li>
                    <li class="t2">
                        <asp:Label ID="lblConpeptopPago" runat="server" style="width:90%; text-align:left;">
                        </asp:Label>
                    </li>
                    <li class="t1"><span class="color">>></span> Monto: </li>
                    <li class="t2">
                        <asp:Label ID="lblMonto" runat="server" style="width:90%; text-align:left;">
                        </asp:Label>
                    </li>
                    <li class="t1"><span class="color">>></span> Servicio: </li>
                    <li class="t2">
                        <asp:Label ID="lblServicio" runat="server" style="width:90%; text-align:left;">
                        </asp:Label>
                    </li>
                    <li class="t1"><span class="color">>></span> Fecha Emitido: </li>
                    <li class="t2">
                        <asp:Label ID="lblFechaEmision" runat="server" style="width:90%; text-align:left;">
                        </asp:Label>
                    </li>
                    <li class="t1"><span class="color">>></span> Fecha Pago: </li>
                    <li class="t2">
                        <asp:Label ID="lblFechaCancelacion" runat="server" style="width:90%; text-align:left;">
                        </asp:Label>
                    </li>
                    <li class="t1"><span class="color">>></span>Estado: </li>
                    <li class="t2">
                        <asp:Label ID="lblEstado" runat="server" style="width:90%; text-align:left;">
                        </asp:Label>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <h4>
                    2. Datos del Cliente</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span>Nro. de DNI: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteNroDNI" runat="server" CssClass="normal" MaxLength="8"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteNroDNI" runat="server" TargetControlID="txtClienteNroDNI"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="rfvClienteNroDNI" runat="server" ErrorMessage="Debe ingresar el número de DNI del cliente."
                            ControlToValidate="txtClienteNroDNI" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revClienteNroDNI" runat="server" ControlToValidate="txtClienteNroDNI"
                            ValidationExpression="^((?!00000000)[\s\S]){8,8}" ErrorMessage="El Nro de DNI es inválido">*</asp:RegularExpressionValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Nombres del cliente : </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteNombres" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvClienteNombres" runat="server" ErrorMessage="Debe ingresar el nombre del cliente."
                            ControlToValidate="txtClienteNombres" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteNombres" runat="server" TargetControlID="txtClienteNombres"
                            FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" 'áéíóúÁÉÍÓÚñÑ">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">>></span> Apellidos del cliente : </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteApellidos" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvClienteApellidos" runat="server" ErrorMessage="Debe ingresar el apellido del cliente."
                            ControlToValidate="txtClienteApellidos" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteApellidos" runat="server" TargetControlID="txtClienteApellidos"
                            FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" 'áéíóúÁÉÍÓÚñÑ">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">>></span>Direcci&oacute;n del cliente: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteDireccion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvClienteDirrecion" runat="server" ErrorMessage="Debe ingresar la direccion del cliente."
                            ControlToValidate="txtClienteDireccion" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <h4>
                    3. Solicitud</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span>Constancia de Devolución: </li>
                    <li class="t2">
                        <asp:FileUpload ID="fuArchivoDevolucion" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvFileUpload" runat="server" ErrorMessage="Debe seleccionar un archivo (Max. 2Mb)."
                            ControlToValidate="fuArchivoDevolucion" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                            <br />
                       
                    </li>
                    <li class="t1"><span class="color">>></span>Comentarios: </li>
                    <li class="t2 txtArea">
                        <asp:TextBox ID="txtObservaciones" runat="server" TextMode="MultiLine"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbObservaciones" runat="server" TargetControlID="txtObservaciones"
                            FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="$#<>^@;,.:%& -_/\*+?¿=()¡![]{}&quot;'@áéíóúÁÉÍÓÚñÑ">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"></li>
                    <li class="t2">
                        <asp:Label ID="lblmensajeObservaciones" runat="server" ForeColor="Red" Font-Bold="true"
                            CssClass="textAreaContador"></asp:Label>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                            Text="Registrar" ValidationGroup="ValidaCampos" />
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul3" Text="Nuevo" Visible="false" />
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <asp:Label ID="lblMensaje" runat="server" Style="padding-left: 30px"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 30px;
                    padding-bottom: 40px" />
                <asp:HiddenField ID="hdfIdDevolucion" runat="server"></asp:HiddenField>
            </div>
        </ContentTemplate>
        
        <triggers>
            <asp:PostBackTrigger ControlID="btnRegistrar" />
        </triggers>
        
    </asp:UpdatePanel>
</asp:Content>
