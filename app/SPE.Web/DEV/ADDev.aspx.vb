﻿Imports _3Dev.FW.Entidades
Imports SPE.Entidades
Imports SPE.Web
Imports System.IO
Imports System.Configuration
Imports System.Globalization
Imports System.Configuration.ConfigurationManager

Partial Class DEV_ADDev
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.COrdenPago)

    Protected Overrides ReadOnly Property BtnInsert As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.URLPageCancelForEdit = "CODev.aspx"
        e.URLPageCancelForInsert = "CODev.aspx"
        e.URLPageCancelForSave = "CODev.aspx"
    End Sub
    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim TieneArchivo As Boolean
        Dim oBEDevolucion As New BEDevolucion
        TieneArchivo = fuArchivoDevolucion.HasFile
        'If txtClienteNroDNI.Text <> String.Empty And txtClienteNombres.Text <> String.Empty And txtClienteApellidos.Text <> String.Empty And txtClienteDireccion.Text <> String.Empty And txtObservaciones.Text <> String.Empty And lblServicio.Text <> String.Empty And lblEstado.Text <> String.Empty And lblMonto.Text <> String.Empty And TieneArchivo <> False Then
        Dim TamanioArchivo As String = AppSettings("TamanioArchivo")
        If lblMonto.Text.Trim() <> String.Empty Then
            If txtClienteNroDNI.Text.Trim() <> String.Empty Then
                If txtClienteNombres.Text.Trim() <> String.Empty Then
                    If txtClienteApellidos.Text.Trim() <> String.Empty Then
                        If txtClienteDireccion.Text.Trim() <> String.Empty Then
                            If TieneArchivo Then
                                If Not fuArchivoDevolucion.PostedFile.ContentLength > CInt(TamanioArchivo) Then
                                    Dim validFileTypes As String() = {"bmp", "jpg", "png", "jpeg", "doc", "docx", "xls", "xlsx", "pdf"}
                                    Dim ext As String = Path.GetExtension(fuArchivoDevolucion.FileName)
                                    Dim isValidType As Boolean = False
                                    For i As Integer = 0 To validFileTypes.Length - 1
                                        If ext = "." & validFileTypes(i) Then
                                            isValidType = True
                                            Exit For
                                        End If
                                    Next
                                    If isValidType Then
                                        oBEDevolucion.MensajeArchivoDevolucion = String.Empty
                                        Try
                                            Dim Times As String
                                            Dim Extension As String
                                            Extension = Path.GetExtension(fuArchivoDevolucion.FileName)
                                            Times = Date.Now().ToString("ddMMyyyyzzmmss")
                                            'Dim NombreArchivoImagen As String = (Replace(txtClienteNombres.Text.Trim(), " ", "") + Replace(txtClienteApellidos.Text.Trim(), " ", "")) + "_" + Times
                                            oBEDevolucion.ArchivoDevolucion = fuArchivoDevolucion.FileBytes
                                            oBEDevolucion.NombreArchivoDevolucion = fuArchivoDevolucion.FileName
                                            oBEDevolucion.ExtensionArchivoDevolucion = Extension
                                            oBEDevolucion.MensajeArchivoDevolucion = String.Empty
                                            '-----------------------------------------------------------------------------
                                            oBEDevolucion.IdUsuarioCreacion = UserInfo.IdUsuario
                                            oBEDevolucion.EmailRepresentante = UserInfo.Email
                                            oBEDevolucion.NroCIP = txtNroCIP.Text.Trim
                                            oBEDevolucion.ClienteNroDNI = txtClienteNroDNI.Text.Trim
                                            oBEDevolucion.ClienteNombres = txtClienteNombres.Text.Trim
                                            oBEDevolucion.ClienteApellidos = txtClienteApellidos.Text.Trim
                                            oBEDevolucion.ClienteDireccion = txtClienteDireccion.Text.Trim
                                            oBEDevolucion.Observaciones = txtObservaciones.Text.Trim
                                            oBEDevolucion.ServicioDescripcion = lblServicio.Text.Trim()
                                            oBEDevolucion.EstadoCipDescripcion = lblEstado.Text.Trim()
                                            oBEDevolucion.FechaCreacion = DateTime.Now.ToString("dd/MM/yyyy")
                                            oBEDevolucion.UsuarioCreacionNombre = UserInfo.NombreCompleto
                                            oBEDevolucion.Monto = lblMonto.Text.Trim()
                                            '-----------------------------------------------------------------------------
                                        Catch ex As Exception
                                            'StatusLabel.Text = "Estado Upload: Ocurrió un error y el archivo no puede ser cargado."
                                        End Try
                                    Else
                                        oBEDevolucion = Nothing
                                        ThrowErrorMessage("Tipo de archivo inválido.")
                                        Return oBEDevolucion
                                    End If
                                Else
                                    oBEDevolucion = Nothing
                                    ThrowErrorMessage("El archivo no puede ser mayor a 2 MB.")
                                    Return oBEDevolucion
                                End If
                            Else
                                oBEDevolucion = Nothing
                                ThrowErrorMessage("Debe seleccionar un archivo (Max. 2Mb).")
                                Return oBEDevolucion
                            End If
                        Else
                            oBEDevolucion = Nothing
                            ThrowErrorMessage("Debe ingresar la direccion del cliente.")
                            Return oBEDevolucion
                        End If
                    Else
                        oBEDevolucion = Nothing
                        ThrowErrorMessage("Debe ingresar el apellido del cliente.")
                        Return oBEDevolucion
                    End If
                Else
                    oBEDevolucion = Nothing
                    ThrowErrorMessage("Debe ingresar el nombre del cliente.")
                    Return oBEDevolucion
                End If
            Else
                oBEDevolucion = Nothing
                ThrowErrorMessage("Debe ingresar el número de DNI del cliente.")
                Return oBEDevolucion
            End If
        Else
            oBEDevolucion = Nothing
            ThrowErrorMessage("Debe completar la búsqueda del CIP.")
        End If
        Return oBEDevolucion
    End Function
    Public Overrides Sub OnMainInsert()
        Dim businessEntityObject As BusinessEntityBase = CreateBusinessEntityForRegister(Nothing)
        If businessEntityObject IsNot Nothing Then
            Dim oBePrueba As New BEDevolucion()
            Dim oBEDevolucionRetorno As New BEDevolucion()
            oBePrueba = businessEntityObject
            If oBePrueba.MensajeArchivoDevolucion = String.Empty Then
                Dim cOrdenPago As New COrdenPago
                oBEDevolucionRetorno = cOrdenPago.RegistrarDevolucion(businessEntityObject)

                If oBEDevolucionRetorno.CodigoError = 0 Then
                    ThrowSuccessfullyMessage(oBEDevolucionRetorno.MensajeRetorno)
                    EnabledButtonControls()
                    DesactivarControles()
                    btnNuevo.Visible = True
                Else
                    ThrowErrorMessage(oBEDevolucionRetorno.MensajeRetorno)
                End If


                'Select Case intResult
                '    Case Is > 0
                '        ThrowSuccessfullyMessage("Se generó con éxito la Solicitud Nº " & intResult)
                '        EnabledButtonControls()
                '        DesactivarControles()
                '        btnNuevo.Visible = True
                '    Case -1
                '        ThrowErrorMessage("Hubo un error en la aplicación")
                '    Case -2
                '        ThrowErrorMessage("El cip ingresado no existe")
                '    Case -3
                '        ThrowErrorMessage("El cip ingresado no pertenece a su empresa")
                '    Case -4
                '        ThrowErrorMessage("El cip ingresado no se encuentra pagado")
                '    Case -5
                '        ThrowErrorMessage("Ya existe una devolución registrada para el CIP ingresado")
                '    Case Else
                '        ThrowErrorMessage("Hubo un error en la aplicación")
                'End Select
            Else
                ThrowErrorMessage(oBePrueba.MensajeArchivoDevolucion)
            End If


        End If
    End Sub
    Public Sub DesactivarControles()
        txtNroCIP.Enabled = False
        txtClienteApellidos.Enabled = False
        txtClienteDireccion.Enabled = False
        txtClienteNombres.Enabled = False
        txtClienteNroDNI.Enabled = False
        txtObservaciones.Enabled = False
        btnRegistrar.Visible = False
    End Sub

    Protected Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("ADDev.aspx")
    End Sub

    Protected Sub DEV_ADDev_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txtObservaciones.Attributes.Add("onkeypress", " ValidarCaracteres2(this, 250, $('#" + lblmensajeObservaciones.ClientID + "')); LimpiarResultado();")
        txtObservaciones.Attributes.Add("onkeyup", " ValidarCaracteres2(this, 250, $('#" + lblmensajeObservaciones.ClientID + "'));")
        'Page.Form.Attributes.Add("enctype", "multipart/form-data")
    End Sub

    Protected Sub ibtnBuscarCIP_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ibtnBuscarCIP.Click

        Dim NroCip As String
        NroCip = txtNroCIP.Text.Trim()
        If Not NroCip = String.Empty Then
            Dim cOrdenPago As New COrdenPago
            Dim oBEOrdenPago As New BEOrdenPago()
            oBEOrdenPago = cOrdenPago.ConsultarCipDevolucion(CInt(NroCip), UserInfo.IdUsuario)
            If oBEOrdenPago IsNot Nothing Then

                If String.IsNullOrEmpty(oBEOrdenPago.MensajeRetorno) Then
                    lblConpeptopPago.Text = oBEOrdenPago.ConceptoPago
                    lblMonto.Text = oBEOrdenPago.SimboloMoneda + " " + oBEOrdenPago.Total.ToString()
                    lblServicio.Text = oBEOrdenPago.NombreServicio
                    lblEstado.Text = oBEOrdenPago.DescripcionEstado
                    lblFechaEmision.Text = oBEOrdenPago.FechaEmision.ToString("dd/MM/yyyy hh:mm:ss")
                    lblFechaCancelacion.Text = oBEOrdenPago.FechaCancelacion.ToString("dd/MM/yyyy hh:mm:ss")
                    lblMensaje.Text = String.Empty
                Else
                    lblConpeptopPago.Text = String.Empty
                    lblMonto.Text = String.Empty
                    lblServicio.Text = String.Empty
                    lblEstado.Text = String.Empty
                    lblFechaEmision.Text = String.Empty
                    lblFechaCancelacion.Text = String.Empty
                    txtClienteNroDNI.Text = String.Empty
                    txtClienteNombres.Text = String.Empty
                    txtClienteApellidos.Text = String.Empty
                    txtClienteDireccion.Text = String.Empty
                    txtObservaciones.Text = String.Empty
                    ThrowErrorMessage(oBEOrdenPago.MensajeRetorno)
                End If

            Else
                lblConpeptopPago.Text = String.Empty
                lblMonto.Text = String.Empty
                lblServicio.Text = String.Empty
                lblEstado.Text = String.Empty
                lblFechaEmision.Text = String.Empty
                lblFechaCancelacion.Text = String.Empty
                ThrowErrorMessage("El cip ingresado no existe y/o no es de la empresa que representa.")
            End If
        Else
            lblConpeptopPago.Text = String.Empty
            lblMonto.Text = String.Empty
            lblServicio.Text = String.Empty
            lblEstado.Text = String.Empty
            lblFechaEmision.Text = String.Empty
            lblFechaCancelacion.Text = String.Empty
            ThrowErrorMessage("Debe ingresar el número de CIP para la búsqueda.")
        End If
    End Sub

    Public Function ObtenerImagen() As Byte()
        Return _3Dev.FW.Web.WebUtil.GetImage(fuArchivoDevolucion)
    End Function
End Class
