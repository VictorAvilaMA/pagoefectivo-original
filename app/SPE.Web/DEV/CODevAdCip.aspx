﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="CODevAdCip.aspx.vb" Inherits="DEV_CODevAdCip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptGlobalization="true">
    </asp:ScriptManager>
    <style type="text/css">
        .ajax__calendar_footer
        {
            display: none;
        }
        .lupa
        {
            background: url(../images/lupa.gif);
            border: 0px;
            width: 16px;
            height: 16px;
        }
        .close
        {
            background: url(../img/closeX.GIF);
            border: 0px;
            width: 14px;
            height: 14px;
        }
        .close:hover, .lupa:hover
        {
            cursor: pointer;
        }
    </style>
    <h2>
        Consultar Devoluciones
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de Búsqueda
        </h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip4 f0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="corta">
                            <asp:ListItem Value="691" Text="Solicitado"></asp:ListItem>
                            <asp:ListItem Value="692" Text="En Proceso"></asp:ListItem>
                            <asp:ListItem Value="693" Text="Cheque Generado"></asp:ListItem>
                            <asp:ListItem Value="694" Text="Emitido"></asp:ListItem>
                            <asp:ListItem Value="695" Text="Expirado"></asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro de Solicitud: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroDevolucionFilter" runat="server" CssClass="corta" MaxLength="8"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbNroDevolucion" runat="server" TargetControlID="txtNroDevolucionFilter"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                </ul>
                <ul class="datos_cip4 f0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombre del cliente: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteNombreFilter" runat="server" CssClass="corta" MaxLength="100"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteNombres" runat="server" TargetControlID="txtClienteNombreFilter"
                            FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" 'áéíóúÁÉÍÓÚñÑ">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro de DNI: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteNroDNIFilter" runat="server" CssClass="corta" MaxLength="8"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteNroDNI" runat="server" TargetControlID="txtClienteNroDNIFilter"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro de CIP: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroCIPFilter" runat="server" CssClass="corta" MaxLength="14"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbNroCIP" runat="server" TargetControlID="txtNroCIPFilter"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <ul class="datos_cip2 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha del:</li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaInicio" Display="Dynamic" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Del' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Del' no válida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaFin"
                                Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                Text="*"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaFin" Display="None" Visible="true" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Al' no válida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div style="clear: both;">
                            </div>
                            <%--<asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de búsqueda no es válido"
                                ControlToCompare="txtFechaFin" ControlToValidate="txtFechaInicio" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>--%>
                        </p>
                    </li>
                </ul>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                    PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                    PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" />
                <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Text="Excel" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Visible="false"></asp:Label>
        <div class="result">
            <asp:UpdatePanel ID="upResultado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblMensajeVal" runat="server" ForeColor="Red"></asp:Label>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text="Registros:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                            AutoGenerateColumns="False" DataKeyNames="IdDevolucion" AllowPaging="True">
                            <Columns>
                                <asp:BoundField DataField="IdDevolucion" HeaderText="Nro. Solicitud" SortExpression="IdDevolucion">
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Solicitud" SortExpression="FechaCreacion">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Fecha Generación" SortExpression="FechaChequeGenerado">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaChequeGenerado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FechaChequeGenerado")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Wrap="false" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="NroCIP" HeaderText="Nro. de CIP" SortExpression="NroCIP">
                                </asp:BoundField>
                                <asp:BoundField DataField="SimboloMoneda" HeaderText="Moneda" SortExpression="SimboloMoneda">
                                </asp:BoundField>
                                <asp:BoundField DataField="Monto" HeaderText="Monto" SortExpression="Monto"></asp:BoundField>
                                <asp:BoundField DataField="ClienteNroDNI" HeaderText="Nro DNI" SortExpression="ClienteNroDNI">
                                </asp:BoundField>
                                <asp:BoundField DataField="NombreCompletoCliente" HeaderText="Cliente" SortExpression="ClienteNombres">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstadoAdmin" HeaderText="Estado" SortExpression="DescripcionEstadoAdmin">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="ibtEdit" runat="server" ToolTip="Ver detalle" CommandName="Edit"
                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem,"IdDevolucion")%>' CssClass="lupa" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.</EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                        <cc1:ModalPopupExtender ID="mppDevolucion" runat="server" BackgroundCssClass="modalBackground"
                            CancelControlID="imgbtnRegresarHidden" PopupControlID="pnlPopupActualizarDevolucion"
                            TargetControlID="imgbtnTargetHidden" Y="0">
                        </cc1:ModalPopupExtender>
                        <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" Style="display: none;"
                            CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                        <asp:ImageButton ID="imgbtnTargetHidden" runat="server" Style="display: none;" CssClass="Hidden"
                            CausesValidation="false"></asp:ImageButton>
                        <asp:UpdatePanel ID="upBarza" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="pnlPopupActualizarDevolucion" runat="server" class="conten_pasos3"
                                    Style="display: none; background: white; padding-bottom: 0px; width: 630px; height: 275px">
                                    <div style="float: right">
                                        <asp:Button ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                                            CausesValidation="false" CssClass="close" />
                                    </div>
                                    <%--ModeloAdd--%>
                                    <div style="height: 500px; overflow-y: scroll; overflow-x: hidden; width: 615px;">
                                        <div style="clear: both">
                                        </div>
                                        <h4>
                                            1. Datos del CIP</h4>
                                        <div class="contentForm clearfix">
                                            <div class="left">
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Nro. de CIP:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="txtNroCIP" runat="server" CssClass="normal" MaxLength="14"></asp:Label>
                                                        <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNroCIP"
                                                            ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                        <asp:RequiredFieldValidator ID="rfvNroCIP" runat="server" ErrorMessage="Debe ingresar el número del CIP."
                                                            ControlToValidate="txtNroCIP" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span> Monto:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblMonto" runat="server" Style="width: 90%; text-align: left;">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span> Servicio:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblServicio" runat="server" Style="width: 90%; text-align: left;">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="right">
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span> Fecha Emitido:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaEmision" runat="server" Style="width: 90%; text-align: left;">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span> Fecha Pago:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaCancelacion" runat="server" Style="width: 90%; text-align: left;">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Estado:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblEstado" runat="server" Style="width: 90%; text-align: left;">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="datos_cip2 type2">
                                            <li class="t1" style="width: 154px;"><span class="color">>></span>Concepto de Pago:
                                            </li>
                                            <li class="t2">
                                                <asp:Label ID="lblConpeptopPago" runat="server" Style="width: 90%; text-align: left;">
                                                </asp:Label>
                                            </li>
                                        </ul>
                                        <div style="clear: both">
                                        </div>
                                        <h4>
                                            2. Datos del Cliente</h4>
                                        <ul class="datos_cip2">
                                            <li class="t1"><span class="color">>></span>Nro. de DNI: </li>
                                            <li class="t2">
                                                <asp:TextBox ID="txtClienteNroDNI" runat="server" CssClass="normal" MaxLength="8"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtClienteNroDNI"
                                                    ValidChars="0123456789">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="rfvClienteNroDNI" runat="server" ErrorMessage="Debe ingresar el número de DNI del cliente."
                                                    ControlToValidate="txtClienteNroDNI" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revClienteNroDNI" runat="server" ControlToValidate="txtClienteNroDNI"
                                                    ValidationExpression="^((?!00000000)[\s\S]){8,8}" ErrorMessage="El Nro de DNI es inválido">*</asp:RegularExpressionValidator>
                                            </li>
                                            <li class="t1"><span class="color">>></span> Nombres del cliente : </li>
                                            <li class="t2">
                                                <asp:TextBox ID="txtClienteNombres" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvClienteNombres" runat="server" ErrorMessage="Debe ingresar el nombre del cliente."
                                                    ControlToValidate="txtClienteNombres" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtClienteNombres"
                                                    FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" 'áéíóúÁÉÍÓÚñÑ">
                                                </cc1:FilteredTextBoxExtender>
                                            </li>
                                            <li class="t1"><span class="color">>></span> Apellidos del cliente : </li>
                                            <li class="t2">
                                                <asp:TextBox ID="txtClienteApellidos" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvClienteApellidos" runat="server" ErrorMessage="Debe ingresar el apellido del cliente."
                                                    ControlToValidate="txtClienteApellidos" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="ftbClienteApellidos" runat="server" TargetControlID="txtClienteApellidos"
                                                    FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" 'áéíóúÁÉÍÓÚñÑ">
                                                </cc1:FilteredTextBoxExtender>
                                            </li>
                                            <li class="t1"><span class="color">>></span>Direcci&oacute;n del cliente: </li>
                                            <li class="t2">
                                                <asp:TextBox ID="txtClienteDireccion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvClienteDirrecion" runat="server" ErrorMessage="Debe ingresar la direccion del cliente."
                                                    ControlToValidate="txtClienteDireccion" ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                            </li>
                                        </ul>
                                        <div style="clear: both">
                                        </div>
                                        <h4>
                                            3. Solicitud</h4>
                                        <div class="contentForm clearfix type2" style="padding-bottom: 0px;">
                                            <div class="left">
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>N° Solicitud:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblNumeroSolicitud" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Fecha Ch. Gen.:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaChequeGenerado" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Flag En Proceso:
                                                    </label>
                                                    <div class="input" style="padding-top: 1px;">
                                                        <asp:CheckBox ID="chbxEnProceso" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="right">
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span> Fecha Registro:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaRegistro" runat="server" Style="width: 90%; text-align: left;">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Fecha Emitido:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaEmitido" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Fecha Expirado:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaExpirado" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <span class="color">>></span>Fecha Proceso:
                                                    </label>
                                                    <div class="input">
                                                        <asp:Label ID="lblFechaEnProceso" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="datos_cip2 clearfix type2" style="margin-top: 0px;">
                                            <li class="t1"><span class="color">>></span>Constancia de Devolución: </li>
                                            <li class="t2">
                                                <asp:Button ID="btnDescargar" runat="server" Text="Descargar Archivo" Style="margin-left: 0px;" />
                                            </li>
                                            <li class="t1"><span class="color">>></span>Estado: </li>
                                            <li class="t2">
                                                <asp:DropDownList ID="ddlEstadoDevolucion" runat="server" Style="margin-left: 0px;">
                                                    <asp:ListItem Value="691" Text="Solicitado"></asp:ListItem>
                                                    <asp:ListItem Value="692" Text="En Proceso"></asp:ListItem>
                                                    <asp:ListItem Value="693" Text="Cheque Generado"></asp:ListItem>
                                                    <asp:ListItem Value="694" Text="Emitido"></asp:ListItem>
                                                    <asp:ListItem Value="695" Text="Expirado"></asp:ListItem>
                                                </asp:DropDownList>
                                            </li>
                                            <li class="t1"><span class="color">>></span>Comentarios: </li>
                                            <li class="t2 txtArea">
                                                <asp:TextBox ID="txtObservaciones" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="ftbObservaciones" runat="server" TargetControlID="txtObservaciones"
                                                    FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="$#<>^@;,.:%& -_/\*+?¿=()¡![]{}&quot;'@áéíóúÁÉÍÓÚñÑ">
                                                </cc1:FilteredTextBoxExtender>
                                            </li>
                                            <li class="t1"></li>
                                            <li class="t2">
                                                <asp:Label ID="lblmensajeObservaciones" runat="server" ForeColor="Red" Font-Bold="true"
                                                    CssClass="textAreaContador"></asp:Label>
                                            </li>
                                        </ul>
                                        <div style="clear: both">
                                        </div>
                                        <asp:Label ID="Label1" runat="server" Style="padding-left: 30px"></asp:Label>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 200px;
                                            padding-bottom: 40px" />
                                        <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdfNombreArchivoDevolucion" runat="server"></asp:HiddenField>
                                    </div>
                                    <%--ModeloAdd--%>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <ul class="datos_cip2" style="margin-top: 0px;">
                                                <li class="complet" style="margin-bottom: 0px;">
                                                    <asp:Label ID="lblMensajeValPop" runat="server" style="color: Red; width: 400px; text-align: left;"></asp:Label>
                                                </li>
                                                <li class="complet" style="margin-top: 0px;">
                                                    <asp:Button ID="btnPActualizar" runat="server" CssClass="input_azul3" Text="Actualizar"
                                                        Style="margin-left: 150px;" />
                                                    <asp:Button ID="btnPCancelar" runat="server" CssClass="input_azul3" Text="Cerrar"
                                                        Style="margin-left: 100px;" />
                                                </li>
                                            </ul>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnPActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:HiddenField ID="hdfIdDevolucion" runat="server" />
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnDescargar" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnPActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        $('#<%= txtNroCIPFilter.ClientID() %>,#<%= txtClienteNombreFilter.ClientID() %>,#<%= txtNroDevolucionFilter.ClientID() %>,#<%= txtClienteNroDNIFilter.ClientID() %>,#<%= txtFechaInicio.ClientID() %>,#<%= txtFechaFin.ClientID() %>').keypress(function (e) {
            if (e.which == 13) {
                $('#<%= btnBuscar.ClientID() %>').click();
                return false;
            }
        });
        function AjaxEnd(sender, args) {
            if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').length > 0)
                if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').text() != 'Página :')
                    $('.pagerStyle>td>table>tbody>tr>td:eq(0)').before('<td>P&aacute;gina :</td>');
            $(".divLoadingMensaje").hide('fast');
            $('.divLoading').hide('fold');
            $('#<%= txtNroCIPFilter.ClientID() %>,#<%= txtClienteNombreFilter.ClientID() %>,#<%= txtNroDevolucionFilter.ClientID() %>,#<%= txtClienteNroDNIFilter.ClientID() %>,#<%= txtFechaInicio.ClientID() %>,#<%= txtFechaFin.ClientID() %>').unbind("keypress");
            $('#<%= txtNroCIPFilter.ClientID() %>,#<%= txtClienteNombreFilter.ClientID() %>,#<%= txtNroDevolucionFilter.ClientID() %>,#<%= txtClienteNroDNIFilter.ClientID() %>,#<%= txtFechaInicio.ClientID() %>,#<%= txtFechaFin.ClientID() %>').keypress(function (e) {
                if (e.which == 13) {
                    $('#<%= btnBuscar.ClientID() %>').click();
                    return false;
                }
            });
        }
    </script>
</asp:Content>
