﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Linq
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports System.Net
Imports System.IO
Imports Microsoft.Reporting.WebForms


Partial Class DEV_CODevAdCip
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)
    Protected Overrides ReadOnly Property BtnSearch As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnNew As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Public Overrides Function AllowToDelete(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As Boolean
        Return False
    End Function
    Public Overrides Sub OnGoToMaintenanceEditPage(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        e.NewEditIndex = -1
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mppDevolucion.Hide()
        If Not IsPostBack Then
            Dim obeServicio As New BEServicio
            Dim cCServicio As New SPE.Web.CServicio
            Dim oCAdministrarParametro As New SPE.Web.CAdministrarParametro
            Dim listaParametros As New List(Of BEParametro), listaParametrosFilter As New List(Of BEParametro)
            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
            DropDownlistBinding(ddlServicio, cCServicio.ConsultarServiciosPorIdUsuario(obeServicio), "Nombre", "IdServicio", "::Todos::")
            listaParametros = oCAdministrarParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoDevolucion)
            'For Each obj As BEParametro In listaParametros
            '    If obj.Id = SPE.EmsambladoComun.ParametrosSistema.EstadoDevolucion.Solicitado Or _
            '        obj.Id = SPE.EmsambladoComun.ParametrosSistema.EstadoDevolucion.Realizado Then
            '        listaParametrosFilter.Add(obj)
            '    End If
            'Next

            DropDownlistBinding(ddlEstado, listaParametros.OrderBy(Function(p1) p1.Id).ToList(), "Descripcion", "Id", "::Todos::")
            txtFechaInicio.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
            txtFechaFin.Text = Today.ToString("dd/MM/yyyy")
        End If
    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As BusinessEntityBase) As System.Collections.Generic.List(Of BusinessEntityBase)
        Dim cOrdenPago As New COrdenPago
        Dim ListaDevoluciones As New List(Of BusinessEntityBase)
        Dim oBEDevolucionesMLAE As New BEDevolucion()
        oBEDevolucionesMLAE = be
        If oBEDevolucionesMLAE.MensajeArchivoDevolucion = String.Empty Then
            ListaDevoluciones = cOrdenPago.ConsultarDevoluciones(be)
            ViewState("ListaDevoluciones") = ListaDevoluciones
            lblMensajeVal.Text = String.Empty
        Else
            gvResultado.DataSource = ListaDevoluciones
            gvResultado.DataBind()
            ViewState("ListaDevoluciones") = ListaDevoluciones
            lblMensajeVal.Text = oBEDevolucionesMLAE.MensajeArchivoDevolucion
        End If
        Return ListaDevoluciones
    End Function
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim oBEDevolucion As New BEDevolucion
        Dim IdDevolucion As Int64 = -1
        If Not Int64.TryParse(txtNroDevolucionFilter.Text, IdDevolucion) Then
            IdDevolucion = -1
        End If
        If StringToDateTime(txtFechaInicio.Text) > StringToDateTime(txtFechaFin.Text) Then
            oBEDevolucion.MensajeArchivoDevolucion = "La fecha de inicio no debe ser mayor a la fecha fin."
            Return oBEDevolucion
        Else
            oBEDevolucion.IdDevolucion = IdDevolucion
            oBEDevolucion.NroCIP = txtNroCIPFilter.Text
            oBEDevolucion.IdServicio = 0
            If Not String.IsNullOrEmpty(ddlServicio.SelectedValue) Then
                oBEDevolucion.IdServicio = CInt(ddlServicio.SelectedValue)
            End If
            oBEDevolucion.ClienteNombres = txtClienteNombreFilter.Text
            oBEDevolucion.IdEstadoCliente = 0
            oBEDevolucion.IdEstadoAdmin = 0
            oBEDevolucion.IdEmpresaContratante = 0
            If Not String.IsNullOrEmpty(ddlEstado.SelectedValue) Then
                oBEDevolucion.IdEstadoCliente = CInt(ddlEstado.SelectedValue)
                oBEDevolucion.IdEstadoAdmin = CInt(ddlEstado.SelectedValue)
            End If
            oBEDevolucion.IdUsuarioCreacion = UserInfo.IdUsuario
            oBEDevolucion.ClienteNroDNI = txtClienteNroDNIFilter.Text
            oBEDevolucion.FechaInicio = StringToDateTime(txtFechaInicio.Text)
            oBEDevolucion.FechaFinal = StringToDateTime(txtFechaFin.Text)
            Return oBEDevolucion
        End If
    End Function
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing And ResultList.Count() > 0 Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, ResultList(0).TotalPageNumbers)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        End If
        divResult.Visible = True
        mppDevolucion.Hide()
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("ADDev.aspx")
    End Sub

    Protected Sub gvResultado_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Edit" : CargarPopup(CInt(e.CommandArgument))
        End Select
    End Sub
    Private Sub CargarPopup(ByVal id As Integer)
        Dim objTransferencia As New BETransferencia
        Dim listaParametros As New List(Of BEParametro), listaParametrosFilter As New List(Of BEParametro)
        Dim objT As New BETransferencia
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        Dim oCAdministrarParametro As New SPE.Web.CAdministrarParametro
        hdfIdDevolucion.Value = id
        listaParametros = oCAdministrarParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoDevolucion)
        Dim ListaDevoluciones As List(Of BusinessEntityBase) = CType(ViewState("ListaDevoluciones"), List(Of BusinessEntityBase))
        For Each item As BusinessEntityBase In ListaDevoluciones
            Dim oBEDevolucion As BEDevolucion = CType(item, BEDevolucion)
            If oBEDevolucion.IdDevolucion = id Then
                txtClienteApellidos.Text = oBEDevolucion.ClienteApellidos
                txtClienteDireccion.Text = oBEDevolucion.ClienteDireccion
                txtClienteDireccion.ToolTip = oBEDevolucion.ClienteDireccion
                txtClienteNombres.Text = oBEDevolucion.ClienteNombres
                txtClienteNroDNI.Text = oBEDevolucion.ClienteNroDNI
                txtNroCIP.Text = oBEDevolucion.NroCIP
                'txtEstado.Text = oBEDevolucion.DescripcionEstadoCliente
                txtObservaciones.Text = oBEDevolucion.Observaciones
                lblConpeptopPago.Text = oBEDevolucion.ConceptoPago
                lblMonto.Text = oBEDevolucion.SimboloMoneda + " " + oBEDevolucion.Monto.ToString()
                lblServicio.Text = oBEDevolucion.ServicioDescripcion
                lblEstado.Text = oBEDevolucion.EstadoCipDescripcion
                hdfNombreArchivoDevolucion.Value = oBEDevolucion.NombreArchivoDevolucion

                lblFechaEmision.Text = IIf(oBEDevolucion.FechaEmision.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaEmision.ToString("dd/MM/yyyy hh:mm"))
                lblFechaCancelacion.Text = IIf(oBEDevolucion.FechaCancelacion.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaCancelacion.ToString("dd/MM/yyyy hh:mm"))

                lblFechaChequeGenerado.Text = IIf(oBEDevolucion.FechaChequeGenerado.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaChequeGenerado.ToString("dd/MM/yyyy hh:mm"))
                lblFechaEmitido.Text = IIf(oBEDevolucion.FechaEmitido.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaEmitido.ToString("dd/MM/yyyy hh:mm"))
                lblFechaExpirado.Text = IIf(oBEDevolucion.FechaExpirado.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaExpirado.ToString("dd/MM/yyyy hh:mm"))
                lblFechaEnProceso.Text = IIf(oBEDevolucion.FechaEnProceso.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaEnProceso.ToString("dd/MM/yyyy hh:mm"))
                lblFechaRegistro.Text = IIf(oBEDevolucion.FechaCreacion.Date() = CDate("1/1/0001"), String.Empty, oBEDevolucion.FechaCreacion.ToString("dd/MM/yyyy hh:mm"))
                lblNumeroSolicitud.Text = hdfIdDevolucion.Value

                txtClienteApellidos.Enabled = False
                txtClienteDireccion.Enabled = False
                txtClienteDireccion.Enabled = False
                txtClienteNombres.Enabled = False
                txtClienteNroDNI.Enabled = False
                txtNroCIP.Enabled = False
                'txtEstado.Text = oBEDevolucion.DescripcionEstadoCliente
                txtObservaciones.Enabled = False
                lblConpeptopPago.Enabled = False
                lblMonto.Enabled = False
                lblServicio.Enabled = False
                lblEstado.Enabled = False
                ddlEstadoDevolucion.Items.Clear()
                DropDownlistBinding(ddlEstadoDevolucion, listaParametros.OrderBy(Function(p1) p1.Id).ToList(), "Descripcion", "Id", "::Todos::")
                If oBEDevolucion.IdEstadoAdmin <> 695 Then
                    ddlEstadoDevolucion.Enabled = True
                    ddlEstadoDevolucion.Items.Remove(ddlEstado.Items.FindByValue("695"))
                    ddlEstadoDevolucion.SelectedValue = oBEDevolucion.IdEstadoAdmin
                Else
                    ddlEstadoDevolucion.Enabled = False
                    ddlEstadoDevolucion.SelectedValue = oBEDevolucion.IdEstadoAdmin
                End If

                ddlEstadoDevolucion.SelectedValue = oBEDevolucion.IdEstadoAdmin
                chbxEnProceso.Checked = oBEDevolucion.FlagEnProceso

            End If
        Next
        lblMensajeValPop.Text = String.Empty
        pnlPopupActualizarDevolucion.Visible = True
        mppDevolucion.Show()
    End Sub
    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbtnRegresar.Click
        mppDevolucion.Hide()
    End Sub

    Protected Sub btnPCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPCancelar.Click
        mppDevolucion.Hide()
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        mppDevolucion.Hide()
    End Sub

    Protected Sub btnDescargar_Click(sender As Object, e As System.EventArgs) Handles btnDescargar.Click
        Dim cOrdenPago As New COrdenPago
        Dim ArchivoTraido As Byte()
        Dim Archivo As String = hdfIdDevolucion.Value.Trim() + "_" + hdfNombreArchivoDevolucion.Value().Trim()
        ArchivoTraido = cOrdenPago.ObtenerArchivoDevolucion(Archivo)

        'getFile(ArchivoTraido, Archivo)

        Dim req As New WebClient
        Dim response As HttpResponse = HttpContext.Current.Response
        response.Clear()
        response.ClearContent()
        response.ClearHeaders()
        response.Buffer = True
        response.AddHeader("Content-disposition", "attachment; filename=" + Archivo)
        response.ContentType = "application/octet-stream"
        'response.ContentType = "application/download"
        response.BinaryWrite(ArchivoTraido)
        response.Flush()
        '//response.End()
        response.Close()

        'response.Clear()
        'response.ContentType = "audio/mpeg3"
        'response.AppendHeader("Content-Disposition", "attachment; filename=" & Archivo)
        'response.BinaryWrite(ArchivoTraido)
        'response.Flush()

    End Sub

    Public Sub getFile(ByVal Files As [Byte](), ByVal Archivo As String)
        'Dim LPath As [String] = "sample.jpg"

        'If File.Exists(LPath) Then
        '    File.Delete(LPath)
        'End If

        'Dim fs As New FileStream(Archivo, FileMode.Create, FileAccess.ReadWrite, FileShare.Write)
        'fs.Write(Files, 0, Files.Length)
        'fs.Flush()
        'fs.Close()

        Dim sFile As String = "c:" & vbTab & "estpdf.pdf"
        'Path
        Dim fs As FileStream = File.Create(sFile)
        Dim bw As New BinaryWriter(fs)

    End Sub

    Protected Sub btnPActualizar_Click(sender As Object, e As System.EventArgs) Handles btnPActualizar.Click
        Dim oCOrdenPago As New COrdenPago
        Dim ListaDevoluciones As List(Of BusinessEntityBase) = CType(ViewState("ListaDevoluciones"), List(Of BusinessEntityBase))
        Dim oBEDevolucion As BEDevolucion = ListaDevoluciones.Where(Function(x) CType(x, BEDevolucion).IdDevolucion = Convert.ToInt64(hdfIdDevolucion.Value)).First()

        If CInt(ddlEstadoDevolucion.SelectedValue) <> 691 Then
            If CInt(ddlEstadoDevolucion.SelectedValue) >= oBEDevolucion.IdEstadoAdmin Then
                lblMensajeValPop.Text = String.Empty
                oBEDevolucion.IdEstadoAdmin = CInt(ddlEstadoDevolucion.SelectedValue)
                oBEDevolucion.DescripcionEstadoCliente = ddlEstadoDevolucion.SelectedItem.Text.Trim()
                oBEDevolucion.IdUsuarioActualizacion = UserInfo.IdUsuario
                oBEDevolucion.FlagEnProceso = chbxEnProceso.Checked
                oCOrdenPago.ActualizarDevolucion(oBEDevolucion)
                JSMessageAlert("OK", "La devolución se actualizo correctamente.", "key")
                BtnImgSearch_Click(Nothing, Nothing)
                mppDevolucion.Hide()
            Else
                'JSMessageAlert("OK", "No se puede regresar de estado.", "key222222")
                CargarPopup(Convert.ToInt32(hdfIdDevolucion.Value))
                mppDevolucion.Show()
                lblMensajeValPop.Text = "No se puede cambiar a un estado anterior del actual."
            End If
        Else
            CargarPopup(Convert.ToInt32(hdfIdDevolucion.Value))
            mppDevolucion.Show()
            lblMensajeValPop.Text = "No se puede actualizar al estado solicitado."
        End If
    End Sub

    Public Sub JSMessageAlert(tipo_alerta As String, mensaje As String, key As String)
        Dim script As String
        script = String.Format("alert('{0}: {1}');", tipo_alerta, mensaje)
        If Me.Master IsNot Nothing Then
            If Me.Master.AppRelativeVirtualPath.Contains("MasterPagePrincipal") Then
                If Me.IsPostBack Then
                    script = String.Format("document.getElementById('errores').innerHTML = '{0}: {1}';", tipo_alerta, mensaje)
                    script += "document.getElementById('errores').style.display = 'block';"
                Else
                    script = "window.onload = function(){"
                    script += String.Format("document.getElementById('errores').innerHTML = '{0}: {1}';", tipo_alerta, mensaje)
                    script += "document.getElementById('errores').style.display = 'block';"
                    script += "}"
                End If
            End If
        End If
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub

    Protected Sub btnExportarExcel_Click(sender As Object, e As System.EventArgs) Handles btnExportarExcel.Click
        Dim cOrdenPago As New COrdenPago
        Dim ListaDevoluciones As New List(Of BusinessEntityBase)
        Dim oBEDevolucionesMLAE As New BEDevolucion()
        Dim oBEDevolucionToExcel As New BEDevolucion()
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            oBEDevolucionToExcel = CreateBusinessEntityForSearch()
            oBEDevolucionesMLAE = oBEDevolucionToExcel
            ListaDevoluciones = cOrdenPago.ConsultarDevolucionesExcel(oBEDevolucionToExcel)
        End If
        For Each item As BEDevolucion In ListaDevoluciones
            If item.DescripcionEstadoAdmin = "En Proceso" Then
                item.DescripcionEstadoAdmin = "Solicitado"
            End If
        Next
        Dim transferenciasBase As List(Of BusinessEntityBase) = ListaDevoluciones
        Dim devoluciones As New List(Of BEDevolucion)
        Dim paramList As New List(Of ReportParameter)

        paramList.Add(New ReportParameter("ServicioDescripcion", ddlServicio.SelectedItem.Text))
        paramList.Add(New ReportParameter("ClienteNombres", txtClienteNombreFilter.Text.Trim()))
        paramList.Add(New ReportParameter("Estado", ddlEstado.SelectedItem.Text))
        paramList.Add(New ReportParameter("DNI", txtClienteNroDNIFilter.Text.Trim()))
        paramList.Add(New ReportParameter("NroSolicitud", txtNroDevolucionFilter.Text.Trim()))
        paramList.Add(New ReportParameter("NroCIP", txtNroCIPFilter.Text.Trim()))
        paramList.Add(New ReportParameter("FechaInicio", txtFechaInicio.Text))
        paramList.Add(New ReportParameter("FechaFin", txtFechaFin.Text))

        For index = 0 To transferenciasBase.Count - 1
            devoluciones.Add(CType(transferenciasBase(index), BEDevolucion))
        Next

        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")

        Dim localReport As New LocalReport()

        localReport.ReportPath = Server.MapPath("~\Reportes\RptCODev.rdlc")

        localReport.SetParameters(paramList)

        If localReport.DataSources.Count > 0 Then
            localReport.DataSources.RemoveAt(0)
        End If

        localReport.DataSources.Add(New ReportDataSource("BEDevolucion", devoluciones))
        ResponseExport(localReport, "EXCEL", "xls") 'asd
    End Sub

    Protected Sub gvResultado_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvResultado.RowDataBound
        If e.Row.Cells.Count > 0 Then
            'Dim lblFechaChequeGenerado As Label = DirectCast(e.Row.FindControl("lblFechaChequeGenerado"), Label)
            Dim lblFechaChequeGenerado As Label = CType(e.Row.FindControl("lblFechaChequeGenerado"), Label)
            If Not lblFechaChequeGenerado Is Nothing Then
                If (CDate(lblFechaChequeGenerado.Text.Trim()) = CDate("1/1/0001")) Then
                    lblFechaChequeGenerado.Text = String.Empty
                End If
            End If

        End If
    End Sub

    Private Sub ResponseExport(ByVal localReport As LocalReport, ByVal Type As String, ByVal fileExtension As String)
        '
        Dim reportType As String = Type
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim fileNameExtension As String = fileExtension

        Dim deviceInfo As String = _
        "<DeviceInfo>" + _
        "  <OutputFormat>" + reportType + "</OutputFormat>" + _
        "</DeviceInfo>"

        Dim warnings As Warning() = Nothing
        Dim streams As String() = Nothing
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render( _
            reportType, _
            deviceInfo, _
            mimeType, _
            encoding, _
            fileNameExtension, _
            streams, _
            warnings)

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=Detalle_de_Devolución_" + Date.Now.ToShortDateString + "." + fileNameExtension)
        Response.BinaryWrite(renderedBytes)
        Response.End()
        '
    End Sub
End Class
