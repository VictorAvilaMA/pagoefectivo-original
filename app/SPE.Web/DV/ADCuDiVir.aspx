﻿<%@ Page Language="VB" Async="true" EnableEventValidation="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADCuDiVir.aspx.vb" Inherits="DV_ADCuDiVir"
    Title="PagoEfectivo - Solicitud de Creación de Cuenta" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        Solicitud de Creaci&oacute;n de Cuenta</h2>
    <div class="conten_pasos3" style="min-height:0px">
        <asp:UpdatePanel ID="UpdatePanelDatos" runat="server">
            <ContentTemplate>
                <h4>
                    1. Informaci&oacute;n de la Cuenta</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt; </span>Tipo de Moneda : </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoMoneda" runat="server" OnSelectedIndexChanged="ddlTipoMoneda_SelectedIndexChanged"
                            CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Alias : </li>
                    <li class="t2" style="height:auto">
                        <asp:TextBox ID="TxtAlias" runat="server" MaxLength="20" CssClass="neu"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revMailAlternativo" runat="server" ControlToValidate="TxtAlias"
                            ErrorMessage="No se permiten tildes ni caracteres extraños..." ValidationExpression="^[a-zA-ZñÑ ]*$" CssClass="w100"/>
                        <asp:Label ID="lblTransaccion" runat="server" Visible="False" Width="100%" CssClass="MensajeTransaccion"
                        Style="text-align:left;height:auto;line-height:normal"></asp:Label>
                        <div class="clear">
                        </div>
                    </li>
                    <li id="fsBotonera" runat="server" class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" Text="Solicitar Cuenta" Visible="true"
                            OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty" CssClass="input_azul3">
                        </asp:Button>
                        <asp:Button ID="btnNuevaCuenta" runat="server" Text="Nueva Cuenta" Visible="false"
                            OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty" CssClass="input_azul3">
                        </asp:Button>
                    </li>
                    <asp:HiddenField ID="hdnIdCliente" runat="server" />
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
