﻿Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web

Partial Class DV_ADCuDiVir
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CargarCombo()
            ddlTipoMoneda.SelectedIndex = 0
            ValidarCuenta()
        End If
        btnNuevaCuenta.Enabled = False
    End Sub

    Private Sub CargarCombo()

        Dim control As New SPE.Web.CDineroVirtual
        Dim listaBEMoneda As New List(Of BEMoneda)
        listaBEMoneda = control.ConsultarMonedaMonederoVirtual()

        With ddlTipoMoneda

            .DataSource = listaBEMoneda
            .DataTextField = "Descripcion"
            .DataValueField = "IdMoneda"
            .DataBind()
        End With
    End Sub

    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
        Dim oCDineroVirtual As New CDineroVirtual
        Dim Res As Integer = 0

        Try
            oBECuentaDineroVirtual.IdMoneda = CInt(ddlTipoMoneda.SelectedValue)
            oBECuentaDineroVirtual.AliasCuenta = CStr(TxtAlias.Text)
            oBECuentaDineroVirtual.IdUsuario = SPE.Web.PaginaBase.UserInfo.IdUsuario '5
            If SoloLetras(CStr(TxtAlias.Text)) = 0 Then
                Res = oCDineroVirtual.RegistrarCuentaDineroVirtual(oBECuentaDineroVirtual)
            Else
                Res = -1
            End If


            If Res = 2 Then
                lblTransaccion.Text = "Su solicitud de creación de Dinero Virtual ha sido enviada con Éxito."
                lblTransaccion.ForeColor = Drawing.Color.Navy
                Limpiar(False)
            ElseIf Res = 3 Then
                lblTransaccion.Text = "El Usuario no se encuentra habilitado."
                lblTransaccion.ForeColor = Drawing.Color.Red
                Limpiar(True)
            ElseIf Res = 0 Then
                lblTransaccion.Text = "Se ha excedido el Número de Cuentas."
                lblTransaccion.ForeColor = Drawing.Color.Red
                Limpiar(True)
            ElseIf Res = 1 Then
                lblTransaccion.Text = "Ya existe una Cuenta con el mismo Alias."
                lblTransaccion.ForeColor = Drawing.Color.Red
                Limpiar(True)
            ElseIf Res = -1 Then
                lblTransaccion.Text = "El Alias solo puede contener Letras"
                lblTransaccion.ForeColor = Drawing.Color.Red
                Limpiar(True)
            Else
                lblTransaccion.Text = "Se ha producido un error al enviar la Solicitud."
                lblTransaccion.ForeColor = Drawing.Color.Red
                Limpiar(True)
            End If

            lblTransaccion.Visible = True

        Catch ex As Exception
            'Limpiar(True)
        End Try
    End Sub

    Protected Sub Limpiar(ByVal cond As Boolean)
        If cond Then
            btnRegistrar.Enabled = True
            btnRegistrar.Visible = True
            btnNuevaCuenta.Visible = False
            btnNuevaCuenta.Enabled = False
            ddlTipoMoneda.Enabled = True
            TxtAlias.Enabled = True
        Else
            btnRegistrar.Enabled = False
            btnRegistrar.Visible = False
            btnNuevaCuenta.Visible = True
            btnNuevaCuenta.Enabled = True
            ddlTipoMoneda.Enabled = False
            TxtAlias.Enabled = False
        End If
    End Sub

    Protected Sub ddlTipoMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoMoneda.SelectedIndexChanged
    End Sub

    Protected Sub btnNuevaCuenta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevaCuenta.Click
        lblTransaccion.Visible = False
        TxtAlias.Text = ""
        ddlTipoMoneda.SelectedIndex = 0
        Limpiar(True)
    End Sub
    Private Sub ValidarCuenta()
        Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
        Dim oCDineroVirtual As New CDineroVirtual
        Dim resultado As BECuentaDineroVirtual
        Dim Res As Integer = 0

        Try
            oBECuentaDineroVirtual.IdUsuario = SPE.Web.PaginaBase.UserInfo.IdUsuario '5
            resultado = oCDineroVirtual.ValidarRegistrarCuentaDineroVirtual(oBECuentaDineroVirtual)
            Res = resultado.IdEstado
            
            If Res = 3 Then
                lblTransaccion.Text = "El Usuario no se encuentra habilitado."
                lblTransaccion.ForeColor = Drawing.Color.Red
                lblTransaccion.Visible = True
                btnRegistrar.Enabled = False
            ElseIf Res = 0 Then
                lblTransaccion.Text = "Se ha excedido el Número de Cuentas."
                lblTransaccion.ForeColor = Drawing.Color.Red
                lblTransaccion.Visible = True
                btnRegistrar.Enabled = False
            Else
                lblTransaccion.Visible = False
                btnRegistrar.Enabled = True
            End If

        Catch ex As Exception
            'Limpiar(True)
        End Try
    End Sub
    Private Function SoloLetras(ByVal strTexto As String) As Integer
        strTexto = strTexto.ToUpper()
        Dim intFlag As Integer = 0
        For Each ch As Char In strTexto
            If InStr("ABCDEFGHIJKLMNÑOPQRSTUVWXYZ ", ch) = 0 Then
                intFlag += 1
            End If
        Next
        Return intFlag
    End Function
End Class
