﻿<%@ Page Title="PagoEfectivo - Retiro de dinero" Async="true" Language="VB" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADReSolRet.aspx.vb" Inherits="DV_ADReSolRet"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('#<%=txtMontoRetirar.ClientID%>').blur(function () {
                if ($('#<%=txtMontoRetirar.ClientID%>').val() != '')
                    $('#<%=txtMontoRetirar.ClientID%>').val(parseFloat($('#<%=txtMontoRetirar.ClientID%>').val()).toFixed(2));
            });
        });

        function validToken() {
            var valToken = $('#<%= txtToken.ClientID%>').val();
            if (valToken.length == 0)
                alertDiv('Validación: Debe Ingresar el Token enviado a su correo para continuar el proceso.');
            else
                if (valToken.length != 10)
                    alertDiv('Validación: El Token debe contener 10  dígitos.');
            return valToken.length == 10;
        }

        function goBack() {
            $('#<%= ddlCuenta.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtMontoRetirar.ClientID%>').attr('disabled', 'disabled');
            $('#<%= ddlBanco.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtNroCuentaBanco.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtTitularCuenta.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnContinuar1.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtToken.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnReenviarToken.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnEnviarSolicitud.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar2.ClientID%>').attr('disabled', 'disabled');
            window.location.href = '../DV/COCUCLI.aspx';
        }

        function goStep2() {
            if ($('#<%=ddlCuenta.ClientID %>').val() == null) {
                alertDiv('Validación: Ud. no tiene cuenta de Dinero Virtual.');
                return false;
            }
            var valMonto = $('#<%=txtMontoRetirar.ClientID%>').val();
            var valSaldo = parseFloat($('#<%=ddlCuenta.ClientID%>').val().split('|')[2]);
            var valSalReten = parseFloat($('#<%=ddlCuenta.ClientID%>').val().split('|')[3]);
            if (valMonto == '') {
                alertDiv('Validación: Debe especificar el Monto a Retirar.');
                return false;
            }
            if (parseFloat(valMonto) <= 0) {
                alertDiv('Validación: El Monto a Retirar debe ser mayor a cero.');
                return false;
            }
            if ((valSaldo - valSalReten) < parseFloat(valMonto)) {
                alertDiv('Validación: Ud. No tiene Saldo suficiente en su Cuenta para retirar el Monto solicitado.' + (valSalReten > 0 ? ' La cuenta tiene saldo reservado para otros Retiros de dinero.' : ''));
                return false;
            }
            if ($('#<%=txtNroCuentaBanco.ClientID%>').val() == '') {
                alertDiv('Validación: Debe especificar el Nº de Cuenta del Banco.');
                return false;
            }
            if ($('#<%=txtTitularCuenta.ClientID%>').val() == '') {
                alertDiv('Validación: Debe especificar el Nombre del Titular de la Cuenta del Banco.');
                return false;
            }

            $.ajax({
                type: "POST",
                url: "../DV/ADReSolRet.aspx/GenerarToken",
                data: "{idCuenta: '" + $('#<%=ddlCuenta.ClientID%>').val().split('|')[0] + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('#<%=hdfInfoStep1.ClientID%>').val($('#<%=ddlCuenta.ClientID%>').val());
                    $('#<%=hdfInfoStep1.ClientID%>').val($('#<%=hdfInfoStep1.ClientID%>').val() + '|' + response.d.IdTokenDineroVirtual);

                    $('#spnCuentaAretirar').text($('#<%=ddlCuenta.ClientID%> option:selected').text());
                    $('#spnMontoARetirar').text($('#<%=txtMontoRetirar.ClientID%>').val());
                    $('#spnBancoARetirar').text($('#<%=ddlBanco.ClientID%> option:selected').text());
                    $('#spnNroCuenta').text($('#<%=txtNroCuentaBanco.ClientID%>').val());
                    $('#spnlTitularCuenta').text($('#<%=txtTitularCuenta.ClientID%>').val());
                    $('#spnInfotoken').text(response.d.Token);

                    $('#dvGrPaso1').css('display', 'none');
                    $('#dvGrPaso2').css('display', 'block');
                },
                error: function (error) { /*alertDiv("error: " + error)*/window.location.href = '../DV/COCUCLI.aspx'; }
            });
            return true;
        }

        function reenviarToken() {
            $.ajax({
                type: "POST",
                url: "../DV/PRTransDiner.aspx/ReenviarToken",
                data: "{idToken: '" + $('#<%=hdfInfoStep1.ClientID%>').val().split("|")[4] + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d.Token == '') {
                        alertDiv('Info: Se ha reenvio el Código Token a su Correo.');
                    }
                    else {
                        if (response.d.IdTokenDineroVirtual != 0) {
                            $('#<%=hdfInfoStep1.ClientID%>').val($('#<%=ddlCuenta.ClientID%>').val());
                            $('#<%=hdfInfoStep1.ClientID%>').val($('#<%=hdfInfoStep1.ClientID%>').val() + '|' + response.d.IdTokenDineroVirtual);
                        }
                        alertDiv('Info: ' + response.d.Token);
                    }
                },
                error: function (error) { /*alertDiv("error: " + error)*/window.location.href = '../DV/COCUCLI.aspx'; }
            });
        }

        //        window.onload = function () {
        //            document.getElementById('errores').innerHTML = 'asd: asd';
        //            document.getElementById('errores').style.display = 'block';
        //        }
    </script>
    <asp:HiddenField ID="hdfInfoStep1" runat="server" />
    <h2>
        Solicitar retiro de dinero</h2>
    <div id="dvGrPaso1">
        <div class="conten_pasos" id="other_back">
            <ul class="pasos">
                <li id="paso1" class="activa"><a href="#">1</a> Datos de transferencia</li>
                <li id="paso2"><a href="#">2</a> Confirmaci&oacute;n</li>
            </ul>
            <h5>
                ¿Desde qu&eacute; cuenta desea retirar?</h5>
            <asp:DropDownList ID="ddlCuenta" runat="server" CssClass="pasos_">
            </asp:DropDownList>
            <h5>
                ¿Qu&eacute; monto desea retirar?</h5>
            <asp:TextBox ID="txtMontoRetirar" MaxLength="14" CssClass="pasos_" runat="server"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="fteTxtMontoRetirar" runat="server" ValidChars="0123456789."
                TargetControlID="txtMontoRetirar">
            </cc1:FilteredTextBoxExtender>
            <h5>
                ¿A qu&eacute; cuenta desea depositar?</h5>
            <ul class="datos_cip">
                <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                <li class="t2">
                    <asp:DropDownList ID="ddlBanco" runat="server" class="pasos_">
                    </asp:DropDownList>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> N&uacute;mero de cuenta :</li>
                <li class="t2 h25">
                    <asp:TextBox ID="txtNroCuentaBanco" MaxLength="14" CssClass="cipCamp" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="ftbeTxtNroCuentaBanco" runat="server" ValidChars="0123456789"
                        TargetControlID="txtNroCuentaBanco">
                    </cc1:FilteredTextBoxExtender>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> Titular de la cuenta :</li>
                <li class="t2 h25">
                    <asp:TextBox ID="txtTitularCuenta" MaxLength="50" CssClass="cipCamp" runat="server"></asp:TextBox>
                </li>
            </ul>
            <asp:Button ID="btnContinuar1" OnClientClick="goStep2();return false;" CssClass="input_azul"
                Text="Continuar" runat="server" />
            <asp:LinkButton ID="btnCancelar" OnClientClick="goBack();return false;" CssClass="cancela"
                Text="Cancelar" runat="server" />
        </div>
    </div>
    <div id="dvGrPaso2" style="display: none">
        <div class="conten_pasos" id="other_back2">
            <ul class="pasos">
                <li id="paso1"><a>1</a> Datos de transferencia</li>
                <li id="paso2" class="activa"><a>2</a> Confirmaci&oacute;n</li>
            </ul>
            <h5>
                Informaci&oacute;n de solicitud</h5>
            <ul class="datos_cip">
                <li class="t1"><span class="color">&gt;&gt;</span> Cuenta a retirar :</li>
                <li class="t2"><strong><span id="spnCuentaAretirar"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Monto a retirar :</li>
                <li class="t2"><strong><span id="spnMontoARetirar"></span></strong></li>
            </ul>
            <h5 class="h0">
                Datos de Cuenta a Depositar</h5>
            <ul class="datos_cip">
                <li class="t1"><span class="color">&gt;&gt;</span> Banco a depositar :</li>
                <li class="t2"><strong><span id="spnBancoARetirar"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> N&uacute;mero de cuenta banco :</li>
                <li class="t2"><strong><span id="spnNroCuenta"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Titular de cuenta :</li>
                <li class="t2"><strong><span id="spnlTitularCuenta"></span></strong></li>
            </ul>
            <div id="other_back_fondo_blue">
            <h5 class="h0">
                Validaci&oacute;n</h5>
             <p class="ubica"><!--<p class="separacion">-->
                <span class="colorazul">>></span>Ha sido enviado a su correo un token de verificaci&oacute;n, el cual deber&aacute;
                ingresar en el campo de texto inferior.</p>
             <p class="centrar_letra"><!--<p class="inan">-->
                <asp:TextBox ID="txtToken" CssClass="input_big2" MaxLength="10" runat="server"></asp:TextBox><!-- class="pasos_2" -->
                <cc1:FilteredTextBoxExtender ID="ftbeTxtToken" runat="server" ValidChars="0123456789"
                    TargetControlID="txtToken">
                </cc1:FilteredTextBoxExtender>
                <asp:Button ID="btnReenviarToken" OnClientClick="reenviarToken();return false;" CssClass="input_azul45"
                    Text="Reenviar" runat="server" />
            </p>
            </div>
            <span id="spnInfotoken"></span>
            <div style="clear: both; margin-bottom: 10px;">
            </div>
            <asp:UpdatePanel ID="upnlBotones" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnEnviarSolicitud" OnClientClick="return validToken();" CssClass="input_azul"
                        runat="server" Text="Continuar" />
                    <asp:LinkButton ID="btnCancelar2" OnClientClick="goBack();return false;" CssClass="cancela"
                        Text="Cancelar" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
