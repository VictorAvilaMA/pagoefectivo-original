﻿Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class DV_ADReSolRet
    Inherits SPE.Web.PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            CargarCombos()
        End If
    End Sub

    Private Sub CargarCombos()
        Dim cntrDV As New SPE.Web.CDineroVirtual
        Dim beParam As New SPE.Entidades.BECuentaDineroVirtual
        Dim listaCuenta As New System.Collections.Generic.List(Of SPE.Entidades.BECuentaDineroVirtual)
        beParam.IdEstado = CType(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado, Integer)
        beParam.IdUsuario = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
        listaCuenta = cntrDV.ConsultarCuentaDineroVirtualUsuario(beParam)
        For Each item As SPE.Entidades.BECuentaDineroVirtual In listaCuenta
            ddlCuenta.Items.Insert(0, New ListItem(String.Concat("Nº ", item.Numero, " (", item.MonedaSimbolo, " ", item.Saldo.ToString("#0.00"), ") [", item.AliasCuenta, "]"), String.Concat(item.IdCuentaDineroVirtual, "|", item.IdMoneda, "|", item.Saldo, "|", item.SaldoRetenido)))
        Next
        Dim dd As New SPE.Web.CBanco

        Dim oBEBanco As New SPE.Entidades.BEBanco
        Dim listaBanco As New List(Of BEBanco)
        Dim listaFormat As New List(Of BEBanco)
        Dim oCntrBanco As New SPE.Web.CBanco
        oBEBanco.IdEstado = EstadoBanco.Activo
        ddlBanco.DataTextField = "Descripcion"
        ddlBanco.DataValueField = "IdBanco"
        listaBanco = oCntrBanco.ConsultarBanco(oBEBanco)

        For Each Entidad As SPE.Entidades.BEBanco In listaBanco
            If (Entidad.IdBanco <> 4) Then
                listaFormat.Add(Entidad)
            End If
        Next
        ddlBanco.DataSource = listaFormat
        ddlBanco.DataBind()
        'ddlBanco.
    End Sub

#Region "Page Metod"

    <System.Web.Services.WebMethod()>
    Public Shared Function GenerarToken(ByVal idCuenta As Long) As BETokenDineroVirtual
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEToken As New BETokenDineroVirtual
        oBEToken.IdCuenta = idCuenta
        oBEToken.IdTipoOperacion = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.RetiroDinero, Integer)
        oBEToken = oCDineroVirtual.RegistrarTokenMonedero(oBEToken)

        Return oBEToken
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function ReenviarToken(ByVal idToken As Long) As BETokenDineroVirtual
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Return oCDineroVirtual.ReenviarEmailToken(idToken)
    End Function
#End Region

    Protected Sub btnEnviarSolicitud_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviarSolicitud.Click
        Dim allowchars As String = "0123456789"
        For Each s As String In txtNroCuentaBanco.Text
            If Not allowchars.Contains(s) Then
                JSMessageAlert("Error: ", "Usted ingreso un número de cuenta no valido.", "aa")
                Return
            End If
        Next
        allowchars = "qwertyuiopasdfghjklñzxcvbnm QWERTYUIOPLKJHGFDSAZXCVBNM"
        For Each s As String In txtTitularCuenta.Text
            If Not allowchars.Contains(s) Then
                JSMessageAlert("Error: ", "Usted ingreso nombre de titular no valido.", "aa")
                Return
            End If
        Next

        Dim solRetDin As New BESolicitudRetiroDineroVirtual
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        solRetDin.IdCuenta = _3Dev.FW.Util.DataUtil.ObjectToInt32(hdfInfoStep1.Value.Split("|"c)(0))
        solRetDin.MontoSolicitado = _3Dev.FW.Util.DataUtil.ObjectToDecimal(txtMontoRetirar.Text)
        solRetDin.IdBanco = CType(ddlBanco.SelectedValue, Integer)
        solRetDin.CuentaBancariaDestino = txtNroCuentaBanco.Text.Trim()
        solRetDin.TitularCuentaBancariaDestino = txtTitularCuenta.Text.Trim()
        solRetDin.IdToken = _3Dev.FW.Util.DataUtil.ObjectToInt32(hdfInfoStep1.Value.Split("|"c)(4))
        solRetDin.NroToken = txtToken.Text.Trim()
        solRetDin.IdUsuario = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario
        solRetDin.Email = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email)

        Dim rsult As BESolicitudRetiroDineroVirtual = oCDineroVirtual.RegistrarSolicitudRetiroDinero(solRetDin)
        If (rsult Is Nothing) Then
            JSMessageAlert("Error: ", "Error al enviar la solicitud de retiro de dinero.", "aa")
        Else
            If (String.IsNullOrEmpty(rsult.ObservacionAdministrador)) Then
                txtToken.Enabled = btnReenviarToken.Enabled = btnEnviarSolicitud.Enabled = btnCancelar2.Enabled = False
                JSMessageAlertWithRedirect("Info: ", "Se ha enviado su Solicitud de Retiro de Dinero. Solicitud Generada: " + rsult.IdSolicitud.ToString("#000000"), "bb", "../DV/COCUCli.aspx")
            Else
                JSMessageAlert("Validación:", rsult.ObservacionAdministrador, "cc")
            End If
        End If
    End Sub

    Private Function HttpRequestValidationException() As Exception
        Throw New NotImplementedException
    End Function

End Class
