<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCUCli.aspx.vb" Inherits="CLI_COCUCli" Title="PagoEfectivo - Consultar Cuentas de Dinero Virtual" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        Mis Cuentas</h2>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional" class="conten_pasos3">
        <ContentTemplate>
            <h4>
                Criterios de b&uacute;squeda</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">>></span> Tipo de Moneda :</li>
                <li class="t2">
                    <asp:DropDownList ID="ddlTipoMoneda" runat="server" CssClass="neu" AutoPostBack="True">
                    </asp:DropDownList>
                </li>
                <li class="t1"><span class="color">>></span> Estado:</li>
                <li class="t2">
                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu" AutoPostBack="True">
                    </asp:DropDownList>
                </li>
                <li class="complet">
                    <asp:Button ID="btnCrearCuenta" runat="server" CssClass="input_azul3" Text="Crear Cuenta"
                        ValidationGroup="GrupoValidacion" />
                    <asp:Button ID="BtnImprimir" runat="server" CssClass="input_azul4" Text="Excel"
                        ValidationGroup="GrupoValidacion" />
                </li>
            </ul>
            <asp:ValidationSummary ID="ValidationSummary" runat="server" ValidationGroup="GrupoValidacion">
            </asp:ValidationSummary>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" class="cont_cel">
                <ContentTemplate>
                    <h5><asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label></h5>
                        <div>
                            <asp:Label ID="lblResultado" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div>
                            <asp:GridView ID="gvResultado" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdCuentaDineroVirtual"
                                OnPageIndexChanging="gvResultado_PageIndexChanging" 
                                OnRowDataBound="gvResultado_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="Numero" HeaderText="Cuenta" SortExpression="Numero" ItemStyle-Width="100px"/>
                                    <asp:BoundField DataField="AliasCuenta" HeaderText="Alias" SortExpression="AliasCuenta" />
                                    <asp:BoundField DataField="MonedaDescripcion" HeaderText="Moneda" SortExpression="MonedaDescripcion" />
                                    <asp:BoundField DataField="Saldo" HeaderText="Saldo" SortExpression="Saldo">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EstadoDescripcion" HeaderText="Estado" SortExpression="EstadoDescripcion">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Ver Movimientos" ItemStyle-HorizontalAlign="Center" >
                                        
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ibtnDetalle" runat="server" CommandName="VerDetalle" ImageUrl="~/images/lupa.gif"
                                                ToolTip="Ver Movimientos de dinero" Text="Ver" CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"  Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Detalles" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ImageButton1" runat="server" CommandName="Edit" ImageUrl="~/images/lupa.gif"
                                                ToolTip="Ver información" Text="Detalle" CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                            <asp:HiddenField ID="hdfIdCuenta" runat="server" Value='<%# Eval("IdCuentaDineroVirtual") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="cabecera" />
                            </asp:GridView>
                        </div>
                    </h2>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlEstado" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlTipoMoneda" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="BtnImprimir" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:button id="btnNuevo" runat="server" cssclassk="btnVolver" text="Nuevo" Visible="false"/>            
</asp:Content>
