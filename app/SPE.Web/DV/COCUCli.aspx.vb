Imports System.Data
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.Web
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.EmsambladoComun

Partial Class CLI_COCUCli

    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CDineroVirtual)

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return MyBase.BtnSearch
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnClear As System.Web.UI.WebControls.Button
        Get
            Return MyBase.BtnClear
        End Get
    End Property
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance As System.Web.UI.WebControls.Label
        Get
            Return lblMsgNroRegistros
        End Get
    End Property
    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CFTPArchivo
    'End Function
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obebusqueda As New SPE.Entidades.BECuentaDineroVirtual

        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
            obebusqueda.IdUsuario = ClienteUsuarioID
        End If


        If ddlTipoMoneda.SelectedValue.ToString = "" Then
            obebusqueda.IdMoneda = 0
        Else
            obebusqueda.IdMoneda = ddlTipoMoneda.SelectedValue
        End If

        If ddlEstado.SelectedValue.ToString = "" Then
            obebusqueda.IdEstado = 0
        Else
            obebusqueda.IdEstado = ddlEstado.SelectedValue
        End If

        Return obebusqueda

    End Function
    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.MaintenanceKey = "Cocude"
        e.MaintenancePageName = "Cocude.aspx"
        'e.ExecuteSearchOnFirstLoad = True
    End Sub


    Public Overrides Sub OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If
        MyBase.OnAfterSearch()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnNuevo.UniqueID()
            Page.SetFocus(ddlEstado)

            'SERVICIO
            Dim objCAdministrarServicio As New SPE.Web.CServicio
            Dim obeServicio As New SPE.Entidades.BEServicio
            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario

            Dim objCParametro As New SPE.Web.CAdministrarParametro()
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("CUVI"), _
            "Descripcion", "Id", "::: Todos :::")

            ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado
            Dim control As New SPE.Web.CDineroVirtual


            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoMoneda, control.ConsultarMonedaMonederoVirtual(), _
            "Descripcion", "IdMoneda", "::: Todos :::")

            buscar()
        End If
        '
    End Sub
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub

    Public Overrides Sub OnBeforeSearch()

    End Sub

    Public Overrides Sub OnMainSearch()
        Try


            Dim businessEntityObject As New BusinessEntityBase()
            businessEntityObject = CreateBusinessEntityForSearch()

            'businessEntityObject.PageNumber = PageNumber;
            'businessEntityObject.PageSize = GridViewResult.PageSize;
            'businessEntityObject.TipoOrder = SortDir == SortDirection.Ascending ? true : false;
            'businessEntityObject.PropOrder = SortExpression;
            'GridViewResult.PageIndex = PageNumber - 1;

            '        ResultList = GetMethodSearchByParameters(businessEntityObject);

            '        ObjectDataSource oObjectDataSource = new ObjectDataSource();
            '        oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID;
            '        oObjectDataSource.EnablePaging = GridViewResult.AllowPaging;
            '        oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter";
            '        oObjectDataSource.SelectMethod = "GetDataGrilla";
            '        oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros";
            '        oObjectDataSource.StartRowIndexParameterName = "filaInicio";
            '        oObjectDataSource.MaximumRowsParameterName = "maxFilas";
            '        oObjectDataSource.EnableViewState = false;
            '        oObjectDataSource.ObjectCreating += new ObjectDataSourceObjectEventHandler(oObjectDataSource_ObjectCreating);
            '        GridViewResult.DataSource = oObjectDataSource;

            '    }
            'Else
            '    {
            Dim resultados As New List(Of BECuentaDineroVirtual)
            resultados = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuario(businessEntityObject)

            If resultados.Count() > 0 Then
                ViewState("DescMoneda") = resultados(0).MonedaDescripcion
                ViewState("SimboloMoneda") = resultados(0).MonedaSimbolo
                ViewState("DescEstado") = resultados(0).EstadoDescripcion
            Else
                ViewState("DescMoneda") = ""
                ViewState("SimboloMoneda") = ""
                ViewState("DescEstado") = ""
            End If


            'ResultList = resultados
            GridViewResult.AllowPaging = False
            GridViewResult.DataSource = resultados
            '}


            GridViewResult.DataBind()
            If (GridViewResult.Rows.Count = 0) Then
                ThrowWarningMessage(MessageNoRecordsWereFound)
            Else
                ThrowWarningMessage("")
            End If

        Catch ex As Exception
            ThrowErrorMessage(ex.Message)
        End Try
    End Sub



    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)        '
    End Sub


    Protected Sub ddlTipoMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoMoneda.SelectedIndexChanged
        buscar()
    End Sub

    Protected Sub ddlEstado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEstado.SelectedIndexChanged
        buscar()
    End Sub

    Private Sub buscar()
        MyBase.BtnSearch_Click(Nothing, New EventArgs())
    End Sub

    Protected Sub btnCrearCuenta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearCuenta.Click
        Response.Redirect("ADCuDiVir.aspx")
    End Sub

    Protected Sub gvResultado_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvResultado.RowEditing
        MyBase.GridViewResult_RowEditing(sender, e)
    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Dim reg As Integer
        Dim Cuenta As String
        Dim index As Integer
        If Integer.TryParse(e.CommandArgument, index) Then
            reg = gvResultado.Rows(e.CommandArgument).RowIndex
            Cuenta = CType(gvResultado.Rows(reg).FindControl("hdfIdCuenta"), HiddenField).Value
            VariableTransicion = Cuenta
            Select Case e.CommandName
                Case "Edit"
                    Response.Redirect("COCUDE.aspx")
                Case "VerDetalle"
                    'Response.Redirect(String.Format("COMoCuDiVi.aspx?pabc={0}", Var))
                    Response.Redirect("COMoCuDiVi.aspx")
            End Select
        End If
        'Dim Var As String = "0000000000xa0b587866116561565asdasda6asdf6a4fa6fa6sf..."
    End Sub

    Protected Sub BtnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnImprimir.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel.")
        Else
            Dim MonedaRq As String
            Dim EstadoRq As String
            Dim DescMonedaRq As String
            Dim SimboloMonedaRq As String
            Dim DescEstadoRq As String

            If ddlTipoMoneda.SelectedIndex = 0 Then
                MonedaRq = "0"
                DescMonedaRq = "Todos"
                SimboloMonedaRq = ""
            Else
                MonedaRq = ddlTipoMoneda.SelectedValue.ToString()
                If Not ViewState("DescMoneda").ToString() = String.Empty Then
                    DescMonedaRq = ViewState("DescMoneda").ToString()
                    SimboloMonedaRq = "(" & ViewState("SimboloMoneda").ToString() & ") "
                Else
                    DescMonedaRq = ddlTipoMoneda.SelectedItem.ToString
                    SimboloMonedaRq = String.Empty
                End If


            End If

            If ddlEstado.SelectedIndex = 0 Then
                EstadoRq = "0"
                DescEstadoRq = "Todos"
            Else
                EstadoRq = ddlEstado.SelectedValue.ToString()
                If Not ViewState("DescEstado").ToString() = String.Empty Then
                    DescEstadoRq = ViewState("DescEstado").ToString()
                Else
                    DescEstadoRq = ddlEstado.SelectedItem.ToString
                End If
            End If
            'Response.Redirect(String.Format("~/Reportes/RptCuentasCliente.aspx?PIdm={0}&PIde={1}&PDMo={2}&PSMo={3}&PDes={4}", _
            'MonedaRq, EstadoRq, DescMonedaRq, SimboloMonedaRq, DescEstadoRq))

            Dim entidad As New SPE.Entidades.BECuentaDineroVirtual
            Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
            Dim AliasUsuario = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).NombreCompleto
            Dim resultados As New List(Of BECuentaDineroVirtual)
            Using oCDineroVirtual As New CDineroVirtual()
                entidad.IdUsuario = ClienteUsuarioID
                entidad.IdMoneda = MonedaRq
                entidad.IdEstado = EstadoRq

                resultados = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuario(entidad)
              
                For Each thisObject As BECuentaDineroVirtual In resultados
                    thisObject.Numero = "N� " & thisObject.Numero
                Next
            End Using

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Titular", AliasUsuario))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", SimboloMonedaRq & DescMonedaRq))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", DescEstadoRq))

            UtilReport.ProcesoExportarGenerico(resultados, Page, "EXCEL", "xls", "Cuentas de Cliente - ", parametros, "RptCuentasCliente.rdlc")



        End If
    End Sub
End Class
