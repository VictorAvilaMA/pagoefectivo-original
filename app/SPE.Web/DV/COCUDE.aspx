﻿<%@ Page Language="VB" Async="true" EnableEventValidation="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCUDE.aspx.vb" Inherits="DV_COCUDE" Title="PagoEfectivo - Solicitud de Creación de Cuenta" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        Detalles de la cuenta</h2>
    <div class="conten_pasos3">
        <h4>
            1. Datos del cliente</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Solicitante :</li>
            <li class="t2">
                <asp:TextBox ID="txtSolicitante" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox></li>
            <li class="t1"><span class="color">&gt;&gt;</span> E-mail:</li>
            <li class="t2">
                <asp:TextBox ID="txtEMail" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox></li>
        </ul>
        <ul class="datos_cip5">
            <li class="t1"><span class="color">&gt;&gt;</span> Cuentas actuales :</li>
            <li class="t3">
                <asp:ListBox ID="lsCuentas" runat="server" CssClass="normal" Style="margin: 0px; text-align:left;">
                </asp:ListBox>
            </li>
        </ul>
        <asp:UpdatePanel ID="UpdatePanelDatos" runat="server" class="cont_cel2">
            <ContentTemplate>
                <h4>
                    2. Datos de la cuenta</h4>
                <ul class="datos_cip4">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nº solicitud :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtNumeroSolicitud" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Moneda solicitada:</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtMoneda" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nº de cuenta :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtNumeroCuenta" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de aprobaci&oacute;n :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtFechaAprobacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                </ul>
                <ul class="datos_cip4">
                    <li class="t1"><span class="color">&gt;&gt;</span> Alias :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtAlias" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtEstado" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha solicitada :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtFechaSolicitud" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de rechazo :</li>
                    <li class="t2">
                        <asp:TextBox ID="TxtFechaRechazo" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                </ul>
                <asp:Panel runat="server" ID="PnlInfoAdicional">
                    <div runat="server" id="Div6" class="even w100 clearfix dlinline">
                        <div class="divgrid">
                            <div class="w50">
                                <div class="cell">
                                    <dl class="dt35 dd50">
                                        <dt class="desc">Usuario Aprobaci&oacute;n: </dt>
                                        <dd class="camp">
                                            <asp:Label ID="TxtUsuarioAprobacion" runat="server" CssClass="miniCamp izq"></asp:Label>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="cell">
                                    <dl class="dt35 dd50">
                                        <dt class="desc">
                                            <label class="izq">
                                                Usuario Rechazo:
                                            </label>
                                        </dt>
                                        <dd class="camp">
                                            <asp:Label ID="TxtUsuarioRechazo" runat="server" CssClass="miniCamp izq"></asp:Label>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ul class="datos_cip5">
                    <li class="t1"><span class="color">&gt;&gt;</span> Observaciones : </li>
                    <li class="t3">
                        <asp:TextBox ID="TxtObservacion" runat="server" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtObservacion"
                            ValidChars="0123456789qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNM,.- ">
                        </cc1:FilteredTextBoxExtender>
                        <asp:Label ID="lblmensaje" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </li>
                    <li class="t3">
                        <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" CssClass="input_azul4" Visible="true"
                            OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty"></asp:Button>
                        <asp:Button ID="btnRechazar" runat="server" Text="Rechazar" Visible="true" CssClass="input_azul4"
                            OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty"></asp:Button>
                        <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="input_azul3" Visible="true">
                        </asp:Button>
                        <asp:Button ID="BtnCancelar" runat="server" Text="Regresar" CssClass="input_azul4"
                            Visible="true" OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty">
                        </asp:Button>
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div class="dlgrid">
        <h1>
            <asp:Literal ID="Literal1" runat="server" Text="Aprobar/Rechazar Solicitud de cuenta"
                Visible="false"></asp:Literal>
        </h1>
        <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/closeX.GIF" %>'
            Style="display: none;" CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
        <div id="fsBotonera" runat="server" class="clearfix dlinline ">
            <div class="w100 clearfix dlinline">
                <dl class="clearfix dt15 ">
                    <dt class="desc"></dt>
                    <dd class="camp">
                        <asp:Label ID="lblTransaccion" runat="server" Visible="False" Width="650px" CssClass="MensajeTransaccion"
                            Style="padding-bottom: 5px"></asp:Label>
                    </dd>
                </dl>
            </div>
        </div>
        <asp:HiddenField ID="hdnIdCuenta" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
</asp:Content>
