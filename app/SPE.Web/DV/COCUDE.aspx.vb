﻿Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web

Partial Class DV_COCUDE
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CDineroVirtual)



    Dim Perfil As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then

            TxtObservacion.Attributes.Add("onkeypress", " ValidarCaracteres(this, 200, $('#" + lblmensaje.ClientID + "'));")
            TxtObservacion.Attributes.Add("onkeyup", " ValidarCaracteres(this, 200, $('#" + lblmensaje.ClientID + "'));")


            Perfil = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)
        End If
        If Not Page.IsPostBack Then
            Dim CuentaId As Int64

            ''*Solo cuando no se usa query string          
            ' ''If Session("UsaElFramework_CuentaId") Is Nothing Then
            ' ''    CuentaId = Convert.ToInt64(MaintenanceKeyValue)
            ' ''Else
            ' ''    CuentaId = Session("UsaElFramework_CuentaId")
            ' ''End If


            ''''''''''''''
            'CuentaId = Convert.ToInt64(Request.QueryString("pabc"))
            CuentaId = Convert.ToInt64(VariableTransicion)
            If CuentaId = 0 Then
                CuentaId = Session("UsaElFramework_CuentaId")
            End If
            ''''''''''''''

            CargarCuenta(CuentaId)
            'Session("UsaElFramework_CuentaId") = Nothing
        End If
        ''btnNuevaCuenta.Enabled = False
    End Sub

    Private Sub CargarCuenta(ByVal Solicitud As Integer)

        hdnIdCuenta.Value = Solicitud
        Dim proxy As New SPE.Web.CDineroVirtual
        Dim entidad = New BECuentaDineroVirtual()
        entidad.IdCuentaDineroVirtual = Solicitud
        entidad = proxy.ConsultarCuentaDineroVirtualUsuarioByCuentaId(entidad)
        Me.TxtObservacion.Text = entidad.Observacion
        Me.TxtAlias.Text = entidad.AliasCuenta
        Me.txtEMail.Text = entidad.Email
        Me.TxtEstado.Text = entidad.EstadoDescripcion


        Me.TxtFechaAprobacion.Text = IIf(entidad.FechaAprobacion = DateTime.MinValue, "", entidad.FechaAprobacion)
        Me.TxtFechaRechazo.Text = IIf(entidad.FechaRechazo = DateTime.MinValue, "", entidad.FechaRechazo)

        Me.TxtUsuarioAprobacion.Text = IIf(entidad.UsuarioAprobacion.Equals(String.Empty), "", entidad.UsuarioAprobacion)
        Me.TxtUsuarioRechazo.Text = IIf(entidad.UsuarioRechazo.Equals(String.Empty), "", entidad.UsuarioRechazo)

        Me.TxtFechaSolicitud.Text = entidad.FechaSolicitud

        Me.TxtMoneda.Text = entidad.MonedaDescripcion
        Me.TxtNumeroCuenta.Text = entidad.Numero
        Me.TxtNumeroSolicitud.Text = entidad.IdCuentaDineroVirtual
        Me.txtSolicitante.Text = entidad.ClienteNombre





        If Perfil = "Cliente" Then
            btnAprobar.Visible = False
            btnRechazar.Visible = False
            btnExcel.CssClass = "input_azul3"
            BtnCancelar.CssClass = "input_azul4"
            TxtObservacion.ReadOnly = True
            Literal1.Text = "Detalles de la cuenta"
            PnlInfoAdicional.Visible = False
        Else
            If entidad.IdEstado.Equals(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Solicitado) Then
                btnAprobar.Visible = True
                btnRechazar.Visible = True
                btnAprobar.CssClass = "input_azul4"
                btnRechazar.CssClass = "input_azul4"
                btnExcel.CssClass = "input_azul4"
                BtnCancelar.CssClass = "input_azul4"
                TxtObservacion.ReadOnly = False
                Literal1.Text = "Aprobar/Rechazar solicitud de creacion de cuenta"
                PnlInfoAdicional.Visible = False
            Else
                btnAprobar.Visible = False
                btnRechazar.Visible = False
                btnExcel.CssClass = "input_azul3"
                BtnCancelar.CssClass = "input_azul4"
                TxtObservacion.ReadOnly = True
                Literal1.Text = "Detalles de la cuenta"
                PnlInfoAdicional.Visible = True
            End If
        End If


        Dim cuentas As New List(Of BECuentaDineroVirtual)
        Dim param = New BECuentaDineroVirtual()
        param.IdEstado = 0
        param.IdMoneda = 0
        param.IdUsuario = entidad.IdUsuario
        cuentas = proxy.ConsultarCuentaDineroVirtualUsuario(param)
        lsCuentas.Items.Clear()
        For Each cuenta As BECuentaDineroVirtual In cuentas
            If cuenta.IdEstado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                lsCuentas.Items.Add("N° Cuenta " + cuenta.Numero + " (Alias: " + cuenta.AliasCuenta + " | Saldo: " + cuenta.MonedaSimbolo _
                                    + " " + cuenta.Saldo.ToString + ")")
            End If
        Next


        ''lsCuentas.Items.Insert(0, "::: Seleccionar :::")


        'WebUtil.DropDownlistBinding(ddlTipoMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::: Seleccione :::")

    End Sub

    '' Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
    'Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
    'Dim oCDineroVirtual As New CDineroVirtual
    'Dim Res As Integer = 0

    'Try
    '    If ddlTipoMoneda.SelectedIndex = 0 Then
    '        lblTransaccion.Text = "Debe seleccionar el Tipo de Moneda ..."
    '        lblTransaccion.ForeColor = Drawing.Color.Red
    '        Limpiar(True)
    '    Else
    '        oBECuentaDineroVirtual.IdMoneda = CInt(ddlTipoMoneda.SelectedValue)
    '        oBECuentaDineroVirtual.AliasCuenta = TxtAlias.Text
    '        oBECuentaDineroVirtual.IdUsuario = SPE.Web.PaginaBase.UserInfo.IdUsuario '5
    '        Res = oCDineroVirtual.RegistrarCuentaDineroVirtual(oBECuentaDineroVirtual)
    '        If Res = 1 Then
    '            lblTransaccion.Text = "Su solicitud de creación de Dinero Virtual ha sido enviada con Éxito"
    '            lblTransaccion.ForeColor = Drawing.Color.Navy
    '            Limpiar(False)
    '        Else
    '            lblTransaccion.Text = "Se ha producido un error al solicitar la cración de su cuenta"
    '            lblTransaccion.ForeColor = Drawing.Color.Red
    '        End If
    '    End If

    '    lblTransaccion.Visible = True

    'Catch ex As Exception

    'End Try
    '' End Sub

    Protected Sub btnAprobar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAprobar.Click
        Actualizar(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado)
    End Sub

    Protected Sub btnRechazar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRechazar.Click
        Actualizar(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Rechazado)
    End Sub
    Sub Actualizar(ByVal estado As Integer)
        Dim proxy As New SPE.Web.CDineroVirtual
        Dim entidad = New BECuentaDineroVirtual()

        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
            entidad.IdUsuarioRespuesta = ClienteUsuarioID
        End If

        entidad.IdCuentaDineroVirtual = Convert.ToInt64(hdnIdCuenta.Value)
        entidad.Observacion = TxtObservacion.Text
        entidad.IdEstado = estado

        If proxy.ActualizarCuentaDineroVirtual(entidad) Then
            lblTransaccion.Text = "Se actualizo correctamente"
        Else
            lblTransaccion.Text = "no se actualizo correctamente"
        End If

        JSMessageAlertWithRedirect("Info", "Se ha guardado los Datos Satisfactoriamente.", "aa", "../DV/COCuentaAdmin.aspx")
        'CargarCuenta(entidad.IdCuentaDineroVirtual)

    End Sub
    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        If Perfil = "Cliente" Then
            Response.Redirect("COCUCli.aspx")
        Else
            Response.Redirect("COCuentaAdmin.aspx")
        End If
    End Sub

    Public Sub JSMessageAlertWithRedirect(ByVal tipo_alerta As String, ByVal mensaje As String, ByVal key As String, ByVal PageRedirect As String)
        Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", tipo_alerta, mensaje, PageRedirect)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim proxy As New SPE.Web.CDineroVirtual
            Dim entidad = New BECuentaDineroVirtual()
            entidad.IdCuentaDineroVirtual = CInt(hdnIdCuenta.Value)
            entidad = proxy.ConsultarCuentaDineroVirtualUsuarioByCuentaId(entidad)

            Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)
            Dim IdUSuarioVisitante As Long = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario)

            Select Case Perfil
                Case SPE.EmsambladoComun.ParametrosSistema.RolCliente
                    If entidad.IdUsuario = IdUSuarioVisitante Then
                    Else
                        Response.Redirect("~/SEG/sinautrz.aspx")
                        Return
                    End If
            End Select

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Observacion", IIf(String.IsNullOrEmpty(entidad.Observacion), " - ", entidad.Observacion)))
            parametros.Add(New KeyValuePair(Of String, String)("AliasCuenta", entidad.AliasCuenta))
            parametros.Add(New KeyValuePair(Of String, String)("EMail", entidad.Email))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", entidad.EstadoDescripcion))
            parametros.Add(New KeyValuePair(Of String, String)("FechaAprobacion", IIf(entidad.FechaAprobacion = DateTime.MinValue, " - ", entidad.FechaAprobacion.ToString("dd/MM/yyyy"))))
            parametros.Add(New KeyValuePair(Of String, String)("FechaRechazo", IIf(entidad.FechaRechazo = DateTime.MinValue, " - ", entidad.FechaRechazo.ToString("dd/MM/yyyy"))))
            parametros.Add(New KeyValuePair(Of String, String)("UsuarioAprobacion", IIf(entidad.UsuarioAprobacion.Equals(String.Empty), " - ", entidad.UsuarioAprobacion)))
            parametros.Add(New KeyValuePair(Of String, String)("UsuarioRechazo", IIf(entidad.UsuarioRechazo.Equals(String.Empty), " - ", entidad.UsuarioRechazo)))
            parametros.Add(New KeyValuePair(Of String, String)("FechaSolicitud", IIf(entidad.FechaSolicitud = DateTime.MinValue, " - ", entidad.FechaSolicitud.ToString("dd/MM/yyyy"))))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", entidad.MonedaDescripcion))
            parametros.Add(New KeyValuePair(Of String, String)("NumeroCuenta", "Nro." & entidad.Numero))
            parametros.Add(New KeyValuePair(Of String, String)("NumeroSolicitud", entidad.IdCuentaDineroVirtual))
            parametros.Add(New KeyValuePair(Of String, String)("Solicitante", entidad.ClienteNombre))

            Dim cuentas As New List(Of BECuentaDineroVirtual)
            Dim param = New BECuentaDineroVirtual()
            param.IdEstado = 0
            param.IdMoneda = 0
            param.IdUsuario = entidad.IdUsuario
            cuentas = proxy.ConsultarCuentaDineroVirtualUsuario(param)

            UtilReport.ProcesoExportarGenerico(cuentas, Page, "EXCEL", "xls", "Detalles de la Cuenta - ", parametros, "RptCOCUDE.rdlc")

        End If
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - Detalle de Cuenta"
    End Sub
End Class
