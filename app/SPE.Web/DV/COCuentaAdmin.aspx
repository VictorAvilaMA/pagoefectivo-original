﻿<%@ Page Title="PagoEfectivo - Consultar Cuentas" Async="true" Language="VB" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/DV/COCuentaAdmin.aspx.vb" Inherits="DV_COCuentaAdmin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Consulta de Cuentas de Dinero Virtual</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de búsqueda</h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt; </span>Cliente:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCliente" MaxLength="200" runat="server" CssClass="neu"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombre" runat="server" ValidChars="AaÁáBbCcDdEeÉéFfGgHhIiÍíJjKkLlMmNnÑñOoÓóPpQqRrSsTtUuÚúVvWwÜüXxYyZz,' "
                            TargetControlID="txtCliente">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Email:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" MaxLength="100" runat="server" CssClass="neu"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" ValidChars="AaBbCcDdEeFfGgHhIiJjKkLlMmNnÑñOoPpQqRrSsTtUuVvWwÜüXxYyZz123456789._-@'"
                            TargetControlID="txtEmail">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha Desde:</li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDesde" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDesde" runat="server" TargetControlID="txtFechaDesde"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaHasta" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderHasta" runat="server" TargetControlID="txtFechaHasta"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <cc1:CalendarExtender ID="CalendarExtenderDesde" runat="server" TargetControlID="txtFechaDesde"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnFechaDesde">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderHasta" runat="server" TargetControlID="txtFechaHasta"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnFechaHasta">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDesde" runat="server" ControlExtender="MaskedEditExtenderDesde"
                            ControlToValidate="txtFechaDesde" ErrorMessage="*" InvalidValueMessage="Fecha Desde no válida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha Desde es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorHasta" runat="server" ControlExtender="MaskedEditExtenderHasta"
                            ControlToValidate="txtFechaHasta" ErrorMessage="*" InvalidValueMessage="Fecha Hasta no válida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha Hasta es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="covFecha" runat="server" ErrorMessage="Rango de fechas de búsqueda no es válido"
                            ControlToCompare="txtFechaHasta" ControlToValidate="txtFechaDesde" Operator="LessThanEqual"
                            Type="Date">*</asp:CompareValidator>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Tipo Moneda:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlIdMoneda" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlIdEstadoCuenta" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                <asp:Button ID="btnImprimir" runat="server" CssClass="input_azul4" Text="Excel" />
            </li>
        </ul>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" class="cont_cel">
            <ContentTemplate>
                <h5 id="h5result" runat="server">
                    <asp:Literal ID="lblResultado" runat="server" Text="" />
                </h5>
                <asp:GridView ID="grdResultado" runat="server" BackColor="White" AllowPaging="True"
                    AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                    CellPadding="3" CssClass="grilla" EnableTheming="True" ShowFooter="False" AllowSorting="True">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" SortExpression="NumeroCuenta">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnIdCuenta" CommandName="Detalle" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdCuentaDineroVirtual")%>'
                                    runat="server" Text='<%#Convert.ToInt32(DataBinder.Eval(Container.DataItem, "IdCuentaDineroVirtual")).ToString("#00000")%>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Config.">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgConfig" runat="server" CommandName="Configurar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/Configurar.png" %>'
                                    ToolTip="Configurar" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdCuentaDineroVirtual")%>' />
                                <asp:HiddenField ID="hdfIdCuenta" runat="server" Value='<%# Eval("IdCuentaDineroVirtual") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ver Movimientos" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="ibtnDetalle" runat="server" CommandName="VerDetalle" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                    ToolTip="Ver Movimientos de dinero" Text="Ver" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdCuentaDineroVirtual")%>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="ClienteNombre" HeaderText="Cliente" SortExpression="CLIENTE">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Nº Cuenta/Alias" SortExpression="NumeroCuenta">
                            <ItemTemplate>
                                <%# String.Concat("[", If(DataBinder.Eval(Container.DataItem, "Numero") = "","-",DataBinder.Eval(Container.DataItem, "Numero")), "]", Environment.NewLine ,"[", If(DataBinder.Eval(Container.DataItem, "AliasCuenta")="","-",DataBinder.Eval(Container.DataItem, "AliasCuenta")),"]")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AbrevMoneda" HeaderText="Mon." SortExpression="ABREV_MONEDA">
                        </asp:BoundField>
                        <asp:BoundField DataField="DiasTranscurridosSolicitud" HeaderText="Días Trans.">
                        </asp:BoundField>
                        <asp:BoundField DataField="EstadoDescripcion" HeaderText="Estado" SortExpression="DESC_ESTADO">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Fecha Solicitud" SortExpression="FechaSolicitud">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaSolicitud"))%>
                                <%--<%#SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>--%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha Aprobación" SortExpression="FechaAprobacion">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaAprobacion"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha Rechazo" SortExpression="FechaRechazo">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaRechazo"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    <EmptyDataTemplate>
                        No se encontraron registros.
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:HiddenField ID="hdfSortProp" runat="server" Value="IdCuenta" />
                <asp:HiddenField ID="hdfSortDir" runat="server" Value="0" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                <%--<asp:AsyncPostBackTrigger ControlID="grdResultado" EventName="PageIndexChanging">
                    </asp:AsyncPostBackTrigger>--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
