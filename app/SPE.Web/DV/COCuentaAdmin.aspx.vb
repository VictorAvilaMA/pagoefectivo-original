﻿Imports System.Collections.Generic
Imports SPE.Entidades

Partial Class DV_COCuentaAdmin
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CDineroVirtual)

    Private _ResultList As New List(Of SPE.Entidades.BECuentaDineroVirtual)
    Public Property ResultList() As List(Of SPE.Entidades.BECuentaDineroVirtual)
        Get
            Return _ResultList
        End Get
        Set(ByVal value As List(Of SPE.Entidades.BECuentaDineroVirtual))
            _ResultList = value
        End Set
    End Property

    Protected Sub DV_COCuentaAdmin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            txtFechaDesde.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy")
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy")
            CargarCombo()


            grdResultado.PageIndex = 0
            CargarGrilla()

        End If
    End Sub


    Private Sub CargarCombo()
        Dim objParametro As New SPE.Web.CAdministrarParametro
        Dim control As New SPE.Web.CDineroVirtual

        ddlIdEstadoCuenta.DataTextField = "Descripcion" : ddlIdEstadoCuenta.DataValueField = "Id" : ddlIdEstadoCuenta.DataSource = objParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudDineroVirtual) : ddlIdEstadoCuenta.DataBind() : ddlIdEstadoCuenta.Items.Insert(0, New ListItem("::: Todos :::", "0"))

        ddlIdMoneda.DataTextField = "Descripcion"
        ddlIdMoneda.DataValueField = "IdMoneda"
        ddlIdMoneda.DataSource = control.ConsultarMonedaMonederoVirtual()
        ddlIdMoneda.DataBind()
        ddlIdMoneda.Items.Insert(0, New ListItem("::: Todos :::", "0"))
        ddlIdEstadoCuenta.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Solicitado
    End Sub

    Private Sub CargarGrilla()
        txtCliente.Text = txtCliente.Text.Trim()
        txtEmail.Text = txtEmail.Text.Trim()
        txtFechaDesde.Text = txtFechaDesde.Text.Trim()
        txtFechaHasta.Text = txtFechaHasta.Text.Trim()

        Dim cntrDV As New SPE.Web.CDineroVirtual
        Dim paramDV As New SPE.Entidades.BECuentaDineroVirtual
        paramDV.ClienteNombre = txtCliente.Text
        paramDV.Email = txtEmail.Text
        paramDV.FechaSolicitud = If(String.IsNullOrEmpty(txtFechaDesde.Text), DateTime.MinValue, Convert.ToDateTime(txtFechaDesde.Text))
        paramDV.FechaAprobacion = If(String.IsNullOrEmpty(txtFechaHasta.Text), DateTime.MinValue, Convert.ToDateTime(txtFechaHasta.Text))
        paramDV.IdMoneda = Convert.ToInt32(ddlIdMoneda.SelectedValue)
        paramDV.IdEstado = Convert.ToInt32(ddlIdEstadoCuenta.SelectedValue)
        paramDV.PageSize = grdResultado.PageSize
        paramDV.PageNumber = grdResultado.PageIndex + 1
        paramDV.PropOrder = hdfSortProp.Value
        paramDV.TipoOrder = hdfSortDir.Value.Equals("1")
        ViewState("CuentaDV") = paramDV
        ResultList = cntrDV.ConsultarCuentasVirtualesAdministrador(paramDV)

        If ResultList.Count() > 0 Then
            ViewState("SCuenta") = ResultList(0).AbrevMoneda
            ViewState("ECuenta") = ResultList(0).EstadoDescripcion
        Else
            ViewState("SCuenta") = "Todas"
            ViewState("ECuenta") = "Todos"
        End If

        Dim oObjectDataSource As New ObjectDataSource
        oObjectDataSource.ID = "oObjectDataSource_" + grdResultado.ID
        oObjectDataSource.EnablePaging = grdResultado.AllowPaging
        oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
        oObjectDataSource.SelectMethod = "GetDataGrilla"
        oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
        oObjectDataSource.StartRowIndexParameterName = "filaInicio"
        oObjectDataSource.MaximumRowsParameterName = "maxFilas"
        oObjectDataSource.EnableViewState = False

        AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating

        grdResultado.DataSource = oObjectDataSource
        grdResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, If(ResultList.Count = 0, 0, ResultList(0).TotalPageNumbers))

    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        Dim TotalGeneral As Int32
        TotalGeneral = If(ResultList.Count = 0, 0, ResultList(0).TotalPageNumbers)
        e.ObjectInstance = New _3Dev.FW.Web.TotalRegistrosTableAdapter(ResultList, TotalGeneral)
    End Sub

    Protected Sub grdResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdResultado.Sorting
        hdfSortProp.Value = e.SortExpression
        hdfSortDir.Value = If(hdfSortDir.Value = "1", "0", "1")
        CargarGrilla()
    End Sub

    Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdResultado.PageIndexChanging
        grdResultado.PageIndex = e.NewPageIndex
        CargarGrilla()
    End Sub

#Region "EVENTO BOTONES"
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        h5result.Visible = True
        grdResultado.PageIndex = 0
        CargarGrilla()
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        h5result.Visible = False
        txtFechaDesde.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy")
        txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy")
        ddlIdEstadoCuenta.SelectedIndex = 0
        ddlIdMoneda.SelectedIndex = 0
        lblResultado.Text = ""
        grdResultado.DataSource = Nothing
        grdResultado.DataBind()
    End Sub
#End Region

    Protected Sub grdResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdResultado.RowCommand

        VariableTransicion = e.CommandArgument
        Session("UsaElFramework_CuentaId") = e.CommandArgument

        Select Case e.CommandName
            Case "Detalle"
                Response.Redirect("COCUDE.aspx")
            Case "Configurar"
                Response.Redirect("~/DV/HaInCuDiVi.aspx")
            Case "VerDetalle"
                Response.Redirect("~/DV/COMoCuDiVi.aspx")
        End Select
    End Sub

    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel.")
        Else
            Dim MonedaRq As String
            Dim EstadoRq As String
            Dim DescMonedaRq As String = ""
            Dim SimboloMonedaRq As String
            Dim DescEstadoRq As String
            '///////////////////////////////////////////////////////////////////////////
            If ddlIdMoneda.SelectedIndex = 0 Then
                MonedaRq = "0"
                'DescMonedaRq = "Todos"
                SimboloMonedaRq = "Todos"
            Else
                MonedaRq = ddlIdMoneda.SelectedValue.ToString()
                'DescMonedaRq = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).MonedaDescripcion.ToString()
                SimboloMonedaRq = "(" & ViewState("SCuenta").ToString() & ") "
            End If
            '///////////////////////////////////////////////////////////////////////////
            If ddlIdEstadoCuenta.SelectedIndex = 0 Then
                EstadoRq = "0"
                DescEstadoRq = "Todos"
            Else
                EstadoRq = ddlIdEstadoCuenta.SelectedValue.ToString()
                DescEstadoRq = ViewState("ECuenta").ToString()
            End If
            Dim strFechaInicio As String = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).FechaSolicitud.ToString() 'CType(txtFechaDesde.Text, Date)
            Dim strFechaFin As String = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).FechaAprobacion.ToString()
            Dim strCliente As String = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).ClienteNombre.ToString()
            Dim strEmail As String = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).Email.ToString()
            'Response.Redirect(String.Format("~/Reportes/RptCuentasClienteAdmin.aspx?PIdm={0}&PIde={1}&PDMo={2}&PSMo={3}&PDes={4}&FIni={5}&FFin={6}&PNcl={7}&Pem={8}", _
            '                                MonedaRq, EstadoRq, DescMonedaRq, SimboloMonedaRq, DescEstadoRq, strFechaInicio, strFechaFin, strCliente, strEmail))

            Dim entidad As New SPE.Entidades.BECuentaDineroVirtual
            Dim resultados As New List(Of BECuentaDineroVirtual)
            Using oCDineroVirtual As New SPE.Web.CDineroVirtual()
                entidad.ClienteNombre = strCliente
                entidad.Email = strEmail
                entidad.FechaSolicitud = strFechaInicio
                entidad.FechaAprobacion = strFechaFin
                entidad.IdMoneda = MonedaRq
                entidad.IdEstado = EstadoRq
                entidad.PageSize = (Int32.MaxValue - 1)
                entidad.PageNumber = 1
                entidad.PropOrder = "FechaAprobacion"
                entidad.TipoOrder = 1
                resultados = oCDineroVirtual.ConsultarCuentasVirtualesAdministrador(entidad)
            End Using

            strFechaInicio = If(String.IsNullOrEmpty(txtFechaDesde.Text.Trim()), DateTime.MinValue, Convert.ToDateTime(txtFechaDesde.Text.Trim()))
            strFechaFin = If(String.IsNullOrEmpty(txtFechaHasta.Text.Trim()), DateTime.MinValue, Convert.ToDateTime(txtFechaDesde.Text.Trim()))
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Desde", strFechaInicio))
            parametros.Add(New KeyValuePair(Of String, String)("Hasta", strFechaFin))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", SimboloMonedaRq & DescMonedaRq))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", DescEstadoRq))

            UtilReport.ProcesoExportarGenerico(resultados, Page, "EXCEL", "xls", "Cuentas de Cliente - ", parametros, "RptCuentasClienteAdmin.rdlc")

        End If
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - PagoEfectivo - Consultar Cuentas"
    End Sub
End Class
