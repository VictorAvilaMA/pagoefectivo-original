﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COMoCuDiVi.aspx.vb" Inherits="DV_COMoCuDiVi"
    Title="PagoEfectivo - Consultar Movimientos de Cuenta de Dinero Virtual" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        function hideModalPopUp() {
            $("#<%= pnlDetalleCuenta.ClientID() %>").hide();
            return;
        }
    </script>
    <h2>
        Consultar Movimientos de dinero</h2>
    <div class="conten_pasos3">
        <div class="datos_head">
            <p class="texto_datos_head">
                <strong>Titular:</strong>
                <asp:Literal ID="ltrTitular" runat="server"></asp:Literal>
            </p>
            <div class="otros_datos">
                <p>
                    <strong>Cuenta:</strong>
                    <asp:Literal ID="ltrCuenta" runat="server"></asp:Literal>
                </p>
                <p>
                    <strong>Moneda:</strong>
                    <asp:Literal ID="ltrMoneda" runat="server"></asp:Literal>
                </p>
            </div>
        </div>
        <h4>
            Criterios de b&uacute;squeda</h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt; </span>Tipo de movimiento : </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoMovimiento" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha de movimiento del: </li>
                    <li class="t2" style="line-height: 1;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDe" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaDe" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Del' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Del' no válida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaA" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaA" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Al' es requerida"
                                ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Al' no válida"
                                IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div class="clear">
                            </div>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Rango de fechas no es válido"
                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                Type="Date" ValidationGroup="GrupoValidacion">*</asp:CompareValidator>
                        </p>
                    </li>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" ValidationGroup="GrupoValidacion">
                    </asp:ValidationSummary>
                    <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                        Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaDe">
                    </cc1:MaskedEditExtender>
                    <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                        Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaA">
                    </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="ibtnFechaDe" TargetControlID="txtFechaDe">
                    </cc1:CalendarExtender>
                    <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA"
                        TargetControlID="txtFechaA">
                    </cc1:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" ValidationGroup="GrupoValidacion" />
                <asp:Button ID="btnImprimir" runat="server" CssClass="input_azul4" Text="Excel" />
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Regresar" />
            </li>
        </ul>
        <div class="sep_a14">
        </div>
        <div class="cont_cel">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <asp:Label ID="lblResultado" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div id="div5">
                        <h5>
                            <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label>
                        </h5>
                    </div>
                    <div>
                        <div>
                            <asp:GridView ID="gvResultado" runat="server" BackColor="White" DataKeyNames="IdMovimientoCuenta"
                                CssClass="grilla" AllowPaging="True" CellPadding="3" BorderWidth="1px" BorderStyle="None"
                                BorderColor="#CCCCCC" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging"
                                OnRowDataBound="gvResultado_RowDataBound" AllowSorting="True" PageSize="12" OnSorting="gvResultado_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Nº Operación" ItemStyle-HorizontalAlign="Center" SortExpression="NumeroOperacion">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdfNumOp" runat="server" Value='<%# Eval("NumeroOperacion") %>' />
                                            <asp:LinkButton ID="lbtnNumeroOperacion" CommandName="Detalle" CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                                runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOperacion")%>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FechaMovimiento" HeaderText="Fecha" SortExpression="FechaMovimiento" />
                                    <asp:BoundField DataField="DescTipoMovimiento" HeaderText="Tipo de Operación" SortExpression="Descripcion" />
                                    <asp:BoundField DataField="Observacion" HeaderText="Descripción" SortExpression="Observacion" />
                                    <asp:BoundField DataField="Monto" HeaderText="Monto" SortExpression="Monto">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="cabecera" />
                            </asp:GridView>
                        </div>
                        <div align="center" style="overflow:hidden">
                            <asp:HiddenField ID="hdfCuentaId" runat="server" />
                            <asp:HiddenField ID="hdfCuentaCli" runat="server" />
                            <asp:HiddenField ID="hdfRol" runat="server" />
                            <asp:Label ID="lblSaldoDisponible" runat="server"></asp:Label>
                        </div>
                        <asp:HiddenField ID="hfSortDir" runat="server" />
                    </div>
                    <div id="fsBotonera" runat="server" class="clearfix dlinline btns2">
                        <asp:Button ID="Button1" runat="server" CssClass="Hidden" Style="display: none" />
                    </div>
                    <cc1:ModalPopupExtender ID="mpeDetalleCuenta" runat="server" TargetControlID="Button1"
                        CancelControlID="imgbtnRegresarHidden" PopupControlID="pnlDetalleCuenta" BackgroundCssClass="modalBackground"
                        DropShadow="true">
                    </cc1:ModalPopupExtender>
                    <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl="~/img/closeX.GIF"
                        Style="display: none;" CssClass="Hidden" CausesValidation="false" />
                    <asp:Panel ID="pnlDetalleCuenta" runat="server" Style="display: none; width: auto;
                        background-color: White;">
                        <div style="float: right">
                            <asp:ImageButton ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" OnClientClick="hideModalPopUp();"
                                runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"img/closeX.GIF" %>' CausesValidation="false"></asp:ImageButton>
                        </div>
                        <asp:HiddenField ID="hdfIdParametro" runat="server" Value="" />
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span> N&uacute;mero de Operac&oacute;n:
                            </li>
                            <li class="t2">
                                <asp:TextBox ID="txtNroOperacion" runat="server" MaxLength="100" ReadOnly="true"
                                    CssClass="normal"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span> Fecha/Hora:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtFecha" runat="server" MaxLength="100" ReadOnly="true" CssClass="normal"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span> Tipo de Operaci&oacute;n:</li>
                            <li class="t2">
                                <asp:TextBox ID="txtTipoOperacion" runat="server" MaxLength="100" ReadOnly="true"
                                    CssClass="normal"></asp:TextBox>
                            </li>
                            <div style="clear: both;"></div>
                            <div id="DivCodRef" runat="server">
                                <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo de Referencia:</li>
                                <li class="t2">
                                    <asp:TextBox ID="txtCodReferencia" runat="server" MaxLength="50" ReadOnly="true"
                                        CssClass="normal"></asp:TextBox>
                                </li>
                            </div>
                            <div id="DivTitular" runat="server">
                                <li class="t1"><span class="color">&gt;&gt;</span> Titular:</li>
                                <li class="t2">
                                    <asp:TextBox ID="txtTitular" runat="server" MaxLength="100" ReadOnly="true" CssClass="normal"></asp:TextBox>
                                </li>
                            </div>
                            <div id="DivPortal" runat="server">
                                <li class="t1"><span class="color">&gt;&gt;</span> Nombre Portal:</li>
                                <li class="t2">
                                    <asp:TextBox ID="txtPortal" runat="server" MaxLength="100" ReadOnly="true" CssClass="normal"></asp:TextBox>
                                </li>
                            </div>
                            <div id="DivConceptoPago" runat="server">
                                <li class="t1"><span class="color">&gt;&gt;</span> Concepto de Pago:</li>
                                <li class="t2">
                                    <asp:TextBox ID="txtConceptoPago" runat="server" MaxLength="100" ReadOnly="true"
                                        CssClass="normal"></asp:TextBox>
                                </li>
                            </div>
                            <li class="complet">
                                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul5" OnClientClick="hideModalPopUp()"
                                    Text="Regresar" />
                            </li>
                        </ul>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
