﻿Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Data
Imports System.Linq
Imports _3Dev.FW.Web
Imports SPE.Web
Imports _3Dev.FW.Util.DataUtil
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class DV_COMoCuDiVi
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CComun)

    Dim listaBEMovimientoCuenta As New List(Of BEMovimientoCuenta)
    Dim saldo As Decimal

    'Permite mostrar el mensaje
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblResultado
        End Get
    End Property

    'Sobreescribe la info de la grilla genérica para asignarle el nombre
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    'Hacer esta precarga controla el paginado (doble load)
    Public Overrides Sub OnMainSearch()
        ListarMovimientoXIdCuenta()
    End Sub

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim TipMov As Integer
        Dim oBEMovimientoCuenta As New BEMovimientoCuenta
        If ddlTipoMovimiento.SelectedIndex = 0 Then
            TipMov = 0
        Else
            TipMov = ddlTipoMovimiento.SelectedValue
        End If

        'If Request.QueryString("pabc") Is Nothing Then
        oBEMovimientoCuenta.IdCuentaVirtual = Convert.ToInt64(hdfCuentaCli.Value)
        'Else : oBEMovimientoCuenta.IdCuentaVirtual = Convert.ToInt64(Request.QueryString("pabc"))
        'End If

        '54 'Modificar para que sea dinámico
        oBEMovimientoCuenta.IdTipoMovimiento = TipMov
        oBEMovimientoCuenta.FechaInicio = CType(txtFechaDe.Text, Date)
        oBEMovimientoCuenta.FechaFin = CType(txtFechaA.Text, Date)
        oBEMovimientoCuenta.PropOrder = SortExpression
        oBEMovimientoCuenta.TipoOrder = SortDir
        oBEMovimientoCuenta.PageNumber = gvResultado.PageIndex
        oBEMovimientoCuenta.PageSize = gvResultado.PageSize
        Return oBEMovimientoCuenta
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim i As Int64 = VariableTransicion
            hdfCuentaId.Value = Convert.ToInt64(MaintenanceKeyValue)
            hdfCuentaCli.Value = i
            If hdfCuentaId.Value = 0 Then
                If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
                    hdfCuentaId.Value = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario)
                End If
            End If
            hdfRol.Value = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)
            CargarCombo()
            ddlTipoMovimiento.SelectedIndex = 0
            hfSortDir.Value = 1
            txtFechaDe.Text = CStr(DateAdd(DateInterval.Day, -30, Date.Now()))
            txtFechaA.Text = CStr(Date.Now())
            ListarMovimientoXIdCuenta()
        End If

        Dim IdCuentaRq As Long
        'If Request.QueryString("pabc") Is Nothing Then
        IdCuentaRq = Convert.ToInt64(hdfCuentaCli.Value)
        'Else : IdCuentaRq = Convert.ToInt64(Request.QueryString("pabc"))   'Va a venir por query string
        'End If

        Dim cuenta As New BECuentaDineroVirtual
        cuenta.IdCuentaDineroVirtual = IdCuentaRq
        cuenta = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(cuenta)
        Dim NroCuenta As String = cuenta.Numero
        Dim DescMoneda As String = cuenta.MonedaDescripcion
        ViewState("Simbolo") = cuenta.MonedaSimbolo
        'ltrTitular.Text = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Apellidos + ", " + CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Nombres
        ltrTitular.Text = cuenta.ClienteNombre
        ltrCuenta.Text = NroCuenta
        ltrMoneda.Text = DescMoneda
        'ltrFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")
        'ltrHora.Text = DateTime.Now.ToString("hh:mm tt")
        DivTitular.Visible = False
        DivConceptoPago.Visible = False
        DivPortal.Visible = False
        txtFechaDe.Focus()
    End Sub

    Private Sub CargarCombo()

        Dim objCAdministrarParametro As New SPE.Web.CAdministrarParametro
        Dim listaBEParametro As New List(Of BEParametro)
        listaBEParametro = objCAdministrarParametro.ConsultarParametroPorCodigoGrupo("TMME")
        With (ddlTipoMovimiento)
            .DataSource = listaBEParametro
            .DataTextField = "Descripcion"
            .DataValueField = "Id"
            .DataBind()
        End With
        ddlTipoMovimiento.Items.Insert(0, ":::Todos:::")

    End Sub

    'Paginado de Grilla
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarMovimientoXIdCuenta()
        '
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvResultado.RowDataBound
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, "IdMovimientoCuenta", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

        SortExpression = e.SortExpression
        Dim SortDirhf As Integer
        SortDirhf = CInt(hfSortDir.Value)
        If (SortDirhf.Equals(SortDirection.Ascending)) Then
            SortDirhf = SortDirection.Descending
            hfSortDir.Value = 1
        Else
            SortDirhf = SortDirection.Ascending
            hfSortDir.Value = 0
        End If
        ListarMovimientoXIdCuenta()
        '
    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        e.ObjectInstance = New TotalRegistrosTableAdapter(listaBEMovimientoCuenta, listaBEMovimientoCuenta.First().TotalPageNumbers)
    End Sub

    Private Sub ListarMovimientoXIdCuenta()

        Try
            Dim objCDineroVirtual As New SPE.Web.CDineroVirtual

            Dim businessEntityObject As BEMovimientoCuenta
            businessEntityObject = CreateBusinessEntityForSearch()

            Dim SortDirhf As Integer
            SortDirhf = CInt(hfSortDir.Value)
            businessEntityObject.PageNumber = PageNumber
            businessEntityObject.PageSize = GridViewResult.PageSize
            businessEntityObject.TipoOrder = IIf(SortDirhf = SortDirection.Ascending, True, False)
            businessEntityObject.PropOrder = SortExpression
            GridViewResult.PageIndex = PageNumber - 1

            listaBEMovimientoCuenta = objCDineroVirtual.ConsultarMovimientoXIdCuenta(businessEntityObject)
            ViewState("listBEMovimientoCuenta") = listaBEMovimientoCuenta
            If listaBEMovimientoCuenta.Count() <> 0 Then

                Dim oObjectDataSource As New ObjectDataSource()
                Dim SaldoDisp As Decimal

                oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID
                oObjectDataSource.EnablePaging = GridViewResult.AllowPaging
                oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
                oObjectDataSource.SelectMethod = "GetDataGrilla"
                oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
                oObjectDataSource.StartRowIndexParameterName = "filaInicio"
                oObjectDataSource.MaximumRowsParameterName = "maxFilas"
                oObjectDataSource.EnableViewState = False

                AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating
                GridViewResult.DataSource = oObjectDataSource

                SaldoDisp = listaBEMovimientoCuenta.Item(0).SaldoPostOperacion
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, listaBEMovimientoCuenta.First().TotalPageNumbers)
                lblSaldoDisponible.Text = "Saldo Disponible:        " & ViewState("Simbolo") & " " & SaldoDisp.ToString()
                lblSaldoDisponible.ForeColor = Drawing.Color.Navy
                lblSaldoDisponible.Font.Bold = True
            Else
                GridViewResult.DataSource = New List(Of BEMovimientoCuenta)
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
            End If

            GridViewResult.DataBind()
            lblResultado.Text = ""

        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        ListarMovimientoXIdCuenta()
    End Sub

    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel.")
        Else
            'Dim IdCuentaRq As Long = Convert.ToInt64(Request.QueryString("pabc"))   'Va a venir por query string
            Dim IdCuentaRq As Long = Convert.ToInt64(hdfCuentaCli.Value)
            Dim cuenta As New BECuentaDineroVirtual
            cuenta.IdCuentaDineroVirtual = IdCuentaRq

            cuenta = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(cuenta)

            Dim CuentaRq As String = cuenta.Numero
            Dim MonedaRq As String = cuenta.MonedaDescripcion
            Dim SimboloMonedaRq As String = cuenta.MonedaSimbolo
            Dim IdTipMovRq As Integer
            Dim TipMovRq As String
            If ddlTipoMovimiento.SelectedIndex = 0 Then
                IdTipMovRq = 0
                TipMovRq = "Todos"
            Else
                IdTipMovRq = ddlTipoMovimiento.SelectedValue
                TipMovRq = ddlTipoMovimiento.SelectedItem.Text.ToString()
            End If
            Dim strFechaInicio As String = CType(txtFechaDe.Text, Date)
            Dim strFechaFin As String = CType(txtFechaA.Text, Date)
            'Response.Redirect(String.Format("~/Reportes/RptMovimientoCuenta.aspx?PNCta={0}&PMnd={1}&PTMov={2}&FIni={3}&FFin={4}&PIdcta={5}&PItm={6}&PSmd={7}", CuentaRq, MonedaRq, TipMovRq, strFechaInicio, strFechaFin, IdCuentaRq, IdTipMovRq, SimboloMonedaRq))
            Dim SaldoDisponible As Decimal = 0
            Dim AliasUsuarioRs As String = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).NombreCompleto
            Dim oBEMovimientoCuenta As New BEMovimientoCuenta
            Dim listaBEMovimientoCuenta As New List(Of BEMovimientoCuenta)
            Using oCDineroVirtual As New CDineroVirtual()
            
                oBEMovimientoCuenta.IdCuentaVirtual = IdCuentaRq
                oBEMovimientoCuenta.IdTipoMovimiento = IdTipMovRq
                oBEMovimientoCuenta.FechaInicio = strFechaInicio
                oBEMovimientoCuenta.FechaFin = DateAdd(DateInterval.Day, 1, CType(txtFechaA.Text, Date))
                listaBEMovimientoCuenta = oCDineroVirtual.ConsultarMovimientoXIdCuentaExportar(oBEMovimientoCuenta)

                If listaBEMovimientoCuenta.Count() <> 0 Then
                    SaldoDisponible = listaBEMovimientoCuenta(0).SaldoPostOperacion

                Else
                    SaldoDisponible = 0
                End If

            End Using


            strFechaInicio = If(String.IsNullOrEmpty(txtFechaDe.Text.Trim()), DateTime.MinValue, Convert.ToDateTime(txtFechaDe.Text.Trim()))
            strFechaFin = If(String.IsNullOrEmpty(txtFechaA.Text.Trim()), DateTime.MinValue, Convert.ToDateTime(txtFechaA.Text.Trim()))
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Titular", AliasUsuarioRs))
            parametros.Add(New KeyValuePair(Of String, String)("Cuenta", CuentaRq))
            parametros.Add(New KeyValuePair(Of String, String)("TipoMovimiento", TipMovRq))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", MonedaRq))
            parametros.Add(New KeyValuePair(Of String, String)("Desde", strFechaInicio))
            parametros.Add(New KeyValuePair(Of String, String)("Hasta", strFechaFin))
            parametros.Add(New KeyValuePair(Of String, String)("Saldo", SimboloMonedaRq & SaldoDisponible.ToString()))

            UtilReport.ProcesoExportarGenerico(listaBEMovimientoCuenta, Page, "EXCEL", "xls", "Movimientos de la cuenta - ", parametros, "RptMovimientoCuenta.rdlc")

        End If
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

        'If Request.QueryString("pabc") Is Nothing Then
        '    Response.Redirect("COCuentaAdmin.aspx")
        'Else : Response.Redirect("COCUCli.aspx")
        'End If
        If hdfRol.Value.ToString.Trim() = RolCliente Then
            Response.Redirect("COCUCli.aspx")
        Else : Response.Redirect("COCuentaAdmin.aspx")
        End If

    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Detalle"
                Dim reg As Integer = gvResultado.Rows(CInt(e.CommandArgument)).RowIndex
                'Dim hdfNumeroOperacion As String = DirectCast(gvResultado.Rows(CInt(e.CommandArgument.ToString())).Controls(0).FindControl("hdfNumOp"), HiddenField).Value
                Dim hdfNumeroOperacion As String = CType(gvResultado.Rows(reg).FindControl("hdfNumOp"), HiddenField).Value
                If ViewState("listBEMovimientoCuenta") IsNot Nothing Then
                    Dim listBEMovimientoCuenta As List(Of BEMovimientoCuenta) = DirectCast(ViewState("listBEMovimientoCuenta"), List(Of BEMovimientoCuenta))
                    Dim oBEMovimientoCuenta As BEMovimientoCuenta = listBEMovimientoCuenta.First(Function(x) x.NumeroOperacion = CInt(hdfNumeroOperacion))
                    CargarPanelParametro(oBEMovimientoCuenta)
                    pnlDetalleCuenta.Visible = True
                    mpeDetalleCuenta.Show()
                End If
        End Select
    End Sub

    Protected Sub CargarPanelParametro(ByVal oBEMovimientoCuenta As BEMovimientoCuenta)
        Dim Top As Integer = CInt(oBEMovimientoCuenta.IdTipoMovimiento)
        txtNroOperacion.Text = oBEMovimientoCuenta.NumeroOperacion.ToString()
        txtNroOperacion.ReadOnly = True

        txtFecha.Text = oBEMovimientoCuenta.FechaMovimiento.ToString()
        txtFecha.ReadOnly = True

        txtTipoOperacion.Text = oBEMovimientoCuenta.DescTipoMovimiento.ToString()
        txtTipoOperacion.ReadOnly = True

        Select Case Top
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaAOtroCta
                txtCodReferencia.Text = "Desde Cta. " & oBEMovimientoCuenta.CodigoReferencia.ToString()
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaDeOtraCta
                txtCodReferencia.Text = "A Cta. " & oBEMovimientoCuenta.CodigoReferencia.ToString()
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIPDesdePortal
                DivCodRef.Visible = False
                'txtCodReferencia.Text = "Solicitud Pago Nº " & oBEMovimientoCuenta.CodigoReferencia.ToString()
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP
                txtCodReferencia.Text = "CIP Nº " & oBEMovimientoCuenta.CodigoReferencia.ToString()
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.Recarga
                txtCodReferencia.Text = "Cuenta Nº " & oBEMovimientoCuenta.CodigoReferencia.ToString()
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.RetiroDinero
                txtCodReferencia.Text = "Solicitud Nº " & oBEMovimientoCuenta.CodigoReferencia.ToString()
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.Comision
                txtCodReferencia.Text = "Cuenta Comercio N° 00000000000000"
        End Select

        'If (Top = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP) _
        '    Or (Top = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIPDesdePortal) Then
        '    txtCodReferencia.Text = "CIP Nº " & oBEMovimientoCuenta.CodigoReferencia.ToString()
        'ElseIf (Top = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaDeOtraCta) Then
        '    txtCodReferencia.Text = "Desde Cta. " & oBEMovimientoCuenta.CodigoReferencia.ToString()
        'ElseIf (Top = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaAOtroCta) Then
        '    txtCodReferencia.Text = "A Cta. " & oBEMovimientoCuenta.CodigoReferencia.ToString()
        'End If
        txtTitular.Text = oBEMovimientoCuenta.TitularCuenta.ToString()
        txtPortal.Text = oBEMovimientoCuenta.NombrePortal.ToString()
        txtConceptoPago.Text = oBEMovimientoCuenta.ConceptoPago.ToString()
        SetVisibleTipoParametro(Top)
    End Sub

    Protected Sub SetVisibleTipoParametro(ByVal TipoOperacion As Integer)
        Select Case TipoOperacion
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaAOtroCta,
                SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaDeOtraCta
                DivTitular.Visible = False
                DivConceptoPago.Visible = True
                DivPortal.Visible = False
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIPDesdePortal
                DivTitular.Visible = False
                DivConceptoPago.Visible = True
                DivPortal.Visible = True
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP
                DivTitular.Visible = False
                DivConceptoPago.Visible = True
                DivPortal.Visible = False
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.Recarga
                DivTitular.Visible = False
                DivConceptoPago.Visible = True
                DivPortal.Visible = False
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.RetiroDinero
                DivTitular.Visible = True
                DivConceptoPago.Visible = True
                DivPortal.Visible = False
            Case SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.Comision
                DivTitular.Visible = False
                DivConceptoPago.Visible = True
                DivPortal.Visible = False
        End Select




        'If (TipoOperacion = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaAOtroCta) _
        '    Or (TipoOperacion = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaDeOtraCta) Then
        '    DivTitular.Visible = True
        'Else : DivTitular.Visible = False
        'End If
        'If (TipoOperacion = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIPDesdePortal Or TipoOperacion = SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP) Then
        '    DivConceptoPago.Visible = True
        '    DivPortal.Visible = True
        'Else
        '    DivConceptoPago.Visible = False
        '    DivPortal.Visible = False
        'End If
    End Sub

    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        mpeDetalleCuenta.Hide()
    End Sub
End Class
