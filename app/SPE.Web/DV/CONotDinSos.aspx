﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="CONotDinSos.aspx.vb" Inherits="DV_CONotDinSos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Consulta de Movimientos Sospechosos Monedero Electr&oacute;nico</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b&uacute;squeda</h4>
        <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Regla:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlRegla" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Año:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlAnio" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Mes:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMes" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Ejecuci&oacute;n:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEjecucion" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </li>
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
       
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" class="cont_cel">
                <ContentTemplate>
                    <h5 id="h5result" runat="server" visible="false">
                        <asp:Literal ID="lblResultado" runat="server" Text="" />
                    </h5>
                    <asp:HiddenField ID="hdfSortProp" runat="server" />
                    <asp:HiddenField ID="hdfSortDir" runat="server" />
                    <asp:HiddenField ID="hdfTotalRows" runat="server" />
                    <asp:GridView ID="grdResultado" runat="server" BackColor="White" AllowPaging="True"
                        AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" CssClass="grilla" EnableTheming="True" ShowFooter="False" AllowSorting="True"
                        PageSize="10">
                        <Columns>
                            <asp:TemplateField HeaderText="ID" SortExpression="IdResultado">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnIdResultado" CommandName="Detalle" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdResultado")%>'
                                        runat="server" Text='<%#Convert.ToInt32(DataBinder.Eval(Container.DataItem, "IdResultado")).ToString("#00000")%>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NumeroCuenta" HeaderText="N° Cuenta" SortExpression="IdRegistro">
                            </asp:BoundField>
                            <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente">
                            </asp:BoundField>
                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"></asp:BoundField>
                            <asp:BoundField DataField="NroTransferenciaRealizadas" HeaderText="Transferencias Hechas"
                                SortExpression="NroTransferenciaRealizada"></asp:BoundField>
                            <asp:TemplateField HeaderText="Total Transferido" SortExpression="TotalTransferido">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "TotalTransferido")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Button ID="btnDetalle" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="mppDetalle" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="imgbtnRegresar" PopupControlID="pnlDetalle" TargetControlID="btnDetalle">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDetalle" runat="server" Style="display: none;">
                        <div id="divBusquedaRepresentantes" class="divContenedor" style="width: 600px">
                            <div id="div8" class="divContenedorTitulo">
                                <div style="float: left">
                                    Detalle de Transferencias de Dinero Realizadas</div>
                                <div style="float: right">
                                    <asp:ImageButton ID="imgbtnRegresar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>' CausesValidation="false"
                                        runat="server"></asp:ImageButton>
                                </div>
                            </div>
                            <br />
                            <asp:HiddenField ID="hdfSortPropD" runat="server" Value="" />
                            <asp:HiddenField ID="hdIdResultado" runat="server" Value="" />
                            <asp:HiddenField ID="hdfSortDirD" runat="server" Value="" />
                            <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" Width="580px" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True" PageSize="5" EnableTheming="True" ShowFooter="False">
                                <Columns>
                                    <asp:BoundField DataField="IdDetalle" HeaderText="ID" SortExpression="IdDetalle" />
                                    <asp:BoundField DataField="NumeroCuenta" HeaderText="N° Cuenta" SortExpression="NumeroCuenta" />
                                    <asp:BoundField DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" />
                                    <asp:BoundField DataField="Email" HeaderText="E-mail" SortExpression="Email" />
                                    <asp:BoundField DataField="MontoTransferencia" HeaderText="Monto Transferencia" SortExpression="MontoTransferencia" />
                                </Columns>
                                <HeaderStyle CssClass="cabecera" />
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                                <PagerSettings PageButtonCount="5" />
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlEjecucion" EventName="SelectedIndexChanged">
                    </asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="ddlAnio" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlMes" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddlRegla" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="Sorting" />
                    <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="PageIndexChanging" />
                </Triggers>
            </asp:UpdatePanel>
       
    </div>
</asp:Content>
