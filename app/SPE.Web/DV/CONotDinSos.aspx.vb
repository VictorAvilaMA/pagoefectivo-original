﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class DV_CONotDinSos
    Inherits System.Web.UI.Page

    Private _ResultList As New List(Of SPE.Entidades.BEResultadoEjecucionRegla)
    Public Property ResultList() As List(Of SPE.Entidades.BEResultadoEjecucionRegla)
        Get
            Return _ResultList
        End Get
        Set(ByVal value As List(Of SPE.Entidades.BEResultadoEjecucionRegla))
            _ResultList = value
        End Set
    End Property
    Private _ResultDetailList As New List(Of SPE.Entidades.BEDetalleResultadoEjecucion)
    Public Property ResultDetailList() As List(Of SPE.Entidades.BEDetalleResultadoEjecucion)
        Get
            Return _ResultDetailList
        End Get
        Set(ByVal value As List(Of SPE.Entidades.BEDetalleResultadoEjecucion))
            _ResultDetailList = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            grdResultado.PageIndex = 0
            gvResultado.PageIndex = 0
            CargarCombos()

        End If
    End Sub

    Private Sub CargarCombos()
        Dim cDineroVirt As New CDineroVirtual()
        ddlRegla.DataTextField = "AbrevRegla"
        ddlRegla.DataValueField = "IdRegla"
        ddlRegla.DataSource = cDineroVirt.ConsultarReglaMovimientoSospechoso()
        ddlRegla.DataBind()
        If (ddlRegla.Items.Count <= 0) Then
            ddlRegla.Items.Add(New ListItem("::Ninguno::", "0"))
        End If

        ddlAnio.Items.Add("2011")
        For index = 2012 To DateTime.Now.Year
            ddlAnio.Items.Add(index.ToString())
        Next
        ddlAnio.SelectedIndex = 0

        ddlMes.Items.Add(New ListItem("Enero", "1"))
        ddlMes.Items.Add(New ListItem("Febrero", "2"))
        ddlMes.Items.Add(New ListItem("Marzo", "3"))
        ddlMes.Items.Add(New ListItem("Abril", "4"))
        ddlMes.Items.Add(New ListItem("Mayo", "5"))
        ddlMes.Items.Add(New ListItem("Junio", "6"))
        ddlMes.Items.Add(New ListItem("Julio", "7"))
        ddlMes.Items.Add(New ListItem("Agosto", "8"))
        ddlMes.Items.Add(New ListItem("Septiembre", "9"))
        ddlMes.Items.Add(New ListItem("Octubre", "10"))
        ddlMes.Items.Add(New ListItem("Noviembre", "11"))
        ddlMes.Items.Add(New ListItem("Diciembre", "12"))
        ddlMes.SelectedValue = DateTime.Now.Month.ToString()

        CargarComboEjecucion()

        CargarGrilla()

    End Sub

    Private Sub CargarComboEjecucion()
        ddlEjecucion.DataTextField = "FechaEjecucion"
        ddlEjecucion.DataValueField = "IdRegistro"
        ddlEjecucion.DataSource = New CDineroVirtual().ConsultarRegistroEjecucion(Convert.ToInt32(ddlRegla.SelectedValue), Convert.ToInt32(ddlAnio.SelectedValue), Convert.ToInt32(ddlMes.SelectedValue))
        ddlEjecucion.DataBind()

        If (ddlEjecucion.Items.Count <= 0) Then
            ddlEjecucion.Items.Add(New ListItem("::Ninguno::", "0"))
        End If
    End Sub

    Private Sub CargarGrilla()
        Dim oResultado As New BEResultadoEjecucionRegla
        'Dim entidad As New List(Of BEResultadoEjecucionRegla)
        oResultado.IdRegistro = Convert.ToInt64(ddlEjecucion.SelectedValue)

        oResultado.PropOrder = hdfSortProp.Value
        oResultado.PageNumber = grdResultado.PageIndex + 1
        oResultado.PageSize = grdResultado.PageSize
        oResultado.TipoOrder = hdfSortDir.Value.Equals("1")
        'grdResultado.PageIndex = grdResultado.PageIndex - 1
        ResultList = New CDineroVirtual().ConsultarResultadoEjecucionRegla(oResultado)
        If ResultList.Count() > 0 Then
            h5result.Visible = True
        Else : h5result.Visible = False
        End If
        Dim oObjectDataSource As New ObjectDataSource
        oObjectDataSource.ID = "oObjectDataSource_" + grdResultado.ID
        oObjectDataSource.EnablePaging = grdResultado.AllowPaging
        oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
        oObjectDataSource.SelectMethod = "GetDataGrilla"
        oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
        oObjectDataSource.StartRowIndexParameterName = "filaInicio"
        oObjectDataSource.MaximumRowsParameterName = "maxFilas"
        oObjectDataSource.EnableViewState = False

        AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating

        grdResultado.DataSource = oObjectDataSource
        grdResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, If(ResultList.Count = 0, 0, ResultList(0).TotalPageNumbers))


    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        Dim TotalGeneral As Int32
        TotalGeneral = If(ResultList.Count = 0, 0, ResultList(0).TotalPageNumbers)
        e.ObjectInstance = New _3Dev.FW.Web.TotalRegistrosTableAdapter(ResultList, TotalGeneral)
    End Sub

    Private Sub oObjectDataSource_ObjectCreatingDetail(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        Dim TotalGeneral As Int32
        TotalGeneral = If(ResultDetailList.Count = 0, 0, ResultDetailList(0).TotalPageNumbers)
        e.ObjectInstance = New _3Dev.FW.Web.TotalRegistrosTableAdapter(ResultDetailList, TotalGeneral)
    End Sub

    Protected Sub grdResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdResultado.Sorting
        hdfSortProp.Value = e.SortExpression
        hdfSortDir.Value = If(hdfSortDir.Value = "1", "0", "1")
        CargarGrilla()
    End Sub

    Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdResultado.PageIndexChanging
        grdResultado.PageIndex = e.NewPageIndex
        CargarGrilla()
    End Sub

    Protected Sub ddlEjecucion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEjecucion.SelectedIndexChanged
        CargarGrilla()
    End Sub

    Protected Sub ddlMes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMes.SelectedIndexChanged
        CargarComboEjecucion()
        CargarGrilla()
    End Sub

    Protected Sub ddlAnio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAnio.SelectedIndexChanged
        CargarComboEjecucion()
        CargarGrilla()
    End Sub

    Protected Sub ddlRegla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRegla.SelectedIndexChanged
        CargarComboEjecucion()
        CargarGrilla()
    End Sub

    Protected Sub grdResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdResultado.RowCommand
        If (e.CommandName.Equals("Detalle")) Then


            hdIdResultado.Value = e.CommandArgument
            cargarDetalle(hdIdResultado.Value)
            btnDetalle_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetalle.Click
        mppDetalle.Show()
    End Sub


    Sub cargarDetalle(ByVal idResultado As Long)
        Dim oResultado As New BEDetalleResultadoEjecucion

        '

        oResultado.IdResultado = Convert.ToInt64(idResultado)

        oResultado.PropOrder = hdfSortPropD.Value
        oResultado.PageNumber = gvResultado.PageIndex + 1
        oResultado.PageSize = gvResultado.PageSize
        oResultado.TipoOrder = hdfSortDirD.Value.Equals("1")
        'grdResultado.PageIndex = grdResultado.PageIndex - 1
        ResultDetailList = New CDineroVirtual().ConsultarDetalleResultadoEjecucion(oResultado)

        Dim oObjectDataSource As New ObjectDataSource
        oObjectDataSource.ID = "oObjectDataSource_" + gvResultado.ID
        oObjectDataSource.EnablePaging = gvResultado.AllowPaging
        oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
        oObjectDataSource.SelectMethod = "GetDataGrilla"
        oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
        oObjectDataSource.StartRowIndexParameterName = "filaInicio"
        oObjectDataSource.MaximumRowsParameterName = "maxFilas"
        oObjectDataSource.EnableViewState = False

        AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreatingDetail

        '
        gvResultado.DataSource = oObjectDataSource
        gvResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, If(ResultDetailList.Count = 0, 0, ResultDetailList(0).TotalPageNumbers))
        mppDetalle.Show()
    End Sub


    'Protected Sub gvResultado_Sorting1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvResultado.Sorting
    '    Dim SortDirhf As Integer
    '    SortDirhf = CInt(hdfDetalle.Value)
    '    If (SortDirhf.Equals(SortDirection.Ascending)) Then
    '        SortDirhf = SortDirection.Descending
    '        hdfDetalle.Value = 1
    '    Else
    '        SortDirhf = SortDirection.Ascending
    '        hdfDetalle.Value = 0
    '    End If
    '    cargarDetalle(SortDirhf, e.SortExpression, CInt(hdIdResultado.Value))
    'End Sub


    'Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdResultado.PageIndexChanging
    '    gvResultado.PageIndex = e.NewPageIndex
    '    hdfPageR.Value = gvResultado.PageIndex
    '    CargarGrilla(SortDirection.Descending, String.Empty, CInt(hdfPageR.Value))
    'End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvResultado.Sorting
        hdfSortPropD.Value = e.SortExpression
        hdfSortDirD.Value = If(hdfSortDirD.Value = "1", "0", "1")
        cargarDetalle(hdIdResultado.Value)
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvResultado.PageIndexChanging
        gvResultado.PageIndex = e.NewPageIndex
        cargarDetalle(hdIdResultado.Value)
    End Sub
End Class
