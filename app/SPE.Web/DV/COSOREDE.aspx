﻿<%@ Page Language="VB" Async="true" EnableEventValidation="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COSOREDE.aspx.vb" Inherits="DV_Cosorede" Title="PagoEfectivo - Consultar Solicitud de Retiro" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="Literal1" runat="server" Text="Detalle solicitud de retiro de dinero"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pnlPage" runat="server">
            <fieldset>
                <asp:Panel ID="pnlInformacionRegistro" CssClass="inner-col-right" runat="server">
                    <asp:UpdatePanel ID="UpdatePanelDatos" runat="server">
                        <ContentTemplate>
                            <h4>
                                1. Datos del Cliente</h4>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> Solicitante :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtSolicitante" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> Correo :</li>
                                <li class="t2">
                                    <asp:TextBox ID="txtEMail" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <div style="clear: both;" />
                            <h4>
                                2. Cuenta PagoEfectivo</h4>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> N° Cuenta a Retirar :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtNumCuentRetirar" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Saldo :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtSaldo" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> Moneda :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtTipoMonedaSolicitada" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <div style="clear: both;" />
                            <h4>
                                3. Datos de la Solicitud</h4>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> N° Solicitud :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtNumeroSolicitud" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Monto Solicitado :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtMontoSolicitado" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> Fecha Solicitada :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtFechaSolicitud" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Estado Solicitud :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtEstadoSolicitud" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <div style="clear: both;" />
                            <h4>
                                3.1 Cuenta Bancaria Dep&oacute;sito</h4>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtBancoDestino" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Moneda :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtMonedaDestino" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip4">
                                <li class="t1"><span class="color">&gt;&gt;</span> Cuenta destino :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtCuentaDestino" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Titular :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtNombreTitularDestino" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <div style="clear: both;" />
                            <h4 id="Ad0" runat="server">
                                <asp:Literal ID="Ad01" runat="server">
                                                            3.2 Informaci&oacute;n Administador
                                </asp:Literal>
                            </h4>
                            <ul class="datos_cip4" runat="server" id="Ad1">
                                <li class="t1"><span class="color">&gt;&gt;</span> Fecha Aprobaci&oacute;n :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtFechaAprobacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Usuario Aprobaci&oacute;n :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtUsuarioAprobacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip4" runat="server" id="Ad2">
                                <li class="t1"><span class="color">&gt;&gt;</span> Fecha Rechazo :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtFechaRechazo" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Usuario Rechazo :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtUsuarioRechazo" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip5" runat="server" id="Ad3">
                                <li class="t1"><span class="color">&gt;&gt;</span> Observaci&oacute;n :</li>
                                <li class="t3">
                                    <asp:TextBox ID="TxtObservacionAdministrador" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <cc1:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server" targetcontrolid="TxtObservacionAdministrador"
                                        validchars="0123456789qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNM,.- ">
                                    </cc1:filteredtextboxextender>
                                    <asp:Label ID="lblmensajeAdmin" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </li>
                            </ul>
                            <div style="clear: both;" />
                            <h4 id="Ts0" runat="server">
                                <asp:Literal ID="Ts01" runat="server">
                                        3.3 Informaci&oacute;n Tesorer&iacute;a
                                </asp:Literal>
                            </h4>
                            <ul class="datos_cip4" runat="server" id="Ts1">
                                <li class="t1"><span class="color">&gt;&gt;</span> Fecha Liquidaci&oacute;n :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtFechaLiquidacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Usuario Liquidaci&oacute;n :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtUsuarioLiquidacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip4" runat="server" id="Ts2">
                                <li class="t1"><span class="color">&gt;&gt;</span> Fecha Anulaci&oacute;n :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtFechaAnulacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Usuario Anulaci&oacute;n :</li>
                                <li class="t2">
                                    <asp:TextBox ID="TxtUsuarioAnulacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox></li>
                            </ul>
                            <ul class="datos_cip5" runat="server" id="Ts3">
                                <li class="t1"><span class="color">&gt;&gt;</span> Observaci&oacute;n :</li>
                                <li class="t3">
                                    <asp:TextBox ID="TxtObservacionTesoreria" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <cc1:filteredtextboxextender id="FilteredTextBoxExtender2" runat="server" targetcontrolid="TxtObservacionTesoreria"
                                        validchars="0123456789qwertyuiopasdfghjklñzxcvbnmQWERTYUIOPASDFGHJKLÑZXCVBNM,.- ">
                                    </cc1:filteredtextboxextender>
                                    <asp:Label ID="lblmensajeTes" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                </li>
                            </ul>
                            <div style="clear: both;" />
                            <h6 id="Ia0" runat="server">
                                <asp:Literal ID="Ia01" runat="server">
                                        3.3.1 Datos de la Cuenta PagoEfectivo
                                </asp:Literal>
                            </h6>
                            <ul class="datos_cip2" runat="server" id="Ia1">
                                <li class="t1"><span class="color">&gt;&gt;</span> N° Cuenta :</li>
                                <li class="t2">
                                    <asp:DropDownList ID="ddlNumeroCuenta" runat="server" CssClass="normal">
                                    </asp:DropDownList>
                                </li>
                                <li class="t1"><span class="color">&gt;&gt;</span> Documento Adjunto :</li>
                                <li class="t2">
                                    <asp:FileUpload ID="TxtDocAdjunto" runat="server" CssClass="normal" Width="453px" />
                                    <asp:Button ID="LnkFile" Text="Descargar" runat="server" Visible="false"></asp:Button>
                                    <asp:HyperLink ID="lnkURL" Text="Ver Archivo" runat="server" Visible="false"></asp:HyperLink>
                                </li>
                            </ul>
                            <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl="~/img/closeX.GIF"
                                Style="display: none;" CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                            <asp:Label ID="lblTransaccion" runat="server" Visible="False" Width="650px" CssClass="MensajeTransaccion"
                                Style="padding-bottom: 5px;padding-left:200px"></asp:Label>
                            <%--<div class="cont_cel2">--%>
                            <ul class="datos_cip5">
                                <li class="t3">
                                    <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" Visible="true" OnClientClick="return ConfirmMe();"
                                        ValidationGroup="IsNullOrEmpty" CssClass="input_azul3"></asp:Button>
                                    <asp:Button ID="btnLiquidar" runat="server" Text="Liquidar" Visible="false" OnClientClick="return ConfirmMe();"
                                        ValidationGroup="IsNullOrEmpty" CssClass=""></asp:Button>
                                    <asp:Button ID="btnRechazar" runat="server" Text="Rechazar" Visible="true" OnClientClick="return ConfirmMe();"
                                        ValidationGroup="IsNullOrEmpty" CssClass="input_azul3"></asp:Button>
                                    
                                    <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="input_azul3"></asp:Button>
                                    <asp:Button ID="BtnCancelar" runat="server" Text="Regresar" Visible="true" OnClientClick="return ConfirmMe();"
                                        ValidationGroup="IsNullOrEmpty" CssClass="input_azul4"></asp:Button>
                                </li>
                            </ul>
                            <%--</div>--%>
                            <asp:HiddenField ID="hdnIdCuenta" runat="server" />
                            &nbsp;
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnExcel" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
                &nbsp;
            </fieldset>
        </asp:Panel>
    </div>
</asp:Content>
