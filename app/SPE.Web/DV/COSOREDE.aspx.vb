﻿Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web

Partial Class DV_Cosorede
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CDineroVirtual)



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            hdnIdCuenta.Value = Convert.ToInt64(MaintenanceKeyValue)
            CargarSolicitudRetiro()

        End If
    End Sub

    Private Sub CargarSolicitudRetiro()

        TxtObservacionAdministrador.Attributes.Add("onkeypress", " ValidarCaracteres(this, 200, $('#" + lblmensajeAdmin.ClientID + "'));")
        TxtObservacionAdministrador.Attributes.Add("onkeyup", " ValidarCaracteres(this, 200, $('#" + lblmensajeAdmin.ClientID + "'));")

        TxtObservacionTesoreria.Attributes.Add("onkeypress", " ValidarCaracteres(this, 200, $('#" + lblmensajeTes.ClientID + "'));")
        TxtObservacionTesoreria.Attributes.Add("onkeyup", " ValidarCaracteres(this, 200, $('#" + lblmensajeTes.ClientID + "'));")

        Dim proxy As New SPE.Web.CDineroVirtual
        Dim entidad = New BESolicitudRetiroDineroVirtual()
        entidad.IdSolicitud = hdnIdCuenta.Value
        entidad = proxy.ConsultarSolicitudRetiroMonederoById(entidad)
        ViewState("Solicitud") = entidad

        Me.txtEMail.Text = entidad.Email
        Me.TxtFechaAprobacion.Text = IIf(entidad.FechaAprobacion = DateTime.MinValue, "-", entidad.FechaAprobacion)
        Me.TxtFechaRechazo.Text = IIf(entidad.FechaRechazo = DateTime.MinValue, "-", entidad.FechaRechazo)

        Me.TxtUsuarioAprobacion.Text = IIf(entidad.UsuarioAprobacion.Equals(String.Empty), "-", entidad.UsuarioAprobacion)
        Me.TxtUsuarioRechazo.Text = IIf(entidad.UsuarioRechazo.Equals(String.Empty), "-", entidad.UsuarioRechazo)

        Me.TxtFechaSolicitud.Text = entidad.FechaSolicitud
        Me.TxtNumCuentRetirar.Text = entidad.Numero
        TxtSaldo.Text = entidad.MonedaSimbolo + " " + entidad.Saldo.ToString("#0.00")

        Me.TxtTipoMonedaSolicitada.Text = entidad.MonedaDescripcion
        Me.TxtSolicitante.Text = entidad.ClienteNombre
        'Me.TxtEstadoCuenta.Text = entidad.EstadoCuenta 
        Me.TxtNumeroSolicitud.Text = entidad.IdSolicitud
        Me.TxtMontoSolicitado.Text = entidad.InfoSolicitud
        Me.TxtEstadoSolicitud.Text = entidad.EstadoDescripcion
        Me.TxtBancoDestino.Text = entidad.BancoDestinoDescripcion
        Me.TxtCuentaDestino.Text = entidad.CuentaBancariaDestino
        Me.TxtNombreTitularDestino.Text = entidad.TitularCuentaBancariaDestino
        Me.TxtMonedaDestino.Text = entidad.MonedaDescripcion
        Me.TxtObservacionAdministrador.Text = entidad.ObservacionAdministrador

        Me.TxtFechaAnulacion.Text = IIf(entidad.FechaAnulacion = DateTime.MinValue, "", entidad.FechaAnulacion)
        Me.TxtUsuarioAnulacion.Text = IIf(entidad.UsuarioAnulacion.Equals(String.Empty), "", entidad.UsuarioAnulacion)
        Me.TxtFechaLiquidacion.Text = IIf(entidad.FechaLiquidacion = DateTime.MinValue, "", entidad.FechaLiquidacion)
        Me.TxtUsuarioLiquidacion.Text = IIf(entidad.UsuarioLiquidacion.Equals(String.Empty), "", entidad.UsuarioLiquidacion)
        Me.TxtObservacionTesoreria.Text = entidad.ObservacionTesoreria

        'LnkFile.Text = entidad.NombreInicialDocAdj
        'LnkFile.NavigateUrl = FTPTesoreria + entidad.NombreDocAdjFtp
        'lblmientras.Text = entidad.NombreInicialDocAdj & " - " & FTPTesoreria & entidad.NombreDocAdjFtp

        Dim cuentas As List(Of BECuentaBanco) = New CBanco().ConsultarCuentaBanco(New BECuentaBanco())
        'For Each x As BECuentaBanco In cuentas
        '    x.IdCuentaBanco

        'Next

        WebUtil.DropDownlistBinding(ddlNumeroCuenta, cuentas, "CustomNombre", "IdCuentaBanco", "::: Seleccione :::")

        Ad1.Visible = True
        Ad2.Visible = True
        Ad3.Visible = True

        Ts1.Visible = True
        Ts2.Visible = True
        Ts3.Visible = True

        Ia1.Visible = True
        'ia2.Visible = True

        If entidad.NumeroCuentaComercioId = 0 Then
            ddlNumeroCuenta.SelectedValue = ""
        Else
            ddlNumeroCuenta.SelectedValue = entidad.NumeroCuentaComercioId
        End If




        Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)
        Dim a As _3Dev.FW.Entidades.Seguridad.BEUserInfo

        '                Administrador	Tesorero	User
        'Solicitado	    Aprob/Rech	    Ver	        Ver
        'Aprobado	    Ver	            Liq/Anul	Ver
        'Rechazado	    Ver	            Ver	        Ver
        'Liquidado	    Ver	            Ver	        Ver
        'Anulado	        Ver	            Ver	        Ver
        Select Case Perfil
            Case SPE.EmsambladoComun.ParametrosSistema.RolJefeProducto
                btnAprobar.Visible = False
                btnRechazar.Visible = False
                Literal1.Text = "Detalle de la Solicitud de Retiro"

                Ad0.Visible = False
                Ad1.Visible = False
                Ad1.Visible = False
                Ad2.Visible = False
                Ad3.Visible = False

                Ts0.Visible = False
                Ts1.Visible = False
                Ts2.Visible = False
                Ts3.Visible = False

                Ia0.Visible = False
                Ia1.Visible = False
                'ia2.Visible = False
            Case SPE.EmsambladoComun.ParametrosSistema.RolCliente
                Select Case entidad.IdEstado
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Solicitado, _
                        SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Aprobado, _
                        SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Rechazado, _
                        SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Liquidado, _
                        SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Anulado
                        btnAprobar.Visible = False
                        btnExcel.CssClass = "input_azul3"
                        BtnCancelar.CssClass = "input_azul4"
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"

                        Ad0.Visible = False
                        Ad1.Visible = False
                        Ad1.Visible = False
                        Ad2.Visible = False
                        Ad3.Visible = False

                        Ts0.Visible = False
                        Ts1.Visible = False
                        Ts2.Visible = False
                        Ts3.Visible = False

                        Ia0.Visible = False
                        Ia1.Visible = False
                        'ia2.Visible = False
                End Select
            Case SPE.EmsambladoComun.ParametrosSistema.RolAdminMonederoElectronico
                ddlNumeroCuenta.Enabled = False
                Select Case entidad.IdEstado
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Solicitado
                        btnAprobar.Visible = True
                        btnRechazar.Visible = True
                        btnExcel.CssClass = "input_azul4"
                        BtnCancelar.CssClass = "input_azul4"
                        btnAprobar.CssClass = "input_azul4"
                        btnRechazar.CssClass = "input_azul4"
                        Literal1.Text = "Aprobar/Rechazar solicitud de Retiro"
                        Ts0.Visible = False
                        Ts1.Visible = False
                        Ts2.Visible = False
                        Ts3.Visible = False

                        Ia0.Visible = False
                        Ia1.Visible = False
                        'ia2.Visible = False
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Aprobado, _
                        SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Rechazado
                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"
                        btnExcel.CssClass = "input_azul3"
                        BtnCancelar.CssClass = "input_azul4"
                        Ts0.Visible = False
                        Ts1.Visible = False
                        Ts2.Visible = False
                        Ts3.Visible = False

                        Ia0.Visible = False
                        Ia1.Visible = False
                        'ia2.Visible = False
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Liquidado
                        btnAprobar.Visible = False
                        btnExcel.CssClass = "input_azul3"
                        BtnCancelar.CssClass = "input_azul4"
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"
                        TxtObservacionAdministrador.Enabled = False
                        LnkFile.Visible = True
                        TxtDocAdjunto.Visible = False
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Anulado
                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"
                        btnExcel.CssClass = "input_azul3"
                        BtnCancelar.CssClass = "input_azul4"
                        TxtDocAdjunto.Visible = False
                        LnkFile.Visible = False
                End Select
            Case SPE.EmsambladoComun.ParametrosSistema.RolTesoreria
                Select Case entidad.IdEstado
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Solicitado
                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"
                        btnExcel.CssClass = "input_azul3"
                        BtnCancelar.CssClass = "input_azul4"
                        Ad0.Visible = False
                        Ad1.Visible = False
                        Ad2.Visible = False
                        Ad3.Visible = False


                        Ts0.Visible = False
                        Ts1.Visible = False
                        Ts2.Visible = False
                        Ts3.Visible = False

                        Ia0.Visible = False
                        Ia1.Visible = False
                        'ia2.Visible = False
                        ddlNumeroCuenta.Enabled = False
                        TxtObservacionAdministrador.Enabled = False
                        TxtObservacionTesoreria.Enabled = False

                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Aprobado

                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"
                        btnExcel.CssClass = "input_azul4"
                        BtnCancelar.CssClass = "input_azul4"
                        btnLiquidar.CssClass = "input_azul4"
                        btnRechazar.CssClass = "input_azul4"
                        btnAprobar.Visible = False
                        btnLiquidar.Visible = True
                        btnRechazar.Visible = True
                        ddlNumeroCuenta.Enabled = True

                        TxtObservacionAdministrador.Enabled = False
                        TxtObservacionTesoreria.Enabled = True

                        TxtDocAdjunto.Visible = True
                        LnkFile.Visible = False

                        Literal1.Text = "Liquidar/Anular solicitud de creación de cuenta"
                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Rechazado
                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        btnLiquidar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"

                        Ts0.Visible = False
                        Ts1.Visible = False
                        Ts2.Visible = False
                        Ts3.Visible = False

                        Ia0.Visible = False
                        Ia1.Visible = False
                        'ia2.Visible = False
                        ddlNumeroCuenta.Enabled = False

                        TxtDocAdjunto.Visible = False
                        LnkFile.Visible = False

                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Liquidado
                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        Literal1.Text = "Detalle de la Solicitud de Retiro"
                        TxtObservacionTesoreria.Enabled = False
                        TxtObservacionAdministrador.Enabled = False
                        LnkFile.Visible = True
                        TxtDocAdjunto.Visible = False
                        btnLiquidar.Visible = False


                    Case SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Anulado
                        btnAprobar.Visible = False
                        btnRechazar.Visible = False
                        btnLiquidar.Visible = False

                        Literal1.Text = "Detalle de la Solicitud de Retiro"

                        ddlNumeroCuenta.Enabled = False
                        TxtObservacionAdministrador.Enabled = False
                        TxtObservacionTesoreria.Enabled = False

                        TxtDocAdjunto.Visible = False
                        LnkFile.Visible = True
                End Select
                DescargarArchivo()
        End Select





    End Sub

    Protected Sub btnAprobar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAprobar.Click
        Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)

        Select Case Perfil
            Case SPE.EmsambladoComun.ParametrosSistema.RolCliente
            Case SPE.EmsambladoComun.ParametrosSistema.RolAdminMonederoElectronico

                lblTransaccion.Visible = True
                If Actualizar(SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Aprobado) Then
                    lblTransaccion.Text = "Usted Aprobó la Solicitud de Retiro de Dinero"
                Else
                    lblTransaccion.Text = "La Operación no se completo"
                End If
        End Select

    End Sub

    Protected Sub btnRechazar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRechazar.Click
        lblTransaccion.Visible = True
        Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)
        Select Case Perfil
            Case SPE.EmsambladoComun.ParametrosSistema.RolCliente
            Case SPE.EmsambladoComun.ParametrosSistema.RolAdminMonederoElectronico
                If Actualizar(SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Rechazado) Then
                    lblTransaccion.Text = "Usted Rechazó la Solicitud de Retiro de Dinero"
                Else
                    lblTransaccion.Text = "La Operación no se completo"
                End If
            Case SPE.EmsambladoComun.ParametrosSistema.RolTesoreria

                If Actualizar(SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Anulado) Then
                    lblTransaccion.Text = "Usted Anuló la Solicitud de Retiro de Dinero"
                Else
                    lblTransaccion.Text = "La Operación no se completo"
                End If

        End Select
    End Sub

    Private Function Actualizar(ByVal estado As Integer) As Boolean
        Dim proxy As New SPE.Web.CDineroVirtual
        Dim entidad = New BESolicitudRetiroDineroVirtual()

        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
            entidad.IdUsuario = ClienteUsuarioID
        End If

        entidad.IdSolicitud = hdnIdCuenta.Value
        entidad.ObservacionAdministrador = TxtObservacionAdministrador.Text
        entidad.ObservacionTesoreria = TxtObservacionTesoreria.Text
        entidad.NombreInicialDocAdj = TxtDocAdjunto.FileName

        Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)

        If Perfil = SPE.EmsambladoComun.ParametrosSistema.RolTesoreria And TxtDocAdjunto.FileName.Length > 0 Then
            Dim nombreFile As String
            Dim ftp As New UtilManagerFTP
            Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
            Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
            Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")

            nombreFile = DateTime.Today.Year.ToString() + "_" + DateTime.Today.Month.ToString() + "_" + DateTime.Today.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + TxtDocAdjunto.FileName

            ftp.UploadFile(TxtDocAdjunto.FileContent, FTPTesoreria + nombreFile, FTPUser, FTPPass)
            entidad.NombreDocAdjFtp = nombreFile
        End If


        If ddlNumeroCuenta.SelectedValue.ToString() = "" Then
            entidad.NumeroCuentaComercioId = 0
        Else
            entidad.NumeroCuentaComercioId = ddlNumeroCuenta.SelectedValue
        End If


        'entidad.Observacion = TxtObservacion.Text
        entidad.IdEstado = estado

        If proxy.ActualizarSolicitudRetiroDinero(entidad) Then
            CargarSolicitudRetiro()
            Return True
        Else
            Return False
        End If



    End Function



    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Response.Redirect("COSORETDIN.aspx")
    End Sub


    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else


            Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)
            'Response.Redirect(String.Format("~/Reportes/RptCosorede.aspx?IdCue={0}&P={1}", hdnIdCuenta.Value, Perfil))
            Dim proxy As New SPE.Web.CDineroVirtual
            Dim entidad = New BESolicitudRetiroDineroVirtual()
            entidad.IdSolicitud = hdnIdCuenta.Value
            entidad = proxy.ConsultarSolicitudRetiroMonederoById(entidad)
            Dim Email As String = entidad.Email
            Dim FechaAprob As String = IIf(entidad.FechaAprobacion = DateTime.MinValue, "-", entidad.FechaAprobacion)
            Dim FechaRechazo As String = IIf(entidad.FechaRechazo = DateTime.MinValue, "-", entidad.FechaRechazo)
            Dim UsuarioAprobacion As String = IIf(entidad.UsuarioAprobacion.Equals(String.Empty), "-", entidad.UsuarioAprobacion)
            Dim UsuarioRechazo As String = IIf(entidad.UsuarioRechazo.Equals(String.Empty), "-", entidad.UsuarioRechazo)
            Dim FechaSolicitud As String = entidad.FechaSolicitud
            Dim Numero As String = "Nro." & entidad.Numero
            Dim Saldo As String = entidad.MonedaSimbolo + " " + entidad.Saldo.ToString("#0.00")
            Dim MonedaDescripcion As String = entidad.MonedaDescripcion
            Dim ClienteNombre As String = entidad.ClienteNombre
            'Me.TxtEstadoCuenta.Text = entidad.EstadoCuenta 
            Dim IdSolicitud As String = entidad.IdSolicitud
            Dim InfoSolicitud As String = entidad.InfoSolicitud
            Dim EstadoDescripcion As String = entidad.EstadoDescripcion
            Dim BancoDestinoDescripcion As String = entidad.BancoDestinoDescripcion
            Dim CuentaBancariaDestino As String = "Nro." & entidad.CuentaBancariaDestino
            Dim TitularCuentaBancariaDestino As String = entidad.TitularCuentaBancariaDestino
            Dim MonedaDestino As String = entidad.MonedaDescripcion
            Dim ObservacionAdministrador As String = entidad.ObservacionAdministrador
            Dim FechaAnulacion As String = IIf(entidad.FechaAnulacion = DateTime.MinValue, "-", entidad.FechaAnulacion)
            Dim UsuarioAnulacion As String = IIf(entidad.UsuarioAnulacion.Equals(String.Empty), "-", entidad.UsuarioAnulacion)
            Dim FechaLiquidacion As String = IIf(entidad.FechaLiquidacion = DateTime.MinValue, "-", entidad.FechaLiquidacion)
            Dim UsuarioLiquidacion As String = IIf(entidad.UsuarioLiquidacion.Equals(String.Empty), "-", entidad.UsuarioLiquidacion)
            Dim ObservacionTesoreria As String = entidad.ObservacionTesoreria

            Dim lista As New List(Of BESolicitudRetiroDineroVirtual)
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Solicitante", ClienteNombre))
            parametros.Add(New KeyValuePair(Of String, String)("Email", Email))
            parametros.Add(New KeyValuePair(Of String, String)("Cuenta", Numero))
            parametros.Add(New KeyValuePair(Of String, String)("MonedaCuenta", MonedaDescripcion))
            parametros.Add(New KeyValuePair(Of String, String)("SaldoCuenta", Saldo))
            parametros.Add(New KeyValuePair(Of String, String)("Solicitud", IdSolicitud))
            parametros.Add(New KeyValuePair(Of String, String)("FechaSolicitud", FechaSolicitud))
            parametros.Add(New KeyValuePair(Of String, String)("MontoSolicitud", InfoSolicitud))
            parametros.Add(New KeyValuePair(Of String, String)("EstadoSolicitud", EstadoDescripcion))
            parametros.Add(New KeyValuePair(Of String, String)("Titular", TitularCuentaBancariaDestino))
            parametros.Add(New KeyValuePair(Of String, String)("Banco", BancoDestinoDescripcion))
            parametros.Add(New KeyValuePair(Of String, String)("CuentaDestino", CuentaBancariaDestino))
            parametros.Add(New KeyValuePair(Of String, String)("MonedaBanco", MonedaDescripcion))
          
            Select Case Perfil
                Case "Cliente"
                    UtilReport.ProcesoExportarGenerico(lista, Page, "EXCEL", "xls", "Solicitud de retiro de dinero - ", parametros, "RptCosoredeC.rdlc")
                Case "Administrador"

                    parametros.Add(New KeyValuePair(Of String, String)("FechaAprobacion", FechaAprob))
                    parametros.Add(New KeyValuePair(Of String, String)("UsuarioAprobacion", UsuarioAprobacion))
                    parametros.Add(New KeyValuePair(Of String, String)("UsuarioRechazo", UsuarioRechazo))
                    parametros.Add(New KeyValuePair(Of String, String)("FechaRechazo", FechaRechazo))
                    parametros.Add(New KeyValuePair(Of String, String)("ObservacionA", ObservacionAdministrador))

                    UtilReport.ProcesoExportarGenerico(lista, Page, "EXCEL", "xls", "Solicitud de retiro de dinero - ", parametros, "RptCosoredeA.rdlc")
                Case "Tesoreria"

                    parametros.Add(New KeyValuePair(Of String, String)("FechaAprobacion", FechaAprob))
                    parametros.Add(New KeyValuePair(Of String, String)("UsuarioAprobacion", UsuarioAprobacion))
                    parametros.Add(New KeyValuePair(Of String, String)("UsuarioRechazoA", UsuarioRechazo))
                    parametros.Add(New KeyValuePair(Of String, String)("FechaRechazoA", FechaRechazo))
                    parametros.Add(New KeyValuePair(Of String, String)("ObservacionA", ObservacionAdministrador))
                    parametros.Add(New KeyValuePair(Of String, String)("UsuarioLiquidacion", UsuarioLiquidacion))
                    parametros.Add(New KeyValuePair(Of String, String)("FechaLiquidacion", FechaLiquidacion))
                    parametros.Add(New KeyValuePair(Of String, String)("UsuarioRechazoT", UsuarioAnulacion))
                    parametros.Add(New KeyValuePair(Of String, String)("FechaRechazoT", FechaAnulacion))
                    parametros.Add(New KeyValuePair(Of String, String)("ObservacionT", ObservacionTesoreria))
                    parametros.Add(New KeyValuePair(Of String, String)("NroCuenta", Me.ddlNumeroCuenta.SelectedItem.ToString))
                    parametros.Add(New KeyValuePair(Of String, String)("DocAdjunto", entidad.NombreDocAdjFtp))
                    UtilReport.ProcesoExportarGenerico(lista, Page, "EXCEL", "xls", "Solicitud de retiro de dinero - ", parametros, "RptCosoredeT.rdlc")
            End Select
        






        End If
    End Sub

    Protected Sub LnkFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkFile.Click
        Dim entidad = New BESolicitudRetiroDineroVirtual()
        Dim ftp As New UtilManagerFTP
        'Dim archivo As Byte()

        Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
        Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
        Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")
        Dim pathCurrent As String
        pathCurrent = Server.MapPath("~/")
        Dim raiz As String
        raiz = pathCurrent & "TempoFiles"

        entidad = DirectCast(ViewState("Solicitud"), BESolicitudRetiroDineroVirtual)

        Try
            ftp.DescargarArchivo(raiz, entidad.NombreDocAdjFtp, FTPTesoreria, FTPUser, FTPPass)
            LnkFile.Visible = False

            If Not IsNothing(entidad) Then
                lnkURL.Text = entidad.NombreInicialDocAdj
                'lnkURL.NavigateUrl = FTPTesoreria + entidad.NombreDocAdjFtp
                lnkURL.NavigateUrl = "~/TempoFiles\" & entidad.NombreDocAdjFtp

                lnkURL.Visible = True
            End If
        Catch ex As Exception
            lnkURL.Text = "Archivo no disponible"
            lnkURL.NavigateUrl = ""
            lnkURL.Visible = True
            LnkFile.Visible = False
        End Try

    End Sub

    Protected Sub btnLiquidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLiquidar.Click
        Dim Perfil As String = (CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol)

        Select Case Perfil
            Case SPE.EmsambladoComun.ParametrosSistema.RolTesoreria

                If ddlNumeroCuenta.SelectedValue = "" Then
                    lblTransaccion.Visible = True
                    lblTransaccion.Text = "Usted debe de seleccionar un banco"
                Else
                    lblTransaccion.Visible = True
                    If Actualizar(SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Liquidado) Then
                        lblTransaccion.Text = "Usted Liquidó la Solicitud de Retiro de Dinero"
                    Else
                        lblTransaccion.Text = "La Operación no se completo"
                    End If
                End If
        End Select
    End Sub
    Private Sub DescargarArchivo()
        Dim entidad = New BESolicitudRetiroDineroVirtual()
        Dim ftp As New UtilManagerFTP
        'Dim archivo As Byte()

        Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
        Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
        Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")
        Dim pathCurrent As String
        pathCurrent = Server.MapPath("~/")
        Dim raiz As String
        raiz = pathCurrent & "TempoFiles"

        entidad = DirectCast(ViewState("Solicitud"), BESolicitudRetiroDineroVirtual)

        Try
            ftp.DescargarArchivo(raiz, entidad.NombreDocAdjFtp, FTPTesoreria, FTPUser, FTPPass)
            LnkFile.Visible = False

            If Not IsNothing(entidad) Then
                lnkURL.Text = entidad.NombreInicialDocAdj
                'lnkURL.NavigateUrl = FTPTesoreria + entidad.NombreDocAdjFtp
                lnkURL.NavigateUrl = "~/TempoFiles\" & entidad.NombreDocAdjFtp

                lnkURL.Visible = True
            End If
        Catch ex As Exception
            lnkURL.Text = "Archivo no disponible"
            lnkURL.NavigateUrl = ""
            lnkURL.Visible = True
            LnkFile.Visible = False
        End Try
    End Sub
End Class
