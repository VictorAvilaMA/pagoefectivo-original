<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COSORETDIN.aspx.vb" Inherits="COSORETDIN" Title="PagoEfectivo - Mis solicitudes de retiro de dinero" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Literal runat="server" ID="LblTitulo"> Mis Solicitudes</asp:Literal>
    </h2>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <style type="text/css">
        .conten_pasos3
        {
            z-index: 10;
        }
        #upnlCriterios datos_cip2 t2 input_cal
        {
            position: relative;
            z-index: 10;
        }
        #ctl00_ContentPlaceHolder1_upnlCriterios .datos_cip2
        {
            z-index: 900;
        }
        #ctl00_ContentPlaceHolder1_upnlCriterios #ctl00_ContentPlaceHolder1_divcliente
        {
            z-index: 10;
            position: relative;
        }
        #ctl00_ContentPlaceHolder1_upnlCriterios #ctl00_ContentPlaceHolder1_divcliente ul
        {
            z-index: 10;
        }
        #ctl00_ContentPlaceHolder1_upnlCriterios #ctl00_ContentPlaceHolder1_divcliente ul li.t2
        {
            z-index: 10;
        }
        #ctl00_ContentPlaceHolder1_upnlCriterios #ctl00_ContentPlaceHolder1_divcliente ul li.t2 input
        {
            position: relative;
            z-index: 10;
        }
    </style>
    <%--<asp:HiddenField ID="hdRolName" runat="server" />
    <asp:HiddenField ID="hdUsuarioId" runat="server" />--%>
    <div class="conten_pasos3">
        <h4>
            Criterios de b&uacute;squeda</h4>
        <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha del: </li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDesde" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDesde" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDesde" runat="server" TargetControlID="txtFechaDesde"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaHasta" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaHasta" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderHasta" runat="server" TargetControlID="txtFechaHasta"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <cc1:CalendarExtender ID="CalendarExtenderDesde" runat="server" TargetControlID="txtFechaDesde"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnFechaDesde">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderHasta" runat="server" TargetControlID="txtFechaHasta"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnFechaHasta">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDesde" runat="server" ControlExtender="MaskedEditExtenderDesde"
                            ControlToValidate="txtFechaDesde" ErrorMessage="*" InvalidValueMessage="Fecha Desde no v�lida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha Desde es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorHasta" runat="server" ControlExtender="MaskedEditExtenderHasta"
                            ControlToValidate="txtFechaHasta" ErrorMessage="*" InvalidValueMessage="Fecha Hasta no v�lida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha Hasta es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="covFecha" runat="server" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                            ControlToCompare="txtFechaHasta" ControlToValidate="txtFechaDesde" Operator="LessThanEqual"
                            Type="Date">*</asp:CompareValidator>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>N&uacute;mero Cuenta: </li>
                    <li class="t2">
                        <asp:TextBox ID="TxtNumeroCuenta" runat="server" CssClass="normal" MaxLength="14">
                        </asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="fteTxtNumeroCuenta" runat="server" ValidChars="0123456789"
                            TargetControlID="TxtNumeroCuenta">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                </ul>
                <div style="clear: both;">
                </div>
                <div id="divcliente" runat="server" visible="false">
                    <ul class="datos_cip2 h0">
                        <li class="t1"><span class="color">&gt;&gt; </span>
                            <asp:Label ID="LblCliente" runat="server" Text="Cliente"> </asp:Label>
                        </li>
                        <li class="t2">
                            <asp:TextBox ID="txtCliente" MaxLength="50" runat="server" CssClass="normal"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombre" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz,' "
                                TargetControlID="txtCliente">
                            </cc1:FilteredTextBoxExtender>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt; </span>
                            <asp:Label ID="LblMail" runat="server" Text="Email:"></asp:Label>
                        </li>
                        <li class="t2">
                            <asp:TextBox ID="txtEmail" MaxLength="70" runat="server" CssClass="normal"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz@1234567890.-_'"
                                TargetControlID="txtEmail">
                            </cc1:FilteredTextBoxExtender>
                        </li>
                    </ul>
                </div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" ValidationGroup="GrupoValidacion" />
                        <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar"
                            ValidationGroup="GrupoValidacion" />
                        <asp:Button ID="BtnImprimir" runat="server" CssClass="input_azul4" Text="Excel" ValidationGroup="GrupoValidacion" />
                        <asp:Button ID="btnNuevo" runat="server" Visible="false" Text="Nuevo" />
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="BtnImprimir" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" style="clear: both">
            <ContentTemplate>
                <div id="divgrilla" runat="server" class="cont_cel">
                    <div id="divlblmensaje" runat="server">
                        <asp:Label ID="lblResultado" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div id="div5" class="divContenedorTitulo" runat="server">
                        <h5>
                            <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label>
                        </h5>
                    </div>
                    <asp:GridView ID="gvResultado" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdSolicitud" OnPageIndexChanging="gvResultado_PageIndexChanging"
                        OnRowDataBound="gvResultado_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Ver Detalle" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/ver.png" %>'
                                        PostBackUrl="cosorede.aspx" ToolTip="Ver Informaci&oacute;n detallada" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ClienteNombre" HeaderText="Cliente" SortExpression="ClienteNombre" />
                            <asp:BoundField DataField="InfoCuenta" HeaderText="Cuenta" SortExpression="InfoCuenta"
                                ItemStyle-Width="100px"></asp:BoundField>
                            <asp:BoundField DataField="InfoSolicitud" HeaderText="Monto Solicitado a retirar"
                                SortExpression="InfoSolicitud">
                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Fecha Solicitud" DataField="FechaSolicitud" SortExpression="FechaSolicitud"
                                ItemStyle-Width="90px" />
                            <asp:TemplateField HeaderText="Fecha de Aprobaci&oacute;n" SortExpression="FechaAprobacion">
                                <ItemTemplate>
                                    <%# IIf(DataBinder.Eval(Container.DataItem, "FechaAprobacion").ToString() = DateTime.MinValue.ToString(), "", DataBinder.Eval(Container.DataItem, "FechaAprobacion"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fecha de Rechazo" SortExpression="FechaRechazo">
                                <ItemTemplate>
                                    <%# IIf(DataBinder.Eval(Container.DataItem, "FechaRechazo").ToString() = DateTime.MinValue.ToString(), "", DataBinder.Eval(Container.DataItem, "FechaRechazo"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="EstadoDescripcion" HeaderText="Estado Solicitud" SortExpression="EstadoDescripcion"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="cabecera" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="BtnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
