Imports System.Data
Imports System.Collections.Generic
Imports SPE.Entidades
Imports SPE.Web
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.EmsambladoComun

Partial Class COSORETDIN

    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CDineroVirtual)
    
    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnClear As System.Web.UI.WebControls.Button
        Get
            Return BtnLimpiar
            ThrowWarningMessage("")
        End Get
    End Property



    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Public Overrides Sub OnClear()
        txtFechaDesde.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy")
        txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy")
        ddlEstado.SelectedIndex = 0
        txtCliente.Text = String.Empty
        txtEmail.Text = String.Empty
        TxtNumeroCuenta.Text = String.Empty

        ddlEstado.SelectedIndex = 0

        lblMsgNroRegistros.Text = String.Empty



        lblResultado.Text = ""
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        ThrowWarningMessage("")

    End Sub

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obebusqueda As New SPE.Entidades.BESolicitudRetiroDineroVirtual
        Dim pIduser As String
        pIduser = ViewState("USI")
        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
            obebusqueda.IdUsuario = ClienteUsuarioID
        End If

        txtCliente.Text = txtCliente.Text.Trim()
        txtEmail.Text = txtEmail.Text.Trim()
        txtFechaDesde.Text = txtFechaDesde.Text.Trim()
        txtFechaHasta.Text = txtFechaHasta.Text.Trim()

        'obebusqueda.IdCliente = If(hdUsuarioId.Value.ToString() = String.Empty, 0, Convert.ToInt64(hdUsuarioId.Value))
        obebusqueda.IdCliente = If(pIduser = String.Empty, 0, Convert.ToInt64(pIduser))
        obebusqueda.ClienteNombre = txtCliente.Text.Trim()
        obebusqueda.Email = txtEmail.Text.Trim()
        obebusqueda.FechaInicio = If(String.IsNullOrEmpty(txtFechaDesde.Text.Trim()), DateTime.MinValue, Convert.ToDateTime(txtFechaDesde.Text.Trim()))
        obebusqueda.FechaFin = If(String.IsNullOrEmpty(txtFechaHasta.Text.Trim()), DateTime.MinValue, Convert.ToDateTime(txtFechaHasta.Text.Trim()))
        obebusqueda.Numero = TxtNumeroCuenta.Text.Trim()
        obebusqueda.IdEstado = If(ddlEstado.SelectedValue = String.Empty, 0, Convert.ToInt32(ddlEstado.SelectedValue))
        ViewState("IdEst") = obebusqueda.IdEstado
        ViewState("SolicitudRetiroDineroVirtual") = obebusqueda

        Return obebusqueda
    End Function



    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "Cocude"
        e.MaintenancePageName = "Cocude.aspx"
        e.ExecuteSearchOnFirstLoad = True

    End Sub


    Public Overrides Sub OnAfterSearch()

        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
            If ResultList.Count = 0 Then
                div5.Visible = False
                'divgrilla.Visible = False
                divlblmensaje.Visible = False
            End If
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
            divlblmensaje.Visible = True
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then

            CargarCombo()
            Page.SetFocus(ddlEstado)
            txtFechaDesde.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy")
            txtFechaHasta.Text = DateTime.Now.ToString("dd/MM/yyyy")
            Dim pRol As String

            If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
                'hdRolName.Value = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol
                pRol = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Rol


                If pRol = "Cliente" Then
                    ViewState("USI") = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
                    txtCliente.Visible = False
                    txtEmail.Visible = False
                    'hdUsuarioId.Value = ClienteUsuarioID
                    divcliente.Visible = False
                    LblCliente.Visible = False
                    LblMail.Visible = False
                    LblTitulo.Text = "Mis Solicitudes"
                    ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Solicitado
                End If
                If pRol = "Tesoreria" Then
                    ViewState("USI") = ""
                    txtEmail.Visible = True
                    txtCliente.Visible = True
                    'hdUsuarioId.Value = ""
                    divcliente.Visible = True
                    LblCliente.Visible = True
                    LblMail.Visible = True
                    LblTitulo.Text = "Consultar Solicitudes de Retiro de Dinero"
                    ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Aprobado
                End If
                If pRol = "Administrador Monedero Electronico" Then
                    ViewState("USI") = ""
                    divcliente.Visible = True
                    txtEmail.Visible = True
                    txtCliente.Visible = True
                    'hdUsuarioId.Value = ""
                    LblCliente.Visible = True
                    LblMail.Visible = True
                    LblTitulo.Text = "Consultar Solicitudes de Retiro de Dinero"
                    ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoSolicitudRetiroDinero.Solicitado
                End If
            End If

            Page.Form.DefaultButton = btnNuevo.UniqueID()

        End If
        'divgrilla.Visible = False
        '
    End Sub

    Private Sub CargarCombo()
        Dim objParametro As New SPE.Web.CAdministrarParametro
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun


        'SERVICIO
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim obeServicio As New SPE.Entidades.BEServicio
        obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("SRD"), _
        "Descripcion", "Id", "::: Todos :::")

    End Sub

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub

    Public Overrides Sub OnBeforeSearch()

    End Sub

    Public Overrides Sub OnMainSearch()
        Try
            Dim businessEntityObject As New BusinessEntityBase()
            businessEntityObject = CreateBusinessEntityForSearch()

            businessEntityObject.PageNumber = PageNumber
            businessEntityObject.PageSize = GridViewResult.PageSize
            businessEntityObject.TipoOrder = IIf(SortDir = SortDirection.Ascending, True, False)
            businessEntityObject.PropOrder = SortExpression
            GridViewResult.PageIndex = PageNumber - 1

            Dim Resu As New List(Of BESolicitudRetiroDineroVirtual)
            Resu = New CDineroVirtual().ConsultarSolicitudRetiroMonedero(businessEntityObject)

            If CType(ViewState("IdEst"), Integer) <> 0 Then
                If Resu.Count() > 0 Then
                    ViewState("DescEstado") = Resu(0).EstadoDescripcion
                Else : ViewState("DescEstado") = "Todos"
                End If
            Else : ViewState("DescEstado") = "Todos"
            End If

            

            ResultList = New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            For Each x As BESolicitudRetiroDineroVirtual In Resu
                ResultList.Add(x)
            Next




            Dim oObjectDataSource As New ObjectDataSource

            oObjectDataSource.ID = "oObjectDataSource_" + GridViewResult.ID
            oObjectDataSource.EnablePaging = GridViewResult.AllowPaging
            oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter"
            oObjectDataSource.SelectMethod = "GetDataGrilla"
            oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros"
            oObjectDataSource.StartRowIndexParameterName = "filaInicio"
            oObjectDataSource.MaximumRowsParameterName = "maxFilas"
            oObjectDataSource.EnableViewState = False
            AddHandler oObjectDataSource.ObjectCreating, AddressOf Me.oObjectDataSource_ObjectCreating
            GridViewResult.DataSource = oObjectDataSource


            GridViewResult.DataBind()
            If (GridViewResult.Rows.Count = 0) Then
                ThrowWarningMessage(MessageNoRecordsWereFound)
            Else
                ThrowWarningMessage("")
            End If

        Catch ex As Exception
            ThrowErrorMessage(ex.Message)
        End Try
    End Sub

    Private Sub oObjectDataSource_ObjectCreating(ByVal sender As Object, ByVal e As ObjectDataSourceEventArgs)
        Dim TotalGeneral As Int32
        TotalGeneral = If(ResultList.Count = 0, 0, ResultList(0).TotalPageNumbers)
        e.ObjectInstance = New _3Dev.FW.Web.TotalRegistrosTableAdapter(ResultList, TotalGeneral)
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvResultado.PageIndex = e.NewPageIndex
        buscar()
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)        '
    End Sub



    Protected Sub ddlEstado_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEstado.SelectedIndexChanged
        buscar()
    End Sub

    Private Sub buscar()
        MyBase.BtnSearch_Click(Nothing, New EventArgs())
    End Sub

    Protected Sub gvResultado_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvResultado.RowEditing
        MyBase.GridViewResult_RowEditing(sender, e)
    End Sub

    Protected Sub BtnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnImprimir.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel.")
        Else
            'Dim entidad As BESolicitudRetiroDineroVirtual
            'entidad = CType(ViewState("SolicitudRetiroDineroVirtual"), BESolicitudRetiroDineroVirtual)
            'Dim IdClienteRq As String = entidad.IdCliente
            'Dim NomCliRq As String = entidad.ClienteNombre
            'Dim EmailRq As String = entidad.Email
            'Dim strFechaInicio As String = entidad.FechaInicio
            'Dim strFechaFin As String = entidad.FechaFin
            'Dim NumeroRq As String = entidad.Numero
            'Dim EstadoRq As String = entidad.IdEstado
            'Dim DescEstadoRq As String = ViewState("DescEstado").ToString()
            'Response.Redirect(String.Format("~/Reportes/RptCoSolRetDin.aspx?Pidc={0}&Pnoc={1}&Pcem={2}&Pnro={3}&PDes={4}&FIni={5}&FFin={6}&Pest={7}", _
            '                                IdClienteRq, NomCliRq, EmailRq, NumeroRq, EstadoRq, strFechaInicio, strFechaFin, DescEstadoRq))

            Dim entidad As BESolicitudRetiroDineroVirtual
            entidad = CType(ViewState("SolicitudRetiroDineroVirtual"), BESolicitudRetiroDineroVirtual)
            Dim IdClienteRq As String = entidad.IdCliente
            Dim NomCliRq As String = entidad.ClienteNombre
            Dim EmailRq As String = entidad.Email
            Dim strFechaInicio As String = entidad.FechaInicio
            Dim strFechaFin As String = entidad.FechaFin
            Dim NumeroRq As String = entidad.Numero
            Dim EstadoRq As String = entidad.IdEstado
            Dim DescEstadoRq As String = ViewState("DescEstado").ToString()

            Dim BESR As New BESolicitudRetiroDineroVirtual

            Dim lista As New List(Of BESolicitudRetiroDineroVirtual)
            Using oCDineroVirtual As New CDineroVirtual()
                BESR.IdCliente = IdClienteRq
                BESR.ClienteNombre = NomCliRq
                BESR.Email = EmailRq
                BESR.FechaInicio = strFechaInicio
                BESR.FechaFin = strFechaFin
                BESR.Numero = NumeroRq
                BESR.IdEstado = EstadoRq
                BESR.PageSize = (Int32.MaxValue - 1)
                BESR.PageNumber = 1
                BESR.PropOrder = ""
                BESR.TipoOrder = 0

                lista = oCDineroVirtual.ConsultarSolicitudRetiroMonedero(entidad)
            End Using

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", strFechaInicio))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", strFechaFin))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", DescEstadoRq))

            UtilReport.ProcesoExportarGenerico(lista, Page, "EXCEL", "xls", "Mis Solicitudes de retiro de dinero - ", parametros, "RptCoSolRetDin.rdlc")



        End If
    End Sub

    Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        divgrilla.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        divgrilla.Visible = True
    End Sub
End Class

