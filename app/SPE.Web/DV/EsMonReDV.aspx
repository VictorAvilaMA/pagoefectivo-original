﻿<%@ Page Language="VB" Async="true" EnableEventValidation="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="EsMonReDV.aspx.vb" Inherits="DV_EsMonReDV"
    Title="PagoEfectivo - Establecer Monto de Recarga" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        function ObtenerNuevoMonto() {
            $('#<%= hdfNuevosMontos.ClientID %>').val('');
            $.each(
            $('td:has(input)')
            , function (i, val) {
                var valTextBox = $(this).children("input:text").val();
                var valueHidden = $('#<%= hdfNuevosMontos.ClientID %>').val();
                $('#<%= hdfNuevosMontos.ClientID %>').val(valueHidden + (valueHidden == '' ? '' : '|') + valTextBox);
                //$(this).children("input:hidden[id*=hdfNuevoMonto]").val(valTextBox);
            });
            //alert($('#<%= hdfNuevosMontos.ClientID %>').val());
        }
    </script>
    <h2>Establecer Monto de Recarga</h2>
    <div class="conten_pasos3">
        <h4>
            Monto de Recarga</h4>
        <asp:HiddenField ID="hdfNuevosMontos" runat="server" Value="" />
        <div class="result">
            <asp:GridView ID="gvResultado" Width="400px" runat="server" BackColor="White" AllowPaging="True"
                CssClass="grilla" AutoGenerateColumns="False" AllowSorting="True" PageSize="12"
                DataKeyNames="IdMoneda, IdCliente" HorizontalAlign="Center">
                <Columns>
                    <asp:BoundField DataField="IdMoneda" HeaderText="IdMoneda" Visible="false" />
                    <asp:BoundField DataField="DescMoneda" HeaderText="Moneda">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdCliente" HeaderText="IdCliente" Visible="false" />
                    <asp:TemplateField HeaderText="Monto" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:TextBox ID="txtMonto" runat="server" Width="80px" Font-Size="10pt" Text='<%# Eval("Monto") %>'
                                MaxLength="5"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="ftetxtMonto" runat="server" ValidChars="0123456789"
                                TargetControlID="txtMonto">
                            </cc1:FilteredTextBoxExtender>
                            <asp:HiddenField ID="hdfCode" runat="server" Value='<%# Eval("IdMoneda") %>' />
                            <asp:HiddenField ID="hdfMonto" runat="server" Value='<%# Eval("Monto") %>' />
                            <asp:HiddenField ID="hdfNuevoMonto" runat="server" Value="0" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No se encontraron registros.
                </EmptyDataTemplate>
                <HeaderStyle CssClass="cabecera" Font-Bold="True" />
            </asp:GridView>
        </div>
        <ul class="datos_cip2" style="margin-top:auto;margin-left:60px">
            <li class="complet">
                <center>
                    <asp:Label ID="lblTransaccion" runat="server" Width="500px" Style="padding-bottom: 5px;
                        top: 0px; left: 0px; color:Red" ></asp:Label>
                </center>
                <asp:Button ID="btnActualizar" OnClientClick="ObtenerNuevoMonto();" runat="server"
                    Text="Actualizar" CssClass="input_azul5"></asp:Button>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="input_azul4">
                </asp:Button>
            </li>
        </ul>
    </div>
</asp:Content>
