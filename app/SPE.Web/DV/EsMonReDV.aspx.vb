﻿Imports System
Imports System.IO
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class DV_EsMonReDV
    Inherits System.Web.UI.Page

    Dim min As Integer
    Dim max As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblTransaccion.CssClass = "MensajeTransaccion"
        Dim dv As New CDineroVirtual
        min = dv.ObtenerParametroMonederoByID(9).ValorDecimal
        max = dv.ObtenerParametroMonederoByID(10).ValorDecimal

        'If Not Page.IsPostBack Then

        CargarInfoMoneda()
        'End If

    End Sub

    Protected Sub CargarInfoMoneda()
        Dim oBEMontoRecargaMonedero As New BEMontoRecargaMonedero
        Dim lista As New List(Of BEMontoRecargaMonedero)
        Dim control As New CDineroVirtual
        oBEMontoRecargaMonedero.IdUsuario = Convert.ToInt64(CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario)
        ViewState("IdUsuario") = oBEMontoRecargaMonedero.IdUsuario
        lista = control.ConsultarMonedaXIdCliente(oBEMontoRecargaMonedero)
        gvResultado.DataSource = lista
        gvResultado.DataBind()
    End Sub

    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click

        Dim listamontorecarga As New List(Of BEMontoRecargaMonedero)

        Dim Control As New CDineroVirtual
        Dim contador As Integer = 0
        If Me.gvResultado.Rows.Count > 0 Then
            Try
                For Each fila As GridViewRow In gvResultado.Rows
                    Dim entidad As New BEMontoRecargaMonedero
                    Dim idMoneda As String = CType(fila.FindControl("hdfCode"), HiddenField).Value

                    Dim Monto As Decimal = _3Dev.FW.Util.DataUtil.ObjectToDecimal(hdfNuevosMontos.Value.Split("|")(contador))
                    entidad.IdMoneda = idMoneda
                    entidad.IdUsuario = Convert.ToInt64(ViewState("IdUsuario"))
                    entidad.Monto = Monto 'DirectCast(fila.Cells(3).Controls(1), TextBox).Text
                    listamontorecarga.Add(entidad)
                    contador = contador + 1

                Next

                For Each item As BEMontoRecargaMonedero In listamontorecarga
                    If item.Monto > max Or item.Monto < min Then
                        lblTransaccion.Text = "El monto de la recarga debe ser mayor a " & min & " y menor a " & max & " para cualquier moneda."
                        lblTransaccion.CssClass = "MensajeTransaccionFail"
                        Return
                    End If
                Next

                Dim Resultado As Integer = Control.ActualizarMontoRecargaDineroVirtual(listamontorecarga)

                lblTransaccion.Text = "Se estableció un nuevo monto de recarga."
                CargarInfoMoneda()
            Catch ex As Exception
                lblTransaccion.Text = "El campo monto es requerido."
                'Return
            End Try
        Else
            lblTransaccion.Text = "La lista no tiene registros."
        End If

    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("COCUCli.aspx")
    End Sub
End Class
