﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="HaInAcCuDiVi.aspx.vb" Inherits="DV_HaInAcCuDiVi"
    Title="PagoEfectivo - Habilitar/Inhabilitar Acceso Cuenta de  Dinero Virtual" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Habilitar/Inhabilitar Acceso Cuenta de Dinero Virtual</h2>
    <div class="conten_pasos3">
        <h4>
            Datos del Cliente</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Email:</li>
            <li class="t2">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" BackColor="Silver" Enabled="False"
                    ReadOnly="True"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Alias:</li>
            <li class="t2">
                <asp:TextBox ID="txtAlias" runat="server" CssClass="normal" BackColor="Silver" Enabled="False"
                    ReadOnly="True"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Nombres:</li>
            <li class="t2">
                <asp:TextBox ID="txtNombre" runat="server" CssClass="normal" BackColor="Silver" Enabled="False"
                    ReadOnly="True"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Documento:</li>
            <li class="t2">
                <asp:TextBox ID="txtTipDoc" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> N&uacute;mero:</li>
            <li class="t2">
                <asp:TextBox ID="txtNumero" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> G&eacute;nero::</li>
            <li class="t2">
                <asp:TextBox ID="txtGenero" runat="server" CssClass="normal" BackColor="Silver" Enabled="False"
                    ReadOnly="True"></asp:TextBox>
            </li>
        </ul>
        <div style="clear: both">
        </div>
        <h4>
            Monedero Electr&oacute;nico</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Estado Actual:</li>
            <li class="t2">
                <asp:TextBox ID="txtEstado" runat="server" CssClass="normal" BackColor="Silver" Enabled="False"
                    ReadOnly="True"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Observaciones:</li>
            <li class="t4" style="height: auto">
                <asp:TextBox ID="txtObservacion" runat="server" CssClass="tarea_3" TextMode="MultiLine"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Debe ingresar hasta un maximo de 200 caracteres"
                    Display="Dynamic" ValidationExpression="^([\S\s]{0,200})$" ControlToValidate="txtObservacion">Debe ingresar hasta un maximo de 200 caracteres</asp:RegularExpressionValidator>
            </li>
            <li class="complet">
                <asp:Button ID="btnActivar" runat="server" CssClass="input_azul3" Text="Activar"
                    OnClientClick="return ConfirmMe();" Visible="False" />
                <asp:Button ID="btnBloquear" runat="server" CssClass="input_azul3" Text="Bloquear"
                    OnClientClick="return ConfirmMe();" Visible="False" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                    Text="Cancelar" CausesValidation="False" />
            </li>
        </ul>
    </div>
</asp:Content>
