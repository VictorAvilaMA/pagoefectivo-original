﻿Imports System
Imports System.IO
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class DV_HaInAcCuDiVi
    'Inherits System.Web.UI.Page
    Inherits SPE.Web.PaginaBase

    Dim control As New CDineroVirtual
    Dim entidad As New BECliente
    Dim str As String

    Protected Sub DV_HaInAcCuDiVi_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarInfo()
        End If
    End Sub

    Protected Sub CargarInfo()
        Dim oBECliente As New BECliente
        entidad.IdCliente = VariableTransicion
        oBECliente = control.ConsultarClienteXIdClienteDV(entidad)

        txtEmail.Text = oBECliente.Email.ToString()
        txtAlias.Text = oBECliente.AliasCliente.ToString()
        txtNombre.Text = oBECliente.NombresApellidos.ToString()
        txtTipDoc.Text = oBECliente.TipoDoc.ToString()
        txtNumero.Text = oBECliente.NumeroDocumento.ToString()
        txtGenero.Text = oBECliente.DescripcionGenero.ToString()
        ViewState("NombreCliente") = oBECliente.NombresApellidos.ToString()
        ViewState("Email") = oBECliente.Email.ToString()

        If oBECliente.HabilitarMonedero = 1 Then
            txtEstado.Text = "Activo"
            btnBloquear.Visible = True
        Else
            txtEstado.Text = "Bloqueado"
            btnActivar.Visible = True
        End If
    End Sub

    Protected Sub btnActivar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivar.Click
        Dim Resultado As Integer
        entidad.IdCliente = VariableTransicion
        entidad.HabilitarMonedero = 1
        entidad.Observacion = txtObservacion.Text.ToString()
        entidad.NombresApellidos = ViewState("NombreCliente").ToString()
        entidad.Email = ViewState("Email").ToString()
        Resultado = control.ActualizarEstadoHabilitarMonederoClienteDV(entidad)

        If Resultado <> -1 Then
            str = "Se Habilitó el Acceso a Monedero Virtual"
        Else : str = "Se produjo un error al habilitar el Acceso..."
        End If
        JSMessageAlertWithRedirect("Info: ", str, "vv1", "../ADM/COClint.aspx")
    End Sub

    Protected Sub btnBloquear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBloquear.Click
        Dim Resultado As Integer
        entidad.IdCliente = VariableTransicion
        entidad.HabilitarMonedero = 0
        entidad.Observacion = txtObservacion.Text.ToString()
        entidad.NombresApellidos = ViewState("NombreCliente").ToString()
        entidad.Email = ViewState("Email").ToString()
        Resultado = control.ActualizarEstadoHabilitarMonederoClienteDV(entidad)

        If Resultado <> -1 Then
            str = "Se Inhabilitó el Acceso a Monedero Virtual"
        Else : str = "Se produjo un error al inhabilitar el Acceso..."
        End If
        JSMessageAlertWithRedirect("Info: ", str, "vv2", "../ADM/COClint.aspx")
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("../ADM/COClint.aspx")
    End Sub

End Class
