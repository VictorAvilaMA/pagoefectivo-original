﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="HaInCuDiVi.aspx.vb" Inherits="DV_HaInCuDiVi"
    Title="PagoEfectivo - Habilitar/Inhabilitar Cuenta de  Dinero Virtual" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Habilitar/Inhabilitar Cuenta de Dinero Virtual</h2>
    <div class="conten_pasos3">
        <h4>
            Datos del Usuario</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Cliente :</li>
            <li class="t2">
                <asp:TextBox ID="txtCliente" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Email:</li>
            <li class="t2">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
        </ul>
       <div style="clear:both;"></div>
        <h4>
            Datos de la Cuenta</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Nº Cuenta:</li>
            <li class="t2">
                <asp:TextBox ID="txtNroCuenta" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Fecha Solicitada:</li>
            <li class="t2">
                <asp:TextBox ID="txtFecSol" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Moneda:</li>
            <li class="t2">
                <asp:TextBox ID="txtMoneda" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Fecha Aprobaci&oacute;n:</li>
            <li class="t2">
                <asp:TextBox ID="txtFecApr" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Saldo Cuenta:</li>
            <li class="t2">
                <asp:TextBox ID="txtSaldo" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Estado Cuenta:</li>
            <li class="t2">
                <asp:TextBox ID="txtEstado" runat="server" CssClass="normal" Enabled="False" ReadOnly="True"
                    BackColor="Silver"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span>Observaciones:</li>
            <li class="t2">
                <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine"  cols="20" Rows="3" Style="height: 50px;float:left" class="neu"></asp:TextBox>
               
                <%--<asp:RegularExpressionValidator ID="revMailAlternativo" runat="server" ControlToValidate="TxtAlias"
                                    ErrorMessage="No se permiten espacios, tildes o caracteres extraños..." ValidationExpression="^[-_a-zA-Z0-9ñÑ]*$" />--%>
            </li>
            <li class="t2">
             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Debe ingresar hasta un maximo de 200 caracteres"
                    Display="Dynamic" ValidationExpression="^([\S\s]{0,200})$" ControlToValidate="txtObservacion">Debe ingresar hasta un maximo de 200 caracteres</asp:RegularExpressionValidator>
            </li>
            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeValidacion"></asp:Label>
            
        </ul>
        <div style="clear:both;"></div>
        <ul class="datos_cip2">

        <li class="complet">
                <asp:Button ID="btnActivar" runat="server" CssClass="input_azul4" Text="Activar"
                    OnClientClick="return ConfirmMe();" Visible="true" />
                <asp:Button ID="btnBloquear" runat="server" CssClass="input_azul4" Text="Bloquear"
                    OnClientClick="return ConfirmMe();" Visible="true" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                    Text="Cancelar" CausesValidation="False" />
            </li>
        </ul>
    </div>
</asp:Content>
