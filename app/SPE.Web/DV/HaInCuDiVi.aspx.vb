﻿Imports System
Imports System.IO
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class DV_HaInCuDiVi
    'Inherits System.Web.UI.Page
    Inherits SPE.Web.PaginaBase

    Dim control As New CDineroVirtual
    Dim entidad As New BECuentaDineroVirtual
    Dim str As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarInfo()
        End If
    End Sub

    Protected Sub CargarInfo()
        Dim oBECuentaDineroVirtual As New BECuentaDineroVirtual
        entidad.IdCuentaDineroVirtual = VariableTransicion
        oBECuentaDineroVirtual = control.ConsultarCuentaDineroVirtualUsuarioByCuentaId(entidad)
        ViewState("CuentaDV") = oBECuentaDineroVirtual
        txtCliente.Text = oBECuentaDineroVirtual.ClienteNombre.ToString()
        txtEmail.Text = oBECuentaDineroVirtual.Email.ToString()
        Dim NroCtaM = oBECuentaDineroVirtual.Numero.ToString()
        txtNroCuenta.Text = NroCtaM
        txtFecSol.Text = oBECuentaDineroVirtual.FechaSolicitud.ToString()
        txtMoneda.Text = oBECuentaDineroVirtual.MonedaDescripcion.ToString() & " (" & oBECuentaDineroVirtual.MonedaSimbolo.ToString() & ")"
        txtFecApr.Text = oBECuentaDineroVirtual.FechaAprobacion.ToString()
        txtSaldo.Text = oBECuentaDineroVirtual.Saldo.ToString()
        txtEstado.Text = oBECuentaDineroVirtual.EstadoDescripcion.ToString()
        txtObservacion.Text = oBECuentaDineroVirtual.ObservacionBloqueo


        Dim Estado As New Integer
        Estado = oBECuentaDineroVirtual.IdEstado

        If NroCtaM <> "Sin Numero" Then
            If Estado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
                btnBloquear.Visible = True
                btnActivar.Visible = False
            ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Inhabilitado Then
                btnActivar.Visible = True
                btnBloquear.Visible = False
            End If
        Else
            btnActivar.Visible = False
            btnBloquear.Visible = False
        End If



    End Sub

    Protected Sub btnActivar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivar.Click
        Dim Resultado As Integer
        Dim NCuenta As String
        NCuenta = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).Numero
        entidad.IdCuentaDineroVirtual = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).IdCuentaDineroVirtual
        entidad.IdEstado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado
        entidad.ObservacionBloqueo = txtObservacion.Text.ToString()
        Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
        entidad.IdUsuario = ClienteUsuarioID
        Resultado = control.ActualizarEstadoCuentaDineroVirtual(entidad)

        If Resultado <> -1 Then
            str = "Se Habilitó la cuenta Nº " & NCuenta
        Else : str = "Se produjo un error al habilitar la cuenta..."
        End If
        JSMessageAlertWithRedirect("Info: ", str, "vv", "COCuentaAdmin.aspx")
        'Response.Write("<script type='text/javascript' language='javascript'>alert('" & str & "');</script>")
        'Response.Redirect("COCuentaAdmin.aspx")
    End Sub

    Protected Sub btnBloquear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBloquear.Click
        Dim Resultado As Integer
        Dim NCuenta As String

        NCuenta = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).Numero
        entidad.IdCuentaDineroVirtual = CType(ViewState("CuentaDV"), BECuentaDineroVirtual).IdCuentaDineroVirtual
        entidad.IdEstado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Inhabilitado
        entidad.ObservacionBloqueo = txtObservacion.Text.ToString()
        Dim ClienteUsuarioID As Integer = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
        entidad.IdUsuario = ClienteUsuarioID
        Resultado = control.ActualizarEstadoCuentaDineroVirtual(entidad)

        If Resultado <> -1 Then
            str = "Se Inhabilitó la cuenta Nº " & NCuenta
        Else : str = "Se produjo un error al inhabilitar la cuenta..."
        End If
        JSMessageAlertWithRedirect("Info: ", str, "vv2", "COCuentaAdmin.aspx")
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("COCuentaAdmin.aspx")
    End Sub
End Class
