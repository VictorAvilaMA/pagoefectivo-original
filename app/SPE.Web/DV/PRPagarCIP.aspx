﻿<%@ Page Title="PagoEfectivo - Pagar CIP" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="PRPagarCIP.aspx.vb" Inherits="DV_PRPagarCIP" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        var VGlobal;
        $(document).ready(function () {
            $('#<%=txtNroCIP.ClientID%>').blur(function () {
                if ($('#<%=txtNroCIP.ClientID%>').val() != '')
                    $('#<%=txtNroCIP.ClientID%>').val($('#<%=txtNroCIP.ClientID%>').val().lpad('0', 14));
            });
        });

        function goBack() {
            $('#<%= txtNroCIP.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnContinuar1.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar.ClientID%>').attr('disabled', 'disabled');
            $('#<%= ddlCuenta.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnContinuar2.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar2.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtNrotoken.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnReenviarToken.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnPagarCIP.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar3.ClientID%>').attr('disabled', 'disabled');
            window.location.href = '../DV/COCUCLI.aspx';
        }

        function goBack2() {
            window.location.href = '../DV/COCUCLI.aspx';
        }

        function validToken() {
            var valToken = $('#<%= txtNrotoken.ClientID%>').val();
            if (valToken.length == 0)
                alertDiv('Validación: Debe Ingresar el Token enviado a su correo para continuar el proceso.');
            else
                if (valToken.length != 10)
                    alertDiv('Validación: El Token debe contener 10  dígitos.');
            return valToken.length == 10;
        }

        function goStep2() {
            var nroCip = $('#' + '<%=txtNroCIP.ClientID%>').val();
            if (nroCip.length <= 0) {
                alertDiv('Validación: Debe Ingresar el Nº de CIP que desea pagar.');
                return;
            }
            if (nroCip.length != 14) {
                alertDiv('Validación: El Nº de CIP debe contener 14 dígitos.');
                return;
            }
            consultarCIP();

        }

        function goStep3() {



            var valueCombo = $('#' + '<%=ddlCuenta.ClientID%>').val();
            if (valueCombo == '') {
                alertDiv('Validación: Ud. No posee una Cuenta Activa.');
                return false;
            }


            $.ajax({
                type: "POST",
                url: "../DV/PRPagarCIP.aspx/ValidarCuentaVSOP",
                data: "{nroOP: '" + $('#<%=txtNroCIP.ClientID%>').val() + "' , nroCuenta: '" + valueCombo + "'} ",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d.MensajeError != '') {
                        alertDiv(response.d.MensajeError);
                        return false
                    }
                    else {
                        $.ajax({
                            type: "POST",
                            url: "../DV/PRPagarCIP.aspx/GenerarToken2",
                            data: "{nroCuenta: '" + valueCombo + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {

                                $('#' + '<%=hdfInfoStep2.ClientID%>').val('')
                                $('#' + '<%=hdfInfoStep2.ClientID%>').val($('#<%=ddlCuenta.ClientID%>').val());
                                $('#' + '<%=hdfInfoStep2.ClientID%>').val($('#<%=hdfInfoStep2.ClientID%>').val() + '|' + response.d.IdTokenDineroVirtual);

                                $('#spCuentaMonedaSaldo').text($('#ctl00_ContentPlaceHolder1_ddlCuenta').children('option:selected').text());

                                $('#spNroCuenta').text($('#<%=ddlCuenta.ClientID%> option:selected').text());
                                $('#spnInfotoken').text(response.d.Token);

                                $('#PanelPaso1').css('display', 'none');
                                $('#PanelPaso2').css('display', 'none');
                                $('#PanelPaso3').css('display', 'block');

                            },
                            error: function (error) { window.location.href = '../DV/COCUCLI.aspx'; ; }
                        });
                    }
                },
                error: function (error) { window.location.href = '../DV/COCUCLI.aspx'; ; }
            });

        }

        function consultarCIP() {



            $.ajax({
                type: "POST",
                url: "../DV/PRPagarCIP.aspx/ConsultarOP",
                data: "{nroOP: '" + $('#<%=txtNroCIP.ClientID%>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == null) {
                        alertDiv('Validación: Nº de CIP no Existe.');
                        $('#<%=txtNroCIP.ClientID%>').focus();
                        return;
                    }
                    if (response.d.MensajeError != '') {
                        alertDiv(response.d.MensajeError);
                        return false;
                    }

                    $('#' + '<%=hdfInfoStep1.ClientID%>').val(response.d.IdOrdenPago + '|' + response.d.IdMoneda + '|' + response.d.Total);


                    $('#spServicio,#spServicio3').text(response.d.DescripcionServicio);
                    $('#spNroCIP,#spNroCIP3').text(response.d.NumeroOrdenPago);
                    $('#spConceptoPago,#spConceptoPago3').text(response.d.ConceptoPago);
                    $('#<%=hdfConcPag.ClientID%>').val(response.d.ConceptoPago);
                    $('#spMontoApagar,#spMontoApagar3,#spMontoApagar2').text(response.d.SimboloMoneda + ' ' + parseFloat(response.d.Total).toFixed(2));
                    $('#spNombreUsuario,#spNombreUsuario3').text(response.d.DescripcionCliente);
                    $('#spFechaEmision,#spFechaEmision3').text(formatearFechaCSharp(response.d.FechaEmision));
                    $('#spFechaLimite,#spFechaLimite3').text(formatearFechaCSharp(response.d.FechaVencimiento));

                    $('#PanelPaso1').css('display', 'none');
                    $('#PanelPaso2').css('display', 'block');
                    $('#PanelPaso3').css('display', 'none');

                },
                error: function (error) { window.location.href = '../DV/COCUCLI.aspx'; }
            });

        }




        function reenviarToken() {
            $.ajax({
                type: "POST",
                url: "../DV/PRPagarCIP.aspx/ReenviarToken",
                data: "{idToken: '" + $('#<%=hdfInfoStep2.ClientID%>').val().split("|")[1] + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d.Token == '')
                        alertDiv('Info: Se ha reenvio el Código Token a su Correo.');
                    else {
                        if (response.d.IdTokenDineroVirtual != 0) {
                            $('#' + '<%=hdfInfoStep2.ClientID%>').val($('#<%=ddlCuenta.ClientID%>').val());
                            $('#' + '<%=hdfInfoStep2.ClientID%>').val($('#<%=hdfInfoStep2.ClientID%>').val() + '|' + response.d.IdTokenDineroVirtual);
                        }
                        alertDiv('Info: ' + response.d.Token);

                    }
                },
                error: function (error) { /*alert("error: " + error)*/history.back(1); }
            });
        }

    </script>
    <asp:HiddenField ID="hdfInfoStep1" runat="server" />
    <asp:HiddenField ID="hdfInfoStep2" runat="server" />
    <asp:HiddenField ID="hdfConcPag" runat="server" />
    <h2>
        Pagar CIP (C&oacute;digo de Identificaci&oacute;n de Pago)</h2>
    <div id="PanelPaso1" style="display: block">
        <div class="conten_pasos">
            <ul class="pasos">
                <li id="paso1" class="activa"><a>1</a> Ingreso CIP</li>
                <li id="paso2"><a>2</a> Selecci&oacute;n de cuenta</li>
                <li id="paso3"><a>3</a> Confirmaci&oacute;n</li>
            </ul>
            <div>
                <h4>
                    N&uacute;mero CIP</h4>
                <asp:TextBox ID="txtNroCIP" MaxLength="14" CssClass="input_big" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0123456789"
                    TargetControlID="txtNroCIP">
                </cc1:FilteredTextBoxExtender>
                <asp:Button ID="btnContinuar1" OnClientClick="goStep2();return false;" CssClass="input_azul"
                    runat="server" Text="Continuar" />
                <asp:LinkButton Text="Cancelar" ID="btnCancelar" CssClass="cancela" OnClientClick="goBack();return false;"
                    runat="server" />
            </div>
        </div>
    </div>
    <div id="PanelPaso2" style='display: none'>
        <div class="conten_pasos2">
            <ul class="pasos">
                <li id="paso4"><a>1</a> Ingreso de CIP</li>
                <li id="paso5" class="activa"><a>2</a> Selecci&oacute;n de cuenta</li>
                <li id="paso6"><a>3</a> Confirmaci&oacute;n</li>
            </ul>
            <h4 style="width:300px;background:url(../App_Themes/SPE/img/flechatextogrande.png) no-repeat left top;">
                Informaci&oacute;n del CIP</h4>
            
            <ul class="datos_cip">
                <li class="t1"><span class="color">&gt;&gt;</span> Nº CIP <span class="alejado">:</span></li>
                <li class="t2"><strong><span id="spNroCIP"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Servicio <span class="alejado">:</span></li>
                <li class="t2"><strong><span id="spServicio"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Concepto de pago <span class="alejado">
                    :</span></li>
                <li class="t2"><strong><span id="spConceptoPago"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Monto a pagar <span class="alejado">
                    :</span></li>
                <li class="t2"><strong><span id="spMontoApagar"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Nombre de usuario <span class="alejado">
                    :</span></li>
                <li class="t2"><strong><span id="spNombreUsuario"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Fecha de emisi&oacute;n <span
                    class="alejado">:</span></li>
                <li class="t2"><strong><span id="spFechaEmision"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Fecha de l&iacute;mite de pago <span
                    class="alejado">:</span></li>
                <li class="t2"><strong><span id="spFechaLimite"></span></strong></li>
            </ul>
            <h5>
                ¿Desde qu&eacute; cuenta deseas pagar?</h5>
            <asp:DropDownList ID="ddlCuenta" cssClass="selecto" runat="server">
            </asp:DropDownList>
            <asp:Button ID="btnContinuar2" OnClientClick="goStep3();return false" CssClass="input_azul"
                Text="Continuar" runat="server" />
            <asp:LinkButton ID="btnCancelar2" CssClass="cancela" OnClientClick="goBack();return false;"
                Text="Cancelar" runat="server" PostBackUrl="../DV/COCUCLI.aspx" />
        </div>
    </div>
    <div id="PanelPaso3" style="display: none">
        <div class="conten_pasos4">
            <ul class="pasos">
                <li id="paso7"><a>1</a> Ingreso de CIP</li>
                <li id="paso8"><a>2</a> Selecci&oacute;n de cuenta</li>
                <li id="paso9" class="activa"><a>3</a> Confirmaci&oacute;n</li>
            </ul>
            <h4>
                Información del CIP</h4>
            <ul class="datos_cip">
                <li class="t1"><span class="color">&gt;&gt;</span> Nº CIP <span class="alejado">:</span></li>
                <li class="t2"><strong><span id="spNroCIP3"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Servicio <span class="alejado">:</span></li>
                <li class="t2"><strong><span id="spServicio3"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Concepto de pago <span class="alejado">
                    :</span></li>
                <li class="t2"><strong><span id="spConceptoPago3"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Monto a pagar <span class="alejado">
                    :</span></li>
                <li class="t2"><strong><span id="spMontoApagar3"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Nombre de usuario <span class="alejado">
                    :</span></li>
                <li class="t2"><strong><span id="spNombreUsuario3"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Fecha de emisi&oacute;n <span
                    class="alejado">:</span></li>
                <li class="t2"><strong><span id="spFechaEmision3"></span></strong></li>
                <li class="t1"><span class="color">&gt;&gt;</span> Fecha de l&iacute;mite de pago <span
                    class="alejado">:</span></li>
                <li class="t2"><strong><span id="spFechaLimite3"></span></strong></li>
            </ul>
            <h5>
                Total a pagar</h5>
            <ul class="datos_cip" id="total">
                <li class="t1"><span class="color">&gt;&gt;</span> Monto total <span class="alejado">
                    :</span></li>
                <li class="t2"><strong class="bigger"><span id="spMontoApagar"></span></strong>
                </li>
            </ul>
            <h5>
                Cuenta dinero Virtual</h5>
            <ul class="datos_cip" id="cuenta" style="width: 600px">
                <li class="t1"><span class="color">&gt;&gt;</span> Nº cuenta <span class="alejado">:</span></li>
                <li class="t2" style="width: 400px"><strong><span id="spCuentaMonedaSaldo"></span></strong>
                </li>
            </ul>
            <h6>
                Validaci&oacute;n</h6>
            <p class="ubica">
                <span class="colorazul">>></span> Ha sido enviado a su correo un token de verificaci&oacute;n,
                el cual deber&aacute; ingresar en el campo de texto inferior.
            </p>
            <p>
                <asp:TextBox ID="txtNrotoken" CssClass="input_big2" MaxLength="10" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="0123456789"
                    TargetControlID="txtNrotoken">
                </cc1:FilteredTextBoxExtender>
                <asp:LinkButton ID="btnReenviarToken" OnClientClick="reenviarToken();return false;"
                    CssClass="cancela2" Text="Reenviar" runat="server" />
                <span id="spnInfotoken"></span>
            </p>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnPagarCIP" OnClientClick="return validToken();" CssClass="input_azul"
                        runat="server" Text="Continuar" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:LinkButton ID="btnCancelar3" OnClientClick="goBack2();return false;" CssClass="cancela"
                runat="server" Text="Cancelar" />
        </div>
    </div>
    <div class="colRight" style="display: none">
        <h1>
            Pagar CIP</h1>
        <div class="inner-col-right">
            <div class="hdr-cip">
                <div class="izq cnt-titular">
                    <span class="bold">Titular:</span> <span class="value">
                        <asp:Literal ID="ltrTitular" runat="server"></asp:Literal>
                    </span>
                </div>
                <div class="der cnt-date">
                    <span class="bold">Fecha:</span> <span class="value">
                        <asp:Literal ID="ltrFecha" runat="server"></asp:Literal>
                    </span>
                    <div class="clear">
                    </div>
                    <span class="bold">Hora:</span> <span class="value">
                        <asp:Literal ID="ltrHora" runat="server"></asp:Literal>
                    </span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="ctn-arrows-tabs">
                <div id="dvStep1" class="select fr_arrow">
                    <span class="queue-first-arrow"></span>
                    <div class="desc">
                        <h4>
                            Paso 1</h4>
                        <p>
                            Ingreso de CIP</p>
                    </div>
                    <span class="go-arrow"></span>
                </div>
                <div id="dvStep2" class="arrow">
                    <span class="queue-arrow"></span>
                    <div class="desc">
                        <h4>
                            Paso 2</h4>
                        <p>
                            Selecci&oacute;n de cuenta</p>
                    </div>
                    <span class="go-arrow"></span>
                </div>
                <div id="dvStep3" class="arrow">
                    <span class="queue-arrow"></span>
                    <div class="desc">
                        <h4>
                            Paso 3</h4>
                        <p>
                            Confirmaci&oacute;n</p>
                    </div>
                    <span class="go-arrow"></span>
                </div>
                <div class="br-r-bg-arrows">
                </div>
            </div>
            <div class="pdng-10">
            </div>
        </div>
        <div class="sep_a14">
        </div>
        <div class="inner-col-right">
            <div id="dvGrPaso1" style="display: block">
                <div class="dlgrid">
                    <div class="even w100 clearfix dlinline">
                        <dl class="clearfix dt15 dd30">
                            <dt class="desc">
                                <label class="der">
                                    N° CIP:</label>
                            </dt>
                            <dd class="camp">
                                <asp:TextBox ID="txtNroCIPXXX" MaxLength="14" CssClass="cipCamp" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="ftbe_txtNroCIP" runat="server" ValidChars="0123456789"
                                    TargetControlID="txtNroCIPXXX">
                                </cc1:FilteredTextBoxExtender>
                            </dd>
                        </dl>
                    </div>
                    <div class="w100 clearfix dlinline btns3">
                        <asp:Button ID="btnContinuar1XXX" OnClientClick="goStep2();return false;" CssClass="btnContinuar"
                            Text="." runat="server" />
                        <asp:Button ID="btnCancelarXXX" CssClass="btnCancelar" OnClientClick="goBack();return false;"
                            Text="." runat="server" />
                    </div>
                </div>
            </div>
            <div id="dvGrPaso2" style="display: none">
                <h4>
                    Informaci&oacute;n del CIP</h4>
                <div class="dlgrid">
                    <div class="inner-col-right">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Servicio:</span> <span id="spServicio2"></span>
                                </dt>
                                <dd class="camp">
                                    <span class="bold">N° CIP:</span> <span id="spNroCIP2"></span>
                                </dd>
                            </dl>
                        </div>
                        <div class="even w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Concepto de pago:</span> <span id="spConceptoPago2">
                                </span></dt>
                                <dd class="camp">
                                    <span class="bold">Monto a pagar:</span> <span id="spMontoApagar2"></span>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Nombre de usuario:</span> <span id="spNombreUsuario2">
                                </span></dt>
                                <dd class="camp">
                                </dd>
                            </dl>
                        </div>
                        <div class="even w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Fecha de emisi&oacute;n:</span> <span id="spFechaEmision2">
                                </span></dt>
                                <dd class="camp">
                                    <span class="bold">Fecha l&iacute;mite de pago::</span> <span id="spFechaLimite2">
                                    </span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="sep_a14">
                    </div>
                    <h4>
                        ¿Desde qu&eacute; cuenta deseas pagar?</h4>
                    <div class="inner-col-right">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc">
                                    <asp:DropDownList ID="ddlCuentaXXX" runat="server">
                                    </asp:DropDownList>
                                </dt>
                                <dd class="camp">
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="sep_a14">
                    </div>
                    <div class="w100 clearfix dlinline btns3">
                        <asp:Button ID="btnContinuar2XXX" OnClientClick="goStep3();return false" CssClass="btnContinuar"
                            Text="." runat="server" />
                        <asp:Button ID="btnCancelar2XXX" CssClass="btnCancelar" OnClientClick="goBack();return false;"
                            Text="." runat="server" PostBackUrl="../DV/COCUCLI.aspx" />
                    </div>
                </div>
            </div>
            <div id="dvGrPaso3">
                <h4>
                    Informaci&oacute;n del CIP</h4>
                <div class="dlgrid">
                    <div class="inner-col-right">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Servicio:</span> <span id="spServicio4"></span>
                                </dt>
                                <dd class="camp">
                                    <span class="bold">N° CIP:</span> <span id="spNroCIP4"></span>
                                </dd>
                            </dl>
                        </div>
                        <div class="even w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Concepto de pago:</span> <span id="spConceptoPago4">
                                </span></dt>
                                <dd class="camp">
                                    <span class="bold">Monto a pagar:</span> <span id="spMontoApagar4"></span>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Nombre de usuario:</span> <span id="spNombreUsuario4">
                                </span></dt>
                                <dd class="camp">
                                </dd>
                            </dl>
                        </div>
                        <div class="even w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Fecha de emisi&oacute;n:</span> <span id="spFechaEmision4">
                                </span></dt>
                                <dd class="camp">
                                    <span class="bold">Fecha l&iacute;mite de pago::</span> <span id="spFechaLimite4">
                                    </span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="sep_a14">
                    </div>
                    <h4>
                        Total a pagar</h4>
                    <div class="inner-col-right">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">Monto total:</span> <span id="spMontoApagarM2"></span>
                                </dt>
                                <dd class="camp">
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="sep_a14">
                    </div>
                    <h4>
                        Cuenta dinero virtual</h4>
                    <div class="inner-col-right">
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt40 dd50">
                                <dt class="desc"><span class="bold">N° Cuenta:</span> <span id="spNroCuenta"></span>
                                </dt>
                                <dd class="camp">
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="sep_a14">
                    </div>
                    <h4>
                        Validaci&oacute;n</h4>
                    <div class="inner-col-right inner-p">
                        <p>
                            Ha sido enviado a su correo un token de verificaci&oacute;n, el cu&aacute;l deber&aacute;
                            ingresarlo en la caja de abajo para validar el pago del CIP
                        </p>
                        <div class="w100 clearfix dlinline">
                            <dl class="clearfix dt30 dd20">
                                <dt class="desc">
                                    <asp:TextBox ID="txtNrotokenXXX" CssClass="tokenCamp" MaxLength="10" runat="server"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="ftbetxtNrotoken" runat="server" ValidChars="0123456789"
                                        TargetControlID="txtNrotokenXXX">
                                    </cc1:FilteredTextBoxExtender>
                                </dt>
                                <dd class="camp">
                                    <asp:Button ID="btnReenviarTokenXXX" OnClientClick="reenviarToken();return false;"
                                        CssClass="btnReenviar" Text="." runat="server" />
                                    <span id="spnInfotokenXXX"></span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="sep_a14">
                    </div>
                    <div class="w100 clearfix dlinline btns3">
                        <asp:UpdatePanel ID="upnlBotones" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnPagarCIPXXX" OnClientClick="return validToken();" CssClass="btnPagarCIP"
                                    runat="server" Text="Pagar CIP" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:Button ID="btnCancelar3XXX" OnClientClick="goBack2();return false;" CssClass="btnCancelar"
                            runat="server" Text="." />
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none;">
            <div id="msje">
                <div class="clearfix" id="overlay">
                    <p>
                        Se ha realizado el Pago del CIP Satisfactoriamente</p>
                    <div class="clear">
                    </div>
                    <div class="p_58">
                    </div>
                    <a class="btnAceptar" href="#"></a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
