﻿Imports System.Web.Services
Imports SPE.Entidades
Imports SPE.Web

Partial Class DV_PRPagarCIP
    Inherits SPE.Web.PaginaBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            txtNroCIP.Text = String.Empty

            CargarCombos()
            ltrTitular.Text = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Apellidos + ", " + CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Nombres
            ltrFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")
            ltrHora.Text = DateTime.Now.ToString("hh:mm tt")
        End If
    End Sub


    Private Sub CargarCombos()
        Dim cntrDV As New SPE.Web.CDineroVirtual
        Dim beParam As New SPE.Entidades.BECuentaDineroVirtual
        Dim listaCuenta As New System.Collections.Generic.List(Of SPE.Entidades.BECuentaDineroVirtual)
        beParam.IdEstado = CType(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado, Integer)
        beParam.IdUsuario = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
        listaCuenta = cntrDV.ConsultarCuentaDineroVirtualUsuario(beParam)
        If (listaCuenta.Count <= 0) Then
            JSMessageAlertWithRedirect("Validación", "Ud. no tiene Cuentas Activas.", "aa", "../DV/COCUCli.aspx")
            Return
        End If
        For Each item As SPE.Entidades.BECuentaDineroVirtual In listaCuenta
            ddlCuenta.Items.Insert(0, New ListItem(String.Concat("Nº ", item.Numero, " (", item.MonedaSimbolo, " ", item.Saldo.ToString("#0.00"), ") [", item.AliasCuenta, "]"), item.Numero))
            'String.Concat(item.IdCuentaDineroVirtual, "|", item.IdMoneda, "|", item.Saldo, "|", item.SaldoRetenido)))
        Next

    End Sub

    Protected Sub btnPagarCIP_Click(sender As Object, e As System.EventArgs) Handles btnPagarCIP.Click
        Dim movCuenta As New BEMovimientoCuenta
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual

        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        Dim InfoCuenta As BECuentaDineroVirtual = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(_3Dev.FW.Util.DataUtil.ObjectToInt64(hdfInfoStep2.Value.Split("|"c)(0)))

        Dim IdEstadoUsuario As Integer
        IdEstadoUsuario = New CDineroVirtual().ConsultaEstadoUsuarioByIdUsuario(IdUsuarioRealizaTransaccion)

        movCuenta.NroToken = txtNrotoken.Text
        movCuenta.IdCuentaVirtual = InfoCuenta.IdCuentaDineroVirtual
        movCuenta.IdTipoMovimiento = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP, Integer)
        movCuenta.Observacion = "Pago de CIP"
        movCuenta.IdTokenDineroVirtual = _3Dev.FW.Util.DataUtil.ObjectToInt64(hdfInfoStep2.Value.Split("|"c)(1))
        movCuenta.CodigoReferencia = txtNroCIP.Text
        movCuenta.ConceptoPago = hdfConcPag.Value
        movCuenta.IdUsuario = IdUsuarioRealizaTransaccion

        Dim rsult As String
        If InfoCuenta.IdUsuario = IdUsuarioRealizaTransaccion Then ' si la cuenta le pertenece al user
            rsult = oCDineroVirtual.RegistrarPagoCIP(movCuenta)
        Else
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
            rsult = Nothing
        End If

        If Not SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo = IdEstadoUsuario Then
            rsult = "Usuario no valido para realizar transacciones"
        End If



        Dim oBEOP As BEOrdenPago = oCDineroVirtual.ObtenerOrdenPagoCompletoPorNroOrdenPago(Convert.ToInt64(movCuenta.CodigoReferencia))
        Dim c As Decimal = 0

        Dim PuedePagarCIPDesdeMonedero As Boolean = False
        For Each t As BETipoOrigenCancelacion In oBEOP.ListaTipoOrigenCancelacion
            If t.IdTipoOrigenCancelacion = SPE.EmsambladoComun.ParametrosSistema.TipoOrigenCancelacion.Monedero Then ''Monedero Electronico
                PuedePagarCIPDesdeMonedero = True
            End If
        Next
        If Not PuedePagarCIPDesdeMonedero Then
            JSMessageAlert("Error: ", "Error Inesperado. al intentar saltar las validaciones", "aa")
            Return
        End If


        If (rsult Is Nothing) Then
            JSMessageAlert("Error: ", "Error al tratar de Pagar el CIP.", "aa")
        Else
            If (rsult.Trim().Equals("")) Then
                txtNrotoken.Enabled = btnReenviarToken.Enabled = btnPagarCIP.Enabled = btnCancelar3.Enabled = False
                JSMessageAlertWithRedirect("Info: ", "Se ha realizado el Pago del CIP Satisfactoriamente.", "bb", "../DV/COCUCli.aspx")
            Else
                JSMessageAlert("Validación:", rsult, "cc")
            End If
        End If
    End Sub

#Region "Page Metod"

#Region "Entity"
    Class OPJQ
        Private _total As Decimal
        Public Property Total As Decimal
            Get
                Return _total
            End Get
            Set(ByVal value As Decimal)
                _total = value
            End Set
        End Property

        'Private _IdEstado As Integer
        'Public Property IdEstado() As Integer
        '    Get
        '        Return _IdEstado
        '    End Get
        '    Set(ByVal value As Integer)
        '        _IdEstado = value
        '    End Set
        'End Property

        Private _NumeroOrdenPago As String
        Public Property NumeroOrdenPago() As String
            Get
                Return _NumeroOrdenPago
            End Get
            Set(ByVal value As String)
                _NumeroOrdenPago = value
            End Set
        End Property
        Private _SimboloMoneda As String
        Public Property SimboloMoneda() As String
            Get
                Return _SimboloMoneda
            End Get
            Set(ByVal value As String)
                _SimboloMoneda = value
            End Set
        End Property
        Private _DescripcionServicio As String
        Public Property DescripcionServicio As String
            Get
                Return _DescripcionServicio
            End Get
            Set(ByVal value As String)
                _DescripcionServicio = value
            End Set
        End Property


        Private _ConceptoPago As String
        Public Property ConceptoPago() As String
            Get
                Return _ConceptoPago
            End Get
            Set(ByVal value As String)
                _ConceptoPago = value
            End Set
        End Property

        'Private _IdEmpresa As Integer
        'Public Property IdEmpresa() As Integer
        '    Get
        '        Return _IdEmpresa
        '    End Get
        '    Set(ByVal value As Integer)
        '        _IdEmpresa = value
        '    End Set
        'End Property

        Private _UsuarioNombre As String
        Public Property UsuarioNombre() As String
            Get
                Return _UsuarioNombre
            End Get
            Set(ByVal value As String)
                _UsuarioNombre = value
            End Set
        End Property
        Private _UsuarioApellidos As String
        Public Property UsuarioApellidos() As String
            Get
                Return _UsuarioApellidos
            End Get
            Set(ByVal value As String)
                _UsuarioApellidos = value
            End Set
        End Property



        'Property OcultarEmpresa As Boolean

        Property MensajeError As String
        Property DescripcionCliente As String

        Property FechaEmision As Date

        Property FechaVencimiento As Date

    End Class

    Class Result
        Property MensajeError As String
    End Class
#End Region



    <System.Web.Services.WebMethod()>
    Public Shared Function ValidarCuentaVSOP(nroOP As Long, nroCuenta As Long) As Result
        Dim r As New Result

        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        Dim InfoCuenta As BECuentaDineroVirtual = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByNumero(nroCuenta)

        If IdUsuarioRealizaTransaccion <> InfoCuenta.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
        End If




        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEOP As New BEOrdenPago
        oBEOP = oCDineroVirtual.ObtenerOrdenPagoCompletoPorNroOrdenPago(nroOP)
        'oCuenta =oCDineroVirtual.ConsultarCuentaDineroVirtualPorNroCuenta(nroCuenta)

        r.MensajeError = String.Empty


        If oBEOP.IdMoneda <> InfoCuenta.IdMoneda Then
            r.MensajeError = "Validación: Debe seleccionar una Cuenta con el mismo tipo de Moneda de la Orden de Publicación."
            Return r
        End If

        If oBEOP.Total > InfoCuenta.Saldo Then
            r.MensajeError = "Validación: La Cuenta seleccionada no tiene saldo suficiente para realizar el Pago."
            Return r
        End If

        If oBEOP.Total > InfoCuenta.Saldo - InfoCuenta.SaldoRetenido Then
            r.MensajeError = "Validación: La Cuenta seleccionada tiene un monto de dinero congelado por una solicitud de retiro pendiente."
            Return r
        End If

        If InfoCuenta.IdEstado <> SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado Then
            r.MensajeError = "Validación: La Cuenta seleccionada esta deshabilitada"
            Return r
        End If


        Return r
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function ConsultarOP(nroOP As Long) As OPJQ
        'Public Shared Function ConsultarOP(nroOP As Long, nroCuenta As String) As OPJQ
        ''validar que el NCuenta pertenece al user logeado
        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        'Dim InfoCuenta As BECuentaDineroVirtual = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByNumero(nroCuenta)

        Dim IdEstadoUsuario As Integer
        IdEstadoUsuario = New CDineroVirtual().ConsultaEstadoUsuarioByIdUsuario(IdUsuarioRealizaTransaccion)



        Dim res As New OPJQ

        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEOP As New BEOrdenPago
        oBEOP = oCDineroVirtual.ObtenerOrdenPagoCompletoPorNroOrdenPago(nroOP)
        Dim c As Decimal = 0

        If oBEOP Is Nothing Then
            Return Nothing
        End If


        Dim PuedePagarCIPDesdeMonedero As Boolean = False

        For Each t As BETipoOrigenCancelacion In oBEOP.ListaTipoOrigenCancelacion
            If t.IdTipoOrigenCancelacion = SPE.EmsambladoComun.ParametrosSistema.TipoOrigenCancelacion.Monedero Then ''Monedero Electronico
                PuedePagarCIPDesdeMonedero = True
            End If
        Next
        If Not PuedePagarCIPDesdeMonedero Then
            res.MensajeError = "CIP no autorizado para cancelación por medio del Monedero Electrónico"
            Return res
        End If

        If Not SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo = IdEstadoUsuario Then
            res.MensajeError = "Usuario no valido para realizar transacciones"
            Return res
        End If

        With res
            .MensajeError = String.Empty
            .ConceptoPago = oBEOP.ConceptoPago
            .DescripcionServicio = oBEOP.DescripcionServicio
            .NumeroOrdenPago = oBEOP.NumeroOrdenPago
            .SimboloMoneda = oBEOP.SimboloMoneda
            .Total = oBEOP.Total
            .UsuarioApellidos = oBEOP.UsuarioApellidos
            .UsuarioNombre = oBEOP.UsuarioNombre
            .DescripcionCliente = oBEOP.DescripcionCliente
            .FechaEmision = oBEOP.FechaEmision
            .FechaVencimiento = oBEOP.FechaVencimiento
        End With
        'if (response.d.IdEstadoConciliacion != 0) {
        '          alert('Validación: No se puede realizar el Pago del CIP ingresado por que se encuentra Conciliado.');
        '          return;
        '      }

        If CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdEstado <> SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo Then
            res.MensajeError = "El usuario no debería poder consultar el CIP, si se encuentra bloqueado."
            Return res
        End If

        If Not CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).HabilitarMonedero Then
            res.MensajeError = "Usted no tiene habilitado el uso del Monedero Electronico"
            Return res
        End If

        If oBEOP.IdEstadoConciliacion > 0 Then
            res.MensajeError = "CIP no valido." '"Validación: No se puede realizar el Pago del CIP ingresado por que se encuentra Conciliado."
            Return res
        End If

        If oBEOP.IdEstado <> SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Generada Then
            If (oBEOP.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada) Then
                res.MensajeError = "Validación: No se puede realizar el Pago del CIP ingresado porque ya ha sido pagado."
            Else
                res.MensajeError = "CIP no valido."
            End If
        End If

        Return res
    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function GenerarToken2(nroCuenta As Long) As BETokenDineroVirtual
        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        Dim InfoCuenta As BECuentaDineroVirtual = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByNumero(nroCuenta)

        If IdUsuarioRealizaTransaccion <> InfoCuenta.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
        End If

        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEToken As New BETokenDineroVirtual
        oBEToken.IdCuenta = oCDineroVirtual.ConsultarCuentaDineroVirtualPorNroCuenta(nroCuenta.ToString).IdCuentaDineroVirtual
        oBEToken.IdTipoOperacion = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP, Integer)
        oBEToken = oCDineroVirtual.RegistrarTokenMonedero(oBEToken)
        oBEToken.Clean()
        Return oBEToken
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GenerarToken(idCuenta As Long) As BETokenDineroVirtual
        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        Dim cuenta As New BECuentaDineroVirtual
        cuenta.IdCuentaDineroVirtual = idCuenta
        Dim InfoCuenta As BECuentaDineroVirtual = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(cuenta)

        If IdUsuarioRealizaTransaccion <> InfoCuenta.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
        End If

        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEToken As New BETokenDineroVirtual
        oBEToken.IdCuenta = idCuenta
        oBEToken.IdTipoOperacion = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIP, Integer)
        oBEToken = oCDineroVirtual.RegistrarTokenMonedero(oBEToken)
        oBEToken.Clean()
        Return oBEToken
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function ReenviarToken(ByVal idToken As Long) As BETokenDineroVirtual

        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEToken As New BETokenDineroVirtual
        oBEToken = oCDineroVirtual.ConsultarTokenByID(idToken)
        ''validando pertenecia del token

        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        Dim cuenta As New BECuentaDineroVirtual
        cuenta.IdCuentaDineroVirtual = oBEToken.IdCuenta
        Dim InfoCuenta As BECuentaDineroVirtual = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(cuenta)

        If IdUsuarioRealizaTransaccion <> InfoCuenta.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
        End If

        oBEToken = oCDineroVirtual.ReenviarEmailToken(idToken)

        oBEToken.Clean()
        Return oBEToken
    End Function
#End Region

End Class
