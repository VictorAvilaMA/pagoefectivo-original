﻿<%@ Page Title="PagoEfectivo - Transferencia de dinero" Language="VB" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" Async="true" CodeFile="PRTransDiner.aspx.vb" Inherits="DV_PRTransDiner" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('#<%=txtMontoTransferir.ClientID%>').blur(function () {
                if ($('#<%=txtMontoTransferir.ClientID%>').val() != '')
                    $('#<%=txtMontoTransferir.ClientID%>').val(parseFloat($('#<%=txtMontoTransferir.ClientID%>').val()).toFixed(2));
            });
        });

        function validToken() {
            var valToken = $('#<%= txtToken.ClientID%>').val();
            if (valToken.length == 0)
                alertDiv('Validación: Debe Ingresar el Token enviado a su correo para continuar el proceso.');
            else
                if (valToken.length != 10)
                    alertDiv('Validaci&oacute;n: El Token debe contener 10  dígitos.');
            return valToken.length == 10;
        }

        function goBack() {
            $('#<%= ddlCuenta.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtMontoTransferir.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtNroCuentaDestino.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnContinuar1.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar.ClientID%>').attr('disabled', 'disabled');
            $('#<%= txtToken.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnReenviarToken.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnEnviarSolicitud.ClientID%>').attr('disabled', 'disabled');
            $('#<%= btnCancelar2.ClientID%>').attr('disabled', 'disabled');
            window.location.href = '../DV/COCUCLI.aspx';
        }

        function goStep2() {

            if ($('#<%=ddlCuenta.ClientID %>').val() == null) {
                alertDiv('Validación: Ud. no tiene cuenta de Dinero Virtual.');
                return false;
            }

            if ($('#<%= txtMontoTransferir.ClientID %>').val() == null) {
                alertDiv('Ingrese un monto valido.');
                return false;
            }

            if ($('#<%= txtNroCuentaDestino.ClientID %>').val() == null) {
                alertDiv('Ingrese una cuenta destino.');
                return false;
            }

            //            var n_cuenta = $('#<%= txtNroCuentaDestino.ClientID %>').val().split("");
            //            for (var i = 0; i < a.length; i++) {
            //                for (var i = 0; i < 10; i++) {
            //                    if (n_cuenta[i] !== obj) {
            //                        return false;   
            //                    }
            //                }
            //            }


            if ($('#<%= txtNroCuentaDestino.ClientID %>').val().length != 14) {
                alertDiv('La Cuenta destino tiene un formato no valido.');
                return false;
            }

            var NroCuentaOrigen = $('#' + '<%=ddlCuenta.ClientID%>').val();
            var MontoTransferir = parseFloat($('#<%= txtMontoTransferir.ClientID %>').val());
            var NroCuentaDestino = $('#<%=txtNroCuentaDestino.ClientID%>').val();

            $.ajax({
                type: "POST",
                url: "../DV/PRTransDiner.aspx/ConsultarCuentaDineroVirtualPorNroCuenta",
                data: "{nroCuentaOrigen: '" + NroCuentaOrigen + "',montoTranferir:" + MontoTransferir + ",nroCuentaDestino: '" + NroCuentaDestino + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d.MensajeError != '') {
                        alertDiv(response.d.MensajeError);
                        return false
                    } else {

                        $('#<%=hdfInfoStep1.ClientID %>').val(response.d.NroCuentaDestino + '|' + response.d.Token.IdTokenDineroVirtual);
                        $('#<%=hdfCtaDestino.ClientID %>').val(response.d.IdCuentaDestino)
                        $('#spnCuentaOrigen').text(response.d.NroCuentaOrigen);
                        $('#spnMontoTransferirOrigen').text(response.d.MontoTrasferir.toFixed(2));
                        var MiMontoComision = response.d.MontoComision;

                        $('#spnMontoComision').text(MiMontoComision.toFixed(2));
                        $('#spnCuentaDestino').text(response.d.NroCuentaDestino);
                        $('#spnTitularCuentaDestino').text(response.d.DestinatarioNombre);
                        $('#spnInfotoken').text(response.d.Token.Token);
                        $('#dvGrPaso1').css('display', 'none');
                        $('#dvGrPaso2').css('display', 'block');
                        if (response.d.FlagDiferentesCliente)
                            $('#<%= divObservacion.ClientID %>').show();
                    }
                },
                error: function (error) {
                    /*alert("error: " + error)*/window.location.href = '../DV/COCUCLI.aspx';
                }
            });

        }


        function reenviarToken() {
            $.ajax({
                type: "POST",
                url: "../DV/PRTransDiner.aspx/ReenviarToken",
                data: "{idToken: '" + $('#<%=hdfInfoStep1.ClientID%>').val().split("|")[1] + "'}",

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d.Token == '')
                        alertDiv('Info: Se reenvió el Código Token a su Correo.');
                    else {

                        if (response.d.IdTokenDineroVirtual != 0) {
                            $('#<%=hdfInfoStep1.ClientID %>').val($('#<%=ddlCuenta.ClientID%>').val().split('|')[0] + /*'|' + $('#<%=hdfCtaDestino.ClientID %>').val() +*/'|' + response.d.IdTokenDineroVirtual);
                        }
                        alertDiv('Info: ' + response.d.Token);
                    }
                },
                error: function (error) { /*alert("error: " + error)*/window.location.href = '../DV/COCUCLI.aspx'; }
            });
        }

    </script>
    <asp:HiddenField ID="hdfInfoStep1" runat="server" />
    <asp:HiddenField ID="hdfCtaDestino" runat="server" />
    <h2>
        Transferencia de Dinero</h2>
    <div class="conten_pasos" id="dvGrPaso1">
        <ul class="pasos">
            <li id="paso1" class="activa"><a href="" onclick="return false;">1</a> Datos de transferencia</li>
            <li id="paso2"><a href="" onclick="return false;">2</a> Confirmaci&oacute;n</li>
        </ul>
        <h5>
            ¿Desde qu&eacute; cuenta desea transferir?</h5>
        <asp:DropDownList ID="ddlCuenta" runat="server" CssClass="pasos_">
        </asp:DropDownList>
        <h5>
            ¿Qu&eacute; Monto desea Transferir?</h5>
        <asp:TextBox ID="txtMontoTransferir" MaxLength="14" CssClass="pasos_" runat="server"></asp:TextBox>
        <cc1:FilteredTextBoxExtender ID="ftetxtMontoTransferir" runat="server" ValidChars="0123456789."
            TargetControlID="txtMontoTransferir">
        </cc1:FilteredTextBoxExtender>
        <h5>
            ¿A qu&eacute; cuenta desea transferir?</h5>
        <asp:TextBox ID="txtNroCuentaDestino" MaxLength="14" CssClass="pasos_" runat="server"></asp:TextBox>
        <cc1:FilteredTextBoxExtender ID="ftbeTxtNroCuentaDestino" runat="server" ValidChars="0123456789"
            TargetControlID="txtNroCuentaDestino">
        </cc1:FilteredTextBoxExtender>
        <asp:Button ID="btnContinuar1" OnClientClick="goStep2();return false;" CssClass="input_azul"
            Text="Continuar" runat="server" />
        <asp:LinkButton ID="btnCancelar" OnClientClick="goBack();return false;" CssClass="cancela"
            Text="Cancelar" runat="server" />
    </div>
    <div id="dvGrPaso2" style="display: none">
        <div id="content">
            <div class="conten_pasos" id="other_back">
                <ul class="pasos">
                    <li id="paso4"><a>1</a> Datos de transferencia</li>
                    <li id="paso5" class="activa"><a>2</a> Confirmaci&oacute;n</li>
                </ul>
                <h5>
                    Informaci&oacute;n de solicitud</h5>
                <table id="Table1" class="grilla">
                    <thead>
                        <tr>
                            <th class="serv">
                                Mi Cuenta
                            </th>
                            <th class="fech" style="width: 91px;">
                                Monto a Transferir
                            </th>
                            <th class="fech" style="width: 91px;">
                                Comisi&oacute;n
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span id="spnCuentaOrigen"></span>
                            </td>
                            <td>
                                <span id="spnMontoTransferirOrigen"></span>
                            </td>
                            <td>
                                <span id="spnMontoComision"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h6>
                    Datos de cuenta a depositar</h6>
                <table id="Table2" class="grilla">
                    <thead>
                        <tr>
                            <th class="serv">
                                Cuenta Destino
                            </th>
                            <th class="fech">
                                Titular Cuenta Destino
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span id="spnCuentaDestino"></span>
                            </td>
                            <td>
                                <span id="spnTitularCuentaDestino"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="divObservacion" runat="server" style="display: none">
                    <h6>
                        Observaci&oacute;n acerca de la transferencia
                    </h6>
                    <ul class="datos_cip2">
                        <li class="t1">Obersvaci&oacute;n adicional: </li>
                        <li class="t4">
                            <asp:TextBox ID="txtObservacionAdicional" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label ID="lblmsjeContenido" runat="server"></asp:Label>
                        </li>
                    </ul>
                </div>
                <div style="clear: both">
                </div>
                <div id="other_back_fondo_blue">
                    <h6 class="barra_azul">
                        Validaci&oacute;n</h6>
                    <!--<p class="inan">-->
                    <p class="ubica">
                        <span class="colorazul">>></span> Ha sido enviado a su correo un token de verificaci&oacute;n,
                        el cual deber&aacute; ingresar en el campo de texto inferior.</p>
                    <p class="centrar_letra">
                        <!--class="inan"-->
                        <asp:TextBox ID="txtToken" CssClass="input_big2" MaxLength="10" runat="server"></asp:TextBox>
                        <!-- class="pasos_2"-->
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0123456789"
                            TargetControlID="txtToken">
                        </cc1:FilteredTextBoxExtender>
                        <asp:Button ID="btnReenviarToken" OnClientClick="reenviarToken();return false;" CssClass="input_azul45"
                            Text="Reenviar" runat="server" />
                        <span id="spnInfotoken"></span>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnEnviarSolicitud" OnClientClick="return validToken();" CssClass="input_azul"
                                    runat="server" Text="Continuar" />
                                <asp:LinkButton ID="btnCancelar2" OnClientClick="goBack();return false;" CssClass="cancela"
                                    Text="Cancelar" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
