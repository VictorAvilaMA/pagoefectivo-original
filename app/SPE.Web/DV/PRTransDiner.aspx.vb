﻿Imports System.Web.Services
Imports SPE.Entidades
Imports SPE.Web

Partial Class DV_PRTransDiner
    Inherits SPE.Web.PaginaBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txtObservacionAdicional.Attributes.Add("onkeypress", " ValidarCaracteres2(this, 2000, $('#" + lblmsjeContenido.ClientID + "'));")
        txtObservacionAdicional.Attributes.Add("onkeyup", " ValidarCaracteres2(this, 2000, $('#" + lblmsjeContenido.ClientID + "'));")
        If (Not Page.IsPostBack) Then
            CargarCombos()
        End If
    End Sub

    Private Sub CargarCombos()
        Dim cntrDV As New SPE.Web.CDineroVirtual
        Dim beParam As New SPE.Entidades.BECuentaDineroVirtual
        Dim listaCuenta As New System.Collections.Generic.List(Of SPE.Entidades.BECuentaDineroVirtual)
        beParam.IdEstado = CType(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado, Integer)
        beParam.IdUsuario = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
        listaCuenta = cntrDV.ConsultarCuentaDineroVirtualUsuario(beParam)
        For Each item As SPE.Entidades.BECuentaDineroVirtual In listaCuenta
            ddlCuenta.Items.Insert(0, New ListItem(String.Concat("Nº ", item.Numero, " (", item.MonedaSimbolo, " ", item.Saldo.ToString("#0.00"), ") [", item.AliasCuenta, "]"), item.Numero))
        Next
    End Sub

#Region "Page Method"
    Class Result
        Property MensajeError As String
        Property Token As BETokenDineroVirtual
        Property NroCuentaDestino As String
        Property NroCuentaOrigen As String
        Property MontoTrasferir As Decimal
        Property IdCuentaDestino As String
        Property MontoComision As Decimal
        Property DestinatarioNombre As String
        Property FlagDiferentesCliente As Boolean
    End Class

    Private Shared Function validar(InfoCuentaOrigen As BECuentaDineroVirtual, InfoCuentaDestino As BECuentaDineroVirtual, ByVal nroCuentaOrigen As String, montoTranferir As Decimal, ByVal nroCuentaDestino As String) As Result
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual()
        Dim Res As New Result
        Res.MensajeError = String.Empty

        If nroCuentaOrigen = nroCuentaDestino Then
            Res.MensajeError = "La Cuenta Origen debe de ser diferente a la cuenta de destino"
            Return Res
        End If

        '******************************************Valida Existencia de la Cuenta origen
        If InfoCuentaOrigen Is Nothing Then
            Res.MensajeError = "Cuenta origen invalida"
            Return Res
        End If

        '******************************************Valida si la cuenta la cuenta pertenece al usuario
        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        If IdUsuarioRealizaTransaccion <> InfoCuentaOrigen.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion.ToString)
        End If


        '******************************************Valida Existencia de la Cuenta destino
        If InfoCuentaDestino Is Nothing Then
            Res.MensajeError = "Cuenta destino invalida"
            Return Res
        End If

        '******************************************Valida que las cuentas esten habilitadas
        If (Not InfoCuentaOrigen.IdEstado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado) Then
            Res.MensajeError = "La Cuenta origen no se encuentra habilitada."
            Return Res
        End If

        If (Not InfoCuentaDestino.IdEstado = SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado) Then
            Res.MensajeError = "La Cuenta destino no se encuentra habilitada."
            Return Res
        End If

        If Not (oCDineroVirtual.ConsultaUsuarioByIdCuenta(InfoCuentaDestino.IdCuentaDineroVirtual) = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo) Then
            Res.MensajeError = "La cuenta de usuario destino no esta activa."
            Return Res
        End If

        If Not (oCDineroVirtual.ConsultaUsuarioByIdCuenta(InfoCuentaOrigen.IdCuentaDineroVirtual) = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo) Then
            Res.MensajeError = "La cuenta de usuario destino no esta activa."
            Return Res
        End If



        '******************************************Valida Misma Moneda
        If InfoCuentaOrigen.IdMoneda <> InfoCuentaDestino.IdMoneda Then
            Res.MensajeError = "Las monedas no coinciden"
            Return Res
        End If


        Dim infocomision As New BECuentaDineroVirtual
        infocomision = oCDineroVirtual.ConsultarCuentaDineroVirtualPorNroCuenta(InfoCuentaOrigen.Numero)

        '******************************************Valida Monto Minimo de transfercia
        If montoTranferir < infocomision.MontoMinTransferencia Then
            Res.MensajeError = "El monto minimo para una transferencia es de " & InfoCuentaDestino.MonedaSimbolo & " " & infocomision.MontoMinTransferencia
            Return Res
        End If

        '******************************************Valida que la cuenta tenga saldo suficiente
        Dim NewMonto As Decimal
        If (infocomision.IdTipoComision = SPE.EmsambladoComun.ParametrosSistema.TipoComisionME.Fijo) Then
            NewMonto = montoTranferir + infocomision.FactorComision
        Else
            NewMonto = montoTranferir * (1 + infocomision.FactorComision)
        End If

        If NewMonto > InfoCuentaOrigen.Saldo - InfoCuentaOrigen.SaldoRetenido Then
            Res.MensajeError = "Saldo insuficiente para realizar la transferencia requiere un saldo mayor a " & InfoCuentaOrigen.MonedaSimbolo & " " & NewMonto.ToString() & " y usted cuenta con saldo disponible de " & InfoCuentaOrigen.MonedaSimbolo & " " & InfoCuentaOrigen.Saldo - InfoCuentaOrigen.SaldoRetenido
            Return Res
        End If

        'completando informacion
        Res.NroCuentaDestino = InfoCuentaDestino.Numero
        Res.MontoTrasferir = InfoCuentaDestino.MonedaSimbolo & " " & montoTranferir
        Res.IdCuentaDestino = InfoCuentaDestino.IdCuentaDineroVirtual
        Res.MontoComision = InfoCuentaDestino.MonedaSimbolo & " " & NewMonto - montoTranferir
        Res.DestinatarioNombre = InfoCuentaDestino.ClienteNombre
        Res.NroCuentaOrigen = InfoCuentaOrigen.Numero
        Return Res
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function ConsultarCuentaDineroVirtualPorNroCuenta(ByVal nroCuentaOrigen As Decimal, montoTranferir As Decimal, ByVal nroCuentaDestino As String) As Result
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual()
        Dim InfoCuentaOrigen As BECuentaDineroVirtual = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(nroCuentaOrigen)
        Dim InfoCuentaDestino As BECuentaDineroVirtual = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(nroCuentaDestino)

        Dim Res As New Result
        Res = validar(InfoCuentaOrigen, InfoCuentaDestino, nroCuentaOrigen, montoTranferir, nroCuentaDestino)
        Res.FlagDiferentesCliente = (InfoCuentaOrigen.IdCliente <> InfoCuentaDestino.IdCliente)
        If Res.MensajeError = String.Empty Then
            Dim oBEToken As New BETokenDineroVirtual
            oBEToken.IdCuenta = InfoCuentaOrigen.IdCuentaDineroVirtual
            oBEToken.IdTipoOperacion = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaAOtroCta, Integer)
            oBEToken = oCDineroVirtual.RegistrarTokenMonedero(oBEToken)
            oBEToken.Clean()
            Res.Token = oBEToken
        Else
            Return Res
        End If

        Return Res
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GenerarToken2(nroCuenta As Long) As BETokenDineroVirtual
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual()
        Dim InfoCuenta As BECuentaDineroVirtual = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(nroCuenta)
        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()

        If IdUsuarioRealizaTransaccion <> InfoCuenta.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
        End If

        Dim oBEToken As New BETokenDineroVirtual

        oBEToken.IdCuenta = nroCuenta
        oBEToken.IdTipoOperacion = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.TransferenciaAOtroCta, Integer)
        oBEToken = oCDineroVirtual.RegistrarTokenMonedero(oBEToken)
        oBEToken.Clean()
        Return oBEToken
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function ReenviarToken(ByVal idToken As Long) As BETokenDineroVirtual

        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEToken As New BETokenDineroVirtual
        oBEToken = oCDineroVirtual.ConsultarTokenByID(idToken)
        ''validando pertenecia del token

        Dim IdUsuarioRealizaTransaccion As Integer = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario()
        Dim cuenta As New BECuentaDineroVirtual
        cuenta.IdCuentaDineroVirtual = oBEToken.IdCuenta
        Dim InfoCuenta As BECuentaDineroVirtual = New CDineroVirtual().ConsultarCuentaDineroVirtualUsuarioByCuentaId(cuenta)

        If IdUsuarioRealizaTransaccion <> InfoCuenta.IdUsuario Then
            Throw New Exception("Se intento vulnerar PE, user :" + IdUsuarioRealizaTransaccion)
        End If

        oBEToken = oCDineroVirtual.ReenviarEmailToken(idToken)
        oBEToken.Clean()
        Return oBEToken
    End Function
#End Region

    Protected Sub btnEnviarSolicitud_Click(sender As Object, e As System.EventArgs) Handles btnEnviarSolicitud.Click
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual()
        Dim InfoCuentaOrigen As BECuentaDineroVirtual = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(ddlCuenta.SelectedValue)
        Dim InfoCuentaDestino As BECuentaDineroVirtual = oCDineroVirtual.ConsultarCuentaDineroVirtualUsuarioByNumero(txtNroCuentaDestino.Text.Trim())

        Dim Res As New Result
        Res = validar(InfoCuentaOrigen, InfoCuentaDestino, ddlCuenta.SelectedValue, Convert.ToDecimal(txtMontoTransferir.Text), txtNroCuentaDestino.Text.Trim())
        If Not Res.MensajeError = String.Empty Then
            JSMessageAlert("Error: ", Res.MensajeError, "aa")
            Return
        End If

        Dim movCuenta As New BEMovimientoCuenta
        movCuenta.NroToken = txtToken.Text.Trim()
        movCuenta.IdCuentaVirtual = InfoCuentaOrigen.IdCuentaDineroVirtual
        movCuenta.IdUsuario = HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo).IdUsuario
        movCuenta.IdMovimientoAsociado = InfoCuentaDestino.IdCuentaDineroVirtual
        movCuenta.IdTokenDineroVirtual = _3Dev.FW.Util.DataUtil.ObjectToInt64(hdfInfoStep1.Value.Split("|"c)(1))
        movCuenta.Monto = _3Dev.FW.Util.DataUtil.ObjectToDecimal(txtMontoTransferir.Text.Trim())
        movCuenta.ObservacionTransferencia = txtObservacionAdicional.Text

        Dim rsult As String = oCDineroVirtual.RegistrarTransferenciaDinero(movCuenta)
        If (rsult Is Nothing) Then
            JSMessageAlert("Error: ", "Error al tratar de realizar la Transferencia de Dinero.", "aa")
        Else
            If (rsult.Trim().Equals("")) Then
                txtToken.Enabled = btnReenviarToken.Enabled = btnEnviarSolicitud.Enabled = btnCancelar2.Enabled = False
                JSMessageAlertWithRedirect("Info: ", "Se ha realizado la Transferencia de dinero Satisfactoriamente.", "bb", "../DV/COCUCli.aspx")
            Else
                JSMessageAlert("Validación:", rsult, "cc")
            End If
        End If
    End Sub


End Class
