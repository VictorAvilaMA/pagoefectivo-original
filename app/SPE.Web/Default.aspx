<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="default.aspx.vb" Inherits="_default" Title="PagoEfectivo" %>
<%@ Register Src="~/UC/UCSeccionSuscribete.ascx" TagName="UCSeccionSuscribete" TagPrefix="uc06" %>
<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <meta http-equiv="Content-language" content="cs" />
    <meta name="description" content=" " />
    <meta name="keywords" content=" " />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/ResourcesVideoPortada/js/swfobject/swfobject.js?<%# System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
        type="text/javascript"></script>
    <script type="text/javascript">
        function checkVideo() {
            if (!!document.createElement('video').canPlayType) {

                var vidTest = document.createElement("video");
                var oggTest = vidTest.canPlayType('video/ogg; codecs="theora, vorbis"');

                if (!oggTest) {
                    h264Test = vidTest.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
                    if (!h264Test) {
                        return false;
                    } else {
                        if (h264Test == "probably") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    if (oggTest == "probably") {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        if (checkVideo() != true) {
            var params = {},
		flashvars = {},
		baseUrl_ = '<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/';
            //alert(baseUrl_);
            params.allowscriptaccess = "always";
            params.allowfullscreen = "true";
            params.wmode = "opaque";
            params.flashvars = "file=" + baseUrl_ + "video/1.flv&repeat=no&stretching=fill&skin=" + baseUrl_ + "swf/md.swf&autostart=false&bufferlength=1&image=" + baseUrl_ + "video/1.jpg";
            swfobject.embedSWF("" + baseUrl_ + "swf/playertv.swf", "mediaplayer", "650", "390", "9", "" + baseUrl_ + "js/swfobject/expressInstall.swf", flashvars, params);
        }
    </script>
    <script type="text/javascript">
        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3
                });
                $('#ulMenu li').removeClass('active');
                $('#liInicio').addClass('active');
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-side-left">
        <%--<iframe id="iframe-video" width="650" height="390" src="https://www.youtube.com/embed/JTq4f1NCh38?showinfo=0&rel=0"
            frameborder="0" allowtransparency="0" scrolling="no" allowfullscreen></iframe>--%>
        <div id="mediaplayer" style="width: 650px; height: 390px; border: 0px;">
            <!--<video controls="controls" poster="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.jpg"
                height="390" width="650">-->
		        <!-- Firefox | Chrome -->
		        <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.webm" type='video/webm; codecs="vp8, vorbis"'/>
		        <!-- IE9 | iOS -->
		        <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
		        <!-- Android -->
		        <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.ogv" type='video/ogg; codecs="theora, vorbis"'/>
		        <!-- Other -->
                <!--<source src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/ResourcesVideoPortada/video/1.m4v"/>
	        <!--</video>-->
            <iframe width="650px" height="390px" src="https://www.youtube.com/embed/N1NJabZgUKQ?autoplay=0&loop=1&modestbranding=1&rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
         <div id="bg_pe_ch" style="margin: 10px 0;">
            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/img/Default/bg_pe_ch.jpg" width="650" height="160"></img>
        </div>
        <h2 class="title-seccion">
            <span>&nbsp;</span> Beneficios</h2>
        <div class="content-benefits">
            <div class="benefits first h146">
                <h3 class="title-benefits easy">
                    F�CIL</h3>
                <p>
                    S�lo tienes que acercarte a cualquiera de los centro autorizados y realizar tu pago.</p>
            </div>
            <div class="benefits h146">
                <h3 class="title-benefits secure">
                    SEGURO</h3>
                <ul>
                    <li>El pago puede ser realizado en Efectivo</li>
                    <li>Certificado SSL</li>
                    <li>Ingreso de Usuario y Contrase�a</li>
                    <li>C�digo de Identificaci�n de Pagos (CIP)</li>
                </ul>
            </div>
            <div class="benefits first h174">
                <h3 class="title-benefits flexible">
                    FLEXIBLE</h3>
                <ul>
                    <li>Permite pactar con los negocios los periodos de expiraci�n de la orden, de acuerdo
                        a sus necesidades.</li>
                    <li>Permite pactar la periodicidad de los dep�sitos a las cuentas del negocio en los
                        diferentes bancos que operan en Per�.</li>
                </ul>
            </div>
            <div class="benefits h174">
                <h3 class="title-benefits accessible">
                    ACCESIBLE</h3>
                <p>
                    A trav�s de Pago Efectivo, tendr�s mayor llegada a clientes potenciales, ya que
                    no es necesario ning�n producto bancario para que puedan realizar la transacci�n.</p>
            </div>
            <div class="clear">
            </div>
        </div>
        <asp:Panel ID="pnlComerciosAfiliados" runat="server">
            <h2 class="title-seccion">
                <span>&nbsp;</span> Comercios afiliados</h2>
            <div class="content-affiliates">
                <asp:Literal ID="ltlComerciosAfiliados" runat="server"></asp:Literal>
                <div class="clear">
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlProximosAfiliados" runat="server" Visible="false">
            <h2 class="title-seccion">
                <span>&nbsp;</span> Pr�ximos afiliados</h2>
            <div class="next-afiliates">
                <asp:Literal ID="ltlProximosAfiliados" runat="server"></asp:Literal>
                <div class="clear">
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="sidebar-rigth">
        <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
            <uc06:UCSeccionSuscribete ID="UCSeccionSuscribete1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
        <%--<uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />--%>
        <div id="divCentrosPagoAutorizados" runat="server">
        </div>
    </div>
</asp:Content>

