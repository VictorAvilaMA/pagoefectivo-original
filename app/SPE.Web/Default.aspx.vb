Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Entidades
Imports System.Collections.Generic
Imports SPE.Web.Seguridad
Imports _3Dev.FW.Entidades.Seguridad

Partial Class _default
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            CType(Master, MPBase).MarcarMenu("liinicio")

        End If

        Dim oBEEquipoRegistradoXusuarioVal As New BEEquipoRegistradoXusuario()

        'oBEEquipoRegistradoXusuarioVal.Usuario = CType(Master.Controls(1).Controls(5).Controls(1).FindControl("UserName"), TextBox).Text.Trim()
        oBEEquipoRegistradoXusuarioVal.Usuario = CType(Master.FindControl("form1").FindControl("dvlinklogin").FindControl("Login1").FindControl("UserName"), TextBox).Text.Trim()
        oBEEquipoRegistradoXusuarioVal.Token = CType(Master.FindControl("IdentificadorPC"), HiddenField).Value.Trim()
        If oBEEquipoRegistradoXusuarioVal.Token <> String.Empty Then
            Session("oBEEquipoRegistradoXusuarioVal") = oBEEquipoRegistradoXusuarioVal
        End If
        Dim CustomRoleProviderClient As New SPERoleProvider()
        If HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) IsNot Nothing Then
            RegPC()
            If Not Context.User Is Nothing Then
                If Context.User.IsInRole(RolAdministrador) Then
                    Response.Redirect("~/ADM/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolCajero) Then
                    Response.Redirect("~/ARE/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolSupervisor) Then
                    Response.Redirect("~/ARE/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolCliente) Then
                    If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
                        If CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).TieneInfoCompleta Then
                            Response.Redirect("~/CLI/PgPrl.aspx")
                        Else
                            Response.Redirect("CLI/ADAcCl.aspx")
                        End If
                    End If
                ElseIf Context.User.IsInRole(RolRepresentante) Then
                    Response.Redirect("~/ECO/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolJefeProducto) Then
                    Response.Redirect("~/JPR/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolTesoreria) Then
                    Response.Redirect("~/TES/CoTrPr.aspx")
                ElseIf Context.User.IsInRole(RolAdminMonederoElectronico) Then
                    Response.Redirect("~/DV/COCuentaAdmin.aspx")
                ElseIf (CustomRoleProviderClient.GetRolesForUser(Context.User.Identity.Name).Length > 0) Then
                    For Each item As BERolPagina In CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).PaginasAcceso
                        If Not String.IsNullOrEmpty(item.Url) Then
                            If Not item.Url.Contains("Principal.aspx") And Not item.Url.Contains("Login.aspx") And Not item.Url.Contains("Default.aspx") Then
                                Response.Redirect(item.Url)
                            End If
                        End If
                    Next
                    Response.Redirect("~/Principal.aspx")
                Else
                    Response.Redirect("~/SEG/sinautrz.aspx")
                End If
                ''Comen
            End If
        End If
        'If HttpContext.Current.Cache("CentrosPagoAutorizados") Is Nothing Then
        Using reader As New System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/CentrosPagoAutorizados.html"))
            Dim htmlCentrosPagoAutorizados As String = reader.ReadToEnd()
            htmlCentrosPagoAutorizados = Replace(htmlCentrosPagoAutorizados, "[urlBase]", ConfigurationManager.AppSettings("DominioFile"))
            htmlCentrosPagoAutorizados = Replace(htmlCentrosPagoAutorizados, "[StaticVersion]", ConfigurationManager.AppSettings("StaticVersion"))
            HttpContext.Current.Cache("CentrosPagoAutorizados") = htmlCentrosPagoAutorizados
            reader.Close()
        End Using
        'End If
        divCentrosPagoAutorizados.InnerHtml = HttpContext.Current.Cache("CentrosPagoAutorizados")

        Dim HTMLComerciosAfiliados As String = UtilServicios.ConsultarComerciosAfiliadosHTML(True, False, 3, anchoLogo:=201, altoLogo:=79)
        Dim HTMLProximosAfiliados As String = UtilServicios.ConsultarComerciosAfiliadosHTML(True, True, 5, False, 116, 70)
        pnlComerciosAfiliados.Visible = Not (HTMLComerciosAfiliados = "")
        ltlComerciosAfiliados.Text = HTMLComerciosAfiliados
        pnlProximosAfiliados.Visible = Not (HTMLProximosAfiliados = "")
        ltlProximosAfiliados.Text = HTMLProximosAfiliados
    End Sub

    Protected Sub RegPC()
        'Using oCComunVal As New SPE.Web.CComun()
        Dim oCComunVal As New SPE.Web.CComun()
        Dim oBEEquipoRegistradoXusuarioVal As New BEEquipoRegistradoXusuario()
        Dim oBEERXUResultado As New List(Of BEEquipoRegistradoXusuario)
        Dim Res As Boolean
        Dim FlagRegPc As Boolean
        Dim oBEEU As New BEEquipoRegistradoXusuario()
        oBEEU = CType(HttpContext.Current.Session("oBEEquipoRegistradoXusuarioVal"), BEEquipoRegistradoXusuario)

        If Not oBEEU Is Nothing Then
            oBEEquipoRegistradoXusuarioVal.Token = oBEEU.Token
            oBEEquipoRegistradoXusuarioVal.Usuario = oBEEU.Usuario
        End If
        oBEERXUResultado = oCComunVal.ValidarEquipoRegistradoXusuario(oBEEquipoRegistradoXusuarioVal)

        Res = oBEERXUResultado(0).Resultado
        FlagRegPc = oBEERXUResultado(0).FlagRegistarPc

        If FlagRegPc Then
            If oBEEquipoRegistradoXusuarioVal.Token.Length > 0 Then
                If Not Res Then
                    HttpContext.Current.Response.Redirect("PrRegDisp.aspx")
                End If
            End If
        End If
        'End Using
    End Sub

End Class
