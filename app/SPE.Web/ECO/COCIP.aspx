<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCIP.aspx.vb" Inherits="ECO_COCIP" Title="PagoEfectivo - Historial de Pagos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True" AsyncPostBackTimeout="900">
    </asp:ScriptManager>
    <h2>
        Consultar Hist&oacute;rico de CIPs
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B&uacute;squeda
        </h4>
        <asp:UpdatePanel ID="UpdatePanelOP" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul id="ulSeccionEmpresa" runat="server" class="datos_cip2" visible="false">
                    <li class="t1 "><span class="color">&gt;&gt; </span>Empresa : </li>
                    <li class="t2 ">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true">
                        </asp:DropDownList>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <ul class="datos_cip4 f0 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Servicio :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                </ul>
                <ul class="datos_cip4 f0 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                </ul>
                <%--<div class="divgrid">
                    <div class="w50">
                        <div class="cell">
                            <dl class="dt25 dd50">
                                <dt class="desc">
                                    <label>
                                        Servicio:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:DropDownList ID="ddlServicio" runat="server" CssClass="miniCamp">
                                    </asp:DropDownList>
                                </dd>
                            </dl>
                        </div>
                        <div class="cell">
                            <dl class="dt25 dd50">
                                <dt class="desc">
                                    <label>
                                        Estado:</label>
                                </dt>
                                <dd class="camp">
                                    <asp:DropDownList ID="ddlEstado" runat="server" CssClass="miniCamp">
                                    </asp:DropDownList>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>--%>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip4 f0 h0">
                    <li class="t1"><span class="color">&gt;&gt;</span> C.I.P. :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="corta" MaxLength="14"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" runat="server" FilterType="Numbers"
                            TargetControlID="txtNroOrdenPago">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Cliente :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCliente" runat="server" CssClass="corta" MaxLength="75"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCliente"
                            ErrorMessage="No se permiten tildes ni caracteres extra�os..." ValidationExpression="^[a-zA-Z�� ]*$">
                        </asp:RegularExpressionValidator>
                    </li>
                </ul>
                <ul class="datos_cip4 f0 h0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro Orden Comercio :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtOrderComercio" runat="server" CssClass="corta" MaxLength="30">
                        </asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Email :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="corta" MaxLength="100"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revMailAlternativo" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="No se permiten tildes ni caracteres extra�os..." ValidationExpression="^[-_a-zA-Z0-9��.@ ]*$">
                        </asp:RegularExpressionValidator>
                    </li>
                </ul>
                <div style="clear: both;">
                </div>
                <ul class="datos_cip2 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de C.I.P. del:</li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaInicio" Display="Dynamic" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Del' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Del' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaFin"
                                Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                Text="*"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaFin" Display="None" Visible="true" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Al' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div style="clear: both;">
                            </div>
                            <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                ControlToCompare="txtFechaFin" ControlToValidate="txtFechaInicio" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>
                        </p>
                    </li>
                    <li style="padding-left: 33px; width: auto;">
                        <b>* El reporte mostrar&aacute; la informaci&oacute;n actualizada despu&eacute;s de 10 minutos.</b>
                    </li>
                    <li class="t1"></li>
                    <li class="t2">
                        <asp:Label ID="lblMensaje" runat="server" Text="" ForeColor="Black"></asp:Label>
                    </li>
                </ul>
                
                
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                    PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                    PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <ul id="Fieldset2" class="datos_cip2">
            <li id="fsBotonera" class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Text="Exportar" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <li style="padding-left: 33px;margin-bottom:20px; width: auto;list-style-type:none;">
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" />
        </li>
        <asp:UpdatePanel ID="upResultadoMensaje" runat="server">
            <ContentTemplate>
                <h5 id="h5Resultado" runat="server">
                    <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                </h5>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" class="result">
            <ContentTemplate>
                <asp:GridView ID="grdResultado" runat="server" BackColor="White" AllowPaging="True"
                    AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                    CellPadding="3" OnRowDataBound="grdResultado_RowDataBound" CssClass="grilla"
                    EnableTheming="True" ShowFooter="False" AllowSorting="True" PageSize="20">
                    <Columns>
                        <asp:BoundField DataField="DescripcionServicio" HeaderText="Servicio" SortExpression="DescripcionServicio">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="N&#250;mero C.I.P." ItemStyle-HorizontalAlign="Center"
                            SortExpression="NumeroOrdenPago">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkNumeroOrdenPago" runat="server" CommandName="Detalle" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                    ToolTip="Ver informaci�n" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOrdenPago")%>'
                                    CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                <asp:HiddenField ID="hdfNumeroOrdenPago" runat="server" Value='<%# Eval("NumeroOrdenPago") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="N&#250;mero C.I.P." SortExpression="NumeroOrdenPago">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkNumeroOrdenPago" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOrdenPago")%>'
                                    PostBackUrl='<%# "~/ECO/DECIP.aspx?id=" + DataBinder.Eval(Container.DataItem, "NumeroOrdenPago") +"&idUri=1"%>'> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <%--IIf(DataBinder.Eval(Container.DataItem, "OrderIdComercio").ToString().Length > 12, DataBinder.Eval(Container.DataItem, "OrderIdComercio").ToString().Substring(0, 9)+ "...", DataBinder.Eval(Container.DataItem, "OrderIdComercio").ToString())--%>
                        <asp:TemplateField HeaderText="Nro Ord Comercio" SortExpression="OrderIdComercio">
                            <ItemTemplate>
                                <asp:Label ID="txtOrderIdComercio" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OrderIdComercio") %>'
                                    ToolTip='<%#DataBinder.Eval(Container.DataItem, "OrderIdComercio")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DescripcionCliente" HeaderText="Nombre Cliente" SortExpression="DescripcionCliente">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Email" SortExpression="ClienteEmailReducido">
                            <ItemTemplate>
                                <asp:Label ID="txtEmailCliente" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ClienteEmailReducido")%>'
                                    ToolTip='<%#DataBinder.Eval(Container.DataItem, "ClienteEmail")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total" SortExpression="Total">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "SimboloMoneda") + " " + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <%--  <asp:BoundField DataField="FechaEmision" HeaderText="Fec.Emision" SortExpression="FechaEmision">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>                        
                        <asp:BoundField DataField="FechaCancelacion" HeaderText="Fec.Cancelacion" SortExpression="FechaCancelacion">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaAnulada" HeaderText="Fec.Anulada" SortExpression="FechaAnulada">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaExpirada" HeaderText="Fec.Expirada" SortExpression="FechaExpirada">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FechaEliminado" HeaderText="Fec.Eliminado" SortExpression="FechaEliminado">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>--%>
                        <asp:TemplateField HeaderText="Fec. Emisi�n" SortExpression="FechaEmision">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEmision"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fec. Pago" SortExpression="FechaCancelacion">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaCancelacion"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:TemplateField HeaderText="Fec. Anulada" SortExpression="FechaAnulada">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaAnulada"))%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Fec. A Expirar" SortExpression="FechaExpirada">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaExpirada"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--          <asp:TemplateField HeaderText="Fec. Eliminado" SortExpression="FechaEliminado">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEliminado"))%>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    <EmptyDataTemplate>
                        No se encontraron registros.
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="grdResultado" EventName="PageIndexChanging">
                </asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
