Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil

Partial Class ECO_COCIP
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return grdResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    'add <PeruCom FaseIII>
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Private Property ActualIdEmpresaContratante() As Integer
        Get
            If (ViewState("IdEmpresaContratante") Is Nothing) Then
                ViewState("IdEmpresaContratante") = 0
            End If
            Return ViewState("IdEmpresaContratante")
        End Get
        Set(ByVal value As Integer)
            ViewState("IdEmpresaContratante") = value
        End Set
    End Property


    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.ExecuteSearchOnFirstLoad = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (Page.IsPostBack) Then
            VariableTransicion = ""
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNroOrdenPago)
            CargarDatos()
            If Session("obeOrdenPagoSearch") IsNot Nothing Then
                CreateSearchOfBusiness(CType(Session("obeOrdenPagoSearch"), BEOrdenPago))
                Session.Remove("obeOrdenPagoSearch")
            End If
        End If
        h5Resultado.Visible = False
    End Sub

    'CARGA DE DATOS
    Private Sub CargarDatos()
        Dim objParametro As New SPE.Web.CAdministrarParametro
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim objCRepresentante As New SPE.Web.CRepresentante

        Dim obeServicio As New BEServicio

        Dim obeUsuarioBase As New SPE.Entidades.BEUsuarioBase
        obeUsuarioBase.IdUsuario = UserInfo.IdUsuario
        Dim listEmpresas As List(Of BEEmpresaContratante) = objCRepresentante.ConsultarEmpresasPorIdUsuario(obeUsuarioBase)

        'ESTADO Código de Identificación de Pago
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoOrdenPago), "Descripcion", "Id", "::: Todos :::")
        obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario

        If listEmpresas.Count > 1 Then
            ulSeccionEmpresa.Visible = True
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlEmpresa, objCRepresentante.ConsultarEmpresasPorIdUsuario(obeUsuarioBase), "RazonSocial", "IdEmpresaContratante", "::: Todos :::")
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::: Todos :::")
        Else
            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
            Dim listBEServicio As List(Of BEServicio) = objCAdministrarServicio.ConsultarServiciosPorIdUsuario(obeServicio)
            If (listBEServicio.Count > 0) Then
                ActualIdEmpresaContratante = listBEServicio(0).IdEmpresaContratante
            Else
                ActualIdEmpresaContratante = -1
            End If
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, listBEServicio, "Nombre", "IdServicio", "::: Todos :::")
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlEmpresa, New List(Of BEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::: Todos :::")
        End If
        InicializarFecha()
        txtNroOrdenPago.Focus()
    End Sub

    Private Sub InicializarFecha()
        'FECHAS

        txtFechaInicio.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaFin.Text = Today.ToShortDateString
    End Sub

    'VALIDAMOS LAS FECHAS
    Private Function ValidarFecha() As Boolean
        If Not IsDate(txtFechaFin.Text) OrElse _
            Not IsDate(txtFechaInicio.Text) Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Overrides Sub OnClear()
        txtNroOrdenPago.Text = String.Empty
        txtCliente.Text = String.Empty
        txtEmail.Text = String.Empty
        ddlServicio.SelectedIndex = 0
        grdResultado.DataSource = Nothing
        grdResultado.DataBind()
        ddlEstado.SelectedIndex = 0
        InicializarFecha()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        txtOrderComercio.Text = String.Empty
        h5Resultado.Visible = False

        MyBase.OnClear()
    End Sub

    ''LIMPIAR
    'Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click

    'End Sub

    ''PAGINADO DE LA GRILLA
    'Protected Sub grdResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    grdResultado.PageIndex = e.NewPageIndex
    '    ListarOrdenesDePago()
    'End Sub

    Private Function ObtenerMensajeResultado(ByVal cantidad As Integer) As String
        Dim strResultado As New StringBuilder()
        strResultado.Append(String.Format("Resultados: {0} registro(s) ", cantidad))
        'If (ddlServicio.SelectedIndex > 0) Then
        '    strResultado.Append(String.Format("para el servicio {0} ", ddlServicio.SelectedItem.Text))
        'End If
        'strResultado.AppendLine(String.Format("entre el {0} y {1} ", txtFechaInicio.Text.TrimEnd(), txtFechaFin.Text.TrimEnd()))
        Return strResultado.ToString()
    End Function
    Public Overrides Function AllowToSearch() As Boolean
        Return ValidarFecha() And MyBase.AllowToSearch()
    End Function
    'BUSCAR
    'Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
    '    '
    '    If ValidarFecha() = True Then
    '        '
    '        ListarOrdenesDePago()
    '        '
    '    End If
    '    '
    'End Sub

    'LISTAR ORDENES DE PAGO

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeOrdenPago As New BEOrdenPago
        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
        obeOrdenPago.IdEmpresa = IIf(ddlEmpresa.SelectedIndex > 0, ddlEmpresa.SelectedValue, 0)
        obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
        obeOrdenPago.UsuarioNombre = txtCliente.Text.TrimEnd()
        obeOrdenPago.UsuarioEmail = txtEmail.Text.Trim
        If ddlEstado.SelectedIndex = 0 Then obeOrdenPago.IdEstado = 0 Else obeOrdenPago.IdEstado = StringToInt(ddlEstado.SelectedValue)
        obeOrdenPago.FechaCreacion = StringToDateTime(txtFechaInicio.Text)
        obeOrdenPago.FechaActualizacion = StringToDateTime(txtFechaFin.Text)
        obeOrdenPago.OrderIdComercio = txtOrderComercio.Text
        obeOrdenPago.OrderBy = SortExpression
        obeOrdenPago.IsAccending = SortDir
        obeOrdenPago.IdUsuarioCreacion = UserInfo.IdUsuario

        Return obeOrdenPago
    End Function
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        h5Resultado.Visible = True
        If Not ResultList Is Nothing Then
            'upd <PeruCom FaseIII>
            lblResultado.Text = ObtenerMensajeResultado(If(ResultList.Count > 0, ResultList.First().TotalPageNumbers, 0))
        Else
            lblResultado.Text = ObtenerMensajeResultado(0)
        End If
    End Sub
    'Private Sub ListarOrdenesDePago()
    '    Try
    '        Dim objCadministrarOrdenPago As New SPE.Web.COrdenPago
    '        Dim obeOrdenPago As New BEOrdenPago
    '        Dim listOrdenPago As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
    '        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
    '        obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
    '        obeOrdenPago.UsuarioNombre = txtCliente.Text.TrimEnd()
    '        If ddlEstado.SelectedIndex = 0 Then obeOrdenPago.IdEstado = 0 Else obeOrdenPago.IdEstado = StringToInt(ddlEstado.SelectedValue)
    '        obeOrdenPago.FechaCreacion = StringToDateTime(txtFechaDe.Text)
    '        obeOrdenPago.FechaActualizacion = StringToDateTime(txtFechaA.Text)

    '        obeOrdenPago.OrderBy = SortExpression
    '        obeOrdenPago.IsAccending = SortDir

    '        listOrdenPago = objCadministrarOrdenPago.SearchByParametersOrderedBy(obeOrdenPago, SortExpression, SortDir)
    '        grdResultado.DataSource = listOrdenPago
    '        grdResultado.DataBind()

    '        If Not listOrdenPago Is Nothing Then
    '            lblResultado.Text = ObtenerMensajeResultado(listOrdenPago.Count)
    '        Else
    '            lblResultado.Text = ObtenerMensajeResultado(0)
    '        End If
    '    Catch ex As Exception
    '        _3Dev.FW.Web.Log.Logger.LogException(ex)
    '        Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
    '    End Try
    '    '
    'End Sub

    Protected Sub grdResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdResultado.RowDataBound
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "DescripcionEstado", _
        "Anulada", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
        Dim txtOrderIdComercio As Label = CType(e.Row.FindControl("txtOrderIdComercio"), Label)
        If (txtOrderIdComercio IsNot Nothing) Then
            If txtOrderIdComercio.Text.Length > 12 Then
                txtOrderIdComercio.Text = txtOrderIdComercio.Text.Substring(0, 9) + "..."
            End If
        End If
    End Sub

    'Protected Sub grdResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    '    '
    '    SortExpression = e.SortExpression
    '    If (SortDir.Equals(SortDirection.Ascending)) Then
    '        SortDir = SortDirection.Descending
    '    Else
    '        SortDir = SortDirection.Ascending
    '    End If
    '    ListarOrdenesDePago()
    '    '
    'End Sub

    Protected Sub txtNroOrdenPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If Right("00000000000000" + txtNroOrdenPago.Text, 14) = "00000000000000" Then
            txtNroOrdenPago.Text = ""
        Else
            txtNroOrdenPago.Text = Right("00000000000000" + txtNroOrdenPago.Text, 14)
        End If

    End Sub
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click

        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            LblMessageMaintenance.Text = "Esta operación puede tardar unos minutos dependiendo del rango de la consulta"
            Dim dtfechainicio As DateTime = StringToDateTime(txtFechaInicio.Text)
            Dim dtfechafin As DateTime = StringToDateTime(txtFechaFin.Text)
            Dim strFechaInicio As String = dtfechainicio.ToString("dd/MM/yyyy")
            Dim strFechaFin As String = dtfechafin.ToString("dd/MM/yyyy")
            Dim idestado As Integer = 0
            If ddlEstado.SelectedIndex = 0 Then idestado = 0 Else idestado = StringToInt(ddlEstado.SelectedValue)
            'Response.Redirect(String.Format("~/Reportes/RptCOCIPDet.aspx?CIP={0}&IdS={1}&C={2}&E={3}&IdE={4}&FIni={5}&FFin={6}&Com={7}&flag={8}", _
            '                                txtNroOrdenPago.Text.TrimEnd(), StringToInt(ddlServicio.SelectedValue), txtCliente.Text.TrimEnd(), _
            '                                idestado, ActualIdEmpresaContratante, strFechaInicio, strFechaFin, txtOrderComercio.Text, 0))
            Dim cuentas As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Dim FechaIniCorta As String = strFechaInicio
            Dim FechaFinCorta As String = strFechaFin
            Dim listaOrdenPublicacion As New List(Of BEOrdenPago)
            Dim be As New _3Dev.FW.Entidades.BusinessEntityBase
            Using cOrdenPago As New SPE.Web.COrdenPago()
                Dim obeOrdenPago As New BEOrdenPago
                obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
                obeOrdenPago.IdEmpresa = IIf(ddlEmpresa.SelectedIndex > 0, ddlEmpresa.SelectedValue, 0)
                obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
                obeOrdenPago.UsuarioNombre = txtCliente.Text.TrimEnd()
                obeOrdenPago.IdEstado = idestado
                obeOrdenPago.FechaCreacion = StringToDateTime(strFechaInicio)
                obeOrdenPago.FechaActualizacion = StringToDateTime(strFechaFin)
                obeOrdenPago.OrderIdComercio = txtOrderComercio.Text
                obeOrdenPago.IdUsuarioCreacion = UserInfo.IdUsuario
                cuentas = cOrdenPago.SearchByParameters(obeOrdenPago)
            End Using
            For index = 0 To cuentas.Count - 1
                listaOrdenPublicacion.Add(CType(cuentas(index), BEOrdenPago))
            Next
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("CIP", txtNroOrdenPago.Text.TrimEnd()))
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", FechaIniCorta))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", FechaFinCorta))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", ddlEstado.SelectedItem.Text.ToString))
            UtilReport.ProcesoExportarGenerico(listaOrdenPublicacion, Page, "EXCEL", "xls", "Consultar CIP - ", parametros, "RptCOCIPDet.rdlc")
            LblMessageMaintenance.Text = ""
        End If
    End Sub

    Protected Sub grdResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdResultado.RowCommand
        Try
            Dim reg As Integer
            Dim NroOP As String
            reg = grdResultado.Rows(e.CommandArgument).RowIndex
            NroOP = CType(grdResultado.Rows(reg).FindControl("hdfNumeroOrdenPago"), HiddenField).Value
            VariableTransicion = NroOP & "|" & "1"
            Session("obeOrdenPagoSearch") = CreateBusinessEntityForSearch()
            Select Case e.CommandName
                Case "Detalle"
                    Response.Redirect("~/ECO/DECIP.aspx")
            End Select
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CreateSearchOfBusiness(ByVal obeOrdenPago As BEOrdenPago)
        txtNroOrdenPago.Text = obeOrdenPago.NumeroOrdenPago
        ActualIdEmpresaContratante = obeOrdenPago.IdEmpresa
        If obeOrdenPago.IdServicio = 0 Then ddlServicio.SelectedIndex = 0 Else ddlServicio.SelectedValue = obeOrdenPago.IdServicio.ToString()
        txtCliente.Text = obeOrdenPago.UsuarioNombre
        txtEmail.Text = obeOrdenPago.UsuarioEmail
        If obeOrdenPago.IdEstado = 0 Then ddlEstado.SelectedIndex = 0 Else ddlEstado.SelectedValue = obeOrdenPago.IdEstado.ToString()
        txtFechaInicio.Text = obeOrdenPago.FechaCreacion.ToString("dd/MM/yyyy")
        txtFechaFin.Text = obeOrdenPago.FechaActualizacion.ToString("dd/MM/yyyy")
        txtOrderComercio.Text = obeOrdenPago.OrderIdComercio
        SortExpression = obeOrdenPago.OrderBy
        SortDir = obeOrdenPago.IsAccending
        BtnSearch_Click(Nothing, Nothing)
    End Sub
    Protected Sub ddlEmpresa_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        Using objCAdministrarServicio As New SPE.Web.CServicio
            If String.IsNullOrEmpty(ddlEmpresa.SelectedValue) Then
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::: Todos :::")
            Else
                Dim obeServicio As New BEServicio
                obeServicio.IdServicio = CInt(ddlEmpresa.SelectedValue)
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, objCAdministrarServicio.ConsultarServicioPorIdEmpresa(obeServicio), "Nombre", "IdServicio", "::: Todos :::")
            End If
        End Using
    End Sub
End Class
