<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COPaSer.aspx.vb" Inherits="ECO_COPaSer" Title="PagoEfectivo - Consultar CIPs Pagados" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>



    <h2>
        Consultar CIPs Pagados
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B&uacute;squeda
        </h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" style="clear: both">
            <ContentTemplate>
                <ul id="ulSeccionEmpresa" runat="server" class="datos_cip2" visible="false">
                    <li class="t1 "><span class="color">&gt;&gt; </span>Empresa : </li>

                    <li class="t2 ">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true"></asp:DropDownList>
                    </li>
                </ul>
                <div style="clear:both"></div>
                <ul class="datos_cip4 f0 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Servicio :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="corta" 
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Origen Cancelaci&oacute;n :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoOrigenCancelacion" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Moneda :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Cliente :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNCliente" runat="server" CssClass="corta" MaxLength="100"></asp:TextBox>
                    </li>
                </ul>
                <ul class="datos_cip4 f0 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro Orden Comercio :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtOrderComercio" runat="server" CssClass="corta" MaxLength="64"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro. Operaci&oacute;n :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtPNroOperacion" runat="server" CssClass="corta"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> C.I.P. :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroOrden" runat="server" CssClass="corta" MaxLength="14"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Email :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCliente" runat="server" CssClass="corta" MaxLength="100"></asp:TextBox>
                    </li>
                </ul>
                <div style="clear:both"></div>
                <ul class="datos_cip2 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de C.I.P. del:</li>
                    <li class="t2" style="line-height: 1px; z-index: 200px;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaInicio" Display="Dynamic" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Del' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Del' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaFin"
                                Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                Text="*"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaFin" Display="None" Visible="true" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Al' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div style="clear: both;">
                            </div>
                            <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                ControlToCompare="txtFechaFin" ControlToValidate="txtFechaInicio" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>
                            <p>
                            </p>                          
                        </p>
                    </li>
                </ul>
                <asp:ValidationSummary ID="ValidationSummary" runat="server" />
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                    PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                    PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtNCliente" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtNCliente">
                </cc1:FilteredTextBoxExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_TxtNroOrden" runat="server" ValidChars="0123456789' "
                    TargetControlID="txtNroOrden">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both;">
        </div>
        <ul class="datos_cip2">
            <li id="fsBotonera" class="complet">
                <asp:Button ID="btnBuscar" runat="server" style="margin-left:120px" CssClass="input_azul5" Text="Buscar" />

                <asp:Button ID="btnLimpiar" CausesValidation="false" runat="server" CssClass="input_azul4"
                    Text="Limpiar" />

                <asp:Button ID="btnExportarExcel" CausesValidation="false" runat="server" CssClass="input_azul4"
                    Text="Exportar" />

                 
                 <%--boton detalle--%>
         
                       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                              <asp:Button ID="btnExportarExcelFormulario" runat="server" CssClass="input_azul4" Text="Exportar Detalle" OnClientClick="return DateRangeValidation();" />                                     
                       
                        </ContentTemplate>

                    <Triggers>

                         <%--<asp:AsyncPostBackTrigger ControlID="ddlServicio" EventName="SelectedIndexChanged" /> 
                         <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />  --%>
                       
                         <asp:PostBackTrigger ControlID="btnExportarExcelFormulario"  /> 
                     
                    </Triggers>
                </asp:UpdatePanel>
           

                  <%--  boton detalle--%>

               
            </li>
        </ul>
        <div style="clear: both;" />
        <asp:UpdatePanel ID="upResultadoMensaje" runat="server">
            <ContentTemplate>
                <h5 id="h5Resultado" runat="server">
                    <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                </h5>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanelResultado" runat="server" class="result">
            <ContentTemplate>
                <div style="clear: both;">
                </div>
                <asp:GridView ID="gvResultado" ShowFooter="False" runat="server" AllowSorting="True"
                    AllowPaging="True" AutoGenerateColumns="False" CssClass="grilla" PageSize="10">
                    <Columns>
                        <asp:TemplateField HeaderText="N&#250;mero C.I.P." ItemStyle-HorizontalAlign="Center"
                            SortExpression="NumeroOrdenPago">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkNumeroOrdenPago" runat="server" CommandName="Detalle" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"images/lupa.gif" %>'
                                    ToolTip="Ver informaci�n" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOrdenPago")%>'
                                    CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                <asp:HiddenField ID="hdfNumeroOrdenPago" runat="server" Value='<%# Eval("NumeroOrdenPago") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Monto" SortExpression="Total">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "SimboloMoneda") + " " + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="OrderIdComercio" HeaderText="Nro.Ord.Comercio" SortExpression="OrderIdComercio">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DescripcionCliente" SortExpression="DescripcionCliente"
                            HeaderText="Cliente" />
                        <asp:TemplateField HeaderText="Email.Cliente" SortExpression="ClienteEmail">
                            <ItemTemplate>
                                <asp:Label ID="txtEmailCliente" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ClienteEmail")%>'
                                    ToolTip='<%#DataBinder.Eval(Container.DataItem, "ClienteEmail")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha.Emisi�n" SortExpression="FechaEmision">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEmision"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha.Cancelaci�n" SortExpression="FechaCancelacion">
                            <ItemTemplate>
                                <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaCancelacion"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DescripcionAgencia" HeaderText=" Agencia" SortExpression="DescripcionAgencia">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle CssClass="cabecera" />
                    <EmptyDataTemplate>
                        No se encontraron registros.
                    </EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script type = "text/javascript">
        function DateRangeValidation() {
            var dateFrom = document.getElementById("<%=txtFechaInicio.ClientID %>").value;
            var dateTo = document.getElementById("<%=txtFechaFin.ClientID %>").value;

            var dateFromValues = dateFrom.split("/");
            var dateToValues = dateTo.split("/");

            var stryear1 = dateFromValues[2];
            var strmth1 = dateFromValues[1];
            var strday1 = dateFromValues[0];
            var d1 = new Date(stryear1, strmth1, strday1);

            var stryear2 = dateToValues[2];
            var strmth2 = dateToValues[1];
            var strday2 = dateToValues[0];
            var d2 = new Date(stryear2, strmth2, strday2);

            if (d1 > d2) {
                alert("Por favor ingrese un rango de fechas valido.");
                return false;
            }

            var nYears = d2.getUTCFullYear() - d1.getUTCFullYear();
            var nMonths = d2.getUTCMonth() - d1.getUTCMonth() + (nYears !== 0 ? nYears * 12 : 0);

            if (nMonths > 3) {
                alert("El rango m�ximo de fechas permitido es de 3 meses.");
                return false;
            }

            return true;
        }
    </script>

</asp:Content>
