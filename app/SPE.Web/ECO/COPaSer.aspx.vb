Imports System.Data
Imports SPE.Entidades
Imports System.Linq
Imports _3Dev.FW.Util.DataUtil
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades

Imports SPE.Logging

Partial Class ECO_COPaSer
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)

    Private ReadOnly _logger As ILogger = New Logger()

    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property
    'add <PeruCom FaseIII>
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim objbePagoServicio As New SPE.Entidades.BEPagoServicio()
        objbePagoServicio.DescripcionAgencia = String.Empty
        objbePagoServicio.DescripcionCliente = txtCliente.Text
        objbePagoServicio.DescripcionServicio = ddlServicio.SelectedItem.Text
        objbePagoServicio.NumeroOrdenPago = txtNroOrden.Text
        objbePagoServicio.FechaCancelacion = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaFin.Text)
        objbePagoServicio.FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaInicio.Text)
        objbePagoServicio.IdServicio = _3Dev.FW.Util.DataUtil.StringToInt(ddlServicio.SelectedValue)
        objbePagoServicio.OrderIdComercio = txtOrderComercio.Text
        objbePagoServicio.IdUsuarioCreacion = UserInfo.IdUsuario
        objbePagoServicio.UsuarioNombre = txtNCliente.Text
        'Add by jonathan - requerimiento filtro por empresa
        objbePagoServicio.IdEmpresa = CInt(IIf(String.IsNullOrEmpty(ddlEmpresa.SelectedValue), 0, ddlEmpresa.SelectedValue))

        If ddlTipoOrigenCancelacion.SelectedValue.Trim <> "" Then
            objbePagoServicio.IdTipoOrigenCancelacion = Convert.ToInt32(ddlTipoOrigenCancelacion.SelectedValue)
        Else
            objbePagoServicio.IdTipoOrigenCancelacion = 0
        End If

        If ddlMoneda.SelectedValue.Trim <> "" Then
            objbePagoServicio.IdMoneda = Convert.ToInt32(ddlMoneda.SelectedValue)
        Else
            objbePagoServicio.IdMoneda = 0
        End If

        Return objbePagoServicio
    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As GridViewRow)
    End Sub

    Sub CargarTipoOrigenCancelacion()
        Dim objTipoOrigenCancelacion As New SPE.Web.CAdministrarComun
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlTipoOrigenCancelacion, objTipoOrigenCancelacion.ConsultarTipoOrigenCancelacion(), "Descripcion", "IdTipoOrigenCancelacion", "::: Todos :::")
    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.ExecuteSearchOnFirstLoad = False
    End Sub

    Public Overrides Function GetMethodSearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Return CType(ControllerObject, SPE.Web.COrdenPago).ConsultarPagosServicios(be, SortExpression, SortDir = SortDirection.Ascending)
    End Function

    Private Sub InicializarFechas()
        txtFechaInicio.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'DateTime.Now.ToShortDateString()
        txtFechaFin.Text = DateTime.Now.ToShortDateString()
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        Page.SetFocus(ddlServicio)
        Page.Form.DefaultButton = btnBuscar.UniqueID
        InicializarFechas()
        'SERVICIO
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        Dim objCRepresentante As New SPE.Web.CRepresentante
        Dim obeServicio As New BEServicio
        Dim obeUsuarioBase As New SPE.Entidades.BEUsuarioBase
        obeUsuarioBase.IdUsuario = UserInfo.IdUsuario
        Dim listEmpresas As List(Of BEEmpresaContratante) = objCRepresentante.ConsultarEmpresasPorIdUsuario(obeUsuarioBase)

        If listEmpresas.Count > 1 Then
            ulSeccionEmpresa.Visible = True
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlEmpresa, objCRepresentante.ConsultarEmpresasPorIdUsuario(obeUsuarioBase), "RazonSocial", "IdEmpresaContratante", "::: Todos :::")
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::: Todos :::")
        Else
            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlServicio, objCAdministrarServicio.ConsultarServiciosPorIdUsuario(obeServicio), "Nombre", "IdServicio", "::: Todos :::")
        End If
        'obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
        'ddlServicio.DataTextField = "Nombre" : ddlServicio.DataValueField = "IdServicio" : ddlServicio.DataSource = objCAdministrarServicio.ConsultarServiciosPorIdUsuario(obeServicio) : ddlServicio.DataBind()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::: Todos :::")
        CargarTipoOrigenCancelacion()
    End Sub

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        h5Resultado.Visible = True
        If Not ResultList Is Nothing Then
            'upd <PeruCom FaseIII>
            'lblResultado.Text = String.Format("Resultados: {0} registro(s) para el servicio {1} entre el {2} y {3}", If(ResultList.Count > 0, ResultList.First().TotalPageNumbers, 0), ddlServicio.SelectedItem.Text, txtFechaInicio.Text, txtFechaFin.Text)
            lblResultado.Text = String.Format("Resultados: {0} registro(s)", If(ResultList.Count > 0, ResultList.First().TotalPageNumbers, 0), ddlServicio.SelectedItem.Text, txtFechaInicio.Text, txtFechaFin.Text)
        Else
            'lblResultado.Text = String.Format("Resultados: 0 registros para el servicio {1} entre el {2} y {3}", "", ddlServicio.SelectedItem.Text, txtFechaInicio.Text, txtFechaFin.Text)
            lblResultado.Text = String.Format("Resultados: 0 registros", "", ddlServicio.SelectedItem.Text, txtFechaInicio.Text, txtFechaFin.Text)
        End If
        NumeroOPCompletado(Me.txtNroOrden.Text)
    End Sub

    Sub NumeroOPCompletado(ByVal pNumeroOrdenPago As String)
        If (pNumeroOrdenPago <> "" And pNumeroOrdenPago <> "0") Then
            pNumeroOrdenPago = "0000000000000" + pNumeroOrdenPago
            pNumeroOrdenPago = pNumeroOrdenPago.Substring(pNumeroOrdenPago.Length - 14, 14)
            Me.txtNroOrden.Text = pNumeroOrdenPago
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            VariableTransicion = ""
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(ddlServicio)

            btnExportarExcelFormulario.Visible = False

        End If
        h5Resultado.Visible = False


    End Sub

    Public Overrides Sub OnClear()
        MyBase.OnClear()
        'txtAgencia.Text = ""
        txtCliente.Text = String.Empty
        txtNroOrden.Text = String.Empty
        InicializarFechas()
        ddlServicio.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        Me.ddlTipoOrigenCancelacion.SelectedIndex = 0
        txtOrderComercio.Text = String.Empty
        txtPNroOperacion.Text = String.Empty
        txtNCliente.Text = String.Empty
        ddlMoneda.SelectedIndex = 0
        btnExportarExcelFormulario.Visible = False
    End Sub

    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click

        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim dtfechainicio As DateTime = StringToDateTime(txtFechaInicio.Text)
            Dim dtfechafin As DateTime = StringToDateTime(txtFechaFin.Text)
            Dim strFechaInicio As String = String.Format("{0}-{1}-{2}", dtfechainicio.Year, dtfechainicio.Month, dtfechainicio.Day)
            Dim strFechaFin As String = String.Format("{0}-{1}-{2}", dtfechafin.Year, dtfechafin.Month, dtfechafin.Day)
            Dim IdTipoOrigenCancelacion As Integer = StringToInt(ddlTipoOrigenCancelacion.SelectedValue)
            Dim IdServicio As Integer = StringToInt(ddlServicio.SelectedValue)
            Dim Agencia As String = String.Empty
            Dim Cliente As String = txtCliente.Text.TrimEnd()
            Dim NroOrdenPago As String = txtNroOrden.Text.TrimEnd()
            Dim FechaInicio As DateTime = StringToDateTime(strFechaInicio)
            Dim FechaFin As DateTime = StringToDateTime(strFechaFin)
            Dim NCliente As String = txtNCliente.Text
            Dim IdMoneda As Integer = StringToInt(ddlMoneda.SelectedValue)
            Dim idOrdenComercio As String = txtOrderComercio.Text.TrimEnd
            Dim IdUserInfo As Integer = ObjectToInt(UserInfo.IdUsuario)
            Dim cOrdenPago As New SPE.Web.COrdenPago()
            Dim obePagoServ As New BEPagoServicio()
            obePagoServ.IdTipoOrigenCancelacion = IdTipoOrigenCancelacion
            obePagoServ.IdServicio = IdServicio
            obePagoServ.DescripcionAgencia = Agencia
            obePagoServ.DescripcionCliente = Cliente
            obePagoServ.NumeroOrdenPago = NroOrdenPago
            obePagoServ.FechaInicio = FechaInicio
            obePagoServ.FechaCancelacion = FechaFin
            obePagoServ.UsuarioNombre = NCliente
            obePagoServ.IdMoneda = IdMoneda
            obePagoServ.OrderIdComercio = idOrdenComercio
            obePagoServ.IdUsuarioCreacion = IdUserInfo
            obePagoServ.PageSize = Integer.MaxValue - 1
            obePagoServ.PageNumber = 1
            obePagoServ.PropOrder = ""
            obePagoServ.TipoOrder = True
            Dim listBEPagoServicioGenerica As List(Of BusinessEntityBase) = cOrdenPago.ConsultarPagosServicios(obePagoServ, "", True)
            Dim listBEPagoServicio As New List(Of BEPagoServicio)
            For index = 0 To listBEPagoServicioGenerica.Count - 1
                listBEPagoServicio.Add(CType(listBEPagoServicioGenerica(index), BEPagoServicio))
            Next

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Servicio", ddlServicio.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("NumeroOrdenComercio", txtOrderComercio.Text))
            parametros.Add(New KeyValuePair(Of String, String)("OrigenCancelacion", ddlTipoOrigenCancelacion.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("NumeroOperacion", txtPNroOperacion.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", ddlMoneda.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("CIP", txtNroOrden.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Cliente", txtNCliente.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Email", txtCliente.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", txtFechaInicio.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", txtFechaFin.Text))

            UtilReport.ProcesoExportarGenerico(listBEPagoServicio, Page, "EXCEL", "xls", "Consultar CIPs Pagados - ", parametros, "RptPagoServiciosDet.rdlc")
        End If
    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Try
            Dim reg As Integer
            Dim NroOP As String
            reg = gvResultado.Rows(e.CommandArgument).RowIndex
            NroOP = CType(gvResultado.Rows(reg).FindControl("hdfNumeroOrdenPago"), HiddenField).Value
            VariableTransicion = NroOP & "|" & "2"
            Select Case e.CommandName
                Case "Detalle"
                    Response.Redirect("~/ECO/DECIP.aspx")
            End Select
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddlEmpresa_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged


        Using objCAdministrarServicio As New SPE.Web.CServicio
            If String.IsNullOrEmpty(ddlEmpresa.SelectedValue) Then
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::: Todos :::")
                FormularioActivo(0)
            Else
                Dim obeServicio As New BEServicio
                obeServicio.IdServicio = CInt(ddlEmpresa.SelectedValue)
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, objCAdministrarServicio.ConsultarServicioPorIdEmpresa(obeServicio), "Nombre", "IdServicio", "::: Todos :::")
                FormularioActivo(0)
            End If
        End Using


    End Sub


    Protected Sub ddlServicio_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlServicio.SelectedIndexChanged
        btnExportarExcelFormulario.Visible = False
        Try
            If ddlServicio.SelectedIndex > 0 Then
                Dim Idservicio As Integer
                Idservicio = Convert.ToInt32(ddlServicio.SelectedValue().ToString())
                FormularioActivo(Idservicio)
            End If
        Catch ex As Exception
            _logger.Error(ex, ex.Message)
        End Try

    End Sub


    Protected Sub btnExportarExcelFormulario_Click(sender As Object, e As System.EventArgs) Handles btnExportarExcelFormulario.Click

        Dim dateFrom As Date = DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", Nothing)
        Dim dateTo As Date = DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", Nothing)
        Dim totalMonth As Long = DateDiff(DateInterval.Month, dateFrom, dateTo)

        If totalMonth > 3 Then
            ThrowErrorMessage("El rango m�ximo de fechas permitido es de 3 meses.")
            Return
        End If

        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim dtfechainicio As DateTime = StringToDateTime(txtFechaInicio.Text)
            Dim dtfechafin As DateTime = StringToDateTime(txtFechaFin.Text)
            Dim strFechaInicio As String = String.Format("{0}-{1}-{2}", dtfechainicio.Year, dtfechainicio.Month, dtfechainicio.Day)
            Dim strFechaFin As String = String.Format("{0}-{1}-{2}", dtfechafin.Year, dtfechafin.Month, dtfechafin.Day)
            Dim IdTipoOrigenCancelacion As Integer = StringToInt(ddlTipoOrigenCancelacion.SelectedValue)
            Dim IdServicio As Integer = StringToInt(ddlServicio.SelectedValue)
            Dim Agencia As String = String.Empty
            Dim Cliente As String = txtCliente.Text.TrimEnd()
            Dim NroOrdenPago As String = txtNroOrden.Text.TrimEnd()
            Dim FechaInicio As DateTime = StringToDateTime(strFechaInicio)
            Dim FechaFin As DateTime = StringToDateTime(strFechaFin)
            Dim NCliente As String = txtNCliente.Text
            Dim IdMoneda As Integer = StringToInt(ddlMoneda.SelectedValue)
            Dim idOrdenComercio As String = txtOrderComercio.Text.TrimEnd
            Dim IdUserInfo As Integer = ObjectToInt(UserInfo.IdUsuario)
            Dim cOrdenPago As New SPE.Web.COrdenPago()
            Dim obePagoServ As New BEPagoServicio()
            obePagoServ.IdTipoOrigenCancelacion = IdTipoOrigenCancelacion
            obePagoServ.IdServicio = IdServicio
            obePagoServ.DescripcionAgencia = Agencia
            obePagoServ.DescripcionCliente = Cliente
            obePagoServ.NumeroOrdenPago = NroOrdenPago
            obePagoServ.FechaInicio = FechaInicio
            obePagoServ.FechaCancelacion = FechaFin
            obePagoServ.UsuarioNombre = NCliente
            obePagoServ.IdMoneda = IdMoneda
            obePagoServ.OrderIdComercio = idOrdenComercio
            obePagoServ.IdUsuarioCreacion = IdUserInfo
            obePagoServ.PageSize = Integer.MaxValue - 1
            obePagoServ.PageNumber = 1
            obePagoServ.PropOrder = ""
            obePagoServ.TipoOrder = True


            Dim listBEPagoServicioGenerica As List(Of BusinessEntityBase) = cOrdenPago.ConsultarPagosServiciosFormulario(obePagoServ, "", True)
            Dim listBEPagoServicio As New List(Of BEPagoServicio)
            For index = 0 To listBEPagoServicioGenerica.Count - 1
                listBEPagoServicio.Add(CType(listBEPagoServicioGenerica(index), BEPagoServicio))
            Next

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Servicio", ddlServicio.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("NumeroOrdenComercio", txtOrderComercio.Text))
            parametros.Add(New KeyValuePair(Of String, String)("OrigenCancelacion", ddlTipoOrigenCancelacion.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("NumeroOperacion", txtPNroOperacion.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", ddlMoneda.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("CIP", txtNroOrden.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Cliente", txtNCliente.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Email", txtCliente.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", txtFechaInicio.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", txtFechaFin.Text))

            UtilReport.ProcesoExportarGenerico(listBEPagoServicio, Page, "EXCEL", "xls", "Consultar CIPs Pagados Detalle - ", parametros, "RptPagoServiciosFormularioDet.rdlc")
        End If
    End Sub



    Private Sub FormularioActivo(ByVal Idservicio As Integer)

        Try

            If ddlServicio.SelectedIndex > 0 Then
                Dim cOrdenPago2 As New SPE.Web.COrdenPago()
                Dim activo As Boolean

                If activo = cOrdenPago2.ConsutarFormularioActivo(Idservicio) Then
                    btnExportarExcelFormulario.Visible = False
                Else
                    btnExportarExcelFormulario.Visible = True
                End If
            Else
                btnExportarExcelFormulario.Visible = False
            End If

        Catch ex As Exception
            _logger.Error(ex, ex.Message)
        End Try
    End Sub


End Class
