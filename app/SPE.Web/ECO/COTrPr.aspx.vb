Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades
Imports System.Diagnostics

Partial Class ADM_COServ
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CEmpresaContratante)
#Region "atributos"
    Private objCParametro As CAdministrarParametro
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager As Boolean
        Get
            Return True
        End Get
    End Property
#End Region

#Region "m�todos base"
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeTransferencia As New SPE.Entidades.BETransferencia()
        Dim obeRepresentante As New BERepresentante
        obeRepresentante.IdUsuario = UserInfo.IdUsuario
        Dim obeFuncion As New CRepresentante
        Dim obeEmpresa As New BERepresentante
        obeEmpresa = obeFuncion.ObtenerEmpresaporIDRepresentante(obeRepresentante)
        obeTransferencia.idEmpresaContratante = _3Dev.FW.Util.DataUtil.StringToInt(obeEmpresa.IdEmpresaContratante)
        obeTransferencia.IdMoneda = _3Dev.FW.Util.DataUtil.StringToInt(ddlMoneda.SelectedValue)
        obeTransferencia.IdEstado = IIf(ddlEstado.SelectedIndex = 0, 0, _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue))
        obeTransferencia.FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaInicio.Text)
        obeTransferencia.FechaFin = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaFin.Text)
        Return obeTransferencia
    End Function
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub
    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        Dim objCParametro As New CAdministrarParametro

        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Todos::")
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESPP"), "Descripcion", "Id", "::Todos::")
        InicializarFecha()
    End Sub
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        h5Resultado.Visible = True
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If

    End Sub
    Private Sub InicializarFecha()
        'FECHAS

        txtFechaInicio.Text = DateAdd(DateInterval.Day, -7, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaFin.Text = Today.ToShortDateString

    End Sub
    Public Overrides Sub OnClear()
        MyBase.OnClear()
        h5Resultado.Visible = False
        ddlMoneda.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Return CType(ControllerObject, SPE.Web.CEmpresaContratante).ConsultarTransaccionesaLiquidar(be)
    End Function
    Private Sub CargarPopup(ByVal id As Integer)
        Dim objTransferencia As New BETransferencia
        Dim objT As New BETransferencia
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        objT.idTransfenciaEmpresas = id
        objTransferencia = objProcesar.ObtenerTransaccionesaLiquidar(objT)
        If Not IsNothing(objTransferencia) Then
            txtPEmpresa.Text = objTransferencia.DescripcionEmpresa
            txtPMoneda.Text = objTransferencia.DescripcionMoneda
            txtPDel.Text = CDate(objTransferencia.FechaInicio).ToShortDateString
            txtPAl.Text = CDate(objTransferencia.FechaFin).ToShortDateString
            txtPNroCuenta.Text = objTransferencia.NumeroCuenta
            txtPNroOperacion.Text = objTransferencia.NumeroOperacion
            txtPObservacion.Text = objTransferencia.Observacion
            txtPTComision.Text = objTransferencia.TotalComision.ToString("#,#0.00")
            txtPTotalOperaciones.Text = objTransferencia.TotalOperaciones
            txtPTPagos.Text = objTransferencia.TotalPago.ToString("#,#0.00")
            lblIdTransferencia.Text = objTransferencia.idTransfenciaEmpresas
            lblIdEmpresa.Text = objTransferencia.idEmpresaContratante
            lblIdMoneda.Text = objTransferencia.IdMoneda
            txtPEstado.Text = objTransferencia.DescipcionEstadoT
            Dim fechaDep As DateTime = objTransferencia.FechaDeposito
            If fechaDep = DateTime.MinValue Then
                txtFechaDeposito.Text = Date.Today.ToShortDateString
            Else
                txtFechaDeposito.Text = fechaDep
            End If
            txtBanco.Text = objTransferencia.DescripcionBanco
            pnlPopupActualizarTransferencia.Visible = True
            mppTransferencia.Show()
        End If
    End Sub
#End Region
#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(ddlMoneda)
        End If
        h5Resultado.Visible = False
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    End Sub
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Edit" : CargarPopup(CInt(e.CommandArgument))
        End Select
        h5Resultado.Visible = True
    End Sub
#End Region
    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        mppTransferencia.Hide()
        h5Resultado.Visible = True
    End Sub

    Protected Sub btnPCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPCancelar.Click
        mppTransferencia.Hide()
        h5Resultado.Visible = True
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim idestado As Integer = 0

            Dim objCServicio As New SPE.Web.CServicio
            Dim obeServicio As New SPE.Entidades.BETransferencia

            Dim ListaTrans As New List(Of BETransferencia)
            obeServicio.idEmpresaContratante = CInt(lblIdEmpresa.Text.TrimEnd())
            obeServicio.IdMoneda = CInt(lblIdMoneda.Text.TrimEnd())
            obeServicio.idTransfenciaEmpresas = CInt(lblIdTransferencia.Text.TrimEnd())
            obeServicio.FechaInicio = CDate(txtPDel.Text.TrimEnd)
            obeServicio.FechaFin = CDate(txtPAl.Text.TrimEnd)

            ListaTrans = objCServicio.ConsultarDetalleTransaccionesxServicio(obeServicio)

            Dim FechaLiquidacion As String
            If ListaTrans.Item(0).FechaLiquidacion = DateTime.MinValue Then
                FechaLiquidacion = " - "
            Else
                FechaLiquidacion = ListaTrans.Item(0).FechaLiquidacion
            End If
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Titulo", "Detalle de Transacciones"))
            parametros.Add(New KeyValuePair(Of String, String)("Empresa", IIf(ListaTrans.Count > 0, ListaTrans(0).DescripcionEmpresa, " - ")))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", IIf(ListaTrans.Count > 0, ListaTrans(0).DescripcionMoneda, " - ")))
            'parametros.Add(New KeyValuePair(Of String, String)("Estado", IIf(ListaTrans.Count > 0, ListaTrans(0).DescipcionEstadoT, " - ")))
            parametros.Add(New KeyValuePair(Of String, String)("Hasta", txtPAl.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Desde", txtPDel.Text))

            parametros.Add(New KeyValuePair(Of String, String)("Total", txtPTPagos.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Comision", txtPTComision.Text))
            parametros.Add(New KeyValuePair(Of String, String)("TotalT", (Decimal.Parse(txtPTPagos.Text) - Decimal.Parse(txtPTComision.Text)).ToString()))

            UtilReport.ProcesoExportarGenerico(ListaTrans, Page, "EXCEL", "xls", "Detalle Transacciones no consideradas - ", parametros, "RptDetalleTransaccionPendiente.rdlc")

            'parametros.Add(New KeyValuePair(Of String, String)("FechaRegistro", IIf(ListaTrans.Count > 0, ListaTrans(0).FechaCreacionOP.ToString("dd/MM/yyyy hh:mm tt"), " - ")))
            'parametros.Add(New KeyValuePair(Of String, String)("FechaLiquidacion", IIf(ListaTrans.Count > 0, IIf(ListaTrans.Item(0).FechaLiquidacion = DateTime.MinValue, " - ", ListaTrans(0).FechaLiquidacion.ToString("dd/MM/yyyy  hh:mm tt")), " - ")))
            'UtilReport.ProcesoExportarGenerico(ListaTrans, Page, "EXCEL", "xls", "Detalle de Transacci�n - ", parametros, "RptDetalleTransaccion.rdlc")

        End If
    End Sub

    Protected Sub btnArchC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchC.Click

        Dim objCServicio As New SPE.Web.CServicio
        Dim obeServicio As New SPE.Entidades.BETransferencia

        Dim ListaTrans As New List(Of BETransferencia)
        obeServicio.idEmpresaContratante = CInt(lblIdEmpresa.Text)
        obeServicio.IdMoneda = CInt(lblIdMoneda.Text)
        obeServicio.idTransfenciaEmpresas = CInt(lblIdTransferencia.Text)
        obeServicio.FechaInicio = CDate(txtPDel.Text)
        obeServicio.FechaFin = CDate(txtPAl.Text)
        ListaTrans = objCServicio.ConsultarDetalleTransaccionesxServicio(obeServicio)

        Dim StringPagoServicios As New StringBuilder("")
        For Each elemento As BusinessEntityBase In ListaTrans
            Dim obj As BETransferencia
            obj = DirectCast(elemento, BETransferencia)
            StringPagoServicios.Append(obj.IdOrdenPago.ToString())
            StringPagoServicios.Append("," & obj.IdCliente)
            StringPagoServicios.Append("," & obj.NumeroOrdenPago)
            StringPagoServicios.Append("," & obj.IdOrderComercio)
            StringPagoServicios.Append("," & obj.IdMoneda)
            StringPagoServicios.Append("," & obj.TotalComision)
            StringPagoServicios.Append("," & obj.TotalPago)
            StringPagoServicios.Append("," & IIf(obj.FechaEmision = DateTime.MinValue, String.Empty, obj.FechaEmision.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & IIf(obj.FechaCancelacion = DateTime.MinValue, String.Empty, obj.FechaCancelacion.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & IIf(obj.FechaAnulacion = DateTime.MinValue, String.Empty, obj.FechaAnulacion.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & obj.IdEstado)
            StringPagoServicios.Append("," & IIf(obj.FechaLiquidacion = DateTime.MinValue, String.Empty, obj.FechaLiquidacion.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & obj.DescripcionBanco)
            StringPagoServicios.Append("," & obj.DescripcionAgencia)
            StringPagoServicios.AppendLine()
        Next

        Dim byteArchivoCorona As Byte() = New UTF8Encoding().GetBytes(StringPagoServicios.ToString())

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = "text/plain"
        Response.AddHeader("content-disposition", "attachment; filename=" & "ArchivoConciliacion_" & DateTime.Now.ToString("yyyyMMdd") & "_" & DateTime.Now.Hour.ToString() & DateTime.Now.Minute.ToString() & ".txt")
        Response.BinaryWrite(byteArchivoCorona)
        Response.Flush()
        Response.[End]()

    End Sub

    Public Overrides Sub OnGoToMaintenanceEditPage(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        e.NewEditIndex = -1
    End Sub
End Class
