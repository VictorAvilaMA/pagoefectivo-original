<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" ValidateRequest ="false" 
    AutoEventWireup="false" CodeFile="DECIP.aspx.vb" Inherits="ADM_ADServ" Title="PagoEfectivo - Detalle CIP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../UC/UCAudit.ascx" TagName="UCAudit" TagPrefix="uc1" %>
<%--<asp:content id="Content2" contentplaceholderid="ContentPlaceHolderHead" runat="Server">
    <link href="../Style/admin.css" rel="stylesheet" type="text/css" />
</asp:content>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblIdServicio" runat="server" Style="display: none;" Text="">
    </asp:Label>
    <h2>
        Detalles de CIP</h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pInfoGeneral" runat="server">
            <h4>
                Datos de CIP
            </h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">&gt;&gt;</span><b> N&uacute;mero CIP :</b>
                    <asp:Label ID="lblIdOrdenPago" CssClass="corta" Visible="false" runat="server" Text=""></asp:Label>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtNumeroCIP" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <ul class="datos_cip4" style="margin-top: 10px">
                <li class="t1"><span class="color">&gt;&gt;</span><b> Nro. Orden Comercio :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtIdComercio" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Monto:</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtMonto" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span><b> Estado : </b></li>
                <li class="t2">
                    <asp:TextBox ID="txtEstado" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Fecha Cancelaci&oacute;n :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtFechaCancelacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Fecha Eliminaci&oacute;n :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtFechaEliminacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <ul class="datos_cip4" style="margin-top: 10px">
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Moneda :</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtMoneda" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span><b>Servicio: </b></li>
                <li class="t2">
                    <asp:TextBox ID="txtServicio" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Fecha Emisi&oacute;n :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Fecha a Expirar :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtFechaExpiracion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Fecha Anulaci&oacute;n :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtFechaAnulacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
            <h4 style="display:none">
                Datos del Cliente
            </h4>
            <ul class="datos_cip4" style="display:none">
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Nombre :</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span><b> Alias:</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtAlias" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Tipo Documento :</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtTipoDoc" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span><b> Tel&eacute;fono :</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtTelefono" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <ul class="datos_cip4" style="display:none">
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Apellidos :</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtApellido" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Correo:</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span><b> N&uacute;mero Documento :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtNumDoc" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
            <fieldset id="pnlDatosConciliacion" runat="server">
                <h4>
                    Datos de Conciliaci&oacute;n
                </h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span><b> Fecha Conciliaci&oacute;n :</b>
                    </li>
                    <li class="t2">
                        <asp:TextBox ID="txtFConciliacion" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                    </li>
                </ul>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span><b> Estado Conciliaci&oacute;n :</b>
                    </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEstadoConciliacion" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                    </li>
                </ul>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span><b> Archivo de Conciliaci&oacute;n:
                    </b></li>
                    <li class="t2">
                        <asp:TextBox ID="txtArchivoConc" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                    </li>
                </ul>
            </fieldset>
            <div style="clear: both;">
            </div>
            <h4>
                Detalle de CIP
            </h4>
            <div class="cont_cel">
                <asp:UpdatePanel ID="UpdatePanelResultado" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grvDetalleCIP" ShowFooter="False" runat="server" AllowSorting="false"
                            AllowPaging="false" AutoGenerateColumns="False" CssClass="grilla" PageSize="15">
                            <Columns>
                                <asp:BoundField DataField="IdOrdenPago" SortExpression="IdOrdenPago" HeaderText="Id">
                                </asp:BoundField>
                                <asp:BoundField DataField="ConceptoPago" SortExpression="ConceptoPago" HeaderText="Concepto de Pago">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Importe" SortExpression="Importe">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Importe"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                               <%-- <asp:BoundField DataField="tipoOrigen" SortExpression="Tipo_Origen" HeaderText="Origen">
                                </asp:BoundField>
                                <asp:BoundField DataField="codOrigen" SortExpression="Cod_Origen" HeaderText="C&oacute;digo.Origen">
                                </asp:BoundField>--%>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <h4>
                Detalle de Movimientos
            </h4>
            <div class="cont_cel">
                <asp:UpdatePanel ID="updMovimientos" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grvMovimientos" ShowFooter="False" runat="server" AllowSorting="False"
                            AllowPaging="False" AutoGenerateColumns="False" CssClass="grilla" PageSize="10">
                            <Columns>
                                <asp:BoundField DataField="IdMovimiento" SortExpression="IdMovimiento" HeaderText="C&oacute;digo Movimiento">
                                </asp:BoundField>
                                <asp:BoundField DataField="NumeroOperacion" SortExpression="NumeroOperacion" HeaderText="Nro. Operaci&oacute;n">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionTipoMovimiento" SortExpression="tipomov" HeaderText="Tipo de Movimiento">
                                </asp:BoundField>
                                <asp:BoundField DataField="MedioPago" SortExpression="mediopago" HeaderText="Medio de Pago">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Monto" SortExpression="Monto">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Monto"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Movimiento" SortExpression="FechaMovimiento">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDate(DataBinder.Eval(Container.DataItem, "FechaMovimiento"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DescripcionOrigenCanc" SortExpression="origenCanc" HeaderText="Origen de Cancelaci&oacute;n">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionAgencia" SortExpression="NombreComercial" HeaderText="Agencia">
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <fieldset id="fisUpload" class="ContenedorEtiquetaTexto" runat="server" visible="false">
            <div id="div9" class="EtiquetaTextoIzquierda">
                <asp:Label ID="lblUploadCarga" runat="server" Text="" Style="color: Red"></asp:Label>
            </div>
        </fieldset>
        <br />
        <asp:Label ID="lblMensaje" runat="server" Width="650px" CssClass="MensajeTransaccion"
            Style="padding-bottom: 5px"></asp:Label>
        <%--<asp:Label ID="lblMensaje" CssClass="MensajeTransaccion" runat="server" Height="25px"
            Style="padding-top: 0px">
        </asp:Label>--%>
        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="GrupoValidacion"
            runat="server" />
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="input_azul3" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Regresar" />
            </li>
        </ul>
    </div>
</asp:Content>
