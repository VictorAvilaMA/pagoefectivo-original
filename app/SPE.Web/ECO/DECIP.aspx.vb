Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Web.PaginaBase



Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.EmsambladoComun

Partial Class ADM_ADServ
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.COrdenPago)

#Region "Propiedades"
    Private objCParametro As CAdministrarParametro
    Private Function InstanciaParametro() As CAdministrarParametro
        If objCParametro Is Nothing Then
            objCParametro = New CAdministrarParametro()
        End If
        Return objCParametro
    End Function

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property


#End Region

#Region "Métodos base"
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        'e.MaintenanceKey = "ADServ"
        'e.URLPageCancelForEdit = "COServ.aspx"
        'e.URLPageCancelForInsert = "COServ.aspx"
        Try
            Dim idUri As String = VariableTransicion.ToString.Split("|"c)(1)
            'Dim idUri As String = Request.QueryString("idUri")
            Select Case idUri
                Case "1"
                    e.URLPageCancelForSave = "COCIP.aspx"
                Case "2"
                    e.URLPageCancelForSave = "COPaSer.aspx"
                Case "3"
                    e.URLPageCancelForSave = "../JPR/COHiPa.aspx"
                Case "4"
                    e.URLPageCancelForSave = "../CLI/COHiOrPa.aspx"
                Case "5"
                    e.URLPageCancelForSave = "../CLI/PgPrl.aspx"
            End Select
            e.UseEntitiesMethod = True
        Catch ex As Exception
            'ThrowErrorMessage(ex.Message)
            Response.Redirect("~/SEG/sinautrz.aspx")
        End Try
    End Sub

    Protected Overrides Sub MaintenancePage_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.MaintenancePage_Load(sender, e)
        If Not IsPostBack Then
        End If
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
    End Sub

    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
    End Sub

    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Select Case UserInfo.Rol
            Case RolJefeProducto
                CType(Me.Master.FindControl("literalMenuCabezera"), Literal).Text = "Principal - Jefe de Producto / Detalle de CIP"
            Case RolRepresentante
                CType(Me.Master.FindControl("literalMenuCabezera"), Literal).Text = "Principal - Representante / Detalle de CIP"
            Case RolCliente
                CType(Me.Master.FindControl("literalMenuCabezera"), Literal).Text = "Principal - Cliente / Detalle de CIP"
            Case Else
                CType(Me.Master.FindControl("literalMenuCabezera"), Literal).Text = "Detalle de CIP"
        End Select
        Page.Title = "PagoEfectivo - Detalle CIP"
    End Sub
    Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
        MyBase.OnInitComplete(e)
        'ucAudit.InitializeAudit(lnkShowAudit)
    End Sub

    Public Overrides Sub OnMainCancel()
        If ViewState("obeOrdenPagoSearch") IsNot Nothing Then
            Session("obeOrdenPagoSearch") = ViewState("obeOrdenPagoSearch")
        End If
        MyBase.OnMainCancel()
    End Sub
#End Region

#Region "Métodos"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = False Then

            Try
                Dim objCParametro As New SPE.Web.CAdministrarParametro()
                Dim objCOrdenPagoC As New SPE.Web.COrdenPago
                Dim textoPorDefecto As String = " - " 'String.Empty

                Dim id As String = VariableTransicion.ToString.Split("|"c)(0)
                'Dim id As String = Request.QueryString("id")
                If id <> "" Then
                    Dim obeOrdenPago As New SPE.Entidades.BEOrdenPago
                    obeOrdenPago = objCOrdenPagoC.ObtenerOrdenPagCabeceraPorNumeroOrden(id)
                    If Not IsNothing(obeOrdenPago) Then

                        lblIdOrdenPago.Text = Convert.ToInt32(obeOrdenPago.IdOrdenPago)
                        txtNumeroCIP.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.NumeroOrdenPago), textoPorDefecto, obeOrdenPago.NumeroOrdenPago)
                        txtMoneda.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.DescripcionMoneda), textoPorDefecto, obeOrdenPago.DescripcionMoneda)
                        txtMonto.Text = CDec(obeOrdenPago.Total)
                        txtAlias.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.UsuarioAlias), textoPorDefecto, obeOrdenPago.UsuarioAlias)
                        txtApellido.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.UsuarioApellidos), textoPorDefecto, obeOrdenPago.UsuarioApellidos)
                        txtEmail.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.UsuarioEmail), textoPorDefecto, obeOrdenPago.UsuarioEmail)
                        txtEstado.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.DescripcionEstado), textoPorDefecto, obeOrdenPago.DescripcionEstado)
                        txtIdComercio.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.OrderIdComercio), textoPorDefecto, obeOrdenPago.OrderIdComercio)

                        If (Convert.ToDateTime(obeOrdenPago.FechaAnulada) = DateTime.MinValue) Then
                            txtFechaAnulacion.Text = textoPorDefecto
                        Else
                            txtFechaAnulacion.Text = Convert.ToDateTime(obeOrdenPago.FechaAnulada).ToShortDateString
                        End If

                        If (Convert.ToDateTime(obeOrdenPago.FechaEliminado) = DateTime.MinValue) Then
                            txtFechaEliminacion.Text = textoPorDefecto
                        Else
                            txtFechaEliminacion.Text = Convert.ToDateTime(obeOrdenPago.FechaEliminado).ToShortDateString
                        End If

                        If (Convert.ToDateTime(obeOrdenPago.FechaEmision) = DateTime.MinValue) Then
                            txtFechaEmision.Text = textoPorDefecto
                        Else
                            txtFechaEmision.Text = Convert.ToDateTime(obeOrdenPago.FechaEmision).ToShortDateString
                        End If

                        If (Convert.ToDateTime(obeOrdenPago.FechaExpirada) = DateTime.MinValue) Then
                            txtFechaExpiracion.Text = textoPorDefecto
                        Else
                            txtFechaExpiracion.Text = Convert.ToDateTime(obeOrdenPago.FechaExpirada).ToShortDateString
                        End If

                        If (Convert.ToDateTime(obeOrdenPago.FechaCancelacion) = DateTime.MinValue) Then
                            txtFechaCancelacion.Text = textoPorDefecto
                        Else
                            txtFechaCancelacion.Text = Convert.ToDateTime(obeOrdenPago.FechaCancelacion).ToShortDateString
                        End If

                        txtNombre.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.UsuarioNombre), textoPorDefecto, obeOrdenPago.UsuarioNombre)
                        txtNumDoc.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.ClienteNroDocumento), textoPorDefecto, obeOrdenPago.ClienteNroDocumento)
                        txtServicio.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.DescripcionServicio), textoPorDefecto, obeOrdenPago.DescripcionServicio)
                        txtTelefono.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.ClienteTelefono), textoPorDefecto, obeOrdenPago.ClienteTelefono)
                        txtTipoDoc.Text = IIf(String.IsNullOrEmpty(obeOrdenPago.ClienteTipoDocumento), textoPorDefecto, obeOrdenPago.ClienteTipoDocumento)

                        'ROL
                        'Dim rol As String = UserInfo.Rol

                        Select Case UserInfo.Rol
                            Case RolRepresentante, RolCliente
                                pnlDatosConciliacion.Visible = False
                            Case Else
                                IIf(Convert.ToDateTime(obeOrdenPago.FechaConciliacion) = DateTime.MinValue, _
                                    txtFConciliacion.Text = Convert.ToDateTime(obeOrdenPago.FechaCancelacion).ToShortDateString, _
                                    txtFConciliacion.Text = textoPorDefecto)
                                txtEstadoConciliacion.Text = obeOrdenPago.DescripcionEstadoConc
                                txtArchivoConc.Text = obeOrdenPago.ArchivoConciliacion
                        End Select

                        Dim objCOrdenPago As New SPE.Web.COrdenPago
                        grvDetalleCIP.DataSource = objCOrdenPago.ConsultarDetalleOrdenPagoPorIdOrdenPago(obeOrdenPago.IdOrdenPago)
                        grvDetalleCIP.DataBind()

                        Dim objMovimientos As New SPE.Web.COrdenPago
                        grvMovimientos.DataSource = objCOrdenPago.ConsultarHistoricoMovimientosporIdOrden(obeOrdenPago.IdOrdenPago)
                        grvMovimientos.DataBind()
                        If Session("obeOrdenPagoSearch") IsNot Nothing Then
                            ViewState("obeOrdenPagoSearch") = Session("obeOrdenPagoSearch")
                            Session.Remove("obeOrdenPagoSearch")
                        End If
                    Else
                        Dim idUri As String = VariableTransicion.ToString.Split("|"c)(1)
                        Dim ReturnPage As String = ""
                        Select Case idUri
                            Case "1"
                                ReturnPage = "COCIP.aspx"
                            Case "2"
                                ReturnPage = "COPaSer.aspx"
                            Case "3"
                                ReturnPage = "../JPR/COHiPa.aspx"
                            Case "4"
                                ReturnPage = "../CLI/COHiOrPa.aspx"
                            Case "5"
                                ReturnPage = "../CLI/PgPrl.aspx"
                        End Select
                        JSMessageAlertWithRedirect("Alerta: ", "El usuario no tiene información completa o actualizada", "cc", ReturnPage)
                    End If
                Else
                    Dim idUri As String = VariableTransicion.ToString.Split("|"c)(1)
                    Dim ReturnPage As String = ""
                    Select Case idUri
                        Case "1"
                            ReturnPage = "COCIP.aspx"
                        Case "2"
                            ReturnPage = "COPaSer.aspx"
                        Case "3"
                            ReturnPage = "../JPR/COHiPa.aspx"
                        Case "4"
                            ReturnPage = "../CLI/COHiOrPa.aspx"
                        Case "5"
                            ReturnPage = "../CLI/PgPrl.aspx"
                    End Select
                    JSMessageAlertWithRedirect("Alerta: ", "La Información de la vista anterior no llego correctamente", "bb", ReturnPage)
                End If
            Catch ex As Exception
                'ThrowErrorMessage(ex.Message)
                Response.Redirect("~/SEG/sinautrz.aspx")
            End Try
        End If
    End Sub
#End Region



    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim idestado As Integer = 0
            Dim NumeroCip As String = txtNumeroCIP.Text.TrimEnd()
            Dim Rol As String = UserInfo.Rol

            Dim objCOrdenPagoC As New SPE.Web.COrdenPago
            Dim obeOrdenPago As New SPE.Entidades.BEOrdenPago
            obeOrdenPago = objCOrdenPagoC.ObtenerOrdenPagCabeceraPorNumeroOrden(NumeroCip)

            Dim idOrdenPago As Integer = Convert.ToInt32(obeOrdenPago.IdOrdenPago)
            Dim NumeroOrdenPago As String = obeOrdenPago.NumeroOrdenPago
            Dim Moneda As String = obeOrdenPago.DescripcionMoneda
            Dim Total As Decimal = CDec(obeOrdenPago.Total)
            Dim UAlias As String = obeOrdenPago.UsuarioAlias
            Dim UApellidos As String = obeOrdenPago.UsuarioApellidos
            Dim Email As String = obeOrdenPago.UsuarioEmail
            Dim DEstado As String = obeOrdenPago.DescripcionEstado
            Dim IdComercio As String = obeOrdenPago.OrderIdComercio
            Dim UNombre As String = obeOrdenPago.UsuarioNombre
            Dim NroDoc As String = obeOrdenPago.ClienteNroDocumento
            Dim Servicio As String = obeOrdenPago.DescripcionServicio
            Dim Telefono As String = obeOrdenPago.ClienteTelefono
            Dim tipoDoc As String = obeOrdenPago.ClienteTipoDocumento

            Dim FechaAnulacion As String = String.Empty
            If (Convert.ToDateTime(obeOrdenPago.FechaAnulada) = DateTime.MinValue) Then
                FechaAnulacion = String.Empty
            Else
                FechaAnulacion = Convert.ToDateTime(obeOrdenPago.FechaAnulada)
            End If
            Dim FechaEliminacion As String = String.Empty
            If (Convert.ToDateTime(obeOrdenPago.FechaEliminado) = DateTime.MinValue) Then
                FechaEliminacion = String.Empty
            Else
                FechaEliminacion = Convert.ToDateTime(obeOrdenPago.FechaEliminado)
            End If
            Dim FechaEmision As String = String.Empty
            If (Convert.ToDateTime(obeOrdenPago.FechaEmision) = DateTime.MinValue) Then
                FechaEmision = String.Empty
            Else
                FechaEmision = Convert.ToDateTime(obeOrdenPago.FechaEmision)
            End If
            Dim FechaExpiracion As String = String.Empty
            If (Convert.ToDateTime(obeOrdenPago.FechaExpirada) = DateTime.MinValue) Then
                FechaExpiracion = String.Empty
            Else
                FechaExpiracion = Convert.ToDateTime(obeOrdenPago.FechaExpirada)
            End If
            Dim FechaCancelacion As String = String.Empty
            If (Convert.ToDateTime(obeOrdenPago.FechaCancelacion) = DateTime.MinValue) Then
                FechaCancelacion = String.Empty
            Else
                FechaCancelacion = Convert.ToDateTime(obeOrdenPago.FechaCancelacion)
            End If
            Dim FechaConciliacion As String = ""
            Dim ArchivoConc As String = ""
            Dim EstadoConc As String = ""
            Dim Reporte As String = ""
            Select Case Rol
                Case ParametrosSistema.RolRepresentante
                    Reporte = "RptDetCIPExpR.rdlc"
                Case ParametrosSistema.RolCliente
                    Reporte = "RptDetCIPExpC.rdlc"
                Case Else
                    Reporte = "RptDetCIPExpJ.rdlc"
                    IIf(obeOrdenPago.FechaConciliacion = DateTime.MinValue, _
                        FechaConciliacion = String.Empty, _
                        FechaConciliacion = obeOrdenPago.FechaConciliacion.ToString)
                    EstadoConc = obeOrdenPago.DescripcionEstadoConc
                    ArchivoConc = obeOrdenPago.ArchivoConciliacion
            End Select

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("NumeroOrdenPago", NumeroOrdenPago))
            parametros.Add(New KeyValuePair(Of String, String)("IdComercio", IdComercio))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", Moneda))
            parametros.Add(New KeyValuePair(Of String, String)("Total", Total))
            parametros.Add(New KeyValuePair(Of String, String)("Servicio", Servicio))
            parametros.Add(New KeyValuePair(Of String, String)("DEstado", DEstado))
            parametros.Add(New KeyValuePair(Of String, String)("FechaEmision", FechaEmision))
            parametros.Add(New KeyValuePair(Of String, String)("FechaCancelacion", FechaCancelacion))
            parametros.Add(New KeyValuePair(Of String, String)("FechaExpiracion", FechaExpiracion))
            parametros.Add(New KeyValuePair(Of String, String)("FechaEliminacion", FechaEliminacion))
            parametros.Add(New KeyValuePair(Of String, String)("UNombre", UNombre))
            parametros.Add(New KeyValuePair(Of String, String)("UApellidos", UApellidos))
            parametros.Add(New KeyValuePair(Of String, String)("UAlias", UAlias))
            parametros.Add(New KeyValuePair(Of String, String)("Email", Email))
            parametros.Add(New KeyValuePair(Of String, String)("tipoDoc", tipoDoc))
            parametros.Add(New KeyValuePair(Of String, String)("NroDoc", NroDoc))
            parametros.Add(New KeyValuePair(Of String, String)("Telefono", Telefono))
            parametros.Add(New KeyValuePair(Of String, String)("FechaConciliacion", FechaConciliacion))
            parametros.Add(New KeyValuePair(Of String, String)("EstadoConc", EstadoConc))
            parametros.Add(New KeyValuePair(Of String, String)("ArchivoConc", ArchivoConc))
            'asd

            Dim objCOrdenPago As New SPE.Web.COrdenPago
            Dim listBEDetalleOrdenPago As List(Of BEDetalleOrdenPago) = objCOrdenPago.ConsultarDetalleOrdenPagoPorIdOrdenPago(obeOrdenPago.IdOrdenPago)

            Dim objMovimientos As New SPE.Web.COrdenPago
            Dim listBEMovimiento As List(Of BEMovimiento) = objCOrdenPago.ConsultarHistoricoMovimientosporIdOrden(obeOrdenPago.IdOrdenPago)

            UtilReport.ProcesoExportarGenerico(listBEDetalleOrdenPago, listBEMovimiento, Page, "EXCEL", "xls", "Detalle de CIP - ", parametros, Reporte)
        End If
    End Sub

    Public Sub JSMessageAlertWithRedirect(ByVal tipo_alerta As String, ByVal mensaje As String, ByVal key As String, ByVal PageRedirect As String)
        Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", tipo_alerta, mensaje, PageRedirect)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub

End Class
