﻿Imports SPE.Web
Imports SPE.Entidades
Imports SPE.Web.Util
Imports _3Dev.FW.Util.DataUtil
Imports SPE.EmsambladoComun
Partial Class ECO_DELCip
    Inherits PaginaBase

    Protected Sub btnEliminar_Click(sender As Object, e As System.EventArgs) Handles btnEliminar.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        'M

        oBEOrdenPago.NumeroOrdenPago = TraductorServ.EncriptarStr(txtNroCIP.Text.PadLeft(14, "0"))
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim intResult As Int64 = oCOrdenPago.EliminarCIPWeb(oBEOrdenPago)
        Select Case intResult
            Case Is > 0
                lblMensaje.Text = ("Se eliminó con éxito el CIP Nº " & intResult.ToString().PadLeft(14, "0"))
                DesactivarControles()
                btnNuevo.Visible = True
            Case -1
                JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
            Case -2
                JSMessageAlert("Validación", "El cip ingresado no existe", "val")
            Case -3
                JSMessageAlert("Validación", "El cip ingresado no pertenece a su empresa", "val")
            Case -4
                JSMessageAlert("Validación", "El cip ingresado ya se encuentra eliminado", "val")
            Case -5
                JSMessageAlert("Validación", "El cip ingresado no se encuentra en estado generado", "val")
            Case Else
                JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
        End Select

    End Sub
    Protected Sub DesactivarControles()
        txtNroCIP.Enabled = False
        btnEliminar.Visible = False
    End Sub

    Protected Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("DELCip.aspx")
    End Sub

    Protected Sub ibtnBuscarCIP_Click(sender As Object, e As System.EventArgs) Handles ibtnBuscarCIP.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        oBEOrdenPago.NumeroOrdenPago = txtNroCIP.Text.Trim()
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim result As BEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorIdOrdenYIdRepresentante(oBEOrdenPago)
        If result Is Nothing Then
            divDatos.Visible = False
            JSMessageAlert("Validación", "No se encontro el número de CIP o no pertenece a su empresa", "val")
        Else
            divDatos.Visible = True
            txtConceptoPagoInfo.Text = result.ConceptoPago
            txtMontoInfo.Text = result.Total.ToString()
            txtEstadoInfo.Text = result.DescripcionEstado
            txtServicioInfo.Text = result.oBEServicio.Nombre
        End If
    End Sub
End Class
