﻿Imports SPE.Web
Imports SPE.Entidades
Imports SPE.Web.Util
Imports _3Dev.FW.Util.DataUtil

Partial Class ECO_ExpToGenCIP
    Inherits PaginaBase
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtFechaExpiracion.Text = DateAdd(DateInterval.Day, 1, DateTime.Now).ToString("dd/MM/yyyy hhh:mm:00 tt") 'Today.ToShortDateString
        End If
    End Sub
    Protected Sub ibtnBuscarCIP_Click(sender As Object, e As System.EventArgs) Handles ibtnBuscarCIP.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        oBEOrdenPago.NumeroOrdenPago = txtNroCIP.Text.Trim()
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim result As BEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorIdOrdenYIdRepresentante(oBEOrdenPago)
        If result Is Nothing Then
            divDatos.Visible = False
            JSMessageAlert("Validación", "No se encontro el número de CIP o no pertenece a su empresa", "val")
        Else
            divDatos.Visible = True
            txtConceptoPagoInfo.Text = result.ConceptoPago
            txtMontoInfo.Text = result.Total.ToString()
            txtEstadoInfo.Text = result.DescripcionEstado
            txtServicioInfo.Text = result.oBEServicio.Nombre
        End If
    End Sub
End Class
