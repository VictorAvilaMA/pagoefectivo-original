<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PgPrl.aspx.vb" Inherits="ECO_PgPrl" Title="PagoEfectivo - P�gina Principal"
    EnableEventValidation="false" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-ui-1.8.13.custom.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/js/ui.dropdownchecklist-1.4-min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion")%>'
        type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(function () {
            var waitMsg = $("div[id$='AsyncWait_Wait']");
            waitMsg.wrap("<div style='display:none; visibility: hidden'></div>");
        });
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <style type="text/css">
        ul.datos_cip2.f0.row-servicio
        {
            z-index: 9999;
        }
        ul.datos_cip2.f0.row-servicio li.t2.f0
        {
            z-index: 9999;
        }
        ul.datos_cip2.f0.row-servicio li.t2.f0 .ui-dropdownchecklist.ui-dropdownchecklist-dropcontainer-wrapper.ui-widget
        {
            z-index: 9999;
        }
        ul.datos_cip2.f0.row-servicio li.t2.f0 .ui-dropdownchecklist.ui-dropdownchecklist-dropcontainer-wrapper.ui-widget .ui-dropdownchecklist-dropcontainer.ui-widget-content
        {
            z-index: 9999;
        }
        #ctl00_ContentPlaceHolder1_UpdatePanel12
        {
            z-index: 30;
            position: relative;
        }
        #ddcl-slcServicios
        {
            width: auto;
        }
        .ui-dropdownchecklist-selector
        {
            width: 300px !important;
        }
        .ui-dropdownchecklist-selector, .ui-dropdownchecklist-text
        {
            height: 20px !important;
            text-align: left !important;
            margin-top: 6px !important;
            padding: 0 !important;
            margin: 0 !important;
            line-height: 20px !important; /*position: relative !important;*/
            float: none !important;
            overflow: hidden !important;
            font-family: MS Shell Dlg;
            font-size: 13.3333px !important;
        }
        .ui-dropdownchecklist-dropcontainer-wrapper
        {
            top: 21px !important;
        }
        span.ui-dropdownchecklist-text
        {
            width: 297px !important;
        }
        .ui-dropdownchecklist-item
        {
            height: 30px;
        }
    </style>
    <asp:HiddenField ID="hdfIdServicios" runat="server" />
    <asp:HiddenField ID="hdfServicios" runat="server" />
    <asp:HiddenField ID="hdfServiciosFormato" runat="server" />
    <h2>
        Resumen de Pagos por Servicio
    </h2>
    <asp:UpdatePanel runat="server" ID="UpdatePanelResultado" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="conten_pasos3">
                <fieldset>
                    <h4>
                        Criterios de B&uacute;squeda
                    </h4>
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlEmpresa" runat="server" Visible="false">
                                <ul class="datos_cip2 f0">
                                    <li class="t1 f0"><span class="color">&gt;&gt; </span>Empresa : </li>
                                    <li class="t2 f0">
                                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </li>
                                </ul>
                            </asp:Panel>
                            <ul class="datos_cip2 f0 row-servicio">
                                <li class="t1 f0"><span class="color">&gt;&gt; </span>Servicio : </li>
                                <li class="t2 f0">
                                    <asp:Literal ID="ddlServicio" runat="server"></asp:Literal>
                                </li>
                            </ul>
                            <ul class="datos_cip2 f0">
                                <li class="t1 f0"><span class="color">&gt;&gt; </span>Origen Cancelaci&oacute;n :
                                </li>
                                <li class="t2 f0">
                                    <asp:DropDownList ID="ddlOrigenCancelacion" runat="server" CssClass="neu">
                                    </asp:DropDownList>
                                </li>
                            </ul>
                            <ul class="datos_cip2 f0">
                                <li class="t1 f0"><span class="color">&gt;&gt; </span>Moneda : </li>
                                <li class="t2 f0">
                                    <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu">
                                    </asp:DropDownList>
                                </li>
                            </ul>
                            <ul class="datos_cip2 f0">
                                <li class="t1 f0"><span class="color">&gt;&gt; </span>Tipo : </li>
                                <li class="t2 f0">
                                    <asp:DropDownList ID="ddlTipo" runat="server" CssClass="neu">
                                    </asp:DropDownList>
                                </li>
                            </ul>
                            <ul class="datos_cip2 f0">
                                <li class="t1 f0"><span class="color">&gt;&gt; </span>Por Mes : </li>
                                <li class="t2 f0">
                                    <asp:CheckBox ID="chbMes" runat="server" CssClass="neu"></asp:CheckBox>
                                </li>
                            </ul>
                            <ul class="datos_cip2 f0">
                                <li class="t1 f0"><span class="color">&gt;&gt;</span> Fecha Pago del:</li>
                                <li class="t2 f0" style="line-height: 1; z-index: 200;">
                                    <p class="input_cal">
                                        <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                        </asp:ImageButton>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                            ControlToValidate="txtFechaInicio" Display="Dynamic" EmptyValueBlurredText="*"
                                            EmptyValueMessage="Fecha 'Del' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                            InvalidValueMessage="Fecha 'Del' no v�lida" IsValidEmpty="False" ValidationGroup="ValidaCampos">
                                        </cc1:MaskedEditValidator>
                                    </p>
                                    <span class="entre">al: </span>
                                    <p class="input_cal">
                                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                            Enabled="true" ValidationGroup="ValidaCampos" ControlToValidate="txtFechaFin"
                                            Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                            Text="*"></asp:RequiredFieldValidator>
                                        <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                                        </asp:ImageButton>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                            ControlToValidate="txtFechaFin" Display="None" Visible="true" EmptyValueBlurredText="*"
                                            EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                            InvalidValueMessage="Fecha 'Al' no v�lida" IsValidEmpty="False" ValidationGroup="ValidaCampos">
                                        </cc1:MaskedEditValidator>
                                        <div style="clear: both;">
                                        </div>
                                        <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                            ControlToCompare="txtFechaFin" ControlToValidate="txtFechaInicio" Operator="LessThanEqual"
                                            Type="Date" ValidationGroup="ValidaCampos">*</asp:CompareValidator>
                                    </p>
                                </li>
                                <li style="padding-left: 33px; width: auto;">
                                    <b>* El reporte mostrar&aacute; la informaci&oacute;n actualizada despu&eacute;s de 10 minutos.</b>
                                </li>
                            </ul>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                                MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                                MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                                PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                                PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanelImage" runat="server" UpdateMode="Conditional" RenderMode="Inline"
                        ChildrenAsTriggers="False">
                        <ContentTemplate>
                            <ul id="fsBotonera" class="datos_cip2">
                                <li id="Li1" class="complet">
                                    <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" Style="padding-left: 200px"
                                        ValidationGroup="ValidaCampos" />
                                    <asp:Button ID="btnGenerarReporte" runat="server" CssClass="input_azul6" Text="Ver Reporte"
                                        ValidationGroup="ValidaCampos" />
                                    <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" CausesValidation="false"
                                        OnClick="btnLimpiar_Click" Text="Limpiar" />
                                    <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Enabled="false"
                                        Text="EXCEL" />
                                    <asp:Button ID="btnExportarPDF" runat="server" CssClass="input_azul4" Enabled="false"
                                        Text="PDF" />
                                </li>
                            </ul>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                            <asp:PostBackTrigger ControlID="btnExportarExcel" />
                            <asp:PostBackTrigger ControlID="btnExportarPdf" />
                        </Triggers>
                    </asp:UpdatePanel>
                </fieldset>
                <div id="divResult" class="result" runat="server" visible="false">
                    <h2 style="text-align: center">
                        Reporte generado</h3>
                        <asp:Label ID="lblTitulo" runat="server" Font-Names="Tahoma" Font-Size="Small" Font-Bold="True"
                            Style="padding-left: 50px"></asp:Label>
                        <rsweb:ReportViewer ID="rptPagoServicios" runat="server" ShowRefreshButton="False"
                            Width="650px" ShowPageNavigationControls="False" ShowExportControls="False" SizeToReportContent="True"
                            ShowDocumentMapButton="False" ShowPrintButton="False" ShowBackButton="False"
                            ShowFindControls="False" ShowToolBar="False" ShowWaitControlCancelLink="False"
                            ShowZoomControl="False">
                        </rsweb:ReportViewer>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnGenerarReporte"></asp:PostBackTrigger>
            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">

        function cargarMultiselect() {
            $("#slcServicios").dropdownchecklist({ maxDropHeight: 150, width: 216, emptyText: '::Todos::',
                onComplete: function (selector) {
                    var hdfIdServicio = $('#<%= hdfIdServicios.ClientID() %>');
                    var hdfServicios = $('#<%= hdfServicios.ClientID() %>');
                    var hdfServiciosFormato = $('#<%= hdfServiciosFormato.ClientID() %>');

                    hdfIdServicio.val('');
                    hdfServicios.val('');
                    hdfServiciosFormato.val('');
                    for (i = 0; i < selector.options.length; i++) {
                        if (selector.options[i].selected) {
                            hdfIdServicio.val(hdfIdServicio.val() + "," + selector.options[i].value);
                            hdfServicios.val(hdfServicios.val() + "," + selector.options[i].text);
                            hdfServiciosFormato.val(hdfServiciosFormato.val() + "','" + selector.options[i].value);
                        }
                    }
                }
            });
        }
        $(document).ready(function () {
            cargarMultiselect();
        });
        function AjaxEnd(sender, args) {
            $(".divLoadingMensaje").hide('fast');
            $(".divLoading").hide('fast');
            if ($(".ui-dropdownchecklist").val() == undefined) {
                var selector = $("#slcServicios");
                var hdfIdServicio = $('#<%= hdfIdServicios.ClientID() %>');
                var idsServicios = hdfIdServicio.val().split(",");
                for (i = 0; i < selector[0].options.length; i++) {
                    for (j = 0; j < idsServicios.length; j++) {
                        if (selector[0].options[i].value == idsServicios[j]) {
                            selector[0].options[i].selected = true;
                        }
                    }
                }
                cargarMultiselect();
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }
    </script>
</asp:Content>
