Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Imports System.Data
Imports SPE.Entidades
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing.Printing
Imports System.Drawing.Imaging



Partial Class ECO_PgPrl
    Inherits _3Dev.FW.Web.PageBase

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolRepresentante})
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            '

            Page.SetFocus(ddlServicio)
            Page.Form.DefaultButton = btnGenerarReporte.UniqueID
            CargarDatos()

            '
        End If

    End Sub

    Private Sub InicializarFechas()
        txtFechaInicio.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'DateTime.Now.ToShortDateString()
        txtFechaFin.Text = DateTime.Now.ToShortDateString()
    End Sub

    'CARGAR DATOS
    Private Sub CargarDatos()
        '
        'FECHAS
        InicializarFechas()

        Dim objEmpresa As New SPE.Web.CEmpresaContratante
        Dim objComun As New SPE.Web.CAdministrarComun
        Dim objParametro As New SPE.Web.CAdministrarParametro
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim objCComun As New SPE.Web.CComun

        'EMPRESA
        If UserInfo.Rol = SPE.EmsambladoComun.ParametrosSistema.RolJefeProducto Then
            pnlEmpresa.Visible = True
            ddlEmpresa.DataTextField = "RazonSocial" : ddlEmpresa.DataValueField = "IdEmpresaContratante" : ddlEmpresa.DataSource = objEmpresa.SearchByParameters(New BEEmpresaContratante()) : ddlEmpresa.DataBind()
        End If

        'SERVICIO
        CargarServicios()

        'MONEDA
        ddlMoneda.DataTextField = "Descripcion" : ddlMoneda.DataValueField = "IdMoneda" : ddlMoneda.DataSource = objComun.ConsultarMoneda() : ddlMoneda.DataBind()
        '
        'Tipo
        ddlTipo.DataTextField = "Descripcion" : ddlTipo.DataValueField = "Id" : ddlTipo.DataSource = objParametro.ListarTiposConsultaPagoServicios() : ddlTipo.DataBind()

        'ORIGEN CANCELACION
        ddlOrigenCancelacion.DataTextField = "Descripcion" : ddlOrigenCancelacion.DataValueField = "IdTipoOrigenCancelacion" : ddlOrigenCancelacion.DataSource = objCComun.ConsultarTipoOrigenCancelacion() : ddlOrigenCancelacion.DataBind() : ddlOrigenCancelacion.Items.Insert(0, "::: Todos :::")

    End Sub

    Private Sub CargarServicios()
        Dim obeServicio As New BEServicio
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim Formato As String
        Dim listServicios As List(Of BEServicio)
        If UserInfo.Rol = SPE.EmsambladoComun.ParametrosSistema.RolJefeProducto Then
            obeServicio.IdServicio = ddlEmpresa.SelectedValue
            listServicios = objCAdministrarServicio.ConsultarServicioPorIdEmpresa(obeServicio)
            Formato = "<option value='{0}'>{1}</option>"
        Else
            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
            listServicios = objCAdministrarServicio.ConsultarServiciosPorIdUsuario(obeServicio)
            Formato = "<option value='{0}'>{1} - ({2})</option>"
        End If
        Dim cmbServicios As New StringBuilder("<select id=""slcServicios"" multiple="""" style=""display: none; "">")
        For Each oServicio As BEServicio In listServicios
            cmbServicios.AppendFormat(Formato, oServicio.IdServicio, oServicio.Nombre, oServicio.NombreEmpresaContrante)
        Next
        cmbServicios.Append("</select>")
        ddlServicio.Text = cmbServicios.ToString()
    End Sub

    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        CargarServicios()
    End Sub

    'GENERAR REPORTE
    Protected Sub btnGenerarReporte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click

        'divResult.Visible = True

        'CargarReporte()
        'Dim msg As New MensajeInformacion

        'lblTitulo.Text = msg.MensajeResultadoReportePS(hdfServicios.Value, CDate(txtFechaInicio.Text), CDate(txtFechaFin.Text), ddlTipo.SelectedIndex)
        Try
            divResult.Visible = True
            Dim dif As Integer

            If chbMes.Checked Then
                dif = DateDiff(DateInterval.Month, CDate(txtFechaInicio.Text), CDate(txtFechaFin.Text))
                If dif <= 3 Then
                    'Jw
                    CargarReporte()
                    Dim msg As New MensajeInformacion
                    lblTitulo.ForeColor = Drawing.Color.Black
                    lblTitulo.Text = msg.MensajeResultadoReportePS(hdfServicios.Value, CDate(txtFechaInicio.Text), CDate(txtFechaFin.Text), ddlTipo.SelectedIndex)
                Else
                    rptPagoServicios.LocalReport.DataSources.Clear()
                    rptPagoServicios.LocalReport.Refresh()
                    rptPagoServicios.Visible = False
                    btnExportarExcel.Enabled = False
                    btnExportarPDF.Enabled = False
                    lblTitulo.ForeColor = Drawing.Color.Red
                    lblTitulo.Text = "El intervalo no debe ser mayor a 3 meses"
                End If
            Else
                dif = DateDiff(DateInterval.Day, CDate(txtFechaInicio.Text), CDate(txtFechaFin.Text))
                If dif <= 30 Then
                    CargarReporte()
                    Dim msg As New MensajeInformacion
                    lblTitulo.ForeColor = Drawing.Color.Black
                    lblTitulo.Text = msg.MensajeResultadoReportePS(hdfServicios.Value, CDate(txtFechaInicio.Text), CDate(txtFechaFin.Text), ddlTipo.SelectedIndex)
                Else
                    rptPagoServicios.LocalReport.DataSources.Clear()
                    rptPagoServicios.LocalReport.Refresh()
                    rptPagoServicios.Visible = False
                    btnExportarExcel.Enabled = False
                    btnExportarPDF.Enabled = False
                    lblTitulo.ForeColor = Drawing.Color.Red
                    lblTitulo.Text = "El intervalo no debe ser mayor a 30 d�as"
                End If
            End If


        Catch ex As Exception

        End Try
    End Sub



    'CARGAR REPORTE
    Private Sub CargarReporte()
        '
        Try
            '
            rptPagoServicios.Visible = True
            Dim listBEOrdenPago As New List(Of BEOrdenPago)

            listBEOrdenPago = GetListBEOrdenPago()

            If rptPagoServicios.LocalReport.DataSources.Count > 0 Then
                rptPagoServicios.LocalReport.DataSources.RemoveAt(0)
            End If

            rptPagoServicios.LocalReport.DataSources.Add(New ReportDataSource("BEOrdenPago", listBEOrdenPago))
            rptPagoServicios.LocalReport.ReportPath = "Reportes\RptPagoSevicios.rdlc"

            Dim listaParametros As List(Of ReportParameter) = GetParametros(listBEOrdenPago)

            Dim paramFlagFecha As New ReportParameter("parFecha", IIf(chbMes.Checked, 1, 0).ToString)

            listaParametros.Add(paramFlagFecha)

            rptPagoServicios.LocalReport.SetParameters(listaParametros)
            rptPagoServicios.LocalReport.Refresh()
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        '
    End Sub

    'IMAGEN PARA EXPORTAR
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        '
        Dim listBEOrdenPago As New List(Of BEOrdenPago)
        '
        listBEOrdenPago = GetListBEOrdenPago()
        '
        If listBEOrdenPago.Count > 0 Then
            '
            ProcesoExportar("EXCEL", "xls", listBEOrdenPago)
            '
        End If
        '
    End Sub

    Protected Sub btnExportarPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarPDF.Click
        '
        Dim listBEOrdenPago As New List(Of BEOrdenPago)
        '
        listBEOrdenPago = GetListBEOrdenPago()
        '
        If listBEOrdenPago.Count > 0 Then
            '
            ProcesoExportar("PDF", "pdf", listBEOrdenPago)
            '
        End If
        '
    End Sub

    'OBTENEMOS LOS PARAMETROS
    Private Function GetParametros(ByVal listBEOrdenPago As List(Of BEOrdenPago)) As List(Of ReportParameter)
        '
        Dim paramList As New List(Of ReportParameter)
        Dim param1 As ReportParameter

        Dim titulo As String = ""

        'If (ddlTipo.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.TipoConsultaPG.Montos) Then
        '    titulo = "Pagos del Servicio ''" & ddlServicio.SelectedItem.Text & "'' desde: " & txtFechaDe.Text & " al " & txtFechaA.Text & " en " & ddlMoneda.SelectedItem.Text
        'Else
        '    titulo = "Nro de Operaciones del Servicio ''" & ddlServicio.SelectedItem.Text & "'' desde: " & txtFechaDe.Text & " al " & txtFechaA.Text & " en " & ddlMoneda.SelectedItem.Text
        'End If

        If Not ddlOrigenCancelacion.SelectedIndex = 0 Then
            titulo = titulo & " - por " & ddlOrigenCancelacion.SelectedItem.Text
        End If
        Dim paramFlagFecha As New ReportParameter("parFecha", IIf(chbMes.Checked, 1, 0).ToString)
        If listBEOrdenPago.Count > 0 Then
            param1 = New ReportParameter("parTitulo", titulo)


            btnExportarExcel.Enabled = True
            btnExportarPDF.Enabled = True
        Else
            param1 = New ReportParameter("parTitulo", "")
            btnExportarExcel.Enabled = False
            btnExportarPDF.Enabled = False
        End If
        '
        paramList.Add(paramFlagFecha)
        paramList.Add(param1)
        '
        Return paramList
        '
    End Function

    'OBTENEMOS LA LISTA DE AGENTE CAJA
    Private Function GetListBEOrdenPago() As List(Of BEOrdenPago)
        '
        Dim listBEOrdenPago As New List(Of SPE.Entidades.BEOrdenPago)
        Dim objCServicio As New SPE.Web.CServicio
        Dim obeOrdenPago As New BEOrdenPago

        Dim IdServicios As String = hdfIdServicios.Value
        Dim ArrayServicios As String = ""
        If IdServicios <> "" Then
            ArrayServicios = hdfIdServicios.Value.ToString.Substring(1, hdfIdServicios.Value.ToString.Length - 1)
        End If
        obeOrdenPago.IdsServicios = ArrayServicios
        obeOrdenPago.IdMoneda = Convert.ToInt32(ddlMoneda.SelectedValue)
        obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Cancelada
        obeOrdenPago.FechaCreacion = Convert.ToDateTime(txtFechaInicio.Text)
        obeOrdenPago.FechaActualizacion = Convert.ToDateTime(txtFechaFin.Text)
        obeOrdenPago.DescripcionEstado = ddlTipo.SelectedValue

        If ddlOrigenCancelacion.SelectedIndex = 0 Then
            obeOrdenPago.IdTipoOrigenCancelacion = 0
        Else
            obeOrdenPago.IdTipoOrigenCancelacion = ddlOrigenCancelacion.SelectedValue
        End If

        If UserInfo.Rol = SPE.EmsambladoComun.ParametrosSistema.RolRepresentante Then
            obeOrdenPago.IdOrdenPago = UserInfo.IdUsuario
            listBEOrdenPago = objCServicio.ConsultarPagoServicios(obeOrdenPago)
        Else
            obeOrdenPago.IdEmpresa = ddlEmpresa.SelectedValue
            listBEOrdenPago = objCServicio.ConsultarPagoServiciosNoRepresentante(obeOrdenPago)
        End If

        Return listBEOrdenPago
        '
    End Function

    'PROCESO PARA EXPORTAR
    Private Sub ProcesoExportar(ByVal opcion As String, ByVal ext As String, ByVal listBEOrdenPago As List(Of BEOrdenPago))
        '
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")

        Dim localReport As New LocalReport()

        localReport.ReportPath = Server.MapPath("~\Reportes\RptPagoSevicios.rdlc")

        localReport.SetParameters(GetParametros(listBEOrdenPago))

        If localReport.DataSources.Count > 0 Then
            localReport.DataSources.RemoveAt(0)
        End If

        localReport.DataSources.Add(New ReportDataSource("BEOrdenPago", listBEOrdenPago))

        ResponseExport(localReport, opcion, ext) 'asd
        '
    End Sub

    'RESONSE DE EXPORTACION
    Private Sub ResponseExport(ByVal localReport As LocalReport, ByVal Type As String, ByVal fileExtension As String)
        '
        Dim reportType As String = Type
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim fileNameExtension As String = fileExtension

        Dim deviceInfo As String = _
        "<DeviceInfo>" + _
        "  <OutputFormat>" + reportType + "</OutputFormat>" + _
        "</DeviceInfo>"

        Dim warnings As Warning() = Nothing
        Dim streams As String() = Nothing
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render( _
            reportType, _
            deviceInfo, _
            mimeType, _
            encoding, _
            fileNameExtension, _
            streams, _
            warnings)

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=ReportePagoServicio_" + Date.Now.ToShortDateString + "." + fileNameExtension)
        Response.BinaryWrite(renderedBytes)
        Response.End()
        '
    End Sub

    Sub Limpiar()
        'Me.ddlServicio.Text = 0

        'Dim obeServicio As New BEServicio
        'Dim objCAdministrarServicio As New SPE.Web.CServicio
        'obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario

        'Dim listServicios As List(Of BEServicio) = objCAdministrarServicio.ConsultarServiciosPorIdUsuario(obeServicio)
        'Dim cmbServicios As New StringBuilder("<select id=""slcServicios"" multiple="""" style=""display: none; "">")
        'For index = 0 To listServicios.Count - 1
        '    cmbServicios.Append("<option value ='" & listServicios(index).IdServicio & "'>" & listServicios(index).Nombre & "</option>")
        'Next
        'cmbServicios.Append("</select>")
        'ddlServicio.Text = cmbServicios.ToString()

        'Me.ddlOrigenCancelacion.SelectedIndex = 0
        'Me.ddlMoneda.SelectedIndex = 0
        'Me.ddlTipo.SelectedIndex = 0
        'InicializarFechas()
        'rptPagoServicios.LocalReport.DataSources.Clear()
        'rptPagoServicios.LocalReport.Refresh()
        'rptPagoServicios.Visible = False
        'btnExportarExcel.Enabled = False
        'btnExportarPDF.Enabled = False
        'lblTitulo.Text = String.Empty
        'divResult.Visible = False
        Response.Redirect("~\ECO\PgPrl.aspx")
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Limpiar()
    End Sub


End Class