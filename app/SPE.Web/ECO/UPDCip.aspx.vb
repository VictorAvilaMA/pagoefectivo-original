﻿Imports SPE.Web
Imports SPE.Entidades
Imports SPE.Web.Util
Imports _3Dev.FW.Util.DataUtil


Partial Class ECO_UPDCip
    Inherits PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtFechaExpiracion.Text = DateAdd(DateInterval.Day, 1, DateTime.Now).ToString("dd/MM/yyyy hhh:mm:00 tt") 'Today.ToShortDateString
        End If
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As System.EventArgs) Handles btnActualizar.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        Dim fechaExpiracion As New DateTime
        oBEOrdenPago.NumeroOrdenPago = txtNroCIP.Text.PadLeft(14, "0")
        If DateTime.TryParseExact(txtFechaExpiracion.Text, "dd/MM/yyyy hhh:mm:00 tt", Nothing, Globalization.DateTimeStyles.None, fechaExpiracion) Then
            oBEOrdenPago.FechaAExpirar = fechaExpiracion
        End If
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim intResult As Int64 = oCOrdenPago.ActualizarCIP(oBEOrdenPago)
        Select Case intResult
            Case Is > 0
                lblMensaje.Text = ("Se actualizó con éxito el CIP Nº " & intResult.ToString().PadLeft(14, "0"))
                DesactivarControles()
                btnNuevo.Visible = True
            Case -1
                JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
            Case -2
                JSMessageAlert("Validación", "El cip ingresado no existe", "val")
            Case -3
                JSMessageAlert("Validación", "El cip ingresado no pertenece a su empresa", "val")
            Case -4
                JSMessageAlert("Validación", "El cip ingresado ya se encuentra expirado", "val")
            Case -5
                JSMessageAlert("Validación", "El cip ingresado no se encuentra en estado generado", "val")
            Case -6
                JSMessageAlert("Validación", "La fecha de expiración debe ser mayor a la de ahora", "val")
            Case -7
                JSMessageAlert("Validación", "La fecha de expiración no puede exeder los 10 años", "val")
            Case Else
                JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
        End Select
    End Sub
    Protected Sub DesactivarControles()
        txtNroCIP.Enabled = False
        txtFechaExpiracion.Enabled = False
        btnActualizar.Enabled = False
        ibtnFechaExpiracion.Enabled = False
        btnActualizar.Visible = False
    End Sub

    Protected Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("UPDCip.aspx")
    End Sub

    Protected Sub ibtnBuscarCIP_Click(sender As Object, e As System.EventArgs) Handles ibtnBuscarCIP.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        oBEOrdenPago.NumeroOrdenPago = txtNroCIP.Text.Trim()
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim result As BEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorIdOrdenYIdRepresentante(oBEOrdenPago)
        If result Is Nothing Then
            divDatos.Visible = False
            JSMessageAlert("Validación", "No se encontro el número de CIP o no pertenece a su empresa", "val")
        Else
            divDatos.Visible = True
            txtConceptoPagoInfo.Text = result.ConceptoPago
            txtMontoInfo.Text = result.Total.ToString()
            txtEstadoInfo.Text = result.DescripcionEstado
            txtServicioInfo.Text = result.oBEServicio.Nombre
        End If
    End Sub
End Class
