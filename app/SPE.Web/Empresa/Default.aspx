﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Empresa_Default" %>

<%--PM--%>
<%@ Register Assembly="WebControlCaptcha" Namespace="WebControlCaptcha" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="head1" runat="server">
    <meta charset="utf-8">
    <title>Pago Efectivo - Transacciones Seguras por Internet en el Perú</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if lt IE 9]>
  <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/js/html5.js"></script>
<![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="perucomsultores.pe" />
    <meta name="viewport" content="width=device-width" />
    <meta name="language" content="es" />
    <meta name="geolocation" content="Peru" />
    <meta name="robots" content="index,follow" />
    <meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta property="og:description" content="" />
    <meta property="og:image" content='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/CIPGenerado/pago-efectivo.png" %>' />
    <link href="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile")%>/favicon.ico"
        rel="icon" type="image/vnd.microsoft.icon" />
    <link href="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile")%>/favicon.ico"
        rel="shortcut icon" type="image/x-icon" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/servicio.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
        rel="stylesheet" charset="utf-8" media="screen" type="text/css" />
<%--    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/waiting.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
        rel="stylesheet" charset="utf-8" media="screen" type="text/css" />--%>
</head>
<body>
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <!-- Begin comScore UDM code 1.1104.26 -->
    <!-- End comScore UDM code -->
    <div class="blockfix hide">
    </div>
    <%--<div id="demo-overlay-false">--%>
        <div class="hack-shadow">
        </div>
        <header class="cip">
        <div class="content-header">        
    	    <div class="top-head">
                <div class="content-top-head">
        	        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/grupo-comercio.png" width="97" height="16" alt="Grupo El Comercio" title="Grupo El Comercio" border="0" />
                </div>
            </div>
            <div class="head">
                <div class="content-head">
        	        <h1><img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/pago-efectivo.png" width="149" height="60" alt="Pago Efectivo - Transacciones seguras por Internet en el Perú" title="Pago Efectivo - Transacciones seguras por Internet en el Perú">&nbsp;</h1>
                    <h2>Transacciones seguras por Internet en el Perú</h2>
                        
                </div>         
            </div>
        </div>
               
    </header>
        <div id="wrapper">
            <div class="content">
                <div class="content_comprar">
                    <asp:Panel ID="PanelGeneral" runat="server">
                        <div class="row">
                            <div class="img_logo">
                                <asp:Image ID="imgImagenLogo" Visible="false" runat="server" Height="90" Width="225" />
                            </div>
                            <h2 class="title_img">
                                <asp:Label ID="lblNombreServicio" CssClass="bold" runat="server" Text=""></asp:Label></h2>
                        </div>
                        <p class="ptxt">
                            Para poder completar tu compra, es necesario confirmar los datos del producto o
                            servicio y llenar tus datos personales.</p>
                        <h2 class="title_box">
                            Datos del Producto o Servicio</h2>
                        <asp:UpdatePanel ID="upnlProductos" runat="server">
                            <ContentTemplate>
                                <div class="frm_group">
                                    <label>
                                        Producto / Servicio:</label>
                                    <div class="frm_control">
                                        <asp:DropDownList ID="ddlProducto" runat="server" AutoPostBack="true" Visible="true">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtProducto" runat="server" autocomplete="off" CssClass="input-text"
                                            Visible="false" Enabled="false" />
                                        <asp:Label ID="lblIdProductotxt" runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lblErrPrd" runat="server" CssClass="inicial"></asp:Label>
                                    </div>
                                </div>
                                <div class="frm_group2">
                                    <label>
                                        Monto:</label>
                                    <div class="frm_control">
                                        <asp:Label ID="lblMoneda" runat="server" Visible="true" Text="S/."></asp:Label>
                                        <asp:TextBox ID="txtMonto" runat="server" autocomplete="off" CssClass="input-text2"
                                            MaxLength="10" Enabled="false" />
                                        <asp:Label ID="lblErrMonto" runat="server" CssClass="inicial"></asp:Label>
                                        <asp:Label ID="lblIdOrdenPago" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <h2 class="title_box">
                            Datos del Usuario</h2>
                        <div class="row">
                            <div class="col_forms">
                                <div class="frm_group">
                                    <label>
                                        Tipo Documento:</label>
                                    <div class="frm_control">
                                        <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="neu" TabIndex="10">
                                            <asp:ListItem Value="1">DNI</asp:ListItem>
                                            <asp:ListItem Value="2">Pasaporte</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="frm_group">
                                    <label for="txtApellidos">
                                        Apellidos:</label>
                                    <div class="frm_control">
                                        <asp:TextBox ID="txtApellidos" runat="server" CssClass="input-text" MaxLength="100"
                                            autocomplete="off" TabIndex="12" ></asp:TextBox>
                                    </div>
                                </div>
                                <div class="frm_group">
                                    <label for="txtEmail">
                                        Email:</label>
                                    <div class="frm_control">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="input-text" MaxLength="100" autocomplete="off"
                                            TabIndex="14" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col_forms">
                                <div class="frm_group">
                                    <label for="txtNroDocumento">
                                        N° Documento:</label>
                                    <div class="frm_control">
                                        <asp:TextBox ID="txtNroDocumento" runat="server" CssClass="input-text" MaxLength="8"
                                            autocomplete="off" TabIndex="11" ></asp:TextBox>
                                    </div>
                                </div>
                                <div class="frm_group">
                                    <label for="txtNombre">
                                        Nombre:</label>
                                    <div class="frm_control">
                                        <asp:TextBox ID="txtNombre" runat="server" CssClass="input-text" MaxLength="100"
                                            autocomplete="off" TabIndex="13" ></asp:TextBox>
                                    </div>
                                </div>
                                <div class="frm_group">
                                    <label for="txtTelefono">
                                        Tel&eacute;fono:</label>
                                    <div class="frm_control">
                                        <asp:TextBox ID="txtTelefono" runat="server" CssClass="input-text" MaxLength="9"
                                            autocomplete="off" TabIndex="15" ></asp:TextBox>
                                    </div>
                                    <asp:Label ID="lblIdProducto" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblIdServicio" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblDescripcion" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblIdMoneda" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblDescMoneda" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblidEditable" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblidEstado" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblFechaCreacion" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblUsuarioId" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblRutaClaPrivas" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblIdUsuario" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblMailComercio" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblClaveAPI" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblCodigo" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Label ID="lblParEmpresa" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="pt20 align-center">
                            <asp:Button ID="btnPagar" runat="server" CssClass="btn btn-primary btn-middle mt20"
                                Text="Pagar" />
                            <br />
                        </div>
                        <div style="float: left; height: 15px; width: 100%;">
                            <asp:Label ID="lblMensajeError" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:Literal ID="ltlGenPagoIF" runat="server" />
                </div>
            </div>
        </div>
        <footer id="foot1" runat="server">
        
    	<div id="content-footer">
        	<span class="foot-logo">
            	<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/foot-pago-efectivo.png" width="97" height="40" alt="Pago Efectivo" title="Pago Efectivo" border="0">
            </span>
        	<ul>
            	<li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Default.aspx">Inicio">Inicio</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/personas.aspx">Personas</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Empresas.aspx">Empresas</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Contactenos.aspx">Contáctenos</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Ayuda.aspx">Ayuda</a></li>
            </ul>
            
            <a class="help-center" href="http://centraldeayuda.pagoefectivo.pe/home">
            	<span class="img"></span>
                <span class="content-help">
                	<span class="title">Central de Ayuda</span>
                	<span class="desc">¿Tienes alguna duda o consulta? estamos para ayudarte</span>
                </span>                
            </a>         
        </div>
        <div class="footer-link">
        	<span>Visite también:</span>
        	<ul>
            	<li class="first"><a target="_blank" title="Diario el Comercio" href="http://elcomercio.pe/?ref=pef">elcomercio.pe</a></li>
                <li><a target="_blank" title="Peru.com" href="http://peru.com/?ref=pef">peru.com</a></li>
                <li><a target="_blank" title="Diario Perú21" href="http://peru21.pe/?ref=pef">peru21.pe</a></li>
                <li><a target="_blank" title="Diario Gestión" href="http://gestion.pe/?ref=pef">gestion.pe</a></li>
                <li><a target="_blank" title="Depor.pe" href="http://depor.pe/?ref=pef">depor.pe</a></li>
                <li><a target="_blank" title="Diario Trome" href="http://trome.pe/?ref=pef">trome.pe</a></li>
                <li><a target="_blank" title="Publimetro" href="http://publimetro.pe/?ref=pef">publimetro.pe</a></li>
                <li><a target="_blank" title="GEC" href="http://gec.pe/?ref=pef">gec.pe</a></li>
                <li><a target="_blank" title="Clasificados.pe" href="http://clasificados.pe/?ref=pef">clasificados.pe</a></li>
                <li><a target="_blank" title="Aptitus" href="http://aptitus.pe/?ref=pef">aptitus.pe</a></li>                
                <li><a target="_blank" title="Urbania" href="http://urbania.pe/?ref=pef">urbania.pe</a></li>
                <li><a target="_blank" title="Neoauto" href="http://neoauto.pe/?ref=pef">neoauto.pe</a></li>
                <li class="first"><a target="_blank" title="GuiaGPS" href="http://guiagps.pe/?ref=pef">guiagps.pe</a></li>
                <li><a target="_blank" title="Ofertop" href="http://ofertop.pe/?ref=pef">ofertop.pe</a></li>
                <li><a target="_blank" title="iQuiero" href="http://iquiero.com/?ref=pef">iquiero.com</a></li>
                <li><a target="_blank" title="NuestroMercado" href="http://nuestromercado.pe/?ref=pef">nuestromercado.pe</a></li>
                <li><a target="_blank" title="Club de Suscriptores" href="http://clubsuscriptores.pe/?ref=pef">clubsuscriptores.pe</a></li>
                <li><a target="_blank" title="PeruRed" href="http://perured.pe/?ref=pef">perured.pe</a></li>
                <li><a target="_blank" title="Shopin" href="http://shopin.pe/?ref=pef">shopin.pe</a></li>                
            </ul>         
        </div>
    <%--</div>--%>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-1.6.1.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/slides.min.jquery.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/miniApp.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/modules.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-1.9.0.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/script.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery.validate.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
<%--    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery.waiting.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        type="text/javascript"></script>--%>
    </footer>
    </form>
</body>
</html>
