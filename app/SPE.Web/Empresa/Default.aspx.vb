﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Net
Imports System.IO
Imports SPE.Web.Util
Imports System.Threading
Imports System.Collections.Generic
Imports System.Globalization
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports _3Dev.FW.Web.Log
Imports SPE.Utilitario
Imports SPE.Api
Imports SPE.Api.Proxys
Imports System.Xml
Imports System.Web.UI
Imports System.Web.UI.Page
Imports _3Dev.FW.Web
Partial Class Empresa_Default

    Inherits System.Web.UI.Page

    Public oBEOrdenPago As New BEOrdenPago()
    Public oBESolicitudPago As New BESolicitudPago()
    Public htTabla As New Hashtable
    ' Public oBEServicio As New BEServicio()



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'btnBUscar.Visible = False
        If Not IsPostBack Then
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
            Try
                htTabla = (ArmarTabla(System.Configuration.ConfigurationManager.AppSettings("CadServDesc")))

                'dato rescatado de la URL
                Dim strParametro As String = Request.QueryString("emp").ToString().ToLower()
                lblParEmpresa.Text = strParametro
                'id del Servicio, ubicado en archivo de configuracion para facil acceso
                Dim strIdServicio As String

                If Not (htTabla(strParametro) Is Nothing) Then
                    strIdServicio = htTabla(strParametro).ToString()
                    lblIdServicio.Text = strIdServicio
                    'Controladora General
                    Dim CtrlServicio As New CServicio()

                    'Servicio General
                    Dim oBEServicio As New BEServicio()
                    oBEServicio = CtrlServicio.RecuperarDatosServicioDesconectado(Convert.ToInt16(strIdServicio))
                    If oBEServicio Is Nothing Then
                        Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("pathBase"))
                    End If

                    'Productos del Servicio
                    Dim oBESPR As New BEServicioProductoRequest
                    oBESPR.idServicio = strIdServicio
                    'Dim oBEServicioProducto As New List(Of BEServicioProducto)
                    'oBEServicioProducto = CtrlServicio.RecuperarDatosProductoServicio(oBESPR)

                    Dim objCServicio As New SPE.Web.CServicio
                    Dim ListSP As List(Of BEServicioProducto) = CtrlServicio.RecuperarDatosProductoServicio(oBESPR)
                    If ListSP.Count = 1 Then
                        txtProducto.Visible = True
                        ddlProducto.Visible = False
                        txtProducto.Text = ListSP(0).Descripcion
                        lblDescripcion.Text = ListSP(0).Descripcion
                        lblIdProducto.Text = ListSP(0).idProducto.ToString
                        lblIdMoneda.Text = ListSP(0).idMoneda.ToString
                        If ListSP(0).idMoneda.ToString = 1 Then
                            lblMoneda.Text = "S/."
                        Else
                            lblMoneda.Text = "$/."
                        End If
                        txtMonto.Text = ListSP(0).Monto.ToString

                        If ListSP(0).idEditable = 0 Then
                            txtMonto.Enabled = False
                        Else
                            txtMonto.Enabled = True
                        End If
                    Else
                        txtProducto.Visible = False
                        ddlProducto.Visible = True
                        WebUtil.DropDownlistBinding(ddlProducto, ListSP, "descripcion", "idproducto", "::: Seleccione :::")
                    End If



                    'datos de servicio
                    lblRutaClaPrivas.Text = oBEServicio.RutaClaPrivas
                    lblIdUsuario.Text = oBEServicio.IdUsuario
                    lblMailComercio.Text = oBEServicio.MailComercio
                    lblClaveAPI.Text = oBEServicio.ClaveAPI
                    lblNombreServicio.Text = oBEServicio.Nombre
                    lblCodigo.Text = oBEServicio.Codigo

                    'imagen servicio
                    Dim bytes As Byte() = oBEServicio.LogoImagen
                    Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                    imgImagenLogo.ImageUrl = "data:image/png;base64," & base64String
                    imgImagenLogo.Visible = True
                    'upnlPaginas.Visible = False


                    Me.head1.DataBind()
                    foot1.DataBind()
                    findControlToDataBind(Me.Controls)
                    Dim CtrlOrdenPago As New COrdenPago()
                    Dim RutaRaiz As String = ""

                    Dim ci As New CultureInfo("Es-Es")
                    Dim diaDeSemana As String = ci.DateTimeFormat.GetDayName(oBEOrdenPago.FechaVencimiento.DayOfWeek)
                    Dim diaNumero As String = oBEOrdenPago.FechaVencimiento.Day.ToString().PadLeft(2, "0")
                    Dim mesNombre As String = ci.DateTimeFormat.GetMonthName(oBEOrdenPago.FechaVencimiento.Month)

                    Dim CtrPlantilla As New CPlantilla()
                    Dim valoresDinamicos As New Dictionary(Of String, String)
                    valoresDinamicos.Add("[CIP]", "23")
                    valoresDinamicos.Add("[FechaVencimiento]", "23/12/2013")
                    valoresDinamicos.Add("[HoraVencimiento]", "05:06:23")
                    valoresDinamicos.Add("[EmailCLiente]", "pjmfb24@gmail.com")
                Else
                    Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("pathBase"))
                End If

            Catch ex As Exception
                Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("pathBase"))
            End Try


        Else
            If Not (ScriptManager.GetCurrent(Me.Page).IsInAsyncPostBack) Then
                If ddlProducto.SelectedIndex = 0 And txtMonto.Text = "" Then
                    lblErrMonto.Text = "Este campo es necesario."
                    lblErrPrd.Text = "Este campo es necesario."
                End If
                If ddlProducto.SelectedIndex <> -1 And txtProducto.Text = "" Or txtProducto.Text <> "" Then

                    If ValidarMonto(txtMonto.Text) Then
                        If Session("update").ToString() = ViewState("update").ToString() Then
                            GenerarCIP()
                            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                        Else
                            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings("pathBase") + "/empresa/Default.aspx?emp=" + lblParEmpresa.Text)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        ViewState("update") = Session("update")
    End Sub

    Public Function IsAjaxRequest(request As HttpRequest) As Boolean
        If request Is Nothing Then
            Throw New ArgumentNullException("request")
        End If
        Return (request("X-Requested-With") = "XMLHttpRequest") OrElse ((request.Headers IsNot Nothing) AndAlso (request.Headers("X-Requested-With") = "XMLHttpRequest"))
    End Function


    Public Function ValidarMonto(ByVal strMonto As String) As Boolean
        If strMonto.Trim <> "" Then
            If IsNumeric(strMonto) Then
                If CDec(strMonto) > 0 Then
                    Return True
                Else
                    lblErrMonto.Text = "Ingrese un monto positivo."
                End If
            Else
                lblErrMonto.Text = "Ingrese sólo digitos."
            End If
        Else
            lblErrMonto.Text = "Este campo es necesario."
        End If
        Return False

    End Function


    Private Function ArmarTabla(ByVal strCadena As String) As Hashtable
        Dim htTabla As New Hashtable()
        Dim arreglo() As String = strCadena.Split("|")

        Dim k As Integer = 0
        While k < arreglo.Length - 1
            htTabla.Add(arreglo(k).ToLower.Trim, arreglo(k + 1))
            k += 2
        End While
        Return htTabla
    End Function


    'Private Sub Ocultar()
    '    Me.upnlPaginas.Visible = False
    'End Sub

    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCeros(ByVal valor As String) As String
        If valor.Length = 1 Then
            Return "0" + valor
        Else
            Return valor
        End If
    End Function



    Public Sub findControlToDataBind(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls And c.GetType IsNot GetType(GridView) Then
                findControlToDataBind(c.Controls)
            End If
            If c.GetType() Is GetType(ImageButton) Or c.GetType() Is GetType(Button) Or c.GetType() Is GetType(Image) Or c.GetType() Is GetType(LinkButton) Then
                c.DataBind()
            End If
        Next
    End Sub

    Private Function Filler(ByVal inicial As String, ByVal valor As String, ByVal tamano As Integer) As String

        If inicial.Length = tamano Then
            Return inicial
        End If
        Dim strRetorno As String = inicial
        For i = 1 To (tamano - inicial.Length)
            strRetorno = valor + strRetorno
        Next
        Return strRetorno
    End Function





    Public Function ArmarXML(ByVal strMoneda As String, ByVal strImporte As String, ByVal strOrdenIdComercio As String, ByVal strMailComercio As String,
                             ByVal strUsuarioId As String, ByVal strNombre As String, ByVal strApellidos As String,
                             ByVal strUsuarioEmail As String, ByVal strUsuarioTipoDoc As String, ByVal strUsuarioNumeroDoc As String, ByVal strConcepto As String, ByVal strCodigo As String) As String
        Dim strXml As String
        Dim dtExpirar As DateTime
        Dim objCParametro As New SPE.Web.CAdministrarParametro




        If strCodigo.Equals("MOP") Then
            Select Case (objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(2).Descripcion)
                Case "02-H"
                    Dim strPrm As String = objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(3).Descripcion
                    Dim strLFS As String = strPrm.Substring(strPrm.IndexOf("-") + 1, strPrm.Length - (strPrm.IndexOf("-") + 1))
                    Dim intTiempoMOP As Integer = CInt(strLFS)
                    dtExpirar = Now.AddHours(intTiempoMOP)
                Case "02-M"
                    Dim strPrm As String = objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(3).Descripcion
                    Dim strLFS As String = strPrm.Substring(strPrm.IndexOf("-") + 1, strPrm.Length - (strPrm.IndexOf("-") + 1))
                    Dim intTiempoMOP As Integer = CInt(strLFS)
                    dtExpirar = Now.AddMonths(intTiempoMOP)
                Case "02-D"
                    Dim strPrm As String = objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(3).Descripcion
                    Dim strLFS As String = strPrm.Substring(strPrm.IndexOf("-") + 1, strPrm.Length - (strPrm.IndexOf("-") + 1))
                    Dim intTiempoMOP As Integer = CInt(strLFS)
                    dtExpirar = Now.AddDays(intTiempoMOP)
            End Select
        Else
            Select Case (objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(0).Descripcion)
                Case "00-H"
                    Dim strPrm As String = objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(1).Descripcion
                    Dim strLFS As String = strPrm.Substring(strPrm.IndexOf("-") + 1, strPrm.Length - (strPrm.IndexOf("-") + 1))
                    Dim intTiempoGenerico As Integer = CInt(strLFS)
                    dtExpirar = Now.AddHours(intTiempoGenerico)
                Case "00-M"
                    Dim strPrm As String = objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(1).Descripcion
                    Dim strLFS As String = strPrm.Substring(strPrm.IndexOf("-") + 1, strPrm.Length - (strPrm.IndexOf("-") + 1))
                    Dim intTiempoGenerico As Integer = CInt(strLFS)
                    dtExpirar = Now.AddMonths(intTiempoGenerico)
                Case "00-D"
                    Dim strPrm As String = objCParametro.ConsultarParametroPorCodigoGrupo("PEDS").Item(1).Descripcion
                    Dim strLFS As String = strPrm.Substring(strPrm.IndexOf("-") + 1, strPrm.Length - (strPrm.IndexOf("-") + 1))
                    Dim intTiempoGenerico As Integer = CInt(strLFS)
                    dtExpirar = Now.AddDays(intTiempoGenerico)

            End Select
        End If



        strXml = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & _
                "<SolPago>" & _
                "<IdMoneda>" & strMoneda & "</IdMoneda>" & _
                "<Total>" & strImporte & "</Total>" & _
                "<MetodosPago>1,2</MetodosPago>" & _
                "<CodServicio>" & strCodigo & "</CodServicio>" & _
                "<Codtransaccion>" & strOrdenIdComercio & "</Codtransaccion>" & _
                "<EmailComercio>" & strMailComercio & "</EmailComercio>" & _
                "<FechaAExpirar>" & Format(dtExpirar, "dd/MM/yyyy") & " " & CompletarCeros(dtExpirar.Hour.ToString()) & ":" & CompletarCeros(dtExpirar.Minute.ToString()) & ":" & CompletarCeros(dtExpirar.Second.ToString()) & "</FechaAExpirar>" & _
                "<UsuarioId>" & strUsuarioId & "</UsuarioId>" & _
                "<DataAdicional>" & txtTelefono.Text.ToString & "</DataAdicional>" & _
                "<UsuarioNombre>" & strNombre & "</UsuarioNombre>" & _
                "<UsuarioApellidos>" & strApellidos & "</UsuarioApellidos>" & _
                "<UsuarioLocalidad>LIMA</UsuarioLocalidad>" & _
                "<UsuarioProvincia>LIMA</UsuarioProvincia>" & _
                "<UsuarioPais>PERU</UsuarioPais>" & _
                "<UsuarioAlias>Usuario" & strCodigo & "</UsuarioAlias>" & _
                "<UsuarioTipoDoc>" & strUsuarioTipoDoc & "</UsuarioTipoDoc>" & _
                "<UsuarioNumeroDoc>" & strUsuarioNumeroDoc & "</UsuarioNumeroDoc>" & _
                "<UsuarioEmail>" & strUsuarioEmail & "</UsuarioEmail>" & _
                "<ConceptoPago>" & strConcepto & "</ConceptoPago>" & _
                "<Detalles>" & _
                "<Detalle>" & _
                "<Cod_Origen>CT</Cod_Origen>" & _
                "<TipoOrigen>TO</TipoOrigen>" & _
                "<ConceptoPago>" & strConcepto & "</ConceptoPago>" & _
                "<Importe>" & strImporte & "</Importe>" & _
                "<Campo1></Campo1>" & _
                "<Campo2></Campo2>" & _
                "<Campo3></Campo3>" & _
                "</Detalle>" & _
                "</Detalles>" & _
                "<ParamsURL>" & _
                "<ParamURL>" & _
                "<Nombre>FechaHoraRegistro</Nombre>" & _
                "<Valor>" & Format(Now, "dd/MM/yyyy") & "</Valor>" & _
                "</ParamURL>" & _
                "</ParamsURL>" & _
                "<ParamsEmail>" & _
                "<ParamEmail>" & _
                "<Nombre>[UsuarioNombre]</Nombre>" & _
                "<Valor>" & strNombre & "</Valor>" & _
                "</ParamEmail>" & _
                "<ParamEmail>" & _
                "<Nombre>[Moneda]</Nombre>" & _
                "<Valor>S/.</Valor>" & _
                "</ParamEmail>" & _
                "</ParamsEmail>" & _
                "</SolPago>"

        Return strXml
    End Function


    Protected Sub btnPagar_Click(sender As Object, e As System.EventArgs) Handles btnPagar.Click


    End Sub


    Public Sub GenerarCIP()
        Dim objCServicio As New SPE.Web.CServicio
        Dim oReqDoc As New BEServicioProductoRequest

        'Agregar el idservicio
        oReqDoc.idServicio = lblIdServicio.Text

        Dim strTxtXML As String
        Dim request As New SPE.Api.Proxys.BEWSGenCIPRequestMod1
        Dim pathPublicKeyContraparte As String = System.Configuration.ConfigurationManager.AppSettings("KeyPublicPathContraparte")


        Dim pathPrivateKey As String = lblRutaClaPrivas.Text
        Dim strMailComercioIPAL As String = lblMailComercio.Text
        Dim strUsuarioIdIPAL As String = lblIdUsuario.Text



        Dim strNombre As String = txtNombre.Text
        Dim strApellidos As String = txtApellidos.Text
        Dim strEmailUsuario As String = txtEmail.Text
        Dim strClaveAPI As String = lblClaveAPI.Text
        Dim strCodigo As String = lblCodigo.Text

        Dim strMoneda As String = lblIdMoneda.Text

        Dim strImporte As String = txtMonto.Text
        Dim strTipoDoc As String = ddlTipoDocumento.SelectedValue.ToString
        Dim strNumDoc As String = txtNroDocumento.Text
        Dim strConcepto As String = ""
        If txtProducto.Text <> "" Then
            strConcepto = txtProducto.Text
        Else
            strConcepto = ddlProducto.SelectedItem.ToString
        End If


        Dim CtrlServicio As New CServicio()

        'Servicio General
        Dim oBEServicio As New BEServicio()

        Dim intOrderIdCom As Integer = CtrlServicio.RegistrarOrderIdComercio(strCodigo)
        Dim strOrderIdComercio As String = Filler(intOrderIdCom.ToString, "0", 7)
        'Dim strOrderIdComercio As String = Today.Year.ToString + CompletarCeros(Today.Month.ToString) + CompletarCeros(Today.Day.ToString) + CompletarCeros(Now.Hour.ToString) + CompletarCeros(Now.Minute.ToString) + CompletarCeros(Now.Second.ToString)




        strTxtXML = ArmarXML(strMoneda, strImporte, strOrderIdComercio, strMailComercioIPAL,
                             strUsuarioIdIPAL, strNombre, strApellidos, strEmailUsuario, strTipoDoc, strNumDoc, strConcepto, strCodigo)
        request.CodServ = strClaveAPI
        request.Xml = strTxtXML

        PagoEfectivo.PrivatePath = pathPrivateKey
        PagoEfectivo.PublicPathContraparte = pathPublicKeyContraparte



        ''Dim res As SPE.Api.Proxys.BEWSSolicitarResponse = PagoEfectivo.SolicitarPago(request)
        Dim res As SPE.Api.Proxys.BEWSGenCIPResponseMod1 = PagoEfectivo.GenerarCIPMod1(request)
        If (res IsNot Nothing) Then
            lblMensajeError.Visible = True
            lblMensajeError.Text = "Su Orden de Pago está siendo generada..."
            btnPagar.Enabled = False
            Dim xml As New XmlDocument
            xml.InnerXml = res.Xml
            'para redireccionamiento
            Dim element As XmlElement = xml.SelectSingleNode("ConfirSolPago")
            Dim elementchild As XmlElement = element.SelectSingleNode("Token")
            'para actualizar documento detalle con cip
            Try
                Dim elementchild2 As XmlElement = element.SelectSingleNode("CIP")
                Dim elementchildchild2 As XmlElement = elementchild2.SelectSingleNode("IdOrdenPago")
                Dim intCIP As Integer = Convert.ToInt32(elementchildchild2.InnerText)
                lblIdOrdenPago.Text = elementchildchild2.InnerText
                'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion                
            Catch ex As Exception
                Response.Redirect("Default.aspx")
            End Try

            Dim UrlRedirect As String = System.Configuration.ConfigurationManager.AppSettings("urlGenPagoIF") & "?Token=" & elementchild.InnerText

            PanelGeneral.Visible = False
            ltlGenPagoIF.Text = "<iframe class=""iframClass"" src=""" + UrlRedirect + """></iframe>"
            'Dim valors As String = "Default.aspx?Valor=" + elementchild.InnerText
            'Response.Redirect(valors, False)


        Else
            lblMensajeError.Visible = True
            lblMensajeError.Text = "No se obtuvo response (servicio del ws esta abajo)..."

        End If
    End Sub


    Protected Sub LimpiarControlesInternos()
        lblIdProducto.Text = ""
        lblIdServicio.Text = ""
        lblDescripcion.Text = ""
        txtMonto.Text = ""
        lblIdMoneda.Text = ""
        lblDescMoneda.Text = ""
        lblidEditable.Text = ""
        lblidEstado.Text = ""
        lblFechaCreacion.Text = ""
        lblUsuarioId.Text = ""
        txtMonto.Enabled = False
    End Sub

    Protected Sub ddlProducto_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlProducto.SelectedIndexChanged
        Dim oBEServicioProductoRequest As New BEServicioProductoRequest
        If ddlProducto.SelectedItem.Value <> "" Then
            lblErrMonto.Text = ""
            lblErrPrd.Text = ""
            oBEServicioProductoRequest.idProducto = ddlProducto.SelectedItem.Value
            Dim CtrlServicio As New CServicio()
            Dim oBESP As New BEServicioProducto
            oBESP = CtrlServicio.RecuperarDetalleProductoServicio(oBEServicioProductoRequest)
            If Not oBESP.DescMoneda Is Nothing Then

                lblIdProducto.Text = oBESP.idProducto
                lblIdServicio.Text = oBESP.idServicio
                lblDescripcion.Text = oBESP.Descripcion
                txtMonto.Text = oBESP.Monto
                lblIdMoneda.Text = oBESP.idMoneda
                lblDescMoneda.Text = oBESP.DescMoneda
                lblidEditable.Text = oBESP.idEditable
                lblidEstado.Text = oBESP.idEstado
                lblFechaCreacion.Text = oBESP.FechaCreacion
                lblUsuarioId.Text = oBESP.UsuarioId
                'txtNroDocumento.Text = IIf(txtNroDocumento.Text <> "", txtNroDocumento.Text, IIf(lblNroDoc.Text <> Nothing, lblNroDoc.Text, ""))
                lblErrPrd.Text = ""
                If oBESP.idEditable = "0" Then
                    If oBESP.idMoneda = "1" Then
                        lblMoneda.Text = "S/."
                        txtMonto.Text = oBESP.Monto.ToString

                    Else
                        lblMoneda.Text = "$ "
                        txtMonto.Text = oBESP.Monto.ToString
                    End If
                    txtMonto.Enabled = False
                Else
                    If oBESP.idMoneda = "1" Then
                        lblMoneda.Text = "S/."
                    Else
                        lblMoneda.Text = "$ "
                    End If
                    txtMonto.Text = ""
                    txtMonto.Enabled = True
                End If
            Else
                LimpiarControlesInternos()
            End If

        Else
            LimpiarControlesInternos()
        End If

    End Sub


    'Protected Sub ddlTipoDocumento_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTipoDocumento.SelectedIndexChanged
    '    If ddlTipoDocumento.SelectedValue = "1" Then
    '        txtNroDocumento.MaxLength = 8
    '        If txtNroDocumento.Text.Length > 8 Then
    '            txtNroDocumento.Text = txtNroDocumento.Text.Substring(0, 8)
    '        End If
    '    Else
    '        txtNroDocumento.MaxLength = 11
    '    End If
    'End Sub


End Class



