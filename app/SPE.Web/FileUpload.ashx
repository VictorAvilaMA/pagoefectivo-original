﻿<%@ WebHandler Language="VB" Class="FileUpload" %>

Imports System
Imports System.Web
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Linq
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging

'Namespace SPE.Web
Public Class FileUpload : Implements IHttpHandler, System.Web.SessionState.IRequiresSessionState

    Dim DestImage As Bitmap
    Dim SourceToImage As Bitmap
    Dim FileName1 As String
    Dim FileName2 As String
    Dim Name As String
    Dim Name2 As String

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        'context.Response.ContentType = "text/plain"
        'context.Response.Write("Hello World")
        If context.Request.Files.Count = 0 Then

            LogRequest("No se envió archivos!!!.")

            context.Response.ContentType = "text/plain"

            context.Response.Write("No se recibió archivos!!!!!!!!.")
        Else
            Try
                Dim uploadedfile As HttpPostedFile = context.Request.Files(0)
                CargarImagen(uploadedfile)
            Catch ex As Exception
                HttpContext.Current.Response.Write(ex.Message.ToString())
            End Try
            
        End If
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Sub LogRequest(ByVal Log As String)
        Dim sw As New StreamWriter(HttpContext.Current.Server.MapPath("/Log") & "\Log.txt", True)
        sw.WriteLine(DateTime.Now.ToString() & " - " & Log)
        sw.Flush()
        sw.Close()
    End Sub

#Region "métodos de compresión"

    'Private Function CreateBECliente(ByVal be As SPE.Entidades.BECliente) As SPE.Entidades.BECliente
    '    be = New SPE.Entidades.BECliente
    '    be.Email = UserInfo.Email
    '    Return be
    'End Function

    Public Shared Function GetEncoderInfo(ByVal mimeType As String) As ImageCodecInfo
        Dim j As Integer
        Dim encoders As ImageCodecInfo()
        encoders = ImageCodecInfo.GetImageEncoders()
        For j = 0 To encoders.Length
            If encoders(j).MimeType = mimeType Then
                Return encoders(j)
            End If
        Next j
        Return Nothing
    End Function

    Public Shared Sub SaveJPGWithCompressionSetting(ByVal image As Image, ByVal szFileName As String, ByVal lCompression As Long)
        Dim eps As EncoderParameters = New EncoderParameters(1)
        eps.Param(0) = New EncoderParameter(Encoder.Quality, lCompression)
        Dim ici As ImageCodecInfo = GetEncoderInfo("image/jpeg")
        Try
            image.Save(szFileName, ici, eps)
        Catch exc As Exception
            'MessageBox.Show(exc, " Atención !", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region


#Region "IMG2BYTE-BYTE2IMG"

    Public Shared Function Image2Bytes(ByVal img As Image) As Byte()
        Dim sTemp As String = Path.GetTempFileName()
        Dim fs As New FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite)
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        '' Cerrarlo y volverlo a abrir
        '' o posicionarlo en el primer byte
        ''fs.Close()
        ''fs = New FileStream(sTemp, FileMode.Open, FileAccess.Read)
        fs.Position = 0
        ''
        Dim imgLength As Integer = CInt(fs.Length)
        Dim bytes(0 To imgLength - 1) As Byte
        fs.Read(bytes, 0, imgLength)
        fs.Close()
        Return bytes
    End Function

    Public Function Bytes2Image(ByVal bytes() As Byte) As Image
        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm
    End Function

#End Region

#Region "Encriptar"

    Function Encripta(ByVal Texto As String) As String
        Dim Clave As String, i As Integer, Pass2 As String
        Dim CAR As String, Codigo As String
        Clave = "#SPE%$"
        Pass2 = ""
        For i = 1 To Len(Texto)
            CAR = Mid(Texto, i, 1)
            Codigo = Mid(Clave, ((i - 1) Mod Len(Clave)) + 1, 1)
            Pass2 = Pass2 & Microsoft.VisualBasic.Strings.Right("0" & Hex(Asc(Codigo) Xor Asc(CAR)), 2)
        Next i
        Encripta = Pass2
    End Function

#End Region

    Protected Sub CargarImagen(ByVal x As HttpPostedFile)
        Dim band As String
        band = String.Empty
        Try
            If x.ContentLength > 0 AndAlso x.FileName <> "" Then

                'Generar un bitmap para el origen
                Dim SourceImage As Bitmap
                SourceImage = New Bitmap(x.InputStream)

                'Generar un bitmap para el resultado
                SourceToImage = New Bitmap(SourceImage.Width, SourceImage.Height)

                'Generar un objeto Gráfico para el Bitmap resultante
                Dim gr_source As Graphics = Graphics.FromImage(SourceToImage)

                'Copiar la imagen origen al Bitmap destino
                gr_source.DrawImage(SourceImage, 0, 0, SourceToImage.Width, SourceToImage.Height)

                'Nombre de Archivos anterior
                Name = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email
                FileName1 = Encripta(Name)
                FileName2 = FileName1 & ".jpg"
                Dim DG As String = "~\img\temposave\" & FileName2
                Dim DGTemporal As String = "~\img\temposave\" & FileName1

                'SE COMPRIME ANTES DE GENERAR LA IMAGEN
                Dim NeedsHorizontalCrop As Boolean = True
                Dim NeedsVerticalCrop As Boolean = False

                'Determina si la imagen es Landscape o Portrait
                If SourceToImage.Height > SourceToImage.Width Then
                    NeedsHorizontalCrop = False
                    NeedsVerticalCrop = True
                End If

                'Determina si la imagen excede el ancho del PictureBox
                If SourceToImage.Width < 200 Then
                    NeedsHorizontalCrop = False
                    If SourceToImage.Height > 150 Then
                        NeedsVerticalCrop = True
                    End If
                End If

                'Calcula el Factor de Ajuste
                Dim scale_factor As Single = 1
                If SourceToImage.Width > 0 Then
                    If NeedsHorizontalCrop = True Then
                        ' Obtiene el Factor de Ajuste
                        scale_factor = 200 / SourceToImage.Width
                    End If
                End If

                If SourceToImage.Height > 0 Then
                    If NeedsVerticalCrop = True Then
                        ' Obtiene el Factor de Ajuste
                        scale_factor = 150 / SourceToImage.Height
                    End If
                End If

                'Generar un bitmap tmp para el resultado. Ajuste Proporcional
                Dim DestTmpImage As New Bitmap(CInt(SourceToImage.Width * scale_factor), CInt(SourceToImage.Height * scale_factor))

                'Generar un objeto Gráfico para el bitmap tmp resultante
                Dim gr_desttmp As Graphics = Graphics.FromImage(DestTmpImage)

                ' Copiar la imagen origen al bitmap tmp destino
                gr_desttmp.DrawImage(SourceToImage, 0, 0, DestTmpImage.Width, DestTmpImage.Height)


                'Comprime y Guarda un Archivo Temporal para calcular su peso en Kb
                Dim pathCurrent As String
                pathCurrent = HttpContext.Current.Server.MapPath("~/")
                Dim raiz As String
                raiz = pathCurrent & "img\temposave\" & FileName1 & "temp.jpg"

                Try
                    SaveJPGWithCompressionSetting(DestTmpImage, raiz, 80)
                    '' '' '' '' '' '' ''lblimg.Visible = False
                    HttpContext.Current.Session("lblimgVisible") = False

                Catch exc As Exception

                End Try

                'La lectura del nuevo archivo no se puede hacer en forma directa y repetitiva
                'ya que está bloqueado por GDI+ la 1era vez que se lo utiliza,
                'por lo tanto resulta necesario resolver en varios pasos. 
                'Al efectuar el Dispose() se libera el recurso
                DestImage = New Bitmap(pathCurrent & "img\temposave\" & FileName1 & "temp.jpg")

                ' Generar un bitmap para el resultado
                Dim DestToImage As New Bitmap(DestImage.Width, DestImage.Height)

                ' Generar un objeto Grafico para el bitmap resultante
                Dim gr_dest As Graphics = Graphics.FromImage(DestToImage)

                ' Copiar la imagen origen al bitmap destino
                gr_dest.DrawImage(DestImage, 0, 0, DestToImage.Width, DestToImage.Height)

                'La almaceno nuevamente en img
                DestToImage.Save(HttpContext.Current.Server.MapPath(DG), System.Drawing.Imaging.ImageFormat.Jpeg)

                'Muestra imagen comprimida
                ' '' '' '' '' '' '' '' ''ImgAvatar.ImageUrl = "~/img/temposave/" & FileName1 & "temp.jpg"

                'Pruebas de Reconversion
                HttpContext.Current.Session("ImageIni") = Image2Bytes(DestImage)
                HttpContext.Current.Session("SeCargo") = True

                'Liberar recursos
                SourceImage.Dispose()
                gr_source.Dispose()
                DestImage.Dispose()
                DestToImage.Dispose()
                gr_dest.Dispose()
                gr_desttmp.Dispose()
                '' '' '' '' '' '' '' '' '' ''lblimg.Visible = False
            End If
        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.Message.ToString() & "  " & band)
        End Try

    End Sub

End Class
'End Namespace