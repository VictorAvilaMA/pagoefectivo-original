﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    Async="true" CodeFile="GenPago.aspx.vb" Inherits="GenPago" %>

<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/base.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/screen.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" charset="utf-8" media="screen"
        type="text/css" />

    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/colorbox.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/gridandforms.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3

                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript" language="javascript">


        /*$('#<%= pagoconcuenta.ClientID %>').select();*/
        $('#<%= btnContinuar.ClientID %>').hide();


        function Tarjeta() {
            $('#<%= btnIniciarSesion.ClientID %>').hide();
            $('#<%= btnContinuar.ClientID %>').show();
            $('#<%= txtCorreo.ClientID %>').attr('disbled', 'disabled');
            $('#<%= txtContrasenia.ClientID %>').attr('disbled', 'disabled');
        }

        function Cuenta() {
            $('#<%= btnIniciarSesion.ClientID %>').show();
            $('#<%= btnContinuar.ClientID %>').hide();
            $('#<%= txtCorreo.ClientID %>').removeAttr('disabled');
            $('#<%= txtContrasenia.ClientID %>').removeAttr('disabled');
        }
        $(document).ready(function () {
            $('#<%= pagocontarjeta.ClientID %>').click();
            $('#MenuCabezera').remove();
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- 
    --%>
    <div class="pasarella">
        <div class="clear">
        </div>
        <div class="container-body clearfix">
            <div class="colLeft">
                <div id="cnt-resumen-ped">
                    <h1 class="subtitle-pasarella">
                        Resumen de su pedido</h1>
                    <div id="body-ped">
                        <div class="dlgrid">
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt35 dd50">
                                    <dt class="desc">
                                        <label>
                                            Importe de compra:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:Label ID="lblImporte" runat="server" Text=""></asp:Label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt35 dd50">
                                    <dt class="desc">
                                        <label>
                                            Concepto de pago:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:Label ID="lblConceptoPago" runat="server" Text=""></asp:Label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="w100 clearfix dlinline">
                                <dl class="even clearfix dt35 dd50">
                                    <dt class="desc">
                                        <label>
                                            Monto total:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:Label ID="lblMontoTotal" runat="server" ViewStateMode="Disabled"></asp:Label>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="colRight">
                <div class="dlgrid">
                    <h1 class="subtitle-pasarella">
                        Pagar sin tarjeta de crédito</h1>
                    <div class="clear">
                    </div>
                    <input type="radio" name="pago" id="pagocontarjeta" runat="server" class="radio"
                        onclick="Tarjeta();" />
                    <div class="cnt-opt-banks">
                        <p>
                            <span>Paga en los bancos afiliados</span> <a href="#" class="lnk-info"></a>
                        </p>
                        <img src="img/banks.jpg" alt="bancos afiliados" class="banks" />
                    </div>
                    <div class="clear">
                    </div>
                    <div id="divPagoDineroVirtual" runat="server" visible="false">
                    <h1 class="subtitle-pasarella">
                        Pagar con mi cuenta virtual de PagoEfectivo</h1>
                    <div class="clear">
                    </div>
                    <input type="radio" name="pago" id="pagoconcuenta" runat="server" class="radio" onclick="Cuenta();" />
                    <div id="cnt-login">
                        <div id="cnt-email">
                            <label for="email">
                                Correo electr&oacute;nico</label>
                            <div class="clear">
                            </div>
                            <asp:TextBox ID="txtCorreo" runat="server"></asp:TextBox>
                        </div>
                        <div id="cnt-pass">
                            <label for="textpass">
                                Contraseña de pagoefectivo.pe</label>
                            <div class="clear">
                            </div>
                            <asp:TextBox ID="txtContrasenia" TextMode="Password" runat="server"></asp:TextBox>
                        </div>
                        <asp:Button ID="btnIniciarSesion" runat="server" CssClass="btnIniSesion" 
                            Width="100px" />
                        <div class="clear">
                        </div>
                        <a href="RecuperarPassword.aspx" target="_blank" class="blueLink">¿Olvid&oacute; su
                            correo electr&oacute;nico o contraseña?</a>
                    </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="sep_a14">
                    </div>
                    <asp:Button ID="btnContinuar" Style="display: none;" runat="server" 
                        CssClass="btnContinuar" />
                    <asp:Button ID="btnCancelar" runat="server" CssClass="btnCancelar" />
                </div>
            </div>
        </div>
        <div class="conten_c" style="display: none">
            <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
            <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
            <uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />
        </div>
    </div>
    <div style="display: none;">
        <div id="help">
            <h2>
                Qué es generaci&oacute;n de c&oacute;digo?</h2>
            <div class="content">
                Es una nueva pasarela de pagos por internet del Grupo El Comercio, diseñada para
                facilitar sus transacciones por internet. Con PagoEfectivo no es necesaria una tarjeta
                de crédito para pagar una transacci&oacute;n por internet, de esta forma evita revelar
                datos confidenciales y tiene una transacci&oacute;n segura. Para pagar con PagoEfectivo,
                s&oacute;lo ser&aacute; necesario acercarse a cualquier agencia del BCP y cancelar
                el monto asociado al c&oacute;digo que PagoEfectivo le brindar&aacute;.
            </div>
        </div>
    </div>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery-1.4.2.min.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery.cookie.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery.treeview.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/colorbox.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery.general.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
</asp:Content>
