﻿Imports SPE.Web
Imports SPE.Entidades
Imports _3Dev.FW.Entidades.Seguridad
Imports System.Net
Imports System.IO
Imports System.Threading
Imports System.Collections.Generic
Imports SPE.Api.Proxys

Partial Class GenPago
    Inherits PaginaBase

    Protected Sub btnContinuar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinuar.Click

        Dim CtrlOrdenPago As New COrdenPago()


        Try
            If ViewState("oBESolicitudPago") IsNot Nothing Then
                Dim oBESolicitudPago As BESolicitudPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago)
                Dim oBEOrdenPago As BEOrdenPago = CtrlOrdenPago.GenerarOrdenPagoFromSolicitud(oBESolicitudPago)
                'Dim oBEOrdenPago As BEOrdenPago = proxy.GenerarOrdenPagoFromSolicitud(oBESolicitudPago)
                If oBEOrdenPago Is Nothing Then
                    JSMessageAlertWithRedirect("Error", "Hubo un excepcion no controlada a la hora de generar el pago", "key", "Default.aspx")
                Else
                    Session("oBEOrdenPago") = oBEOrdenPago
                    Session("oBESolicitudPago") = oBESolicitudPago
                    Response.Redirect("~/CIPGenerado.aspx")
                End If
            End If
        Catch ex As Exception
            JSMessageAlertWithRedirect("Error", "Hubo un excepcion no controlada a la hora de generar el pago", "key", "Default.aspx")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If HttpContext.Current.Request.QueryString("token") Is Nothing Then
                Response.Redirect("Default.aspx")
            Else
                'pagocontcredito.Checked = True
                'btnIniciarSesion.Enabled = False
                'btnIniciarSesion.Visible = False
                'RBSeleccion(True)
                pagoconcuenta.Checked = True
                'btnContinuar.Visible = False
                Dim Token As String = HttpContext.Current.Request.QueryString("token")
                Dim oBELogRedirect As New BELogRedirect

                oBELogRedirect.Descripcion = String.Format("Token:{0}", Token)
                oBELogRedirect.IdTipo = SPE.EmsambladoComun.ParametrosSistema.LogRedirect.Tipo.redirectEntrante
                Try
                    oBELogRedirect.UrlOrigen = IIf(Me.Request.UrlReferrer IsNot Nothing, Me.Request.UrlReferrer.ToString(), String.Empty)
                Catch ex As Exception
                    oBELogRedirect.UrlOrigen = String.Empty
                End Try

                oBELogRedirect.UrlDestino = Me.Request.Url.ToString()
                oBELogRedirect.Parametro1 = UtilAuditoria.GetHostName(Me)
                oBELogRedirect.Parametro2 = UtilAuditoria.GetIPAddress(Me)

                Dim oCComun As New CComun
                oCComun.RegistrarLogRedirectInst(oBELogRedirect)

                Dim oCOrdenPago As New COrdenPago()
                Dim oBESolicitudPago As New BESolicitudPago
                oBESolicitudPago.Token = Token
                oBESolicitudPago = oCOrdenPago.ConsultarSolicitudPagoPorTokenInst(oBESolicitudPago)
                If oBESolicitudPago Is Nothing Then
                    JSMessageAlertWithRedirect("Validacion", "No se encuentra una solicitud pendiente con el Token: " + Token, "key", "Default.aspx")
                Else
                    Select Case oBESolicitudPago.Idestado
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.GeneradaCIP
                            Dim oBEOrdenPago As New BEOrdenPago
                            oBEOrdenPago.IdOrdenPago = oBESolicitudPago.IdOrdenPago
                            oBEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorId(oBEOrdenPago)
                            Session("oBEOrdenPago") = oBEOrdenPago
                            Session("oBESolicitudPago") = oBESolicitudPago
                            Response.Redirect("~/CIPGenerado.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Expirada
                            JSMessageAlertWithRedirect("Validacion", "El token: " + Token + " se encuentra expirado", "key", "Default.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.ExpiradaCIP
                            JSMessageAlertWithRedirect("Validacion", "El CIP con token: " + Token + " se encuentra expirado", "key", "Default.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pagada
                            JSMessageAlertWithRedirect("Validacion", "El CIP con Token: " + Token + " se encuentra pagada", "key", "Default.aspx")
                        Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pendiente
                            Dim listaMetodosDePago As New List(Of String)
                            If oBESolicitudPago.MetodosPago IsNot Nothing Then
                                listaMetodosDePago.AddRange(oBESolicitudPago.MetodosPago.Split(","))
                            End If
                            If listaMetodosDePago.Contains(SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.MetodoPago.CuentaVirtual.ToString()) And CType(ConfigurationManager.AppSettings("ActivarMonedero").ToString(), Boolean) Then
                                divPagoDineroVirtual.Visible = True
                            End If
                            lblImporte.Text = oBESolicitudPago.Total.ToString("#,#0.00")
                            lblConceptoPago.Text = oBESolicitudPago.ConceptoPago
                            lblMontoTotal.Text = oBESolicitudPago.Total.ToString("#,#0.00")
                            ViewState("oBESolicitudPago") = oBESolicitudPago

                            If CType(ConfigurationManager.AppSettings("ActivarMonedero").ToString(), Boolean) = False Then
                                btnContinuar_Click(Nothing, Nothing)
                            End If
                        Case Else
                    End Select
                End If
            End If
        End If
    End Sub

    Protected Function CargarOrdenPagoDesdeSolicitud(ByVal oBESolicitudPago As BESolicitudPago) As BEOrdenPago
        Dim oBEordenPago As New BEOrdenPago()
        oBEordenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.Cliente
        'oBEordenPago.IdUsuarioCreacion
        oBEordenPago.IdServicio = oBESolicitudPago.IdServicio
        oBEordenPago.IdMoneda = oBESolicitudPago.IdMoneda
        'oBEordenPago.FechaEmision en BL se obtiene este dato
        oBEordenPago.Total = oBESolicitudPago.Total
        oBEordenPago.MerchantID = oBESolicitudPago.CodServicio
        oBEordenPago.OrderIdComercio = oBESolicitudPago.CodTransaccion
        'oBEordenPago.UrlOk = ya no se usa
        'oBEordenPago.UrlError = ya no se usa
        oBEordenPago.MailComercio = oBESolicitudPago.MailComercio
        oBEordenPago.IdUsuarioCreacion = oBESolicitudPago.IdUsuarioCreacion
        oBEordenPago.UsuarioID = oBESolicitudPago.UsuarioId
        oBEordenPago.DataAdicional = oBESolicitudPago.DataAdicional
        oBEordenPago.UsuarioNombre = oBESolicitudPago.UsuarioNombre
        oBEordenPago.UsuarioApellidos = oBESolicitudPago.UsuarioApellidos
        'oBEordenPago.UsuarioDomicilio ya no se usa
        oBEordenPago.UsuarioLocalidad = oBESolicitudPago.UsuarioLocalidad
        oBEordenPago.UsuarioProvincia = oBESolicitudPago.UsuarioProvincia
        oBEordenPago.UsuarioPais = oBESolicitudPago.UsuarioPais
        oBEordenPago.UsuarioAlias = oBESolicitudPago.UsuarioAlias
        oBEordenPago.UsuarioEmail = oBESolicitudPago.UsuarioEmail
        oBEordenPago.TramaSolicitud = oBESolicitudPago.TramaSolicitud
        oBEordenPago.IdTramaTipo = SPE.EmsambladoComun.ParametrosSistema.Servicio.TipoIntegracion.WebService
        oBEordenPago.TiempoExpiracion = oBESolicitudPago.TiempoExpiracion
        Return oBEordenPago
    End Function

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Try
            Dim oBESolicitudPago As BESolicitudPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago)
            Dim url As String
            Using ocntrlServicio As New CServicio()
                Dim objbeservicio As New BEServicio()
                'Dim obeServicio As BEServicio = ocntrlServicio.GetRecordByID(oBESolicitudPago.IdServicio)
                Dim obeServicio As BEServicio = ocntrlServicio.ObtenerServicioPorID(oBESolicitudPago.IdServicio)

                url = obeServicio.UrlRedireccionamiento
            End Using

            Dim oBELogRedirect As New BELogRedirect
            oBELogRedirect.Descripcion = String.Format("Token:{0},IdSolicitud:{1},Accion:canceloUsuario", oBESolicitudPago.Token, oBESolicitudPago.IdSolicitudPago)
            oBELogRedirect.IdTipo = SPE.EmsambladoComun.ParametrosSistema.LogRedirect.Tipo.redirectSaliente
            oBELogRedirect.UrlOrigen = Me.Request.Url.ToString()
            oBELogRedirect.UrlDestino = url
            oBELogRedirect.Parametro1 = UtilAuditoria.GetHostName(Me)
            oBELogRedirect.Parametro2 = UtilAuditoria.GetIPAddress(Me)

            Dim oCComun As New CComun
            oCComun.RegistrarLogRedirect(oBELogRedirect)
            Response.Redirect(url)
        Catch exThreadAbord As ThreadAbortException
            'Do nothing. ASP.NET is redirecting.
            'Always comment this so other developers know why the exception 
            'is being swallowed.
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnIniciarSesion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIniciarSesion.Click
        If (Not ExisteUsuario()) Then
            Return
        End If
        Dim CtrlOrdenPago As New COrdenPago()
        Dim userClient As New SPE.Web.Seguridad.SPEUsuarioClient
        If ViewState("oBESolicitudPago") IsNot Nothing Then
            Dim oBESolicitudPago As BESolicitudPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago)
            Session("oBESolicitudPago") = oBESolicitudPago
            Session("oBEUserInfo") = userClient.GetUserInfoByUserName(txtCorreo.Text)
            Response.Redirect("~/GenPagoVirtual.aspx")
        End If
    End Sub

    Private Function ExisteUsuario() As Boolean
        Dim memberShip As New SPE.Web.Seguridad.SPEMemberShipProviderClient
        If (Not memberShip.ValidateUser(txtCorreo.Text.Trim(), txtContrasenia.Text)) Then
            JSMessageAlert("Error", "Usuario no Existe.", "aa")
            Return False
        End If
        Return True
    End Function

    'Protected Sub pagocontcredito_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pagocontcredito.CheckedChanged
    '    RBSeleccion(True)
    'End Sub

    'Protected Sub pagosintcredito_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pagosintcredito.CheckedChanged
    '    RBSeleccion(False)
    'End Sub
#Region "Page Metod"
    <System.Web.Services.WebMethod()>
    Public Sub RBSeleccion(ByVal segun As Boolean)
        pagocontarjeta.Checked = segun
        btnIniciarSesion.Enabled = Not (segun)
        btnIniciarSesion.Visible = Not (segun)
        txtCorreo.Enabled = Not (segun)
        txtContrasenia.Enabled = Not (segun)
        pagoconcuenta.Checked = Not (segun)
    End Sub
#End Region


    Protected Sub btnContinuar_Load(sender As Object, e As System.EventArgs) Handles btnContinuar.Load

    End Sub
End Class
