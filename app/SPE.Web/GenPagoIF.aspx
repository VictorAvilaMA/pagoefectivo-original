﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GenPagoIF.aspx.vb" Inherits="GenPagoIF" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="head1" runat="server">
    <meta charset="utf-8" />
    <title>PagoEfectivo - Transacciones Seguras por Internet en el Perú</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/js/html5.js"></script>
    <![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="perucomsultores.pe" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta name="language" content="es" />
    <meta name="geolocation" content="Peru" />
    <meta name="robots" content="index,follow" />
    <meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta property="og:description" content="" />
    <meta property="og:image" content='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/CIPGenerado/logo_pagoefectivo_header.png" %>' />
    <link href="favicon.ico" rel="icon" type="image/vnd.microsoft.icon" />
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/GenPagoIF.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/fonts.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/all.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/layout.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/pagoEfectivo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' rel="stylesheet" type="text/css" />    
    <!-- Google Tag Manager -->
    <script>        (function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-TL2DDQ');</script>
    <!-- End Google Tag Manager -->
</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TL2DDQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <form id="Form1" runat="server">
    <div class="hack-shadow">
    </div>
    

     <!--aqui empieza-->
     
     <asp:Literal ID="ltlConfirmGen" runat="server"></asp:Literal>

            <!--aqui varia -->
              <div class="accordion_affiliated_office">
                <asp:Literal ID="ltSeccionBancos" runat="server"></asp:Literal>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      </section>
     <!--cierra aqui empieza-->
    <footer id="foot1" runat="server">
        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-1.6.1.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
            type="text/javascript"></script>
        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/slides.min.jquery.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
            type="text/javascript"></script>
        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/miniApp.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
            type="text/javascript"></script>
        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/modules.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
            type="text/javascript"></script>
         <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/accordion.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' 
            type="text/javascript"></script>
        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/JScripts/Messaging/bundle.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'  
            type="text/javascript"></script>
    </footer>
    </form>
</body>
</html>
