﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Globalization
Imports Amazon.S3

Partial Class GenPagoIF
    Inherits PaginaBase


    Private Shared _s3Client As AmazonS3Client = Nothing
    Private ReadOnly s3AccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_ACCESS_KEY")
    Private ReadOnly s3SecretAccessKey As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_SECRET_ACCESS_KEY")
    Private ReadOnly s3Region As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_REGION")
    Private ReadOnly s3BucketName As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_BUCKET_NAME")
    Private ReadOnly s3DirectoryPath As String = ConfigurationManager.AppSettings.[Get]("COBRANZA_S3_DIRECTORY_PATH")

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.head1.DataBind()
            foot1.DataBind()
            findControlToDataBind(Me.Controls)
            If HttpContext.Current.Request.QueryString("token") Is Nothing Then
                Response.Redirect("Default.aspx")
            Else
                Dim Token As String = HttpContext.Current.Request.QueryString("token")
                Dim oBELogRedirect As New BELogRedirect
                Dim RutaRaiz As String = ""
                oBELogRedirect.Descripcion = String.Format("Token:{0}", Token)
                oBELogRedirect.IdTipo = SPE.EmsambladoComun.ParametrosSistema.LogRedirect.Tipo.redirectEntrante
                Try
                    oBELogRedirect.UrlOrigen = IIf(Me.Request.UrlReferrer IsNot Nothing, Me.Request.UrlReferrer.ToString(), String.Empty)
                Catch ex As Exception
                    oBELogRedirect.UrlOrigen = String.Empty
                End Try

                oBELogRedirect.UrlDestino = Me.Request.Url.ToString()
                oBELogRedirect.Parametro1 = UtilAuditoria.GetHostName(Me)
                oBELogRedirect.Parametro2 = UtilAuditoria.GetIPAddress(Me)

                Dim oCComun As New CComun
                oCComun.RegistrarLogRedirect(oBELogRedirect)

                Try
                    Dim oCOrdenPago As New COrdenPago()
                    Dim oBESolicitudPago As New BESolicitudPago
                    oBESolicitudPago.Token = Token
                    oBESolicitudPago = oCOrdenPago.ConsultarSolicitudPagoPorToken(oBESolicitudPago)
                    If oBESolicitudPago Is Nothing Then
                        JSMessageAlertWithRedirect("Validacion", "No se encuentra una solicitud pendiente con el Token: " + Token, "key", "Default.aspx")
                    Else
                        Dim oBEOrdenPago As New BEOrdenPago
                        Dim CtrlServicio As New CServicio()
                        Dim oBEServicio As New BEServicio()
                        Select Case oBESolicitudPago.Idestado
                            Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.GeneradaCIP
                                oBEOrdenPago.IdOrdenPago = oBESolicitudPago.IdOrdenPago
                                oBEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorId(oBEOrdenPago)
                            Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Expirada
                                JSMessageAlertWithRedirect("Validacion", "El token: " + Token + " se encuentra expirado", "key", "Default.aspx")
                            Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pagada
                                JSMessageAlertWithRedirect("Validacion", "El CIP con Token: " + Token + " se encuentra pagada", "key", "Default.aspx")
                            Case SPE.EmsambladoComun.ParametrosSistema.SolicitudPago.Tipo.Pendiente
                                Dim listaMetodosDePago As New List(Of String)
                                If oBESolicitudPago.MetodosPago IsNot Nothing Then
                                    listaMetodosDePago.AddRange(oBESolicitudPago.MetodosPago.Split(","))
                                End If
                                Dim CtrlOrdenPago As New COrdenPago()
                                oBEOrdenPago = CtrlOrdenPago.GenerarOrdenPagoFromSolicitud(oBESolicitudPago)
                            Case Else
                        End Select
                        If oBEOrdenPago Is Nothing Then
                            JSMessageAlertWithRedirect("Error", "Hubo un excepcion no controlada a la hora de generar el pago", "key", "Default.aspx")
                        Else
                            oBEServicio = CType(CtrlServicio.GetRecordByID(oBEOrdenPago.IdServicio), BEServicio)
                            'imgServicio.ImageUrl = System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/logopagoefectivo.png"
                            'imgServicio.Width = "225"
                            'imgServicio.BorderWidth = "0"
                            'imgCIP.ImageUrl = "CLI/ADOPImg.aspx?numero=" & oBEOrdenPago.NumeroOrdenPago



                            'ltlCIP.Text = oBEOrdenPago.NumeroOrdenPago
                            'ltlTotal.Text = IIf(oBEOrdenPago.IdMoneda = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles, "S/. ", "$ ") & oBEOrdenPago.Total
                            'ltlTotalmovil.Text = ltlTotal.Text
                            'ltlNombreServicio.Text = oBEServicio.Nombre
                            'NombreServicio2.Text = oBEServicio.Nombre




                            'ltlCodTransaccion.Text = oBESolicitudPago.CodTransaccion
                            'ltlConceptoPago.Text = oBESolicitudPago.ConceptoPago
                            'ltlConceptoPagoAdicional.Text = String.Empty
                            'For Each item As BEParametroEmail In oBESolicitudPago.ParametrosEmail
                            '    If (item.Nombre = "ConceptoPago") Then
                            '        ltlConceptoPagoAdicional.Text = item.Valor
                            '    End If
                            'Next
                            Dim ci As CultureInfo
                            Try
                                ci = New CultureInfo(oBEOrdenPago.CodPlantillaTipoVariacion)
                            Catch ex As CultureNotFoundException
                                ci = New CultureInfo("es")
                            End Try

                            Dim diaDeSemana As String = ci.DateTimeFormat.GetDayName(oBEOrdenPago.FechaVencimiento.DayOfWeek)
                            Dim diaNumero As String = oBEOrdenPago.FechaVencimiento.Day.ToString().PadLeft(2, "0")
                            Dim mesNombre As String = ci.DateTimeFormat.GetMonthName(oBEOrdenPago.FechaVencimiento.Month)


                            Dim strFechaVencimiento As String = String.Format("{0} {1}/{2}/{3}", DiasSemana(oBEOrdenPago.FechaVencimiento.DayOfWeek), _
                                                                      oBEOrdenPago.FechaVencimiento.Day.ToString("00"), _
                                                                      oBEOrdenPago.FechaVencimiento.Month.ToString("00"), _
                                                                      oBEOrdenPago.FechaVencimiento.Year.ToString("0000"))

                            Dim CtrPlantilla As New CPlantilla()
                            Dim valoresDinamicos As New Dictionary(Of String, String)
                            valoresDinamicos.Add("[CIP]", oBEOrdenPago.IdOrdenPago)
                            valoresDinamicos.Add("[FechaVencimiento]", strFechaVencimiento)
                            valoresDinamicos.Add("[HoraVencimiento]", oBEOrdenPago.FechaVencimiento.ToString(" hh:mm tt"))
                            valoresDinamicos.Add("[EmailCLiente]", oBEOrdenPago.UsuarioEmail)
                            valoresDinamicos.Add("[NroPedido]", oBESolicitudPago.CodTransaccion)
                            valoresDinamicos.Add("[ServicioNombre]", oBEServicio.Nombre)
                            valoresDinamicos.Add("[Moneda]", IIf(oBEOrdenPago.IdMoneda = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles, "S/. ", "$ "))
                            valoresDinamicos.Add("[Monto]", oBEOrdenPago.Total)
                            valoresDinamicos.Add("[ConceptoPago]", oBESolicitudPago.ConceptoPago)
                            valoresDinamicos.Add("[idServicio]", oBEServicio.IdServicio)
                            If (oBEOrdenPago.IdMoneda = 1) Then
                                valoresDinamicos.Add("[MostrarBancos]", "display:none;")
                                valoresDinamicos.Add("[MonedaNombre]", "Soles")
                            Else
                                valoresDinamicos.Add("[MonedaNombre]", "Dólares")
                                valoresDinamicos.Add("[MostrarBancos]", "")
                            End If

                            If oBEServicio.PlantillaFormato = 53 Then
                                Dim Seccionheader As String
                                Dim TextoXReemplazar As String = "src=""App_Themes"
                                Dim TextoReemplazo As String
                                Dim Seccionpagos As String
                                Seccionpagos = CtrPlantilla.ConsultarPLantillasXServicioGenPago(oBEOrdenPago.IdServicio, oBEOrdenPago.IdMoneda, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoPagoGeneralIF, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)

                                TextoReemplazo = "src=""" & System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") & "/App_Themes"
                                'Seccionheader = CtrPlantilla.GenerarPlantillaToHtmlByTipo(oBEOrdenPago.IdServicio, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoConfirmIF, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
                                Seccionheader = CtrPlantilla.ConsultarPLantillasXServicioGenPago(oBEOrdenPago.IdServicio, oBEOrdenPago.IdMoneda, SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.InfoConfirmIF, valoresDinamicos, oBEOrdenPago.CodPlantillaTipoVariacion)
                                Seccionheader = Seccionheader.Replace(TextoXReemplazar, TextoReemplazo)

                                RutaRaiz = ObtenerRaiz()
                                TextoXReemplazar = "[token]"
                                TextoReemplazo = "javascript:window.open('" + RutaRaiz + "CIPImpresion.aspx?token=" + oBESolicitudPago.Token + "');"
                                Seccionheader = Seccionheader.Replace(TextoXReemplazar, TextoReemplazo)
                                ltlConfirmGen.Text = Seccionheader

                                Seccionpagos = Seccionpagos.Replace(TextoXReemplazar, TextoReemplazo)
                                ltSeccionBancos.Text = Seccionpagos
                            Else
                                Dim strPlantillaTipo As String
                                If (oBEServicio.PlantillaFormato = SPE.EmsambladoComun.ParametrosSistema.PlantillaFormato.Tipo.Estandar) Then
                                    strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.ConfGen & "Est"
                                Else
                                    strPlantillaTipo = SPE.EmsambladoComun.ParametrosSistema.Plantilla.Tipo.ConfGen & "Exp"
                                End If

                                Dim obePlantilla = CtrPlantilla.ConsultarPlantillaYParametrosPorTipoYVariacion(oBEOrdenPago.IdServicio, strPlantillaTipo, "st")
                                'Cargo plantilla desde S3
                                Dim fileHtml = ObtenerPlantillaS3(obePlantilla.FileS3)

                                If (Not fileHtml = "") Then
                                    Dim lstSecciones As List(Of BEPlantillaSeccion) = CtrPlantilla.ObtenerSeccionesPlantillaPorServicio(oBEServicio.IdServicio, obePlantilla.IdPlantillaTipo)
                                    For i As Integer = 0 To lstSecciones.Count - 1
                                        fileHtml = fileHtml.Replace("[Seccion" + lstSecciones(i).IdSeccion.ToString + "]", lstSecciones(i).Contenido)
                                    Next
                                    For Each param As KeyValuePair(Of String, String) In valoresDinamicos
                                        fileHtml = fileHtml.Replace(param.Key, param.Value)
                                    Next
                                    For i As Integer = 0 To obePlantilla.ListaParametros.Count - 1
                                        If (obePlantilla.ListaParametros(i).IdTipoParametro = 411) Then
                                            fileHtml = fileHtml.Replace(obePlantilla.ListaParametros(i).Etiqueta.ToString, obePlantilla.ListaParametros(i).Valor)
                                        End If
                                    Next

                                    Dim urlToken As String
                                    Dim TextoXReemplazar As String
                                    RutaRaiz = ObtenerRaiz()
                                    TextoXReemplazar = "[token]"
                                    urlToken = "javascript:window.open('" + RutaRaiz + "CIPImpresion.aspx?token=" + oBESolicitudPago.Token + "');"
                                    fileHtml = fileHtml.Replace(TextoXReemplazar, urlToken)
                                    ltSeccionBancos.Text = fileHtml
                                Else
                                    Exit Sub
                                End If

                            End If


                            'btnImprimir_footer.OnClientClick = "javascript:window.open('" + RutaRaiz + "CIPImpresion.aspx?token=" + oBESolicitudPago.Token + "');"
                            Session.Remove("oBEOrdenPago")
                            Session.Remove("oBESolicitudPago")
                        End If
                    End If
                Catch ex As Exception
                    oBELogRedirect.Descripcion = String.Format("Exception:{0}", ex.ToString())
                    oCComun.RegistrarLogRedirect(oBELogRedirect)
                    Throw ex
                End Try
            End If
        End If
    End Sub
    Public Function ObtenerPlantillaS3(ByVal fileName As String) As String

        Dim amazonS3 = New AWS.AmazonS3(s3AccessKey, s3SecretAccessKey, s3Region, s3BucketName, s3DirectoryPath)

        If (amazonS3.FileExists(fileName)) Then
            Dim fileStream = amazonS3.ReadStreamFromFile(fileName)
            Dim bytes = fileStream.ToArray()

            Return System.Text.Encoding.UTF8.GetString(bytes)
        Else
            Return "-1"
        End If

    End Function

    Private _DiasSemana As Dictionary(Of DayOfWeek, String)
    Public ReadOnly Property DiasSemana() As Dictionary(Of DayOfWeek, String)
        Get
            If (_DiasSemana Is Nothing) Then
                _DiasSemana = New Dictionary(Of DayOfWeek, String)
                _DiasSemana.Add(DayOfWeek.Monday, "Lunes")
                _DiasSemana.Add(DayOfWeek.Tuesday, "Martes")
                _DiasSemana.Add(DayOfWeek.Wednesday, "Miércoles")
                _DiasSemana.Add(DayOfWeek.Thursday, "Jueves")
                _DiasSemana.Add(DayOfWeek.Friday, "Viernes")
                _DiasSemana.Add(DayOfWeek.Saturday, "Sábado")
                _DiasSemana.Add(DayOfWeek.Sunday, "Domingo")
            End If
            Return _DiasSemana
        End Get
    End Property
    Private Function ObtenerRaiz() As String
        Dim Ruta As String
        Dim RutaFormateada As String
        Dim NomPag As String
        Dim TamanioNomPag As Integer
        Dim RutaInvertida As String = ""
        Ruta = Request.Url.ToString
        For i As Integer = Ruta.Length - 1 To 0 Step -1
            RutaInvertida = RutaInvertida & Ruta.Substring(i, 1)
        Next
        NomPag = RutaInvertida.Split("/")(0)
        TamanioNomPag = NomPag.Length
        RutaFormateada = Ruta.Substring(0, Ruta.Length - TamanioNomPag)
        Return RutaFormateada
    End Function
    Public Sub findControlToDataBind(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls And c.GetType IsNot GetType(GridView) Then
                findControlToDataBind(c.Controls)
            End If
            If c.GetType() Is GetType(ImageButton) Or c.GetType() Is GetType(Button) Or c.GetType() Is GetType(Image) Or c.GetType() Is GetType(LinkButton) Then
                c.DataBind()
            End If
        Next
    End Sub
End Class
