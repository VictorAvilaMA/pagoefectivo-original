﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    Async="true" CodeFile="GenPagoVirtual.aspx.vb" Inherits="GenPagoVirtual" %>

<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/base.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/screen.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/colorbox.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <%--<script src="App_Themes/SPE/js/jquery.treeview.js" type="text/javascript"></script>
    <script src="App_Themes/SPE/js/colorbox.js" type="text/javascript"></script>
    <script src="App_Themes/SPE/js/jquery.general.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $('#<%=ddlCuenta.ClientID%>').change(function () {
                $('#<%=hdfValues.ClientID%>').val('');
            });
            $('#MenuCabezera').remove();
        });

        function validarConfirmacion() {
            if ($('#<%= ddlCuenta.ClientID%>').val() == '') {
                alertDiv('Validación: Ud. no tiene Cuenta de Dinero Virtual.');
                return false;
            }
            var montoPagar = parseFloat($('#<%= hdfMontoPagar.ClientID%>').val());
            var saldoCuenta = parseFloat($('#<%= ddlCuenta.ClientID%>').val().split('|')(2));
            var saldoRetenido = parseFloat($('#<%= ddlCuenta.ClientID%>').val().split('|')(3));

            var idMoneda = parseInt('<%= CType(ViewState("oBESolicitudPago"), SPE.Entidades.BESolicitudPago).IdMoneda %>', 10)

            if (idMoneda != parseInt(parseFloat($('#<%= ddlCuenta.ClientID%>').val().split('|')(1)), 10)) {
                alertDiv('Validación: El tipo de Moneda del CIP es diferente al tipo de Moneda de la Cuenta.');
                return false;
            }
            if (montoPagar > (saldoCuenta - saldoRetenido)) {
                alertDiv('Validación: Ud. no tiene Saldo Suficiente para realizar el Pago.');
                return false;
            }

            if ($('#<%= hdfValues.ClientID%>').val() == '') {
                alertDiv('Validación: Debe Generar el Nº de Token.');
                return false;
            }
            return true;
        }

    </script>
    <div id="homeAdmin" class="pasarella">
        <div class="wrapper">
            <div class="clear">
            </div>
            <div class="container-body clearfix">
                <div class="inner-body">
                    <div class="colLeft">
                        <div id="cnt-resumen-ped">
                            <h1 class="subtitle-pasarella">
                                Resumen de su pedido</h1>
                            <div id="body-ped">
                                <div class="dlgrid">
                                    <div class="w100 clearfix dlinline">
                                        <dl class="even clearfix dt35 dd50">
                                            <dt class="desc">
                                                <label>
                                                    Importe de compra:</label>
                                            </dt>
                                            <dd class="camp">
                                                <p>
                                                    <asp:Literal ID="lblAbrevMoneda" runat="server" Text=""></asp:Literal>
                                                    <span class="bold">
                                                        <asp:Literal ID="lblImporte" runat="server" Text=""></asp:Literal></span>
                                                    <asp:Literal ID="lblNombreMoneda" runat="server" Text=""></asp:Literal>
                                                </p>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="w100 clearfix dlinline">
                                        <dl class="even clearfix dt35 dd50">
                                            <dt class="desc">
                                                <label>
                                                    Concepto de pago:</label>
                                            </dt>
                                            <dd class="camp">
                                                <asp:Label ID="lblConceptoPago" runat="server" Text=""></asp:Label>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="w100 clearfix dlinline">
                                        <dl class="even clearfix dt35 dd50">
                                            <dt class="desc">
                                                <label>
                                                    Monto total:</label>
                                            </dt>
                                            <dd class="camp">
                                                <div id="bg-mnt-total">
                                                    <span class="lft-price"><span class="bold rgt-price">
                                                        <asp:Literal ID="lblMontoTotal" runat="server" Text=""></asp:Literal></span></span></div>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="colRight">
                        <form class="dlgrid" method="post" action="">
                        <h1 class="subtitle-pasarella">
                            Pagar con mi cuenta virtual de PagoEfectivo</h1>
                        <div class="clear">
                        </div>
                        <span class="italic tip-info">Revise su informaci&oacute;n</span>
                        <div class="clear">
                        </div>
                        <div id="cnt-detail-virtual">
                            <h3>
                                Datos de cuenta</h3>
                            <div class="clear">
                            </div>
                            <ul class="data-cta">
                                <li><span class="title">email:</span> <span class="desc">
                                    <asp:Label ID="lblEmail" runat="server"></asp:Label></span></li>
                                <li><span class="title">Nombre:</span> <span class="desc">
                                    <asp:Label ID="lblNombres" runat="server"></asp:Label></span></li>
                                <li><span class="title">Apellidos:</span> <span class="desc">
                                    <asp:Label ID="lblApellidos" runat="server"></asp:Label></span> </li>
                            </ul>
                            <div class="clear">
                            </div>
                            <h3>
                                Cuenta a utilizar</h3>
                            <div class="clear">
                            </div>
                            <div id="cnt-cta-usar">
                                <p style="padding-top: 10px; margin-bottom: 0px; padding-bottom: 10px; margin-top: 0px;">
                                    <asp:DropDownList ID="ddlCuenta" runat="server" Style="width: 95%;">
                                    </asp:DropDownList>
                                </p>
                                <p class="mnt">
                                    Monto:
                                    <asp:Literal ID="ltrMonto" runat="server"></asp:Literal></p>
                            </div>
                            <div class="clear">
                            </div>
                            <h3>
                                C&oacute;digo de confirmaci&oacute;n</h3>
                            <div class="clear">
                            </div>
                            <div id="cnt-cod-conf">
                                <asp:TextBox ID="txtToken" CssClass="izq" runat="server"></asp:TextBox>
                                <asp:Button CssClass="btnGenCod codconf" ID="btnGenerarToken" Style="width: 100px"
                                    runat="server" Text="Generar Token" />
                                <a href="#" class="lnk-info"></a>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="cnt-term">
                                <input type="checkbox" name="chkterm" id="chkAcepto" runat="server" />
                                <p>
                                    Estoy de acuerdo y he leido los <a href="#" class="blueLink">terminos y condiciones</a>
                                    de uso de PagoEfectivo
                                    <br />
                                    Para confirmar el pago debe hacer click en Confirmar</p>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <asp:Button ID="btnConfirmar" CssClass="btnConfirmar pas-init" OnClientClick="return validarConfirmacion();"
                            runat="server" Text="Confirmar" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="conten_c" style="display: none">
        <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
        <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
        <uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />
    </div>
    <div style="display: none;">
        <div id="msje">
            <div class="clearfix" id="overlay">
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/progress_popup.gif" alt="" style="float: left; margin: 65px 0px 0px 24px;" />
                <p style="float: left; margin-top: 36px;">
                    PagoEfectivo est&aacute; procesando su pago
                    <br />
                    Espere un momento por favor</p>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <div id="help">
            <h2>
                Qué es generaci&oacute;n de c&oacute;digo?</h2>
            <div class="content">
                La funcionalidad de “Generar C&oacute;digo” sirve para completar 
                la operaci&oacute;n y confirmar su autenticidad, el c&oacute;digo le 
                llegar&aacute; al correo con el cual registr&oacute; su cuenta.
            </div>
        </div>
    </div>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery-1.4.2.min.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery.cookie.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery.treeview.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/colorbox.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/js/jquery.general.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/javascript"></script>
    <asp:HiddenField ID="hdfValues" runat="server" />
    <asp:HiddenField ID="hdfMontoPagar" runat="server" />
</asp:Content>
