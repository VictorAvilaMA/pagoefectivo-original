﻿Imports SPE.Web
Imports SPE.Entidades

Partial Class GenPagoVirtual
    Inherits PaginaBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            CargarDataInicial()
        End If
    End Sub

    Private Sub CargarDataInicial()
        If (Session("oBESolicitudPago") Is Nothing Or Session("oBEUserInfo") Is Nothing) Then
            Response.Redirect("~/Login.aspx")
        End If
        ViewState("oBESolicitudPago") = Session("oBESolicitudPago")
        ViewState("oBEUserInfo") = Session("oBEUserInfo")
        Session.Remove("oBEOrdenPago")
        Session.Remove("oBESolicitudPago")
        'Session.Remove("oBEUserInfo")

        Dim oUserInfo As _3Dev.FW.Entidades.Seguridad.BEUserInfo = CType(ViewState("oBEUserInfo"), _3Dev.FW.Entidades.Seguridad.BEUserInfo)
        Dim oBESolicitudPago As New BESolicitudPago
        oBESolicitudPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago)
        lblEmail.Text = oUserInfo.Email
        lblNombres.Text = oUserInfo.Nombres
        lblApellidos.Text = oUserInfo.Apellidos
        lblAbrevMoneda.Text = oBESolicitudPago.AbreviaturaMoneda
        lblNombreMoneda.Text = String.Concat("(", oBESolicitudPago.NombreMoneda, ")")
        lblImporte.Text = oBESolicitudPago.Total.ToString("#0.00")
        lblConceptoPago.Text = oBESolicitudPago.ConceptoPago
        lblMontoTotal.Text = oBESolicitudPago.AbreviaturaMoneda + " " + oBESolicitudPago.Total.ToString("#0.00")
        ltrMonto.Text = lblMontoTotal.Text
        hdfMontoPagar.Value = oBESolicitudPago.Total
        CargarCombos(oUserInfo.IdUsuario)

    End Sub

    Private Sub CargarCombos(idUsuario As Integer)
        Dim cntrDV As New SPE.Web.CDineroVirtual
        Dim beParam As New SPE.Entidades.BECuentaDineroVirtual
        Dim listaCuenta As New System.Collections.Generic.List(Of SPE.Entidades.BECuentaDineroVirtual)
        beParam.IdEstado = CType(SPE.EmsambladoComun.ParametrosSistema.CuentaVirtualEstado.Habilitado, Integer)
        beParam.IdUsuario = idUsuario
        listaCuenta = cntrDV.ConsultarCuentaDineroVirtualUsuario(beParam)
        For Each item As SPE.Entidades.BECuentaDineroVirtual In listaCuenta
            ddlCuenta.Items.Insert(0, New ListItem(String.Concat("Nº ", item.Numero, " (", item.MonedaSimbolo, " ", item.Saldo.ToString("#0.00"), ") [", item.AliasCuenta, "]"), String.Concat(item.IdCuentaDineroVirtual, "|", item.IdMoneda, "|", item.Saldo, "|", item.SaldoRetenido)))
        Next
    End Sub


    Private Sub GenerarToken(idCuenta As Long)
        Dim oCDineroVirtual As New SPE.Web.CDineroVirtual
        Dim oBEToken As New BETokenDineroVirtual
        oBEToken.IdCuenta = idCuenta
        oBEToken.IdTipoOperacion = CType(SPE.EmsambladoComun.ParametrosSistema.TipoMovimientoMonedero.PagoCIPDesdePortal, Integer)
        oBEToken = oCDineroVirtual.RegistrarTokenMonedero(oBEToken)
        hdfValues.Value = oBEToken.IdTokenDineroVirtual
        'ltNroToken.Text = oBEToken.Token
    End Sub


    Protected Sub btnGenerarToken_Click(sender As Object, e As System.EventArgs) Handles btnGenerarToken.Click
        GenerarToken(Convert.ToInt32(ddlCuenta.SelectedValue.Split("|"c)(0)))
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As System.EventArgs) Handles btnConfirmar.Click
        If (hdfValues.Value.Equals("")) Then
            JSMessageAlert("Validación", "Debe Generar el Nº de token.", "aa")
            Return
        End If

        If chkAcepto.Checked Then
            Dim cDineroVirt As New CDineroVirtual
            Dim oMovCuent As New BEMovimientoCuenta
            oMovCuent.IdCuentaVirtual = Convert.ToInt64(ddlCuenta.SelectedValue.Split("|"c)(0))
            oMovCuent.IdMovimientoAsociado = CType(ViewState("oBESolicitudPago"), BESolicitudPago).IdSolicitudPago
            oMovCuent.IdUsuario = CType(ViewState("oBEUserInfo"), _3Dev.FW.Entidades.Seguridad.BEUserInfo).IdUsuario
            oMovCuent.IdTokenDineroVirtual = Convert.ToInt64(hdfValues.Value)
            oMovCuent.ConceptoPago = CType(ViewState("oBESolicitudPago"), BESolicitudPago).ConceptoPago
            oMovCuent.NroToken = txtToken.Text
            oMovCuent.CodigoReferencia = CType(ViewState("oBESolicitudPago"), BESolicitudPago).IdSolicitudPago
            Dim rsult As String = ""
            rsult = cDineroVirt.RegistrarPagoCIPPortal(oMovCuent, CType(ViewState("oBESolicitudPago"), BESolicitudPago))
            If (rsult Is Nothing) Then
                JSMessageAlert("Error: ", "Error al tratar de Pagar el CIP.", "aa")
            Else
                If (rsult.Trim().Equals("")) Then
                    'txtNrotoken.Enabled = btnReenviarToken.Enabled = btnPagarCIP.Enabled = btnCancelar3.Enabled = False
                    JSMessageAlertWithRedirect("Info: ", "Se ha realizado el Pago del CIP Satisfactoriamente.", "bb", "Default.aspx")
                Else
                    JSMessageAlert("Validación:", rsult, "cc")
                End If
            End If
        Else : JSMessageAlert("Alerta", "Primero debe Aceptar el Acuerdo de Términos y Condiciones", "ab")
        End If

        
    End Sub
End Class
