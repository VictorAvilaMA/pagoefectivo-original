﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="_3Dev.FW.Web.Log" %>
<%@ Import Namespace="SPE.Entidades" %>

<script RunAt="server">

    Function EstablishConnection() As Boolean
        Try
            Return SPE.Web.RemoteServices.Instance.SetupNetworkEnvironment()
        Catch ex As Exception
            Throw New SPE.Exceptions.GeneralConexionException(ex.Message)
        End Try
        
    End Function


    
    'Sub RegisterRoutes(routes As RouteCollection)
    '    Dim _urlPattern As String = Nothing
    '    Dim _idk_app_Route As Route = Nothing

    '    routes.Add(New Route("{resource}.axd/{*pathInfo}", New StopRoutingHandler()))
    '    routes.Add(New Route("{resource}.ico/{*pathInfo}", New StopRoutingHandler()))
    '    routes.Add(New Route("uImage/{resource}.aspx/{*pathInfo}", New StopRoutingHandler()))

    '    _urlPattern = "{param1}/{param2}"
    '    _idk_app_Route = New Route(_urlPattern, New CustomRouteHandler.navigateRouteHandler())
    '    _idk_app_Route.Defaults = New RouteValueDictionary(New With { _
    '     Key .param1 = "", _
    '     Key .param2 = "" _
    '    })

    '    routes.Add("navigateRoute", _idk_app_Route)

    'End Sub
    
    

    
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
            
        
        
        Try
            SPE.Web.RemoteServices.Instance = New SPE.Web.RemoteServices()
            If Not EstablishConnection() Then
                Return
            End If
        Catch ex1 As SPE.Exceptions.GeneralConexionException
            _3Dev.FW.Web.Log.Logger.LogException(ex1)
            Throw ex1
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try
        
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
        'SPE.Web.RemoteServices.Instance = Nothing
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        
        Logger.LogException(Me.Server.GetLastError.GetBaseException)
        Logger.LogTransaction("Error de Transacción")
        
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub
    
    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        Response.CacheControl = ""
        Response.Expires = 0
        Response.AddHeader("pragma", "no-cache")
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
       
    End Sub
</script>
