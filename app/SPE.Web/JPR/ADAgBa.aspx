<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADAgBa.aspx.vb" Inherits="ADM_ADAgBa" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <label>
            <asp:Literal ID="lblProceso" runat="server" Text="Registrar Agencia Bancaria"></asp:Literal>
        </label>
    </h2>
    <div id="Page" class="conten_pasos3">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:Panel ID="PnlEmpresa" runat="server"  Style="left: 0px;
            top: 0px">
            <h4>
                1. Informaci�n general</h4>
            <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Nombre :</li>
                        <li class="t2">
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="neu" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Ingrese el Nombre." Display="Dynamic" ControlToValidate="txtNombre">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Direcci�n :</li>
                        <li class="t2">
                            <asp:TextBox ID="txtDireccion" runat="server" CssClass="neu" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Ingrese la Direcci�n." Display="Dynamic" ControlToValidate="txtDireccion">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> C�digo :</li>
                        <li class="t2">
                            <asp:TextBox ID="txtCodigo" runat="server" CssClass="neu" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Ingrese el C�digo." Display="Dynamic" ControlToValidate="txtCodigo">*</asp:RequiredFieldValidator>
                            <asp:Label ID="lblMensajeCodigo" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Banco :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu" AutoPostBack="False">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Debe seleccionar el banco." ControlToValidate="ddlBanco">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Pa�s :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlPais" runat="server" CssClass="neu" AutoPostBack="True">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Departamento :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="neu" AutoPostBack="True">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Ciudad :</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="ValidaCampos"
                                ErrorMessage="Debe seleccionar la ciudad." ControlToValidate="ddlCiudad">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1">
                            <asp:Literal ID="lblEstado" runat="server" Text="<span class='color'>&gt;&gt;</span> Estado:" Visible="False"></asp:Literal>
                        </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu" Visible="False">
                            </asp:DropDownList>
                        </li>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TxtNombre" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&_- "
                            TargetControlID="txtNombre">
                        </cc1:FilteredTextBoxExtender>
                        <asp:Label ID="lblIdAgenciaBancaria" runat="server"  style="display:none;" Text=""></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
            <fieldset id="fisUpload" class="ContenedorEtiquetaTexto" runat="server" visible="false">
                <div id="div6" class="EtiquetaTextoIzquierda">
                    <asp:Label ID="lblUploadCarga" runat="server" Text="" ForeColor="red"></asp:Label>
                </div>
            </fieldset>
            <div style="clear:both;"></div>
            <div id="div2" >
                <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
             <div style="clear:both;"></div>
            <ul class="datos_cip2">
                <li class="complet">
                    <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul5" OnClientClick="return RegistrarAgenciaBancaria();"
                        Text="Registrar" ValidationGroup="ValidaCampos"></asp:Button>
                    <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul5" OnClientClick="return RegistrarAgenciaBancaria();"
                        Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False"></asp:Button>
                    <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return confirm('Esta seguro que desea cancelar este documento? ');"
                        Text="Cancelar"></asp:Button>
                </li>
            </ul>
        </asp:Panel>
    </div>
</asp:Content>
