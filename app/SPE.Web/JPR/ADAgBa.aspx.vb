Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Imports System.IO
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class ADM_ADAgBa
    Inherits _3Dev.FW.Web.PageBase

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '

            Try
                OcultarDDL(True)
                '
                Page.SetFocus(txtNombre)
                CargarCombos()
                '
                If Page.PreviousPage Is Nothing OrElse Context.Items.Item("IdAgenciaBancaria") Is Nothing Then 'CONSULTAR AGENCIA
                    CargarProcesoRegistro()
                Else
                    CargarProcesoActualizar(Convert.ToInt32(Context.Items.Item("IdAgenciaBancaria")))
                End If
                '
            Catch ex As Exception
                '
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
                '
            End Try

        End If
        '
    End Sub

    'PROCESO DE REGISTRO
    Private Sub CargarProcesoRegistro()
        '
        'Title = "PagoEfectivo - Registrar Agencia Bancaria"
        lblProceso.Text = "Registrar Agencia Bancaria"
        txtNombre.Text = ""
        btnRegistrar.Visible = True
        btnActualizar.Visible = False

        ddlEstado.Visible = False
        lblEstado.Visible = False
        PnlEmpresa.Enabled = True

        lblIdAgenciaBancaria.Text = "0"

        '
        CargarCombosUbigeo("Pais")
        CargarCombosUbigeo("Departamento")
        CargarCombosUbigeo("Ciudad")
        '
    End Sub

    'PROCESO DE ACTUALIZACION
    Private Sub CargarProcesoActualizar(ByVal IdAgencia As Integer)
        '
        'Title = "PagoEfectivo - Actualizar Agencia Bancaria"
        lblProceso.Text = "Actualizar Agencia Bancaria"

        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        PnlEmpresa.Enabled = True

        lblEstado.Visible = True
        ddlEstado.Visible = True
        '
        Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        Dim obeAgenciaBancaria As New BEAgenciaBancaria
        obeAgenciaBancaria = objCAgenciaBancaria.ConsultarAgenciaBancariaxId(IdAgencia)
        '
        txtNombre.Text = obeAgenciaBancaria.Descripcion
        txtDireccion.Text = obeAgenciaBancaria.Direccion
        txtCodigo.Text = obeAgenciaBancaria.Codigo
        ddlBanco.SelectedValue = obeAgenciaBancaria.IdBanco
        ddlEstado.SelectedValue = obeAgenciaBancaria.IdEstado

        ddlEstado.SelectedValue = obeAgenciaBancaria.IdEstado

        lblIdAgenciaBancaria.Text = IdAgencia
        CargarCombosUbigeo("Pais")
        ddlPais.SelectedValue = obeAgenciaBancaria.IdPais
        CargarCombosUbigeo("Departamento")
        ddlDepartamento.SelectedValue = obeAgenciaBancaria.IdDepartamento
        CargarCombosUbigeo("Ciudad")
        ddlCiudad.SelectedValue = obeAgenciaBancaria.IdCiudad
        '
    End Sub

    'REGISTRAR AGENTE
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        '
        Try
            '
            'PROCESO DE REGISTRO
            Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
            If objCAgenciaBancaria.RegistrarAgenciaBancaria(CargarDatosBEAgencia()) = ExisteAgenciaBancaria.Existe Then
                lblMensajeCodigo.Visible = True
                lblMensajeCodigo.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeExisteAgenciaBancaria
                ScriptManager1.SetFocus(txtCodigo)
            Else
                lblMensajeCodigo.Visible = False
                btnRegistrar.Enabled = False
                btnCancelar.Visible = True
                PnlEmpresa.Enabled = False
                btnCancelar.Text = "Regresar"
                btnCancelar.OnClientClick = ""
                lblMensaje.Text = "** Se registr� satisfactoriamente la agencia: ''" & txtNombre.Text.Trim() & "''."
            End If
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try

    End Sub

    'ACTUALIZAR AGENTE
    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        '
        Try

            Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria

            If objCAgenciaBancaria.ActualizarAgenciaBancaria(CargarDatosBEAgencia()) = ExisteAgenciaBancaria.Existe Then
                lblMensajeCodigo.Visible = True
                lblMensajeCodigo.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeExisteAgenciaBancaria
                ScriptManager1.SetFocus(txtCodigo)
            Else
                lblMensajeCodigo.Visible = False
                btnCancelar.Visible = True
                lblMensaje.Visible = True
                PnlEmpresa.Enabled = False
                btnActualizar.Enabled = False
                btnCancelar.Text = "Regresar"
                btnCancelar.OnClientClick = ""
                lblMensaje.Text = "** Se actualiz� satisfactoriamente la agencia :''" & txtNombre.Text.Trim()

            End If
            '

            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Sub

    'CARGAMOS LOS DATOS DE LA AGENCIA BANCARIA
    Private Function CargarDatosBEAgencia() As BEAgenciaBancaria
        '
        Try
            '
            Dim obeAgencia As New BEAgenciaBancaria


            obeAgencia.IdBanco = CInt(ddlBanco.SelectedValue)
            obeAgencia.Codigo = txtCodigo.Text.Trim()
            obeAgencia.Descripcion = txtNombre.Text.Trim()
            obeAgencia.Direccion = txtDireccion.Text.Trim()

            If lblIdAgenciaBancaria.Text = "0" Then 'CUANDO SE REGISTRA
                obeAgencia.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenciaBancaria.Activo
            Else 'CUANDO SE ACTUALIZA
                obeAgencia.IdEstado = ddlEstado.SelectedValue
                obeAgencia.IdAgenciaBancaria = CInt(lblIdAgenciaBancaria.Text)
            End If

            obeAgencia.IdUsuarioCreacion = UserInfo.IdUsuario
            obeAgencia.IdUsuarioActualizacion = UserInfo.IdUsuario
            obeAgencia.IdCiudad = CInt(ddlCiudad.SelectedValue)

            Return obeAgencia
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Function

    'FILTRAR DEPARTAMENTO POR PAIS
    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPais.SelectedIndexChanged
        '
        CargarCombosUbigeo("Departamento")
        CargarCombosUbigeo("Ciudad")
        ScriptManager1.SetFocus(ddlPais)
        OcultarDDL(True)
        '
    End Sub

    'FILTRAR CIUDADES POR DEPARTAMENTO
    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartamento.SelectedIndexChanged
        '
        CargarCombosUbigeo("Ciudad")
        ScriptManager1.SetFocus(ddlDepartamento)
        '
    End Sub

    'CARGAMOS LOSC COMBOS DE UBIGEO
    Private Sub CargarCombosUbigeo(ByVal opcionCombo As String)
        '
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        '
        Select Case opcionCombo

            Case "Pais"
                ddlPais.DataTextField = "Descripcion" : ddlPais.DataValueField = "IdPais" : ddlPais.DataSource = objCUbigeo.ConsultarPais() : ddlPais.DataBind() : ddlPais.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlPais.SelectedIndex = 0
                '
            Case "Departamento"
                ddlDepartamento.DataTextField = "Descripcion" : ddlDepartamento.DataValueField = "IdDepartamento"
                If ddlPais.SelectedIndex = 0 Then
                    ddlDepartamento.DataSource = Nothing : ddlDepartamento.DataBind() : ddlDepartamento.Items.Clear() : ddlDepartamento.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
                Else
                    ddlDepartamento.DataSource = objCUbigeo.ConsultarDepartamentoPorIdPais(ddlPais.SelectedValue) : ddlDepartamento.DataBind()
                    ddlDepartamento.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlDepartamento.SelectedIndex = 0
                End If
            Case "Ciudad"
                ddlCiudad.DataTextField = "Descripcion" : ddlCiudad.DataValueField = "IdCiudad"
                If ddlDepartamento.SelectedIndex = 0 Then
                    ddlCiudad.DataSource = Nothing : ddlCiudad.DataBind() : ddlCiudad.Items.Clear() : ddlCiudad.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
                Else
                    ddlCiudad.DataSource = objCUbigeo.ConsultarCiudadPorIdDepartamento(ddlDepartamento.SelectedValue) : ddlCiudad.DataBind()
                    ddlCiudad.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, "")) : ddlCiudad.SelectedIndex = 0
                End If
        End Select
        '
    End Sub

    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        Dim objCBanco As New SPE.Web.CBanco
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = ConsultarEstadoAgBa(objCParametro.ConsultarParametroPorCodigoGrupo("ESAB")) : ddlEstado.DataBind()



        Dim obeBanco As New BEBanco
        obeBanco.Codigo = ""
        obeBanco.Descripcion = ""
        obeBanco.IdEstado = 0
        ddlBanco.DataTextField = "Descripcion" : ddlBanco.DataValueField = "IdBanco" : ddlBanco.DataSource = objCBanco.ConsultarBanco(obeBanco) : ddlBanco.DataBind() : ddlBanco.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))

        '
    End Sub

    'LIMPIAR FORM
    Protected Sub LimpiarForm()
        '
        txtNombre.Text = ""
        lblIdAgenciaBancaria.Text = "0"
        txtDireccion.Text = ""
        txtCodigo.Text = ""
        '
    End Sub

    'REGRESAR
    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        '
        btnCancelar_Click(sender, e)
        '
    End Sub

    'CANCELAR
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("~/JPR/COAgBa.aspx")
        '
    End Sub

    Private Sub OcultarDDL(ByVal flag As Boolean)
        ddlCiudad.Visible = flag
        ddlDepartamento.Visible = flag
        ddlPais.Visible = flag
    End Sub

    Private Function ConsultarEstadoAgBa(ByVal list As System.Collections.Generic.List(Of BEParametro)) As System.Collections.Generic.List(Of BEParametro)
        Dim i, num As Int16
        num = list.Count - 1

        For i = 0 To num
            If list(i).Id = SPE.EmsambladoComun.ParametrosSistema.EstadoAgenciaBancaria.Modificar Then
                list.RemoveAt(i)
            End If
        Next

        Return list

    End Function

    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Title = "PagoEfectivo - " & lblProceso.Text
    End Sub

End Class
