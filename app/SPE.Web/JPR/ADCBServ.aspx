<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADCBServ.aspx.vb" Inherits="POS_ADEstab" Title="Asociar Cuentas Bancarias a Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <style type="text/css">
        .neu
        {
            float: left;
        }
    </style>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Asociar Cuentas Bancarias a Servicio"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="upnlDatos" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="PnlEmpresa" runat="server">
                    <h4>
                        1. Datos del Servicio</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span>Empresa Contratante: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvEmpresa" runat="server" ValidationGroup="ValidaCampos"
                                Style="float: left" ErrorMessage="Debe seleccionar la Empresa." ControlToValidate="ddlEmpresa">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>Servicio: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvServicio" runat="server" ValidationGroup="ValidaCampos"
                                Style="float: left" ErrorMessage="Debe seleccionar la Servicio." ControlToValidate="ddlServicio">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>Banco: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="ValidaCampos"
                                Style="float: left" ErrorMessage="Debe seleccionar el Operador." ControlToValidate="ddlBanco">*</asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">>></span>Moneda: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvMoneda" runat="server" ValidationGroup="ValidaCampos"
                                Style="float: left" ErrorMessage="Debe seleccionar la Moneda." ControlToValidate="ddlMoneda">*</asp:RequiredFieldValidator>
                        </li>
                    </ul>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnRegistrar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <h4>
            2. Datos de la Cuenta</h4>
        <asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span>Codigo Servicio: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigoServicio" runat="server" CssClass="normal" MaxLength="9"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftetxtCodigoServicio" runat="server" ValidChars="0123456789"
                            TargetControlID="txtCodigoServicio">
                        </cc1:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="rfvTxtCodigoServicio" runat="server" ControlToValidate="txtCodigoServicio"
                            Style="float: left" ErrorMessage="Debe escribir un codigo para la Cuenta." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span>Nro. de Cuenta: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlNroCuenta" runat="server" CssClass="neu">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvNroCuenta" runat="server" ValidationGroup="ValidaCampos"
                            Style="float: left" ErrorMessage="Debe seleccionar el Nro de Cuenta." ControlToValidate="ddlNroCuenta">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span>Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvEstado" runat="server" ValidationGroup="ValidaCampos"
                            Style="float: left" ErrorMessage="Debe seleccionar el Estado." ControlToValidate="ddlEstado">*</asp:RequiredFieldValidator>
                    </li>
                    <li class="t1"><span class="color">>></span> Visible en Gen. de CIP : </li>
                    <li class="t2">
                        <asp:RadioButtonList ID="rblVisibleGenPago" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Si" Value="True" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" OnClientClick="return RegistrarEmpresa();"
                            Text="Registrar" ValidationGroup="ValidaCampos" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" OnClientClick="return RegistrarEmpresa();"
                            Text="Actualizar" ValidationGroup="ValidaCampos" />
                        <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return confirm('Esta seguro que desea cancelar? ');"
                            Text="Cancelar" />
                        <asp:Button ID="btnRegresar" runat="server" Visible="false" CssClass="input_azul4"
                            Text="Regresar" />
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 200px;
                    padding-bottom: 40px" />
                <asp:HiddenField ID="hdfIdServicioBanco" runat="server"></asp:HiddenField>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlBanco" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlMoneda" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
