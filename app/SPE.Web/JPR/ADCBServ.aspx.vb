Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class POS_ADEstab
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CServicio)

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim oBEServicioBanco As BEServicioBanco = CType(be, BEServicioBanco)
        ddlEmpresa.SelectedValue = oBEServicioBanco.idEmpresaContratante
        ddlEmpresa_SelectedIndexChanged(Nothing, Nothing)
        ddlServicio.SelectedValue = oBEServicioBanco.idServicio
        ddlBanco.SelectedValue = oBEServicioBanco.IdBanco
        ddlMoneda.SelectedValue = oBEServicioBanco.IdMoneda
        ddlMoneda_SelectedIndexChanged(Nothing, Nothing)
        ddlNroCuenta.SelectedValue = oBEServicioBanco.idCuentaBanco
        ddlEstado.SelectedValue = oBEServicioBanco.IdEstado
        txtCodigoServicio.Text = oBEServicioBanco.CodigoServicio
        rblVisibleGenPago.SelectedValue = oBEServicioBanco.FlagVisibleGenPago

        ddlEmpresa.Enabled = False
        ddlServicio.Enabled = False
        ddlBanco.Enabled = False
        ddlMoneda.Enabled = False
    End Sub

    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.URLPageCancelForEdit = "COCBServ.aspx"
        e.URLPageCancelForInsert = "COCBServ.aspx"
        e.URLPageCancelForSave = "COCBServ.aspx"
    End Sub

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim oBEServicioBanco As New BEServicioBanco
        oBEServicioBanco.IdServicioBanco = hdfIdServicioBanco.Value
        oBEServicioBanco.idServicio = ddlServicio.SelectedValue
        oBEServicioBanco.IdBanco = ddlBanco.SelectedValue
        oBEServicioBanco.IdMoneda = ddlMoneda.SelectedValue
        oBEServicioBanco.CodigoServicio = txtCodigoServicio.Text
        oBEServicioBanco.IdEstado = ddlEstado.SelectedValue
        oBEServicioBanco.idCuentaBanco = ddlNroCuenta.SelectedValue
        oBEServicioBanco.IdUsuarioActualizacion = UserInfo.IdUsuario
        oBEServicioBanco.FlagVisibleGenPago = rblVisibleGenPago.SelectedValue
        Return oBEServicioBanco
    End Function

    Public Overrides Function DoUpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Dim CtrlServicio As New CServicio
        Return CtrlServicio.ActualizarAsociacionCuentaServicio(be)
    End Function

    Public Overrides Sub OnLoadInformation()
        Dim oBEServicioBanco As New BEServicioBanco
        Dim oCServicio As New CServicio
        oBEServicioBanco.IdServicioBanco = MaintenanceKeyValue
        hdfIdServicioBanco.Value = oBEServicioBanco.IdServicioBanco
        Dim oBEServicioBancoResponse As BEServicioBanco = oCServicio.ObtenerAsociacionCuentaServicio(oBEServicioBanco)
        AssignBusinessEntityValuesInLoadingInfo(oBEServicioBancoResponse)
    End Sub


    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Dim oBEServicioBanco As New BEServicioBanco
        oBEServicioBanco.idServicio = ddlServicio.SelectedValue
        oBEServicioBanco.IdBanco = ddlBanco.SelectedValue
        oBEServicioBanco.IdMoneda = ddlMoneda.SelectedValue
        oBEServicioBanco.CodigoServicio = txtCodigoServicio.Text
        oBEServicioBanco.IdEstado = ddlEstado.SelectedValue
        oBEServicioBanco.idCuentaBanco = ddlNroCuenta.SelectedValue
        oBEServicioBanco.IdUsuarioCreacion = UserInfo.IdUsuario
        oBEServicioBanco.FlagVisibleGenPago = rblVisibleGenPago.SelectedValue
        Return oBEServicioBanco
    End Function
    Public Overrides Function DoInsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Using oCtrlServicio As New CServicio
            Dim RESUL As Integer
            RESUL = oCtrlServicio.RegistrarAsociacionCuentaServicio(be)
            Return RESUL
        End Using
    End Function
    Public Overrides Sub OnAfterInsert()
        ddlEmpresa.Enabled = False
        ddlServicio.Enabled = False
        ddlBanco.Enabled = False
        ddlMoneda.Enabled = False
        txtCodigoServicio.Enabled = False
        ddlNroCuenta.Enabled = False
        ddlEstado.Enabled = False
        btnRegistrar.Visible = False
    End Sub
    Public Overrides Sub OnAfterUpdate()
        ddlEmpresa.Enabled = False
        ddlServicio.Enabled = False
        ddlBanco.Enabled = False
        ddlMoneda.Enabled = False
        txtCodigoServicio.Enabled = False
        ddlNroCuenta.Enabled = False
        ddlEstado.Enabled = False
        btnActualizar.Visible = False
        BtnCancel.Visible = False
        btnRegresar.Visible = True
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim oBEBanco As New BEBanco
            Dim oCntrBanco As New CBanco
            Dim oCntrPlantilla As New CPlantilla()
            Dim oCntrParametro As New CAdministrarParametro()
            Dim oBEEmpresaContratante As New BEEmpresaContratante()
            Dim oCntrAdministracionComun As New CAdministrarComun()
            Using ocntrlEmpresaContrante As New CEmpresaContratante()
                If Not Me.PageManager.IsEditing Then
                    oBEEmpresaContratante.IdEstado = EstadoMantenimiento.Activo
                End If
                DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Seleccione::")
                DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Seleccione::")
            End Using
            oBEBanco.IdEstado = EstadoBanco.Activo
            DropDownlistBinding(ddlBanco, oCntrBanco.ConsultarBanco(oBEBanco), "Descripcion", "IdBanco", "::Seleccione::")
            DropDownlistBinding(ddlMoneda, oCntrAdministracionComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
            DropDownlistBinding(ddlEstado, oCntrParametro.ConsultarParametroPorCodigoGrupo("ESMA"), "Descripcion", "Id")
            DropDownlistBinding(ddlNroCuenta, New List(Of BECuentaBanco), "NroCuenta", "IdCuentaBanco", "::Seleccione::")
        End If
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - Asociar Cuentas Bancarias a Servicio"
    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

    End Sub
    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        If ddlEmpresa.SelectedIndex = 0 Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", ":: Seleccione ::")
            Return
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            objbeservicio.IdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
            If Not Me.PageManager.IsEditing Then
                objbeservicio.IdEstado = EstadoMantenimiento.Activo
            End If
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, ocntrlServicio.SearchByParameters(objbeservicio), "Nombre", "IdServicio", ":: Seleccione ::")
        End Using
    End Sub
    Protected Sub ddlBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBanco.SelectedIndexChanged
        CargarCuentasBanco()
    End Sub
    Protected Sub ddlMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMoneda.SelectedIndexChanged
        CargarCuentasBanco()
    End Sub
    Protected Sub CargarCuentasBanco()
        Dim oCntrlBanco As New CBanco
        If ddlBanco.SelectedIndex <> 0 And ddlMoneda.SelectedIndex <> 0 Then
            Dim oBECuentaBanco As New BECuentaBanco
            oBECuentaBanco.IdBanco = ddlBanco.SelectedValue
            oBECuentaBanco.IdMoneda = ddlMoneda.SelectedValue
            DropDownlistBinding(ddlNroCuenta, oCntrlBanco.ConsultarCuentaBanco(oBECuentaBanco), "NroCuenta", "IdCuentaBanco", "::Seleccione::")
        Else
            DropDownlistBinding(ddlNroCuenta, New List(Of BECuentaBanco), "NroCuenta", "IdCuentaBanco", "::Seleccione::")
        End If
    End Sub

    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("~/JPR/COCBServ.aspx")
    End Sub
    Public Overrides Sub OnMainInsert()
        Dim businessEntityObject As New BusinessEntityBase
        Dim intResult As Integer = 0
        businessEntityObject = CreateBusinessEntityForRegister(businessEntityObject)
        intResult = DoInsertRecord(businessEntityObject)
        If intResult = -1 Then
            ThrowErrorMessage("Ya existe una cuenta asociada al servicio")
        Else
            ThrowSuccessfullyMessage(DefaultSuccessfullMessage)
            EnabledButtonControls()
        End If

    End Sub

End Class
