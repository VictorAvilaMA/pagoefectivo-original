<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADCom.aspx.vb" Inherits="JPR_ADCom" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Literal ID="lblProceso" runat="server">Registrar Comercio</asp:Literal></h2>
    <div id="Page" class="conten_pasos3">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:Panel ID="PnlEmpresa" runat="server">
            <h4>
                1. Informaci�n general</h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">&gt;&gt;</span>C&oacute;digo Comercio: :</li>
                <li class="t2">
                    <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="11"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCodigo"
                        Display="Dynamic" ErrorMessage="Ingrese el C�digo del Comercio." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span>Raz�n Social: :</li>
                <li class="t2">
                    <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    <asp:Label ID="lblIdEmpresaContrante" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRazonSocial"
                        Display="Dynamic" ErrorMessage="Ingrese la Raz�n Social." ValidationGroup="ValidaCampos" style="float:left;">*</asp:RequiredFieldValidator>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span>R.U.C.: </li>
                <li class="t2">
                    <asp:TextBox ID="txtRuc" runat="server" CssClass="normal" MaxLength="11"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRuc"
                        Display="Dynamic" ErrorMessage="Ingrese el RUC." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator><asp:RangeValidator
                            ID="rvaRuc" runat="server" ErrorMessage="Ingrese un formato correcto para el RUC."
                            MinimumValue="10000000000" MaximumValue="1000000000000" ControlToValidate="txtRuc"
                            Type="Double">*</asp:RangeValidator>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span>Contacto:</li>
                <li class="t2">
                    <asp:TextBox ID="txtContacto" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span>Tel&eacute;fono:</li>
                <li class="t2">
                    <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span>Direcci&oacute;n:</li>
                <li class="t2">
                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" MaxLength="200"></asp:TextBox>
                </li>
                <asp:Label ID="lblIdComercio" runat="server" Text="" Style="display: none;"></asp:Label>
                <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <li class="t1"><span class="color">&gt;&gt;</span>Pa&iacute;s:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlPais" runat="server" CssClass="neu" AutoPostBack="True">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span>Departamento:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="neu" AutoPostBack="True">
                            </asp:DropDownList>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span>Ciudad:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <li class="t1" id="t1Estado" runat="server" visible="false"><span class="color">&gt;&gt;</span>
                            <asp:Label ID="lblEstado" runat="server" Text="Estado:" Visible="False">
                            </asp:Label>
                        </li>
                        <li class="t2" id="t2Estado" runat="server" visible="false">
                            <asp:DropDownList ID="ddlEstado" runat="server" Visible="False" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <li class="complet">
                    <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" OnClientClick="return RegistrarEmpresa();"
                        Text="Registrar" ValidationGroup="ValidaCampos" />
                    <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul4" OnClientClick="return RegistrarEmpresa();"
                        Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False" />
                    <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return confirm('Esta seguro que desea cancelar este documento? ');"
                        Text="Cancelar" />
                </li>
                <asp:Label ID="lblMensaje" runat="server"></asp:Label>
            </ul>
        </asp:Panel>
        <cc1:FilteredTextBoxExtender ID="FTBE_txtTelefono" runat="server" ValidChars="0123456789-"
            TargetControlID="txtTelefono">
        </cc1:FilteredTextBoxExtender>
        <cc1:FilteredTextBoxExtender ID="FTBE_TxtContacto" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'& "
            TargetControlID="txtContacto">
        </cc1:FilteredTextBoxExtender>
    </div>
</asp:Content>
