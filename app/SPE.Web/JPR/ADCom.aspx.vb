Imports SPE.Entidades
Imports _3Dev.FW.Web.Security


Partial Class JPR_ADCom
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CComercio)
    'Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Page.SetFocus(txtCodigo)
            SeleccionarPais(0)
            SeleccionarDepartamento(0)
            SeleccionarCiudad(0)
            CargarComboEstado()
            'CargaOperadores()
            ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
        End If

    End Sub
   
    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim beComercio As BEComercio = CreateBEComercio(be)
        beComercio.idComercio = lblIdComercio.Text
        Return beComercio

    End Function

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property


    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartamento.SelectedIndexChanged
        SeleccionarCiudad(0)
        ScriptManager1.SetFocus(ddlDepartamento)
    End Sub

    Private Sub CargarCombosUbigeo(ByVal opcionCombo As String)

        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Select Case opcionCombo
            Case "Pais"
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            Case "Departamento"
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(ddlPais.SelectedValue), _
                "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            Case "Ciudad"
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(ddlDepartamento.SelectedValue), _
                "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        End Select

    End Sub


    Sub SeleccionarPais(ByVal IdPais As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        If IdPais > 0 Then
            Me.ddlPais.SelectedValue = IdPais
        End If

    End Sub

    Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlPais.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlPais.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), _
                "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        If IdDepartamento > 0 Then
            Me.ddlDepartamento.SelectedValue = IdDepartamento
        Else
            Me.ddlDepartamento.SelectedIndex = 0
        End If
    End Sub

    Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlDepartamento.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlDepartamento.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), _
                "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        If IdCiudad > 0 Then
            Me.ddlCiudad.SelectedValue = IdCiudad
        Else
            Me.ddlCiudad.SelectedIndex = 0
        End If
    End Sub

    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
        PnlEmpresa.Enabled = True

    End Sub

    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPais.SelectedIndexChanged
        SeleccionarDepartamento(0)
        SeleccionarCiudad(0)
        ScriptManager1.SetFocus(ddlPais)
    End Sub

    Private Sub CargarComboEstado()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", "")

    End Sub


    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Return CreateBEComercio(be)

    End Function

    Protected Function CreateBEComercio(ByVal obeComercio As SPE.Entidades.BEComercio) As SPE.Entidades.BEComercio

        obeComercio = New SPE.Entidades.BEComercio
        obeComercio.codComercio = txtCodigo.Text
        obeComercio.razonSocial = txtRazonSocial.Text
        obeComercio.ruc = txtRuc.Text
        obeComercio.contacto = txtContacto.Text
        obeComercio.telefono = txtTelefono.Text
        obeComercio.direccion = txtDireccion.Text
        obeComercio.idCiudad = CInt(ddlCiudad.SelectedValue)
        obeComercio.idEstado = CInt(ddlEstado.SelectedValue)
        obeComercio.IdUsuarioCreacion = UserInfo.IdUsuario


        Return obeComercio

    End Function
    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase

    '    Return New SPE.Web.CComercio

    'End Function
    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)

        Dim obeComercio As SPE.Entidades.BEComercio = CType(be, SPE.Entidades.BEComercio)

        txtRazonSocial.Text = obeComercio.razonSocial
        ddlEstado.SelectedValue = obeComercio.idEstado
        ddlPais.SelectedValue = obeComercio.idPais
        SeleccionarDepartamento(obeComercio.idDepartamento)
        SeleccionarCiudad(obeComercio.idCiudad)
        txtTelefono.Text = obeComercio.telefono
        txtRuc.Text = obeComercio.ruc
        txtTelefono.Text = obeComercio.telefono
        txtDireccion.Text = obeComercio.direccion
        txtContacto.Text = obeComercio.contacto
        lblIdComercio.Text = obeComercio.idComercio
        txtCodigo.Text = obeComercio.codComercio

        'OCULTAR CONTROLES
        lblEstado.Visible = True
        ddlEstado.Visible = True
        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        lblProceso.Text = "Actualizar Comercio"
        t1Estado.Visible = True
        t2Estado.Visible = True


    End Sub

    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADCom"
        e.URLPageCancelForEdit = "COCom.aspx"
        e.URLPageCancelForInsert = "COCom.aspx"

    End Sub
    Public Overrides Sub OnAfterUpdate()

        PnlEmpresa.Enabled = False



        If valueUpdated = 5 Then
            ThrowErrorMessage("El RUC ya existe.")
            PnlEmpresa.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager1.SetFocus(txtRuc)
        ElseIf valueUpdated = 6 Then
            ThrowErrorMessage("El C�digo de Comercio ya existe.")
            PnlEmpresa.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager1.SetFocus(txtCodigo)
        Else
            ControlesMantenimiento(False)
            BtnCancel.Text = "Regresar"
            PnlEmpresa.Enabled = False
            btnActualizar.Enabled = False
        End If





    End Sub
    Public Overrides Sub OnAfterInsert()
        PnlEmpresa.Enabled = False
        If valueRegister = 5 Then
            ThrowErrorMessage("El RUC ya existe.")
            PnlEmpresa.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager1.SetFocus(txtRuc)
        ElseIf valueRegister = 6 Then

            ThrowErrorMessage("El C�digo de Comercio ya existe.")
            PnlEmpresa.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager1.SetFocus(txtCodigo)
        Else
            ControlesMantenimiento(False)
            BtnCancel.Text = "Regresar"
        End If

    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub
    Private Sub ControlesMantenimiento(ByVal Habilitar As Boolean)
        OcultarControles(Habilitar)
        btnActualizar.Enabled = Habilitar
        btnRegistrar.Enabled = Habilitar

    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - " + lblProceso.Text
    End Sub
    Private Sub OcultarControles(ByVal flag As Boolean)
        'txtCodigoEstablecimiento.Enabled = flag
        'ibtnBuscar.Enabled = flag
        'ddlOperador.Enabled = flag
        'txtDescripcion.Enabled = flag
        'txtComercio.Enabled = flag

    End Sub
End Class
