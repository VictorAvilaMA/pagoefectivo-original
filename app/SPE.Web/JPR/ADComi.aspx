<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADComi.aspx.vb" Inherits="JPR_ADComi" Title="Actualizar Comisiones por Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #Error div ul li
        {
            color: Red !important;
        }
        ul.datos_cip2 li select, input.neu
        {
            float: left !important;
        }
        li span[style*="hidden"]
        {
            width: auto !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Comisiones por Servicio"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPage" runat="server">
                    <h4>
                        1. Informaci�n general</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Empresa Contratante: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEmpresaContratante" runat="server" CssClass="neu" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlEmpresaContratante" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar la empresa contratante." ControlToValidate="ddlEmpresaContratante">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Servicio: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar el servicio." ControlToValidate="ddlServicio">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Moneda: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar la moneda." ControlToValidate="ddlMoneda">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Tipo de Comisi&oacute;n: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlTipoComision" runat="server" CssClass="neu" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar el tipo de comision." ControlToValidate="ddlTipoComision">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <li id="liDscFija" runat="server" class="t1"><span class="color">&gt;&gt;</span>Comisi&oacute;n
                            por OP
                            <% IIf(ddlMoneda.SelectedValue = "1", "(S/.)", IIf(ddlMoneda.SelectedValue = "2", "($)", ""))%>
                            : </li>
                        <li id="liComisionFija" runat="server" class="t2">
                            <asp:TextBox ID="txtMontoFijo" runat="server" CssClass="neu" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTBE_txtMonto" runat="server" ValidChars="0123456789."
                                TargetControlID="txtMontoFijo">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RangeValidator ID="rvDescripcion" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese solo n�meros v�lidos" ControlToValidate="txtMontoFijo"
                                MaximumValue="1000000" MinimumValue="0" Type="Double">* </asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese la Comisi�n por OP." ControlToValidate="txtMontoFijo"
                                ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                        </li>
                        <li id="liDscMixta" runat="server" class="t1"><span class="color">&gt;&gt;</span>Monto
                            Limite (mixta) : </li>
                        <li id="liComisionMixta" runat="server" class="t2">
                            <asp:TextBox ID="txtMontoLimite" runat="server" CssClass="neu" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" ValidChars="0123456789."
                                TargetControlID="txtMontoLimite">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese solo n�meros v�lidos" ControlToValidate="txtMontoLimite"
                                MaximumValue="1000000" MinimumValue="0" Type="Double">*</asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese Monto Limite (mixta)" ControlToValidate="txtMontoLimite"
                                ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                            <asp:Label ID="Label3" runat="server" Visible="false" Text=""></asp:Label>
                            <asp:Label ID="Label4" runat="server" Visible="false" Text=""></asp:Label>
                        </li>
                        <li id="liDscPorcentaje" runat="server" class="t1"><span class="color">&gt;&gt;</span>%
                            Comisi&oacute;n por OP<% IIf(ddlMoneda.SelectedValue = "1", "(S/.)", IIf(ddlMoneda.SelectedValue = "2", "($)", ""))%>
                            : </li>
                        <li id="liComisionPorcentaje" runat="server" class="t2">
                            <asp:TextBox ID="txtMontoPorcentaje" runat="server" CssClass="neu" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" ValidChars="0123456789."
                                TargetControlID="txtMontoPorcentaje">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese solo n�meros v�lidos" ControlToValidate="txtMontoPorcentaje"
                                MaximumValue="1000000" MinimumValue="0" Type="Double">* </asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese % Comisi�n por OP." ControlToValidate="txtMontoPorcentaje"
                                ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                        </li>
                        <li id="liDscMontoLimitePorcent" runat="server" class="t1"><span class="color">&gt;&gt;</span>Monto
                            M&aacute;ximo por comisi&oacute;n: </li>
                        <li id="liMontoLimitePorcent" runat="server" class="t2">
                            <asp:TextBox ID="txtMontoLimitePorc" runat="server" CssClass="neu" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" ValidChars="0123456789."
                                TargetControlID="txtMontoLimitePorc">
                            </cc1:FilteredTextBoxExtender>
                            <asp:RangeValidator ID="RangeValidator3" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese solo n�meros v�lidos" ControlToValidate="txtMontoLimitePorc"
                                MaximumValue="1000000" MinimumValue="0" Type="Double">*</asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="IsNullOrEmpty"
                                ForeColor="Red" ErrorMessage="Ingrese Monto M�ximo por comisi�n." ControlToValidate="txtMontoLimitePorc"
                                ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                            <asp:Label ID="Label1" runat="server" Visible="false" Text=""></asp:Label>
                            <asp:Label ID="Label2" runat="server" Visible="false" Text=""></asp:Label>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvEstado" runat="server" ValidationGroup="ValidaCampos"
                                ForeColor="Red" ErrorMessage="Debe seleccionar el Estado." ControlToValidate="ddlTipoComision">*
                            </asp:RequiredFieldValidator>
                        </li>
                        <asp:HiddenField ID="hdfServicio" runat="server" Value=""></asp:HiddenField>
                        <asp:HiddenField ID="hdfMoneda" runat="server" Value=""></asp:HiddenField>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <div id="Error" class="validateSummary">
                        <asp:ValidationSummary ID="vsmMessage" runat="server" ValidationGroup="IsNullOrEmpty">
                        </asp:ValidationSummary>
                        <ul id="ulMensaje" runat="server" visible="false">
                            <li>
                                <asp:Literal ID="ltlMensaje" runat="server" Text="asd"></asp:Literal></li>
                        </ul>
                        <div style="color: navy; font-weight: bold; padding-bottom: 15px;">
                            <asp:Label ID="lblTransaccion" runat="server" CssClass="MensajeTransaccion" Width="650px">
                            </asp:Label>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <ul id="fsBotonera" runat="server" class="datos_cip2 h0">
                        <li id="botoneraA" runat="server" class="complet">
                            <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Registrar"
                                OnClientClick="return ConfirmMe();" Visible="true"></asp:Button>
                            <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" Text="Actualizar"
                                OnClientClick="return ConfirmMe();" Visible="false"></asp:Button>
                            <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Cancelar"
                                OnClientClick="return confirm('�Est� Ud. seguro que desea cancelar la operaci�n? ');">
                            </asp:Button>
                        </li>
                        <li id="botoneraB" runat="server" visible="false" class="complet">
                            <asp:Button ID="btnRegresar" runat="server" CssClass="input_azul3" Text="Regresar">
                            </asp:Button>
                        </li>
                    </ul>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
