Imports System.Data
Imports SPE.Entidades
Imports SPE.Web
Imports SPE.EmsambladoComun
Imports System.Collections.Generic

Partial Class JPR_ADComi
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CServicio)

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblTransaccion
        End Get
    End Property

    Protected Function CreateBEServicioUpd(ByVal obeServicio As SPE.Entidades.BEServicio) As SPE.Entidades.BEServicio
        obeServicio = New SPE.Entidades.BEServicio
        obeServicio.IdServicio = Convert.ToInt32(hdfServicio.Value)
        obeServicio.IdMoneda = CInt(hdfMoneda.Value)
        obeServicio.TipoComision = CInt(ddlTipoComision.SelectedValue)
        Dim dv As New CDineroVirtual
        Dim NroDec As Integer
        NroDec = dv.ObtenerParametroMonederoByID(12).ValorInteger
        obeServicio.MontoComision = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoFijo.Text), "0.00", txtMontoFijo.Text)), NroDec)
        obeServicio.PorcentajeComision = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoPorcentaje.Text), "0.00", txtMontoPorcentaje.Text)), NroDec)
        obeServicio.MontoLimite = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoLimite.Text), "0.00", txtMontoLimite.Text)), NroDec)
        obeServicio.MontoLimitePorcentaje = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoLimitePorc.Text), "0.00", txtMontoLimitePorc.Text)), NroDec)
        obeServicio.IdUsuarioActualizacion = UserInfo.IdUsuario
        obeServicio.IdEstadoComision = ddlEstado.SelectedValue
        Return obeServicio
    End Function
    Protected Function CreateBEServicioIns(ByVal obeServicio As SPE.Entidades.BEServicio) As SPE.Entidades.BEServicio
        obeServicio = New SPE.Entidades.BEServicio
        obeServicio.IdServicio = CInt(ddlServicio.SelectedValue)
        obeServicio.IdMoneda = CInt(ddlMoneda.SelectedValue)
        obeServicio.TipoComision = CInt(ddlTipoComision.SelectedValue)
        Dim dv As New CDineroVirtual
        Dim NroDec As Integer
        NroDec = dv.ObtenerParametroMonederoByID(12).ValorInteger
        obeServicio.MontoComision = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoFijo.Text), "0.00", txtMontoFijo.Text)), NroDec)
        obeServicio.PorcentajeComision = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoPorcentaje.Text), "0.00", txtMontoPorcentaje.Text)), NroDec)
        obeServicio.MontoLimite = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoLimite.Text), "0.00", txtMontoLimite.Text)), NroDec)
        obeServicio.MontoLimitePorcentaje = Math.Round(CDec(IIf(String.IsNullOrEmpty(txtMontoLimitePorc.Text), "0.00", txtMontoLimitePorc.Text)), NroDec)
        obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
        Return obeServicio
    End Function
    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Return CreateBEServicioIns(be)
    End Function

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase
        Return CreateBEServicioUpd(be)
    End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeServicio As SPE.Entidades.BEServicio = CType(be, SPE.Entidades.BEServicio)
        'txtCodigo.Text = obeServicio.Codigo
        hdfServicio.Value = obeServicio.IdServicio
        hdfMoneda.Value = obeServicio.IdMoneda

        ddlEmpresaContratante.SelectedValue = obeServicio.IdEmpresaContratante
        ddlMoneda.SelectedValue = obeServicio.IdMoneda
        ddlTipoComision.SelectedValue = obeServicio.IdTipoComision

        CargarServicio()
        ddlServicio.SelectedValue = obeServicio.IdServicio
        ddlEstado.SelectedValue = obeServicio.IdEstadoComision

        txtMontoFijo.Text = obeServicio.MontoComision
        txtMontoPorcentaje.Text = obeServicio.PorcentajeComision
        txtMontoLimite.Text = obeServicio.MontoLimite
        txtMontoLimitePorc.Text = obeServicio.MontoLimitePorcentaje

        'Activar controles
        ddlEmpresaContratante.Enabled = False
        ddlServicio.Enabled = False
        ddlMoneda.Enabled = False
        ddlEstado.Enabled = True

        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        lblProceso.Text = "Actualizar Comisiones por Servicio"
        VisualizarComisiones(obeServicio.IdTipoComision)
    End Sub
    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
        pnlPage.Enabled = True
    End Sub
    Public Overrides Sub OnAfterUpdate()
        'pnlPage.Enabled = False
        btnActualizar.Enabled = False
        BtnCancel.Enabled = False
        txtMontoFijo.Enabled = False
        txtMontoLimite.Enabled = False
        txtMontoPorcentaje.Enabled = False
        txtMontoLimitePorc.Enabled = False
        ddlTipoComision.Enabled = False
        ddlEstado.Enabled = False
        botoneraA.Visible = False
        botoneraB.Visible = True
    End Sub
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADOper"
        e.URLPageCancelForEdit = "COComi.aspx"
        e.URLPageCancelForInsert = "COComi.aspx"
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            CargarComboEstado()
            VisualizarComisiones(ddlTipoComision.SelectedValue)
        End If
    End Sub
    ''CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarComboEstado()
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        ddlTipoComision.DataTextField = "Descripcion"
        ddlTipoComision.DataValueField = "Id"
        ddlTipoComision.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("TCOM")
        ddlTipoComision.DataBind()
        ddlTipoComision.Items.Insert(0, New ListItem("::Seleccione::", ""))
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - " + Me.lblProceso.Text
        btnRegresar.Enabled = True
    End Sub
    Public Overrides Function DoUpdateRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Return CType(ControllerObject, SPE.Web.CServicio).ActualizaServicioxEmpresa(be)
    End Function

    Public Overrides Sub OnLoadInformation()
        Dim oBEServicio As New BEServicio
        Dim obeSC As SPE.Entidades.BEServicio = CType(MaintenanceKeyValue, SPE.Entidades.BEServicio)
        Dim oCServicio As New CServicio
        Dim obResponse As BEServicio = oCServicio.ObtenerServicioComisionporID(obeSC)
        AssignBusinessEntityValuesInLoadingInfo(obResponse)
    End Sub

    Protected Sub btnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        Response.Redirect("~/JPR/COComi.aspx")
    End Sub

    Protected Sub ddlEmpresaContratante_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresaContratante.SelectedIndexChanged
        CargarServicio()
    End Sub
    Private Sub CargarServicio()
        If ddlEmpresaContratante.SelectedIndex = 0 Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", ":: Seleccione ::")
            Return
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            objbeservicio.NombreEmpresaContrante = ""
            objbeservicio.IdEmpresaContratante = CInt(ddlEmpresaContratante.SelectedValue)
            objbeservicio.Nombre = ""
            objbeservicio.IdEstado = 0
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, ocntrlServicio.SearchByParameters(objbeservicio), "Nombre", "IdServicio", ":: Seleccione ::")
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
            Dim objCParametro As New CAdministrarParametro
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
            Using ocntrlEmpresaContrante As New CEmpresaContratante()
                Dim oBEEmpresaContratante As New BEEmpresaContratante()
                oBEEmpresaContratante.Codigo = ""
                oBEEmpresaContratante.RazonSocial = ""
                oBEEmpresaContratante.RUC = ""
                oBEEmpresaContratante.IdEstado = 41
                oBEEmpresaContratante.Email = ""
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEmpresaContratante, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Seleccione::")
            End Using
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), "Descripcion", "Id")
            ddlEstado.Enabled = False
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio")
        End If
    End Sub
    Public Overrides Sub OnMainInsert()
        Dim businessEntityObject As New _3Dev.FW.Entidades.BusinessEntityBase
        Dim beServ As New BEServicio
        beServ = CreateBusinessEntityForRegister(beServ)
        valueRegister = DoInsertRecord(beServ)
        If valueRegister = -1 Then
            ThrowErrorMessage("Ya existe el tipo de comisi�n asociado al servicio")
        Else
            ThrowSuccessfullyMessage("Se registro correctamente la operacion con c�digo:" & valueRegister)
            btnRegistrar.Enabled = False
            BtnCancel.Enabled = False
            ddlEmpresaContratante.Enabled = False
            ddlServicio.Enabled = False
            ddlMoneda.Enabled = False
            txtMontoFijo.Enabled = False
            txtMontoPorcentaje.Enabled = False
            txtMontoLimite.Enabled = False
            txtMontoLimitePorc.Enabled = False
            ddlTipoComision.Enabled = False
            botoneraA.Visible = False
            botoneraB.Visible = True
        End If
    End Sub
    Public Overrides Function DoInsertRecord(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As Integer
        Return CType(ControllerObject, SPE.Web.CServicio).RegistrarServicioxEmpresa(be)
    End Function
    Protected Sub ddlTipoComision_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTipoComision.SelectedIndexChanged
        If ddlTipoComision.SelectedValue <> String.Empty Then
            VisualizarComisiones(ddlTipoComision.SelectedValue)
        End If
    End Sub
    Protected Sub VisualizarComisiones(ByVal tipoComision As String)
        liComisionFija.Visible = False
        liComisionMixta.Visible = False
        liComisionPorcentaje.Visible = False
        liMontoLimitePorcent.Visible = False
        liDscFija.Visible = False
        liDscPorcentaje.Visible = False
        liDscMixta.Visible = False
        liDscMontoLimitePorcent.Visible = False
        ulMensaje.Visible = False
        Select Case tipoComision
            Case ParametrosSistema.TipoComisionServicio.Mixto
                liComisionFija.Visible = True
                liComisionMixta.Visible = True
                liComisionPorcentaje.Visible = True
                liMontoLimitePorcent.Visible = True
                liDscFija.Visible = True
                liDscPorcentaje.Visible = True
                liDscMixta.Visible = True
                liDscMontoLimitePorcent.Visible = True
                ulMensaje.Visible = True
                ltlMensaje.Text = "<ul><li><b>(*)  Comisi�n por OP:</b> es el monto fijo de comisi�n que se cobrar� por cada transacci�n</li>" & _
                    "<li><b>(*)  % Comisi�n por OP:</b> es el porcentaje de comisi�n que se aplicar� al monto de cada transacci�n</li>" & _
                    "<li><b>(*)  Monto M�ximo por comisi�n:</b> es el monto maximo de comisi�n que se cobrar� por transacci�n cuando se le aplique la comisi�n por porcentaje</li>" & _
                    "<li><b>(*)  Monto Limite:</b> es el monto que define desde cuando se cobrar� por comisi�n porcentual, mientras no " & _
                    "supere este monto limite se cobrar� una comisi�n por monto fijo</li><ul>"
            Case ParametrosSistema.TipoComisionServicio.Fijo
                liDscFija.Visible = True
                liComisionFija.Visible = True
            Case ParametrosSistema.TipoComisionServicio.Porcentual
                liDscFija.Visible = True
                liComisionFija.Visible = True
                liDscPorcentaje.Visible = True
                liComisionPorcentaje.Visible = True
                liDscMontoLimitePorcent.Visible = True
                liMontoLimitePorcent.Visible = True
                ulMensaje.Visible = True
                ltlMensaje.Text = "<ul><li><b>(*)  Comisi�n por OP:</b> es el monto fijo de comisi�n que se cobrar� por cada transacci�n</li>" & _
                    "<li><b>(*)  % Comisi�n por OP:</b> es el porcentaje de comisi�n que se aplicar� al monto de cada transacci�n</li>" & _
                    "<li><b>(*)  Monto M�ximo por comisi�n:</b> es el monto maximo de comisi�n que se cobrar� por transacci�n cuando se le aplique la comisi�n por porcentaje</li></ul>"
        End Select
    End Sub
End Class
