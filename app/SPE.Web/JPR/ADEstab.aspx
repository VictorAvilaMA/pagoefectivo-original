<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADEstab.aspx.vb" Inherits="JPR_ADEstab" Title="Empresa Contratante" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Establecimiento"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="upnlDatos" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <fieldset>
                    <asp:Panel ID="PnlEmpresa" runat="server">
                        <h4>
                            1. Informaci�n general</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span>C&oacute;digo Establecimiento:
                            </li>
                            <li class="t2">
                                <asp:TextBox ID="txtCodigoEstablecimiento" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                <asp:Label ID="lblIdEstablecimiento" runat="server" Style="display: none; width: auto"
                                    Text=""></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="ValidaCampos"
                                    ErrorMessage="Ingrese el C�digo de Establecimiento." Display="Dynamic" ControlToValidate="txtCodigoEstablecimiento"
                                    Style="float: left;">*</asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span>Descripci&oacute;n: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="normal" MaxLength="100"
                                    __designer:wfdid="w6"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="ValidaCampos"
                                    ErrorMessage="Debe ingresar una descripci�n." ControlToValidate="txtDescripcion">*</asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span>Operador: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlOperador" runat="server" CssClass="neu" __designer:wfdid="w9">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="ValidaCampos"
                                    ErrorMessage="Debe seleccionar el Operador." ControlToValidate="ddlOperador"
                                    Style="float: left;"></asp:RequiredFieldValidator>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span><asp:Label ID="lblEstado" runat="server"
                                Visible="false" Text="Estado:" __designer:wfdid="w1"></asp:Label></li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu" Visible="False" __designer:wfdid="w12">
                                </asp:DropDownList>
                            </li>
                        </ul>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnRegistrar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="Panel1" runat="server">
            <fieldset>
                <h4>
                    2. Comercio</h4>
                <asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span>Nombre: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtComercio" runat="server" CssClass="corta" MaxLength="100" ReadOnly="True"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComercio"
                                    ErrorMessage="Debe seleccionar un Comercio." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
                                <asp:ImageButton ID="ibtnBuscar" runat="server" ImageUrl="~/images/lupa.gif" ToolTip="Buscar Comercio"
                                    OnClick="ibtnBuscar_Click" />&nbsp; </li>
                            <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                            <li class="complet">
                                <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" OnClientClick="return RegistrarEmpresa();"
                                    Text="Registrar" ValidationGroup="ValidaCampos" />
                                <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" OnClientClick="return RegistrarEmpresa();"
                                    Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False" />
                                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return confirm('Esta seguro que desea cancelar este documento? ');"
                                    Text="Cancelar" />
                            </li>
                        </ul>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Label ID="lblidComercio" runat="server" Text="idComercio" Style="display: none;"></asp:Label>
                <cc1:ModalPopupExtender ID="mppComercio" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="ImgHidden" PopupControlID="pnlPopupComercios" TargetControlID="btnPOPup">
                </cc1:ModalPopupExtender>
                <asp:Button ID="btnPOPup" runat="server" Style="display: none;" />
            </fieldset>
        </asp:Panel>
    </div>
    <asp:Panel Style="display: none; background: white; min-height: 0px" ID="pnlPopupComercios"
        runat="server" CssClass="conten_pasos3">
        <asp:UpdatePanel ID="upnlComercios" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar" runat="server" ImageUrl="" CausesValidation="false"
                        OnClick="imgbtnRegresar_Click" AlternateText="X" CssClass="cierra_1"></asp:ImageButton>
                </div>
                <div id="div21" class="divContenedorTitulo">
                    <h4>
                        B�squeda de Comercios
                    </h4>
                </div>
                <asp:ImageButton ID="ImageButton2" ImageUrl="" CausesValidation="false" Visible="true"
                    Style="display: none;" runat="server"></asp:ImageButton>
                <div style="clear: both">
                </div>
                <ul class="datos_cip2">
                    <li class="t1">Nombre Comercial: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtBusComercio" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnBuscarComercio" OnClick="btnBuscarComercio_Click" runat="server"
                            Text="Buscar" CssClass="input_azul" CausesValidation="false"></asp:Button>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" Width="530px" AutoGenerateColumns="False"
                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True"
                    BackColor="White" OnRowCommand="gvResultado_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="idComercio"></asp:BoundField>
                        <asp:BoundField DataField="razonSocial" HeaderText="Razon Social"></asp:BoundField>
                        <asp:BoundField DataField="ruc" HeaderText="RUC"></asp:BoundField>
                        <asp:BoundField DataField="contacto" HeaderText="Contacto"></asp:BoundField>
                        <asp:TemplateField HeaderText="Ver">
                            <ItemTemplate>
                                <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                    ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/ok2.jpg" %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--<asp:CommandField SelectImageUrl="~/images/ok2.jpg" SelectText="Seleccionar" ShowSelectButton="True"
                            ButtonType="Image"></asp:CommandField>--%>
                    </Columns>
                    <EmptyDataTemplate>
                        No se encontraron Registros
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                </asp:GridView>
                <asp:ImageButton ID="ImgHidden" ImageUrl="" CausesValidation="false" Visible="true"
                    Style="display: none;" runat="server"></asp:ImageButton>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
