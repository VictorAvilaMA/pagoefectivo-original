Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Partial Class JPR_ADEstab
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CEstablecimiento)

    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id")
    End Sub

    Protected Function CreateBEEstablecimiento(ByVal obeEstablecimiento As SPE.Entidades.BEEstablecimiento) As SPE.Entidades.BEEstablecimiento

        obeEstablecimiento = New SPE.Entidades.BEEstablecimiento
        obeEstablecimiento.SerieTerminal = txtCodigoEstablecimiento.Text.Trim()
        obeEstablecimiento.Descripcion = txtDescripcion.Text
        obeEstablecimiento.IdOperador = CInt(ddlOperador.SelectedValue)
        obeEstablecimiento.IdUsuarioCreacion = UserInfo.IdUsuario
        obeEstablecimiento.idComercio = lblidComercio.Text
        obeEstablecimiento.IdEstado = CInt(ddlEstado.SelectedValue)
        Return obeEstablecimiento

    End Function

    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Return CreateBEEstablecimiento(be)

    End Function

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim beEstablecimiento As BEEstablecimiento = CreateBEEstablecimiento(be)
        beEstablecimiento.IdEstablecimiento = CInt(lblIdEstablecimiento.Text)
        Return beEstablecimiento

    End Function

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase

    '    Return New SPE.Web.CEstablecimiento

    'End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)

        Dim obeEstablecimiento As SPE.Entidades.BEEstablecimiento = CType(be, SPE.Entidades.BEEstablecimiento)
        txtCodigoEstablecimiento.Text = obeEstablecimiento.SerieTerminal
        ddlOperador.SelectedValue = obeEstablecimiento.IdOperador
        txtDescripcion.Text = obeEstablecimiento.Descripcion
        lblIdEstablecimiento.Text = obeEstablecimiento.IdEstablecimiento
        txtComercio.Text = obeEstablecimiento.DescripcionComercio
        lblidComercio.Text = obeEstablecimiento.CodigoComercio


        ddlEstado.SelectedValue = obeEstablecimiento.IdEstado


        'OCULTAR CONTROLES
        lblEstado.Visible = True
        ddlEstado.Visible = True
        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        lblProceso.Text = "Actualizar Establecimiento"


    End Sub

    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADEstab"
        e.URLPageCancelForEdit = "COEstab.aspx"
        e.URLPageCancelForInsert = "COEstab.aspx"

    End Sub


    Private Sub CargaOperadores()

        Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlOperador, objCEstablecimiento.ObtenerOperadores(), "Descripcion", "IdOperador", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            '  Page.SetFocus(txtRazonSocial)

            CargaOperadores()
            '   ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo

        End If

    End Sub



    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Sub OnAfterInsert()


        PnlEmpresa.Enabled = False

        If valueRegister = 5 Then
            ThrowErrorMessage("El c�digo de Establecimiento ya existe.")
            PnlEmpresa.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager1.SetFocus(txtCodigoEstablecimiento)
        Else

            ControlesMantenimiento(False)
            BtnCancel.Text = "Regresar"
            PnlEmpresa.Enabled = False
            btnActualizar.Enabled = False
        End If

    End Sub

    Public Overrides Sub OnAfterUpdate()

        PnlEmpresa.Enabled = False


        If valueUpdated = 5 Then
            ThrowErrorMessage("El c�digo de Establecimiento ya existe.")
            PnlEmpresa.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager1.SetFocus(txtCodigoEstablecimiento)

        Else
            ControlesMantenimiento(False)
            BtnCancel.Text = "Regresar"
            PnlEmpresa.Enabled = False
            btnActualizar.Enabled = False
        End If



    End Sub

    Private Sub ControlesMantenimiento(ByVal Habilitar As Boolean)
        OcultarControles(Habilitar)
        btnActualizar.Enabled = Habilitar
        btnRegistrar.Enabled = Habilitar

    End Sub

    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)

        If Not IsPostBack Then
            If (PageManager.IsEditing) Then
                lblProceso.Text = "Actualizar Establecimiento"
            Else
                lblProceso.Text = "Registrar Establecimiento"
            End If
        End If

        Page.Title = "PagoEfectivo - " + lblProceso.Text
    End Sub

    Private Sub OcultarControles(ByVal flag As Boolean)
        txtCodigoEstablecimiento.Enabled = flag
        ibtnBuscar.Enabled = flag
        ddlOperador.Enabled = flag
        txtDescripcion.Enabled = flag
        txtComercio.Enabled = flag
        ddlEstado.Enabled = flag

    End Sub
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        '
        If Not e Is Nothing Then
            txtComercio.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument)).Cells(1).Text
            lblidComercio.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).Text
            gvResultado.DataSource = Nothing
            gvResultado.DataBind()
            txtBusComercio.Text = String.Empty
            mppComercio.Hide()
        End If
        '
    End Sub

    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        mppComercio.Hide()
    End Sub

    Protected Sub btnBuscarComercio_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ocComercio As New SPE.Web.CComercio()
        Dim obeComercio As New BEComercio

        obeComercio.razonSocial = txtBusComercio.Text

        'SPE.Web.Util.UtilPopPupSearchGridView.VisualizarGridView(gvResultado, ocRepresentante.GetListByParameters("", obeEmpresa), Nothing)
        gvResultado.DataSource = ocComercio.GetListByParameters("", obeComercio)
        gvResultado.DataBind()
        ' OcultarDDLs(False)
    End Sub


    Protected Sub ibtnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        mppComercio.Show()

    End Sub
End Class
