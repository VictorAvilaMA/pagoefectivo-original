<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="ADOper.aspx.vb" Inherits="ADM_ADOper" Title="PagoEfectivo " %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h1>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar Operador"></asp:Literal>
    </h1>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" class="conten_pasos3">
        <ContentTemplate>
            <asp:Panel ID="pnlPage" runat="server">
                <h4>
                    1. Informaci�n general</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> C�digo :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="11"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCodigo" runat="server" ValidationGroup="IsNullOrEmpty"
                            ErrorMessage="Ingrese el Codigo" ControlToValidate="txtCodigo" ToolTip="Dato necesario"
                            Style="width: auto; float: left ; margin:10px">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rvaCodigo" runat="server" ValidationGroup="IsNullOrEmpty"
                            ErrorMessage="Ingrese solo n�meros" ControlToValidate="txtCodigo" MaximumValue="100000000000"
                            MinimumValue="0" Type="Double" Style="width: auto; float: left; margin: 10px">*</asp:RangeValidator>
                        <asp:Label ID="lblIdOperador" runat="server" Style="display: none; width: auto; float: left"
                            Text=""></asp:Label>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Descripci&oacute;n :</li>
                    <li class="t2">
                        <asp:TextBox ID="txtDescripcion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescripcion" runat="server" ValidationGroup="IsNullOrEmpty"
                            Style="width: auto; float: left; margin: 10px" ErrorMessage="Ingrese la Descripcion"
                            ControlToValidate="txtDescripcion" ToolTip="Dato necesario">*</asp:RequiredFieldValidator>
                    </li>
                    <fieldset id="fisEstado" class="ContenedorEtiquetaTexto stripe" runat="server" visible="false">
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                    </fieldset>
                    <div style="clear: both;">
                    </div>
                    <asp:ValidationSummary ID="vsmMessage" runat="server" ValidationGroup="IsNullOrEmpty"
                        Style="padding-left: 200px"></asp:ValidationSummary>
                    <div style="clear: both;">
                    </div>
                    <li class="complet">
                        <asp:Label ID="lblTransaccion" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Registrar"
                            OnClientClick="return ConfirmMe();" OnClick="btnRegistrar_Click"></asp:Button>
                        <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul3" Text="Actualizar"
                            OnClientClick="return ConfirmMe();" OnClick="btnActualizar_Click"></asp:Button>
                        <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Cancelar"
                            OnClientClick="return confirm('�Est� Ud. seguro que desea cancelar la operaci�n? ');"
                            OnClick="btnCancelar_Click"></asp:Button>
                    </li>
                </ul>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
