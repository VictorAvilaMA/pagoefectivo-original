Imports System.Data
Imports SPE.Entidades

Partial Class ADM_ADOper
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.COperador)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub


    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblTransaccion
        End Get
    End Property

    Protected Function CreateBEOperador(ByVal obeOperador As SPE.Entidades.BEOperador) As SPE.Entidades.BEOperador

        obeOperador = New SPE.Entidades.BEOperador

        obeOperador.Codigo = txtCodigo.Text
        obeOperador.Descripcion = txtDescripcion.Text
        obeOperador.IdEstado = CInt(ddlEstado.SelectedValue)
        obeOperador.IdUsuarioCreacion = UserInfo.IdUsuario
        Return obeOperador

    End Function

    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Return CreateBEOperador(be)

    End Function

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeOperador As BEOperador = CreateBEOperador(be)
        obeOperador.IdOperador = Convert.ToInt32(lblIdOperador.Text)
        obeOperador.IdUsuarioActualizacion = UserInfo.IdUsuario
        Return obeOperador

    End Function

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase

    '    Return New SPE.Web.COperador

    'End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeOperador As SPE.Entidades.BEOperador = CType(be, SPE.Entidades.BEOperador)
        txtCodigo.Text = obeOperador.Codigo
        txtDescripcion.Text = obeOperador.Descripcion
        lblIdOperador.Text = obeOperador.IdOperador
        ddlEstado.SelectedValue = obeOperador.IdEstado

        'Activar controles
        fisEstado.Visible = True
        'lblEstado.Visible = True
        'ddlEstado.Visible = True
        lblProceso.Text = "Actualizar Operador"
    End Sub
    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
        pnlPage.Enabled = True
    End Sub
    Public Overrides Sub OnAfterInsert()
        pnlPage.Enabled = False
        btnRegistrar.Enabled = False
        If valueRegister = 1 Then
            ThrowErrorMessage(SPE.EmsambladoComun.ParametrosSistema.MensajeExisteOperador)
            pnlPage.Enabled = True
            btnRegistrar.Enabled = True
            ScriptManager.SetFocus(txtCodigo)
        End If

    End Sub

    Public Overrides Sub OnAfterUpdate()
        pnlPage.Enabled = False
        btnActualizar.Enabled = False
    End Sub
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADOper"
        e.URLPageCancelForEdit = "COOper.aspx"
        e.URLPageCancelForInsert = "COOper.aspx"

    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            Page.SetFocus(txtCodigo)
            CargarComboEstado()
        End If
    End Sub
    ''CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarComboEstado()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESMA") : ddlEstado.DataBind()
        '
    End Sub


    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

   
    Protected Sub btnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - " + Me.lblProceso.Text
    End Sub
End Class
