<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COAgBa.aspx.vb" Inherits="ADM_COAgBa" Title="PagoEfectivo - Consultar Agencia Bancaria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Agencia Bancaria
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B�squeda
        </h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt;</span>C&oacute;digo : </li>
                    <li class="t2">
                       <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Descripci&oacute;n: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtDescripcion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                         <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                  <cc1:FilteredTextBoxExtender ID="FTBE_txtDescripcion" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'- "  TargetControlID="txtDescripcion">
                              </cc1:FilteredTextBoxExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ul>
        <div style="clear: both;">
        </div>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" PostBackUrl="~/JPR/ADAgBa.aspx" />
                 <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
            </li>
        </ul>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
         <div style="clear:both;"></div>
      
        <div class="result">
          <asp:UpdatePanel ID="upResultadoMensaje" runat="server">
            <ContentTemplate>
              
            </ContentTemplate>
        </asp:UpdatePanel>
            <asp:UpdatePanel ID="upResultado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h5 id="h5Resultado" runat="server">
                    <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                </h5>
                    <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                        AutoGenerateColumns="False" AllowPaging="True">
                        <Columns>
                          <asp:BoundField DataField="IdAgenciaBancaria" HeaderText="IdAgencia" Visible="False"></asp:BoundField>
                                <asp:BoundField DataField="Codigo" HeaderText="C&#243;digo" SortExpression="Codigo"></asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion"></asp:BoundField>
                                <asp:BoundField DataField="Direccion" HeaderText="Direcci&#243;n" SortExpression="Direccion"></asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado"></asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton id="ibtnActualizar" runat="server" PostBackUrl="~/JPR/ADAgBa.aspx"  ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' ToolTip="Actualizar" CommandName="Actualizar_Command" OnCommand="Actualizar_Command" CommandArgument='<%# Eval("IdAgenciaBancaria") %>'></asp:ImageButton>   
                                    </ItemTemplate>
                                </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros.</EmptyDataTemplate>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
