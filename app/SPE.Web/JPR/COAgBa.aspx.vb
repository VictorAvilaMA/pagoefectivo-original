Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class ADM_COAgBa
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAgenciaBancaria)
    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Protected Overrides ReadOnly Property FlagPager As Boolean
        Get
            Return True
        End Get
    End Property
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtDescripcion)
            CargarCombos()
            h5Resultado.Visible = False
        End If
        '
    End Sub

    'BUSQUEDA DE AGENCIAS
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        '
        'ListarAgencias()
        '
    End Sub
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeCliente As New SPE.Entidades.BECliente
        Dim obeAgencia As New BEAgenciaBancaria
        Try
            Dim listAgencia As New List(Of BEAgenciaBancaria)
            obeAgencia.Descripcion = txtDescripcion.Text.Trim()
            obeAgencia.Codigo = txtCodigo.Text.Trim()
            If ddlEstado.SelectedIndex = 0 Then
                obeAgencia.IdEstado = 0
            Else
                obeAgencia.IdEstado = ddlEstado.SelectedValue
            End If

            If Not listAgencia Is Nothing Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listAgencia.Count)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If
            obeAgencia.PageNumber = PageNumber
            obeAgencia.PageSize = gvResultado.PageSize
            obeAgencia.TipoOrder = True
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        Return obeAgencia
    End Function
    'LISTAR AGENCIAS

    Public Overrides Function GetMethodSearchByParameters(be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Return CType(ControllerObject, SPE.Web.CAgenciaBancaria).ConsultarAgenciaBancaria(CType(be, BEAgenciaBancaria))
    End Function
    Private Sub ListarAgencias()
        '
        'Try
        '    Dim objCAgenciaBancaria As New SPE.Web.CAgenciaBancaria
        '    Dim obeAgencia As New BEAgenciaBancaria
        '    Dim listAgencia As New List(Of BEAgenciaBancaria)
        '    obeAgencia.Descripcion = txtDescripcion.Text.Trim()
        '    obeAgencia.Codigo = txtCodigo.Text.Trim()
        '    If ddlEstado.SelectedIndex = 0 Then
        '        obeAgencia.IdEstado = 0
        '    Else
        '        obeAgencia.IdEstado = ddlEstado.SelectedValue
        '    End If

        '    obeAgencia.OrderBy = SortExpression
        '    obeAgencia.IsAccending = SortDir
        '    h5Resultado.Visible = True
        '    '
        '    'obeAgencia.OrderBy = SortExpression
        '    listAgencia = objCAgenciaBancaria.ConsultarAgenciaBancaria(obeAgencia)
        '    gvResultado.DataSource = listAgencia
        '    gvResultado.DataBind()

        '    If Not listAgencia Is Nothing Then
        '        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listAgencia.Count)
        '    Else
        '        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        '    End If

        '    '
        'Catch ex As Exception
        '    _3Dev.FW.Web.Log.Logger.LogException(ex)
        '    Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        'End Try
        '
    End Sub

    'ACTUALIZAR AGENTE
    Protected Sub Actualizar_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Context.Items.Add("IdAgenciaBancaria", Convert.ToInt32(e.CommandArgument.ToString()))
        '
    End Sub

    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        '
        ddlEstado.DataTextField = "Descripcion" : ddlEstado.DataValueField = "Id" : ddlEstado.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("ESAB") : ddlEstado.DataBind() : ddlEstado.Items.Insert(0, "::: Todos :::")
        '
    End Sub

    'LIMPIAR  
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
    Public Overrides Sub OnClear()
        txtDescripcion.Text = String.Empty
        txtCodigo.Text = String.Empty
        ddlEstado.SelectedIndex = 0
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        h5Resultado.Visible = False

        MyBase.OnClear()
    End Sub
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        'h5Resultado.Visible = True
        'If Not ResultList Is Nothing Then
        '    'upd <PeruCom FaseIII>
        '    lblResultado.Text = ObtenerMensajeResultado(ResultList.Count)
        'Else
        '    lblResultado.Text = ObtenerMensajeResultado(0)
        'End If
    End Sub
    Private Function ObtenerMensajeResultado(ByVal cantidad As Integer) As String
        Dim strResultado As New StringBuilder()
        strResultado.Append(String.Format("Resultados: {0} registro(s) ", cantidad))
        'If (ddlServicio.SelectedIndex > 0) Then
        '    strResultado.Append(String.Format("para el servicio {0} ", ddlServicio.SelectedItem.Text))
        'End If
        'strResultado.AppendLine(String.Format("entre el {0} y {1} ", txtFechaInicio.Text.TrimEnd(), txtFechaFin.Text.TrimEnd()))
        Return strResultado.ToString()
    End Function
    'PAGINADO DE LA GRILLA
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'gvResultado.PageIndex = e.NewPageIndex
        'ListarAgencias()
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        '"DescripcionEstado", _
        '"Modificar", _
        'SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoAgenciaBancaria.Modificar, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    End Sub

    Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        ''
        'SortExpression = e.SortExpression
        'If (SortDir.Equals(SortDirection.Ascending)) Then
        '    SortDir = SortDirection.Descending
        'Else
        '    SortDir = SortDirection.Ascending
        'End If
        'ListarAgencias()
        '
    End Sub

End Class
