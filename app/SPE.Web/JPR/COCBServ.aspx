<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCBServ.aspx.vb" Inherits="ADM_COAgRe" Title="PagoEfectivo - Consultar Agencia Recaudadora" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Asociaci�n de Cuentas Bancarias por Servicio
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B�squeda
        </h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt;</span> Empresa Contratante: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu" DataValueField='""'
                            DataTextField="::Seleccione::">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Banco: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Moneda: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ul>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        <div class="result">
            <asp:UpdatePanel ID="upResultado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text="Registros:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                            AutoGenerateColumns="False" DataKeyNames="IdServicioBanco" AllowPaging="True">
                            <Columns>
                                <asp:BoundField DataField="DescripcionEmpresa" HeaderText="Empresa" SortExpression="RazonSocial">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionServicio" HeaderText="Servicio" SortExpression="servicio">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionBanco" HeaderText="Banco" SortExpression="banco">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionMoneda" HeaderText="Moneda" SortExpression="moneda">
                                </asp:BoundField>
                                <asp:BoundField DataField="CodigoServicio" HeaderText="Codigo" SortExpression="Codigo">
                                </asp:BoundField>
                                <asp:BoundField DataField="NumeroCuenta" HeaderText="Nro.Cuenta" SortExpression="NroCuenta">
                                </asp:BoundField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADCBServ.aspx" runat="server"
                                            ToolTip="Actualizar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtndelete" CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                            CommandName="Delete" CausesValidation="false" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/borrar.jpg" %>'
                                            ToolTip="Eliminar" />
                                        <asp:HiddenField ID="hdfIdAsocia" runat="server" Value='<%# Eval("IdServicioBanco") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="right" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.</EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
