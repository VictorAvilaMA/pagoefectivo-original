Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic
Imports SPE.Web
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class ADM_COAgRe
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)
    Protected Overrides ReadOnly Property BtnSearch As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnNew As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Public Overrides Function AllowToDelete(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As Boolean
        Return False
    End Function
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As BusinessEntityBase) As System.Collections.Generic.List(Of BusinessEntityBase)
        Dim ctrlServicio As New CServicio
        Return ctrlServicio.ConsultarAsociacionCuentaServicio(be)
    End Function
    Public Overrides Function OnMainDelete(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As _3Dev.FW.Entidades.BusinessMessageBase

    End Function
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing And ResultList.Count() > 0 Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, ResultList(0).TotalPageNumbers)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        End If
        divResult.Visible = True
    End Sub
    Public Overrides Function CreateBusinessEntityForSearch() As BusinessEntityBase
        Dim oBEServicioBanco As New BEServicioBanco
        oBEServicioBanco.idEmpresaContratante = If(ddlEmpresa.SelectedValue = String.Empty, 0, CInt(ddlEmpresa.SelectedValue))
        oBEServicioBanco.idServicio = If(ddlServicio.SelectedValue = String.Empty, 0, CInt(ddlServicio.SelectedValue))
        oBEServicioBanco.IdBanco = If(ddlBanco.SelectedValue = String.Empty, 0, CInt(ddlBanco.SelectedValue))
        oBEServicioBanco.IdMoneda = If(ddlMoneda.SelectedValue = String.Empty, 0, CInt(ddlMoneda.SelectedValue))
        oBEServicioBanco.IdEstado = If(ddlEstado.SelectedValue = String.Empty, 0, CInt(ddlEstado.SelectedValue))
        Return (oBEServicioBanco)
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim oBEBanco As New BEBanco
            Dim oCntrBanco As New CBanco
            Dim oCntrPlantilla As New CPlantilla()
            Dim oCntrParametro As New CAdministrarParametro()
            Dim oBEEmpresaContratante As New BEEmpresaContratante()
            Dim oCntrAdministracionComun As New CAdministrarComun()
            Using ocntrlEmpresaContrante As New CEmpresaContratante()
                oBEEmpresaContratante.IdEstado = EstadoMantenimiento.Activo
                DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Todos::")
                DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Todos::")
            End Using
            oBEBanco.IdEstado = EstadoBanco.Activo
            DropDownlistBinding(ddlBanco, oCntrBanco.ConsultarBanco(oBEBanco), "Descripcion", "IdBanco", "::Todos::")
            DropDownlistBinding(ddlMoneda, oCntrAdministracionComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Todos::")
            DropDownlistBinding(ddlEstado, oCntrParametro.ConsultarParametroPorCodigoGrupo("ESMA"), "Descripcion", "Id", "::Todos::")
        End If


    End Sub
    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        If ddlEmpresa.SelectedIndex = 0 Then
            DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Todos::")
            Return
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            objbeservicio.IdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
            objbeservicio.IdEstado = EstadoMantenimiento.Activo
            DropDownlistBinding(ddlServicio, ocntrlServicio.SearchByParameters(objbeservicio), "Nombre", "IdServicio", "::Todos::")
        End Using
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("ADCBServ.aspx")
    End Sub

    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand

        Select Case e.CommandName
            Case "Delete"
                Dim reg As Integer
                Dim idAs As String
                reg = gvResultado.Rows(e.CommandArgument).RowIndex
                idAs = CType(gvResultado.Rows(reg).FindControl("hdfIdAsocia"), HiddenField).Value
                Dim oBEServicioBanco As New BEServicioBanco
                Dim oCServicio As New CServicio
                oBEServicioBanco.IdServicioBanco = idAs
                Dim oBEServicioBancoResponse As BEServicioBanco = oCServicio.ObtenerAsociacionCuentaServicio(oBEServicioBanco)
                Dim CtrlServicio As New CServicio
                oBEServicioBancoResponse.IdEstado = EstadoMantenimiento.Inactivo
                oBEServicioBancoResponse.IdUsuarioActualizacion = UserInfo.IdUsuario
                CtrlServicio.ActualizarAsociacionCuentaServicio(oBEServicioBancoResponse)
                Dim oSB As New BEServicioBanco
                oSB.idEmpresaContratante = If(ddlEmpresa.SelectedValue = String.Empty, 0, CInt(ddlEmpresa.SelectedValue))
                oSB.idServicio = If(ddlServicio.SelectedValue = String.Empty, 0, CInt(ddlServicio.SelectedValue))
                oSB.IdBanco = If(ddlBanco.SelectedValue = String.Empty, 0, CInt(ddlBanco.SelectedValue))
                oSB.IdMoneda = If(ddlMoneda.SelectedValue = String.Empty, 0, CInt(ddlMoneda.SelectedValue))
                oSB.IdEstado = If(ddlEstado.SelectedValue = String.Empty, 0, CInt(ddlEstado.SelectedValue))
                oSB.PageNumber = PageNumber
                oSB.PageSize = GridViewResult.PageSize
                gvResultado.DataSource = CtrlServicio.ConsultarAsociacionCuentaServicio(oSB)
                gvResultado.DataBind()
        End Select
    End Sub

End Class
