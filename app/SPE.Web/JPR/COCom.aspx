<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COCom.aspx.vb" Inherits="JPR_COCom" Title="PagoEfectivo - Consultar Comercio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="colRight">
        <asp:ScriptManager ID="ScriptManager" runat="server">
        </asp:ScriptManager>
        <h2>
            Consultar Comercio
        </h2>
        <div class="conten_pasos3">
            <h4>
                Criterios de B�squeda
            </h4>
            <ul class="datos_cip2">
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo Comercio: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtCodigo" runat="server" CssClass="neu"> 
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Raz&oacute;n Social: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="neu"> 
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> R.U.C.: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtRUC" runat="server" CssClass="neu"> 
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Contacto: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtContacto" runat="server" CssClass="neu"> 
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                        <li class="t2">
                            <asp:DropDownList ID="ddlIdEstado" runat="server" CssClass="neu">
                            </asp:DropDownList>
                        </li>
                        <cc1:FilteredTextBoxExtender ID="FTBE_Ruc" runat="server" ValidChars="0123456789"
                            TargetControlID="txtRUC">
                        </cc1:FilteredTextBoxExtender>
                        <cc1:FilteredTextBoxExtender ID="FTBE_TXTCONTACTO" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'& "
                            TargetControlID="txtContacto">
                        </cc1:FilteredTextBoxExtender>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </ul>
            <div style="clear: both;">
            </div>
            <ul id="Ul1" class="datos_cip2 h0">
                <li class="complet">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                    <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" />
                </li>
            </ul>
            <div style="clear: both;">
            </div>
            <div class="result">
                <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="div7">
                            <h5 id="h5Resultado" runat="server">
                                <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                            </h5>
                        </div>
                        <div id="divOrdenesPago" class="divSubContenedorGrilla">
                            <asp:GridView ID="gvComercio" runat="server" CssClass="grilla" AutoGenerateColumns="False"
                                AllowPaging="True" DataKeyNames="IdComercio" AllowSorting="true">
                                <Columns>
                                    <asp:BoundField DataField="RazonSocial" SortExpression="RazonSocial" HeaderText="Raz&#243;n Social" />
                                    <asp:BoundField DataField="Contacto" SortExpression="Contacto" HeaderText="Contacto" />
                                    <asp:BoundField DataField="Ruc" SortExpression="Ruc" HeaderText="R.U.C.">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Telefono" SortExpression="Telefono" HeaderText="Tel&#233;fono">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                        HeaderText="Estado">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgActualizar" PostBackUrl="ADCom.aspx" CommandName="Edit" runat="server"
                                                ImageUrl="~/images/lupa.gif" ToolTip="Actualizar" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IdComercio" HeaderText="IdComercio" Visible="false" />
                                </Columns>
                                <HeaderStyle CssClass="cabecera" />
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
