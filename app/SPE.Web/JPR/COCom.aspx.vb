
Partial Class JPR_COCom
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CComercio)
    'Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtRazonSocial)
        End If
        h5Resultado.Visible = False
        '
    End Sub
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvComercio
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeComercio As New SPE.Entidades.BEComercio
        obeComercio.razonSocial = txtRazonSocial.Text
        obeComercio.ruc = txtRUC.Text
        obeComercio.contacto = txtContacto.Text
        obeComercio.codComercio = txtCodigo.Text

        If ddlIdEstado.SelectedValue.ToString = "" Then
            obeComercio.idEstado = 0
        Else
            obeComercio.idEstado = CInt(ddlIdEstado.SelectedValue)
        End If


        Return obeComercio

    End Function
    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADCom"
        e.MaintenancePageName = "ADCom.aspx"
        e.ExecuteSearchOnFirstLoad = False

    End Sub

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        h5Resultado.Visible = True
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If

    End Sub

    Public Overrides Sub OnFirstLoadPage()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", "::: Todos :::")

        'Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        '_3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlOperador, objCEstablecimiento.ObtenerOperadores(), "Descripcion", "IdOperador", "::: Todos :::")

    End Sub

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CComercio
    'End Function

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvComercio.DataKeys(e.RowIndex).Values(0)
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        h5Resultado.Visible = False
        txtCodigo.Text = String.Empty
        txtContacto.Text = String.Empty
        txtRazonSocial.Text = String.Empty
        txtRUC.Text = String.Empty
        gvComercio.DataSource = Nothing
        gvComercio.DataBind()

    End Sub
End Class
