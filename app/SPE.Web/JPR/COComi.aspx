<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COComi.aspx.vb" Inherits="ADM_COServ" Title="Consultar Comisiones por Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Comisiones por Servicio</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> C�digo: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="normal" MaxLength="6"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Empresa Contratante: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmpresaContratante" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Nombre de servicio: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreServicio" MaxLength="100" runat="server" CssClass="normal"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">>></span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="dropEstado" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> Moneda: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMoneda" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> Tipo Comision: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlTipoComision" CssClass="normal" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" Text="Buscar" />
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
                <div class="cont-panel-search">
                    <cc1:FilteredTextBoxExtender ID="FTBE_txtNombreServicio" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&.- "
                        TargetControlID="txtNombreServicio">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="FTBE_TxtEmpresaContratante" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz'&.- "
                        TargetControlID="txtEmpresaContratante">
                    </cc1:FilteredTextBoxExtender>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="True" runat="server" CssClass="grilla"
                            AutoGenerateColumns="False" DataKeyNames="IdServicio,idMoneda,IdEstadoComision"
                            AllowPaging="True" OnRowDataBound="gvResultado_RowDataBound">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADComi.aspx" runat="server"
                                            ToolTip="Actualizar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="NombreEmpresaContrante" SortExpression="NombreEmpresaContrante"
                                    HeaderText="Emp. Contratante"></asp:BoundField>
                                <asp:BoundField DataField="Codigo" SortExpression="Codigo" HeaderText="Cod. Serv">
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" SortExpression="Nombre" HeaderText="Servicio">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionMoneda" SortExpression="Moneda" HeaderText="Moneda">
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoComision" SortExpression="TipoComision" HeaderText="Tipo Comision"
                                    ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Valor Fijo" SortExpression="MontoComision">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "MontoComision"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                                <%--  
                                <asp:BoundField DataField="MontoComision" SortExpression="MontoComision"
                                HeaderText="Valor de Comision" ItemStyle-HorizontalAlign="Center" ></asp:BoundField>--%>
                                <asp:TemplateField HeaderText="Valor Porc" SortExpression="PorcentajeComision">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "PorcentajeComision"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                    HeaderText="Estado" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaCreacion" SortExpression="FechaCracionComision" HeaderText="Fec.Creacion"
                                    ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Fec.Actualizacion" SortExpression="FechaActualizacionComision">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaActualizacion"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
