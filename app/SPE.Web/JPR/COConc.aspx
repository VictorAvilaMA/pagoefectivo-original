<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="COConc.aspx.vb" Inherits="COConc" title="PagoEfectivo - Consultar Conciliaciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager id="ScriptManager" runat="server" EnableScriptLocalization="True" EnableScriptGlobalization="True"></asp:ScriptManager>
    <div id="Page1" >
        <h1>Consultar Conciliaciones</h1>
        <div id="div50"  >
            <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo">Criterios de b�squeda</div>
                <div id="divCriteriosBusqueda">
                    <asp:UpdatePanel runat="server" id="UpdatePanelFiltros" UpdateMode="Conditional">
                        <contenttemplate>
<DIV id="divCriterios" class="divSubContenedorCriteriosBusqueda"><FIELDSET class="ContenedorEtiquetaTexto "><DIV id="div3" class="EtiquetaTextoIzquierda"><asp:Label id="lblNroOperacion" runat="server" CssClass="Etiqueta" Text="Nro Operaci�n:"></asp:Label> <asp:TextBox id="txtNroOperacion" runat="server" CssClass="Texto"></asp:TextBox> </DIV><DIV id="div4" class="EtiquetaTextoIzquierda"><asp:Label id="lblNroOrdenPago" runat="server" CssClass="Etiqueta" Text="C.I.P."></asp:Label> <asp:TextBox id="txtNroOrdenPago" runat="server" CssClass="Texto"></asp:TextBox> </DIV></FIELDSET> <FIELDSET class="ContenedorEtiquetaTexto stripe"><DIV id="div6" class="EtiquetaTextoIzquierda"><asp:Label id="lblEstado" runat="server" CssClass="Etiqueta" Text="Estado:"></asp:Label> <asp:DropDownList id="ddlEstado" runat="server" CssClass="TextoComboLargo"></asp:DropDownList> </DIV></FIELDSET> <FIELDSET class="ContenedorEtiquetaTexto"><DIV id="div1" class="EtiquetaTextoIzquierda"><asp:Label id="lblFechaDe" runat="server" CssClass="Etiqueta" Text="Del :" Width="57px"></asp:Label> <asp:TextBox id="txtFechaDe" runat="server" CssClass="TextoCalendar"></asp:TextBox> <asp:ImageButton id="ibtnFechaDe" runat="server" ImageUrl="~/Images_Buton/date.gif"></asp:ImageButton>&nbsp; <cc1:MaskedEditValidator id="MaskedEditValidatorDel" runat="server" IsValidEmpty="False" InvalidValueMessage="Fecha 'Del' no v�lida" InvalidValueBlurredMessage="*" ErrorMessage="*" EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe" ControlExtender="mdeDel"></cc1:MaskedEditValidator></DIV><DIV id="div7" class="EtiquetaTextoIzquierda" ><asp:Label id="lblFechaA" runat="server" CssClass="Etiqueta" Text="Al:" Width="36px"></asp:Label> <asp:TextBox id="txtFechaA" runat="server" CssClass="TextoCalendar"></asp:TextBox> <asp:ImageButton id="ibtnFechaA" runat="server" ImageUrl="~/Images_Buton/date.gif"></asp:ImageButton>&nbsp; <cc1:MaskedEditValidator id="MaskedEditValidatorAl" runat="server" IsValidEmpty="False" InvalidValueMessage="Fecha 'Al' no v�lida" InvalidValueBlurredMessage="*" EmptyValueMessage="Fecha 'Al' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaA" ControlExtender="mdeA">*</cc1:MaskedEditValidator> <asp:CompareValidator id="CompareValidator1" runat="server" ErrorMessage="El Rango de fechas no es v�lido" ControlToValidate="txtFechaA" Type="Date" Operator="GreaterThanEqual" ControlToCompare="txtFechaDe">*</asp:CompareValidator></DIV></FIELDSET> <asp:ValidationSummary id="ValidationSummary" runat="server"></asp:ValidationSummary> <cc1:CalendarExtender id="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender> <cc1:CalendarExtender id="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender> <cc1:MaskedEditExtender id="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                                </cc1:MaskedEditExtender> <cc1:MaskedEditExtender id="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date" Mask="99/99/9999" CultureName="es-PE">
                                </cc1:MaskedEditExtender> 
    <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" ValidChars="0123456789" runat="server" TargetControlID="txtNroOrdenPago">
    </cc1:FilteredTextBoxExtender>
</DIV>
</contenttemplate>
                        <Triggers>
<asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
</Triggers>
                    </asp:UpdatePanel>
                    <fieldset id="Fieldset2" class="ContenedorBotoneraCriteriosBusqueda" >
                            <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar" Text="Buscar" />
                            <br />
                            <asp:Button ID="btnLimpiar" runat="server" CssClass="btnLimpiar" Text="Limpiar" />
                        </fieldset>
                </div>
              </div>
        &nbsp;&nbsp;
                <div id="divOrdenesPago" class="divSubContenedorGrilla" >
                <asp:UpdatePanel id="UpdatePanel" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                        
                    
                    
                    
                        <div id="div2" class="divContenedorTitulo" ><asp:Label ID="lblResultado" runat="server" Text=""/></div>
                                <asp:GridView id="gvResultado" AllowSorting="True" runat="server" CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging"   >
                   <Columns>
                        <asp:BoundField DataField="NumeroOperacion" SortExpression="NumeroOperacion"  HeaderText="Nro Operacion"></asp:BoundField>
                        <asp:BoundField DataField="NumeroOrdenPago" SortExpression="NumeroOrdenPago"  HeaderText="Cod. Identif. Pago"  ></asp:BoundField>
                        <asp:BoundField DataField="FechaConciliacion"  SortExpression="FechaConciliacion" HeaderText="Fecha Conciliaci&#243;n">
                            <ItemStyle HorizontalAlign="Right" Width="5px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"  HeaderText="Estado Conciliaci�n">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                       <asp:BoundField DataField="Observacion" SortExpression="Observacion" HeaderText="Observacion" />
                   </Columns>
                 <HeaderStyle CssClass="cabecera"></HeaderStyle>
               </asp:GridView>
                        </contenttemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                    </Triggers>
                        
                    </asp:UpdatePanel>
                </div>
            </div>
</asp:Content>

