Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class COConc
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAgenciaBancaria)


    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)

    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Function CrearEntidadParaBusqueda() As BEConciliacion
        Dim objConciliacion As New SPE.Entidades.BEConciliacion
        objConciliacion.NumeroOrdenPago = Me.txtNroOrdenPago.Text
        objConciliacion.OperacionBancaria = Me.txtNroOperacion.Text
        Dim x As Object
        objConciliacion.FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaDe.Text)
        objConciliacion.FechaFin = _3Dev.FW.Util.DataUtil.StringToDateTime(Me.txtFechaA.Text)
        x = ddlEstado.SelectedValue.Trim
        If ddlEstado.SelectedValue.Trim <> "" Then
            objConciliacion.IdEStado = _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue)
        Else
            objConciliacion.IdEStado = 0
        End If

        Return objConciliacion
    End Function
    Protected Sub grdResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Return CrearEntidadParaBusqueda()
    End Function
    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    Sub NumeroOPCompletado(ByVal pNumeroOrdenPago As String)
        If (pNumeroOrdenPago <> "" And pNumeroOrdenPago <> "0") Then
            pNumeroOrdenPago = "0000000000000" + pNumeroOrdenPago
            pNumeroOrdenPago = pNumeroOrdenPago.Substring(pNumeroOrdenPago.Length - 14, 14)
            Me.txtNroOrdenPago.Text = pNumeroOrdenPago
        End If


    End Sub
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property


    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return Me.lblResultado
        End Get
    End Property
    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CAgenciaBancaria
    'End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objCParametros As New SPE.Web.CAdministrarParametro
            WebUtil.DropDownlistBinding(ddlEstado, objCParametros.ConsultarParametroPorCodigoGrupo("ESCO"), "Descripcion", "Id", "::: Seleccione :::")
            txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'DateTime.Now.ToShortDateString()
            txtFechaA.Text = DateTime.Now.ToShortDateString()
        End If
    End Sub
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Limpiar()
    End Sub
    Sub Limpiar()
        Me.txtNroOperacion.Text = ""
        Me.txtNroOrdenPago.Text = ""
        Me.txtFechaA.Text = DateTime.Now.ToShortDateString()
        Me.txtFechaDe.Text = DateTime.Now.ToShortDateString()
        Me.lblResultado.Text = ""
        Me.ddlEstado.SelectedIndex = 0
        Me.gvResultado.DataSource = Nothing
    End Sub

    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvResultado.PageIndex = e.NewPageIndex

    End Sub
    Public Overrides Sub OnAfterSearch()
        NumeroOPCompletado(Me.txtNroOrdenPago.Text)
    End Sub
End Class
