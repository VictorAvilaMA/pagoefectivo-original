﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="CODepo.aspx.vb" Inherits="JPR_CODepo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Depositos Empresas Recaudadoras
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de Búsqueda
        </h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Banco: </li>
            <li class="t2">
                <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                </asp:DropDownList>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Nro de Transacci&oacute;n: </li>
            <li class="t2">
                <asp:TextBox ID="txtNroDeTransaccion" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo de Operaci&oacute;n:
            </li>
            <li class="t2">
                <asp:TextBox ID="txtCodOperacion" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> CIP: </li>
            <li class="t2">
                <asp:TextBox ID="txtCIP" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Fecha dep&oacute;sito desde: </li>
            <li class="t2" style="line-height: 1; z-index: 200;">
                <p class="input_cal">
                    <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                    <asp:ImageButton CssClass="calendario" ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                    </asp:ImageButton>
                    <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                        InvalidValueMessage="Fecha 'Del' no válida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                        EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                        ControlExtender="mdeDel"></cc1:MaskedEditValidator>
                </p>
                <span class="entre">al: </span>
                <p class="input_cal">
                    <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                        Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                        Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                        Text="*"></asp:RequiredFieldValidator>
                    <asp:ImageButton CssClass="calendario" ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                    </asp:ImageButton>
                    <cc1:MaskedEditValidator CssClass="izq" ID="MaskedEditValidatorAl" runat="server"
                        IsValidEmpty="False" InvalidValueMessage="Fecha 'Al' no válida" InvalidValueBlurredMessage="*"
                        EmptyValueMessage="Fecha 'Al' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaA"
                        ControlExtender="mdeA">*
                    </cc1:MaskedEditValidator>
                    <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de búsqueda no es válido"
                        ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                        Type="Date">*</asp:CompareValidator>
                </p>
            </li>
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnExportar" runat="server" CssClass="input_azul4" Text="Exportar Excel" />
            </li>
        </ul>
        <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
            Format="dd/MM/yyyy">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
            Format="dd/MM/yyyy">
        </cc1:CalendarExtender>
        <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
            Mask="99/99/9999" CultureName="es-PE">
        </cc1:MaskedEditExtender>
        <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
            Mask="99/99/9999" CultureName="es-PE">
        </cc1:MaskedEditExtender>
        <div style="clear: both;">
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        <div class="result">
            <asp:UpdatePanel ID="upResultado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text="Registros:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                            OnRowCommand="gvResultado_RowCommand" AutoGenerateColumns="False" DataKeyNames="IdDeposito,IdConciliacionArchivo"
                            AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText ="Ver">
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                               <%-- <asp:CommandField ButtonType="Image" SelectImageUrl ="~/images/lupa.gif" SelectText="Ver"
                                    ShowSelectButton="True" HeaderText="Ver">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>--%>
                                <asp:BoundField DataField="IdDeposito" HeaderText="ID" SortExpression="IdDeposito">
                                </asp:BoundField>
                                <asp:BoundField DataField="CodigoOperacion" HeaderText="Cod. Operacion" SortExpression="CodigoOperacion">
                                </asp:BoundField>
                                <asp:BoundField DataField="NroTransaccion" HeaderText="Nro. Transaccion" SortExpression="NroTransaccion">
                                </asp:BoundField>
                                <asp:BoundField DataField="MontoDepositado" HeaderText="Monto" SortExpression="MontoDepositado">
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaDeposito" HeaderText="Fch. Deposito" DataFormatString="{0:d}" SortExpression="moneda">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionBanco" HeaderText="Banco" SortExpression="DescripcionBanco">
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.</EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="hdfIdDeposito" runat="server" Value="0" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div style="clear: both">
    </div>
    <asp:UpdatePanel ID="upnlListaOperacionDeposito" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc1:ModalPopupExtender ID="mppOperacionesArchivo" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="ImageButton1" PopupControlID="pnlPopupOrdenesArchivo" TargetControlID="ImageButton2"
                Drag="true" Y="0">
            </cc1:ModalPopupExtender>
            <asp:ImageButton ID="ImageButton1" ImageUrl="" CausesValidation="false" Visible="true"
                CssClass="Hidden" runat="server" Style="display: none"></asp:ImageButton>
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' CssClass="Hidden"
                ToolTip="Buscar Empresa" Style="display: none" />
            <asp:Panel ID="pnlPopupOrdenesArchivo" runat="server" Style="display: none; background: white;
                max-height: 500px; min-height: 0px" CssClass="conten_pasos3">
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>' CausesValidation="false"
                        runat="server"></asp:ImageButton>
                </div>
                <div style="clear: both">
                </div>
                <div class="result" style="margin: 10px; overflow: auto; height: 300px">
                    <h5 id="h1" runat="server">
                        <asp:Literal ID="lblResultadoDetalle" runat="server" Text=""></asp:Literal>
                    </h5>
                    <div style="clear: both;">
                    </div>
                    <asp:GridView ID="gvOperaciones" runat="server" CssClass="grilla" AllowPaging="True"
                        AutoGenerateColumns="False" PageSize="10">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdOrdenPago" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdOrdenPago") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagSeleccionado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FlagSeleccionado") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaConciliacion" DataFormatString="{0:d}" SortExpression="Fecha"
                                HeaderText="Fecha">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NumeroOrdenPago" SortExpression="CIP" HeaderText="CIP">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaCancelacion" DataFormatString="{0:d}" SortExpression="FechaCancelacion"
                                HeaderText="Fecha Cancelacion">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ArchivoConciliacion" SortExpression="NombreArchivo" HeaderText="Nombre Archivo">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="Estado" HeaderText="Estado">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Total" HeaderText="Monto" SortExpression="Total"></asp:BoundField>
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron registros
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
