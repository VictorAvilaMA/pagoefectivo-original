﻿Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic
Imports SPE.Web
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports SPE.EmsambladoComun.ParametrosSistema
Imports System.Linq
Imports SPE.EmsambladoComun

Partial Class JPR_CODepo
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CBanco)
#Region "Propiedades"
    Protected Overrides ReadOnly Property BtnSearch As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
#End Region
#Region "Metodos"
    Public Overrides Function AllowToDelete(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As Boolean
        Return False
    End Function
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As BusinessEntityBase) As System.Collections.Generic.List(Of BusinessEntityBase)
        Dim ocBanco As New CBanco
        Return ocBanco.ConsultarDeposito(be)
    End Function
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing And ResultList.Count() > 0 Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, ResultList(0).TotalPageNumbers)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        End If
        If ViewState("listaBanco") IsNot Nothing Then
            Dim listaBanco As List(Of BEBanco) = CType(ViewState("listaBanco"), List(Of BEBanco))
            Dim obeBanco As New BEBanco
            obeBanco = listaBanco.Where(Function(beBanco As BEBanco) beBanco.IdBanco = ddlBanco.SelectedValue).First()
            Select Case obeBanco.Codigo
                Case ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank
                    gvResultado.Columns(2).Visible = False
                Case ParametrosSistema.Conciliacion.CodigoBancos.FullCarga, ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion
                    gvResultado.Columns(3).Visible = False
            End Select
        End If
        divResult.Visible = True
    End Sub
    Public Overrides Sub OnBeforeSearch()
        gvResultado.Columns(3).Visible = True
        gvResultado.Columns(2).Visible = True
        MyBase.OnBeforeSearch()
    End Sub
    Public Overrides Function CreateBusinessEntityForSearch() As BusinessEntityBase
        Dim obeDeposito As New BEDeposito
        obeDeposito.CodigoOperacion = txtCodOperacion.Text
        obeDeposito.NroTransaccion = txtNroDeTransaccion.Text
        obeDeposito.FechaDesde = ObjectToDateTime(txtFechaDe.Text)
        obeDeposito.FechaHasta = ObjectToDateTime(txtFechaA.Text)
        obeDeposito.IdBanco = IIf(ddlBanco.SelectedValue = String.Empty, 0, ddlBanco.SelectedValue)
        obeDeposito.CIP = txtCIP.Text
        Return obeDeposito
    End Function
#End Region
#Region "Eventos"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim oBEBanco As New BEBanco
            Dim oCntrBanco As New CBanco
            oBEBanco.IdEstado = EstadoBanco.Activo
            Dim listaBanco As List(Of SPE.Entidades.BEBanco) = oCntrBanco.ConsultarBanco(oBEBanco).Where(Function(beBanco As BEBanco) _
                                                                                               beBanco.Codigo = ParametrosSistema.Conciliacion.CodigoBancos.Scotiabank Or _
                                                                                               beBanco.Codigo = ParametrosSistema.Conciliacion.CodigoBancos.FullCarga Or _
                                                                                               beBanco.Codigo = ParametrosSistema.Conciliacion.CodigoBancos.WesterUnion).ToList()
            ViewState("listaBanco") = listaBanco
            DropDownlistBinding(ddlBanco, listaBanco, "Descripcion", "IdBanco")
            txtFechaA.Text = DateTime.Now.ToShortDateString()
            txtFechaDe.Text = DateTime.Now.AddMonths(-1).ToShortDateString()
        End If
    End Sub
    Protected Sub gvResultado_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Select"
                Dim indice As Integer = Convert.ToInt32(e.CommandArgument)
                hdfIdDeposito.Value = gvResultado.DataKeys(indice).Values(0)
                ViewState.Remove("listDetalleDeposito")
                CargarDetalleDeposito()
        End Select
    End Sub
    Protected Sub btnExportar_Click(sender As Object, e As System.EventArgs) Handles btnExportar.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            LblMessageMaintenance.Text = "Esta operación puede tardar unos minutos dependiendo del rango de la consulta"
            Dim listaBusinessEntityBase As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Dim listaBEDetDeposito As New List(Of BEDetDeposito)
            Using cBanco As New SPE.Web.CBanco()
                Dim obeDeposito As New BEDeposito
                obeDeposito.CodigoOperacion = txtCodOperacion.Text
                obeDeposito.NroTransaccion = txtNroDeTransaccion.Text
                obeDeposito.FechaDesde = ObjectToDateTime(txtFechaDe.Text)
                obeDeposito.FechaHasta = ObjectToDateTime(txtFechaA.Text)
                obeDeposito.IdBanco = IIf(ddlBanco.SelectedValue = String.Empty, 0, ddlBanco.SelectedValue)
                obeDeposito.CIP = txtCIP.Text
                listaBusinessEntityBase = cBanco.ConsultarDepositoReporte(obeDeposito)
            End Using

            For Each item As BusinessEntityBase In listaBusinessEntityBase
                Dim obeDetDeposito As BEDetDeposito = CType(item, BEDetDeposito)
                obeDetDeposito.Tipo = IIf(obeDetDeposito.CodigoEmpresa = ParametrosSistema.Empresa.Codigo.Comercio, "Interno", "Externo")
                listaBEDetDeposito.Add(obeDetDeposito)
            Next
            For Each item As BEDetDeposito In listaBEDetDeposito
                Dim result As Decimal
                Dim idDeposito As Int64 = item.IdDeposito
                result = (From w In listaBEDetDeposito Where w.IdDeposito = idDeposito Select w.Total).Sum()
                result = result - (From w In listaBEDetDeposito Where w.IdDeposito = idDeposito Select w.Comision).Sum()
                item.EstadoTX = IIf(result = item.MontoDepositado, ParametrosSistema.EstadosTX.Ok, ParametrosSistema.EstadosTX.Pendiente)
            Next
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            UtilReport.ProcesoExportarGenerico(listaBEDetDeposito, Page, "EXCEL", "xls", "Detalle Operaciones Deposito - ", parametros, "RptCODepo.rdlc")
            LblMessageMaintenance.Text = ""
        End If
    End Sub
    Protected Sub gvOperaciones_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOperaciones.PageIndexChanging
        gvOperaciones.PageIndex = e.NewPageIndex
        CargarDetalleDeposito()
    End Sub
#End Region
#Region "Funciones"
    Protected Sub CargarDetalleDeposito()
        Dim cBanco As New CBanco
        Dim oBEDeposito As New BEDeposito
        Dim response As New List(Of BEOrdenPago)
        oBEDeposito.IdDeposito = hdfIdDeposito.Value
        If ViewState("listDetalleDeposito") Is Nothing Then
            response = cBanco.ConsultarDepositoDetalleByIdDeposito(oBEDeposito)
            ViewState("listDetalleDeposito") = response
        End If
        response = CType(ViewState("listDetalleDeposito"), List(Of BEOrdenPago))
        gvOperaciones.DataSource = response
        gvOperaciones.DataBind()

        lblResultadoDetalle.Text = "Se encontraron " & response.Count.ToString() & " registros"
        mppOperacionesArchivo.Show()
    End Sub
#End Region
End Class
