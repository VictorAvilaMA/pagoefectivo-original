﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="CODev.aspx.vb" Inherits="JPR_CODev" Culture="es-PE" UICulture="es-PE" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <style type="text/css">
        .ajax__calendar_footer
        {
            display: none;
        }
        .lupa
        {
            background: url(../images/lupa.gif);
            border: 0px;
            width: 16px;
        }
        .close
        {
            background: url(../img/closeX.GIF);
            border: 0px;
            width: 14px;
            height: 14px;
        }
        .close:hover,.lupa:hover
        {
            cursor:pointer;
        }
    </style>
    <h2>
        Consultar Devoluciones
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de Búsqueda
        </h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Empresa: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true">
                        </asp:DropDownList>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <ul class="datos_cip4 f0 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="corta">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro de Devoluci&oacute;n: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroDevolucionFilter" runat="server" CssClass="corta" MaxLength="8"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbNroDevolucion" runat="server" TargetControlID="txtNroDevolucionFilter"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                </ul>
                <ul class="datos_cip4 f0 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombre del cliente: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteNombreFilter" runat="server" CssClass="corta" MaxLength="100"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteNombres" runat="server" TargetControlID="txtClienteNombreFilter"
                            FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" 'áéíóúÁÉÍÓÚñÑ">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro de DNI: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtClienteNroDNIFilter" runat="server" CssClass="corta" MaxLength="8"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbClienteNroDNI" runat="server" TargetControlID="txtClienteNroDNIFilter"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nro de CIP: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroCIPFilter" runat="server" CssClass="corta" MaxLength="14"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="ftbNroCIP" runat="server" TargetControlID="txtNroCIPFilter"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <ul class="datos_cip2 t0">
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha de la Devoluci&oacute;n del:</li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaInicio" Display="Dynamic" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Del' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Del' no válida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaFin"
                                Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                                Text="*"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="ibtnFechaFin" CssClass="calendario" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                            </asp:ImageButton>
                            <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaFin" Display="None" Visible="true" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Al' no válida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>
                            <div style="clear: both;">
                            </div>
                            <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de búsqueda no es válido"
                                ControlToCompare="txtFechaFin" ControlToValidate="txtFechaInicio" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>
                        </p>
                    </li>
                </ul>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                    MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                    PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy" TodaysDateFormat="">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                    PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul3" Text="Buscar" />
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul4" Text="Nuevo" Visible="false" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        <div class="result">
            <asp:UpdatePanel ID="upResultado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text="Registros:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                            AutoGenerateColumns="False" DataKeyNames="IdDevolucion" AllowPaging="True">
                            <Columns>
                                <asp:BoundField DataField="IdDevolucion" HeaderText="Nro. Devolución" SortExpression="IdDevolucion">
                                </asp:BoundField>
                                <asp:BoundField DataField="SimboloMoneda" HeaderText="Moneda" SortExpression="SimboloMoneda">
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Solicitud" SortExpression="FechaCreacion">
                                </asp:BoundField>
                                <asp:BoundField DataField="NroCIP" HeaderText="Nro. de CIP" SortExpression="NroCIP">
                                </asp:BoundField>
                                <asp:BoundField DataField="Monto" HeaderText="Monto" SortExpression="Monto"></asp:BoundField>
                                <asp:BoundField DataField="ClienteNroDNI" HeaderText="Nro DNI" SortExpression="ClienteNroDNI">
                                </asp:BoundField>
                                <asp:BoundField DataField="ClienteNombres" HeaderText="Nombre Cliente" SortExpression="ClienteNombres">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstadoAdmin" HeaderText="Estado" SortExpression="DescripcionEstadoCliente">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="ibtEdit" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                            ToolTip="Actualizar" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"IdDevolucion")%>' CssClass="lupa"/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.</EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                        <cc1:ModalPopupExtender ID="mppDevolucion" runat="server" BackgroundCssClass="modalBackground"
                            CancelControlID="imgbtnRegresarHidden" PopupControlID="pnlPopupActualizarDevolucion"
                            TargetControlID="imgbtnTargetHidden" Y="0">
                        </cc1:ModalPopupExtender>
                        <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" Style="display: none;"
                            CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                        <asp:ImageButton ID="imgbtnTargetHidden" runat="server" Style="display: none;" CssClass="Hidden"
                            CausesValidation="false"></asp:ImageButton>
                        <asp:Panel ID="pnlPopupActualizarDevolucion" runat="server" class="conten_pasos3"
                            Style="display: none; background: white; padding-bottom: 0px; width: 600px; min-height: 275px">
                            <div style="float: right">
                                <asp:Button ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" runat="server"
                                    ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                                    CausesValidation="false" CssClass="close" />
                            </div>
                            <h4>
                                Datos de la devoluci&oacute;n:</h4>
                            <ul class="datos_cip2">
                                <li class="t1"><span class="color">>></span>Nro. de CIP: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtNroCIP" runat="server" CssClass="normal" MaxLength="14" ReadOnly="true"></asp:TextBox>
                                </li>
                                <li class="t1"><span class="color">>></span>Nro. de DNI: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtClienteNroDNI" runat="server" CssClass="normal" MaxLength="8"
                                        ReadOnly="true"></asp:TextBox>
                                </li>
                                <li class="t1"><span class="color">>></span> Nombres del cliente : </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtClienteNombres" runat="server" CssClass="normal" MaxLength="100"
                                        ReadOnly="true"></asp:TextBox>
                                </li>
                                <li class="t1"><span class="color">>></span> Apellidos del cliente : </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtClienteApellidos" runat="server" CssClass="normal" MaxLength="100"
                                        ReadOnly="true"></asp:TextBox>
                                </li>
                                <li class="t1"><span class="color">>></span>Direcci&oacute;n del cliente: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtClienteDireccion" runat="server" CssClass="normal" MaxLength="100"
                                        ReadOnly="true"></asp:TextBox>
                                </li>
                                <li class="t1"><span class="color">>></span>Estado: </li>
                                <li class="t2">
                                    <asp:TextBox ID="txtEstado" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                                </li>
                                <li class="t1"><span class="color">>></span>Observaciones: </li>
                                <li class="t2 txtArea">
                                    <asp:TextBox ID="txtObservaciones" runat="server" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                </li>
                            </ul>
                            <div style="clear: both">
                            </div>
                            <div id="divDatosActualizar" runat="server">
                                <h4>
                                    Datos a actualizar:</h4>
                                <ul class="datos_cip2">
                                    <li class="t1"><span class="color">>></span> Estado a actualizar: </li>
                                    <li class="t2">
                                        <asp:DropDownList ID="ddlEstadoNuevo" runat="server" CssClass="neu">
                                        </asp:DropDownList>
                                    </li>
                                </ul>
                            </div>
                            <div style="clear: both">
                            </div>
                            <ul class="datos_cip2">
                                <li class="complet" style="margin-top: 10px">
                                    <asp:Button ID="btnPActualizar" runat="server" CssClass="input_azul5" Text="Actualizar" />
                                    <asp:Button ID="btnPCancelar" runat="server" CssClass="input_azul4" Text="Cancelar" />
                                    <asp:Button ID="btnPCerrar" runat="server" CssClass="input_azul3" Text="Cerrar" Visible="false" />
                                </li>
                            </ul>
                            <asp:HiddenField ID="hdfIdDevolucion" runat="server" />
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= txtNroCIPFilter.ClientID() %>,#<%= txtClienteNombreFilter.ClientID() %>,#<%= txtNroDevolucionFilter.ClientID() %>,#<%= txtClienteNroDNIFilter.ClientID() %>,#<%= txtFechaInicio.ClientID() %>,#<%= txtFechaFin.ClientID() %>').keypress(function (e) {
                if (e.which == 13) {
                    $('#<%= btnBuscar.ClientID() %>').click();
                    return false;
                }
            });
        });
        function AjaxEnd(sender, args) {
            if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').length > 0)
                if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').text() != 'Página :')
                    $('.pagerStyle>td>table>tbody>tr>td:eq(0)').before('<td>P&aacute;gina :</td>');
            $(".divLoadingMensaje").hide('fast');
            $('.divLoading').hide('fold');
            $('#<%= txtNroCIPFilter.ClientID() %>,#<%= txtClienteNombreFilter.ClientID() %>,#<%= txtNroDevolucionFilter.ClientID() %>,#<%= txtClienteNroDNIFilter.ClientID() %>,#<%= txtFechaInicio.ClientID() %>,#<%= txtFechaFin.ClientID() %>').unbind("keypress");
            $('#<%= txtNroCIPFilter.ClientID() %>,#<%= txtClienteNombreFilter.ClientID() %>,#<%= txtNroDevolucionFilter.ClientID() %>,#<%= txtClienteNroDNIFilter.ClientID() %>,#<%= txtFechaInicio.ClientID() %>,#<%= txtFechaFin.ClientID() %>').keypress(function (e) {
                if (e.which == 13) {
                    $('#<%= btnBuscar.ClientID() %>').click();
                    return false;
                }
            });
        }
    </script>
</asp:Content>
