﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports System.Linq
Imports _3Dev.FW.Web.WebUtil
Imports _3Dev.FW.Entidades
Imports _3Dev.FW.Util.DataUtil
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class JPR_CODev
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)
    Protected Overrides ReadOnly Property BtnSearch As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnNew As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Public Overrides Function AllowToDelete(ByVal request As _3Dev.FW.Entidades.BusinessMessageBase) As Boolean
        Return False
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim obeServicio As New BEServicio
            Dim cCServicio As New SPE.Web.CServicio
            Dim oCAdministrarParametro As New SPE.Web.CAdministrarParametro
            Dim oCEmpresaContrante As New CEmpresaContratante()
            Dim listaParametros As New List(Of BEParametro), listaParametrosFilter As New List(Of BEParametro)
            Dim oBEEmpresaContratante As New BEEmpresaContratante()
            obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario
            oBEEmpresaContratante.IdEstado = EstadoMantenimiento.Activo
            DropDownlistBinding(ddlEmpresa, oCEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Todos::")
            DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Todos::")
            'DropDownlistBinding(ddlServicio, cCServicio.ConsultarServiciosPorIdUsuario(obeServicio), "Nombre", "IdServicio", "::Todos::")
            DropDownlistBinding(ddlEstado, oCAdministrarParametro.ConsultarParametroPorCodigoGrupo(GrupoEstadoDevolucion), "Descripcion", "Id", "::Todos::")
            txtFechaInicio.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
            txtFechaFin.Text = Today.ToString("dd/MM/yyyy")
        End If
    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As BusinessEntityBase) As System.Collections.Generic.List(Of BusinessEntityBase)
        Dim cOrdenPago As New COrdenPago
        Dim ListaDevoluciones As List(Of BusinessEntityBase) = cOrdenPago.ConsultarDevoluciones(be)
        ViewState("ListaDevoluciones") = ListaDevoluciones
        Return ListaDevoluciones
    End Function
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim oBEDevolucion As New BEDevolucion
        oBEDevolucion.NroCIP = txtNroCIPFilter.Text
        Dim IdDevolucion As Int64 = -1
        If Not Int64.TryParse(txtNroDevolucionFilter.Text, IdDevolucion) Then
            IdDevolucion = -1
        End If
        oBEDevolucion.IdDevolucion = IdDevolucion
        oBEDevolucion.IdEmpresaContratante = 0
        If Not String.IsNullOrEmpty(ddlEmpresa.SelectedValue) Then
            oBEDevolucion.IdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
        End If
        oBEDevolucion.IdServicio = 0
        If Not String.IsNullOrEmpty(ddlServicio.SelectedValue) Then
            oBEDevolucion.IdServicio = CInt(ddlServicio.SelectedValue)
        End If
        oBEDevolucion.ClienteNombres = txtClienteNombreFilter.Text
        oBEDevolucion.IdEstadoCliente = 0
        oBEDevolucion.IdEstadoAdmin = 0
        If Not String.IsNullOrEmpty(ddlEstado.SelectedValue) Then
            oBEDevolucion.IdEstadoAdmin = CInt(ddlEstado.SelectedValue)
        End If
        oBEDevolucion.IdUsuarioCreacion = UserInfo.IdUsuario
        oBEDevolucion.ClienteNroDNI = txtClienteNroDNIFilter.Text
        oBEDevolucion.FechaInicio = StringToDateTime(txtFechaInicio.Text)
        oBEDevolucion.FechaFinal = StringToDateTime(txtFechaFin.Text)
        Return oBEDevolucion
    End Function
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing And ResultList.Count() > 0 Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, ResultList(0).TotalPageNumbers)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        End If
        divResult.Visible = True
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("../DEV/ADDev.aspx")
    End Sub
    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbtnRegresar.Click
        mppDevolucion.Hide()
    End Sub

    Protected Sub btnPCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPCancelar.Click
        mppDevolucion.Hide()
    End Sub

    Protected Sub btnPCerrar_Click(sender As Object, e As System.EventArgs) Handles btnPCerrar.Click
        mppDevolucion.Hide()
    End Sub
    Protected Sub ddlEmpresa_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        If ddlEmpresa.SelectedIndex = 0 Then
            DropDownlistBinding(ddlServicio, New List(Of BEServicio), "Nombre", "IdServicio", "::Todos::")
            Return
        End If
        Using ocntrlServicio As New CServicio()
            Dim objbeservicio As New BEServicio()
            objbeservicio.IdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
            objbeservicio.IdEstado = EstadoMantenimiento.Activo
            DropDownlistBinding(ddlServicio, ocntrlServicio.SearchByParameters(objbeservicio), "Nombre", "IdServicio", "::Todos::")
        End Using
    End Sub

    Protected Sub gvResultado_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Edit" : CargarPopup(CInt(e.CommandArgument))
        End Select
    End Sub

    Private Sub CargarPopup(ByVal id As Integer)
        Dim objTransferencia As New BETransferencia
        Dim objT As New BETransferencia
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        hdfIdDevolucion.Value = id

        Dim ListaDevoluciones As List(Of BusinessEntityBase) = CType(ViewState("ListaDevoluciones"), List(Of BusinessEntityBase))
        For Each item As BusinessEntityBase In ListaDevoluciones
            Dim oBEDevolucion As BEDevolucion = CType(item, BEDevolucion)
            If oBEDevolucion.IdDevolucion = id Then
                ddlEstadoNuevo.Items.Clear()
                divDatosActualizar.Visible = True
                btnPActualizar.Visible = True
                btnPCancelar.Visible = True
                btnPCerrar.Visible = False
                Select Case oBEDevolucion.IdEstadoAdmin
                    Case EstadoDevolucion.Solicitado
                        ddlEstadoNuevo.Items.Add(New ListItem(EstadoDevolucionDescripcion.EnProceso, EstadoDevolucion.EnProceso))
                        ' ddlEstadoNuevo.Items.Add(New ListItem(EstadoDevolucionDescripcion.Realizado, EstadoDevolucion.Realizado))
                    Case EstadoDevolucion.EnProceso
                        'ddlEstadoNuevo.Items.Add(New ListItem(EstadoDevolucionDescripcion.Realizado, EstadoDevolucion.Realizado))
                        'Case EstadoDevolucion.Realizado
                        '    divDatosActualizar.Visible = False
                        '    btnPCerrar.Visible = True
                        '    btnPActualizar.Visible = False
                        '    btnPCancelar.Visible = False
                        '    'JSMessageAlert("Validación", "La devolución se encuentra en estado realizado, ya no se puede modificar.", "key")
                        '    'Return
                End Select
                txtClienteApellidos.Text = oBEDevolucion.ClienteApellidos
                txtClienteDireccion.Text = oBEDevolucion.ClienteDireccion
                txtClienteDireccion.ToolTip = oBEDevolucion.ClienteDireccion
                txtClienteNombres.Text = oBEDevolucion.ClienteNombres
                txtClienteNroDNI.Text = oBEDevolucion.ClienteNroDNI
                txtNroCIP.Text = oBEDevolucion.NroCIP
                txtEstado.Text = oBEDevolucion.DescripcionEstadoCliente
                txtObservaciones.Text = oBEDevolucion.Observaciones
            End If
        Next
        pnlPopupActualizarDevolucion.Visible = True
        mppDevolucion.Show()
    End Sub
    Public Overrides Sub OnGoToMaintenanceEditPage(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        e.NewEditIndex = -1
    End Sub

    Protected Sub btnPActualizar_Click(sender As Object, e As System.EventArgs) Handles btnPActualizar.Click
        Dim oCOrdenPago As New COrdenPago
        Dim ListaDevoluciones As List(Of BusinessEntityBase) = CType(ViewState("ListaDevoluciones"), List(Of BusinessEntityBase))
        Dim oBEDevolucion As BEDevolucion = ListaDevoluciones.Where(Function(x) CType(x, BEDevolucion).IdDevolucion = Convert.ToInt64(hdfIdDevolucion.Value)).First()
        oBEDevolucion.IdEstadoAdmin = CInt(ddlEstadoNuevo.SelectedValue)
        oBEDevolucion.IdUsuarioActualizacion = UserInfo.IdUsuario
        oCOrdenPago.ActualizarDevolucion(oBEDevolucion)
        JSMessageAlert("OK", "La devolución se actualizo correctamente.", "key")
        BtnImgSearch_Click(Nothing, Nothing)
        mppDevolucion.Hide()
    End Sub

    Public Sub JSMessageAlert(tipo_alerta As String, mensaje As String, key As String)
        Dim script As String
        script = String.Format("alert('{0}: {1}');", tipo_alerta, mensaje)
        If Me.Master IsNot Nothing Then
            If Me.Master.AppRelativeVirtualPath.Contains("MasterPagePrincipal") Then
                If Me.IsPostBack Then
                    script = String.Format("document.getElementById('errores').innerHTML = '{0}: {1}';", tipo_alerta, mensaje)
                    script += "document.getElementById('errores').style.display = 'block';"
                Else
                    script = "window.onload = function(){"
                    script += String.Format("document.getElementById('errores').innerHTML = '{0}: {1}';", tipo_alerta, mensaje)
                    script += "document.getElementById('errores').style.display = 'block';"
                    script += "}"
                End If
            End If
        End If
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub
End Class
