<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COEstab.aspx.vb" Inherits="POS_COEstab" Title="PagoEfectivo - Consultar Establecimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Establecimiento</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B&uacute;squeda
        </h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo de Establecimiento:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigoEstablecimiento" runat="server" CssClass="neu"> 
                        </asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Descripci&oacute;n:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtDescripcion" runat="server" CssClass="neu"> 
                        </asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Operador:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlIdEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombre Comercio:</li>
                    <li class="t2">
                        <asp:TextBox ID="txtNombreComercio" runat="server" CssClass="neu"> 
                        </asp:TextBox>
                    </li>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </ul>
        <div style="clear: both;">
        </div>
        <ul id="Fieldset2" class="datos_cip2 h0">
            <li class="complet">
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" />
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h5 id="h5Resultado" runat="server">
                    <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                </h5>
                <div id="divOrdenesPago" class="result">
                    <asp:GridView ID="gvEstablecimiento" runat="server" CssClass="result" AutoGenerateColumns="False"
                        AllowPaging="True" DataKeyNames="IdEstablecimiento" AllowSorting="true">
                        <Columns>
                            <asp:BoundField DataField="idEstablecimiento" Visible="false" />
                            <asp:BoundField DataField="SerieTerminal" SortExpression="SerieTerminal" HeaderText="Cod.Establecimiento">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionComercio" SortExpression="RazonSocial" HeaderText="Raz&#243;n Social" />
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                HeaderText="Estado">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgActualizar" PostBackUrl="ADEstab.aspx" CommandName="Edit"
                                        runat="server" ImageUrl="~/images/lupa.gif" ToolTip="Actualizar" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="IdEstablecimiento" HeaderText="IEstablecimiento" Visible="false" />
                        </Columns>
                        <HeaderStyle CssClass="cabecera" />
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
