Imports System.Data
Imports System.Collections.Generic

Partial Class POS_COEstab
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CEstablecimiento)

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvEstablecimiento
        End Get
    End Property

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CEstablecimiento
    'End Function

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeEstablecimiento As New SPE.Entidades.BEEstablecimiento
        obeEstablecimiento.SerieTerminal = txtCodigoEstablecimiento.Text
        obeEstablecimiento.Descripcion = txtDescripcion.Text

        If ddlIdEstado.SelectedValue.ToString = "" Then
            obeEstablecimiento.IdEstado = 0
        Else
            obeEstablecimiento.IdEstado = CInt(ddlIdEstado.SelectedValue)
        End If

        If ddlOperador.SelectedValue.ToString = "" Then
            obeEstablecimiento.IdOperador = 0
        Else
            obeEstablecimiento.IdOperador = CInt(ddlOperador.SelectedValue)
        End If

        obeEstablecimiento.DescripcionComercio = txtNombreComercio.Text

        Return obeEstablecimiento


   

    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)

        key = gvEstablecimiento.DataKeys(e.RowIndex).Values(0)

    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADEstab"
        e.MaintenancePageName = "ADEstab.aspx"
        e.ExecuteSearchOnFirstLoad = False

    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        h5Resultado.Visible = False
        txtCodigoEstablecimiento.Text = ""
        txtDescripcion.Text = ""
        txtNombreComercio.Text = ""
        ddlIdEstado.SelectedIndex = 0
        ddlOperador.SelectedIndex = 0
        gvEstablecimiento.DataSource = Nothing
        gvEstablecimiento.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
    End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        h5Resultado.Visible = True
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If

    End Sub

    Public Overrides Sub OnFirstLoadPage()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", "::: Todos :::")

        Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlOperador, objCEstablecimiento.ObtenerOperadores(), "Descripcion", "IdOperador", "::: Todos :::")



    End Sub

    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtCodigoEstablecimiento)
        End If
        h5Resultado.Visible = False
        '
    End Sub

    Protected Sub gvEstablecimiento_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    End Sub

    Protected Sub gvEstablecimiento_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "DescripcionEstado", _
        "Inactivo", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub

End Class
