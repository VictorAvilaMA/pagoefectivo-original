﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="COFactu.aspx.vb" Inherits="JPR_COFactu" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Facturas Empresas Recaudadoras
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de Búsqueda
        </h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">&gt;&gt;</span> Banco: </li>
            <li class="t2">
                <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                </asp:DropDownList>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Nro de Transacci&oacute;n: </li>
            <li class="t2">
                <asp:TextBox ID="txtNroDeTransaccion" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo de Operaci&oacute;n:
            </li>
            <li class="t2">
                <asp:TextBox ID="txtCodOperacion" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> CIP: </li>
            <li class="t2">
                <asp:TextBox ID="txtCIP" runat="server" CssClass="normal" MaxLength="50"></asp:TextBox>
            </li>
            <li class="t1"><span class="color">&gt;&gt;</span> Fecha Emisi&oacute;n desde: </li>
            <li class="t2" style="line-height: 1; z-index: 200;">
                <p class="input_cal">
                    <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                    <asp:ImageButton CssClass="calendario" ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                    </asp:ImageButton>
                    <cc1:MaskedEditValidator ID="MaskedEditValidatorDel" runat="server" IsValidEmpty="False"
                        InvalidValueMessage="Fecha 'Del' no válida" InvalidValueBlurredMessage="*" ErrorMessage="*"
                        EmptyValueMessage="Fecha 'Del' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaDe"
                        ControlExtender="mdeDel"></cc1:MaskedEditValidator>
                </p>
                <span class="entre">al: </span>
                <p class="input_cal">
                    <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                        Enabled="true" ValidationGroup="GrupoValidacion" ControlToValidate="txtFechaA"
                        Display="None" ErrorMessage="Campo Requerido" Visible="true" EnableClientScript="true"
                        Text="*"></asp:RequiredFieldValidator>
                    <asp:ImageButton CssClass="calendario" ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'>
                    </asp:ImageButton>
                    <cc1:MaskedEditValidator CssClass="izq" ID="MaskedEditValidatorAl" runat="server"
                        IsValidEmpty="False" InvalidValueMessage="Fecha 'Al' no válida" InvalidValueBlurredMessage="*"
                        EmptyValueMessage="Fecha 'Al' es requerida" EmptyValueBlurredText="*" ControlToValidate="txtFechaA"
                        ControlExtender="mdeA">*
                    </cc1:MaskedEditValidator>
                    <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de búsqueda no es válido"
                        ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                        Type="Date">*</asp:CompareValidator>
                </p>
            </li>
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnExportar" runat="server" CssClass="input_azul4" Text="Exportar Excel" />
            </li>
        </ul>
        <cc1:CalendarExtender ID="calDel" runat="server" TargetControlID="txtFechaDe" PopupButtonID="ibtnFechaDe"
            Format="dd/MM/yyyy">
        </cc1:CalendarExtender>
        <cc1:CalendarExtender ID="calAl" runat="server" TargetControlID="txtFechaA" PopupButtonID="ibtnFechaA"
            Format="dd/MM/yyyy">
        </cc1:CalendarExtender>
        <cc1:MaskedEditExtender ID="mdeDel" runat="server" TargetControlID="txtFechaDe" MaskType="Date"
            Mask="99/99/9999" CultureName="es-PE">
        </cc1:MaskedEditExtender>
        <cc1:MaskedEditExtender ID="mdeA" runat="server" TargetControlID="txtFechaA" MaskType="Date"
            Mask="99/99/9999" CultureName="es-PE">
        </cc1:MaskedEditExtender>
        <div style="clear: both;">
        </div>
        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        <div class="result">
            <asp:UpdatePanel ID="upResultado" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text="Registros:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                            AutoGenerateColumns="False" DataKeyNames="IdFactura" AllowPaging="True" OnRowCommand="gvResultado_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText ="Ver">
                                    <ItemTemplate>
                                        <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:CommandField ButtonType="Image" SelectImageUrl="~/images/lupa.gif" SelectText="Ver"
                                    ShowSelectButton="True" HeaderText="Ver">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CommandField>--%>
                                <asp:BoundField DataField="DescripcionBanco" HeaderText="Banco" SortExpression="DescripcionBanco">
                                </asp:BoundField>
                                <asp:BoundField DataField="NroFactura" HeaderText="Nro. Transaccion" SortExpression="NroTransaccion">
                                </asp:BoundField>
                                <asp:BoundField DataField="MontoFactura" HeaderText="Monto" SortExpression="MontoFactura">
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaEmision" HeaderText="Fch. Emision" DataFormatString="{0:d}"
                                    SortExpression="FechaEmision"></asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.</EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div style="clear: both">
    </div>
    <asp:UpdatePanel ID="upnlListaDeposito" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc1:ModalPopupExtender ID="mppResultadoDetalle" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="ImageButton1" PopupControlID="pnlPopupResultadoDetalle" TargetControlID="ImageButton2"
                Drag="true" Y="0">
            </cc1:ModalPopupExtender>
            <asp:ImageButton ID="ImageButton1" ImageUrl="" CausesValidation="false" Visible="true"
                CssClass="Hidden" runat="server" Style="display: none"></asp:ImageButton>
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/lupa.gif"%>' CssClass="Hidden"
                ToolTip="Buscar Empresa" Style="display: none" />
            <asp:Panel ID="pnlPopupResultadoDetalle" runat="server" Style="display: none; background: white;
                max-height: 500px; min-height: 0px" CssClass="conten_pasos3">
                <div style="float: right">
                    <asp:ImageButton ID="imgbtnRegresar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/closeX.GIF"%>' CausesValidation="false"
                        runat="server"></asp:ImageButton>
                </div>
                <div style="clear: both">
                </div>
                <div class="result" style="margin: 10px; overflow: auto; height: 300px">
                    <h5 id="h1" runat="server">
                        <asp:Literal ID="lblResultadoDetalle" runat="server" Text=""></asp:Literal>
                    </h5>
                    <div style="clear: both;">
                    </div>
                    <asp:GridView ID="gvResultadoDetalle" DataKeyNames="IdConciliacionArchivo,IdBanco"
                        runat="server" CssClass="grilla" AllowPaging="True" AutoGenerateColumns="False"
                        OnRowCommand="gvResultado_RowCommand">
                        <Columns>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlagSeleccionado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FlagSeleccionado") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIdDeposito" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "IdDeposito") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaDeposito" DataFormatString="{0:d}" SortExpression="Fecha"
                                HeaderText="Fecha Deposito">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CodigoOperacion" SortExpression="CodigoOperacion" HeaderText="Cod. Operacion">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NroTransaccion" SortExpression="NroTransaccion" HeaderText="Nro. Transacción">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MontoDepositado" HeaderText="Monto Dep." SortExpression="MontoDepositado">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Comentarios" SortExpression="Comentarios" HeaderText="Comentarios">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                    <asp:HiddenField ID="hdfFactura" runat="server" />
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
