<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    EnableEventValidation="false" AutoEventWireup="false" CodeFile="COHiPa.aspx.vb"
    Inherits="ECO_COCIP" Title="PagoEfectivo - Historial de Pagos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True" AsyncPostBackTimeout="900">
    </asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        //        $(document).ready(function () {
        //            $('#<%=txtNroOrdenPago.ClientID%>').blur(function () {
        //                //alert('jajaja');
        //                if ($('#<%=txtNroOrdenPago.ClientID%>').val() != '')
        //                    $('#<%=txtNroOrdenPago.ClientID%>').val($('#<%=txtNroOrdenPago.ClientID%>').val().lpad('0', 14));
        //            });
        //        });
    </script>
    <h2>
        Consultar Historial de Pagos
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanelOP" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <%-- <script type="text/javascript" language="javascript">
                    $(document).ready(function () {
                        $('#<%=txtNroOrdenPago.ClientID%>').blur(function () {
                            //alert('jajaja');
                            if ($('#<%=txtNroOrdenPago.ClientID%>').val() != '')
                                $('#<%=txtNroOrdenPago.ClientID%>').val($('#<%=txtNroOrdenPago.ClientID%>').val().lpad('0', 14));
                        });
                    });
                </script>--%>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Empresa: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="true"
                            Style="float: left">
                            <asp:ListItem Selected="True">::: TODOS :::</asp:ListItem>
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="ValidaCampos"
                            ErrorMessage="Debe seleccionar la empresa." ControlToValidate="ddlEmpresa">*</asp:RequiredFieldValidator>--%>
                    </li>
                    <li class="t1"><span class="color">>></span> Servicio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                            <asp:ListItem Selected="True">::: TODOS :::</asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">>></span> C.I.P.: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroOrdenPago" runat="server" CssClass="normal" MaxLength="14"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTBE_txtNroOrdenPago" runat="server" FilterType="Numbers"
                            TargetControlID="txtNroOrdenPago">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <div style="clear: both;">
                    </div>
                    <li class="t1"><span class="color">>></span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> Nro de Transacci�n (portal): </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroTransaccion" runat="server" CssClass="normal" MaxLength="75"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="RegularExpressionValidator2" runat="server" TargetControlID="txtNroTransaccion"
                            ValidChars="abcdefghijklmnopqrstuvwxyz0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </li>
                    <li class="t1"><span class="color">>></span> Cliente: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtCliente" runat="server" CssClass="normal" MaxLength="75"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCliente"
                            ErrorMessage="No se permiten tildes ni caracteres extra�os..." ValidationExpression="^[a-zA-Z�� ]*$" />
                    </li>
                    <li class="t1"><span class="color">>></span> Email de Cliente: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revMailAlternativo" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="No se permiten tildes ni caracteres extra�os..." ValidationExpression="^[-_a-zA-Z0-9��.@ ]*$" />
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha del: </li>
                    <li class="t2" style="line-height: 1; z-index: 20;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <%--<cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                ControlToValidate="txtFechaDe" ErrorMessage="*" InvalidValueMessage="Fecha 'Del' no v�lida"
                                IsValidEmpty="False" EmptyValueMessage="Fecha 'Del' es requerida" Display="Dynamic"
                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>--%>
                                 <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                    ControlToValidate="txtFechaDe" Display="Dynamic" EmptyValueBlurredText="*"
                                    EmptyValueMessage="Fecha 'Del' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                    InvalidValueMessage="Fecha 'Del' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                                </cc1:MaskedEditValidator>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>

                           <%-- <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaA" ErrorMessage="*" Visible="true" InvalidValueMessage="Fecha 'Al' no v�lida"
                                IsValidEmpty="False" EmptyValueMessage="Fecha 'Al' es requerida" Display="Dynamic"
                                EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>--%>

                                <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtenderA"
                                ControlToValidate="txtFechaA" Display="None" Visible="true" EmptyValueBlurredText="*"
                                EmptyValueMessage="Fecha 'Al' es requerida" ErrorMessage="*" InvalidValueBlurredMessage="*"
                                InvalidValueMessage="Fecha 'Al' no v�lida" IsValidEmpty="False" ValidationGroup="GrupoValidacion">
                            </cc1:MaskedEditValidator>

                           <%-- <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>--%>

                                <asp:CompareValidator ID="covFecha" runat="server" Display="None" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                                ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                Type="Date">*</asp:CompareValidator>
                        </p>
                    </li>

                    <li style="padding-left: 33px; width: auto;">
                        <b>* El reporte mostrar&aacute; la informaci&oacute;n actualizada despu&eacute;s de 10 minutos.</b>
                    </li>                    
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar"  />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                        <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Text="Exportar Excel"/>
                    </li>
                    <li class="t1"><span class="color"></span></li>                   
                </ul>
                
                <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" TargetControlID="txtFechaDe"
                    Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                </cc1:MaskedEditExtender>
                <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" TargetControlID="txtFechaA"
                    Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaDe"
                    Format="dd/MM/yyyy" PopupButtonID="ibtnFechaDe">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaA"
                    Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA">
                </cc1:CalendarExtender>
                <cc1:FilteredTextBoxExtender ID="FTBE_txtCliente" runat="server" ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' "
                    TargetControlID="txtCliente">
                </cc1:FilteredTextBoxExtender>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                <asp:PostBackTrigger ControlID="btnExportarExcel" />
            </Triggers>
        </asp:UpdatePanel>
        
        <div style="clear: both">
        </div>
        <li style="padding-left: 33px;margin-bottom:20px; width: auto;list-style-type:none;">
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" />
        </li>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" class="result">
            <ContentTemplate>
                <div id="divResult" runat="server" visible="false">
                    <h5>
                        <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                    </h5>
                    <asp:GridView ID="grdResultado" runat="server" BackColor="White" AllowPaging="True"
                        AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                        CellPadding="3" OnRowDataBound="grdResultado_RowDataBound" CssClass="grilla"
                        EnableTheming="True" AllowSorting="True" PageSize="20">
                        <Columns>
                            <asp:BoundField DataField="DescripcionServicio" HeaderText="Servicio" SortExpression="DescripcionServicio">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="C.I.P." ItemStyle-HorizontalAlign="Center" SortExpression="NumeroOrdenPago">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkNumeroOrdenPago" runat="server" CommandName="Detalle" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                        ToolTip="Ver informaci�n" Text='<%#DataBinder.Eval(Container.DataItem, "NumeroOrdenPago")%>'
                                        CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>' />
                                    <asp:HiddenField ID="hdfNumeroOrdenPago" runat="server" Value='<%# Eval("NumeroOrdenPago") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionCliente" HeaderText="Cliente" SortExpression="DescripcionCliente">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Email.Cliente" SortExpression="Email">
                                <ItemTemplate>
                                    <asp:Label ID="txtEmailCliente" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ClienteEmailReducido")%>'
                                        ToolTip='<%#DataBinder.Eval(Container.DataItem, "ClienteEmail")%>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" SortExpression="Total">
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "SimboloMoneda") + " " + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Total"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" SortExpression="DescripcionEstado">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Fec.Emision" SortExpression="FechaEmision">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEmision"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fec.Pago" SortExpression="FechaCancelacion">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaCancelacion"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="Fec.Anulada" SortExpression="FechaAnulada">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaAnulada"))%>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Fec. A Expirar" SortExpression="FechaExpirada">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaExpirada"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--  <asp:TemplateField HeaderText="Fec.Eliminado" SortExpression="FechaEliminado">
                                <ItemTemplate>
                                    <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaEliminado"))%>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="grdResultado" EventName="PageIndexChanging" />
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
