Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports SPE.Entidades
Imports _3Dev.FW.Util.DataUtil

Partial Class ECO_COCIP
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COrdenPago)
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return grdResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    'add <PeruCom FaseIII>
    Protected Overrides ReadOnly Property FlagPager() As Boolean
        Get
            Return True
        End Get
    End Property
    Private Property ActualIdEmpresaContratante() As Integer
        Get
            If (ViewState("IdEmpresaContratante") Is Nothing) Then
                ViewState("IdEmpresaContratante") = 0
            End If
            Return ViewState("IdEmpresaContratante")
        End Get
        Set(ByVal value As Integer)
            ViewState("IdEmpresaContratante") = value
        End Set
    End Property


    'Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
    '    Get
    '        Return lblResultado
    '    End Get
    'End Property


    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        e.ExecuteSearchOnFirstLoad = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (Page.IsPostBack) Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtNroOrdenPago)
            CargarDatos()
            If Session("obeOrdenPagoSearch") IsNot Nothing Then
                CreateSearchOfBusiness(CType(Session("obeOrdenPagoSearch"), BEOrdenPago))
                Session.Remove("obeOrdenPagoSearch")
            End If
        End If
    End Sub

    'CARGA DE DATOS
    Private Sub CargarDatos()
        Using ocntrlEmpresaContrante As New SPE.Web.CEmpresaContratante()
            Dim oBEEmpresaContratante As New BEEmpresaContratante() 'asd
            oBEEmpresaContratante.Codigo = ""
            oBEEmpresaContratante.RazonSocial = ""
            oBEEmpresaContratante.RUC = ""
            oBEEmpresaContratante.IdEstado = 41
            oBEEmpresaContratante.Email = ""
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::: TODOS :::")
        End Using
        Using objParametro As New SPE.Web.CAdministrarParametro
            'ESTADO Código de Identificación de Pago
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoEstadoOrdenPago), "Descripcion", "Id", "::: TODOS :::")
        End Using

        InicializarFecha()
        txtNroOrdenPago.Focus()
    End Sub
    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        Using objCAdministrarServicio As New SPE.Web.CServicio
            Dim obeServicio As New BEServicio
            'obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario

            If ddlEmpresa.SelectedValue = "" Then
                obeServicio.IdServicio = 0
            Else
                obeServicio.IdServicio = CInt(ddlEmpresa.SelectedValue)
            End If
            Dim servicio As New List(Of BEServicio)
            servicio = objCAdministrarServicio.ConsultarServicioPorIdEmpresa(obeServicio)
            If (servicio.Count > 0) Then
                ActualIdEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
            End If
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlServicio, servicio, "Nombre", "IdServicio", "::: TODOS :::")
        End Using

    End Sub
    Private Sub InicializarFecha()
        'FECHAS

        txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaA.Text = Today.ToShortDateString
    End Sub

    'VALIDAMOS LAS FECHAS
    Private Function ValidarFecha() As Boolean
        If Not IsDate(txtFechaA.Text) OrElse _
            Not IsDate(txtFechaDe.Text) Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Overrides Sub OnClear()
        txtNroOrdenPago.Text = String.Empty
        txtCliente.Text = String.Empty
        txtEmail.Text = String.Empty
        ddlServicio.SelectedIndex = 0
        grdResultado.DataSource = Nothing
        grdResultado.DataBind()
        ddlEstado.SelectedIndex = 0
        InicializarFecha()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        divResult.Visible = False

        MyBase.OnClear()
    End Sub



    Private Function ObtenerMensajeResultado(ByVal cantidad As Integer) As String
        Dim strResultado As New StringBuilder()
        strResultado.Append(String.Format("Resultados: {0} registro(s) ", cantidad))

        'If (ddlServicio.SelectedIndex > 0) Then
        '    strResultado.Append(String.Format("para el servicio {0} ", ddlServicio.SelectedItem.Text))
        'End If
        'strResultado.AppendLine(String.Format("entre el {0} y {1} ", txtFechaDe.Text.TrimEnd(), txtFechaA.Text.TrimEnd()))

        Return strResultado.ToString()
    End Function
    Public Overrides Function AllowToSearch() As Boolean
        Return ValidarFecha() And MyBase.AllowToSearch()
    End Function


    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeOrdenPago As New BEOrdenPago
        obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
        obeOrdenPago.IdEmpresa = ActualIdEmpresaContratante

        If ddlServicio.SelectedIndex = 0 Then
            obeOrdenPago.IdServicio = 0
        Else
            obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
        End If
        obeOrdenPago.OrderIdComercio = txtNroTransaccion.Text.TrimEnd()
        obeOrdenPago.UsuarioNombre = txtCliente.Text.TrimEnd()
        obeOrdenPago.UsuarioEmail = txtEmail.Text.Trim
        If ddlEstado.SelectedIndex = 0 Then obeOrdenPago.IdEstado = 0 Else obeOrdenPago.IdEstado = StringToInt(ddlEstado.SelectedValue)
        obeOrdenPago.FechaCreacion = StringToDateTime(txtFechaDe.Text)
        obeOrdenPago.FechaActualizacion = StringToDateTime(txtFechaA.Text)
        obeOrdenPago.OrderBy = SortExpression
        obeOrdenPago.IsAccending = SortDir
        Return obeOrdenPago
    End Function
    Private Sub CreateSearchOfBusiness(ByVal obeOrdenPago As BEOrdenPago)
        txtNroOrdenPago.Text = obeOrdenPago.NumeroOrdenPago
        ActualIdEmpresaContratante = obeOrdenPago.IdEmpresa
        If obeOrdenPago.IdServicio = 0 Then ddlServicio.SelectedIndex = 0 Else ddlServicio.SelectedValue = obeOrdenPago.IdServicio.ToString()
        txtCliente.Text = obeOrdenPago.UsuarioNombre
        txtEmail.Text = obeOrdenPago.UsuarioEmail
        If obeOrdenPago.IdEstado = 0 Then ddlEstado.SelectedIndex = 0 Else ddlEstado.SelectedValue = obeOrdenPago.IdEstado.ToString()
        txtFechaDe.Text = obeOrdenPago.FechaCreacion.ToString("dd/MM/yyyy")
        txtFechaA.Text = obeOrdenPago.FechaActualizacion.ToString("dd/MM/yyyy")
        txtNroTransaccion.Text = obeOrdenPago.OrderIdComercio
        SortExpression = obeOrdenPago.OrderBy
        SortDir = obeOrdenPago.IsAccending
        BtnSearch_Click(Nothing, Nothing)
    End Sub
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            'upd <PeruCom FaseIII>
            lblResultado.Text = ObtenerMensajeResultado(If(ResultList.Count > 0, ResultList.First().TotalPageNumbers, 0))
        Else
            lblResultado.Text = ObtenerMensajeResultado(0)
        End If
        divResult.Visible = True
    End Sub

    Protected Sub grdResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdResultado.RowDataBound
        '
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "DescripcionEstado", _
        "Anulada", _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
        '
    End Sub



    'Protected Sub txtNroOrdenPago_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    '    If Right("00000000000000" + txtNroOrdenPago.Text, 14) = "00000000000000" Then
    '        txtNroOrdenPago.Text = ""
    '    Else
    '        txtNroOrdenPago.Text = Right("00000000000000" + txtNroOrdenPago.Text, 14)
    '    End If

    'End Sub
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tiene permisos para exportar el excel .")
        Else
            Dim dtfechainicio As DateTime = StringToDateTime(txtFechaDe.Text)
            Dim dtfechafin As DateTime = StringToDateTime(txtFechaA.Text)
            Dim strFechaInicio As String = String.Format("{0}-{1}-{2}", dtfechainicio.Year, dtfechainicio.Month, dtfechainicio.Day)
            Dim strFechaFin As String = String.Format("{0}-{1}-{2}", dtfechafin.Year, dtfechafin.Month, dtfechafin.Day)
            Dim idestado As Integer = 0
            If ddlEstado.SelectedIndex = 0 Then idestado = 0 Else idestado = StringToInt(ddlEstado.SelectedValue)
            'Response.Redirect(String.Format("~/Reportes/RptCOCIPDet.aspx?CIP={0}&IdS={1}&C={2}&E={3}&IdE={4}&FIni={5}&FFin={6}&flag={7}", _
            'txtNroOrdenPago.Text.TrimEnd(), StringToInt(ddlServicio.SelectedValue), txtCliente.Text.TrimEnd(), idestado, ActualIdEmpresaContratante, strFechaInicio, strFechaFin, 0))
            Dim cuentas As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Dim FechaIniCorta As String = strFechaInicio
            Dim FechaFinCorta As String = strFechaFin
            Dim listaOrdenPublicacion As New List(Of BEOrdenPago)
            Using cOrdenPago As New SPE.Web.COrdenPago()
                Dim obeOrdenPago As New BEOrdenPago
                obeOrdenPago.NumeroOrdenPago = txtNroOrdenPago.Text.TrimEnd()
                obeOrdenPago.IdEmpresa = ActualIdEmpresaContratante

                If ddlServicio.SelectedIndex = 0 Then
                    obeOrdenPago.IdServicio = 0
                Else
                    obeOrdenPago.IdServicio = StringToInt(ddlServicio.SelectedValue)
                End If

                obeOrdenPago.UsuarioNombre = txtCliente.Text.TrimEnd()
                obeOrdenPago.IdEstado = idestado
                obeOrdenPago.FechaCreacion = StringToDateTime(strFechaInicio)
                obeOrdenPago.FechaActualizacion = StringToDateTime(strFechaFin)
                obeOrdenPago.OrderIdComercio = txtNroTransaccion.Text.TrimEnd()
                cuentas = cOrdenPago.SearchByParameters(obeOrdenPago)
            End Using
            For index = 0 To cuentas.Count - 1
                listaOrdenPublicacion.Add(CType(cuentas(index), BEOrdenPago))
            Next
            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("CIP", txtNroOrdenPago.Text.TrimEnd()))
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", txtFechaDe.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", txtFechaA.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", ddlEstado.SelectedItem.Text.ToString))
            UtilReport.ProcesoExportarGenerico(listaOrdenPublicacion, Page, "EXCEL", "xls", "Consultar CIP - ", parametros, "RptCOCIPDet.rdlc")
            lblResultado.Text = ""
        End If
    End Sub

    Protected Sub grdResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdResultado.RowCommand
        Try
            Dim reg As Integer
            Dim NroOP As String
            reg = grdResultado.Rows(e.CommandArgument).RowIndex
            NroOP = CType(grdResultado.Rows(reg).FindControl("hdfNumeroOrdenPago"), HiddenField).Value
            VariableTransicion = NroOP & "|" & "3"
            Session("obeOrdenPagoSearch") = CreateBusinessEntityForSearch()
            Select Case e.CommandName
                Case "Detalle"
                    Response.Redirect("~/ECO/DECIP.aspx")
            End Select
        Catch ex As Exception

        End Try
    End Sub
End Class
