<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COOper.aspx.vb" Inherits="ADM_COOper" Title="PagoEfectivo - Consultar Operador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Operador
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de B�squeda</h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt;</span> C&oacute;digo: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtCodigo" runat="server" CssClass="neu" MaxLength="50"> 
                        </asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Nombre: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtOperador" runat="server" CssClass="neu" MaxLength="100"> 
                        </asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado: </li>
                    <li class="t2">
                        <asp:DropDownList ID="dropEstado" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </ul>
        <div style="clear: both;">
        </div>
        <ul id="fsBotonera" class="datos_cip2 h0">
            <li class="complet">
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" />
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
            </li>
        </ul>
        <div style="clear: both;">
        </div>
        <div id="div3" class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div4">
                        <h4 id="h5Resultado" runat="server">
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h4>
                    </div>
                    <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" AllowPaging="True"
                        DataKeyNames="IdOperador" AutoGenerateColumns="False" AllowSorting="true">
                        <Columns>
                            <asp:BoundField DataField="Codigo" SortExpression="Codigo" HeaderText="C�digo"></asp:BoundField>
                            <asp:BoundField DataField="Descripcion" SortExpression="Descripcion" HeaderText="Descripci�n">
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADOper.aspx" runat="server"
                                        ToolTip="Actualizar" ImageUrl="~/images/lupa.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros</EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
