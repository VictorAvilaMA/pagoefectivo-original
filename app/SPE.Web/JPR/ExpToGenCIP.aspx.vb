﻿Imports SPE.Web
Imports SPE.Entidades
Imports SPE.Web.Util
Imports _3Dev.FW.Util.DataUtil
Partial Class ECO_ExpToGenCIP
    Inherits PaginaBase

    Protected Sub ibtnBuscarCIP_Click(sender As Object, e As System.EventArgs) Handles ibtnBuscarCIP.Click
        LimpiarSesion()
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        oBEOrdenPago.IdOrdenPago = txtNroCIP.Text.Trim()
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim result As BEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorIdSoloGenYExp(oBEOrdenPago)
        Session("RespuestaConsulta") = result
        If result.DescripcionEstado Is Nothing Then
            divDatos.Visible = False
            JSMessageAlert("Validación", "No se encontro el número de CIP o no se encuentra en los estados Generado o Expirado.", "val")
        Else
            divDatos.Visible = True
            txtConceptoPagoInfo.Text = result.ConceptoPago
            txtMontoInfo.Text = result.SimboloMoneda + " " + result.Total.ToString()
            txtServicioInfo.Text = result.DescripcionServicio
            txtEstadoInfo.Text = result.DescripcionEstado
            If result.FechaExpirada.ToString("dd/MM/yyyy") = "01/01/0001" Then
                txtFechaExpiracionActual.Text = String.Empty
            Else
                txtFechaExpiracionActual.Text = result.FechaExpirada.ToString("dd/MM/yyyy hh:mm:ss tt")
            End If
            txtFechaExpiracion.Text = Date.Now.AddHours(2).ToString("dd/MM/yyyy hh:mm:ss tt")
        End If
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As System.EventArgs) Handles btnActualizar.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        Dim oBEOrdenPagoSession As New BEOrdenPago
        Dim fechaExpiracion As New DateTime
        Dim cemail As New SPE.Web.Util.UtilEmail()

        Try
            oBEOrdenPagoSession = DirectCast(Session("RespuestaConsulta"), BEOrdenPago)
            oBEOrdenPago.NumeroOrdenPago = txtNroCIP.Text.PadLeft(14, "0")
            If DateTime.TryParseExact(txtFechaExpiracion.Text, "dd/MM/yyyy hh:mm:ss tt", Nothing, Globalization.DateTimeStyles.None, fechaExpiracion) Then
                oBEOrdenPago.FechaAExpirar = fechaExpiracion
            End If
            oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
            oBEOrdenPago.MotivoDeExpAGen = txtContenido.Text.Trim()
            oBEOrdenPago.SimboloMoneda = oBEOrdenPagoSession.SimboloMoneda
            oBEOrdenPago.Total = oBEOrdenPagoSession.Total
            Dim intResult As Int64 = oCOrdenPago.ActualizarCIPDeExpAGen(oBEOrdenPago)
            Select Case intResult
                Case Is > 0
                    Dim intRpta As Integer = cemail.EnviarEmailCambioDeEstadoCipDeExpAGen(oBEOrdenPago)
                    If intRpta = 1 Then
                        lblMensaje.Text = ("Se actualizó con éxito el CIP Nº " & intResult.ToString().PadLeft(14, "0"))
                        DesactivarControles()
                        LimpiarSesion()
                        btnNuevo.Visible = True
                    Else
                        Throw New Exception()
                    End If
                Case -1
                    JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
                Case -2
                    JSMessageAlert("Validación", "El cip ingresado no existe", "val")
                Case -3
                    JSMessageAlert("Validación", "El cip ingresado no pertenece a su empresa", "val")
                Case -4
                    JSMessageAlert("Validación", "El cip ingresado ya se encuentra expirado", "val")
                Case -5
                    JSMessageAlert("Validación", "El cip ingresado no se encuentra en estado generado", "val")
                Case -6
                    JSMessageAlert("Validación", "La fecha de expiración debe ser mayor a la de ahora", "val")
                Case -7
                    JSMessageAlert("Validación", "La fecha de expiración no puede exeder los 10 años", "val")
                Case Else
                    JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
            End Select
        Catch ex As Exception
            JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
        End Try
        
    End Sub

    Private Sub DesactivarControles()
        txtNroCIP.Enabled = False
        txtFechaExpiracion.Enabled = False
        btnActualizar.Enabled = False
        ibtnFechaExpiracion.Enabled = False
        btnActualizar.Visible = False
        txtFechaExpiracionActual.Enabled = False
        txtContenido.Enabled = False
        ibtnBuscarCIP.Enabled = False
    End Sub

    Protected Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("ExpToGenCIP.aspx")
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        txtContenido.Attributes.Add("onkeypress", " ValidarCaracteres(this, 120, $('#" + lblmensajeContenido.ClientID + "')); LimpiarResultado();")
        txtContenido.Attributes.Add("onkeyup", " ValidarCaracteres(this, 120, $('#" + lblmensajeContenido.ClientID + "'));")
        
    End Sub

    Private Sub LimpiarSesion()
        Session("RespuestaConsulta") = DBNull.Value
    End Sub

End Class
