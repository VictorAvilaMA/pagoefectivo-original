﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="ExtornarCIP.aspx.vb" Inherits="JPR_ExtornarCIP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
    </asp:ScriptManager>
    <style type="text/css">
        li span[style*="hidden"]
        {
            width: 0px !important;
        }
        .ajax__calendar_footer
        {
            display: none;
        }
        .lupa
        {
            background: url(../images/lupa.gif) no-repeat;
            border: 0px;
            width: 16px;
            height: 16px;
            margin: 10px;
        }
        .lupa:hover
        {
            cursor: pointer;
        }
    </style>
    <h2>
        Extornar CIP
    </h2>
    <div class="conten_pasos3">
        <div style="clear: both">
        </div>
        <asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h4>
                    1. Buscar CIP</h4>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span>Nro. de CIP: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNroCIP" runat="server" CssClass="normal" MaxLength="14" ValidationGroup="ValidaCamposBuscar ValidaCampos"></asp:TextBox>
                        <asp:Button ID="ibtnBuscarCIP" runat="server" ToolTip="Cargar Usuario" Style="float: left;
                            padding: 10px" CssClass="lupa" ValidationGroup="ValidaCamposBuscar ValidaCampos" />
                        <cc1:FilteredTextBoxExtender ID="ftbNroCIP" runat="server" TargetControlID="txtNroCIP"
                            ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                        <br />
                        <asp:RequiredFieldValidator ID="rfvNroCIP" runat="server" ErrorMessage="" ControlToValidate="txtNroCIP"
                            ValidationGroup="ValidaCamposBuscar ValidaCampos" Style="width: 200px;">* Debe ingresar el número del CIP.</asp:RequiredFieldValidator>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <div id="divDatos" runat="server" visible="false">
                    <h4>
                        2. Datos del CIP</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span>Nro CIP: </li>
                        <li class="t2">
                            <asp:Literal ID="txtNumeroCIP" runat="server"></asp:Literal>
                        </li>
                        <li class="t1"><span class="color">>></span>Concepto de Pago: </li>
                        <li class="t2">
                            <asp:Literal ID="txtConceptoPagoInfo" runat="server"></asp:Literal>
                        </li>
                        <li class="t1"><span class="color">>></span>Monto: </li>
                        <li class="t2">
                            <asp:Literal ID="txtMontoInfo" runat="server"></asp:Literal>
                        </li>
                        <li class="t1"><span class="color">>></span>Servicio: </li>
                        <li class="t2">
                            <asp:Literal ID="txtServicioInfo" runat="server"></asp:Literal>
                        </li>
                        <li class="t1"><span class="color">>></span>Estado: </li>
                        <li class="t2">
                            <asp:Literal ID="txtEstadoInfo" runat="server"></asp:Literal>
                        </li>
                        <li class="t1"><span class="color">>></span>Fecha de Cancelaci&oacute;n: </li>
                        <li class="t2">
                            <asp:TextBox ID="txtFechaPago" runat="server" Enabled="false"></asp:TextBox>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <h4>
                        3. Datos a Actualizar</h4>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">>></span>Tipo de Extorno: </li>
                        <li class="t2" style="line-height: 1; z-index: 200;">
                            <p class="input_cal">                                
                                <asp:DropDownList ID="ddlTipoExtorno" runat="server" CssClass="neu">
                                    <asp:ListItem Text="Extorno Eliminacion" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Extorno Pendiente" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </p>
                        </li>
                        <li class="complet" style="line-height: 1; z-index: 100;">
                            <br />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 200px;
                                padding-bottom: 19px" />
                            <asp:Button ID="btnExtornar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                                Text="Extornar" ValidationGroup="ValidaCampos" />
                            &nbsp;<asp:Button ID="btnNuevo" runat="server" CssClass="input_azul3" Text="Nuevo"
                                Visible="false" />
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                </div>
                
                <asp:Label ID="lblMensaje" runat="server" Style="padding-left: 30px; color: Blue"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">

        function LimpiarResultado() {
            $('#' + '<%= lblMensaje.ClientID %>').text("");
        }
    </script>
</asp:Content>
