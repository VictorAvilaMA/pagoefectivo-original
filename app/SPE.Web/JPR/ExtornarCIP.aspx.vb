﻿Imports SPE.Web
Imports SPE.Entidades
Imports SPE.Web.Util
Imports _3Dev.FW.Util.DataUtil

Partial Class JPR_ExtornarCIP
    Inherits PaginaBase

    Protected Sub ibtnBuscarCIP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ibtnBuscarCIP.Click
        'LimpiarSesion()
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        oBEOrdenPago.IdOrdenPago = txtNroCIP.Text.Trim()
        oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
        Dim result As BEOrdenPago = oCOrdenPago.ConsultarOrdenPagoPorId(oBEOrdenPago)


        Session("RespuestaConsulta") = result
        If result.DescripcionEstado Is Nothing Or Not result.IdEstado = 23 Then
            divDatos.Visible = False
            JSMessageAlert("Validación", "El número de CIP ingresado no existe o no se encuentra cancelado.", "val")
        Else
            divDatos.Visible = True
            txtNumeroCIP.Text = result.NumeroOrdenPago
            txtConceptoPagoInfo.Text = result.ConceptoPago
            txtMontoInfo.Text = result.SimboloMoneda + " " + result.Total.ToString()
            txtServicioInfo.Text = result.DescripcionServicio
            txtEstadoInfo.Text = result.DescripcionEstado
            If result.FechaCancelacion.ToString("dd/MM/yyyy") = "01/01/0001" Then
                txtFechaPago.Text = String.Empty
            Else
                txtFechaPago.Text = result.FechaCancelacion.ToString("dd/MM/yyyy hh:mm:ss tt")
            End If
        End If
    End Sub




    Protected Sub btnExtornar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExtornar.Click
        Dim oCOrdenPago As New COrdenPago
        Dim oBEOrdenPago As New BEOrdenPago
        Dim oBEOrdenPagoSession As New BEOrdenPago
        Dim fechaExpiracion As New DateTime
        Dim cemail As New SPE.Web.Util.UtilEmail()

        Try
            oBEOrdenPago.NumeroOrdenPago = txtNroCIP.Text.PadLeft(14, "0")
            'oBEOrdenPago.IdUsuarioActualizacion = UserInfo.IdUsuario
            Dim intResult As Int64 = oCOrdenPago.ExtornarCIP(oBEOrdenPago.NumeroOrdenPago, ddlTipoExtorno.SelectedValue)
            Select Case intResult
                Case Is > 0
                    'Dim intRpta As Integer = cemail.EnviarEmailCambioDeEstadoCipDeExpAGen(oBEOrdenPago)
                    'If intRpta = 1 Then
                    intResult = oBEOrdenPago.NumeroOrdenPago
                    lblMensaje.Text = ("Se actualizó con éxito el CIP Nº " & intResult.ToString().PadLeft(14, "0"))
                    DesactivarControles()
                    'LimpiarSesion()
                    btnNuevo.Visible = True
                    'Else
                    'Throw New Exception
                    'End If
                Case -1
                    JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
                Case -2
                    JSMessageAlert("Validación", "El cip ingresado no existe", "val")
                Case -3
                    JSMessageAlert("Validación", "El cip ingresado no pertenece a su empresa", "val")
                Case -4
                    JSMessageAlert("Validación", "El cip ingresado ya se encuentra expirado", "val")
                Case -5
                    JSMessageAlert("Validación", "El cip ingresado no se encuentra en estado generado", "val")
                Case -6
                    JSMessageAlert("Validación", "La fecha de expiración debe ser mayor a la de ahora", "val")
                Case -7
                    JSMessageAlert("Validación", "La fecha de expiración no puede exeder los 10 años", "val")
                Case Else
                    JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
            End Select
        Catch ex As Exception
            JSMessageAlert("Validación", "Hubo un error en la aplicación", "val")
        End Try

    End Sub
    Private Sub DesactivarControles()
        txtNroCIP.Enabled = False
        btnExtornar.Enabled = False
        btnExtornar.Visible = False
        ibtnBuscarCIP.Enabled = False
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Response.Redirect("ExtornarCIP.aspx")
    End Sub
End Class
