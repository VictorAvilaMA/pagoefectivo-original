Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Partial Class PRConOpeBan
    Inherits System.Web.UI.Page
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Protected Sub gvEmpresa_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Protected Sub gvEmpresa_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    End Sub


    Sub ConciliacionBancaria()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False

        Me.lblMensaje.Text = ""
        Dim objControladoraConciliacion As New SPE.Web.CAgenciaBancaria
        Dim objConciliacionBancaria As New BEConciliacion
        ' CargarDatosConciliacion()
        objConciliacionBancaria = CargarDatosConciliacion()
        If Not objConciliacionBancaria Is Nothing Then
            objConciliacionBancaria = objControladoraConciliacion.RegistrarConciliacionBancaria(objConciliacionBancaria)
            If Not objConciliacionBancaria.ListaOperacionesConciliadas Is Nothing Then
                If objConciliacionBancaria.ListaOperacionesConciliadas.Count > 0 Then
                    Me.pnlConciliadas.Visible = True
                    Me.gvResultado.DataSource = objConciliacionBancaria.ListaOperacionesConciliadas
                    Me.gvResultado.DataBind()
                Else
                    Me.pnlConciliadas.Visible = False
                End If
                Me.lblMensaje.Visible = True

                Me.lblMensaje.Text = "Se conciliaron " + objConciliacionBancaria.ListaOperacionesConciliadas.Count.ToString + " operaciones bancarias."
            End If
            If Not objConciliacionBancaria.ListaOperacionesNoConciliadas Is Nothing Then
                If objConciliacionBancaria.ListaOperacionesNoConciliadas.Count > 0 Then
                    Me.pnlNoConciliadas.Visible = True
                    Me.gvResultadoDos.DataSource = objConciliacionBancaria.ListaOperacionesNoConciliadas
                    Me.gvResultadoDos.DataBind()

                Else
                    Me.pnlNoConciliadas.Visible = False
                End If
            End If
            If objConciliacionBancaria.ListaOperacionesConciliadas Is Nothing And objConciliacionBancaria.ListaOperacionesNoConciliadas Is Nothing Then
                'lblMensaje.Visible = True
                'lblMensaje.Text = "El archivo ya ha sido conciliado anteriormente."
                MostrarValidacionFileUpload(True, "El archivo ya ha sido conciliado anteriormente.")
            End If

        Else

            'lblMensaje.Visible = True
            'lblMensaje.Text = "El archivo no es válido para la conciliación."
            MostrarValidacionFileUpload(True, "El archivo no es válido para la conciliación.")
            Ocultar()
        End If
    End Sub
    Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub

    Function CargarDatosConciliacion() As BEConciliacion
        Ocultar()
        Dim objConciliacion As New BEConciliacion

        Dim file As String = Me.fulArchivoConciliacion.FileName
        Dim extension As String = IO.Path.GetExtension(file)
        'Dim fullPath As String = fulArchivoConciliacion.PostedFile.FileName
        'Dim x As New System.IO.StreamReader(fulArchivoConciliacion.PostedFile.InputStream)

        'If fullPath <> "" Then
        Dim sr As New System.IO.StreamReader(fulArchivoConciliacion.PostedFile.InputStream)

        If Not fulArchivoConciliacion.PostedFile Is Nothing And fulArchivoConciliacion.PostedFile.ContentLength > 0 Then

            If extension.ToUpper = ".TXT" Then
                If fulArchivoConciliacion.HasFile Then
                    objConciliacion.NombreArchivo = Me.fulArchivoConciliacion.FileName.ToString().Trim
                    objConciliacion.ListaOperacionesBancarias = ListaOperacionesBancarias(sr)
                    sr.Close()
                    Return objConciliacion
                End If
            Else
                Return Nothing
            End If

        Else
            Return Nothing
        End If
        Return Nothing
    End Function
    Function ListaOperacionesBancarias(ByVal sr As System.IO.StreamReader) As List(Of String)
        Dim objListaOperacionesBancarias As New List(Of String)
        Do While sr.Peek() > 0
            objListaOperacionesBancarias.Add(sr.ReadLine)
        Loop

        Return objListaOperacionesBancarias
    End Function
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        ConciliacionBancaria()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Ocultar()
        Response.Redirect("~/JPR/COConc.aspx")
    End Sub
    Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.gvResultadoDos.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
        Me.pnlNoConciliadas.Visible = False

    End Sub

End Class