Imports System.Data
Imports System.Collections.Generic
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Web.Security
Imports SPE.Entidades

Partial Class ARE_PRLiCaBl
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CAdministrarAgenciaRecaudadora)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CargarCajasCerradas()
        End If
    End Sub

    Private Function LiquidarCaja() As String

        Dim control As CheckBox, mensaje As String, Ejecutar As Boolean = False
        Dim ListAgenteCaja As New List(Of SPE.Entidades.BEAgenteCaja)
        Dim ListCajas As List(Of SPE.Entidades.BEAgenteCaja) = CType(Session("Cajas"), List(Of SPE.Entidades.BEAgenteCaja))
        Dim obeAgenteCaja As SPE.Entidades.BEAgenteCaja
        Dim objCAgenteCaja As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        mensaje = "Debe seleccionar por lo menos una caja."

        Try

            For Each row As GridViewRow In gvCajasCerradas.Rows
                control = CType(row.FindControl("ckbLiquidar"), CheckBox)
                If (control.Checked = True) Then
                    obeAgenteCaja = New SPE.Entidades.BEAgenteCaja
                    obeAgenteCaja.IdAgenteCaja = ListCajas(row.RowIndex).IdAgenteCaja
                    obeAgenteCaja.IdUsuarioActualizacion = UserInfo.IdUsuario
                    ListAgenteCaja.Add(obeAgenteCaja)
                    Ejecutar = True
                End If
            Next

            If Ejecutar = True Then
                '
                Dim objParametro As New SPE.Web.CAdministrarParametro
                Dim obeOrdenPago As New BEOrdenPago
                '
                obeOrdenPago.IdEnteGenerador = SPE.EmsambladoComun.ParametrosSistema.EnteGeneradorOP.AgenciaRecaudadora
                obeOrdenPago.IdEnte = Convert.ToInt32(lblIdAgencia.Text)
                obeOrdenPago.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoOrdenPago.Generada
                obeOrdenPago.IdServicio = Convert.ToInt32(objParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoTipoServicioOP).Item(0).Descripcion)
                obeOrdenPago.FechaEmision = objCAdministrarComun.ConsultarFechaHoraActual()
                obeOrdenPago.Total = 0
                obeOrdenPago.IdUsuarioCreacion = UserInfo.IdUsuario
                '
                objCAgenteCaja.LiquidarCajaBloque(obeOrdenPago, ListAgenteCaja)
                mensaje = "La operaci�n se realiz� con �xito."
                Session.Remove("Cajas")
                CargarCajasCerradas()
            End If

        Catch ex As Exception
            'Logger.LogException(ex)
            lblMensaje.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
        End Try

        Return mensaje
    End Function

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolCajero, SPE.EmsambladoComun.ParametrosSistema.RolSupervisor})
    'End Sub

    Private Sub CargarCajasCerradas()

        Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
        Dim listCajasCerradas As New List(Of BEAgenteCaja)
        Dim obeAgenteCaja As New BEAgenteCaja
        Dim obeAgente As New BEAgente
        obeAgente = objCAdministrarAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(2, UserInfo.IdUsuario)
        lblIdAgencia.Text = obeAgente.IdAgenciaRecaudadora
        obeAgenteCaja.IdUsuarioCreacion = UserInfo.IdUsuario
        obeAgenteCaja.OrderBy = SortExpression
        obeAgenteCaja.IsAccending = SortDir
        listCajasCerradas = objCAdministrarAgenciaRecaudadora.ConsultarCajasCerradas(obeAgenteCaja)
        Session("Cajas") = listCajasCerradas

        gvCajasCerradas.DataSource = CType(Session("Cajas"), List(Of SPE.Entidades.BEAgenteCaja))
        gvCajasCerradas.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listCajasCerradas.Count)
        '
        If gvCajasCerradas.Rows.Count = 0 Then
            btnLiquidar.Enabled = False
            btnCancelar.Enabled = False
        Else
            btnLiquidar.Enabled = True
            btnCancelar.Enabled = True
        End If
        '
    End Sub

    Protected Sub btnLiquidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLiquidar.Click

        lblMensaje.Text = LiquidarCaja()
        '
        If lblMensaje.Text <> "Debe seleccionar por lo menos una caja." Then
            '
            Dim url As String = "PRLiCaBlReporte.aspx?IdUsuario=" + UserInfo.IdUsuario.ToString
            '
            Dim script As String = "<script>window.open('" & _
                                            url & _
                                            "')</script>"
            ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", script)
            '
        End If
        '
    End Sub

    Protected Sub ibtnVerDetalle_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)

        Dim IdAgente As Integer
        Dim control As Label
        control = CType(gvCajasCerradas.Rows(CInt(e.CommandArgument)).FindControl("lblId"), Label)
        IdAgente = CInt(control.Text)
        control = CType(gvCajasCerradas.Rows(CInt(e.CommandArgument)).FindControl("lblNombreCaja"), Label)
        DetalleCaja(IdAgente)
        lblTitulo.Text = "Resumen de la " + control.Text
        mppDetalleCaja.Show()

    End Sub

    Private Sub DetalleCaja(ByVal IdAgenteCaja As Integer)
        Dim objCDetalleCaja As New SPE.Web.CAdministrarAgenciaRecaudadora
        gvResultado.DataSource = objCDetalleCaja.ConsultarDetalleCaja(IdAgenteCaja)
        gvResultado.DataBind()
    End Sub
    '
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("~\ARE\PgPrl.aspx")
    End Sub

    Protected Sub gvCajasCerradas_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCajasCerradas.PageIndexChanging
        '
        gvCajasCerradas.PageIndex = e.NewPageIndex
        CargarCajasCerradas()
        '
    End Sub

    Protected Sub gvCajasCerradas_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvCajasCerradas.Sorting
        '
        SortExpression = e.SortExpression
        If (SortDir.Equals(SortDirection.Ascending)) Then
            SortDir = SortDirection.Descending
        Else
            SortDir = SortDirection.Ascending
        End If
        CargarCajasCerradas()
        '
    End Sub

    Protected Sub ckbLiquidarTodos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chktodos As CheckBox = CType(sender, CheckBox)
        For Each gvr As GridViewRow In gvCajasCerradas.Rows
            CType(gvr.FindControl("ckbLiquidar"), CheckBox).Checked = chktodos.Checked
        Next
    End Sub
End Class


