<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRLiEs.aspx.vb" Inherits="JPR_PRLiEs" Title="Liquidar Caja en Bloque POS" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Liquidar Caja en Bloque</h2>
    <asp:Label ID="lblIdComercio" runat="server" Text="" Visible="false" />
    <div class="conten_pasos3">
        <ul class="datos_cip2">
            <asp:UpdatePanel ID="uplLiquidacion" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divOrdenesPago" class="result">
                        <div id="div7">
                            <h5 id="h5Resultado" runat="server">
                                <asp:Literal ID="lblResultado" runat="server" Text="" />
                            </h5>
                        </div>
                        <asp:GridView ID="gvComercio" DataKeyNames="idComercio" CssClass="grilla" runat="server"
                            AutoGenerateColumns="False" CellPadding="3" BackColor="White" BorderColor="#CCCCCC"
                            BorderStyle="None" BorderWidth="1px" ShowFooter="False" AllowPaging="True">
                            <Columns>
                                <asp:BoundField Visible="False" DataField="idComercio">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ControlStyle CssClass="Hidden" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="C&#243;digo" DataField="CodComercio" SortExpression="CodComercio">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Comercio" DataField="RazonSocial" SortExpression="FechaApertura">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Fecha M&#237;nima " DataField="FechaMinima" SortExpression="FechaApertura">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaMaxima" HeaderText="Fecha M&#225;xima" SortExpression="FechaCierre">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="#" DataField="Cantidad" SortExpression="NombreCaja">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnVerDetalle" runat="server" ImageUrl="~/images/lupa.gif"
                                            ToolTip="Ver Detalle" OnCommand="ibtnVerDetalle_Command" CommandArgument="<%# Ctype(container,gridviewrow).rowindex %>" />
                                        <asp:Label ID="lblNombreCaja" runat="server" Visible="False" Width="1px"></asp:Label>
                                        <asp:Label ID="lblId" runat="server" Visible="False"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gvComercio" EventName="PageIndexChanging" />
                </Triggers>
            </asp:UpdatePanel>
        </ul>
        <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
        <ul id="Ul1" class="datos_cip2 h0">
            <li class="complet">
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" Text="Refrescar" />
            </li>
        </ul>
    </div>
</asp:Content>
