
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class JPR_PRLiEs
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CComercio)



   
    'Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
    '    Get
    '        Return btnBuscar
    '    End Get
    'End Property
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeComercio As New SPE.Entidades.BEComercio
        ' obeComercio.razonSocial = txtRazonSocial.Text
        '  obeComercio.ruc = txtRUC.Text
        ' obeComercio.contacto = txtContacto.Text
        '  obeComercio.codComercio = txtCodigo.Text

        ' If ddlIdEstado.SelectedValue.ToString = "" Then
        'obeComercio.idEstado = 0
        ' Else
        '  obeComercio.idEstado = CInt(ddlIdEstado.SelectedValue)
        ' End If


        Return obeComercio

    End Function

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        End If

    End Sub



    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CComercio
    'End Function

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvComercio
        End Get
    End Property
    'Public Overrides Sub OnAfterSearch()
    '    MyBase.OnAfterSearch()
    '    If Not ResultList Is Nothing Then
    '        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
    '    Else
    '        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
    '    End If

    'End Sub
    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADCom"
        e.MaintenancePageName = "ADCom.aspx"
        e.ExecuteSearchOnFirstLoad = True

    End Sub
    Private Sub LlenarComercio()
        Dim objBusinessEntityBase As New List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim obeComercio As New List(Of BEComercio)
        Dim objCComercio As New SPE.Web.CComercio()
        obeComercio = objCComercio.GetComercioWithAO()
        gvComercio.DataSource = obeComercio
        gvComercio.DataBind()

    End Sub
    Public Overrides Sub OnFirstLoadPage()

        LlenarComercio()




        'Dim objCParametro As New SPE.Web.CAdministrarParametro()
        '_3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlIdEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        '"Descripcion", "Id", "::: Todos :::")

        'Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        '_3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlOperador, objCEstablecimiento.ObtenerOperadores(), "Descripcion", "IdOperador", "::: Todos :::")

    End Sub
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvComercio.DataKeys(e.RowIndex).Values(0)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then



            '        '
            '        If Not Page.IsPostBack Then
            '            'Page.Form.DefaultButton = btnBuscar.UniqueID
            '            'Page.SetFocus(txtRazonSocial)
            '        End If
            '        'Dim ds As New DataTable


            '        ''ds.Columns.Add("FechaMinima", Type.GetType("System.String"))
            '        ''ds.Columns.Add("FechaMaxima", Type.GetType("System.String"))
            '        ''ds.Columns.Add("Caja", Type.GetType("System.String"))


            '        'ds.Columns.Add("Comercio", Type.GetType("System.String"))
            '        'ds.Columns.Add("FechaMinima", Type.GetType("System.String"))
            '        'ds.Columns.Add("FechaMaxima", Type.GetType("System.String"))
            '        'ds.Columns.Add("Caja", Type.GetType("System.String"))




            '        'Dim Row As DataRow = ds.NewRow()

            '        'Row(0) = "BTL"
            '        'Row(1) = "12/01/2009"
            '        'Row(2) = "23/01/2009"
            '        'Row(3) = "9"

            '        'ds.Rows.Add(Row)

            '        'gvCajasCerradas.DataSource = ds
            '        'gvCajasCerradas.DataBind()




        End If
    End Sub



    Protected Sub ibtnVerDetalle_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim index As Integer = e.CommandArgument
        'Dim idComercio As Integer = Convert.ToInt32(gvComercio.Rows(index).Cells(0).Text)
        Dim idComercio As Integer = Convert.ToInt32(gvComercio.DataKeys(index).Value)
        'Dim codComercio As String = gvComercio.Rows(index).Cells(0).Text
        Dim NomComercio As String = gvComercio.Rows(index).Cells(2).Text
        Dim CodComercio As String = gvComercio.Rows(index).Cells(1).Text
        'Context.Items.Add("CodComercio", "")
        Context.Session.Add("NomComercio", NomComercio)
        Context.Session.Add("CodComercio", CodComercio)
        Response.Redirect("PRLiEs1.aspx?idCo=" + idComercio.ToString())
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        LlenarComercio()
    End Sub

    Protected Sub gvComercio_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvComercio.PageIndexChanging


        Dim idComercio As String = Request.QueryString("idCo")
        gvComercio.PageIndex = e.NewPageIndex
        LlenarComercio()





    End Sub
End Class
