<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="PRLiEs1.aspx.vb" Inherits="JPR_PRLiEs1" title="Liquidar Caja en Bloque POS" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager" runat="server">
</asp:ScriptManager>
<h1>Liquidar Caja en Bloque</h1>
<div class="inner-col-right dlgrid">
	<asp:Panel ID="pnlDatosUsuario" runat="server" CssClass="w100 clearfix dlinline">
	<fieldset>
		<h4>1. Información de Comercio</h4>
		<div class="w100 clearfix dlinline">
			<dl class="clearfix dt15 dd30">
				<dt class="desc">
					<asp:Label ID="lblComercio" CssClass="EtiquetaLargo" runat="server" Text="CodComercio:"></asp:Label>
				</dt>
				<dd class="camp">
					<asp:Label ID="txtComercio" CssClass="EtiquetaLargo2" runat="server" style="text-align: left"></asp:Label>
					<asp:Label ID="lblidComercio" runat="server" Visible="False" CssClass="Hidden"></asp:Label>
				</dd>
			</dl>
		</div>
		<div class="w100 clearfix dlinline">
			<dl class="clearfix dt15 dd30">
				<dt class="desc">
					<asp:Label ID="lblRazonSocial" CssClass="EtiquetaLargo" runat="server" Text="Razon Social:"></asp:Label>
				</dt>
				<dd class="camp">
					<asp:Label ID="txtRazonSocial" CssClass="EtiquetaLargo" runat="server" style="text-align: left"></asp:Label>
				</dd>
			</dl>
		</div>		
	</fieldset>		
	</asp:Panel>
     <asp:UpdatePanel ID="upnlGrid" runat="server" UpdateMode="Conditional">     
     <ContentTemplate>	
            <div id="div1" class="w100 clearfix dlinline" style="left: 0px; top: 0px;">       
	          <div id="div2" class="divContenedorTitulo" >
                <asp:Label ID="lblResultado" runat="server" Text=""/>
                <asp:Label ID="lblIdAgencia" runat="server" Text="" Visible="false"/>
              </div>
              <div id="divOrdenesPago" class="divSubContenedorGrilla" >
                  <asp:GridView ID="gvPendientes"  CssClass="grilla" runat="server" AutoGenerateColumns="False" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" ShowFooter="False" AllowPaging="True" PageSize="7" DataKeyNames="IdAperturaOrigen" OnPageIndexChanging="gvPendientes_PageIndexChanging">
                  <Columns>
                        <asp:BoundField Visible = "False"  DataField="idEstablecimiento" >
                          <ItemStyle HorizontalAlign="Center" />
                            <ControlStyle CssClass="Hidden" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Serie POS" DataField="SerieTerminal" SortExpression="SerieTerminal" >
                          <ItemStyle HorizontalAlign="Center" />
                      </asp:BoundField>
                   
                      <asp:BoundField HeaderText="Fecha Apertura" DataField="FechaApertura" SortExpression="FechaApertura">
                          <ItemStyle HorizontalAlign="Center" />
                      </asp:BoundField>
                      <asp:BoundField DataField="FechaCierre" HeaderText="Fecha Cierre" SortExpression="FechaCierre">
                          <ItemStyle HorizontalAlign="Center" />
                      </asp:BoundField>
                      <asp:BoundField DataField="NumOperaciones" HeaderText="C.I.P." SortExpression="NumOperaciones">
                          <ItemStyle HorizontalAlign="Center" />
                      </asp:BoundField>
                    
                      <asp:TemplateField HeaderText="Liquidar">
                          <HeaderTemplate >
                              <asp:Label ID="lblLiquidar" runat="server" Text="Liquidar"></asp:Label> 
                              <asp:CheckBox ID="ckbLiquidarTodos" runat="server" CausesValidation="True" Text="" Width="40px" AutoPostBack="True" OnCheckedChanged="ckbLiquidarTodos_CheckedChanged"   />
                          </HeaderTemplate>                      
                          <ItemTemplate>
                            <asp:CheckBox ID="ckbLiquidar" runat="server" CausesValidation="True" />
                            </ItemTemplate>
                          <ItemStyle HorizontalAlign="Center" Width="60px" />
                          <HeaderStyle HorizontalAlign="Center" Width="55px" />
                      </asp:TemplateField>
                      <asp:TemplateField>
                          <ItemTemplate>
                              <asp:ImageButton ID="ibtnVerDetalle" runat="server" ImageUrl="~/images/lupa.gif" CommandArgument="<%# Ctype(container,gridviewrow).rowindex %>"
                                  ToolTip="Ver Detalle" OnCommand="ibtnVerDetalle_Command"   />
                              <asp:Label ID="lblIdAperturaOrigen" Text='<%# Eval("idAperturaOrigen") %>' runat="server"  Visible="False" CssClass= "Hidden"
                                  Width="1px"></asp:Label>
                               <asp:Label ID="lblId" runat="server" Visible="False" ></asp:Label>
                          </ItemTemplate>
                      </asp:TemplateField>
                  </Columns>
                  <HeaderStyle CssClass="cabecera" />
                  <EmptyDataTemplate>No existen cajas para liquidar </EmptyDataTemplate>
                  </asp:GridView>     
              </div>
            </div>	
	
	
	<asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
	<br />
            </ContentTemplate>
         <Triggers>
             <asp:AsyncPostBackTrigger ControlID="gvPendientes" EventName="PageIndexChanging" />
         </Triggers>
            
       </asp:UpdatePanel>     	
	<asp:Panel ID="pnlBotones" runat="server" Height="50px" Width="358px" Enabled="true">
		<asp:UpdatePanel ID="uplButtons" runat="server" UpdateMode="Conditional">
		<ContentTemplate>	
		<fieldset id="fsBotonera" class="w100 clearfix dlinline btns2">
		    <asp:Button ID="btnLiquidar" runat="server" Text="Liquidar Cajas"  CssClass="btnLiquidar" />
		    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar"  CssClass="btnCancelar" />
		</fieldset>		
		</ContentTemplate>
			<Triggers>
				<asp:PostBackTrigger ControlID="btnLiquidar" />
			</Triggers>
		</asp:UpdatePanel>                                     		
	</asp:Panel>	
    <cc1:ModalPopupExtender ID="mppDetalleXComerio"  runat="server" BackgroundCssClass="modalBackground" CancelControlID="imgbtnRegresar" X="300" Y="200"  PopupControlID="pnlPopupDetallexComerio" TargetControlID="btnPopup">
    </cc1:ModalPopupExtender>	
	<asp:Button ID="btnPopup" runat="server" BackColor="White" BorderColor="White" BorderStyle="None" Enabled="False" Width="1px" />
	<asp:Panel ID="pnlPopupDetallexComerio" runat="server"  style="display:none;"  >
	    <div id="divBusquedaRepresentantes" class="divContenedor" style="left: 1px; top: 0px; text-align: center;">
		 <div id="div11" class="divContenedorTitulo">
		   <div style="FLOAT: left">
			   <asp:Label ID="lblTitulo" runat="server" Text="Resumen"></asp:Label>
		   </div>
		   <div style="FLOAT: right">
			 <asp:ImageButton id="imgbtnRegresar"  ImageUrl="~/img/closeX.GIF"  CausesValidation="false"   runat="server"></asp:ImageButton> 
		   </div>
		 </div>	
	    <asp:UpdatePanel ID="upnlListaPendientes" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
                 
            <div style="MARGIN-LEFT: 10px">&nbsp;
            </div>
            <div>
                <asp:GridView id="gvResultado" runat="server" AutoGenerateColumns="False" CssClass="grilla">
                 <Columns>
                   <asp:BoundField DataField="SerieTerminal" HeaderText="Cod. POS"></asp:BoundField>
                   <asp:BoundField DataField="medioPago" HeaderText="MedioPago"></asp:BoundField>
                   <asp:BoundField DataField="moneda" HeaderText="Moneda">
                    <ItemStyle HorizontalAlign="Center" />
                  </asp:BoundField>
                  <asp:BoundField DataField="MontoTotal" HeaderText="Monto" DataFormatString="{0:#,#0.00}">
                 <HeaderStyle HorizontalAlign="Center" />
                  <ItemStyle HorizontalAlign="Right" />
                 </asp:BoundField>
                </Columns>
                <HeaderStyle CssClass="cabecera"></HeaderStyle>
            </asp:GridView> 
            </div>

            </ContentTemplate>
            <Triggers>

            </Triggers>
		</asp:UpdatePanel>    	
		</div>
	</asp:Panel>
</div>
</asp:Content>

