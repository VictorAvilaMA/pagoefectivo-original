Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Web.Log
Imports SPE.Web


Partial Class JPR_PRLiEs1
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim idComercio As Integer

        If Not Page.IsPostBack Then
            idComercio = Request.QueryString("idCo")
            cargarPendientes(idComercio)
            txtComercio.Text = Convert.ToString(Context.Session("CodComercio"))
            txtRazonSocial.Text = Convert.ToString(Context.Session("NomComercio"))
        End If
    End Sub

    Private Sub cargarPendientes(ByVal idComercio As String)

        Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        Dim objBEComercio As New BEComercio
        Dim objListEstablecimientos As New List(Of BEEstablecimiento)
        objBEComercio.idComercio = idComercio
        objListEstablecimientos = objCEstablecimiento.GetPendientesCancelacion(objBEComercio)
        If (objListEstablecimientos.Count > 0) Then
            txtRazonSocial.Text = objListEstablecimientos(0).RazonSocial
            txtComercio.Text = objListEstablecimientos(0).CodigoComercio
        End If
        gvPendientes.DataSource = objListEstablecimientos
        gvPendientes.DataBind()
    End Sub

    Protected Sub ibtnVerDetalle_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim index As Integer = e.CommandArgument
        'Dim idEstablecimiento As Integer = Convert.ToInt32(gvPendientes.Rows(index).Cells(0).Text)

        Dim id As Integer = gvPendientes.DataKeys(index).Value
        cargarAOPendientesxidComercio(id)
        mppDetalleXComerio.Show()


    End Sub
    Private Sub cargarAOPendientesxidComercio(ByVal idEstablecimiento As Integer)
        Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        Dim objEstablecimiento As New BEEstablecimiento
        objEstablecimiento.IdEstablecimiento = idEstablecimiento
        gvResultado.DataSource = objCEstablecimiento.GetLiquidacionPendientexIdComercio(objEstablecimiento)
        gvResultado.DataBind()
    End Sub

    Protected Sub btnLiquidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLiquidar.Click

        Dim control As CheckBox, mensaje As String, Ejecutar As Boolean = False, ok As String
        ' Dim ListAgenteCaja As New List(Of SPE.Entidades.BEAgenteCaja)
        'Dim ListCajas As List(Of SPE.Entidades.BEAgenteCaja) = CType(Session("Cajas"), List(Of SPE.Entidades.BEAgenteCaja))

        Dim obeListBEAperturaOrigen As New List(Of BEAperturaOrigen)
        Dim obeBEAperturaOrigen As BEAperturaOrigen
        Dim objCEstablecimiento As New CEstablecimiento


        mensaje = "Debe seleccionar por lo menos una caja."
        ok = "La operaci�n se realiz� con exito"


        Try

            For Each row As GridViewRow In gvPendientes.Rows
                control = CType(row.FindControl("ckbLiquidar"), CheckBox)
                If (control.Checked = True) Then
                    obeBEAperturaOrigen = New BEAperturaOrigen

                    Dim id As String = CType(row.FindControl("lblIdAperturaOrigen"), Label).Text
                    obeBEAperturaOrigen.idEstablecimiento = id
                    obeListBEAperturaOrigen.Add(obeBEAperturaOrigen)


                    Ejecutar = True
                End If
            Next

            If Ejecutar = True Then

                objCEstablecimiento.LiquidarBloque(obeListBEAperturaOrigen)
                Dim idComercio As String = Request.QueryString("idCo")
                cargarPendientes(idComercio)

                Dim url As String = "PRLiEsReporte.aspx?Id=" + "Liquidar"

                Dim script As String = "<script>window.open('" & _
                                                        url & _
                                                        "')</script>"
                ClientScript.RegisterStartupScript(Me.GetType(), "PopupScript", script)
                lblMensaje.Text = ok

            Else
                lblMensaje.Text = mensaje
            End If


        

        

        Catch ex As Exception
            'Logger.LogException(ex)
            lblMensaje.CssClass = "MensajeValidacion"
            lblMensaje.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente

        End Try

        'Return mensaje
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Response.Redirect("PRLiEs.aspx")
    End Sub

    Protected Sub ckbLiquidarTodos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chktodos As CheckBox = CType(sender, CheckBox)
        For Each gvr As GridViewRow In gvPendientes.Rows
            CType(gvr.FindControl("ckbLiquidar"), CheckBox).Checked = chktodos.Checked
        Next
    End Sub

 

    Protected Sub gvPendientes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)


        Dim idComercio As String = Request.QueryString("idCo")
        gvPendientes.PageIndex = e.NewPageIndex
        cargarPendientes(idComercio)
    

    End Sub
End Class
