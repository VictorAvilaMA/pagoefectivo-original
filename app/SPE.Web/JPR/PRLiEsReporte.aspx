<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PRLiEsReporte.aspx.vb" Inherits="JPR_PRLiEsReporte"
    Title="PagoEfectivo - Reporte de Cajas Liquidadas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(function () {
            var waitMsg = $("div[id$='AsyncWait_Wait']");
            waitMsg.wrap("<div style='display:none; visibility: hidden'></div>");
        });
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True">
    </asp:ScriptManager>
    <h2>
        Reporte de Cajas Liquidadas
    </h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanelImagen" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False"
            RenderMode="Inline">
            <ContentTemplate>
                <ul id="datos_cip2" class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt; </span>Comercio: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlComercio" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>C.I.P.: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtNumeroOrdenPago" runat="server" CssClass="neu"></asp:TextBox>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha Del: </li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtDel" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="imgDel" runat="server" ImageUrl="~/App_Themes/SPE/img/calendario.png"
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="mexDel" runat="server" Mask="99/99/9999" MaskType="Date"
                                TargetControlID="txtDel" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtAl" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="imgAl" runat="server" ImageUrl="~/App_Themes/SPE/img/calendario.png"
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="mexAl" runat="server" Mask="99/99/9999" MaskType="Date"
                                TargetControlID="txtAl" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <cc1:CalendarExtender ID="cexDel" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgDel"
                            TargetControlID="txtDel">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="cexAl" runat="server" PopupButtonID="imgAl" TargetControlID="txtAl"
                            Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="mexDel"
                            ControlToValidate="txtDel" ErrorMessage="*" InvalidValueMessage="Fecha de inicio no v�lida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha de inicio es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="mexAl"
                            ControlToValidate="txtAl" ErrorMessage="*" InvalidValueMessage="Fecha de fin no v�lida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha de fin es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*"></cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="cvaAl" runat="server" ControlToCompare="txtAl" ControlToValidate="txtDel"
                            ErrorMessage="Rango de fechas no es v�lido" Operator="LessThanEqual" Type="Date">*</asp:CompareValidator>
                        <cc1:FilteredTextBoxExtender ID="FTBE_txtNumeroOrdenPago" runat="server" ValidChars="0123456789"
                            TargetControlID="txtNumeroOrdenPago">
                        </cc1:FilteredTextBoxExtender>
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" Style="font-size: 11pt;
                            height: auto; width:auto; padding-bottom: 15px;" />
                        <div style="clear: both;">
                        </div>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnVer" runat="server" Text="Ver Reporte" CssClass="input_azul5" />
                        <asp:Button ID="btnExportarPDF" runat="server" CssClass="input_azul4" Enabled="false"
                            Text="PDF" />
                        <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Text="EXCEL"
                            Enabled="false" />
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <%--                <asp:AsyncPostBackTrigger ControlID="btnVer" EventName="Click" />--%>
                <asp:PostBackTrigger ControlID="btnVer" />
                <asp:PostBackTrigger ControlID="btnExportarExcel" />
                <asp:PostBackTrigger ControlID="btnExportarPDF" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <rsweb:ReportViewer ID="RptCaja" runat="server" Height="720px" Width="800px" ShowRefreshButton="False"
                    ShowExportControls="False" SizeToReportContent="True" ShowPrintButton="False"
                    ShowToolBar="False" ShowWaitControlCancelLink="False" ShowZoomControl="False">
                </rsweb:ReportViewer>
            </ContentTemplate>
            <Triggers>
                <%--   <asp:AsyncPostBackTrigger ControlID="btnVer" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
