Imports SPE.Entidades
Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Imports _3Dev.FW.Web.Security

Partial Class JPR_PRLiEsReporte
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            CargarDatos()
            Dim Liq As String = Request.QueryString("Id")
            If Liq <> String.Empty Then
                CargarReporte()

            End If


        End If
    End Sub
    Private Sub CargarDatos()
        '
        'FECHAS
        txtDel.Text = Today.ToShortDateString
        txtAl.Text = Today.ToShortDateString
        Dim objComercio As New SPE.Web.CComercio



        Dim obeComercio As New BEComercio



        ddlComercio.DataSource = objComercio.SearchByParameters(obeComercio)

        ddlComercio.DataValueField = "idComercio"
        ddlComercio.DataTextField = "RazonSocial"
        ddlComercio.DataBind()









        'Dim objComun As New SPE.Web.CAdministrarComun
        'Dim objParametro As New SPE.Web.CAdministrarParametro
        'Dim objCAdministrarServicio As New SPE.Web.CServicio
        'Dim objCComun As New SPE.Web.CComun
        'Dim obeServicio As New BEServicio
        'obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario

        
    End Sub

    Protected Sub btnVer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVer.Click
        '
        CargarReporte()
        '
    End Sub
    'CARGAR REPORTE
    Private Sub CargarReporte()
        '
        Try
            '
            RptCaja.Visible = True
            Dim listBEMovimiento As New List(Of BEMovimiento)

            listBEMovimiento = GetListBEMovimiento()

            If RptCaja.LocalReport.DataSources.Count > 0 Then
                RptCaja.LocalReport.DataSources.RemoveAt(0)
            End If

            RptCaja.LocalReport.DataSources.Add(New ReportDataSource("BEMovimiento", listBEMovimiento))
            RptCaja.LocalReport.ReportPath = "Reportes\RptPOSCajasLiquidadas.rdlc"

            RptCaja.LocalReport.SetParameters(GetParametros(listBEMovimiento))
            RptCaja.LocalReport.Refresh()
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente)
        End Try
        '
    End Sub
    'IMAGEN PARA EXPORTAR
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        '
        Dim listBEMovimiento As New List(Of BEMovimiento)
        '
        listBEMovimiento = GetListBEMovimiento()
        '
        If listBEMovimiento.Count > 0 Then
            '
            ProcesoExportar("EXCEL", "xls", listBEMovimiento)
            '
        End If
        '

    End Sub
    Protected Sub btnExportarPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarPDF.Click
        ''
        Dim listBEMovimiento As New List(Of BEMovimiento)
        '
        listBEMovimiento = GetListBEMovimiento()
        '
        If listBEMovimiento.Count > 0 Then
            '
            ProcesoExportar("PDF", "pdf", listBEMovimiento)
            '
        End If
        '
    End Sub

    'OBTENEMOS LOS PARAMETROS
    Private Function GetParametros(ByVal listBEMovimiento As List(Of BEMovimiento)) As List(Of ReportParameter)
        '
        Dim paramList As New List(Of ReportParameter)
        Dim param1 As ReportParameter

        Dim titulo As String

        titulo = "Reporte Liquidar Caja en bloque POS desde el: " & txtDel.Text & " al " & txtAl.Text
     
        If listBEMovimiento.Count > 0 Then
            param1 = New ReportParameter("parTitulo", titulo)
            btnExportarExcel.Enabled = True
            btnExportarPDF.Enabled = True
        Else
            param1 = New ReportParameter("parTitulo", "")
            btnExportarExcel.Enabled = False
            btnExportarPDF.Enabled = False
        End If
        '
        paramList.Add(param1)
        '
        Return paramList
        '
    End Function

    'OBTENEMOS LA LISTA DE AGENTE CAJA
    Private Function GetListBEMovimiento() As List(Of BEMovimiento)
        '
        Dim listBEMovimiento As New List(Of SPE.Entidades.BEMovimiento)
        Dim objCEstablecimiento As New SPE.Web.CEstablecimiento

        Dim obeOrdenPago As New BEOrdenPago

        obeOrdenPago.NumeroOrdenPago = txtNumeroOrdenPago.Text
        obeOrdenPago.FechaCreacion = Convert.ToDateTime(txtDel.Text)
        obeOrdenPago.FechaActualizacion = Convert.ToDateTime(txtAl.Text)
        obeOrdenPago.DataAdicional = Convert.ToInt32(ddlComercio.SelectedValue)

        listBEMovimiento = objCEstablecimiento.ConsultarCajasLiquidadas(obeOrdenPago)

        Return listBEMovimiento
        '
    End Function

    'PROCESO PARA EXPORTAR
    Private Sub ProcesoExportar(ByVal opcion As String, ByVal ext As String, ByVal listBEMovimiento As List(Of BEMovimiento))
        '
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-PE")

        Dim localReport As New LocalReport()

        localReport.ReportPath = Server.MapPath("~\Reportes\RptPOSCajasLiquidadas.rdlc")


        localReport.SetParameters(GetParametros(listBEMovimiento))

        If localReport.DataSources.Count > 0 Then
            localReport.DataSources.RemoveAt(0)
        End If

        localReport.DataSources.Add(New ReportDataSource("BEMovimiento", listBEMovimiento))

        ResponseExport(localReport, opcion, ext)
        '
    End Sub

    'RESONSE DE EXPORTACION
    Private Sub ResponseExport(ByVal localReport As LocalReport, ByVal Type As String, ByVal fileExtension As String)
        '
        Dim reportType As String = Type
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim fileNameExtension As String = fileExtension

        Dim deviceInfo As String = _
        "<DeviceInfo>" + _
        "  <OutputFormat>" + reportType + "</OutputFormat>" + _
        "</DeviceInfo>"

        Dim warnings As Warning() = Nothing
        Dim streams As String() = Nothing
        Dim renderedBytes As Byte()

        'Render the report
        renderedBytes = localReport.Render( _
            reportType, _
            deviceInfo, _
            mimeType, _
            encoding, _
            fileNameExtension, _
            streams, _
            warnings)

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=ReportePagoServicio_" + Date.Now.ToShortDateString + "." + fileNameExtension)
        Response.BinaryWrite(renderedBytes)
        Response.End()
        '
    End Sub

    Sub Limpiar()
        'Me.ddlServicio.SelectedIndex = 0
        'Me.ddlOrigenCancelacion.SelectedIndex = 0
        'Me.ddlMoneda.SelectedIndex = 0
        'Me.ddlTipo.SelectedIndex = 0
        'Me.txtFechaA.Text = Now.Date
        'Me.txtFechaDe.Text = Now.Date
        'rptPagoServicios.LocalReport.DataSources.Clear()
        'rptPagoServicios.LocalReport.Refresh()
        'rptPagoServicios.Visible = False
        btnExportarExcel.Enabled = False
        btnExportarPDF.Enabled = False
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Limpiar()
    End Sub
End Class
