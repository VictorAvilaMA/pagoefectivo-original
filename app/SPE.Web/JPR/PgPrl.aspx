<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PgPrl.aspx.vb" Inherits="JPR_PgPrl" Title="PagoEfectivo - Página Principal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/base.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/screen.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/fixScreen.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
       #contenidos #content h3
        {
            margin: 0;
            line-height: 16px;
            text-align: left;
        }
        #header
        {
            padding-left: 0;            
        }
        .colRight .widget-content ul li
        {
            float: left;
        }
    </style>
    <div class="colRight" style="width: 740px; padding-top: 20px">
        <div class="inner-col-right">
            <div class="col-first">
                <div id="li1" runat="server" class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Agentes</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="../ADM/ADAgen.aspx">Registrar Agente</a></li>
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="../ADM/COAgen.aspx">Consultar Agente</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/ic-agente.jpg" alt="agente" /></div>
                    </div>
                </div>
                <div id="li2" runat="server" class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Agencias recaudadoras</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="../ADM/ADAgRe.aspx">Registrar Agencia Recaudadora</a></li>
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="../ADM/COAgRe.aspx">Consultar Agencia Recaudadora</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/ic-agencia.jpg" alt="agencia" /></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="li3" runat="server" class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Agencia Bancaria</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COAgBa.aspx">Consultar Agencia Bancaria</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/Bancos.PNG" alt="empresa" /></div>
                    </div>
                </div>
            </div>
            <div class="col-last">
                <div id="li4" runat="server" class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Operadores</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COOper.aspx">Consultar Operadores</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/Representante.JPG" alt="cliente" /></div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="li5" runat="server" class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Establecimiento</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="COEstab.aspx">Consultar Establecimientos</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/empresa.JPG" alt="empresa" /></div>
                    </div>
                </div>
                <div id="li6" runat="server" class="widget-fix-border-ie">
                    <div class="widget-content">
                        <div class="inner-widget">
                            <h3>
                                Conciliaciones</h3>
                            <ul class="options">
                                <li>
                                    <div class="bgcryellow">
                                    </div>
                                    <a href="../CONC/PRConcDiaria.aspx">Conciliación Diaria</a></li>
                            </ul>
                        </div>
                        <div class="cnt-icon">
                            <span></span>
                            <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/Conciliacion.PNG" alt="cliente" /></div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
