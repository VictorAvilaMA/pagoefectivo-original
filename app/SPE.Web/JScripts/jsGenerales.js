﻿// JScript File
function AbrirPopPup(url, titulo, width, height) {
    window.open(url, titulo, "width=" + width + ", height=" + height + ", scrollbars=no,menubar=no,location=no,resizable=no");
}

function ConfirmMeWithDisabled(obj) {
    if (Page_ClientValidate()) {
        confirm('¿Desea confirmar la operación?');
        obj.disabled = true;
    }
}

//Popup para detalle de movimientos de caja
function openPRPRDeMoCaReporte() {
    setTimeout("window.open('PRDeMoCaReporte.aspx')", 1500);
}

//Mensaje de Confirmacion
function ConfirmMe() {
    if (Page_ClientValidate())
        return confirm('¿Desea confirmar la operación?');
    return false;
}

//Mensaje de Cancelación
function CancelMe() {
    return confirm('¿Está seguro que desea cancelar la operación?');
}

function DeleteMe() {
    return confirm('¿Está seguro que desea Eliminar ?');
}

//Mensaje de confirmacion para liquidación de caja en bloque
function ConfirmLiCaBlo() {
    return confirm('¿Está seguro que desea liquidar la(s) caja(s)?');
}
//Mensaje de confirmacion para Registrar Empresa
function RegistrarEmpresa() {
    if (Page_ClientValidate())
        return confirm('¿Desea realizar la operación?');

}
//Mensaje de confirmacion para Registrar Representante
function RegistrarRepresentante() {
    if (Page_ClientValidate())
        return confirm('¿Desea realizar la operación?');
    return false;
}
//Mensaje de confirmacion para Registrar Caja
function RegistrarCaja() {
    if (Page_ClientValidate())
        return confirm('¿Desea ejecutar la operación?');
}


//Mensaje de Confirmación de Liquidación de Caja
function LiquidarCaja() {
    return confirm('¿Desea liquidar la caja?');
}

//Mensaje de Confirmación de Registrar Sobrante Faltante
function ConfirmReSoFa() {
    if (Page_ClientValidate())
        return confirm('¿Desea registrar el monto?');
}

//Mensaje de confirmacion de cliente
function ConfirmReCli() {
    return confirm('¿Sus datos seran registrados, está seguro de continuar? ');
}


function RegistrarAgenciaBancaria() {
    if (Page_ClientValidate())
        return confirm('¿Desea realizar la operación?');

}


function openPRLiCaBlReporte(URL) {
    window.open('../ARE/PRLiCaBlReporte.aspx');
}
function openPOSPRLiCaBlReporte(URL) {
    window.open('../JPR/PRLiesReporte.aspx');
}
//FUNCIONES PARA VALIDAR EL RUC
//Mensaje de Confirmacion
function ConfirmMeRUC(textValidar) {
    if (Page_ClientValidate())
        return confirm('¿Desea confirmar la operación?');
    return false;
}

function ValidarRuc(source, arguments) {
    var ruc = arguments.Value;
    var ok = (!(esnulo(ruc) || !esnumero(ruc) || !eslongrucok(ruc) || !valruc(ruc)));
    if (ok == false) {
        arguments.IsValid = false;
    }
    else {
        arguments.IsValid = true;
    }
}

function valruc(valor) {
    valor = trim(valor)
    if (esnumero(valor)) {
        if (valor.length == 8) {
            suma = 0
            for (i = 0; i < valor.length - 1; i++) {
                digito = valor.charAt(i);
                if (i == 0) suma += (digito * 2)
                else suma += (digito * (valor.length - i))
            }
            resto = suma % 11;
            if (resto == 1) resto = 11;
            if (resto + (valor.charAt(valor.length - 1)) == 11) {
                return true
            }
        }
        else if (valor.length == 11) {
            suma = 0
            x = 6
            for (i = 0; i < valor.length - 1; i++) {
                if (i == 4) { x = 8 }
                digito = valor.charAt(i);
                x--
                if (i == 0) suma += (digito * x)
                else suma += (digito * x)
            }
            resto = suma % 11;
            resto = 11 - resto
            if (resto >= 10) resto = resto - 10;
            if (resto == valor.charAt(valor.length - 1)) {
                return true
            }
        }
    }
    return false
}

function esnulo(campo) { return (campo == null || campo == ""); }
function esnumero(campo) { return (!(isNaN(campo))); }
function eslongrucok(ruc) { return (ruc.length == 11); }

function trim(cadena) {
    temp_cadena = "";
    len = cadena.length;
    for (var i = 0; i <= len; i++) if (cadena.charAt(i) != " ") { temp_cadena += cadena.charAt(i); }
    return temp_cadena;
}



/********************* CLAVES API ***********************/

function ConfirmaGenClaveSecreta() {
    if (Page_ClientValidate()) {
        return confirm('¿ Está seguro que desea regenerar la clave secreta ?');
    }
    else
        return false;
}

function ConfirmaGenClaveAPI() {
    if (Page_ClientValidate()) {
        var txtClaveSecreta = document.getElementById("ctl00_ContentPlaceHolder1_txtClaveSecreta");
        var msg = "";
        if (txtClaveSecreta.value == "") {
            msg = "¿Está seguro que desea generar la clave API y la clave Secreta?";
        } else {
            msg = "¿Está seguro que desea generar la clave API?";
        }
        return confirm(msg);
    }
    else
        return false;
}

/********************* LLAVE ENCRIPTACION ***********************/

function ConfirmaGenLlaveEncriptacion() {
    if (Page_ClientValidate()) {
        return confirm('¿ Está seguro que desea regenerar la llave de encriptación ?');
    }
    else
        return false;
}

/******************************* fUNCION DE vALIDACION cOKKIES ***********************/
/*function getCookie(nombreKey) {
    var valueCook = $.cookie(nombreKey);
    return valueCook;
}*/

function uuid() {
    var chars = '0123456789abcdef'.split('');
    var uuid = [], rnd = Math.random, r;
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
    uuid[14] = '4'; // version 4 

    for (var i = 0; i < 36; i++) {
        if (!uuid[i]) {
            r = 0 | rnd() * 16;
            uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r & 0xf];
        }
    }
    return uuid.join('');

}

/******************************* fUNCION DE vALIDACION cOKKIES ***********************/
function getCookieJS(check_name) {
    var a_all_cookies = document.cookie.split(';');
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false; // set boolean t/f default f

    for (i = 0; i < a_all_cookies.length; i++) {
        a_temp_cookie = a_all_cookies[i].split('=');
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
        if (cookie_name == check_name) {
            b_cookie_found = true;
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if (!b_cookie_found) {
        return null;
    }
}
function setCookieJS(name, value) {
    //valores en duro:
    var expires = 360;
    var path = "/";
    var domain = "";
    var secure = "";
    //
    var today = new Date();
    today.setTime(today.getTime());

    if (expires) {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date(today.getTime() + (expires));

    document.cookie = name + "=" + escape(value) +
        ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
        ((path) ? ";path=" + path : "") +
        ((domain) ? ";domain=" + domain : "") +
        ((secure) ? ";secure" : "");
}

/*************************************************************************************/
$(document).ready(function () {
    $(".divLoading").hide();
    $(".divLoadingMensaje").hide();

    $(".divLoading").ajaxStart(function () {
        $(".divLoadingMensaje").show('slow');
        $(this).show('blind');
        $('#errores').css('display', 'none');
    })
    $(".divLoading").ajaxComplete(function () {
        if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').length > 0)
            if ($('.pagerStyle>td>table>tbody>tr>td:eq(0)').text() != 'Página :')
                $('.pagerStyle>td>table>tbody>tr>td:eq(0)').before('<td>P&aacute;gina :</td>');
        $(".divLoadingMensaje").hide('normal');
        $(this).hide('fold');
    });
});





//trimming space from both side of the string
String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}

//trimming space from left side of the string
String.prototype.ltrim = function () {
    return this.replace(/^\s+/, "");
}

//trimming space from right side of the string
String.prototype.rtrim = function () {
    return this.replace(/\s+$/, "");
}

//pads left
String.prototype.lpad = function (padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

/*
//pads right, este es el script de la muerte
String.prototype.rpad = function (padString, length) {
var str = this;
while (str.length < length)
str = str + padString;
return str;
}
*/

function ValidarCaracteres(textareaControl, maxlength, lblrpta) {
    if (textareaControl.value.length > maxlength) {
        textareaControl.value = textareaControl.value.substring(0, maxlength);
        //alert("Debe ingresar hasta un maximo de " + maxlength + " caracteres");
        //return textareaControl.value.length.toString() + "de " + maxlength + "caracteres.";
    }
    lblrpta.text((maxlength - textareaControl.value.length) + " de " + maxlength);
}
function ValidarCaracteres2(textareaControl, maxlength, lblrpta) {
    if (textareaControl.value.length > maxlength) {
        textareaControl.value = textareaControl.value.substring(0, maxlength);
        //alert("Debe ingresar hasta un maximo de " + maxlength + " caracteres");
        //return textareaControl.value.length.toString() + "de " + maxlength + "caracteres.";
    }
    lblrpta.text("Caracteres restantes "+(maxlength - textareaControl.value.length) + " de " + maxlength);
}

//Estilo de Cosigo Captcha
$(document).ready(function () {
    loadStyles();
    function loadStyles() {
        $(".captchaContent").children().children("span:eq(0)").css({
            "width": "180px",
            "height": "50px"
        });
        $(".captchaContent").children().children("span").css({
            "display": "inline-block",
            "vertical-align": "top",
            "float": "none"
        });
    }
    var i = 1;
    /*$("body").on("change",".corta",function () {
        console.log('corta:' + i);
        i++;
        setTimeout(function () { loadStyles(); }, 1000);
        
    });*/
    /*$(".corta2").on("change",function () {
        console.log('corta2:' + i);
        loadStyles();
        i++;
    });*/
   /* $("body").on("change", ".corta2", function () {
        console.log('corta2:' + i);
        i++;
        setTimeout(function () { loadStyles(); }, 1000);
    });*/
});
