﻿////////////////////////////////
/// CORE //////////////////////


(function($){
	
	var settings = {};
	var defaults = {};
	
	
	// Aplicacion Neoauto Persona Natural
	$.pagoEfectivo = {
			
		init: function(options){			
			this.settings = $.extend(true, {}, defaults, options);	
		},
			
		ready: Function,
		modDefault:{
		    init: function() {

	            var this_ = this;
	            $('#simple_tabs a.tab').simpleTabs();

	            $("a.loadcontent").click(function(e) {
	                e.preventDefault();
	                var relContent = $(this).attr("rel");

	                if ($(relContent).not(':animated').length) {
	                    $(".layercontentmain").fadeOut("fast", function() {
	                        setTimeout(function() { $(relContent).fadeIn("slow") }, 100);
	                    });
	                } else {
	                    return false;
	                }
	            });
	            $(".cerrar_layer").click(function(e) {
	                e.preventDefault();
	                $(".layercontentmain").hide("slow");
	            });	
						
				$(".sesion_user").toggle(function(e){
					e.preventDefault();
					$(this).removeClass("noiniciosesion").addClass("iniciosesion");
					$(".contentdeplegable").removeClass("display_off").addClass("display_on")
				},function(e){
					e.preventDefault();
					$(this).removeClass("iniciosesion").addClass("noiniciosesion");
					$(".contentdeplegable").removeClass("display_on").addClass("display_off")
				});	
			}
		}
		
		
		
	};
	
	$(document).ready(function(){
		// Constructor del core.
		$.pagoEfectivo.init($.pagoEfectivo.defaults);
		$.pagoEfectivo.ready && $.pagoEfectivo.ready();		
		
		// Carga de scripts exclusivos por vista, usando el ID del body.	
		var $body = $('body');
		var $idpage = $body.attr('id'); 		
				
		switch ($idpage) {		
			default:
				$.pagoEfectivo.modDefault.init();
				// aca lo que sea e404. 
			break;
		};			
		
	});
	
})(jQuery);