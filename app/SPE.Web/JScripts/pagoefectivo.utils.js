﻿/* jQuery simpleTabs by Oscar Sobrevilla 
-------------------------------------------------*/				
(function($){		
$.extend($.fn,{
		 
	simpleTabs: function(options){
		var this_ = this;
		
		this.defaults = {
			triggerType: 'a',
			prntTag : 'ul',
			prntTrigger: 'li',
			selectedClass:'selected',
			tabCurrentClass: 'current-tab'
		};
		
		this.settings = $.extend({}, this.defaults, options); 
		
		this.each(function(){
						  
			$this = $(this);
			
			if($this.hasClass('display')){
				$($this.attr('rel')).addClass(this_.settings.tabCurrentClass);					
			}else{
				$($this.attr('rel')).hide();	
			}
				
			$this.bind('click.simpleTabs', function(e){ 
				e.preventDefault();
				var $trg = $(this);
				var $trp = $trg.parents(this_.settings.prntTrigger);
				var $pnt = $trg.parents(this_.settings.prntTag);
				var $btab = $($trg.attr('rel'));
				
				if(this_.cbtab){
					this_.cbtab.hide();
					this_.ctrg.parent(this_.settings.prntTrigger).removeClass(this_.settings.selectedClass);
				}else{
					$($pnt.find('.' + this_.settings.selectedClass)
					.removeClass(this_.settings.selectedClass)
					.find(this_.settings.triggerType)
					.attr('rel')).hide();
				}
				$btab.show();
				$trp.addClass(this_.settings.selectedClass);
				this_.cbtab = $btab;
				this_.ctrg = $trg;
			});
			
		});
	}		 
});		

})(jQuery);