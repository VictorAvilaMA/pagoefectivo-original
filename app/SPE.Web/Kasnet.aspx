﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Kasnet.aspx.vb" Inherits="Kasnet" %>

<!DOCTYPE html>
<html lang="es">
<head id="head1" runat="server">
    <meta charset="UTF-8">
    <title>Pago Efectivo - Agente Kasnet</title>    
    <meta description="Encuentra el Agente Kasnet más cercano a ti">
	<meta name='viewport' content='width=device-width, user-scalable=no'>    
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/landing_kasnet.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/fonts.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/normalize.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="Form1" runat="server">
<header>    
		<div class="content_header">
			<a href="">
				<h1>
					<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/logo_pagoefectivo_landing_kasnet.png" alt="Pago Efectivo" alt="Pago Efectivo">                    
				</h1>
			</a>
			<h2>Transacciones seguras por Internet en el Perú</h2>
		</div>
	</header>
	<section>
		<div class="wrapper_slide">
			<div class="container pt">
				<div class="slide_img">
					<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/mascota.png" alt="Mascota Kasnet" title="Mascota Kasnet">
				</div>
				<div class="slide_text">
					<h3>Ahora puedes cancelar tus pagos con <b>PagoEfectivo</b> en</h3>
					<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/logo_kasnet.jpg" alt="kasnet" title="Kasnet">
					<h4>La primera y más grande <br><b>red de Agentes Corresponsales Multibanca</b> del país</h4>
					<a href="http://www.agentekasnet.com" target="_blank" title="Infórmate más">Infórmate más</a>
				</div>
			</div>
		</div>
		<div class="points_bar">
			<div class="container">
				<h5>¡Encuentra el <b>Agente Kasnet</b><br> más cercano a ti!</h5>
				<a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/Archivos/RelacionagentesKasnetLimaSeptiembre.2014.pdf" class="point lima" title="Puntos en Lima" onclick="_gaq.push(['_trackEvent', 'Punto_Agente_Kasnet','Punto_Lima' , 'Punto_Lima']);">                
					<span></span>
					<p>Puntos<br> en Lima</p>
				</a>
				<a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/Archivos/RelacionagentesKasnetProvinciaSeptiembre.2014.pdf" class="point provincias" title="Puntos en Provincias" onclick="_gaq.push(['_trackEvent', 'Punto_Agente_Kasnet','Punto_Provincia' , 'Punto_Provincia']);">
					<span></span>
					<p>Puntos<br> en provincias</p>
				</a>
			</div>
		</div>
		<div class="container_bottom">
			<div class="container">
				<h3 class="steps_title">¿Cómo realizo mis pagos en Agentes Kasnet?</h3>
				<div class="steps">
					<div class="step">
						<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/step_pagoefectivo.png" alt="PagoEfectivo" title="PagoEfectivo">
						<div class="description">
							<span></span>
							<p>Has tus compras por internet usando <b>PagoEfectivo</b> como medio de pago.</p>
						</div>
					</div>
					<div class="step">
						<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/step_cip.png" alt="Código CIP" title="Código CIP">
						<div class="description">
							<span></span>
							<p>Revisa tu email y copia el <b>código CIP</b> de tu compra realizada.</p>
						</div>
					</div>
					<div class="step">
						<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/step_kasnet.png" alt="Agente Kasnet" title="Agente Kasnet">
						<div class="description">
							<span></span>
							<p>Acércate a un <b>Agente Kasnet</b>, presenta tu <b>código CIP</b>, indica que el pago es a la empresa <b>PagoEfectivo</b> y ¡Listo!</p>
						</div>
					</div>
				</div>
				<h3 class="steps_title">Comercios Afiliados</h3>
				<div class="shops">
					<div class="shop"><span class="peruvian"></span></div>
					<div class="shop"><span class="nuevomundo"></span></div>
					<div class="shop"><span class="linio"></span></div>
					<div class="shop"><span class="sagafalabella"></span></div>
					<div class="shop"><span class="sodimac"></span></div>
					<div class="shop"><span class="movistar"></span></div>
					<div class="shop"><span class="jockeyplaza"></span></div>
					<div class="shop"><span class="platanitos"></span></div>
					<div class="shop"><span class="inkabet"></span></div>
					<div class="shop"><span class="pokerstar"></span></div>
					<div class="shop"><span class="deltron"></span></div>
					<div class="shop"><span class="intralot"></span></div>
					<div class="shop mobile"><span class="domiruth"></span></div>
					<div class="shop mobile"><span class="ofertop"></span></div>
					<div class="shop mobile"><span class="avansys"></span></div>
					<div class="shop mobile"><span class="betcris"></span></div>
					<div class="shop mobile"><span class="lcperu"></span></div>
					<div class="shop mobile"><span class="civa"></span></div>
					<div class="shop mobile"><span class="oltursa"></span></div>
					<div class="shop mobile"><span class="cruzdelsur"></span></div>
					<div class="shop mobile"><span class="santanatura"></span></div>
					<div class="shop mobile"><span class="boacompra"></span></div>
					<div class="shop mobile"><span class="cuponatic"></span></div>
					<div class="shop mobile"><span class="yumax"></span></div>
					<div class="shop mobile"><span class="loginstore"></span></div>
					<div class="shop mobile"><span class="triathlon"></span></div>
					<div class="shop mobile"><span class="astropay"></span></div>
					<div class="shop mobile"><span class="lukana"></span></div>
					<div class="shop mobile"><span class="babyintanti"></span></div>
					<div class="shop mobile"><span class="estilomio"></span></div>
					<div class="shop mobile"><span class="kukyflor"></span></div>
					<div class="shop mobile"><span class="clubventa"></span></div>
					<div class="shop mobile"><span class="mediamanzana"></span></div>
					<div class="shop mobile"><span class="yachay"></span></div>
					<div class="shop mobile"><span class="costamar"></span></div>
					<div class="shop mobile"><span class="cuponidad"></span></div>
					<div class="shop mobile"><span class="puntope"></span></div>
					<div class="shop mobile"><span class="globalnet"></span></div>
					<div class="shop mobile"><span class="bruno"></span></div>
					<div class="shop mobile"><span class="golazobet"></span></div>
				</div>
				<a href="https://pagoefectivo.pe/ComerciosAfiliados.aspx" target="_blank" class="more" title="Mucho más" onclick="_gaq.push(['_trackEvent', 'Afiliados_Agente_Kasnet','Muchos_Mas_Afiliados' , ' Muchos_Mas_Afiliados ']);">y mucho más...</a>
			</div>
		</div>
	</section>
	<footer>
		<p class="copyright"><span>©</span> 2014 PagoEfectivo</p>
		<p class="schedule"><b>Horarios de atención:</b> L-D 8:30 am a 9:00 pm. (horario según la localidad de agentes con horario diferente) Monto máximo 1000 soles para pago de servicios.</p>
	</footer>
</form>
	
</body>
</html>
