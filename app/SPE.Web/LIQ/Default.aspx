﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="LIQ_Default" Title="PagoEfectivo - Registrar Periodo Liquidacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Registrar / Actualizar Agente Recaudador"></asp:Literal></h2>
    <div class="conten_pasos3">
        <asp:UpdatePanel ID="UpdatePanelDatos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:Panel ID="pnlPage" runat="server">
                    <asp:Panel ID="pnlInformacionRegistro" CssClass="inner-col-right" runat="server">
                        <h4>
                            1. Informaci&oacute;n general</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Periodo de Liquidaci&oacute;n: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtPeriodoLiquidacion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                            </li>
                            <li class="t1"><span class="color">>></span> Estado: </li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                                    <asp:ListItem Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="2">Inactivo</asp:ListItem>
                                </asp:DropDownList>
                            </li>
                            <li class="t1"><span class="color">>></span> Dias Liquidación: </li>
                            <li class="t2" id="table-dates">
                                <asp:GridView ID="gvResultado" runat="server" AutoGenerateColumns="false" PageSize="1">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                L</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch1" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                M</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch2" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                M</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch3" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                J</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch4" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                V</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch5" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                S</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch6" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                D</HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="Ch7" runat="server" /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Contacto" HeaderText="Contacto" Visible="false" />
                                    </Columns>
                                </asp:GridView>
                            </li>
                            <li class="t1"><span class="color">>></span> Fechas Fin: </li>
                            <li class="t2">
                                <asp:TextBox ID="txtFechasFin" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                                <asp:Label ID="lblErrFechasFin" runat="server" CssClass="inicial"></asp:Label>
                            </li>
                            <li class="complet">
                                <asp:Button ID="btnRegistrar" CssClass="input_azul3" runat="server" Text="Registrar"
                                    OnClientClick=""></asp:Button>
                                <asp:Button ID="btnActualizar" CssClass="input_azul3" runat="server" Text="Actualizar"
                                    OnClientClick="return ConfirmMe();"></asp:Button>
                                <asp:Button ID="btnCancelar" CssClass="input_azul4" runat="server" Text="Cancelar"
                                    OnClientClick="return CancelMe();"></asp:Button>
                                <asp:Label ID="lblTransaccion" runat="server" Width="650px" CssClass="MensajeTransaccion"
                                    Style="padding-bottom: 5px"></asp:Label>
                                <asp:Label ID="lblIdPeriodo" runat="server" Visible="false"></asp:Label>
                            </li>
                        </ul>
                        <%--<asp:HiddenField ID="hdnIdCliente" runat="server" />--%>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnRegistrar"></asp:PostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnCancelar" EventName="Click"></asp:AsyncPostBackTrigger>
                <%--<asp:PostBackTrigger ControlID="btnCancelar" />--%>
                <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery.validate.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
        type="text/javascript"></script>
    <script src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/validacionLiq.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"
        type="text/javascript"></script>
</asp:Content>
