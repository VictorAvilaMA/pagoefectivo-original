﻿Imports SPE.Entidades
Imports System.IO
Imports System.Collections.Generic
Imports System.Configuration

Partial Class LIQ_Default
    Inherits _3Dev.FW.Web.PageBase

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Page.SetFocus(txtPeriodoLiquidacion)
            CargarGrid()
            Try
                If Page.PreviousPage Is Nothing OrElse Context.Items.Item("IdPeriodo") Is Nothing Then 'CONSULTAR PERIODO
                    lblProceso.Text = "Registrar Periodo Liquidaci&oacute;n"
                    btnRegistrar.Visible = True
                    btnActualizar.Visible = False
                Else
                    btnRegistrar.Visible = False
                    btnActualizar.Visible = True
                    CargarProcesoActualizar(Convert.ToInt32(Context.Items.Item("IdPeriodo")))
                    lblIdPeriodo.Text = Context.Items.Item("IdPeriodo").ToString()
                End If
            Catch ex As Exception
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
            End Try
            Page.SetFocus(txtPeriodoLiquidacion)

        End If
        '
    End Sub


    Private Sub CargarProcesoActualizar(ByVal IdPeriodo As Integer)
        '
        Title = "PagoEfectivo - Actualizar Periodo"
        lblProceso.Text = "Actualizar Periodo Liquidaci&oacute;n"

        '
        Dim objPeriodoLiquidacion As New SPE.Web.CComun
        Dim obePeriodoReq As New BEPeriodoLiquidacion
        Dim obePeriodoRes As New BEPeriodoLiquidacion
        Dim listPeriodo As New List(Of BEPeriodoLiquidacion)
        obePeriodoReq.IdPeriodo = IdPeriodo
        obePeriodoReq.IdEstado = 0
        listPeriodo = objPeriodoLiquidacion.RecuperarDatosPeriodoLiquidacion(obePeriodoReq)
        obePeriodoRes = listPeriodo(0)
        txtPeriodoLiquidacion.Text = obePeriodoRes.Descripcion
        ddlEstado.SelectedValue = obePeriodoRes.IdEstado.ToString
        Dim gvr As GridViewRow = gvResultado.Rows(0)
        If obePeriodoRes.FechasFin = "" Then
            For i As Integer = 0 To 6
                If obePeriodoRes.DiasLiquidacion.Substring(i, 1) = "1" Then
                    DirectCast(gvr.FindControl("Ch" + (i + 1).ToString), CheckBox).Checked = True
                Else
                    DirectCast(gvr.FindControl("Ch" + (i + 1).ToString), CheckBox).Checked = False
                End If
            Next i
        Else
            txtFechasFin.Text = obePeriodoRes.FechasFin
        End If
        



    End Sub


    Public Sub CargarGrid()
        Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
        obeAgenciaRecaudadora.Contacto = "A"
        Dim ListFake As New List(Of BEAgenciaRecaudadora)
        ListFake.Add(obeAgenciaRecaudadora)
        gvResultado.DataSource = ListFake
        gvResultado.DataBind()
    End Sub


    'REGISTRAR AGENTE
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click

        Try
            Dim objComun As New SPE.Web.CComun
            Dim oBEPeriodo As BEPeriodoLiquidacion = CargarDatosBEPeriodo(0)

            If objComun.RegistrarPeriodo(oBEPeriodo) = 2 Then
                lblTransaccion.Text = "El nombre de perdiodo ya existe."
            Else
                btnRegistrar.Enabled = False
                btnCancelar.Enabled = False
                pnlPage.Enabled = False
                btnCancelar.OnClientClick = ""
                lblTransaccion.Text = "El Periodo se registró satisfactoriamente."
            End If




        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try

    End Sub



    Private Function CargarDatosBEPeriodo(ByVal idPeriodo As Integer) As BEPeriodoLiquidacion
        Dim obePeriodo As New BEPeriodoLiquidacion
        Try
            Dim strCadena As String = ""
            Dim strFechasFin As String = ""
            Dim gvr As GridViewRow = gvResultado.Rows(0)
            If txtFechasFin.Text = "" Then
                For i As Integer = 1 To 7
                    If DirectCast(gvr.FindControl("ch" + i.ToString), CheckBox).Checked = True Then
                        strCadena += "1"
                    Else
                        strCadena += "0"
                    End If
                Next i
                strFechasFin = String.Empty
            Else
                strFechasFin = txtFechasFin.Text
                strCadena = String.Empty
            End If
            obePeriodo.IdPeriodo = idPeriodo
            obePeriodo.Descripcion = txtPeriodoLiquidacion.Text
            obePeriodo.FechaCreacion = Date.Today
            obePeriodo.IdEstado = 1
            obePeriodo.DiasLiquidacion = strCadena
            obePeriodo.FechasFin = strFechasFin
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        Return obePeriodo
    End Function



    Private Sub InicializarddlEstado(ByVal idEstado As String)
        If idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Or idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            ddlEstado.Enabled = False
            ddlEstado.SelectedValue = idEstado
        Else
            ddlEstado.Enabled = True
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente)))
            ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado)))
            ddlEstado.SelectedValue = idEstado
        End If
    End Sub


    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()

        '
    End Sub

    'LIMIAR FORM
    Protected Sub LimpiarForm()
        '
        'txtDescAgenciaRecaudadora.Text = ""
        'lblIdAgenciaRecaudadora.Text = "0"
        'txtEmail.Text = ""
        'txtContrasenaAnterior.Text = ""
        'txtContrasena.Text = ""
        'txtRepitaContrasena.Text = ""
        'txtNombres.Text = ""
        'txtApellidos.Text = ""
        'txtNumeroDoc.Text = ""
        'txtTelefono.Text = ""
        'txtDireccion.Text = ""
        '
    End Sub



    'Listar Agencia Recaudadora
    Private Sub ListarAgenciaRecaudadora()
        '
        Try
            '

            Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
            obeAgenciaRecaudadora.Contacto = "A"
            Dim ListFake As New List(Of BEAgenciaRecaudadora)
            ListFake.Add(obeAgenciaRecaudadora)
            gvResultado.DataSource = ListFake
            gvResultado.DataBind()
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        '
    End Sub

    'SELECCIONAMOS LA AGENCIA
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        '
        'If Not e Is Nothing Then
        '    lblIdAgenciaRecaudadora.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument.ToString())).Cells(0).Text
        '    txtDescAgenciaRecaudadora.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument.ToString())).Cells(1).Text
        '    OcultarDDL(True)
        'End If
        '
    End Sub

    'CANCELAR
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        '
        Response.Redirect("~/LIQ/LiqConsulta.aspx")
        '
    End Sub

    'PAGINACION DE GRILLA
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarAgenciaRecaudadora()

        '
    End Sub

    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub




    Public Sub generarClaves(ByVal codigoEntidad As String)
        'Dim objCSeguridad As New CSeguridad
        'Dim successfull As Boolean = objCSeguridad.GenerarClavePublicaPrivada(codigoEntidad)

        'Dim keyPublic As Byte() = objCSeguridad.ObtenerClavePublica(codigoEntidad)
        'Dim numBytesToRead As Integer = keyPublic.Length

        'Using fsNew As FileStream = New FileStream("C:\\keyPublic" + codigoEntidad + ".p1z", _
        '        FileMode.Create, FileAccess.Write)
        '    fsNew.Write(keyPublic, 0, numBytesToRead)
        'End Using

        'Dim keyPrivate As Byte() = objCSeguridad.ObtenerClavePrivada(codigoEntidad)
        'numBytesToRead = keyPrivate.Length

        'Using fsNew As FileStream = New FileStream("C:\\keyPrivate" + codigoEntidad + ".p1z", _
        '        FileMode.Create, FileAccess.Write)
        '    fsNew.Write(keyPrivate, 0, numBytesToRead)
        'End Using
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As System.EventArgs) Handles btnActualizar.Click
        Try
            Dim objComun As New SPE.Web.CComun
            Dim oBEPeriodo As BEPeriodoLiquidacion = CargarDatosBEPeriodo(lblIdPeriodo.Text)

            If objComun.ActualizarPeriodo(oBEPeriodo) = 0 Then
                lblTransaccion.Text = "El nombre de perdiodo ya existe."
            Else
                btnRegistrar.Enabled = False
                btnCancelar.Enabled = True
                pnlPage.Enabled = False
                btnCancelar.OnClientClick = ""
                lblTransaccion.Text = "El Periodo se actualiz&oacute; satisfactoriamente."
            End If




        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
    End Sub
End Class
