﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="LiqConsulta.aspx.vb" Inherits="LiqConsulta"
    Title="PagoEfectivo - Consultar Liquidacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .hidden
        {
            display: none;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Agente</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de búsqueda</h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Periodo Liquidaci&oacute;n</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlPerdiodo" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Estado:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="neu">
                            <asp:ListItem Value="1">Activo</asp:ListItem>
                            <asp:ListItem Value="2">Inactivo</asp:ListItem>
                        </asp:DropDownList>
                    </li>
                    <li class="complet">
                        <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul5" Text="Nuevo" PostBackUrl="~/LIQ/Default.aspx" />
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul4" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" class="cont_cel" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblResultado" runat="server" Text=""></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvResultado" runat="server" BackColor="White" CssClass="grilla"
                            AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                            CellPadding="3">
                            <Columns>
                                <asp:TemplateField HeaderText="Sel.">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnActualizar" runat="server" PostBackUrl="~/LIQ/Default.aspx"
                                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                            ToolTip="Actualizar" CommandName="Actualizar_Command" OnCommand="Actualizar_Command"
                                            CommandArgument='<%# Eval("IdPeriodo") %>'></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdPeriodo" HeaderText="IdPeriodo" Visible="False"></asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Periodo de Liquidación"></asp:BoundField>
                                <asp:BoundField DataField="ValoresPeriodo" HeaderText="Valores de Periodo de Liquidación">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DesEstado" HeaderText="Estado"></asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtndelete" CausesValidation="false" runat="server" 
                                        ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/Images_Buton/borrar.jpg" %>'
                                        OnClientClick="return DeleteMe();" ToolTip="Eliminar" OnCommand="Delete_Command" CommandArgument='<%# Eval("IdPeriodo") %>'/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="right" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="DiasLiquidacion" HeaderText="DiasLiquidacion" ItemStyle-CssClass="hidden"
                                    HeaderStyle-CssClass="hidden">
                                    <HeaderStyle CssClass="hidden" />
                                    <ItemStyle CssClass="hidden" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechasFin" HeaderText="FechasFin" ItemStyle-CssClass="hidden"
                                    HeaderStyle-CssClass="hidden">
                                    <HeaderStyle CssClass="hidden" />
                                    <ItemStyle CssClass="hidden" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </div>

                    <div style="float: left; height: 15px; width: 100%;">                                                                                
                                        <asp:Label ID="lblMensajeError" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                        </div>


                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
