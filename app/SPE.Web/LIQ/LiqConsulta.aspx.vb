﻿
Imports System.Data
Imports SPE.Entidades
Imports System.Collections.Generic





Partial Class LiqConsulta
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CComun)

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(ddlPerdiodo)
            CargarCombos()
        End If
        '
    End Sub

    'BUSQUEDA DE Periodo
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        ListarPeriodo()

    End Sub

    'LISTAR AGENTES
    Private Sub ListarPeriodo()
        '
        Try
            Dim objPeriodoLiquidacion As New SPE.Web.CComun
            Dim obePeriodo As New BEPeriodoLiquidacion
            Dim listPeriodo As New List(Of BEPeriodoLiquidacion)

            If ddlPerdiodo.SelectedIndex = 0 Then
                obePeriodo.IdPeriodo = 0
            Else
                obePeriodo.IdPeriodo = Convert.ToInt32(Me.ddlPerdiodo.SelectedValue)
            End If

            If ddlEstado.SelectedIndex = 0 Then
                obePeriodo.IdEstado = 0
            Else
                obePeriodo.IdEstado = Convert.ToInt32(Me.ddlEstado.SelectedValue)
            End If




            listPeriodo = objPeriodoLiquidacion.RecuperarDatosPeriodoLiquidacion(obePeriodo)
            Dim strDias As String = ""
            Dim strFechas As String = ""
            For Each be As BEPeriodoLiquidacion In listPeriodo
                If be.FechasFin = "" Then
                    strDias += "|" + be.DiasLiquidacion
                    strFechas += "|"
                Else
                    strDias += "|"
                    strFechas += "|" + be.FechasFin
                End If
            Next
            strDias = strDias.Substring(1)
            strFechas = strFechas.Substring(1)
            gvResultado.DataSource = listPeriodo
            gvResultado.DataBind()
            ActualizarValores(strDias, strFechas)
            If Not listPeriodo Is Nothing Then
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, listPeriodo.Count)
            Else
                SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
            End If
            divResult.Visible = True
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Sub

    Public Sub ActualizarValores(ByVal diassemana As String, ByVal fechasfin As String)
        Dim cadenaDiasSem As String() = "Lu.Ma.Mi.Ju.Vi.Sa.Do".Split(".")
        Dim valorFinal As String = ""
        Dim cont As Integer = 0
        For Each gvr As GridViewRow In gvResultado.Rows
            If gvr.Cells(3).Text = "D" Then
                Dim cadenaDiasNum As String = diassemana.Split("|")(cont)
                For ii As Integer = 0 To cadenaDiasNum.Length - 1
                    If cadenaDiasNum(ii) = "1" Then
                        valorFinal += "-" + cadenaDiasSem(ii)
                    End If
                Next ii
                valorFinal = valorFinal.Substring(1)
            Else
                valorFinal = fechasfin.Split("|")(cont)
            End If
            gvr.Cells(3).Text = valorFinal
            valorFinal = ""
            cont += 1
        Next

    End Sub

    'ACTUALIZAR Periodo
    Protected Sub Actualizar_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        '
        Context.Items.Add("IdPeriodo", Convert.ToInt32(e.CommandArgument.ToString()))
        '
    End Sub

    'Eliminar Periodo
    Protected Sub Delete_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)

        Dim IdPeriodo As Integer = Convert.ToInt32(e.CommandArgument.ToString())

        Dim objPeriodoLiquidacion As New SPE.Web.CComun
        Dim obePeriodo As New BEPeriodoLiquidacion
        
        obePeriodo.IdPeriodo = IdPeriodo
        

        Dim intRespuesta As Integer = objPeriodoLiquidacion.DeletePeriodo(obePeriodo)
        If intRespuesta = 1 Then
            Response.Redirect("~/LIQ/LiqConsulta.aspx")
        Else
            lblMensajeError.Text = "No se puede eliminar el periodo porque tiene empresas asociadas"
        End If



    End Sub


    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()
        Dim objCComun As New SPE.Web.CComun
        ddlPerdiodo.DataTextField = "Descripcion" : ddlPerdiodo.DataValueField = "IdPeriodo" : ddlPerdiodo.DataSource = objCComun.ConsultarIdDescripcionPeriodo() : ddlPerdiodo.DataBind() : ddlPerdiodo.Items.Insert(0, "::: Todos :::")
        '
    End Sub

    'LIMPIAR
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        '
        
        gvResultado.DataSource = Nothing
        gvResultado.DataBind()
        ddlEstado.SelectedIndex = 0
        ddlPerdiodo.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblResultado, 0)
        divResult.Visible = False
        '
    End Sub

    'PAGINADO DE LA GRILLA
    'Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    '
    '    gvResultado.PageIndex = e.NewPageIndex
    '    ListarPeriodo()
    '    '
    'End Sub

    'Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
    '    '
    '    SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
    '    "IdEstado", _
    '    SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
    '    SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    '    '
    'End Sub

    'Protected Sub gvResultado_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    '    '
    '    SortExpression = e.SortExpression
    '    If (SortDir.Equals(SortDirection.Ascending)) Then
    '        SortDir = SortDirection.Descending
    '    Else
    '        SortDir = SortDirection.Ascending
    '    End If
    '    ListarAgentes()
    '    '
    'End Sub


End Class
