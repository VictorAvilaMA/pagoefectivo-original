<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="Login.aspx.vb" Inherits="Login" Title="PagoEfectivo - Ingreso" %>

<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <%--<script type="text/javascript" src="jscripts/jquery-1.4.2.min.js"></script>        
	<script type="text/javascript" src="jscripts/jquery.simplyscroll-1.0.4.min.js"></script>
    <script type="text/javascript" src="jscripts/pagoefectivo.utils.js"></script>
	<script type="text/javascript" src="jscripts/pagoefectivo.js"></script>
	<script type="text/javascript" src="jscripts/accordian.pack.js"></script>--%>
    <!--[if IE 6]>
	<script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	<script type="text/javascript">
	(function($) {
		$(document).ready(function(){
			try {	
				DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			}catch(e){}});
		})(jQuery);	
	</script>
	<![endif]-->
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/gridandforms.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3

                });
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="body_pe">
        <div class="conten_ab">
            <asp:Login ID="Login1" runat="server" CssClass="Login" LoginButtonText="Ingresa"
                PasswordLabelText="Clave:" PasswordRequiredErrorMessage="La clave es requerida."
                TitleText="Identificate" UserNameLabelText="E-mail:" UserNameRequiredErrorMessage="El email es requerido."
                FailureText="Su intento de acceso no tuvo �xito. Por favor, int�ntalo de nuevo."
                RememberMeText="Recordarme la pr�xima vez" CreateUserText="Registrarse" CreateUserUrl="~/CLI/ADReCl.aspx"
                DestinationPageUrl="~/Principal.aspx" PasswordRecoveryText="Recuperar contrase�a"
                PasswordRecoveryUrl="~/RecuperarPassword.aspx">
                <LayoutTemplate>
                    <div class="registrocont_pe">
                        <h2>
                            Iniciar Sesi&oacute;n</h2>
                        <div class="content_form">
                            <div class="dlinline">
                                <div class="w100 ">
                                    <dl class="dt25 dd50 clearfix no_padding">
                                        <dt>
                                            <label for="vu_txtEmail">
                                                E-mail
                                            </label>
                                        </dt>
                                        <dd>
                                            <asp:TextBox ID="UserName" runat="server" CssClass="text block fleft"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                CssClass="LoginValidator" ErrorMessage="El email es requerido." ToolTip="El email es requerido.">*</asp:RequiredFieldValidator>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="w100 ">
                                    <dl class="dt25 dd50 clearfix no_padding">
                                        <dt>
                                            <label for="vu_txtCodeUser">
                                                Contrase&ntilde;a
                                            </label>
                                        </dt>
                                        <dd>
                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="text block fleft" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                CssClass="LoginValidator" ErrorMessage="La clave es requerida." ToolTip="La clave es requerida.">*</asp:RequiredFieldValidator>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="w100 ">
                                    <div class="mensaje_error" style="float: left; width: 300px;">
                                        <asp:Literal ID="FailureText" runat="server" Visible="false" EnableViewState="True"></asp:Literal>
                                        <asp:Literal ID="TextoValidacion" runat="server" EnableViewState="True"></asp:Literal>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="mensaje_error" />
                                    </div>
                                </div>
                                <div class="botones_recupera">
                                    <asp:Button ID="LoginButton" CommandName="Login" runat="server" CssClass="ingre_pe png_bg" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="false" CssClass="cancelar_pe png_bg"
                                        PostBackUrl="~/Default.aspx" />
                                </div>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
            </asp:Login>
        </div>
        <div class="sidebar-rigth">
            <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
        </div>
    </div>
</asp:Content>
