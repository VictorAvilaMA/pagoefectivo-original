
Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class Login
    Inherits System.Web.UI.Page

    'Protected WebMessage As WebMessage


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        ConfigureLoginManager()
        'WebMessage = New WebMessage(lblMensaje)
        'WebMessage.Clear()
        CType(Master, MPBase).OcultarLogin()
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            If (_3Dev.FW.Util.DataUtil.ObjectToString(Session("LoginErrorDetails")) <> "") Then
                CType(Login1.FindControl("TextoValidacion"), Literal).Text = HttpContext.Current.Session("LoginErrorDetails")
                HttpContext.Current.Session.Remove("LoginErrorDetails")
            End If
        End If
        Dim oBEEquipoRegistradoXusuarioVal As New BEEquipoRegistradoXusuario()
        oBEEquipoRegistradoXusuarioVal.Usuario = CType(Login1.FindControl("UserName"), TextBox).Text.Trim()
        oBEEquipoRegistradoXusuarioVal.Token = CType(Master.FindControl("IdentificadorPC"), HiddenField).Value.Trim()

        If oBEEquipoRegistradoXusuarioVal.Usuario <> "" Then
            Session("oBEEquipoRegistradoXusuarioVal") = oBEEquipoRegistradoXusuarioVal
        End If

    End Sub

    Private Sub ConfigureLoginManager()
        Dim _speLogin As New SPELogin(Login1)

        AddHandler _speLogin.AfterLoggedInEventHandler, AddressOf AfterLoggedIn
    End Sub

    Protected Sub AfterLoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            'Login1.DestinationPageUrl = "PRGeCIP.aspx?lg=2"
        Catch ex As Exception
            'Logger.LogException(ex)
            'WebMessage.ShowErrorMessage(ex, True)
        End Try

    End Sub

    'Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    '    Dim oBEEquipoRegistradoXusuarioVal As New BEEquipoRegistradoXusuario()
    '    oBEEquipoRegistradoXusuarioVal.Usuario = CType(Login1.FindControl("UserName"), TextBox).Text.Trim()
    '    oBEEquipoRegistradoXusuarioVal.Token = CType(Master.FindControl("IdentificadorPC"), HiddenField).Value.Trim()

    '    If oBEEquipoRegistradoXusuarioVal.Usuario <> "" Then
    '        Session("oBEEquipoRegistradoXusuarioVal") = oBEEquipoRegistradoXusuarioVal
    '    End If

    'End Sub
End Class
