Imports _3Dev.FW.Web.Security
Imports _3Dev.FW.Web
Imports _3Dev.FW.Web.Log
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil
Imports System.Collections.Generic
Imports System.IO

Partial Class MPBase
    Inherits System.Web.UI.MasterPage

    Protected WebMessage As WebMessage
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
        ConfigureLoginManager()
        'Tagueo DAX
        Try
            Dim URL As String = Request.AppRelativeCurrentExecutionFilePath.ToString()
            Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPaginasTagDaxReg")
            Dim ListPagDaxReg As New List(Of URLDAX)
            Dim Linea As String = ""
            Dim Info As String()
            Dim URLTag As String = ""
            Dim NombreDax As String = ""
            Dim lector As New StreamReader(ruta)
            Do
                Dim PagDaxReg As New URLDAX
                Linea = lector.ReadLine()
                If Not Linea Is Nothing Then
                    Info = Linea.Split(",")
                    URLTag = Info(0).ToString
                    NombreDax = Info(1).ToString
                    PagDaxReg.URLTag = URLTag
                    PagDaxReg.NombreDax = NombreDax
                    ListPagDaxReg.Add(PagDaxReg)
                End If
            Loop Until Linea Is Nothing
            For i As Integer = 0 To ListPagDaxReg.Count - 1
                If ListPagDaxReg(i).URLTag = URL Then
                    Session("NomDAX") = ListPagDaxReg(i).NombreDax
                    Exit For
                Else
                    Session("NomDAX") = ""
                End If
            Next
            lector.Close()
        Catch ex As Exception
            'Logger.LogException(ex)
        End Try
    End Sub
    Private Sub ConfigureLoginManager()
        Dim _speLogin As New SPELogin(Login1)
        _speLogin.HabilitaRedirect = True
        AddHandler _speLogin.AfterLoggedInEventHandler, AddressOf AfterLoggedIn
    End Sub
    Protected Sub AfterLoggedIn(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Login1.DestinationPageUrl = "PRGeCIP.aspx?lg=2"
        Catch ex As Exception
            'Logger.LogException(ex)
            'WebMessage.ShowErrorMessage(ex, True)
        End Try
    End Sub
    'Protected Sub Login1_LoginError(ByVal sender As Object, ByVal e As System.EventArgs) Handles Login1.LoginError
    '    Response.Redirect("~/login.aspx")
    'End Sub
    Public Sub MarcarMenu(ByVal id As String)
        'liinicio.Attributes("class") = ""
        'lipersonas.Attributes("class") = ""
        'liempresas.Attributes("class") = ""
        'Select Case id
        '    Case "liinicio"
        '        liinicio.Attributes("class") = "active_nav"
        '    Case "lipersonas"
        '        lipersonas.Attributes("class") = "active_nav"
        '    Case "liempresas"
        '        liempresas.Attributes("class") = "active_nav"
        'End Select
    End Sub
    Public Sub OcultarLogin()
        dvlnkregistroingreso.Visible = False
        dvlinklogin.Visible = False
    End Sub

    Public Sub MostrarLogin()
        dvlnkregistroingreso.Visible = True
        dvlinklogin.Visible = True
    End Sub

    ''Tagueo DAX
    'Public ReadOnly Property myVariable() As String
    '    Get
    '        Return Session("NomDAX").ToString
    '    End Get
    'End Property

    Protected Sub MPBase_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            Page.Header.DataBind()
            Page.DataBind()
        End If

    End Sub
End Class