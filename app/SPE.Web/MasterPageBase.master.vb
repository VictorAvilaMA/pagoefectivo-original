
Partial Class MasterPageBase
    Inherits System.Web.UI.MasterPage

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")
    End Sub

    Protected Sub MasterPageBase_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Page.Header.DataBind()
        End If
    End Sub
End Class

