Imports _3Dev.FW.Web.Security
Partial Class MasterPageBase2
    Inherits System.Web.UI.MasterPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            lblNombreUsuario.Text = UserInfo.Alias
            If (lblNombreUsuario.Text = "") Then
                lblNombreUsuario.Text = UserInfo.Nombres
            End If
            If Not (UserInfo.Rol = "Cliente") Then
                lblRol.Text = " (" + UserInfo.Rol + ")"
            Else
                lblRol.Text = ""
            End If

        End If
    End Sub


    Protected Sub LoginStatus1_LoggedOut(ByVal sender As Object, ByVal e As System.EventArgs) Handles LoginStatus1.LoggedOut
        ClearUserInfo()
    End Sub
End Class

