Imports System.Web.Security
Imports _3Dev.FW.Web.Security
Imports System.Web
Imports SPE.Web.Seguridad
Imports System.Collections.Generic
Imports _3Dev.FW.Web
Imports System.IO
Imports System.Drawing
Imports SPE.EmsambladoComun
Imports SPE.Entidades

Partial Class MasterPagePrincipal
    Inherits System.Web.UI.MasterPage

    Dim DestImage As Bitmap

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) IsNot Nothing Then
            RegPC()
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()

        If Not Context.User Is Nothing Then

            Dim mySMP As SPE.Web.Seguridad.SPESiteMapProvider = CType(System.Web.SiteMap.Providers("SPESiteMapProvider"), SPE.Web.Seguridad.SPESiteMapProvider)
            mySMP.OnSiteMapChanged()

            If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
                Response.Redirect("~/SEG/sinautrz.aspx")
            End If

            literalMenu.Text = UtilSiteMap.getSiteMap(mySMP, UserInfo.PaginasAcceso) 'devolucion se modifica
            literalMenuCabezera.Text = UtilSiteMap.getSiteMapCabezera()

        End If
        If Not Page.IsPostBack Then
            lblNombreUsuario.Text = SPE.Web.PaginaBase.UserInfoNombreOAlias()
            'If (lblNombreUsuario.Text = "") Then
            '    lblNombreUsuario.Text = 
            'End If
            If Not (UserInfo.Rol = "Cliente") Then
                lblRol.Text = " (" + UserInfo.Rol + ")"
            Else
                lblRol.Text = ""
            End If
            '
            'ltFechaActual.Text = Date.Now.ToShortDateString()
            RegistrarLog()
            ManejoImagenes()
        Else

        End If
        lblFecha.Text = CStr(Date.Now.ToLongDateString)
        literalFecha.Text = "<img src=""" + System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/calendario.png"" alt=""""> " & Date.Now.ToString(" dd MMM yyyy ") _
            & " <img src=""" + System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/reloj.png"" alt="""">  " & Date.Now.ToString(" hh:mm tt")
        'findControlGridView(Me.Controls, 2)

    End Sub
    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        findControlGridView(Me.Controls)
        findControlToDataBind(Me.Controls)
        MyBase.OnPreRender(e)
    End Sub

    Public Sub findControlGridView(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls Then
                findControlGridView(c.Controls)
            End If
            If c.GetType() Is GetType(GridView) Then
                Dim gridViewPage As GridView = CType(c, GridView)
                If gridViewPage.HeaderRow IsNot Nothing Then
                    gridViewPage.UseAccessibleHeader = True
                    gridViewPage.HeaderRow.TableSection = TableRowSection.TableHeader
                    'gridViewPage.PagerSettings.Mode = PagerButtons.NextPreviousFirstLast
                End If
            End If
        Next
    End Sub

    Public Sub findControlToDataBind(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls And c.GetType IsNot GetType(GridView) Then
                findControlToDataBind(c.Controls)
            End If
            If c.GetType() Is GetType(ImageButton) Or c.GetType() Is GetType(Button) Or c.GetType() Is GetType(Image) Or c.GetType() Is GetType(LinkButton) Then
                c.DataBind()
            End If
        Next
    End Sub


#Region "IMG2BYTE-BYTE2IMG"

    Public Shared Function Image2Bytes(ByVal img As Image) As Byte()
        Dim sTemp As String = Path.GetTempFileName()
        Dim fs As New FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite)
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        '' Cerrarlo y volverlo a abrir
        '' o posicionarlo en el primer byte
        ''fs.Close()
        ''fs = New FileStream(sTemp, FileMode.Open, FileAccess.Read)
        fs.Position = 0
        ''
        Dim imgLength As Integer = CInt(fs.Length)
        Dim bytes(0 To imgLength - 1) As Byte
        fs.Read(bytes, 0, imgLength)
        fs.Close()
        Return bytes
    End Function

    Public Function Bytes2Image(ByVal bytes() As Byte) As Image
        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm
    End Function

#End Region

#Region "Encriptar"

    Function Encripta(ByVal Texto As String) As String
        Dim Clave As String, i As Integer, Pass2 As String
        Dim CAR As String, Codigo As String
        Clave = "#SPE%$"
        Pass2 = ""
        For i = 1 To Len(Texto)
            CAR = Mid(Texto, i, 1)
            Codigo = Mid(Clave, ((i - 1) Mod Len(Clave)) + 1, 1)
            Pass2 = Pass2 & Microsoft.VisualBasic.Strings.Right("0" & Hex(Asc(Codigo) Xor Asc(CAR)), 2)
        Next i
        Encripta = Pass2
    End Function

#End Region

#Region "Eliminar Tempo"

    Private Sub DeleteFile(ByVal directory1 As DirectoryInfo, ByVal directory2 As DirectoryInfo, ByVal Img As String, ByVal ImgTemp As String)
        For Each f As FileInfo In directory1.GetFiles()
            If f.Name = Img Then
                If f.IsReadOnly Then
                    f.Attributes = f.Attributes And Not IO.FileAttributes.ReadOnly
                End If
                File.Delete(f.FullName)
            End If
        Next
        For Each f As FileInfo In directory2.GetFiles()
            If f.Name = Img Or f.Name = ImgTemp Then
                If f.IsReadOnly Then
                    f.Attributes = f.Attributes And Not IO.FileAttributes.ReadOnly
                End If
                File.Delete(f.FullName)
            End If
        Next
    End Sub
#End Region

    Protected Sub LoginStatus1_LoggedOut(ByVal sender As Object, ByVal e As System.EventArgs) Handles LoginStatus1.LoggedOut
        Try
            Dim Correo As String = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).Email
            Dim Ini As String = Encripta(Correo)
            Dim Img As String = Ini & ".jpg"
            Dim ImgTemp As String = Ini & "temp.jpg"

            Dim pathCurrent As String
            pathCurrent = HttpContext.Current.Server.MapPath("~/")
            Dim ruta1, ruta2 As String
            ruta1 = pathCurrent & "img\tempoload"
            ruta2 = pathCurrent & "img\temposave"
            Dim raiz1 As New DirectoryInfo(ruta1)
            Dim raiz2 As New DirectoryInfo(ruta2)
            DeleteFile(raiz1, raiz2, Img, ImgTemp)
        Catch ex As Exception
        End Try
        ClearUserInfo()
    End Sub

    Protected Sub RegistrarLog()

        'computer_name = "io"
        'System.Net.Dns.Resolve(Request.ServerVariables("remote_addr")).HostName
        'Split(System.Net.Dns.Resolve(Request.ServerVariables("remote_addr")).HostName, ".")
        Dim PC As String
        PC = Request.ServerVariables("REMOTE_ADDR")

        If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
            Dim TieneDataCompleta As Boolean = CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).TieneInfoCompleta
            If Not TieneDataCompleta Then
                literalMenu.Visible = False
            End If
        End If

        'Dim PC As String
        'PC = Request.LogonUserIdentity.User.ToString()
        'Dim PC2 As String
        'PC2 = Request.LogonUserIdentity.Name
        'Dim PC3 As String
        'PC3 = Request.LogonUserIdentity.Owner.ToString()

        Using oCComun As New SPE.Web.CComun()
            Dim oLogNavegacion As New BELogNavegacion()
            Dim RutaFormateada As String
            Dim TamanioRuta As Integer
            RutaFormateada = Request.Url.AbsolutePath
            TamanioRuta = RutaFormateada.Length - 1
            oLogNavegacion.IdUsuario = SPE.Web.PaginaBase.UserInfo.IdUsuario.ToString()
            oLogNavegacion.UrlPage = RutaFormateada.Substring(1, TamanioRuta)
            oLogNavegacion.Accion = ""
            oLogNavegacion.PCName = PC
            oCComun.RegistrarLogNavegacion(oLogNavegacion)
        End Using
    End Sub

    Public Sub ManejoImagenes()
        'Manejo de Imágenes*************************************************************************************
        Dim ClaveSinMD5 As String
        Dim Clave As String
        Dim img As Image
        Dim DG As String = ""
        ClaveSinMD5 = SPE.Web.PaginaBase.UserInfo.Email
        Clave = Encripta(ClaveSinMD5)
        'Si el campo ImagenAvatar trae información
        If Not SPE.Web.PaginaBase.UserInfo.ImagenAvatar Is Nothing Then
            'Obtengo el array de byte y lo convierto en imagen
            img = Bytes2Image(CType(SPE.Web.PaginaBase.UserInfo.ImagenAvatar, Byte()))

            'le digo que el nombre obtenido sea el del correo 
            DG = "~\img\temposave\" & Clave & ".jpg"
            Try
                DestImage = New Bitmap(img)
            Catch ex As Exception
                Dim pathCurrent As String
                pathCurrent = Server.MapPath("~/")
                Dim raiz As String
                raiz = pathCurrent & "img\avatar\avatar.jpg"
                'la convierto en imagen
                DestImage = New Bitmap(raiz)
            End Try

            'si es NULL entonces cojo la imagen por defecto de Avatar en el Else
        Else
            'Obtengo la carpeta raiz para anexarla a la imagen por defecto
            Dim pathCurrent As String
            pathCurrent = Server.MapPath("~/")
            Dim raiz As String
            raiz = pathCurrent & "img\avatar\avatar.jpg"
            'la convierto en imagen
            DestImage = New Bitmap(raiz)

        End If
        ' Generar un bitmap para el resultado
        Dim DestToImage As New Bitmap(DestImage.Width, DestImage.Height)

        ' Generar un objeto Grafico para el bitmap resultante
        Dim gr_dest As Graphics = Graphics.FromImage(DestToImage)

        ' Copiar la imagen origen al bitmap destino
        gr_dest.DrawImage(DestImage, 0, 0, DestToImage.Width, DestToImage.Height)

        Try
            If Not SPE.Web.PaginaBase.UserInfo.ImagenAvatar Is Nothing Then
                'La almaceno en img para poder ponerlo como URL
                DestToImage.Save(Context.Server.MapPath(DG), System.Drawing.Imaging.ImageFormat.Jpeg)
            End If

            'Guardo la imagen cargada en un viewstate para poder almacenarla en caso no se cambie la imagen
            Session("ImageIni") = Image2Bytes(DestImage)

            DestImage.Dispose()
            DestToImage.Dispose()
            gr_dest.Dispose()
            '
            'Muestra imagen comprimida
            If Not SPE.Web.PaginaBase.UserInfo.ImagenAvatar Is Nothing Then
                Me.ImgAvatar.ImageUrl = "~/img/temposave/" & Clave & ".jpg?654321"
            Else
                Me.ImgAvatar.ImageUrl = System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") & "/App_Themes/SPE/img/avatar.jpg"
            End If
            'End If
        Catch ex As Exception
            Me.ImgAvatar.ImageUrl = System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") & "/App_Themes/SPE/img/avatar.jpg"
        End Try



        'Manejo de Imágenes**************************************************************************************
    End Sub

    Protected Sub RegPC()
        'Using oCComunVal As New SPE.Web.CComun()
        Dim oCComunVal As New SPE.Web.CComun()
        Dim oBEEquipoRegistradoXusuarioVal As New BEEquipoRegistradoXusuario()
        Dim oBEERXUResultado As New List(Of BEEquipoRegistradoXusuario)
        Dim Res As Boolean
        Dim FlagRegPc As Boolean
        Dim oBEEU As New BEEquipoRegistradoXusuario()
        oBEEU = CType(HttpContext.Current.Session("oBEEquipoRegistradoXusuarioVal"), BEEquipoRegistradoXusuario)

        If Not oBEEU Is Nothing Then
            oBEEquipoRegistradoXusuarioVal.Token = oBEEU.Token
            oBEEquipoRegistradoXusuarioVal.Usuario = oBEEU.Usuario
        End If
        oBEERXUResultado = oCComunVal.ValidarEquipoRegistradoXusuario(oBEEquipoRegistradoXusuarioVal)

        Res = oBEERXUResultado(0).Resultado
        FlagRegPc = oBEERXUResultado(0).FlagRegistarPc

        If FlagRegPc Then
            If oBEEquipoRegistradoXusuarioVal.Token.Length > 0 Then
                If Not Res Then
                    HttpContext.Current.Response.Redirect("../PrRegDisp.aspx")
                End If
            End If
        End If
        'End Using
    End Sub

    'Protected Sub MenuPE_MenuItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuPE.MenuItemDataBound
    '    '
    '    If e.Item.DataPath = "40" Then
    '        e.Item.NavigateUrl = "javascript:openPRLiCaBlReporte();"
    '    End If
    '    If e.Item.DataPath = "57" Then
    '        e.Item.NavigateUrl = "javascript:openPOSPRLiCaBlReporte();"
    '    End If
    '    '
    '    If sender.BindingContainer.Page.AppRelativeVirtualPath.ToString.ToUpper() = e.Item.NavigateUrl.ToUpper() Then
    '        e.Item.Selected = True
    '    End If
    '    '
    'End Sub

    'Public Function GetIpAddres() As String
    '     Dim  hostEntry As  System.Net.IPHostEntry hostEntry = System.Net.Dns.GetHostEntry(Request.ServerVariables["REMOTE_ADDR"]);
    'End Function

    '    public String GetIPAddress()
    '{
    '    System.Net.IPHostEntry hostEntry = System.Net.Dns.GetHostEntry(Request.ServerVariables["REMOTE_ADDR"]);
    '    System.Net.IPAddress[] ipAdressArray = hostEntry.AddressList;
    '    return ipAdressArray[ipAdressArray.Length - 1].ToString();
    '}
End Class

