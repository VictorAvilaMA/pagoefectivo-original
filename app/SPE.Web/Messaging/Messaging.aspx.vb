﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Drawing.Text
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Web.Script.Services
Imports System.Web.Services
Imports Newtonsoft.Json
Imports SPE.Aws
Imports SPE.Aws.Contracts
Imports SPE.Entidades
Imports SPE.Logging
Imports SPE.Web.SMS
Imports System.Globalization

Namespace SPE.Web.Messaging
    Partial Class Messaging
        Inherits System.Web.UI.Page

        Private Const MessageId As Integer = 1
        Private Const Distortion As Integer = 0
        Private Const Height As Integer = 36
        Private Const Width As Integer = 116
        Private Const FontFamily As String = "Consolas"
        Private Const FontSize As Integer = 20
        Private Const CaptchaSize As Integer = 5
        Private Const CaptchaChars As String = "0123456789"

        Private Shared ReadOnly BackgroundColor As Color = Color.White
        Public Shared captchaCodeSession As String = String.Empty

        Private Shared ReadOnly _logger As ILogger = New Logger()

#Region "Generate Captcha"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Page.IsPostBack Then
                CreateCaptchaImage()
            End If
        End Sub

        Public Sub CreateCaptchaImage()

            Dim captchaCode = GenerateCaptchaCode(CaptchaSize)

            captchaCodeSession = captchaCode

            Dim imageStream = BuildImage(captchaCode)
            imageStream.Position = 0

            Dim bmp = New Bitmap(imageStream)

            Response.ContentType = "image/jpeg"
            bmp.Save(Response.OutputStream, ImageFormat.Jpeg)
            bmp.Dispose()

        End Sub

        Function GenerateCaptchaCode(ByVal size As Integer) As String
            If size <= 0 Then
                Throw New ArgumentException("Size cannot be less than or equal to 0")
            End If

            Const chars As String = CaptchaChars
            Dim charsLength = CaptchaChars.Length
            Dim stringChars = New Char(size - 1) {}
            Dim fillBuffer = New Byte(3) {}
            Dim rngCsp = New RNGCryptoServiceProvider()
            For i = 0 To stringChars.Length - 1
                rngCsp.GetBytes(fillBuffer)
                Dim number = Math.Abs(BitConverter.ToInt32(fillBuffer, 0)) Mod charsLength
                stringChars(i) = chars(number)
            Next

            Dim captcha = New String(stringChars)
            Return captcha
        End Function

        Function BuildImage(ByVal captchaCode As String, Optional ByVal imageHeight As Integer = Height, Optional ByVal imageWidth As Integer = Width, Optional ByVal fontSize As Integer = FontSize, Optional ByVal distortion As Integer = Distortion) As MemoryStream
            Dim memoryStream = New MemoryStream()
            Dim captchaImage = New Bitmap(imageWidth, imageHeight, PixelFormat.Format64bppArgb)
            Dim cache = New Bitmap(imageWidth, imageHeight, PixelFormat.Format64bppArgb)
            Dim graphicsTextHolder = Graphics.FromImage(captchaImage)
            graphicsTextHolder.Clear(BackgroundColor)
            graphicsTextHolder.TextRenderingHint = TextRenderingHint.ClearTypeGridFit
            graphicsTextHolder.SmoothingMode = SmoothingMode.AntiAlias
            graphicsTextHolder.FillRectangle(Brushes.White, New Rectangle(0, 0, captchaImage.Width, captchaImage.Height))
            Dim i As Integer
            Dim pen = New Pen(Color.SteelBlue)
            Dim rand = New Random(Convert.ToInt32(DateTime.Now.Millisecond))
            For i = 1 To 10 - 1
                pen.Color = Color.FromArgb((rand.[Next](0, 255)), (rand.Next(0, 255)), (rand.Next(0, 255)))
                Dim r = rand.Next(0, Convert.ToInt32((imageWidth / 3)))
                Dim x = rand.Next(0, imageWidth)
                Dim y = rand.Next(0, imageHeight)
                graphicsTextHolder.DrawEllipse(pen, x - r, y - r, r, r)
            Next

            Using sf = New StringFormat() With {.Alignment = StringAlignment.Center, .LineAlignment = StringAlignment.Center}
                graphicsTextHolder.DrawString(captchaCode, New Font(FontFamily, fontSize, FontStyle.Italic), New SolidBrush(Color.Black), New Rectangle(0, 0, captchaImage.Width, captchaImage.Height), sf)
            End Using

            For y = 0 To imageHeight - 1
                For x = 0 To imageWidth - 1
                    Dim newX = CInt((x + (distortion * Math.Sin(Math.PI * y / 64))))
                    Dim newY = CInt((y + (distortion * Math.Cos(Math.PI * x / 64))))
                    If newX < 0 OrElse newX >= imageWidth Then newX = 0
                    If newY < 0 OrElse newY >= imageHeight Then newY = 0
                    cache.SetPixel(x, y, captchaImage.GetPixel(newX, newY))
                Next
            Next

            cache.Save(memoryStream, ImageFormat.Jpeg)
            memoryStream.Position = 0
            Return memoryStream
        End Function
#End Region

#Region "Validate Captcha"
        <WebMethod(EnableSession:=True)> _
        <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)> _
        Public Shared Sub ValidateCaptcha(ByVal captchaCode As String)
            Dim js = New Script.Serialization.JavaScriptSerializer()
            Dim invData() As Object = New Object(0) {}
            Dim reqData() As Object = New Object(0) {}
            Dim errData() As Object = New Object() {}
            Dim errSession() As Object = New Object() {}
            Dim response = New Object
            Dim data = New Object

            Try
                invData(0) = New With {.code = 113, .message = "Valor inválido.", .field = "captchaCode"}
                reqData(0) = New With {.code = 112, .message = "Requerido.", .field = "captchaCode"}

                If captchaCodeSession IsNot Nothing Then

                    If String.IsNullOrEmpty(captchaCode) = False Then

                        If captchaCode = captchaCodeSession Then
                            response = New With {.code = 100, .message = "Solicitud exitosa.", .data = data}
                        Else
                            response = New With {.code = 111, .message = "Solicitud con datos inválidos.", .data = invData}
                        End If

                    Else
                        response = New With {.code = 111, .message = "Solicitud con datos inválidos.", .data = reqData}
                    End If

                Else
                    response = New With {.code = 111, .message = "Solicitud con datos inválidos.", .data = errSession}
                End If

            Catch ex As Exception
                response = New With {.code = 166, .message = "Internal server error.", .data = errData}
            End Try

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ContentType = "application/json; charset=utf-8"
            HttpContext.Current.Response.Write(js.Serialize(response))
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.End()

        End Sub
#End Region

#Region "Messaging"
        Private Shared Sub CreateResponse(codeResponse As ResponseCodeSms, messageResponse As String, errors As Object)
            Dim responseMessage As New ResponseSms() With {.Code = codeResponse.GetHashCode(), .Message = messageResponse, .Data = errors}
            Dim response As String = JsonConvert.SerializeObject(responseMessage)

            _logger.Info("Se devuelve respuesta: " + response)

            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Buffer = True
            HttpContext.Current.Response.Charset = "utf-8"
            HttpContext.Current.Response.ContentEncoding = Encoding.UTF8
            'HttpContext.Current.Response.StatusCode = statusCode
            HttpContext.Current.Response.ContentType = "application/json"
            HttpContext.Current.Response.Output.Write(response)
            HttpContext.Current.Response.Flush()
        End Sub

        Private Shared Function ValidateExpiration(cip As Long) As List(Of ErrorData)
            Dim errors As New List(Of ErrorData)
            Dim validate As String = New COrdenPago().ValidateCipExpiration(cip)

            If validate = "0" Then
                errors.Add(New ErrorData() With {.Code = ResponseCodeSms.Expirated, .Message = ResponseMessageSms.Expirated, .Field = "cip"})
            End If

            Return errors
        End Function

        Private Shared Function RegisterCellPhoneNumber(requestSms As RequestSms) As Boolean
            Dim cellPhoneNumber As New BEMobileMessaging() With {
                    .PaymentOrderId = Long.Parse(requestSms.Cip),
                    .CountryCode = requestSms.CountryCode,
                    .PhoneNumber = requestSms.CellPhoneNumber}

            Dim result As Integer = New COrdenPago().RegisterCellPhoneNumber(cellPhoneNumber)

            If result = 0 Then
                Return True
            End If

            Return False
        End Function

        Private Shared Function ValidateMessage(requestSms As RequestSms) As List(Of ErrorData)
            Dim errors As New List(Of ErrorData)

            If String.IsNullOrEmpty(requestSms.CountryCode) Then
                errors.Add(New ErrorData() With {.Code = ResponseCodeSms.Requerid, .Message = ResponseMessageSms.Requerid, .Field = "CountryCode"})
            End If

            If String.IsNullOrEmpty(requestSms.CellPhoneNumber) Then
                errors.Add(New ErrorData() With {.Code = ResponseCodeSms.Requerid, .Message = ResponseMessageSms.Requerid, .Field = "cellphonenumber"})
            End If

            If String.IsNullOrEmpty(requestSms.Cip) Then
                errors.Add(New ErrorData() With {.Code = ResponseCodeSms.Requerid, .Message = ResponseMessageSms.Requerid, .Field = "cip"})
            End If

            Return errors
        End Function

        Private Shared Function SendMessage(requestSms As RequestSms) As ResponseMessage
            Dim message As RequestMessageApi

            Dim messagingApi As New ApiGateway(awsAccessKey:=ConfigurationManager.AppSettings("MESSAGING_APIGATEWAY_ACCESS_KEY"),
                                               awsSecretKey:=ConfigurationManager.AppSettings("MESSAGING_APIGATEWAY_SECRET_ACCESS_KEY"),
                                               Region:=ConfigurationManager.AppSettings("MESSAGING_APIGATEWAY_REGION"))

            _logger.Info("Se consulta OrdePago")
            Dim ordenPago As New BEOrdenPago With {.IdOrdenPago = Integer.Parse(requestSms.Cip)}
            ordenPago = New COrdenPago().ConsultarOrdenPagoPorId(ordenPago)

            _logger.Info("Se consulta Servicio")
            Dim servicio As BEServicio = New CServicio().ObtenerServicioPorID(ordenPago.IdServicio)

            _logger.Info("Se consulta Mensaje")
            Dim messageTemplate As String = New COrdenPago().GetMessageSms(MessageId)

            _logger.Info("Se reemplaza parametros en mensaje")
            Dim expirationDateString As String = (ordenPago.FechaVencimiento.ToString("dd MMM hh:mm", New CultureInfo("es-PE")) _
                                       & " " & ordenPago.FechaVencimiento.ToString("tt", CultureInfo.InvariantCulture)).Replace(".", "").ToUpper()

            messageTemplate = messageTemplate.Replace("[Empresa]", servicio.Nombre).Replace("[CIP]", ordenPago.IdOrdenPago.ToString()).Replace("[Moneda]", (IIf(ordenPago.IdMoneda = 1, "S/", "$"))) _
                .Replace("[Importe]", ordenPago.Total.ToString("#,##0.00")).Replace("[FechaVencimiento]", expirationDateString)

            message = New RequestMessageApi() With {.CellPhoneNumber = requestSms.CountryCode & requestSms.CellPhoneNumber,
                .Message = messageTemplate}

            _logger.Info("Invocación del apigateway")
            Return messagingApi.PostMessage(Of RequestMessageApi)(message:=message, endPointUrl:=ConfigurationManager.AppSettings("MESSAGING_APIGATEWAY_URL"))
        End Function

        ''' <summary>
        ''' Autor   : Angello Peña
        ''' Proyecto: Mensajeria
        ''' Fecha   : 22/02/2018
        ''' Descripción: Envia un mensaje de texto de generación de cip
        ''' </summary>
        ''' <param name="requestSms">Solicitud del mensaje</param>
        <WebMethod()>
        <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
        Public Shared Sub SendSmsMessage(requestSms As RequestSms)
            Try
                Dim errors As List(Of ErrorData) = ValidateMessage(requestSms)

                If errors.Count > 0 Then
                    CreateResponse(ResponseCodeSms.BadRequest, ResponseMessageSms.BadRequest, errors.ToArray())
                    Exit Sub
                End If

                _logger.Info("Se valida Expiracion de cip: " + requestSms.Cip)
                errors = ValidateExpiration(Long.Parse(requestSms.Cip))
                If errors.Count > 0 Then
                    CreateResponse(ResponseCodeSms.BadRequest, ResponseMessageSms.BadRequest, errors.ToArray())
                    Exit Sub
                End If

                _logger.Info("Se registra cellphone: " + requestSms.CellPhoneNumber + " - cip:" + requestSms.Cip)
                Dim result As Boolean = RegisterCellPhoneNumber(requestSms)
                If Not result Then
                    CreateResponse(ResponseCodeSms.InternalError, ResponseMessageSms.InternalError, New Object() {})
                    Exit Sub
                End If

                _logger.Info("Se envia mensaje al apigateway")
                Dim responseApi As ResponseMessage = SendMessage(requestSms)

                If responseApi.Code <> ResponseCodeSms.Ok Then
                    CreateResponse(ResponseCodeSms.BadRequest, ResponseMessageSms.BadRequest, New Object() {})
                Else
                    CreateResponse(ResponseCodeSms.Ok, ResponseMessageSms.Success, New Object)
                End If
            Catch ex As Exception
                _logger.Debug("Error | Message: " + ex.Message + " - Trace: " + ex.InnerException.StackTrace)
                CreateResponse(ResponseCodeSms.InternalError, ResponseMessageSms.InternalError, New Object() {})
            Finally
                HttpContext.Current.Response.End()
            End Try
        End Sub
#End Region
    End Class
End Namespace