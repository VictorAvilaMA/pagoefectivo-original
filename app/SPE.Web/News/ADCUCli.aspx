<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="~/News/ADCUCli.aspx.vb" Inherits="ADM_ADAgen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server" ID="ScriptManager">
    </asp:ScriptManager>
    <div class="dlgrid">
        <h1>
            <asp:Literal runat="server" Text="Solicitud de Creacion de Cuenta"></asp:Literal>
        </h1>
        <asp:UpdatePanel ID="UpdatePanelDatos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
            <ContentTemplate>
                <asp:Panel ID="pnlPage" runat="server">
                    <fieldset>
                        <asp:Panel ID="pnlInformacionRegistro" CssClass="inner-col-right" runat="server">
                            <h4>
                                1. Información de la Cuenta</h4>
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt15 dd30">
                                    <dt class="desc">
                                        <asp:Label ID="lblTipoAgente" runat="server" Text="Tipo Moneda:" CssClass="EtiquetaLargo"></asp:Label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:DropDownList ID="ddlTipoMoneda" runat="server" CssClass="TextoLargo">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvTipoAgente" runat="server" ErrorMessage="Seleccione un Tipo Agente"
                                            ValidationGroup="IsNullOrEmpty" ControlToValidate="ddlTipoAgente">*</asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                            </div>
                            <fieldset id="fisEstado" class="ContenedorEtiquetaTexto" runat="server">
                                <div class="w100 clearfix dlinline">
                                    <dl class="clearfix dt15 dd30">
                                        <dt class="desc">
                                            <asp:Label ID="lblEstado" runat="server" Text="Alias:" CssClass="EtiquetaLargo"></asp:Label>
                                        </dt>
                                        <dd class="camp">
                                            <asp:TextBox ID="TxtAlias" runat="server" CssClass="***"></asp:TextBox>
                                        </dd>
                                    </dl>
                                </div>
                            </fieldset>
                            <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" ImageUrl="~/img/closeX.GIF"
                                Style="display: none;" CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                            <div id="fsBotonera" runat="server" class="clearfix dlinline btns3">
                                <asp:Label ID="lblTransaccion" runat="server" Visible="False" Width="650px" CssClass="MensajeTransaccion"
                                    Style="padding-bottom: 5px"></asp:Label>
                                <asp:Button ID="btnRegistrar" CssClass="btnSolicitar" runat="server" Text="Solicitar Cuenta"
                                    Visible="False" OnClientClick="return ConfirmMe();" ValidationGroup="IsNullOrEmpty">
                                </asp:Button>
                            </div>
                            <asp:HiddenField ID="hdnIdCliente" runat="server" />
                        </asp:Panel>
                    </fieldset>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRegistrar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="gvResultado" EventName="RowCommand"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:Panel Style="display: none" ID="pnlPopupAgencias" runat="server">
        <div id="divBusqueda" class="divContenedor">
            <asp:UpdatePanel ID="upnlListAgencias" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div21" class="divContenedorTitulo">
                        <div style="float: left">
                            Busqueda de Agencias Recaudadoras
                        </div>
                        <div style="float: right">
                            <asp:ImageButton ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" runat="server"
                                ImageUrl="~/img/closeX.GIF" CausesValidation="false"></asp:ImageButton>
                        </div>
                    </div>
                    <br />
                    <fieldset class="ContenedorEtiquetaTexto">
                        <div id="div19" class="EtiquetaTextoIzquierda">
                            <asp:Label ID="Label1" runat="server" CssClass="EtiquetaLargo" Text="Nombre Comercial:"></asp:Label>
                            <asp:TextBox ID="txtBusAgencia" runat="server" CssClass="Texto" Width="253px"></asp:TextBox>
                            <asp:ImageButton ID="ibtnListarAgencia" runat="server" CausesValidation="false" ToolTip="Buscar"
                                ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/images/lupa.gif" %>'>
                            </asp:ImageButton>
                        </div>
                    </fieldset>
                    <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" Width="580px" BackColor="White"
                        AllowPaging="True" OnRowCommand="gvResultado_RowCommand" CellPadding="3" BorderWidth="1px"
                        BorderStyle="None" BorderColor="#CCCCCC" AutoGenerateColumns="False" OnPageIndexChanging="gvResultado_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="IdAgenciaRecaudadora"></asp:BoundField>
                            <asp:BoundField DataField="NombreComercial" HeaderText="Nombre Comercial" HtmlEncode="False">
                            </asp:BoundField>
                            <asp:BoundField DataField="Contacto" HeaderText="Contacto"></asp:BoundField>
                            <asp:BoundField DataField="Telefono" HeaderText="Telefono" Visible="False"></asp:BoundField>
                            <asp:BoundField DataField="Email" HeaderText="E-mail"></asp:BoundField>
                            <asp:TemplateField HeaderText="Seleccionar">
                                <ItemTemplate>
                                    <asp:ImageButton CommandArgument='<%# (DirectCast(Container,GridViewRow)).RowIndex %>'
                                        ID="imgActualizar" runat="server" CommandName="Select" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/ok2.jpg" %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--<asp:CommandField SelectImageUrl="~/images/ok2.jpg" SelectText="Seleccionar" ShowSelectButton="True"
                                ButtonType="Image"></asp:CommandField>--%>
                        </Columns>
                        <EmptyDataTemplate>
                            No se encontraron Registros
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
</asp:Content>
