Imports SPE.Entidades
Imports System.IO
Partial Class ADM_ADAgen
    Inherits _3Dev.FW.Web.PageBase

    'EVENTO LOAD DE LA PAGINA
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            '

            Try
                OcultarDDL(True)
                '
                Page.SetFocus(TxtAlias)
                CargarCombos()

            Catch ex As Exception
                '
                '_3Dev.FW.Web.Log.Logger.LogException(ex)
                Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
                '
            End Try
            
        End If
        '
    End Sub

    'PROCESO DE REGISTRO
    Private Sub CargarProcesoRegistro()
        '
        '
        'CargarCombosUbigeo("Pais")
        'CargarCombosUbigeo("Departamento")
        'CargarCombosUbigeo("Ciudad")
        '
    End Sub

    'REGISTRAR AGENTE
    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        '
        Try
            '
            'PROCESO DE REGISTRO
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgente As BEAgente = CargarDatosBEAgente()
            obeAgente.IdUsuarioCreacion = UserInfo.IdUsuario
            obeAgente.CodigoRegistro = Guid.NewGuid.ToString
            obeAgente.IdOrigenRegistro = SPE.EmsambladoComun.ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto
            If objCAdministrarAgenciaRecaudadora.RegistrarAgente(obeAgente) = _
                SPE.EmsambladoComun.ParametrosSistema.ExisteEmail.Existe Then

            Else
                lblTransaccion.Visible = True
                btnRegistrar.Enabled = False
                pnlPage.Enabled = False
                lblTransaccion.Text = "Se registr� satisfactoriamente la Solicitud"
            End If
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try

    End Sub


    Private Sub InicializarddlEstado(ByVal idEstado As String)
        'If idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Or idEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
        '    ddlEstado.Enabled = False
        '    ddlEstado.SelectedValue = idEstado
        'Else
        '    ddlEstado.Enabled = True
        '    ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente)))
        '    ddlEstado.Items.RemoveAt(ddlEstado.Items.IndexOf(ddlEstado.Items.FindByValue(SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado)))
        '    ddlEstado.SelectedValue = idEstado
        'End If
    End Sub
    'CARGAMOS LOS DATOS DE LA AGENCIA RECAUDADORA
    Private Function CargarDatosBEAgente() As BEAgente
        '
        Try
            '
            Dim obeAgente As New BEAgente
            obeAgente.Email = TxtAlias.Text
            obeAgente.IdTipoAgente = ddlTipoMoneda.SelectedValue

            Return obeAgente
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
            Throw New Exception(New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage)
        End Try
        '
    End Function

    'CARGAMOS LOS COMBOS DE ESTADO
    Private Sub CargarCombos()
        '
        Dim objCParametro As New SPE.Web.CAdministrarParametro
        ddlTipoMoneda.DataTextField = "Descripcion" : ddlTipoMoneda.DataValueField = "Id" : ddlTipoMoneda.DataSource = objCParametro.ConsultarParametroPorCodigoGrupo("TAGT") : ddlTipoMoneda.DataBind() : ddlTipoMoneda.Items.Insert(0, New ListItem(SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo, ""))
        '
    End Sub

    'LIMIAR FORM
    Protected Sub LimpiarForm()
        '
        TxtAlias.Text = ""
      
    End Sub

    'Lista de Agencias Recaudadoras
    Protected Sub ibtnListarAgencia_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnListarAgencia.Click
        '
        ListarAgenciaRecaudadora()
        OcultarDDL(False)
        '
    End Sub

    'Listar Agencia Recaudadora
    Private Sub ListarAgenciaRecaudadora()
        '
        Try
            '
            Dim objCAdministrarAgenciaRecaudadora As New SPE.Web.CAdministrarAgenciaRecaudadora
            Dim obeAgenciaRecaudadora As New BEAgenciaRecaudadora
            '
            obeAgenciaRecaudadora.NombreComercial = txtBusAgencia.Text
            obeAgenciaRecaudadora.Contacto = ""
            obeAgenciaRecaudadora.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo
            '
            gvResultado.DataSource = objCAdministrarAgenciaRecaudadora.ConsultarAgenciaRecaudadora(obeAgenciaRecaudadora)
            gvResultado.DataBind()
            '
        Catch ex As Exception
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        '
    End Sub

    'SELECCIONAMOS LA AGENCIA
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        '
        'If Not e Is Nothing Then
        '    lblIdAgenciaRecaudadora.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument.ToString())).Cells(0).Text
        '    txtDescAgenciaRecaudadora.Text = gvResultado.Rows(Convert.ToInt32(e.CommandArgument.ToString())).Cells(1).Text
        '    OcultarDDL(True)
        'End If
        '
    End Sub



    'PAGINACION DE GRILLA
    Protected Sub gvResultado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        '
        gvResultado.PageIndex = e.NewPageIndex
        ListarAgenciaRecaudadora()
        '
    End Sub

    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        OcultarDDL(True)
    End Sub

    Private Sub OcultarDDL(ByVal flag As Boolean)
        '    ddlCiudad.Visible = flag
        '    ddlDepartamento.Visible = flag
        '    ddlPais.Visible = flag

    End Sub


End Class
