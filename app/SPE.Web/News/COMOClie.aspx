<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COMOClie.aspx.vb" Inherits="ADM_COEmCo" Title="PagoEfectivo - Consultar Archivos Generados" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Movimientos de dinero</h2>
    <div class="inner-col-right">
        <div class="dlgrid">
            <fieldset>
                <div>
                    <div>
                        Numero de Cuenta &&&&&&&&&&&&
                    </div>
                    <div>
                        Alias de la Cuenta &&&&&&&&&&&&
                    </div>
                </div>
                <h3>
                    Criterios de b�squeda</h3>
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="cont-panel-search">
                            <div class="w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd60">
                                    <dt class="desc">
                                        <label>
                                            Tipo Movimiento:</label>
                                    </dt>
                                    <dd class="camp">
                                        <!--<input type="text" class="normalCamp"/>-->
                                        <asp:DropDownList ID="ddlTipoMovimiento" runat="server" CssClass="clsform1">
                                        </asp:DropDownList>
                                    </dd>
                                </dl>
                            </div>
                            <div class="even w100 clearfix dlinline">
                                <dl class="clearfix dt10 dd70">
                                    <dt class="desc">
                                        <label>
                                            Del:</label>
                                    </dt>
                                    <dd class="camp">
                                        <asp:TextBox ID="txtFechaDe" runat="server" CssClass="miniCamp izq"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaDe" CssClass="icon-calendar" runat="server" ImageUrl="../App_Themes/SPE/img/icon-calendar.gif">
                                        </asp:ImageButton>
                                        <label class="izq">
                                            Al:</label>
                                        <asp:TextBox ID="txtFechaA" runat="server" CssClass="miniCamp izq"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnFechaA" runat="server" ImageUrl="../App_Themes/SPE/img/icon-calendar.gif">
                                        </asp:ImageButton>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                                            ControlToValidate="txtFechaDe" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Del' es requerida"
                                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Del' no v�lida"
                                            IsValidEmpty="False" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlExtender="MaskedEditExtenderA"
                                            ControlToValidate="txtFechaA" Display="Dynamic" EmptyValueBlurredText="*" EmptyValueMessage="Fecha 'Al' es requerida"
                                            ErrorMessage="*" InvalidValueBlurredMessage="*" InvalidValueMessage="Fecha 'Al' no v�lida"
                                            IsValidEmpty="False" ValidationGroup="GrupoValidacion"></cc1:MaskedEditValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Rango de fechas no es v�lido"
                                            ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                                            Type="Date" ValidationGroup="GrupoValidacion">*</asp:CompareValidator>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" ValidationGroup="GrupoValidacion">
                        </asp:ValidationSummary>
                        <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                            Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaDe">
                        </cc1:MaskedEditExtender>
                        <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                            Mask="99/99/9999" MaskType="Date" TargetControlID="txtFechaA">
                        </cc1:MaskedEditExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" Format="dd/MM/yyyy"
                            PopupButtonID="ibtnFechaDe" TargetControlID="txtFechaDe">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" Format="dd/MM/yyyy" PopupButtonID="ibtnFechaA"
                            TargetControlID="txtFechaA">
                        </cc1:CalendarExtender>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
                <div class="izq btns4">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btnBuscar" Text="Buscar" ValidationGroup="GrupoValidacion" />
                    <asp:Button ID="btnImprimir" runat="server" CssClass="btnImprimir" Text="Excel" />
                    <asp:Button ID="btnNuevo" runat="server" CssClass="btnCancelar" Text="Nuevo" />
                </div>
            </fieldset>
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h2>
                        <div>
                            <asp:Label ID="lblResultado" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div id="div5" class="divContenedorTitulo">
                            <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label>
                        </div>
                        <h2>
                        </h2>
                        <div>
                            <asp:GridView ID="gvArchivo" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdFtpArchivo" OnPageIndexChanging="gvArchivo_PageIndexChanging"
                                OnRowDataBound="gvArchivo_RowDataBound">
                                <Columns>
                                    <asp:HyperLinkField HeaderText="N� Operaci�n" Text="N� Operaci�n" />
                                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" Visible="False" SortExpression="Fecha" />
                                    <asp:BoundField DataField="Tipo de Operaci�n" HeaderText="Tipo de Operaci�n" SortExpression="Tipo de Operaci�n" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" SortExpression="Descripcion" />
                                    <asp:BoundField DataField="Monto" HeaderText="Monto" SortExpression="Monto">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="cabecera" />
                            </asp:GridView>
                        </div>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                        <h2>
                        </h2>
                    </h2>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
