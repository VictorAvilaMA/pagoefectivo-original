Imports System.Data
Imports System.Collections.Generic

Partial Class ADM_COEmCo

    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CFTPArchivo)

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar       
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvArchivo
        End Get
    End Property

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.CFTPArchivo
    'End Function

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeFTPArchivo As New SPE.Entidades.BEFTPArchivo
       
        obeFTPArchivo.FechaGenerado = CDate(txtFechaDe.Text)
        obeFTPArchivo.FechaHasta = CDate(txtFechaA.Text)


        If ddlTipoMovimiento.SelectedValue.ToString = "" Then
            obeFTPArchivo.IdEstado = 0
        Else
            obeFTPArchivo.IdEstado = ddlTipoMovimiento.SelectedValue
        End If

        Return obeFTPArchivo

    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)

        key = gvArchivo.DataKeys(e.RowIndex).Values(0)

    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADArGe"
        e.MaintenancePageName = "ADArGe.aspx"
        e.ExecuteSearchOnFirstLoad = False

    End Sub

    'Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click

    '    txtFechaDe.Text = Today.ToShortDateString
    '    txtFechaA.Text = Today.ToShortDateString
    '    ddlTipoMovimiento.SelectedIndex = 0

    '    gvArchivo.DataSource = Nothing
    '    gvArchivo.DataBind()
    '    SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
    'End Sub

    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Sub OnAfterSearch()

        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If

    End Sub

    Public Overrides Sub OnFirstLoadPage()

        MyBase.OnFirstLoadPage()
        Page.SetFocus(ddlTipoMovimiento)

        txtFechaDe.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaA.Text = Today.ToShortDateString
        Page.Form.DefaultButton = btnBuscar.UniqueID

        'SERVICIO
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim obeServicio As New SPE.Entidades.BEServicio
        obeServicio.IdUsuarioCreacion = UserInfo.IdUsuario



        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlTipoMovimiento, objCParametro.ConsultarParametroPorCodigoGrupo("ESAG"), _
        "Descripcion", "Id", "::: Todos :::")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID

        End If
        '
    End Sub

    Protected Sub gvArchivo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    End Sub

    Protected Sub gvArchivo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)        '
    End Sub

End Class
