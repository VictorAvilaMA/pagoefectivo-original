Imports System.Collections.Generic
Imports _3Dev.FW.Web
Imports _3Dev.FW.Util

Imports SPE.Entidades
Imports SPE.EmsambladoComun

Partial Class PAS_PRSolPago
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim propServicio As String = ""
        Dim propDatosEnc As String = ""
        Dim propMensajeErr As String = ""
        Dim response As BEGenPasResponse

        Try

            If Not Page.IsPostBack Then

                ' verificar si la solicitud es por GET
                If Page.Request.QueryString.Count = 0 Then Exit Sub

                'verificar si existe servicio
                If Page.Request.QueryString("serv") IsNot Nothing Then
                    propServicio = Request.QueryString("serv").ToString
                    hdnServicio.Value = Request.QueryString("serv").ToString
                Else
                    propMensajeErr = "Falta el nombre del servicio."
                End If

                'verificar si existe datos encriptados
                If Request.QueryString("datosEnc") IsNot Nothing Then
                    propDatosEnc = Request.QueryString("datosEnc").ToString
                    hdnDatosEncr.Value = Request.QueryString("datosEnc").ToString
                Else
                    propMensajeErr += "Falta los datos encriptados."
                End If

                'validar los errores
                If hdnServicio.Value.Length = 0 OrElse hdnDatosEncr.Value.Length = 0 Then
                    Page.Response.Redirect("~/PAS/PasError.aspx?err=" + propMensajeErr)
                    Exit Sub
                End If

                Using oCPasarela As New SPE.Web.CPasarela

                    'proceder a guardar los datos de la solicitud recibida en el LOG
                    Using oCComun As New SPE.Web.CComun()
                        Dim oPasLog As New BELog()

                        oPasLog.IdTipo = ParametrosSistema.Log.Tipo.idTipoPasarelSolicitud
                        oPasLog.Origen = "Registra Solicitud-RecibeParametros"
                        'contiene la URL recepcionada
                        oPasLog.Descripcion = Request.Url.AbsoluteUri
                        'contiene la URL de origen
                        oPasLog.Descripcion2 = Request.UrlReferrer.AbsoluteUri
                        'Request.UserHostAddress

                        oCComun.RegistrarPassLog(oPasLog)

                    End Using

                    'proceder a generar la pasarela de la solicitud
                    response = oCPasarela.GenerarPasarela(hdnServicio.Value.Trim, hdnDatosEncr.Value.Trim)

                    Select Case response.Estado
                        Case "-1"
                            'estado con excepciones

                        Case "0"
                            'estado con validaciones logicas erradas

                        Case "1"
                            'estado con validaciones logicas correctas

                    End Select

                End Using


            End If


        Catch ex As Exception
            Page.Response.Redirect("~/PAS/PasError.aspx?err=" + ex.Message)
        End Try


    End Sub
End Class
