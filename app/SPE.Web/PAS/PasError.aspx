<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="PasError.aspx.vb"
    Inherits="PAS_PasError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <title>Pagina de Errores de Pasarela</title>
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="container">
        <div id="header">
            <h1>
                &nbsp;</h1>
        </div>
        <div id="content">
            <div id="main">
                <div style="text-align: left">
                    <form id="frmGw" runat="server">
                    &nbsp;
                    <table width="768" border="0" cellpadding="8" cellspacing="0" align="center">
                        <tr>
                            <td height="25" valign="top">
                                <span class="ng18">Pasarela de pagos<!--Datos de contacto-->
                                </span>
                            </td>
                        </tr>
                    </table>
                    <table width="768" height="4" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="4" rowspan="2" class="fdregctl">
                            </td>
                            <td width="760" height="1" colspan="2" class="fdgrad">
                            </td>
                            <td rowspan="2" class="fdregctrA" style="width: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td width="595" height="3">
                            </td>
                            <td width="173" class="gris10">
                            </td>
                        </tr>
                    </table>
                    <table width="768" border="0" align="center" cellpadding="0" cellspacing="0" class="bdgrad">
                        <tr>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0">
                                    <tr>
                                        <td height="28" class="pl6">
                                            <span class="cl14"><b>Mensaje de la Aplicación</b></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 17px">
                                            <asp:Label ID="lblMsgErr" runat="server" Text="? - Error"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" class="gris10" style="width: 160px">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                            </td>
                            <td valign="top" class="gris10" style="width: 160px">
                            </td>
                        </tr>
                    </table>
                    <table width="768" height="42" border="0" align="center" cellpadding="0" cellspacing="0"
                        class="fdgrefbdad">
                        <tr>
                            <td align="center" style="height: 50px">
                                <!--	<input type="button" class="boton" value="Continuar" onclick="confirmar(document.forms[0]);">
				  <input type="button" class="boton" value="Cancelar" onclick="irA(document.forms[0].pd.value);">-->
                                <div>
                                    <asp:Button ID="btnEnviar" runat="server" Text="Aceptar" CssClass="boton" OnClick="btnEnviar_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
