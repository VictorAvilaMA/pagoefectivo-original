
Partial Class PAS_PasError
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strErrMsg As String = HttpUtility.UrlDecode(Page.Request("err"))
        Try
            If strErrMsg.Trim.Length > 0 Then lblMsgErr.Text = strErrMsg
        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        Response.Redirect("http://elcomercio.pe")
    End Sub
End Class
