<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="PError.aspx.vb" Inherits="PError" Title="PagoEfectivo - P�gina de Error" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Page1">
        <div id="divInfGeneral" class="mensaje_error" style="margin-left: 50px; text-align: center;
            width: 600px;">
            <h1>
                Ha ocurrido un error en el servidor. Por favor, vuelva a intentar la acci�n. Si
                el error persiste, contactese con el administrador."</h1>
        </div>
    </div>
    <div class="conten_c" style="display: none">
        <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
    </div>
</asp:Content>
