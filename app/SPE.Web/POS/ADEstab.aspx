<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="ADEstab.aspx.vb" Inherits="POS_ADEstab" title="Empresa Contratante" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="Page">
    <asp:ScriptManager id="ScriptManager1" runat="server">
            </asp:ScriptManager>
           
        <h1>
            
            <asp:Label ID="lblProceso" runat="server" Text="Registrar Establecimiento"></asp:Label>
            </h1>
            
      
            
            <asp:Panel ID="PnlEmpresa" runat="server" Cssclass="divContenedor"   >
            
           
		   <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo" >
               1. Informaci�n general</div>
                 
             <fieldset  class="ContenedorEtiquetaTexto stripe">
                <div id="divCodigoEstablecimiento" class="EtiquetaTextoIzquierda">
                   <asp:Label id="lblCodigo" runat="server" Text="C�digo Establecimiento:"   CssClass="EtiquetaLargo"></asp:Label> 
                   <asp:TextBox id="txtCodigoEstablecimiento" runat="server"  CssClass="TextoLargo" MaxLength="100"></asp:TextBox> 
                   <asp:Label id="lblIdEstablecimiento" runat="server" Text=""  CssClass="Hidden"></asp:Label> 
                   <asp:RequiredFieldValidator id="RequiredFieldValidator6" runat="server" ControlToValidate="txtCodigoEstablecimiento" Display="Dynamic" ErrorMessage="Ingrese el Codigo de Establecimiento." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator> 
                </div>
             </fieldset>
             
             <fieldset  class="ContenedorEtiquetaTexto">
                <div id="divNroOrdenPago" class="EtiquetaTextoIzquierda">
                   <asp:Label id="lbl1" runat="server" Text="Raz�n Social:"   CssClass="EtiquetaLargo"></asp:Label> 
                   <asp:TextBox id="txtRazonSocial" runat="server"  CssClass="TextoLargo" MaxLength="100"></asp:TextBox> 
                   <asp:Label id="lblIdEmpresaContrante" runat="server" Text=""  CssClass="Hidden"></asp:Label> 
                   <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ControlToValidate="txtRazonSocial" Display="Dynamic" ErrorMessage="Ingrese la Razon social." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator> 
                </div>
             </fieldset> 
             <fieldset class="ContenedorEtiquetaTexto stripe">
                <div id="div1" class="EtiquetaTextoIzquierda">
                  <asp:Label id="lbl2" runat="server" Text="Ruc:"  CssClass="EtiquetaLargo"></asp:Label> 
                  <asp:TextBox id="txtRuc" runat="server" CssClass="TextoLargo" MaxLength="11"></asp:TextBox> 
                  <asp:RangeValidator ID="rvaRuc" runat="server" ErrorMessage="Ingrese solo n�meros" MinimumValue="0" MaximumValue="100000000000" ControlToValidate="txtRuc" Type="Double">*</asp:RangeValidator>      
                  <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtRuc" Display="Dynamic" ErrorMessage="Ingrese el RUC." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>

                </div>
             </fieldset>
             <fieldset class="ContenedorEtiquetaTexto">
                <div id="div9" class="EtiquetaTextoIzquierda">
                  <asp:Label id="Label4" runat="server" Text="Contacto:"   CssClass="EtiquetaLargo"></asp:Label> 
                  <asp:TextBox id="txtContacto" runat="server" CssClass="TextoLargo" MaxLength="200"></asp:TextBox> 
                </div>
             </fieldset> 
             <fieldset class="ContenedorEtiquetaTexto stripe">
               <div id="div10" class="EtiquetaTextoIzquierda">
                <asp:Label id="Label5" runat="server" Text="Tel�fono:"  CssClass="EtiquetaLargo"></asp:Label> 
                <asp:TextBox id="txtTelefono" runat="server"  CssClass="TextoLargo" MaxLength="15"></asp:TextBox>
               </div>
             </fieldset> 
            <fieldset class="ContenedorEtiquetaTexto">
              <div id="div12" class="EtiquetaTextoIzquierda">
                <asp:Label id="Label7" runat="server" Text="Direcci�n:" CssClass="EtiquetaLargo"></asp:Label> 
                <asp:TextBox id="txtDireccion" runat="server" CssClass="TextoLargo" MaxLength="200"></asp:TextBox> 
              </div>
            </fieldset>
            <fieldset class="ContenedorEtiquetaTexto stripe">
              <div id="div13" class="EtiquetaTextoIzquierda">
                <asp:Label id="Label8" runat="server" Text="Operador:"   CssClass="EtiquetaLargo"></asp:Label> 
                <asp:DropDownList id="ddlOperador" runat="server" CssClass="TextoComboLargo"></asp:DropDownList>
                <asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" ControlToValidate="ddlOperador" ErrorMessage="Debe seleccionar el Operador." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>                
              </div>
            </fieldset>
        <asp:UpdatePanel id="upnlEmpresa"  runat="server" UpdateMode="Conditional">
            <contenttemplate>
            <fieldset class="ContenedorEtiquetaTexto">
              <div id="div15" class="EtiquetaTextoIzquierda">
                <asp:Label id="Label10" runat="server" Text="Pa�s:"  CssClass="EtiquetaLargo"></asp:Label>
                <asp:DropDownList id="ddlPais" runat="server"  CssClass="TextoComboLargo" AutoPostBack="True"></asp:DropDownList> 
              </div>
            </fieldset> 
            <fieldset class="ContenedorEtiquetaTexto stripe">
              <div id="div3" class="EtiquetaTextoIzquierda">
                 <asp:Label id="Label2" runat="server" Text="Departamento:"  CssClass="EtiquetaLargo"></asp:Label> 
                 <asp:DropDownList id="ddlDepartamento" runat="server"  CssClass="TextoComboLargo" AutoPostBack="True"></asp:DropDownList> 
              </div>
            </fieldset> 
            <fieldset class="ContenedorEtiquetaTexto">
              <div id="div16" class="EtiquetaTextoIzquierda">
                <asp:Label id="Label11" runat="server" Text="Ciudad:"  CssClass="EtiquetaLargo"></asp:Label> 
                <asp:DropDownList id="ddlCiudad" runat="server"  CssClass="TextoComboLargo"></asp:DropDownList> 
                <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCiudad" ErrorMessage="Debe seleccionar la ciudad." ValidationGroup="ValidaCampos">*</asp:RequiredFieldValidator>
              </div>
            </fieldset>
            <fieldset class="ContenedorEtiquetaTexto stripe">
              <div id="div4" class="EtiquetaTextoIzquierda">
                <asp:Label id="lblEstado" runat="server" Text="Estado:" Visible="False"   CssClass="EtiquetaLargo"></asp:Label> 
                <asp:DropDownList id="ddlEstado" runat="server" Visible="False" CssClass="TextoCombo"></asp:DropDownList> 
              </div>
            </fieldset> 
            
</contenttemplate>
                </asp:UpdatePanel>
          </asp:Panel>
         
          <asp:Label ID="lblMensaje" runat="server"></asp:Label>
          <asp:ValidationSummary ID="ValidationSummary1" runat="server" /> 
          <fieldset id="fsBotonera" class="ContenedorBotonera" >
            <asp:Button ID="btnRegistrar" runat="server" CssClass="Boton" OnClientClick="return RegistrarEmpresa();" Text="Registrar" ValidationGroup="ValidaCampos" />
            <asp:Button ID="btnActualizar" runat="server" CssClass="Boton" OnClientClick="return RegistrarEmpresa();"  Text="Actualizar" ValidationGroup="ValidaCampos" Visible="False" />
            <asp:Button ID="btnCancelar" runat="server" CssClass="Boton" OnClientClick="return confirm('Esta seguro que desea cancelar este documento? ');"   Text="Cancelar" />
          </fieldset>

                
    </div>

</asp:Content>

