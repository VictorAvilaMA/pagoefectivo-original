Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Partial Class POS_ADEstab
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CEstablecimiento)


    Protected Overrides ReadOnly Property BtnInsert() As System.Web.UI.WebControls.Button
        Get
            Return btnRegistrar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnUpdate() As System.Web.UI.WebControls.Button
        Get
            Return btnActualizar
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Function CreateBEEstablecimiento(ByVal obeEstablecimiento As SPE.Entidades.BEEstablecimiento) As SPE.Entidades.BEEstablecimiento

        obeEstablecimiento = New SPE.Entidades.BEEstablecimiento
        obeEstablecimiento.SerieTerminal = CInt(txtCodigoEstablecimiento.Text).ToString()
        'obeEstablecimiento.RazonSocial = txtRazonSocial.Text
        'obeEstablecimiento.RUC = txtRuc.Text
        'obeEstablecimiento.Contacto = txtContacto.Text
        'obeEstablecimiento.Telefono = txtTelefono.Text
        'obeEstablecimiento.Direccion = txtDireccion.Text
        'obeEstablecimiento.IdOperador = CInt(ddlOperador.SelectedValue)
        'obeEstablecimiento.IdCiudad = CInt(ddlCiudad.SelectedValue)
        'obeEstablecimiento.IdEstado = CInt(ddlEstado.SelectedValue)
        obeEstablecimiento.IdUsuarioCreacion = UserInfo.IdUsuario

        Return obeEstablecimiento

    End Function

    Public Overrides Function CreateBusinessEntityForRegister(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Return CreateBEEstablecimiento(be)

    End Function

    Public Overrides Function CreateBusinessEntityForUpdate(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As _3Dev.FW.Entidades.BusinessEntityBase

        Dim beEstablecimiento As BEEstablecimiento = CreateBEEstablecimiento(be)
        beEstablecimiento.IdEstablecimiento = CInt(lblIdEstablecimiento.Text)
        Return beEstablecimiento

    End Function

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase

    '    Return New SPE.Web.CEstablecimiento

    'End Function

    Public Overrides Sub AssignBusinessEntityValuesInLoadingInfo(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase)

        Dim obeEstablecimiento As SPE.Entidades.BEEstablecimiento = CType(be, SPE.Entidades.BEEstablecimiento)
        txtCodigoEstablecimiento.Text = obeEstablecimiento.SerieTerminal
        'txtRazonSocial.Text = obeEstablecimiento.RazonSocial
        'txtRuc.Text = obeEstablecimiento.RUC
        'txtContacto.Text = obeEstablecimiento.Contacto
        'txtTelefono.Text = obeEstablecimiento.Telefono
        'txtDireccion.Text = obeEstablecimiento.Direccion
        'ddlOperador.SelectedValue = obeEstablecimiento.IdOperador
        'ddlPais.SelectedValue = obeEstablecimiento.IdPais
        'Me.SeleccionarDepartamento(obeEstablecimiento.IdDepartamento)
        'ddlDepartamento.SelectedValue = obeEstablecimiento.IdDepartamento

        'Me.SeleccionarCiudad(obeEstablecimiento.IdCiudad)
        'ddlCiudad.SelectedValue = obeEstablecimiento.IdCiudad
        'ddlEstado.SelectedValue = obeEstablecimiento.IdEstado
        'lblIdEstablecimiento.Text = obeEstablecimiento.IdEstablecimiento

        'OCULTAR CONTROLES
        lblEstado.Visible = True
        ddlEstado.Visible = True
        btnRegistrar.Visible = False
        btnActualizar.Visible = True
        lblProceso.Text = "Actualizar Establecimiento"


    End Sub

    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.MaintenanceKey = "ADEstab"
        e.URLPageCancelForEdit = "COEstab.aspx"
        e.URLPageCancelForInsert = "COEstab.aspx"

    End Sub

    Private Sub CargarCombosUbigeo(ByVal opcionCombo As String)

        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Select Case opcionCombo
            Case "Pais"
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            Case "Departamento"
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(ddlPais.SelectedValue), _
                "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

            Case "Ciudad"
                _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(ddlDepartamento.SelectedValue), _
                "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        End Select
    End Sub
    Sub SeleccionarPais(ByVal IdPais As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        If IdPais > 0 Then
            Me.ddlPais.SelectedValue = IdPais
        End If
    End Sub
    Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlPais.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlPais.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), _
                "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        If IdDepartamento > 0 Then
            Me.ddlDepartamento.SelectedValue = IdDepartamento
        Else
            Me.ddlDepartamento.SelectedIndex = 0
        End If
    End Sub
    Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlDepartamento.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlDepartamento.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), _
                "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

        If IdCiudad > 0 Then
            Me.ddlCiudad.SelectedValue = IdCiudad
        Else
            Me.ddlCiudad.SelectedIndex = 0
        End If
    End Sub
    Private Sub CargarComboEstado()

        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    End Sub

    Private Sub CargaOperadores()

        Dim objCEstablecimiento As New SPE.Web.CEstablecimiento
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlOperador, objCEstablecimiento.ObtenerOperadores(), "Descripcion", "IdOperador", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)

    End Sub

    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPais.SelectedIndexChanged
        SeleccionarDepartamento(0)
        SeleccionarCiudad(0)
        ScriptManager1.SetFocus(ddlPais)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Page.SetFocus(txtRazonSocial)
            SeleccionarPais(0)
            SeleccionarDepartamento(0)
            SeleccionarCiudad(0)
            CargarComboEstado()
            CargaOperadores()
            ddlEstado.SelectedValue = SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Activo

        End If

    End Sub

    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartamento.SelectedIndexChanged
        SeleccionarCiudad(0)

    End Sub

    
    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Public Overrides Sub OnAfterInsert()
        ControlesMantenimiento(False)
    End Sub

    Public Overrides Sub OnAfterUpdate()
        ControlesMantenimiento(False)
    End Sub

    Private Sub ControlesMantenimiento(ByVal Habilitar As Boolean)
        OcultarControles(Habilitar)
        btnActualizar.Enabled = Habilitar
        btnRegistrar.Enabled = Habilitar

    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        Page.Title = "PagoEfectivo - " + lblProceso.Text
    End Sub

  

    Private Sub OcultarControles(ByVal flag As Boolean)
        txtCodigoEstablecimiento.Enabled = flag
        txtRazonSocial.Enabled = flag
        txtRuc.Enabled = flag
        txtContacto.Enabled = flag
        txtTelefono.Enabled = flag
        txtDireccion.Enabled = flag

        ddlPais.Enabled = flag
        ddlDepartamento.Enabled = flag
        ddlCiudad.Enabled = flag
        ddlEstado.Enabled = flag
        ddlOperador.Enabled = flag
       
    End Sub

End Class
