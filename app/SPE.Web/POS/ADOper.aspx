<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="ADOper.aspx.vb" Inherits="ADM_ADOper" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager id="ScriptManager" runat="server">
</asp:ScriptManager>
  <asp:UpdatePanel id="UpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
          <asp:Panel id="pnlPage" runat="server">
            <h1>
              <asp:Label id="lblProceso" runat="server" Text="Registrar Operador"></asp:Label> 
            </h1>
            <div  id="divInfGeneral" class="divContenedor">
              <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo">1. Informaci�n general</div>
                 <fieldset class="ContenedorEtiquetaTexto stripe">
                    <div id="div6" class="EtiquetaTextoIzquierda">
                       <asp:Label id="lblCodigo" runat="server" CssClass="EtiquetaLargo" Text="C�digo:"></asp:Label> 
                       <asp:Label id="lblIdOperador" runat="server" CssClass="Hidden" Text=""></asp:Label> 
                       <asp:TextBox id="txtCodigo" runat="server" CssClass="Texto"  MaxLength="11"></asp:TextBox> 
                       <asp:RangeValidator id="rvaCodigo" runat="server" ValidationGroup="IsNullOrEmpty" ErrorMessage="Ingrese solo n�meros" ControlToValidate="txtCodigo" MaximumValue="100000000000" MinimumValue="0" Type="Double">* </asp:RangeValidator> 
                       <asp:RequiredFieldValidator id="rfvCodigo" runat="server" ValidationGroup="IsNullOrEmpty" ErrorMessage="Ingrese el Codigo" ControlToValidate="txtCodigo" ToolTip="Dato necesario">*</asp:RequiredFieldValidator> 
                    </div>
                </fieldset>     
                          
                <fieldset class="ContenedorEtiquetaTexto">
                  <div id="divNroOrdenPago" class="EtiquetaTextoIzquierda">
                       <asp:Label id="lblDescripcion" runat="server" CssClass="EtiquetaLargo" Text="Descripcion:"></asp:Label> 
                       <asp:TextBox id="txtDescripcion" runat="server" CssClass="Texto" Width="250px" MaxLength="100"></asp:TextBox> 
                       <asp:RequiredFieldValidator id="rfvDescripcion" runat="server" ValidationGroup="IsNullOrEmpty" ErrorMessage="Ingrese la Descripcion" ControlToValidate="txtDescripcion" ToolTip="Dato necesario">*</asp:RequiredFieldValidator> 
                  </div>
                </fieldset> 
                
                <fieldset id="fisEstado" class="ContenedorEtiquetaTexto stripe" runat="server">
                  <div id="Div2" class="EtiquetaTextoIzquierda">
                       <asp:Label id="lblEstado" runat="server" CssClass="EtiquetaLargo" Text="Estado:" Visible = "False"></asp:Label> 
                       <asp:DropDownList id="ddlEstado" runat="server" CssClass="TextoComboLargo" Visible = "False"></asp:DropDownList> 
                  </div>
               </fieldset> 
               
               <fieldset  id="Fieldset2" class="ContenedorEtiquetaTexto" runat="server">
                       <asp:ValidationSummary id="vsmMessage" runat="server" ValidationGroup="IsNullOrEmpty"></asp:ValidationSummary> 
               </fieldset> 
          </div>
        </asp:Panel> 
     </contenttemplate>
     <triggers>
        <asp:AsyncPostBackTrigger ControlID="btnRegistrar" ></asp:AsyncPostBackTrigger>
        <asp:AsyncPostBackTrigger ControlID="btnActualizar"></asp:AsyncPostBackTrigger>
     </triggers>
  </asp:UpdatePanel>
    
  <asp:UpdatePanel id="UpdatePanelMensaje" runat="server" UpdateMode="Conditional">
    <contenttemplate>
       <asp:Label id="lblTransaccion" runat="server" CssClass="MensajeTransaccion"></asp:Label> 
        <fieldset style="WIDTH: 500px" id="fsBotonera" class="ContenedorBotonera">
          <asp:Button id="btnRegistrar" runat="server" CssClass="Boton" Text="Registrar" OnClientClick="return ConfirmMe();" OnClick="btnRegistrar_Click"></asp:Button> 
          <asp:Button id="btnActualizar" runat="server" CssClass="Boton" Text="Actualizar" OnClientClick="return ConfirmMe();" OnClick="btnActualizar_Click"> </asp:Button> 
          <asp:Button id="btnCancelar" runat="server" CssClass="Boton" Text="Cancelar" OnClientClick="return confirm('�Est� Ud. seguro que desea cancelar la operaci�n? ');" OnClick="btnCancelar_Click"></asp:Button> 
        </fieldset> 
   </contenttemplate>
   <triggers>
         <asp:PostBackTrigger ControlID="btnCancelar"></asp:PostBackTrigger>
   </triggers>
  </asp:UpdatePanel>
</asp:Content>

