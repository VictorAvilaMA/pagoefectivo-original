<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COEstab.aspx.vb" Inherits="POS_COEstab" Title="PagoEfectivo - Consultar Empresa Contratante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Page">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <div style="float: left;">
                <h1>
                    Consultar Establecimiento</h1>
            </div>
            <div style="float: right;">
                <asp:Button ID="btnNuevo" runat="server" CssClass="BotonFI" Text="Nuevo" />
            </div>
        </div>
        <div id="divHistorialOrdenesPago" class="divContenedor">
            <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo">
                Criterios de B�squeda</div>
            <div id="divCriteriosBusqueda">
                <div id="divCriterios" class="divSubContenedorCriteriosBusqueda">
                    <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="div5" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblCodigo" runat="server" CssClass="EtiquetaLargo" Text="Codigo Establecimiento:"></asp:Label>
                                    <asp:TextBox ID="txtCodigoEstablecimiento" runat="server" CssClass="TextoLargo" Width="150px"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto">
                                <div id="divNroOrdenPago" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lbl1" runat="server" CssClass="EtiquetaLargo" Text="Raz�n Social:"></asp:Label>
                                    <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="TextoLargo" Width="150px"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="Div1" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lbl2" runat="server" CssClass="EtiquetaLargo" Text="Ruc:"></asp:Label>
                                    <asp:TextBox ID="txtRUC" runat="server" CssClass="Texto" Width="150px"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto">
                                <div id="Div2" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="Label1" runat="server" CssClass="EtiquetaLargo" Text="Contacto:"></asp:Label>
                                    <asp:TextBox ID="txtContacto" runat="server" CssClass="TextoLargo" Width="150px"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="Div6" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblOperador" runat="server" CssClass="EtiquetaLargo" Text="Operador:"></asp:Label>
                                    <asp:DropDownList ID="ddlOperador" runat="server" CssClass="TextoCombo" Width="150px">
                                    </asp:DropDownList>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto">
                                <div id="Div4" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="Label2" runat="server" CssClass="EtiquetaLargo" Text="Estado:"></asp:Label>
                                    <asp:DropDownList ID="ddlIdEstado" runat="server" CssClass="TextoCombo" Width="150px">
                                    </asp:DropDownList>
                                </div>
                            </fieldset>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <fieldset id="fsBotonera" class="ContenedorBotoneraCriteriosBusqueda">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="Boton" Text="Buscar" />
                    <br />
                    <asp:Button ID="btnLimpiar" runat="server" CssClass="Boton" Text="Limpiar" />
                </fieldset>
            </div>
        </div>
        <div id="div3" class="divContenedor">
            <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div7" class="divContenedorTitulo">
                        <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label>
                    </div>
                    <div id="divOrdenesPago" class="divSubContenedorGrilla">
                        <asp:GridView ID="gvEstablecimiento" runat="server" CssClass="grilla" OnRowDataBound="gvEstablecimiento_RowDataBound"
                            OnPageIndexChanging="gvEstablecimiento_PageIndexChanging" AutoGenerateColumns="False"
                            AllowPaging="True" DataKeyNames="IdEstablecimiento" AllowSorting="true">
                            <Columns>
                                <asp:BoundField DataField="SerieTerminal" SortExpression="SerieTerminal" HeaderText="Cod.Establecimiento">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RazonSocial" SortExpression="RazonSocial" HeaderText="Raz&#243;n Social" />
                                <asp:BoundField DataField="Contacto" SortExpression="Contacto" HeaderText="Contacto" />
                                <asp:BoundField DataField="Ruc" SortExpression="Ruc" HeaderText="Ruc">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Telefono" SortExpression="Telefono" HeaderText="Tel&#233;fono">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                    HeaderText="Estado">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgActualizar" PostBackUrl="ADEstab.aspx" CommandName="Edit"
                                            runat="server" ImageUrl="~/images/lupa.gif" ToolTip="Actualizar" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdEstablecimiento" HeaderText="IEstablecimiento" Visible="false" />
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            &nbsp;
        </div>
    </div>
</asp:Content>
