<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COOper.aspx.vb" Inherits="ADM_COOper" Title="PagoEfectivo - Consultar Operador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div id="Page">
        <div>
            <div style="float: left;">
                <h1>
                    Consultar Operador</h1>
            </div>
            <div style="float: right;">
                <asp:Button ID="btnNuevo" runat="server" CssClass="BotonFI" Text="Nuevo" />
            </div>
        </div>
        <div id="divHistorialOperadores" class="divContenedor">
            <div id="divHistorialOrdenesPagoTitulo" class="divContenedorTitulo">
                Criterios de B�squeda</div>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divCriteriosBusqueda">
                        <div id="divCriterios" class="divSubContenedorCriteriosBusqueda">
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="div1" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblCodigo" runat="server" CssClass="EtiquetaLargo" Text="C�digo"> </asp:Label>
                                    <asp:TextBox ID="txtCodigo" runat="server" CssClass="Texto" MaxLength="100"> </asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto">
                                <div id="divNroOrdenPago" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lbl1" runat="server" CssClass="EtiquetaLargo" Text="Nombre "> </asp:Label>
                                    <asp:TextBox ID="txtOperador" runat="server" CssClass="TextoLargo" MaxLength="100"></asp:TextBox>
                                </div>
                            </fieldset>
                            <fieldset class="ContenedorEtiquetaTexto stripe">
                                <div id="div2" class="EtiquetaTextoIzquierda">
                                    <asp:Label ID="lblEstado" runat="server" CssClass="EtiquetaLargo" Text="Estado:"></asp:Label>
                                    <asp:DropDownList ID="dropEstado" runat="server" CssClass="TextoCombo" Width="150px">
                                    </asp:DropDownList>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <fieldset id="fsBotonera" class="ContenedorBotoneraCriteriosBusqueda">
                <asp:Button ID="btnBuscar" runat="server" CssClass="Boton" Text="Buscar" />
                <br />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="Boton" Text="Limpiar" />
            </fieldset>
        </div>
        <div id="div3" class="divContenedor">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="div4" class="divContenedorTitulo">
                        <asp:Label ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Label>
                    </div>
                    <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                    <div id="divOrdenesPago" class="divSubContenedorGrilla">
                        <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" AllowPaging="True"
                            DataKeyNames="IdOperador" AutoGenerateColumns="False" AllowSorting="true">
                            <Columns>
                                <asp:BoundField DataField="Codigo" SortExpression="Codigo" HeaderText="C�digo"></asp:BoundField>
                                <asp:BoundField DataField="Descripcion" SortExpression="Descripcion" HeaderText="Descripci�n">
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" SortExpression="DescripcionEstado"
                                    HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnedit" CommandName="Edit" PostBackUrl="ADOper.aspx" runat="server"
                                            ToolTip="Actualizar" ImageUrl="~/images/lupa.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                            <EmptyDataTemplate>
                                No se encontraron registros</EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
