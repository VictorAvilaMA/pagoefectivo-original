Imports System.Data
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class ADM_COOper
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.COperador)


    'Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
    '    MyBase.OnInitComplete(e)
    '    CType(Master, MasterPagePrincipal).AsignarRoles(New String() {SPE.EmsambladoComun.ParametrosSistema.RolAdministrador})
    'End Sub

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnNew() As System.Web.UI.WebControls.Button
        Get
            Return btnNuevo
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    'Public Overrides Function GetControllerObject() As _3Dev.FW.Web.ControllerMaintenanceBase
    '    Return New SPE.Web.COperador
    'End Function

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase

        Dim obeOperador As New SPE.Entidades.BEOperador
        obeOperador.Codigo = txtCodigo.Text
        obeOperador.Descripcion = txtOperador.Text

        If dropEstado.SelectedValue = "" Then
            obeOperador.IdEstado = 0
        Else
            obeOperador.IdEstado = dropEstado.SelectedValue
        End If

        Return obeOperador

    End Function


    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)

        e.MaintenanceKey = "ADOper"
        e.MaintenancePageName = "ADOper.aspx"
        e.ExecuteSearchOnFirstLoad = False

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '
        If Page.IsPostBack = False Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(txtCodigo)
            CargarComboEstado()
        End If
        '
    End Sub

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)

        key = gvResultado.DataKeys(e.RowIndex).Values(0)

    End Sub
    Private Sub CargarComboEstado()
        Dim objCParametro As New SPE.Web.CAdministrarParametro()
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(dropEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESMA"), _
        "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
    End Sub
    Public Overrides Sub OnClear()
        MyBase.OnClear()
        txtOperador.Text = ""
        txtCodigo.Text = ""

    End Sub



End Class
