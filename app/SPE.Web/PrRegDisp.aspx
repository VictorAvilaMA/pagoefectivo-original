﻿<%@ Page Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false" CodeFile="PrRegDisp.aspx.vb"
    Async="true" Inherits="PrRegDisp" Title="Registrar Dispositivo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/base.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/css/screen.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" charset="utf-8" media="screen"
        type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="Page" class="conten_pasos3">
        <div style="margin: 170px 75px 0px 75px;">
            <div id="div3" style="border: 1px solid #DDD;">
                <div style="margin: 10px 10px 0px; font-size: 2em; font-weight: bold; font-family: Helvetica;
                    color: #333;">
                    Nombre de Nuevo Dispositivo</div>
                <div style="padding: 10px 1%;">
                    <hr width="100%" />
                </div>
                <h3 style="margin: 10px 10px; font-weight: bold;">
                    Este dispositivo aún no ha sido registrado. Por niveles de seguridad, favor de registrarlo.<br />
                    Una vez realizado este proceso, se le notificar&aacute; los datos ingresados con
                    el envío de un e-mail.</h3>
                <div class="w100 clearfix dlinline">
                    <dl class="clearfix dt10 dd30">
                        <dt class="desc" style="margin-top: 15px;">
                            <label title="Nombre de Dispositivo.">
                                <asp:Literal ID="Label4" runat="server" Text="Dispositivo:">
                                </asp:Literal></label>
                        </dt>
                        <dd class="camp">
                            <asp:TextBox ID="txtNombre" runat="server" class="normalCamp mright10" ValidationGroup="ValidaControles"
                                MaxLength="50">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtNombre"
                                ValidationGroup="ValidaControles" ErrorMessage="Ingrese un nombre para el equipo"
                                Text="*">
                            </asp:RequiredFieldValidator>
                        </dd>
                    </dl>
                </div>
                <asp:Label ID="lblmensaje" runat="server" Style="margin: 0px 10px; font-weight: bold;
                    color: Navy;" Visible="false"></asp:Label>
            </div>
            <div id="div1" style="border: 1px solid #DDD;">
                <div class="w100 clearfix dlinline">
                    <dl class="even clearfix dt15 dd60">
                        <dd class="camp">
                            <asp:Button ID="btnOperacion" runat="server" CssClass="btnRegistrar" ValidationGroup="ValidaControles"
                                Text="Registrar" />
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="sep_a14">
            </div>
        </div>
        <div class="conten_c" style="display: none">
            <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
            <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
            <uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />
        </div>
    </div>
</asp:Content>
