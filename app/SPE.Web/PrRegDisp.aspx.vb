﻿Imports SPE.Web.Seguridad
Imports SPE.Entidades
Imports System.Collections.Generic

Partial Class PrRegDisp
    Inherits System.Web.UI.Page

    Protected Sub RegistrarPc_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            CType(Master, MPBase).MarcarMenu("liinicio")
            If (_3Dev.FW.Util.DataUtil.ObjectToString(Session("LoginErrorDetails")) <> "") Then
                CType(Master.FindControl("Login1").FindControl("TextoValidacion"), Literal).Text = HttpContext.Current.Session("LoginErrorDetails")
                HttpContext.Current.Session.Remove("LoginErrorDetails")
            End If
        End If
        CType(Master.FindControl("ValidarLinks"), HiddenField).Value = "Eliminar Links"
        Master.FindControl("Form1").FindControl("dvlnkregistroingreso").Visible = False
        lblmensaje.Visible = False

    End Sub

    Protected Sub btnOperacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOperacion.Click
        Using oCComun As New SPE.Web.CComun()
            Dim oBEEquipoRegistradoXusuario As New BEEquipoRegistradoXusuario()
            Dim oBEEU As New BEEquipoRegistradoXusuario()
            oBEEU = CType(Session("oBEEquipoRegistradoXusuarioVal"), BEEquipoRegistradoXusuario)

            If txtNombre.Text.Trim() <> "" Then
                If Not oBEEU Is Nothing Then
                    oBEEquipoRegistradoXusuario.Token = oBEEU.Token
                    oBEEquipoRegistradoXusuario.PcName = txtNombre.Text.ToString()
                    oBEEquipoRegistradoXusuario.Usuario = oBEEU.Usuario
                    oCComun.RegistrarEquipoRegistradoXusuario(oBEEquipoRegistradoXusuario)
                End If
                Response.Redirect("Principal.aspx")
            Else
                lblmensaje.Visible = True
                lblmensaje.Text = "Es necesario ingresar un nombre de Dispositivo."
            End If
        End Using
    End Sub
End Class
