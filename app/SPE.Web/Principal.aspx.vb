Imports SPE.Entidades
Imports SPE.EmsambladoComun.ParametrosSistema
Imports SPE.Web.Seguridad
Imports _3Dev.FW.Entidades.Seguridad

Partial Class Principal
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CustomRoleProviderClient As New SPERoleProvider()
        If HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) IsNot Nothing Then
            If Not Context.User Is Nothing Then
                If Context.User.IsInRole(RolAdministrador) Then
                    Response.Redirect("~/ADM/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolCajero) Then
                    Response.Redirect("~/ARE/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolSupervisor) Then
                    Response.Redirect("~/ARE/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolCliente) Then
                    If Not HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo) Is Nothing Then
                        If CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).TieneInfoCompleta Then
                            Response.Redirect("~/CLI/PgPrl.aspx")
                        Else
                            Response.Redirect("CLI/ADAcCl.aspx")
                        End If
                    End If
                ElseIf Context.User.IsInRole(RolRepresentante) Then
                    Response.Redirect("~/ECO/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolJefeProducto) Then
                    Response.Redirect("~/JPR/PgPrl.aspx")
                ElseIf Context.User.IsInRole(RolTesoreria) Then
                    Response.Redirect("~/TES/CoTrPr.aspx")
                ElseIf Context.User.IsInRole(RolAdminMonederoElectronico) Then
                    Response.Redirect("~/DV/COCuentaAdmin.aspx")
                ElseIf (CustomRoleProviderClient.GetRolesForUser(Context.User.Identity.Name).Length > 0) Then
                    For Each item As BERolPagina In CType(HttpContext.Current.Session(_3Dev.FW.EmsambladoComun.SystemParameters.Security.UserInfo), _3Dev.FW.Entidades.Seguridad.BEUserInfo).PaginasAcceso
                        If Not String.IsNullOrEmpty(item.Url) Then
                            If Not item.Url.Contains("Principal.aspx") And Not item.Url.Contains("Login.aspx") And Not item.Url.Contains("Default.aspx") Then
                                Response.Redirect(item.Url)
                            End If
                        End If
                    Next
                    Response.Redirect("~/Login.aspx")
                Else
                    Response.Redirect("~/SEG/sinautrz.aspx")
                End If
                ''Comen
            End If
        End If
    End Sub

End Class
