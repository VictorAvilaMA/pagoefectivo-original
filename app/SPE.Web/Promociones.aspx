﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="Promociones.aspx.vb" Inherits="Promociones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ulMenu li').removeClass('active');
            $('#liPromociones').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content">
        
        <asp:Panel ID="pnlProximosAfiliados" runat="server">
            
            
            
            <!--#include file="Promociones/ListaPromociones.txt"-->
              <script type="text/javascript">
                $(".promotion-section a").colorbox({
                    width: "850",
                    height: "600",
                    top: "20",
                    fixed: true,
                    href:$(this).attr('href')
                });

                $("#cboxWrapper").find('a').attr("target", "blank_");
              </script>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
</asp:Content>
