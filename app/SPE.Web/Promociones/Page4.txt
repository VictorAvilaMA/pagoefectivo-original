<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="ctl00_Head1"><meta charset="utf-8" /><title>
	Paga con PagoEfectivo en Golazobet
</title><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="https://pagoefectivo.pe/App_Themes/SPE/js/html5.js"></script>
<![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" /><meta name="description" /><meta name="keywords" /><meta name="author" content="perucomsultores.pe" /><meta name="viewport" content="width=device-width" /><meta name="language" content="es" /><meta name="geolocation" content="Peru" /><meta name="robots" content="index,follow" /><meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" /><meta property="og:description" /><meta property="og:image" content="images/pago-efectivo.png" />
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="description" content=" " />
    <meta name="keywords" content=" " />
<link rel="shortcut icon" href="favicon.ico" />
<style> 
table {border-spacing: 0;}
img {display:block;}
.texto {color:#FFF; font-family:Arial, Helvetica, sans-serif; font-size:16px; padding-left:15px; padding-right:15px;}
</style>
</head>
<body>
<table style="padding: 0px !important; margin: 0px auto !important; border: none; border-collapse: collapse !important; width: 720px;" border="0" cellspacing="0" cellpadding="0"  align="center">
<tbody>
<tr>
<td style="background: transparent; height: 244px; padding: 0; margin: 0px; border: none;"><img style="margin: 0px; padding: 0px; border: 0px;" usemap="#header-link" src="http://mailing.e3.pe/Pagoefectivo/NL/Golazobet/mailing-img-01.jpg" alt="" height="244" /> 
<map name="header-link">
<area shape="rect" coords="36,54,695,149" href="https://pagoefectivo.pe/" target="_blank" /> 
</map>
</td>
</tr>
<tr>
<td style="background: transparent; height: 357px; padding: 0; margin: 0px; border: none;"><img style="margin: 0px; padding: 0px; border: 0px;" usemap="#bet-link" src="http://mailing.e3.pe/Pagoefectivo/NL/Golazobet/mailing-img-02.jpg" alt="" height="357" /> 
<map name="bet-link">
<area shape="rect" coords="512,309,599,340" href="https://www.golazobet.com" target="_blank" /> 
</map>
</td>
</tr>
<tr>
<td style="background: transparent; height: 282px; padding: 0; margin: 0px; border: none;"><img style="margin: 0px; padding: 0px; border: 0px;" usemap="#contact-link" src="http://mailing.e3.pe/Pagoefectivo/NL/Golazobet/mailing-img-03.jpg" alt="" height="282" /> 
<map name="contact-link">
<area shape="rect" coords="81,26,354,51" href="https://www.golazobet.com/?Lang=es-PE#!information/termsandconditions" target="_blank" />
<area shape="rect" coords="363,26,636,51" href="https://www.facebook.com/Golazobet" target="_blank" /> 
</map>
</td>
</tr>
<tr>
<td style="height: 71px; padding: 0px; margin: 0px; border: none; text-align: center; color: #000000;"><span style="font-size: xx-small;"><span style="line-height: 12px; color: #666666;">Para asegurar la entrega de nuestros e-mail en su correo, agregue&nbsp;</span><span style="line-height: 12px; color: #00a0c6;">newsletter@nl.pagoefectivo.pe&nbsp;</span><span style="line-height: 12px; color: #666666;">a su libreta de direcciones de correo.</span></span></td>
</tr>
</tbody>
</table>
</body>
</html>