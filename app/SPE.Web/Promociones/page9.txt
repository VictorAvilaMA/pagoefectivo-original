<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="ctl00_Head1"><meta charset="utf-8" /><title>
	Paga con PagoEfectivo en PokerStar
</title><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--[if lt IE 9]>
  <script type="text/javascript" src="https://pagoefectivo.pe/App_Themes/SPE/js/html5.js"></script>
<![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" /><meta name="description" /><meta name="keywords" /><meta name="author" content="perucomsultores.pe" /><meta name="viewport" content="width=device-width" /><meta name="language" content="es" /><meta name="geolocation" content="Peru" /><meta name="robots" content="index,follow" /><meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" /><meta property="og:description" /><meta property="og:image" content="images/pago-efectivo.png" />
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="cs" />
    <meta name="description" content=" " />
    <meta name="keywords" content=" " />
<link rel="shortcut icon" href="favicon.ico" />
<style> 
table {border-spacing: 0;}
img {display:block;}
.texto {color:#FFF; font-family:Arial, Helvetica, sans-serif; font-size:16px; padding-left:15px; padding-right:15px;}
</style>
</head>
<body>
<table width="640" border="0" align="center" cellpadding="0" cellspacing="0" border-spacing="0">
  <tr>
    <td style="line-height:0" ><img src="http://mailing.e3.pe/Pagoefectivo/NL/PokerStar/Branding15mayo/01.jpg" width="640" height="27" style="display:block!important; font-size:2px float: left" align="absbottom"/></td>
  </tr>
  <tr>
    <td style="line-height:0" ><a href="https://pagoefectivo.pe/?utm_source=Boletin-PagoEfectivo-15052013&utm_medium=NL&utm_campaign=Header-Logo" target="_blank"><img src="http://mailing.e3.pe/Pagoefectivo/NL/PokerStar/Branding15mayo/02.jpg" width="640" height="95" border="0" align="absbottom" style="display:block!important; font-size:2px float: left"/></a></td>
  </tr>

  <tr>
    <td style="line-height:0" ><a href="http://www.pokerstars.com/espanol/?utm_source=Boletin-PokerStar-15052013&utm_medium=NL&utm_campaign=Contenido-PokerStar" target="_blank"><img src="http://mailing.e3.pe/Pagoefectivo/NL/PokerStar/Branding15mayo/03.jpg" width="640" height="519" border="0" align="absbottom" style="display:block!important; font-size:2px float: left"/></a></td>
  </tr>
    <tr>
    <td colspan="3" bgcolor="#000000" height="93" class="texto" align="center">PagoEfectivo es exclusivo para jugadores de Perú y te permitirá recargar tu cuenta de manera fácil, rápida y segura. <br />Sólo elige PagoEfectivo como opción de pago
</td>
  </tr>
  <tr>
    <td style="line-height:0" ><a href="https://pagoefectivo.pe/CentrosPago.aspx?utm_source=Boletin-PagoEfectivo-CentrosdePago-15052014&utm_medium=NL&utm_campaign=Footer-Bancos" target="_blank"><img src="http://mailing.e3.pe/Pagoefectivo/NL/PokerStar/Branding15mayo/04.jpg" width="640" height="141" border="0" align="absbottom" style="display:block!important; font-size:2px float: left"/></a></td>
  </tr>
  <tr>
    <td style="padding:10px; text-align:center; font-family:Verdana, Geneva, sans-serif; font-size:10px;">PagoEfectivo es la nueva forma de pagar en Internet con efectivo.<br />
    Ingresa a <a href="https://pagoefectivo.pe/?utm_source=NL-PagoEfectivo-21032013&amp;utm_medium=NL&amp;utm_campaign=FooterUrlPagoEfectivo" target="_blank">www.pagoefectivo.pe</a> y entérate de más ventajas de comprar sin tarjeta.</td>
  </tr>
</table>
</body>
</html>
