<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="RecuperarPassword.aspx.vb" Inherits="RecuperarPassword" Title="PagoEfectivo - Recuperar Contrase�a" %>

<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/gridandforms.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    
    <%--	<script type="text/javascript" src="jscripts/validate.js"></script>--%>
    
    
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/JScripts/accordian.pack.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <!--[if IE 6]>
	    <script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	    <script type="text/javascript">
	    (function($) {
		    $(document).ready(function(){
			    try {	
				    DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			    }catch(e){}});
		    })(jQuery);	
	    </script>
	    <![endif]-->
    <script type="text/javascript">

        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3

                });
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div class="content-side-left">
            <div class="registrocont_pe">
                <h2>
                    Recuperar Contrase&ntilde;a</h2>
                <div class="content_form">
                    <%--                             	<form id="frm_recover_pass" class="dlgrid" >                           --%>
                    <div class="dlinline">
                        <div class="w100 ">
                            <dl class="dt10 dd60 clearfix no_padding">
                                <dt>
                                    <label for="txtRecoverpass">
                                        E-mail
                                    </label>
                                </dt>
                                <dd>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="text block fleft "></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="El email es mandatorio."
                                        Text="*" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Formato de e-mail no v�lido"
                                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                </dd>
                            </dl>
                        </div>
                        <div class="mensaje_error" style="float: left; width: 500px;">
                            <asp:Label ID="lblMensaje" runat="server" Style="color: blue;"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="color: Red;" />
                        </div>
                        <div class="botones_recupera">
                            <asp:Button ID="btnRecuperar" runat="server" Text="" CssClass="recuperar_pe png_bg" />
                            <asp:Button ID="btnIrALogin" runat="server" Text="" CssClass="regresar_pe png_bg"
                                CausesValidation="false" PostBackUrl="~/default.aspx" />
                            <%--  <input name="" type="reset" value=""  class="regresar_pe"/>--%>
                        </div>
                    </div>
                    <%--	</form>--%>
                </div>
            </div>
        </div>
        <div class="sidebar-rigth">
            <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
            <%--<uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />--%>
            <div id="divCentrosPagoAutorizados" runat="server">
        </div>
        </div>
    <!-- fin body-->
</asp:Content>
