Imports SPE.Entidades

Partial Class RecuperarPassword
    Inherits System.Web.UI.Page

    Protected Sub btnRecuperar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecuperar.Click
        Try
            Dim CCliente As New SPE.Web.CComun
            Dim obeUsuario As New BEUsuarioBase
            obeUsuario.Email = Me.txtEmail.Text.Trim
            obeUsuario = CCliente.RegistrarTokenCambioCorreo(obeUsuario)
            Me.lblMensaje.Text = obeUsuario.DescripcionEstado

        Catch ex As Exception
            btnRecuperar.Enabled = True
            lblMensaje.Text = SPE.EmsambladoComun.ParametrosSistema.MensajeDeErrorCliente
            lblMensaje.CssClass = "MensajeValidacion"
            '_3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try


    End Sub
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            Page.SetFocus(txtEmail)
        End If
        If HttpContext.Current.Cache("CentrosPagoAutorizados") Is Nothing Then
            Using reader As New System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/CentrosPagoAutorizados.html"))
                Dim htmlCentrosPagoAutorizados As String = reader.ReadToEnd()
                htmlCentrosPagoAutorizados = Replace(htmlCentrosPagoAutorizados, "[urlBase]", ConfigurationManager.AppSettings("DominioFile"))
                HttpContext.Current.Cache("CentrosPagoAutorizados") = htmlCentrosPagoAutorizados
                reader.Close()
            End Using
        End If
        divCentrosPagoAutorizados.InnerHtml = HttpContext.Current.Cache("CentrosPagoAutorizados")
    End Sub


End Class
