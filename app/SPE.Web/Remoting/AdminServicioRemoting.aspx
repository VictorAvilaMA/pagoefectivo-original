<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="AdminServicioRemoting.aspx.vb"
    Inherits="Remoting_AdminServicioRemoting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <title>Untitled Page</title>
</head>
<body>
    <script src="https://sb.scorecardresearch.com/c2/6906602/cs.js#sitio_id=235360&path=/online/otros/"></script>
    <form id="form1" runat="server">
    <div class="TituloPagina" style="color: #006699; text-align: center">
        &nbsp;Parámetros de Conexión de .NET Remoting<br />
        <br />
        <table border="2" align="center" bordercolor="#006699" style="width: 465px">
            <tr>
                <td style="width: 117px">
                    <asp:Label ID="Label7" runat="server" Height="17px" Text="Servidor Remoto" Width="119px"
                        CssClass="txtAzul"></asp:Label>
                </td>
                <td style="width: 636px">
                    <asp:Label ID="Label5" runat="server" Width="64px"></asp:Label>
                </td>
                <td style="width: 544px">
                    <asp:Label ID="Label8" runat="server" Text="Puerto de Conexión" CssClass="txtAzul"></asp:Label>
                </td>
                <td style="width: 316px">
                    <asp:Label ID="Label9" runat="server" CssClass="Style.css"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 117px; height: 40px;">
                    <asp:Label ID="Label6" runat="server" Text="Canal" CssClass="txtAzul"></asp:Label>
                </td>
                <td style="width: 636px; height: 40px;">
                    <asp:Label ID="Label4" runat="server" Width="65px"></asp:Label>
                </td>
                <td style="width: 544px; height: 40px;">
                    <asp:Label ID="Label10" runat="server" Height="22px" Text="Tiempo Espera a una peticion del Cliente (ms)"
                        Width="148px" CssClass="txtAzul"></asp:Label>
                </td>
                <td style="width: 316px; height: 40px;">
                    <asp:Label ID="Label11" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    &nbsp; &nbsp;&nbsp;
    <hr width="51%" />
    Administración del Servicio .NET Remoting<br />
    <br />
    <table style="width: 368px" align="center" border="2" bordercolor="#006699">
        <tr>
            <td style="width: 87px">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Estado Conexion"
                    Width="119px" CssClass="txtAzul" BackColor="#C2E2F7" ForeColor="Black" />
            </td>
            <td colspan="2" style="width: 242px">
                <asp:Label ID="Label2" runat="server" Width="237px" Height="18px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 87px">
                <asp:Button ID="Button2" runat="server" Text="Conectarse" Width="119px" OnClick="Button2_Click"
                    CssClass="txtAzul" BackColor="#C2E2F7" ForeColor="Black" />
            </td>
            <td colspan="2" style="width: 242px">
                <asp:Label ID="Label12" runat="server" Height="18px" Width="237px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                &nbsp;
            </td>
        </tr>
    </table>
    &nbsp; &nbsp;&nbsp;
    <hr width="51%" />
    Administración del Cache en el Servidor<br />
    <br />
    <table style="width: 368px" align="center" border="2" bordercolor="#006699">
        <tr>
            <td style="width: 87px">
                <asp:Button ID="btnCacheInicializarTodo" runat="server" Text="Inicializar Todo" Width="119px"
                    CssClass="txtAzul" BackColor="#C2E2F7" ForeColor="Black" />
            </td>
            <td colspan="2" style="width: 242px">
                <asp:Label ID="lblCacheResultadoInicializarTodo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 87px">
                <asp:Button ID="btnCacheInializarElemento" runat="server" Text="Inicializar Elemento"
                    Width="119px" CssClass="txtAzul" BackColor="#C2E2F7" ForeColor="Black" />
            </td>
            <td colspan="2" style="width: 242px">
                <asp:TextBox ID="txtCacheElemento" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                &nbsp;<asp:Label ID="lblCacheResultado" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
