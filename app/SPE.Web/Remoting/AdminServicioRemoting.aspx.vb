Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
'Imports Bermit.Soat.FachadaRemotingCliente;
Imports System.Collections
'Imports Bermit.Soat.EnsambladoComun.Services;
Imports Belikov.GenuineChannels
Imports Belikov.GenuineChannels.DotNetRemotingLayer
Imports Belikov.GenuineChannels.GenuineTcp
Imports Belikov.GenuineChannels.GenuineHttp
Imports Belikov.GenuineChannels.TransportContext
Imports Belikov.GenuineChannels.Security
Imports Belikov.GenuineChannels.Security.ZeroProofAuthorization
Imports System.Runtime.Remoting.Channels
Imports SPE.Web.RemoteServices

Partial Class Remoting_AdminServicioRemoting
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Console.WriteLine(User.Identity.Name)
            Label5.Text = ConfigurationManager.AppSettings.Get("tcpDefaultURL")
            Label4.Text = ConfigurationManager.AppSettings.Get("protocolo")
            Label9.Text = ConfigurationManager.AppSettings.Get("puerto")
            Label11.Text = ConfigurationManager.AppSettings.Get("InvocationTimeout")

            txtCacheElemento.Text = ""
            lblCacheResultadoInicializarTodo.Text = ""
            lblCacheResultado.Text = ""

        End If
    End Sub

    Private Function Conexion() As Boolean
        Try
            Return SPE.Web.RemoteServices.Instance.SetupNetworkEnvironment()

        Catch ex As Exception
            Return False
        End Try
    End Function


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Not (Conexion()) Then
            Try
                Dim cntrlServicio As New SPE.Web.CServicio()
                If (cntrlServicio.GetRecordByID(0) Is Nothing) Then
                    Label2.Text = "Hay Conexion con el servidor"

                End If

            Catch ex As Exception
                Dim mensaje As String = ex.Message
                Label2.Text = mensaje.Replace(ex.Message.ToString(), "No hay conexion con el Servidor")
            End Try
        Else
            Try
                Dim cntrlServicio As New SPE.Web.CServicio()
                If (cntrlServicio.GetRecordByID(0) Is Nothing) Then
                    Label2.Text = "Hay Conexion con el servidorr"

                End If

            Catch ex As Exception
                Dim mensaje As String = ex.Message
                Label2.Text = mensaje.Replace(ex.Message.ToString(), "No hay conexion con el Servidor")
            End Try

        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Conexion()
        Try

            Dim cntrlServicio As New SPE.Web.CServicio()
            If (cntrlServicio.GetRecordByID(0) Is Nothing) Then
                Label2.Text = "Hay Conexion con el servidor"

            End If

        Catch ex As Exception
            Dim mensaje As String = ex.Message
            Label2.Text = mensaje.Replace(ex.Message.ToString(), "No hay conexion con el Servidor")
        End Try


            'If Not (Conexion()) Then
            '    Try
            '        Dim cntrlServicio As New SPE.Web.CServicio()
            '        If (cntrlServicio.GetRecordByID(0) Is Nothing) Then
            '            Label2.Text = "no hay Conexion con el servidor"
            '        Else
            '            Label2.Text = "Hay Conexion con el servidor"
            '        End If

            '    Catch ex As Exception
            '        Dim mensaje As String = ex.Message
            '        Label2.Text = mensaje.Replace(ex.Message.ToString(), "No hay conexion con el Servidor")
            '    End Try
            'Else
            '    Try
            '        Dim cntrlServicio As New SPE.Web.CServicio()
            '        If (cntrlServicio.GetRecordByID(0) Is Nothing) Then
            '            Label2.Text = "no hay Conexion con el servidor"
            '        Else
            '            Label2.Text = "Hay Conexion con el servidor"
            '        End If

            '    Catch ex As Exception
            '        Dim mensaje As String = ex.Message
            '        Label2.Text = mensaje.Replace(ex.Message.ToString(), "No hay conexion con el Servidor")
            '    End Try

            'End If
    End Sub

    Protected Sub btnCacheInicializarTodo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCacheInicializarTodo.Click
        Try
            Using cntrlComun As New SPE.Web.CAdministrarComun()
                cntrlComun.InicializarTodoCache()
                lblCacheResultadoInicializarTodo.Text = "Todo el Cache se ha inicializado."
            End Using
        Catch ex As Exception
            lblCacheResultadoInicializarTodo.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCacheInializarElemento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCacheInializarElemento.Click
        Try
            Using cntrlComun As New SPE.Web.CAdministrarComun()
                cntrlComun.InicializarElementoCache(txtCacheElemento.Text)
                lblCacheResultado.Text = String.Format("Se ha inicializado el elemento cache: {0} .", txtCacheElemento.Text)
            End Using
        Catch ex As Exception
            lblCacheResultadoInicializarTodo.Text = ex.Message
        End Try
    End Sub
End Class
