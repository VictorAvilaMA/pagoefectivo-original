﻿<%@ Page Title="Asignar Roles y Controles a Pagina" Language="C#" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="true" CodeFile="SegAsigRCPagina.aspx.cs" Inherits="SegAsigSRCPagina"
    Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div class="colRight">
        <h2>
            Asignar Roles a la Página</h2>
        <div class="inner-col-right">
            <div class="dlgrid">
                <fieldset>
                    <div class="cont-panel-search">
                        <div class="even w100 clearfix dlinline">
                            <dl class="clearfix dt10 dd30">
                                <dt class="desc">
                                    <asp:Label ID="Label2" runat="server" CssClass="EtiquetaLargo" Text="Rol:"></asp:Label>
                                </dt>
                                <dd class="camp">
                                    <asp:TextBox ID="txtRol" runat="server" Enabled="false" CssClass="TextoLargo SoloLectura"></asp:TextBox>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="izq btns4">
                        <asp:Button ID="btnMant" runat="server" CssClass="btnVolver" Text="Regresar" OnClick="btnMant_Click"
                            Visible="True" />
                        <br />
                        <asp:Button ID="btnBuscarRol" runat="server" CssClass="btnBuscar" Text="Buscar" OnClick="btnBuscarRol_Click" />
                    </div>
                </fieldset>
            </div>
            <div class="result">
                <asp:UpdatePanel ID="upnlPaginas" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h3>
                            <asp:Literal ID="Label3" runat="server" Text="Resultados:"></asp:Literal>
                        </h3>
                        <asp:GridView ID="gvPaginas" AllowSorting="True" runat="server" CssClass="grilla"
                            DataKeyNames="Id" AllowPaging="True" AutoGenerateColumns="False" PageSize="10"
                            OnPageIndexChanging="gvPaginas_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="Asignado">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAsociado" runat="server" AutoPostBack="true" OnCheckedChanged="chkAsociado_CheckedChanged" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="T&#237;tulo">
                                    <ItemTemplate>
                                        <%# Eval("Title") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Url" HeaderText="Url"></asp:BoundField>
                                <asp:BoundField DataField="Parent" HeaderText="Parent" />
                                <asp:BoundField DataField="Display" HeaderText="Display"></asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
