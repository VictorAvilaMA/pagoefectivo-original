﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SPE.Web.Seguridad;
using _3Dev.FW.Entidades.Seguridad;

public partial class SegAsigSRCPagina : Page
{
    int IdSistema;
    int IdSiteMap;
    bool EsMenu;
    //SistemasClient sc = new SistemasClient();

    SPESiteMapProviderClient csmpc = new SPESiteMapProviderClient();
    SPEUsuarioClient uc = new SPEUsuarioClient();
    SPERoleProviderClient crpc = new SPERoleProviderClient();


    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if( Session["NombreRol"]!=null)
                txtRol.Text = Session["NombreRol"].ToString();
            BindearPaginas();
            ConstruirPaginasPorRol();
               
        }
    }
    private static int _idpaginatemp;
    private static bool PredicateRolTemp(BESiteMap oBESiteMap)
    {
        return (oBESiteMap.Id == _idpaginatemp);
    }
    public void BindearPaginas()
    {
        gvPaginas.DataSource = csmpc.GetPagesLikNameForSistema("", 1);
        gvPaginas.DataBind();
    }
    public void ConstruirPaginasPorRol()
    {
        int idrol = Convert.ToInt32(Request["IdRol"]);
        List<BESiteMap> list=  csmpc.GetSiteMapsByIdRol(idrol);
      foreach (GridViewRow gvr in gvPaginas.Rows){
          _idpaginatemp = Convert.ToInt32(gvPaginas.DataKeys[gvr.RowIndex].Value);
          ((CheckBox)gvr.FindControl("chkAsociado")).Checked = (list.Find(PredicateRolTemp) != null);
      }
        
    }

    protected void btnBuscarRol_Click(object sender, EventArgs e)
    {
    }
    protected void gvRoles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }
    protected void btnMant_Click(object sender, EventArgs e)
    {
        Response.Redirect("SegMantRol.aspx");
    }
    private void MensajeJS(string key, string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("alert('");
        sb.Append(mensaje);
        sb.Append("');");

        ScriptManager.RegisterClientScriptBlock(this, GetType(), key, sb.ToString(), true);
    }
    protected void gvPaginas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPaginas.DataSource = csmpc.GetPagesLikNameForSistema("", 1);
        gvPaginas.PageIndex = e.NewPageIndex;
        gvPaginas.DataBind();
        ConstruirPaginasPorRol();
    }
    protected void chkAsociado_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox) sender ;
         
        GridViewRow gvr =  (GridViewRow) chk.NamingContainer;
        int id = Convert.ToInt32( gvPaginas.DataKeys[gvr.RowIndex ].Value );
        int idrol = Convert.ToInt32(Request["IdRol"]);
        if (!chk.Checked)
        {
            crpc.RemoveSiteMapFromRol(id, idrol);
        }
        else {
            crpc.AddSiteMapToRol (id, idrol);
        }
        //SPE.Web.Seguridad.SPESiteMapProvider x = new SPESiteMapProvider();
        

    }
}
