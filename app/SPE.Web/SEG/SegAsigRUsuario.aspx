﻿<%@ Page Title="Asignar Roles a Usuario" Language="C#" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="true" CodeFile="SegAsigRUsuario.aspx.cs" Inherits="SegAsigSRUsuario"
    Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
       <h2>Asignar Roles a Usuario</h2>
    <div class="conten_pasos3">
        <h6>Usuario: <asp:Literal ID="lblUsuario" runat="server"></asp:Literal></h6>
           <ul class="datos_cip2">
                   <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <li class="t1"><span class="color">&gt;&gt; </span>Rol: </li>
                                <li class="t2" style="line-height: 1">
                                    <asp:DropDownList ID="ddlRol" runat="server" CssClass="normal" 
                                        AutoPostBack="true" onselectedindexchanged="ddlRol_SelectedIndexChanged1" >
                                     </asp:DropDownList>
                                </li>  
                                 <asp:Panel runat="server" ID="pnlTipoOrigenCancelacion" Visible="false">
                                
                                    <li class="t1"><span class="color">>></span> Origenes de Cancelación: </li>
                                    <li class="t2" style="height: auto; ">
                                        <asp:CheckBoxList ID="cklOrigenesCancelacion" runat="server" DataTextField="Descripcion"
                                            DataValueField="IdTipoOrigenCancelacion" class="normal checklistsm">
                                        </asp:CheckBoxList>
                                    </li>
                                
                            </asp:Panel> 
                            </ContentTemplate>
                   </asp:UpdatePanel>
                   
                    <li class="complet">
                        <asp:Button ID="btnAsignar" runat="server" CssClass="input_azul5" Text="Asignar" OnClick="btnAsignar_Click" />
                        <asp:Button ID="btnRegresar" runat="server" CssClass="input_azul4" Text="Regresar"
                            OnClick="btnRegresar_Click" />
                    </li>
               
            </ul>
             <div class="cont_cel">
       
                <asp:UpdatePanel ID="upnlEmpresa" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h5 id="h5result" runat="server">
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>

                        <div>
                            &nbsp;<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <asp:GridView ID="gvRoles" runat="server" CssClass="grilla" OnPageIndexChanging="gvRoles_PageIndexChanging"
                            OnRowCommand="gvRoles_RowCommand" DataKeyNames="IdRol" PageSize="10" AutoGenerateColumns="False"
                            AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="descripcion" HeaderText="Rol"></asp:BoundField>
                                <asp:TemplateField HeaderText="Eliminar">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEliminar" runat="server" Width="16px" CommandArgument='<%# Eval("IdRol") %>'
                                            ToolTip="Eliminar Asociación" ImageUrl="~/images/delete.png" CommandName="Eliminar"
                                            __designer:wfdid="w1"></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAsignar" EventName="Click"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            
        </div>
    </div>
</asp:Content>
