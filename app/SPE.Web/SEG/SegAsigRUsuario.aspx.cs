﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SPE.Web.Seguridad;
using _3Dev.FW.EmsambladoComun;
using _3Dev.FW.Entidades.Seguridad;
using SPE.Entidades;
using System.Collections.Generic;

public partial class SegAsigSRUsuario : Page
{
    int IdUsuario;
    SPEUsuarioClient uc = new SPEUsuarioClient();
    SPERoleProviderClient crpc = new SPERoleProviderClient();
    private void MensajeJS(string key, string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("alert('");
        sb.Append(mensaje);
        sb.Append("');");

        ScriptManager.RegisterClientScriptBlock(this, GetType(), key, sb.ToString(), true);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlRol.DataTextField = "Descripcion";
            ddlRol.DataValueField  = "IdRol";
            ddlRol.DataSource = crpc.GetRolBySistema(1);
            ddlRol.DataBind();
            CargaOrigenCancelacion();
        }
        if ((Session[SystemParameters.Security.S_USUARIO] == null) && (Request["IdUsuario"] == null))
        {
            Response.Redirect("SegConUsuario.aspx");
        }
        else
        {
            if (Request["IdUsuario"] != null)
            {
                IdUsuario = int.Parse(Request["IdUsuario"]);
                if(!IsPostBack)
                {
                     BEUsuario user = uc.GetUsuarioById(IdUsuario);
                    lblUsuario.Text = user.Nombres;
                }
            }
        }
        if (!IsPostBack)
        {
            Session["GrillaRol"] = crpc.GetRolForUserId(IdUsuario);
            gvRoles.DataSource = (Session["GrillaRol"]);
            gvRoles.DataBind();
        }
    }
    protected void gvRoles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Eliminar"))
        {
            crpc.RemoveUserFromRol(IdUsuario, int.Parse(e.CommandArgument.ToString()));
            Session["GrillaRol"] = crpc.GetRolForUserId(IdUsuario);
            gvRoles.DataSource = (Session["GrillaRol"]);
            gvRoles.DataBind();
        }
    }
    protected void btnAsignar_Click(object sender, EventArgs e)
    {
        if (ddlRol.SelectedIndex >= 0)
        {
            bool noesta = true;
            for (int i = 0; i < gvRoles.Rows.Count; i++)
            {
                if (gvRoles.Rows[i].Cells[0].Text.Equals(ddlRol.SelectedItem.Text))
                {
                    noesta = false;
                    break;
                }
            }
            if (noesta)
            {
                crpc.AddUserToRol(IdUsuario, int.Parse(ddlRol.SelectedValue));
                Session["GrillaRol"] = crpc.GetRolForUserId(IdUsuario);
                gvRoles.DataSource = (Session["GrillaRol"]);
                gvRoles.DataBind();
                if (ddlRol.SelectedItem.Text.Equals(SPE.EmsambladoComun.ParametrosSistema.RolJefeProducto))
                {
                    if (CklIsSelect())
                    {
                        SPE.Web.CJefeProducto fnc = new SPE.Web.CJefeProducto();

                        SPE.Entidades.BEJefeProducto obeJefeProducto = (BEJefeProducto)fnc.GetRecordByID(IdUsuario);

                        obeJefeProducto.ListaTipoOrigenCancelacion = CargarTipoOrigenCancelacion();
                        
                        fnc.UpdateRecord(obeJefeProducto);

                    }
                    else
                    {
                        MensajeJS("1", "Debe seleccionar por lo menos un tipo de origen de cancelación.");
                        return;
                    }
                }

               
              
            }
            else
            {
                MensajeJS("1", "El Rol ya esta Ingresado");
            }
        }
        else
        {
            MensajeJS("1", "No tiene Rol Seleccionado");
        }
    }
    protected void ddlSistema_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlRol.DataSource = crpc.GetRolBySistema(1);
        ddlRol.DataBind();
        if (ddlRol.Items.Count >= 0)
        {
            Session["GrillaRol"] = crpc.GetRolForUserId(IdUsuario);
            gvRoles.DataSource = (Session["GrillaRol"]);
            gvRoles.DataBind();
        }
    }
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("SegConUsuario.aspx?IdUsuario="+IdUsuario.ToString());
    }
    protected void gvRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRoles.DataSource = (Session["GrillaRol"]);
        gvRoles.PageIndex = e.NewPageIndex;
        gvRoles.DataBind();
    }
    //MDS
    protected void CargaOrigenCancelacion()
    {
        SPE.Web.CComun objOrigenCancelacion = new SPE.Web.CComun();
        cklOrigenesCancelacion.DataSource = objOrigenCancelacion.ConsultarTipoOrigenCancelacion();
        cklOrigenesCancelacion.DataBind();

    }
   
    protected bool CklIsSelect()
    {

        System.Web.UI.WebControls.ListItem Item = null;
        foreach (System.Web.UI.WebControls.ListItem Item_loopVariable in cklOrigenesCancelacion.Items)
        {
            Item = Item_loopVariable;
            if (Item.Selected)
                return true;
        }
        return false;
    }
    protected List<BETipoOrigenCancelacion> CargarTipoOrigenCancelacion()
    {

        System.Web.UI.WebControls.ListItem Item = null;
        SPE.Entidades.BETipoOrigenCancelacion obeTipoOrigenCancelacion = default(SPE.Entidades.BETipoOrigenCancelacion);
        List<SPE.Entidades.BETipoOrigenCancelacion> ListTipoOrigenCancelacion = new List<SPE.Entidades.BETipoOrigenCancelacion>();

        foreach (System.Web.UI.WebControls.ListItem Item_loopVariable in cklOrigenesCancelacion.Items)
        {
            Item = Item_loopVariable;
            obeTipoOrigenCancelacion = new SPE.Entidades.BETipoOrigenCancelacion();
            obeTipoOrigenCancelacion.IdTipoOrigenCancelacion = Convert.ToInt32(Item.Value);
            if (Item.Selected == true)
            {
                obeTipoOrigenCancelacion.IdEstado = 1;
            }
            else
            {
                obeTipoOrigenCancelacion.IdEstado = 0;
            }
            ListTipoOrigenCancelacion.Add(obeTipoOrigenCancelacion);
        }
        return ListTipoOrigenCancelacion;
    }
 
    protected void ddlRol_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlRol.SelectedItem.Text.Equals(SPE.EmsambladoComun.ParametrosSistema.RolJefeProducto))
        {
            
            pnlTipoOrigenCancelacion.Visible = true;
        }
        else
        {
            pnlTipoOrigenCancelacion.Visible = false;
        }
    }
}
