﻿<%@ Page Title="Asignar Sistemas y Usuariosa Rol" Language="C#" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="true" CodeFile="SegAsigSURol.aspx.cs" Inherits="SegAsigSURol" Async ="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Page">
        <asp:ScriptManager id="ScriptManager1" runat="server"></asp:ScriptManager>
        <div style="float:left;">
            <h1>Asignar Usuario a Rol</h1>
        </div>

        <div id="div1" class="divContenedor">
            <div id="AsigUsuarios" class="divContenedorTitulo">
                Asignación de Usuarios para el Rol:                          
                <div id="div4">
                    <div id="div5" class="divSubContenedorCriteriosBusqueda" >
                        <fieldset class="ContenedorEtiquetaTexto stripe">
                            <div id="div6" class="EtiquetaTextoIzquierda">
                                <asp:Label ID="Label2" runat="server" CssClass="EtiquetaLargo" Text="Usuario:"></asp:Label>
                                <asp:TextBox ID="txtUsuario" runat="server" CssClass="TextoLargo"></asp:TextBox>
                            <asp:CheckBox ID="chkUsuario" runat="server" Text="Seleccionar Solo Asociados" />
                            </div>
                        </fieldset>
                    </div>
                    <fieldset id="Fieldset1" class="ContenedorBotoneraCriteriosBusqueda" >
                        <asp:Button ID="btnBuscarUsuario" runat="server" CssClass="Boton" Text="Buscar" onclick="btnBuscarUsuario_Click" />                            
                    </fieldset>
                </div>
                <asp:UpdatePanel id="upnlUsuario" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                        <div id="div9" class="divContenedorTitulo">
                            <asp:Label ID="Label3"  runat="server"  Text="Resultados:"></asp:Label>
                        </div>
                        <div id="div10" class="divSubContenedorGrilla">
                            <asp:GridView id="gvUsuario" AllowSorting="True"  runat="server" 
                                CssClass="grilla" DataKeyNames="IdUsuario"  AllowPaging="True" 
                                AutoGenerateColumns="False" PageSize="10" onrowcommand="gvUsuario_RowCommand" 
                                onpageindexchanging="gvUsuario_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="Usuario" DataField="UserName" />
                                    <asp:BoundField HeaderText="Nombres" DataField="PrimerNombre" />
                                    <asp:BoundField HeaderText="Apellidos" DataField="ApellidoPaterno" />
                                    <asp:TemplateField HeaderText="Asociar">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnAsociar" runat="server" CommandName="Asociar" ImageUrl='<%# (Eval("Pertenece").ToString()=="0")?"~/images/preferences.png":"~/images/blanco.png" %>'  ToolTip="Asociar" Width="16px" CommandArgument='<%# Eval("IdUsuario") %>'  Enabled='<%# (Eval("Pertenece").ToString()=="0")?true:false %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Retirar">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnEliminar" CommandName="Eliminar" runat="server" ImageUrl='<%# (Eval("Pertenece").ToString()=="0")?"~/images/blanco.png":"~/images/delete.png" %>'  ToolTip="Eliminar Asociación" Width="16px" CommandArgument='<%# Eval("IdUsuario") %>'  Enabled='<%# (Eval("Pertenece").ToString()=="0")?false:true %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="cabecera" />
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </contenttemplate>
                    <triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnBuscarUsuario" EventName="Click">
                        </asp:AsyncPostBackTrigger>
                    </triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

