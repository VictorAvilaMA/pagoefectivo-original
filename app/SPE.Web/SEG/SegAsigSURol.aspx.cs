﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SPE.Web.Seguridad;

public partial class SegAsigSURol : Page
{
    int IdRol;
    //SistemasClient sc = new SistemasClient();
    SPEUsuarioClient uc = new SPEUsuarioClient();
    SPERoleProviderClient crpc = new SPERoleProviderClient();
//CustomRoleProviderClient a = new Custom

    private void MensajeJS(string key, string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("alert('");
        sb.Append(mensaje);
        sb.Append("');");

        ScriptManager.RegisterClientScriptBlock(this, GetType(), key, sb.ToString(), true);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IdRol == 0)
        {
            if (Request["IdRol"] != null)
            {
                IdRol = int.Parse(Request["IdRol"]);
                //lblRol1.Text = crpc.getRolById(IdRol)[0].Descripcion ;
            }
            else
            {
                Response.Redirect("./SegMantRol.aspx");
            }
        }
    }
    protected void btnBuscarSistema_Click(object sender, EventArgs e)
    {
        //if (!chkSistema.Checked)
        //{
        //    Session["GrillaSistema"] = sc.GetSistemasLikeNameForRol(txtSistema.Text, IdRol);
        //}
        //else{
        //    Session["GrillaSistema"] = sc.GetSistemasLikeNameForRolRelated(txtSistema.Text, IdRol);
        //}

        //gvSistema.DataSource = ( BESistema[])(Session["GrillaSistema"]);
        //gvSistema.DataBind(); 
    }
    protected void btnBuscarUsuario_Click(object sender, EventArgs e)
    {
        //if (!chkUsuario.Checked)
        //{
        //    Session["GrillaUsuario"] = uc.GetUsuarioLikeNameForRol(txtUsuario.Text, IdRol);
        //}
        //else
        //{
        //    Session["GrillaUsuario"] = uc.GetUsuarioLikeNameForRolRelated(txtUsuario.Text, IdRol);
        //} 
        //gvUsuario.DataSource = ( BEUsuario[])(Session["GrillaUsuario"]);
        //gvUsuario.DataBind();
    }
    protected void gvSistema_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool paso = true;
        if (e.CommandName.Equals("Asociar"))
        {
            if (!crpc.AddSistemaToRol(int.Parse(e.CommandArgument.ToString()), IdRol))
            {
                paso = false;
                MensajeJS("2", "No se Pudo Asignar al Sistema");
            }
        }
        else
        {
            if (e.CommandName.Equals("Eliminar"))
            {
                if (!crpc.RemoveSistemaFromRol(int.Parse(e.CommandArgument.ToString()), IdRol))
                {
                    paso = false;
                    MensajeJS("2", "No se Pudo Remover al Sistema");
                }
            }
        }
        if (paso)
        {
            //gvSistema.DataSource = sc.GetSistemasLikeNameForRol(txtSistema.Text, IdRol);
            //gvSistema.DataBind();
        }
    }
    protected void gvUsuario_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool paso = true;
        if (e.CommandName.Equals("Asociar"))
        {
            if (!crpc.AddUserToRol(int.Parse(e.CommandArgument.ToString()), IdRol))
            {
                paso = false;
                MensajeJS("2", "No se Pudo Asignar al Usuario");
            }
        }
        else
        {
            if (e.CommandName.Equals("Eliminar"))
            {
                if (!crpc.RemoveUserFromRol(int.Parse(e.CommandArgument.ToString()), IdRol))
                {
                    paso = false;
                    MensajeJS("2", "No se Pudo Remover al Usuario");
                }
            }
        }
        if (paso)
        {
            gvUsuario.DataSource = uc.GetUsuarioLikeNameForRol(txtUsuario.Text,IdRol);
            gvUsuario.DataBind();
        }
    }
    protected void btnGrabar_Click(object sender, EventArgs e)
    {
        //if (txtRol.Text != "")
        //{
        //    if (!txtRol.Text.Trim().Equals(lblRol1.Text.Trim()))
        //    {
        //        if (crpc.GetRolIdByName(txtRol.Text) == 0)
        //        {
        //            crpc.UpdateRoleName(IdRol, txtRol.Text, "");
        //            lblRol1.Text = txtRol.Text;
        //        }
        //        else
        //        {
        //            MensajeJS("1", "El Rol ya Existe");
        //        }
        //    }
        //    else
        //    {
        //        MensajeJS("2", "El Nombre del Rol es Distinto");
        //    }
        //}
        //else
        //{
        //    MensajeJS("2", "Debe Ingresar un Nombre a Cambiar");
        //}
    }
   
    protected void gvUsuario_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsuario.DataSource = (Session["GrillaUsuario"]);
        gvUsuario.PageIndex = e.NewPageIndex;
        gvUsuario.DataBind();
    }
}