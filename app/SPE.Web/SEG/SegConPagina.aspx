﻿<%@ Page Title="Consultar Paginas" Language="C#" MasterPageFile="~/MasterPagePrincipal.master"
    Async="true" AutoEventWireup="true" CodeFile="SegConPagina.aspx.cs" Inherits="SegConPagina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consulta de Páginas</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de búsqueda</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">>></span> Página: </li>
            <li class="t2">
                <asp:TextBox ID="txtPagina" runat="server" CssClass="normal"></asp:TextBox>
            </li>
            <li class="complet">
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul3" Text="Nuevo" Visible="false" />
                <asp:Button ID="btnBuscarPagina" runat="server" CssClass="input_azul3" Text="Buscar"
                    OnClick="btnBuscarPagina_Click" />
            </li>
        </ul>
        <div style="clear:both"></div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvPaginas" AllowSorting="True" runat="server" CssClass="grilla"
                            DataKeyNames="Id" AllowPaging="True" AutoGenerateColumns="False" PageSize="10"
                            OnPageIndexChanging="gvPaginas_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="Título">
                                    <ItemTemplate>
                                        <%# Eval("Title") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Url" HeaderText="Url"></asp:BoundField>
                                <asp:BoundField DataField="Parent" HeaderText="Parent" />
                                <asp:BoundField DataField="Display" HeaderText="Display"></asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscarPagina" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
