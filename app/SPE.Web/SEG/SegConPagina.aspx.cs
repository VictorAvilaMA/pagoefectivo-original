﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SPE.Web.Seguridad;
using _3Dev.FW.Entidades.Seguridad;
using System.Collections.Generic;

public partial class SegConPagina : Page
{
    int IdSistema;
 //   SistemasClient sc = new SistemasClient();
    SPESiteMapProviderClient csmpc = new SPESiteMapProviderClient();
    private void MensajeJS(string key, string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("alert('");
        sb.Append(mensaje);
        sb.Append("');");

        ScriptManager.RegisterClientScriptBlock(this, GetType(), key, sb.ToString(), true);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    ddlSistema.DataSource = sc.GetAllSistemasList();
        //    ddlSistema.DataBind();
        //    if (Request["IdSistema"] != null)
        //    {
        //        IdSistema = int.Parse(Request["IdSistema"].ToString());
        //        //ddlSistema.Enabled = false;            
        //    }
        //    if (Request["IdPagina"] != null)
        //    {
        //        BESiteMap[] lista = new BESiteMap[1];
        //        lista[0] = csmpc.GetSiteMapById(int.Parse(Request["IdPagina"].ToString()));
        //        Session["GrillaPagina"] = lista;
        //    }
        //    else
        //    {
        //        Session["GrillaPagina"] = csmpc.GetPagesLikNameForSistema(txtPagina.Text, int.Parse(ddlSistema.SelectedValue));
        //    }
        //    gvPaginas.DataSource = (BESiteMap[])(Session["GrillaPagina"]);
        //    gvPaginas.DataBind();
        //}        
    }
    protected void btnBuscarPagina_Click(object sender, EventArgs e)
    {
        List<BESiteMap> listPaginas = csmpc.GetPagesLikNameForSistema(txtPagina.Text, 1);
        gvPaginas.DataSource = listPaginas;
        gvPaginas.DataBind();
        divResult.Visible = true;
        lblMsgNroRegistros.Text = "Resultados: " + listPaginas.Count + " registros.";
    }
   
    
   
    protected void gvPaginas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPaginas.DataSource = csmpc.GetPagesLikNameForSistema(txtPagina.Text, 1);
        gvPaginas.PageIndex = e.NewPageIndex;
        gvPaginas.DataBind();
    }
}
