﻿<%@ Page Title="Consultar Usuarios" Language="C#" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="true" CodeFile="SegConUsuario.aspx.cs" Inherits="SegConUsuario"
    Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consulta de Usuarios</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de búsqueda</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">>></span> Email: </li>
            <li class="t2">
                <asp:TextBox ID="txtUsuario" runat="server" CssClass="normal"></asp:TextBox>
            </li>
            <li class="complet">
                <asp:Button ID="btnNuevo" runat="server" CssClass="input_azul3" Text="Nuevo" PostBackUrl="~/SEG/SegMantUsuario.aspx"
                    OnClick="btnNuevo_Click" />
                <asp:Button ID="btnBuscarUsuarios" runat="server" CssClass="input_azul4" Text="Buscar"
                    OnClick="btnBuscarUsuarios_Click" />
            </li>
        </ul>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="upnlUsuario" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <div>
                            &nbsp;<asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <asp:GridView ID="gvUsuarios" AllowSorting="True" runat="server" CssClass="grilla"
                            DataKeyNames="IdUsuario" AllowPaging="True" AutoGenerateColumns="False" PageSize="10"
                            OnRowCommand="gvUsuarios_RowCommand" OnPageIndexChanging="gvUsuarios_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" />
                                <asp:BoundField DataField="Nombres" HeaderText="Nombre">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionEstado" HeaderText="Estado" Visible="False" />
                                <asp:BoundField DataField="Email" HeaderText="E-Mail">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnDetalle" CommandName="Detalle" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/search.png" %>'
                                            ToolTip="Ver Detalle" Width="16px" CommandArgument='<%# Eval("IdUsuario") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnEditar" runat="server" CommandName="Editar" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/Settings.png" %>'
                                            ToolTip="Editar Sistema y Roles" Width="16px" CommandArgument='<%# Eval("IdUsuario") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscarUsuarios" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
