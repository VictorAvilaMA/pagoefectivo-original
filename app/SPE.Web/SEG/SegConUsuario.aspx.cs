﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SPE.Web.Seguridad;
using _3Dev.FW.Entidades;
using _3Dev.FW.Entidades.Seguridad;
using _3Dev.FW.Web;

public partial class SegConUsuario : Page
{
    SPEUsuarioClient uc = new SPEUsuarioClient();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["IdUsuario"] != null)
            {
                List<BEUsuario> lista = new List<BEUsuario>();
                lista.Add(uc.GetUsuarioById(int.Parse(Request["IdUsuario"])));
                Session["GrillaUsuarios"] = lista;
                gvUsuarios.DataSource = (Session["GrillaUsuarios"]);
                gvUsuarios.DataBind();
            }
        }

    }
    protected void gvUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Session[SES.S_USUARIO] = null;
        if (e.CommandName.Equals("Detalle"))
        {
            Response.Redirect("SegMantUsuario.aspx?IdUsuario=" + int.Parse(e.CommandArgument.ToString()));
        }
        else
        {
            if (e.CommandName.Equals("Editar"))
            {
                Response.Redirect("SegAsigRUsuario.aspx?IdUsuario=" + int.Parse(e.CommandArgument.ToString()));
            }
        }
    }
    protected void btnBuscarUsuarios_Click(object sender, EventArgs e)
    {
        //Session["GrillaUsuarios"] = uc.GetUsuarioLikeName(txtUsuario.Text , GetEntity ());
        //gvUsuarios.DataSource = (List <BEUsuario>)(Session["GrillaUsuarios"]);
        //gvUsuarios.DataBind();
        gvUsuarios.PageIndex = 0;
        cargarGrilla();
        divResult.Visible = true;
    }
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        //Response.Redirect("SegMantUsuario.aspx");
    }
    protected void gvUsuarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsuarios.PageIndex = e.NewPageIndex;
        cargarGrilla();
        //gvUsuarios.DataSource = (List<BEUsuario>)(Session["GrillaUsuarios"]);
        //gvUsuarios.PageIndex = e.NewPageIndex;
        //gvUsuarios.DataBind();
    }

    protected BusinessEntityBase GetEntity()
    {
        BusinessEntityBase entidad = new BusinessEntityBase();
        entidad.PageNumber = gvUsuarios.PageIndex + 1;
        entidad.PageSize = gvUsuarios.PageSize;
        entidad.PropOrder = "";
        entidad.TipoOrder = false;
        return entidad;
    }

    protected void cargarGrilla()
    {
        //gvUsuarios.PageIndex = gri - 1;
        List<BEUsuario> ResultList = uc.GetUsuarioLikeName(txtUsuario.Text, GetEntity());

        ObjectDataSource oObjectDataSource = new ObjectDataSource();
        oObjectDataSource.ID = "oObjectDataSource_" + gvUsuarios.ID;
        oObjectDataSource.EnablePaging = gvUsuarios.AllowPaging;
        oObjectDataSource.TypeName = "_3Dev.FW.Web.TotalRegistrosTableAdapter";
        oObjectDataSource.SelectMethod = "GetDataGrilla";
        oObjectDataSource.SelectCountMethod = "ObtenerTotalRegistros";
        oObjectDataSource.StartRowIndexParameterName = "filaInicio";
        oObjectDataSource.MaximumRowsParameterName = "maxFilas";
        oObjectDataSource.EnableViewState = false;
        //oObjectDataSource.ObjectCreating += new ObjectDataSourceObjectEventHandler(oObjectDataSource_ObjectCreating);
        int TotalGeneral = ResultList.Count == 0 ? 0 : ResultList.First().TotalPageNumbers;
        oObjectDataSource.ObjectCreating += delegate(Object sender, ObjectDataSourceEventArgs e) { e.ObjectInstance = new TotalRegistrosTableAdapter(ResultList, TotalGeneral); };
        lblMsgNroRegistros.Text = "Resultados: " + TotalGeneral + " registro(s):";
        gvUsuarios.DataSource = oObjectDataSource;
        gvUsuarios.DataBind();
    }

    //private void oObjectDataSource_ObjectCreating(Object sender, ObjectDataSourceEventArgs e)
    //{
    //    e.ObjectInstance = new _3Dev.FW.Web.TotalRegistrosTableAdapter(ResultList, ResultList.First().TotalPageNumbers);
    //}

}
