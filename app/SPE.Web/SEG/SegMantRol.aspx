﻿<%@ Page Title="Mantenimiento de Roles" Language="C#" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="true" CodeFile="SegMantRol.aspx.cs" Inherits="SegMantRol" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="Label1" runat="server" Text="Mantenimiento de Roles"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <h4>
            Información del Rol</h4>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">>></span> Rol: </li>
            <li class="t2">
                <asp:TextBox ID="txtRol" runat="server" CssClass="normal"></asp:TextBox>
            </li>
            <li class="complet">
                <asp:Button ID="btnAgregar" runat="server" CssClass="input_azul3" Text="Agregar"
                    OnClick="btnAgregar_Click" />
                <asp:Button ID="btnBuscarRol" runat="server" CssClass="input_azul4" Text="Buscar"
                    OnClick="btnBuscarRol_Click" />
            </li>
        </ul>
        <div style="clear:both"></div>
        <div class="result">
            <asp:UpdatePanel ID="upnlRoles" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                        </h5>
                        <asp:GridView ID="gvRol" AllowSorting="True" runat="server" CssClass="grilla" DataKeyNames="IdRol"
                            AllowPaging="True" AutoGenerateColumns="False" PageSize="12" OnRowCommand="gvRol_RowCommand"
                            OnPageIndexChanging="gvRol_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="Descripcion" HeaderText="Rol" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnAsociar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/Settings.png" %>'
                                            CommandName="Asociar" ToolTip="Asociar Paginas y Controles" Width="16px" CommandArgument='<%# Eval("IdRol") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibtnEliminar" CommandName="Eliminar" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/delete.png" %>'
                                            ToolTip="Eliminar Rol" Width="16px" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAgregar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscarRol" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
