﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SPE.Web.Seguridad;
using _3Dev.FW.Entidades.Seguridad;
using System.Collections.Generic;

public partial class SegMantRol : Page
{
    SPERoleProvider CustomRoleProviderClient = new SPERoleProvider();
  
         
    protected void Page_Load(object sender, EventArgs e)
    {
        //lblError.Text = "123";
       
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        if (txtRol.Text != "")
        {
            if (!CustomRoleProviderClient.RoleExists(txtRol.Text) )
            {
                CustomRoleProviderClient.CreateRole(txtRol.Text);
                gvRol.DataSource = CustomRoleProviderClient.GetAllRolesList();
                gvRol.DataBind();
                MensajeJS("0", "Se registro el rol con éxito");
             }
            else
            {
                MensajeJS("1", "El Rol ya Existe");
            }
        }
        else
        {
            MensajeJS("2", "Debe Ingresar un Rol a Agregar");
        }
    }
    protected void btnBuscarRol_Click(object sender, EventArgs e)
    {
        List<BERol> listaRol = CustomRoleProviderClient.GetRolLikeName(txtRol.Text);
        Session["GrillaRol"] = listaRol;
        gvRol.DataSource = listaRol;
        gvRol.DataBind();
        lblMsgNroRegistros.Text = "Resultados: " + listaRol.Count + " registros.";
        divResult.Visible = true;
    }

    private void MensajeJS(string key, string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("alert('");
        sb.Append(mensaje);
        sb.Append("');");

        ScriptManager.RegisterClientScriptBlock(this, GetType(), key, sb.ToString(), true);
    }
    protected void gvRol_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Editar"))
        {
            Response.Redirect("SegAsigRCPagina.aspx?IdRol=" + int.Parse(e.CommandArgument.ToString()));
        }
        else
        {
            if (e.CommandName.Equals("Eliminar"))
            {
                if (CustomRoleProviderClient.DeleteRole(gvRol.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text, true))
                {
                    gvRol.DataSource = CustomRoleProviderClient.GetRolLikeName(txtRol.Text);
                    gvRol.DataBind();
                }
                else
                {
                    MensajeJS("2", "Es Posible que el Rol tenga Relación con otras entidades (Usuarios o Páginas), elimine primero dichas relaciones");
                }
            }
            else
            {
                if (e.CommandName.Equals("Asociar"))
                {
                    //Response.Redirect("SegAsigCPRol.aspx?IdRol=" + int.Parse(e.CommandArgument.ToString()));
                    ImageButton imgbt = (ImageButton)e.CommandSource ;
                    Session["NombreRol"] = ((GridViewRow)imgbt.NamingContainer).Cells[0].Text;
                    Response.Redirect("SegAsigRCPagina.aspx?IdRol=" + int.Parse(e.CommandArgument.ToString()));
                }
            }
        }
    }
    protected void gvRol_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRol.PageIndex = e.NewPageIndex;
        gvRol.DataSource = (List<BERol>)(Session["GrillaRol"]);
        gvRol.DataBind();
    }
}
