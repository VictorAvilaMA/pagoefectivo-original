﻿<%@ Page Title="Mantenimiento de Usuarios" Language="C#" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="true" CodeFile="SegMantUsuario.aspx.cs" Inherits="SegMantUsuario"
    Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        <asp:Literal ID="lblProceso" runat="server" Text="Mantenimiento de Usuarios"></asp:Literal>
    </h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pnlPage" runat="server">
            <asp:Panel ID="pnlDatosUsuario" runat="server" CssClass="inner-col-right">
                <h4>
                    1. Información del Usuario</h4>
                <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">>></span> E-mail: </li>
                    <li class="t2">
                        <asp:TextBox ID="txtEmailPrincipal" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailPrincipal"
                            ErrorMessage="Formato de email invalido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailPrincipal"
                            ErrorMessage="Debe ingresar su email">*</asp:RequiredFieldValidator>
                    </li>
                </ul>
                <div style="clear: both">
                </div>
                <h4>
                    2. Información General</h4>
                <asp:Panel ID="pnlInformacionRegistro" runat="server">
                    <spe:ucUsuarioDatosComun ID="ucUsuarioDatosComun" runat="server"></spe:ucUsuarioDatosComun>
                </asp:Panel>
                <asp:HiddenField ID="hdnIdEstado" runat="server" />
                <asp:HiddenField ID="hdnIdUsuario" runat="server" />
                <div style="clear:both"></div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ValidationGroup="ValidaCampos" style="padding-left:200px"/>
                <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" OnClientClick="return ConfirmMe();"
                            Text="Registrar" ValidationGroup="ValidaCampos" OnClick="btnRegistrar_Click" />
                        <asp:Button ID="btnActualizar" runat="server" CssClass="input_azul4" Text="Actualizar"
                            ValidationGroup="ValidaCampos" Visible="False" OnClick="btnActualizar_Click" />
                        <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                            Text="Cancelar" OnClick="btnCancelar_Click" />
                        <asp:Button ID="btnAsignar" runat="server" CssClass="input_azul4" Text="Asignar Roles"
                            OnClick="btnAsignar_Click" Visible="False" />
                    </li>
                </ul>
            </asp:Panel>
        </asp:Panel>
    </div>
</asp:Content>
