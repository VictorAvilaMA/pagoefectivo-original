﻿using System;
using System.Web.UI;
using SPE.EmsambladoComun;
using SPE.Entidades;
using SPE.Web.Seguridad;
using _3Dev.FW.Entidades.Seguridad;
using _3Dev.FW.Util;

public partial class SegMantUsuario : Page
{
    SPEUsuarioClient uc = new SPEUsuarioClient();
    int IdUsuario = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["IdUsuario"] != null)
            {
                IdUsuario = int.Parse(Request["IdUsuario"].ToString());
                hdnIdUsuario.Value = IdUsuario.ToString();
                if (!IsPostBack)
                {
                    BEUsuario user = uc.GetUsuarioById(IdUsuario);
                    txtEmailPrincipal.ReadOnly = true;
                    txtEmailPrincipal.CssClass = "normal";
                    txtEmailPrincipal.Text = user.Email;
                    ucUsuarioDatosComun.RefrescarVista((BEUsuarioBase)user);
                    hdnIdEstado.Value = DataUtil.IntToString(user.IdEstado);
                    ActivarSecuencia(false,true );
                    pnlInformacionRegistro.Enabled = true;
                    btnAsignar.Visible = true;
                }
            }
            else
            {
                ucUsuarioDatosComun.InicializarVista();
                lblProceso.Text = "Registrar Usuario";
                btnAsignar.Visible = false;
                ActivarSecuencia(true , true);
                Page.SetFocus(txtEmailPrincipal);
            }
        }
    }
    protected void btnAsignar_Click(object sender, EventArgs e)
    {
      Response.Redirect("SegAsigRUsuario.aspx?IdUsuario=" + hdnIdUsuario.Value);
    }
    protected void btnRegistrar_Click(object sender, EventArgs e)
    {
        int result = 0;
        try
        {
                BEUsuarioBase user = new BEUsuarioBase();
                user.Email = txtEmailPrincipal.Text;
                ucUsuarioDatosComun.CargarBEUsuario(user);
                user.IdEstado = DataUtil.StringToInt(hdnIdEstado.Value);
                user.IdEstado =(int) ParametrosSistema.EstadoUsuario.Pendiente;
                user.IdOrigenRegistro =(int) ParametrosSistema.Cliente.OrigenRegistro.RegistroCompleto;
                user.IdCiudad = Convert.ToInt32(ucUsuarioDatosComun.Ciudad.SelectedValue);
                result = uc.RegistrarUsuario(user);
              
                if (result == (int)ParametrosSistema.ExisteEmail.Existe)
                {
                    lblMensaje.Text = "Este correo está siendo usado por otro usuario.";
                    lblMensaje.CssClass = "MensajeValidacion";
                    hdnIdUsuario.Value = result.ToString();
                    pnlInformacionRegistro.Enabled = true;
                }
                else
                {
                    ActivarSecuencia(true, false);
                    lblMensaje.Text = "Los cambios se actualizaron satisfactoriamente.";
                    lblMensaje.CssClass = "MensajeTransaccion";
                    pnlInformacionRegistro.Enabled = false ;
                }
                btnAsignar.Visible = false;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message ;
            lblMensaje.CssClass = "MensajeValidacion";
        }
    }
    private void ActivarSecuencia(bool esRegistro,bool flagActivar)
    {
        btnActualizar.Visible =!esRegistro ;
        btnRegistrar.Visible = esRegistro;

        if (esRegistro)
        {
            if (flagActivar)
            {
                txtEmailPrincipal.ReadOnly = false;
                txtEmailPrincipal.CssClass = "normal";
            }
            else {
                txtEmailPrincipal.ReadOnly = true;
                txtEmailPrincipal.CssClass = "normal";
            }
            btnRegistrar.Enabled = flagActivar;
            btnCancelar.Text = "Cancelar";
            if (!btnRegistrar.Enabled)
            {
                btnCancelar.Text = "Regresar";
                btnCancelar.OnClientClick = "";
            }
        }
        else {
            txtEmailPrincipal.ReadOnly = true;
            txtEmailPrincipal.CssClass = "normal";
            btnActualizar.Enabled = flagActivar;
            btnCancelar.Text = "Regresar";
            btnCancelar.OnClientClick = "";
        }
        btnAsignar.Visible = (hdnIdUsuario.Value != "") && (hdnIdUsuario.Value != "");        
    }
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        try
        {
            BEUsuarioBase user = new BEUsuarioBase();
            user.Email = txtEmailPrincipal.Text;
            ucUsuarioDatosComun.CargarBEUsuario(user);
            user.IdEstado = DataUtil.StringToInt(hdnIdEstado.Value);
            user.IdUsuario = Convert.ToInt32(String.IsNullOrEmpty(hdnIdUsuario.Value) ? "0" : hdnIdUsuario.Value);
            uc.ModificarUsuario(user);
            pnlInformacionRegistro.Enabled = false;
            lblMensaje.Text = "Los cambios se actualizaron satisfactoriamente.";
            lblMensaje.CssClass = "MensajeTransaccion";
            ActivarSecuencia(false , false);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message ;
            lblMensaje.CssClass = "MensajeValidacion";
        }
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Response.Redirect("SegConUsuario.aspx");
    }
    

    
    
}
