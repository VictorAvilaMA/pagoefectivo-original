﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="ConsPagoDefault" %>

<%--PM--%>
<%@ Register Assembly="WebControlCaptcha" Namespace="WebControlCaptcha" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<head id="head1" runat="server">
    <meta charset="utf-8">
    <title>Pago Efectivo - Transacciones Seguras por Internet en el Perú</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if lt IE 9]>
  <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>/App_Themes/SPE/js/html5.js"></script>
<![endif]-->
    <meta name="title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="perucomsultores.pe" />
    <meta name="viewport" content="width=device-width" />
    <meta name="language" content="es" />
    <meta name="geolocation" content="Peru" />
    <meta name="robots" content="index,follow" />
    <meta property="og:title" content="Pago Efectivo - Transacciones Seguras por Internet en el Perú" />
    <meta property="og:description" content="" />
    <meta property="og:image" content='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/CIPGenerado/pago-efectivo.png" %>' />
    <link href="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile")%>/favicon.ico" rel="icon" type="image/vnd.microsoft.icon" />
    <link href="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile")%>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/css/servicio.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>'
        rel="stylesheet" type="text/css" />
    <%--<link href="css/style.css" rel="stylesheet" type="text/css"/>--%>
</head>
<body>
    <form id="Form1" runat="server">
    <!-- Begin comScore UDM code 1.1104.26 -->    
    <!-- End comScore UDM code -->
    <div class="hack-shadow">
    </div>
    <header class="cip">
        <div class="content-header">        
    	    <div class="top-head">
                <div class="content-top-head">
        	        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/grupo-comercio.png" width="97" height="16" alt="Grupo El Comercio" title="Grupo El Comercio" border="0" />
                </div>
            </div>
            <div class="head">
                <div class="content-head">
        	        <h1><img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/pago-efectivo.png" width="149" height="60" alt="Pago Efectivo - Transacciones seguras por Internet en el Perú" title="Pago Efectivo - Transacciones seguras por Internet en el Perú">&nbsp;</h1>
                    <h2>Transacciones seguras por Internet en el Perú</h2>
                        
                </div>         
            </div>
        </div>
               
    </header>
    <div id="wrapper">
        <div class="content">
            <div class="content-contact">
                <h2 class="title-seccion">
                    <span>&nbsp;</span> Consulta de Pagos de Servicios                    
                    <div class="img-logo">
                        <asp:Image ID="imgImagenLogo" Visible="false" runat="server" Height="90" Width="225" /></div>
                </h2>
                <div style="margin-left:320px;margin-top:15px;">
                    <label for="text-telefono">
                        C&oacute;digo del Alumno:
                    </label>
                    <asp:TextBox ID="txtCodUsuario" runat="server" CssClass="input-text" MaxLength="100" autocomplete="off"/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCodUsuario"
                        ErrorMessage="Ingrese el Código del Usuario.">*</asp:RequiredFieldValidator>
                    <div class="clear"></div>

                <div style="float: left; height: 15px; width: 100%;">
                    <asp:Label ID="lblmensajeContenido" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </div>
                <div style="float: left; width: 100%;">
                    <label for="text-comentario" style="float: left">
                        Ingrese la palabra que aparece en la imagen:</label>
                    </br>
                    <cc2:CaptchaControl ID="CaptchaControl1" runat="server" CaptchaMaxTimeout="3600"
                        Text="" CaptchaMinTimeout="0" />
                </div>
                <asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="mensaje_error" />
                </div>
                <div class="pt20 align-center">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-primary btn-middle mt20" Text="Buscar" />
                    <br />                    
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="navy"></asp:Label>
                    
                </div>
                <asp:UpdatePanel ID="upnlPaginas" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h2 class="title-seccion2">
                        </h2>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Código del Alumno:
                                <asp:Label ID="ltlCodigoAlumno" runat="server" Text="" CssClass="bold"></asp:Label>
                            </li>
                            <li class="t1"><span class="color">>></span> Nombre del Alumno:
                                
                                <asp:Label ID="ltlNombreAlumno"  CssClass="bold" runat="server" Text=""></asp:Label>
                                <asp:Label ID="ltlApellidosAlumno" CssClass="bold" runat="server" Text=""></asp:Label>
                                <asp:Label ID="ltlEmailUsuario" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                                <asp:Label ID="ltlClaveAPI" CssClass="bold" runat="server" Text="" Visible="false"></asp:Label>
                            </li>
                        </ul>
                        <h3>
                            Instrucciones:                            
                        </h3>
                        Haga check para seleccionar los items que desea pagar y haga click en el boton &quot;Generar
                        CIP&quot;                        
                                <br>
                                <p>
                                    <h3>
                                        <asp:Literal ID="Label3" runat="server" Text="Resultados:"></asp:Literal>
                                    </h3>
                                    <br>
                                    <asp:GridView ID="gvPaginas" runat="server" AutoGenerateColumns="False" CssClass="grilla"
                                        OnPageIndexChanging="gvPaginas_PageIndexChanging" AllowPaging="True">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAsociado" runat="server" data-monto=""/>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NroDocumento" HeaderText="N° Documento"></asp:BoundField>
                                            <asp:BoundField DataField="DescripcionOrden" HeaderText="Descripción" />
                                            <asp:BoundField DataField="Moneda" HeaderText="Moneda" />
                                            <asp:BoundField DataField="Importe" HeaderText="Importe"></asp:BoundField>
                                            <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emisión" />
                                            <asp:BoundField DataField="FechaVencimiento" HeaderText="Fecha Vencimiento" />
                                            <asp:BoundField DataField="Mora" HeaderText="Mora" />
                                            <asp:BoundField DataField="IdOrdenPago" HeaderText="CIP" />
                                            <asp:BoundField DataField="IdEstado" HeaderText="Estado" />
                                            <asp:BoundField DataField="Total" HeaderText="Total" Visible="false"/>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%--<span name="lblTotal"><%#Eval("Total") %></span>--%>
                                                    <asp:Label CssClass="total" ID="lblTotal" runat="server" Text='<%#Eval("Total") %>' />
                                                </ItemTemplate>
                                                
                                            </asp:TemplateField>
                                            
                                        </Columns>
                                        <HeaderStyle CssClass="cabecera" />
                                        <EmptyDataTemplate>
                                            No se encontraron registros.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:ScriptManager ID="ScriptManager" runat="server">
                                    </asp:ScriptManager>
                                    <h3 class="align-right">
                                        <br>
                                            <asp:label ID="ltlTotal" runat="server" Text="Total: S/."></asp:label>
                                            <asp:label ID="ltlTotalImporte" runat="server" CssClass="fs20 red" Text="0.00"></asp:label>
                                      
                                            <br>                                           
                     
                                    </h3>
                                    <div class="pt20 align-center bt">
                                        <asp:Button ID="btnGenerarCIP" runat="server" CssClass="btn btn-primary btn-middle"
                                            Text="Generar CIP" />
                                    </div>
                                    <div style="float: left; height: 15px; width: 100%;">                                                                                
                                        <asp:Label ID="lblMensajeError" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                                    <p>
                                    </p>
                                    
                                        <p>
                                        </p>
                                        <br></br>
                                    
                                </p>
                            </br>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <footer id="foot1" runat="server">
        
    	<div id="content-footer">
        	<span class="foot-logo">
            	<img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/App_Themes/SPE/img/CIPGenerado/foot-pago-efectivo.png" width="97" height="40" alt="Pago Efectivo" title="Pago Efectivo" border="0">
            </span>
        	<ul>
            	<li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Default.aspx">Inicio</a></li> 
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/personas.aspx">Personas</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Empresas.aspx">Empresas</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Contactenos.aspx">Contáctenos</a></li>
                <li class="break">|</li>
                <li><a href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase")%>/Ayuda.aspx">Ayuda</a></li>
            </ul>
            
            <a class="help-center" href="http://centraldeayuda.pagoefectivo.pe/home">
            	<span class="img"></span>
                <span class="content-help">
                	<span class="title">Central de Ayuda</span>
                	<span class="desc">¿Tienes alguna duda o consulta? estamos para ayudarte</span>
                </span>                
            </a>         
        </div>
        <div class="footer-link">
        	<span>Visite también:</span>
        	<ul>
            	<li class="first"><a target="_blank" title="Diario el Comercio" href="http://elcomercio.pe/?ref=pef">elcomercio.pe</a></li>
                <li><a target="_blank" title="Peru.com" href="http://peru.com/?ref=pef">peru.com</a></li>
                <li><a target="_blank" title="Diario Perú21" href="http://peru21.pe/?ref=pef">peru21.pe</a></li>
                <li><a target="_blank" title="Diario Gestión" href="http://gestion.pe/?ref=pef">gestion.pe</a></li>
                <li><a target="_blank" title="Depor.pe" href="http://depor.pe/?ref=pef">depor.pe</a></li>
                <li><a target="_blank" title="Diario Trome" href="http://trome.pe/?ref=pef">trome.pe</a></li>
                <li><a target="_blank" title="Publimetro" href="http://publimetro.pe/?ref=pef">publimetro.pe</a></li>
                <li><a target="_blank" title="GEC" href="http://gec.pe/?ref=pef">gec.pe</a></li>
                <li><a target="_blank" title="Clasificados.pe" href="http://clasificados.pe/?ref=pef">clasificados.pe</a></li>
                <li><a target="_blank" title="Aptitus" href="http://aptitus.pe/?ref=pef">aptitus.pe</a></li>                
                <li><a target="_blank" title="Urbania" href="http://urbania.pe/?ref=pef">urbania.pe</a></li>
                <li><a target="_blank" title="Neoauto" href="http://neoauto.pe/?ref=pef">neoauto.pe</a></li>
                <li class="first"><a target="_blank" title="GuiaGPS" href="http://guiagps.pe/?ref=pef">guiagps.pe</a></li>
                <li><a target="_blank" title="Ofertop" href="http://ofertop.pe/?ref=pef">ofertop.pe</a></li>
                <li><a target="_blank" title="iQuiero" href="http://iquiero.com/?ref=pef">iquiero.com</a></li>
                <li><a target="_blank" title="NuestroMercado" href="http://nuestromercado.pe/?ref=pef">nuestromercado.pe</a></li>
                <li><a target="_blank" title="Club de Suscriptores" href="http://clubsuscriptores.pe/?ref=pef">clubsuscriptores.pe</a></li>
                <li><a target="_blank" title="PeruRed" href="http://perured.pe/?ref=pef">perured.pe</a></li>
                <li><a target="_blank" title="Shopin" href="http://shopin.pe/?ref=pef">shopin.pe</a></li>                
            </ul>         
        </div>

        
        


        <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/jquery-1.6.1.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/slides.min.jquery.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/miniApp.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    <script src='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/js/modules.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>' type="text/javascript"></script>
    </footer>
    </form>
    <script language="JavaScript1.3" src="https://sb.scorecardresearch.com/c2/6906602/ct.js"></script>

    <script>
        $(document).ready(function () {
            var $tabla = $("#gvPaginas");

            $tabla.find('input[type="checkbox"]').click(function () {
                var suma = 0;
                $tabla.find('tr').each(function () {
                    var row = $(this);
                    if (row.find('input[type="checkbox"]').is(':checked')) {
                        var label = row.find('span.total');
                        suma += parseFloat(label.text());
                    }
                });
                $("#ltlTotalImporte").text(suma.toFixed(2));                
            });
        });
    </script>
</body>
</html>