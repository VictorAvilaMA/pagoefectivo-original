﻿Imports SPE.Web
Imports SPE.Entidades
Imports System.Net
Imports System.IO
Imports SPE.Web.Util
Imports System.Threading
Imports System.Collections.Generic
Imports System.Globalization
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports _3Dev.FW.Web.Log
Imports SPE.Utilitario
Imports SPE.Api
Imports SPE.Api.Proxys
Imports System.Xml
Imports System.Web.UI
Imports System.Web.UI.Page
Partial Class ConsPagoDefault

    Inherits System.Web.UI.Page

    Public oBEOrdenPago As New BEOrdenPago()
    Public oBESolicitudPago As New BESolicitudPago()
    ' Public oBEServicio As New BEServicio()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'btnBUscar.Visible = False
        If Not IsPostBack Then

            'datos servicio

            Dim strIdServicio As String = System.Configuration.ConfigurationManager.AppSettings("IdServicioIPAL")
            Dim CtrlServicio As New CServicio()
            Dim objCServicio As New SPE.Web.CServicio()
            Dim oBEServicio As New BEServicio()
            oBEServicio = CtrlServicio.ObtenerServicioInstitucionPorId(Convert.ToInt16(strIdServicio))

            If oBEServicio Is Nothing Then
                Response.Redirect("Default.aspx")
            End If



            'imagen servicio

            Dim bytes As Byte() = oBEServicio.LogoImagen
            Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
            imgImagenLogo.ImageUrl = "data:image/png;base64," & base64String
            imgImagenLogo.Visible = True
            upnlPaginas.Visible = False


            Me.head1.DataBind()
            foot1.DataBind()
            findControlToDataBind(Me.Controls)
            Dim CtrlOrdenPago As New COrdenPago()
            'Dim CtrlServicio As New CServicio()
            Dim RutaRaiz As String = ""

            Dim ci As New CultureInfo("Es-Es")
            Dim diaDeSemana As String = ci.DateTimeFormat.GetDayName(oBEOrdenPago.FechaVencimiento.DayOfWeek)
            Dim diaNumero As String = oBEOrdenPago.FechaVencimiento.Day.ToString().PadLeft(2, "0")
            Dim mesNombre As String = ci.DateTimeFormat.GetMonthName(oBEOrdenPago.FechaVencimiento.Month)

            Dim CtrPlantilla As New CPlantilla()
            Dim valoresDinamicos As New Dictionary(Of String, String)
            valoresDinamicos.Add("[CIP]", "23")
            valoresDinamicos.Add("[FechaVencimiento]", "23/12/2013")
            valoresDinamicos.Add("[HoraVencimiento]", "05:06:23")
            valoresDinamicos.Add("[EmailCLiente]", "pjmfb24@gmail.com")
        End If
    End Sub

    Private Sub Ocultar()
        Me.gvPaginas.DataSource = Nothing
        Me.upnlPaginas.Visible = False
    End Sub

    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCeros(ByVal valor As String) As String
        If valor.Length = 1 Then
            Return "0" + valor
        Else
            Return valor
        End If
    End Function



    Private Function CargarDatosArchivoConciliacion() As BEServicioInstitucionRequest
        Ocultar()
        Dim strIdServicio As String = System.Configuration.ConfigurationManager.AppSettings("IdServicioIPAL")
        Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
        With oBEServicioInstitucionRequest
            .IdServicio = Integer.Parse(strIdServicio)
            .CodUsuario = txtCodUsuario.Text
        End With
        Return oBEServicioInstitucionRequest

    End Function

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        Try
            If Not Page.IsValid Then
                Ocultar()
                lblMensaje.Text = ""
                Exit Sub
            End If
            lblMensajeError.Text = ""
            ltlTotal.Text = "Total: S/. "
            ltlTotalImporte.Text = "0.00"
            ConsultarDetallePagoServicioInstitucion()
        Catch ex As Exception
            'Logger.LogException(ex)
            lblMensaje.Text = New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage
            lblMensaje.CssClass = "MensajeValidacion"

        End Try
    End Sub

    Private Sub ConsultarDetallePagoServicioInstitucion()


        Me.lblMensaje.Text = ""
        Dim request As BEServicioInstitucionRequest = CargarDatosArchivoConciliacion()
        If request IsNot Nothing Then
            'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion
            Dim objCServicioInstitucion As New SPE.Web.CServicio

            Dim response As List(Of BEServicioInstitucion) = objCServicioInstitucion.ConsultarDetallePagoServicioInstitucion(request)

            If response.Count = 0 Then
                gvPaginas.Visible = False
                upnlPaginas.Visible = False
                Me.gvPaginas.DataSource = Nothing
                Me.lblMensaje.Visible = True
                lblMensaje.Text = "No se encontraron Registros."
            Else
                Me.ltlCodigoAlumno.Text = response.Item(0).CodUsuario
                Me.ltlNombreAlumno.Text = response.Item(0).NombreUsuario + " "
                Me.ltlApellidosAlumno.Text = response.Item(0).ApePatUsuario + " " + response.Item(0).ApeMatUsuario
                Me.ltlEmailUsuario.Text = response.Item(0).EmailUsuario
                Me.ltlClaveAPI.Text = response.Item(0).ClaveAPI
                Me.upnlPaginas.Visible = True
                Me.gvPaginas.Visible = True
                Me.gvPaginas.DataSource = response
                gvPaginas.DataBind()
                Me.lblMensaje.Visible = True
                'If response.Count() = 1 Then
                '    Me.lblMensaje.Text = "Resultados: 1 registro."
                'ElseIf response.Count() > 1 Then
                '    Me.lblMensaje.Text = "Resultados: " & response.Count.ToString() & " registros."
                'End If
                ActualizarCampos()
            End If
        Else
            Ocultar()
        End If
    End Sub


    Public Sub ActualizarCampos()

        For Each gvr As GridViewRow In gvPaginas.Rows
            If gvr.Cells(3).Text = SPE.EmsambladoComun.ParametrosSistema.Moneda.Tipo.soles.ToString() Then
                gvr.Cells(3).Text = "Soles"
            Else
                gvr.Cells(3).Text = "Dolares"
            End If
            gvr.Cells(5).Text = gvr.Cells(5).Text.Trim().Substring(0, 10)
            gvr.Cells(6).Text = gvr.Cells(6).Text.Trim().Substring(0, 10)

            If gvr.Cells(8).Text.Trim() <> "0" Then
                gvr.Enabled = False
            Else
                gvr.Cells(8).Text = ""
            End If

            If gvr.Cells(9).Text = "22" Then
                gvr.Cells(9).Text = "Pendiente"
            ElseIf gvr.Cells(9).Text = "23" Then
                gvr.Cells(9).Text = "Pagado"
            Else
                gvr.Cells(9).Text = ""
            End If
        Next

    End Sub


    Public Sub findControlToDataBind(ByVal controls As ControlCollection)
        For Each c As Control In controls
            If c.HasControls And c.GetType IsNot GetType(GridView) Then
                findControlToDataBind(c.Controls)
            End If
            If c.GetType() Is GetType(ImageButton) Or c.GetType() Is GetType(Button) Or c.GetType() Is GetType(Image) Or c.GetType() Is GetType(LinkButton) Then
                c.DataBind()
            End If
        Next
    End Sub



    Public Sub chkAsociado_CheckedChanged(sender As Object, e As EventArgs)
        Dim decSuma As Decimal = 0
        For Each gvr As GridViewRow In gvPaginas.Rows
            If DirectCast(gvr.FindControl("chkAsociado"), CheckBox).Checked = True Then
                decSuma = decSuma + Decimal.Parse(gvr.Cells(10).Text)
            End If
        Next
        ltlTotal.Text = "Total: S/. "
        ltlTotalImporte.Text = decSuma.ToString()

    End Sub



    Protected Sub gvPaginas_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPaginas.PageIndexChanging

    End Sub





    Protected Sub btnGenerarCIP_Click(sender As Object, e As System.EventArgs) Handles btnGenerarCIP.Click

        Dim suma As Decimal = 0
        'lblMensajeError
        If gvPaginas.Rows.Count > 0 Then
            Dim intChecks As Integer = 0
            Dim objCServicioInstitucion As New SPE.Web.CServicio
            Dim oReqDoc As New BEServicioInstitucionRequest
            oReqDoc.IdServicio = System.Configuration.ConfigurationManager.AppSettings("IdServicioIPAL")

            Dim intCantCIPNroDoc As Integer = 0
            For Each gvr As GridViewRow In gvPaginas.Rows
                If DirectCast(gvr.FindControl("chkAsociado"), CheckBox).Checked = True Then
                    intChecks += 1
                    oReqDoc.NroDocumento = gvr.Cells(1).Text
                    Dim total As Decimal = DirectCast(gvr.Cells(10).FindControl("lblTotal"), Label).Text
                    suma += total
                    intCantCIPNroDoc = objCServicioInstitucion.ConsultarCIPDocumento(oReqDoc)
                    If intCantCIPNroDoc > 0 Then
                        Exit For
                    End If
                End If
            Next
            If intChecks > 0 Then
                If intCantCIPNroDoc = 0 Then
                    Dim strTxtXML As String
                    Dim request As New SPE.Api.Proxys.BEWSGenCIPRequestMod1
                    Dim pathPublicKeyContraparte As String = System.Configuration.ConfigurationManager.AppSettings("KeyPublicPathContraparte")
                    Dim pathPrivateKey As String = System.Configuration.ConfigurationManager.AppSettings("KeyPrivatePathIPAL")
                    Dim strMailComercioIPAL As String = System.Configuration.ConfigurationManager.AppSettings("MailComercioIPAL")
                    Dim strUsuarioIdIPAL As String = System.Configuration.ConfigurationManager.AppSettings("UsuarioIdIPAL")
                    Dim strClienteIdIPAL As String = System.Configuration.ConfigurationManager.AppSettings("ClienteIdIPAL")


                    Dim strNombre As String = ltlNombreAlumno.Text
                    Dim strApellidos As String = ltlApellidosAlumno.Text
                    Dim strEmailUsuario As String = ltlEmailUsuario.Text
                    Dim strClaveAPI As String = ltlClaveAPI.Text

                    Dim strMoneda As String = "1"
                    Dim strImporte As String = suma 'ltlTotalImporte.Text
                    Dim strOrderIdComercio As String = ""

                    For Each gvr As GridViewRow In gvPaginas.Rows
                        If DirectCast(gvr.FindControl("chkAsociado"), CheckBox).Checked = True Then
                            strOrderIdComercio = Today.Year.ToString + CompletarCeros(Today.Month.ToString) + CompletarCeros(Today.Day.ToString) + CompletarCeros(Now.Hour.ToString) + CompletarCeros(Now.Minute.ToString) + CompletarCeros(Now.Second.ToString)
                        End If
                    Next


                    strTxtXML = ArmarXML(strMoneda, strImporte, strOrderIdComercio, strMailComercioIPAL,
                                         strUsuarioIdIPAL, strNombre, strApellidos, "UsuarioIPL", strEmailUsuario, strClienteIdIPAL)
                    request.CodServ = strClaveAPI
                    request.Xml = strTxtXML

                    PagoEfectivo.PrivatePath = pathPrivateKey
                    PagoEfectivo.PublicPathContraparte = pathPublicKeyContraparte

                    

                    'Dim res As SPE.Api.Proxys.BEWSSolicitarResponse = PagoEfectivo.SolicitarPago(request)
                    Dim res As SPE.Api.Proxys.BEWSGenCIPResponseMod1 = PagoEfectivo.GenerarCIPMod1(request)
                    If (res IsNot Nothing) Then
                        lblMensajeError.Visible = True
                        lblMensajeError.Text = "Su Orden de Pago está siendo generada..."
                        btnGenerarCIP.Enabled = False
                        Dim xml As New XmlDocument
                        xml.InnerXml = res.Xml
                        'para redireccionamiento
                        Dim element As XmlElement = xml.SelectSingleNode("ConfirSolPago")
                        Dim elementchild As XmlElement = element.SelectSingleNode("Token")
                        'para actualizar documento detalle con cip
                        Try
                            Dim elementchild2 As XmlElement = element.SelectSingleNode("CIP")
                            Dim elementchildchild2 As XmlElement = elementchild2.SelectSingleNode("IdOrdenPago")
                            Dim intCIP As Integer = Convert.ToInt32(elementchildchild2.InnerText)

                            'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion


                            Dim IdError As Integer
                            For Each gvr As GridViewRow In gvPaginas.Rows
                                If DirectCast(gvr.FindControl("chkAsociado"), CheckBox).Checked = True Then
                                    Dim req As New BEServicioInstitucionRequest
                                    req.NroDocumento = gvr.Cells(1).Text.Trim
                                    req.IdOrdenPago = intCIP
                                    IdError = objCServicioInstitucion.ActualizarDocumentoInstitucion(req)
                                End If
                            Next
                        Catch ex As Exception
                            Response.Redirect("Default.aspx")
                        End Try

                        Dim UrlRedirect As String = System.Configuration.ConfigurationManager.AppSettings("urlGenPago") & "?Token=" & elementchild.InnerText
                        Response.Redirect(UrlRedirect)

                    Else
                        lblMensajeError.Visible = True
                        lblMensajeError.Text = "No se obtuvo response (servicio del ws esta abajo)..."

                    End If
                Else
                    lblMensajeError.Visible = True
                    lblMensajeError.Text = "Alguno de los registros seleccionados ya tiene asociada una Orden de Pago en Pago Efectivo... Vuelva a cargar el formulario..."
                End If
            Else
                lblMensajeError.Visible = True
                lblMensajeError.Text = "Debe seleccionar un registro para generar la Orden de Pago..."
            End If
        Else
            lblMensajeError.Visible = True
            lblMensajeError.Text = "No se registran pagos pendientes..."
        End If

    End Sub

    Public Function ArmarXML2(ByVal strMoneda As String, ByVal strImporte As String, ByVal strOrdenIdComercio As String, ByVal strUrlOk As String, ByVal strUrlError As String,
                             ByVal strMailComercio As String, ByVal strFechaExpirar As String, ByVal strUsuarioId As String, ByVal strNombre As String, ByVal strApellidos As String,
                             ByVal strUsuarioAlias As String, ByVal strUsuarioEmail As String) As String
        Dim strXml As String
        strXml = "<?xml version='1.0' encoding='utf-8' ?>" & _
      "<OrdenPago>" & _
      "<IdMoneda>" & strMoneda & "</IdMoneda>" & _
      "<Total>" & strImporte & "</Total>" & _
      "<MerchantId>IPL</MerchantId>" & _
      "<OrdenIdComercio>" & strOrdenIdComercio & "</OrdenIdComercio>" & _
      "<UrlOk>" & strUrlOk & "</UrlOk>" & _
      "<UrlError>" & strUrlError & "</UrlError>" & _
      "<MailComercio>" & strMailComercio & "</MailComercio>" & _
      "<FechaAExpirar>" & strFechaExpirar & "</FechaAExpirar>" & _
      "<UsuarioId>" & strUsuarioId & "</UsuarioId>" & _
      "<DataAdicional></DataAdicional>" & _
      "<UsuarioNombre>" & strNombre & "</UsuarioNombre>" & _
      "<UsuarioApellidos>" & strApellidos & "</UsuarioApellidos>" & _
      "<UsuarioLocalidad>LIMA</UsuarioLocalidad>" & _
      "<UsuarioProvincia>LIMA</UsuarioProvincia>" & _
      "<UsuarioPais>PERU</UsuarioPais>" & _
      "<UsuarioAlias>" & strUsuarioAlias & "</UsuarioAlias>" & _
      "<UsuarioEmail>" & strUsuarioEmail & "</UsuarioEmail>" & _
      "<Detalles>" & _
      "  <Detalle>" & _
      "    <Cod_Origen>CT</Cod_Origen>" & _
      "    <TipoOrigen>TO</TipoOrigen>" & _
      "    <ConceptoPago>Transacción IPAL</ConceptoPago>" & _
      "    <Importe>" & strImporte & "</Importe>" & _
      "    <Campo1></Campo1>" & _
      "    <Campo2></Campo2>" & _
      "    <Campo3></Campo3>" & _
      "  </Detalle>" & _
      "</Detalles>" & _
    "</OrdenPago>"
        Return strXml
    End Function







    Public Function ArmarXML(ByVal strMoneda As String, ByVal strImporte As String, ByVal strOrdenIdComercio As String, ByVal strMailComercio As String,
                             ByVal strUsuarioId As String, ByVal strNombre As String, ByVal strApellidos As String,
                             ByVal strUsuarioAlias As String, ByVal strUsuarioEmail As String, ByVal strClienteIdIPAL As String) As String
        Dim strXml As String
        Dim dtExpirar As DateTime = Now.AddMonths(1)
        strXml = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & _
                "<SolPago>" & _
                "<IdMoneda>1</IdMoneda>" & _
                "<Total>" & strImporte & "</Total>" & _
                "<MetodosPago>1,2</MetodosPago>" & _
                "<CodServicio>IPL</CodServicio>" & _
                "<Codtransaccion>" & strOrdenIdComercio & "</Codtransaccion>" & _
                "<EmailComercio>" & strMailComercio & "</EmailComercio>" & _
                "<FechaAExpirar>" & Format(dtExpirar, "dd/MM/yyyy") & " " & CompletarCeros(dtExpirar.Hour.ToString()) & ":" & CompletarCeros(dtExpirar.Minute.ToString()) & ":" & CompletarCeros(dtExpirar.Second.ToString()) & "</FechaAExpirar>" & _
                "<UsuarioId>" & strUsuarioId & "</UsuarioId>" & _
                "<DataAdicional></DataAdicional>" & _
                "<UsuarioNombre>" & strNombre & "</UsuarioNombre>" & _
                "<UsuarioApellidos>" & strApellidos & "</UsuarioApellidos>" & _
                "<UsuarioLocalidad>LIMA</UsuarioLocalidad>" & _
                "<UsuarioProvincia>LIMA</UsuarioProvincia>" & _
                "<UsuarioPais>PERU</UsuarioPais>" & _
                "<UsuarioAlias>" & strUsuarioAlias & "</UsuarioAlias>" & _
                "<UsuarioTipoDoc>1</UsuarioTipoDoc>" & _
                "<UsuarioNumeroDoc>1</UsuarioNumeroDoc>" & _
                "<UsuarioEmail>" & strUsuarioEmail & "</UsuarioEmail>" & _
                "<ConceptoPago>" & System.Configuration.ConfigurationManager.AppSettings("CPAvansys") & "</ConceptoPago>" & _
                "<Detalles>" & _
                "<Detalle>" & _
                "<Cod_Origen>CT</Cod_Origen>" & _
                "<TipoOrigen>TO</TipoOrigen>" & _
                "<ConceptoPago>Transaccion Comision</ConceptoPago>" & _
                "<Importe>" & strImporte & "</Importe>" & _
                "<Campo1></Campo1>" & _
                "<Campo2></Campo2>" & _
                "<Campo3></Campo3>" & _
                "</Detalle>" & _
                "</Detalles>" & _
                "<ParamsURL>" & _
                "<ParamURL>" & _
                "<Nombre>IDCliente</Nombre>" & _
                "<Valor>" & strClienteIdIPAL & "</Valor>" & _
                "</ParamURL>" & _
                "<ParamURL>" & _
                "<Nombre>FechaHoraRegistro</Nombre>" & _
                "<Valor>" & Format(Now, "dd/MM/yyyy") & "</Valor>" & _
                "</ParamURL>" & _
                "</ParamsURL>" & _
                "<ParamsEmail>" & _
                "<ParamEmail>" & _
                "<Nombre>[UsuarioNombre]</Nombre>" & _
                "<Valor>" & strNombre & "</Valor>" & _
                "</ParamEmail>" & _
                "<ParamEmail>" & _
                "<Nombre>[Moneda]</Nombre>" & _
                "<Valor>S/.</Valor>" & _
                "</ParamEmail>" & _
                "</ParamsEmail>" & _
                "</SolPago>"

        Return strXml
    End Function


    Protected Sub gvResultado_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPaginas.PageIndexChanging
        gvPaginas.PageIndex = e.NewPageIndex
        ConsultarDetallePagoServicioInstitucion()
    End Sub
End Class



