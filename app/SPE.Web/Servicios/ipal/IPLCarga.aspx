﻿
<%@ Page Language="VB" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="IPLCarga.aspx.vb" Inherits="IPLCarga"
    Title="PagoEfectivo - Carga IPAL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" AsyncPostBackTimeOut= "3600">
    </asp:ScriptManager>
    <h2>
        Cargar Ordenes Pendientes
    </h2>
    <div class="conten_pasos3">
        <div method="post" action="">
            <fieldset>
                <h4>
                    1. Realizar Carga</h4>
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span> Servicio :</li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                                </asp:DropDownList>
                            </li>
                            <li class="t1"><span class="color">&gt;&gt;</span> Archivo de Carga :</li>
                            <li class="t2">
                                <asp:FileUpload ID="fulArchivoConciliacion" runat="server"></asp:FileUpload>
                            </li>
                            <br/><br/>
                            <asp:Label ID="lblMensaje" runat="server"></asp:Label><br/></br>
                            <asp:Label ID="lblMensajeCabArc" runat="server" ></asp:Label></br>
                            <asp:Label ID="lblMensajeCabProc" runat="server" ></asp:Label></br>
                            <asp:Label ID="lblMensajeCabFail" runat="server" ></asp:Label></br>
                            <asp:Label ID="lblMensajeDetArc" runat="server" ></asp:Label></br>
                            <asp:Label ID="lblMensajeDetProc" runat="server" ></asp:Label></br>
                            <asp:Label ID="lblMensajeDetFail" runat="server"></asp:Label></br></br>
                            <asp:label ID="lblMensajeCadena" runat="server"></asp:label>                            
                            <asp:Panel ID="pnlFileUpLoad" runat="server" Visible="false">
                                <asp:Label ID="lblUploadFile" CssClass="MensajeValidacion" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </asp:Panel>                            
                        </ul>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="clear:both;"></div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnProcesar" runat="server" CssClass="input_azul3" Text="Procesar"
                            ValidationGroup="ValidaCampos" />
                        <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>                    
                </ul>
            </fieldset>
        </div>
        <div>
            <asp:Panel ID="pnlConciliadas" runat="server" CssClass="divContenedor" Visible="False">
                <%--<asp:Label ID="lblOperacionesConciliadas" runat="server" Text="Operaciones conciliadas" CssClass="divContenedorTitulo"></asp:Label>--%>
                <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" 
                    AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="Banco" HeaderText="Banco" />
                        <asp:BoundField DataField="CIP" HeaderText="CIP">
                        </asp:BoundField>
                        <asp:BoundField DataField="Estado" HeaderText="Estado" />
                        <asp:BoundField DataField="Servicio" HeaderText="Servicio" />
                        <asp:BoundField DataField="ServicioDes" HeaderText="Descipción Servicio" 
                            Visible="False" />
                        <asp:BoundField DataField="Moneda" HeaderText="Moneda" />
                        <asp:BoundField DataField="Total" HeaderText="Total" />
                        <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Creación" />
                        <asp:BoundField DataField="FechaCancelacion" HeaderText="Fecha Cancelación" />
                        <asp:BoundField DataField="FechaExpiracion" HeaderText="Fecha Expiración" />
                        <asp:BoundField DataField="FechaAnulacion" HeaderText="Fecha Anulación" />
                        <asp:BoundField DataField="OrderIdComercio" HeaderText="Orden Id Comercio">
                        </asp:BoundField>
                        <asp:BoundField DataField="Observacion" HeaderText="Observación" />
                        <asp:BoundField DataField="NombreArchivo" HeaderText="Nombre Archivo" />
                    </Columns>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
