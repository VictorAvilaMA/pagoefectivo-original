﻿<%@ Page Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="IPLDescarga.aspx.vb" Inherits="IPLDescarga" Title="PagoEfectivo - Conciliación Bancaria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server" AsyncPostBackTimeOut= "3600">
    </asp:ScriptManager>
    <h2>
        Descargar 
        de órdenes Canceladas</h2>
    <div class="conten_pasos3">
        <div method="post" action="">
            <fieldset>
                <h4>
                    1. Realizar Descarga</h4>
                <asp:UpdatePanel ID="upnlCriterios" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">&gt;&gt;</span> Servicio :</li>
                            <li class="t2">
                                <asp:DropDownList ID="ddlServicio" runat="server" CssClass="neu">
                                </asp:DropDownList>
                            </li>
                            
                            
                            
                            <li class="t1"><span class="color">&gt;&gt; </span>Fecha del: </li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaConciliacion" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDesde" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDesde" runat="server" TargetControlID="txtFechaConciliacion"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaConciliacion2" runat="server" CssClass="corta" MaxLength="10"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaHasta" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderHasta" runat="server" TargetControlID="txtFechaConciliacion2"
                                Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <cc1:CalendarExtender ID="CalendarExtenderDesde" runat="server" TargetControlID="txtFechaConciliacion"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnFechaDesde">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderHasta" runat="server" TargetControlID="txtFechaConciliacion2"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnFechaHasta">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDesde" runat="server" ControlExtender="MaskedEditExtenderDesde"
                            ControlToValidate="txtFechaConciliacion" ErrorMessage="" InvalidValueMessage=""
                            IsValidEmpty="False" EmptyValueMessage="" Display="Dynamic"
                            EmptyValueBlurredText="" InvalidValueBlurredMessage=""></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorHasta" runat="server" ControlExtender="MaskedEditExtenderHasta"
                            ControlToValidate="txtFechaConciliacion2" ErrorMessage="" InvalidValueMessage=""
                            IsValidEmpty="False" EmptyValueMessage="" Display="Dynamic"
                            EmptyValueBlurredText="" InvalidValueBlurredMessage=""></cc1:MaskedEditValidator>                        
                    </li>                    
                            <asp:Panel ID="pnlFileUpLoad" runat="server" Visible="false">
                                <asp:Label ID="lblUploadFile" CssClass="MensajeValidacion" runat="server" Text=""
                                    ForeColor="Red"></asp:Label>
                            </asp:Panel>
                        </ul>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="clear: both;">
                </div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul3" Text="Generar"
                            ValidationGroup="ValidaCampos" />
                        <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                    </li>
                    
                            <asp:Label ID="lblMensaje" runat="server" CssClass="MensajeTransaccion"></asp:Label>
                    

                </ul>
            </fieldset>
        </div>
        <div>
            <asp:Panel ID="pnlConciliadas" runat="server" CssClass="divContenedor" Visible="False">
                <%--<asp:Label ID="lblOperacionesConciliadas" runat="server" Text="Operaciones conciliadas" CssClass="divContenedorTitulo"></asp:Label>--%>
                <asp:GridView ID="gvResultado" runat="server" CssClass="grilla" AutoGenerateColumns="False"
                    AllowSorting="True" OnRowCommand="gvResultado_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="IdServicio" HeaderText="IdServicio" 
                            Visible="False" />
                        <asp:BoundField DataField="MerchantId" HeaderText="MerchantID"></asp:BoundField>
                        <asp:BoundField DataField="NombreServicio" HeaderText="Servicio" />
                        <asp:BoundField DataField="FechaCarga" HeaderText="Fecha Cancelación" />
                        <asp:BoundField DataField="Tamano" HeaderText="Tamaño" />
                        <asp:BoundField DataField="NombreArchivoTXT" HeaderText="NombreArchivoTXT" />
                        <asp:BoundField DataField="NombreArchivoXLS" HeaderText="NombreArchivoXLS" Visible="False"/>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Descargar en txt">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDownload" runat="server" CommandArgument='<%# Eval("Orden") %>' CommandName="cmd"><img class="lnkDownload"  src="<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/filetxt.gif" %>" alt="Descargar"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:ImageField HeaderText="Descargar en xls" Visible="False">
                        </asp:ImageField>
                        <asp:BoundField DataField="Orden" HeaderText="Orden" Visible="False"/>
                    </Columns>
                    <HeaderStyle CssClass="cabecera"></HeaderStyle>
                </asp:GridView>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
