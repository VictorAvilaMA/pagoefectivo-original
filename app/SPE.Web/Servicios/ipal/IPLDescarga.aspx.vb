﻿Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class IPLDescarga
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub

    Private Sub CargarCombos()
        Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
        oBEServicioInstitucionRequest.GCParametro = SPE.EmsambladoComun.ParametrosSistema.GrupoClasificacionServicio
        oBEServicioInstitucionRequest.DesParametro = "Institucional"
        Dim objCServicioInstitucion As New SPE.Web.CServicio
        Dim IdParametro As Integer = objCServicioInstitucion.DeterminarIdParametro(oBEServicioInstitucionRequest)


        Dim objCServicio As New SPE.Web.CServicio
        Dim oBEServicio As New BEServicio
        With oBEServicio
            .Clasificacion = IdParametro
        End With
        WebUtil.DropDownlistBinding(ddlServicio, objCServicio.ConsultarServicioPorClasificacion(oBEServicio), "Nombre", "IdServicio", "::: Seleccione :::")
    End Sub

    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub

    Private Function ValidaDatos() As Boolean
        If Not (ddlServicio.SelectedIndex > 0 And ddlServicio.SelectedItem.Text <> "" And ddlServicio.SelectedItem.Text <> "::: Seleccione :::") Then
            MostrarValidacionFileUpload(True, "Debe seleccionar un servicio.")
            ddlServicio.Focus()
            Return False
        End If

        If Not (txtFechaConciliacion.Text <> "") Then
            MostrarValidacionFileUpload(True, "Debe Ingresar una Fecha de Carga Inicial.")
            txtFechaConciliacion.Focus()
            Return False
        End If

        If Not (txtFechaConciliacion2.Text <> "") Then
            MostrarValidacionFileUpload(True, "Debe Ingresar una Fecha de Carga Final.")
            txtFechaConciliacion2.Focus()
            Return False
        End If

        Return True
    End Function


    Private Function CargarDatos() As BEServicioInstitucionRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
            With oBEServicioInstitucionRequest
                .IdServicio = Integer.Parse(ddlServicio.SelectedValue.ToString())
                .FechaCargaInicial = Convert.ToDateTime(txtFechaConciliacion.Text)
                .FechaCargaFinal = Convert.ToDateTime(txtFechaConciliacion2.Text)
            End With
            Return oBEServicioInstitucionRequest
        Else
            Return Nothing
        End If
    End Function


    Private Function CargarDatosDetalle(ByVal strMerchantId As String, ByVal dtFechaCarga As DateTime) As BEServicioInstitucionRequest

        Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
        With oBEServicioInstitucionRequest
            .MerchantID = strMerchantId
            .FechaCargaInicial = dtFechaCarga
        End With
        Return oBEServicioInstitucionRequest

    End Function


    Protected Sub BtnLimpiar_Click(sender As Object, e As System.EventArgs) Handles BtnLimpiar.Click
        ddlServicio.SelectedIndex = 0
        lblMensaje.Text = ""
    End Sub

    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCeros(ByVal valor As String) As String
        If valor.Length = 1 Then
            Return "0" + valor
        Else
            Return valor
        End If
    End Function


    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCerosCIP(ByVal valor As String) As String
        If valor.Length = 7 Then
            Return "0000000" + valor
        Else
            Return valor
        End If
    End Function

    Public Function FormarLineaEscritura(ByVal linea As BEServicioInstitucion) As String
        Dim strRetorno As String = ""
        strRetorno = linea.MerchantID + "|" +
                     linea.CodUsuario + "|" +
                     linea.CodUsuarioComp + "|" +
                     linea.IdOrden.ToString() + "|" +
                     linea.NroDocumento + "|" +
                     linea.DescripcionOrden + "|" +
                     linea.Moneda.ToString() + "|" +
                     linea.Importe.ToString() + "|" +
                     CompletarCeros(linea.FechaEmision.Day.ToString) + "-" + CompletarCeros(linea.FechaEmision.Month.ToString) + "-" + linea.FechaEmision.Year.ToString + "|" +
                     CompletarCeros(linea.FechaVencimiento.Day.ToString) + "-" + CompletarCeros(linea.FechaVencimiento.Month.ToString) + "-" + linea.FechaVencimiento.Year.ToString + "|" +
                     linea.Mora.ToString() + "|" +
                     linea.Moneda.ToString() + "|" +
                     linea.Total.ToString + "|" +
                     CompletarCeros(linea.FechaCancelacion.Day.ToString) + "-" + CompletarCeros(linea.FechaCancelacion.Month.ToString) + "-" + linea.FechaCancelacion.Year.ToString + "|" +
                     CompletarCerosCIP(linea.NumeroOrdenPago)
        Return strRetorno

    End Function


    Protected Sub gvResultado_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If (e.CommandName = "cmd") Then
            Dim ind As Integer = Convert.ToInt32(e.CommandArgument) - 1
            Dim row As GridViewRow = gvResultado.Rows(ind)
            Dim strMerchantId = row.Cells(1).Text
            Dim strFechaCarga = row.Cells(3).Text
            Dim dtFechaCarga = Convert.ToDateTime(strFechaCarga)
            Dim strRutaDescarga = System.Configuration.ConfigurationManager.AppSettings("ArchivosDetalleServicio")

            Dim request As BEServicioInstitucionRequest = CargarDatosDetalle(strMerchantId, dtFechaCarga)
            'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion
            Dim objCServicioInstitucion As New SPE.Web.CServicio
            Dim ListaServicioInstitucion As List(Of BEServicioInstitucion) = objCServicioInstitucion.ConsultarDetalleArchivoDescarga(request)


            Dim strFileName As String = System.IO.Path.GetRandomFileName()
            Dim strFriendlyName As String = strMerchantId + dtFechaCarga.Year.ToString + CompletarCeros(dtFechaCarga.Month.ToString) + CompletarCeros(dtFechaCarga.Day.ToString) + ".txt"

            Using sw As New System.IO.StreamWriter(Server.MapPath("TextFiles/" + strFileName + ".txt"))
                For Each lineServ As BEServicioInstitucion In ListaServicioInstitucion
                    sw.WriteLine(FormarLineaEscritura(lineServ))
                Next
                sw.Close()
            End Using

            Dim fs As System.IO.FileStream = Nothing


            fs = System.IO.File.Open(Server.MapPath("TextFiles/" + strFileName + ".txt"), System.IO.FileMode.Open)
            Dim btFile(fs.Length) As Byte
            fs.Read(btFile, 0, fs.Length)
            fs.Close()
            With Response
                .AddHeader("Content-disposition", "attachment;filename=" & strFriendlyName)
                .ContentType = "application/octet-stream"
                .BinaryWrite(btFile)
                .End()
            End With




        End If
    End Sub

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub

    Private Sub ConsultaArchivosFecha()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False
        Me.lblMensaje.Text = ""
        Dim request As BEServicioInstitucionRequest = CargarDatos()
        If request IsNot Nothing Then
            'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion
            Dim objCServicioInstitucion As New SPE.Web.CServicio

            Dim response As List(Of BEServicioInstitucion) = objCServicioInstitucion.ConsultarArchivosDescarga(request)

            If response.Count = 0 Then
                gvResultado.Visible = False
                Me.gvResultado.DataSource = Nothing
                lblMensaje.Visible = True
                lblMensaje.Text = "No se encontraron Registros."
            Else
                Me.pnlConciliadas.Visible = True
                Me.gvResultado.Visible = True
                Me.gvResultado.DataSource = response
                lblMensaje.Visible = True
                gvResultado.DataBind()
                ActualizarCampos()
                If response.Count() = 1 Then
                    Me.lblMensaje.Text = "Resultado: 1"
                ElseIf response.Count() > 1 Then
                    Me.lblMensaje.Text = "Resultados: " & response.Count.ToString()
                End If
            End If
        Else
            Ocultar()
        End If

    End Sub

    Public Sub ActualizarCampos()

        For Each gvr As GridViewRow In gvResultado.Rows
            gvr.Cells(3).Text = gvr.Cells(3).Text.Trim().Substring(0, 10)
            If (Integer.Parse(gvr.Cells(4).Text.Trim()) \ 7 = 0) Then
                gvr.Cells(4).Text = "1 Kb"
            Else
                gvr.Cells(4).Text = Convert.ToString(Integer.Parse(gvr.Cells(4).Text.Trim()) \ 7) + " Kb"
            End If



        Next

    End Sub

    Protected Sub btnRegistrar_Click(sender As Object, e As System.EventArgs) Handles btnRegistrar.Click
        ConsultaArchivosFecha()
    End Sub



End Class



