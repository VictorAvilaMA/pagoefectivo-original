﻿


Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Web.Log


Partial Class IPLGeneral
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub

    Private Sub CargarCombos()
        Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
        oBEServicioInstitucionRequest.GCParametro = SPE.EmsambladoComun.ParametrosSistema.GrupoClasificacionServicio
        oBEServicioInstitucionRequest.DesParametro = "Institucional"
        Dim objCServicioInstitucion As New SPE.Web.CServicio
        Dim IdParametro As Integer = objCServicioInstitucion.DeterminarIdParametro(oBEServicioInstitucionRequest)


        Dim objCServicio As New SPE.Web.CServicio
        Dim oBEServicio As New BEServicio
        With oBEServicio
            .Clasificacion = IdParametro
        End With
        WebUtil.DropDownlistBinding(ddlServicio, objCServicio.ConsultarServicioPorClasificacion(oBEServicio), "Nombre", "IdServicio", "::: Seleccione :::")
    End Sub


    Protected Sub btnProcesar_Click(sender As Object, e As System.EventArgs) Handles btnProcesar.Click
        ProcesarArchivo()
    End Sub



    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub


    Private Function CargarDatos() As BEServicioInstitucionRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEServicioInstitucionRequest As New BEServicioInstitucionRequest
            With oBEServicioInstitucionRequest
                .IdServicio = Integer.Parse(ddlServicio.SelectedValue.ToString())
                .NombreArchivo = fulArchivoConciliacion.FileName
                .Bytes = fulArchivoConciliacion.FileBytes
            End With
            Return oBEServicioInstitucionRequest
        Else
            Return Nothing
        End If
    End Function


    Private Function CargarDatosNumeroCargaDiaria() As BEServicioInstitucionRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEServicioInstitucion As New BEServicioInstitucionRequest
            With oBEServicioInstitucion
                .Bytes = fulArchivoConciliacion.FileBytes
            End With
            Return oBEServicioInstitucion
        Else
            Return Nothing
        End If
    End Function


    Private Function ValidarFormato() As Boolean
        If fulArchivoConciliacion.FileName.ToLower().Contains(".txt") Then
            Return True
        End If
        Return False
    End Function

    Private Sub ProcesarArchivo()
        Dim idError As Integer
        Dim oBEServicioInstitucion As New BEServicioInstitucion
        Try
            lblMensaje.Text = ""
            lblMensajeCabArc.Text = ""
            lblMensajeCabProc.Text = ""
            lblMensajeCabFail.Text = ""
            lblMensajeDetArc.Text = ""
            lblMensajeDetProc.Text = ""
            lblMensajeDetFail.Text = ""
            MostrarValidacionFileUpload(False, "")
            Me.lblMensaje.Visible = False
            Me.lblMensaje.Text = ""
            If ValidarFormato() Then
                Dim requestCantidadCarga As BEServicioInstitucionRequest = CargarDatosNumeroCargaDiaria()
                Dim request As BEServicioInstitucionRequest = CargarDatos()
                If requestCantidadCarga IsNot Nothing Then
                    'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion
                    Dim objCServicioInstitucion As New SPE.Web.CServicio
                    Dim responseCantidadCarga As Integer = objCServicioInstitucion.ValidarCarga(requestCantidadCarga)
                    'Se actualiza la cantidad de cargas por dia del respectivo servicio
                    request.NumeroCargaDiaria = responseCantidadCarga
                    If responseCantidadCarga = 0 Then
                        request.NumeroCargaDiaria = 1
                        oBEServicioInstitucion = objCServicioInstitucion.ProcesarArchivoServicioInstitucion(request)
                    Else
                        idError = objCServicioInstitucion.ActualizarDocumentosCIP(request)
                        request.NumeroCargaDiaria = responseCantidadCarga + 1
                        oBEServicioInstitucion = objCServicioInstitucion.ProcesarArchivoServicioInstitucion(request)
                    End If
                    If oBEServicioInstitucion.IdError = 0 Then
                        lblMensaje.Text = "El archivo se ha procesado correctamente."
                        lblMensajeCabArc.Text = oBEServicioInstitucion.CantCabArchivo.ToString + " registros de cabecera en el archivo"
                        lblMensajeCabProc.Text = oBEServicioInstitucion.CantCabProc.ToString + " registros de cabecera procesados"
                        lblMensajeCabFail.Text = (oBEServicioInstitucion.CantCabArchivo - oBEServicioInstitucion.CantCabProc).ToString + " registros de cabecera no procesados"
                        lblMensajeDetArc.Text = oBEServicioInstitucion.CantDetArchivo.ToString + " registros de detalle en el archivo"
                        lblMensajeDetProc.Text = oBEServicioInstitucion.CantDetProc.ToString + " registros de detalle procesados"
                        lblMensajeDetFail.Text = (oBEServicioInstitucion.CantDetArchivo - oBEServicioInstitucion.CantDetProc).ToString + " registros de detalle no procesados"
                        If oBEServicioInstitucion.CadenaErrores <> "" Then
                            lblMensajeCadena.Text = "Se tienen errores de formato en las lineas </br></br><ul>" + oBEServicioInstitucion.CadenaErrores.ToString + "</ul>"
                        End If
                    Else
                        lblMensaje.Text = "El formato del archivo no es correcto."
                    End If
                Else
                    lblMensaje.Text = "No se cargó ninguno de los registros enviados en el archivo."
                End If
            Else
                lblMensaje.Text = "Debe cargar un documento de tipo ""txt""..."
            End If
            Me.lblMensaje.Visible = True

        Catch ex As Exception
            'Logger.LogException(ex)
        End Try
    End Sub

    Private Function ValidaDatos() As Boolean
        If Not (ddlServicio.SelectedIndex > 0 And ddlServicio.SelectedItem.Text <> "" And ddlServicio.SelectedItem.Text <> "::: Seleccione :::") Then
            MostrarValidacionFileUpload(True, "Debe seleccionar un servicio.")
            ddlServicio.Focus()
            Return False
        End If
        If Not fulArchivoConciliacion.HasFile Then
            MostrarValidacionFileUpload(True, "No se ha seleccionado ningún archivo.")
            fulArchivoConciliacion.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub


    Protected Sub BtnLimpiar_Click(sender As Object, e As System.EventArgs) Handles BtnLimpiar.Click
        Ocultar()
        ddlServicio.SelectedIndex = 0
        lblMensaje.Text = ""
    End Sub
End Class
