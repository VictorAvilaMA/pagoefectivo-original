<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COMoCuBa.aspx.vb" Inherits="ADM_COMoCuBa" Title="PagoEfectivo - Consultar Empresa Contratante" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #Error div ul li
        {
            color: Red !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Movimiento de Cuentas</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Banco:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlBanco" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Moneda: </li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu">
                        </asp:DropDownList>
                    </li>
                </ul>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha de Movimiento del: </li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                                MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaFin" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                                MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                        </p>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaInicio"
                            PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechaFin"
                            PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" InvalidValueBlurredMessage="*"
                            EmptyValueBlurredText="*" Display="Dynamic" EmptyValueMessage="Fecha 'Del' es requerida"
                            IsValidEmpty="False" InvalidValueMessage="Fecha Desde no v�lida" ErrorMessage="*"
                            ControlToValidate="txtFechaInicio" ControlExtender="MaskedEditExtenderDe"></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" InvalidValueBlurredMessage="*"
                            EmptyValueBlurredText="*" Display="Dynamic" EmptyValueMessage="Fecha 'Al' es requerida"
                            IsValidEmpty="False" InvalidValueMessage="Fecha Hasta no v�lida" ErrorMessage="*"
                            ControlToValidate="txtFechaFin" ControlExtender="MaskedEditExtenderA"></cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                            ControlToValidate="txtFechaInicio" Type="Date" Operator="LessThanEqual" ControlToCompare="txtFechaFin">*</asp:CompareValidator>
                    </li>
                    </li>
                </ul>
                <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                    PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                    PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                </cc1:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator" runat="server" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                    ControlToValidate="txtFechaInicio" Type="Date" Operator="LessThanEqual" ControlToCompare="txtFechaFin">*</asp:CompareValidator>
                <div style="clear: both;">
                </div>
                <ul class="datos_cip2">
                    <li class="complet">
                        <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                        <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                        <asp:Button ID="btnRecalcular" runat="server" CssClass="input_azul4" Text="Recalcular" />
                    </li>
                </ul>
                <div id="Error">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both">
        </div>
        <div class="result">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divResult" runat="server" visible="false">
                        <h5>
                            <asp:Label ID="lblMsgNroRegistros" runat="server" Text=""></asp:Label>
                        </h5>
                        <div id="divlblmensaje" runat="server">
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <div id="divgrilla" runat="server">
                            <asp:GridView ID="gvMovimientos" runat="server" AllowPaging="True" AllowSorting="true"
                                AutoGenerateColumns="False" CssClass="grilla" DataKeyNames="IdMovimientoCuentaBanco"
                                OnRowDataBound="gvEmpresa_RowDataBound" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Sel.">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgActualizar" runat="server" CommandName="Edit" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>'
                                                PostBackUrl="DeMoCuBa.aspx" ToolTip="Ver Detalle" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "IdMovimientoCuentaBanco")%>'  />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NumeroOperacion" HeaderText="Nro. Orden" SortExpression="NumeroOperacion" />
                                    <asp:BoundField DataField="TipoMovimientoDescripcion" HeaderText="Tipo de Pago" SortExpression="TipoMovimientoDescripcion" />
                                    <asp:BoundField DataField="FechaMovimiento" HeaderText="Fec. de Operacion" SortExpression="FechaMovimiento" />
                                    <asp:BoundField DataField="DescripcionBanco" HeaderText="Banco" SortExpression="DescripcionBanco" />
                                    <asp:BoundField DataField="ArchivoDescripcion" HeaderText="Archivo o Lote" SortExpression="ArchivoDescripcion">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Monto" HeaderText="Importe" SortExpression="Monto">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="cabecera" />
                                <EmptyDataTemplate>
                                    No se encontraron registros.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
