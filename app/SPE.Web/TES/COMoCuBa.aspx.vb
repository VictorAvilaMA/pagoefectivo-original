Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports SPE.Utilitario
Imports SPE.Entidades
Imports SPE.Web
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class ADM_COMoCuBa

    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CCuenta)

#Region "atributos"
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property

    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvMovimientos
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager As Boolean
        Get
            Return True
        End Get
    End Property
#End Region

#Region "m�todos base"
    Public Overrides Sub OnClear()
        MyBase.OnClear()
        ddlMoneda.SelectedIndex = 0
        ddlBanco.SelectedIndex = 0
        gvMovimientos.DataSource = Nothing
        gvMovimientos.DataBind()
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        divResult.Visible = False
    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        MyBase.ConfigureMaintenanceSearch(e)
    End Sub
    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        divResult.Visible = True
        divgrilla.Visible = True
        Dim obeMovimientoCuentaBanco As New SPE.Entidades.BEMovimientoCuentaBanco
        obeMovimientoCuentaBanco.IdBanco = IIf(ddlBanco.SelectedIndex = 0, 0, ddlBanco.SelectedValue)
        obeMovimientoCuentaBanco.IdMoneda = IIf(ddlMoneda.SelectedIndex = 0, 0, ddlMoneda.SelectedValue)
        obeMovimientoCuentaBanco.FechaMovimientoDesde = txtFechaInicio.Text
        obeMovimientoCuentaBanco.FechaMovimientoHasta = txtFechaFin.Text
        obeMovimientoCuentaBanco.PageNumber = 1
        obeMovimientoCuentaBanco.PageSize = 10
        Return obeMovimientoCuentaBanco
    End Function

    Public Overrides Function GetMethodSearchByParameters(be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Dim listaMovimientos As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) = CType(ControllerObject, SPE.Web.CCuenta).ConsultarMovimientoCuenta(be)
        ViewState.Add("listaMovimientos", listaMovimientos)
        Return listaMovimientos
    End Function

    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As System.Web.UI.WebControls.GridViewRow)
        key = gvMovimientos.DataKeys(e.RowIndex).Values(0)
    End Sub

    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()
        If Not ResultList Is Nothing And ResultList.Count() > 0 Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList(0).TotalPageNumbers)
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If
        divResult.Visible = True
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        Dim oBEBanco As New BEBanco
        Dim oCntrBanco As New CBanco
        Dim oCntrAdministracionComun As New CAdministrarComun()
        oBEBanco.IdEstado = EstadoBanco.Activo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlBanco, oCntrBanco.ConsultarBanco(oBEBanco), "Descripcion", "IdBanco", "::Seleccione::")
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlMoneda, oCntrAdministracionComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
        txtFechaFin.Text = DateTime.Now.ToString("dd/MM/yyyy")
        txtFechaInicio.Text = DateAdd(DateInterval.Month, -1, Date.Now).Date.ToString("dd/MM/yyyy")
    End Sub
#End Region

#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(ddlBanco)
        End If
    End Sub

    Protected Sub gvEmpresa_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
    End Sub
#End Region

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        divResult.Visible = False
        divgrilla.Visible = False
    End Sub

    Protected Sub gvMovimientos_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMovimientos.RowCommand
        If e.CommandName.Equals("Edit") Then
            Dim listaMovimientos As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) = (CType(ViewState("listaMovimientos"), System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)))
            Dim beMovimientoCuenta As SPE.Entidades.BEMovimientoCuentaBanco = listaMovimientos.First(Function(x) CType(x, SPE.Entidades.BEMovimientoCuentaBanco).IdMovimientoCuentaBanco = CInt(e.CommandArgument))
            Session.Add("beMovimientoCuentaToDetalle", beMovimientoCuenta)
            VariableTransicion = e.CommandArgument
        End If
        
    End Sub
End Class
