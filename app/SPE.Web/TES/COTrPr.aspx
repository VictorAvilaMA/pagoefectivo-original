<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="COTrPr.aspx.vb" Inherits="ADM_COServ" Title="Consultar Comisiones por Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #ctl00_ContentPlaceHolder1_pnlPopupActualizarTransferencia li
        {
            margin-bottom: 5px;
        }
        #ctl00_ContentPlaceHolder1_UpdatePanel12
        {
            position: relative;
            z-index: 10;
        }
        #ctl00_ContentPlaceHolder1_UpdatePanel12 ul.datos_cip2
        {
            z-index: 10;
        }
        .cont_cel
        {
            position: static;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <h2>
        Consultar Transferencias Procesadas</h2>

    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda</h4>
        <ul class="datos_cip2">
            <asp:UpdatePanel runat="server" ID="UpdatePanelFiltros" UpdateMode="Conditional">
                <ContentTemplate>
                    <li class="t1"><span class="color">&gt;&gt; </span>Empresa: </li>
                    <li class="t2" style="line-height: 1">
                        <asp:DropDownList ID="ddlEmpresa" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Moneda: </li>
                    <li class="t2" style="line-height: 1">
                        <asp:DropDownList ID="ddlMoneda" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Estado: </li>
                    <li class="t2" style="line-height: 1">
                        <asp:DropDownList ID="ddlEstado" CssClass="neu" runat="server">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt; </span>Fecha del: </li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaInicio" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" CultureName="es-PE"
                                MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaFin" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" CultureName="es-PE"
                                MaskType="Date" Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                        </p>
                        <%--<asp:ValidationSummary ID="ValidationSummary" runat="server"></asp:ValidationSummary>--%>
                        <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaInicio"
                            PopupButtonID="ibtnFechaInicio" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaFin"
                            PopupButtonID="ibtnFechaFin" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" InvalidValueBlurredMessage="*"
                            EmptyValueBlurredText="*" Display="Dynamic" EmptyValueMessage="Fecha 'Del' es requerida"
                            IsValidEmpty="False" InvalidValueMessage="Fecha Desde no v�lida" ErrorMessage="*"
                            ControlToValidate="txtFechaInicio" ControlExtender="MaskedEditExtenderDe"></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" InvalidValueBlurredMessage="*"
                            EmptyValueBlurredText="*" Display="Dynamic" EmptyValueMessage="Fecha 'Al' es requerida"
                            IsValidEmpty="False" InvalidValueMessage="Fecha Hasta no v�lida" ErrorMessage="*"
                            ControlToValidate="txtFechaFin" ControlExtender="MaskedEditExtenderA"></cc1:MaskedEditValidator>
                        <asp:CompareValidator ID="CompareValidator" runat="server" ErrorMessage="Rango de fechas de b�squeda no es v�lido"
                            ControlToValidate="txtFechaInicio" Type="Date" Operator="LessThanEqual" ControlToCompare="txtFechaFin">*</asp:CompareValidator>
                    </li>
                    <div id="divMensaje" runat="server" class="divContenedorTitulo">
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <li class="complet">
                <asp:Button ID="btnBuscar" runat="server" CssClass="input_azul5" Text="Buscar" />
                <asp:Button ID="btnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar" />
                <asp:Button ID="btnExportarExcel" runat="server" CssClass="input_azul4" Text="Excel" />
            </li>
        </ul>
        <div class="cont_cel">
            <asp:UpdatePanel ID="UpdatePanelGrilla" runat="server">
                <ContentTemplate>
                    <h5 id="h5result" runat="server" visible="false">
                        <asp:Literal ID="lblMsgNroRegistros" runat="server" Text="Resultados:"></asp:Literal>
                    </h5>
                    <asp:GridView ID="gvResultado" AllowSorting="true" runat="server" CssClass="grilla"
                        AutoGenerateColumns="False" DataKeyNames="IdServicio" AllowPaging="True" OnRowDataBound="gvResultado_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ibtEdit" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/images/lupa.gif" %>' ToolTip="Actualizar"
                                        CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"idTransfenciaEmpresas")%>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="idTransfenciaEmpresas" SortExpression="IdTransfenciaEmpresas"
                                HeaderText="Nro Proceso"></asp:BoundField>
                            <asp:BoundField DataField="FechaCreacion" SortExpression="FechaRegistro" HeaderText="F. de Proceso">
                            </asp:BoundField>
                            <asp:BoundField DataField="DescripcionEmpresa" SortExpression="Empresa" HeaderText="Empresa">
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaInicio" SortExpression="FechaInicio" HeaderText="F. Inicial">
                            </asp:BoundField>
                            <asp:BoundField DataField="FechaFin" SortExpression="FechaFin" HeaderText="F. Final">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Pago" SortExpression="TotalPago">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "SimboloMoneda") + "" + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "TotalPago"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comisi&oacute;n" SortExpression="TotalComision">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "SimboloMoneda") + "" + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "TotalComision"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" SortExpression="Resultado">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "SimboloMoneda") + "" + SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Resultado"))%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="cabecera"></HeaderStyle>
                        <EmptyDataTemplate>
                            No se encontraron registros.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <cc1:ModalPopupExtender ID="mppTransferencia" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="imgbtnRegresarHidden" PopupControlID="pnlPopupActualizarTransferencia"
                        TargetControlID="imgbtnTargetHidden">
                    </cc1:ModalPopupExtender>
                    <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" Style="display: none;"
                        CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                    <asp:ImageButton ID="imgbtnTargetHidden" runat="server" Style="display: none;" CssClass="Hidden"
                        CausesValidation="false"></asp:ImageButton>
                    <asp:Panel ID="pnlPopupActualizarTransferencia" runat="server" class="conten_pasos3"
                        Style="display: none; background: white">
                        <div style="float: right">
                            <asp:ImageButton ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" runat="server"
                                ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/img/closeX.GIF" %>'  CausesValidation="false"></asp:ImageButton>
                        </div>
                        <h4>
                            Datos de Transferencia:</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Empresa: </li>
                            <li class="t2">
                                <asp:Literal ID="txtPEmpresa" runat="server">
                                </asp:Literal>
                                <asp:Label ID="lblIdTransferencia" Visible="false" runat="server" CssClass="MensajeValidacion"></asp:Label>
                                <asp:Label ID="lblIdEmpresa" Visible="false" runat="server" CssClass="MensajeValidacion"></asp:Label>
                                <asp:Label ID="lblIdMoneda" Visible="false" runat="server" CssClass="MensajeValidacion"></asp:Label>
                            </li>
                            <li class="t1"><span class="color">>></span> Estado: </li>
                            <li class="t2">
                                <asp:Literal ID="txtPEstado" runat="server">
                                </asp:Literal>
                            </li>
                            <li class="t1"><span class="color">>></span> Fecha del: </li>
                            <li class="t2">
                                <p class="input_cal">
                                    <asp:TextBox ID="txtPDel" runat="server" CssClass="corta"></asp:TextBox>
                                </p>
                                <span class="entre">al</span>
                                <p class="input_cal">
                                    <asp:TextBox ID="txtPAl" runat="server" CssClass="corta"></asp:TextBox>
                                </p>
                            </li>
                            <li class="t1"><span class="color">>></span> N&uacute;mero de Operaciones: </li>
                            <li class="t2">
                                <p class="input_cal2">
                                    <asp:Literal ID="txtPTotalOperaciones" runat="server"></asp:Literal>
                                </p>
                                <span class="entre2">Moneda :</span>
                                <p class="input_cal">
                                    <asp:Literal ID="txtPMoneda" runat="server"></asp:Literal>
                                </p>
                            </li>
                            <li class="t1"><span class="color">>></span> Total Comisi&oacute;n: </li>
                            <li class="t2">
                                <p class="input_cal2">
                                    <asp:Literal ID="txtPTComision" runat="server"></asp:Literal>
                                </p>
                                <span class="entre2">Total de Pagos:</span>
                                <p class="input_cal2">
                                    <asp:Literal ID="txtPTPagos" runat="server"></asp:Literal>
                                </p>
                            </li>
                        </ul>
                        <div style="clear: both">
                        </div>
                        <h4>
                            Datos Arch. de Conciliaci&oacute;n:</h4>
                        <ul class="datos_cip2">
                            <li class="t1"><span class="color">>></span> Fecha de Dep&oacute;sito: </li>
                            <li class="t2">
                                <p class="input_cal">
                                    <asp:TextBox ID="txtFechaDeposito" runat="server" CssClass="corta"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/App_Themes/SPE/img/calendario.png"%>'
                                        CssClass="calendario"></asp:ImageButton>
                                        
                                </p>
                                <span class="entre2">Nro. Operaci&oacute;n:</span>
                                <p class="input_cal2">
                                    <asp:TextBox ID="txtPNroOperacion" runat="server" CssClass="corta" MaxLength="50"></asp:TextBox>
                                </p>
                            </li>
                            <li class="t1"><span class="color">>></span> Banco: </li>
                            <li class="t2">
                                <p class="input_cal2" style="width: 133px">
                                    <asp:TextBox ID="txtBanco" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                                </p>
                            </li>
                            <li class="t1"><span class="color">>></span> Numero de Cuenta: </li>
                            <li class="t2">
                                <p class="input_cal2" style="width: 133px">
                                    <asp:TextBox ID="txtPNroCuenta" runat="server" CssClass="corta" MaxLength="50" ReadOnly="true"></asp:TextBox>
                                </p>
                                <span class="entre2">
                                    <asp:LinkButton ID="btnExcel" runat="server">Exportar a Excel</asp:LinkButton></span>
                                <p class="input_cal2" style="width: 150px">
                                    <asp:LinkButton ID="btnArchC" runat="server" Style="text-decoration: underline">Archivo de Conciliaci&oacute;n</asp:LinkButton>
                                </p>
                            </li>
                            <li class="t1"><span class="color">>></span> Adjuntar Documento: </li>
                            <li class="t2">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:HyperLink ID="lnkURL" Text="Ver Archivo" runat="server" Visible="false"></asp:HyperLink>
                            </li>
                            <li class="t1"><span class="color">>></span> Observaci&oacute;n: </li>
                            <li class="t2" style="width: 100px">
                                <asp:TextBox ID="txtPObservacion" runat="server" TextMode="MultiLine" MaxLength="500"
                                    CssClass="MultilineMiniCamp"></asp:TextBox>
                            </li>
                            <li class="complet" style="margin-top: 10px">
                                <asp:Button ID="btnPActualizar" runat="server" CssClass="input_azul5" Text="Actualizar" />
                                <asp:Button ID="btnPLiquidar" runat="server" CssClass="input_azul4" Text="Liquidar"
                                    ValidationGroup="ValidaTransaccion" />
                                <asp:Button ID="btnPCancelar" runat="server" CssClass="input_azul4" Text="Cancelar" />
                            </li>
                        </ul>
                        <fieldset class="ContenedorEtiquetaTexto">
                            <div class="even w100 clearfix dlinline">
                                <div class="divgrid">
                                    <div class="w50">
                                        <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtenderDe"
                                            ControlToValidate="txtFechaDeposito" ErrorMessage="*" InvalidValueMessage="Fecha de Deposito no v�lida"
                                            IsValidEmpty="False" EmptyValueMessage="Fecha de Deposito es requerida" Display="Dynamic"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="ValidaTransaccion"></cc1:MaskedEditValidator>
                                        <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtFechaDeposito"
                                            Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaDeposito"
                                            Format="dd/MM/yyyy" PopupButtonID="ImageButton1">
                                        </cc1:CalendarExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="even w100 clearfix dlinline">
                                <asp:Label ID="lblPmensaje" runat="server" CssClass="MensajeValidacion" Text="Empresa:"
                                    Visible="false"></asp:Label>
                            </div>
                        </fieldset>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click"></asp:AsyncPostBackTrigger>
                    <asp:PostBackTrigger ControlID="btnArchC" />
                    <asp:PostBackTrigger ControlID="btnPLiquidar" />
                    <asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
                    <asp:PostBackTrigger ControlID="btnPActualizar"></asp:PostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
