Imports System.Data
Imports _3Dev.FW.Web
Imports SPE.Web
Imports SPE.Entidades
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades
Imports SPE.Web.PaginaBase
Imports _3Dev.FW.Util.DataUtil
Partial Class ADM_COServ
    Inherits _3Dev.FW.Web.MaintenanceSearchPageBase(Of SPE.Web.CEmpresaContratante)


    'prueba prueba

#Region "atributos"
    Private objCParametro As CAdministrarParametro



    Protected Overrides ReadOnly Property BtnClear() As System.Web.UI.WebControls.Button
        Get
            Return btnLimpiar
        End Get
    End Property
    Protected Overrides ReadOnly Property BtnSearch() As System.Web.UI.WebControls.Button
        Get
            Return btnBuscar
        End Get
    End Property

    Protected Overrides ReadOnly Property GridViewResult() As System.Web.UI.WebControls.GridView
        Get
            Return gvResultado
        End Get
    End Property

    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
    Protected Overrides ReadOnly Property FlagPager As Boolean
        Get
            Return True
        End Get
    End Property
#End Region

#Region "m�todos base"

    Public Overrides Function CreateBusinessEntityForSearch() As _3Dev.FW.Entidades.BusinessEntityBase
        Dim obeTransferencia As New SPE.Entidades.BETransferencia()
        obeTransferencia.idEmpresaContratante = _3Dev.FW.Util.DataUtil.StringToInt(ddlEmpresa.SelectedValue)
        obeTransferencia.IdMoneda = _3Dev.FW.Util.DataUtil.StringToInt(ddlMoneda.SelectedValue)
        obeTransferencia.IdEstado = _3Dev.FW.Util.DataUtil.StringToInt(ddlEstado.SelectedValue)
        obeTransferencia.FechaInicio = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaInicio.Text)
        obeTransferencia.FechaFin = _3Dev.FW.Util.DataUtil.StringToDateTime(txtFechaFin.Text)
        Return obeTransferencia
    End Function
    Public Overrides Sub AssignParametersToLoadMaintenanceEdit(ByRef key As Object, ByVal e As GridViewRow)
        key = gvResultado.DataKeys(e.RowIndex).Values(0)
    End Sub

    Protected Overrides Sub ConfigureMaintenanceSearch(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceSearchArgs)
        'e.MaintenanceKey = "ADComi"
        'e.MaintenancePageName = "ADComi.aspx"
        'e.ExecuteSearchOnFirstLoad = False
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        Dim objCParametro As New CAdministrarParametro
        WebUtil.DropDownlistBinding(ddlEstado, objCParametro.ConsultarParametroPorCodigoGrupo("ESPP"), _
        "Descripcion", "Id", "::Todos::")
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Todos::")
        Using ocntrlEmpresaContrante As New CEmpresaContratante()
            Dim oBEEmpresaContratante As New BEEmpresaContratante()
            oBEEmpresaContratante.Codigo = ""
            oBEEmpresaContratante.RazonSocial = ""
            oBEEmpresaContratante.RUC = ""
            oBEEmpresaContratante.IdEstado = 41
            oBEEmpresaContratante.Email = ""
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Todos::")
        End Using
        InicializarFecha()
    End Sub
    Public Overrides Sub OnAfterSearch()
        MyBase.OnAfterSearch()

        h5result.Visible = True
        divMensaje.Visible = False
        If Not ResultList Is Nothing Then
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, ResultList.Count)
            If ResultList.Count = 0 Then
                divMensaje.Visible = False
            End If
        Else
            SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)
        End If

    End Sub
    Private Sub InicializarFecha()
        'FECHAS

        txtFechaInicio.Text = DateAdd(DateInterval.Day, -30, Date.Now).Date.ToString("dd/MM/yyyy") 'Today.ToShortDateString
        txtFechaFin.Text = Today.ToShortDateString

    End Sub
    Public Overrides Sub OnClear()
        MyBase.OnClear()

        h5result.Visible = False
        ddlEmpresa.SelectedIndex = 0
        ddlMoneda.SelectedIndex = 0
        ddlEstado.SelectedIndex = 0
        SPE.Web.Util.UtilFormatGridView.ContadorRegistroGridView(lblMsgNroRegistros, 0)

    End Sub
    Public Overrides Function GetMethodSearchByParameters(ByVal be As _3Dev.FW.Entidades.BusinessEntityBase) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
        Return CType(ControllerObject, SPE.Web.CEmpresaContratante).ConsultarTransaccionesaLiquidar(be)
    End Function
    Private Sub CargarPopup(ByVal id As Integer)
        Dim objTransferencia As New BETransferencia
        Dim objT As New BETransferencia
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        objT.idTransfenciaEmpresas = id
        objTransferencia = objProcesar.ObtenerTransaccionesaLiquidar(objT)
        If Not IsNothing(objTransferencia) Then
            txtPEmpresa.Text = objTransferencia.DescripcionEmpresa
            txtPMoneda.Text = objTransferencia.DescripcionMoneda
            txtPAl.Text = CDate(objTransferencia.FechaFin).ToShortDateString
            txtPDel.Text = CDate(objTransferencia.FechaInicio).ToShortDateString
            txtPNroCuenta.Text = objTransferencia.NumeroCuenta
            txtPNroOperacion.Text = objTransferencia.NumeroOperacion
            txtPObservacion.Text = objTransferencia.Observacion
            txtPTComision.Text = objTransferencia.TotalComision.ToString("#,#0.00")
            txtPTotalOperaciones.Text = objTransferencia.TotalOperaciones
            txtPTPagos.Text = objTransferencia.TotalPago.ToString("#,#0.00")
            lblIdTransferencia.Text = objTransferencia.idTransfenciaEmpresas
            lblIdEmpresa.Text = objTransferencia.idEmpresaContratante
            lblIdMoneda.Text = objTransferencia.IdMoneda
            txtPEstado.Text = objTransferencia.DescipcionEstadoT
            Dim fechaDep As DateTime = objTransferencia.FechaDeposito
            divMensaje.Visible = False
            If objTransferencia.Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoPeriodoPagoServicio.Liquidado Then
                Try
                    DescargarArchivo(objTransferencia)
                Catch ex As Exception
                    lnkURL.Text = "Archivo no disponible"
                    lnkURL.NavigateUrl = ""
                    lnkURL.Visible = True
                    FileUpload1.Visible = False
                End Try

                btnPActualizar.Enabled = False
                btnPLiquidar.Enabled = False
                'btnPLiquidar.CssClass = "input_azul4 btn-disabled"
                'btnPActualizar.CssClass = "input_azul5 btn-disabled"
                txtFechaDeposito.Enabled = False
                ImageButton1.Enabled = False
                txtBanco.Enabled = False
                txtPNroOperacion.Enabled = False
                txtPNroCuenta.Enabled = False
                txtPObservacion.Enabled = False
            Else
                btnPActualizar.Enabled = True
                btnPLiquidar.Enabled = True
                btnPLiquidar.CssClass = "input_azul4"
                btnPActualizar.CssClass = "input_azul5"
                txtFechaDeposito.Enabled = True
                ImageButton1.Enabled = True
                'txtBanco.Enabled = True
                txtPNroOperacion.Enabled = True
                'txtPNroCuenta.Enabled = True
                txtPObservacion.Enabled = True
            End If


            If fechaDep = DateTime.MinValue Then
                txtFechaDeposito.Text = Date.Today.ToShortDateString
            Else
                txtFechaDeposito.Text = fechaDep
            End If

            'Dim objCParametros As New SPE.Web.CAdministrarParametro
            'Dim objCBanco As New SPE.Web.CBanco
            'Dim oBEBanco As New BEBanco
            'With oBEBanco
            '    .Codigo = ""
            '    .Descripcion = ""
            '    .IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoBanco.Activo
            'End With
            'WebUtil.DropDownlistBinding(ddlPBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "idBanco", "::Seleccione::")
            'If objTransferencia.IdBanco = 0 Then
            '    ddlPBanco.SelectedValue = String.Empty
            'Else
            '    ddlPBanco.SelectedValue = objTransferencia.IdBanco
            'End If
            txtBanco.Text = objTransferencia.DescripcionBanco

            pnlPopupActualizarTransferencia.Visible = True
            mppTransferencia.Show()
        End If
    End Sub
#End Region
#Region "Eventos"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Page.Form.DefaultButton = btnBuscar.UniqueID
            Page.SetFocus(ddlEmpresa)
        End If
    End Sub

    Protected Sub gvResultado_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        SPE.Web.Util.UtilFormatGridView.BackGroundGridView(e, _
        "IdEstado", _
        SPE.EmsambladoComun.ParametrosSistema.EstadoMantenimiento.Inactivo, _
        SPE.EmsambladoComun.ParametrosSistema.BackColorAnulado)
    End Sub
    Protected Sub gvResultado_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResultado.RowCommand
        Select Case e.CommandName
            Case "Edit" : CargarPopup(CInt(e.CommandArgument))
        End Select
    End Sub
#End Region


    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        mppTransferencia.Hide()
    End Sub

    Protected Sub btnPActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPActualizar.Click
        '
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        Dim obMensaje As New MensajeInformacion
        Dim intResultado As Integer = 0
        intResultado = objProcesar.ActualizarTransaccionesaLiquidar(FillEntityforUpdateT(SPE.EmsambladoComun.ParametrosSistema.EstadoPeriodoPagoServicio.Registrado))
        If intResultado = -1 Then
            'lblPmensaje.Text = MensajesError.MensajeTransaccionUpdateLiquidado
            lblMensaje.ForeColor = Drawing.Color.Red
            ThrowErrorMessage("No se pueden actualizar transacciones liquidadas")
            'lblPmensaje.Visible = True
        Else
            lblMensaje.ForeColor = Drawing.Color.Blue
            lblMensaje.Text = obMensaje.MensajeOkConOperacion(intResultado)
            lblMensaje.Visible = True
            'ThrowSuccessfullyMessage(obMensaje.MensajeOkConOperacion(intResultado))
        End If
        divMensaje.Visible = True

    End Sub
    Protected Sub btnPLiquidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPLiquidar.Click
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        Dim obMensaje As New MensajeInformacion
        Dim intResultado As Integer = 0
        intResultado = objProcesar.ActualizarTransaccionesaLiquidar(FillEntityforUpdateT(SPE.EmsambladoComun.ParametrosSistema.EstadoPeriodoPagoServicio.Liquidado))
        If intResultado = -1 Then
            lblMensaje.ForeColor = Drawing.Color.Red
            ThrowErrorMessage("No se pueden actualizar transacciones liquidadas")
        Else
            lblMensaje.Text = "Se registro la Operaci�n"
            lblMensaje.ForeColor = Drawing.Color.Blue
            lblMensaje.Visible = True
        End If
        divMensaje.Visible = True

    End Sub

    Private Function FillEntityforUpdateT(ByVal intEstado As Integer) As BETransferencia
        Dim objTransferencia As New BETransferencia
        objTransferencia.idTransfenciaEmpresas = lblIdTransferencia.Text
        objTransferencia.IdUsuarioActualizacion = UserInfo.IdUsuario
        'If ddlPBanco.SelectedValue = String.Empty Then
        '    objTransferencia.IdBanco = 0
        'Else
        '    objTransferencia.IdBanco = ddlPBanco.SelectedValue
        'End If

        objTransferencia.DescripcionBanco = txtBanco.Text

        If (txtFechaDeposito.Text) = String.Empty Then
            objTransferencia.FechaDeposito = DateTime.MinValue.ToShortDateString
        Else
            objTransferencia.FechaDeposito = CDate(txtFechaDeposito.Text)
        End If

        objTransferencia.NumeroOperacion = txtPNroOperacion.Text
        objTransferencia.NumeroCuenta = txtPNroCuenta.Text
        objTransferencia.Observacion = txtPObservacion.Text

        If FileUpload1.FileName.Length > 0 Then
            Dim nombreFile As String
            Dim ftp As New UtilManagerFTP
            Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
            Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
            Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")

            nombreFile = DateTime.Today.Year.ToString() + "_" + DateTime.Today.Month.ToString() + "_" + DateTime.Today.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + FileUpload1.FileName
            Try
                ftp.UploadFile(FileUpload1.FileContent, FTPTesoreria + nombreFile, FTPUser, FTPPass)
                objTransferencia.ArchivoAdjunto = nombreFile
            Catch ex As Exception
                Throw ex
            End Try


        Else
            objTransferencia.ArchivoAdjunto = String.Empty
        End If

        objTransferencia.IdEstado = intEstado
        Return objTransferencia
    End Function

    Protected Sub btnPCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPCancelar.Click
        mppTransferencia.Hide()
        divMensaje.Visible = False

    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim objCServicio As New SPE.Web.CServicio
            Dim obeServicio As New SPE.Entidades.BETransferencia

            Dim ListaTrans As New List(Of BETransferencia)
            obeServicio.idEmpresaContratante = CInt(lblIdEmpresa.Text)
            obeServicio.IdMoneda = CInt(lblIdMoneda.Text)
            obeServicio.idTransfenciaEmpresas = CInt(lblIdTransferencia.Text)
            obeServicio.FechaInicio = CDate(txtPDel.Text)
            obeServicio.FechaFin = CDate(txtPAl.Text)
            ListaTrans = objCServicio.ConsultarDetalleTransaccionesxServicio(obeServicio)

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Empresa", txtPEmpresa.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", txtPMoneda.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", txtPEstado.Text))
            If ListaTrans.Count > 0 Then
                parametros.Add(New KeyValuePair(Of String, String)("FechaRegistro", ListaTrans(0).FechaCreacionOP.ToString("dd/MM/yyyy hh:mm tt")))
                parametros.Add(New KeyValuePair(Of String, String)("FechaLiquidacion", IIf(ListaTrans.Item(0).FechaLiquidacion = DateTime.MinValue, " - ", ListaTrans(0).FechaLiquidacion.ToString("dd/MM/yyyy hh:mm tt"))))
            Else
                parametros.Add(New KeyValuePair(Of String, String)("FechaRegistro", " - "))
                parametros.Add(New KeyValuePair(Of String, String)("FechaLiquidacion", " - "))
            End If
            UtilReport.ProcesoExportarGenerico(ListaTrans, Page, "EXCEL", "xls", "Detalle Transacciones - ", parametros, "RptDetalleTransaccion.rdlc")
        End If
    End Sub

    Protected Sub btnArchC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchC.Click

        Dim objCServicio As New SPE.Web.CServicio
        Dim obeServicio As New SPE.Entidades.BETransferencia
        Dim idEmpSaga As String = System.Configuration.ConfigurationManager.AppSettings("idEmpSaga")
        Dim idEmpRipley As String = System.Configuration.ConfigurationManager.AppSettings("idEmpRipley")
        Dim ListaTrans As New List(Of BETransferencia)
        obeServicio.idEmpresaContratante = CInt(lblIdEmpresa.Text)
        obeServicio.IdMoneda = CInt(lblIdMoneda.Text)
        obeServicio.idTransfenciaEmpresas = CInt(lblIdTransferencia.Text)
        obeServicio.FechaDeposito = CDate(txtFechaDeposito.Text)
        obeServicio.FechaInicio = CDate(txtPDel.Text)
        obeServicio.FechaFin = CDate(txtPAl.Text)

        If lblIdEmpresa.Text = idEmpSaga Then
            GeneracionPlanoSaga(objCServicio, obeServicio, txtPDel.Text, txtPAl.Text)
        ElseIf lblIdEmpresa.Text = idEmpRipley Then
            GeneracionPlanoRipley(objCServicio, obeServicio, txtPDel.Text, txtPAl.Text, txtFechaDeposito.Text)
        Else
            GeneracionPlanoNormal(objCServicio, obeServicio)
        End If


    End Sub



    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCeros(ByVal valor As String) As String
        If valor.Length = 1 Then
            Return "0" + valor
        Else
            Return valor
        End If
    End Function


    Private Function ObtenerFinal(ByVal inicial As String, ByVal valor As String, ByVal tamano As Integer) As String

        If inicial.Length <= 6 Then
            Return Filler(inicial, valor, 6)
        End If
        Return inicial.Substring(inicial.Length - tamano)
    End Function

    Private Function Filler(ByVal inicial As String, ByVal valor As String, ByVal tamano As Integer) As String

        If inicial.Length = tamano Then
            Return inicial
        End If
        Dim strRetorno As String = inicial
        For i = 1 To (tamano - inicial.Length)
            strRetorno = valor + strRetorno
        Next
        Return strRetorno
    End Function

    Private Function CadEntero(ByVal valor As String) As String
        If Not valor.Contains(".") Then
            Return valor + "00"
        Else
            Dim Cad As String() = valor.Split(".")
            If Cad(1).Trim().Length = 1 Then
                Return Cad(0) + Cad(1) + "0"
            Else
                Return Cad(0) + Cad(1)
            End If
        End If
    End Function

    Public Sub GeneracionPlanoSaga(ByVal objCServicio As SPE.Web.CServicio, ByVal obeServicio As SPE.Entidades.BETransferencia, ByVal fdel As String, ByVal fal As String)

        Dim strCadDel = fdel.Split("/")
        Dim strDiaDel = strCadDel(0)
        Dim strMesDel = strCadDel(1)
        Dim strAniDel = strCadDel(2)
        Dim strCadAl = fal.Split("/")
        Dim strDiaAl = strCadAl(0)
        Dim strMesAl = strCadAl(1)
        Dim strAniAl = strCadAl(2)


        Dim ListaTrans As New List(Of BETransferenciaSaga)
        ListaTrans = objCServicio.ConsultarDetalleTransaccionesxServicioSaga(obeServicio)

        Dim BEFirst As New BETransferenciaSaga
        BEFirst = ListaTrans(0)

        Dim StringPagoServicios As New StringBuilder("")

        StringPagoServicios.Append(strDiaDel & strMesDel & strAniDel)
        StringPagoServicios.Append(" ")
        StringPagoServicios.Append(strDiaAl & strMesAl & strAniAl)
        StringPagoServicios.Append(" ")
        StringPagoServicios.Append(CompletarCeros(BEFirst.FechaRegistro.Day.ToString) & CompletarCeros(BEFirst.FechaRegistro.Month.ToString) & BEFirst.FechaRegistro.Year.ToString)
        StringPagoServicios.Append(" ")
        StringPagoServicios.Append(CompletarCeros(BEFirst.FechaDeposito.Day.ToString) & CompletarCeros(BEFirst.FechaDeposito.Month.ToString) & BEFirst.FechaDeposito.Year.ToString)
        StringPagoServicios.Append(BEFirst.CodigoCliente)
        StringPagoServicios.Append(Filler(BEFirst.NombreCliente, " ", 20))
        StringPagoServicios.Append(Filler("", " ", 71))
        StringPagoServicios.Append("HEADER")
        StringPagoServicios.AppendLine()


        For Each elto As BETransferenciaSaga In ListaTrans
            StringPagoServicios.Append(elto.Liq_Numc)
            StringPagoServicios.Append(CompletarCeros(BEFirst.FechaRegistro.Day.ToString) & CompletarCeros(BEFirst.FechaRegistro.Month.ToString) & BEFirst.FechaRegistro.Year.ToString) 'cambio realizado por Randolph Rosas
            'StringPagoServicios.Append(CompletarCeros(BEFirst.Liq_Fproc.Day.ToString) & CompletarCeros(BEFirst.Liq_Fproc.Month.ToString) & BEFirst.Liq_Fproc.Year.ToString)'codigo original
            StringPagoServicios.Append(CompletarCeros(elto.Liq_Fpago.Day.ToString) & CompletarCeros(elto.Liq_Fpago.Month.ToString) & elto.Liq_Fpago.Year.ToString) 'cambio realizado por Randolph Rosas
            'StringPagoServicios.Append(CompletarCeros(BEFirst.Liq_Fcom.Day.ToString) & CompletarCeros(BEFirst.Liq_Fcom.Month.ToString) & BEFirst.Liq_Fcom.Year.ToString)'codigo original
            StringPagoServicios.Append(elto.Liq_Micr)
            StringPagoServicios.Append(elto.Liq_Numta)
            StringPagoServicios.Append(elto.Liq_Marca)
            StringPagoServicios.Append(Filler(CadEntero(elto.Liq_Monto.ToString()), "0", 11))
            StringPagoServicios.Append(elto.Liq_Moneda)
            StringPagoServicios.Append(elto.Liq_Txs)
            StringPagoServicios.Append(elto.Liq_Rete)
            StringPagoServicios.Append(elto.Liq_Cprin)
            StringPagoServicios.Append(CompletarCeros(BEFirst.FechaRegistro.Day.ToString) & CompletarCeros(BEFirst.FechaRegistro.Month.ToString) & BEFirst.FechaRegistro.Year.ToString) ''cambio realizado por Randolph
            'StringPagoServicios.Append(CompletarCeros(BEFirst.Liq_Fcom.Day.ToString) & CompletarCeros(BEFirst.Liq_Fcom.Month.ToString) & BEFirst.Liq_Fcom.Year.ToString) 'cambio x 3r
            'StringPagoServicios.Append(CompletarCeros(BEFirst.Liq_Fpago.Day.ToString) & CompletarCeros(BEFirst.Liq_Fpago.Month.ToString) & BEFirst.Liq_Fpago.Year.ToString)'codigo original
            StringPagoServicios.Append(Filler(elto.Liq_Nro_Unico, " ", 26))
            StringPagoServicios.Append(ObtenerFinal(elto.Liq_codaut, " ", 6))
            StringPagoServicios.Append(Filler("", " ", 21))
            StringPagoServicios.Append(Filler(CadEntero(elto.Liq_Monto_Com_Neta.ToString()), "0", 11))
            StringPagoServicios.Append(Filler(CadEntero(elto.Liq_Monto_Com_Afect.ToString()), "0", 11))
            StringPagoServicios.Append(Filler(CadEntero(elto.Liq_Monto_igv.ToString()), "0", 11))
            StringPagoServicios.Append(Filler(CadEntero(elto.Liq_Monto_Liquidar.ToString()), "0", 11))
            StringPagoServicios.Append(Filler(Trim(elto.Liq_Num_Cuenta_banco.ToString), " ", 26))
            StringPagoServicios.Append(Filler(elto.Liq_Numero_orden.ToString, " ", 50))
            StringPagoServicios.AppendLine()
        Next

        StringPagoServicios.Append(Filler(BEFirst.Sal_Contador.ToString, "0", 10))
        StringPagoServicios.Append(" ")
        StringPagoServicios.Append(Filler((CadEntero(BEFirst.Sal_Monto).ToString()), "0", 13))
        StringPagoServicios.Append(" ")
        StringPagoServicios.Append(Filler("0", "0", 10))
        StringPagoServicios.Append(" ")
        StringPagoServicios.Append(Filler("0", "0", 13))
        StringPagoServicios.Append(Filler("", " ", 85))
        StringPagoServicios.Append("FOOTER")



        Dim byteArchivoCorona As Byte() = New UTF8Encoding().GetBytes(StringPagoServicios.ToString())

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = "text/plain"
        Response.AddHeader("content-disposition", "attachment; filename=" & "PE.00000672.00000005." & CompletarCeros(BEFirst.FechaRegistro.Year.ToString) & CompletarCeros(BEFirst.FechaRegistro.Month.ToString) & CompletarCeros(BEFirst.FechaRegistro.Day.ToString) & ".001.txt")
        Response.BinaryWrite(byteArchivoCorona)
        Response.Flush()
        Response.[End]()
    End Sub

    Public Sub GeneracionPlanoRipley(ByVal objCServicio As SPE.Web.CServicio, ByVal obeServicio As SPE.Entidades.BETransferencia, ByVal fdel As String, ByVal fal As String, ByVal fdeposito As String)

        'INICIO
        Dim strCadDel = fdel.Split("/")
        Dim strDiaDel = strCadDel(0)
        Dim strMesDel = strCadDel(1)
        Dim strAniDel = strCadDel(2)
        'FIN
        Dim strCadAl = fal.Split("/")
        Dim strDiaAl = strCadAl(0)
        Dim strMesAl = strCadAl(1)
        Dim strAniAl = strCadAl(2)

        'FECHA DEPOSITO
        Dim strCadDeposito = fdeposito.Split("/")
        Dim strDiaDeposito = strCadDeposito(0)
        Dim strMesDeposito = strCadDeposito(1)
        Dim strAniDeposito = strCadDeposito(2)


        Dim ListaTrans As New List(Of BETransferenciaRipley)
        ListaTrans = objCServicio.ConsultarDetalleTransaccionesxServicioRipley(obeServicio)

        Dim BEFirst As BETransferenciaRipley = ListaTrans(0)


        Dim StringPagoServicios As New StringBuilder("")

        For Each elto As BETransferenciaRipley In ListaTrans


            '[0]Fecha Dep�sito
            StringPagoServicios.Append(strDiaDeposito & strMesDeposito & strAniDeposito)
            '[1]Periodo Inicio
            StringPagoServicios.Append(strDiaDel & strMesDel & strAniDel & "000000")
            '[2]Periodo Fin
            StringPagoServicios.Append(strDiaAl & strMesAl & strAniAl)
            '[3]CIP
            StringPagoServicios.Append(LlenarCeros(elto.Liq_Nro_Unico, 14))
            '[4]CodTransaccion
            StringPagoServicios.Append(LlenarCeros(elto.Liq_Numero_orden, 10))
            '[5]Comsion
            StringPagoServicios.Append(LlenarCeros(elto.Comision_Servicio, 10))
            '[6]Monto
            StringPagoServicios.Append(LlenarCeros(elto.Liq_Monto, 18))
            StringPagoServicios.AppendLine()
        Next

        Dim byteArchivoCorona As Byte() = New UTF8Encoding().GetBytes(StringPagoServicios.ToString())

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = "text/plain"
        Dim FechaFRipley As Date = Date.Now
        Response.AddHeader("content-disposition", "attachment; filename=" & "PEL" & FechaFRipley.ToString("yyyyMMddHHmmss") & ".txt")
        Response.BinaryWrite(byteArchivoCorona)
        Response.Flush()
        Response.[End]()
    End Sub


    Public Sub GeneracionPlanoNormal(ByVal objCServicio As SPE.Web.CServicio, ByVal obeServicio As SPE.Entidades.BETransferencia)
        Dim ListaTrans As New List(Of BETransferencia)
        ListaTrans = objCServicio.ConsultarDetalleTransaccionesxServicio(obeServicio)

        Dim StringPagoServicios As New StringBuilder("")
        For Each elemento As BusinessEntityBase In ListaTrans
            Dim obj As BETransferencia
            obj = DirectCast(elemento, BETransferencia)
            StringPagoServicios.Append(obj.IdOrdenPago.ToString())
            StringPagoServicios.Append("," & obj.IdCliente)
            StringPagoServicios.Append("," & obj.NumeroOrdenPago)
            StringPagoServicios.Append("," & obj.IdOrderComercio)
            StringPagoServicios.Append("," & obj.IdMoneda)
            StringPagoServicios.Append("," & obj.TotalComision)
            StringPagoServicios.Append("," & obj.TotalPago)
            StringPagoServicios.Append("," & IIf(obj.FechaEmision = DateTime.MinValue, String.Empty, obj.FechaEmision.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & IIf(obj.FechaCancelacion = DateTime.MinValue, String.Empty, obj.FechaCancelacion.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & IIf(obj.FechaAnulacion = DateTime.MinValue, String.Empty, obj.FechaAnulacion.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & obj.IdEstado)
            StringPagoServicios.Append("," & IIf(obj.FechaLiquidacion = DateTime.MinValue, String.Empty, obj.FechaLiquidacion.ToString("dd/MM/yyyy hh:mm:ss")))
            StringPagoServicios.Append("," & obj.DescripcionBanco)
            StringPagoServicios.Append("," & obj.DescripcionAgencia)
            StringPagoServicios.AppendLine()
        Next

        Dim byteArchivoCorona As Byte() = New UTF8Encoding().GetBytes(StringPagoServicios.ToString())

        Response.Clear()
        Response.ClearHeaders()
        Response.SuppressContent = False
        Response.ContentType = "text/plain"
        Response.AddHeader("content-disposition", "attachment; filename=" & "ArchivoConciliacion_" & DateTime.Now.ToString("yyyyMMdd") & "_" & DateTime.Now.Hour.ToString() & DateTime.Now.Minute.ToString() & ".txt")
        Response.BinaryWrite(byteArchivoCorona)
        Response.Flush()
        Response.[End]()
    End Sub



    Public Overrides Sub OnGoToMaintenanceEditPage(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        e.NewEditIndex = -1
    End Sub
    Protected Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click

        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            Dim objCServicio As New SPE.Web.CEmpresaContratante
            Dim obeServicio As New SPE.Entidades.BETransferencia
            Dim dtfechainicio As DateTime = StringToDateTime(txtFechaInicio.Text)
            Dim dtfechafin As DateTime = StringToDateTime(txtFechaFin.Text)
            Dim strFechaInicio As String = String.Format("{0}-{1}-{2}", dtfechainicio.Year, dtfechainicio.Month, dtfechainicio.Day)
            Dim strFechaFin As String = String.Format("{0}-{1}-{2}", dtfechafin.Year, dtfechafin.Month, dtfechafin.Day)

            obeServicio.idEmpresaContratante = StringToInt(ddlEmpresa.SelectedValue)
            obeServicio.IdMoneda = StringToInt(ddlMoneda.SelectedValue)
            obeServicio.IdEstado = StringToInt(ddlEstado.SelectedValue)
            obeServicio.FechaInicio = CDate(strFechaInicio)
            obeServicio.FechaFin = CDate(strFechaFin)

            obeServicio.PageSize = Integer.MaxValue - 1
            obeServicio.PageNumber = 1
            obeServicio.PropOrder = ""
            obeServicio.TipoOrder = True
            Dim transferenciasBase As List(Of BusinessEntityBase) = objCServicio.ConsultarTransaccionesaLiquidar(obeServicio)
            Dim transferencias As New List(Of BETransferencia)

            Dim parametros As New List(Of KeyValuePair(Of String, String))()
            parametros.Add(New KeyValuePair(Of String, String)("Empresa", ddlEmpresa.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Moneda", ddlMoneda.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("Estado", ddlEstado.SelectedItem.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaDesde", txtFechaInicio.Text))
            parametros.Add(New KeyValuePair(Of String, String)("FechaHasta", txtFechaFin.Text))

            For index = 0 To transferenciasBase.Count - 1
                transferencias.Add(CType(transferenciasBase(index), BETransferencia))
            Next

            UtilReport.ProcesoExportarGenerico(transferencias, Page, "EXCEL", "xls", "Detalle de Transacci�n - ", parametros, "RptCOTrPr.rdlc")

        End If
    End Sub
    Private Sub DescargarArchivo(ByVal entidad As BETransferencia)

        Dim ftp As New UtilManagerFTP
        'Dim archivo As Byte()

        Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
        Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
        Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")
        Dim pathCurrent As String
        pathCurrent = Server.MapPath("~/")
        Dim raiz As String
        raiz = pathCurrent & "TempoFiles"



        ftp.DescargarArchivo(raiz, entidad.ArchivoAdjunto, FTPTesoreria, FTPUser, FTPPass)
        FileUpload1.Visible = False

        If Not IsNothing(entidad) Then
            lnkURL.Text = entidad.ArchivoAdjunto
            'lnkURL.NavigateUrl = FTPTesoreria + entidad.NombreDocAdjFtp
            lnkURL.NavigateUrl = "~/TempoFiles\" & entidad.ArchivoAdjunto

            lnkURL.Visible = True
        End If

    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        divMensaje.Visible = False

    End Sub

    Private Function LlenarCeros(monto As Decimal, length As Integer) As String
        Dim decString As String = Math.Round(monto, 2).ToString().Replace(".", "")
        Return decString.ToString.PadLeft(length, "0")
    End Function

    Private Function LlenarCeros(cadena As String, length As Integer) As String
        Return cadena.ToString.PadLeft(length, "0")
    End Function

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

    End Sub
End Class
