﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false"
    CodeFile="DeMoCuBa.aspx.vb" Inherits="TES_DeMoCuBa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblIdServicio" runat="server" Style="display: none;" Text="">
    </asp:Label>
    <h2>
        Detalle del Movimiento de Cuenta</h2>
    <div class="conten_pasos3">
        <asp:Panel ID="pInfoGeneral" runat="server">
            <h4>
                Datos del Movimiento
            </h4>
            <ul class="datos_cip2">
                <li class="t1"><span class="color">&gt;&gt;</span><b> Banco :</b>
                    <asp:Label ID="lblIdOrdenPago" CssClass="corta" Visible="false" runat="server" Text=""></asp:Label>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtBanco" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <ul class="datos_cip4" style="margin-top: 10px">
                <li class="t1"><span class="color">&gt;&gt;</span><b> N&uacute;mero de Operacion :</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtNumeroOperacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Moneda:</b> </li>
                <li class="t2">
                    <asp:TextBox ID="txtMoneda" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <ul class="datos_cip4" style="margin-top: 10px">
                <li class="t1"><span class="color">&gt;&gt;</span><b> CuentaComercio : </b></li>
                <li class="t2">
                    <asp:TextBox ID="txtCuentaComercio" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
                <li class="t1"><span class="color">&gt;&gt;</span> <b>Fecha Operaci&oacute;n :</b>
                </li>
                <li class="t2">
                    <asp:TextBox ID="txtFechaOperacion" runat="server" CssClass="corta" ReadOnly="true"></asp:TextBox>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
            <h4>
                Movimientos Virtuales
            </h4>
            <div class="cont_cel">
                <asp:UpdatePanel ID="UpdatePanelResultado" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grvDetalleMov" ShowFooter="False" runat="server" AllowSorting="True"
                            AllowPaging="True" AutoGenerateColumns="False" CssClass="grilla" PageSize="15">
                            <Columns>
                                <asp:BoundField DataField="IdMovimientoCuentaMonedero" SortExpression="IdMovimientoCuentaMonedero"
                                    HeaderText="Num. Mov."></asp:BoundField>
                                <asp:BoundField DataField="CuentaDineroVirtual" SortExpression="CuentaDineroVirtual"
                                    HeaderText="Nro. Cuenta"></asp:BoundField>
                                <asp:TemplateField HeaderText="Fecha Operacion" SortExpression="FechaRegistro">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.DatetimeToStringDateandTime(DataBinder.Eval(Container.DataItem, "FechaRegistro"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Monto" SortExpression="Monto">
                                    <ItemTemplate>
                                        <%# SPE.Web.Util.UtilFormatGridView.ObjectDecimalToStringFormatMiles(DataBinder.Eval(Container.DataItem, "Monto"))%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="ArchivoDescripcion" SortExpression="ArchivoDescripcion" HeaderText="Archivo">
                                </asp:BoundField>--%>
                                <asp:BoundField DataField="TipoMovimientoDescripcion" SortExpression="TipoMovimientoDescripcion"
                                    HeaderText="Tipo de Mov."></asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="cabecera" />
                            <EmptyDataTemplate>
                                No se encontraron registros.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <fieldset id="fisUpload" class="ContenedorEtiquetaTexto" runat="server" visible="false">
            <div id="div9" class="EtiquetaTextoIzquierda">
                <asp:Label ID="lblUploadCarga" runat="server" Text="" Style="color: Red"></asp:Label>
            </div>
        </fieldset>
        <br />
        <asp:Label ID="lblMensaje" runat="server" Width="650px" CssClass="MensajeTransaccion"
            Style="padding-bottom: 5px"></asp:Label>
        <%--<asp:Label ID="lblMensaje" CssClass="MensajeTransaccion" runat="server" Height="25px"
            Style="padding-top: 0px">
        </asp:Label>--%>
        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="GrupoValidacion"
            runat="server" />
        <ul class="datos_cip2">
            <li class="complet">
                <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul3" Text="Regresar" />
            </li>
        </ul>
    </div>
</asp:Content>
