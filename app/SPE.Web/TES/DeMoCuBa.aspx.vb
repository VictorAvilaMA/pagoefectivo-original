﻿Imports SPE.Web

Partial Class TES_DeMoCuBa
    Inherits _3Dev.FW.Web.MaintenancePageBase(Of SPE.Web.CCuenta)

#Region "Propiedades"
    Private objCParametro As CAdministrarParametro
    Private Function InstanciaParametro() As CAdministrarParametro
        If objCParametro Is Nothing Then
            objCParametro = New CAdministrarParametro()
        End If
        Return objCParametro
    End Function

    Protected Overrides ReadOnly Property BtnCancel() As System.Web.UI.WebControls.Button
        Get
            Return btnCancelar
        End Get
    End Property
    Protected Overrides ReadOnly Property LblMessageMaintenance() As System.Web.UI.WebControls.Label
        Get
            Return lblMensaje
        End Get
    End Property
#End Region

#Region "Métodos base"
    Protected Overrides Sub ConfigureMaintenance(ByVal e As _3Dev.FW.Web.ConfigureMaintenanceArgs)
        e.URLPageCancelForSave = "COMoCuBa.aspx"
        e.URLPageCancelForInsert = "COMoCuBa.aspx"
        e.URLPageCancelForEdit = "COMoCuBa.aspx"
        e.UseEntitiesMethod = True
    End Sub

    Protected Overrides Sub MaintenancePage_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        MyBase.MaintenancePage_Load(sender, e)
        If Not IsPostBack Then
        End If
    End Sub

    Public Overrides Sub OnFirstLoadPage()
        MyBase.OnFirstLoadPage()
    End Sub

    Public Overrides Sub OnAfterLoadInformation()
        MyBase.OnAfterLoadInformation()
    End Sub

    Protected Overrides Sub OnInitComplete(ByVal e As System.EventArgs)
        MyBase.OnInitComplete(e)
    End Sub

    Public Overrides Sub OnMainCancel()
        MyBase.OnMainCancel()
    End Sub
#End Region

    Public Overrides Sub OnLoadInformation()

    End Sub


#Region "Métodos"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim objCParametro As New SPE.Web.CAdministrarParametro()
            Dim objCCuenta As New SPE.Web.CCuenta
            Dim textoPorDefecto As String = " - "
            Dim listaMovimientos As New System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase)
            Dim id As String = VariableTransicion.ToString()

            If id <> "" Then
                Dim obeDetMovimientoCuenta As New SPE.Entidades.BEDetMovimientoCuenta
                obeDetMovimientoCuenta.IdMovimientoCuentaBanco = Convert.ToInt32(id)
                listaMovimientos = objCCuenta.ConsultarDetMovCuentaPorIdMovCuentaBanco(obeDetMovimientoCuenta)
                grvDetalleMov.DataSource = listaMovimientos
                grvDetalleMov.DataBind()

                If Session("beMovimientoCuentaToDetalle") IsNot Nothing Then
                    Dim beMovimientoCuenta As SPE.Entidades.BEMovimientoCuentaBanco = CType(Session("beMovimientoCuentaToDetalle"), SPE.Entidades.BEMovimientoCuentaBanco)
                    txtBanco.Text = beMovimientoCuenta.DescripcionBanco
                    txtCuentaComercio.Text = beMovimientoCuenta.CuentaComercio
                    txtFechaOperacion.Text = beMovimientoCuenta.FechaMovimiento.ToString("dd/MM/yyyy")
                    txtMoneda.Text = beMovimientoCuenta.CodMonedaBanco
                    txtNumeroOperacion.Text = beMovimientoCuenta.NumeroOperacion
                    Session.Remove("beMovimientoCuentaToDetalle")
                End If
            Else
                Dim idUri As String = VariableTransicion.ToString.Split("|"c)(1)
                Dim ReturnPage As String = "COMoCuBa.aspx"
                JSMessageAlertWithRedirect("Alerta: ", "La Información de la vista anterior no llego correctamente", "bb", ReturnPage)
            End If
        End If
    End Sub
#End Region

    Public Sub JSMessageAlertWithRedirect(ByVal tipo_alerta As String, ByVal mensaje As String, ByVal key As String, ByVal PageRedirect As String)
        Dim script As String = String.Format("alert('{0}: {1}'); window.location.href='{2}';", tipo_alerta, mensaje, PageRedirect)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), key, script, True)
    End Sub

End Class
