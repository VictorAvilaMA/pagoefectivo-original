<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master"
    AutoEventWireup="false" CodeFile="PgPrl.aspx.vb" Inherits="ECO_PgPrl" Title="PagoEfectivo - P�gina Principal"
    EnableEventValidation="false" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        #ctl00_ContentPlaceHolder1_pnlPopupActualizarTransferencia li
        {
            margin-bottom: 5px;
        }
        #ctl00_ContentPlaceHolder1_UpdatePanel12
        {
            position: relative;
            z-index: 10;
        }
        #ctl00_ContentPlaceHolder1_UpdatePanel12 ul.datos_cip2
        {
            z-index: 10;
        }
    </style>

    <asp:ScriptManager ID="ScriptManager" runat="server" EnableScriptLocalization="True"
        EnableScriptGlobalization="True" AsyncPostBackTimeOut= "3600">
    </asp:ScriptManager>
    <h2>
        Liquidar Transacciones Pendientes</h2>
    <div class="conten_pasos3">
        <h4>
            Criterios de b�squeda
        </h4>
        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
            <ContentTemplate>
                <ul class="datos_cip2">
                    <li class="t1"><span class="color">&gt;&gt;</span> Empresa :</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="neu" AutoPostBack="True">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Moneda:</li>
                    <li class="t2">
                        <asp:DropDownList ID="ddlMoneda" runat="server" CssClass="neu" AutoPostBack="True">
                        </asp:DropDownList>
                    </li>
                    <li class="t1"><span class="color">&gt;&gt;</span> Fecha Del :</li>
                    <li class="t2" style="line-height: 1; z-index: 200;">
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaDe" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaDe" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderDe" runat="server" MaskType="Date"
                                Mask="99/99/9999" TargetControlID="txtFechaDe" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <span class="entre">al: </span>
                        <p class="input_cal">
                            <asp:TextBox ID="txtFechaA" runat="server" CssClass="corta"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFechaA" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                CssClass="calendario"></asp:ImageButton>
                            <cc1:MaskedEditExtender ID="MaskedEditExtenderA" runat="server" MaskType="Date" Mask="99/99/9999"
                                TargetControlID="txtFechaA" CultureName="es-PE">
                            </cc1:MaskedEditExtender>
                        </p>
                        <cc1:CalendarExtender ID="CalendarExtenderDe" runat="server" TargetControlID="txtFechaDe"
                            PopupButtonID="ibtnFechaDe" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="CalendarExtenderA" runat="server" TargetControlID="txtFechaA"
                            PopupButtonID="ibtnFechaA" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorDe" runat="server" ControlExtender="MaskedEditExtenderDe"
                            ControlToValidate="txtFechaDe" ErrorMessage="*" InvalidValueMessage="Fecha 'Del' no v�lida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha 'Del' es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="ValidaCampos"></cc1:MaskedEditValidator>
                        <cc1:MaskedEditValidator ID="MaskedEditValidatorA" runat="server" ControlExtender="MaskedEditExtenderA"
                            ControlToValidate="txtFechaA" ErrorMessage="*" InvalidValueMessage="Fecha 'Al' no v�lida"
                            IsValidEmpty="False" EmptyValueMessage="Fecha 'Al' es requerida" Display="Dynamic"
                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="ValidaCampos"></cc1:MaskedEditValidator>
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" />
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Rango de fechas no es v�lido"
                            ControlToCompare="txtFechaA" ControlToValidate="txtFechaDe" Operator="LessThanEqual"
                            Type="Date" ValidationGroup="ValidaCampos">*</asp:CompareValidator>
                    </li>
                    <div style="clear: both;">
                    </div>
                    <asp:Label ID="lblResultado" runat="server" CssClass="MensajeValidacion" Font-Bold="True"
                        ForeColor="Navy"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanelImage" runat="server" UpdateMode="Conditional" RenderMode="Inline"
            ChildrenAsTriggers="False">
            <ContentTemplate>
                <ul class="datos_cip2 h0">
                    <li class="complet">
                        <asp:Button ID="btnConsultar" runat="server" CssClass="input_azul3" ValidationGroup="ValidaCampos"
                            Text="Generar" />
                        <asp:Button ID="BtnLimpiar" runat="server" CssClass="input_azul4" Text="Limpiar"
                            ValidationGroup="ValidaCampos" />
                    </li>
                    <li class="t1"></li>
                    <li id="liNoConsiderados" class="t2" runat="server" visible="false">
                        <asp:Literal ID="ltlAviso" runat="server"></asp:Literal><asp:LinkButton ID="lnkVerNoContemplados"
                            runat="server">Ver detalle</asp:LinkButton>
                    </li>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnConsultar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="BtnLimpiar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPActualizar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPCancelar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnCancelar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ddlEmpresa" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlMoneda" EventName="SelectedIndexChanged" />
                <asp:PostBackTrigger ControlID="lnkVerNoContemplados"></asp:PostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div style="clear: both;">
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanelResultado" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divresumen" runat="server" visible="false" class="cont_cel2">
                    <h6>
                        Resumen de Operaciones y Montos</h6>
                    <ul class="datos_cip2">
                        <li class="t1"><span class="color">&gt;&gt;</span> Nro. Operaciones:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtNumOpe" ReadOnly="True" CssClass="text" runat="server">
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Total de Pagos:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtTotalPago" ReadOnly="True" CssClass="text" runat="server">
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Total de Comisi&oacute;n:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtTotalComi" ReadOnly="True" CssClass="text" runat="server">
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Moneda:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtMoneda" ReadOnly="True" CssClass="text" runat="server">
                            </asp:TextBox>
                        </li>
                        <li class="t1"><span class="color">&gt;&gt;</span> Total a Pagar:</li>
                        <li class="t2">
                            <asp:TextBox ID="txtPagar" ReadOnly="True" CssClass="text" runat="server">
                            </asp:TextBox>
                        </li>
                        <li class="t1"></li>
                        <li class="t2">
                            <asp:LinkButton ID="lnkVerDetalle" runat="server">Ver Detalle</asp:LinkButton>
                        </li>
                        <asp:Label ID="lblResultadoNo" runat="server" CssClass="MensajeTransaccion" Visible="false"></asp:Label>
                        <%--<asp:Button ID="lnkVerDetalle" runat="server" CssClass="btnExcel" />--%>
                        <li class="t2">
                            <asp:Button ID="btnRegistrar" runat="server" CssClass="input_azul4" Text="Continuar" />
                            <asp:Button ID="btnCancelar" runat="server" CssClass="input_azul4" OnClientClick="return CancelMe();"
                                Text="Cancelar" />
                        </li>
                    </ul>
                </div>
                <cc1:ModalPopupExtender ID="mppTransferencia" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="imgbtnRegresarHidden" PopupControlID="pnlPopupActualizarTransferencia"
                    TargetControlID="imgbtnTargetHidden">
                </cc1:ModalPopupExtender>
                <asp:ImageButton ID="imgbtnRegresarHidden" runat="server" Style="display: none;"
                    CssClass="Hidden" CausesValidation="false"></asp:ImageButton>
                <asp:ImageButton ID="imgbtnTargetHidden" runat="server" Style="display: none;" CssClass="Hidden"
                    CausesValidation="false"></asp:ImageButton>
                <asp:Panel ID="pnlPopupActualizarTransferencia" runat="server" class="conten_pasos3"
                    Style="display: none; background: white">
                    <div style="float: right">
                        <asp:ImageButton ID="imgbtnRegresar" OnClick="imgbtnRegresar_Click" runat="server"
                            ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/img/closeX.GIF" %>'
                            CausesValidation="false"></asp:ImageButton>
                    </div>
                    <h4>
                        Datos de Transferencia:</h4>
                    <ul class="datos_cip2">
                        <li class="t1" style="font-weight: bold"><span class="color">>></span> Empresa:
                        </li>
                        <li class="t2">
                            <asp:Literal ID="txtPEmpresa" runat="server">
                            </asp:Literal>
                            <asp:Label ID="lblIdTransferencia" Visible="false" runat="server" CssClass="MensajeValidacion"></asp:Label>
                        </li>
                        <li class="t1" style="font-weight: bold"><span class="color">>></span>Estado: </li>
                        <li class="t2">
                            <asp:Literal ID="txtPEstado" runat="server">
                            </asp:Literal>
                        </li>
                        <li class="t1" style="font-weight: bold"><span class="color">>></span> Fecha del:
                        </li>
                        <li class="t2">
                            <p class="input_cal">
                                <asp:TextBox ID="txtPDel" runat="server" CssClass="corta" Enabled="false"></asp:TextBox>
                            </p>
                            <span class="entre" style="font-weight: bold">al</span>
                            <p class="input_cal">
                                <asp:TextBox ID="txtPAl" runat="server" CssClass="corta" Enabled="false"></asp:TextBox>
                            </p>
                        </li>
                        <li class="t1" style="font-weight: bold"><span class="color">>></span> N&uacute;mero
                            de Operaciones: </li>
                        <li class="t2">
                            <p class="input_cal2">
                                <asp:Literal ID="txtPTotalOperaciones" runat="server"></asp:Literal>
                            </p>
                            <span class="entre" style="width: 75px; font-weight: bold;">Moneda :</span>
                            <p class="input_cal2" style="width: 200px">
                                <asp:Literal ID="txtPMoneda" runat="server"></asp:Literal>
                            </p>
                        </li>
                        <li class="t1" style="font-weight: bold"><span class="color">>></span> Total Comisi&oacute;n:
                        </li>
                        <li class="t2">
                            <p class="input_cal2">
                                <asp:Literal ID="txtPTComision" runat="server"></asp:Literal>
                            </p>
                            <span class="entre2" style="font-weight: bold">Total de Pagos:</span>
                            <p class="input_cal2">
                                <asp:Literal ID="txtPTPagos" runat="server"></asp:Literal>
                            </p>
                        </li>
                    </ul>
                    <div style="clear: both">
                    </div>
                    <div style="display: none">
                        <h4>Datos Arch. de Conciliaci&oacute;n:</h4>
                        <ul class="datos_cip2">
                            <li class="t1" style="font-weight: bold"><span class="color">>></span> Fecha de Dep&oacute;sito:
                            </li>
                            <li class="t2" style="line-height: 1; z-index: 200;">
                                <p class="input_cal">
                                    <asp:TextBox ID="txtFechaDeposito" runat="server" CssClass="corta"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") +"/App_Themes/SPE/img/calendario.png" %>'
                                        CssClass="calendario"></asp:ImageButton>
                                </p>
                                <span class="entre2" style="font-weight: bold">Nro. Operaci&oacute;n:</span>
                                <p class="input_cal2">
                                    <asp:TextBox ID="txtPNroOperacion" runat="server" CssClass="corta" MaxLength="50"></asp:TextBox>
                                </p>
                            </li>
                            <li class="t1" style="font-weight: bold"><span class="color">>></span> Banco: </li>
                            <li class="t2">
                                <p class="input_cal2" style="width: 133px">
                                    <asp:TextBox ID="txtBanco" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                                </p>
                            </li>
                            <li class="t1" style="font-weight: bold"><span class="color">>></span> Numero de Cuenta:
                            </li>
                            <li class="t2">
                                <p class="input_cal2" style="width: 133px">
                                    <asp:TextBox ID="txtPNroCuenta" runat="server" CssClass="normal" ReadOnly="true"></asp:TextBox>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtenderDe"
                        ControlToValidate="txtFechaDeposito" ErrorMessage="*" InvalidValueMessage="Fecha de Deposito no v�lida"
                        IsValidEmpty="False" EmptyValueMessage="Fecha de Deposito es requerida" Display="Dynamic"
                        EmptyValueBlurredText="*" InvalidValueBlurredMessage="*" ValidationGroup="ValidaTransaccion"></cc1:MaskedEditValidator>
                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtFechaDeposito"
                        Mask="99/99/9999" MaskType="Date" CultureName="es-PE">
                    </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaDeposito"
                        Format="dd/MM/yyyy" PopupButtonID="ImageButton1">
                    </cc1:CalendarExtender>
                    <div style="clear: both">
                    </div>
                    <ul class="datos_cip2 t0">
                        <li class="t1" style="font-weight: bold; display: none;"><span class="color">>></span> Adjuntar Documento:
                        </li>
                        <li class="t2" style="display: none;">
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            <asp:HyperLink ID="lnkURL" Text="Ver Archivo" runat="server" Visible="false"></asp:HyperLink>
                        </li>
                        <li class="t1" style="display: none; font-weight: bold"><span class="color">>></span> Observaci&oacute;n:
                        </li>
                        <li class="t2" style="display: none;width: 100px">
                            <asp:TextBox ID="txtPObservacion" runat="server" TextMode="MultiLine" MaxLength="500"
                                CssClass="Multilinetext"></asp:TextBox>
                        </li>
                        <li class="complet" style="margin-top: 10px">
                            <asp:Button ID="btnPActualizar" runat="server" CssClass="input_azul5" Text="Generar" />
                            <asp:Button ID="btnPCancelar" runat="server" CssClass="input_azul4" Text="Cancelar" />
                        </li>
                    </ul>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnConsultar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="btnRegistrar" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="BtnLimpiar" EventName="Click" />
                <asp:PostBackTrigger ControlID="lnkVerDetalle"></asp:PostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
