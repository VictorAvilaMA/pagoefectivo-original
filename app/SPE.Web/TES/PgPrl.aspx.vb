Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Imports System.Data
Imports SPE.Entidades
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing.Printing
Imports System.Drawing.Imaging
Imports SPE.Web
Imports System.Linq
Imports _3Dev.FW.Web



Partial Class ECO_PgPrl
    'Inherits _3Dev.FW.Web.PageBase
    Inherits SPE.Web.PaginaBase
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''JV
        'ScriptManager.GetCurrent(Me).AsyncPostBackTimeout = timeOut

        If Not Page.IsPostBack Then
            '
            Page.SetFocus(ddlEmpresa)
            Page.Form.DefaultButton = btnConsultar.UniqueID

            CargarDatos()
            '
        End If
    End Sub

    Private Sub InicializarFechas()
        'ObtenerFechaInicioTransaccionesaLiquidar
        Dim objValidaFecha As New SPE.Web.CEmpresaContratante
        Dim dtFechaDe As DateTime
        Dim dtFechaHasta As DateTime
        Dim objTransferencia As New BETransferencia
        objTransferencia.idEmpresaContratante = ddlEmpresa.SelectedValue
        objTransferencia.IdMoneda = ddlMoneda.SelectedValue
        dtFechaDe = CDate(objValidaFecha.ObtenerFechaInicioTransaccionesaLiquidar(objTransferencia))
        dtFechaHasta = CDate(objValidaFecha.ObtenerFechaFinTransaccionesaLiquidar(objTransferencia))
        ltlAviso.Visible = False
        lnkVerNoContemplados.Visible = False
        liNoConsiderados.Visible = False
        If (dtFechaDe.CompareTo(dtFechaHasta) > 0) Then
            lblResultado.Text = "El periodo aun no se ha cumplido"
            txtFechaDe.Text = IIf(dtFechaDe = DateTime.MinValue, New DateTime(2009, 1, 1), dtFechaDe)
            lblResultadoNo.Visible = True
        Else
            txtFechaDe.Text = IIf(dtFechaDe = DateTime.MinValue, New DateTime(2009, 1, 1), dtFechaDe)
            txtFechaA.Text = IIf(dtFechaHasta = DateTime.MinValue, DateTime.Now.AddDays(-1), dtFechaHasta.AddDays(-1))
            txtFechaDeposito.Text = Date.Now.ToShortDateString
            lblResultado.Text = String.Empty
            lblResultadoNo.Visible = False
        End If
    End Sub

    'CARGAR DATOS
    Private Sub CargarDatos()
        '
        'FECHAS


        Dim objComun As New SPE.Web.CAdministrarComun
        Dim objParametro As New SPE.Web.CAdministrarParametro
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim objCComun As New SPE.Web.CComun

        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
        Using ocntrlEmpresaContrante As New CEmpresaContratante()
            Dim oBEEmpresaContratante As New BEEmpresaContratante() 'asd
            oBEEmpresaContratante.Codigo = ""
            oBEEmpresaContratante.RazonSocial = ""
            oBEEmpresaContratante.RUC = ""
            oBEEmpresaContratante.IdEstado = 41
            oBEEmpresaContratante.Email = ""
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlEmpresa, ocntrlEmpresaContrante.SearchByParameters(oBEEmpresaContratante), "RazonSocial", "IdEmpresaContratante", "::Seleccione::")
        End Using
    End Sub



    Sub Limpiar()
        Me.ddlEmpresa.SelectedIndex = 0
        Me.ddlMoneda.SelectedIndex = 0
        'habilitarOpciones(False)
        txtNumOpe.Text = String.Empty
        txtTotalComi.Text = String.Empty
        txtTotalPago.Text = String.Empty
        txtMoneda.Text = String.Empty
        txtPagar.Text = String.Empty
        lblResultado.Text = String.Empty
        txtFechaDe.Text = String.Empty
        txtFechaA.Text = String.Empty
        ibtnFechaA.Enabled = True
        ddlEmpresa.Enabled = True
        ddlMoneda.Enabled = True
        'txtFechaDe.Enabled = True
        'ibtnFechaDe.Enabled = True
        'txtFechaA.Enabled = True
        'ibtnFechaA.Enabled = True
        liNoConsiderados.Visible = False
        ltlAviso.Visible = False
        ltlAviso.Text = String.Empty
        lnkVerNoContemplados.Visible = False
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        Limpiar()
        divresumen.Visible = False
        btnConsultar.Enabled = True
    End Sub
    Protected Sub btnConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultar.Click

        'asd
        If ddlEmpresa.SelectedValue = String.Empty Then
            lblResultado.Text = MensajesValidacion.MensajeEmpresaReq
            Exit Sub
        End If

        If ddlMoneda.SelectedValue = String.Empty Then
            lblResultado.Text = MensajesValidacion.MensajeMonedaReq
            Exit Sub
        End If

        Dim dtFchA As DateTime = CDate(txtFechaA.Text)
        Dim beServicio As New BEServicio
        beServicio.IdEmpresaContratante = ddlEmpresa.SelectedValue
        beServicio.IdMoneda = ddlMoneda.SelectedValue
        Dim objEmpresaContratante As New CEmpresaContratante
        Dim objServicio As New SPE.Web.CServicio
        Dim objResultado As New BEServicio
        Dim intValida As Integer = 0

        Dim objTransferencia As New BETransferencia
        objTransferencia.idEmpresaContratante = ddlEmpresa.SelectedValue
        objTransferencia.IdMoneda = ddlMoneda.SelectedValue
        objTransferencia.FechaInicio = CDate(txtFechaDe.Text)
        objTransferencia.FechaFin = CDate(txtFechaA.Text)

        intValida = objEmpresaContratante.ValidarPendientesaConciliar(objTransferencia)
        Dim val As Boolean = intValida > 0
        liNoConsiderados.Visible = val
        ltlAviso.Visible = val
        lnkVerNoContemplados.Visible = val
        ltlAviso.Text = String.Format("Existen {0} pagos no considerados en el periodo, ", intValida)

        objResultado = objServicio.ConsultarTransaccionesxServicio(beServicio, CDate(txtFechaDe.Text), CDate(txtFechaA.Text))

        If Not IsNothing(objResultado) Then
            divresumen.Visible = True
            txtNumOpe.Text = objResultado.CantidadLimite
            txtTotalPago.Text = objResultado.MontoComision.ToString("#,#0.00")
            txtTotalComi.Text = objResultado.PorcentajeComision.ToString("#,#0.00")
            txtMoneda.Text = objResultado.DescripcionMoneda
            txtPagar.Text = (objResultado.MontoComision - objResultado.PorcentajeComision).ToString("#,#0.00")
            'habilitarOpciones(True)
            lblResultadoNo.Visible = False
            controlesAlBuscar(False)
            'btnConsultar.Enabled = False
            btnRegistrar.Enabled = True
            ibtnFechaA.Enabled = False
            lblResultado.Text = String.Empty
            lblResultadoNo.Visible = False

            'divresumen.Visible = False
            'Dim mns As New MensajesValidacion
            'lblResultado.Text = mns.MensajeResultadoReportePS(txtFechaDe.Text, txtFechaA.Text)
            'lblResultadoNo.Visible = True

        Else
            lblResultado.Text = MensajesError.MensajeTransaccionPendiente
        End If



    End Sub

    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        If CInt(txtNumOpe.Text) > 0 Then
            Dim beServicio As New BEServicio
            beServicio.IdEmpresaContratante = ddlEmpresa.SelectedValue
            beServicio.IdMoneda = ddlMoneda.SelectedValue
            Dim objEmpresaContratante As New CEmpresaContratante
            Dim objServicio As New SPE.Web.CServicio
            Dim objResultado As New BEServicio
            Dim intValida As Integer = 0

            Dim objTransferencia As New BETransferencia
            objTransferencia.idEmpresaContratante = ddlEmpresa.SelectedValue
            objTransferencia.IdMoneda = ddlMoneda.SelectedValue
            objTransferencia.FechaInicio = CDate(txtFechaDe.Text)
            objTransferencia.FechaFin = CDate(txtFechaA.Text)

            intValida = objEmpresaContratante.ValidarPendientesaConciliar(objTransferencia)

            objResultado = objServicio.ConsultarTransaccionesxServicio(beServicio, CDate(txtFechaDe.Text), CDate(txtFechaA.Text))

            If Not IsNothing(objResultado) Then
                txtPEmpresa.Text = ddlEmpresa.SelectedItem.Text.ToString
                txtPMoneda.Text = ddlMoneda.SelectedItem.Text.ToString
                txtPAl.Text = CDate(txtFechaA.Text).ToShortDateString
                txtPDel.Text = CDate(txtFechaDe.Text).ToShortDateString
                txtPTComision.Text = objResultado.PorcentajeComision
                txtPTotalOperaciones.Text = objResultado.CantidadLimite
                txtPTPagos.Text = objResultado.MontoComision
                txtPEstado.Text = SPE.EmsambladoComun.ParametrosSistema.Registrar
                Dim objCEmpresaCon As New SPE.Web.CEmpresaContratante
                Dim objbeCuentaEmpresa As New SPE.Entidades.BECuentaEmpresa
                With objbeCuentaEmpresa
                    .IdEmpresaContratante = ddlEmpresa.SelectedValue
                    .IdMoneda = ddlMoneda.SelectedValue
                    .PageSize = 1
                    .PageNumber = 1
                End With
                Dim listCuentaEmpresa As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = objCEmpresaCon.ConsultarCuentaEmpresa(objbeCuentaEmpresa)
                If listCuentaEmpresa.Count > 0 Then
                    txtBanco.Text = CType(listCuentaEmpresa(0), BECuentaEmpresa).Banco
                    txtPNroCuenta.Text = CType(listCuentaEmpresa(0), BECuentaEmpresa).NroCuenta
                End If

                'WebUtil.DropDownlistBinding(ddlPBanco, objCBanco.ConsultarBanco(oBEBanco), "Descripcion", "idBanco", "::Seleccione::")
                pnlPopupActualizarTransferencia.Visible = True
                mppTransferencia.Show()
            Else
                lblResultado.Text = MensajesError.MensajeTransaccionPendiente
            End If

        End If

    End Sub
    Private Sub controlesAlBuscar(ByVal flag As Boolean)
        'txtFechaDe.Enabled = False
        'txtFechaA.Enabled = False
        ddlEmpresa.Enabled = flag
        ddlMoneda.Enabled = flag
    End Sub
    Private Sub CargarPopup(ByVal id As Integer)
        Dim objTransferencia As New BETransferencia
        Dim objT As New BETransferencia
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        objT.idTransfenciaEmpresas = id
        objTransferencia = objProcesar.ObtenerTransaccionesaLiquidar(objT)
        If Not IsNothing(objTransferencia) Then
            txtPEmpresa.Text = objTransferencia.DescripcionEmpresa
            txtPMoneda.Text = objTransferencia.DescripcionMoneda
            txtPAl.Text = CDate(objTransferencia.FechaInicio).ToShortDateString
            txtPDel.Text = CDate(objTransferencia.FechaFin).ToShortDateString
            txtPNroCuenta.Text = objTransferencia.NumeroCuenta
            txtPNroOperacion.Text = objTransferencia.NumeroOperacion
            txtPObservacion.Text = objTransferencia.Observacion
            txtPTComision.Text = objTransferencia.TotalComision
            txtPTotalOperaciones.Text = objTransferencia.TotalOperaciones
            txtPTPagos.Text = objTransferencia.TotalPago
            txtPEstado.Text = objTransferencia.DescipcionEstadoT
            lblIdTransferencia.Text = objTransferencia.idTransfenciaEmpresas
            Dim objCEmpresaCon As New SPE.Web.CEmpresaContratante
            Dim objbeCuentaEmpresa As New SPE.Entidades.BECuentaEmpresa
            With objbeCuentaEmpresa
                .IdEmpresaContratante = ddlEmpresa.SelectedValue
                .IdMoneda = ddlMoneda.SelectedValue
                .PageSize = 1
                .PageNumber = 1
            End With
            Dim listCuentaEmpresa As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = objCEmpresaCon.ConsultarCuentaEmpresa(objbeCuentaEmpresa)
            If listCuentaEmpresa.Count > 0 Then
                txtBanco.Text = CType(listCuentaEmpresa(0), BECuentaEmpresa).Banco
                txtPNroCuenta.Text = CType(listCuentaEmpresa(0), BECuentaEmpresa).NroCuenta
            End If
            pnlPopupActualizarTransferencia.Visible = True
            mppTransferencia.Show()
        End If
    End Sub
    Private Function getEntityFilled() As BETransferencia
        Dim objTransferencia As New BETransferencia
        objTransferencia.idEmpresaContratante = ddlEmpresa.SelectedValue
        objTransferencia.IdMoneda = ddlMoneda.SelectedValue
        objTransferencia.FechaInicio = CDate(txtFechaDe.Text)
        objTransferencia.FechaFin = CDate(txtFechaA.Text)
        objTransferencia.TotalOperaciones = CInt(txtNumOpe.Text)
        objTransferencia.TotalComision = CDec(txtTotalComi.Text)
        objTransferencia.TotalPago = CDec(txtTotalPago.Text)
        objTransferencia.IdUsuarioCreacion = UserInfo.IdUsuario

        objTransferencia.DescripcionBanco = txtBanco.Text

        If txtFechaDeposito.Text = String.Empty Then
            'objTransferencia.FechaDeposito 
        Else
            objTransferencia.FechaDeposito = CDate(txtFechaDeposito.Text)
        End If

        objTransferencia.NumeroOperacion = txtPNroOperacion.Text
        objTransferencia.NumeroCuenta = txtPNroCuenta.Text
        objTransferencia.Observacion = txtPObservacion.Text
        If FileUpload1.HasFile Then
            objTransferencia.ArchivoAdjunto = String.Empty
        Else
            Dim nombreFile As String
            Dim ftp As New UtilManagerFTP
            Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
            Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
            Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")

            nombreFile = DateTime.Today.Year.ToString() + "_" + DateTime.Today.Month.ToString() + "_" + DateTime.Today.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + FileUpload1.FileName
            Try
                ftp.UploadFile(FileUpload1.FileContent, FTPTesoreria + nombreFile, FTPUser, FTPPass)
                objTransferencia.ArchivoAdjunto = nombreFile
            Catch ex As Exception
                Throw ex
            End Try
        End If

        objTransferencia.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoPeriodoPagoServicio.Registrado
        Return (objTransferencia)
    End Function

    Protected Sub btnPCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPCancelar.Click
        'controlesAlBuscar(True)
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Limpiar()
        divresumen.Visible = False
        controlesAlBuscar(True)
        liNoConsiderados.Visible = False
        btnCancelar.Enabled = True
        btnRegistrar.Enabled = False
    End Sub

    Protected Sub btnPActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPActualizar.Click

        Dim objTransferencia As New BETransferencia
        objTransferencia = getEntityFilled()
        Dim objProcesar As New SPE.Web.CEmpresaContratante
        Dim obMensaje As New MensajeInformacion
        Dim intResultado As Integer = 0
        intResultado = objProcesar.RegistrarTransaccionesaLiquidar(objTransferencia)
        If intResultado > 0 Then
            lblResultado.Text = obMensaje.MensajeOkConOperacion(intResultado)
            JSMessageAlert("Mensaje:", obMensaje.MensajeOkConOperacion(intResultado), "1")
            Limpiar()
            divresumen.Visible = False
            controlesAlBuscar(True)
            btnCancelar.Enabled = True
            btnRegistrar.Enabled = False

        Else
            lblResultado.Text = MensajesError.MensajeTransaccionRango
        End If

    End Sub

    Private Function FillEntityforUpdateT(ByVal intEstado As Integer) As BETransferencia
        Dim objTransferencia As New BETransferencia
        objTransferencia.idTransfenciaEmpresas = lblIdTransferencia.Text
        objTransferencia.IdUsuarioActualizacion = UserInfo.IdUsuario

        objTransferencia.DescripcionBanco = txtBanco.Text

        If CDate(txtFechaDeposito.Text) = String.Empty Then
            objTransferencia.FechaDeposito = DateTime.MinValue.ToShortDateString
        Else
            objTransferencia.FechaDeposito = CDate(txtFechaDeposito.Text)
        End If

        objTransferencia.NumeroOperacion = txtPNroOperacion.Text
        objTransferencia.NumeroCuenta = txtPNroCuenta.Text
        objTransferencia.Observacion = txtPObservacion.Text
        If FileUpload1.HasFile Then
            objTransferencia.ArchivoAdjunto = String.Empty
        Else

            Dim nombreFile As String
            Dim ftp As New UtilManagerFTP
            Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
            Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
            Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")

            nombreFile = DateTime.Today.Year.ToString() + "_" + DateTime.Today.Month.ToString() + "_" + DateTime.Today.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + FileUpload1.FileName
            Try
                ftp.UploadFile(FileUpload1.FileContent, FTPTesoreria + nombreFile, FTPUser, FTPPass)
                objTransferencia.ArchivoAdjunto = nombreFile
            Catch ex As Exception
                Throw ex
            End Try
        End If
        objTransferencia.IdEstado = intEstado
        Return objTransferencia
    End Function


    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        mppTransferencia.Hide()
    End Sub

    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        If Not ddlEmpresa.SelectedValue = String.Empty Then
            If ddlMoneda.SelectedValue = String.Empty Then
                'lblResultado.Text = MensajesValidacion.MensajeMonedaReq
            Else : InicializarFechas()
            End If
            'Else : lblResultado.Text = MensajesValidacion.MensajeEmpresaReq
        End If

    End Sub

    Protected Sub ddlMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMoneda.SelectedIndexChanged
        If Not ddlMoneda.SelectedValue = String.Empty Then

            If ddlEmpresa.SelectedValue = String.Empty Then
                'lblResultado.Text = MensajesValidacion.MensajeEmpresaReq
            Else : InicializarFechas()
            End If
            'Else : lblResultado.Text = MensajesValidacion.MensajeMonedaReq
        End If

    End Sub

    Protected Sub lnkVerDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkVerDetalle.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            'ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            'Response.Redirect(String.Format("~/Reportes/RptDetalleTransaccionPendiente.aspx?idEm={0}&idMo={1}&Fini={2}&Ffin={3}", _
            '                                ddlEmpresa.SelectedValue, ddlMoneda.SelectedValue, txtFechaA.Text.TrimEnd, txtFechaDe.Text.TrimEnd))
            ExportarExcel(True)
        End If 'asd
    End Sub

    Protected Sub lnkVerNoContemplados_Click(sender As Object, e As System.EventArgs) Handles lnkVerNoContemplados.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
            'ThrowErrorMessage("Usted no tienen permisos para exportar el excel .")
        Else
            'Response.Redirect(String.Format("~/Reportes/RptDetalleTransaccionPendiente.aspx?idEm={0}&idMo={1}&Fini={2}&Ffin={3}", _
            '                                ddlEmpresa.SelectedValue, ddlMoneda.SelectedValue, txtFechaA.Text.TrimEnd, txtFechaDe.Text.TrimEnd))

            ExportarExcel(False)
        End If 'asd
    End Sub
    Protected Sub ExportarExcel(ByVal FlagConciliado As Boolean)
        Dim objCServicio As New SPE.Web.CServicio
        Dim obeServicio As New SPE.Entidades.BETransferencia

        Dim ListaTrans As New List(Of BETransferencia)
        obeServicio.idEmpresaContratante = CInt(ddlEmpresa.SelectedValue)
        obeServicio.IdMoneda = CInt(ddlMoneda.SelectedValue)
        obeServicio.FechaInicio = CDate(txtFechaDe.Text.TrimEnd)
        obeServicio.FechaFin = CDate(txtFechaA.Text.TrimEnd)
        obeServicio.FlagConciliado = FlagConciliado
        ListaTrans = objCServicio.ConsultarDetalleTransaccionesPendientes(obeServicio)


        Dim parametros As New List(Of KeyValuePair(Of String, String))()
        parametros.Add(New KeyValuePair(Of String, String)("Titulo", IIf(FlagConciliado, "Detalle de Transacciones Pendientes", "Detalle de Transacciones no consideradas")))
        parametros.Add(New KeyValuePair(Of String, String)("Empresa", ddlEmpresa.SelectedItem.Text.ToString))
        parametros.Add(New KeyValuePair(Of String, String)("Moneda", ddlMoneda.SelectedItem.Text.ToString))
        parametros.Add(New KeyValuePair(Of String, String)("Hasta", txtFechaA.Text))
        parametros.Add(New KeyValuePair(Of String, String)("Desde", txtFechaDe.Text))
        'parametros.Add(New KeyValuePair(Of String, String)("Total", txtTotalPago.Text))
        parametros.Add(New KeyValuePair(Of String, String)("Total", ListaTrans.Sum(Function(x) x.TotalPago).ToString("#.#0")))
        parametros.Add(New KeyValuePair(Of String, String)("Comision", txtTotalComi.Text))
        parametros.Add(New KeyValuePair(Of String, String)("TotalT", txtPagar.Text))
        UtilReport.ProcesoExportarGenerico(ListaTrans, Page, "EXCEL", "xls", "Detalle Transacciones no consideradas - ", parametros, "RptDetalleTransaccionPendiente.rdlc")
    End Sub
End Class
