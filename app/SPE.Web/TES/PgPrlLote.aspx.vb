﻿Imports SPE.EmsambladoComun.ParametrosSistema
Imports System.IO.Compression
Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Imports System.Data
Imports SPE.Entidades
Imports System.IO
Imports System.Drawing.Printing
Imports System.Drawing.Imaging
Imports SPE.Web
Imports System.Linq
Imports _3Dev.FW.Web
Imports System.Web.UI
Imports System.Web.UI.Page
Imports Ionic.Zip
'Imports OfficeOpenXml
Imports System.Reflection
Imports _3Dev.FW.Web.Log

Partial Class PgPrlLote
    Inherits SPE.Web.PaginaBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            '
            Page.SetFocus(ddlMoneda)
            Page.Form.DefaultButton = btnConsultar.UniqueID

            CargarDatos()
            '
        End If
    End Sub

    Private Sub InicializarFechas()
        'ObtenerFechaInicioTransaccionesaLiquidar
        Dim objValidaFecha As New SPE.Web.CEmpresaContratante
        Dim dtFechaDe As DateTime
        Dim dtFechaHasta As DateTime
        Dim objPerdiodo As New BEPeriodoLiquidacion
        'objTransferencia.idEmpresaContratante = ddlEmpresa.SelectedValue
        objPerdiodo.IdPeriodo = ddlPeriodo.SelectedValue
        dtFechaDe = CDate(objValidaFecha.ObtenerFechaInicioTransaccionesaLiquidarLote(objPerdiodo))
        dtFechaHasta = CDate(objValidaFecha.ObtenerFechaFinTransaccionesaLiquidarLote(objPerdiodo))
        ltlAviso.Visible = False
        lnkVerNoContemplados.Visible = False
        liNoConsiderados.Visible = False

        If (dtFechaDe.CompareTo(dtFechaHasta) > 0) Then
            lblResultado.Text = "El periodo aun no se ha cumplido"
            txtFechaDe.Text = IIf(dtFechaDe = DateTime.MinValue, New DateTime(2009, 1, 1), dtFechaDe)
            lblResultadoNo.Visible = True
        Else
            txtFechaDe.Text = IIf(dtFechaDe = DateTime.MinValue, New DateTime(2009, 1, 1), dtFechaDe)
            txtFechaA.Text = IIf(dtFechaHasta = DateTime.MinValue, DateTime.Now.AddDays(-1), dtFechaHasta.AddDays(-1))
            'txtFechaDeposito.Text = Date.Now.ToShortDateString
            lblResultado.Text = String.Empty
            lblResultadoNo.Visible = False
        End If
    End Sub

    'CARGAR DATOS
    Private Sub CargarDatos()
        '
        'FECHAS


        Dim objComun As New SPE.Web.CAdministrarComun
        Dim objParametro As New SPE.Web.CComun
        Dim objCAdministrarServicio As New SPE.Web.CServicio
        Dim objCComun As New SPE.Web.CComun


        Dim objCAdministrarComun As New SPE.Web.CAdministrarComun
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlMoneda, objCAdministrarComun.ConsultarMoneda(), "Descripcion", "IdMoneda", "::Seleccione::")
        Using ocntrlEmpresaContrante As New CEmpresaContratante()
            Dim oBEEmpresaContratante As New BEEmpresaContratante() 'asd
            oBEEmpresaContratante.Codigo = ""
            oBEEmpresaContratante.RazonSocial = ""
            oBEEmpresaContratante.RUC = ""
            oBEEmpresaContratante.IdEstado = 41
            oBEEmpresaContratante.Email = ""

            _3Dev.FW.Web.WebUtil.DropDownlistBinding(ddlPeriodo, objParametro.ConsultarIdDescripcionPeriodo(), "Descripcion", "IdPeriodo", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        End Using
    End Sub



    Sub Limpiar()
        Me.ddlMoneda.SelectedIndex = 0
        Me.ddlPeriodo.SelectedIndex = 0
        'habilitarOpciones(False)
        txtNumOpe.Text = String.Empty
        txtTotalComi.Text = String.Empty
        txtTotalPago.Text = String.Empty
        txtMoneda.Text = String.Empty
        txtPagar.Text = String.Empty
        lblResultado.Text = String.Empty
        txtFechaDe.Text = String.Empty
        txtFechaA.Text = String.Empty
        ibtnFechaA.Enabled = True
        ddlPeriodo.Enabled = True
        ddlMoneda.Enabled = True
        'txtFechaDe.Enabled = True
        'ibtnFechaDe.Enabled = True
        'txtFechaA.Enabled = True
        'ibtnFechaA.Enabled = True
        liNoConsiderados.Visible = False
        ltlAviso.Visible = False
        ltlAviso.Text = String.Empty
        lnkVerNoContemplados.Visible = False
    End Sub

    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        Limpiar()
        divresumen.Visible = False
        btnConsultar.Enabled = True
    End Sub


    Protected Sub btnConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Try
            'pmonzon
            If ddlMoneda.SelectedValue = String.Empty Then
                lblResultado.Text = MensajesValidacion.MensajeMonedaReq
                Exit Sub
            End If
            If ddlPeriodo.SelectedValue = String.Empty Then
                lblResultado.Text = MensajesValidacion.MensajePeriodoReq
                Exit Sub
            End If


            Dim objRes As New SPE.Web.CEmpresaContratante
            Dim ListaEmpresas As New List(Of BEEmpresaContratante)
            ListaEmpresas = objRes.RecuperarEmpresasLiquidacion(CDate(txtFechaDe.Text), CDate(txtFechaA.Text), ddlPeriodo.SelectedValue, ddlMoneda.SelectedValue)

            Dim objLiq As New SPE.Web.CComun
            Dim ListaNoConc As New List(Of BELiquidacion)
            ListaNoConc = objLiq.RecuperarCipsNoConciliados(ddlMoneda.SelectedValue, CDate(txtFechaA.Text))


            Dim strValor As String = "#"
            Dim intCont As Integer = 0

            For Each oBEEmpresaContratante As BEEmpresaContratante In ListaEmpresas
                If (oBEEmpresaContratante.CantOP.ToString <> "0") Then
                    strValor += intCont.ToString + "#"
                End If
                intCont += 1
            Next
            intCont = 0
            If ListaEmpresas.Count > 0 Then
                divReultadoGrilla.Visible = True

                gvPaginas.DataSource = ListaEmpresas
                gvPaginas.DataBind()
                For Each gvr As GridViewRow In gvPaginas.Rows
                    If strValor.Contains("#" + intCont.ToString + "#") Then
                        gvr.ForeColor = System.Drawing.Color.Red
                    End If
                    intCont += 1
                Next
                If ListaNoConc.Count > 0 Then
                    gvNoConc.DataSource = ListaNoConc
                    gvNoConc.DataBind()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub



    Protected Sub btnRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegistrar.Click
        Dim decPTComision As Decimal = 0.0
        Dim intPTotalOperaciones As Integer = 0
        Dim decPTPagos As Decimal = 0.0
        Dim objEmpresaContratante As New CEmpresaContratante
        Dim objServicio As New SPE.Web.CServicio
        Dim objResultado As New BEServicio
        Dim intValida As Integer = 0
        Dim beServicio As New BEServicio
        Dim strNombreEmpresa As String = "<ul>"
        Dim leastOne As Boolean = False
        Dim intContador As Integer = 0
        txtCadenaGrid.Text = "#"



        For Each dgvRow As GridViewRow In gvPaginas.Rows

            If DirectCast(dgvRow.FindControl("chkAsociado"), CheckBox).Checked = True Then
                leastOne = True
                Dim intIdEmpresaContratante As Integer = CInt(dgvRow.Cells(9).Text)
                Dim intIdMoneda As Integer = IIf(dgvRow.Cells(4).Text.ToLower = "soles", 1, 2)

                beServicio.IdEmpresaContratante = intIdEmpresaContratante
                beServicio.IdMoneda = intIdMoneda
                Dim objTransferencia As New BETransferencia
                objTransferencia.idEmpresaContratante = intIdEmpresaContratante
                objTransferencia.IdMoneda = intIdMoneda
                objTransferencia.FechaInicio = CDate(txtFechaDe.Text)
                objTransferencia.FechaFin = CDate(txtFechaA.Text)

                'intValida = objEmpresaContratante.ValidarPendientesaConciliar(objTransferencia)

                objResultado = objServicio.ConsultarTransaccionesxServicioLote(beServicio, CDate(txtFechaDe.Text), CDate(txtFechaA.Text))

                decPTComision += objResultado.PorcentajeComision
                intPTotalOperaciones += objResultado.CantidadLimite
                decPTPagos += objResultado.MontoComision
                strNombreEmpresa += "<li>" + dgvRow.Cells(3).Text + "</li>"
                txtCadenaGrid.Text += intContador.ToString + "#"
            End If
            intContador += 1
        Next

        If leastOne Then


            strNombreEmpresa += "</ul>"
            'If Not IsNothing(objResultado) Then
            txtPEmpresa.Text = strNombreEmpresa
            txtPMoneda.Text = ddlMoneda.SelectedItem.Text.ToString
            'txtPAl.Text = CDate(txtFechaA.Text).ToShortDateString
            'txtPDel.Text = CDate(txtFechaDe.Text).ToShortDateString
            txtPTComision.Text = decPTComision.ToString("#,#0.00")
            txtPTotalOperaciones.Text = intPTotalOperaciones.ToString
            txtPTPagos.Text = decPTPagos.ToString("#,#0.00")
            'txtPEstado.Text = SPE.EmsambladoComun.ParametrosSistema.Registrar
            Dim objCEmpresaCon As New SPE.Web.CEmpresaContratante
            Dim objbeCuentaEmpresa As New SPE.Entidades.BECuentaEmpresa
            With objbeCuentaEmpresa
                .IdEmpresaContratante = 0 'se quieren considerar todos
                .IdMoneda = ddlMoneda.SelectedValue
                .PageSize = 1
                .PageNumber = 1
            End With
            Dim listCuentaEmpresa As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = objCEmpresaCon.ConsultarCuentaEmpresa(objbeCuentaEmpresa)


            'If listCuentaEmpresa.Count > 0 Then
            '    txtBanco.Text = CType(listCuentaEmpresa(0), BECuentaEmpresa).Banco
            '    txtPNroCuenta.Text = CType(listCuentaEmpresa(0), BECuentaEmpresa).NroCuenta
            'End If


            pnlPopupActualizarTransferencia.Visible = True
            btnForLiq.Visible = True
            btnVerDetalle.Visible = False
            mppTransferencia.Show()
        Else
            lblResultado.Text = MensajesError.MensajeSeleccionados
            'End If
        End If

    End Sub
    Private Sub controlesAlBuscar(ByVal flag As Boolean)
        'txtFechaDe.Enabled = False
        'txtFechaA.Enabled = False
        'ddlEmpresa.Enabled = flag
        ddlPeriodo.Enabled = flag
        ddlMoneda.Enabled = flag
    End Sub



    Protected Sub btnPCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPCancelar.Click
        'controlesAlBuscar(True)
    End Sub




    Private Function getEntityFilled(ByVal idEmCont As Integer, idMoneda As Integer, ByVal fechaDe As String, ByVal fechaA As String, _
                                     ByVal totOpe As Integer, ByVal totCom As Decimal, ByVal totalPago As Decimal) As BETransferencia

        Dim strBanco As String = ""
        Dim strPNroCuenta As String = ""
        Dim strFechaDeposito As String = ""
        Dim objCEmpresaCon As New SPE.Web.CEmpresaContratante
        Dim objbeCuentaEmpresa As New SPE.Entidades.BECuentaEmpresa
        With objbeCuentaEmpresa
            .IdEmpresaContratante = idEmCont
            .IdMoneda = idMoneda
            .PageSize = 1
            .PageNumber = 1
        End With
        Dim listCuentaEmpresa As List(Of _3Dev.FW.Entidades.BusinessEntityBase) = objCEmpresaCon.ConsultarCuentaEmpresa(objbeCuentaEmpresa)
        If listCuentaEmpresa.Count > 0 Then
            strBanco = CType(listCuentaEmpresa(0), BECuentaEmpresa).Banco
            strPNroCuenta = CType(listCuentaEmpresa(0), BECuentaEmpresa).NroCuenta
        End If

        Dim objTransferencia As New BETransferencia
        objTransferencia.idEmpresaContratante = idEmCont
        objTransferencia.IdMoneda = idMoneda
        objTransferencia.FechaInicio = CDate(fechaDe)
        objTransferencia.FechaFin = CDate(fechaA)
        objTransferencia.TotalOperaciones = totOpe
        objTransferencia.TotalComision = totCom
        objTransferencia.TotalPago = totalPago
        objTransferencia.IdUsuarioCreacion = UserInfo.IdUsuario
        objTransferencia.DescripcionBanco = strBanco
        strFechaDeposito = DateTime.MinValue.ToShortDateString
        objTransferencia.NumeroOperacion = ""
        objTransferencia.NumeroCuenta = strPNroCuenta

        objTransferencia.Observacion = ""
        objTransferencia.ArchivoAdjunto = String.Empty
        objTransferencia.IdEstado = SPE.EmsambladoComun.ParametrosSistema.EstadoPeriodoPagoServicio.Registrado
        Return (objTransferencia)
    End Function

    Protected Sub btnPActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPActualizar.Click
        Dim intContador As Integer = 0
        Dim strcadenaLiquidaciones As String = "-"
        For Each dgvRow As GridViewRow In gvPaginas.Rows
            If txtCadenaGrid.Text.Contains("#" + intContador.ToString + "#") Then

                Dim objTransferencia As New BETransferencia
                objTransferencia = getEntityFilled(CInt(dgvRow.Cells(9).Text), CInt(ddlMoneda.SelectedValue), CDate(txtFechaDe.Text), CDate(txtFechaA.Text),
                                                   CInt(dgvRow.Cells(5).Text), CDec(dgvRow.Cells(7).Text), CDec(dgvRow.Cells(6).Text))
                Dim objProcesar As New SPE.Web.CEmpresaContratante
                Dim obMensaje As New MensajeInformacion
                Dim intResultado As Integer = 0
                intResultado = objProcesar.RegistrarTransaccionesaLiquidar(objTransferencia)
                If intResultado > 0 Then
                    lblResultado.Text = obMensaje.MensajeOkConOperacion(intResultado)
                    JSMessageAlert("Mensaje:", obMensaje.MensajeOkConOperacion(intResultado), "1")
                    Limpiar()
                    divresumen.Visible = False
                    controlesAlBuscar(True)
                    'btnCancelar.Enabled = True
                    btnRegistrar.Enabled = False
                    strcadenaLiquidaciones += intResultado.ToString + "-"
                Else
                    lblResultado.Text = MensajesError.MensajeTransaccionRango
                End If
            End If
            intContador += 1
        Next
        txtCadenaLiquidacion.Text = strcadenaLiquidaciones
        txtIdMoneda.Text = IIf(txtPMoneda.Text = "NUEVOS SOLES", "1", "2")

    End Sub

    Private Function FillEntityforUpdateT(ByVal intEstado As Integer) As BETransferencia
        Dim objTransferencia As New BETransferencia
        'objTransferencia.idTransfenciaEmpresas = lblIdTransferencia.Text
        'objTransferencia.IdUsuarioActualizacion = UserInfo.IdUsuario

        'objTransferencia.DescripcionBanco = txtBanco.Text

        'If CDate(txtFechaDeposito.Text) = String.Empty Then
        '    objTransferencia.FechaDeposito = DateTime.MinValue.ToShortDateString
        'Else
        '    objTransferencia.FechaDeposito = CDate(txtFechaDeposito.Text)
        'End If

        'objTransferencia.NumeroOperacion = txtPNroOperacion.Text
        'objTransferencia.NumeroCuenta = txtPNroCuenta.Text
        'objTransferencia.Observacion = txtPObservacion.Text
        'If FileUpload1.HasFile Then
        '    objTransferencia.ArchivoAdjunto = String.Empty
        'Else

        '    Dim nombreFile As String
        '    Dim ftp As New UtilManagerFTP
        '    Dim FTPTesoreria As String = ConfigurationManager.AppSettings("FTPTesoreria")
        '    Dim FTPUser As String = ConfigurationManager.AppSettings("FTPUser")
        '    Dim FTPPass As String = ConfigurationManager.AppSettings("FTPPass")

        '    nombreFile = DateTime.Today.Year.ToString() + "_" + DateTime.Today.Month.ToString() + "_" + DateTime.Today.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + FileUpload1.FileName
        '    Try
        '        ftp.UploadFile(FileUpload1.FileContent, FTPTesoreria + nombreFile, FTPUser, FTPPass)
        '        objTransferencia.ArchivoAdjunto = nombreFile
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End If
        'objTransferencia.IdEstado = intEstado
        Return objTransferencia
    End Function


    Protected Sub imgbtnRegresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnRegresar.Click
        mppTransferencia.Hide()
    End Sub


    Protected Sub ddlPeriodo_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPeriodo.SelectedIndexChanged
        If Not ddlPeriodo.SelectedValue = String.Empty Then
            If ddlMoneda.SelectedValue = String.Empty Then
                'lblResultado.Text = MensajesValidacion.MensajeMonedaReq
            Else : InicializarFechas()
            End If
            'Else : lblResultado.Text = MensajesValidacion.MensajeEmpresaReq
        End If
    End Sub




    Protected Sub ddlMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMoneda.SelectedIndexChanged
        If Not ddlMoneda.SelectedValue = String.Empty Then

            If ddlPeriodo.SelectedValue = String.Empty Then
                'lblResultado.Text = MensajesValidacion.MensajeEmpresaReq
            Else : InicializarFechas()
            End If
            'Else : lblResultado.Text = MensajesValidacion.MensajeMonedaReq
        End If

    End Sub

    Protected Sub lnkVerDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkVerDetalle.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
        Else
            ' ExportarExcel(True)
        End If 'asd
    End Sub




    Protected Sub ExportarExcel(ByVal FlagConciliado As Boolean, ByVal detEmCont As String, ByVal detMoneda As String, ByVal idEmCont As Integer, idMoneda As Integer, ByVal fechaDe As String, ByVal fechaA As String, _
                                     ByVal totOpe As Integer, ByVal totCom As Decimal, ByVal totalPago As Decimal, ByVal strFolderName As String, ByVal strFileName As String)
        Dim objCServicio As New SPE.Web.CServicio
        Dim obeServicio As New SPE.Entidades.BETransferencia

        Dim ListaTrans As New List(Of BETransferencia)
        obeServicio.idEmpresaContratante = idEmCont
        obeServicio.IdMoneda = idMoneda
        obeServicio.FechaInicio = fechaDe
        obeServicio.FechaFin = fechaA
        obeServicio.FlagConciliado = FlagConciliado
        ListaTrans = objCServicio.ConsultarDetalleTransaccionesPendientes(obeServicio)

        Dim parametros As New List(Of KeyValuePair(Of String, String))()
        parametros.Add(New KeyValuePair(Of String, String)("Titulo", IIf(FlagConciliado, "Detalle de Transacciones Pendientes", "Detalle de Transacciones no consideradas")))
        parametros.Add(New KeyValuePair(Of String, String)("Empresa", detEmCont))
        parametros.Add(New KeyValuePair(Of String, String)("Moneda", detMoneda))
        parametros.Add(New KeyValuePair(Of String, String)("Hasta", fechaDe))
        parametros.Add(New KeyValuePair(Of String, String)("Desde", fechaA))
        'parametros.Add(New KeyValuePair(Of String, String)("Total", txtTotalPago.Text))
        parametros.Add(New KeyValuePair(Of String, String)("Total", ListaTrans.Sum(Function(x) x.TotalPago).ToString("#.#0")))
        parametros.Add(New KeyValuePair(Of String, String)("Comision", totCom))
        parametros.Add(New KeyValuePair(Of String, String)("TotalT", (totalPago - totCom).ToString("#.#0")))
        UtilReport.ProcesoExportarGenericoLote(ListaTrans, Page, "EXCEL", "xls", "Detalle Transacciones no consideradas - ", parametros, "RptDetalleTransaccionPendiente.rdlc", strFolderName, strFileName)
    End Sub


    Protected Sub btnVerDetalle_Click(sender As Object, e As System.EventArgs) Handles btnVerDetalle.Click
        If (Not _3Dev.FW.Web.Security.ValidatePageAccess(Me.Page.AppRelativeVirtualPath) And (Me.Page.AppRelativeVirtualPath <> "~/SEG/sinautrz.aspx") And (Me.Page.AppRelativeVirtualPath <> "~/Principal.aspx")) Then
        Else
            Dim strFolderName As String = System.Configuration.ConfigurationManager.AppSettings("CarpetaRaizLote") + System.IO.Path.GetRandomFileName() + "\"
            Dim strFileName As String = ""
            Dim blSeleccionados As Boolean = False
            If Not Directory.Exists(strFolderName) Then
                Directory.CreateDirectory(strFolderName)
            End If
            For Each dgvRow As GridViewRow In gvPaginas.Rows
                If DirectCast(dgvRow.FindControl("chkAsociado"), CheckBox).Checked = True Then
                    blSeleccionados = True
                    strFileName = dgvRow.Cells(3).Text + "_" + txtFechaA.Text.Replace("/", "") + ".xls"
                    ExportarExcel(True, dgvRow.Cells(3).Text, dgvRow.Cells(4).Text, CInt(dgvRow.Cells(9).Text), CInt(ddlMoneda.SelectedValue),
                                  CDate(txtFechaDe.Text), CDate(txtFechaA.Text), CInt(dgvRow.Cells(5).Text), CDec(dgvRow.Cells(7).Text), CDec(dgvRow.Cells(6).Text), strFolderName, strFileName)
                End If
            Next
            If blSeleccionados Then
                Dim fileEntries As String() = Directory.GetFileSystemEntries(strFolderName)
                Try
                    Using zip As New ZipFile()
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary
                        For Each fileName As String In fileEntries
                            zip.AddFile(fileName, "")
                        Next
                        Dim zipName As String = [String].Format("Liquidacion_{0}", txtFechaA.Text.Replace("/", ""))
                        zip.Save(strFolderName + zipName + ".zip")


                        Response.ContentType = "application/zip"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + zipName + ".zip")
                        Response.TransmitFile(strFolderName + zipName + ".zip")
                        Response.End()

                    End Using

                Catch ex As Exception
                    lblResultado.Visible = True
                    lblResultado.Text = "Revise la información de las cuentas por empresa."
                End Try
            Else
                lblResultado.Text = MensajesError.MensajeSeleccionadosDet
            End If
        End If
    End Sub

    Private Function Filler(ByVal inicial As String, ByVal valor As String, ByVal tamano As Integer) As String
        Dim strRetorno As String = inicial
        For i = 1 To (tamano - inicial.Length)
            strRetorno = valor + strRetorno
        Next
        Return strRetorno
    End Function

    Private Function FillerRight(ByVal inicial As String, ByVal valor As String, ByVal tamano As Integer) As String
        Dim strRetorno As String = inicial
        For i = 1 To (tamano - inicial.Length)
            strRetorno = strRetorno + valor
        Next
        Return strRetorno
    End Function

    Public Function EsLetra(ByVal Caracter As String) As Boolean
        Dim CodigoAscii As Integer
        If Len(Caracter) > 0 Then
            CodigoAscii = Asc(Caracter)
            If (CodigoAscii >= 65 And CodigoAscii <= 90) Or _
               (CodigoAscii >= 97 And CodigoAscii <= 122) Then
                EsLetra = True
            Else
                EsLetra = False
            End If
        Else
            EsLetra = False
        End If
    End Function

    Public Function ReemplazarLetraPorNumero(ByVal Cadena As String) As String
        Dim CadenaTemp As String
        Dim Caracter As String
        Dim LargoCadena As Integer
        Dim i As Integer
        Const Cero As String = "0"

        LargoCadena = Len(Trim(Cadena))
        CadenaTemp = ""
        Caracter = ""

        If LargoCadena > 0 Then
            For i = 1 To LargoCadena
                Caracter = Mid(Trim(Cadena), i, 1)
                If EsLetra(Caracter) Then
                    CadenaTemp = CadenaTemp & Cero
                Else
                    CadenaTemp = CadenaTemp & Caracter
                End If
            Next i
            ReemplazarLetraPorNumero = CadenaTemp
        Else
            ReemplazarLetraPorNumero = ""
        End If
    End Function


    Public Function ObtenerTotalControl(ByVal xlWorksheet As Microsoft.Office.Interop.Excel._Worksheet) As Double
        Dim TotalAbonos As Double
        Dim TotalCheques As Double
        Dim TotalCargo As Double
        Dim TotalControl As Double
        Dim NroCuentaCargo As String
        Dim NroCuentaAbono As String
        Dim Indice As Integer
        Dim CabeceraChequeGerencia As String
        Dim NroDocChequeGerencia As String
        Const Abono As String = "A"

        Dim columnaA As String
        Dim columnaB As String
        Dim columnaC As String
        Dim columnaD As String
        Dim columnaE As String
        Dim columnaF As String
        Dim columnaG As String
        Dim columnaH As String
        Dim columnaI As String
        Dim columnaJ As String
        Dim columnaK As String
        Dim columnaL As String
        Dim columnaM As String
        Dim columnaN As String
        Dim columnaO As String

        TotalAbonos = 0
        TotalCargo = 0
        Indice = 11
        NroCuentaCargo = Trim(CStr(xlWorksheet.Range("E7").Value))
        TotalCargo = TotalCargo + CDbl(Mid(NroCuentaCargo, 4, Len(NroCuentaCargo) - 3))

        columnaA = Trim(CStr(xlWorksheet.Range("A" & Indice).Value))
        columnaB = Trim(CStr(xlWorksheet.Range("B" & Indice).Value))
        columnaC = Trim(CStr(xlWorksheet.Range("C" & Indice).Value))
        columnaD = Trim(CStr(xlWorksheet.Range("D" & Indice).Value))
        columnaE = Trim(CStr(xlWorksheet.Range("E" & Indice).Value))
        columnaF = Trim(CStr(xlWorksheet.Range("F" & Indice).Value))
        columnaG = Trim(CStr(xlWorksheet.Range("G" & Indice).Value))
        columnaH = Trim(CStr(xlWorksheet.Range("H" & Indice).Value))
        columnaI = Trim(CStr(xlWorksheet.Range("I" & Indice).Value))
        columnaJ = Trim(CStr(xlWorksheet.Range("J" & Indice).Value))
        columnaK = Trim(CStr(xlWorksheet.Range("K" & Indice).Value))
        columnaL = Trim(CStr(xlWorksheet.Range("L" & Indice).Value))
        columnaM = Trim(CStr(xlWorksheet.Range("M" & Indice).Value))
        columnaN = Trim(CStr(xlWorksheet.Range("N" & Indice).Value))
        columnaO = Trim(CStr(xlWorksheet.Range("O" & Indice).Value))

        Do While (columnaA <> "" Or columnaB <> "" Or columnaC <> "" Or columnaD <> "" _
                    Or columnaE <> "" Or columnaF <> "" Or columnaG <> "" Or columnaH <> "" _
                    Or columnaI <> "" Or columnaJ <> "" Or columnaK <> "" Or columnaL <> "" _
                    Or columnaM <> "" Or columnaN <> "" Or columnaO <> "")
            If columnaA = Abono Then
                NroCuentaAbono = columnaC
                If Len(NroCuentaAbono) > 0 Then
                    If columnaB = "B" Then
                        TotalAbonos = TotalAbonos + CDbl(Mid(NroCuentaAbono, 11, Len(NroCuentaAbono) - 10))
                    Else
                        TotalAbonos = TotalAbonos + CDbl(Mid(NroCuentaAbono, 4, Len(NroCuentaAbono) - 3))
                    End If
                End If
            End If
            Indice = Indice + 1

            columnaA = Trim(CStr(xlWorksheet.Range("A" & Indice).Value))
            columnaB = Trim(CStr(xlWorksheet.Range("B" & Indice).Value))
            columnaC = Trim(CStr(xlWorksheet.Range("C" & Indice).Value))
            columnaD = Trim(CStr(xlWorksheet.Range("D" & Indice).Value))
            columnaE = Trim(CStr(xlWorksheet.Range("E" & Indice).Value))
            columnaF = Trim(CStr(xlWorksheet.Range("F" & Indice).Value))
            columnaG = Trim(CStr(xlWorksheet.Range("G" & Indice).Value))
            columnaH = Trim(CStr(xlWorksheet.Range("H" & Indice).Value))
            columnaI = Trim(CStr(xlWorksheet.Range("I" & Indice).Value))
            columnaJ = Trim(CStr(xlWorksheet.Range("J" & Indice).Value))
            columnaK = Trim(CStr(xlWorksheet.Range("K" & Indice).Value))
            columnaL = Trim(CStr(xlWorksheet.Range("L" & Indice).Value))
            columnaM = Trim(CStr(xlWorksheet.Range("M" & Indice).Value))
            columnaN = Trim(CStr(xlWorksheet.Range("N" & Indice).Value))
            columnaO = Trim(CStr(xlWorksheet.Range("O" & Indice).Value))

        Loop


        CabeceraChequeGerencia = Trim(CStr(xlWorksheet.Range("B1").Value))
        CabeceraChequeGerencia = Replace(Replace(CabeceraChequeGerencia, vbLf, " "), vbCr, " ")
        Indice = 1
        TotalCheques = 0

        Do While (UCase(CabeceraChequeGerencia) <> "TIPO DE MONEDA DE ABONO")
            CabeceraChequeGerencia = Trim(CStr(xlWorksheet.Range("B" & Indice).Value))
            Indice = Indice + 1
            'indec = Indice
        Loop

        columnaA = Trim(CStr(xlWorksheet.Range("A" & Indice).Value))
        columnaB = Trim(CStr(xlWorksheet.Range("B" & Indice).Value))
        columnaC = Trim(CStr(xlWorksheet.Range("C" & Indice).Value))
        columnaD = Trim(CStr(xlWorksheet.Range("D" & Indice).Value))
        columnaE = Trim(CStr(xlWorksheet.Range("E" & Indice).Value))
        columnaF = Trim(CStr(xlWorksheet.Range("F" & Indice).Value))
        columnaG = Trim(CStr(xlWorksheet.Range("G" & Indice).Value))
        columnaH = Trim(CStr(xlWorksheet.Range("H" & Indice).Value))
        columnaI = Trim(CStr(xlWorksheet.Range("I" & Indice).Value))
        columnaJ = Trim(CStr(xlWorksheet.Range("J" & Indice).Value))
        columnaK = Trim(CStr(xlWorksheet.Range("K" & Indice).Value))
        columnaL = Trim(CStr(xlWorksheet.Range("L" & Indice).Value))

        Do While (columnaA <> "" Or columnaB <> "" Or columnaC <> "" Or columnaD <> "" _
                    Or columnaE <> "" Or columnaF <> "" Or columnaG <> "" Or columnaH <> "" _
                    Or columnaI <> "" Or columnaJ <> "" Or columnaK <> "" Or columnaL <> "" _
                    )
            If columnaA = Abono Then
                NroDocChequeGerencia = ReemplazarLetraPorNumero(columnaE)
                If Len(NroDocChequeGerencia) > 0 Then
                    TotalCheques = TotalCheques + CDbl(NroDocChequeGerencia)
                End If
            End If

            Indice = Indice + 1

            columnaA = Trim(CStr(xlWorksheet.Range("A" & Indice).Value))
            columnaB = Trim(CStr(xlWorksheet.Range("B" & Indice).Value))
            columnaC = Trim(CStr(xlWorksheet.Range("C" & Indice).Value))
            columnaD = Trim(CStr(xlWorksheet.Range("D" & Indice).Value))
            columnaE = Trim(CStr(xlWorksheet.Range("E" & Indice).Value))
            columnaF = Trim(CStr(xlWorksheet.Range("F" & Indice).Value))
            columnaG = Trim(CStr(xlWorksheet.Range("G" & Indice).Value))
            columnaH = Trim(CStr(xlWorksheet.Range("H" & Indice).Value))
            columnaI = Trim(CStr(xlWorksheet.Range("I" & Indice).Value))
            columnaJ = Trim(CStr(xlWorksheet.Range("J" & Indice).Value))
            columnaK = Trim(CStr(xlWorksheet.Range("K" & Indice).Value))
            columnaL = Trim(CStr(xlWorksheet.Range("L" & Indice).Value))

        Loop


        TotalControl = TotalCargo + TotalAbonos + TotalCheques
        ObtenerTotalControl = TotalControl

    End Function

    Protected Sub btnForLiq_Click(sender As Object, e As System.EventArgs) Handles btnForLiq.Click

        If txtCadenaLiquidacion.Text <> "" Then
            Dim strArchivoPadre As String = ""
            Dim strCarpetaFormatos As String = System.Configuration.ConfigurationManager.AppSettings("CarpetaFormatos")
            Dim strFakeFolderName As String = System.IO.Path.GetRandomFileName() + "\"
            Dim strFileOutName As String = ""
            Dim strReferenciaPlanilla As String = System.Configuration.ConfigurationManager.AppSettings("ReferenciaPlanilla")
            Dim strCadenaLiquidacion As String = txtCadenaLiquidacion.Text
            Dim intIdMoneda As Integer = CInt(txtIdMoneda.Text)
            Dim strCuentaCargo As String = ""
            Dim strTipoMoneda As String
            Dim decTotal As Decimal = 0



            'logica
            Dim objLiq As New SPE.Web.CComun
            Dim ListaBCP As New List(Of BELiquidacion)
            ListaBCP = objLiq.RecuperarDepositosBCP(intIdMoneda, strCadenaLiquidacion)


            If ListaBCP.Count > 0 Then
                If ddlMoneda.SelectedValue = "1" Then
                    strArchivoPadre = System.Configuration.ConfigurationManager.AppSettings("ArchivoPadreS")
                    strCuentaCargo = System.Configuration.ConfigurationManager.AppSettings("CuentaCargoBCPs")
                    strTipoMoneda = "S"
                    strFileOutName = "PagoEfectivo_telecredito_soles_plantilla_periodo_diario.xls"
                Else
                    strArchivoPadre = System.Configuration.ConfigurationManager.AppSettings("ArchivoPadreD")
                    strCuentaCargo = System.Configuration.ConfigurationManager.AppSettings("CuentaCargoBCPd")
                    strTipoMoneda = "D"
                    strFileOutName = "PagoEfectivo_telecredito_dolares_plantilla_periodo_diario"
                End If

                'valores archivo
                Dim strCantidadRegistros As String = Filler(ListaBCP.Count.ToString, "0", 6)
                Dim strTotalPlanilla As String = ""


                'creación del archivo de excel
                Dim newFile As New FileInfo(strArchivoPadre)
                Dim xlApp As Microsoft.Office.Interop.Excel.Application = New Microsoft.Office.Interop.Excel.Application()
                Dim xlWorkbook As Microsoft.Office.Interop.Excel.Workbook = xlApp.Workbooks.Open(strArchivoPadre)
                Dim xlWorksheet As Microsoft.Office.Interop.Excel._Worksheet = xlWorkbook.Sheets(1)

                'insertamos las columnas necesarias
                For i As Integer = 1 To ListaBCP.Count - 1
                    Dim a1 As Microsoft.Office.Interop.Excel.Range = xlWorksheet.Range("A12", "A12")
                    a1.EntireRow.Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown, Type.Missing)
                Next i

                'actlualizamos las columnas insetadas
                Dim Fila As Integer = 11

                For Each oBELiquidacion As BELiquidacion In ListaBCP
                    xlWorksheet.Cells(Fila, 1) = "A"
                    xlWorksheet.Cells(Fila, 2) = oBELiquidacion.TipoCuenta
                    xlWorksheet.Cells(Fila, 3) = oBELiquidacion.CuentaAbono
                    xlWorksheet.Cells(Fila, 4) = oBELiquidacion.TipoDocumento
                    xlWorksheet.Cells(Fila, 5) = oBELiquidacion.NumeroDocumento
                    xlWorksheet.Cells(Fila, 7) = oBELiquidacion.NombreProveedor
                    xlWorksheet.Cells(Fila, 8) = strTipoMoneda
                    xlWorksheet.Cells(Fila, 9) = oBELiquidacion.MontoAbono.ToString
                    xlWorksheet.Cells(Fila, 10) = "N"
                    xlWorksheet.Cells(Fila, 11) = "0000"
                    Fila += 1
                    decTotal += oBELiquidacion.MontoAbono
                Next

                'actualizamos las celdas duras
                xlWorksheet.Cells(7, 2) = strCantidadRegistros ' cantidad de abonos de la planilla
                xlWorksheet.Cells(7, 3) = Today.Year.ToString + CompletarCeros(Today.Month.ToString) + CompletarCeros(Today.Day.ToString)
                xlWorksheet.Cells(7, 5) = strCuentaCargo
                xlWorksheet.Cells(7, 6) = Convert.ToString(Math.Round(decTotal, 2))
                xlWorksheet.Cells(7, 7) = strReferenciaPlanilla


                'Dim Sum_Range As Microsoft.Office.Interop.Excel.Range = xlWorksheet.Range("F7")
                'Sum_Range.Formula = "=sum(I11:I" + (Fila - 1).ToString + ")"

                Dim doubObtenerTotalControl As Double = ObtenerTotalControl(xlWorksheet)
                'Si no existe la carpeta la creamos
                If Not Directory.Exists(strCarpetaFormatos + strFakeFolderName) Then
                    Directory.CreateDirectory(strCarpetaFormatos + strFakeFolderName)
                End If
                'concatenamos el nombre final del archivo
                strFileOutName = strCarpetaFormatos + strFakeFolderName + strFileOutName

                'guardamos y cerramos procesos
                xlWorkbook.SaveAs(strFileOutName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Missing.Value, Missing.Value, Missing.Value, Missing.Value, _
                Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value)
                xlWorkbook.Close(Missing.Value, Missing.Value, Missing.Value)
                xlWorksheet = Nothing
                xlWorkbook = Nothing
                xlApp.Quit()
                'descargamos el archivo


                GenerarPlanoBCP(ListaBCP, strCantidadRegistros, strCuentaCargo, Convert.ToString(Math.Round(decTotal, 2)), strCarpetaFormatos + strFakeFolderName, doubObtenerTotalControl.ToString.Split(".")(0))




                Dim fileEntries As String() = Directory.GetFiles(strCarpetaFormatos + strFakeFolderName)
                Try
                    Using zip As New ZipFile()
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary
                        For Each fileName As String In fileEntries
                            zip.AddFile(fileName, "")
                        Next
                        Dim zipName As String = [String].Format("LiquidacionBCP_{0}", txtFechaA.Text.Replace("/", ""))
                        zip.Save(strCarpetaFormatos + strFakeFolderName + zipName + ".zip")


                        Response.ContentType = "application/zip"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + zipName + ".zip")
                        Response.TransmitFile(strCarpetaFormatos + strFakeFolderName + zipName + ".zip")
                        Response.End()

                    End Using

                Catch ex As Exception
                    lblResultado.Visible = True
                    lblResultado.Text = "Revise la información de las cuentas por empresa."
                End Try







            End If
        End If
    End Sub

    Private Sub GenerarPlanoBCP(ByVal ListaBCP As List(Of BELiquidacion), ByVal strCantidad As String, ByVal strCuentaCargo As String, ByVal strTotal As String, ByVal strFolderName As String, ByVal strObtenerTotalControl As String)
        Try


            Dim strFecha = Today.Year.ToString + CompletarCeros(Today.Month.ToString) + CompletarCeros(Today.Day.ToString)
            Dim StringPagoServicios As New StringBuilder("")
            'cabecera

            Dim byteArchivoCorona As Byte() = New UTF8Encoding().GetBytes(StringPagoServicios.ToString())
            'Try
            '    Dim sw As StreamWriter = File.CreateText(strFolderName + "PROVEEDORES" + strFecha + ".txt")
            '    sw.Write(byteArchivoCorona)
            '    sw.Close()
            'Catch ex As Exception

            'End Try

            Dim sw As StreamWriter = File.CreateText(strFolderName + "PROVEEDORES" + strFecha + ".txt")
            sw.WriteLine("1" + strCantidad + strFecha + "C0001" + strCuentaCargo + "       " + Filler(strTotal, "0", 17) + FillerRight("Liquidacion PagoEfectivo", " ", 40) + "N" + Filler(strObtenerTotalControl, "0", 15))
            Dim cont As Integer = 1
            For Each elto As BELiquidacion In ListaBCP
                If cont < ListaBCP.Count Then
                    sw.WriteLine("2" + elto.TipoCuenta + FillerRight(elto.CuentaAbono, " ", 20) + "1" + elto.TipoDocumento + FillerRight(elto.NumeroDocumento, " ", 15) + FillerRight(elto.NombreProveedor, " ", 75) + "Referencia Beneficiario " + FillerRight(elto.NumeroDocumento, " ", 16) + "Ref Emp " + elto.NumeroDocumento + " 0001" + Filler(elto.MontoAbono.ToString, "0", 17) + "N")
                    cont += 1
                Else
                    sw.Write("2" + elto.TipoCuenta + FillerRight(elto.CuentaAbono, " ", 20) + "1" + elto.TipoDocumento + FillerRight(elto.NumeroDocumento, " ", 15) + FillerRight(elto.NombreProveedor, " ", 75) + "Referencia Beneficiario " + FillerRight(elto.NumeroDocumento, " ", 16) + "Ref Emp " + elto.NumeroDocumento + " 0001" + Filler(elto.MontoAbono.ToString, "0", 17) + "N")
                End If
            Next
            sw.Close()


        Catch ex As Exception
            '023Logger.LogException(ex)
        End Try

    End Sub


    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCeros(ByVal valor As String) As String
        If valor.Length = 1 Then
            Return "0" + valor
        Else
            Return valor
        End If
    End Function

    Public Sub selectall_OnCheckedChanged(sender As Object, e As EventArgs)

        Dim grvv As GridViewRow = gvPaginas.HeaderRow
        If DirectCast(grvv.FindControl("selectall"), CheckBox).Checked = True Then
            For Each gvr As GridViewRow In gvPaginas.Rows
                DirectCast(gvr.FindControl("chkAsociado"), CheckBox).Checked = True
            Next
        Else
            For Each gvr As GridViewRow In gvPaginas.Rows
                DirectCast(gvr.FindControl("chkAsociado"), CheckBox).Checked = False
            Next
        End If

    End Sub





End Class






