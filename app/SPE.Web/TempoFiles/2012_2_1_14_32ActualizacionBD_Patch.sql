alter PROCEDURE [PagoEfectivo].[prc_ConsultarCabeceraOP]      
(      
@pstrNumeroOrden varchar(14)    
)      
AS      
BEGIN      

 SET NOCOUNT ON;      
 
 Select IdOrdenPago,    
dmnd000 as moneda,    
s.Nombre as servicio,    
NumeroOrdenPago ,    
p1.Descripcion as Estado,    
Total as monto,    
UsuarioNombre,     
UsuarioApellidos,    
UsuarioEmail,    
UsuarioAlias,    
p2.Descripcion as TipoDocumento,    
NumeroDocumento,    
Telefono,    
FechaEmision,    
FechaCancelacion,    
FechaAnulada,    
FechaExpirada,    
FechaEliminado,    
op.OrderIdComercio,    
op.FechaConciliacion,    
p3.Descripcion as EstadoConciliacion,    
CAA.NombreArchivo as ArchivoConciliacion    
From PagoEfectivo.Usuario U   
inner join PagoEfectivo.Cliente C ON U.IdUsuario = C.IdUsuario  
INNER JOIN PagoEfectivo.OrdenPago op on C.IdCliente=op.IdCliente   
inner join PagoEfectivo.Moneda m  on op.IdMoneda=m.IdMoneda   
inner join PagoEfectivo.Servicio s  on op.IdServicio=s.IdServicio   
inner join PagoEfectivo.Parametro p1  on op.IdEstado=p1.Id   
inner join PagoEfectivo.Parametro p2  on u.IdTipoDocumento=p2.Id   
left join PagoEfectivo.Parametro p3 on p3.Id=op.IdEstadoConciliacion
left join PagoEfectivo.ConciliacionArchivo CAA on op.IdConciliacionArchivo=CAA.IdConciliacionArchivo
where op.NumeroOrdenPago=@pstrNumeroOrden

SET NOCOUNT OFF;      
end

