﻿<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="TestIntegra.aspx.vb"
    Inherits="TestIntegra" ValidateRequest="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <title>Página sin título</title>
</head>
<body>
    <script src="https://sb.scorecardresearch.com/c2/6906602/cs.js#sitio_id=235360&path=/online/otros/"></script>
    <form id="form1" runat="server">
    <div>
        <table style="width: 494px">
            <tr>
                <td style="width: 869px; height: 21px; color: #ffffff; background-color: #006699;">
                </td>
                <td style="width: 869px; height: 21px; text-align: center; color: #ffffff; background-color: #006699;">
                    <strong>TEST DE INTEGRACION</strong>
                </td>
            </tr>
            <tr>
                <td style="width: 869px; height: 19px">
                    <asp:Label ID="Label1" runat="server" Text="Url Ref.:"></asp:Label>
                </td>
                <td style="width: 450px; height: 19px">
                    <asp:TextBox ID="txtUrl" runat="server" Width="465px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 869px; height: 21px;">
                    <asp:Label ID="Label2" runat="server" Text="Parametros:"></asp:Label>
                </td>
                <td style="width: 450px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 869px">
                    <asp:Label ID="Label3" runat="server" Text="Datos Enc:"></asp:Label>
                </td>
                <td style="width: 450px">
                </td>
            </tr>
            <tr>
                <td style="width: 869px">
                </td>
                <td style="width: 450px; text-align: center;">
                    <table style="width: 474px; height: 61px">
                        <tr>
                            <td style="width: 21px; height: 26px;">
                                1-
                            </td>
                            <td style="width: 91px; height: 26px;">
                                OrdenId
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left;">
                                <asp:TextBox ID="txtOrdenId" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                2-
                            </td>
                            <td style="width: 91px">
                                UrlOk
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtUrlOk" runat="server" Width="294px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 26px;">
                                3-
                            </td>
                            <td style="width: 91px; height: 26px;">
                                UrlError
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left;">
                                <asp:TextBox ID="UrlError" runat="server" Width="297px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 16px">
                                4-
                            </td>
                            <td style="width: 91px; height: 16px">
                                MailCom
                            </td>
                            <td style="width: 176px; height: 16px; text-align: left;">
                                <asp:TextBox ID="MailCom" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 2px">
                                5-
                            </td>
                            <td style="width: 91px; height: 2px">
                                OrdenDesc
                            </td>
                            <td style="width: 176px; height: 2px; text-align: left;">
                                <asp:TextBox ID="txtOrdenDesc" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 26px;">
                                6-
                            </td>
                            <td style="width: 91px; height: 26px;">
                                Moneda
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left;">
                                <asp:TextBox ID="txtMoneda" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                7-
                            </td>
                            <td style="width: 91px">
                                Monto
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtMonto" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 26px">
                                8-
                            </td>
                            <td style="width: 91px; height: 26px">
                                UsuarioID
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left;">
                                <asp:TextBox ID="txtUsarioId" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 26px;">
                                9-
                            </td>
                            <td style="width: 91px; height: 26px;">
                                DataAdicional
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left;">
                                <asp:TextBox ID="txtDataAdicional" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 22px">
                                10-
                            </td>
                            <td style="width: 91px; height: 22px">
                                UsuarioNombre
                            </td>
                            <td style="width: 176px; height: 22px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioNombre" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                11-
                            </td>
                            <td style="width: 91px">
                                UsuarioApellidos
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioApellido" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 26px;">
                                12-
                            </td>
                            <td style="width: 91px; height: 26px;">
                                UsuarioLocalidad
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioLocalidad" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                13-
                            </td>
                            <td style="width: 91px">
                                UsuarioDomicilio
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioDomicilio" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                14-
                            </td>
                            <td style="width: 91px">
                                UsuarioProvincia
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioProvincia" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                15-
                            </td>
                            <td style="width: 91px">
                                UsuarioPais
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioPais" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px">
                                16-
                            </td>
                            <td style="width: 91px">
                                UsuarioAlias
                            </td>
                            <td style="width: 176px; text-align: left;">
                                <asp:TextBox ID="txtUsuarioAlias" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 21px; height: 26px">
                                17
                            </td>
                            <td style="width: 91px; height: 26px; text-align: left">
                                Empresa
                            </td>
                            <td style="width: 176px; height: 26px; text-align: left">
                                &nbsp;<asp:TextBox ID="txtEmpresa" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 450px; height: 32px; color: buttonhighlight; background-color: #006699;">
                    <asp:Label ID="Label4" runat="server" Text="Prueba de Integracion"></asp:Label>
                </td>
                <td style="width: 450px; height: 32px; color: buttonhighlight; background-color: #006699;">
                    <table style="width: 476px; height: 29px">
                        <tr>
                            <td style="height: 26px">
                                <asp:Label ID="Label5" runat="server" Text="¿Encontro Url?"></asp:Label>
                            </td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtValidacion" runat="server" ForeColor="#0000CC" Width="353px"></asp:TextBox>
                            </td>
                            <td style="height: 26px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 869px">
                </td>
                <td style="width: 450px">
                </td>
            </tr>
            <tr>
                <td style="width: 869px">
                </td>
                <td style="width: 450px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 869px; height: 130px">
                    Datos no Encriptados
                </td>
                <td style="width: 450px; height: 130px">
                    <table style="width: 496px; height: 76px">
                        <tr>
                            <td style="width: 90px; height: 26px">
                            </td>
                            <td style="width: 90px; height: 26px;">
                            </td>
                            <td style="width: 66px; height: 26px;">
                            </td>
                            <td style="height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px">
                                <table style="width: 474px; height: 61px">
                                    <tr>
                                        <td style="width: 21px; height: 26px">
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:Label ID="Label11" runat="server" Text="Empresa Id"></asp:Label>
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtEmpresa2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px;">
                                            1-
                                        </td>
                                        <td style="width: 91px; height: 26px;">
                                            OrdenId
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtOrdenId2" runat="server" Width="147px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px">
                                            2-
                                        </td>
                                        <td style="width: 91px">
                                            UrlOk
                                        </td>
                                        <td style="width: 91px">
                                            <asp:TextBox ID="txtUrlOk2" runat="server" Width="297px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px;">
                                            3-
                                        </td>
                                        <td style="width: 91px; height: 26px;">
                                            UrlError
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtUrlError2" runat="server" Width="296px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 16px">
                                            4-
                                        </td>
                                        <td style="width: 91px; height: 16px">
                                            MailCom
                                        </td>
                                        <td style="width: 91px; height: 16px">
                                            <asp:TextBox ID="txtMail2" runat="server" Width="295px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 2px">
                                            5-
                                        </td>
                                        <td style="width: 91px; height: 2px">
                                            OrdenDesc
                                        </td>
                                        <td style="width: 91px; height: 2px">
                                            <asp:TextBox ID="txtOrdenDesc2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px;">
                                            6-
                                        </td>
                                        <td style="width: 91px; height: 26px;">
                                            Moneda
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtMoneda2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px">
                                            7-
                                        </td>
                                        <td style="width: 91px">
                                            Monto
                                        </td>
                                        <td style="width: 91px">
                                            <asp:TextBox ID="txtMonto2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px">
                                            8-
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            UsuarioID
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtUsuarioId2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px;">
                                            9-
                                        </td>
                                        <td style="width: 91px; height: 26px;">
                                            DataAdicional
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtDataAdicional2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 22px">
                                            10-
                                        </td>
                                        <td style="width: 91px; height: 22px">
                                            UsuarioNombre
                                        </td>
                                        <td style="width: 91px; height: 22px">
                                            <asp:TextBox ID="txtNombre2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px">
                                            11-
                                        </td>
                                        <td style="width: 91px">
                                            UsuarioApellidos
                                        </td>
                                        <td style="width: 91px">
                                            <asp:TextBox ID="txtApellidos2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px;">
                                            12-
                                        </td>
                                        <td style="width: 91px; height: 26px;">
                                            UsuarioLocalidad
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtLocalidad2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px">
                                            13-
                                        </td>
                                        <td style="width: 91px">
                                            UsuarioDomicilio
                                        </td>
                                        <td style="width: 91px">
                                            <asp:TextBox ID="txtDomicilio2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px; height: 26px;">
                                            14-
                                        </td>
                                        <td style="width: 91px; height: 26px;">
                                            UsuarioProvincia
                                        </td>
                                        <td style="width: 91px; height: 26px">
                                            <asp:TextBox ID="txtProvincia2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px">
                                            15-
                                        </td>
                                        <td style="width: 91px">
                                            UsuarioPais
                                        </td>
                                        <td style="width: 91px">
                                            <asp:TextBox ID="txtPais2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 21px">
                                            16-
                                        </td>
                                        <td style="width: 91px">
                                            UsuarioAlias
                                        </td>
                                        <td style="width: 91px">
                                            <asp:TextBox ID="txtAlias2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 66px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px; height: 21px">
                            </td>
                            <td style="width: 90px; height: 21px">
                            </td>
                            <td style="width: 66px; height: 21px">
                            </td>
                            <td style="height: 21px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 66px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px; height: 21px">
                            </td>
                            <td style="width: 90px; height: 21px">
                            </td>
                            <td style="width: 66px; height: 21px">
                            </td>
                            <td style="height: 21px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 66px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 90px">
                            </td>
                            <td style="width: 66px">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
