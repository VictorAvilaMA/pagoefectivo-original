﻿Imports System.Data
Imports SPE.Entidades
Imports _3Dev.FW.Web.Security
Imports SPE.Utilitario
Imports System.Collections.Generic
Imports System.IO
Imports System.Drawing.Text
Imports SPE.EmsambladoComun.ParametrosSistema
Imports System.Xml
Imports SPE.EmsambladoComun

Partial Class TestIntegra
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            Try
                Dim MerchantID, orderId, urlOk, urlError, mailComercio, orderDescripcion, moneda, monto As String
                Dim idservicio As String = "" : Dim idempresa As String = ""
                moneda = ""
                monto = ""
                orderDescripcion = ""
                Dim urlServicio As String
                If ((Request Is Nothing) Or (Request.UrlReferrer Is Nothing) Or (Request.UrlReferrer.AbsoluteUri = Nothing)) Then
                    urlServicio = ""
                Else
                    urlServicio = _3Dev.FW.Web.WebUtil.GetAbsoluteUriWithOutQuery(Request.UrlReferrer)
                End If

                Dim strRutaServ As String = ""
                Dim strTargetPath As String = ""
                Try

                    strTargetPath = System.Configuration.ConfigurationManager.AppSettings("targetPath")
                    If Not System.IO.File.Exists(strTargetPath) Then
                        strRutaServ = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings("xmlTramaServRuta")
                        If Not System.IO.File.Exists(strRutaServ) Then
                            Throw New SPE.Exceptions.ContratoNoEncontradoException(System.Configuration.ConfigurationManager.AppSettings("CodErrorContrato"))
                        End If
                    Else
                        strRutaServ = strTargetPath
                    End If
                Catch exx As SPE.Exceptions.ContratoNoEncontradoException


                End Try


                Dim trans As TraductorServ = New TraductorServ(strRutaServ, Request.RawUrl.ToString, urlServicio)
                _3Dev.FW.Web.Log.Logger.LogMessage("strRutaServ" & strRutaServ.ToString)
                _3Dev.FW.Web.Log.Logger.LogMessage("Request.RawUr" & Request.RawUrl.ToString)
                _3Dev.FW.Web.Log.Logger.LogMessage("urlServicio" & urlServicio.ToString)
                Try
                    _3Dev.FW.Web.Log.Logger.LogMessage("UsaEncriptacionValor" & trans.UsaEncriptacionValor.ToString())
                    _3Dev.FW.Web.Log.Logger.LogMessage("UsuarioIDValor" & trans.UsuarioIDValor.ToString())

                    If trans.UsaEncriptacionValor = "True" Then

                        'Llenado de los datos desencriptados

                        txtOrdenId.Text = trans.OrdenIdValor
                        txtUrl.Text = urlServicio
                        Me.txtUsarioId.Text = trans.UsuarioIDValor
                        Me.txtOrdenId.Text = trans.OrdenIdValor
                        Me.txtMonto.Text = trans.MontoValor
                        txtOrdenDesc.Text = trans.OrdenDescValor
                        Me.txtUrlOk.Text = trans.UrlOkValor
                        Me.txtUsuarioAlias.Text = trans.UsuarioAliasValor
                        Me.txtDataAdicional.Text = trans.DataAdicionalValor
                        Me.txtUsuarioApellido.Text = trans.UsuarioApellidosValor
                        Me.txtUsuarioLocalidad.Text = trans.UsuarioLocalidadValor
                        Me.txtUsuarioNombre.Text = trans.UsuarioNombreValor
                        Me.txtUsuarioDomicilio.Text = trans.UsuarioDomicilioValor
                        Me.txtUsuarioProvincia.Text = trans.UsuarioProvinciaValor
                        Me.txtUsuarioPais.Text = trans.UsuarioPaisValor
                        Me.txtEmpresa.Text = trans.MerchantIdValor
                        Me.UrlError.Text = trans.UrlErrorValor
                        Me.txtMoneda.Text = trans.MonedaValor
                        Me.MailCom.Text = trans.MailComValor
                        _3Dev.FW.Web.Log.Logger.LogMessage(trans.MontoValor.ToString())

                    Else

                        txtOrdenId2.Text = trans.OrdenIdValor
                        txtUrl.Text = urlServicio
                        Me.txtUsuarioId2.Text = trans.UsuarioIDValor
                        Me.txtOrdenId2.Text = trans.OrdenIdValor
                        Me.txtMonto2.Text = trans.MontoValor
                        txtOrdenDesc2.Text = HttpUtility.UrlDecode(trans.OrdenDescValor)
                        Me.txtUrlOk2.Text = trans.UrlOkValor
                        Me.txtAlias2.Text = trans.UsuarioAliasValor
                        Me.txtDataAdicional2.Text = HttpUtility.UrlDecode(trans.DataAdicionalValor)
                        Me.txtApellidos2.Text = HttpUtility.UrlDecode(trans.UsuarioApellidosValor)
                        Me.txtLocalidad2.Text = HttpUtility.UrlDecode(trans.UsuarioLocalidadValor)
                        Me.txtNombre2.Text = HttpUtility.UrlDecode(trans.UsuarioNombreValor)
                        Me.txtDomicilio2.Text = HttpUtility.UrlDecode(trans.UsuarioDomicilioValor)
                        Me.txtProvincia2.Text = trans.UsuarioProvinciaValor
                        Me.txtPais2.Text = trans.UsuarioPaisValor
                        Me.txtEmpresa2.Text = trans.MerchantIdValor
                        Me.txtUrlError2.Text = trans.UrlErrorValor
                        Me.txtMoneda2.Text = trans.MonedaValor
                        Me.txtMail2.Text = HttpUtility.UrlDecode(trans.MailComValor)
                        _3Dev.FW.Web.Log.Logger.LogMessage("else" & trans.MontoValor.ToString())
                    End If

                    If trans.MontoValor <> "" Or trans.MontoValor <> Nothing Then
                        monto = TraductorServ.ObtenerMontoDeFormatoServicio(trans.MontoValor).ToString()
                    Else
                        Throw New SPE.Exceptions.TramaGetException("monto")
                    End If

                    MerchantID = trans.MerchantIdValor
                    urlServicio = trans.UrlServicioValor
                Catch ex2 As SPE.Exceptions.TramaGetException


                Catch ex As Exception

                    Throw New SPE.Exceptions.URLNoEncontradaExcepcion(ex.Message)

                End Try

                trans.ValidarCamposRequeridos()
                '
                If urlServicio <> "" Then
                    Dim obeServicio As New BEServicio
                    Dim objCServicio As New SPE.Web.CServicio
                    obeServicio = objCServicio.ConsultarServicioPorUrl(urlServicio)
                    If obeServicio Is Nothing Then
                        'Agregado para mostrar aviso de validacion
                        Me.txtValidacion.Text = "No, revise contrato Xml y la BD"
                    Else
                        idservicio = obeServicio.IdServicio
                        idempresa = obeServicio.IdEmpresaContratante
                        Me.txtValidacion.Text = "Se encontró la Url..Ok"
                    End If
                Else
                    Response.Redirect("~/ServicioNoEncontrado.aspx")
                End If
                Dim objclsLibreriax As New ClsLibreriax
                Dim sbparUtil As New StringBuilder
                Dim sbparametersUrl As New StringBuilder

                sbparUtil.Append("parIdServicio=") : sbparUtil.Append(idservicio.ToString) : sbparUtil.Append("|")
                sbparUtil.Append("parIdEmpresa=") : sbparUtil.Append(idempresa.ToString) : sbparUtil.Append("|")
                sbparUtil.Append("parUsaEncriptacion=") : sbparUtil.Append(trans.UsaEncriptacionValor) : sbparUtil.Append("|")
                sbparUtil.Append("parUrlServicio=") : sbparUtil.Append(urlServicio.ToString)
                If (trans.UsaEncriptacionValor) Then

                    sbparametersUrl.Append(trans.MerchantId) : sbparametersUrl.Append("=") : sbparametersUrl.Append(MerchantID) : sbparametersUrl.Append("&")
                    sbparametersUrl.Append(trans.DatosEnc) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.DatosEncValor) : sbparametersUrl.Append("&")
                Else
                    sbparametersUrl.Append(trans.MerchantId) : sbparametersUrl.Append("=") : sbparametersUrl.Append(MerchantID) : sbparametersUrl.Append("&")
                    sbparametersUrl.Append(trans.Monto) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.MontoValor) : sbparametersUrl.Append("&")
                    sbparametersUrl.Append(trans.UsuarioID) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.UsuarioIDValor) : sbparametersUrl.Append("&")
                    sbparametersUrl.Append(trans.UsuarioAlias) : sbparametersUrl.Append("=") : sbparametersUrl.Append(trans.UsuarioAliasValor) : sbparametersUrl.Append("&")
                End If
                sbparametersUrl.Append("parUtil") : sbparametersUrl.Append("=") : sbparametersUrl.Append(objclsLibreriax.Encrypt(sbparUtil.ToString, objclsLibreriax.fEncriptaKey))
            Catch ex2 As SPE.Exceptions.TramaGetException
            Catch ex1 As SPE.Exceptions.URLNoEncontradaExcepcion
            Catch ex3 As Exception
            End Try

        End If
    End Sub
End Class





