<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCAudit.ascx.vb" Inherits="UC_UCAudit" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="dvContentAudit">
            <label id="titleAudit">
                Auditoría</label>
            <ul>
                <li>
                    <label>
                        Fecha Creación:</label>
                    <asp:Label ID="lblFechaCreacion" runat="server" Text=""></asp:Label>
                </li>
                <li>
                    <label>
                        Creado Por:</label>
                    <asp:Label ID="lblCreadoPor" runat="server" Text=""></asp:Label>
                </li>
                <li>
                    <label>
                        Fecha Actualización:</label>
                    <asp:Label ID="lblFechaActualizacion" runat="server" Text=""></asp:Label>
                </li>
                <li>
                    <label>
                        Actualizado Por:</label>
                    <asp:Label ID="lblActualizadoPor" runat="server" Text=""></asp:Label>
                </li>
            </ul>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
