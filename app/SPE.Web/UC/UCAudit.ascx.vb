Imports _3Dev.FW.Entidades

Partial Class UC_UCAudit
    Inherits System.Web.UI.UserControl

    Const _ConstFLAGAUDIT As String = "Audit_FlagAudit"

    Public Sub PaintAudit()
        'If (IsVisible()) Then
        '    me.Visible =
        'End If
        Me.Visible = IsVisible()
        If (_lnkbtnAction IsNot Nothing) Then
            If (Me.Visible) Then
                _lnkbtnAction.Text = "[Ocultar Auditor�a]"
            Else
                _lnkbtnAction.Text = "[Mostrar Auditor�a]"
            End If
        End If
    End Sub
    Public Sub ShowAudit()
        SetFlagCommon(True)
    End Sub
    Public Sub HideAudit()
        SetFlagCommon(False)
    End Sub
    Private _lnkbtnAction As LinkButton = Nothing
    Public Sub InitializeAudit(ByVal btnlinkAction As LinkButton)
        _lnkbtnAction = btnlinkAction
        If (_lnkbtnAction IsNot Nothing) Then
            AddHandler _lnkbtnAction.Click, AddressOf lnkbtnAction_Click
        End If
        PaintAudit()
    End Sub
    Protected Sub lnkbtnAction_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If (IsVisible()) Then
            HideAudit()
        Else
            ShowAudit()
        End If
    End Sub
    Public Function IsVisible() As Boolean
        key = _ConstFLAGAUDIT
        Dim obeflagShowAudit As BEKeyValue(Of String, Object) = _3Dev.FW.Web.PageBase.UserInfo.SessionUser.Find(PredicateSessionUser)
        If (obeflagShowAudit Is Nothing) Then
            Return False
        Else
            Return obeflagShowAudit.Value
        End If
    End Function

    Public Sub SetFlagCommon(ByVal flag As Boolean)
        key = _ConstFLAGAUDIT
        Dim obeflagShowAudit As BEKeyValue(Of String, Object) = _3Dev.FW.Web.PageBase.UserInfo.SessionUser.Find(PredicateSessionUser)
        If (obeflagShowAudit Is Nothing) Then
            _3Dev.FW.Web.PageBase.UserInfo.SessionUser.Add(New BEKeyValue(Of String, Object)(_ConstFLAGAUDIT, flag))
        Else
            obeflagShowAudit.Value = flag
        End If
        PaintAudit()
    End Sub

    Public key As String = ""
    Public Function EvaluaSessionUser(ByVal obeKeyValue As _3Dev.FW.Entidades.BEKeyValue(Of String, Object)) As Boolean
        Return obeKeyValue.Key = key
    End Function
    Dim PredicateSessionUser As New Predicate(Of _3Dev.FW.Entidades.BEKeyValue(Of String, Object))(AddressOf EvaluaSessionUser)

    Public Sub RefreshView(ByVal creationDate As DateTime, ByVal updatedate As DateTime, ByVal usercreationName As String, ByVal userupdateName As String)
        lblCreadoPor.Text = usercreationName
        lblActualizadoPor.Text = userupdateName
        lblFechaCreacion.Text = creationDate.ToShortDateString()
        lblFechaActualizacion.Text = updatedate.ToShortDateString()
    End Sub

    Public Sub RefreshView(ByVal obeAudit As SPE.Entidades.IBEAuditoria)
        If (obeAudit IsNot Nothing) Then
            RefreshView(obeAudit.FechaCreacion, obeAudit.FechaActualizacion, obeAudit.UsuarioCreacionNombre, obeAudit.UsuarioActualizacionNombre)
        End If

    End Sub



End Class
