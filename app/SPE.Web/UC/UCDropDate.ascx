<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCDropDate.ascx.vb" Inherits="SPE.Web.UC.UC_UCDropDate" %>
 <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="margin-top: 0px;" >
   <ContentTemplate>
      <p class="cal">
            <asp:DropDownList ID="ddlFechaMes" runat="server" AutoPostBack="true"  CssClass="corta3" OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="ddlFechaDia" runat="server"  CssClass="corta3" >
            </asp:DropDownList>
            <asp:DropDownList ID="ddlFechaAnio" runat="server" AutoPostBack="true"   CssClass="corta3" OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged" >
            </asp:DropDownList>
            <strong id="strgFechaNacimiento" runat="server" style="color: darkgray">*</strong>     
            <asp:RequiredFieldValidator ID="rfvFechaDia" runat="server" ControlToValidate="ddlFechaDia" ErrorMessage="Seleccione el d�a.">*</asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvFechaMes" runat="server" ControlToValidate="ddlFechaMes" ErrorMessage="Seleccione el mes">*</asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvFechaAnio" runat="server" ControlToValidate="ddlFechaAnio" ErrorMessage="Seleccione el a�o">*</asp:RequiredFieldValidator>
        </p>
    </ContentTemplate>
  </asp:UpdatePanel>
