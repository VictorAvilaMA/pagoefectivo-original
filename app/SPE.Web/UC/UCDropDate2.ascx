<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCDropDate2.ascx.vb" Inherits="SPE.Web.UC.UC_UCDropDate2" %>
 <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
   <ContentTemplate>
   
                        <div class="w50 bgf8f8f8  ">
                            <dl class="dt45 dd45 clearfix no_padding wdocu3">
                                <dt>
                                    <label for="r_fecNac">Fecha de Nacimiento:<span id="strgFechaNacimiento"  runat="server" class="required_red ">*</span></label>
                                </dt>
                                <dd>
<%--                                    <select id="r_cboDay" name="r_cboDay" class="">
										<option value="">Seleccionar</option>
                                        <option value="01">01</option>
                                    </select>  --%>       
                                        <asp:DropDownList ID="ddlFechaMes" runat="server"  CssClass="fleft a1" AutoPostBack="true"   OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                                        </asp:DropDownList>                                                               
                                    <asp:RequiredFieldValidator ID="rfvFechaMes" runat="server" ControlToValidate="ddlFechaMes" ErrorMessage="Seleccione el mes de nacimiento.">*</asp:RequiredFieldValidator>    
                                </dd>
                            </dl>
							 <dl class="dt20 dd50 clearfix no_padding  wnac">
                                <dt>
                                   <%--<select id="r_cboMonth" name="r_cboMonth" class="">
										<option value="">Seleccionar</option>
                                        <option value="01">01</option>
                                    </select>--%>
                                    <asp:DropDownList ID="ddlFechaDia" runat="server"  CssClass="fleft a2"  >
                                    </asp:DropDownList>                                    
                                    <asp:RequiredFieldValidator ID="rfvFechaDia" runat="server" ControlToValidate="ddlFechaDia" ErrorMessage="Seleccione el d�a de nacimiento.">*</asp:RequiredFieldValidator>
                                </dt>
                                <dd>                                
<%--                                    <select id="r_cboYear" name="r_cboYear" class="">
										<option value="">Seleccionar</option>
                                        <option value="2010">2010</option>
                                    </select>
--%>                            
                                    <asp:DropDownList ID="ddlFechaAnio" runat="server" AutoPostBack="true" CssClass="fleft a3" OnSelectedIndexChanged="ddlFecha_SelectedIndexChanged">
                                    </asp:DropDownList>    
                                    <asp:RequiredFieldValidator ID="rfvFechaAnio" runat="server" ControlToValidate="ddlFechaAnio" ErrorMessage="Seleccione la fecha de nacimiento."></asp:RequiredFieldValidator>                                    
                                    
                                </dd>
                            </dl>
							
                        </div>   
   
   
   
    </ContentTemplate>
  </asp:UpdatePanel>
