Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization

Imports _3Dev.FW.Entidades

Namespace SPE.Web.UC


    Partial Class UC_UCDropDate2
        Inherits UCDropDateBase

        Public Sub New()
            _flagPreMostrarCamposMandatorios = False

        End Sub

        Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
            MyBase.OnInit(e)
            MyBase.Initialize(ddlFechaDia, ddlFechaMes, ddlFechaAnio)
        End Sub

        Protected _flagPreMostrarCamposMandatorios As Boolean
        Public Property FlagPreMostrarCamposMandatorios() As Boolean
            Get
                Return _flagPreMostrarCamposMandatorios
            End Get
            Set(ByVal value As Boolean)
                _flagPreMostrarCamposMandatorios = value
            End Set
        End Property

        Public Sub PreMostrarCamposMandatorios()
            strgFechaNacimiento.Visible = _flagPreMostrarCamposMandatorios
        End Sub


        '        Public Sub New()
        '            _scriptManagerId = "ScriptManager"
        '        End Sub
        '#Region "propiedades"
        '        Public _scriptManagerId As String
        '        Public Property ScriptManagerId() As String
        '            Get
        '                Return _scriptManagerId
        '            End Get
        '            Set(ByVal value As String)
        '                _scriptManagerId = value
        '            End Set
        '        End Property

        '        Public ReadOnly Property FechaDia() As DropDownList
        '            Get
        '                Return ddlFechaDia
        '            End Get
        '        End Property

        '        Public ReadOnly Property FechaMes() As DropDownList
        '            Get
        '                Return ddlFechaMes
        '            End Get
        '        End Property

        '        Public ReadOnly Property FechaAnio() As DropDownList
        '            Get
        '                Return ddlFechaAnio
        '            End Get
        '        End Property

        '#End Region

        '        Public Function DameFechaSeleccionada()
        '            Return DateTime.Now
        '        End Function

        '        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        '            MyBase.OnLoad(e)
        '            If Not Page.IsPostBack Then
        '                GenerarAnio()
        '                GenerarMeses()
        '                GenerarDias()
        '            End If
        '        End Sub

        '        Private Sub GenerarAnio()

        '            Dim anio As Integer = Now.Year
        '            ddlFechaAnio.Items.Clear()
        '            ddlFechaAnio.Items.Add(New ListItem("", ""))

        '            For i As Integer = 0 To 100
        '                ddlFechaAnio.Items.Add(New ListItem(anio - 1, anio - 1))
        '                anio = anio - 1
        '            Next

        '        End Sub

        '        Private Sub GenerarMeses()
        '            ddlFechaMes.Items.Clear()
        '            ddlFechaMes.Items.Add(New ListItem("", ""))

        '            Dim cultureInfo As CultureInfo = cultureInfo.CreateSpecificCulture("es-PE")
        '            Dim meses As String() = cultureInfo.DateTimeFormat.MonthNames()
        '            Dim list As New List(Of _3Dev.FW.Entidades.BEKeyValue(Of String, String))()
        '            For Each mes As String In meses
        '                ddlFechaMes.Items.Add(New ListItem(mes, mes))
        '            Next

        '        End Sub

        '        Private Sub GenerarDias()
        '            'Date.
        '            ddlFechaDia.Items.Clear()
        '            ddlFechaDia.Items.Add(New ListItem("", ""))
        '            Dim anio As String = ddlFechaAnio.SelectedValue
        '            Dim mes As String = ddlFechaMes.SelectedValue
        '            Dim dias As Integer = 31
        '            If Not ((anio = "") Or (mes = "")) Then
        '                dias = Date.DaysInMonth(anio, mes)
        '            End If

        '            For i As Integer = 1 To dias
        '                ddlFechaDia.Items.Add(New ListItem(i, i))
        '            Next
        '        End Sub


        '        Protected Sub ddlFecha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '            GenerarDias()
        '        End Sub
    End Class

End Namespace