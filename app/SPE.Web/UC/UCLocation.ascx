<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCLocation.ascx.vb" Inherits="UC_UCLocation" %>
<ul class="datos_cip2">
    <li class="t1"><span class="color">>></span> Pa�s: </li>
    <li class="t2">
        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
            AutoPostBack="True">
        </asp:DropDownList>
        <strong id="strgCountry" runat="server" style="color: darkgray">*</strong> </li>
    <li class="t1"><span class="color">>></span> Departamento: </li>
    <li class="t2">
        <asp:DropDownList ID="ddlDepartament" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlDepartament_SelectedIndexChanged"
            AutoPostBack="True">
        </asp:DropDownList>
        <strong id="strgDepartament" runat="server" style="color: darkgray">*</strong>
    </li>
    <li class="t1"><span class="color">>></span> Ciudad: </li>
    <li class="t2">
        <asp:DropDownList ID="ddlCity" runat="server" CssClass="corta">
        </asp:DropDownList>
        <strong id="strgCity" runat="server" style="color: darkgray">*</strong>
        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ErrorMessage="Seleccione una Ciudad"
            ControlToValidate="ddlCity">*</asp:RequiredFieldValidator>
    </li>
</ul>
