
Partial Class UC_UCLocation
    Inherits System.Web.UI.UserControl



    Public ReadOnly Property Country() As DropDownList
        Get
            Return ddlCountry
        End Get
    End Property

    Public ReadOnly Property Departament() As DropDownList
        Get
            Return ddlDepartament
        End Get
    End Property

    Public ReadOnly Property City() As DropDownList
        Get
            Return ddlCity
        End Get
    End Property

    Public Sub InitializeView()
        SelectCountry(0)
    End Sub

    Public Sub RefreshView(ByVal countryId As Integer, ByVal departmentId As Integer, ByVal cityId As Integer)
        Me.SelectCountry(countryId)
        Me.SelectDepartament(departmentId)
        Me.SelectCity(cityId)
        Me.ddlCity.SelectedValue = cityId
    End Sub

    Public Sub RefreshView(ByVal obeLocation As SPE.Entidades.IBEUbigeo)
        RefreshView(obeLocation.IdPais, obeLocation.IdDepartamento, obeLocation.IdCiudad)
    End Sub

    Public Function LoadEntityLocation(ByVal obeLocation As SPE.Entidades.IBEUbigeo) As SPE.Entidades.IBEUbigeo
        obeLocation.IdPais = Me.ddlCountry.SelectedValue
        obeLocation.IdDepartamento = Me.ddlDepartament.SelectedValue
        obeLocation.IdCiudad = Me.ddlCity.SelectedValue
        Return obeLocation
    End Function


#Region "Ubicacion"
    Public Sub SelectCountry(ByVal countryId As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlCountry, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        If countryId > 0 Then
            Me.ddlCountry.SelectedValue = countryId
        End If
    End Sub

    Public Sub SelectDepartament(ByVal departamentId As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlCountry.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlCountry.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlDepartament, objCUbigeo.ConsultarDepartamentoPorIdPais(i), "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)

        If departamentId > 0 Then
            Me.ddlDepartament.SelectedValue = departamentId
        Else
            Me.ddlDepartament.SelectedIndex = 0
        End If
    End Sub

    Public Sub SelectCity(ByVal cityId As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlDepartament.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlDepartament.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlCity, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        If cityId > 0 Then
            Me.ddlCity.SelectedValue = cityId
        Else
            Me.ddlCity.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SelectDepartament(0)
        SelectCity(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlCountry)
    End Sub

    Protected Sub ddlDepartament_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SelectCity(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlDepartament)
    End Sub

#End Region

End Class
