<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSeccionCentrosAutoriz.ascx.vb"
    Inherits="UC_UCSeccionCentrosAutoriz" %>
<div class="centros_autorizados decocaja">
    <h2>
        Centros Afiliados</h2>
    <h3>
        Puedes realizar sus pagos mediante:</h3>
    <!--<ul class="pagoeagencia">
	<li>Pagos en Agencia</li>
    <li class="padingright0">Internet por Via BCP</li>
</ul>-->
    <div class="eresnoerescliente2">
        <h2 class="subtitle_bcp">
            Banco de Cr&eacute;dito BCP</h2>
        <ul>
            <li>Agencias bancarias </li>
            <li>Banca por internet www.viabcp.com </li>
            <li>Agentes BCP </li>
        </ul>
    </div>
    <div class="eresnoerescliente2">
        <h2 class="subtitle_bbva">
            Banco BBVA Continental</h2>
        <ul>
            <li>Agencias bancarias </li>
            <li>Banca por internet www.bbvabancocontinental.com </li>
            <li>Agentes Express </li>
            <li>Agentes Kasnet </li>
        </ul>
    </div>
    <div class="eresnoerescliente2">
        <h2 class="subtitle_fc">
            FullCarga</h2>
        <ul>
            <li>Puntos de venta:</li>
            <li>Bodegas</li>
            <li>Locutorios</li>
            <li>Farmacias</li>
            <li>Cabinas</li>
        </ul>
    </div>
</div>
