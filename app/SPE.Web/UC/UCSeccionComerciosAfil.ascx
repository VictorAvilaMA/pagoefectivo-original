<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSeccionComerciosAfil.ascx.vb"
    Inherits="UC_UCSeccionComerciosAfil" %>
<div class="bienvenido_pe decocaja">
    <h2>
        Comercios Afiliados</h2>
    <div class="empresas_afilidadas_pe">
        <ul id="scroller">
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_suscriptores.jpg" alt="Suscriptores" title="Suscriptores" width="150"
                    height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_neoauto.jpg" alt="Neoauto" title="Neoauto" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_urbania.jpg" alt="Urbania" title="Urbania" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_kotear.jpg" alt="Kotear" title="Kotear" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_ofertop.jpg" alt="OferTOP" title="OferTOP" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_aptitus.jpg" alt="Aptitus" title="Aptitus" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_perured.jpg" alt="Per�Red" title="Per�Red" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_mapfre.jpg" alt="MAPFRE" title="MAPFRE" width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_nuestromercado.jpg" alt="NuestroMercado" title="NuestroMercado"
                    width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_quioscodigital.jpg" alt="QuioscoDigital" title="QuioscoDigital"
                    width="150" height="58" /></li>
            <li>
                <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/logo_vozt.jpg" alt="Vozt" title="Vozt" width="150" height="58" /></li>
        </ul>
    </div>
</div>
