<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSeccionInfo.ascx.vb"
    Inherits="UC_UCSeccionInfo" %>
<h2 class="title-seccion">
    <span>&nbsp;</span> Preguntas Frecuentes</h2>
<div class="content-slip">
    <div id="test1-header" class="title-faqs">
        <h3>
            �Necesito abrir una cuenta en PagoEfectivo?</h3>
        <a href="#faq1" class="slip"></a>
    </div>
    <div class="faqs-content" id="faq1">
        No es necesario, debido a que PagoEfectivo te crear� una cuenta de usuario cuando
        generes el c�digo de pago (CIP).<br />
        La cuenta se crear� con el correo electr�nico que utilices para hacer la compra.
        <br />
        con esta cuenta tendr�s un control adecuado de tus c�digos pendientes de pago.
    </div>
    <div id="test2-header" class="title-faqs">
        <h3>
            �PagoEfectivo comparte tu informaci�n?</h3>
        <a href="#faq2" class="slip"></a>
    </div>
    <div class="faqs-content" id="faq2">
        No. La informaci�n personal registrada en PagoEfectivo, no es compartida con el
        negocio proveedor del bien o servicio.
        <br />
        Los negocios tienen �nicamente la opci�n de estad�sticas, para lo cual s�lo se les
        informar� el c�digo de pago y el monto.
    </div>
    <div id="test3-header" class="title-faqs">
        <h3>
            �Cu�nto tiempo tengo para pagar el CIP?</h3>
        <a href="#faq3" class="slip"></a>
    </div>
    <div class="faqs-content" id="faq3">
        Este per�odo var�a de negocio a negocio, de acuerdo al per�odo que se haya gestionado.<br />
        <br />
        Al momento de generar el c�digo de pago (CIP), el Comercio Afiliado le indicar�
        cuanto tiempo tiene para pagar.
    </div>
</div>
