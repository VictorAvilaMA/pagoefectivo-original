﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSeccionSuscribete.ascx.vb"
    Inherits="UC_UCSeccionSuscribete" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:ToolkitScriptManager ID="ScriptManager01" runat="server">
</cc1:ToolkitScriptManager>

<script type="text/javascript">
    $(document).ready(function () {
        $('#ctl00_ContentPlaceHolder1_ctl00').click(function () {
            if ($('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_hdfSuscribeteExistente').val() != '') {
                $('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_hdfSuscribeteExistente').val('');
                $('#MensajeCorreoExistente').remove();
            }
        });


    });
    function CorreoExistente() {
        $('.control').append('<span style="color: Red; display: inline;" id="MensajeCorreoExistente">Su email ya se encuentra registrado.</span>');
    }
    function CorreoNuevo() {
        $('.control').append('<span style="color: Red; display: inline;" id="MensajeCorreoExistente">Tu email ha sido suscrito con éxito.</span>');
    }
</script>
<div class="box-suscribe">
    <span class="buzon-img"></span>
    <div class="sbt-content">
        <h2>
            Suscríbete!</h2>
        <p>
            Recibe las últimas noticias y<br />
            promociones especiales de<br />
            nuestros comercios afiliados</p>
        <div class="control">
            <asp:TextBox runat="server" ValidationGroup="ValSusc" AutoComplete="off" ID="txtEmailSuscriptor"
                CssClass="input-mail" MaxLength="100"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="ftbetxtCorreo" runat="server" TargetControlID="txtEmailSuscriptor"
                ValidChars="-_.@" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                InvalidChars="áéíóúÁÉÍÓÚ ">
            </cc1:FilteredTextBoxExtender>
            <asp:RequiredFieldValidator ID="rfvEmailPrincipal" runat="server" ControlToValidate="txtEmailSuscriptor"
                ErrorMessage="Ingrese su correo electrónico." Display="Dynamic" ValidationGroup="ValSusc"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailSuscriptor"
                Display="Dynamic" ErrorMessage="Ingrese correo electrónico válido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ValidationGroup="ValSusc">
            </asp:RegularExpressionValidator>
            <%--<asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" Style="padding-left: 200px" />--%>
            <%--<input id="textMail" type="text" class="input-mail" />--%>
        </div>
        <%--<button type="submit" class="btn-sbt">
            Suscríbete</button>--%>
        <asp:Button ID="btnSuscribete" runat="server" Text="Suscríbete" CssClass="btn-sbt"
            ValidationGroup="ValSusc" />
        <asp:HiddenField runat="server" ID="hdfSuscribeteExistente" />
    </div>
</div>
