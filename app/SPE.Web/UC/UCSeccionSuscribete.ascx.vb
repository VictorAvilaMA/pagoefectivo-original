﻿Partial Class UC_UCSeccionSuscribete
    Inherits System.Web.UI.UserControl
    'ctl00_ContentPlaceHolder1_ctl00


    Protected Sub btnSuscribete_Click(sender As Object, e As System.EventArgs) Handles btnSuscribete.Click
        Dim oCntrComun As New SPE.Web.CComun
        Dim Retorno As String
        Dim oBESuscriptor As New SPE.Entidades.BESuscriptor
        Dim script As New StringBuilder
        hdfSuscribeteExistente.Value = ""

        oBESuscriptor.Correo = txtEmailSuscriptor.Text.Trim
        Retorno = oCntrComun.SuscriptorRegistrar(oBESuscriptor)
        If Not String.IsNullOrEmpty(Retorno) Then
            script.AppendLine("CorreoExistente();")
            'script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').css('visibility', 'visible');")
            ScriptManager.RegisterClientScriptBlock(Me, Page.GetType, "MP1", script.ToString(), True)
            hdfSuscribeteExistente.Value = "SuscriptorExistente"
        Else
            script.AppendLine("CorreoNuevo();")
            'script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').css('visibility', 'visible');")
            ScriptManager.RegisterClientScriptBlock(Me, Page.GetType, "MP2", script.ToString(), True)
            hdfSuscribeteExistente.Value = "SuscriptorNuevo"
            Dim cemail As New SPE.Web.Util.UtilEmail()
            Dim intRpta As Integer = cemail.EnviarCorreoSuscriptor(oBESuscriptor.Correo)


        End If

        Me.txtEmailSuscriptor.Text = String.Empty


        'If IsValidEmail(txtEmailSuscriptor.Text.Trim) Then
        '    oBESuscriptor.Correo = txtEmailSuscriptor.Text.Trim
        '    Retorno = oCntrComun.SuscriptorRegistrar(oBESuscriptor)
        '    If Not String.IsNullOrEmpty(Retorno) Then
        '        script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').text('Su email ya se encuentra registrado.');")
        '        script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').css('visibility', 'visible');")
        '        ScriptManager.RegisterClientScriptBlock(Me, Page.GetType, "MP1", script.ToString(), True)
        '        hdfSuscribeteExistente.Value = "SuscriptorExistente"
        '    Else
        '        script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').text('jeffer');")
        '        script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').css('visibility', 'hidden');")
        '        ScriptManager.RegisterClientScriptBlock(Me, Page.GetType, "MP1", script.ToString(), True)
        '    End If

        '    Me.txtEmailSuscriptor.Text = String.Empty
        'Else
        '    script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_RegularExpressionValidator1').css('margin-top','-12px');")
        '    'script.AppendLine("$('#ctl00_ContentPlaceHolder1_UCSeccionSuscribete1_rfvEmailPrincipal').css('visibility', 'visible');")
        '    ScriptManager.RegisterClientScriptBlock(Me, Page.GetType, "MP2", script.ToString(), True)
        '    Return
        'End If



    End Sub

   
End Class
