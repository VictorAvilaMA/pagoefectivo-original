<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCUsuarioDatosComun.ascx.vb"
    Inherits="UC_UCUsuarioDatosComun" %>
<%@ Register Src="UCDropDate.ascx" TagName="ucDropDate" TagPrefix="spe" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<cc1:FilteredTextBoxExtender ID="FTBE_TxtApellidos" runat="server" TargetControlID="txtApellidos"
    ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' ">
</cc1:FilteredTextBoxExtender>
<cc1:FilteredTextBoxExtender ID="FTBE_TxtNombres" runat="server" TargetControlID="txtNombres"
    ValidChars="Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWw��XxYyZz' ">
</cc1:FilteredTextBoxExtender>
<cc1:FilteredTextBoxExtender ID="FTBE_TxtTelefono" runat="server" TargetControlID="txtTelefono"
    ValidChars="0123456789- ">
</cc1:FilteredTextBoxExtender>
<ul class="datos_cip2 h0">
    <li class="t1"><span class="color">>></span> Nombres: </li>
    <li class="t2">
        <asp:TextBox ID="txtNombres" runat="server" CssClass="normal" MaxLength="1000"></asp:TextBox>
        <strong id="strgNombres" runat="server" style="color: darkgray">*</strong>
        <asp:RequiredFieldValidator ID="rfvNombres" runat="server" ControlToValidate="txtNombres"
            ErrorMessage="Ingrese su nombre">*</asp:RequiredFieldValidator>
    </li>
</ul>
<ul class="datos_cip2 h0">
    <li class="t1"><span class="color">>></span> Apellidos: </li>
    <li class="t2">
        <asp:TextBox ID="txtApellidos" runat="server" CssClass="normal" ReadOnly="false"
            MaxLength="100"></asp:TextBox>
        <strong id="strgApellidos" runat="server" style="color: darkgray">*</strong>
        <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ControlToValidate="txtApellidos"
            ErrorMessage="Ingrese su apellido" Style="display: none" EnableTheming="True">*</asp:RequiredFieldValidator>
    </li>
</ul>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" style="margin-top: 0px;">
    <ContentTemplate>
        <ul class="datos_cip2">
            <li class="t1"><span class="color">>></span> Documento: </li>
            <li class="t2">
                <p class="cal">
                    <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="corta2" AutoPostBack="true"
                        Style="width: 133px">
                    </asp:DropDownList>
                    <asp:Label ID="labelNumero" runat="server" Text="N�:" CssClass="entre"></asp:Label>
                    <asp:TextBox ID="txtNumeroDocumento" runat="server" CssClass="corta" MaxLength="11"></asp:TextBox>
                    <strong id="strgTipoDocumento" runat="server" style="color: darkgray">*</strong>
                    <asp:RequiredFieldValidator ID="rfvTipoDocumento" runat="server" ControlToValidate="ddlTipoDocumento"
                        ErrorMessage="Seleccione un tipo de documento.">*</asp:RequiredFieldValidator>
                    <strong id="strgNroDocumento" runat="server" style="color: darkgray">*</strong>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNumeroDocumento"
                        ErrorMessage="Ingrese el n�mero de documento.">*</asp:RequiredFieldValidator>
                </p>
            </li>
        </ul>
    </ContentTemplate>
</asp:UpdatePanel>
<ul class="datos_cip2 h0">
    <li class="t1"><span class="color">>></span>
        <asp:Label ID="lblGenero" runat="server" Text="G�nero:"></asp:Label>
    </li>
    <li class="t2">
        <p class="cal">
            <asp:DropDownList ID="ddlGenero" runat="server" CssClass="corta">
            </asp:DropDownList>
            <strong id="strgGenero" runat="server" style="color: darkgray">*</strong>
            <asp:RequiredFieldValidator ID="rfvGenero" runat="server" ControlToValidate="ddlGenero"
                ErrorMessage="Seleccione su g�nero.">*</asp:RequiredFieldValidator>
        </p>
    </li>
</ul>
<ul class="datos_cip2" style="width:700px;">
    <li class="t1"><span class="color">>></span> Fecha Nacimiento: </li>
    <li class="t2" style="width:500px;">
        <spe:ucDropDate ID="ucFechaNacimiento" runat="server" />
    </li>
</ul>
<ul class="datos_cip2 h0">
    <li class="t1"><span class="color">>></span> Tel�fono: </li>
    <li class="t2">
        <asp:TextBox ID="txtTelefono" runat="server" CssClass="normal" MaxLength="15"></asp:TextBox>
    </li>
</ul>
<asp:UpdatePanel ID="Ubigeo" runat="server" style="margin-top: 0px;">
    <ContentTemplate>
        <ul class="datos_cip2 f0" style="clear:both">
            <li class="t1 f0"><span class="color">>></span> Pa�s: </li>
            <li class="t2 f0">
                <p class="cal">
                    <asp:DropDownList ID="ddlPais" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <strong id="strgPais" runat="server" style="color: darkgray">*</strong>
                </p>
            </li>
        </ul>
        <ul class="datos_cip2 f0" style="clear:both">
            <li class="t1 f0"><span class="color">>></span> Departamento: </li>
            <li class="t2 f0">
                <p class="cal">
                    <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="corta" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <strong id="strgDepartamento" runat="server" style="color: darkgray">*</strong>
                </p>
            </li>
        </ul>        
        <ul class="datos_cip2 f0" style="clear:both">
            <li class="t1 f0"><span class="color">>></span> Ciudad: </li>
            <li class="t2 f0">
                <p class="cal">
                    <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="corta">
                    </asp:DropDownList>
                    <strong id="strgCiudad" runat="server" style="color: darkgray">*</strong>
                    <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="Seleccione una Ciudad"
                        ControlToValidate="ddlCiudad">*</asp:RequiredFieldValidator>
                </p>
            </li>
        </ul>
    </ContentTemplate>
</asp:UpdatePanel>
<ul class="datos_cip2">
    <li class="t1"><span class="color">>></span> Direcci�n: </li>
    <li class="t2">
        <asp:TextBox ID="txtDireccion" runat="server" CssClass="normal" MaxLength="100"></asp:TextBox>
    </li>
</ul>
