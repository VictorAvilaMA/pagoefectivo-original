<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCUsuarioDatosComun2.ascx.vb"
    Inherits="UC_UCUsuarioDatosComun2" %>
<%@ Register Src="UCDropDate2.ascx" TagName="UCDropDate2" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div class="w100 bgf8f8f8 ">
    <dl class="dt15 dd40 clearfix no_padding">
        <dt>
            <label for="r_txtNombrse">
                Nombres:<span id="strgNombres" runat="server" class="required_red ">*</span></label>
        </dt>
        <dd>
            <%--<input name="r_txtNombres" type="password" class="text" id="r_txtNombres"  maxlength="25" />--%>
            <asp:TextBox ID="txtNombres" runat="server" CssClass="text" MaxLength="1000"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvNombres" runat="server" ControlToValidate="txtNombres"
                ErrorMessage="Ingrese su nombre."></asp:RequiredFieldValidator>
        </dd>
    </dl>
</div>
<div class="w100 ">
    <dl class="dt15 dd40 clearfix no_padding">
        <dt>
            <label for="r_txtApellidos">
                Apellidos:<span id="strgApellidos" runat="server" class="required_red ">*</span></label>
        </dt>
        <dd>
            <asp:TextBox ID="txtApellidos" runat="server" CssClass="text" ReadOnly="false" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvApellidos" runat="server" ControlToValidate="txtApellidos"
                ErrorMessage="Ingrese su apellido."></asp:RequiredFieldValidator>
        </dd>
    </dl>
</div>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" class="w50  bgf8f8f8 ">
    <ContentTemplate>
        <dl class="dt45 dd40 clearfix no_padding wdocu">
            <dt>
                <label for="r_cboDocumento">
                    Documento:<span id="strgTipoDocumento" runat="server" class="required_red ">*</span></label>
            </dt>
            <dd>
                <asp:DropDownList ID="ddlTipoDocumento" runat="server" CssClass="wauto fleft" AutoPostBack="true">
                </asp:DropDownList>
            </dd>
        </dl>
        <dl class="dt5 dd60 clearfix no_padding wdocu2">
            <dt>
                <label class="txt_s1 inline" for="r_txtNdocumento">
                    N�:<span id="strgNroDocumento" runat="server" class="required_red " style="display: none;">*</span></label>
            </dt>
            <dd>
                <%--<input name="r_txtNdocumento" type="text " class="text" id="r_txtNdocumento"  maxlength="25" />					--%>
                <asp:TextBox ID="txtNumeroDocumento" runat="server" CssClass="text fleft dni" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNumeroDocumento"
                    ErrorMessage="Ingrese N� de documento."></asp:RequiredFieldValidator>
            </dd>
        </dl>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="w100">
    <dl class="dt15 dd50 clearfix no_padding">
        <dt>
            <label for="r_cboGenero">
                G&eacute;nero:<span id="strgGenero" runat="server" class="required_red ">*</span></label>
        </dt>
        <dd>
            <%--                                    <select id="r_cboGenero" name="r_cboGenero" class="">
                                        <option value="">Seleccionar</option>
                                        <option value="">Masculino</option>
                                        <option value="">Femenino</option>
                                    </select>--%>
            <asp:DropDownList ID="ddlGenero" runat="server" CssClass="TextoCombo">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvGenero" runat="server" ControlToValidate="ddlGenero"
                ErrorMessage="Seleccione su g�nero."></asp:RequiredFieldValidator>
        </dd>
    </dl>
</div>
<uc1:UCDropDate2 ID="ucFechaNacimiento" runat="server"></uc1:UCDropDate2>
<div class="w100 ">
    <dl class="dt15 dd30 clearfix no_padding">
        <dt>
            <label for="r_txtTelefono">
                Tel&eacute;fono</label>
        </dt>
        <dd>
            <%--<input name="r_txtTelefono" type="text" class="text" id="r_txtTelefono"  maxlength="25" /> --%>
            <asp:TextBox ID="txtTelefono" runat="server" CssClass="text" MaxLength="15"></asp:TextBox>
        </dd>
    </dl>
</div>
<asp:UpdatePanel ID="Ubigeo" runat="server">
    <ContentTemplate>
        <div class="w100 bgf8f8f8 ">
            <dl class="dt15 dd30">
                <dt>
                    <label for="r_cboCountry">
                        Pa&iacute;s:<span id="strgPais" runat="server" class="required_red ">*</span></label>
                </dt>
                <dd>
                    <%--	<select id="r_cboCountry" name="r_cboCountry" class="">
										<option value="">Seleccionar</option>
									</select>--%>
                    <asp:DropDownList ID="ddlPais" runat="server" CssClass="" OnSelectedIndexChanged="ddlPais_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </dd>
            </dl>
        </div>
        <div class="w100 ">
            <dl class="dt15 dd30">
                <dt>
                    <label for="r_cboDepartment">
                        Departamento:<span id="strgDepartamento" runat="server" class="required_red ">*</span></label>
                </dt>
                <dd>
                    <%--	<select id="r_cboDepartment" name="r_cboDepartment" class="">
										<option value="">Seleccionar</option>
									</select>--%>
                    <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </dd>
            </dl>
        </div>
        <div class="w100 bgf8f8f8">
            <dl class="dt15 dd40">
                <dt>
                    <label for="r_cboCity">
                        Ciudad:<span id="strgCiudad" runat="Server" class="required_red ">*</span></label>
                </dt>
                <dd>
                    <%-- <select id="r_cboCity" name="r_cboCity"  class="">
                                        <option value="">Seleccionar</option>
                                    </select>--%>
                    <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="Seleccione una Ciudad"
                        ControlToValidate="ddlCiudad"></asp:RequiredFieldValidator>
                </dd>
            </dl>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="w100 ">
    <dl class="dt15 dd50">
        <dt>
            <label for="r_txtDireccion">
                Direcci&oacute;n</label>
        </dt>
        <dd>
            <%--<input name="r_txtDireccion" type="text" class="text" id="r_txtDireccion"  maxlength="25" />--%>
            <asp:TextBox ID="txtDireccion" runat="server" CssClass="text" MaxLength="100"></asp:TextBox>
        </dd>
    </dl>
</div>
