Imports System.Collections
Imports System.Collections.Generic
Imports _3Dev.FW.Entidades.Seguridad
Imports _3Dev.FW.Entidades.Seguridad.BEUserInfo
Imports _3Dev.FW.Util
Imports _3Dev.FW.Util.DataUtil


Partial Class UC_UCUsuarioDatosComun2
    Inherits System.Web.UI.UserControl

    Public Sub New()
        _flagPreMostrarCamposMandatorios = True

    End Sub


#Region "Propiedades"

    'Public _scriptManagerId As String
    'Public Property ScriptManagerId() As String
    '    Get
    '        Return _scriptManagerId
    '    End Get
    '    Set(ByVal value As String)
    '        _scriptManagerId = value
    '    End Set
    'End Property

    Protected _flagPreMostrarCamposMandatorios As Boolean
    Public Property FlagPreMostrarCamposMandatorios() As Boolean
        Get
            Return _flagPreMostrarCamposMandatorios
        End Get
        Set(ByVal value As Boolean)
            _flagPreMostrarCamposMandatorios = value
            '            ucFechaNacimiento.FlagPreMostrarCamposMandatorios = value
        End Set
    End Property

    Public ReadOnly Property Nombres() As TextBox
        Get
            Return txtNombres
        End Get
    End Property

    Public ReadOnly Property Apellidos() As TextBox
        Get
            Return txtApellidos
        End Get
    End Property

    Public ReadOnly Property TipoDocumento() As DropDownList
        Get
            Return ddlTipoDocumento
        End Get
    End Property

    Public ReadOnly Property NroDocumento() As TextBox
        Get
            Return txtNumeroDocumento
        End Get
    End Property

    Public ReadOnly Property Genero() As DropDownList
        Get
            Return ddlGenero
        End Get
    End Property

    Public ReadOnly Property FechaNacimiento() As UCDropDateBase
        Get
            Return Me.FindControl("ucFechaNacimiento")
        End Get
    End Property


    Public ReadOnly Property Telefono() As TextBox
        Get
            Return txtTelefono
        End Get
    End Property

    Public ReadOnly Property Pais() As DropDownList
        Get
            Return ddlPais
        End Get
    End Property

    Public ReadOnly Property Departamento() As DropDownList
        Get
            Return ddlDepartamento
        End Get
    End Property

    Public ReadOnly Property Ciudad() As DropDownList
        Get
            Return ddlCiudad
        End Get
    End Property

    Public ReadOnly Property Direccion() As TextBox
        Get
            Return txtDireccion
        End Get
    End Property
#End Region

#Region "Metodos"

    Private _cntrlParametro As SPE.Web.CAdministrarParametro = Nothing
    Private ReadOnly Property CntrlParametro() As SPE.Web.CAdministrarParametro
        Get
            If (_cntrlParametro Is Nothing) Then
                _cntrlParametro = New SPE.Web.CAdministrarParametro()
            End If
            Return _cntrlParametro
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            PreMostrarCamposMandatorios()
            txtNumeroDocumento.MaxLength = 11
            ' InicializarVista()
        End If
    End Sub


    Private Sub PreMostrarCamposMandatorios()
        strgNombres.Visible = _flagPreMostrarCamposMandatorios
        strgApellidos.Visible = _flagPreMostrarCamposMandatorios
        strgGenero.Visible = _flagPreMostrarCamposMandatorios
        strgNroDocumento.Visible = _flagPreMostrarCamposMandatorios
        strgTipoDocumento.Visible = _flagPreMostrarCamposMandatorios
        strgCiudad.Visible = _flagPreMostrarCamposMandatorios

        strgPais.Visible = _flagPreMostrarCamposMandatorios
        strgDepartamento.Visible = _flagPreMostrarCamposMandatorios
        strgCiudad.Visible = _flagPreMostrarCamposMandatorios
        ucFechaNacimiento.FlagPreMostrarCamposMandatorios = _flagPreMostrarCamposMandatorios
        ucFechaNacimiento.PreMostrarCamposMandatorios()

    End Sub

    Public Sub InicializarVista()
        CargarComboTipoDocumento()
        CargarComboGenero()
        CargarCombosFecha()
        SeleccionarPais(0)
    End Sub

    Private Sub CargarComboTipoDocumento()
        CargarComboTipoDocumento(False)
    End Sub
    Private Sub CargarComboTipoDocumento(ByVal recargar As Boolean)
        If (recargar) Then
            Me.ddlGenero.Items.Clear()
        End If
        If ((Me.ddlGenero.Items.Count = 0)) Then
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlTipoDocumento, CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoTipoDocumento), "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        End If

    End Sub

    Private Sub CargarComboGenero()
        CargarComboGenero(False)
    End Sub
    Private Sub CargarComboGenero(ByVal recargar As Boolean)
        If (recargar) Then
            Me.ddlGenero.Items.Clear()
        End If
        If ((Me.ddlGenero.Items.Count = 0)) Then
            'lis(CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero))
            Dim lgeneros As List(Of SPE.Entidades.BEParametro) = CntrlParametro.ConsultarParametroPorCodigoGrupo(SPE.EmsambladoComun.ParametrosSistema.GrupoGenero)
            _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlGenero, lgeneros, "Descripcion", "Id", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultCombo)
        End If

    End Sub

    Private Sub CargarCombosFecha()
        ucFechaNacimiento.RefrescarVista()
    End Sub

    Public Function CargarBEUsuario(ByVal obeUsuario As SPE.Entidades.BEUsuarioBase) As SPE.Entidades.BEUsuarioBase
        obeUsuario.Nombres = Me.txtNombres.Text
        obeUsuario.Apellidos = Me.txtApellidos.Text
        obeUsuario.IdTipoDocumento = ObjectToInt(Me.ddlTipoDocumento.SelectedValue)
        obeUsuario.NumeroDocumento = Me.txtNumeroDocumento.Text
        obeUsuario.IdCiudad = ObjectToInt(Me.ddlCiudad.SelectedValue)
        obeUsuario.Direccion = Me.txtDireccion.Text
        obeUsuario.Telefono = Me.txtTelefono.Text
        obeUsuario.IdGenero = ObjectToInt(ddlGenero.SelectedValue)
        obeUsuario.FechaNacimiento = ucFechaNacimiento.DameFechaSeleccionada()
        Return obeUsuario
    End Function

    Public Sub RefrescarVista(ByVal obeUsuario As SPE.Entidades.BEUsuarioBase)

        'Me.txtNombres.Text = obeUsuario.Nombres
        'Me.txtApellidos.Text = obeUsuario.Apellidos
        'Me.ddlTipoDocumento.SelectedValue = obeUsuario.IdTipoDocumento
        'Me.txtNumeroDocumento.Text = obeUsuario.NumeroDocumento.ToString
        ''Carga de Ubigeo
        'Me.SeleccionarPais(obeUsuario.IdPais)
        'Me.SeleccionarDepartamento(obeUsuario.IdDepartamento)
        'Me.SeleccionarCiudad(obeUsuario.IdCiudad)
        'Me.ddlCiudad.SelectedValue = obeUsuario.IdCiudad
        'Me.txtDireccion.Text = obeUsuario.Direccion
        'Me.txtTelefono.Text = obeUsuario.Telefono
        RefrescarVista(obeUsuario.Nombres, obeUsuario.Apellidos, obeUsuario.IdTipoDocumento, obeUsuario.NumeroDocumento, _
        obeUsuario.IdGenero, obeUsuario.FechaNacimiento, obeUsuario.Telefono, obeUsuario.IdPais, obeUsuario.IdDepartamento, obeUsuario.IdCiudad, _
        obeUsuario.Direccion)

    End Sub

    Public Sub RefrescarVista(ByVal nombre As String, ByVal apellidos As String, ByVal idTipoDocumento As Integer, _
    ByVal nroDocumento As String, ByVal Idgenero As Integer, ByVal fechaNacimiento As DateTime, _
    ByVal telefono As String, ByVal idPais As Integer, ByVal idDepartamento As Integer, _
    ByVal idCiudad As Integer, ByVal direccion As String)
        Me.txtNombres.Text = nombre
        Me.txtApellidos.Text = apellidos

        CargarComboTipoDocumento()
        Me.ddlTipoDocumento.SelectedValue = idTipoDocumento

        Me.txtNumeroDocumento.Text = nroDocumento

        CargarComboGenero()
        Me.ddlGenero.SelectedValue = Idgenero
        'Carga de Ubigeo
        Me.SeleccionarPais(idPais)
        Me.SeleccionarDepartamento(idDepartamento)
        Me.SeleccionarCiudad(idCiudad)
        Me.ddlCiudad.SelectedValue = idCiudad
        Me.txtDireccion.Text = direccion
        Me.txtTelefono.Text = telefono
        ucFechaNacimiento.RefrescarVista(fechaNacimiento)
    End Sub
    Protected Sub ddlTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoDocumento.SelectedIndexChanged
        If ddlTipoDocumento.SelectedValue = "11" Then
            txtNumeroDocumento.MaxLength = 8
            If txtNumeroDocumento.Text.Length > 8 Then
                txtNumeroDocumento.Text = txtNumeroDocumento.Text.Substring(0, 8)
            End If
        Else
            txtNumeroDocumento.MaxLength = 11
        End If
    End Sub
#End Region

#Region "Ubicacion"
    Public Sub SeleccionarPais(ByVal IdPais As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlPais, objCUbigeo.ConsultarPais, "Descripcion", "IdPais", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        'Me.ddlPais.DataBind()
        If IdPais > 0 Then
            Me.ddlPais.SelectedValue = IdPais
        End If
    End Sub

    Public Sub SeleccionarDepartamento(ByVal IdDepartamento As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlPais.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlPais.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlDepartamento, objCUbigeo.ConsultarDepartamentoPorIdPais(i), "Descripcion", "IdDepartamento", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        ' Me.ddlDepartamento.DataBind()

        If IdDepartamento > 0 Then
            Me.ddlDepartamento.SelectedValue = IdDepartamento
        Else
            Me.ddlDepartamento.SelectedIndex = 0
        End If
    End Sub

    Public Sub SeleccionarCiudad(ByVal IdCiudad As Integer)
        Dim objCUbigeo As New SPE.Web.CAdministrarUbigeo
        Dim i As Integer = 0
        If ddlDepartamento.SelectedValue = "" Then
            i = 0
        Else
            i = Me.ddlDepartamento.SelectedValue
        End If
        _3Dev.FW.Web.WebUtil.DropDownlistBinding(Me.ddlCiudad, objCUbigeo.ConsultarCiudadPorIdDepartamento(i), "Descripcion", "IdCiudad", SPE.EmsambladoComun.ParametrosSistema.ItemDefaultComboSmall)
        ' Me.ddlCiudad.DataBind()
        If IdCiudad > 0 Then
            Me.ddlCiudad.SelectedValue = IdCiudad
        Else
            Me.ddlCiudad.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlPais_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarDepartamento(0)
        SeleccionarCiudad(0)

        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlPais)
    End Sub

    Protected Sub ddlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarCiudad(0)
        'ScriptManager.GetCurrent(Me.Page).SetFocus(ddlDepartamento)
    End Sub

#End Region



End Class
