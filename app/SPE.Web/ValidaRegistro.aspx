<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="ValidaRegistro.aspx.vb" Inherits="ValidaRegistro" Title="PagoEfectivo - Validaci�n de Registro" %>

<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/estructura_pagoefectivo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/letras_pagoefectivo.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/gridandforms.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/accordian-src.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <%--	<script type="text/javascript" src="jscripts/validate.js"></script>--%>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/pagoefectivo.utils.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/pagoefectivo.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/Style/jquery.simplyscroll-1.0.4.css?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/jquery.simplyscroll-1.0.4.min.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") + "/jscripts/accordian.pack.js?" + System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <!--[if IE 6]>
	    <script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	    <script type="text/javascript">
	    (function($) {
		    $(document).ready(function(){
			    try {	
				    DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			    }catch(e){}});
		    })(jQuery);	
	    </script>
	    <![endif]-->
    <script type="text/javascript">

        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3

                });
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="body_pe">
        <div class="conten_ab">
            <div class="registrocont_pe">
                <h2>
                    Validar Usuario</h2>
                <div class="content_form">
                    <%--	<form id="frm_validate_user" class="dlgrid" > --%>
                    <div class="dlinline">
                        <div class="w100 ">
                            <dl class="dt25 dd50 clearfix no_padding">
                                <dt>
                                    <label for="vu_txtEmail">
                                        E-mail
                                    </label>
                                </dt>
                                <dd>
                                    <%--<input name="vu_txtEmail" type="text" class="text block" id="vu_txtEmail"  maxlength="25" /> --%>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="text block fleft"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="Formato de Email incorrecto" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="El email es mandatorio."
                                        Text="*" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                </dd>
                            </dl>
                        </div>
                        <div class="w100 ">
                            <dl class="dt25 dd50 clearfix no_padding">
                                <dt>
                                    <label for="vu_txtCodeUser">
                                        C&oacute;digo de Registro
                                    </label>
                                </dt>
                                <dd>
                                    <%--<input name="vu_txtCodeUser" type="text" class="text block" id="vu_txtCodeUser"  maxlength="25" /> --%>
                                    <asp:TextBox ID="txtGuid" runat="server" CssClass="text block fleft"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGuid" runat="server" ErrorMessage="El c�digo de registro es mandatorio."
                                        Text="*" ControlToValidate="txtGuid"></asp:RequiredFieldValidator>
                                </dd>
                            </dl>
                        </div>
                        <div class="mensaje_error" style="float: left; width: 500px;">
                            <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary" runat="server" />
                        </div>
                        <div class="botones_recupera">
                            <asp:Button ID="btnValidar" runat="server" Text="" CssClass="validar_pe png_bg" />
                            <%--<input name="" type="submit" class="validar_pe" />--%>
                            <%--<input name="" type="reset" value=""  class="ir_a_pe"/>--%>
                            <asp:Button ID="btnIrALogin" runat="server" Text="" CssClass="ir_a_pe png_bg" CausesValidation="false" />
                        </div>
                    </div>
                    <%--</form>--%>
                </div>
            </div>
        </div>
        <div class="conten_c">
            <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
            <%--<uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />--%>
        </div>
    </div>
</asp:Content>
