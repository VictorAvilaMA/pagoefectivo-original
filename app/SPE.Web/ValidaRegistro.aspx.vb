Imports _3Dev.FW.Web
Imports SPE.Web
Partial Class ValidaRegistro
    Inherits System.Web.UI.Page
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        If Not Page.IsPostBack Then
            If (Request.QueryString("Email") <> "") And (Request.QueryString("Valida") <> "") Then
                txtEmail.Text = Request.QueryString("Email")
                txtGuid.Text = Request.QueryString("Valida")
            End If
        End If
    End Sub

    Protected Sub btnValidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        Try
            Dim cCliente As New SPE.Web.CCliente()
            Dim obeOrdenCliente As SPE.Entidades.BECliente = cCliente.ValidaConfirmacionDeUsuario(txtEmail.Text, txtGuid.Text)

            If Not (obeOrdenCliente Is Nothing) Then

                If (obeOrdenCliente.IdCliente = -50) Then
                    lblMensaje.Text = "La cuenta de E-mail no se encuentra registrado."
                ElseIf (obeOrdenCliente.IdCliente = -150) Then
                    lblMensaje.Text = "El c�digo de registro de validaci�n no se encuentra registrado."
                ElseIf (obeOrdenCliente.IdCliente = -100) Then
                    btnValidar.Enabled = False
                    lblMensaje.Text = "Usted ya ha validado el registro de su cuenta. Ahora puede ingresar a PagoEfectivo."
                Else
                    btnValidar.Enabled = False
                    lblMensaje.ForeColor = Drawing.Color.Blue
                    lblMensaje.Text = "Usted se valid� correctamente. Ahora puede ingresar a PagoEfectivo."
                    If Not (obeOrdenCliente.OrdenPago Is Nothing) Then
                        If (obeOrdenCliente.OrdenPago.Url <> "" And obeOrdenCliente.OrdenPago.XmlString = "") Then

                            Response.Redirect("CLI/PRGeOrPaLo.aspx" + obeOrdenCliente.OrdenPago.Url + "&vr=1")
                        ElseIf (obeOrdenCliente.OrdenPago.XmlString <> "") Then
                            'Session("Entidad") = obeOrdenCliente.OrdenPago.XmlString
                            'Session("Url") = obeOrdenCliente.OrdenPago.UrlServicio
                            'Response.Redirect("CLI/PrGeOrPSLog.aspx?vr=1")
                            Session(SPE.Web.Util.UtilGenCIP.__XmlTramaPre) = obeOrdenCliente.OrdenPago.XmlString
                            Session(SPE.Web.Util.UtilGenCIP.__UrlServicioPre) = obeOrdenCliente.OrdenPago.UrlServicio
                            Response.Redirect("CLI/PrGeOrPSLog.aspx?vr=1")
                        End If

                    End If

                    End If

            Else
                lblMensaje.Text = "La validaci�n no fue exitosa. Por favor, intente de nuevo."
                btnValidar.Enabled = True
            End If

        Catch ex As Exception
            lblMensaje.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnIrALogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrALogin.Click
        Response.Redirect("default.aspx")
    End Sub
End Class
