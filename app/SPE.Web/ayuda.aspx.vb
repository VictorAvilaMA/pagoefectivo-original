Imports System.IO

Partial Class ayuda
    Inherits System.Web.UI.Page

    Protected Sub ayuda_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If HttpContext.Current.Cache("CentrosPagoAutorizados") Is Nothing Then
            Using reader As New System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/CentrosPagoAutorizados.html"))
                Dim htmlCentrosPagoAutorizados As String = reader.ReadToEnd()
                htmlCentrosPagoAutorizados = Replace(htmlCentrosPagoAutorizados, "[urlBase]", ConfigurationManager.AppSettings("DominioFile"))
                HttpContext.Current.Cache("CentrosPagoAutorizados") = htmlCentrosPagoAutorizados
                reader.Close()
            End Using
        End If
        divCentrosPagoAutorizados.InnerHtml = HttpContext.Current.Cache("CentrosPagoAutorizados")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPaginaAyuda")
        Dim lector As StreamReader
        Dim linea As String = ""
        Me.Master.FindControl("dvlnkregistroingreso").Visible = False
        Try
            lector = File.OpenText(ruta)
            ltrHTML.Text = lector.ReadToEnd
            lector.Close()
        Catch ex As Exception
        End Try
    End Sub


End Class
