<%@ Page Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false" CodeFile="contactenos.aspx.vb"
    Inherits="contactenos" Title="PagoEfectivo - Cont�ctenos" ValidateRequest="true" %>

<%--JJ--%>
<%@ Register Assembly="WebControlCaptcha" Namespace="WebControlCaptcha" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <div class="content">
        <div class="box-yll">
            <div class="close-yll">
                <a id="CerrarSoportT" style="cursor: pointer;">x</a></div>
            <p>
                Si necesita realizar una consulta de soporte t&eacute;cnico, acceda al <a href="http://centraldeayuda.pagoefectivo.pe/anonymous_requests/new"
                    target="_blank">formulario de Central de Ayuda</a>.</p>
        </div>
        <h2 class="title-seccion">
            <span>&nbsp;</span> Cont�ctenos Empresa/Comercio Afiliado</h2>
        <div class="content-contact">
            <p>
                Para m�s informaci�n acerca de PagoEfectivo, ventas o marketing; por favor utilice
                el formulario para enviarnos un mensaje y un representante de PagoEfectivo le responder�
                a la brevedad.</p>
            <p style="color: Gray;">
                Los campos marcados con (<b style="color: Red;">*</b>) son obligatorios</p>
            
            <div class="contact">
                <label for="text-razon-social">
                    Para:
                </label>
                contacto@pagoefectivo.pe
            </div>
            <div class="contact">
                <b>Datos de la empresa</b><br />
                <br />
                <label for="text-razon-social">
                    <span class="requerido">*</span> Raz&oacute;n Social:
                </label>
                <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="input-text" MaxLength="100" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRazonSocial"
                    ErrorMessage="Ingrese la raz�n social.">*</asp:RequiredFieldValidator>
                <br />
                <label for="text-ruc">
                    <span class="requerido">*</span> RUC:
                </label>
                <asp:TextBox ID="txtRuc" runat="server" CssClass="input-text" MaxLength="11" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRuc"
                    ErrorMessage="Ingrese el RUC.">*</asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftbetxtRUC" runat="server" TargetControlID="txtRuc"
                    FilterType="Numbers">
                </cc1:FilteredTextBoxExtender>
                <br />
                <label for="text-rubro">
                    <span class="requerido">*</span> Rubro:
                </label>
                <asp:TextBox ID="txtRubro" runat="server" CssClass="input-text" MaxLength="100" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRubro"
                    ErrorMessage="Ingrese el rubro.">*</asp:RequiredFieldValidator>
                <br />
                <label for="text-telefono">
                    <span class="requerido">*</span> Tel&eacute;fono:
                </label>
                <asp:TextBox ID="txtTelefono" runat="server" CssClass="input-text" MaxLength="100" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTelefono"
                    ErrorMessage="Ingrese el tel�fono de la empresa.">*</asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTelefono"
                    ValidChars="0123456789-#* " />
                <br />
                <label for="text-pagina-web" style="padding: 0px 3px 10px 0px; text-indent: 20px;">
                    P&aacute;gina Web:</label><asp:TextBox ID="txtPaginaWeb" runat="server" CssClass="input-text"
                        MaxLength="100" />
                <br />
                <b>Datos de contacto</b><br />
                <br />
                <label for="text-nombre">
                    <span class="requerido">*</span> Nombres y Apellidos:
                </label>
                <asp:TextBox ID="txtNomApe" runat="server" CssClass="input-text" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ControlToValidate="txtNomApe"
                    ErrorMessage="Ingrese nombres y apellidos.">*</asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtNomApe"
                    FilterType="UppercaseLetters,LowercaseLetters,Custom" ValidChars=" ������������">
                </cc1:FilteredTextBoxExtender>
                <br />
                <label for="text-telefono">
                    <span class="requerido">*</span> Tel&eacute;fono:
                </label>
                <asp:TextBox ID="txtTema" runat="server" CssClass="input-text" MaxLength="100" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtTema"
                    ErrorMessage="Ingrese el tel�fono.">*</asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTema"
                    ValidChars="0123456789-#* " />
                <br />
                <label for="text-correo">
                    <span class="requerido">*</span> Correo Electr&oacute;nico:
                </label>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="input-text" ReadOnly="false"
                    MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                    ErrorMessage="Ingrese correo electr&oacute;nico" Text="*"></asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftbetxtCorreo" runat="server" TargetControlID="txtEmail"
                    ValidChars="-_.@" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                    InvalidChars="���������� ">
                </cc1:FilteredTextBoxExtender>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                    ErrorMessage="Formato invalido del correo electr&oacute;nico" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator><br />
                <label for="text-comentario">
                    <span class="requerido">*</span> Comentario:
                </label>
                <asp:TextBox ID="txtContenido" runat="server" CssClass="textarea" TextMode="MultiLine" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContenido"
                    ErrorMessage="Ingrese el contenido del mensaje.">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Debe ingresar hasta un maximo de 2000 caracteres"
                    Display="Dynamic" ValidationExpression="^([\S\s]{0,2000})$" ControlToValidate="txtContenido">*</asp:RegularExpressionValidator>
                <div class="clear">
                </div>
                <div style="float: left; height: 15px; width: 100%;">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="navy"></asp:Label>
                    <div style="float: left; height: 1px; width: 190px;">
                    </div>
                    <asp:Label ID="lblmensajeContenido" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </div>
                <div style="float: left; width: 100%;">
                    <label for="text-comentario" style="float: left">
                        <span class="requerido">*</span> Ingrese c�digo de seguridad:</label>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="margin-bottom: 15px;" EnableViewState="false">
                        <ContentTemplate>
                            <cc2:CaptchaControl ID="CaptchaControl1" runat="server" CaptchaMaxTimeout="3600"
                                Text="Ingrese el c�digo mostrado:" CaptchaMinTimeout="0" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="mensaje_error" />
                <div class="form-actions">
                    <asp:Button ID="btniRegistrar" runat="server" OnClientClick="return ConfirmReCli();"
                        CssClass="btn btn-primary btn-middle" Text="Enviar" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <script type="text/javascript" src="https://sb.scorecardresearch.com/c2/6906602/cs.js#sitio_id=235360&path=/online/otros/"></script>
    <!--[if IE 6]>
	    <script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	    <script type="text/javascript">
	    (function($) {
		    $(document).ready(function(){
			    try {	
				    DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			    }catch(e){}});
		    })(jQuery);	
	    </script>
	    <![endif]-->
    <script type="text/javascript">

        $(document).ready(function () {
            $("#scroller").simplyScroll({
                autoMode: 'loop',
                speed: 3

            });
            $('#ulMenu li').removeClass('active');
            $('#liContactenos').addClass('active');
        });

        function LimpiarResultado() {
            $('#' + '<%= lblMensaje.ClientID %>').text("");
        }
    </script>
</asp:Content>
