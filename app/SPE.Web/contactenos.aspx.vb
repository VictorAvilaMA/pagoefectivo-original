Imports System.Data
Imports SPE.Web.Util
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Web.Log

Partial Class contactenos
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        Dim strRuta As String = HttpContext.Current.Server.MapPath("~/App_Data/SubtituloContactenos.html")

        Dim boolRuta As Boolean = System.IO.File.Exists(strRuta)

        If HttpContext.Current.Cache("SubTituloContactenos") Is Nothing Then

            If boolRuta Then

                Using reader As New System.IO.StreamReader(strRuta)
                    HttpContext.Current.Cache("SubTituloContactenos") = reader.ReadToEnd()
                    reader.Close()
                End Using

            End If

        End If

        'If boolRuta Then divSubtitulo.InnerHtml = HttpContext.Current.Cache("SubTituloContactenos")

    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        txtContenido.Attributes.Add("onkeypress", " ValidarCaracteres(this, 2000, $('#" + lblmensajeContenido.ClientID + "')); LimpiarResultado();")
        txtContenido.Attributes.Add("onkeyup", " ValidarCaracteres(this, 2000, $('#" + lblmensajeContenido.ClientID + "'));")
        MyBase.OnLoad(e)
        CType(Page.Master.FindControl("LnkIngresar"), HyperLink).NavigateUrl = "Login.aspx"
        Page.Master.FindControl("dvlinklogin").Visible = False
        'CType(Page.Master.FindControl("dvlinklogin"), HyperLink).Attributes.Add("cssClass", "abc")
        lblMensaje.Text = ""
        If Not Page.IsPostBack Then
            CType(Master, MPBase).MarcarMenu("licontactenos")
            Page.SetFocus(Me.txtNomApe)
        End If
    End Sub

    Protected Sub btniRegistrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btniRegistrar.Click

        Try
            If Not Page.IsValid Then Exit Sub

            If txtNomApe.Text.Trim <> "" AndAlso txtEmail.Text.Trim <> "" AndAlso txtContenido.Text.Trim <> "" Then

                Dim cemail As New SPE.Web.Util.UtilEmail()

                Dim intRpta As Integer = cemail.EnviarEmailContactenosEmpresa(New SPE.Entidades.BEMailContactenosEmpresa(txtNomApe.Text.Trim, txtEmail.Text.Trim, txtTema.Text.Trim, txtContenido.Text.Trim, txtRazonSocial.Text.Trim, txtRuc.Text.Trim, txtRubro.Text.Trim, txtTelefono.Text.Trim, txtPaginaWeb.Text.Trim))


                If intRpta = 1 Then
                    lblMensaje.Text = "El mensaje se envi� correctamente al administrador."
                    Limpiar()
                Else
                    lblMensaje.Text = "El mensaje no se envi� correctamente al administrador."
                End If

            End If
        Catch ex As Exception
            'Logger.LogException(ex)
            lblMensaje.Text = New SPE.Exceptions.GeneralConexionException(ex.Message).CustomMessage
            lblMensaje.CssClass = "MensajeValidacion"

        End Try

    End Sub

    Private Sub Limpiar()
        Me.txtNomApe.Text = ""
        Me.txtEmail.Text = ""
        Me.txtTema.Text = ""
        Me.txtContenido.Text = ""
        Me.txtRazonSocial.Text = String.Empty
        Me.txtRuc.Text = String.Empty
        Me.txtRubro.Text = String.Empty
        Me.txtTelefono.Text = String.Empty
        Me.txtPaginaWeb.Text = String.Empty
    End Sub


End Class
