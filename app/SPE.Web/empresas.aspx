﻿<%@ Page Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false" CodeFile="empresas.aspx.vb"
    Inherits="empresas" Title="PagoEfectivo - Empresas" %>

<%@ Register Src="UC/UCSeccionCentrosAutoriz.ascx" TagName="UCSeccionCentrosAutoriz"
    TagPrefix="uc4" %>
<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/gridandforms.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <!--[if IE 6]>
	    <script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	    <script type="text/javascript">
	    (function($) {
		    $(document).ready(function(){
			    try {	
				    DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			    }catch(e){}});
		    })(jQuery);	
	    </script>
	    <![endif]-->
    <script type="text/javascript">
        var urlBase = '<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>';
    </script>
    <script type="text/javascript">
        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3
                });
                $('#ulMenu li').removeClass('active');
                $('#liEmpresas').addClass('active');
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-side-left">
        <div id="slide-home" class="slide">
            <div class="slides_container">
                <div>
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/banner1.jpg" width="648" height="258" alt="Banner 1" title="Banner 1" />
                    <span class="coment1">Ahora es más sencillo</span> <span class="coment2">Pagar por Internet</span>
                </div>
            </div>
        </div>
        <h2 class="title-seccion">
            <span>&nbsp;</span> Beneficios</h2>
        <div class="benefits">
            <h3 class="title-benefits flexible">
                FLEXIBLE</h3>
            <p>
                Pago efectivo permite pactar con los negocios los periodos de expiración de la orden,
                de acuerdo a lo que más se adecue a sus necesidades. Asimismo, también puede pactar
                la periodicidad de los depósitos a las cuentas del negocio en los diferentes banco
                que operan en Perú.</p>
        </div>
        <div class="benefits">
            <h3 class="title-benefits accessible">
                ACCESIBLE</h3>
            <p>
                A través de PagoEfectivo, tendrás una llegada a una mayor proporción de la totalidad
                de clientes potenciales, debido a que no es necesario ningún producto bancario para
                que puedan realizar la transacción.</p>
        </div>
    </div>
    <div class="sidebar-rigth">
        <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
        <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
        <%--<uc2:UCSeccionComerciosAfil ID="UCSeccionComerciosAfil1" runat="server" />--%>
        <div id="divCentrosPagoAutorizados" runat="server">
        </div>
    </div>
</asp:Content>
