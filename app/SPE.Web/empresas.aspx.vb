Imports System.IO

Partial Class empresas
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If Not Page.IsPostBack Then
            CType(Master, MPBase).MarcarMenu("liempresas")
        End If

        If HttpContext.Current.Cache("CentrosPagoAutorizados") Is Nothing Then
            Using reader As New System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/CentrosPagoAutorizados.html"))
                Dim htmlCentrosPagoAutorizados As String = reader.ReadToEnd()
                htmlCentrosPagoAutorizados = Replace(htmlCentrosPagoAutorizados, "[urlBase]", ConfigurationManager.AppSettings("DominioFile"))
                HttpContext.Current.Cache("CentrosPagoAutorizados") = htmlCentrosPagoAutorizados
                reader.Close()
            End Using
        End If
        divCentrosPagoAutorizados.InnerHtml = HttpContext.Current.Cache("CentrosPagoAutorizados")

    End Sub

    Protected Sub empresas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPaginaEmpresas")
        'Dim lector As StreamReader
        'Dim linea As String = ""
        'Try
        '    lector = File.OpenText(ruta)
        '    ltrHTML.Text = lector.ReadToEnd
        '    lector.Close()
        'Catch ex As Exception
        'End Try
    End Sub
End Class
