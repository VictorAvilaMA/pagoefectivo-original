﻿Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema
Imports _3Dev.FW.Web.Log

Partial Class municipalidades_barranco_MBACarga
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ScriptManager.RegisterPostBackControl(btnProcesar)
            CargarCombos()
        End If
    End Sub

    Protected Sub btnProcesar_Click(sender As Object, e As System.EventArgs) Handles btnProcesar.Click
        If fulArchivoConciliacion.HasFile Then
            ProcesarArchivo()
        Else
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Por Favor Seleccione el Archivo de Texto');", True)
        End If
    End Sub

    Protected Sub BtnLimpiar_Click(sender As Object, e As System.EventArgs) Handles BtnLimpiar.Click
        Ocultar()
        ddlServicio.SelectedIndex = 0
        lblMensaje.Text = ""
        lblMensajeCabArc.Text = ""
        lblMensajeCabProc.Text = ""
        lblMensajeCabFail.Text = ""
        lblMensajeDetArc.Text = ""
        lblMensajeDetProc.Text = ""
        lblMensajeDetFail.Text = ""
        lblMensajeCadena.Text = ""
    End Sub

    Private Sub CargarCombos()
        Dim oBEServicioMunicipalidadBarrancoRequest As New BEServicioMunicipalidadBarrancoRequest
        oBEServicioMunicipalidadBarrancoRequest.GCParametro = SPE.EmsambladoComun.ParametrosSistema.GrupoClasificacionServicio
        oBEServicioMunicipalidadBarrancoRequest.DesParametro = "Municipalidad de Barranco"
        Dim objCServicioMunicipalidadBarranco As New SPE.Web.CServicio
        Dim IdParametro As Integer = objCServicioMunicipalidadBarranco.DeterminarIdParametroMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)

        Dim objCServicio As New SPE.Web.CServicio
        Dim oBEServicio As New BEServicio
        With oBEServicio
            .Clasificacion = IdParametro
        End With
        WebUtil.DropDownlistBinding(ddlServicio, objCServicio.ConsultarServicioPorClasificacion(oBEServicio), "Nombre", "IdServicio", "::: Seleccione :::")
    End Sub

    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub

    Private Function CargarDatos() As BEServicioMunicipalidadBarrancoRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEServicioMunicipalidadBarrancoRequest As New BEServicioMunicipalidadBarrancoRequest
            With oBEServicioMunicipalidadBarrancoRequest
                .IdServicio = Integer.Parse(ddlServicio.SelectedValue.ToString())
                .NombreArchivo = fulArchivoConciliacion.FileName
                .Bytes = fulArchivoConciliacion.FileBytes
            End With
            Return oBEServicioMunicipalidadBarrancoRequest
        Else
            Return Nothing
        End If
    End Function

    Private Function CargarDatosNumeroCargaDiaria() As BEServicioMunicipalidadBarrancoRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEServicioMunicipalidadBarranco As New BEServicioMunicipalidadBarrancoRequest
            With oBEServicioMunicipalidadBarranco
                .Bytes = fulArchivoConciliacion.FileBytes
            End With
            Return oBEServicioMunicipalidadBarranco
        Else
            Return Nothing
        End If
    End Function

    Private Function ValidarFormato() As Boolean
        If fulArchivoConciliacion.FileName.ToLower().Contains(".txt") Then
            Return True
        End If
        Return False
    End Function

    Private Sub ProcesarArchivo()
        Dim idError As Integer
        Dim oBEServicioMunicipalidadBarranco As New BEServicioMunicipalidadBarranco
        Try
            lblMensaje.Text = ""
            lblMensajeCabArc.Text = ""
            lblMensajeCabProc.Text = ""
            lblMensajeCabFail.Text = ""
            lblMensajeDetArc.Text = ""
            lblMensajeDetProc.Text = ""
            lblMensajeDetFail.Text = ""
            lblMensajeCadena.Text = ""

            MostrarValidacionFileUpload(False, "")
            Me.lblMensaje.Visible = False
            Me.lblMensaje.Text = ""
            If ValidarFormato() Then
                Dim requestCantidadCarga As BEServicioMunicipalidadBarrancoRequest = CargarDatosNumeroCargaDiaria()
                Dim request As BEServicioMunicipalidadBarrancoRequest = CargarDatos()
                If requestCantidadCarga IsNot Nothing Then
                    Dim objCServicioMunicipalidadBarranco As New SPE.Web.CServicio
                    Dim responseCantidadCarga As Integer = objCServicioMunicipalidadBarranco.ValidarCargaMunicipalidadBarranco(requestCantidadCarga)
                    'Se actualiza la cantidad de cargas por dia del respectivo servicio
                    request.NumeroCargaDiaria = responseCantidadCarga

                    'If responseCantidadCarga = 0 Then
                    '    request.NumeroCargaDiaria = 1
                    '    oBEServicioMunicipalidadBarranco = objCServicioMunicipalidadBarranco.ProcesarArchivoServicioMunicipalidadBarranco(request)
                    'Else
                    '    idError = objCServicioMunicipalidadBarranco.ActualizarDocumentosCIPMunicipalidadBarranco(request)
                    '    request.NumeroCargaDiaria = responseCantidadCarga + 1
                    '    oBEServicioMunicipalidadBarranco = objCServicioMunicipalidadBarranco.ProcesarArchivoServicioMunicipalidadBarranco(request)
                    'End If

                    Dim CadenaConexion As String = ConfigurationManager.ConnectionStrings("SPESQLPagoEfectivo").ConnectionString
                    Dim ValTimeOut As String = ConfigurationManager.AppSettings("ValTimeOut")
                    Dim UsuarioMunicipalidadBarranco As String = ConfigurationManager.AppSettings("UsuarioIdMunicipalidadBarranco")
                    Dim EmailUsuarioMunicipalidadBarranco As String = ConfigurationManager.AppSettings("EmailUsuarioMunicipalidadBarranco")
                    Dim Vigente As Integer = SPE.EmsambladoComun.ParametrosSistema.VigenciaArchivoServicioInstitucion.Vigente
                    Dim BatchSize As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("BatchSize"))

                    Dim oServicioMunicipalidadBarrancoBE As New SPER.BusinessEntity.ServicioMunicipalidadBarrancoBE
                    Dim oServicioMunicipalidadBarrancoRequestBE As New SPER.BusinessEntity.ServicioMunicipalidadBarrancoRequestBE

                    oServicioMunicipalidadBarrancoRequestBE.Bytes = request.Bytes
                    oServicioMunicipalidadBarrancoRequestBE.CodUsuario = request.CodUsuario
                    oServicioMunicipalidadBarrancoRequestBE.DesParametro = request.DesParametro
                    oServicioMunicipalidadBarrancoRequestBE.FechaCargaFinal = request.FechaCargaFinal
                    oServicioMunicipalidadBarrancoRequestBE.FechaCargaInicial = request.FechaCargaInicial
                    oServicioMunicipalidadBarrancoRequestBE.FechaEmision = request.FechaEmision
                    oServicioMunicipalidadBarrancoRequestBE.GCParametro = request.GCParametro
                    oServicioMunicipalidadBarrancoRequestBE.IdOrdenPago = request.IdOrdenPago
                    oServicioMunicipalidadBarrancoRequestBE.IdOrigenEliminacion = request.IdOrigenEliminacion
                    oServicioMunicipalidadBarrancoRequestBE.IdServicio = request.IdServicio
                    oServicioMunicipalidadBarrancoRequestBE.IdUsuarioActualizacion = request.IdUsuarioActualizacion
                    oServicioMunicipalidadBarrancoRequestBE.MerchantID = request.MerchantID
                    oServicioMunicipalidadBarrancoRequestBE.NombreArchivo = request.NombreArchivo
                    oServicioMunicipalidadBarrancoRequestBE.NroDocumento = request.NroDocumento
                    oServicioMunicipalidadBarrancoRequestBE.NumeroCargaDiaria = request.NumeroCargaDiaria
                    oServicioMunicipalidadBarrancoRequestBE.TipoArchivo = request.TipoArchivo

                    Dim oMunicipalidadBL As New SPER.BusinessLogic.MunicipalidadBL

                    If responseCantidadCarga = 0 Then
                        request.NumeroCargaDiaria = 1
                        oServicioMunicipalidadBarrancoBE = oMunicipalidadBL.ProcesarArchivoServicioMunicipalidadBarranco(oServicioMunicipalidadBarrancoRequestBE, EmailUsuarioMunicipalidadBarranco, Vigente, CadenaConexion, ValTimeOut, BatchSize)
                    Else
                        idError = oMunicipalidadBL.ActualizarDocumentosCIPMunicipalidadBarranco(oServicioMunicipalidadBarrancoRequestBE, UsuarioMunicipalidadBarranco, CadenaConexion, ValTimeOut)
                        request.NumeroCargaDiaria = responseCantidadCarga + 1
                        oServicioMunicipalidadBarrancoBE = oMunicipalidadBL.ProcesarArchivoServicioMunicipalidadBarranco(oServicioMunicipalidadBarrancoRequestBE, EmailUsuarioMunicipalidadBarranco, Vigente, CadenaConexion, ValTimeOut, BatchSize)
                    End If

                    oBEServicioMunicipalidadBarranco.CadenaErrores = oServicioMunicipalidadBarrancoBE.CadenaErrores
                    oBEServicioMunicipalidadBarranco.CantCabArchivo = oServicioMunicipalidadBarrancoBE.CantCabArchivo
                    oBEServicioMunicipalidadBarranco.CantCabProc = oServicioMunicipalidadBarrancoBE.CantCabProc
                    oBEServicioMunicipalidadBarranco.CantDetArchivo = oServicioMunicipalidadBarrancoBE.CantDetArchivo
                    oBEServicioMunicipalidadBarranco.CantDetProc = oServicioMunicipalidadBarrancoBE.CantDetProc
                    oBEServicioMunicipalidadBarranco.CantDetProc = oServicioMunicipalidadBarrancoBE.CantDetProc

                    If oBEServicioMunicipalidadBarranco.IdError = 0 Then
                        lblMensaje.Text = "El archivo se ha procesado correctamente."
                        lblMensajeCabArc.Text = oBEServicioMunicipalidadBarranco.CantCabArchivo.ToString + " registros de cabecera en el archivo"
                        lblMensajeCabProc.Text = oBEServicioMunicipalidadBarranco.CantCabProc.ToString + " registros de cabecera procesados"
                        lblMensajeCabFail.Text = (oBEServicioMunicipalidadBarranco.CantCabArchivo - oBEServicioMunicipalidadBarranco.CantCabProc).ToString + " registros de cabecera no procesados"
                        lblMensajeDetArc.Text = oBEServicioMunicipalidadBarranco.CantDetArchivo.ToString + " registros de detalle en el archivo"
                        lblMensajeDetProc.Text = oBEServicioMunicipalidadBarranco.CantDetProc.ToString + " registros de detalle procesados"
                        lblMensajeDetFail.Text = (oBEServicioMunicipalidadBarranco.CantDetArchivo - oBEServicioMunicipalidadBarranco.CantDetProc).ToString + " registros de detalle no procesados"
                        If oBEServicioMunicipalidadBarranco.CadenaErrores <> "" Then
                            lblMensajeCadena.Text = "Se tienen errores de formato en las lineas </br></br><ul>" + oBEServicioMunicipalidadBarranco.CadenaErrores.ToString + "</ul>"
                        End If
                    Else
                        lblMensaje.Text = "El formato del archivo no es correcto."
                    End If
                Else
                    lblMensaje.Text = "No se cargó ninguno de los registros enviados en el archivo."
                End If
            Else
                lblMensaje.Text = "Debe cargar un documento de tipo ""txt""..."
            End If
            Me.lblMensaje.Visible = True

        Catch ex As Exception
            'Logger.LogException(ex)
        End Try
    End Sub

    Private Function ValidaDatos() As Boolean
        If Not (ddlServicio.SelectedIndex > 0 And ddlServicio.SelectedItem.Text <> "" And ddlServicio.SelectedItem.Text <> "::: Seleccione :::") Then
            MostrarValidacionFileUpload(True, "Debe seleccionar un servicio.")
            ddlServicio.Focus()
            Return False
        End If
        If Not fulArchivoConciliacion.HasFile Then
            MostrarValidacionFileUpload(True, "No se ha seleccionado ningún archivo.")
            fulArchivoConciliacion.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub

End Class
