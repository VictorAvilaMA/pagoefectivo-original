﻿Imports SPE.Entidades
Imports _3Dev.FW.Web
Imports SPE.Web
Imports System.Collections.Generic
Imports SPE.EmsambladoComun.ParametrosSistema

Partial Class municipalidades_barranco_MBADescarga
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CargarCombos()
        End If
    End Sub

    Protected Sub btnRegistrar_Click(sender As Object, e As System.EventArgs) Handles btnRegistrar.Click
        ConsultaArchivosFecha()
    End Sub

    Protected Sub BtnLimpiar_Click(sender As Object, e As System.EventArgs) Handles BtnLimpiar.Click
        ddlServicio.SelectedIndex = 0
        lblMensaje.Text = ""
    End Sub

    Protected Sub gvResultado_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If (e.CommandName = "cmd") Then
            Dim ind As Integer = Convert.ToInt32(e.CommandArgument) - 1
            Dim row As GridViewRow = gvResultado.Rows(ind)
            Dim strMerchantId = row.Cells(1).Text
            Dim strFechaCarga = row.Cells(3).Text
            Dim dtFechaCarga = Convert.ToDateTime(strFechaCarga)
            Dim strRutaDescarga = System.Configuration.ConfigurationManager.AppSettings("ArchivosDetalleServicio")

            Dim request As BEServicioMunicipalidadBarrancoRequest = CargarDatosDetalle(strMerchantId, dtFechaCarga)
            'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion
            Dim objCServicioMunicipalidadBarranco As New SPE.Web.CServicio

            Dim strFileName As String = System.IO.Path.GetRandomFileName()

            'Dim strFriendlyName As String = strMerchantId + dtFechaCarga.Year.ToString + CompletarCeros(dtFechaCarga.Month.ToString) + CompletarCeros(dtFechaCarga.Day.ToString) + ".txt"

            Dim strFriendlyName As String = strMerchantId + "_" + CompletarCeros(dtFechaCarga.Day.ToString) + CompletarCeros(dtFechaCarga.Month.ToString) + CompletarCeros(dtFechaCarga.Year.ToString) + ".txt"

            Using sw As New System.IO.StreamWriter(Server.MapPath("TextFiles/" + strFileName + ".txt"))

                Dim CadenaConexion As String = ConfigurationManager.ConnectionStrings("SPESQLPagoEfectivo").ConnectionString
                Dim ValTimeOut As String = ConfigurationManager.AppSettings("ValTimeOut")
                Dim BatchSize As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("BatchSize"))

                Dim oServicioMunicipalidadBarrancoRequestBE As New SPER.BusinessEntity.ServicioMunicipalidadBarrancoRequestBE
                oServicioMunicipalidadBarrancoRequestBE.MerchantID = strMerchantId
                oServicioMunicipalidadBarrancoRequestBE.FechaCargaInicial = dtFechaCarga
                Dim oMunicipalidadBL As New SPER.BusinessLogic.MunicipalidadBL

                Dim ListaServicioMunicipalidadBarranco As List(Of SPER.BusinessEntity.ServicioMunicipalidadBarrancoBE) = oMunicipalidadBL.ConsultarDetalleArchivoDescargaMunicipalidadBarranco(oServicioMunicipalidadBarrancoRequestBE, CadenaConexion, ValTimeOut)

                For Each lineServ As SPER.BusinessEntity.ServicioMunicipalidadBarrancoBE In ListaServicioMunicipalidadBarranco
                    sw.WriteLine(FormarLineaEscritura(lineServ))
                Next
                sw.Close()
            End Using

            Dim fs As System.IO.FileStream = Nothing

            fs = System.IO.File.Open(Server.MapPath("TextFiles/" + strFileName + ".txt"), System.IO.FileMode.Open)
            Dim btFile(fs.Length) As Byte
            fs.Read(btFile, 0, fs.Length)
            fs.Close()
            With Response
                .AddHeader("Content-disposition", "attachment;filename=" & strFriendlyName)
                .ContentType = "application/octet-stream"
                .BinaryWrite(btFile)
                .End()
            End With
        End If
    End Sub

    Private Sub CargarCombos()
        Dim oBEServicioMunicipalidadBarrancoRequest As New BEServicioMunicipalidadBarrancoRequest
        oBEServicioMunicipalidadBarrancoRequest.GCParametro = SPE.EmsambladoComun.ParametrosSistema.GrupoClasificacionServicio
        oBEServicioMunicipalidadBarrancoRequest.DesParametro = "Municipalidad de Barranco"
        Dim objCServicioMunicipalidadBarranco As New SPE.Web.CServicio
        Dim IdParametro As Integer = objCServicioMunicipalidadBarranco.DeterminarIdParametroMunicipalidadBarranco(oBEServicioMunicipalidadBarrancoRequest)
        Dim objCServicio As New SPE.Web.CServicio
        Dim oBEServicio As New BEServicio
        With oBEServicio
            .Clasificacion = IdParametro
        End With
        WebUtil.DropDownlistBinding(ddlServicio, objCServicio.ConsultarServicioPorClasificacion(oBEServicio), "Nombre", "IdServicio", "::: Seleccione :::")
    End Sub

    Private Sub MostrarValidacionFileUpload(ByVal valor As Boolean, ByVal mensaje As String)
        pnlFileUpLoad.Visible = valor
        lblUploadFile.Text = mensaje
    End Sub

    Private Function ValidaDatos() As Boolean

        MostrarValidacionFileUpload(True, "")

        If Not (ddlServicio.SelectedIndex > 0 And ddlServicio.SelectedItem.Text <> "" And ddlServicio.SelectedItem.Text <> "::: Seleccione :::") Then
            MostrarValidacionFileUpload(True, "Debe seleccionar un servicio.")
            ddlServicio.Focus()
            Return False
        End If

        If Not (txtFechaConciliacion.Text <> "") Then
            MostrarValidacionFileUpload(True, "Debe Ingresar una Fecha de Carga Inicial.")
            txtFechaConciliacion.Focus()
            Return False
        End If

        If Not (txtFechaConciliacion2.Text <> "") Then
            MostrarValidacionFileUpload(True, "Debe Ingresar una Fecha de Carga Final.")
            txtFechaConciliacion2.Focus()
            Return False
        End If

        If Microsoft.VisualBasic.IsDate(txtFechaConciliacion.Text.Trim) = False Then
            MostrarValidacionFileUpload(True, "Debe Ingresar una Fecha de Carga Inicial Válida")
            txtFechaConciliacion.Focus()
            Return False
        End If

        If Microsoft.VisualBasic.IsDate(txtFechaConciliacion2.Text.Trim) = False Then
            MostrarValidacionFileUpload(True, "Debe Ingresar una Fecha de Carga Final Válida")
            txtFechaConciliacion2.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function CargarDatos() As BEServicioMunicipalidadBarrancoRequest
        Ocultar()
        If ValidaDatos() Then
            Dim oBEServicioMunicipalidadBarrancoRequest As New BEServicioMunicipalidadBarrancoRequest
            With oBEServicioMunicipalidadBarrancoRequest
                .IdServicio = Integer.Parse(ddlServicio.SelectedValue.ToString())
                .FechaCargaInicial = Convert.ToDateTime(txtFechaConciliacion.Text)
                .FechaCargaFinal = Convert.ToDateTime(txtFechaConciliacion2.Text)
            End With
            Return oBEServicioMunicipalidadBarrancoRequest
        Else
            Return Nothing
        End If
    End Function

    Private Function CargarDatosDetalle(ByVal strMerchantId As String, ByVal dtFechaCarga As DateTime) As BEServicioMunicipalidadBarrancoRequest

        Dim oBEServicioMunicipalidadBarrancoRequest As New BEServicioMunicipalidadBarrancoRequest
        With oBEServicioMunicipalidadBarrancoRequest
            .MerchantID = strMerchantId
            .FechaCargaInicial = dtFechaCarga
        End With
        Return oBEServicioMunicipalidadBarrancoRequest

    End Function

    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCeros(ByVal valor As String) As String
        If valor.Length = 1 Then
            Return "0" + valor
        Else
            Return valor
        End If
    End Function

    'COMPLETA CEROS EN VALORES DE FECHA
    Private Function CompletarCerosCIP(ByVal valor As String) As String
        If valor.Length = 7 Then
            Return "0000000" + valor
        Else
            Return valor
        End If
    End Function

    Public Function FormarLineaEscritura(ByVal linea As SPER.BusinessEntity.ServicioMunicipalidadBarrancoBE) As String

        'Dim strRetorno As String = ""
        'strRetorno = linea.MerchantID + "|" +
        '             linea.CodigoActual + "|" +
        '             linea.CodigoContribuyenteComp + "|" +
        '             linea.IdOrden.ToString() + "|" +
        '             linea.SerieNroDocumento + "|" +
        '             linea.DescripcionOrden + "|" +
        '             linea.MonedaDocPago.ToString() + "|" +
        '             linea.ImporteDocPago.ToString() + "|" +
        '             CompletarCeros(linea.FechaEmision.Day.ToString) + "-" + CompletarCeros(linea.FechaEmision.Month.ToString) + "-" + linea.FechaEmision.Year.ToString + "|" +
        '             CompletarCeros(linea.FechaVencimiento.Day.ToString) + "-" + CompletarCeros(linea.FechaVencimiento.Month.ToString) + "-" + linea.FechaVencimiento.Year.ToString + "|" +
        '             linea.ImporteMora.ToString() + "|" +
        '             linea.MonedaDocPago.ToString() + "|" +
        '             linea.Total.ToString + "|" +
        '             CompletarCeros(linea.FechaCancelacion.Day.ToString) + "-" + CompletarCeros(linea.FechaCancelacion.Month.ToString) + "-" + linea.FechaCancelacion.Year.ToString + "|" +
        '             CompletarCerosCIP(linea.NumeroOrdenPago)
        'Return strRetorno

        Dim strFechaVencimiento As String = ""
        If linea.strFechaVencimiento <> String.Empty Then
            strFechaVencimiento = CompletarCeros(Convert.ToDateTime(linea.strFechaVencimiento).Day.ToString) + "-" + CompletarCeros(Convert.ToDateTime(linea.strFechaVencimiento).Month.ToString) + "-" + Convert.ToDateTime(linea.strFechaVencimiento).Year.ToString + "|"
        End If

        Dim strImporteMora As String = "0.00"
        If linea.strImporteMora <> String.Empty Then
            strImporteMora = linea.strImporteMora + "|"
        End If

        Dim strRetorno As String = ""
        strRetorno = linea.MerchantID + "|" +
                     linea.CodigoActual + "|" +
                     linea.CodigoContribuyenteComp + "|" +
                     linea.IdOrden.ToString() + "|" +
                     linea.SerieNroDocumento + "|" +
                     linea.DescripcionOrden + "|" +
                     linea.MonedaDocPago.ToString() + "|" +
                     linea.ImporteDocPago.ToString() + "|" +
                     CompletarCeros(linea.FechaEmision.Day.ToString) + "-" + CompletarCeros(linea.FechaEmision.Month.ToString) + "-" + linea.FechaEmision.Year.ToString + "|" +
                     strFechaVencimiento + "|" +
                     strImporteMora + "|" +
                     linea.MonedaDocPago.ToString() + "|" +
                     linea.Total.ToString + "|" +
                     CompletarCeros(linea.FechaCancelacion.Day.ToString) + "-" + CompletarCeros(linea.FechaCancelacion.Month.ToString) + "-" + linea.FechaCancelacion.Year.ToString + "|" +
                     CompletarCerosCIP(linea.NumeroOrdenPago)
        Return strRetorno

    End Function

    Private Sub Ocultar()
        Me.gvResultado.DataSource = Nothing
        Me.pnlConciliadas.Visible = False
    End Sub

    Private Sub ConsultaArchivosFecha()
        MostrarValidacionFileUpload(False, "")
        Me.lblMensaje.Visible = False
        Me.lblMensaje.Text = ""
        Dim request As BEServicioMunicipalidadBarrancoRequest = CargarDatos()
        If request IsNot Nothing Then
            'Dim objCServicioInstitucion As New SPE.Web.CServicioInstitucion
            Dim objCServicioMunicipalidadBarranco As New SPE.Web.CServicio

            Dim response As List(Of BEServicioMunicipalidadBarranco) = objCServicioMunicipalidadBarranco.ConsultarArchivosDescargaMunicipalidadBarranco(request)

            If response.Count = 0 Then
                gvResultado.Visible = False
                Me.gvResultado.DataSource = Nothing
                lblMensaje.Visible = True
                lblMensaje.Text = "No se encontraron Registros."
            Else
                Me.pnlConciliadas.Visible = True
                Me.gvResultado.Visible = True
                Me.gvResultado.DataSource = response
                lblMensaje.Visible = True
                gvResultado.DataBind()
                ActualizarCampos()
                If response.Count() = 1 Then
                    Me.lblMensaje.Text = "Resultado: 1"
                ElseIf response.Count() > 1 Then
                    Me.lblMensaje.Text = "Resultados: " & response.Count.ToString()
                End If
            End If
        Else
            Ocultar()
        End If

    End Sub

    Public Sub ActualizarCampos()
        For Each gvr As GridViewRow In gvResultado.Rows
            gvr.Cells(3).Text = gvr.Cells(3).Text.Trim().Substring(0, 10)
            If (Integer.Parse(gvr.Cells(4).Text.Trim()) \ 7 = 0) Then
                gvr.Cells(4).Text = "1 Kb"
            Else
                gvr.Cells(4).Text = Convert.ToString(Integer.Parse(gvr.Cells(4).Text.Trim()) \ 7) + " Kb"
            End If
        Next

    End Sub

End Class
