<%@ Page Language="VB" MasterPageFile="~/MPBase.master" AutoEventWireup="false" CodeFile="personas.aspx.vb"
    Inherits="personas" Title="PagoEfectivo - Personas" %>

<%@ Register Src="UC/UCSeccionCentrosAutoriz.ascx" TagName="UCSeccionCentrosAutoriz"
    TagPrefix="uc4" %>
<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <!--[if IE 6]>
	<script type="text/javascript" src="jscripts/DD_belatedPNG-min.js"></script>
	<script type="text/javascript">
	(function($) {
		$(document).ready(function(){
			try {	
				DD_belatedPNG && DD_belatedPNG.fix('.png_bg');
			}catch(e){}});
		})(jQuery);	
	</script>
	<![endif]-->
    <script type="text/javascript">
        var urlBase = '<%= System.Configuration.ConfigurationManager.AppSettings.Get("pathBase") %>';
    </script>
    <script type="text/javascript">
        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3
                });
                $('#ulMenu li').removeClass('active');
                $('#liPersonas').addClass('active');
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderCertificaJS" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-side-left">
        <div id="slide-home" class="slide">
            <div class="slides_container">
                <div>
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/images/banner1.jpg" width="648" height="258" alt="Banner 1" title="Banner 1">
                    <span class="coment1">Ahora es m�s sencillo</span> <span class="coment2">Pagar por Internet</span>
                </div>
            </div>
        </div>
        <h2 class="title-seccion">
            <span>&nbsp;</span> Beneficios</h2>
        <div class="benefits">
            <h3 class="title-benefits easy">
                F�CIL</h3>
            <p>
                Sabemos que para compras en general, no hay medio m�s seguro que el efectivo. De
                esta forma Ud. y nosotros asegura de pagar �nicamente el objeto comprado. Solo tiene
                que acercarse a alguno de los centros autorizados de pago.</p>
        </div>
        <div class="benefits">
            <h3 class="title-benefits secure">
                SEGURO</h3>
            <p>
                PagoEfectivo cuenta con los siguientes elementos que le brindan a sus clientes mayor
                seguridad:</p>
            <ul>
                <li>El Pago puede ser realizado en Efectivo: sin necesidad de datos personales o claves
                    en cualquiera de nuestros locales; o a trav�s de la p�gina web www.viabcp.com, la
                    segura p�gina del Banco de Cr�dito, en el que entre otras opciones de seguridad
                    como usuario, y contrase�a, requiere del pin virtual personal para poder realizar
                    transacciones electr�nicas.</li>
                <li>Certificado SSL: Certificado el cual permite una transmisi�n de datos encriptada
                    y por tanto m�s segura entre el sistema de negocio virtual y PagoEfectivo, evitando
                    escuchas, falsificaci�n de identidad y mantener la integridad del mensaje.</li>
                <li>Ingreso de Usuario y Contrase�a: una vez que la transmisi�n fue realizada, para
                    poder pasar a generar el ticket de pago por la transacci�n, el interesado, deber�
                    ingresar a PagoEfectivo, colocando usuario y contrase�a.</li>
                <li>C�digo de Identificaci�n de Pagos (CIP): A trav�s del CIP, se asigna un n�mero �nico
                    e irrepetible con un c�digo de barras a cada transacci�n, los cuales almacenan caracter�sticas
                    como montos, contepto, as� como la empresa proveedora. Estos c�digos son corroborados
                    al momento de realizar el pago.</li>
            </ul>
        </div>
        <div class="benefits last">
            <h3 class="title-benefits accessible">
                ACCESIBLE</h3>
            <p>
                Sin necesidad de contar con productos bancarios: En PagoEfectivo, podr� aprovechar
                las ventajas del comercio electr�nico y pagar su compra con efectivo, en cualquiera
                de nuestros centros autorizados de pago a nivel nacional. Es decir, podr�n acceder
                a realizar comprar por Internet.</p>
            <ul>
                <li>Menores de edad, los cuales no pueden acreditar ingresos y por tanto, no cuentan
                    con tarjeta de cr�dito.</li>
                <li>Personas que no tienen un producto bancario.</li>
                <li>Personas que no desean colocar su tarjeta de cr�dito o d�bito y su clave, en cualquier
                    p�gina por seguridad.</li>
            </ul>
        </div>
    </div>
    <div class="sidebar-rigth">
        <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
        <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
        <div id="divCentrosPagoAutorizados" runat="server">
        </div>
    </div>
    <!-- fin body-->
</asp:Content>
