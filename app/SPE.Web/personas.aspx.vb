Imports System.IO

Partial Class personas
    Inherits System.Web.UI.Page
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If Not Page.IsPostBack Then
            CType(Master, MPBase).MarcarMenu("lipersonas")
        End If

    End Sub


    Protected Sub personas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Dim ruta As String = AppDomain.CurrentDomain.BaseDirectory() + ConfigurationManager.AppSettings("RutaPaginaEmpresas")
        'Dim lector As StreamReader
        'Dim linea As String = ""
        ''*----------------------------------------------------------
        ''Dim UCRuta As String = ""
        ''Dim UCRenderizado As String = ""

        ''Dim Ruta As String
        ''Dim RutaFormateada As String
        ''Dim NomPag As String
        ''Dim TamanioNomPag As Integer
        ''Dim RutaInvertida As String = ""
        ''Ruta = Request.Url.AbsolutePath
        ''For i As Integer = Ruta.Length - 1 To 0 Step -1
        ''    RutaInvertida = RutaInvertida & Ruta.Substring(i, 1)
        ''Next
        ''NomPag = RutaInvertida.Split("/")(0)
        ''TamanioNomPag = NomPag.Length
        ''RutaFormateada = Ruta.Substring(0, Ruta.Length - TamanioNomPag)
        ''UCRuta = RutaFormateada & "UC/UCSeccionCentrosAutoriz.ascx"
        ''-----------------------------------------------------------
        'Try
        '    lector = File.OpenText(ruta)
        '    ltrHTML.Text = lector.ReadToEnd
        '    ltrHTML.Text = lector.ReadToEnd
        '    lector.Close()
        '    '*----------------------------------------------------------
        '    'ltrHTML.Text = RenderizarUC(UCRuta)
        '    '-----------------------------------------------------------
        'Catch ex As Exception
        'End Try
        If HttpContext.Current.Cache("CentrosPagoAutorizados") Is Nothing Then
            Using reader As New System.IO.StreamReader(HttpContext.Current.Server.MapPath("~/App_Data/CentrosPagoAutorizados.html"))
                Dim htmlCentrosPagoAutorizados As String = reader.ReadToEnd()
                htmlCentrosPagoAutorizados = Replace(htmlCentrosPagoAutorizados, "[urlBase]", ConfigurationManager.AppSettings("DominioFile"))
                HttpContext.Current.Cache("CentrosPagoAutorizados") = htmlCentrosPagoAutorizados
                reader.Close()
            End Using
        End If
        divCentrosPagoAutorizados.InnerHtml = HttpContext.Current.Cache("CentrosPagoAutorizados")
    End Sub

    Protected Function RenderizarUC(ByVal ruta As String) As String
        Dim htmlResponse As New StringBuilder()
        Using sw As New StringWriter(htmlResponse)
            Using textWriter As New HtmlTextWriter(sw)
                Dim myControl As New UserControl()
                myControl.LoadControl("/SPE.Web/UC/UCSeccionCentrosAutoriz.ascx")
                myControl.RenderControl(textWriter)
            End Using
        End Using
        Return htmlResponse.ToString
    End Function
End Class
