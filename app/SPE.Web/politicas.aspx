﻿<%@ Page Language="VB" Async="true" MasterPageFile="~/MPBase.master" AutoEventWireup="false"
    CodeFile="politicas.aspx.vb" Inherits="politicas" Title="PagoEfectivo - Politicas" %>

<%@ Register Src="UC/UCSeccionRegistro.ascx" TagName="UCSeccionRegistro" TagPrefix="uc3" %>
<%@ Register Src="UC/UCSeccionInfo.ascx" TagName="UCSeccionInfo" TagPrefix="uc1" %>
<%@ Register Src="UC/UCSeccionComerciosAfil.ascx" TagName="UCSeccionComerciosAfil"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHeaderCSS" runat="Server">
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/estructura_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/letras_pagoefectivo.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
    <link href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/jquery.simplyscroll-1.0.4.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHeaderJS" runat="Server">
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/jscripts/jquery.simplyscroll-1.0.4.min.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/jscripts/accordian-src.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
    <script type="text/javascript">
        (function ($) {
            $(function () { //on DOM ready
                $("#scroller").simplyScroll({
                    autoMode: 'loop',
                    speed: 3

                });
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="DivLiteral" runat="server" class="content-side-left">
        <asp:Literal ID="ltrHTML" runat="server">
        </asp:Literal>
    </div>
    <div class="sidebar-rigth">
        <uc3:UCSeccionRegistro ID="UCSeccionRegistro1" runat="server" />
        <uc1:UCSeccionInfo ID="UCSeccionInfo1" runat="server" />
    </div>
</asp:Content>
