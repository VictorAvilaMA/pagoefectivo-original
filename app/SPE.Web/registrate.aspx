﻿<%@ Page Language="VB" Async="true" AutoEventWireup="false" CodeFile="registrate.aspx.vb"
    Inherits="registrate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- Google Analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28965570-1']);
        _gaq.push(['_setDomainName', '.pagoefectivo.pe']);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackPageLoadTime']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <title>PagoEfectivo</title>
    <meta http-equiv="Content-language" content="cs" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="description" content=" " />
    <meta name="keywords" content=" " />
    <link rel="stylesheet" href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/default.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/css" />
    <link rel="stylesheet" href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/screen.main.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" title="default" type="text/css" />
    <link rel="stylesheet" href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/screen.ie.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/css" />
    <link rel="stylesheet" href="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/Style/global.css?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>" type="text/css" />
    <script type="text/javascript" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/JScripts/jsGenerales.js?<%= System.Configuration.ConfigurationManager.AppSettings.Get("StaticVersion") %>"></script>
</head>
<body id="portadaUsuario">
    <script src="https://sb.scorecardresearch.com/c2/6906602/cs.js#sitio_id=235360&path=/online/otros/"></script>
    <div id="container">
        <form id="form1" runat="server">
        <div id="header" style="left: 0px; top: 0px;">
            <div class="left">
                <h1>
                    <a id="A1" href="~/Default.aspx" runat="server">PagoEfectivo</a></h1>
                <p class="slogan" style="left: 247px; top: 62px;">
                    Transacciones seguras
                    <br />
                    por Internet en el Perú</p>
            </div>
            <div class="menu_login_container right">
                <asp:Login ID="Login1" runat="server" CssClass="Login-fcb" LoginButtonText="Ingresa"
                    PasswordLabelText="Clave:" PasswordRequiredErrorMessage="La clave es requerida."
                    TitleText="Identificate" UserNameLabelText="E-mail:" UserNameRequiredErrorMessage="El email es requerido."
                    FailureText="Su intento de acceso no tuvo éxito. Por favor, inténtalo de nuevo."
                    RememberMeText="Recordarme la próxima vez" CreateUserText="Registrarse" CreateUserUrl="~/CLI/ADReCl.aspx"
                    DestinationPageUrl="~/Default.aspx" PasswordRecoveryText="Recuperar contraseña"
                    PasswordRecoveryUrl="~/RecuperarPassword.aspx">
                    <LayoutTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="RememberMe" runat="server" Text="Recordar mi sesión" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:HyperLink ID="CreateUserLink" runat="server" NavigateUrl="~/CLI/ADReCl.aspx">Registrarse</asp:HyperLink>
                                    |
                                    <asp:HyperLink ID="PasswordRecoveryLink" runat="server" NavigateUrl="~/RecuperarPassword.aspx">Recuperar contraseña</asp:HyperLink>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">E-mail:</asp:Label>
                                    <asp:TextBox ID="UserName" runat="server" CssClass="txtLog"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        CssClass="LoginValidator" ErrorMessage="El email es requerido." ToolTip="El email es requerido."
                                        ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contraseña:</asp:Label>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="txtLog"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        CssClass="LoginValidator" ErrorMessage="La clave es requerida." ToolTip="La clave es requerida."
                                        ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="FailureText" runat="server" Visible="false" EnableViewState="True"></asp:Literal><br />
                                    <asp:Literal ID="TextoValidacion" runat="server" EnableViewState="True"></asp:Literal>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="text-align: right;">
                                    <asp:Button ID="LoginButton" CommandName="Login" runat="server" ValidationGroup="Login1"
                                        CssClass="BotonLogin" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                </asp:Login>
            </div>
        </div>
        <div class="agente-wrapper-content">
            <div class="content">
                <div class="espaciado">
                    <a href="registrate.aspx">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/btn_registrate.png" style="width: 130px; height: 38px; padding: 0 70px;" /></a>
                    <a href="personas.aspx">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/btn_personas.png" style="width: 130px; height: 38px; padding: 0 70px;" /></a>
                    <a href="empresas.aspx">
                        <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/btn_empresas.png" style="width: 130px; height: 38px; padding: 0 70px;" /></a>
                </div>
                <div class="espaciado">
                    <h2>
                        Requisitos para Afiliarse</h2>
                    <ul class="content-menu">
                        <li>a) Contrato de Afiliación</li>
                        <li>b) Adjuntar:
                            <ul>
                                <li>DNI y poderes vigentes del Representante Legal</li>
                                <li>Poseer RUC activo con domicilio habido</li>
                                <li>Copia del registro del dominio (URL)</li>
                                <li>Tienda virtual</li>
                                <li>Carta del proveedor de Hosting
                                    <ul>
                                        <li>Copia del registro del certificado SSL, si tuviera </li>
                                    </ul>
                                </li>
                                <li>Diagrama de flujo de compra</li>
                                <li>Referencias comerciales</li>
                                <li>Información de Proveedores y Clientes </li>
                                <li>Cartas con las políticas de entrega (Plazos de entrega, horarios de entrega, cobertura
                                    de entrega, medios de entrega, proveedor de la entrega, confirmación de la entrega,
                                    tarifas o informar como se cobrará)</li>
                                <li>Cartas con las políticas de devolución (Política de cambios, política de cancelación,
                                    condiciones, tiempo y procedimiento)</li>
                                <li>Nombre del encargado de Sistemas en la Empresa, con quien coordinar la integración.</li>
                            </ul>
                        </li>
                        <li>Soporte Presencial: Si fuera necesario, personal de PagoEfectivo se acercará a su
                            negocio, a fin de dejar completamente operativa la aplicación.</li>
                    </ul>
                </div>
                <div style="text-align: center;">
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/img_empresas_afiliadas.png" />
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/img_centro_pagos.png" />
                    <div class="espaciado" />
                </div>
                <div>
                    <h4 class="espaciado">
                        Centro Autorizados de Pago</h4>
                    <div class="content-text-det left">
                        <p style="text-align: left">
                            <strong>Chorrillos:</strong> Av. Alejandro Iglesias 472</p>
                        <p style="text-align: left">
                            <strong>Jesús María:</strong> ° Edificio Las Orquídeas 11, Residencial San Felipe</p>
                        <p style="text-align: left">
                            ° Av. General Garzón 1420</p>
                        <p style="text-align: left">
                            <strong>Magdalena del Mar:</strong> Av Brasil 3245</p>
                        <p style="text-align: left">
                            <strong>Miraflores:</strong>° Av. José Pardo 680 ° Av. 28 de Julio 445-3 ° Calle
                            Schell 374</p>
                    </div>
                    <div class="content-text-det left">
                        <p style="text-align: left">
                            <strong>San Borja:</strong> Av. San Luis 2004</p>
                        <p style="text-align: left">
                            <strong>San Isidro:</strong> Av. Aramburú 365</p>
                        <p style="text-align: left">
                            <strong>San Miguel:</strong> Av. La Marina 2287</p>
                        <p style="text-align: left">
                            <strong>Santiago de Surco:</strong> ° Av. Primavera 120 A, of 213 ° Calle Preciados
                            149, of 201</p>
                        <p style="text-align: left">
                            <strong>Pueblo Libre:</strong> Av. Sucre 493</p>
                    </div>
                    <div class="clear espaciado" />
                </div>
                <div class="footer">
                    <ul>
                        <li><a href="politicas.aspx">| Términos y Condiciones</a></li>
                        <li><a href="ayuda.aspx">| Ayuda</a></li>
                        <li><a href="empresas.aspx">| Empresas</a></li>
                        <li><a href="personas.aspx">| Personas</a></li>
                        <li><a href="CLI/ADReCl.aspx">| Registrate</a></li>
                        <li><a href="default.aspx">Inicio</a></li>
                    </ul>
                </div>
            </div>
        </div>
        </form>
    </div>
</body>
</html>
