Imports _3Dev.FW.Web.Security
Partial Class registrate
    Inherits System.Web.UI.Page
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not Page.IsPostBack Then
            If (User.Identity.IsAuthenticated) Then
                System.Web.Security.FormsAuthentication.SignOut()
            End If
        End If
    End Sub

    Protected Sub Login1_LoggedIn(ByVal sender As Object, ByVal e As System.EventArgs) Handles Login1.LoggedIn
        Dim ucliente As New SPE.Web.Seguridad.SPEUsuarioClient()
        LoadUserInfo(ucliente.GetUserInfoByUserName(CType(Login1.FindControl("UserName"), TextBox).Text))
    End Sub

    Protected Sub Login1_LoginError(ByVal sender As Object, ByVal e As System.EventArgs) Handles Login1.LoginError
        Dim beUsuario As New SPE.Web.Seguridad.SPEUsuarioClient()
        Dim Estado As Integer
        Dim Pass As String = ""
        Dim userInfo As _3Dev.FW.Entidades.Seguridad.BEUserInfo = beUsuario.GetUserInfoByUserName(CType(Login1.FindControl("UserName"), TextBox).Text)
        Dim LoginErrorDetails As Literal = CType(Login1.FindControl("TextoValidacion"), Literal)
        If Not userInfo Is Nothing Then
            Estado = userInfo.IdEstado
        End If
        If userInfo Is Nothing Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. No existe un usuario con ese Email"
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Bloqueado Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta ha sido bloqueada. Debe contactarse con el Administrador"
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Pendiente Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta esta en estado pendiente debido a que no se ha completado el proceso de registro"
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Inactivo Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Su cuenta ha sido deshabilitada. Debe contactarse con el Administrador"
        ElseIf Estado = SPE.EmsambladoComun.ParametrosSistema.EstadoUsuario.Activo Then
            LoginErrorDetails.Text = "Su intento de acceso no tuvo �xito. Verifique que su email y clave sean correctos"
            ' Login1.FailureText = ""
        Else
            LoginErrorDetails.Text = Login1.FailureText
        End If
    End Sub
    Protected Overrides Sub OnLoadComplete(ByVal e As System.EventArgs)
        MyBase.OnLoadComplete(e)
        If Not Page.IsPostBack Then
            Page.SetFocus(Login1.FindControl("UserName"))
            Page.Form.DefaultButton = Login1.FindControl("LoginButton").UniqueID

        End If
    End Sub
End Class

