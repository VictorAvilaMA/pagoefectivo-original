<%@ Page Language="VB" Async="true" MasterPageFile="~/MasterPagePrincipal.master" AutoEventWireup="false" CodeFile="sinautrz.aspx.vb" Inherits="sinautrz" title="PagoEfectivo - Sin Autorización" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table  width="99%" align="center">
	<tr>
		<td class="ErrorMsg">
			<p><img alt="" src="<%= System.Configuration.ConfigurationManager.AppSettings.Get("DominioFile") %>/img/restrict.png"></p>
			<p>
                No tienes permisos para acceder a esta funcionalidad. Por favor, contactese con el administrador.</p>
			<p>
			  
			</p>
		</td>
	</tr>
</table>
</asp:Content>

