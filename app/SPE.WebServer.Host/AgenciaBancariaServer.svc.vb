﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation
Imports _3Dev.FW.Entidades

<AspNetCompatibilityRequirements(RequirementsMode:=ServiceModel.Activation.AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class AgenciaBancariaServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IAgenciaBancaria

    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function
    '
    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLAgenciaBancaria
    End Function
    Public Function ConsultarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As List(Of BusinessEntityBase) Implements IAgenciaBancaria.ConsultarAgenciaBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarAgenciaBancaria(obeAgenciaBancaria)
    End Function

    Public Function RegistrarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer Implements IAgenciaBancaria.RegistrarAgenciaBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.RegistrarAgenciaBancaria(obeAgenciaBancaria)
    End Function

    Public Function ActualizarAgenciaBancaria(ByVal obeAgenciaBancaria As BEAgenciaBancaria) As Integer Implements IAgenciaBancaria.ActualizarAgenciaBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ActualizarAgenciaBancaria(obeAgenciaBancaria)
    End Function

    Public Function ConsultarAgenciaBancariaxCodigo(ByVal codAgenciaBancaria As String) As BEAgenciaBancaria Implements IAgenciaBancaria.ConsultarAgenciaBancariaxCodigo
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarAgenciaBancariaxCodigo(codAgenciaBancaria)
    End Function

    Function ConsultarAgenciaBancariaxId(ByVal idAgenciaBancaria As Integer) As BEAgenciaBancaria Implements IAgenciaBancaria.ConsultarAgenciaBancariaxId
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarAgenciaBancariaxId(idAgenciaBancaria)
    End Function

    Public Function AnularOperacionPago(ByVal obeMovimiento As BEMovimiento) As Integer Implements IAgenciaBancaria.AnularOperacionPago
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.AnularOperacionPago(obeMovimiento)
    End Function


    'Public Function RegistrarCancelacionOrdenPagoAgenciaBancaria(ByVal obeMovimiento As BEMovimiento) As Integer Implements IAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria
    '    Dim objAgenciaBancaria As New BLAgenciaBancaria()
    '    Return objAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria(obeMovimiento)
    'End Function
    Public Function RegistrarCancelacionOrdenPagoAgenciaBancaria(ByVal obeMovimiento As BEMovimiento) As Integer Implements IAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria()
        Return objAgenciaBancaria.RegistrarCancelacionOrdenPagoAgenciaBancaria(obeMovimiento)
    End Function

    'Public Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As BEOrdenPago Implements IAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria
    '    Dim objAgenciaBancaria As New BLAgenciaBancaria
    '    Return objAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)
    'End Function
    Public Function ConsultarOrdenPagoAgenciaBancaria(ByVal obeOrdenPago As BEOrdenPago) As String() Implements IAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria 'SPE.Entidades.webServiceXResponse Implements IAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarOrdenPagoAgenciaBancaria(obeOrdenPago)
    End Function

    Public Function GenerarEstructuraXML(ByVal array() As String) As SPE.Entidades.webServiceXResponse Implements IAgenciaBancaria.GenerarEstructuraXML
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.GenerarEstructuraXML(array)
    End Function

    Public Function RegistrarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As BEConciliacion Implements IAgenciaBancaria.RegistrarConciliacionBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.RegistrarConciliacionBancaria(obeConciliacion)
    End Function
    Public Function ConsultarConciliacionBancaria(ByVal obeConciliacion As BEConciliacion) As List(Of BEOperacionBancaria) Implements IAgenciaBancaria.ConsultarConciliacionBancaria
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarConciliacionBancaria(obeConciliacion)
    End Function


#Region "Integracion BCP"

    Public Function ConsultarBCP(ByVal obeOrdenPago As BEOrdenPago) As BEBCPResponse Implements IAgenciaBancaria.ConsultarBCP
        Using oAB As New BLAgenciaBancaria
            Dim resultado As BEBCPResponse = oAB.ConsultarBCP(obeOrdenPago)
            Return resultado
        End Using
    End Function

    Public Function PagarBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse Implements IAgenciaBancaria.PagarBCP

        Using oAB As New BLAgenciaBancaria
            Return oAB.PagarBCP(obeMovimiento)
        End Using
    End Function

    Public Function AnularBCP(ByVal obeMovimiento As BEMovimiento) As BEBCPResponse Implements IAgenciaBancaria.AnularBCP

        Using oAB As New BLAgenciaBancaria
            Return oAB.AnularBCP(obeMovimiento)
        End Using

    End Function

#End Region

#Region "Integración BBVA"

    Public Function ConsultarBBVA(ByVal request As BEBBVAConsultarRequest) As BEBBVAResponse(Of BEBBVAConsultarResponse) Implements IAgenciaBancaria.ConsultarBBVA
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.ConsultarBBVA(request)
        End Using
    End Function

    Public Function PagarBBVA(ByVal request As BEBBVAPagarRequest) As BEBBVAResponse(Of BEBBVAPagarResponse) Implements IAgenciaBancaria.PagarBBVA
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.PagarBBVA(request)
        End Using
    End Function


    Public Function AnularBBVA(ByVal request As BEBBVAAnularRequest) As BEBBVAResponse(Of BEBBVAAnularResponse) Implements IAgenciaBancaria.AnularBBVA
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.AnularBBVA(request)
        End Using
    End Function

    Public Function ExtornarBBVA(ByVal request As BEBBVAExtornarRequest) As BEBBVAResponse(Of BEBBVAExtornarResponse) Implements IAgenciaBancaria.ExtornarBBVA
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.ExtornarBBVA(request)
        End Using
    End Function

#End Region

#Region "Integración Scotiabank"

    Public Function ConsultarSBK(ByVal request As BESBKConsultarRequest) As BEBKResponse(Of BESBKConsultarResponse) Implements IAgenciaBancaria.ConsultarSBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarSBK(request)
    End Function
    Public Function CancelarSBK(ByVal request As BESBKCancelarRequest) As BEBKResponse(Of BESBKCancelarResponse) Implements IAgenciaBancaria.CancelarSBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.CancelarSBK(request)
    End Function
    Public Function AnularSBK(ByVal request As BESBKAnularRequest) As BEBKResponse(Of BESBKAnularResponse) Implements IAgenciaBancaria.AnularSBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.AnularSBK(request)
    End Function
    Public Function ExtornarCancelacionSBK(ByVal request As BESBKExtornarCancelacionRequest) As BEBKResponse(Of BESBKExtornarCancelacionResponse) Implements IAgenciaBancaria.ExtornarCancelacionSBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ExtornarCancelacionSBK(request)
    End Function
    Public Function ExtornarAnulacionSBK(ByVal request As BESBKExtornarAnulacionRequest) As BEBKResponse(Of BESBKExtornarAnulacionResponse) Implements IAgenciaBancaria.ExtornarAnulacionSBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ExtornarAnulacionSBK(request)
    End Function

#End Region

#Region "Integración Interbank"

    Public Function ConsultarIBK(ByVal request As BEIBKConsultarRequest) As BEBKResponse(Of BEIBKConsultarResponse) Implements IAgenciaBancaria.ConsultarIBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarIBK(request)
    End Function
    Public Function CancelarIBK(ByVal request As BEIBKCancelarRequest) As BEBKResponse(Of BEIBKCancelarResponse) Implements IAgenciaBancaria.CancelarIBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.CancelarIBK(request)
    End Function
    Public Function AnularIBK(ByVal request As BEIBKAnularRequest) As BEBKResponse(Of BEIBKAnularResponse) Implements IAgenciaBancaria.AnularIBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.AnularIBK(request)
    End Function
    Public Function ExtornarCancelacionIBK(ByVal request As BEIBKExtornarCancelacionRequest) As BEBKResponse(Of BEIBKExtornarCancelacionResponse) Implements IAgenciaBancaria.ExtornarCancelacionIBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ExtornarCancelacionIBK(request)
    End Function
    Public Function ExtornarAnulacionIBK(ByVal request As BEIBKExtornarAnulacionRequest) As BEBKResponse(Of BEIBKExtornarAnulacionResponse) Implements IAgenciaBancaria.ExtornarAnulacionIBK
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ExtornarAnulacionIBK(request)
    End Function

#End Region

#Region "Integración Western Union"
    Public Function ConsultarWU(ByVal request As BEWUConsultarRequest) As BEWUConsultarResponse Implements IAgenciaBancaria.ConsultarWU
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarWU(request)
    End Function
    Public Function CancelarWU(ByVal request As BEWUCancelarRequest) As BEWUCancelarResponse Implements IAgenciaBancaria.CancelarWU
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.CancelarWU(request)
    End Function
    Public Function AnularWU(ByVal request As BEWUAnularRequest) As BEWUAnularResponse Implements IAgenciaBancaria.AnularWU
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.AnularWU(request)
    End Function
#End Region

#Region "Integración BanBif"
    Public Function ConsultarBanBif(request As Entidades.BEBanBifConsultarRequest) As Entidades.BEBanBifResponse(Of Entidades.BEBanBifConsultarResponse) Implements EmsambladoComun.IAgenciaBancaria.ConsultarBanBif
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ConsultarBanBif(request)
    End Function

    Public Function PagarBanBif(request As Entidades.BEBanBifPagarRequest) As Entidades.BEBanBifResponse(Of Entidades.BEBanBifPagarResponse) Implements EmsambladoComun.IAgenciaBancaria.PagarBanBif
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.PagarBanBif(request)
    End Function

    Public Function ExtornarBanBif(request As Entidades.BEBanBifExtornoRequest) As Entidades.BEBanBifResponse(Of Entidades.BEBanBifExtornoResponse) Implements EmsambladoComun.IAgenciaBancaria.ExtornarBanBif
        Dim objAgenciaBancaria As New BLAgenciaBancaria
        Return objAgenciaBancaria.ExtornarBanBif(request)
    End Function

#End Region

#Region "Integración Kasnet"

    Public Function ConsultarKasnet(ByVal request As BEKasnetConsultarRequest) As BEKasnetResponse(Of BEKasnetConsultarResponse) Implements IAgenciaBancaria.ConsultarKasnet
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.ConsultarKasnet(request)
        End Using
    End Function

    Public Function PagarKasnet(ByVal request As BEKasnetPagarRequest) As BEKasnetResponse(Of BEKasnetPagarResponse) Implements IAgenciaBancaria.PagarKasnet
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.PagarKasnet(request)
        End Using
    End Function

    Public Function ExtornarKasnet(ByVal request As BEKasnetExtornarRequest) As BEKasnetResponse(Of BEKasnetExtornarResponse) Implements IAgenciaBancaria.ExtornarKasnet
        Using oblAgenciaBancaria As New BLAgenciaBancaria()
            Return oblAgenciaBancaria.ExtornarKasnet(request)
        End Using
    End Function

#End Region

End Class
