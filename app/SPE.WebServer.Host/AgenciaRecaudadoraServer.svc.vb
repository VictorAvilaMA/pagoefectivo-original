﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio

Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class AgenciaRecaudadoraServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IAgenciaRecaudadora

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLAgenciaRecaudadora
    End Function

    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function

    Public Function RegistrarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer Implements IAgenciaRecaudadora.RegistrarAgenciaRecaudadora
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.RegistrarAgenciaRecaudadora(obeAgenciaRecaudadora)
        '
    End Function

    Public Function ActualizarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As Integer Implements IAgenciaRecaudadora.ActualizarAgenciaRecaudadora
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ActualizarAgenciaRecaudadora(obeAgenciaRecaudadora)
        '
    End Function

    Public Function ConsultarAgenciaRecaudadora(ByVal obeAgenciaRecaudadora As BEAgenciaRecaudadora) As List(Of BEAgenciaRecaudadora) Implements IAgenciaRecaudadora.ConsultarAgenciaRecaudadora
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarAgenciaRecaudadora(obeAgenciaRecaudadora)
        '
    End Function

    Public Function ConsultarAgenciaRecaudadoraPorIdAgencia(ByVal IdAgencia As Integer) As BEAgenciaRecaudadora Implements IAgenciaRecaudadora.ConsultarAgenciaRecaudadoraPorIdAgencia
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarAgenciaRecaudadoraPorIdAgencia(IdAgencia)
        '
    End Function

    Public Function ConsultarAgenciaRecaudadoraPorIdUsuario(ByVal IdUsuario As Integer) As BEAgenciaRecaudadora Implements IAgenciaRecaudadora.ConsultarAgenciaRecaudadoraPorIdUsuario
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarAgenciaRecaudadoraPorIdUsuario(IdUsuario)
        '
    End Function

    Public Function ConsultarAgente(ByVal obeAgente As BEAgente) As List(Of BEAgente) Implements IAgenciaRecaudadora.ConsultarAgente
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarAgente(obeAgente)
        '
    End Function

    Public Function ConsultarAgentePorIdAgenteOrIdUsuario(ByVal opcion As Integer, ByVal IdAgenteUsuario As Integer) As BEAgente Implements IAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarAgentePorIdAgenteOrIdUsuario(opcion, IdAgenteUsuario)
        '
    End Function

    Public Function RegistrarAgente(ByVal obeAgente As BEAgente) As Integer Implements IAgenciaRecaudadora.RegistrarAgente
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.RegistrarAgente(obeAgente)
        '
    End Function

    Public Function ActualizarAgente(ByVal obeAgente As BEAgente) As Integer Implements IAgenciaRecaudadora.ActualizarAgente
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ActualizarAgente(obeAgente)
        '
    End Function
    '
    Public Function ConsultaCajaActiva(ByVal IdAgente As Integer) As Entidades.BEAgenteCaja Implements EmsambladoComun.IAgenciaRecaudadora.ConsultaCajaActiva
        Dim objCajaActiva As New BLAgenciaRecaudadora
        'Console.WriteLine(System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.Action)
        'Console.WriteLine(System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.FaultTo)
        'Console.WriteLine(System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.Count)
        'Console.WriteLine(System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.From)
        'Console.WriteLine(System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.RelatesTo)
        'Console.WriteLine(System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.To)
        'For Each a As System.ServiceModel.Channels.MessageHeaderInfo In System.ServiceModel.OperationContext.Current.IncomingMessageHeaders.UnderstoodHeaders
        '    Console.WriteLine("{0} {1} {2} {3} {4} {5}", a.Actor, a.IsReferenceParameter, a.MustUnderstand, a.Name, a.Namespace, a.Relay)

        'Next




        Return objCajaActiva.ConsultaCajaActiva(IdAgente)
    End Function

    Public Function ConsultaMontosPorAgenteCaja(ByVal IdAgenteCaja As Integer, ByRef ListCierre As Collection(Of BEMovimiento)) As Collection(Of BEMovimiento) Implements EmsambladoComun.IAgenciaRecaudadora.ConsultaMontosPorAgenteCaja
        Dim objAgenteCaja As New BLAgenciaRecaudadora
        Return objAgenteCaja.ConsultaMontosPorAgenteCaja(IdAgenteCaja, ListCierre)

    End Function

    Public Function ConsultarCaja(ByVal obeCaja As BECaja) As List(Of BECaja) Implements IAgenciaRecaudadora.ConsultarCaja
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarCaja(obeCaja)
        '
    End Function

    Public Function ConsultarCajaAvanzada(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja) Implements IAgenciaRecaudadora.ConsultarCajaAvanzada
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarCajaAvanzada(obeAgenteCaja)
        '
    End Function

    Public Function RegistrarAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer Implements IAgenciaRecaudadora.RegistrarAgenteCaja
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.RegistrarAgenteCaja(obeAgenteCaja)
        '
    End Function

    Public Function RegistrarMovimiento(ByVal obeMovimiento As BEMovimiento) As Integer Implements IAgenciaRecaudadora.RegistrarMovimiento
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.RegistrarMovimiento(obeMovimiento)
        '
    End Function

    Public Function ConsultarAgenteCajaPorIdUsuarioIdEstado(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja) Implements IAgenciaRecaudadora.ConsultarAgenteCajaPorIdUsuarioIdEstado
        '
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarAgenteCajaPorIdUsuarioIdEstado(obeAgenteCaja)
        '
    End Function
    '
    Public Function ConsultarFechaValutaPorAgente(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta Implements IAgenciaRecaudadora.ConsultarFechaValutaPorAgente
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.ConsultarFechaValutaPorAgente(obeFechaValuta)
    End Function

    Public Function LiquidarAgenteCaja(ByVal obeAgenteCaja As Entidades.BEAgenteCaja) As Integer Implements EmsambladoComun.IAgenciaRecaudadora.LiquidarAgenteCaja

        Dim objLiquidar As New BLAgenciaRecaudadora
        Return objLiquidar.LiquidarAgenteCaja(obeAgenteCaja)

    End Function

    Public Function ConsultarCajasCerradas(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEAgenteCaja) Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarCajasCerradas

        Dim objCajasCerradas As New BLAgenciaRecaudadora
        Return objCajasCerradas.ConsultarCajasCerradas(obeAgenteCaja)

    End Function

    Public Function LiquidarCajaEnBloque(ByVal obeOrdenPago As BEOrdenPago, ByVal ListAgenteCaja As System.Collections.Generic.List(Of Entidades.BEAgenteCaja)) As Integer Implements EmsambladoComun.IAgenciaRecaudadora.LiquidarCajaEnBloque

        Dim objLiquidar As New BLAgenciaRecaudadora
        Return objLiquidar.LiquidarCajaEnBloque(obeOrdenPago, ListAgenteCaja)

    End Function

    Public Function ConsultarAgentesPorAgencia(ByVal IdSupervisor As Integer) As System.Collections.Generic.List(Of Entidades.BEAgenteCaja) Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarAgentesPorAgencia

        Dim objAgentesAgencia As New BLAgenciaRecaudadora
        Return objAgentesAgencia.ConsultarAgentesPorAgencia(IdSupervisor)

    End Function

    Public Function ConsultarCajaActivaPorAgente(ByVal IdAgente As Integer) As Entidades.BEAgenteCaja Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarCajaActivaPorAgente

        Dim objCajaActiva As New BLAgenciaRecaudadora
        Return objCajaActiva.ConsultarCajaActivaPorAgente(IdAgente)

    End Function

    Public Function RegistrarSobranteFaltante(ByVal obeMovimiento As Entidades.BEMovimiento) As Integer Implements EmsambladoComun.IAgenciaRecaudadora.RegistrarSobranteFaltante

        Dim objSobranteFaltante As New BLAgenciaRecaudadora
        Return objSobranteFaltante.RegistrarSobranteFaltante(obeMovimiento)

    End Function

    Public Function ConsultarAgentesConCajaPendientesdeCierre(ByVal IdTipoFechaValuta As Integer, ByVal IdAgencia As Integer) As List(Of BEAgente) Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarAgentesConCajaPendientesdeCierre
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.ConsultarAgentesConCajaPendientesdeCierre(IdTipoFechaValuta, IdAgencia)

    End Function

    Public Function ConsultarCajaActivaPorIdAgente(ByVal IdAgente As Integer) As BEAgenteCaja Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarCajaActivaPorIdAgente
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.ConsultarCajaActivaPorIdAgente(IdAgente)
    End Function

    Public Function RegistrarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer Implements EmsambladoComun.IAgenciaRecaudadora.RegistrarFechaValuta
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.RegistrarFechaValuta(obeFechaValuta)
    End Function

    Public Function ActualizarEstadoFechaValuta(ByVal obeAgenteCaja As BEAgenteCaja) As Integer Implements EmsambladoComun.IAgenciaRecaudadora.ActualizarEstadoFechaValuta
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.ActualizarEstadoFechaValuta(obeAgenteCaja)
    End Function

    Public Function ActualizarEstadoFechaValutaPorIdAgente(ByVal obeFechaValuta As BEFechaValuta) As Integer Implements IAgenciaRecaudadora.ActualizarEstadoFechaValutaPorIdAgente
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.ActualizarEstadoFechaValutaPorIdAgente(obeFechaValuta)
    End Function

    Public Function ConsultarFechaValutaAgenteCaja(ByVal obeFechaValuta As BEFechaValuta) As BEFechaValuta Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarFechaValutaAgenteCaja
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.ConsultarFechaValutaAgenteCaja(obeFechaValuta)
    End Function

    Public Function AnularAgenteCaja(ByVal obeAgenteCaja As BEAgenteCaja) As Integer Implements EmsambladoComun.IAgenciaRecaudadora.AnularAgenteCaja
        Dim oblAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return oblAgenciaRecaudadora.AnularAgenteCaja(obeAgenteCaja)
    End Function

    Public Function ConsultarDetalleDeCaja(ByVal IdAgenteCaja As Integer) As System.Collections.Generic.List(Of Entidades.BEMovimiento) Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarDetalleDeCaja

        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarDetalleDeCaja(IdAgenteCaja)

    End Function

    Public Function ConsultarCajasLiquidadas(ByVal IdUsuario As Integer, ByVal NroOrdenPago As String, ByVal FechaInicio As DateTime, ByVal FechaFin As DateTime) As System.Collections.Generic.List(Of Entidades.BEAgenteCaja) Implements EmsambladoComun.IAgenciaRecaudadora.ConsultarCajasLiquidadas

        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarCajasLiquidadas(IdUsuario, NroOrdenPago, FechaInicio, FechaFin)

    End Function

    Public Function ConsultarDetalleMovimientosCaja(ByVal obeAgenteCaja As BEAgenteCaja) As List(Of BEMovimientoConsulta) Implements IAgenciaRecaudadora.ConsultarDetalleMovimientosCaja
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarDetalleMovimientosCaja(obeAgenteCaja)
    End Function

    Public Function ValidarEstadoCaja(ByVal Form As String, ByVal IdUsuario As Integer) As String()() Implements IAgenciaRecaudadora.ValidarEstadoCaja

        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ValidarEstadoCaja(Form, IdUsuario)

    End Function

    Function ConsultarMovimiento(ByVal obeMovimiento As BEMovimiento) As BEMovimiento Implements IAgenciaRecaudadora.ConsultarMovimiento
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ConsultarMovimiento(obeMovimiento)

    End Function

    Public Function ProbarConexion() As String Implements IAgenciaRecaudadora.ProbarConexion
        Dim objAgenciaRecaudadora As New BLAgenciaRecaudadora
        Return objAgenciaRecaudadora.ProbarConexion()
    End Function

End Class
