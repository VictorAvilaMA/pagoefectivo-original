﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio

Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class BancoServer
    Inherits MarshalByRefObject
    Implements IBanco
    '

    Public Overrides Function InitializeLifetimeService() As Object
        '
        Return Nothing
        '
    End Function


    '<add Peru.Com FaseIII>
    Function RegistrarBanco(ByVal oBEBanco As BEBanco) As Integer Implements IBanco.RegistrarBanco
        '
        Dim objBanco As New BLBanco
        Return objBanco.RegistrarBanco(oBEBanco)
        '
    End Function

    Function ConsultarBanco(ByVal oBEBanco As BEBanco) As List(Of BEBanco) Implements IBanco.ConsultarBanco
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarBanco(oBEBanco)

    End Function

    Function ConsultarCuentaBanco(ByVal oBECuentaBanco As BECuentaBanco) As List(Of BECuentaBanco) Implements IBanco.ConsultarCuentaBanco
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarCuentaBanco(oBECuentaBanco)
    End Function

    '<upd proc.Tesoreria>
    Public Function RegistrarDeposito(ByVal beDeposito As BEDeposito, ByVal detalleDeposito As List(Of BEOrdenPago)) As Integer Implements IBanco.RegistrarDeposito
        Dim objBanco As New BLBanco
        Return objBanco.RegistrarDeposito(beDeposito, detalleDeposito)
    End Function
    Public Function ConsultarDepositoRegistroFactura(ByVal beDeposito As BEDeposito) As List(Of BEDeposito) Implements IBanco.ConsultarDepositoRegistroFactura
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarDepositoRegistroFactura(beDeposito)
    End Function
    Public Function RegistrarFactura(ByVal beFactura As BEFactura, ByVal detalleDeposito As List(Of BEDeposito)) As Integer Implements IBanco.RegistrarFactura
        Dim objBanco As New BLBanco
        Return objBanco.RegistrarFactura(beFactura, detalleDeposito)
    End Function
    Public Function ConsultarDeposito(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements IBanco.ConsultarDeposito
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarDeposito(beDeposito)
    End Function
    Public Function ConsultarDepositoByIdFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements IBanco.ConsultarDepositoByIdFactura
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarDepositoByIdFactura(beDeposito)
    End Function
    Public Function ConsultarDepositoDetalleByIdDeposito(ByVal request As BEDeposito) As List(Of BEOrdenPago) Implements IBanco.ConsultarDepositoDetalleByIdDeposito
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarDepositoDetalleByIdDeposito(request)
    End Function
    Public Function ConsultarFactura(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements IBanco.ConsultarFactura
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarFactura(beDeposito)
    End Function
    Public Function ConsultarDepositoReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements IBanco.ConsultarDepositoReporte
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarDepositoReporte(beDeposito)
    End Function
    Public Function ConsultarFacturaReporte(ByVal beDeposito As BEDeposito) As List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements IBanco.ConsultarFacturaReporte
        Dim objBanco As New BLBanco
        Return objBanco.ConsultarFacturaReporte(beDeposito)
    End Function

    Public Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As System.Collections.Generic.List(Of Entidades.BEConciliacionArchivo) Implements IBanco.ConsultarConciliacionArchivo
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarConciliacionArchivo(request)
        End Using
    End Function
    Public Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago) Implements IBanco.ConsultarOrdenesConcArch
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarOrdenesConcArch(request)
        End Using
    End Function
End Class
