﻿Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio

Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class CajaServer
    Inherits MarshalByRefObject
    Implements ICaja

    Public Overrides Function InitializeLifetimeService() As Object

        Return Nothing

    End Function

    Public Function ActualizarCaja(ByVal obeCaja As Entidades.BECaja) As Object Implements EmsambladoComun.ICaja.ActualizarCaja

        Dim objCaja As New BLCaja
        Return objCaja.ActualizarCaja(obeCaja)

    End Function

    Public Function ConsultarCaja(ByVal IdAgenciaRecaudadora As Integer) As System.Collections.Generic.List(Of Entidades.BECaja) Implements EmsambladoComun.ICaja.ConsultarCaja

        Dim objCaja As New BLCaja
        Return objCaja.ConsultarCaja(IdAgenciaRecaudadora)

    End Function

    Public Function RegistrarCaja(ByVal obeCaja As Entidades.BECaja) As Object Implements EmsambladoComun.ICaja.RegistrarCaja

        Dim objCaja As New BLCaja
        Return objCaja.RegistrarCaja(obeCaja)

    End Function

    Public Function OtorgarFechaValuta(ByVal obeFechaValuta As BEFechaValuta) As Integer Implements EmsambladoComun.ICaja.OtorgarFechaValuta
        Dim objCaja As New BLCaja
        Return objCaja.OtorgarFechaValuta(obeFechaValuta)
    End Function

    Public Function ObtenerAgenciaPorUsuario(ByVal IdUsuario As Integer) As Entidades.BEAgenciaRecaudadora Implements EmsambladoComun.ICaja.ObtenerAgenciaPorUsuario

        Dim objCaja As New BLCaja
        Return objCaja.ObtenerAgenciaPorUsuario(IdUsuario)

    End Function
End Class
