﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class ConciliacionServer
    Inherits MarshalByRefObject
    Implements IConciliacion

    Public Overrides Function InitializeLifetimeService() As Object
        '
        Return Nothing
        '
    End Function

    Public Function ProcesarArchivoConciliacion(ByVal request As BEConciliacionArchivo) As BEConciliacionResponse Implements EmsambladoComun.IConciliacion.ProcesarArchivoConciliacion
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ProcesarArchivoConciliacion(request)
        End Using
    End Function

    Public Function ConsultarConciliacionDiaria(ByVal request As BEConciliacionArchivo) As List(Of BEConciliacionDiaria) Implements EmsambladoComun.IConciliacion.ConsultarConciliacionDiaria
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarConciliacionDiaria(request)
        End Using
    End Function

    Public Function ConsultarPreConciliacionDetalle(ByVal request As BEConciliacionArchivo) As List(Of BEPreConciliacionDetalle) Implements EmsambladoComun.IConciliacion.ConsultarPreConciliacionDetalle
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarPreConciliacionDetalle(request)
        End Using
    End Function

    Public Function ConsultarUltimasConciliaciones(ByVal request As BEConciliacionRequest) As List(Of BEConciliacionResponse) Implements EmsambladoComun.IConciliacion.ConsultarUltimasConciliaciones
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarUltimasConciliaciones(request)
        End Using
    End Function

    Public Function ConsultarConciliacionArchivo(ByVal request As BEConciliacionArchivo) As System.Collections.Generic.List(Of Entidades.BEConciliacionArchivo) Implements EmsambladoComun.IConciliacion.ConsultarConciliacionArchivo
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarConciliacionArchivo(request)
        End Using
    End Function

    Public Function ConsultarConciliacionEntidad(ByVal request As BEConciliacionEntidad) As System.Collections.Generic.List(Of Entidades.BEConciliacionEntidad) Implements EmsambladoComun.IConciliacion.ConsultarConciliacionEntidad
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarConciliacionEntidad(request)
        End Using
    End Function

    Public Function ConsultarDetalleConciliacion(ByVal request As Entidades.BEConciliacionDetalle) As System.Collections.Generic.List(Of Entidades.BEConciliacionDetalle) Implements EmsambladoComun.IConciliacion.ConsultarDetalleConciliacion
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarDetalleConciliacion(request)
        End Using
    End Function

    Public Sub RecalcularCIPsConciliacion(ByVal IdUsuario As Integer) Implements EmsambladoComun.IConciliacion.RecalcularCIPsConciliacion
        Using oBLConciliacion As New BLConciliacion
            oBLConciliacion.RecalcularCIPsConciliacion(IdUsuario)
        End Using
    End Sub
    Public Function ConsultarOrdenesConcArch(ByVal request As BEConciliacionDetalle) As List(Of BEOrdenPago) Implements EmsambladoComun.IConciliacion.ConsultarOrdenesConcArch
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ConsultarOrdenesConcArch(request)
        End Using
    End Function

    Public Function ProcesarArchivoPreConciliacion(request As Entidades.BEPreConciliacionRequest) As Entidades.BEConciliacionResponse Implements EmsambladoComun.IConciliacion.ProcesarArchivoPreConciliacion
        Using oBLConciliacion As New BLConciliacion
            Return oBLConciliacion.ProcesarArchivoPreConciliacion(request)
        End Using
    End Function
End Class