﻿Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class CuentaServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.ICuenta


    Public Function ConsultarMovimientoCuenta(ByVal be As Entidades.BEMovimientoCuentaBanco) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements EmsambladoComun.ICuenta.ConsultarMovimientoCuenta
        Dim obe As New Negocio.BLCuenta
        Return obe.ConsultarMovimientoCuenta(be)
    End Function

    Public Function ConsultarDetMovCuentaPorIdMovCuentaBanco(ByVal be As Entidades.BEDetMovimientoCuenta) As System.Collections.Generic.List(Of _3Dev.FW.Entidades.BusinessEntityBase) Implements EmsambladoComun.ICuenta.ConsultarDetMovCuentaPorIdMovCuentaBanco
        Dim obe As New Negocio.BLCuenta
        Return obe.ConsultarDetMovCuentaPorIdMovCuentaBanco(be)
    End Function

End Class
