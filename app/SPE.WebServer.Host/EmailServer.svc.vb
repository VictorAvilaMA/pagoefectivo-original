﻿Imports SPE.EmsambladoComun
Imports SPE.Negocio
Imports System.ServiceModel.Activation
Imports SPE.Entidades

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class EmailServer
    Inherits MarshalByRefObject
    Implements IEmail

    Public Overrides Function InitializeLifetimeService() As Object

        Return Nothing

    End Function
    Private blemail As BLEmail

    Private Function InstanciaBLEmail() As BLEmail
        If (blemail Is Nothing) Then
            blemail = New BLEmail()
        End If
        Return blemail
    End Function

    Public Function EnviarCorreoAAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer Implements EmsambladoComun.IEmail.EnviarCorreoAAdministrador
        Return InstanciaBLEmail().EnviarCorreoAAdministrador(pfrom, psubject, pbody)
    End Function

    'agregado
    Public Function EnviarCorreoContactarAdministrador(ByVal pfrom As String, ByVal psubject As String, ByVal pbody As String) As Integer Implements EmsambladoComun.IEmail.EnviarCorreoContactarAdministrador
        Return InstanciaBLEmail().EnviarCorreoContactarAdministrador(pfrom, psubject, pbody)
    End Function


    Public Function EnviarCorreoConfirmacionUsuario(ByVal userId As Integer, ByVal nombrecliente As String, ByVal clave As String, ByVal pguid As String, ByVal pto As String) As Integer Implements EmsambladoComun.IEmail.EnviarCorreoConfirmacionUsuario
        Return InstanciaBLEmail().EnviarCorreoConfirmacionUsuario(userId, nombrecliente, clave, pguid, pto)
    End Function

    Public Function EnviarEmail(ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer Implements EmsambladoComun.IEmail.EnviarEmail
        Return InstanciaBLEmail().EnviarEmail(pfrom, pto, psubject, pbody, pisBodyHtml)
    End Function

    Public Function EnviarEmailGeneral(ByVal phost As String, ByVal pfrom As String, ByVal pto As String, ByVal psubject As String, ByVal pbody As String, ByVal pisBodyHtml As Boolean) As Integer Implements EmsambladoComun.IEmail.EnviarEmailGeneral
        Return InstanciaBLEmail().EnviarEmailGeneral(phost, pfrom, pto, psubject, pbody, pisBodyHtml)
    End Function

    Public Function EnviarEmailContactenos(ByVal obeMail As SPE.Entidades.BEMailContactenos) As Integer Implements EmsambladoComun.IEmail.EnviarEmailContactenos
        Return InstanciaBLEmail.EnviarEmailContactenos(obeMail)
    End Function

    'jeffersonmendoza
    Public Function EnviarEmailContactenosEmpresa(ByVal obeMail As SPE.Entidades.BEMailContactenosEmpresa) As Integer Implements EmsambladoComun.IEmail.EnviarEmailContactenosEmpresa
        Return InstanciaBLEmail.EnviarEmailContactenosEmpresa(obeMail)
    End Function
    Public Function EnviarCorreoSuscriptor(ByVal EmailSuscriptor As String) As Integer Implements EmsambladoComun.IEmail.EnviarCorreoSuscriptor
        Return InstanciaBLEmail.EnviarCorreoSuscriptor(EmailSuscriptor)
    End Function

    'ToGenerado
    Public Function EnviarEmailCambioDeEstadoCipDeExpAGen(ByVal oBEOrdenPago As SPE.Entidades.BEOrdenPago) As Integer Implements EmsambladoComun.IEmail.EnviarEmailCambioDeEstadoCipDeExpAGen
        Return InstanciaBLEmail.EnviarEmailCambioDeEstadoCipDeExpAGen(oBEOrdenPago)
    End Function

End Class
