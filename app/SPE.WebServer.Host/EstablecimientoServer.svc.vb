﻿Imports System
Imports System.Collections.Generic
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class EstablecimientoServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.IEstablecimiento





    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLEstablecimiento
    End Function

    Public Function ConsultarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String Implements EmsambladoComun.IEstablecimiento.ConsultarOrdenPago

        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.ConsultarOrdenPago(obeParametroPOS)

    End Function

    Public Function CancelarOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String Implements EmsambladoComun.IEstablecimiento.CancelarOrdenPago

        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.CancelarOrdenPago(obeParametroPOS)

    End Function

    Public Function AnularOrdenPago(ByVal obeParametroPOS As BEParametroPOS) As String Implements EmsambladoComun.IEstablecimiento.AnularOrdenPago

        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.AnularOrdenPago(obeParametroPOS)

    End Function

    Public Function AperturarCaja(ByVal obeParametroPOS As BEParametroPOS) As String Implements EmsambladoComun.IEstablecimiento.AperturarCaja

        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.AperturarCaja(obeParametroPOS)

    End Function

    Public Function ObtenerOperadores() As System.Collections.Generic.List(Of Entidades.BEOperador) Implements EmsambladoComun.IEstablecimiento.ObtenerOperadores

        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.ObtenerOperadores()

    End Function

    Public Function CerrarCaja(ByVal obeParametroPOS As BEParametroPOS) As String Implements EmsambladoComun.IEstablecimiento.CerrarCaja

        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.CerrarCaja(obeParametroPOS)

    End Function

    Public Function CerrarAperturarCaja() As String Implements EmsambladoComun.IEstablecimiento.CerrarAperturarCaja
        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.CerrarAperturarCaja()
    End Function

    Public Function AperturarCaja() As String Implements EmsambladoComun.IEstablecimiento.AperturarCaja
        Dim objEstablecimiento As New BLEstablecimiento
        Return objEstablecimiento.AperturarCaja()
    End Function

    Public Function GetPendientesCancelacion(ByVal objComercio As Entidades.BEComercio) As System.Collections.Generic.List(Of Entidades.BEEstablecimiento) Implements EmsambladoComun.IEstablecimiento.GetPendientesCancelacion
        Return New BLEstablecimiento().GetPendientesCancelacion(objComercio)
    End Function

    Public Function GetLiquidacionPendientexIdEstablecimiento(ByVal objEstablecimiento As Entidades.BEEstablecimiento) As System.Collections.Generic.List(Of Entidades.BEEstablecimiento) Implements EmsambladoComun.IEstablecimiento.GetLiquidacionPendientexIdEstablecimiento
        Return New BLEstablecimiento().GetLiquidacionPendientexIdEstablecimiento(objEstablecimiento)
    End Function


    Public Function LiquidarBloque(ByVal objAperturaOrigen As System.Collections.Generic.List(Of Entidades.BEAperturaOrigen)) As Integer Implements EmsambladoComun.IEstablecimiento.LiquidarBloque
        Return New BLEstablecimiento().LiquidarBloque(objAperturaOrigen)
    End Function

    Public Function ConsultarCajasLiquidadas(ByVal obeOrdenPago As Entidades.BEOrdenPago) As System.Collections.Generic.List(Of Entidades.BEMovimiento) Implements EmsambladoComun.IEstablecimiento.ConsultarCajasLiquidadas
        Return New BLEstablecimiento().ConsultarCajasLiquidadas(obeOrdenPago)
    End Function
End Class
