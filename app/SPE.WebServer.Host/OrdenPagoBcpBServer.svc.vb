﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation
Imports _3Dev.FW.Entidades

Public Class OrdenPagoBcpBServer
    Implements IOrdenPagoBcpB

    Public Function ConsultarHistoricoMovimientosporIdOrden(idOrdenPago As String) As System.Collections.Generic.List(Of Entidades.BEMovimiento) Implements EmsambladoComun.IOrdenPagoBcpB.ConsultarHistoricoMovimientosporIdOrden
        Return New BLOrdenPago().ConsultarHistoricoMovimientosporIdOrden(idOrdenPago)
    End Function

    Public Function ConsultarOrdenPagoPorId(obeOrdenPago As Entidades.BEOrdenPago) As Entidades.BEOrdenPago Implements EmsambladoComun.IOrdenPagoBcpB.ConsultarOrdenPagoPorId
        Return New BLOrdenPago().ConsultarOrdenPagoPorId(obeOrdenPago)
    End Function

End Class
