﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class PlantillaServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IPlantilla

    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New BLPlantilla()
    End Function

    Public Function ConsultarPlantillaPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As Entidades.BEPlantilla Implements EmsambladoComun.IPlantilla.ConsultarPlantillaPorTipoYVariacion
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarPlantillaPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
        End Using
    End Function

    Public Function ConsultarPlantillaPorTipoYVariacionExiste(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As Entidades.BEPlantilla Implements EmsambladoComun.IPlantilla.ConsultarPlantillaPorTipoYVariacionExiste
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarPlantillaPorTipoYVariacionExiste(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
        End Using
    End Function

    '<add Peru.Com FaseIII>
    Public Function ConsultarPlantillaTipo(ByVal idMoneda As Int32) As List(Of BEPlantillaTipo) Implements EmsambladoComun.IPlantilla.ConsultarPlantillaTipoPorMoneda
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarPlantillaTipoPorMoneda(idMoneda)
        End Using
    End Function
    Public Function ConsultarPlantillaTipoPorMonedaYServicio(ByVal idMoneda As Int32, ByVal idServicio As Int32) As List(Of BEPlantillaTipo) Implements EmsambladoComun.IPlantilla.ConsultarPlantillaTipoPorMonedaYServicio
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarPlantillaTipoPorMonedaYServicio(idMoneda, idServicio)
        End Using
    End Function
    Public Function GenerarPlantillaDesdePlantillaBase(ByVal oBEPlantilla As BEPlantilla) As BEPlantilla Implements EmsambladoComun.IPlantilla.GenerarPlantillaDesdePlantillaBase
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.GenerarPlantillaDesdePlantillaBase(oBEPlantilla)
        End Using
    End Function
    Public Function ConsultarPlantillaParametroPorIdPlantilla(ByVal idPlantilla As Int32) As List(Of BEPlantillaParametro) Implements EmsambladoComun.IPlantilla.ConsultarPlantillaParametroPorIdPlantilla
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarPlantillaParametroPorIdPlantilla(idPlantilla)
        End Using
    End Function
    Public Function ConsultarParametrosFuentes() As List(Of BEPlantillaParametro) Implements EmsambladoComun.IPlantilla.ConsultarParametrosFuentes
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarParametrosFuentes()
        End Using
    End Function
    Public Function RegistrarPlantillaParametros(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer Implements EmsambladoComun.IPlantilla.RegistrarPlantillaParametros
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.RegistrarPlantillaParametros(oBEPlantillaParametro)
        End Using
    End Function
    Public Function ActualizarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer Implements EmsambladoComun.IPlantilla.ActualizarPlantillaParametro
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ActualizarPlantillaParametro(oBEPlantillaParametro)
        End Using
    End Function
    Public Function EliminarPlantillaParametro(ByVal oBEPlantillaParametro As BEPlantillaParametro) As Integer Implements EmsambladoComun.IPlantilla.EliminarPlantillaParametro
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.EliminarPlantillaParametro(oBEPlantillaParametro)
        End Using
    End Function
    Public Function ActualizarPlantilla(ByVal oBEPlantilla As BEPlantilla) As Integer Implements EmsambladoComun.IPlantilla.ActualizarPlantilla
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ActualizarPlantilla(oBEPlantilla)
        End Using
    End Function

    '************************************************************************************************** 
    ' Método          : ConsultarPLantillasXServicioGenPago
    ' Descripción     : Consulta las plantillas de los correos correspondientes a un servicio para la pasarela de Pagos
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 29/05/2012
    ' Parametros_In   : idServicio , idMoneda
    ' Parametros_Out  : Cadena HTML descripción de bancos
    ''**************************************************************************************************
    Public Function ConsultarPLantillasXServicioGenPago(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal codPlantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String Implements EmsambladoComun.IPlantilla.ConsultarPLantillasXServicioGenPago
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.ConsultarPLantillasXServicioGenPago(idServicio, idMoneda, codPlantillaTipo, valoresDinamicos, codPlantillaTipoVariacion)
        End Using
    End Function
    '************************************************************************************************** 
    ' Método          : GenerarPlantillaToHtmlByTipo
    ' Descripción     : Devuelve el html de una plantilla con los valoresDinamicos y staticos cambiados apartir del IdServicio y Codigo de tipo de PLantilla
    ' Autor           : Jonathan Bastidas
    ' Fecha/Hora      : 30/05/2012
    ' Parametros_In   : IdServicio, codTipoPLantilla , valoresDinamicos
    ' Parametros_Out  : Cadena HTML de la plantilla
    ''**************************************************************************************************
    Public Function GenerarPlantillaToHtmlByTipo(ByVal idServicio As Integer, ByVal codPLantillaTipo As String, ByVal valoresDinamicos As Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String Implements EmsambladoComun.IPlantilla.GenerarPlantillaToHtmlByTipo
        Using oblPlantilla As New BLPlantilla
            Return oblPlantilla.GenerarPlantillaToHtmlByTipo(idServicio, codPLantillaTipo, valoresDinamicos, codPlantillaTipoVariacion)
        End Using
    End Function

    Public Function ConsultarPlantillasXServicioQueEsPE(ByVal idServicio As Integer, ByVal idMoneda As Integer, ByVal valoresDinamicos As System.Collections.Generic.Dictionary(Of String, String), ByVal codPlantillaTipoVariacion As String) As String Implements EmsambladoComun.IPlantilla.ConsultarPlantillasXServicioQueEsPE
        Using oBLPlantilla As New BLPlantilla
            Return oBLPlantilla.ConsultarPlantillasXServicioQueEsPE(idServicio, idMoneda, valoresDinamicos, codPlantillaTipoVariacion)
        End Using
    End Function

    Public Function ObtenerSeccionesPlantillaPorServicio(ByVal idServicio As Integer, ByVal idPlantillaFormato As Integer) As List(Of BEPlantillaSeccion) Implements EmsambladoComun.IPlantilla.ObtenerSeccionesPlantillaPorServicio
        Using oBLPlantilla As New BLPlantilla
            Return oBLPlantilla.ObtenerSeccionesPlantillaPorServicio(idServicio, idPlantillaFormato)
        End Using
    End Function

    Public Function ActualizarServicioSeccion(ByVal lstPlantillaSeccion As List(Of BEPlantillaSeccion), ByVal idUsuario As Integer) As Integer Implements EmsambladoComun.IPlantilla.ActualizarServicioSeccion
        Using oBLPlantilla As New BLPlantilla
            Return oBLPlantilla.ActualizarServicioSeccion(lstPlantillaSeccion, idUsuario)
        End Using
    End Function

    Function ConsultarPlantillaYParametrosPorTipoYVariacion(ByVal idServicio As Integer, ByVal codPlantillaTipo As String, ByVal codPlantillaTipoVariacion As String) As BEPlantilla Implements EmsambladoComun.IPlantilla.ConsultarPlantillaYParametrosPorTipoYVariacion
        Using oBLPlantilla As New BLPlantilla
            Return oBLPlantilla.ConsultarPlantillaYParametrosPorTipoYVariacion(idServicio, codPlantillaTipo, codPlantillaTipoVariacion)
        End Using
    End Function

End Class

