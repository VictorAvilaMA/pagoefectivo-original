﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Text
Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class PuntoVentaServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements IPuntoVenta

    Public Function ConsultarFC(ByVal request As BEFCConsultarRequest) As BEFCResponse(Of BEFCConsultarResponse) Implements EmsambladoComun.IPuntoVenta.ConsultarFC
        Return New BLPuntoVenta().ConsultarFC(request)
    End Function

    Public Function CancelarFC(ByVal request As BEFCCancelarRequest) As BEFCResponse(Of BEFCCancelarResponse) Implements EmsambladoComun.IPuntoVenta.CancelarFC
        Return New BLPuntoVenta().CancelarFC(request)
    End Function

    Public Function AnularFC(ByVal request As BEFCAnularRequest) As BEFCResponse(Of BEFCAnularResponse) Implements EmsambladoComun.IPuntoVenta.AnularFC
        Return New BLPuntoVenta().AnularFC(request)
    End Function

    'LIFE
    Public Function ConsultarLF(ByVal request As BELFConsultarRequest) As BELFResponse(Of BELFConsultarResponse) Implements EmsambladoComun.IPuntoVenta.ConsultarLF
        Return New BLPuntoVenta().ConsultarLF(request)
    End Function

    Public Function CancelarLF(ByVal request As BELFCancelarRequest) As BELFResponse(Of BELFCancelarResponse) Implements EmsambladoComun.IPuntoVenta.CancelarLF
        Return New BLPuntoVenta().CancelarLF(request)
    End Function

    Public Function ConsultarMoMo(ByVal request As BEMoMoConsultarRequest) As BEMoMoResponse(Of BEMoMoConsultarResponse) Implements EmsambladoComun.IPuntoVenta.ConsultarMoMo
        Return New BLPuntoVenta().ConsultarMoMo(request)
    End Function

    Public Function CancelarMoMo(ByVal request As BEMoMoCancelarRequest) As BEMoMoResponse(Of BEMoMoCancelarResponse) Implements EmsambladoComun.IPuntoVenta.CancelarMoMo
        Return New BLPuntoVenta().CancelarMoMo(request)
    End Function

    Public Function AnularMoMo(ByVal request As BEMoMoAnularRequest) As BEMoMoResponse(Of BEMoMoAnularResponse) Implements EmsambladoComun.IPuntoVenta.AnularMoMo
        Return New BLPuntoVenta().AnularMoMo(request)
    End Function
End Class
