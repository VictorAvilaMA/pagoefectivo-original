﻿Imports System.ServiceModel.Activation
Imports SPE.Entidades
Imports SPE.Negocio
Imports SPE.EmsambladoComun

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class RepresentanteServer
    Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.IRepresentante

    Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
        Return New SPE.Negocio.BLRepresentante
    End Function
    Public Function ObtenerEmpresaporIDRepresentante(ByVal objRepresentante As BERepresentante) As BERepresentante Implements IRepresentante.ObtenerEmpresaporIDRepresentante
        Dim obeRepr As New BLRepresentante
        Return obeRepr.ObtenerEmpresaporIDRepresentante(objRepresentante)
    End Function
    Public Function ConsultarEmpresasPorIdUsuario(ByVal be As BEUsuarioBase) As List(Of BEEmpresaContratante) Implements IRepresentante.ConsultarEmpresasPorIdUsuario
        Return New BLRepresentante().ConsultarEmpresasPorIdUsuario(be)
    End Function
End Class
