﻿Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class SPEMemberShipServer
    Inherits _3Dev.FW.ServidorRemoting.Seguridad.CustomMemberShipServer
    Implements SPE.EmsambladoComun.Seguridad.IMemberShipProvider

    Public Overrides Function GetConstructorBusinessProviderObject() As _3Dev.FW.Negocio.Seguridad.BLCustomMembershipProvider
        Return New SPE.Negocio.Seguridad.BLMemberShipProvider()
    End Function
    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function
End Class