﻿Imports System.ServiceModel.Activation
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class SPEUsuarioServer
    Inherits _3Dev.FW.ServidorRemoting.Seguridad.UsuarioServer
    'Inherits _3Dev.FW.ServidorRemoting.BFServerMaintenaceBase
    Implements SPE.EmsambladoComun.Seguridad.IUSuario

    Public Overrides Function GetConstructorBusinessProviderObject() As _3Dev.FW.Negocio.Seguridad.BLUsuario
        Return New SPE.Negocio.Seguridad.BLUsuario
    End Function

    'Public Overrides Function GetConstructorBusinessLogicObject() As _3Dev.FW.Negocio.BLMaintenanceBase
    '    Return New SPE.Negocio.Seguridad.BLUsuario
    'End Function

    Public Overrides Function InitializeLifetimeService() As Object
        Return Nothing
    End Function

End Class