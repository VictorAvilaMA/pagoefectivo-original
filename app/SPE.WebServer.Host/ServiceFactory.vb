﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Description
Imports SPE.EmsambladoComun

Public Class ServiceFactory
    Inherits ServiceHostFactory

    Public Overrides Function CreateServiceHost(constructorString As String, baseAddresses() As System.Uri) As System.ServiceModel.ServiceHostBase
        Dim host As ServiceHost = MyBase.CreateServiceHost(constructorString, baseAddresses)
        For Each operation In host.Description.Endpoints.First().Contract.Operations
            operation.Behaviors.Find(Of DataContractSerializerOperationBehavior)().DataContractResolver = New EntityBaseContractResolver()
        Next
        Return host
    End Function

End Class
