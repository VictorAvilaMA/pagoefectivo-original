﻿Imports SPE.EmsambladoComun
Imports SPE.Entidades
Imports SPE.Negocio
Imports System.ServiceModel.Activation

<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class UbigeoServer
    Inherits MarshalByRefObject
    Implements IUbigeo

    Public Overrides Function InitializeLifetimeService() As Object

        Return Nothing

    End Function

    Public Function ConsultarPais() As List(Of BEPais) Implements IUbigeo.ConsultarPais
        '
        Dim objUgigeo As New BLUbigeo
        Return objUgigeo.ConsultarPais()
        '
    End Function

    Public Function ConsultarDepartamentoPorIdPais(ByVal IdPais As Integer) As List(Of BEDepartamento) Implements IUbigeo.ConsultarDepartamentoPorIdPais
        '
        Dim objUgigeo As New BLUbigeo
        Return objUgigeo.ConsultarDepartamentoPorIdPais(IdPais)
        '
    End Function


    Public Function ConsultarCiudadPorIdDepartamento(ByVal IdDepartamento As Integer) As List(Of BECiudad) Implements IUbigeo.ConsultarCiudadPorIdDepartamento
        '
        Dim objUgigeo As New BLUbigeo
        Return objUgigeo.ConsultarCiudadPorIdDepartamento(IdDepartamento)
        '
    End Function

End Class
