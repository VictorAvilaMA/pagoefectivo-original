﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ConsultarApp.aspx.vb" Inherits="SPE.WebServiceTest.ConsultarApp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Verificar valores de configuracion</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlLogin" runat="server">
    <div id="divLogin" style=" border-right: 1px solid #000000; width:300px;">
    <fieldset>
      <legend>Login</legend>
      <table width="100%">
        <tr>
          <td style="width:100px;">Usuario:</td>
          <td style="width: 153px"><asp:TextBox ID="txtUsuario" runat="server" Width="100%"></asp:TextBox></td>
          <td>
            <asp:RequiredFieldValidator ID="rfvtxtUsuario" runat="server" 
                                                    ControlToValidate="txtUsuario" ErrorMessage="Ingresa el usuario" Display="Dynamic">*
                                                    </asp:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td>Contraseña:</td>
          <td style="width: 153px"><asp:TextBox ID="txtContraseña" runat="server" Width="100%" TextMode="Password"></asp:TextBox></td>
          <td>
            <asp:RequiredFieldValidator ID="rfvtxtContraseña" runat="server" 
                                                    ControlToValidate="txtContraseña" ErrorMessage="Ingresa la contraseña" 
                                                    Display="Dynamic">*
                                                    </asp:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="width: 153px"><asp:Button ID="btnIngresar" runat="server" Text="Ingresar" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="width: 153px"><asp:Label ID="lblLogeo" runat="server" Width="100%"></asp:Label></td>
        </tr>
      </table>
      <asp:ValidationSummary ID="ValidationSummary01" runat="server" />
    </fieldset>
    </div>
    </asp:Panel>
       <br />
        <asp:TextBox ID="txtValidar" runat="server" Text="8D24A9B7-B6DC-483f-A8EE-3437AF9BBA7APasarela" Width="358px" TextMode="Password" Visible="False"></asp:TextBox>
      <asp:Button ID="btnPanel" runat="server"  Text="Validar" Visible="False" />   <asp:Button ID="btnSalir" runat="server"  Text="Salir" Visible="False" />
      <asp:Panel ID="pnlDatos" runat="server" Visible="false"  >  
            <div>
            <br />
          1)  Estructura del Web.Config del Portal
                <br />
                
                <table>
                <tr>
                    <td>Resultado:</td>
                    <td>
                        <asp:TextBox ID="txtResultado" runat="server" TextMode="MultiLine" Height="600px" Width="701px"></asp:TextBox>
                    </td>
                </tr>
                   
                </table>
             </div>    
    </asp:Panel>    
    </form>
</body>
</html>
