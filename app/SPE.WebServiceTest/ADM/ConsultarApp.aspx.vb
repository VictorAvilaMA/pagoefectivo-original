﻿Imports System.Configuration
Imports System.Configuration.ConfigurationManager
Public Class ConsultarApp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("SesAdmUsuario") = Nothing
            Page.Form.DefaultButton = btnIngresar.UniqueID
            txtUsuario.Focus()
        End If
    End Sub

    Protected Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        Dim esLogin As Boolean
        lblLogeo.Text = ""
        pnlLogin.Visible = True
        Try

            If txtUsuario.Text.Trim = "JVARGAS" Then
                esLogin = True
            Else
                esLogin = False
                MostrarMensajeError(lblLogeo, "Usuario no valido")
                Exit Sub
            End If

            If txtContraseña.Text.Trim = "S0p0rt3182" Then
                esLogin = True
            Else
                esLogin = False
                MostrarMensajeError(lblLogeo, "Contraseña no valida")
                Exit Sub
            End If

            If esLogin Then
               
                txtValidar.Visible = True
                btnPanel.Visible = True
                btnSalir.Visible = True
                pnlLogin.Visible = False
            Else
                txtValidar.Visible = False
                btnPanel.Visible = False
                btnSalir.Visible = False
            End If

        Catch ex As Exception
            MostrarMensajeError(lblLogeo, "Error: " + ex.Message)
        End Try
    End Sub
    Private Sub MostrarMensajeOk(ByVal lbl As Label, ByVal msg As String)
        lbl.CssClass = "message_ok"
        lbl.Text = msg
    End Sub

    Private Sub MostrarMensajeError(ByVal lbl As Label, ByVal msg As String)
        lbl.CssClass = "message_error"
        lbl.Text = msg
    End Sub
    Dim claveExcluir As String
    Protected Sub btnPanel_Click(sender As Object, e As EventArgs) Handles btnPanel.Click
        Dim arrString As String() = {"ValidarAppWebservistest"}
        Dim buscarTermino As Predicate(Of String) = AddressOf ExisteTerminoApp

        Try
            pnlDatos.Visible = (AppSettings("ValidarAppWebservistest") <> "" And txtValidar.Text = AppSettings("ValidarAppWebservistest"))

            If Not pnlDatos.Visible Then Exit Sub

            Dim sbText As New System.Text.StringBuilder

            For Each clave As String In ConfigurationManager.AppSettings.AllKeys

                claveExcluir = clave

                If Array.Exists(arrString, buscarTermino) Then Continue For

                sbText.AppendFormat("{0} => {1}" + vbNewLine, clave, AppSettings(clave).Trim)
            Next

            txtResultado.Text = sbText.ToString

        Catch ex As Exception
            Response.Write("Error: " + ex.Message)
        End Try
    End Sub
    Private Function ExisteTerminoApp(ByVal palabra As String) As Boolean
        Return (palabra.Trim = claveExcluir.Trim)
    End Function
End Class