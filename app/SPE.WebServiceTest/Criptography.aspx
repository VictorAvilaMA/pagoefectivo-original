﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Criptography.aspx.vb" Inherits="SPE.WebServiceTest.Criptography" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <h3> Encriptamiento 1.0 </h3>
    <div>
        <asp:TextBox ID="txtTextoDesencriptado" runat="server" TextMode="MultiLine" style="width:45%;float:left;height:250px"></asp:TextBox>
        <asp:TextBox ID="txtTextoEncriptado" runat="server" TextMode="MultiLine" style="width:45%;float:right;height:250px"></asp:TextBox>
    </div>
    <div style="width:45%;float:left">
        <asp:Button ID="btnEncriptar" runat="server" Text="Encriptar" />
    </div>
    <div style="width:45%;float:right">
        <asp:Button ID="btnDesencriptar" runat="server" Text="Desencriptar" />
    </div>
    <br />
    <br />
    <h3> Encriptamiento 2.0 </h3>
     <div>
        <label style="width:45%;float:left">Direccion clave Privada</label>
        <label style="width:45%;float:right">Direccion clave Publica</label>
    </div>
    <div>
        <asp:TextBox ID="txtPathPrivate" runat="server" style="width:45%;float:left">C:\\PagoEfectivo2\\Claves\\SPE_PrivateKey.1pz</asp:TextBox>
        <asp:TextBox ID="txtPathPublic" runat="server" style="width:45%;float:right">C:\\PagoEfectivo2\\Claves\\SPE_PublicKey.1pz</asp:TextBox>
    </div>
    <div>
        <asp:TextBox ID="txtDesencriptadov2" runat="server" TextMode="MultiLine" style="width:45%;float:left;height:250px"></asp:TextBox>
        <asp:TextBox ID="txtEncriptadov2" runat="server" TextMode="MultiLine" style="width:45%;float:right;height:250px"></asp:TextBox>
    </div>
    <div style="width:45%;float:left">
        <asp:Button ID="btnEncriptarv2" runat="server" Text="Encriptar v2" />
    </div>
    <div style="width:45%;float:right">
        <asp:Button ID="btnDesencriptarv2" runat="server" Text="Desencriptar v2" />
    </div>
    <div style="clear:both"></div>
    <br />
    <br />
    <h3> Firma 2.0 </h3>
    Resultado: <asp:Literal ID="ltlResultado" runat="server"></asp:literal>
    <div>
        <asp:TextBox ID="txtFirmador" runat="server" TextMode="MultiLine" style="width:45%;float:left;height:250px"></asp:TextBox>
        <asp:TextBox ID="txtValFirmador" runat="server" TextMode="MultiLine" style="width:45%;float:right;height:250px"></asp:TextBox>
    </div>
    <div style="width:45%;float:left">
        <asp:Button ID="btnFirmar" runat="server" Text="Firmar v2" />
    </div>
    <div style="width:45%;float:right">
        <asp:Button ID="btnValFirmar" runat="server" Text="ValidarFirma v2" />
    </div>
    </form>
</body>
</html>
