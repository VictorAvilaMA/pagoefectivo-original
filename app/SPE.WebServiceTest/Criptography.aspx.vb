﻿Imports SPE.Utilitario
Imports SPE.Api

Public Class Criptography
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnEncriptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEncriptar.Click
        Dim clsEncripta As New ClsLibreriax()
        txtTextoEncriptado.Text = clsEncripta.Encrypt(txtTextoDesencriptado.Text, clsEncripta.fEncriptaKey)
    End Sub

    Protected Sub btnDesencriptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDesencriptar.Click
        Dim clsEncripta As New ClsLibreriax()
        txtTextoDesencriptado.Text = clsEncripta.Decrypt(txtTextoEncriptado.Text, clsEncripta.fEncriptaKey)
    End Sub

    Protected Sub btnEncriptarv2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEncriptarv2.Click
        Api.PagoEfectivo.PublicPathContraparte = txtPathPublic.Text
        txtEncriptadov2.Text = Api.PagoEfectivo.EnciptarTexto(txtDesencriptadov2.Text)
    End Sub

    Protected Sub btnDesencriptarv2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDesencriptarv2.Click
        Api.PagoEfectivo.PrivatePath = txtPathPrivate.Text
        txtDesencriptadov2.Text = Api.PagoEfectivo.DesenciptarTexto(txtEncriptadov2.Text)
    End Sub

    Protected Sub btnFirmar_Click(sender As Object, e As EventArgs) Handles btnFirmar.Click
        Api.PagoEfectivo.PrivatePath = txtPathPrivate.Text
        txtValFirmador.Text = Api.PagoEfectivo.Firmar(txtFirmador.Text)
    End Sub

    Protected Sub btnValFirmar_Click(sender As Object, e As EventArgs) Handles btnValFirmar.Click
        Api.PagoEfectivo.PublicPathContraparte = txtPathPublic.Text
        ltlResultado.Text = "<b>" & Api.PagoEfectivo.ValidarFirma(txtFirmador.Text, txtValFirmador.Text).ToString() & "</b>"
        'RegisterClientScriptBlock("key", "alert(" + +");")
        'ClientScript.RegisterClientScriptBlock(Me.GetType, "key", "alert(" & Api.PagoEfectivo.ValidarFirma(txtFirmador.Text, txtValFirmador.Text).ToString() & ");", True)
    End Sub
End Class