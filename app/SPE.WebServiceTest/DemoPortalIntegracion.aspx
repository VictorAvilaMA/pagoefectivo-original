﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DemoPortalIntegracion.aspx.vb"
    Inherits="SPE.WebServiceTest.DemoPortalIntegracion" %>

<!DOCTYPE html>
<html class="no-js" lang="es">
<head runat="server">
    <title>Demo Integracion</title>
    <link href="colorbox/colorbox.css" rel="stylesheet" type="text/css" />
    <link href="css/estilos.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/pagoEfectivo.js"></script>
    <script type="text/javascript" src="colorbox/jquery.colorbox-min.js" ></script>
    <script type="text/javascript">
        $(document).ready(function () {
            listaInformacion = {};
            var urlBasePE = '<%= System.Configuration.ConfigurationManager.AppSettings.Get("urlSPE") + "/INFO/" %>';
            $("#btnAsignar").click(function () {
                cargarInfoPago({ url: urlBasePE, elementTarget: $('#infoPago'), codigo: $('#txtCodigo').val() });
                var urlPe = '<%= System.Configuration.ConfigurationManager.AppSettings.Get("urlSPE") + "/CNT/" %>' + "QueEsPagoEfectivo.aspx?cod=" + $('#txtCodigo').val() + "&mon=" + $('#txtMoneda').val();
                $(".iframe").colorbox({ iframe: true, width: "642", height: "715", href: urlPe });
                $("#infoExample").fadeOut();
                $('#bancos').fadeOut();
                $("#infoExample").fadeIn();
                return false;
            });
            $("#btnGenerarCIP").click(function () {
                cargarInfoBancos({ elementTarget: $("#bancos"), moneda: $('#txtMoneda').val() })
                $("#bancos").fadeIn();
                return false;
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="pe_pago" style="overflow: hidden;">
            <div class="pe_detail">
                <h3>
                    Ingrese el codigo y moneda del Servicio:</h3>
                Codigo:
                <input id="txtCodigo" type="text" value="KOT" />
                <br />
                Moneda:
                <input id="txtMoneda" type="text" value="1" />
                <button id="btnAsignar" value="Generar" name="generar">
                    Generar</button>
            </div>
            <div style="clear: both">
            </div>
            <div id="infoExample" style="display: none; overflow: hidden;">
                <div class="pe_detail">
                    <h3>
                        Detalle de compra <a class="pe_inline_mdl cboxElement" href="#mira-tus-avisos"></a>
                    </h3>
                    <div class="pe_content">
                        <ul>
                            <li>Detalle del producto</li>
                            <li>comentarios adicionales</li>
                        </ul>
                        <p class="pe_total">
                            Total: <span>S/. <span id="pe_s-total">10.00</span></span></p>
                        <p class="pe_obs">
                            El precio incluye IGV. La compra, en la fecha indicada, est&aacute; sujeta al pago
                            antes de la fecha de cierre.</p>
                    </div>
                </div>
                <div class="pe_payment">
                    <h3>
                        M&eacute;todo de pago</h3>
                    <div class="pe_option_pago">
                        <input type="radio" style="float: left;" value="2" name="metodopago" checked="checked"
                            id="form-validation-field-1" />
                        <div style="float: left; width: 100px;">
                            <img width="74" height="38" src="img/pago-efectivo.jpg" /></div>
                        <div style="float: left; text-align: justify; width: 400px;">
                            <span id="infoPago">Compra con PagoEfectivo y paga a trav&eacute;s de internet o en
                                cualquier oficina del BCP, BBVA, Scotiabank, en Agencias de Pago de Servicios Western
                                Union y en establecimientosautorizados que tengan el logo de PagoEfectivo y/o FullCarga.</span><span
                                    style="font-size: 10px;"> <a class="iframe" href="http://dev.2.pagoefectivo.pe/CNT/QueEsPagoEfectivo.aspx"
                                        style="color: #006999; text-decoration: none;">&iquest;Qu&eacute; es PagoEfectivo?</a></span>
                        </div>
                        <div class="clear">
                        </div>
                        <br />
                        <div class="pe_total">
                            <asp:Button ID="btnGenerarCIP" runat="server" Text="Pagar" />
                        </div>
                    </div>
                </div>
                <div style="margin-top: 10px; width: 95%; overflow:hidden; display: none; border: solid 1px rgba(0,0,0,.5);" id="bancos"></div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
