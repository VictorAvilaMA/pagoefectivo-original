﻿Public Class TransaccionPagoExtorno
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim randNumber As New Random(DateTime.Now.Millisecond)

        Dim codMoneda As String = "001".TrimEnd()
        Dim CodServ As String = If("0004".Trim = "", "0", "0004")
        Dim NroOperacion As String = randNumber.Next(0, 99999999)
        Dim CodAgencia As String = "015"

        Dim NroOperacionAnulada As String = randNumber.Next(0, 99999999)
        Dim monto As String = "1.00".TrimEnd()
        Dim cip As String = System.Configuration.ConfigurationManager.AppSettings("CIPPrueba")
        If Not String.IsNullOrEmpty(cip) Then
            Try
                Using proxy As New ProxyWSAGBA.Service()
                    'pago
                    Dim Resultado As String = proxy.WSAGOperacion(cip, CodServ, codMoneda, NroOperacion, "", CodAgencia, "0", cip, "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", monto, "CA")
                    Response.Write(Resultado)
                    'extorno
                    Resultado = proxy.WSAGOperacion(cip, CodServ, codMoneda, NroOperacionAnulada, NroOperacion, CodAgencia, "0", cip, "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", monto, "EX")
                    Response.Write(Resultado)
                End Using
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        End If
    End Sub

End Class