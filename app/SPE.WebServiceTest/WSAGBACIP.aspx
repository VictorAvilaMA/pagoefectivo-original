<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSAGBACIP.aspx.vb" Inherits="SPE.WebServiceTest.WSAGBACIP"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prueba de Servicios Web de Agencias Bancarias</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
    <asp:Button ID="btnPanel" runat="server" Text="Validar" />
    <asp:Panel ID="pPanel" runat="server" Visible="false">
        <div>
            <br />
            1) Prueba de Servicios Web de Agencias Bancarias
            <table>
                <tr>
                    <td>
                        Nro Operacion:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtNroOperacion" runat="server" Width="304px"></asp:TextBox>
                    </td>
                </tr>
                <div id="DivExtorno" runat="server" visible="false">
                    <tr>
                        <td>
                            Nro Operacion Original:
                        </td>
                        <td style="width: 506px">
                            <asp:TextBox ID="txtNroOpOriginal" runat="server" Width="304px"></asp:TextBox>
                        </td>
                    </tr>
                </div>
                <tr>
                    <td>
                        CIP:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCIP" runat="server" Width="304px"></asp:TextBox>Colocar CIP sin ceros!
                    </td>
                </tr>
                <tr>
                    <td>
                        Moneda:
                    </td>
                    <td style="width: 506px">
                        <asp:DropDownList ID="dropMoneda" runat="server">
                            <asp:ListItem Value="001">Soles (001)</asp:ListItem>
                            <asp:ListItem Value="002">Dolares (002)</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Monto:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtMonto" runat="server" Width="305px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        C�digo de Servicio:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCodigoServicio" runat="server" Width="305px">0004</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        C�digo de Agencia:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtAgencia" runat="server" Width="305px"></asp:TextBox><%--015--%>Colocar solo en caso de Pago!
                    </td>
                </tr>
                <tr>
                    <td>
                        Operaci�n:
                    </td>
                    <td style="width: 506px">
                        <asp:DropDownList ID="dropOperacion" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="CO">Consulta (CO)</asp:ListItem>
                            <asp:ListItem Value="CA">Cancelar (CA)</asp:ListItem>
                            <asp:ListItem Value="EX">Extornar (EX)</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ejecutar en LOTE:
                    </td>
                    <td style="width: 506px">
                        <asp:CheckBox ID="chkLote" runat="server" Checked="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="width: 506px">
                        <asp:Button ID="btnEjecutar" runat="server" Text="Ejecutar" />
                    </td>
                </tr>
            </table>
            <br />
            <table style="float: left">
                <tr>
                    <td>
                        Resultado:
                    </td>
                    <td>
                        <asp:TextBox ID="txtResultado" runat="server" TextMode="MultiLine" Height="276px"
                            Width="501px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <div id="divLote" runat="server" visible="false">
                <table style="float: left">
                    <tr>
                        <td>
                            Numero de Operaciones Generadas:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNumOperacionesGeneradas" runat="server" Width="501px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Resultado de Lote:
                        </td>
                        <td>
                            <asp:TextBox ID="txtResultadoLote" runat="server" TextMode="MultiLine" Height="276px"
                                Width="501px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
