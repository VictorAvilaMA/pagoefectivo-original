Imports System.Xml
Imports ProxyWSAGBA
Partial Public Class WSAGBACIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GenerarRandom()
    End Sub

    Protected Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click

        Dim codMoneda As String = dropMoneda.SelectedValue.TrimEnd()
        Dim CodServ As String = If(txtCodigoServicio.Text.Trim = "", "0", txtCodigoServicio.Text)
        Dim NroOperacion As String = txtNroOperacion.Text
        Dim CodAgencia As String = txtAgencia.Text
        Dim resultado As String = ""

        Dim NroOperacionAnulada As String = If(dropOperacion.SelectedValue = "EX", txtNroOpOriginal.Text, "0")
        Dim monto As String = txtMonto.Text.TrimEnd()
        'Dim cip As String = txtCIP.Text.TrimEnd().PadLeft(14, "0")
        'Editado para que Dummy=Prod 
        Dim cip As String = txtCIP.Text.TrimEnd()

        If (chkLote.Checked) Then
            NroOperacionAnulada = If(NroOperacionAnulada = String.Empty, "0", NroOperacionAnulada)
            Dim CIPs As String() = cip.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
            Dim Montos As String() = monto.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
            Dim NroOpAnuladas As String() = NroOperacionAnulada.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
            Try
                divLote.Visible = True
                For index As Integer = 0 To CIPs.Length - 1
                    Dim indexMonto As Integer = ((index) Mod Montos.Length)
                    Dim indexNroOpAnuladas As Integer = ((index) Mod NroOpAnuladas.Length)

                    If index <> 0 Then
                        NroOperacion = GetRandom()
                    End If

                    Using proxy As New ProxyWSAGBA.Service()
                        resultado = proxy.WSAGOperacion(CIPs(index), _
                                                        CodServ, _
                                                        codMoneda, _
                                                        NroOperacion, _
                                                        NroOpAnuladas(indexNroOpAnuladas), _
                                                        CodAgencia, "1", _
                                                        CIPs(index), _
                                                        Montos(indexMonto), _
                                                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", _
                                                        Montos(indexMonto), _
                                                        dropOperacion.SelectedValue)
                    End Using

                    txtNumOperacionesGeneradas.Text = If(txtNumOperacionesGeneradas.Text.Trim = String.Empty, "", txtNumOperacionesGeneradas.Text + " ") & NroOperacion
                    txtResultado.Text = resultado
                    Dim xml As New XmlDocument
                    xml.InnerXml = "<?xml version=""1.0"" encoding=""utf-8"" ?>" & resultado
                    Dim element As XmlElement = xml.SelectSingleNode("string-array").SelectSingleNode("string")
                    txtResultadoLote.Text = txtResultadoLote.Text & "Operacion " & (index + 1) & ": " & element.InnerText & vbCrLf
                Next
            Catch ex As Exception
                txtResultado.Text = ex.ToString()
            End Try
        Else
            Try
                Using proxy As New ProxyWSAGBA.Service()
                    'resultado = proxy.WSAGOperacion(cip, CodServ, codMoneda, NroOperacion, NroOperacionAnulada, txtAgencia.Text, "1", cip, monto, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", monto, dropOperacion.SelectedValue)
                    resultado = proxy.WSAGOperacion(cip, CodServ, codMoneda, NroOperacion, NroOperacionAnulada, txtAgencia.Text, "0", cip, "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", "", "0.00", monto, dropOperacion.SelectedValue)
                End Using
                txtResultado.Text = resultado
            Catch ex As Exception
                txtResultado.Text = ex.ToString()
            End Try
        End If


    End Sub

    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSAGBACIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub


    Protected Sub dropOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dropOperacion.SelectedIndexChanged
        Select Case dropOperacion.SelectedValue
            Case "EX"
                DivExtorno.Visible = True
            Case Else
                DivExtorno.Visible = False
        End Select
        GenerarRandom()
    End Sub

    Private Sub GenerarRandom()
        Dim value As Integer
        Dim randNumber As New Random(DateTime.Now.Millisecond)
        value = randNumber.Next(0, 99999999)
        txtNroOperacion.Text = value.ToString("D8")
    End Sub
    Function GetRandom() As String
        Dim value As Integer
        Dim randNumber As New Random(DateTime.Now.Millisecond)
        value = randNumber.Next(0, 99999999)
        Return value.ToString("D8")
    End Function
End Class
