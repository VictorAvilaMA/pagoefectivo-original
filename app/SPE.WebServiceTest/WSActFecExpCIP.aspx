﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSActFecExpCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSActFecExpCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>   
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
        <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
        <asp:Button ID="btnPanel" runat="server"  Text="Validar" />   
        <asp:Panel ID="pPanel" runat="server" Visible="false"  >
           <div>
            <br />
             1)  Prueba Actualizaci&oacute;n de CIP desde WS
            <table>
             <tr>
                <td>CAPI:</td>
                <td style="width: 506px">
                    <asp:TextBox ID="txtCAPI" runat="server" Width="304px">6964dc43-1507-47f0-978c-954d7bb41b95</asp:TextBox>
                </td>
             </tr>
             <tr>
                <td>CClave:</td>
                <td style="width: 506px">
                    <asp:TextBox ID="txtCClave" runat="server" Width="304px">6ca4c8cb-7a6d-4327-b439-b0ec3a5988e9</asp:TextBox>
                </td>
             </tr>
             <tr>
                <td>cip:</td>
                <td style="width: 506px">
                                        <asp:TextBox ID="txtCIP" runat="server"></asp:TextBox>
                </td>
             </tr>  
             <tr>
                <td>Fecha de Expiración:</td>
                <td style="width: 506px">

    <div id="datetimepicker" class="input-append date">
      <asp:TextBox ID="txtFechaExpiracion" runat="server"></asp:TextBox>
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.pt-BR.js"></script>

    <script type="text/javascript">
        $('#datetimepicker').datetimepicker({
            format: 'dd/MM/yyyy hh:mm:ss',
            language: 'pt-BR'
        });
    </script>
                </td>
             </tr>          
                     
             <tr>
                <td>InfoRequest:</td>
                <td style="width: 506px">
                    <asp:TextBox ID="txtInfoRequest" TextMode="MultiLine"  runat="server" Height="200px" Width="200px"></asp:TextBox>
                </td>
             </tr>                    
             <tr>
                <td></td>
                <td style="width: 506px">
                    <asp:Button ID="btnActualizarCIP" runat="server" Text="Actualizar CIP" />
                </td>
             </tr>                         
            </table>
                
                <br />
                
                <table>
                <tr>
                    <td>Resultado:</td>
                    <td></td>
                </tr>
                    <tr>
                        <td>Estado:</td>
                        <td>
                            <asp:TextBox ID="txtEstado" runat="server"></asp:TextBox>
                        </td>
                     </tr>                            
                    <tr>
                        <td>Mensaje:</td>
                        <td>
                            <asp:TextBox ID="txtMensaje" runat="server"></asp:TextBox>
                        </td>
                     </tr>   
                    <tr>
                        <td>InfoResponse:</td>
                        <td>
                            <asp:TextBox ID="txtInfoResponse" runat="server"></asp:TextBox>
                        </td>
                     </tr>                           
                </table>
            </div>
        </asp:Panel>    
    </form>
</body>
</html>
