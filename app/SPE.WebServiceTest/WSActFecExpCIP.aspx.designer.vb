﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class WSActFecExpCIP

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''txtValidar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValidar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnPanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPanel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pPanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtCAPI control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCAPI As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCClave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCClave As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCIP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCIP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFechaExpiracion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaExpiracion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtInfoRequest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInfoRequest As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnActualizarCIP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnActualizarCIP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtEstado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEstado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMensaje control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMensaje As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtInfoResponse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInfoResponse As Global.System.Web.UI.WebControls.TextBox
End Class
