﻿Imports SPE.Api

Public Class WSActFecExpCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
        End If
    End Sub

    Protected Sub btnActualizarCIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarCIP.Click
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Dim proxy As New Proxys.WSCIP()
        Dim request As New Proxys.BEWSActualizaCIPRequest
        request.CAPI = txtCAPI.Text
        request.CClave = txtCClave.Text
        request.CIP = txtCIP.Text
        request.FechaExpira = txtFechaExpiracion.Text
        request.InfoRequest = clsEncripta.Encrypt(txtInfoRequest.Text, clsEncripta.fEncriptaKey)
        Try
            Dim response As Proxys.BEWSActualizaCIPResponse = proxy.ActualizarCIP(request)
            If (response IsNot Nothing) Then
                txtEstado.Text = response.Estado
                txtMensaje.Text = response.Mensaje
                txtInfoResponse.Text = clsEncripta.Decrypt(response.InfoResponse, clsEncripta.fEncriptaKey)
            End If
        Catch ex As Exception
            txtMensaje.Text = ex.Message
            Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ActualizarFechaExpiracion"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub
End Class