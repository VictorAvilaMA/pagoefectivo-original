﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSBANBIFCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSBANBIFCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
    <asp:Button ID="btnPanel" runat="server" Text="Validar" />
    <asp:Panel ID="pPanel" runat="server" Visible="false" Width="70%">
        <br />
        1) Prueba de Servicios Web de Agencias Bancarias
        <table>
            <tr>
                <td>
                    Operación:

                </td>
                <td style="width: 506px">
                    <asp:DropDownList ID="dropOperacion" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="C">Consulta</asp:ListItem>
                        <asp:ListItem Value="P">Pagar</asp:ListItem>
                        <asp:ListItem Value="E">Extornar</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <table id="tbconsultar" runat="server">
            <tr>
                <td>
                    Identificador de solicitud
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_IdSolicitud" runat="server">550e8400-e29b-41d4-a716-446655440000</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de usuario Banbif
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoUsuarioBanbif" runat="server">BANBIF</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    C&oacute;digo de usuario
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoUsuario" runat="server">C1505A</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    N&uacute;mero de referencia
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_NroReferencia" runat="server">123456</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    C&oacute;digo SBS Banbif
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoSBSBanBif" runat="server">0038</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo Canal
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoCanal" runat="server">IB</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de Concepto de cobro
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoServicio" runat="server">001</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de Entidad
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoEntidadIBS" runat="server">35215</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de deudor(CIP)
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoDeudor" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de Codigo Clasificacion
                </td>
                <td>
                    <asp:TextBox ID="txtconsultar_CodigoClasificacion" runat="server">0001</asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table id="tbpagar" visible="false" runat="server">
            <tr>
                <td>
                    Identificador de solicitud
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_RqUID" runat="server">550e8400-e29b-41d4-a716-446655440000</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de usuario Banbif
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_Login" runat="server">BANBIF</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de usuario
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_SegRol" runat="server">C1505A</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo de Operación
                </td>
                <td>
                    <asp:TextBox ID="ddl_ExtornoTp" runat="server">P</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Número de referencia
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_OpnNro" runat="server">123456</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de BanBif
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_OrgCod" runat="server">0038</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de canal de la transacción
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_ClientCod" runat="server">IB</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Cantidad de pagos
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_Ctd" runat="server">1</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de Concepto de cobro
                </td>
                <td>                   
                     <asp:TextBox ID="txtpagar_SvcId" runat="server">001</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de Entidad
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_Spcod" runat="server">35215</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de deudor
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_PagId" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Númerode recibo
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_Recibo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de clasificación
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_clasificacion" runat="server">4000</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Forma de Pago
                </td>
                <td>
                    <asp:DropDownList ID="ddlpagar_MedioAbonoCod" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="01">EFECTIVO</asp:ListItem>
                        <asp:ListItem Value="06">CARGO CUENTA BANCARIA</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Moneda del recibo
                </td>
                <td>
                    <asp:DropDownList ID="dllpagar_Moneda" runat="server">
                        <asp:ListItem Value="0001">Soles (0001)</asp:ListItem>
                        <asp:ListItem Value="1001">Dólares (1001)</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Monto total de la transacción
                </td>
                <td>
                    <asp:TextBox ID="txtpagar_Mto" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table id="tbextornar" visible="false" runat="server">
            <tr>
                <td>
                    Identificador de solicitud
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_RqUID" runat="server">550e8400-e29b-41d4-a716-446655440000</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de usuario Banbif
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_Login" runat="server">BANBIF</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de usuario
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_SegRol" runat="server">C1505A</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo de Operación
                </td>
                <td>                    
                    <asp:TextBox ID="DropDownList1" runat="server">E</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Número de referencia
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_OpnNro" runat="server">123456</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de BanBif
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_OrgCod" runat="server">0038</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codigo de canal de la transacción
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_ClientCod" runat="server">IB</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Cantidad de pagos
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_Ctd" runat="server">1</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de Concepto de cobro
                </td>
                <td>                 
                    <asp:TextBox ID="txtExtornar_SvcId" runat="server">001</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de Entidad
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_Spcod" runat="server">35215</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de deudor
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_PagId" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Númerode recibo
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_Recibo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Código de clasificación
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_clasificacion" runat="server">4000</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Forma de Pago
                </td>
                <td>
                    <asp:DropDownList ID="ddlextornar_MedioAbonoCod" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="01">EFECTIVO</asp:ListItem>
                        <asp:ListItem Value="06">CARGO CUENTA BANCARIA</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Moneda del recibo
                </td>
                <td>
                    <asp:DropDownList ID="dllextornar_Moneda" runat="server">
                        <asp:ListItem Value="0001">Soles (0001)</asp:ListItem>
                        <asp:ListItem Value="1001">Dólares (1001)</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Monto total de la transacción
                </td>
                <td>
                    <asp:TextBox ID="txtextornar_Mto" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnEjecutar" runat="server" Text="Ejecutar" />
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td>
                    Resultado:
                </td>
                <td>
                    <asp:TextBox ID="txtResultado" runat="server" TextMode="MultiLine" Height="800px"
                        Width="800px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
