﻿
Imports ProxyWSBanBif

Public Class WSBANBIFCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnEjecutar_Click(sender As Object, e As EventArgs) Handles btnEjecutar.Click
        Dim strResultado As New StringBuilder()
        Dim codMoneda As String = "" 'dropMoneda.SelectedValue.TrimEnd()
        Dim monto As String = "" ' txtMonto.Text.TrimEnd()
        Dim cip As String = txtpagar_Recibo.Text.TrimEnd()
        Dim cip2 As String = txtextornar_Recibo.Text.TrimEnd()



        'Dim zeros As String = "00000000000000"


        'Dim stri2 As String = cip2
        'cip2 = zeros.Substring(0, zeros.Length - stri2.Length) + stri2

        Dim resultado As String = ""
        Try
            'Using proxy As New ProxyWSBANBIF.Service()
            Using proxy As New ProxyWSBanBif.Service

                Select Case dropOperacion.SelectedValue

                    Case "C"

                        Dim requestConsultar As New ProxyWSBanBif.BEBanBifConsultarRequest()
                        requestConsultar.RqUID = txtconsultar_IdSolicitud.Text
                        requestConsultar.Login = txtconsultar_CodigoUsuarioBanbif.Text
                        requestConsultar.SegRol = txtconsultar_CodigoUsuario.Text

                        requestConsultar.OpnNro = txtconsultar_NroReferencia.Text
                        requestConsultar.OrgCod = txtconsultar_CodigoSBSBanBif.Text
                        requestConsultar.ClientCod = txtconsultar_CodigoCanal.Text
                        requestConsultar.SvcId = txtconsultar_CodigoServicio.Text
                        requestConsultar.Spcod = txtconsultar_CodigoEntidadIBS.Text
                        requestConsultar.PagId = txtconsultar_CodigoDeudor.Text
                        requestConsultar.Clasificacion = txtconsultar_CodigoClasificacion.Text

                        Dim responseConsultar As ProxyWSBanBif.BEBanBifConsultarResponse = proxy.consultar(requestConsultar)
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.RqUID, "RqUID"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.Login, "Login"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.SegRol, "SegRol"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.ServFec, "ServFec"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.Spcod, "Spcod"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.PagId, "PagId"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.NomCompleto, "NomCompleto"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.Clasificacion, "Clasificacion"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.Ctd, "Ctd"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.OpnNro, "OpnNro"))

                        If responseConsultar.detalle IsNot Nothing Then

                            Dim consultarDetalle As ProxyWSBanBif.BEBanBifConsultarDetalle = responseConsultar.detalle(0)

                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.Recibo, "Recibo"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.SvcId, "SvcId"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.Descripcion, "Descripcion"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.FecVct, "FecVct"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.FecEmision, "FecEmision"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.MonCod, "MonCod"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.Total, "Total"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.Ctd, "Ctd"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.DocRef, "DocRef"))



                            If consultarDetalle.subConceptos IsNot Nothing Then
                                Dim consultarSubConceptos As ProxyWSBanBif.BEBanBifConsultarSubConcepto = responseConsultar.detalle(0).subConceptos(0)

                                'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarSubConceptos.codigoSubconcepto, "codigoSubconcepto"))
                                strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarSubConceptos.MtoTp, "MtoTp"))
                                strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarSubConceptos.Mto, "Mto"))

                            End If

                        Else
                            'Detalle
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "Recibo"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "SvcId"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "Descripcion"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "FecVct"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "FecEmision"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "MonCod"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "Total"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "Ctd"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "DocRef"))
                            'Conceptos
                            'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "codigoSubconcepto"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "MtoTp"))
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "Mto"))
                        End If

                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.StatusCod, "StatusCod"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.ServStatusCod, "ServStatusCod")) 'responseConsultar.numeroOperacion = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.Severidad, "Severidad")) 'responseConsultar.codigoBanco = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.StatusDes, "StatusDes")) 'responseConsultar.codigoConvenio = ""


                        Dim caso As String = Convert.ToString(ViewState("panel"))
                        'Visualizar(caso)

                    Case "P"

                        Dim requestPagar As New ProxyWSBanBif.BEBanBifPagarRequest()
                        requestPagar.RqUID = txtpagar_RqUID.Text
                        requestPagar.Login = txtpagar_Login.Text
                        requestPagar.SegRol = txtpagar_SegRol.Text
                        requestPagar.ExtornoTp = ddl_ExtornoTp.Text

                        requestPagar.OpnNro = txtpagar_OpnNro.Text
                        requestPagar.OrgCod = txtpagar_OrgCod.Text
                        requestPagar.ClientCod = txtpagar_ClientCod.Text
                        requestPagar.Ctd = txtpagar_Ctd.Text
                        requestPagar.SvcId = txtpagar_SvcId.Text
                        requestPagar.Spcod = txtpagar_Spcod.Text
                        requestPagar.PagId = txtpagar_PagId.Text
                        requestPagar.Recibo = cip
                        'requestPagar.Recibo = txtpagar_Recibo.Text
                        requestPagar.Clasificacion = txtpagar_clasificacion.Text
                        requestPagar.MedioAbonoCod = ddlpagar_MedioAbonoCod.SelectedItem.Value
                        requestPagar.Moneda = dllpagar_Moneda.Text
                        requestPagar.Mto = txtpagar_Mto.Text

                        Dim responsePagar As ProxyWSBanBif.BEBanBifPagarResponse = proxy.pagar(requestPagar)

                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.ExtornoTp, "ExtornoTp")) 'codigoOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.RqUID, "RqUID")) 'numeroOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Login, "Login ")) 'codigoBanco()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.SegRol, "SegRol")) 'codigoConvenio()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.ServFec, "ServFec")) 'tipoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.OpnNro, "OpnNro")) 'codigoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Ctd, "Ctd")) 'numeroReferenciaDeuda()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.SvcId, "SvcId")) 'numeroOperacionEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Spcod, "Spcod")) 'referenciaDeudaAdicional()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.PagId, "PagId")) 'codigoResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Recibo, "Recibo")) 'mensajeResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Clasificacion, "Clasificacion")) 'datosEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Moneda, "Moneda")) 'datosEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Mto, "Mto")) 'datosEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.StatusCod, "StatusCod"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.ServStatusCod, "ServStatusCod")) 'responseConsultar.numeroOperacion = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.Severidad, "Severidad")) 'responseConsultar.codigoBanco = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.StatusDes, "StatusDes")) 'responseConsultar.codigoConvenio = ""

                    Case "E"

                        Dim requestExtornar As New ProxyWSBanBif.BEBanBifExtornoRequest()
                        requestExtornar.RqUID = txtextornar_RqUID.Text
                        requestExtornar.Login = txtextornar_Login.Text
                        requestExtornar.SegRol = txtextornar_SegRol.Text
                        requestExtornar.ExtornoTp = DropDownList1.Text
                        'requestExtornar.ClientFec = Today.ToString("yyyy-MM-dd") & "T" & Today.ToString("hh:mm:ss")
                        requestExtornar.OpnNro = txtextornar_OpnNro.Text
                        requestExtornar.OrgCod = txtextornar_OrgCod.Text
                        requestExtornar.ClientCod = txtextornar_ClientCod.Text
                        requestExtornar.Ctd = txtextornar_Ctd.Text
                        requestExtornar.SvcId = txtExtornar_SvcId.Text
                        requestExtornar.Spcod = txtextornar_Spcod.Text
                        requestExtornar.PagId = txtextornar_PagId.Text
                        requestExtornar.Recibo = cip2
                        requestExtornar.Clasificacion = txtextornar_clasificacion.Text
                        requestExtornar.MedioAbonoCod = ddlextornar_MedioAbonoCod.SelectedItem.Value
                        requestExtornar.Moneda = dllextornar_Moneda.Text
                        requestExtornar.Mto = txtextornar_Mto.Text

                        Dim responseExtornar As ProxyWSBanBif.BEBanBifExtornoResponse = proxy.extornar(requestExtornar)

                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.ExtornoTp, "ExtornoTp")) 'codigoOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.RqUID, "RqUID")) 'numeroOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Login, "Login ")) 'codigoBanco()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.SegRol, "SegRol")) 'codigoConvenio()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.ServFec, "ServFec")) 'tipoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.OpnNro, "OpnNro")) 'codigoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Ctd, "Ctd")) 'numeroReferenciaDeuda()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.SvcId, "SvcId")) 'numeroOperacionEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Spcod, "Spcod")) 'referenciaDeudaAdicional()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.PagId, "PagId")) 'codigoResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Recibo, "Recibo")) 'mensajeResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Clasificacion, "Clasificacion")) 'datosEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Moneda, "Moneda")) 'datosEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Mto, "Mto")) 'datosEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.StatusCod, "StatusCod"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.ServStatusCod, "ServStatusCod")) 'responseConsultar.numeroOperacion = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.Severidad, "Severidad")) 'responseConsultar.codigoBanco = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.StatusDes, "StatusDes")) 'responseConsultar.codigoConvenio = ""

                    Case "EA"

                        'Dim requestExtornar As New ProxyWSBBVA.BEBBVAExtornarRequest()
                        'requestExtornar.codigoOperacion = txtext_codigoOperacion.Text
                        'requestExtornar.numeroOperacion = txtext_numeroOperacion.Text
                        'requestExtornar.codigoBanco = txtext_codigoBanco.Text
                        'requestExtornar.codigoConvenio = txtext_codigoConvenio.Text
                        'requestExtornar.cuentaRecaudadora = txtext_cuentaRecaudadora.Text
                        'requestExtornar.tipoCliente = txtext_tipoCliente.Text
                        'requestExtornar.codigoCliente = txtext_codigoCliente.Text
                        'requestExtornar.numeroReferenciaDeuda = txtext_numeroReferenciaDeuda.Text
                        'requestExtornar.cantidadDocumentos = txtext_cantidadDocumentos.Text
                        'requestExtornar.formaPago = txtext_formaPago.Text
                        'requestExtornar.codigoMoneda = txtext_codigoMoneda.Text
                        'requestExtornar.importeTotalPagado = txtext_importeTotalPagado.Text
                        'requestExtornar.numeroOperacionOriginal = txtext_numeroOperacionOriginal.Text
                        'requestExtornar.fechaOperacionOriginal = txtext_fechaOperacionOriginal.Text
                        'requestExtornar.referenciaDeudaAdicional = txtext_referenciaDeudaAdicional.Text

                        'Dim listaextornarDetalle As New List(Of ProxyWSBBVA.BEBBVAExtornarDetalle)
                        'Dim extornarDetalle As New ProxyWSBBVA.BEBBVAExtornarDetalle()
                        'extornarDetalle.numeroReferenciaDocumento = txtext_det_numeroReferenciaDocumento.Text
                        'extornarDetalle.descripcionDocumento = txtext_det_descripcionDocumento.Text
                        'extornarDetalle.numeroOperacionBanco = txtext_det_numeroOperacionBanco.Text
                        'extornarDetalle.importeDeudaPagada = txtext_det_importeDeudaPagada.Text
                        'extornarDetalle.referenciaPagoAdicional = txtext_det_referenciaPagoAdicional.Text
                        'listaextornarDetalle.Add(extornarDetalle)
                        'requestExtornar.detalle = listaextornarDetalle.ToArray()

                        'requestExtornar.canalOperacion = txtext_canalOperacion.Text
                        'requestExtornar.codigoOficina = txtext_codigoOficina.Text
                        'requestExtornar.fechaOperacion = txtext_fechaOperacion.Text
                        'requestExtornar.horaOperacion = txtext_horaOperacion.Text
                        'requestExtornar.datosEmpresa = txtext_datosEmpresa.Text

                        'Dim responseExtornar As ProxyWSBBVA.BEBBVAExtornarResponse = proxy.extornar(requestExtornar)

                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoOperacion, "codigoOperacion")) ' codigoOperacion()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.numeroOperacion, "numeroOperacion")) 'numeroOperacion()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoBanco, "codigoBanco")) 'codigoBanco()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoConvenio, "codigoConvenio")) 'codigoConvenio()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.tipoCliente, "tipoCliente")) 'tipoCliente()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoCliente, "codigoCliente")) 'codigoCliente()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.numeroReferenciaDeuda, "numeroReferenciaDeuda")) 'numeroReferenciaDeuda()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.numeroOperacionEmpresa, "numeroOperacionEmpresa")) 'numeroOperacionEmpresa()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.referenciaDeudaAdicional, "referenciaDeudaAdicional")) 'referenciaDeudaAdicional()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoResultado, "codigoResultado")) 'codigoResultado()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.mensajeResultado, "mensajeResultado")) 'mensajeResultado()
                        'strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.datosEmpresa, "datosEmpresa")) 'datosEmpresa()

                        'Dim caso As String = Convert.ToString(ViewState("panel"))
                        'Visualizar(caso)
                End Select
                'resultado = proxy.WSAGOperacion(cip, "001", codMoneda, "010", "001", "1", cip, monto, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", monto, dropOperacion.SelectedValue)
            End Using
            'txtResultado.Text = resultado
            txtResultado.Text = strResultado.ToString()
        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Protected Sub dropOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dropOperacion.SelectedIndexChanged
        If dropOperacion.SelectedItem.Value = "C" Then
            tbconsultar.Visible = True
            tbpagar.Visible = False
            tbextornar.Visible = False
        ElseIf dropOperacion.SelectedItem.Value = "P" Then
            tbconsultar.Visible = False
            tbpagar.Visible = True
            tbextornar.Visible = False
        ElseIf dropOperacion.SelectedItem.Value = "E" Then
            tbconsultar.Visible = False
            tbpagar.Visible = False
            tbextornar.Visible = True
        End If
        txtResultado.Text = ""
    End Sub


    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSBANBIFCIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub
End Class