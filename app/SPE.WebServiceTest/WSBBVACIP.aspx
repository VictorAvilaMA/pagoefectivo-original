<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSBBVACIP.aspx.vb" Inherits="SPE.WebServiceTest.WSBBVACIP" ValidateRequest="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
      <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
      <asp:Button ID="btnPanel" runat="server"  Text="Validar" />   
      <asp:Panel ID="pPanel" runat="server" Visible="false" Width="70%">  
            <div>
            <br />
          1)  Prueba de Servicios Web de Agencias Bancarias
            <table>      
             <tr>
                <td>Operación:</td>
                <td style="width: 506px">
                    <asp:DropDownList ID="dropOperacion" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="1010">Consulta (1010)</asp:ListItem>
                        <asp:ListItem Value="2010">Pagar (2010)</asp:ListItem>
                        <asp:ListItem Value="3010">Anular (3010)</asp:ListItem>
                        <asp:ListItem Value="4010">Extorno Automatico (4010)</asp:ListItem>
                    </asp:DropDownList>
                </td>
             </tr>                                     
            </table>
                
                <br />
                <table id="tbconsultar" runat="server">
                <tr>
                    <td>codigoOperacion</td> <td><asp:TextBox ID="txtconsultar_codigooperacion" runat="server">1010</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroOperacion</td> <td><asp:TextBox ID="txtconsultar_numeroOperacion" runat="server">0000008501</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoBanco</td> <td><asp:TextBox ID="txtconsultar_codigoBanco" runat="server">0011</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoConvenio</td> <td><asp:TextBox ID="txtconsultar_codigoConvenio" runat="server">0001003</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>tipoCliente</td> <td><asp:TextBox ID="txtconsultar_tipoCliente" runat="server">00</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoCliente</td> <td><asp:TextBox ID="txtconsultar_codigoCliente" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroReferenciaDeuda</td> <td><asp:TextBox ID="txtconsultar_numeroReferenciaDeuda" runat="server">00000001025248</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>referenciaDeudaAdicional</td> <td><asp:TextBox ID="txtconsultar_referenciaDeudaAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>canalOperacion </td> <td><asp:TextBox ID="txtconsultar_canalOperacion" runat="server">TF</asp:TextBox> </td>
                </tr>                                
                <tr>
                    <td>codigoOficina </td> <td><asp:TextBox ID="txtconsultar_codigoOficina" runat="server">0130</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>fechaOperacion </td> <td><asp:TextBox ID="txtconsultar_fechaOperacion" runat="server">2011-06-27</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>horaOperacion  </td> <td><asp:TextBox ID="txtconsultar_horaOperacion" runat="server">123736</asp:TextBox> </td>
                </tr>                                                                 
                <tr>
                    <td>datosEmpresa  </td> <td><asp:TextBox ID="txtconsultar_datosEmpresa" runat="server"></asp:TextBox> </td>
                </tr>                                                
                </table>                                                 
              
                <table id="tbpagar" runat="server">
                <tr>
                    <td>codigoOperacion</td> <td><asp:TextBox ID="txtpagar_codigooperacion" runat="server">2010</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroOperacion</td> <td><asp:TextBox ID="txtpagar_numeroOperacion" runat="server">0000008501</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoBanco</td> <td><asp:TextBox ID="txtpagar_codigoBanco" runat="server">0011</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoConvenio</td> <td><asp:TextBox ID="txtpagar_codigoConvenio" runat="server">0001003</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>cuentaRecaudadora</td> <td><asp:TextBox ID="txtpagar_cuentaRecaudadora" runat="server">111-222-333</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>tipoCliente</td> <td><asp:TextBox ID="txtpagar_tipoCliente" runat="server">00</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoCliente</td> <td><asp:TextBox ID="txtpagar_codigoCliente" runat="server">41768292</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroReferenciaDeuda</td> <td><asp:TextBox ID="txtpagar_numeroReferenciaDeuda" runat="server">00000001025248</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>cantidadDocumentos</td> <td><asp:TextBox ID="txtpagar_cantidadDocumentos" runat="server">01</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>formaPago</td> <td><asp:TextBox ID="txtpagar_formaPago" runat="server">EF</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoMoneda</td> <td><asp:TextBox ID="txtpagar_codigoMoneda" runat="server">PEN</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>importeTotalPagado</td> <td><asp:TextBox ID="txtpagar_importeTotalPagado" runat="server">4000</asp:TextBox> </td>
                </tr>
                <%--<tr>
                    <td>numeroOperacionOriginal</td> <td><asp:TextBox ID="txtpagar_numeroOperacionOriginal" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>fechaOperacionOriginal </td> <td><asp:TextBox ID="txtpagar_fechaOperacionOriginal" runat="server"></asp:TextBox> </td>
                </tr>--%>
                <tr>
                    <td>referenciaDeudaAdicional</td> <td><asp:TextBox ID="txtpagar_referenciaDeudaAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.numeroReferenciaDocumento</td> <td><asp:TextBox ID="txtpagar_det_numeroReferenciaDocumento" runat="server">00000001025248</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.descripcionDocumento </td> <td><asp:TextBox ID="txtpagar_det_descripcionDocumento" runat="server">Pago desde Urbania</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.numeroOperacionBanco</td> <td><asp:TextBox ID="txtpagar_det_numeroOperacionBanco" runat="server">0000000159</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.importeDeudaPagada </td> <td><asp:TextBox ID="txtpagar_det_importeDeudaPagada" runat="server">4000</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.referenciaPagoAdicional</td> <td><asp:TextBox ID="txtpagar_det_referenciaPagoAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>canalOperacion </td> <td><asp:TextBox ID="txtpagar_canalOperacion" runat="server">TF</asp:TextBox> </td>
                </tr>                                
                <tr>
                    <td>codigoOficina </td> <td><asp:TextBox ID="txtpagar_codigoOficina" runat="server">0130</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>fechaOperacion </td> <td><asp:TextBox ID="txtpagar_fechaOperacion" runat="server">2011-06-27</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>horaOperacion  </td> <td><asp:TextBox ID="txtpagar_horaOperacion" runat="server">123736</asp:TextBox> </td>
                </tr>                                                                 
                <tr>
                    <td>datosEmpresa  </td> <td><asp:TextBox ID="txtpagar_datosEmpresa" runat="server"></asp:TextBox> </td>
                </tr>                                                
                </table>
                
                <table id="tbanular" runat="server">
                <tr>
                    <td>codigoOperacion</td> <td><asp:TextBox ID="txtanular_codigoOperacion" runat="server">3010</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroOperacion</td> <td><asp:TextBox ID="txtanular_numeroOperacion" runat="server">0000008501</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoBanco</td> <td><asp:TextBox ID="txtanular_codigoBanco" runat="server">0011</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoConvenio</td> <td><asp:TextBox ID="txtanular_codigoConvenio" runat="server">0001003</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>cuentaRecaudadora</td> <td><asp:TextBox ID="txtanular_cuentaRecaudadora" runat="server">111-222-333</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>tipoCliente</td> <td><asp:TextBox ID="txtanular_tipoCliente" runat="server">00</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoCliente</td> <td><asp:TextBox ID="txtanular_codigoCliente" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroReferenciaDeuda</td> <td><asp:TextBox ID="txtanular_numeroReferenciaDeuda" runat="server">00000001025248</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>cantidadDocumentos</td> <td><asp:TextBox ID="txtanular_cantidadDocumentos" runat="server">01</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>formaPago</td> <td><asp:TextBox ID="txtanular_formaPago" runat="server">EF</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoMoneda</td> <td><asp:TextBox ID="txtanular_codigoMoneda" runat="server">PEN</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>importeTotalPagado</td> <td><asp:TextBox ID="txtanular_importeTotalPagado" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroOperacionOriginal</td> <td><asp:TextBox ID="txtanular_numeroOperacionOriginal" runat="server">0000000159</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>fechaOperacionOriginal </td> <td><asp:TextBox ID="txtanular_fechaOperacionOriginal" runat="server">2011-06-27</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>referenciaDeudaAdicional</td> <td><asp:TextBox ID="txtanular_referenciaDeudaAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.numeroReferenciaDocumento</td> <td><asp:TextBox ID="txtanular_det_numeroReferenciaDocumento" runat="server">00000001025248</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.descripcionDocumento </td> <td><asp:TextBox ID="txtanular_det_descripcionDocumento" runat="server">Pago desde Urbania</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.numeroOperacionBanco</td> <td><asp:TextBox ID="txtanular_det_numeroOperacionBanco" runat="server">0000000199</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.importeDeudaPagada </td> <td><asp:TextBox ID="txtanular_det_importeDeudaPagada" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.referenciaPagoAdicional</td> <td><asp:TextBox ID="txtanular_det_referenciaPagoAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>canalOperacion </td> <td><asp:TextBox ID="txtanular_canalOperacion" runat="server">TF</asp:TextBox> </td>
                </tr>                                
                <tr>
                    <td>codigoOficina </td> <td><asp:TextBox ID="txtanular_codigoOficina" runat="server">0130</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>fechaOperacion </td> <td><asp:TextBox ID="txtanular_fechaOperacion" runat="server">2011-06-27</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>horaOperacion  </td> <td><asp:TextBox ID="txtanular_horaOperacion" runat="server">123736</asp:TextBox> </td>
                </tr>                                                                 
                <tr>
                    <td>datosEmpresa  </td> <td><asp:TextBox ID="txtanular_datosEmpresa" runat="server"></asp:TextBox> </td>
                </tr>                                                
                </table>                              
                
                <table id="tbextornar" runat="server">
                <tr>
                    <td>codigoOperacion</td> <td><asp:TextBox ID="txtext_codigoOperacion" runat="server">4010</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroOperacion</td> <td><asp:TextBox ID="txtext_numeroOperacion" runat="server">100</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoBanco</td> <td><asp:TextBox ID="txtext_codigoBanco" runat="server">03</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoConvenio</td> <td><asp:TextBox ID="txtext_codigoConvenio" runat="server">748</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>cuentaRecaudadora</td> <td><asp:TextBox ID="txtext_cuentaRecaudadora" runat="server">333-222-111</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>tipoCliente</td> <td><asp:TextBox ID="txtext_tipoCliente" runat="server">DNI</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoCliente</td> <td><asp:TextBox ID="txtext_codigoCliente" runat="server">41768292</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroReferenciaDeuda</td> <td><asp:TextBox ID="txtext_numeroReferenciaDeuda" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>cantidadDocumentos</td> <td><asp:TextBox ID="txtext_cantidadDocumentos" runat="server">1</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>formaPago</td> <td><asp:TextBox ID="txtext_formaPago" runat="server">EF</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>codigoMoneda</td> <td><asp:TextBox ID="txtext_codigoMoneda" runat="server">PEN</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>importeTotalPagado</td> <td><asp:TextBox ID="txtext_importeTotalPagado" runat="server">22</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>numeroOperacionOriginal</td> <td><asp:TextBox ID="txtext_numeroOperacionOriginal" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>fechaOperacionOriginal </td> <td><asp:TextBox ID="txtext_fechaOperacionOriginal" runat="server">2011-06-27</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>referenciaDeudaAdicional</td> <td><asp:TextBox ID="txtext_referenciaDeudaAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.numeroReferenciaDocumento</td> <td><asp:TextBox ID="txtext_det_numeroReferenciaDocumento" runat="server">00000001025248</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.descripcionDocumento </td> <td><asp:TextBox ID="txtext_det_descripcionDocumento" runat="server">Pago desde Urbania</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.numeroOperacionBanco</td> <td><asp:TextBox ID="txtext_det_numeroOperacionBanco" runat="server">199</asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.importeDeudaPagada </td> <td><asp:TextBox ID="txtext_det_importeDeudaPagada" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>detalle.referenciaPagoAdicional</td> <td><asp:TextBox ID="txtext_det_referenciaPagoAdicional" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td>canalOperacion </td> <td><asp:TextBox ID="txtext_canalOperacion" runat="server">TF</asp:TextBox> </td>
                </tr>                                
                <tr>
                    <td>codigoOficina </td> <td><asp:TextBox ID="txtext_codigoOficina" runat="server">0130</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>fechaOperacion </td> <td><asp:TextBox ID="txtext_fechaOperacion" runat="server">2011-06-27</asp:TextBox> </td>
                </tr>                 
                <tr>
                    <td>horaOperacion  </td> <td><asp:TextBox ID="txtext_horaOperacion" runat="server">123736</asp:TextBox> </td>
                </tr>                                                                 
                <tr>
                    <td style="height: 26px">datosEmpresa  </td> <td style="height: 26px"><asp:TextBox ID="txtext_datosEmpresa" runat="server"></asp:TextBox> </td>
                </tr>                                                
                </table>
                
                <table>
                <tr>
                    <td><asp:Button ID="btnEjecutar" runat="server" Text="Ejecutar" /></td>
                </tr>
                </table>                
                
                <table width="100%">
                <tr>
                    <td>Resultado:</td>
                    <td>
                        <asp:TextBox ID="txtResultado" runat="server" TextMode="MultiLine" Height="800px" Width="800px"></asp:TextBox>
                    </td>
                </tr>
                   
                </table>
             </div>    
    </asp:Panel>    
    </form>
</body>
</html>
