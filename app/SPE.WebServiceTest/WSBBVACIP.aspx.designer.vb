'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3615
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On



'''<summary>
'''WSBBVACIP class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class WSBBVACIP

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''txtValidar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtValidar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnPanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPanel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pPanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pPanel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dropOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dropOperacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''tbconsultar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbconsultar As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''txtconsultar_codigooperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_codigooperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_numeroOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_numeroOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_codigoBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_codigoBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_codigoConvenio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_codigoConvenio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_tipoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_tipoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_codigoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_codigoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_numeroReferenciaDeuda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_numeroReferenciaDeuda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_referenciaDeudaAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_referenciaDeudaAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_canalOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_canalOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_codigoOficina control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_codigoOficina As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_fechaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_fechaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_horaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_horaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtconsultar_datosEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtconsultar_datosEmpresa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tbpagar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbpagar As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''txtpagar_codigooperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_codigooperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_numeroOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_numeroOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_codigoBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_codigoBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_codigoConvenio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_codigoConvenio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_cuentaRecaudadora control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_cuentaRecaudadora As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_tipoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_tipoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_codigoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_codigoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_numeroReferenciaDeuda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_numeroReferenciaDeuda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_cantidadDocumentos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_cantidadDocumentos As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_formaPago control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_formaPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_codigoMoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_codigoMoneda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_importeTotalPagado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_importeTotalPagado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_referenciaDeudaAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_referenciaDeudaAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_det_numeroReferenciaDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_det_numeroReferenciaDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_det_descripcionDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_det_descripcionDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_det_numeroOperacionBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_det_numeroOperacionBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_det_importeDeudaPagada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_det_importeDeudaPagada As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_det_referenciaPagoAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_det_referenciaPagoAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_canalOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_canalOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_codigoOficina control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_codigoOficina As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_fechaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_fechaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_horaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_horaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtpagar_datosEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtpagar_datosEmpresa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tbanular control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbanular As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''txtanular_codigoOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_codigoOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_numeroOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_numeroOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_codigoBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_codigoBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_codigoConvenio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_codigoConvenio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_cuentaRecaudadora control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_cuentaRecaudadora As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_tipoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_tipoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_codigoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_codigoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_numeroReferenciaDeuda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_numeroReferenciaDeuda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_cantidadDocumentos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_cantidadDocumentos As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_formaPago control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_formaPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_codigoMoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_codigoMoneda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_importeTotalPagado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_importeTotalPagado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_numeroOperacionOriginal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_numeroOperacionOriginal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_fechaOperacionOriginal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_fechaOperacionOriginal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_referenciaDeudaAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_referenciaDeudaAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_det_numeroReferenciaDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_det_numeroReferenciaDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_det_descripcionDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_det_descripcionDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_det_numeroOperacionBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_det_numeroOperacionBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_det_importeDeudaPagada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_det_importeDeudaPagada As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_det_referenciaPagoAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_det_referenciaPagoAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_canalOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_canalOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_codigoOficina control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_codigoOficina As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_fechaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_fechaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_horaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_horaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtanular_datosEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtanular_datosEmpresa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tbextornar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbextornar As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''txtext_codigoOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_codigoOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_numeroOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_numeroOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_codigoBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_codigoBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_codigoConvenio control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_codigoConvenio As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_cuentaRecaudadora control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_cuentaRecaudadora As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_tipoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_tipoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_codigoCliente control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_codigoCliente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_numeroReferenciaDeuda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_numeroReferenciaDeuda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_cantidadDocumentos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_cantidadDocumentos As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_formaPago control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_formaPago As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_codigoMoneda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_codigoMoneda As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_importeTotalPagado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_importeTotalPagado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_numeroOperacionOriginal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_numeroOperacionOriginal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_fechaOperacionOriginal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_fechaOperacionOriginal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_referenciaDeudaAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_referenciaDeudaAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_det_numeroReferenciaDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_det_numeroReferenciaDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_det_descripcionDocumento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_det_descripcionDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_det_numeroOperacionBanco control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_det_numeroOperacionBanco As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_det_importeDeudaPagada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_det_importeDeudaPagada As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_det_referenciaPagoAdicional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_det_referenciaPagoAdicional As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_canalOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_canalOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_codigoOficina control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_codigoOficina As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_fechaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_fechaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_horaOperacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_horaOperacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtext_datosEmpresa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtext_datosEmpresa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnEjecutar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEjecutar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtResultado control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtResultado As Global.System.Web.UI.WebControls.TextBox
End Class
