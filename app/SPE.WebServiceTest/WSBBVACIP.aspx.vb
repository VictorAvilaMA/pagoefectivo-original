Imports ProxyWSBBVA

Partial Public Class WSBBVACIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            tbconsultar.Visible = True
            dropOperacion.SelectedValue = "1010"
            ViewState("panel") = "1010"
            Dim caso As String = Convert.ToString(ViewState("panel"))
            Visualizar(caso)
        End If
        GenerarRandom()
    End Sub

    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSBBVACIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        'Dim flag As Boolean = True

        pPanel.Visible = flag

        Inittablas()
        'tbextornar.Visible = True
        'dropOperacion.SelectedValue = "4010"

    End Sub

    Protected Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
        Dim strResultado As New StringBuilder()
        Dim codMoneda As String = "" 'dropMoneda.SelectedValue.TrimEnd()
        Dim monto As String = "" ' txtMonto.Text.TrimEnd()
        Dim cip As String = "" ' txtCIP.Text.TrimEnd()
        Dim zeros As String = "00000000000000"

        Dim stri As String = cip
        cip = zeros.Substring(0, zeros.Length - stri.Length) + stri
        Dim resultado As String = ""
        Try
            Using proxy As New ProxyWSBBVA.Service()
                
                Select Case dropOperacion.SelectedValue

                    Case "1010"

                        Dim requestConsultar As New ProxyWSBBVA.BEBBVAConsultarRequest()
                        requestConsultar.codigoOperacion = txtconsultar_codigooperacion.Text
                        requestConsultar.numeroOperacion = txtconsultar_numeroOperacion.Text
                        requestConsultar.codigoBanco = txtconsultar_codigoBanco.Text
                        requestConsultar.codigoConvenio = txtconsultar_codigoConvenio.Text
                        requestConsultar.tipocliente = txtconsultar_tipoCliente.Text
                        requestConsultar.codigocliente = txtconsultar_codigoCliente.Text
                        requestConsultar.numeroReferenciaDeuda = txtconsultar_numeroReferenciaDeuda.Text
                        requestConsultar.referenciaDeudaAdicional = txtconsultar_referenciaDeudaAdicional.Text
                        requestConsultar.canalOperacion = txtconsultar_canalOperacion.Text
                        requestConsultar.codigoOficina = txtconsultar_codigoOficina.Text
                        requestConsultar.fechaOperacion = txtconsultar_fechaOperacion.Text
                        requestConsultar.horaOperacion = txtconsultar_horaOperacion.Text
                        requestConsultar.datosEmpresa = txtconsultar_datosEmpresa.Text

                        Dim responseConsultar As ProxyWSBBVA.BEBBVAConsultarResponse = proxy.consultar(requestConsultar)
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.codigoOperacion, "codigoOperacion"))
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.numeroOperacion, "numeroOperacion")) 'responseConsultar.numeroOperacion = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.codigoBanco, "codigoBanco")) 'responseConsultar.codigoBanco = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.codigoConvenio, "codigoConvenio")) 'responseConsultar.codigoConvenio = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.tipoCliente, "tipoCliente")) 'responseConsultar.tipoCliente = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.codigoCliente, "codigoCliente")) 'responseConsultar.codigoCliente = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.datoCliente, "datoCliente")) 'responseConsultar.datoCliente = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.numeroReferenciaDeuda, "numeroReferenciaDeuda")) 'responseConsultar.numeroReferenciaDeuda = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.cantidadDocumentos, "cantidadDocumentos")) 'responseConsultar.cantidadDocumentos = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.codigoMoneda, "codigoMoneda")) 'responseConsultar.codigoMoneda = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.numeroOperacionEmpresa, "numeroOperacionEmpresa")) 'responseConsultar.numeroOperacionEmpresa = ""
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.referenciaDeudaAdicional, "referenciaDeudaAdicional")) 'responseConsultar.referenciaDeudaAdicional = ""



                        If responseConsultar.detalle IsNot Nothing Then

                            Dim consultarDetalle As ProxyWSBBVA.BEBBVAConsultarDetalle = responseConsultar.detalle(0)

                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.numeroReferenciaDocumento, "numeroReferenciaDocumento")) 'consultarDetalle.numeroReferenciaDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.importeDeudaDocumento, "importeDeudaDocumento")) 'consultarDetalle.importeDeudaDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.importeDeudaMinimaDocumento, "importeDeudaMinimaDocumento")) 'consultarDetalle.importeDeudaMinimaDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.fechaVencimientoDocumento, "fechaVencimientoDocumento")) 'consultarDetalle.fechaVencimientoDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.fechaEmisionDocumento, "fechaEmisionDocumento")) 'consultarDetalle.fechaEmisionDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.descripcionDocumento, "descripcionDocumento")) 'consultarDetalle.descripcionDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.numeroDocumento, "numeroDocumento")) 'consultarDetalle.numeroDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.indicadorRestriccPago, "indicadorRestriccPago")) 'consultarDetalle.indicadorRestriccPago = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.indicadorSituacionDocumento, "indicadorSituacionDocumento")) 'consultarDetalle.indicadorSituacionDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", consultarDetalle.cantidadSubconceptos, "cantidadSubconceptos")) 'consultarDetalle.cantidadSubconceptos = "0"
                        Else
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "numeroReferenciaDocumento")) 'consultarDetalle.numeroReferenciaDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "importeDeudaDocumento")) 'consultarDetalle.importeDeudaDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "importeDeudaMinimaDocumento")) 'consultarDetalle.importeDeudaMinimaDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "fechaVencimientoDocumento")) 'consultarDetalle.fechaVencimientoDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "fechaEmisionDocumento")) 'consultarDetalle.fechaEmisionDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "descripcionDocumento")) 'consultarDetalle.descripcionDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "numeroDocumento")) 'consultarDetalle.numeroDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "indicadorRestriccPago")) 'consultarDetalle.indicadorRestriccPago = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "indicadorSituacionDocumento")) 'consultarDetalle.indicadorSituacionDocumento = ""
                            strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", "", "cantidadSubconceptos")) 'consultarDetalle.cantidadSubconceptos = "0"

                        End If

                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.codigoResultado, "codigoResultado")) ' codigoResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.mensajeResultado, "mensajeResultado")) 'mensajeResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseConsultar.datosEmpresa, "datosEmpresa")) 'DatosEmpresa()


                        Dim caso As String = Convert.ToString(ViewState("panel"))
                        Visualizar(caso)

                    Case "2010"

                        Dim requestPagar As New ProxyWSBBVA.BEBBVAPagarRequest()
                        requestPagar.codigoOperacion = txtpagar_codigooperacion.Text
                        requestPagar.numeroOperacion = txtpagar_numeroOperacion.Text
                        requestPagar.codigoBanco = txtpagar_codigoBanco.Text
                        requestPagar.codigoConvenio = txtpagar_codigoConvenio.Text
                        requestPagar.cuentaRecaudadora = txtpagar_cuentaRecaudadora.Text
                        requestPagar.tipoCliente = txtpagar_tipoCliente.Text
                        requestPagar.codigoCliente = txtpagar_codigoCliente.Text
                        requestPagar.numeroReferenciaDeuda = txtpagar_numeroReferenciaDeuda.Text
                        requestPagar.cantidadDocumentos = txtpagar_cantidadDocumentos.Text
                        requestPagar.formaPago = txtpagar_formaPago.Text
                        requestPagar.codigoMoneda = txtpagar_codigoMoneda.Text
                        requestPagar.importeTotalPagado = txtpagar_importeTotalPagado.Text
                        requestPagar.referenciaDeudaAdicional = txtpagar_referenciaDeudaAdicional.Text

                        Dim listapagarDetalle As New List(Of ProxyWSBBVA.BEBBVAPagarDetalle)
                        Dim pagarDetalle As New ProxyWSBBVA.BEBBVAPagarDetalle()
                        pagarDetalle.numeroReferenciaDocumento = txtpagar_det_numeroReferenciaDocumento.Text
                        pagarDetalle.descripcionDocumento = txtpagar_det_descripcionDocumento.Text
                        pagarDetalle.numeroOperacionBanco = txtpagar_det_numeroOperacionBanco.Text
                        pagarDetalle.importeDeudaPagada = txtpagar_det_importeDeudaPagada.Text
                        pagarDetalle.referenciaPagoAdicional = txtpagar_det_referenciaPagoAdicional.Text
                        listapagarDetalle.Add(pagarDetalle)
                        requestPagar.detalle = listapagarDetalle.ToArray()

                        requestPagar.canalOperacion = txtpagar_canalOperacion.Text
                        requestPagar.codigoOficina = txtpagar_codigoOficina.Text
                        requestPagar.fechaOperacion = txtpagar_fechaOperacion.Text
                        requestPagar.horaOperacion = txtpagar_horaOperacion.Text
                        requestPagar.datosEmpresa = txtpagar_datosEmpresa.Text
                        Dim responsePagar As ProxyWSBBVA.BEBBVAPagarResponse = proxy.pagar(requestPagar)

                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.codigoOperacion, "codigoOperacion")) 'codigoOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.numeroOperacion, "numeroOperacion")) 'numeroOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.codigoBanco, "codigoBanco")) 'codigoBanco()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.codigoConvenio, "codigoConvenio")) 'codigoConvenio()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.tipoCliente, "tipoCliente")) 'tipoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.codigoCliente, "codigoCliente")) 'codigoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.numeroReferenciaDeuda, "numeroReferenciaDeuda")) 'numeroReferenciaDeuda()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.numeroOperacionEmpresa, "numeroOperacionEmpresa")) 'numeroOperacionEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.referenciaDeudaAdicional, "referenciaDeudaAdicional")) 'referenciaDeudaAdicional()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.codigoResultado, "codigoResultado")) 'codigoResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.mensajeResultado, "mensajeResultado")) 'mensajeResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responsePagar.datosEmpresa, "datosEmpresa")) 'datosEmpresa()

                        Dim caso As String = Convert.ToString(ViewState("panel"))
                        Visualizar(caso)

                    Case "3010"

                        Dim requestAnular As New ProxyWSBBVA.BEBBVAAnularRequest()
                        requestAnular.codigoOperacion = txtanular_codigoOperacion.Text
                        requestAnular.numeroOperacion = txtanular_numeroOperacion.Text
                        requestAnular.codigoBanco = txtanular_codigoBanco.Text
                        requestAnular.codigoConvenio = txtanular_codigoConvenio.Text
                        requestAnular.cuentaRecaudadora = txtanular_cuentaRecaudadora.Text
                        requestAnular.tipoCliente = txtanular_tipoCliente.Text
                        requestAnular.codigoCliente = txtanular_codigoCliente.Text
                        requestAnular.numeroReferenciaDeuda = txtanular_numeroReferenciaDeuda.Text
                        requestAnular.cantidadDocumentos = txtanular_cantidadDocumentos.Text
                        requestAnular.formaPago = txtanular_formaPago.Text
                        requestAnular.codigoMoneda = txtanular_codigoMoneda.Text
                        requestAnular.importeTotalPagado = txtanular_importeTotalPagado.Text
                        requestAnular.numeroOperacionOriginal = txtanular_numeroOperacionOriginal.Text
                        requestAnular.fechaOperacionOriginal = txtanular_fechaOperacionOriginal.Text
                        requestAnular.referenciaDeudaAdicional = txtanular_referenciaDeudaAdicional.Text

                        Dim listaanularDetalle As New List(Of ProxyWSBBVA.BEBBVAAnularDetalle)
                        Dim anularDetalle As New ProxyWSBBVA.BEBBVAAnularDetalle()
                        anularDetalle.numeroReferenciaDocumento = txtanular_det_numeroReferenciaDocumento.Text
                        anularDetalle.descripcionDocumento = txtanular_det_descripcionDocumento.Text
                        anularDetalle.numeroOperacionBanco = txtanular_det_numeroOperacionBanco.Text
                        anularDetalle.importeDeudaPagada = txtanular_det_importeDeudaPagada.Text
                        anularDetalle.referenciaPagoAdicional = txtanular_det_referenciaPagoAdicional.Text
                        listaanularDetalle.Add(anularDetalle)
                        requestAnular.detalle = listaanularDetalle.ToArray()

                        requestAnular.canalOperacion = txtanular_canalOperacion.Text
                        requestAnular.codigoOficina = txtanular_codigoOficina.Text
                        requestAnular.fechaOperacion = txtanular_fechaOperacion.Text
                        requestAnular.horaOperacion = txtanular_horaOperacion.Text
                        requestAnular.datosEmpresa = txtanular_datosEmpresa.Text

                        Dim responseAnular As ProxyWSBBVA.BEBBVAAnularResponse = proxy.anular(requestAnular)
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.codigoOperacion, "codigoOperacion")) 'codigoOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.numeroOperacion, "numeroOperacion")) 'numeroOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.codigoBanco, "codigoBanco")) 'codigoBanco()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.codigoConvenio, "codigoConvenio")) 'codigoConvenio()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.tipoCliente, "tipoCliente")) 'tipoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.codigoCliente, "codigoCliente")) 'codigoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.numeroReferenciaDeuda, "numeroReferenciaDeuda")) 'numeroReferenciaDeuda()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.numeroOperacionEmpresa, "numeroOperacionEmpresa")) 'numeroOperacionEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.referenciaDeudaAdicional, "referenciaDeudaAdicional")) 'referenciaDeudaAdicional()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.codigoResultado, "codigoResultado")) 'codigoResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.mensajeResultado, "mensajeResultado")) 'mensajeResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseAnular.datosEmpresa, "datosEmpresa")) 'datosEmpresa()

                        Dim caso As String = Convert.ToString(ViewState("panel"))
                        Visualizar(caso)

                    Case "4010"

                        Dim requestExtornar As New ProxyWSBBVA.BEBBVAExtornarRequest()
                        requestExtornar.codigoOperacion = txtext_codigoOperacion.Text
                        requestExtornar.numeroOperacion = txtext_numeroOperacion.Text
                        requestExtornar.codigoBanco = txtext_codigoBanco.Text
                        requestExtornar.codigoConvenio = txtext_codigoConvenio.Text
                        requestExtornar.cuentaRecaudadora = txtext_cuentaRecaudadora.Text
                        requestExtornar.tipoCliente = txtext_tipoCliente.Text
                        requestExtornar.codigoCliente = txtext_codigoCliente.Text
                        requestExtornar.numeroReferenciaDeuda = txtext_numeroReferenciaDeuda.Text
                        requestExtornar.cantidadDocumentos = txtext_cantidadDocumentos.Text
                        requestExtornar.formaPago = txtext_formaPago.Text
                        requestExtornar.codigoMoneda = txtext_codigoMoneda.Text
                        requestExtornar.importeTotalPagado = txtext_importeTotalPagado.Text
                        requestExtornar.numeroOperacionOriginal = txtext_numeroOperacionOriginal.Text
                        requestExtornar.fechaOperacionOriginal = txtext_fechaOperacionOriginal.Text
                        requestExtornar.referenciaDeudaAdicional = txtext_referenciaDeudaAdicional.Text

                        Dim listaextornarDetalle As New List(Of ProxyWSBBVA.BEBBVAExtornarDetalle)
                        Dim extornarDetalle As New ProxyWSBBVA.BEBBVAExtornarDetalle()
                        extornarDetalle.numeroReferenciaDocumento = txtext_det_numeroReferenciaDocumento.Text
                        extornarDetalle.descripcionDocumento = txtext_det_descripcionDocumento.Text
                        extornarDetalle.numeroOperacionBanco = txtext_det_numeroOperacionBanco.Text
                        extornarDetalle.importeDeudaPagada = txtext_det_importeDeudaPagada.Text
                        extornarDetalle.referenciaPagoAdicional = txtext_det_referenciaPagoAdicional.Text
                        listaextornarDetalle.Add(extornarDetalle)
                        requestExtornar.detalle = listaextornarDetalle.ToArray()

                        requestExtornar.canalOperacion = txtext_canalOperacion.Text
                        requestExtornar.codigoOficina = txtext_codigoOficina.Text
                        requestExtornar.fechaOperacion = txtext_fechaOperacion.Text
                        requestExtornar.horaOperacion = txtext_horaOperacion.Text
                        requestExtornar.datosEmpresa = txtext_datosEmpresa.Text

                        Dim responseExtornar As ProxyWSBBVA.BEBBVAExtornarResponse = proxy.extornar(requestExtornar)

                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoOperacion, "codigoOperacion")) ' codigoOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.numeroOperacion, "numeroOperacion")) 'numeroOperacion()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoBanco, "codigoBanco")) 'codigoBanco()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoConvenio, "codigoConvenio")) 'codigoConvenio()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.tipoCliente, "tipoCliente")) 'tipoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoCliente, "codigoCliente")) 'codigoCliente()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.numeroReferenciaDeuda, "numeroReferenciaDeuda")) 'numeroReferenciaDeuda()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.numeroOperacionEmpresa, "numeroOperacionEmpresa")) 'numeroOperacionEmpresa()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.referenciaDeudaAdicional, "referenciaDeudaAdicional")) 'referenciaDeudaAdicional()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.codigoResultado, "codigoResultado")) 'codigoResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.mensajeResultado, "mensajeResultado")) 'mensajeResultado()
                        strResultado.AppendLine(String.Format("<{1}>{0}</{1}>", responseExtornar.datosEmpresa, "datosEmpresa")) 'datosEmpresa()

                        Dim caso As String = Convert.ToString(ViewState("panel"))
                        Visualizar(caso)
                End Select
                'resultado = proxy.WSAGOperacion(cip, "001", codMoneda, "010", "001", "1", cip, monto, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", monto, dropOperacion.SelectedValue)
            End Using
            'txtResultado.Text = resultado
            txtResultado.Text = strResultado.ToString()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Sub Inittablas()
        tbconsultar.Visible = False
        tbpagar.Visible = False
        tbanular.Visible = False
        tbextornar.Visible = False
    End Sub

    Protected Sub dropOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dropOperacion.SelectedIndexChanged
        Dim caso As String
        caso = dropOperacion.SelectedValue
        Visualizar(caso)
        txtResultado.Text = String.Empty
        GenerarRandom()

    End Sub

    Private Sub Visualizar(ByVal segun As String)
        Inittablas()
        Select Case segun
            Case "1010"
                tbconsultar.Visible = True
                dropOperacion.SelectedValue = "1010"
                ViewState("panel") = "1010"
            Case "2010"
                tbpagar.Visible = True
                dropOperacion.SelectedValue = "2010"
                ViewState("panel") = "2010"
            Case "3010"
                tbanular.Visible = True
                dropOperacion.SelectedValue = "3010"
                ViewState("panel") = "3010"
            Case "4010"
                tbextornar.Visible = True
                dropOperacion.SelectedValue = "4010"
                ViewState("panel") = "4010"
        End Select
    End Sub

    Private Sub GenerarRandom()
        'Randomize()
        '' Generate random value between 1 and 6.
        'Dim value As Integer = CInt(Int((6 * Rnd()) + 1))
        'txtNroOperacion.Text = value.ToString()
        Dim value As Integer
        Dim valuestr As String
        Dim randNumber As New Random(DateTime.Now.Millisecond)
        value = randNumber.Next(0, Integer.MaxValue)
        valuestr = value.ToString("0000000000")
        txtconsultar_numeroOperacion.Text = valuestr
        txtpagar_numeroOperacion.Text = valuestr
        txtanular_numeroOperacion.Text = valuestr
        txtext_numeroOperacion.Text = valuestr
    End Sub
End Class

