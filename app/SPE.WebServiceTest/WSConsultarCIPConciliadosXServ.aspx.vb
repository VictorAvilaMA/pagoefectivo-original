Imports System.Configuration.ConfigurationManager
Imports SPE.Utilitario
Imports SPE.Api

Partial Public Class WSConsultarCIPConciliadosXServ
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            pnlValidacion.Visible = True
            Dim strCApi As String = AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
            txtFechaDesde.Text = DateAdd(DateInterval.Day, -1, Date.Now).Date.ToString("yyyy-MM-dd")
            txtFechaHasta.Text = DateTime.Now.ToString("yyyy-MM-dd")
        End If
    End Sub

    Protected Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
        Dim clsEncripta As New ClsLibreriax()
        Dim proxy As New Proxys.WSCIP()
        Dim request As New Proxys.BEWSConsCIPsConciliadosRequest()
        With request
            .CAPI = txtCAPI.Text
            .CClave = txtCClave.Text
            'request.IdServicio = Convert.ToInt64(ddlServicio.SelectedValue.Trim())
            .FechaDesde = txtFechaDesde.Text.Trim
            .FechaHasta = txtFechaHasta.Text.Trim
            .Tipo = ddlTipo.SelectedValue.Trim()
        End With
        'If (cbCancelados.Checked) Then
        '    'request.IdEstado = cbCancelados.Checked
        'End If
        Dim response As Proxys.BEWSConsCIPsConciliadosResponse = proxy.ConsultarCIPConciliados(request)
        If (response IsNot Nothing And response.CIPs IsNot Nothing) Then
            Dim CIPs As New StringBuilder
            CIPs.Append("<ArrayOfBEWSConsCIP>" + Environment.NewLine)
            For Each CIP As Proxys.BEWSConsCIP In response.CIPs
                CIPs.Append("    <BEWSConsCIP>" + Environment.NewLine)
                For Each prop As System.Reflection.PropertyInfo In GetType(Proxys.BEWSConsCIP).GetProperties()
                    If prop.Name <> "Detalle" Then
                        CIPs.AppendFormat("        <{0}>{1}</{0}>" + Environment.NewLine, prop.Name, prop.GetValue(CIP, Nothing))
                    End If
                Next
                CIPs.Append("        <ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                For Each CIPDetalle As Proxys.BEWSConsCIPDetalle In CIP.Detalle
                    CIPs.Append("            <BEWSConsCIPDetalle>" + Environment.NewLine)
                    For Each prop2 As System.Reflection.PropertyInfo In GetType(Proxys.BEWSConsCIPDetalle).GetProperties()
                        CIPs.AppendFormat("                <{0}>{1}<{0}>" + Environment.NewLine, prop2.Name, prop2.GetValue(CIPDetalle, Nothing))
                    Next
                    CIPs.Append("            </BEWSConsCIPDetalle>" + Environment.NewLine)
                Next
                CIPs.Append("        </ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                CIPs.Append("    </BEWSConsCIP>" + Environment.NewLine)
            Next
            CIPs.Append("</ArrayOfBEWSConsCIP>" + Environment.NewLine)
            txtResultado.Text = CIPs.ToString
        Else
            txtResultado.Text = ""
        End If

        txtEstado.Text = response.Estado
        txtMensaje.Text = response.Mensaje

    End Sub

    Private Sub btnValidacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidacion.Click
        pnlValidacion.Visible = (AppSettings("ValidarTestWSConsultarCIPXServ") <> "" And txtValidacion.Text = AppSettings("ValidarTestWSConsultarCIPXServ"))
    End Sub

End Class