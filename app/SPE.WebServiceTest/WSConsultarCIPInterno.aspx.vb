﻿Imports SPE.Utilitario
Imports System.Configuration.ConfigurationManager
Imports ProxyWSGeneralInterno

Public Class WSConsultarCIPInterno
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
        End If
    End Sub

    Protected Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
        Dim clsEncripta As New ClsLibreriax()
        Dim proxy As New ProxyWSGeneralInterno.WebService()
        Dim request As New ProxyWSGeneralInterno.BEWSConsultarCIPv2Request()
        request.CAPI = txtCAPI.Text
        request.CClave = txtCClave.Text
        request.CIPS = txtCIPs.Text.Trim
        Try
            Dim response As ProxyWSGeneralInterno.BEWSConsultarCIPv2Response = proxy.ConsultarCIP(request)
            If response IsNot Nothing Then
                If response.CIPs IsNot Nothing Then
                    Dim CIPs As New StringBuilder
                    CIPs.Append("<ArrayOfBEWSConsCIP>" + Environment.NewLine)
                    For Each CIP As ProxyWSGeneralInterno.BEWSConsCIPv2 In response.CIPs
                        CIPs.Append("    <BEWSConsCIPv2>" + Environment.NewLine)
                        For Each prop As System.Reflection.PropertyInfo In GetType(ProxyWSGeneralInterno.BEWSConsCIPv2).GetProperties()
                            If prop.Name <> "Detalle" Then
                                CIPs.AppendFormat("        <{0}>{1}</{0}>" + Environment.NewLine, prop.Name, prop.GetValue(CIP, Nothing))
                            End If
                        Next
                        CIPs.Append("        <ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                        For Each CIPDetalle As ProxyWSGeneralInterno.BEWSConsCIPDetalle In CIP.Detalle
                            CIPs.Append("            <BEWSConsCIPDetalle>" + Environment.NewLine)
                            For Each prop2 As System.Reflection.PropertyInfo In GetType(ProxyWSGeneralInterno.BEWSConsCIPDetalle).GetProperties()
                                CIPs.AppendFormat("                <{0}>{1}<{0}>" + Environment.NewLine, prop2.Name, prop2.GetValue(CIPDetalle, Nothing))
                            Next
                            CIPs.Append("            </BEWSConsCIPDetalle>" + Environment.NewLine)
                        Next
                        CIPs.Append("        </ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                        CIPs.Append("    </BEWSConsCIPv2>" + Environment.NewLine)
                    Next
                    CIPs.Append("</ArrayOfBEWSConsCIP>" + Environment.NewLine)
                    txtResultado.Text = CIPs.ToString
                Else
                    txtResultado.Text = ""
                End If
            Else
                txtResultado.Text = ""
            End If
            txtEstado.Text = response.Estado
            txtMensaje.Text = response.Mensaje
        Catch ex As Exception
            txtResultado.Text = ex.ToString()
        End Try
    End Sub

    Protected Sub btnValidacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidacion.Click
        pnlValidacion.Visible = (AppSettings("ValidarTestWSConsultarCIPv2") <> "" And txtValidacion.Text = AppSettings("ValidarTestWSConsultarCIPv2"))
    End Sub

End Class
