<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSConsultarCIPs.aspx.vb"
    Inherits="SPE.WebServiceTest.WSConsultarCIPs" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prueba de Servicios Web de Agencias Bancarias</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:TextBox ID="txtValidacion" runat="server" TextMode="Password" Width="358px"></asp:TextBox>
    <asp:Button ID="btnValidacion" runat="server" Text="Validar" /><br />
    <asp:Panel ID="pnlValidacion" runat="server" Visible="false">
        <div>
            <br />
            1) Prueba de Servicios Web de Agencias Bancarias
            <table>
                <tr>
                    <td>
                        CAPI:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCAPI" runat="server" Width="304px">6964dc43-1507-47f0-978c-954d7bb41b95</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CClave:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCClave" runat="server" Width="304px">6ca4c8cb-7a6d-4327-b439-b0ec3a5988e9</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CIP(s):
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCIPs" runat="server" Width="305px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="width: 506px">
                        <asp:Button ID="btnEjecutar" runat="server" Text="Ejecutar" />
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        Resultado:
                    </td>
                    <td style="width: 70%;">
                        <asp:TextBox ID="txtResultado" runat="server" TextMode="MultiLine" Height="276px"
                            Width="501px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Xml:
                    </td>
                    <td style="width: 70%;">
                        <asp:TextBox ID="txtXml" runat="server" TextMode="MultiLine" Height="276px" Width="501px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Estado:
                    </td>
                    <td style="width: 70%;">
                        <asp:TextBox ID="txtEstado" runat="server" TextMode="MultiLine" Height="16px" Width="501px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Mensaje:
                    </td>
                    <td style="width: 70%;">
                        <asp:TextBox ID="txtMensaje" runat="server" TextMode="MultiLine" Height="16px" Width="501px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
