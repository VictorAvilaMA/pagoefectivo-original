Imports SPE.Utilitario
Imports System.Configuration.ConfigurationManager
Imports SPE.Api

Partial Public Class WSConsultarCIPs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
        End If
    End Sub

    Protected Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
        Dim clsEncripta As New ClsLibreriax()
        Dim proxy As New Proxys.WSCIP()
        Dim request As New Proxys.BEWSConsultarCIPRequest()
        request.CAPI = txtCAPI.Text
        request.CClave = txtCClave.Text
        request.CIPS = txtCIPs.Text.Trim
        Dim response As Proxys.BEWSConsultarCIPResponse = proxy.ConsultarCIP(request)
        If (response IsNot Nothing) Then
            If response.CIPs IsNot Nothing Then
                Dim CIPs As New StringBuilder
                CIPs.Append("<ArrayOfBEWSConsCIP>" + Environment.NewLine)
                For Each CIP As Proxys.BEWSConsCIP In response.CIPs
                    CIPs.Append("    <BEWSConsCIP>" + Environment.NewLine)
                    For Each prop As System.Reflection.PropertyInfo In GetType(Proxys.BEWSConsCIP).GetProperties()
                        If prop.Name <> "Detalle" Then
                            CIPs.AppendFormat("        <{0}>{1}</{0}>" + Environment.NewLine, prop.Name, prop.GetValue(CIP, Nothing))
                        End If
                    Next
                    CIPs.Append("        <ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                    For Each CIPDetalle As Proxys.BEWSConsCIPDetalle In CIP.Detalle
                        CIPs.Append("            <BEWSConsCIPDetalle>" + Environment.NewLine)
                        For Each prop2 As System.Reflection.PropertyInfo In GetType(Proxys.BEWSConsCIPDetalle).GetProperties()
                            CIPs.AppendFormat("                <{0}>{1}<{0}>" + Environment.NewLine, prop2.Name, prop2.GetValue(CIPDetalle, Nothing))
                        Next
                        CIPs.Append("            </BEWSConsCIPDetalle>" + Environment.NewLine)
                    Next
                    CIPs.Append("        </ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                    CIPs.Append("    </BEWSConsCIP>" + Environment.NewLine)
                Next
                CIPs.Append("</ArrayOfBEWSConsCIP>" + Environment.NewLine)
                txtResultado.Text = CIPs.ToString
            Else
                txtResultado.Text = ""
            End If
            txtXml.Text = response.XML
            txtEstado.Text = response.Estado
            txtMensaje.Text = response.Mensaje
        Else
            txtXml.Text = ""
            txtResultado.Text = ""
            txtEstado.Text = ""
            txtMensaje.Text = ""
        End If

    End Sub
    Protected Sub btnValidacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidacion.Click
        pnlValidacion.Visible = (AppSettings("ValidarTestWSConsultarCIP") <> "" And txtValidacion.Text = AppSettings("ValidarTestWSConsultarCIP"))
    End Sub
End Class