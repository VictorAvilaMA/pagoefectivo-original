﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSConsultarSolicitudPagoV3.aspx.vb"
    Inherits="SPE.WebServiceTest.WSConsultarSolicitudPagoV2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlValidacion" runat="server" Visible="true">
        <div>
            <br />
            1) Prueba de consulta de Solicitud Pago
            <table>
                <tr>
                    <td>
                        Dir KeyPublicSPE.:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtKeyPublicPath" runat="server" Width="304px">C:\\PagoEfectivo2\\Claves\\SPE_PublicKey.1pz</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Dir KeyPrivateEntidad.:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtKeyPrivatePath" runat="server" Width="304px">C:\\PagoEfectivo2\\Claves\\SGA_PrivateKey.1pz</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CAPI:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCAPI" runat="server" Width="304px">b4958a52-be57-4f6c-b400-08361aacdf17</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CClave(firma generada):
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCClave" runat="server" Width="304px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="width: 506px">
                        <asp:Button ID="btnEjecutar" runat="server" Text="Ejecutar" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Xml:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtXml" TextMode="MultiLine" runat="server" Height="100px" Width="440px"><?xml version="1.0" encoding="utf-8" ?>
                        </asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                    <td>
                        Estado:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEstado" runat="server" Width="700px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Mensaje:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMensaje" runat="server" Width="700px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        XML:
                    </td>
                    <td>
                        <asp:TextBox ID="txtXMLRes" runat="server" Width="700px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        XML Desencriptado:
                    </td>
                    <td>
                        <asp:TextBox ID="txtXMLResDesc" runat="server" TextMode="MultiLine" Height="700px"
                            Width="700px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
