﻿Imports System.Configuration.ConfigurationManager
Imports SPE.Utilitario
Imports SPE.Criptography
Imports System.IO
Imports SPE.Api

Public Class WSConsultarSolicitudPagoV2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
        End If
    End Sub

    Protected Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
        txtEstado.Text = String.Empty
        txtMensaje.Text = String.Empty
        txtXMLRes.Text = String.Empty

        Dim clsEncripta As New ClsLibreriax()
        ' Dim proxy As New WSGeneralv2.Service()

        Dim request As New SPE.WebServiceTest.WSGeneralv2.BEWSConsultarSolicitudRequest()
        Dim response As New SPE.WebServiceTest.WSGeneralv2.BEWSConsultarSolicitudResponse()


        With request
            .cServ = txtCAPI.Text.Trim()
            .Xml = txtXml.Text.Trim()
        End With

        txtCClave.Text = request.CClave
        'PagoEfectivo.PublicPathContraparte = txtKeyPublicPath.Text
        'PagoEfectivo.PrivatePath = txtKeyPrivatePath.Text
        'response = PagoEfectivo.ConsultarSolicitudPago(request)


        Try
            Using proxy As New WSGeneralv2.Service()
                'resultado = proxy.WSAGOperacion(cip, CodServ, codMoneda, NroOperacion, NroOperacionAnulada, txtAgencia.Text, "1", cip, monto, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", monto, dropOperacion.SelectedValue)
                response = proxy.ConsultarSolicitudPagov2(request)
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString())
        End Try



        If (response IsNot Nothing) Then
            txtEstado.Text = response.Estado
            txtMensaje.Text = response.Mensaje
            txtXMLRes.Text = response.Xml
            Try
                txtXMLResDesc.Text = response.Xml
            Catch ex As Exception
                txtXMLResDesc.Text = ex.ToString()
            End Try
        End If
    End Sub

End Class