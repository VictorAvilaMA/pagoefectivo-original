<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSElimCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSElimCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Prueba Eliminación de CIP desde WS</title>
</head>
<body>
    <form id="form1" runat="server">        
        <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
        <asp:Button ID="btnPanel" runat="server"  Text="Validar" />   
        <asp:Panel ID="pPanel" runat="server" Visible="false"  >
           <div>
            <br />
             1)  Prueba Eliminación de CIP desde WS
            <table>
             <tr>
                <td>CAPI:</td>
                <td style="width: 506px">
                    <asp:TextBox ID="txtCAPI" runat="server" Width="304px">6964dc43-1507-47f0-978c-954d7bb41b95</asp:TextBox>
                </td>
             </tr>
             <tr>
                <td>CClave:</td>
                <td style="width: 506px">
                    <asp:TextBox ID="txtCClave" runat="server" Width="304px">6ca4c8cb-7a6d-4327-b439-b0ec3a5988e9</asp:TextBox>
                </td>
             </tr>
             <tr>
                <td>cip:</td>
                <td style="width: 506px">
                                        <asp:TextBox ID="txtCIP" runat="server"></asp:TextBox>
                </td>
             </tr>          
                     
             <tr>
                <td>InfoRequest:</td>
                <td style="width: 506px">
                    <asp:TextBox ID="txtInfoRequest" TextMode="MultiLine"  runat="server" Height="200px" Width="200px"></asp:TextBox>
                </td>
             </tr>                    
             <tr>
                <td></td>
                <td style="width: 506px">
                    <asp:Button ID="btnEliminarCIP" runat="server" Text="Eliminaar CIP" />
                </td>
             </tr>                         
            </table>
                
                <br />
                
                <table>
                <tr>
                    <td>Resultado:</td>
                    <td></td>
                </tr>
                    <tr>
                        <td>Estado:</td>
                        <td>
                            <asp:TextBox ID="txtEstado" runat="server"></asp:TextBox>
                        </td>
                     </tr>                            
                    <tr>
                        <td>Mensaje:</td>
                        <td>
                            <asp:TextBox ID="txtMensaje" runat="server"></asp:TextBox>
                        </td>
                     </tr>   
                    <tr>
                        <td>InfoResponse:</td>
                        <td>
                            <asp:TextBox ID="txtInfoResponse" runat="server"></asp:TextBox>
                        </td>
                     </tr>                           
                </table>
            </div>
        </asp:Panel>    
    </form>
</body>
</html>
