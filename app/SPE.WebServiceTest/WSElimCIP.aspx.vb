Imports SPE.Api
Partial Public Class WSElimCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
        End If
    End Sub
    'Private Sub MostrarPrueba(ByVal flag As Boolean)
    '    pPrueba.Visible = flag
    '    pValida.Visible = Not flag
    'End Sub

    'Protected Sub btnE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnE.Click
    '    Dim pas As String = System.Configuration.ConfigurationManager.AppSettings("PSELIMCIP")
    '    'If (txtUse.Text = "WSElimCIP01" And txtP.Text = pas) Then
    '    MostrarPrueba(True)
    '    'End If
    'End Sub

    Protected Sub btnEliminarCIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarCIP.Click
        Dim clsEncripta As New Utilitario.ClsLibreriax()
        Dim proxy As New Proxys.WSCIP()
        Dim request As New Proxys.BEWSElimCIPRequest()
        request.CAPI = txtCAPI.Text
        request.CClave = txtCClave.Text
        request.CIP = txtCIP.Text
        request.InfoRequest = clsEncripta.Encrypt(txtInfoRequest.Text, clsEncripta.fEncriptaKey)
        Dim response As Proxys.BEWSElimCIPResponse = proxy.EliminarCIP(request)
        If (response IsNot Nothing) Then
            txtEstado.Text = response.Estado
            txtMensaje.Text = response.Mensaje
            'txtinformacionCIP.Text = clsEncripta.Encrypt(response.InformacionCIP, clsEncripta.fEncriptaKey)
            txtInfoResponse.Text = clsEncripta.Decrypt(response.InfoResponse, clsEncripta.fEncriptaKey)
        End If
    End Sub
    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSElimCIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub


End Class