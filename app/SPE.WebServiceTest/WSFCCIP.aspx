<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSFCCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSFCConsultar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FullCarga</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtValidar" runat="server" TextMode="Password" Width="358px"></asp:TextBox>
        <asp:Button ID="btnValidar" runat="server" Text="Validar" /><br /><br />
        <asp:Panel ID="pnlVer" runat="server" Visible="false">
            Operación:
            <asp:DropDownList ID="ddlOperacion" runat="server" AutoPostBack="true">
                <asp:ListItem Text="Consultar"></asp:ListItem>
                <asp:ListItem Text="Cancelar"></asp:ListItem>
                <asp:ListItem Text="Anular"></asp:ListItem>
            </asp:DropDownList><br /><br />
            <asp:Panel ID="pnlConsultar" runat="server">
                <fieldset>
                    <legend>Request</legend>
                    <table>
                        <tr>
                            <td>CodPuntoVenta:</td>
                            <td><asp:TextBox ID="CONtxtPuntoVenta" runat="server" MaxLength="12" Text="ABCDEFGHIJKL" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroSerieTerminal:</td>
                            <td><asp:TextBox ID="CONtxtNroSerieTerminal" runat="server" MaxLength="8" Text="12345678"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroOperacionFullCarga:</td>
                            <td><asp:TextBox ID="CONtxtNroOperacionFullCarga" runat="server" MaxLength="9" Text="987654321"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CIP:</td>
                            <td><asp:TextBox ID="CONtxtCIP" runat="server" MaxLength="14" Text="00001234567890"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodServicio:</td>
                            <td><asp:TextBox ID="CONtxtCodServicio" runat="server" MaxLength="10" Text="0001"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><asp:Button ID="btnConsultar" runat="server" Text="Consultar" /></td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Response</legend>
                    <table>
                        <tr>
                            <td>CIP:</td>
                            <td><asp:TextBox ID="CONtxtCIPr" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodResultado:</td>
                            <td><asp:TextBox ID="CONtxtCodResultado" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>MensajeResultado:</td>
                            <td><asp:TextBox ID="CONtxtMensajeResultado" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Monto:</td>
                            <td><asp:TextBox ID="CONtxtMonto" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMoneda:</td>
                            <td><asp:TextBox ID="CONtxtCodMoneda" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>ConceptoPago:</td>
                            <td><asp:TextBox ID="CONtxtConceptoPago" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>FechaVencimiento:</td>
                            <td><asp:TextBox ID="CONtxtFechaVencimiento" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodServicio:</td>
                            <td><asp:TextBox ID="CONtxtCodServicior" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlCancelar" runat="server" Visible="false">
                <fieldset>
                    <legend>Request</legend>
                    <table>
                        <tr>
                            <td>CodPuntoVenta:</td>
                            <td><asp:TextBox ID="CANtxtPuntoVenta" runat="server" MaxLength="12" Text="FCCENTROLIMA" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroSerieTerminal:</td>
                            <td><asp:TextBox ID="CANtxtNroSerieTerminal" runat="server" MaxLength="8" Text="12345678"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroOperacionFullCarga:</td>
                            <td><asp:TextBox ID="CANtxtNroOperacionFullCarga" runat="server" MaxLength="9" Text="987654321"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMedioPago:</td>
                            <td><asp:TextBox ID="CANtxtCodMedioPago" runat="server" MaxLength="1" Text="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CIP:</td>
                            <td><asp:TextBox ID="CANtxtCIP" runat="server" MaxLength="14" Text="00000001029539"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Monto:</td>
                            <td><asp:TextBox ID="CANtxtMonto" runat="server" MaxLength="14" Text="00000000022.00"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMoneda:</td>
                            <td><asp:TextBox ID="CANtxtCodMoneda" runat="server" MaxLength="1" Text="1"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodServicio:</td>
                            <td><asp:TextBox ID="CANtxtCodServicio" runat="server" MaxLength="10" Text="0001198"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" /></td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Response</legend>
                    <table>
                        <tr>
                            <td>CodResultado:</td>
                            <td><asp:TextBox ID="CANtxtCodResultado" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>MensajeResultado:</td>
                            <td><asp:TextBox ID="CANtxtMensajeResultado" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMovimiento:</td>
                            <td><asp:TextBox ID="CANtxtCodMovimiento" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodServicio:</td>
                            <td><asp:TextBox ID="CANtxtCodServicior" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlAnular" runat="server" Visible="false">
                <fieldset>
                    <legend>Request</legend>
                    <table>
                        <tr>
                            <td>CodPuntoVenta:</td>
                            <td><asp:TextBox ID="ANUtxtPuntoVenta" runat="server" MaxLength="12" Text="FCCENTROLIMA" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroSerieTerminal:</td>
                            <td><asp:TextBox ID="ANUtxtNroSerieTerminal" runat="server" MaxLength="8" Text="12345678"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroOperacionFullCargaPago:</td>
                            <td><asp:TextBox ID="ANUtxtNroOperacionFullCargaPago" runat="server" MaxLength="9" Text="123456789"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>NroOperacionFullCarga:</td>
                            <td><asp:TextBox ID="ANUtxtNroOperacionFullCarga" runat="server" MaxLength="9" Text="987654322"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMedioPago:</td>
                            <td><asp:TextBox ID="ANUtxtCodMedioPago" runat="server" MaxLength="1" Text="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CIP:</td>
                            <td><asp:TextBox ID="ANUtxtCIP" runat="server" MaxLength="14" Text="00001234567890"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Monto:</td>
                            <td><asp:TextBox ID="ANUtxtMonto" runat="server" MaxLength="14" Text="00000000012.12"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMoneda:</td>
                            <td><asp:TextBox ID="ANUtxtCodMoneda" runat="server" MaxLength="1" Text="1"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodServicio:</td>
                            <td><asp:TextBox ID="ANUtxtCodServicio" runat="server" MaxLength="10" Text="0001"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><asp:Button ID="btnAnular" runat="server" Text="Anular" /></td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Response</legend>
                    <table>
                        <tr>
                            <td>CodResultado:</td>
                            <td><asp:TextBox ID="ANUtxtCodResultado" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>MensajeResultado:</td>
                            <td><asp:TextBox ID="ANUtxtMensajeResultado" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodMovimiento:</td>
                            <td><asp:TextBox ID="ANUtxtCodMovimiento" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>CodServicio:</td>
                            <td><asp:TextBox ID="ANUtxtCodServicior" runat="server" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
