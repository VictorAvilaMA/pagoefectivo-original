﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSGenArchConc.aspx.vb"
    Inherits="SPE.WebServiceTest.WSGenArchConc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div>
        <form id="form2" runat="server">
        <asp:Panel ID="pPanel" runat="server" Visible="true">
            <div>
                <br />
                1) Generación de Archivo Conciliacion
                <table>
                    <tr>
                        <td>
                            Fecha:
                        </td>
                        <td style="width: 506px">
                            <asp:TextBox ID="txtFecha" runat="server" Width="304px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CIPs:
                        </td>
                        <td style="width: 506px">
                            <asp:TextBox ID="txtCIPs" runat="server" Width="304px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            NumsOpe.:
                        </td>
                        <td style="width: 506px">
                            <asp:TextBox ID="txtNumOp" runat="server" Width="305px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Montos:
                        </td>
                        <td style="width: 506px">
                            <asp:TextBox ID="txtMonto" runat="server" Width="305px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trama:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTramaCabezera" runat="server" Width="950px" Text="CC19301764249C[fecha]0000000010000000000300201915TT7Q02"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtTramaDetalle" runat="server" Width="100%" Text="DD19301764249[CIP][CIP]                [fecha][fecha]000000000000000000000000000000[monto]111023[numOpe]EFECTIVO[CIP]TBBN  00[numOperacion]120920"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td style="width: 506px">
                            <asp:Button ID="btnGenerarArchivo" runat="server" Text="Generar" />
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </asp:Panel>
        </form>
    </div>
</body>
</html>
