﻿Public Class WSGenArchConc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtFecha.Text = Date.Now.ToString("yyyyMMdd")
    End Sub

    Protected Sub btnGenerarArchivo_Click(sender As Object, e As EventArgs) Handles btnGenerarArchivo.Click
        Dim charSeparators As Char = ","
        Dim CIPs As String() = txtCIPs.Text.Trim.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
        Dim Montos As String() = txtMonto.Text.Trim.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
        Dim NumsOp As String() = txtNumOp.Text.Trim.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
        Dim contenido As New StringBuilder
        Try
            contenido.AppendLine(txtTramaCabezera.Text.Trim.Replace("[fecha]", txtFecha.Text))
            For index As Integer = 0 To CIPs.Length - 1
                Dim indexMonto As Integer = ((index) Mod Montos.Length)
                Dim indexNroOpAnuladas As Integer = ((index) Mod NumsOp.Length)
                Dim lineaDetalle As String = txtTramaDetalle.Text

                lineaDetalle = lineaDetalle.Replace("[CIP]", CIPs(index).PadLeft(14, "0").Substring(0, 14))
                lineaDetalle = lineaDetalle.Replace("[fecha]", txtFecha.Text)
                lineaDetalle = lineaDetalle.Replace("[monto]", Montos(indexMonto).Replace(".", "").PadLeft(15, "0"))
                lineaDetalle = lineaDetalle.Replace("[numOperacion]", NumsOp(indexNroOpAnuladas).PadRight(8, " "))
                lineaDetalle = lineaDetalle.Replace("[numOpe]", NumsOp(indexNroOpAnuladas).PadRight(6, " ").Substring(2, 6))

                contenido.AppendLine(lineaDetalle)
            Next

            Dim byteArchivoConciliacion As Byte() = New UTF8Encoding().GetBytes(contenido.ToString())
            Response.Clear()
            Response.ClearHeaders()
            Response.SuppressContent = False
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment; filename=" & "ArchConciBCP_" & DateTime.Now.ToString("yyyyMMdd") & "_" & DateTime.Now.Hour.ToString() & DateTime.Now.Minute.ToString() & ".txt")
            Response.BinaryWrite(byteArchivoConciliacion)
            Response.Flush()
            Response.[End]()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class