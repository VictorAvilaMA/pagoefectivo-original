<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSGenerarCIP.aspx.vb"
    Inherits="SPE.WebServiceTest.WSGenerarCIP" ValidateRequest="false" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prueba Generación de CIP desde WS</title>
    <style type="text/css">
        .style1
        {
            width: 148px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updpanel1" runat="server">
        <ContentTemplate>
            <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
            <asp:Button ID="btnPanel" runat="server" Text="Validar" />
            <asp:Panel ID="pPanel" runat="server" Visible="false">
                <div>
                    <br />
                    1) Prueba Generación de CIP desde WS
                    <table>
                        <tr>
                            <td>
                                CAPI:
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtCAPI" runat="server" Width="304px">1870bb1b-fd48-49dc-ba50-26ff286ccf46</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                CClave:
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtCClave" runat="server" Width="304px">c7232f1f-825a-4248-9082-7b50607468aa</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email:
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtEmail" runat="server" Width="305px">joe.olivera@ec.pe</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password:
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtPassword" runat="server" Width="303px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Xml:
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtXml" TextMode="MultiLine" runat="server" Height="520px" Width="771px"><?xml version="1.0" encoding="utf-8" ?>
    <OrdenPago>
      <IdMoneda>1</IdMoneda>
      <Total>22.00</Total>
      <MerchantId>KOT</MerchantId>
      <OrdenIdComercio>0000001</OrdenIdComercio>
      <UrlOk>http://dev.2.pagoefectivo.pe/WebServiceTest/wsServicioNotificacionTest.aspx</UrlOk>
      <UrlError>http://dev.2.pagoefectivo.pe/WebServiceTest/wsServicioNotificacionTest.aspx</UrlError>
      <MailComercio>admin@kotear.pe</MailComercio>
      <FechaAExpirar>19/08/2016 17:00:00</FechaAExpirar> 
      <UsuarioId>001</UsuarioId>
      <DataAdicional></DataAdicional>
      <UsuarioNombre>Cesar</UsuarioNombre>
      <UsuarioApellidos>Miranda</UsuarioApellidos>
      <UsuarioLocalidad>LIMA</UsuarioLocalidad>
      <UsuarioProvincia>LIMA</UsuarioProvincia>
      <UsuarioPais>PERU</UsuarioPais>
      <UsuarioAlias>cesarmmg</UsuarioAlias>
      <UsuarioEmail>joe.olivera@ec.pe</UsuarioEmail>
      <Detalles>
        <Detalle>
          <Cod_Origen>CT</Cod_Origen>
          <TipoOrigen>TO</TipoOrigen>
          <ConceptoPago>Transacción Comisión 1</ConceptoPago>
          <Importe>10.00</Importe>
          <Campo1></Campo1>      
          <Campo2></Campo2>
          <Campo3></Campo3>
        </Detalle>
	    <Detalle>
          <Cod_Origen>CT</Cod_Origen>
          <TipoOrigen>TO</TipoOrigen>
          <ConceptoPago>Transacción Comisión 2</ConceptoPago>
          <Importe>12.00</Importe>
          <Campo1></Campo1>      
          <Campo2></Campo2>
          <Campo3></Campo3>
        </Detalle>
      </Detalles>
    </OrdenPago></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="width: 506px">
                                <asp:Button ID="btnGenerarCIP" runat="server" Text="Generar CIP" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table style="width: 658px">
                        <tr>
                            <td class="style1">
                                Resultado:
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Estado:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEstado" runat="server" Width="465px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                CIP:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCIP" runat="server" Width="464px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Mensaje:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMensaje" runat="server" Width="465px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                InformaciónCIP:
                            </td>
                            <td>
                                <asp:TextBox ID="txtinformacionCIP" runat="server" Width="461px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
