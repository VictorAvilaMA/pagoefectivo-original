Imports SPE.Utilitario
Imports SPE.Api
Partial Public Class WSGenerarCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
            If (strCClave <> "") Then txtCClave.Text = strCClave
        End If
    End Sub

    Protected Sub btnGenerarCIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarCIP.Click
        Dim clsEncripta As New ClsLibreriax()
        Dim proxy As New Proxys.WSCIP()
        Dim request As New Proxys.BEWSGenCIPRequest()
        request.CAPI = txtCAPI.Text
        request.CClave = txtCClave.Text
        request.Email = txtEmail.Text
        request.Password = txtPassword.Text
        request.Xml = clsEncripta.Encrypt(txtXml.Text, clsEncripta.fEncriptaKey)
        Dim response As Proxys.BEWSGenCIPResponse = proxy.GenerarCIP(request)
        If (response IsNot Nothing) Then
            txtEstado.Text = response.Estado
            txtCIP.Text = response.CIP
            txtMensaje.Text = response.Mensaje
            'txtinformacionCIP.Text = clsEncripta.Encrypt(response.InformacionCIP, clsEncripta.fEncriptaKey)
            txtinformacionCIP.Text = clsEncripta.Decrypt(response.InformacionCIP, clsEncripta.fEncriptaKey)
        End If
    End Sub



    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSGenerarCIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub


End Class