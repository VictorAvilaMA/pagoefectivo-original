﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSGenerarCIPs.aspx.vb"
    Inherits="SPE.WebServiceTest.WSGenerarCIPs" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Generación de CIPs en LOTE</title>
    <style type="text/css">
        .style1
        {
            width: 148px;
        }
        .obligatorio
        {
            color: Blue;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updGenerarCIPs" runat="server">
        <ContentTemplate>
            <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
            <asp:Button ID="btnPanel" runat="server" Text="Validar" />
            <asp:Panel ID="pPanel" runat="server" Visible="false">
                <div>
                    <h4 class="obligatorio">
                        1) Prueba Generación de CIPS desde WS</h4>
                    <table>
                        <tr>
                            <td>
                                CAPIs (separados por comas):
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtCAPI" runat="server" Width="506px">1870bb1b-fd48-49dc-ba50-26ff286ccf46 b3c76a14-fef0-43d8-880c-f163d9ab7c9b</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                CClave (separado por comas):
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtCClave" runat="server" Width="506px">2d3b154c-63e3-4a82-8f7a-0780d6d3c32f dee455ca-e545-4892-9e6d-6dcf7a84c085</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email (separado por comas):
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtEmail" runat="server" Width="506px">jpba_666@hotmail.com</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cantidad (empieza en 1 hasta):
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtCantidad" runat="server" Width="506px">2</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cod. de Serv (separado por comas):
                            </td>
                            <td style="width: 506px">
                                <asp:TextBox ID="txtCodServicio" runat="server" Width="506px">KOT NEO</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Xml plantilla:
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 506px" colspan="2">
                                <asp:TextBox ID="txtXml" TextMode="MultiLine" runat="server" Height="520px" Width="771px">
<?xml version="1.0" encoding="utf-8" ?>
<OrdenPago>
    <IdMoneda>1</IdMoneda>
    <Total>26.00</Total>
    <MerchantId>KOT</MerchantId>
    <OrdenIdComercio>GCIPS[correlativo]</OrdenIdComercio>
    <UrlOk>http://dev.2.pagoefectivo.pe/WebServiceTest/wsServicioNotificacionTest.aspx</UrlOk>
    <UrlError>http://dev.2.pagoefectivo.pe/WebServiceTest/wsServicioNotificacionTest.aspx</UrlError>
    <MailComercio>jonathan.bastidas@ec.pe</MailComercio>
    <FechaAExpirar>01/01/2016 10:00:00</FechaAExpirar> 
    <UsuarioId>001</UsuarioId>
    <DataAdicional></DataAdicional>
    <UsuarioNombre>Usuario[correlativo]</UsuarioNombre>
    <UsuarioApellidos>Apellido[correlativo]</UsuarioApellidos>
    <UsuarioLocalidad>LIMA</UsuarioLocalidad>
    <UsuarioProvincia>LIMA</UsuarioProvincia>
    <UsuarioPais>PERU</UsuarioPais>
    <UsuarioAlias>usuario[correlativo]</UsuarioAlias>
    <UsuarioEmail>[email]</UsuarioEmail>
    <Detalles>
    <Detalle>
        <Cod_Origen>CT</Cod_Origen>
        <TipoOrigen>TO</TipoOrigen>
        <ConceptoPago>Transacción Comisión 1</ConceptoPago>
        <Importe>13.00</Importe>
        <Campo1></Campo1>      
        <Campo2></Campo2>
        <Campo3></Campo3>
    </Detalle>
	<Detalle>
        <Cod_Origen>CT</Cod_Origen>
        <TipoOrigen>TO</TipoOrigen>
        <ConceptoPago>Transacción Comisión 2</ConceptoPago>
        <Importe>13.00</Importe>
        <Campo1></Campo1>      
        <Campo2></Campo2>
        <Campo3></Campo3>
    </Detalle>
    </Detalles>
</OrdenPago></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="width: 506px">
                                <asp:Button ID="btnGenerarCIP" runat="server" Text="Generar CIPs" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table style="width: 658px">
                        <tr>
                            <td class="style1 obligatorio">
                                Resultado:
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Estado:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEstado" runat="server" Width="465px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                CIPs:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCIP" runat="server" Width="464px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Montos:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMonto" runat="server" Width="464px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Mensaje:
                            </td>
                            <td>
                                <asp:TextBox ID="txtMensaje" runat="server" Width="465px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                InformaciónCIP:
                            </td>
                            <td>
                                <asp:TextBox ID="txtinformacionCIP" runat="server" Width="461px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
