﻿Imports SPE.Utilitario
Imports System.Xml
Imports SPE.Api

Public Class WSGenerarCIPs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnGenerarCIP_Click(sender As Object, e As EventArgs) Handles btnGenerarCIP.Click
        Dim clsEncripta As New ClsLibreriax()
        Dim proxy As New Proxys.WSCIP()
        Dim request As New Proxys.BEWSGenCIPRequest()

        Dim CCAPIS As String() = txtCAPI.Text.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
        Dim CCLAVES As String() = txtCClave.Text.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
        Dim CSERV As String() = txtCodServicio.Text.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)
        Dim EMAILS As String() = txtEmail.Text.Split(New String() {}, StringSplitOptions.RemoveEmptyEntries)

        Dim max As Integer = Integer.Parse(txtCantidad.Text.Trim)
        Dim seccion As Integer = 1
        Try
            For index As Integer = 0 To max - 1
                If (index > (((max - 1) * seccion) / CCAPIS.Length())) Then
                    seccion += 1
                End If
                request.CAPI = CCAPIS(seccion - 1)
                request.CClave = CCLAVES(seccion - 1)
                request.Email = EMAILS(index Mod (EMAILS.Length))

                Dim xmlText As String = txtXml.Text.Trim
                xmlText = xmlText.Replace("[correlativo]", "" & index)
                xmlText = xmlText.Replace("[email]", request.Email)
                xmlText = xmlText.Replace("[codigo]", CSERV(seccion - 1))

                request.Xml = clsEncripta.Encrypt(xmlText, clsEncripta.fEncriptaKey)
                Dim xml As New XmlDocument
                xml.InnerXml = xmlText
                Dim element As XmlElement = xml.SelectSingleNode("OrdenPago").SelectSingleNode("Total")

                Dim response As Proxys.BEWSGenCIPResponse = proxy.GenerarCIP(request)
                If (response IsNot Nothing) Then
                    txtEstado.Text = If(txtEstado.Text.Trim = String.Empty, "", txtEstado.Text + " ") & response.Estado
                    txtCIP.Text = If(txtCIP.Text.Trim = String.Empty, "", txtCIP.Text + " ") & response.CIP
                    txtMensaje.Text = response.Mensaje
                    txtMonto.Text = element.InnerText
                    'txtinformacionCIP.Text = clsEncripta.Encrypt(response.InformacionCIP, clsEncripta.fEncriptaKey)
                    txtinformacionCIP.Text = clsEncripta.Decrypt(response.InformacionCIP, clsEncripta.fEncriptaKey)
                End If
            Next
        Catch ex As Exception
            txtinformacionCIP.Text = ex.ToString()
        End Try

    End Sub
    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSGenerarCIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub
    Protected Sub btnPanel_Click(sender As Object, e As EventArgs) Handles btnPanel.Click
        Validar()
    End Sub
End Class