<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSIBKCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSIBKCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Dummy Interbank</title>
    <style type="text/css">
        dl
        {
            margin: 0;
        }
        dl dt
        {
            float: left;
            padding: 5px;
            width: 275px;
        }
        dl dd
        {
            margin: 2px 0;
            padding: 5px 0;
        }
        fieldset.entrada, fieldset.salida
        {
            margin: 0;
            padding: 0;
            float: left;
            width: 49%;
        }
        legend
        {
            font-weight: bold;
        }
        input.trama
        {
            width: 95%;
        }
        
        .tooltip
        {
            background-color: #000;
            border: 1px solid #fff;
            padding: 10px 15px;
            width: 200px;
            display: none;
            color: #fff;
            text-align: left; /* font-size:12px;

	        outline radius for mozilla/firefox only */
            -moz-box-shadow: 0 0 10px #000;
            -webkit-box-shadow: 0 0 10px #000;
        }
    </style>
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.tools.min.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("input[title]").tooltip({
                position: "center right",
                offset: [-2, 10],
                effect: "fade",
                opacity: 0.7
            });
            DarFormatoInicial();
            DarFormato();
            $('.botonentrada').click(function () {
                DarFormato();
                var ok = true;
                $(this).parent().find('input:text').not($('.trama')).each(function () {
                    if ($(this).val().length != $(this).attr('maxlength')) {
                        $(this).css('border-color', 'Red');
                        ok = false;
                    }
                });
                if (!ok)
                    alert('!Verificar longitud!')
                else
                    Ensamblar('fieldset.entrada');
                return ok;
            });
            $('.ensamblarentrada').click(function () {
                Ensamblar('fieldset.entrada');
                return false;
            });
            $('.desensamblarentrada').click(function () {
                Desensamblar('fieldset.entrada');
                return false;
            });
            $('.ensamblarsalida').click(function () {
                Ensamblar('fieldset.salida');
                return false;
            });
            $('.desensamblarsalida').click(function () {
                Desensamblar('fieldset.salida');
                return false;
            });
            Desensamblar('fieldset.salida');
        });
        function DarFormatoInicial() {
            $('fieldset.salida').find('input:text').not($('.trama')).each(function () {
                if ($(this).attr('format') != undefined)
                    $(this).after(' <i>' + $(this).attr('format') + '</i>');
                if ($(this).attr('maxlength') != undefined)
                    $(this).after('<span>(' + $(this).attr('maxlength') + ')</span>');
            });
        }
        function DarFormato() {
            $('fieldset.entrada').find('input:text').not($('.trama')).each(function () {
                $(this).parent().find('i,span').remove();
                if ($(this).attr('format') != undefined)
                    $(this).after(' <i>' + $(this).attr('format') + '</i>');
                if ($(this).attr('maxlength') != undefined)
                    $(this).after('<span>(' + $(this).attr('maxlength') + ')</span>');
                if ($(this).attr('readonly') == undefined)
                    $(this).css('border-color', 'Blue');
            });
            $('fieldset.salida').find('input:text').not($('.trama')).each(function () {
                if ($(this).val() != '' && $(this).attr('maxlength') != $(this).val().length)
                    $(this).css('border-color', 'Red');
            });
        }
        function Ensamblar(selector) {
            var trama = $(selector).find('.trama');
            trama.val('');
            for (i = 1; i <= $(selector).find('input:text').not($('.trama')).length; i++) {
                var campo = $(selector).find("input:text[tabindex='" + i + "']");
                trama.val(trama.val() + "" + campo.val());
            }
        }
        function Desensamblar(selector) {
            var trama = $(selector).find('.trama');
            var indice = 0;
            for (i = 1; i <= $(selector).find('input:text').not($('.trama')).length; i++) {
                var campo = $(selector).find("input:text[tabindex='" + i + "']");
                campo.val(trama.val().substring(indice, indice + parseInt(campo.attr('maxlength'))));
                indice = indice + parseInt(campo.attr('maxlength'));
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlValidar" runat="server">
        <asp:TextBox ID="txtCodigoSeguridad" runat="server" TextMode="Password" Width="400"></asp:TextBox>
        <asp:Button ID="btnValidar" runat="server" Text="Validar" />
    </asp:Panel>
    <asp:Panel ID="pnlOperaciones" runat="server" Visible="false">
        <div>
            <center>
                <asp:Label ID="lblTipoOperacion" runat="server" Text="Tipo de operaci�n:" Font-Bold="true"
                    AssociatedControlID="ddlTipoOperacion"></asp:Label>
                <asp:DropDownList ID="ddlTipoOperacion" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="Consulta (310000 - 0200)" Value="310000,0200"></asp:ListItem>
                    <asp:ListItem Text="Pago (210000 - 0200)" Value="210000,0200"></asp:ListItem>
                    <asp:ListItem Text="Anulaci�n (220000 - 0200)" Value="220000,0200"></asp:ListItem>
                    <asp:ListItem Text="Extorno Pago (210000 - 0400)" Value="210000,0400"></asp:ListItem>
                    <asp:ListItem Text="Extorno Anulaci�n (220000 - 0400)" Value="220000,0400"></asp:ListItem>
                </asp:DropDownList>
            </center>
            <br />
            <asp:Panel ID="pnlConsultar" runat="server">
                <fieldset class="entrada">
                    <legend>Consulta de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtConsultaEntrada" runat="server" class="trama"
                            maxlength="334" /></dd></dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd>
                            <input type="text" id="consinMessageTypeIdentification" runat="server" tabindex="1"
                                value="0200" maxlength="4" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd>
                            <input type="text" id="consinPrimaryBitMap" runat="server" tabindex="2" value="F038048188E08000"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd>
                            <input type="text" id="consinSecondaryBitMap" runat="server" tabindex="3" value="0000000000000080"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Primary Account Number:</dt>
                        <dd>
                            <input type="text" id="consinPrimaryAccountNumber" runat="server" tabindex="4" value="1600000000000000000"
                                maxlength="19" readonly="readonly" /></dd>
                        <dt>Processing Code:</dt>
                        <dd>
                            <input type="text" id="consinProcessingCode" runat="server" tabindex="5" value="310000"
                                maxlength="6" readonly="readonly" /></dd>
                        <dt>Amount Transaction:</dt>
                        <dd>
                            <input type="text" id="consinAmountTransaction" runat="server" tabindex="6" maxlength="12"
                                value="000000000000" title="Monto" readonly="readonly" /></dd>
                        <dt>Trace</dt>
                        <dd>
                            <input type="text" id="consinTrace" runat="server" tabindex="7" maxlength="6" value="123456"
                                readonly="readonly" /></dd>
                        <dt>Time Local Transaction:</dt>
                        <dd>
                            <input type="text" id="consinTimeLocalTransaction" runat="server" tabindex="8" maxlength="6"
                                format="hhmmss" readonly="readonly" /></dd>
                        <dt>Date Local Transaction:</dt>
                        <dd>
                            <input type="text" id="consinDateLocalTransaction" runat="server" tabindex="9" maxlength="8"
                                format="ddmmaaaa" readonly="readonly" /></dd>
                        <dt>POS Entry Mode:</dt>
                        <dd>
                            <input type="text" id="consinPOSEntryMode" runat="server" tabindex="10" maxlength="3"
                                readonly="readonly" value="010" /></dd>
                        <dt>POS Condition Mode:</dt>
                        <dd>
                            <input type="text" id="consinPOSConditionMode" runat="server" tabindex="11" maxlength="2"
                                value="10" title="C�digo de Canal"/></dd>
                        <dt>Acquirer Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="consinAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                maxlength="8" readonly="readonly" value="20001000" /></dd>
                        <dt>Forward Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="consinForwardInstitutionIDCode" runat="server" tabindex="13"
                                maxlength="8" readonly="readonly" value="10013003" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd>
                            <input type="text" id="consinRetrievalReferenceNumber" runat="server" tabindex="14"
                                maxlength="12" title="N�mero de operaci�n" value="545127490045" readonly="readonly" /></dd>
                        <dt>Card Acceptor Terminal ID:</dt>
                        <dd>
                            <input type="text" id="consinCardAcceptorTerminalID" runat="server" tabindex="15"
                                maxlength="8" readonly="readonly" value="00000379" /></dd>
                        <dt>Card Acceptor ID Code:</dt>
                        <dd>
                            <input type="text" id="consinCardAcceptorIDCode" runat="server" tabindex="16" maxlength="15"
                                title="C�digo de agencia" value="000000000001085" readonly="readonly" /></dd>
                        <dt>Card Acceptor Name Location:</dt>
                        <dd>
                            <input type="text" id="consinCardAcceptorNameLocation" runat="server" tabindex="17"
                                maxlength="40" readonly="readonly" value="Av. Carlos Villaran 140, Santa Catalina " /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="consinTransactionCurrencyCode" runat="server" tabindex="18"
                                maxlength="3" readonly="readonly" value="000" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="consinLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" value="0016" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="consinCIP" runat="server" tabindex="20" maxlength="14" title="CIP" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="consinCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    title="C�digo de servicio" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarConsulta" runat="server" Text="Consultar" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta a Consulta</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtConsultaSalida" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt><dd><input type="text" id="consoutMessageTypeIdentification"
                            runat="server" tabindex="1" maxlength="4" readonly="readonly" /></dd><dt>Primary Bit
                                Map:</dt><dd><input type="text" id="consoutPrimaryBitMap" runat="server" tabindex="2"
                                    maxlength="16" readonly="readonly" /></dd><dt>Secondary Bit Map:</dt><dd><input type="text"
                                        id="consoutSecondaryBitMap" runat="server" tabindex="3" maxlength="16" readonly="readonly" /></dd><dt>Primary
                                            Account Number:</dt><dd><input type="text" id="consoutPrimaryAccountNumber" runat="server"
                                                tabindex="4" maxlength="19" readonly="readonly" /></dd><dt>Processing Code:</dt><dd><input
                                                    type="text" id="consoutProcessingCode" runat="server" tabindex="5" maxlength="6"
                                                    readonly="readonly" /></dd><dt>Amount Transaction:</dt><dd><input type="text" id="consoutAmountTransaction"
                                                        runat="server" tabindex="6" maxlength="12" readonly="readonly" /></dd><dt>Trace:</dt><dd><input
                                                            type="text" id="consoutTrace" runat="server" tabindex="7" maxlength="6" readonly="readonly" /></dd><dt>Time
                                                                Local Transaction:</dt><dd><input type="text" id="consoutTimeLocalTransaction" runat="server"
                                                                    tabindex="8" maxlength="6" readonly="readonly" /></dd><dt>Date Local Transaction:</dt><dd><input
                                                                        type="text" id="consoutDateLocalTransaction" runat="server" tabindex="9" maxlength="8"
                                                                        readonly="readonly" /></dd><dt>POS Entry Mode:</dt><dd><input type="text" id="consoutPOSEntryMode"
                                                                            runat="server" tabindex="10" maxlength="3" readonly="readonly" /></dd><dt>POS Condition
                                                                                Code:</dt><dd><input type="text" id="consoutPOSConditionCode" runat="server" tabindex="11"
                                                                                    maxlength="2" readonly="readonly" /></dd><dt>Acquirer Institution ID Code:</dt><dd><input
                                                                                        type="text" id="consoutAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                                                                        maxlength="8" readonly="readonly" /></dd><dt>Forward Institution ID Code:</dt><dd><input
                                                                                            type="text" id="consoutForwardInstitutionIDCode" runat="server" tabindex="13"
                                                                                            maxlength="8" readonly="readonly" /></dd><dt>Retrieval Reference Number:</dt><dd><input
                                                                                                type="text" id="consoutRetrievalReferenceNumber" runat="server" tabindex="14"
                                                                                                maxlength="12" readonly="readonly" /></dd><dt>Approval Code:</dt><dd><input type="text"
                                                                                                    id="consoutApprovalCode" runat="server" tabindex="15" maxlength="6" readonly="readonly" /></dd><dt>Response
                                                                                                        Code:</dt><dd><input type="text" id="consoutResponseCode" runat="server" tabindex="16"
                                                                                                            maxlength="2" readonly="readonly" /></dd><dt>Card Acceptor Terminal ID:</dt><dd><input
                                                                                                                type="text" id="consoutCardAcceptorTerminalID" runat="server" tabindex="17" maxlength="8"
                                                                                                                readonly="readonly" /></dd><dt>Transaction Currency Code:</dt><dd><input type="text"
                                                                                                                    id="consoutTransactionCurrencyD" runat="server" tabindex="18" maxlength="3" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="consoutLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="consoutCIP" runat="server" tabindex="20" maxlength="14" readonly="readonly" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="consoutCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="consoutCodigoRetorno" runat="server" tabindex="22" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>Descripci�n de c�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="consoutDescripcionCodigoRetorno" runat="server" tabindex="23"
                                    maxlength="30" readonly="readonly" /></dd>
                            <dt>Nombre / Raz�n Social:</dt>
                            <dd>
                                <input type="text" id="consoutNombreRazonSocial" runat="server" tabindex="24" maxlength="40"
                                    readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlPagar" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Pagar CIPs</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtPagoEntrada" runat="server" class="trama"
                            maxlength="334" /></dd></dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt><dd>
                            <input type="text" id="paginMessageTypeIdentification" runat="server" tabindex="1"
                                value="0200" maxlength="4" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd>
                            <input type="text" id="paginPrimaryBitMap" runat="server" tabindex="2" value="F038048188E08000"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd>
                            <input type="text" id="paginSecondaryBitmap" runat="server" tabindex="3" value="0000000000000080"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Primary Account Number:</dt>
                        <dd>
                            <input type="text" id="paginPrimaryAccountNumber" runat="server" tabindex="4" value="0000000000000000000"
                                maxlength="19" readonly="readonly" /></dd>
                        <dt>Processing Code:</dt>
                        <dd>
                            <input type="text" id="paginProcessingCode" runat="server" tabindex="5" value="210000"
                                maxlength="6" readonly="readonly" /></dd>
                        <dt>Amount Transaction:</dt>
                        <dd>
                            <input type="text" id="paginAmountTransaction" runat="server" tabindex="6" maxlength="12"
                                value="000000000000" title="Monto" /></dd>
                        <dt>Trace</dt>
                        <dd>
                            <input type="text" id="paginTrace" runat="server" tabindex="7" maxlength="6" value="123456"
                                readonly="readonly" /></dd>
                        <dt>Time Local Transaction:</dt>
                        <dd>
                            <input type="text" id="paginTimeLocalTransaction" runat="server" tabindex="8" maxlength="6"
                                format="hhmmss" readonly="readonly" /></dd>
                        <dt>Date Local Transaction:</dt>
                        <dd>
                            <input type="text" id="paginDateLocalTransaction" runat="server" tabindex="9" maxlength="8"
                                format="ddmmaaaa" readonly="readonly" /></dd>
                        <dt>POS Entry Mode:</dt>
                        <dd>
                            <input type="text" id="paginPOSEntryMode" runat="server" tabindex="10" maxlength="3"
                                readonly="readonly" value="010" /></dd>
                        <dt>POS Condition Mode:</dt>
                        <dd>
                            <input type="text" id="paginPOSConditionMode" runat="server" tabindex="11" maxlength="2" value="10" /></dd>
                        <dt>Acquirer Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="paginAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                maxlength="8" readonly="readonly" value="20001000" /></dd>
                        <dt>Forward Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="paginForwardInstitutionIDCode" runat="server" tabindex="13"
                                maxlength="8" readonly="readonly" value="10013003" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd>
                            <input type="text" id="paginRetrievalReferenceNumber" runat="server" tabindex="14"
                                maxlength="12" title="N�mero de operaci�n" value="000989300759" /></dd>
                        <dt>Card Acceptor Terminal ID:</dt>
                        <dd>
                            <input type="text" id="paginCardAcceptorTerminalID" runat="server" tabindex="15"
                                maxlength="8" readonly="readonly" value="00000379" /></dd>
                        <dt>Card Acceptor ID Code:</dt>
                        <dd>
                            <input type="text" id="paginCardAcceptorIDCode" runat="server" tabindex="16" maxlength="15"
                                title="C�digo de agencia" value="000000000001085" /></dd>
                        <dt>Card Acceptor Name Location:</dt>
                        <dd>
                            <input type="text" id="paginCardAcceptorNameLocation" runat="server" tabindex="17"
                                maxlength="40" readonly="readonly" value="Av. Carlos Villaran 140, Santa Catalina " /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="paginTransactionCurrencyCode" runat="server" tabindex="18"
                                maxlength="3" value="604" title="Moneda" format="604:Soles, 840:D�lares" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="paginLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" value="0016" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="paginCIP" runat="server" tabindex="20" maxlength="14" title="CIP" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="paginCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    title="C�digo de servicio" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarPago" runat="server" Text="Pagar" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta a Pago</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtPagoSalida" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt><dd><input type="text" id="pagoutMessageTypeIdentification"
                            runat="server" tabindex="1" maxlength="4" readonly="readonly" /></dd><dt>Primary Bit
                                Map:</dt><dd><input type="text" id="pagoutPrimaryBitMap" runat="server" tabindex="2"
                                    maxlength="16" readonly="readonly" /></dd><dt>Secondary Bit Map:</dt><dd><input type="text"
                                        id="pagoutSecondaryBitMap" runat="server" tabindex="3" maxlength="16" readonly="readonly" /></dd><dt>Primary
                                            Account Number:</dt><dd><input type="text" id="pagoutPrimaryAccountNumber" runat="server"
                                                tabindex="4" maxlength="19" readonly="readonly" /></dd><dt>Processing Code:</dt><dd><input
                                                    type="text" id="pagoutProcessingCode" runat="server" tabindex="5" maxlength="6"
                                                    readonly="readonly" /></dd><dt>Amount Transaction:</dt><dd><input type="text" id="pagoutAmountTransaction"
                                                        runat="server" tabindex="6" maxlength="12" readonly="readonly" /></dd><dt>Trace:</dt><dd><input
                                                            type="text" id="pagoutTrace" runat="server" tabindex="7" maxlength="6" readonly="readonly" /></dd><dt>Time
                                                                Local Transaction:</dt><dd><input type="text" id="pagoutTimeLocalTransaction" runat="server"
                                                                    tabindex="8" maxlength="6" readonly="readonly" /></dd><dt>Date Local Transaction:</dt><dd><input
                                                                        type="text" id="pagoutDateLocalTransaction" runat="server" tabindex="9" maxlength="8"
                                                                        readonly="readonly" /></dd><dt>POS Entry Mode:</dt><dd><input type="text" id="pagoutPOSEntryMode"
                                                                            runat="server" tabindex="10" maxlength="3" readonly="readonly" /></dd><dt>POS Condition
                                                                                Code:</dt><dd><input type="text" id="pagoutPOSConditionMode" runat="server" tabindex="11"
                                                                                    maxlength="2" readonly="readonly" /></dd><dt>Acquirer Institution ID Code:</dt><dd><input
                                                                                        type="text" id="pagoutAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                                                                        maxlength="8" readonly="readonly" /></dd><dt>Forward Institution ID Code:</dt><dd><input
                                                                                            type="text" id="pagoutForwardInstitutionIDCode" runat="server" tabindex="13"
                                                                                            maxlength="8" readonly="readonly" /></dd><dt>Retrieval Reference Number:</dt><dd><input
                                                                                                type="text" id="pagoutRetrievalReferenceNumber" runat="server" tabindex="14"
                                                                                                maxlength="12" readonly="readonly" /></dd><dt>Approval Code:</dt><dd><input type="text"
                                                                                                    id="pagoutAprovalCode" runat="server" tabindex="15" maxlength="6" readonly="readonly" /></dd><dt>Response
                                                                                                        Code:</dt><dd><input type="text" id="pagoutResponseCode" runat="server" tabindex="16"
                                                                                                            maxlength="2" readonly="readonly" /></dd><dt>Card Acceptor Terminal ID:</dt><dd><input
                                                                                                                type="text" id="pagoutCardAcceptorTerminalID" runat="server" tabindex="17" maxlength="8"
                                                                                                                readonly="readonly" /></dd><dt>Transaction Currency Code:</dt><dd><input type="text"
                                                                                                                    id="pagoutTransactionCurrencyCode" runat="server" tabindex="18" maxlength="3"
                                                                                                                    readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="pagoutLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="pagoutCIP" runat="server" tabindex="20" maxlength="14" readonly="readonly" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="pagoutCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="pagoutCodigoRetorno" runat="server" tabindex="22" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>Descripci�n de c�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="pagoutDescripcionCodigoRetorno" runat="server" tabindex="23"
                                    maxlength="30" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlAnular" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Anulaci�n de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtAnulacionEntrada" runat="server" class="trama"
                            maxlength="334" /></dd></dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd>
                            <input type="text" id="anuinMessageTypeIdentification" runat="server" tabindex="1"
                                value="0200" maxlength="4" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd>
                            <input type="text" id="anuinPrimaryBitMap" runat="server" tabindex="2" value="F038048188E08000"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd>
                            <input type="text" id="anuinSecondaryBitMap" runat="server" tabindex="3" value="0000000000000080"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Primary Account Number:</dt>
                        <dd>
                            <input type="text" id="anuinPrimaryAccountNumber" runat="server" tabindex="4" value="0000000000000000000"
                                maxlength="19" readonly="readonly" /></dd>
                        <dt>Processing Code:</dt>
                        <dd>
                            <input type="text" id="anuinProcessingCode" runat="server" tabindex="5" value="220000"
                                maxlength="6" readonly="readonly" /></dd>
                        <dt>Amount Transaction:</dt>
                        <dd>
                            <input type="text" id="anuinAmountTransaction" runat="server" tabindex="6" maxlength="12"
                                value="000000000000" title="Monto" /></dd>
                        <dt>Trace:</dt>
                        <dd>
                            <input type="text" id="anuinTrace" runat="server" tabindex="7" maxlength="6" value="123456"
                                readonly="readonly" /></dd>
                        <dt>Time Local Transaction:</dt>
                        <dd>
                            <input type="text" id="anuinTimeLocalTransaction" runat="server" tabindex="8" maxlength="6"
                                format="hhmmss" readonly="readonly" /></dd>
                        <dt>Date Local Transaction:</dt>
                        <dd>
                            <input type="text" id="anuinDateLocalTransaction" runat="server" tabindex="9" maxlength="8"
                                format="ddmmaaaa" readonly="readonly" /></dd>
                        <dt>POS Entry Mode:</dt>
                        <dd>
                            <input type="text" id="anuinPOSEntryMode" runat="server" tabindex="10" maxlength="3"
                                readonly="readonly" value="010" /></dd>
                        <dt>POS Condition Mode:</dt>
                        <dd>
                            <input type="text" id="anuinPOSConditionMode" runat="server" tabindex="11" maxlength="2"
                                readonly="readonly" value="10" /></dd>
                        <dt>Acquirer Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="anuinAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                maxlength="8" readonly="readonly" value="20001000" /></dd>
                        <dt>Forward Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="anuinForwardInstitutionIDCode" runat="server" tabindex="13"
                                maxlength="8" readonly="readonly" value="10013003" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd>
                            <input type="text" id="anuinRetrievalReferenceNumber" runat="server" tabindex="14"
                                maxlength="12" title="N�mero de operaci�n" value="000989300759" /></dd>
                        <dt>Approval Code:</dt>
                        <dd>
                            <input type="text" id="anuinApprovalCode" runat="server" tabindex="15" maxlength="6"
                                readonly="readonly" value="123456" /></dd>
                        <dt>Card Acceptor Terminal ID:</dt>
                        <dd>
                            <input type="text" id="anuinCardAcceptorTerminalID" runat="server" tabindex="16"
                                maxlength="8" readonly="readonly" value="00000379" /></dd>
                        <dt>Card Acceptor ID Code:</dt>
                        <dd>
                            <input type="text" id="anuinCardAcceptorIDCode" runat="server" tabindex="17" maxlength="15"
                                title="C�digo de agencia" value="000000000001085" /></dd>
                        <dt>Card Acceptor Name Location:</dt>
                        <dd>
                            <input type="text" id="anuinCardAcceptorNameLocation" runat="server" tabindex="18"
                                maxlength="40" readonly="readonly" value="Av. Carlos Villaran 140, Santa Catalina " /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="anuinTransactionCurrencyCode" runat="server" tabindex="19"
                                maxlength="3" value="604" title="Moneda" format="604:Soles, 840:D�lares" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="anuinLongitudCampo" runat="server" tabindex="20" maxlength="4"
                                    readonly="readonly" value="0028" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="anuinCIP" runat="server" tabindex="21" maxlength="14" title="CIP" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="anuinCodigoServicio" runat="server" tabindex="22" maxlength="2"
                                    title="C�digo de servicio" /></dd>
                            <dt>N�mero de operaci�n Pago:</dt>
                            <dd>
                                <input type="text" id="anuinNumeroOperacionPago" runat="server" tabindex="23" maxlength="12"
                                    title="N�mero de operaci�n de pago" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarAnulacion" runat="server" Text="Anular" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta a anulaci�n</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtAnulacionSalida" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt><dd><input type="text" id="anuoutMessageTypeIdentification"
                            runat="server" tabindex="1" maxlength="4" readonly="readonly" /></dd><dt>Primary Bit
                                Map:</dt><dd><input type="text" id="anuoutPrimaryBitMap" runat="server" tabindex="2"
                                    maxlength="16" readonly="readonly" /></dd><dt>Secondary Bit Map:</dt><dd><input type="text"
                                        id="anuoutSecondaryBitMap" runat="server" tabindex="3" maxlength="16" readonly="readonly" /></dd><dt>Primary
                                            Account Number:</dt><dd><input type="text" id="anuoutPrimaryAccountNumber" runat="server"
                                                tabindex="4" maxlength="19" readonly="readonly" /></dd><dt>Processing Code:</dt><dd><input
                                                    type="text" id="anuoutProcessingCode" runat="server" tabindex="5" maxlength="6"
                                                    readonly="readonly" /></dd><dt>Amount Transaction:</dt><dd><input type="text" id="anuoutAmountTransaction"
                                                        runat="server" tabindex="6" maxlength="12" readonly="readonly" /></dd><dt>Trace:</dt><dd><input
                                                            type="text" id="anuoutTrace" runat="server" tabindex="7" maxlength="6" readonly="readonly" /></dd><dt>Time
                                                                Local Transaction:</dt><dd><input type="text" id="anuoutTimeLocalTransaction" runat="server"
                                                                    tabindex="8" maxlength="6" readonly="readonly" /></dd><dt>Date Local Transaction:</dt><dd><input
                                                                        type="text" id="anuoutDateLocalTransaction" runat="server" tabindex="9" maxlength="8"
                                                                        readonly="readonly" /></dd><dt>POS Entry Mode:</dt><dd><input type="text" id="anuoutPOSEntryMode"
                                                                            runat="server" tabindex="10" maxlength="3" readonly="readonly" /></dd><dt>POS Condition
                                                                                Code:</dt><dd><input type="text" id="anuoutPOSConditionCode" runat="server" tabindex="11"
                                                                                    maxlength="2" readonly="readonly" /></dd><dt>Acquirer Institution ID Code:</dt><dd><input
                                                                                        type="text" id="anuoutAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                                                                        maxlength="8" readonly="readonly" /></dd><dt>Forward Institution ID Code:</dt><dd><input
                                                                                            type="text" id="anuoutForwardInstitutionIDCode" runat="server" tabindex="13"
                                                                                            maxlength="8" readonly="readonly" /></dd><dt>Retrieval Reference Number:</dt><dd><input
                                                                                                type="text" id="anuoutRetrievalReferenceNumber" runat="server" tabindex="14"
                                                                                                maxlength="12" readonly="readonly" /></dd><dt>Approval Code:</dt><dd><input type="text"
                                                                                                    id="anuoutApprovalCode" runat="server" tabindex="15" maxlength="6" readonly="readonly" /></dd><dt>Response
                                                                                                        Code:</dt><dd><input type="text" id="anuoutResponseCode" runat="server" tabindex="16"
                                                                                                            maxlength="2" readonly="readonly" /></dd><dt>Card Acceptor Terminal ID:</dt><dd><input
                                                                                                                type="text" id="anuoutCardAcceptorTerminalID" runat="server" tabindex="17" maxlength="8"
                                                                                                                readonly="readonly" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="anuoutTransactionCurrencyCode" runat="server" tabindex="18"
                                maxlength="3" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="anuoutLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="anuoutCIP" runat="server" tabindex="20" maxlength="14" readonly="readonly" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="anuoutCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="anuoutCodigoRetorno" runat="server" tabindex="22" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>Descripci�n de c�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="anuoutDescripcionCodigoRetorno" runat="server" tabindex="23"
                                    maxlength="30" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlExtornarPago" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Extorno autom�tico de pago de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtExtornoPagoEntrada" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd>
                            <input type="text" id="pagautinMessageTypeIdentification" runat="server" tabindex="1"
                                value="0400" maxlength="4" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd>
                            <input type="text" id="pagautinPrimaryBitMap" runat="server" tabindex="2" value="F038048188E08000"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd>
                            <input type="text" id="pagautinSecondaryBitMap" runat="server" tabindex="3" value="0000000000000080"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Primary Account Number:</dt>
                        <dd>
                            <input type="text" id="pagautinPrimaryAccountNumber" runat="server" tabindex="4"
                                value="0000000000000000000" maxlength="19" readonly="readonly" /></dd>
                        <dt>Processing Code:</dt>
                        <dd>
                            <input type="text" id="pagautinProcessingCode" runat="server" tabindex="5" value="210000"
                                maxlength="6" readonly="readonly" /></dd>
                        <dt>Amount Transaction:</dt>
                        <dd>
                            <input type="text" id="pagautinAmountTransaction" runat="server" tabindex="6" maxlength="12"
                                value="000000000000" title="Monto" /></dd>
                        <dt>Trace:</dt>
                        <dd>
                            <input type="text" id="pagautinTrace" runat="server" tabindex="7" maxlength="6" value="123456"
                                readonly="readonly" /></dd>
                        <dt>Time Local Transaction:</dt>
                        <dd>
                            <input type="text" id="pagautinTimeLocaltransaction" runat="server" tabindex="8"
                                maxlength="6" format="hhmmss" readonly="readonly" /></dd>
                        <dt>Date Local Transaction:</dt>
                        <dd>
                            <input type="text" id="pagautinDateLocalTransaction" runat="server" tabindex="9"
                                maxlength="8" format="ddmmaaaa" readonly="readonly" /></dd>
                        <dt>POS Entry Mode:</dt>
                        <dd>
                            <input type="text" id="pagautinPOSEntryMode" runat="server" tabindex="10" maxlength="3"
                                readonly="readonly" value="010" /></dd>
                        <dt>POS Condition Mode:</dt>
                        <dd>
                            <input type="text" id="pagautinPOSConditionMode" runat="server" tabindex="11" maxlength="2"
                                readonly="readonly" value="10" /></dd>
                        <dt>Acquirer Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="pagautinAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                maxlength="8" readonly="readonly" value="20001000" /></dd>
                        <dt>Forward Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="pagautinForwardInstitutionIDCode" runat="server" tabindex="13"
                                maxlength="8" readonly="readonly" value="10013003" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd>
                            <input type="text" id="pagautinRetrievalReferenceNumber" runat="server" tabindex="14"
                                maxlength="12" title="N�mero de operaci�n" value="000989300759" /></dd>
                        <dt>Card Acceptor Terminal ID:</dt>
                        <dd>
                            <input type="text" id="pagautinCardAcceptorTerminalID" runat="server" tabindex="15"
                                maxlength="8" readonly="readonly" value="00000379" /></dd>
                        <dt>Card Acceptor ID Code:</dt>
                        <dd>
                            <input type="text" id="pagautinCardAcceptorIDCode" runat="server" tabindex="16" maxlength="15"
                                title="C�digo de agencia" value="000000000001085" /></dd>
                        <dt>Card Acceptor Name Location:</dt>
                        <dd>
                            <input type="text" id="pagautinCardAcceptorNameLocation" runat="server" tabindex="17"
                                maxlength="40" readonly="readonly" value="Av. Carlos Villaran 140, Santa Catalina " /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="pagautinTrasactionCurrencyCode" runat="server" tabindex="18"
                                maxlength="3" readonly="readonly" value="604" format="604:Soles, 840:D�lares" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="pagautinLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" value="0016" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="pagautinCIP" runat="server" tabindex="20" maxlength="14" title="CIP" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="pagautinCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    title="C�digo de servicio" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarPagoAuto" runat="server" Text="Extornar Pago" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta de extorno autom�tico de pago</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtExtornoPagoSalida" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt><dd><input type="text" id="pagautoutMessageTypeIdentification"
                            runat="server" tabindex="1" maxlength="4" readonly="readonly" /></dd><dt>Primary Bit
                                Map:</dt><dd><input type="text" id="pagautoutPrimaryBitMap" runat="server" tabindex="2"
                                    maxlength="16" readonly="readonly" /></dd><dt>Secondary Bit Map:</dt><dd><input type="text"
                                        id="pagautoutSecondaryBitMap" runat="server" tabindex="3" maxlength="16" readonly="readonly" /></dd><dt>Primary
                                            Account Number:</dt><dd><input type="text" id="pagautoutPrimaryAccountNumber" runat="server"
                                                tabindex="4" maxlength="19" readonly="readonly" /></dd><dt>Processing Code:</dt><dd><input
                                                    type="text" id="pagautoutProcessingCode" runat="server" tabindex="5" maxlength="6"
                                                    readonly="readonly" /></dd><dt>Amount Transaction:</dt><dd><input type="text" id="pagautoutAmountTransaction"
                                                        runat="server" tabindex="6" maxlength="12" readonly="readonly" /></dd><dt>Trace:</dt><dd><input
                                                            type="text" id="pagautoutTrace" runat="server" tabindex="7" maxlength="6" readonly="readonly" /></dd><dt>Time
                                                                Local Transaction:</dt><dd><input type="text" id="pagautoutTimeLocalTransaction"
                                                                    runat="server" tabindex="8" maxlength="6" readonly="readonly" /></dd><dt>Date Local
                                                                        Transaction:</dt><dd><input type="text" id="pagautoutDateLocalTransaction" runat="server"
                                                                            tabindex="9" maxlength="8" readonly="readonly" /></dd><dt>POS Entry Mode:</dt><dd><input
                                                                                type="text" id="pagautoutPOSEntryMode" runat="server" tabindex="10" maxlength="3"
                                                                                readonly="readonly" /></dd><dt>POS Condition Code:</dt><dd><input type="text" id="pagautoutPOSConditionCode"
                                                                                    runat="server" tabindex="11" maxlength="2" readonly="readonly" /></dd><dt>Acquirer Institution
                                                                                        ID Code:</dt><dd><input type="text" id="pagautoutAcquirerInstitutionIDCode" runat="server"
                                                                                            tabindex="12" maxlength="8" readonly="readonly" /></dd><dt>Forward Institution ID Code:</dt><dd><input
                                                                                                type="text" id="pagautoutForwardInstitutionIDCode" runat="server" tabindex="13"
                                                                                                maxlength="8" readonly="readonly" /></dd><dt>Retrieval Reference Number:</dt><dd><input
                                                                                                    type="text" id="pagautoutRetrievalReferenceNumber" runat="server" tabindex="14"
                                                                                                    maxlength="12" readonly="readonly" /></dd><dt>Approval Code:</dt><dd><input type="text"
                                                                                                        id="pagautoutApprovalCode" runat="server" tabindex="15" maxlength="6" readonly="readonly" /></dd><dt>Response
                                                                                                            Code:</dt><dd><input type="text" id="pagautoutResponseCode" runat="server" tabindex="16"
                                                                                                                maxlength="2" readonly="readonly" /></dd><dt>Card Acceptor Terminal ID:</dt><dd><input
                                                                                                                    type="text" id="pagautoutCardAcceptorTerminalID" runat="server" tabindex="17"
                                                                                                                    maxlength="8" readonly="readonly" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="pagautoutTransactionCurrencyCode" runat="server" tabindex="18"
                                maxlength="3" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="pagautoutLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="pagautoutCIP" runat="server" tabindex="20" maxlength="14"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="pagautoutCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="pagautoutCodigoRetorno" runat="server" tabindex="22" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>Descripci�n de c�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="pagautoutDescripcionCodigoRetorno" runat="server" tabindex="23"
                                    maxlength="30" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlExtornarAnulacion" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Extorno autom�tico de anulaci�n de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtExtornoAnulacionEntrada" class="trama"
                            runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd>
                            <input type="text" id="anuautinMessageTypeIdentification" runat="server" tabindex="1"
                                value="0400" maxlength="4" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd>
                            <input type="text" id="anuautinPrimaryBitMap" runat="server" tabindex="2" value="F038048188E08000"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd>
                            <input type="text" id="anuautinSecondaryBitMap" runat="server" tabindex="3" value="0000000000000080"
                                maxlength="16" readonly="readonly" /></dd>
                        <dt>Primary Account Number:</dt>
                        <dd>
                            <input type="text" id="anuautinPrimaryAccountNumber" runat="server" tabindex="4"
                                value="0000000000000000000" maxlength="19" readonly="readonly" /></dd>
                        <dt>Processing Code:</dt>
                        <dd>
                            <input type="text" id="anuautinProcessingCode" runat="server" tabindex="5" value="220000"
                                maxlength="6" readonly="readonly" /></dd>
                        <dt>Amount Transaction:</dt>
                        <dd>
                            <input type="text" id="anuautinAmountTransaction" runat="server" tabindex="6" maxlength="12"
                                value="000000000000" title="Moneda" /></dd>
                        <dt>Trace:</dt>
                        <dd>
                            <input type="text" id="anuautinTrace" runat="server" tabindex="7" maxlength="6" value="123456"
                                readonly="readonly" /></dd>
                        <dt>Time Local Transaction:</dt>
                        <dd>
                            <input type="text" id="anuautinTimeLocalTransaction" runat="server" tabindex="8"
                                maxlength="6" format="hhmmss" readonly="readonly" /></dd>
                        <dt>Date Local Transaction:</dt>
                        <dd>
                            <input type="text" id="anuautinDateLocalTransaction" runat="server" tabindex="9"
                                maxlength="8" format="ddmmaaaa" readonly="readonly" /></dd>
                        <dt>POS Entry Mode:</dt>
                        <dd>
                            <input type="text" id="anuautinPOSEntryMode" runat="server" tabindex="10" maxlength="3"
                                readonly="readonly" value="010" /></dd>
                        <dt>POS Condition Mode:</dt>
                        <dd>
                            <input type="text" id="anuautinPOSConditionMode" runat="server" tabindex="11" maxlength="2"
                                readonly="readonly" value="10" /></dd>
                        <dt>Acquirer Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="anuautinAcquirerInstitutionIDCode" runat="server" tabindex="12"
                                maxlength="8" readonly="readonly" value="20001000" /></dd>
                        <dt>Forward Institution ID Code:</dt>
                        <dd>
                            <input type="text" id="anuautinForwardInstitutionIDCode" runat="server" tabindex="13"
                                maxlength="8" readonly="readonly" value="10013003" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd>
                            <input type="text" id="anuautinRetrievalReferenceNumber" runat="server" tabindex="14"
                                maxlength="12" title="N�mero de operaci�n" value="000989300759" /></dd>
                        <dt>Approval Code:</dt>
                        <dd>
                            <input type="text" id="anuautinApprovalCode" runat="server" tabindex="15" maxlength="6"
                                readonly="readonly" value="123456" /></dd>
                        <dt>Card Acceptor Terminal ID:</dt>
                        <dd>
                            <input type="text" id="anuautinCardAcceptorTerminalID" runat="server" tabindex="16"
                                maxlength="8" readonly="readonly" value="00000379" /></dd>
                        <dt>Card Acceptor ID Code:</dt>
                        <dd>
                            <input type="text" id="anuautinCardAcceptorIDCode" runat="server" tabindex="17" maxlength="15"
                                title="C�digo de agencia" value="000000000001085" /></dd>
                        <dt>Card Acceptor Name Location:</dt>
                        <dd>
                            <input type="text" id="anuautinCardAcceptorNameLocation" runat="server" tabindex="18"
                                maxlength="40" readonly="readonly" value="Av. Carlos Villaran 140, Santa Catalina " /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="anuautinTransactionCurrencyCode" runat="server" tabindex="19"
                                maxlength="3" readonly="readonly" value="604" format="604:Soles, 840:D�lares" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="anuautinLongitudCampo" runat="server" tabindex="20" maxlength="4"
                                    readonly="readonly" value="0028" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="anuautinCIP" runat="server" tabindex="21" maxlength="14" title="CIP" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="anuautinCodigoServicio" runat="server" tabindex="22" maxlength="2"
                                    title="C�digo de servicio" /></dd>
                            <dt>N�mero de operaci�n Pago:</dt>
                            <dd>
                                <input type="text" id="anuautinNumeroOperacionPago" runat="server" tabindex="23"
                                    maxlength="12" title="N�mero de operaci�n de pago" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarAnulacionAuto" runat="server" Text="Extornar Anulaci�n"
                        CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta de extorno autom�tico de anulaci�n de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="txtExtornoAnulacionSalida" class="trama"
                            runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification: </dt>
                        <dd>
                            <input id="anuautoutMessageTypeIdentification" maxlength="4" readonly="readonly"
                                runat="server" tabindex="1" type="text" />
                        </dd>
                        <dt>Primary Bit Map: </dt>
                        <dd>
                            <input id="anuautoutPrimaryBitMap" maxlength="16" readonly="readonly" runat="server"
                                tabindex="2" type="text" />
                        </dd>
                        <dt>Secondary Bit Map: </dt>
                        <dd>
                            <input id="anuautoutSecondaryBitMap" maxlength="16" readonly="readonly" runat="server"
                                tabindex="3" type="text" />
                        </dd>
                        <dt>Primary Account Number: </dt>
                        <dd>
                            <input id="anuautoutPrimaryAccountNumber" maxlength="19" readonly="readonly" runat="server"
                                tabindex="4" type="text" />
                        </dd>
                        <dt>Processing Code: </dt>
                        <dd>
                            <input id="anuautoutProcessingCode" maxlength="6" readonly="readonly" runat="server"
                                tabindex="5" type="text" />
                        </dd>
                        <dt>Amount Transaction: </dt>
                        <dd>
                            <input id="anuautoutAmountTransaction" maxlength="12" readonly="readonly" runat="server"
                                tabindex="6" type="text" />
                        </dd>
                        <dt>Trace: </dt>
                        <dd>
                            <input id="anuautoutTrace" maxlength="6" readonly="readonly" runat="server" tabindex="7"
                                type="text" />
                        </dd>
                        <dt>Time Local Transaction: </dt>
                        <dd>
                            <input id="anuautoutTimeLocalTransaction" maxlength="6" readonly="readonly" runat="server"
                                tabindex="8" type="text" />
                        </dd>
                        <dt>Date Local Transaction: </dt>
                        <dd>
                            <input id="anuautoutDateLocalTransaction" maxlength="8" readonly="readonly" runat="server"
                                tabindex="9" type="text" />
                        </dd>
                        <dt>POS Entry Mode: </dt>
                        <dd>
                            <input id="anuautoutPOSEntryMode" maxlength="3" readonly="readonly" runat="server"
                                tabindex="10" type="text" />
                        </dd>
                        <dt>POS Condition Code: </dt>
                        <dd>
                            <input id="anuautoutPOSConditionCode" maxlength="2" readonly="readonly" runat="server"
                                tabindex="11" type="text" />
                        </dd>
                        <dt>Acquirer Institution ID Code: </dt>
                        <dd>
                            <input id="anuautoutAcquirerInstitutionIDCode" maxlength="8" readonly="readonly"
                                runat="server" tabindex="12" type="text" />
                        </dd>
                        <dt>Forward Institution ID Code: </dt>
                        <dd>
                            <input id="anuautoutForwardInstitutionIDCode" maxlength="8" readonly="readonly" runat="server"
                                tabindex="13" type="text" />
                        </dd>
                        <dt>Retrieval Reference Number: </dt>
                        <dd>
                            <input id="anuautoutRetrievalReferenceNumber" maxlength="12" readonly="readonly"
                                runat="server" tabindex="14" type="text" />
                        </dd>
                        <dt>Approval Code: </dt>
                        <dd>
                            <input id="anuautoutApprovalCode" maxlength="6" readonly="readonly" runat="server"
                                tabindex="15" type="text" />
                        </dd>
                        <dt>Response Code: </dt>
                        <dd>
                            <input id="anuautoutResponseCode" maxlength="2" readonly="readonly" runat="server"
                                tabindex="16" type="text" />
                        </dd>
                        <dt>Card Acceptor Terminal ID: </dt>
                        <dd>
                            <input id="anuautoutCardAccpeetorTerminalID" maxlength="8" readonly="readonly" runat="server"
                                tabindex="17" type="text" />
                        </dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd>
                            <input type="text" id="anuautoutTransactionCurrencyCode" runat="server" tabindex="18"
                                maxlength="3" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Private Data:</legend>
                        <dl>
                            <dt>Longitud del campo:</dt>
                            <dd>
                                <input type="text" id="anuautoutLongitudCampo" runat="server" tabindex="19" maxlength="4"
                                    readonly="readonly" /></dd>
                            <dt>CIP:</dt>
                            <dd>
                                <input type="text" id="anuautoutCIP" runat="server" tabindex="20" maxlength="14"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd>
                                <input type="text" id="anuautoutCodigoServicio" runat="server" tabindex="21" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>C�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="anuautoutCodigoRetorno" runat="server" tabindex="22" maxlength="2"
                                    readonly="readonly" /></dd>
                            <dt>Descripci�n de c�digo de retorno:</dt>
                            <dd>
                                <input type="text" id="anuautoutDescripcionCodigoProceso" runat="server" tabindex="23"
                                    maxlength="30" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
