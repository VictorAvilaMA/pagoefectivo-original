Imports System.Configuration.ConfigurationManager
Imports ProxyWSIBK
Partial Public Class WSIBKCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EstablecerValoresPredeterminados()
    End Sub

    Private Sub EstablecerValoresPredeterminados()
        consinTimeLocalTransaction.Value = DateTime.Now.ToString("HHmmss")
        consinDateLocalTransaction.Value = DateTime.Now.ToString("ddMMyyyy")

        paginTimeLocalTransaction.Value = DateTime.Now.ToString("HHmmss")
        paginDateLocalTransaction.Value = DateTime.Now.ToString("ddMMyyyy")

        anuinTimeLocalTransaction.Value = DateTime.Now.ToString("HHmmss")
        anuinDateLocalTransaction.Value = DateTime.Now.ToString("ddMMyyyy")

        pagautinTimeLocaltransaction.Value = DateTime.Now.ToString("HHmmss")
        pagautinDateLocalTransaction.Value = DateTime.Now.ToString("ddMMyyyy")

        anuautinTimeLocalTransaction.Value = DateTime.Now.ToString("HHmmss")
        anuautinDateLocalTransaction.Value = DateTime.Now.ToString("ddMMyyyy")
    End Sub

    Private Sub OcultarPaneles()
        pnlConsultar.Visible = False
        pnlPagar.Visible = False
        pnlAnular.Visible = False
        pnlExtornarPago.Visible = False
        pnlExtornarAnulacion.Visible = False
    End Sub

    Protected Sub ddlTipoOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTipoOperacion.SelectedIndexChanged
        OcultarPaneles()
        Select Case ddlTipoOperacion.SelectedValue
            Case "310000,0200"
                pnlConsultar.Visible = True
            Case "210000,0200"
                pnlPagar.Visible = True
            Case "220000,0200"
                pnlAnular.Visible = True
            Case "210000,0400"
                pnlExtornarPago.Visible = True
            Case "220000,0400"
                pnlExtornarAnulacion.Visible = True
        End Select
    End Sub

    Protected Sub btnProcesarConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarConsulta.Click
        Using Proxy As New ProxyWSIBK.Service
            txtConsultaSalida.Value = Proxy.ejecutarTransaccionInterbank(txtConsultaEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarPago.Click
        Using Proxy As New ProxyWSIBK.Service
            txtPagoSalida.Value = Proxy.ejecutarTransaccionInterbank(txtPagoEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarAnulacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarAnulacion.Click
        Using Proxy As New ProxyWSIBK.Service
            txtAnulacionSalida.Value = Proxy.ejecutarTransaccionInterbank(txtAnulacionEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarPagoAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarPagoAuto.Click
        Using Proxy As New ProxyWSIBK.Service
            txtExtornoPagoSalida.Value = Proxy.ejecutarTransaccionInterbank(txtExtornoPagoEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarAnulacionAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarAnulacionAuto.Click
        Using Proxy As New ProxyWSIBK.Service
            txtExtornoAnulacionSalida.Value = Proxy.ejecutarTransaccionInterbank(txtExtornoAnulacionEntrada.Value)
        End Using
    End Sub
    Protected Sub btnValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        pnlValidar.Visible = Not (AppSettings("ValidarTestWSIBKCIP") <> "" And txtCodigoSeguridad.Text = AppSettings("ValidarTestWSIBKCIP"))
        pnlOperaciones.Visible = (AppSettings("ValidarTestWSIBKCIP") <> "" And txtCodigoSeguridad.Text = AppSettings("ValidarTestWSIBKCIP"))
    End Sub
End Class