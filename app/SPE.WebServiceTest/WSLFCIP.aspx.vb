Imports System.Configuration.ConfigurationManager
Imports ProxyWSLF


Partial Public Class WSLFConsultar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GenerarRandom()
    End Sub

    Protected Sub btnValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        pnlVer.Visible = (AppSettings("ValidarTestWSLFCIP") <> "" And txtValidar.Text = AppSettings("ValidarTestWSLFCIP"))
    End Sub

    Protected Sub ddlOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlOperacion.SelectedIndexChanged
        Select Case ddlOperacion.Text
            Case "Consultar"
                pnlConsultar.Visible = True
                pnlCancelar.Visible = False
            Case "Cancelar"
                pnlConsultar.Visible = False
                pnlCancelar.Visible = True
        End Select
        GenerarRandom()
    End Sub

    Protected Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Using Proxy As New ProxyWSLF.Service
            Dim request As New ProxyWSLF.BELFConsultarRequest
            With request
                .CIP = CONtxtCIP.Text
                .CodServicio = CONtxtCodServicio.Text
            End With
            Dim response As ProxyWSLF.BELFConsultarResponse = Proxy.Consultar(request)
            If response IsNot Nothing Then
                CONtxtCIPr.Text = response.CIP
                CONtxtCodResultado.Text = response.CodResultado
                CONtxtMensajeResultado.Text = response.MensajeResultado
                CONtxtMonto.Text = response.Monto
                CONtxtCodMoneda.Text = response.CodMoneda
                CONtxtDni.Text = response.Dni
                CONtxtFechaVencimiento.Text = response.FechaVencimiento
            End If
        End Using
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Using Proxy As New ProxyWSLF.Service
            Dim request As New ProxyWSLF.BELFCancelarRequest
            With request
                .CIP = CANtxtCIP.Text
                .Monto = CANtxtMonto.Text
                .CodMoneda = CANtxtCodMoneda.Text
                .CodServicio = CANtxtCodServicio.Text
            End With
            Dim response As ProxyWSLF.BELFCancelarResponse = Proxy.Cancelar(request)
            If response IsNot Nothing Then
                CANtxtCodResultado.Text = response.CodResultado
                CANtxtMensajeResultado.Text = response.MensajeResultado
                CANtxtCodMovimiento.Text = response.CodMovimiento
                CANtxtCodServicior.Text = response.CodServicio
            End If
        End Using
    End Sub

    Private Sub GenerarRandom()
        'Randomize()
        '' Generate random value between 1 and 6.
        'Dim value As Integer = CInt(Int((6 * Rnd()) + 1))
        'txtNroOperacion.Text = value.ToString()
        Dim value As Integer
        Dim valuestr As String
        Dim randNumber As New Random(DateTime.Now.Millisecond)
        value = randNumber.Next(0, "999999999")
        valuestr = value.ToString("D9")
    End Sub

End Class