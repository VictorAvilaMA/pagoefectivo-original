Imports System.Configuration.ConfigurationManager
Imports ProxyWSMoMo

Partial Public Class WSMoMoConsultar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GenerarRandom()
    End Sub

    Protected Sub btnValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        pnlVer.Visible = (AppSettings("ValidarTestWSMoMoCIP") <> "" And txtValidar.Text = AppSettings("ValidarTestWSMoMoCIP"))
    End Sub

    Protected Sub ddlOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlOperacion.SelectedIndexChanged
        Select Case ddlOperacion.Text
            Case "Consultar"
                pnlConsultar.Visible = True
                pnlCancelar.Visible = False
                pnlAnular.Visible = False
            Case "Cancelar"
                pnlConsultar.Visible = False
                pnlCancelar.Visible = True
                pnlAnular.Visible = False
            Case "Anular"
                pnlConsultar.Visible = False
                pnlCancelar.Visible = False
                pnlAnular.Visible = True
        End Select
        GenerarRandom()
    End Sub

    Protected Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Using Proxy As New ProxyWSMoMo.Service1
            Dim request As New ProxyWSMoMo.BEMoMoConsultarRequest
            With request
                .CodPuntoVenta = CONtxtPuntoVenta.Text
                .NroSerieTerminal = CONtxtNroSerieTerminal.Text
                .NroOperacionMoMo = CONtxtNroOperacionFullCarga.Text
                .CIP = CONtxtCIP.Text
                .CodServicio = CONtxtCodServicio.Text
                .CodCliente = CONtxtCodCliente.Text
                .CelularCliente = ContxtCelularCliente.Text
                .CodCanal = ContxtCodCanal.Text
            End With
            Dim response As ProxyWSMoMo.BEMoMoConsultarResponse = Proxy.Consultar(request)
            If response IsNot Nothing Then
                CONtxtCIPr.Text = response.CIP
                CONtxtCodResultado.Text = response.CodResultado
                CONtxtMensajeResultado.Text = response.MensajeResultado
                CONtxtMonto.Text = response.Monto
                CONtxtCodMoneda.Text = response.CodMoneda
                CONtxtConceptoPago.Text = response.ConceptoPago
                CONtxtFechaVencimiento.Text = response.FechaVencimiento
                CONtxtCodServicior.Text = response.CodServicio
            End If
        End Using
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Using Proxy As New ProxyWSMoMo.Service1
            Dim request As New ProxyWSMoMo.BEMoMoCancelarRequest
            With request
                .CodPuntoVenta = CANtxtPuntoVenta.Text
                .NroSerieTerminal = CANtxtNroSerieTerminal.Text
                .NroOperacionMoMo = CANtxtNroOperacionFullCarga.Text
                .CodMedioPago = CANtxtCodMedioPago.Text
                .CIP = CANtxtCIP.Text
                .Monto = CANtxtMonto.Text
                .CodMoneda = CANtxtCodMoneda.Text
                .CodServicio = CANtxtCodServicio.Text
                .CodCliente = CANtxtCodCliente.Text
                .CelularCliente = CANtxtCelularCliente.Text
                .CodCanal = CANtxtCodCanal.Text
            End With
            Dim response As ProxyWSMoMo.BEMoMoCancelarResponse = Proxy.Cancelar(request)
            If response IsNot Nothing Then
                CANtxtCodResultado.Text = response.CodResultado
                CANtxtMensajeResultado.Text = response.MensajeResultado
                CANtxtCodMovimiento.Text = response.CodMovimiento
                CANtxtCodServicior.Text = response.CodServicio
            End If
        End Using
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Using Proxy As New ProxyWSMoMo.Service1
            Dim request As New ProxyWSMoMo.BEMoMoAnularRequest
            With request
                .CodPuntoVenta = ANUtxtPuntoVenta.Text
                .NroSerieTerminal = ANUtxtNroSerieTerminal.Text
                .NroOperacionMoMoPago = ANUtxtNroOperacionFullCargaPago.Text
                .NroOperacionMoMo = ANUtxtNroOperacionFullCarga.Text
                .CodMedioPago = ANUtxtCodMedioPago.Text
                .CIP = ANUtxtCIP.Text
                .Monto = ANUtxtMonto.Text
                .CodMoneda = ANUtxtCodMoneda.Text
                .CodServicio = ANUtxtCodServicio.Text
                .CodCliente = ANUtxtCodCliente.Text
                .CelularCliente = ANUtxtCelularCliente.Text
                .CodCanal = ANUtxtCodCanal.Text
            End With
            Dim response As ProxyWSMoMo.BEMoMoAnularResponse = Proxy.Anular(request)
            If response IsNot Nothing Then
                ANUtxtCodResultado.Text = response.CodResultado
                ANUtxtMensajeResultado.Text = response.MensajeResultado
                ANUtxtCodMovimiento.Text = response.CodMovimiento
                ANUtxtCodServicior.Text = response.CodServicio
            End If
        End Using
    End Sub

    Private Sub GenerarRandom()
        'Randomize()
        '' Generate random value between 1 and 6.
        'Dim value As Integer = CInt(Int((6 * Rnd()) + 1))
        'txtNroOperacion.Text = value.ToString()
        Dim value As Integer
        Dim valuestr As String
        Dim randNumber As New Random(DateTime.Now.Millisecond)
        value = randNumber.Next(0, "999999999")
        valuestr = value.ToString("D9")
        CONtxtNroOperacionFullCarga.Text = valuestr
        CANtxtNroOperacionFullCarga.Text = valuestr
        ANUtxtNroOperacionFullCarga.Text = valuestr
    End Sub

End Class