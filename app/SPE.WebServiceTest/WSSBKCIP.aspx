<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSSBKCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSSBKCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Dummy Scotiabank</title>
    <style type="text/css">
        dl { margin:0;}
        dl dt { float:left; padding:5px; width:275px; }
        dl dd { margin:2px 0; padding:5px 0; }
        fieldset.entrada, fieldset.salida { margin:0; padding:0; float:left; width:49%; }
        legend { font-weight:bold; }
        input.trama { width:95%; }
        
        .tooltip {
	        background-color:#000;
	        border:1px solid #fff;
	        padding:10px 15px;
	        width:200px;
	        display:none;
	        color:#fff;
	        text-align:left;
	        /* font-size:12px;

	        outline radius for mozilla/firefox only */
	        -moz-box-shadow:0 0 10px #000;
	        -webkit-box-shadow:0 0 10px #000;
        }
    </style>
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.tools.min.js" ></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("input[title]").tooltip({
                position: "center right",
                offset: [-2, 10],
                effect: "fade",
                opacity: 0.7
            });
            DarFormatoInicial();
            DarFormato();
            $('.botonentrada').click(function() {
                DarFormato();
                var ok = true;
                $(this).parent().find('input:text').not($('.trama')).each(function() {
                    if ($(this).val().length != $(this).attr('maxlength')){
                        $(this).css('border-color', 'Red');
                        ok = false;
                    }
                });
                if (!ok)
                    alert('!Verificar longitud!')
                else
                    Ensamblar('fieldset.entrada');
                return ok;
            });
            $('.ensamblarentrada').click(function () {
                Ensamblar('fieldset.entrada');
                return false;
            });
            $('.desensamblarentrada').click(function () {
                Desensamblar('fieldset.entrada');
                return false;
            });
            $('.ensamblarsalida').click(function() {
                Ensamblar('fieldset.salida');
                return false;
            });
            $('.desensamblarsalida').click(function() {
                Desensamblar('fieldset.salida');
                return false;
            });
            Desensamblar('fieldset.salida');
        });
        function DarFormatoInicial() {
            $('fieldset.salida').find('input:text').not($('.trama')).each(function(){
                if ($(this).attr('format') != undefined)
                    $(this).after(' <i>' + $(this).attr('format') + '</i>');
                if ($(this).attr('maxlength') != undefined)
                    $(this).after('<span>(' + $(this).attr('maxlength') + ')</span>');
            });
        }
        function DarFormato() {
            $('fieldset.entrada').find('input:text').not($('.trama')).each(function(){
                $(this).parent().find('i,span').remove();
                if ($(this).attr('format') != undefined)
                    $(this).after(' <i>' + $(this).attr('format') + '</i>');
                if ($(this).attr('maxlength') != undefined)
                    $(this).after('<span>(' + $(this).attr('maxlength') + ')</span>');
                if ($(this).attr('readonly') == undefined)
                    $(this).css('border-color', 'Blue');
            });
            $('fieldset.salida').find('input:text').not($('.trama')).each(function() {
                if ($(this).val() != '' && $(this).attr('maxlength') != $(this).val().length)
                    $(this).css('border-color', 'Red');
            });
        }
        function Ensamblar(selector) {
            var trama = $(selector).find('.trama');
            trama.val('');
            for (i = 1; i <= $(selector).find('input:text').not($('.trama')).length; i++)
            {
                var campo = $(selector).find("input:text[tabindex='" + i + "']");
                trama.val(trama.val() + "" + campo.val());
            }
        }
        function Desensamblar(selector) {
            var trama = $(selector).find('.trama');
            var indice = 0;
            for (i = 1; i <= $(selector).find('input:text').not($('.trama')).length; i++)
            {
                var campo = $(selector).find("input:text[tabindex='" + i + "']");
                campo.val(trama.val().substring(indice, indice + parseInt(campo.attr('maxlength'))));
                indice = indice + parseInt(campo.attr('maxlength'));   
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlValidar" runat="server">
            <asp:TextBox ID="txtCodigoSeguridad" runat="server" TextMode="Password" Width="400"></asp:TextBox>
            <asp:Button ID="btnValidar" runat="server" Text="Validar" />
        </asp:Panel>
        <asp:Panel ID="pnlOperaciones" runat="server" Visible="false">
            <center>
                <asp:Label ID="lblTipoOperacion" runat="server" Text="Tipo de operaci�n:" Font-Bold="true" AssociatedControlID="ddlTipoOperacion"></asp:Label>
                <asp:DropDownList ID="ddlTipoOperacion" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="Consulta (355000 - 0200)" Value="355000,0200"></asp:ListItem>
                    <asp:ListItem Text="Pago (945000 - 0200)" Value="945000,0200"></asp:ListItem>
                    <asp:ListItem Text="Anulaci�n (965000 - 0200)" Value="965000,0200"></asp:ListItem>
                    <asp:ListItem Text="Extorno Pago (945000 - 0400)" Value="945000,0400"></asp:ListItem>
                    <asp:ListItem Text="Extorno Anulaci�n (965000 - 0400)" Value="965000,0400"></asp:ListItem>
                </asp:DropDownList>
            </center>
            <br />
            <asp:Panel ID="pnlConsultar" runat="server" >
                <fieldset class="entrada">
                    <legend>Consulta de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="txtConsultaEntrada" runat="server" class="trama" maxlength="334" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="consinMessageTypeIdentification" runat="server" tabindex="1" value="0200" maxlength="4" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="consinPrimaryBitMap" runat="server" tabindex="2" value="F220848188E08000" maxlength="16" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="consinSecondaryBitMap" runat="server" tabindex="3" value="0000000000000018" maxlength="16" readonly="readonly" /></dd>
                        <dt>Numero de tarjeta:</dt>
                        <dd><input type="text" id="consinNumeroTarjeta" runat="server" tabindex="4" value="160000000000000000" maxlength="18" readonly="readonly" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="consinCodigoProceso" runat="server" tabindex="5" value="355000" maxlength="6" readonly="readonly" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="consinMonto" runat="server" tabindex="6" maxlength="12" value="000000000000" readonly="readonly" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="consinFechaHoraTransaccion" runat="server" tabindex="7" maxlength="10" format="MMDDhhmmss" readonly="readonly" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="consinTrace" runat="server" tabindex="8" value="123456" maxlength="6" title="N�mero de operaci�n" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="consinFechaCaptura" tabindex="9" runat="server" maxlength="4" format="MMDD" readonly="readonly" /></dd>
                        <dt>Modo de ingreso de datos:</dt>
                        <dd><input type="text" id="consinModoIngresoDatos" runat="server" tabindex="10" maxlength="3" value="000" readonly="readonly" /></dd>
                        <dt>Canal:</dt>
                        <dd><input type="text" id="consinCanal" runat="server" tabindex="11" maxlength="2" value="90" readonly="readonly" /></dd>
                        <dt>Bin adquiriente:</dt>
                        <dd><input type="text" id="consinBinAdquiriente" runat="server" tabindex="12" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Forward Institution Code:</dt>
                        <dd><input type="text" id="consinForwardInstitutionCode" runat="server" tabindex="13" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="consinRetrievalReferenceNumber" runat="server" tabindex="14" maxlength="12" value="            " readonly="readonly" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="consinTerminalID" runat="server" tabindex="15" maxlength="8" value="12345678" readonly="readonly" /></dd>
                        <dt>Comercio:</dt>
                        <dd><input type="text" id="consinComercio" runat="server" tabindex="16" maxlength="15" value="000000000000000" readonly="readonly" /></dd>
                        <dt>Card Acceptor Location:</dt>
                        <dd><input type="text" id="consinCardAcceptorLocation" runat="server" tabindex="17" maxlength="40" value="*WIESENET - APLICATIVO                  " readonly="readonly" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd><input type="text" id="consinTransactionCurrencyCode" runat="server" tabindex="18" maxlength="3" value="604" format="604:Soles, 840:D�lares" title="Moneda" /></dd>
                        <dt>Datos Reservados:</dt>
                        <dd><input type="text" id="consinDatosReservados" runat="server" tabindex="19" maxlength="5" value="00230" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del requerimiento:</legend>
                        <dl>
                            <dt>Longitud del requerimiento:</dt>
                            <dd><input type="text" id="consinLongitudRequerimiento" runat="server" tabindex="20" maxlength="3" readonly="readonly" value="135" /></dd>
                            <dt>C�digo de formato:</dt>
                            <dd><input type="text" id="consinCodigoFormato" runat="server" tabindex="21" maxlength="2" readonly="readonly" value="02" /></dd>
                            <dt>Bin Procesador:</dt>
                            <dd><input type="text" id="consinBinProcesador" runat="server" tabindex="22" maxlength="11" readonly="readonly" value="000002     " /></dd>
                            <dt>C�digo de acreedor:</dt>
                            <dd><input type="text" id="consinCodigoAcreedor" runat="server" tabindex="23" maxlength="11" readonly="readonly" value="111111     " /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="consinCodigoProducto" runat="server" tabindex="24" maxlength="8" value="        " title="C�digo de servicio" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="consinCodigoPlazaRecaudador" runat="server" tabindex="25" maxlength="4" value="0000" readonly="readonly" /></dd>
                            <dt>C�digo de agencia de recaudador:</dt>
                            <dd><input type="text" id="consinCodigoAgenciaRecaudador" runat="server" tabindex="26" maxlength="4" value="1234" title="C�digo de agencia" /></dd>
                            <dt>Tipo de datos de consulta:</dt>
                            <dd><input type="text" id="consinTipoDatoConsulta" runat="server" tabindex="27" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Dato de consulta:</dt>
                            <dd><input type="text" id="consinDatosConsulta" runat="server" tabindex="28" maxlength="21" value="000000000000000000001" title="CIP"/></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="consinCodigoCiudad" runat="server" tabindex="29" maxlength="3" value="000" readonly="readonly" /></dd>
                            <dt>C�digo de servicio:</dt>
                            <dd><input type="text" id="consinCodigoServicio" runat="server" tabindex="30" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>N�mero de documento:</dt>
                            <dd><input type="text" id="consinNumeroDocumento" runat="server" tabindex="31" maxlength="16" value="1234567890123456" readonly="readonly" /></dd>
                            <dt>N�mero de operaci�n:</dt>
                            <dd><input type="text" id="consinNumeroOperacion" runat="server" tabindex="32" maxlength="12" value="            " readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="consinFiller" runat="server" tabindex="33" maxlength="20" value="                    " readonly="readonly" /></dd>
                            <dt>Tama�o m�ximo de bloque:</dt>
                            <dd><input type="text" id="consinTamanoMaximoBloque" runat="server" tabindex="34" maxlength="5" value="01000" readonly="readonly" /></dd>
                            <dt>Posici�n del �ltimo documento:</dt>
                            <dd><input type="text" id="consinPosicionUltimoDocumento" runat="server" tabindex="35" maxlength="3" value="000" readonly="readonly" /></dd>
                            <dt>Puntero de la base de datos:</dt>
                            <dd><input type="text" id="consinPunteroBaseDatos" runat="server" tabindex="36" maxlength="10" value="          " readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarConsulta" runat="server" Text="Consultar" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta a Consulta</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="consSalida" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="consoutMessageTypeIdentification" runat="server" maxlength="4" readonly="readonly" tabindex="1" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="consoutPrimaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="2" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="consoutSecondaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="3" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="consoutCodigoProceso" runat="server" maxlength="6" readonly="readonly" tabindex="4" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="consoutMonto" runat="server" maxlength="12" readonly="readonly" tabindex="5" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="consoutFechaHoraTransaccion" runat="server" maxlength="10" readonly="readonly" format="MMDDhhmmss" tabindex="6" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="consoutTrace" runat="server" maxlength="6" readonly="readonly" tabindex="7" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="consoutFechaCaptura" runat="server" maxlength="4" readonly="readonly" format="MMDD" tabindex="8" /></dd>
                        <dt>Bin adquiriente:</dt>
                        <dd><input type="text" id="consoutBinAdquiriente" runat="server" maxlength="8" readonly="readonly" tabindex="9" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="consoutRetrievalReferenceNumber" runat="server" maxlength="12" readonly="readonly" tabindex="10" /></dd>
                        <dt>Authorization ID Response:</dt>
                        <dd><input type="text" id="consoutAuthorizationIDResponse" runat="server" maxlength="6" readonly="readonly" tabindex="11" />
                        </dd><dt>Response Code:</dt>
                        <dd><input type="text" id="consoutResponseCode" runat="server" maxlength="2" readonly="readonly" tabindex="12" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="consoutTerminalID" runat="server" maxlength="8" readonly="readonly" tabindex="13" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd><input type="text" id="consoutTransactionCurrencyCode" runat="server" maxlength="3" readonly="readonly" tabindex="14" /></dd>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="consoutDatosReservados" runat="server" maxlength="5" readonly="readonly" tabindex="15" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Respuesta de requerimiento:</legend>
                        <fieldset>
                            <legend>Cabecera de Rpta.:</legend>
                            <dl>
                                <dt>Longitud del campo:</dt>
                                <dd><input type="text" id="consoutLongitudCampo" runat="server" maxlength="3" readonly="readonly" tabindex="16" /></dd>
                                <dt>C�digo de formato:</dt>
                                <dd><input type="text" id="consoutCodigoFormato" runat="server" maxlength="2" readonly="readonly" tabindex="17" /></dd>
                                <dt>Bin procesador:</dt>
                                <dd><input type="text" id="consoutBinProcesador" runat="server" maxlength="11" readonly="readonly" tabindex="18" /></dd>
                                <dt>Bin acreedor:</dt>
                                <dd><input type="text" id="consoutBinAcreedor" runat="server" maxlength="11" readonly="readonly" tabindex="19" /></dd>
                                <dt>C�digo de producto/servicio:</dt>
                                <dd><input type="text" id="consoutProductoServicio" runat="server" maxlength="8" readonly="readonly" tabindex="20" /></dd>
                                <dt>Agencia:</dt>
                                <dd><input type="text" id="consoutAgencia" runat="server" maxlength="4" readonly="readonly" tabindex="21" /></dd>
                                <dt>Tipo de identificaci�n:</dt>
                                <dd><input type="text" id="consoutTipoIdentificacion" runat="server" maxlength="2" readonly="readonly" tabindex="22" /></dd>
                                <dt>N�mero de identificaci�n:</dt>
                                <dd><input type="text" id="consoutNumeroIdentificacion" runat="server" maxlength="21" readonly="readonly" tabindex="23" /></dd>
                                <dt>Nombre del deudor:</dt>
                                <dd><input type="text" id="consoutNombreDeudor" runat="server" maxlength="20" readonly="readonly" tabindex="24" /></dd>
                                <dt>N�mero de servicios devueltos:</dt>
                                <dd><input type="text" id="consoutNumeroServiciosDevueltos" runat="server" maxlength="2" readonly="readonly" tabindex="25" /></dd>
                                <dt>N�mero de operaci�n de cobranza:</dt>
                                <dd><input type="text" id="consoutNumeroOperacionCobranza" runat="server" maxlength="12" readonly="readonly" tabindex="26" /></dd>
                                <dt>Indicador si hay m�s documentos:</dt>
                                <dd><input type="text" id="consoutIndicadorMasDocumentos" runat="server" maxlength="1" readonly="readonly" tabindex="27" /></dd>
                                <dt>Tama�o m�ximo del bloque:</dt>
                                <dd><input type="text" id="consoutTamanoMaximoBloque" runat="server" maxlength="5" readonly="readonly" tabindex="28" /></dd>
                                <dt>Posici�n del �ltimo documento:</dt>
                                <dd><input type="text" id="consoutPosicionUltimoDocumento" runat="server" maxlength="3" readonly="readonly" tabindex="29" /></dd>
                                <dt>Puntero de la base de datos:</dt>
                                <dd><input type="text" id="consoutPunteroBaseDatos" runat="server" maxlength="10" readonly="readonly" tabindex="30" /></dd>
                                <dt>Origen de respuesta:</dt>
                                <dd><input type="text" id="consoutOrigenRespuesta" runat="server" maxlength="1" readonly="readonly" tabindex="31" /></dd>
                                <dt>C�digo de respuesta:</dt>
                                <dd><input type="text" id="consoutCodigoRespuesta" runat="server" maxlength="3" readonly="readonly" tabindex="32" /></dd>
                                <dt>Filler:</dt>
                                <dd><input type="text" id="consoutFiller" runat="server" maxlength="10" readonly="readonly" tabindex="33" /></dd>
                            </dl>
                        </fieldset>
                        <fieldset>
                            <legend>Detalle de respuesta:</legend>
                            <fieldset>
                                <legend>Cabecera del servicio:</legend>
                                <dl>
                                    <dt>C�digo de producto/servicio:</dt>
                                    <dd><input type="text" id="consoutdetCodigoProductoServicio" runat="server" maxlength="3" readonly="readonly" tabindex="34" /></dd>
                                    <dt>Moneda:</dt>
                                    <dd><input type="text" id="consoutdetMoneda" runat="server" maxlength="3" readonly="readonly" tabindex="35" /></dd>
                                    <dt>Estado del deudor:</dt>
                                    <dd><input type="text" id="consoutdetEstadoDeudor" runat="server" maxlength="2" readonly="readonly" tabindex="36" /></dd>
                                    <dt>Mensaje 1 al Deudor:</dt>
                                    <dd><input type="text" id="consoutdetMensaje1Deudor" runat="server" maxlength="40" readonly="readonly" tabindex="37" /></dd>
                                    <dt>Mensaje 2 al Deudor:</dt>
                                    <dd><input type="text" id="consoutdetMensaje2Deudor" runat="server" maxlength="40" readonly="readonly" tabindex="38" /></dd>
                                    <dt>Indicador de cronolog�a:</dt>
                                    <dd><input type="text" id="consoutdetIndicadorCronologia" runat="server" maxlength="1" readonly="readonly" tabindex="39" /></dd>
                                    <dt>Indicador de pagos vencidos:</dt>
                                    <dd><input type="text" id="consoutdetIndicadorPagosVencidos" runat="server" maxlength="1" readonly="readonly" tabindex="40" /></dd>
                                    <dt>Restricci�n de pagos:</dt>
                                    <dd><input type="text" id="consoutdetRestriccionPagos" runat="server" maxlength="1" readonly="readonly" tabindex="41" /></dd>
                                    <dt>Documentos por servicio:</dt>
                                    <dd><input type="text" id="consoutdetDocumentosServicio" runat="server" maxlength="2" readonly="readonly" tabindex="42" /></dd>
                                    <dt>Filler:</dt>
                                    <dd><input type="text" id="consoutdetFiller" runat="server" maxlength="5" readonly="readonly" tabindex="43" /></dd>
                                </dl>
                            </fieldset>
                            <fieldset>
                                <legend>Detalle del servicio:</legend>
                                <dl>
                                    <dt>Tipo de servicio:</dt>
                                    <dd><input type="text" id="consoutdetTipoServicio" runat="server" maxlength="3" readonly="readonly" tabindex="44" /></dd>
                                    <dt>N�mero de documento:</dt>
                                    <dd><input type="text" id="consoutdetNumeroDocumento" runat="server" maxlength="16" readonly="readonly" tabindex="45" /></dd>
                                    <dt>Referencia de deuda:</dt>
                                    <dd><input type="text" id="consoutdetReferenciaDeuda" runat="server" maxlength="16" readonly="readonly" tabindex="46" /></dd>
                                    <dt>Fecha de vencimiento:</dt>
                                    <dd><input type="text" id="consoutdetFechaVencimiento" runat="server" maxlength="8" readonly="readonly" tabindex="47" /></dd>
                                    <dt>Importe m�nimo:</dt>
                                    <dd><input type="text" id="consoutdetImporteMinimo" runat="server" maxlength="11" readonly="readonly" tabindex="48" /></dd>
                                    <dt>Importe Total:</dt>
                                    <dd><input type="text" id="consoutdetImporteTotal" runat="server" maxlength="11" readonly="readonly" tabindex="49" /></dd>
                                </dl>
                            </fieldset>
                        </fieldset>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlPagar" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Pagar CIPs</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="txtPagoEntrada" runat="server" class="trama" maxlength="667" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="paginMessageTypeIdentification" runat="server" tabindex="1" maxlength="4" value="0200" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="paginPrimaryBitMap" runat="server" tabindex="2" maxlength="16" value="F220848188E08000" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="paginSecondaryBitMap" runat="server" tabindex="3" maxlength="16" value="0000000000000018" readonly="readonly" /></dd>
                        <dt>N�mero de tarjeta:</dt>
                        <dd><input type="text" id="paginNumeroTarjeta" runat="server" tabindex="4" maxlength="18" value="160000000000000000" readonly="readonly" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="paginCodigoProceso" runat="server" tabindex="5" maxlength="6" value="945000" readonly="readonly" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="paginMonto" runat="server" tabindex="6" maxlength="12" value="000000001100" readonly="readonly" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="paginFechaHoraTransaccion" runat="server" tabindex="7" maxlength="10" format="MMDDhhmmss" readonly="readonly" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="paginTrace" runat="server" tabindex="8" maxlength="6" value="123456" title="N�mero de operaci�n" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="paginFechaCaptura" runat="server" tabindex="9" maxlength="4" format="MMDD" readonly="readonly" /></dd>
                        <dt>Modo de ingreso de datos:</dt>
                        <dd><input type="text" id="paginModoIngresoDatos" runat="server" tabindex="10" maxlength="3" value="000" readonly="readonly" /></dd>
                        <dt>Canal:</dt>
                        <dd><input type="text" id="paginCanal" runat="server" tabindex="11" maxlength="2" value="90" /></dd>
                        <dt>Bin Adquiriente:</dt>
                        <dd><input type="text" id="paginBinAdquiriente" runat="server" tabindex="12" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Forward Institution Code:</dt>
                        <dd><input type="text" id="paginForwardInstitutionCode" runat="server" tabindex="13" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="paginRetrievalReferenceNumber" runat="server" tabindex="14" maxlength="12" value="784501860295" readonly="readonly" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="paginTerminalID" runat="server" tabindex="15" maxlength="8" value="0056    " readonly="readonly" /></dd>
                        <dt>Comercio:</dt>
                        <dd><input type="text" id="paginComercio" runat="server" tabindex="16" maxlength="15" value="000000000000000" readonly="readonly" /></dd>
                        <dt>Card Acceptor Location:</dt>
                        <dd><input type="text" id="paginCardAcceptorLocation" runat="server" tabindex="17" maxlength="40" value="*WIESENET - APLICATIVO                  " readonly="readonly" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd><input type="text" id="paginTransactionCurrencyCode" runat="server" tabindex="18" maxlength="3" value="604" title="Moneda" format="604:Soles, 840:D�lares" /></dd>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="paginDatosReservados" runat="server" tabindex="19" maxlength="5" value="00220" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del requerimiento:</legend>
                        <dl>
                            <dt>Longitud de la trama:</dt>
                            <dd><input type="text" id="paginLongitudTrama" runat="server" tabindex="20" maxlength="3" value="428" readonly="readonly" /></dd>
                            <dt>C�digo de formato:</dt>
                            <dd><input type="text" id="paginCodigoFormato" runat="server" tabindex="21" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="paginBinProcesador" runat="server" tabindex="22" maxlength="11" readonly="readonly" value="000002     " /></dd>
                            <dt>C�digo de acreedor:</dt>
                            <dd><input type="text" id="paginCodigoAcreedor" runat="server" tabindex="23" maxlength="11" readonly="readonly" value="111111     " /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="paginCodigoProducto" runat="server" tabindex="24" maxlength="8" value="000     " readonly="readonly" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="paginCodigoPlazaRecaudador" runat="server" tabindex="25" maxlength="4" value="0000" readonly="readonly" /></dd>
                            <dt>C�digo de agencia de recaudador:</dt>
                            <dd><input type="text" id="paginCodigoAgenciaRecaudador" runat="server" tabindex="26" maxlength="4" value="1234" title="C�digo de agencia" /></dd>
                            <dt>Tipo de datos de pago:</dt>
                            <dd><input type="text" id="paginTipoDatosPago" runat="server" tabindex="27" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="paginDatoPago" runat="server" tabindex="28" maxlength="21" value="17507902             " title="CIP"/></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="paginCodigoCiudad" runat="server" tabindex="29" maxlength="3" value="784" readonly="readonly" /></dd>
                            <dt>N�mero de producto/servicio pagado:</dt>
                            <dd><input type="text" id="paginNumeroProductoServicioPagado" runat="server" tabindex="30" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>N�mero total de documento pagado:</dt>
                            <dd><input type="text" id="paginNumeroTotalDocumentoPagado" runat="server" tabindex="31" maxlength="3" value="001" readonly="readonly" /></dd>
                            <dt>Filler</dt>
                            <dd><input type="text" id="paginFiller" runat="server" tabindex="32" maxlength="10" value="          " readonly="readonly" /></dd>
                            <dt>Medio de pago:</dt>
                            <dd><input type="text" id="paginMedioPago" runat="server" tabindex="33" maxlength="2" value="00" title="00:Efectivo, 01:Cheque, 02:Tarjeta de cr�dito, 03:Tarjeta de d�bito, 04:Tarjeta virtual, 05:Cargo a cuenta, 06:Efectivo+Cargo a cuenta, 07:Efectivo+Cheque" /></dd>
                            <dt>Importe pagado efectivo:</dt>
                            <dd><input type="text" id="paginImportePagadoEfectivo" runat="server" tabindex="34" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Importe pagado con cuenta:</dt>
                            <dd><input type="text" id="paginImportePagadoCuenta" runat="server" tabindex="35" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Numero cheque 1:</dt>
                            <dd><input type="text" id="paginNumeroCheque1" runat="server" tabindex="36" maxlength="15" value="               " readonly="readonly" /></dd>
                            <dt>Banco girador 1:</dt>
                            <dd><input type="text" id="paginBancoGirador1" runat="server" tabindex="37" maxlength="3" value="   " readonly="readonly" /></dd>
                            <dt>Importe cheque 1:</dt>
                            <dd><input type="text" id="paginImporteCheque1" runat="server" tabindex="38" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>Plaza Cheque 1:</dt>
                            <dd><input type="text" id="paginPlazaCheque1" runat="server" tabindex="39" maxlength="1" value=" " readonly="readonly" /></dd>
                            <dt>Numero Cheque 2:</dt>
                            <dd><input type="text" id="paginNumeroCheque2" runat="server" tabindex="40" maxlength="15" value="               " readonly="readonly" /></dd>
                            <dt>Banco girador 2:</dt>
                            <dd><input type="text" id="paginBancoGirador2" runat="server" tabindex="41" maxlength="3" value="   " readonly="readonly" /></dd>
                            <dt>Importe cheque 2:</dt>
                            <dd><input type="text" id="paginImporteCheque2" runat="server" tabindex="42" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>Plaza Cheque 2:</dt>
                            <dd><input type="text" id="paginPlazaCheque2" runat="server" tabindex="43" maxlength="1" value=" " readonly="readonly" /></dd>
                            <dt>Numero Cheque 3:</dt>
                            <dd><input type="text" id="paginNumeroCheque3" runat="server" tabindex="44" maxlength="15" value="               " readonly="readonly" /></dd>
                            <dt>Banco girador 3:</dt>
                            <dd><input type="text" id="paginBancoGirador3" runat="server" tabindex="45" maxlength="3" value="   " readonly="readonly" /></dd>
                            <dt>Importe cheque 3:</dt>
                            <dd><input type="text" id="paginImporteCheque3" runat="server" tabindex="46" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>Plaza Cheque 3:</dt>
                            <dd><input type="text" id="paginPlazaCheque3" runat="server" tabindex="47" maxlength="1" value=" " readonly="readonly" /></dd>
                            <dt>Moneda de pago:</dt>
                            <dd><input type="text" id="paginMonedaPago" runat="server" tabindex="48" maxlength="3" value="604" readonly="readonly" /></dd>
                            <dt>Tipo de cambio aplicado:</dt>
                            <dd><input type="text" id="paginTipoCambioAplicado" runat="server" tabindex="49" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>Pago total realizado:</dt>
                            <dd><input type="text" id="paginPagoTotalRealizado" runat="server" tabindex="50" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="paginFiller2" runat="server" tabindex="51" maxlength="10" value="          " readonly="readonly" /></dd>
                            <dt>C�digo del servicio pagado:</dt>
                            <dd><input type="text" id="paginCodigoServicioPagado" runat="server" tabindex="52" maxlength="3" value="000" title="C�digo de servicio" /></dd>
                            <dt>Estado del deudor:</dt>
                            <dd><input type="text" id="paginEstadoDeudor" runat="server" tabindex="53" maxlength="2" value="S " readonly="readonly" /></dd>
                            <dt>Importe total por producto/servicio:</dt>
                            <dd><input type="text" id="paginImporteTotalProducto" runat="server" tabindex="54" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>N�mero de cuenta de abono:</dt>
                            <dd><input type="text" id="paginNumeroCuentaAbono" runat="server" tabindex="55" maxlength="19" value="                   " readonly="readonly" /></dd>
                            <dt>N�mero de referencia del abono:</dt>
                            <dd><input type="text" id="paginNumeroReferenciaAbono" runat="server" tabindex="56" maxlength="12" value="784501860295" readonly="readonly" /></dd>
                            <dt>N�mero de documentos pagados:</dt>
                            <dd><input type="text" id="paginNumeroDocumentosPagados" runat="server" tabindex="57" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="paginFiller3" runat="server" tabindex="58" maxlength="10" value="          " readonly="readonly" /></dd>
                            <dt>Tipo de documento de pago:</dt>
                            <dd><input type="text" id="paginTipoDocumentoPago" runat="server" tabindex="59" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>N�mero de documento de pago:</dt>
                            <dd><input type="text" id="paginNumeroDocumentoPago" runat="server" tabindex="60" maxlength="16" value="T001-0062563    " readonly="readonly" /></dd>
                            <dt>Per�odo de cotizaci�n:</dt>
                            <dd><input type="text" id="paginPeriodoCotizacion" runat="server" tabindex="61" maxlength="6" value="      " readonly="readonly" /></dd>
                            <dt>Tipo documento ID Deudor:</dt>
                            <dd><input type="text" id="paginTipoDocumentoIDDeudor" runat="server" tabindex="62" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>N�mero documento ID Deudor:</dt>
                            <dd><input type="text" id="paginNumeroDocumentoIDDeudor" runat="server" tabindex="63" maxlength="15" value="               " readonly="readonly" /></dd>
                            <dt>Importe original de la deuda:</dt>
                            <dd><input type="text" id="paginImporteOriginalDeuda" runat="server" tabindex="64" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Importe pagado del documento:</dt>
                            <dd><input type="text" id="paginImportePagadoDocumento" runat="server" tabindex="65" maxlength="11" value="00000002200" title="Monto" /></dd>
                            <dt>C�digo de concepto 1:</dt>
                            <dd><input type="text" id="paginCodigoConcepto1" runat="server" tabindex="66" maxlength="2" value="00" readonly="readonly" /></dd>
                            <dt>Importe de concepto 1:</dt>
                            <dd><input type="text" id="paginImporteConcepto1" runat="server" tabindex="67" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>C�digo de concepto 2:</dt>
                            <dd><input type="text" id="paginCodigoConcepto2" runat="server" tabindex="68" maxlength="2" value="00" readonly="readonly" /></dd>
                            <dt>Importe de concepto 2:</dt>
                            <dd><input type="text" id="paginImporteConcepto2" runat="server" tabindex="69" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>C�digo de concepto 3:</dt>
                            <dd><input type="text" id="paginCodigoConcepto3" runat="server" tabindex="70" maxlength="2" value="00" readonly="readonly" /></dd>
                            <dt>Importe de concepto 3:</dt>
                            <dd><input type="text" id="paginImporteConcepto3" runat="server" tabindex="71" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>C�digo de concepto 4:</dt>
                            <dd><input type="text" id="paginCodigoConcepto4" runat="server" tabindex="72" maxlength="2" value="00" readonly="readonly" /></dd>
                            <dt>Importe de concepto 4:</dt>
                            <dd><input type="text" id="paginImporteConcepto4" runat="server" tabindex="73" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>C�digo de concepto 5:</dt>
                            <dd><input type="text" id="paginCodigoConcepto5" runat="server" tabindex="74" maxlength="2" value="00" readonly="readonly" /></dd>
                            <dt>Importe de concepto 5:</dt>
                            <dd><input type="text" id="paginImporteConcepto5" runat="server" tabindex="75" maxlength="11" value="00000000000" readonly="readonly" /></dd>
                            <dt>Referencia de la deuda:</dt>
                            <dd><input type="text" id="paginReferenciaDeuda" runat="server" tabindex="76" maxlength="16" value="                " readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="paginFiller4" runat="server" tabindex="77" maxlength="34" value="                                  " readonly="readonly" /></dd></dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarPago" runat="server" Text="Pagar" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta a Pago</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="pagoSalida" class="trama" runat="server" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="pagoutMessageTypeIdentification" runat="server" maxlength="4" readonly="readonly" tabindex="1" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="pagoutPrimaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="2" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="pagoutSecondaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="3" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="pagoutCodigoProceso" runat="server" maxlength="6" readonly="readonly" tabindex="4" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="pagoutMonto" runat="server" maxlength="12" readonly="readonly" tabindex="5" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="pagoutFechaHoraTransaccion" runat="server" maxlength="10" readonly="readonly" tabindex="6" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="pagoutTrace" runat="server" maxlength="6" readonly="readonly" tabindex="7" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="pagoutFechaCaptura" runat="server" maxlength="4" readonly="readonly" tabindex="8" /></dd>
                        <dt>Identificacion empresa:</dt>
                        <dd><input type="text" id="pagoutIdentificacionEmpresa" runat="server" maxlength="8" readonly="readonly" tabindex="9" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="pagoutRetrievalReferenceNumber" runat="server" maxlength="12" readonly="readonly" tabindex="10" /></dd>
                        <dt>Authorization ID Response:</dt>
                        <dd><input type="text" id="pagoutAuthorizationIDResponse" runat="server" maxlength="6" readonly="readonly" tabindex="11" /></dd>
                        <dt>Response Code:</dt>
                        <dd><input type="text" id="pagoutResponseCode" runat="server" maxlength="2" readonly="readonly" tabindex="12" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="pagoutTerminalID" runat="server" maxlength="8" readonly="readonly" tabindex="13" /></dd>
                        <dt>Transaction Currency Card:</dt>
                        <dd><input type="text" id="pagoutTransactionCurrencyCard" runat="server" maxlength="3" readonly="readonly" tabindex="14" /></dd>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="pagoutDatosReservados" runat="server" maxlength="5" readonly="readonly" tabindex="15" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Respuesta de requerimiento</legend>
                        <dl>
                            <dt>Tama�o del bloque:</dt>
                            <dd><input type="text" id="pagoutTamanoBloque" runat="server" maxlength="3" readonly="readonly" tabindex="16" /></dd>
                            <dt>C�digo del formato:</dt>
                            <dd><input type="text" id="pagoutCodigoFormato" runat="server" maxlength="2" readonly="readonly" tabindex="17" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="pagoutBinProcesador" runat="server" maxlength="11" readonly="readonly" tabindex="18" /></dd>
                            <dt>C�digo de acreedor:</dt>
                            <dd><input type="text" id="pagoutCodigoAcreedor" runat="server" maxlength="11" readonly="readonly" tabindex="19" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="pagoutCodigoProductoServicio" runat="server" maxlength="8" readonly="readonly" tabindex="20" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="pagoutCodigoPlazaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="21" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="pagoutCodigoAgenciaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="22" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="pagoutTipoDatoPago" runat="server" maxlength="2" readonly="readonly" tabindex="23" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="pagoutDatoPago" runat="server" maxlength="21" readonly="readonly" tabindex="24" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="pagoutCodigoCiudad" runat="server" maxlength="3" readonly="readonly" tabindex="25" /></dd>
                            <dt>N�mero de operaci�n cobranza:</dt>
                            <dd><input type="text" id="pagoutNumeroOperacionCobranza" runat="server" maxlength="12" readonly="readonly" tabindex="26" /></dd>
                            <dt>N�mero de operaci�n acreedor:</dt>
                            <dd><input type="text" id="pagoutNumeroOperacionAcreedor" runat="server" maxlength="12" readonly="readonly" tabindex="27" /></dd>
                            <dt>N�mero de producto/servicio pagado:</dt>
                            <dd><input type="text" id="pagoutNumeroProductoServicioPagado" runat="server" maxlength="2" readonly="readonly" tabindex="28" /></dd>
                            <dt>N�mero total de documentos pagados:</dt>
                            <dd><input type="text" id="pagoutNumeroTotalDocumentosPagados" runat="server" maxlength="3" readonly="readonly" tabindex="29" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagoutFiller" runat="server" maxlength="10" readonly="readonly" tabindex="30" /></dd>
                            <dt>Origen de respuesta:</dt>
                            <dd><input type="text" id="pagoutOrigenRespuesta" runat="server" maxlength="1" readonly="readonly" tabindex="31" /></dd>
                            <dt>C�digo de respuesta extendida:</dt>
                            <dd><input type="text" id="pagoutCodigoRespuestaExtendida" runat="server" maxlength="3" readonly="readonly" tabindex="32" /></dd>
                            <dt>Descripci�n de la respuesta aplicativa:</dt>
                            <dd><input type="text" id="pagoutDescripcionRespuestaAplicativa" runat="server" maxlength="30" readonly="readonly" tabindex="33" /></dd>
                            <dt>Nombre del deudor:</dt>
                            <dd><input type="text" id="pagoutNombreDeudor" runat="server" maxlength="20" readonly="readonly" tabindex="34" /></dd>
                            <dt>RUC del deudor:</dt>
                            <dd><input type="text" id="pagoutRUCDeudor" runat="server" maxlength="15" readonly="readonly" tabindex="35" /></dd>
                            <dt>RUC del acreedor:</dt>
                            <dd><input type="text" id="pagoutRUCAcreedor" runat="server" maxlength="15" readonly="readonly" tabindex="36" /></dd>
                            <dt>C�digo de zona del acreedor:</dt>
                            <dd><input type="text" id="pagoutCodigoZonaAcreedor" runat="server" maxlength="6" readonly="readonly" tabindex="37" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagoutFiller2" runat="server" maxlength="20" readonly="readonly" tabindex="38" /></dd>
                            <dt>C�digo del producto/servicio:</dt>
                            <dd><input type="text" id="pagoutCodigoProductoServicio2" runat="server" maxlength="3" readonly="readonly" tabindex="39" /></dd>
                            <dt>Descripci�n del producto/servicio:</dt>
                            <dd><input type="text" id="pagoutDescripcionProductoServicio" runat="server" maxlength="15" readonly="readonly" tabindex="40" /></dd>
                            <dt>Importe total por producto/servicio:</dt>
                            <dd><input type="text" id="pagoutImporteTotalProductoServicio" runat="server" maxlength="11" readonly="readonly" tabindex="41" /></dd>
                            <dt>Mensaje 1:</dt>
                            <dd><input type="text" id="pagoutMensaje1" runat="server" maxlength="40" readonly="readonly" tabindex="42" /></dd>
                            <dt>Mensaje 2:</dt>
                            <dd><input type="text" id="pagoutMensaje2" runat="server" maxlength="40" readonly="readonly" tabindex="43" /></dd>
                            <dt>N�mero de documentos:</dt>
                            <dd><input type="text" id="pagoutNumeroDocumentos" runat="server" maxlength="2" readonly="readonly" tabindex="44" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagoutFiller3" runat="server" maxlength="20" readonly="readonly" tabindex="45" /></dd>
                            <dt>Tipo de servicio:</dt>
                            <dd><input type="text" id="pagoutTipoServicio" runat="server" maxlength="3" readonly="readonly" tabindex="46" /></dd>
                            <dt>Descripci�n del documento:</dt>
                            <dd><input type="text" id="pagoutDescripcionDocumento" runat="server" maxlength="15" readonly="readonly" tabindex="47" /></dd>
                            <dt>N�mero del documento:</dt>
                            <dd><input type="text" id="pagoutNumeroDocumento" runat="server" maxlength="16" readonly="readonly" tabindex="48" /></dd>
                            <dt>Per�odo de cotizaci�n:</dt>
                            <dd><input type="text" id="pagoutPeriodoCotizacion" runat="server" maxlength="6" readonly="readonly" tabindex="49" /></dd>
                            <dt>Tipo de documento de identidad:</dt>
                            <dd><input type="text" id="pagoutTipoDocumentoIdentidad" runat="server" maxlength="2" readonly="readonly" tabindex="50" /></dd>
                            <dt>N�mero de documento de identidad:</dt>
                            <dd><input type="text" id="pagoutNumeroDocumentoIdentidad" runat="server" maxlength="15" readonly="readonly" tabindex="51" /></dd>
                            <dt>Fecha de emisi�n:</dt>
                            <dd><input type="text" id="pagoutFechaEmision" runat="server" maxlength="8" readonly="readonly" tabindex="52" /></dd>
                            <dt>Fecha de vencimiento:</dt>
                            <dd><input type="text" id="pagoutFechaVencimiento" runat="server" maxlength="8" readonly="readonly" tabindex="53" /></dd>
                            <dt>Importe pagado:</dt>
                            <dd><input type="text" id="pagoutImportePagado" runat="server" maxlength="11" readonly="readonly" tabindex="54" /></dd>
                            <dt>C�digo de concepto 1:</dt>
                            <dd><input type="text" id="pagoutCodigoConcepto1" runat="server" maxlength="2" readonly="readonly" tabindex="55" /></dd>
                            <dt>Importe de concepto 1:</dt>
                            <dd><input type="text" id="pagoutImporteConcepto1" runat="server" maxlength="11" readonly="readonly" tabindex="56" /></dd>
                            <dt>C�digo de concepto 2:</dt>
                            <dd><input type="text" id="pagoutCodigoConcepto2" runat="server" maxlength="2" readonly="readonly" tabindex="57" /></dd>
                            <dt>Importe de concepto 2:</dt>
                            <dd><input type="text" id="pagoutImporteConcepto2" runat="server" maxlength="11" readonly="readonly" tabindex="58" /></dd>
                            <dt>C�digo de concepto 3:</dt>
                            <dd><input type="text" id="pagoutCodigoConcepto3" runat="server" maxlength="2" readonly="readonly" tabindex="59" /></dd>
                            <dt>Importe de concepto 3:</dt>
                            <dd><input type="text" id="pagoutImporteConcepto3" runat="server" maxlength="11" readonly="readonly" tabindex="60" /></dd>
                            <dt>C�digo de concepto 4:</dt>
                            <dd><input type="text" id="pagoutCodigoConcepto4" runat="server" maxlength="2" readonly="readonly" tabindex="61" /></dd>
                            <dt>Importe de concepto 4:</dt>
                            <dd><input type="text" id="pagoutImporteConcepto4" runat="server" maxlength="11" readonly="readonly" tabindex="62" /></dd>
                            <dt>C�digo de concepto 5:</dt>
                            <dd><input type="text" id="pagoutCodigoConcepto5" runat="server" maxlength="2" readonly="readonly" tabindex="63" /></dd>
                            <dt>Importe de concepto 5:</dt>
                            <dd><input type="text" id="pagoutImporteConcepto5" runat="server" maxlength="11" readonly="readonly" tabindex="64" /></dd>
                            <dt>Indicador de facturaci�n:</dt>
                            <dd><input type="text" id="pagoutIndicadorFacturacion" runat="server" maxlength="1" readonly="readonly" tabindex="65" /></dd>
                            <dt>N�mero de factura:</dt>
                            <dd><input type="text" id="pagoutNumeroFactura" runat="server" maxlength="11" readonly="readonly" tabindex="66" /></dd>
                            <dt>Referencia de la deuda:</dt>
                            <dd><input type="text" id="pagoutReferenciaDeuda" runat="server" maxlength="16" readonly="readonly" tabindex="67" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagoutFiller4" runat="server" maxlength="34" readonly="readonly" tabindex="68" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlAnular" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Anulaci�n de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="txtAnulacionEntrada" runat="server" class="trama" maxlength="393" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="anuinMessageTypeIdentification" runat="server" tabindex="1" maxlength="4" value="0200" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="anuinPrimaryBitMap" runat="server" tabindex="2" maxlength="16" value="F220848188E08000" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="anuinSecondaryBitMap" runat="server" tabindex="3" maxlength="16" value="0000004000000018" readonly="readonly" /></dd>
                        <dt>N�mero de tarjeta:</dt>
                        <dd><input type="text" id="anuinNumeroTarjeta" runat="server" tabindex="4" maxlength="18" value="160000000000000000" readonly="readonly" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="anuinCodigoProceso" runat="server" tabindex="5" maxlength="6" value="965000" readonly="readonly" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="anuinMonto" runat="server" tabindex="6" maxlength="12" value="000000001100" title="Monto" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="anuinFechaHoraTransaccion" runat="server" tabindex="7" maxlength="10" format="MMDDhhmmss" readonly="readonly" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="anuinTrace" runat="server" tabindex="8" maxlength="6" value="123456" title="N�mero de operaci�n" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="anuinFechaCaptura" runat="server" tabindex="9" maxlength="4" format="MMDD" readonly="readonly" /></dd>
                        <dt>Modo de ingreso de datos:</dt>
                        <dd><input type="text" id="anuinModoIngresoDatos" runat="server" tabindex="10" maxlength="3" value="123" readonly="readonly" /></dd>
                        <dt>Canal:</dt>
                        <dd><input type="text" id="anuinCanal" runat="server" tabindex="11" maxlength="2" value="12" readonly="readonly" /></dd>
                        <dt>Bin Adquiriente:</dt>
                        <dd><input type="text" id="anuinBinAdquiriente" runat="server" tabindex="12" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Forward Institution Code:</dt>
                        <dd><input type="text" id="anuinForwardInstitutionCode" runat="server" tabindex="13" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="anuinRetrievalReferenceNumber" runat="server" tabindex="14" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="anuinTerminalID" runat="server" tabindex="15" maxlength="8" value="12345678" readonly="readonly" /></dd>
                        <dt>Comercio:</dt>
                        <dd><input type="text" id="anuinComercio" runat="server" tabindex="16" maxlength="15" value="000000000000000" readonly="readonly" /></dd>
                        <dt>Card Acceptor Location:</dt>
                        <dd><input type="text" id="anuinCardAcceptorLocation" runat="server" tabindex="17" maxlength="40" value="                                        " readonly="readonly" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd><input type="text" id="anuinTransactionCurrencyCode" runat="server" tabindex="18" maxlength="3" value="604" format="604:Soles, 840:D�lares" title="Moneda" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Original Data Elements:</legend>
                        <dl>
                            <dt>Message Type Identification:</dt>
                            <dd><input type="text" id="anuindetMessageTypeIdentification" runat="server" tabindex="19" maxlength="4" value="0200" readonly="readonly" /></dd>
                            <dt>Trace:</dt>
                            <dd><input type="text" id="anuindetTrace" runat="server" tabindex="20" maxlength="6" value="123456" title="N�mero de operaci�n de pago" /></dd>
                            <dt>Fecha y hora de transacci�n:</dt>
                            <dd><input type="text" id="anuindetFechaHoraTransaccion" runat="server" tabindex="21" maxlength="10" readonly="readonly" /></dd>
                            <dt>Bin adquiriente:</dt>
                            <dd><input type="text" id="anuindetBinAdquiriente" runat="server" tabindex="22" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>Forward Institution Code:</dt>
                            <dd><input type="text" id="anuindetForwardInstitutionCode" runat="server" tabindex="23" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <dl>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="anuinDatosReservados" runat="server" tabindex="24" maxlength="5" value="00220" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del requerimiento:</legend>
                        <dl>
                            <dt>Longitud de dato:</dt>
                            <dd><input type="text" id="anuindetLongitudDato" runat="server" tabindex="25" maxlength="3" value="152" readonly="readonly" /></dd>
                            <dt>C�digo de  formato:</dt>
                            <dd><input type="text" id="anuindetCodigoFormato" runat="server" tabindex="26" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="anuindetBinProcesador" runat="server" tabindex="27" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>C�digo acreedor:</dt>
                            <dd><input type="text" id="anuindetCodigoAcreedor" runat="server" tabindex="28" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="anuindetCodigoProductoServicio" runat="server" tabindex="29" maxlength="8" value="00012345" readonly="readonly" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="anuindetCodigoPlazaRecaudador" runat="server" tabindex="30" maxlength="4" value="0000" readonly="readonly" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="anuindetCodigoAgenciaRecaudador" runat="server" tabindex="31" maxlength="4" value="1234" title="C�digo de agencia" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="anuindetTipoDatoPago" runat="server" tabindex="32" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="anuindetDatoPago" runat="server" tabindex="33" maxlength="21" value="123456789012345678901" title="CIP" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="anuindetCodigoCiudad" runat="server" tabindex="34" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuindetFiller" runat="server" tabindex="35" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                            <dt>Filler (Recibo a anular):</dt>
                            <dd><input type="text" id="anuindetFiller2" runat="server" tabindex="36" maxlength="0" value="" readonly="readonly" /></dd>
                            <dt>Tipo de servicio:</dt>
                            <dd><input type="text" id="anuindetTipoServicio" runat="server" tabindex="37" maxlength="3" value="000"  title="C�digo de servicio" /></dd>
                            <dt>N�mero de documento:</dt>
                            <dd><input type="text" id="anuindetNumeroDocumento" runat="server" tabindex="38" maxlength="16" value="1234567890123456" readonly="readonly" /></dd>
                            <dt>Disponible:</dt>
                            <dd><input type="text" id="anuindetDisponible" runat="server" tabindex="39" maxlength="31" value="                               " readonly="readonly" /></dd>
                            <dt>N�mero de transacciones de Cob. Ori.:</dt>
                            <dd><input type="text" id="anuindetNumeroTransaccionesCobOri" runat="server" tabindex="40" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                            <dt>N�mero de operaciones original acreedor:</dt>
                            <dd><input type="text" id="anuindetNumeroOperacionesOriginalAcreedor" runat="server" tabindex="41" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarAnulacion" runat="server" Text="Anular" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta de anulaci�n</legend>
                    <dl>
                        <dt>Trama:</dt><dd><input type="text" id="AnulacionSalida" class="trama" runat="server" />
                        </dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="anuoutMessageTypeIdentification" runat="server" maxlength="4" readonly="readonly" tabindex="1" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="anuoutPrimaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="2" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="anuoutSecondaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="3" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="anuoutCodigoProceso" runat="server" maxlength="6" readonly="readonly" tabindex="4" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="anuoutMonto" runat="server" maxlength="12" readonly="readonly" tabindex="5" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="anuoutFechaHoraTransaccion" runat="server" maxlength="10" readonly="readonly" format="MMDDhhmmss" tabindex="6" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="anuoutTrace" runat="server" maxlength="6" readonly="readonly" tabindex="7" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="anuoutFechaCaptura" runat="server" maxlength="4" readonly="readonly" format="MMDD" tabindex="8" /></dd>
                        <dt>Identificaci�n empresa:</dt>
                        <dd><input type="text" id="anuoutIdentificacionEmpresa" runat="server" maxlength="8" readonly="readonly" tabindex="9" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="anuoutRetrievalReferenceNumber" runat="server" maxlength="12" readonly="readonly" tabindex="10" /></dd>
                        <dt>Authorization ID Response:</dt>
                        <dd><input type="text" id="anuoutAuthorizationIDResponse" runat="server" maxlength="6" readonly="readonly" tabindex="11" /></dd>
                        <dt>Response Code:</dt>
                        <dd><input type="text" id="anuoutResponseCode" runat="server" maxlength="2" readonly="readonly" tabindex="12" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="anuoutTerminalID" runat="server" maxlength="8" readonly="readonly" tabindex="13" /></dd>
                        <dt>Transaction Currency Card:</dt>
                        <dd><input type="text" id="anuoutTransactionCurrencyCard" runat="server" maxlength="3" readonly="readonly" tabindex="14" /></dd>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="anuoutDatosReservados" runat="server" maxlength="5" readonly="readonly" tabindex="15" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del documento a extornar:</legend>
                        <dl>
                            <dt>Longitud de la trama:</dt>
                            <dd><input type="text" id="anuoutdetLongitudTrama" runat="server" maxlength="3" readonly="readonly" tabindex="16" /></dd>
                            <dt>C�digo del formato:</dt>
                            <dd><input type="text" id="anuoutdetCodigoFormato" runat="server" maxlength="2" readonly="readonly" tabindex="17" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="anuoutdetBinProcesador" runat="server" maxlength="11" readonly="readonly" tabindex="18" /></dd>
                            <dt>C�digo de acreedor:</dt>
                            <dd><input type="text" id="anuoutdetCodigoAcreedor" runat="server" maxlength="11" readonly="readonly" tabindex="19" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="anuoutdetCodigoProductoServicio" runat="server" maxlength="8" readonly="readonly" tabindex="20" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="anuoutdetCodigoPlazaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="21" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="anuoutdetCodigoAgenciaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="22" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="anuoutdetTipoDatoPago" runat="server" maxlength="2" readonly="readonly" tabindex="23" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="anuoutdetDatoPago" runat="server" maxlength="21" readonly="readonly" tabindex="24" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="anuoutdetCodigoCiudad" runat="server" maxlength="3" readonly="readonly" tabindex="25" /></dd>
                            <dt>Nombre del cliente:</dt>
                            <dd><input type="text" id="anuoutdetNombreCliente" runat="server" maxlength="20" readonly="readonly" tabindex="26" /></dd>
                            <dt>RUC del deudor:</dt>
                            <dd><input type="text" id="anuoutdetRUCDeudor" runat="server" maxlength="15" readonly="readonly" tabindex="27" /></dd>
                            <dt>RUC del acreedor:</dt>
                            <dd><input type="text" id="anuoutdetRUCAcreedor" runat="server" maxlength="15" readonly="readonly" tabindex="28" /></dd>
                            <dt>N�mero de transacciones de Cob. Ori.:</dt>
                            <dd><input type="text" id="anuoutdetNumeroTransaccionesCobOri" runat="server" maxlength="12" readonly="readonly" tabindex="29" /></dd>
                            <dt>N�mero de operaciones original acreedor:</dt>
                            <dd><input type="text" id="anuoutdetNumeroOperacionesOriginalAcreedor" runat="server" maxlength="12" readonly="readonly" tabindex="30" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuoutdetFiller" runat="server" maxlength="30" readonly="readonly" tabindex="31" /></dd>
                            <dt>Origen de respuesta:</dt>
                            <dd><input type="text" id="anuoutdetOrigenRespuesta" runat="server" maxlength="1" readonly="readonly" tabindex="32" /></dd>
                            <dt>C�digo de respuesta extendida:</dt>
                            <dd><input type="text" id="anuoutdetCodigoRespuestaExtendida" runat="server" maxlength="3" readonly="readonly" tabindex="33" /></dd>
                            <dt>Descripci�n de la respuesta aplicativa:</dt>
                            <dd><input type="text" id="anuoutdetDescripcionRespuestaAplicativa" runat="server" maxlength="30" readonly="readonly" tabindex="34" /></dd>
                            <dt>C�digo del producto/servicio:</dt>
                            <dd><input type="text" id="anuoutdetCodigoProductoServicio2" runat="server" maxlength="3" readonly="readonly" tabindex="35" /></dd>
                            <dt>Descripci�n del producto/servicio:</dt>
                            <dd><input type="text" id="anuoutdetDescripcionProductoServicio" runat="server" maxlength="15" readonly="readonly" tabindex="36" /></dd>
                            <dt>Importe del producto/servicio:</dt>
                            <dd><input type="text" id="anuoutdetImporteProductoServicio" runat="server" maxlength="11" readonly="readonly" tabindex="37" /></dd>
                            <dt>Mensaje 1 Marketing:</dt>
                            <dd><input type="text" id="anuoutdetMensaje1Marketing" runat="server" maxlength="40" readonly="readonly" tabindex="38" /></dd>
                            <dt>Mensaje 2 Marketing:</dt>
                            <dd><input type="text" id="anuoutdetMensaje2Marketing" runat="server" maxlength="40" readonly="readonly" tabindex="39" /></dd>
                            <dt>N�mero de documentos:</dt>
                            <dd><input type="text" id="anuoutdetNumeroDocumentos" runat="server" maxlength="2" readonly="readonly" tabindex="40" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuoutdetFiller2" runat="server" maxlength="20" readonly="readonly" tabindex="41" /></dd>
                            <dt>Tipo de documento/servicio:</dt>
                            <dd><input type="text" id="anuoutdetTipoDocumentoServicio" runat="server" maxlength="3" readonly="readonly" tabindex="42" /></dd>
                            <dt>Descripci�n del documento:</dt>
                            <dd><input type="text" id="anuoutdetDescripcionDocumento" runat="server" maxlength="15" readonly="readonly" tabindex="43" /></dd>
                            <dt>N�mero del documento:</dt>
                            <dd><input type="text" id="anuoutdetNumeroDocumento" runat="server" maxlength="16" readonly="readonly" tabindex="44" /></dd>
                            <dt>Per�odo de cotizaci�n:</dt>
                            <dd><input type="text" id="anuoutdetPeriodoCotizacion" runat="server" maxlength="6" readonly="readonly" tabindex="45" /></dd>
                            <dt>Tipo de documento de identidad:</dt>
                            <dd><input type="text" id="anuoutdetTipoDocumentoIdentidad" runat="server" maxlength="2" readonly="readonly" tabindex="46" /></dd>
                            <dt>N�mero de documento de identidad:</dt>
                            <dd><input type="text" id="anuoutdetNumeroDocumentoIdentidad" runat="server" maxlength="15" readonly="readonly" tabindex="47" /></dd>
                            <dt>Fecha de emisi�n:</dt>
                            <dd><input type="text" id="anuoutdetFechaEmision" runat="server" maxlength="8" readonly="readonly" tabindex="48" /></dd>
                            <dt>Fecha de vencimiento:</dt>
                            <dd><input type="text" id="anuoutdetFechaVencimiento" runat="server" maxlength="8" readonly="readonly" tabindex="49" /></dd>
                            <dt>Importe pagado:</dt>
                            <dd><input type="text" id="anuoutdetImporteAnuladoDocumento" runat="server" maxlength="11" readonly="readonly" tabindex="50" /></dd>
                            <dt>C�digo de concepto 1:</dt>
                            <dd><input type="text" id="anuoutdetCodigoConcepto1" runat="server" maxlength="2" readonly="readonly" tabindex="51" /></dd>
                            <dt>Importe de concepto 1:</dt>
                            <dd><input type="text" id="anuoutdetImporteConcepto1" runat="server" maxlength="11" readonly="readonly" tabindex="52" /></dd>
                            <dt>C�digo de concepto 2:</dt>
                            <dd><input type="text" id="anuoutdetCodigoConcepto2" runat="server" maxlength="2" readonly="readonly" tabindex="53" /></dd>
                            <dt>Importe de concepto 2:</dt>
                            <dd><input type="text" id="anuoutdetImporteConcepto2" runat="server" maxlength="11" readonly="readonly" tabindex="54" /></dd>
                            <dt>C�digo de concepto 3:</dt>
                            <dd><input type="text" id="anuoutdetCodigoConcepto3" runat="server" maxlength="2" readonly="readonly" tabindex="55" /></dd>
                            <dt>Importe de concepto 3:</dt>
                            <dd><input type="text" id="anuoutdetImporteConcepto3" runat="server" maxlength="11" readonly="readonly" tabindex="56" /></dd>
                            <dt>C�digo de concepto 4:</dt>
                            <dd><input type="text" id="anuoutdetCodigoConcepto4" runat="server" maxlength="2" readonly="readonly" tabindex="57" /></dd>
                            <dt>Importe de concepto 4:</dt>
                            <dd><input type="text" id="anuoutdetImporteConcepto4" runat="server" maxlength="11" readonly="readonly" tabindex="58" /></dd>
                            <dt>C�digo de concepto 5:</dt>
                            <dd><input type="text" id="anuoutdetCodigoConcepto5" runat="server" maxlength="2" readonly="readonly" tabindex="59" /></dd>
                            <dt>Importe de concepto 5:</dt>
                            <dd><input type="text" id="anuoutdetImporteConcepto5" runat="server" maxlength="11" readonly="readonly" tabindex="60" /></dd>
                            <dt>Indicador de facturaci�n:</dt>
                            <dd><input type="text" id="anuoutdetIndicadorComprobante" runat="server" maxlength="1" readonly="readonly" tabindex="61" /></dd>
                            <dt>N�mero de factura:</dt>
                            <dd><input type="text" id="anuoutdetNumeroFacturaAnulada" runat="server" maxlength="11" readonly="readonly" tabindex="62" /></dd>
                            <dt>Referencia de la deuda:</dt>
                            <dd><input type="text" id="anuoutdetReferenciaDeuda" runat="server" maxlength="16" readonly="readonly" tabindex="63" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuoutdetFiller3" runat="server" maxlength="34" readonly="readonly" tabindex="64" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlExtornarPago" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Extorno autom�tico de pago de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="txtPagoAutoEntrada" runat="server" class="trama" maxlength="711" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />  
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="pagautinMessageTypeIdentification" runat="server" tabindex="1" maxlength="4" value="0400" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="pagautinPrimaryBitMap" runat="server" tabindex="2" maxlength="16" value="F220848188E08000" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="pagautinSecondayBitMap" runat="server" tabindex="3" maxlength="16" value="0000004000000018" readonly="readonly" /></dd>
                        <dt>N�mero de tarjeta:</dt>
                        <dd><input type="text" id="pagautinNumeroTarjeta" runat="server" tabindex="4" maxlength="18" value="160000000000000000" readonly="readonly" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="pagautinCodigoProceso" runat="server" tabindex="5" maxlength="6" value="945000" readonly="readonly" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="pagautinMonto" runat="server" tabindex="6" maxlength="12" value="000000001100" readonly="readonly" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="pagautinFechaHoraTransaccion" runat="server" tabindex="7" maxlength="10" format="MMDDhhmmss" readonly="readonly" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="pagautinTrace" runat="server" tabindex="8" maxlength="6" value="123456" title="N�mero de operaci�n" /></dd>
                        <dt>Fecha de captura:</dt><dd>
                        <input type="text" id="pagautinFechaCaptura" runat="server" tabindex="9" maxlength="4" format="MMDD" readonly="readonly" /></dd>
                        <dt>Modo de ingreso de datos:</dt>
                        <dd><input type="text" id="pagautinModoIngresoDatos" runat="server" tabindex="10" maxlength="3" value="000" readonly="readonly" /></dd>
                        <dt>Canal:</dt>
                        <dd><input type="text" id="pagautinCanal" runat="server" tabindex="11" maxlength="2" value="12" readonly="readonly" /></dd>
                        <dt>Bin Adquiriente:</dt>
                        <dd><input type="text" id="pagautinBinAdquiriente" runat="server" tabindex="12" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Forward Institution Code:</dt>
                        <dd><input type="text" id="pagautinForwardInstitutionCode" runat="server" tabindex="13" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="pagautinRetrievalReferenceNumber" runat="server" tabindex="14" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                        <dt>Response code:</dt>
                        <dd><input type="text" id="pagautinResponseCode" runat="server" tabindex="15" maxlength="2" value="22" readonly="readonly" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="pagautinTerminalID" runat="server" tabindex="16" maxlength="8" value="12345678" readonly="readonly" /></dd>
                        <dt>Comercio:</dt>
                        <dd><input type="text" id="pagautinComercio" runat="server" tabindex="17" maxlength="15" value="000000000000000" readonly="readonly" /></dd>
                        <dt>Card Acceptor Location:</dt>
                        <dd><input type="text" id="pagautinCardAcceptorLocation" runat="server" tabindex="18" maxlength="40" value="                                        " readonly="readonly" />
                        </dd><dt>Transaction Currency Code:</dt>
                        <dd><input type="text" id="pagautinTransactionCurrencyCode" runat="server" tabindex="19" maxlength="3" value="604" title="Moneda" format="604:Soles, 840:D�lares" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Original Data Elements:</legend>
                        <dl>
                            <dt>Message Type Identification:</dt>
                            <dd><input type="text" id="pagautindetMessageTypeIdentification" runat="server" tabindex="20" maxlength="4" value="0200" readonly="readonly" /></dd>
                            <dt>Trace:</dt>
                            <dd><input type="text" id="pagautindetTrace" runat="server" tabindex="21" maxlength="6" value="123456" title="N�mero de operaci�n de pago" /></dd>
                            <dt>Fecha y hora de transacci�n:</dt>
                            <dd><input type="text" id="pagautindetFechaHoraTransaccion" runat="server" tabindex="22" maxlength="10" readonly="readonly" /></dd>
                            <dt>Bin adquiriente:</dt>
                            <dd><input type="text" id="pagautindetBinAdquiriente" runat="server" tabindex="23" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>Forward Institution Code:</dt>
                            <dd><input type="text" id="pagautindetForwardInstitutionCode" runat="server" tabindex="24" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <dl>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="pagautinDatosReservados" runat="server" tabindex="25" maxlength="5" value="00220" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del requerimiento:</legend>
                        <dl>
                            <dt>Longitud de la trama:</dt>
                            <dd><input type="text" id="pagautindetLongitudTrama" runat="server" tabindex="26" maxlength="3" value="428" readonly="readonly" /></dd>
                            <dt>C�digo de  formato:</dt>
                            <dd><input type="text" id="pagautindetCodigoFormato" runat="server" tabindex="27" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="pagautindetBinProcesador" runat="server" tabindex="28" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>C�digo acreedor:</dt>
                            <dd><input type="text" id="pagautindetCodigoAcreedor" runat="server" tabindex="29" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="pagautindetCodigoProductoServicio" runat="server" tabindex="30" maxlength="8" value="000     " readonly="readonly" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="pagautindetCodigoPlazaRecaudador" runat="server" tabindex="31" maxlength="4" value="0000" readonly="readonly" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="pagautindetCodigoAgenciaRecaudador" runat="server" tabindex="32" maxlength="4" value="1234" title="C�digo de agencia" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="pagautindetTipoDatoPago" runat="server" tabindex="33" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="pagautindetDatoPago" runat="server" tabindex="34" maxlength="21" value="123456789012345678901" title="CIP" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="pagautindetCodigoCiudad" runat="server" tabindex="35" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>N�mero de producto/servicio pagado:</dt>
                            <dd><input type="text" id="pagautindetNumeroProductoServicioPagado" runat="server" tabindex="36" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>N�mero total de documento pagado:</dt>
                            <dd><input type="text" id="pagautindetNumeroTotalDocumentoPagado" runat="server" tabindex="37" maxlength="3" value="001" readonly="readonly" /></dd>
                            <dt>Filler</dt>
                            <dd><input type="text" id="pagautindetFiller" runat="server" tabindex="38" maxlength="10" value="          " readonly="readonly" /></dd>
                            <dt>Medio de pago:</dt>
                            <dd><input type="text" id="pagautindetMedioPago" runat="server" tabindex="39" maxlength="2" value="00" title="00:Efectivo, 01:Cheque, 02:Tarjeta de cr�dito, 03:Tarjeta de d�bito, 04:Tarjeta virtual, 05:Cargo a cuenta, 06:Efectivo+Cargo a cuenta, 07:Efectivo+Cheque" /></dd>
                            <dt>Importe pagado efectivo:</dt><dd>
                            <input type="text" id="pagautindetImportePagadoEfectivo" runat="server" tabindex="40" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Importe pagado con cuenta:</dt>
                            <dd><input type="text" id="pagautindetImportePagadoCuenta" runat="server" tabindex="41" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Numero cheque 1:</dt>
                            <dd><input type="text" id="pagautindetNumeroCheque1" runat="server" tabindex="42" maxlength="15" value="123456789012345" readonly="readonly" /></dd>
                            <dt>Banco girador 1:</dt>
                            <dd><input type="text" id="pagautindetBancoGirador1" runat="server" tabindex="43" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>Importe cheque 1:</dt>
                            <dd><input type="text" id="pagautindetImporteCheque1" runat="server" tabindex="44" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Plaza Cheque 1:</dt>
                            <dd><input type="text" id="pagautindetPlazaCheque1" runat="server" tabindex="45" maxlength="1" value="1" readonly="readonly" /></dd>
                            <dt>Numero Cheque 2:</dt>
                            <dd><input type="text" id="pagautindetNumeroCheque2" runat="server" tabindex="46" maxlength="15" value="123456789012345" readonly="readonly" /></dd>
                            <dt>Banco girador 2:</dt>
                            <dd><input type="text" id="pagautindetBancoGirador2" runat="server" tabindex="47" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>Importe cheque 2:</dt>
                            <dd><input type="text" id="pagautindetImporteCheque2" runat="server" tabindex="48" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Plaza Cheque 2:</dt>
                            <dd><input type="text" id="pagautindetPlazaCheque2" runat="server" tabindex="49" maxlength="1" value="1" readonly="readonly" /></dd>
                            <dt>Numero Cheque 3:</dt>
                            <dd><input type="text" id="pagautindetNumeroCheque3" runat="server" tabindex="50" maxlength="15" value="123456789012345" readonly="readonly" /></dd>
                            <dt>Banco girador 3:</dt>
                            <dd><input type="text" id="pagautindetBancoGirador3" runat="server" tabindex="51" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>Importe cheque 3:</dt>
                            <dd><input type="text" id="pagautindetImporteCheque3" runat="server" tabindex="52" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Plaza Cheque 3:</dt>
                            <dd><input type="text" id="pagautindetPlazaCheque3" runat="server" tabindex="53" maxlength="1" value="1" readonly="readonly" /></dd>
                            <dt>Moneda de pago:</dt>
                            <dd><input type="text" id="pagautindetMonedaPago" runat="server" tabindex="54" maxlength="3" value="604" readonly="readonly" /></dd>
                            <dt>Tipo de cambio aplicado:</dt>
                            <dd><input type="text" id="pagautindetTipoCambioAplicado" runat="server" tabindex="55" maxlength="11" value="00000000270" readonly="readonly" /></dd>
                            <dt>Pago total realizado:</dt>
                            <dd><input type="text" id="pagautindetPagoTotalRealizado" runat="server" tabindex="56" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautindetFiller2" runat="server" tabindex="57" maxlength="10" value="          " readonly="readonly" /></dd>
                            <dt>C�digo del servicio pagado:</dt>
                            <dd><input type="text" id="pagautindetCodigoServicioPagado" runat="server" tabindex="58" maxlength="3" value="000" title="C�digo de servicio" /></dd>
                            <dt>Estado del deudor:</dt>
                            <dd><input type="text" id="pagautindetEstadoDeudor" runat="server" tabindex="59" maxlength="2" value="VI" readonly="readonly" /></dd>
                            <dt>Importe total por producto/servicio:</dt>
                            <dd><input type="text" id="pagautindetImporteTotalProductoServicio" runat="server" tabindex="60" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>N�mero de cuenta de abono:</dt>
                            <dd><input type="text" id="pagautindetNumeroCuentaAbono" runat="server" tabindex="61" maxlength="19" value="1234567891234567891" readonly="readonly" /></dd>
                            <dt>N�mero de referencia del abono:</dt>
                            <dd><input type="text" id="pagautindetNumeroReferenciaAbono" runat="server" tabindex="62" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                            <dt>N�mero de documentos pagados:</dt>
                            <dd><input type="text" id="pagautindetNumeroDocumentosPagados" runat="server" tabindex="63" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautindetFiller3" runat="server" tabindex="64" maxlength="10" value="          " readonly="readonly" /></dd>
                            <dt>Tipo de documento de pago:</dt>
                            <dd><input type="text" id="pagautindetTipoDocumentoPago" runat="server" tabindex="65" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>N�mero de documento de pago:</dt>
                            <dd><input type="text" id="pagautindetNumeroDocumentoPago" runat="server" tabindex="66" maxlength="16" value="                " readonly="readonly" /></dd>
                            <dt>Per�odo de cotizaci�n:</dt>
                            <dd><input type="text" id="pagautindetPeriodoCotizacion" runat="server" tabindex="67" maxlength="6" value="      " readonly="readonly" /></dd>
                            <dt>Tipo documento ID Deudor:</dt>
                            <dd><input type="text" id="pagautindetTipoDocumentoIDDeudor" runat="server" tabindex="68" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>N�mero documento ID Deudor:</dt>
                            <dd><input type="text" id="pagautindetNumeroDocumentoIDDeudor" runat="server" tabindex="69" maxlength="15" value="               " readonly="readonly" /></dd>
                            <dt>Importe original de la deuda:</dt>
                            <dd><input type="text" id="pagautindetImporteOriginalDeuda" runat="server" tabindex="70" maxlength="11" value="00000002200" readonly="readonly" /></dd>
                            <dt>Importe pagado del documento:</dt>
                            <dd><input type="text" id="pagautindetImportePagadoDocumento" runat="server" tabindex="71" maxlength="11" value="00000002200" title="Monto" /></dd>
                            <dt>C�digo de concepto 1:</dt>
                            <dd><input type="text" id="pagautindetCodigoConcepto1" runat="server" tabindex="72" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>Importe de concepto 1:</dt>
                            <dd><input type="text" id="pagautindetImporteConcepto1" runat="server" tabindex="73" maxlength="11" value="           " readonly="readonly" /></dd>
                            <dt>C�digo de concepto 2:</dt>
                            <dd><input type="text" id="pagautindetCodigoConcepto2" runat="server" tabindex="74" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>Importe de concepto 2:</dt>
                            <dd><input type="text" id="pagautindetImporteConcepto2" runat="server" tabindex="75" maxlength="11" value="           " readonly="readonly" /></dd>
                            <dt>C�digo de concepto 3:</dt>
                            <dd><input type="text" id="pagautindetCodigoConcepto3" runat="server" tabindex="76" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>Importe de concepto 3:</dt>
                            <dd><input type="text" id="pagautindetImporteConcepto3" runat="server" tabindex="77" maxlength="11" value="           " readonly="readonly" /></dd>
                            <dt>C�digo de concepto 4:</dt>
                            <dd><input type="text" id="pagautindetCodigoConcepto4" runat="server" tabindex="78" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>Importe de concepto 4:</dt>
                            <dd><input type="text" id="pagautindetImporteConcepto4" runat="server" tabindex="79" maxlength="11" value="           " readonly="readonly" /></dd>
                            <dt>C�digo de concepto 5:</dt>
                            <dd><input type="text" id="pagautindetCodigoConcepto5" runat="server" tabindex="80" maxlength="2" value="  " readonly="readonly" /></dd>
                            <dt>Importe de concepto 5:</dt>
                            <dd><input type="text" id="pagautindetImporteConcepto5" runat="server" tabindex="81" maxlength="11" value="           " readonly="readonly" /></dd>
                            <dt>Referencia de la deuda:</dt>
                            <dd><input type="text" id="pagautindetReferenciaDeuda" runat="server" tabindex="82" maxlength="16" value="                " readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautindetFiller4" runat="server" tabindex="83" maxlength="34" value="                                  " readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarPagoAuto" runat="server" Text="Extornar Pago" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta de extorno autom�tico de pago</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="PagoAutoSalida" class="trama" runat="server" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="pagautoutMessageTypeIdentification" runat="server" maxlength="4" readonly="readonly" tabindex="1" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="pagautoutPrimaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="2" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="pagautoutSecondaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="3" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="pagautoutCodigoProceso" runat="server" maxlength="6" readonly="readonly" tabindex="4" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="pagautoutMonto" runat="server" maxlength="12" readonly="readonly" tabindex="5" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="pagautoutFechaHoraTransaccion" runat="server" maxlength="10" readonly="readonly" format="MMDDhhmmss" tabindex="6" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="pagautoutTrace" runat="server" maxlength="6" readonly="readonly" tabindex="7" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="pagautoutFechaCaptura" runat="server" maxlength="4" readonly="readonly" format="MMDD" tabindex="8" /></dd>
                        <dt>Identificaci�n empresa:</dt>
                        <dd><input type="text" id="pagautoutIdentificacionEmpresa" runat="server" maxlength="8" readonly="readonly" tabindex="9" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="pagautoutRetrievalReferenceNumber" runat="server" maxlength="12" readonly="readonly" tabindex="10" /></dd>
                        <dt>Authorization ID Response:</dt>
                        <dd><input type="text" id="pagautoutAuthorizationIDResponse" runat="server" maxlength="6" readonly="readonly" tabindex="11" /></dd>
                        <dt>Response Code:</dt>
                        <dd><input type="text" id="pagautoutResponseCode" runat="server" maxlength="2" readonly="readonly" tabindex="12" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="pagautoutTerminalID" runat="server" maxlength="8" readonly="readonly" tabindex="13" /></dd>
                        <dt>Transaction Currency Card:</dt>
                        <dd><input type="text" id="pagautoutTransactionCurrencyCard" runat="server" maxlength="3" readonly="readonly" tabindex="14" /></dd>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="pagautoutDatosReservados" runat="server" maxlength="5" readonly="readonly" tabindex="15" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del documento a extornar:</legend>
                        <dl>
                            <dt>Tama�o del bloque:</dt>
                            <dd><input type="text" id="pagautoutdetTamanoBloque" runat="server" maxlength="3" readonly="readonly" tabindex="16" /></dd>
                            <dt>C�digo del formato:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoFormato" runat="server" maxlength="2" readonly="readonly" tabindex="17" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="pagautoutdetBinProcesador" runat="server" maxlength="11" readonly="readonly" tabindex="18" /></dd>
                            <dt>C�digo de acreedor:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoAcreedor" runat="server" maxlength="11" readonly="readonly" tabindex="19" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoProductoServicio" runat="server" maxlength="8" tabindex="20" title="C�digo de servicio" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoPlazaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="21" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoAgenciaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="22" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="pagautoutdetTipoDatoPago" runat="server" maxlength="2" readonly="readonly" tabindex="23" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="pagautoutdetDatoPago" runat="server" maxlength="21" readonly="readonly" tabindex="24" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoCiudad" runat="server" maxlength="3" readonly="readonly" tabindex="25" /></dd>
                            <dt>N�mero de operaci�n cobranza:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroOperacionCobranza" runat="server" maxlength="12" readonly="readonly" tabindex="26" /></dd>
                            <dt>N�mero de operaci�n acreedor:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroOperacionAcreedor" runat="server" maxlength="12" readonly="readonly" tabindex="27" /></dd>
                            <dt>N�mero de producto/servicio pagado:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroProductoServicioPagado" runat="server" maxlength="2" readonly="readonly" tabindex="28" /></dd>
                            <dt>N�mero total de documentos pagados:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroTotalDocumentosPagados" runat="server" maxlength="3" readonly="readonly" tabindex="29" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautoutdetFiller" runat="server" maxlength="10" readonly="readonly" tabindex="30" /></dd>
                            <dt>Origen de respuesta:</dt>
                            <dd><input type="text" id="pagautoutdetOrigenRespuesta" runat="server" maxlength="1" readonly="readonly" tabindex="31" /></dd>
                            <dt>C�digo de respuesta extendida:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoRespuestaExtendida" runat="server" maxlength="3" readonly="readonly" tabindex="32" /></dd>
                            <dt>Descripci�n de la respuesta aplicativa:</dt>
                            <dd><input type="text" id="pagautoutdetDescripcionRespuestaAplicativa" runat="server" maxlength="30" readonly="readonly" tabindex="33" /></dd>
                            <dt>Nombre del deudor:</dt>
                            <dd><input type="text" id="pagautoutdetNombreDeudor" runat="server" maxlength="20" readonly="readonly" tabindex="34" /></dd>
                            <dt>RUC del deudor:</dt>
                            <dd><input type="text" id="pagautoutdetRUCDeudor" runat="server" maxlength="15" readonly="readonly" tabindex="35" /></dd>
                            <dt>RUC del acreedor:</dt>
                            <dd><input type="text" id="pagautoutdetRUCAcreedor" runat="server" maxlength="15" readonly="readonly" tabindex="36" /></dd>
                            <dt>C�digo de zona del acreedor:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoZonaAcreedor" runat="server" maxlength="6" readonly="readonly" tabindex="37" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautoutdetFiller2" runat="server" maxlength="20" readonly="readonly" tabindex="38" /></dd>
                            <dt>C�digo del producto/servicio:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoProductoServicio2" runat="server" maxlength="3" readonly="readonly" tabindex="39" /></dd>
                            <dt>Descripci�n del producto/servicio:</dt>
                            <dd><input type="text" id="pagautoutdetDescripcionProductoServicio" runat="server" maxlength="15" readonly="readonly" tabindex="40" /></dd>
                            <dt>Importe total por producto/servicio:</dt>
                            <dd><input type="text" id="pagautoutdetImporteTotalProductoServicio" runat="server" maxlength="11" readonly="readonly" tabindex="41" /></dd>
                            <dt>Mensaje 1:</dt>
                            <dd><input type="text" id="pagautoutdetMensaje1" runat="server" maxlength="40" readonly="readonly" tabindex="42" /></dd>
                            <dt>Mensaje 2:</dt>
                            <dd><input type="text" id="pagautoutdetMensaje2" runat="server" maxlength="40" readonly="readonly" tabindex="43" /></dd>
                            <dt>N�mero de documentos:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroDocumentos" runat="server" maxlength="2" readonly="readonly" tabindex="44" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautoutdetFiller3" runat="server" maxlength="20" readonly="readonly" tabindex="45" /></dd>
                            <dt>Tipo de servicio:</dt>
                            <dd><input type="text" id="pagautoutdetTipoServicio" runat="server" maxlength="3" readonly="readonly" tabindex="46" /></dd>
                            <dt>Descripci�n del documento:</dt>
                            <dd><input type="text" id="pagautoutdetDescripcionDocumento" runat="server" maxlength="15" readonly="readonly" tabindex="47" /></dd>
                            <dt>N�mero del documento:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroDocumento" runat="server" maxlength="16" readonly="readonly" tabindex="48" /></dd>
                            <dt>Per�odo de cotizaci�n:</dt>
                            <dd><input type="text" id="pagautoutdetPeriodoCotizacion" runat="server" maxlength="6" readonly="readonly" tabindex="49" /></dd>
                            <dt>Tipo de documento de identidad:</dt>
                            <dd><input type="text" id="pagautoutdetTipoDocumentoIdentidad" runat="server" maxlength="2" readonly="readonly" tabindex="50" /></dd>
                            <dt>N�mero de documento de identidad:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroDocumentoIdentidad" runat="server" maxlength="15" readonly="readonly" tabindex="51" /></dd>
                            <dt>Fecha de emisi�n:</dt>
                            <dd><input type="text" id="pagautoutdetFechaEmision" runat="server" maxlength="8" readonly="readonly" tabindex="52" /></dd>
                            <dt>Fecha de vencimiento:</dt>
                            <dd><input type="text" id="pagautoutdetFechaVencimiento" runat="server" maxlength="8" readonly="readonly" tabindex="53" /></dd>
                            <dt>Importe pagado:</dt>
                            <dd><input type="text" id="pagautoutdetImportePagado" runat="server" maxlength="11" readonly="readonly" tabindex="54" /></dd>
                            <dt>C�digo de concepto 1:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoConcepto1" runat="server" maxlength="2" readonly="readonly" tabindex="55" /></dd>
                            <dt>Importe de concepto 1:</dt>
                            <dd><input type="text" id="pagautoutdetImporteConcepto1" runat="server" maxlength="11" readonly="readonly" tabindex="56" /></dd>
                            <dt>C�digo de concepto 2:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoConcepto2" runat="server" maxlength="2" readonly="readonly" tabindex="57" /></dd>
                            <dt>Importe de concepto 2:</dt>
                            <dd><input type="text" id="pagautoutdetImporteConcepto2" runat="server" maxlength="11" readonly="readonly" tabindex="58" /></dd>
                            <dt>C�digo de concepto 3:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoConcepto3" runat="server" maxlength="2" readonly="readonly" tabindex="59" /></dd>
                            <dt>Importe de concepto 3:</dt>
                            <dd><input type="text" id="pagautoutdetImporteConcepto3" runat="server" maxlength="11" readonly="readonly" tabindex="60" /></dd>
                            <dt>C�digo de concepto 4:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoConcepto4" runat="server" maxlength="2" readonly="readonly" tabindex="61" /></dd>
                            <dt>Importe de concepto 4:</dt>
                            <dd><input type="text" id="pagautoutdetImporteConcepto4" runat="server" maxlength="11" readonly="readonly" tabindex="62" /></dd>
                            <dt>C�digo de concepto 5:</dt>
                            <dd><input type="text" id="pagautoutdetCodigoConcepto5" runat="server" maxlength="2" readonly="readonly" tabindex="63" /></dd>
                            <dt>Importe de concepto 5:</dt>
                            <dd><input type="text" id="pagautoutdetImporteConcepto5" runat="server" maxlength="11" readonly="readonly" tabindex="64" /></dd>
                            <dt>Indicador de facturaci�n:</dt>
                            <dd><input type="text" id="pagautoutdetIndicadorFacturacion" runat="server" maxlength="1" readonly="readonly" tabindex="65" /></dd>
                            <dt>N�mero de factura:</dt>
                            <dd><input type="text" id="pagautoutdetNumeroFactura" runat="server" maxlength="11" readonly="readonly" tabindex="66" /></dd>
                            <dt>Referencia de la deuda:</dt>
                            <dd><input type="text" id="pagautoutdetReferenciaDeuda" runat="server" maxlength="16" readonly="readonly" tabindex="67" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="pagautoutdetFiller4" runat="server" maxlength="34" readonly="readonly" tabindex="68" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <asp:Panel ID="pnlExtornarAnulacion" runat="server" Visible="false">
                <fieldset class="entrada">
                    <legend>Extorno autom�tico de anulaci�n de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="txtAnulacionAutoEntrada" runat="server" class="trama" maxlength="395" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarentrada">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarentrada">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="anuautinMessageTypeIdentification" runat="server" tabindex="1" maxlength="4" value="0400" readonly="readonly" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="anuautinPrimaryBitMap" runat="server" tabindex="2" maxlength="16" value="F220848188E08000" readonly="readonly" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="anuautinSecondaryBitMap" runat="server" tabindex="3" maxlength="16" value="0000004000000018" readonly="readonly" /></dd>
                        <dt>N�mero de tarjeta:</dt>
                        <dd><input type="text" id="anuautinNumeroTarjeta" runat="server" tabindex="4" maxlength="18" value="160000000000000000" readonly="readonly" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="anuautinCodigoProceso" runat="server" tabindex="5" maxlength="6" value="965000" readonly="readonly" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="anuautinMonto" runat="server" tabindex="6" maxlength="12" value="000000001100" title="Monto" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="anuautinFechaHoraTransaccion" runat="server" tabindex="7" maxlength="10" format="MMDDhhmmss" readonly="readonly" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="anuautinTrace" runat="server" tabindex="8" maxlength="6" value="123456" title="N�mero de operaci�n" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="anuautinFechaCaptura" runat="server" tabindex="9" maxlength="4" format="MMDD" readonly="readonly" /></dd>
                        <dt>Modo de ingreso de datos:</dt>
                        <dd><input type="text" id="anuautinModoIngresoDatos" runat="server" tabindex="10" maxlength="3" value="123" readonly="readonly" /></dd>
                        <dt>Canal:</dt>
                        <dd><input type="text" id="anuautinCanal" runat="server" tabindex="11" maxlength="2" value="12" readonly="readonly" /></dd>
                        <dt>Bin Adquiriente:</dt>
                        <dd><input type="text" id="anuautinBinAdquiriente" runat="server" tabindex="12" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Forward Institution Code:</dt>
                        <dd><input type="text" id="anuautinForwardInstitutionCode" runat="server" tabindex="13" maxlength="8" value="06520900" readonly="readonly" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="anuautinRetrievalReferenceNumber" runat="server" tabindex="14" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                        <dt>Response Code:</dt>
                        <dd><input type="text" id="anuautinResponseCode" runat="server" tabindex="15" maxlength="2" value="12" readonly="readonly" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="anuautinTerminalID" runat="server" tabindex="16" maxlength="8" value="12345678" readonly="readonly" /></dd>
                        <dt>Comercio:</dt>
                        <dd><input type="text" id="anuautinComercio" runat="server" tabindex="17" maxlength="15" value="000000000000000" readonly="readonly" /></dd>
                        <dt>Card Acceptor Location:</dt>
                        <dd><input type="text" id="anuautinCardAcceptorLocation" runat="server" tabindex="18" maxlength="40" value="                                        " readonly="readonly" /></dd>
                        <dt>Transaction Currency Code:</dt>
                        <dd><input type="text" id="anuautinTransactionCurrencyCode" runat="server" tabindex="19" maxlength="3" value="604" title="Moneda" format="604:Soles, 840:D�lares" /></dd></dl>
                    <fieldset>
                        <legend>Original Data Elements:</legend>
                        <dl>
                            <dt>Message Type Identification:</dt>
                            <dd><input type="text" id="anuautindetMessageTypeIdentification" runat="server" tabindex="20" maxlength="4" value="0200" readonly="readonly" /></dd>
                            <dt>Trace:</dt>
                            <dd><input type="text" id="anuautindetTrace" runat="server" tabindex="21" maxlength="6" value="123456" title="N�mero de operaci�n de anulaci�n" /></dd>
                            <dt>Fecha y hora de transacci�n:</dt>
                            <dd><input type="text" id="anuautindetFechaHoraTransaccion" runat="server" tabindex="22" maxlength="10" readonly="readonly" /></dd>
                            <dt>Bin adquiriente:</dt>
                            <dd><input type="text" id="anuautindetBinAdquiriente" runat="server" tabindex="23" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>Forward Institution Code:</dt>
                            <dd><input type="text" id="anuautindetForwardInstitutionCode" runat="server" tabindex="24" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <dl>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="anuautinDatosReservados" runat="server" tabindex="25" maxlength="5" value="00220" readonly="readonly" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del requerimiento:</legend>
                        <dl>
                            <dt>Longitud de dato:</dt>
                            <dd><input type="text" id="anuautindetLongitudDato" runat="server" tabindex="26" maxlength="3" value="152" readonly="readonly" /></dd>
                            <dt>C�digo de  formato:</dt>
                            <dd><input type="text" id="anuautindetCodigoFormato" runat="server" tabindex="27" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="anuautindetBinProcesador" runat="server" tabindex="28" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>C�digo acreedor:</dt>
                            <dd><input type="text" id="anuautindetCodigoAcreedor" runat="server" tabindex="29" maxlength="11" value="12345678901" readonly="readonly" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="anuautindetCodigoProductoServicio" runat="server" tabindex="30" maxlength="8" value="00012345" readonly="readonly" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="anuautindetCodigoPlazaRecaudador" runat="server" tabindex="31" maxlength="4" value="0000" readonly="readonly" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="anuautindetCodigoAgenciaRecaudador" runat="server" tabindex="32" maxlength="4" value="1234" title="C�digo de agencia" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="anuautindetTipoDatoPago" runat="server" tabindex="33" maxlength="2" value="01" readonly="readonly" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="anuautindetDatoPago" runat="server" tabindex="34" maxlength="21" value="123456789012345678901" title="CIP" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="anuautindetCodigoCiudad" runat="server" tabindex="35" maxlength="3" value="123" readonly="readonly" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuautindetFiller" runat="server" tabindex="36" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                            <dt>Filler (Recibo a anular):</dt>
                            <dd><input type="text" id="anuautindetFiller2" runat="server" tabindex="37" maxlength="0" value="" readonly="readonly" /></dd>
                            <dt>Tipo de servicio:</dt>
                            <dd><input type="text" id="anuautindetTipoServicio" runat="server" tabindex="38" maxlength="3" value="123" title="C�digo de servicio" /></dd>
                            <dt>N�mero de documento:</dt>
                            <dd><input type="text" id="anuautindetNumeroDocumento" runat="server" tabindex="39" maxlength="16" value="1234567890123456" readonly="readonly" /></dd>
                            <dt>Disponible:</dt>
                            <dd><input type="text" id="anuautindetDisponible" runat="server" tabindex="40" maxlength="31" value="                               " readonly="readonly" /></dd>
                            <dt>N�mero de transacciones de Cob. Ori.:</dt>
                            <dd><input type="text" id="anuautindetNumeroTransaccionesCobOri" runat="server" tabindex="41" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                            <dt>N�mero de operaciones original acreedor:</dt>
                            <dd><input type="text" id="anuautindetNumeroOperacionesOriginalAcreedor" runat="server" tabindex="42" maxlength="12" value="123456789012" readonly="readonly" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Button ID="btnProcesarAnulacionAuto" runat="server" Text="Extornar Anulaci�n" CssClass="botonentrada" />
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta de extorno autom�tico de anulaci�n de CIPs</legend>
                    <dl>
                        <dt>Trama:</dt>
                        <dd><input type="text" id="AnulacionAutoSalida" class="trama" runat="server" /></dd>
                    </dl>
                    <center>
                        <a href="#" class="desensamblarsalida">Desensamblar &dArr;</a>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        <a href="#" class="ensamblarsalida">&uArr; Ensamblar</a>
                    </center>
                    <hr />
                    <dl>
                        <dt>Message Type Identification:</dt>
                        <dd><input type="text" id="anuautoutMessageTypeIdentification" runat="server" maxlength="4" readonly="readonly" tabindex="1" /></dd>
                        <dt>Primary Bit Map:</dt>
                        <dd><input type="text" id="anuautoutPrimaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="2" /></dd>
                        <dt>Secondary Bit Map:</dt>
                        <dd><input type="text" id="anuautoutSecondaryBitMap" runat="server" maxlength="16" readonly="readonly" tabindex="3" /></dd>
                        <dt>C�digo de proceso:</dt>
                        <dd><input type="text" id="anuautoutCodigoProceso" runat="server" maxlength="6" readonly="readonly" tabindex="4" /></dd>
                        <dt>Monto:</dt>
                        <dd><input type="text" id="anuautoutMonto" runat="server" maxlength="12" readonly="readonly" tabindex="5" /></dd>
                        <dt>Fecha y hora de transacci�n:</dt>
                        <dd><input type="text" id="anuautoutFechaHoraTransaccion" runat="server" maxlength="10" readonly="readonly" format="MMDDhhmmss" tabindex="6" /></dd>
                        <dt>Trace:</dt>
                        <dd><input type="text" id="anuautoutTrace" runat="server" maxlength="6" readonly="readonly" tabindex="7" /></dd>
                        <dt>Fecha de captura:</dt>
                        <dd><input type="text" id="anuautoutFechaCaptura" runat="server" maxlength="4" readonly="readonly" format="MMDD" tabindex="8" /></dd>
                        <dt>Identificaci�n empresa:</dt>
                        <dd><input type="text" id="anuautoutIdentificacionEmpresa" runat="server" maxlength="8" readonly="readonly" tabindex="9" /></dd>
                        <dt>Retrieval Reference Number:</dt>
                        <dd><input type="text" id="anuautoutRetrievalReferenceNumber" runat="server" maxlength="12" readonly="readonly" tabindex="10" /></dd>
                        <dt>Authorization ID Response:</dt>
                        <dd><input type="text" id="anuautoutAuthorizationIDResponse" runat="server" maxlength="6" readonly="readonly" tabindex="11" /></dd>
                        <dt>Response Code:</dt>
                        <dd><input type="text" id="anuautoutResponseCode" runat="server" maxlength="2" readonly="readonly" tabindex="12" /></dd>
                        <dt>Terminal ID:</dt>
                        <dd><input type="text" id="anuautoutTerminalID" runat="server" maxlength="8" readonly="readonly" tabindex="13" /></dd>
                        <dt>Transaction Currency Card:</dt>
                        <dd><input type="text" id="anuautoutTransactionCurrencyCard" runat="server" maxlength="3" readonly="readonly" tabindex="14" /></dd>
                        <dt>Datos reservados:</dt>
                        <dd><input type="text" id="anuautoutDatosReservados" runat="server" maxlength="2" readonly="readonly" tabindex="15" /></dd>
                    </dl>
                    <fieldset>
                        <legend>Datos del documento a extornar:</legend>
                        <dl>
                            <dt>Longitud de la trama:</dt>
                            <dd><input type="text" id="anuautoutdetLongitudTrama" runat="server" maxlength="3" readonly="readonly" tabindex="16" /></dd>
                            <dt>C�digo del formato:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoFormato" runat="server" maxlength="2" readonly="readonly" tabindex="17" /></dd>
                            <dt>Bin procesador:</dt>
                            <dd><input type="text" id="anuautoutdetBinProcesador" runat="server" maxlength="11" readonly="readonly" tabindex="18" /></dd>
                            <dt>C�digo de acreedor:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoAcreedor" runat="server" maxlength="11" readonly="readonly" tabindex="19" /></dd>
                            <dt>C�digo de producto/servicio:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoProductoServicio" runat="server" maxlength="8" readonly="readonly" tabindex="20" /></dd>
                            <dt>C�digo de plaza del recaudador:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoPlazaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="21" /></dd>
                            <dt>C�digo de agencia del recaudador:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoAgenciaRecaudador" runat="server" maxlength="4" readonly="readonly" tabindex="22" /></dd>
                            <dt>Tipo de dato de pago:</dt>
                            <dd><input type="text" id="anuautoutdetTipoDatoPago" runat="server" maxlength="2" readonly="readonly" tabindex="23" /></dd>
                            <dt>Dato de pago:</dt>
                            <dd><input type="text" id="anuautoutdetDatoPago" runat="server" maxlength="21" readonly="readonly" tabindex="24" /></dd>
                            <dt>C�digo de ciudad:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoCiudad" runat="server" maxlength="3" readonly="readonly" tabindex="25" /></dd>
                            <dt>Nombre del cliente:</dt>
                            <dd><input type="text" id="anuautoutdetNombreCliente" runat="server" maxlength="20" readonly="readonly" tabindex="26" /></dd>
                            <dt>RUC del deudor:</dt>
                            <dd><input type="text" id="anuautoutdetRUCDeudor" runat="server" maxlength="15" readonly="readonly" tabindex="27" /></dd>
                            <dt>RUC del acreedor:</dt>
                            <dd><input type="text" id="anuautoutdetRUCAcreedor" runat="server" maxlength="15" readonly="readonly" tabindex="28" /></dd>
                            <dt>N�mero de transacciones de Cob. Ori.:</dt>
                            <dd><input type="text" id="anuautoutdetNumeroTransaccionesCobOri" runat="server" maxlength="12" readonly="readonly" tabindex="29" /></dd>
                            <dt>N�mero de operaciones original acreedor:</dt>
                            <dd><input type="text" id="anuautoutdetNumeroOperacionesOriginalAcreedor" runat="server" maxlength="12" readonly="readonly" tabindex="30" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuautoutdetFiller" runat="server" maxlength="30" readonly="readonly" tabindex="31" /></dd>
                            <dt>Origen de respuesta:</dt>
                            <dd><input type="text" id="anuautoutdetOrigenRespuesta" runat="server" maxlength="1" readonly="readonly" tabindex="32" /></dd>
                            <dt>C�digo de respuesta extendida:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoRespuestaExtendida" runat="server" maxlength="3" readonly="readonly" tabindex="33" /></dd>
                            <dt>Descripci�n de la respuesta aplicativa:</dt>
                            <dd><input type="text" id="anuautoutdetDescripcionRespuestaAplicativa" runat="server" maxlength="30" readonly="readonly" tabindex="34" /></dd>
                            <dt>C�digo del producto/servicio:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoProductoServicio2" runat="server" maxlength="3" readonly="readonly" tabindex="35" /></dd>
                            <dt>Descripci�n del producto/servicio:</dt>
                            <dd><input type="text" id="anuautoutdetDescripcionProductoServicio" runat="server" maxlength="15" readonly="readonly" tabindex="36" /></dd>
                            <dt>Importe del producto/servicio:</dt>
                            <dd><input type="text" id="anuautoutdetImporteProductoServicio" runat="server" maxlength="11" readonly="readonly" tabindex="37" /></dd>
                            <dt>Mensaje 1 Marketing:</dt>
                            <dd><input type="text" id="anuautoutdetMensaje1Marketing" runat="server" maxlength="40" readonly="readonly" tabindex="38" /></dd>
                            <dt>Mensaje 2 Marketing:</dt>
                            <dd><input type="text" id="anuautoutdetMensaje2Marketing" runat="server" maxlength="40" readonly="readonly" tabindex="39" /></dd>
                            <dt>N�mero de documentos:</dt>
                            <dd><input type="text" id="anuautoutdetNumeroDocumentos" runat="server" maxlength="2" readonly="readonly" tabindex="40" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuautoutdetFiller2" runat="server" maxlength="20" readonly="readonly" tabindex="41" /></dd>
                            <dt>Tipo de documento/servicio:</dt>
                            <dd><input type="text" id="anuautoutdetTipoDocumentoServicio" runat="server" maxlength="3" readonly="readonly" tabindex="42" /></dd>
                            <dt>Descripci�n del documento:</dt>
                            <dd><input type="text" id="anuautoutdetDescripcionDocumento" runat="server" maxlength="15" readonly="readonly" tabindex="43" /></dd>
                            <dt>N�mero del documento:</dt>
                            <dd><input type="text" id="anuautoutdetNumeroDocumento" runat="server" maxlength="16" readonly="readonly" tabindex="44" /></dd>
                            <dt>Per�odo de cotizaci�n:</dt>
                            <dd><input type="text" id="anuautoutdetPeriodoCotizacion" runat="server" maxlength="6" readonly="readonly" tabindex="45" /></dd>
                            <dt>Tipo de documento de identidad:</dt>
                            <dd><input type="text" id="anuautoutdetTipoDocumentoIdentidad" runat="server" maxlength="2" readonly="readonly" tabindex="46" /></dd>
                            <dt>N�mero de documento de identidad:</dt>
                            <dd><input type="text" id="anuautoutdetNumeroDocumentoIdentidad" runat="server" maxlength="15" readonly="readonly" tabindex="47" /></dd>
                            <dt>Fecha de emisi�n:</dt>
                            <dd><input type="text" id="anuautoutdetFechaEmision" runat="server" maxlength="8" readonly="readonly" tabindex="48" /></dd>
                            <dt>Fecha de vencimiento:</dt>
                            <dd><input type="text" id="anuautoutdetFechaVencimiento" runat="server" maxlength="8" readonly="readonly" tabindex="49" /></dd>
                            <dt>Importe anulado del documento:</dt>
                            <dd><input type="text" id="anuautoutdetImporteAnuladoDocumento" runat="server" maxlength="11" readonly="readonly" tabindex="50" /></dd>
                            <dt>C�digo de concepto 1:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoConcepto1" runat="server" maxlength="2" readonly="readonly" tabindex="51" /></dd>
                            <dt>Importe de concepto 1:</dt>
                            <dd><input type="text" id="anuautoutdetImporteConcepto1" runat="server" maxlength="11" readonly="readonly" tabindex="52" /></dd>
                            <dt>C�digo de concepto 2:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoConcepto2" runat="server" maxlength="2" readonly="readonly" tabindex="53" /></dd>
                            <dt>Importe de concepto 2:</dt>
                            <dd><input type="text" id="anuautoutdetImporteConcepto2" runat="server" maxlength="11" readonly="readonly" tabindex="54" /></dd>
                            <dt>C�digo de concepto 3:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoConcepto3" runat="server" maxlength="2" readonly="readonly" tabindex="55" /></dd>
                            <dt>Importe de concepto 3:</dt>
                            <dd><input type="text" id="anuautoutdetImporteConcepto3" runat="server" maxlength="11" readonly="readonly" tabindex="56" /></dd>
                            <dt>C�digo de concepto 4:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoConcepto4" runat="server" maxlength="2" readonly="readonly" tabindex="57" /></dd>
                            <dt>Importe de concepto 4:</dt>
                            <dd><input type="text" id="anuautoutdetImporteConcepto4" runat="server" maxlength="11" readonly="readonly" tabindex="58" /></dd>
                            <dt>C�digo de concepto 5:</dt>
                            <dd><input type="text" id="anuautoutdetCodigoConcepto5" runat="server" maxlength="2" readonly="readonly" tabindex="59" /></dd>
                            <dt>Importe de concepto 5:</dt>
                            <dd><input type="text" id="anuautoutdetImporteConcepto5" runat="server" maxlength="11" readonly="readonly" tabindex="60" /></dd>
                            <dt>Indicador de facturaci�n:</dt>
                            <dd><input type="text" id="anuautoutdetIndicadorComprobante" runat="server" maxlength="1" readonly="readonly" tabindex="61" /></dd>
                            <dt>N�mero de factura:</dt>
                            <dd><input type="text" id="anuautoutdetNumeroFacturaAnulada" runat="server" maxlength="11" readonly="readonly" tabindex="62" /></dd>
                            <dt>Referencia de la deuda:</dt>
                            <dd><input type="text" id="anuautoutdetReferenciaDeuda" runat="server" maxlength="16" readonly="readonly" tabindex="63" /></dd>
                            <dt>Filler:</dt>
                            <dd><input type="text" id="anuautoutdetFiller3" runat="server" maxlength="34" readonly="readonly" tabindex="64" /></dd>
                        </dl>
                    </fieldset>
                </fieldset>
            </asp:Panel>
        </asp:Panel>
    </div>
    </form>
</body>
</html>