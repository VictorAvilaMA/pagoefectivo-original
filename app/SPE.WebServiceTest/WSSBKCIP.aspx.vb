Imports System.Configuration.ConfigurationManager
Imports ProxyWSSBK


Partial Public Class WSSBKCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EstablecerValoresPredeterminados()
    End Sub

    Private Sub EstablecerValoresPredeterminados()
        consinFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")
        consinFechaCaptura.Value = DateTime.Now.ToString("MMdd")

        paginFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")
        paginFechaCaptura.Value = DateTime.Now.ToString("MMdd")

        anuinFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")
        anuinFechaCaptura.Value = DateTime.Now.ToString("MMdd")
        anuindetFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")

        pagautinFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")
        pagautinFechaCaptura.Value = DateTime.Now.ToString("MMdd")
        pagautindetFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")

        anuautinFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")
        anuautinFechaCaptura.Value = DateTime.Now.ToString("MMdd")
        anuautindetFechaHoraTransaccion.Value = DateTime.Now.ToString("MMddHHmmss")
    End Sub

    Private Sub OcultarPaneles()
        pnlConsultar.Visible = False
        pnlPagar.Visible = False
        pnlAnular.Visible = False
        pnlExtornarPago.Visible = False
        pnlExtornarAnulacion.Visible = False
    End Sub

    Protected Sub ddlTipoOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTipoOperacion.SelectedIndexChanged
        OcultarPaneles()
        Select Case ddlTipoOperacion.SelectedValue
            Case "355000,0200"
                pnlConsultar.Visible = True
            Case "945000,0200"
                pnlPagar.Visible = True
            Case "965000,0200"
                pnlAnular.Visible = True
            Case "945000,0400"
                pnlExtornarPago.Visible = True
            Case "965000,0400"
                pnlExtornarAnulacion.Visible = True
        End Select
    End Sub

    Protected Sub btnProcesarConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarConsulta.Click
        Using Proxy As New ProxyWSSBK.EjecTransaccionScotiabankService
            consSalida.Value = Proxy.ejecutarTransaccionScotiabank(txtConsultaEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarPago.Click
        Using Proxy As New ProxyWSSBK.EjecTransaccionScotiabankService
            pagoSalida.Value = Proxy.ejecutarTransaccionScotiabank(txtPagoEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarAnulacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarAnulacion.Click
        Using Proxy As New ProxyWSSBK.EjecTransaccionScotiabankService
            AnulacionSalida.Value = Proxy.ejecutarTransaccionScotiabank(txtAnulacionEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarPagoAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarPagoAuto.Click
        Using Proxy As New ProxyWSSBK.EjecTransaccionScotiabankService
            PagoAutoSalida.Value = Proxy.ejecutarTransaccionScotiabank(txtPagoAutoEntrada.Value)
        End Using
    End Sub

    Protected Sub btnProcesarAnulacionAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarAnulacionAuto.Click
        Using Proxy As New ProxyWSSBK.EjecTransaccionScotiabankService
            AnulacionAutoSalida.Value = Proxy.ejecutarTransaccionScotiabank(txtAnulacionAutoEntrada.Value)
        End Using
    End Sub

    Protected Sub btnValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        pnlValidar.Visible = Not (AppSettings("ValidarTestWSSBKCIP") <> "" And txtCodigoSeguridad.Text = AppSettings("ValidarTestWSSBKCIP"))
        pnlOperaciones.Visible = (AppSettings("ValidarTestWSSBKCIP") <> "" And txtCodigoSeguridad.Text = AppSettings("ValidarTestWSSBKCIP"))
    End Sub

End Class