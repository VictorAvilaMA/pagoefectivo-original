﻿Imports System.IO

Public Class ServicioNotificacionTest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Write("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        If Request.HttpMethod() = "POST" Then
            Write("Post :>>>> " & Request.Params(0))
        Else
            Write("Get  :>>>> " & Request.QueryString.ToString())
        End If
        Write("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
    End Sub
    Public Shared Sub Write(m As String)
        Try
            Dim path As String = System.Configuration.ConfigurationManager.AppSettings("ArchivoLogTXT") '"C:\log.txt"

            Dim tw As TextWriter = New StreamWriter(path, True)
            tw.WriteLine(DateTime.Now.ToString() & "   >>>>" & m)
            tw.Close()
        Catch ex2 As Exception
            System.Diagnostics.EventLog.WriteEntry("Application", "Exception: " + ex2.Message)
        End Try
    End Sub

End Class