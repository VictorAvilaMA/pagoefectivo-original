﻿Imports SPE.Utilitario
Imports System.IO
Imports SPE.Api
Imports SPE.Api.Proxys
Imports System.Xml

Public Class WSSolicitarCIPIF
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPIoCOD.Text = strCApi
            'If (strCClave <> "") Then txtCClave.Text = strCClave
            'PagoEfectivo.PrivatePath = "D:\\SPEKeys\\NuestroMercado_PrivateKey.1pz"
            'PagoEfectivo.PublicPathContraparte = "D:\\SPEKeys\\NuestroMercado_PublicKey.1pz"
            'Dim firmado As String = PagoEfectivo.Firmar("Hola Mundo")
            'txtinformacionCIP.Text = PagoEfectivo.ValidarFirma(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnGenerarCIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarCIP.Click
        Try
            txtEstado.Text = String.Empty
            txtMensaje.Text = String.Empty
            txtinformacionCIP.Text = String.Empty
            txtException.Text = String.Empty
            txtCIP.Text = String.Empty

            Dim request As New SPE.Api.Proxys.BEWSSolicitarRequest
            Dim pathPublicKeyContraparte As String = txtKeyPublicPath.Text
            Dim pathPrivateKey As String = txtKeyPrivatePath.Text

            txtXml.Text = txtXml.Text.Trim()
            request.cServ = txtCAPIoCOD.Text
            request.Xml = txtXml.Text

            PagoEfectivo.PrivatePath = pathPrivateKey
            PagoEfectivo.PublicPathContraparte = pathPublicKeyContraparte

            Dim response As SPE.Api.Proxys.BEWSSolicitarResponse = PagoEfectivo.SolicitarPago(request)
            If (response IsNot Nothing) Then
                txtEstado.Text = response.Estado
                txtMensaje.Text = response.Mensaje
                txtinformacionCIP.Text = response.Xml
                Dim xml As New XmlDocument
                xml.InnerXml = response.Xml
                Dim element As XmlElement = xml.SelectSingleNode("ResSolPago")
                Dim elementchild As XmlElement = element.SelectSingleNode("Token")
                Dim UrlRedirect As String = System.Configuration.ConfigurationManager.AppSettings("urlSPE") & "/GenPagoIF.aspx?Token=" & elementchild.InnerText
                'ltlUrl.Text = "<a href='" & UrlRedirect & "'>Ir a Pago Efectivo con Redirect (Token = """ & elementchild.InnerText & """ )</a>"
                ltlUrl.Text = "<iframe src=" & UrlRedirect & "></iframe>"
            Else
                txtinformacionCIP.Text = "No se obtuvo response (servicio del ws esta abajo)..."
            End If
        Catch ex As Exception
            txtException.Text = ex.ToString()
        End Try
    End Sub
    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSGenerarCIP"))
        pPanel.Visible = flag
    End Sub


End Class