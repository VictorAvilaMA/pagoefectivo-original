﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSWUCIP.aspx.vb" Inherits="SPE.WebServiceTest.WSWUCIP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dummy Western Union</title>
    <style type="text/css">
        dl
        {
            margin: 0;
        }
        dl dt
        {
            float: left;
            padding: 5px;
            width: 275px;
        }
        dl dd
        {
            margin: 2px 0;
            padding: 5px 0;
        }
        fieldset.entrada, fieldset.salida
        {
            margin: 0;
            padding: 0;
            float: left;
            width: 49%;
        }
        legend
        {
            font-weight: bold;
        }
        input.trama
        {
            width: 95%;
        }
        
        .tooltip
        {
            background-color: #000;
            border: 1px solid #fff;
            padding: 10px 15px;
            width: 200px;
            display: none;
            color: #fff;
            text-align: left; /* font-size:12px;
	        outline radius for mozilla/firefox only */
            -moz-box-shadow: 0 0 10px #000;
            -webkit-box-shadow: 0 0 10px #000;
        }
        .variable
        {
            border-color: Black;
            border-width: 2px;
        }
    </style>
    <script type="text/javascript" language="javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.tools.min.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("input[title]").tooltip({
                position: "center right",
                offset: [-2, 10],
                effect: "fade",
                opacity: 0.7
            });
            DarFormatoInicial();
            DarFormato();
            $('.botonentrada').click(function () {
                DarFormato();
                var ok = true;
                /*$(this).parent().find('input:text').not($('.trama')).each(function () {
                if ($(this).val().length != $(this).attr('maxlength')) {
                $(this).css('border-color', 'Red');
                ok = false;
                }
                });
                if (!ok)
                alert('!Verificar longitud!')
                else
                Ensamblar('fieldset.entrada');
                */
                return ok;
            });
        });
        function DarFormatoInicial() {
            $('fieldset.salida').find('input:text').not($('.trama')).each(function () {
                if ($(this).attr('format') != undefined)
                    $(this).after(' <i>' + $(this).attr('format') + '</i>');
                if ($(this).attr('maxlength') != undefined)
                    $(this).after('<span>(' + $(this).attr('maxlength') + ')</span>');
            });
        }
        function DarFormato() {
            $('fieldset.entrada').find('input:text').not($('.trama')).each(function () {
                $(this).parent().find('i,span').remove();
                if ($(this).attr('format') != undefined)
                    $(this).after(' <i>' + $(this).attr('format') + '</i>');
                if ($(this).attr('maxlength') != undefined)
                    $(this).after('<span>(' + $(this).attr('maxlength') + ')</span>');
                if ($(this).attr('readonly') == undefined)
                    $(this).css('border-color', 'Blue');
            });
            /*$('fieldset.salida').find('input:text').not($('.trama')).each(function () {
            if ($(this).val() != '' && $(this).attr('maxlength') != $(this).val().length)
            $(this).css('border-color', 'Red');
            });*/
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlValidar" runat="server">
        <asp:TextBox ID="txtCodigoSeguridad" runat="server" TextMode="Password" Width="400"></asp:TextBox>
        <asp:Button ID="btnValidar" runat="server" Text="Validar" />
    </asp:Panel>
    <asp:Panel ID="pnlOperaciones" runat="server" Visible="false">
        <div>
            <div>
                <center>
                    <asp:Label ID="lblTipoOperacion" runat="server" Text="Tipo de operación:" Font-Bold="true"
                        AssociatedControlID="ddlTipoOperacion"></asp:Label>
                    <asp:DropDownList ID="ddlTipoOperacion" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="Consulta (Consulta)" Value="C"></asp:ListItem>
                        <asp:ListItem Text="Pago (Directa)" Value="D"></asp:ListItem>
                        <asp:ListItem Text="Anulación (Reversa)" Value="R"></asp:ListItem>
                    </asp:DropDownList>
                </center>
                <br />
                <fieldset class="entrada">
                    <legend>Consulta de CIPs</legend>
                    <fieldset>
                        <legend>Header Request</legend>
                        <dl>
                            <dt>Algoritmo:</dt>
                            <dd>
                                <input type="text" id="txtAlgoritmoReq" runat="server" tabindex="1" value="AL001Dummy"
                                    maxlength="100" readonly="readonly" /></dd>
                            <dt>Cajero:</dt>
                            <dd>
                                <input type="text" id="txtCajeroReq" runat="server" tabindex="2" value="CA001Dummy"
                                    maxlength="100" readonly="readonly" /></dd>
                            <dt>Codigo Trx:</dt>
                            <dd>
                                <input type="text" id="txtCodTrx" runat="server" tabindex="3" value="06650" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Fecha Hora(dd/MM/yyyy hh:mm):</dt>
                            <dd>
                                <input type="text" id="txtFechaHoraReq" runat="server" tabindex="4" value="" readonly="readonly"
                                    class="variable" /></dd>
                            <dt>Id Mensaje:</dt>
                            <dd>
                                <input type="text" id="txtIdMensajeReq" runat="server" tabindex="5" value="D" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Marca:</dt>
                            <dd>
                                <input type="text" id="txtMarcaReq" runat="server" tabindex="6" maxlength="100" value="MRC001Dummy"
                                    title="Monto" readonly="readonly" /></dd>
                            <dt>Numero de Secuencia</dt>
                            <dd>
                                <input type="text" id="txtNroSecuenciaReq" runat="server" tabindex="7" maxlength="100"
                                    class="variable" value="" readonly="readonly" /></dd>
                            <dt>Plataforma:</dt>
                            <dd>
                                <input type="text" id="txtPlataforma" runat="server" tabindex="8" maxlength="100"
                                    readonly="readonly" value="PL001Dummy" /></dd>
                            <dt>Puesto:</dt>
                            <dd>
                                <input type="text" id="txtPuesto" runat="server" tabindex="9" maxlength="100" readonly="readonly"
                                    value="PU001Dummy" /></dd>
                            <dt>Supervisor:</dt>
                            <dd>
                                <input type="text" id="txtSupervisor" runat="server" tabindex="10" maxlength="100"
                                    readonly="readonly" value="SP001Dummy" /></dd>
                            <dt>Terminal:</dt>
                            <dd>
                                <input type="text" id="txtTerminalReq" runat="server" tabindex="11" maxlength="100"
                                    readonly="readonly" value="TER001Dummy" /></dd>
                            <dt>Version:</dt>
                            <dd>
                                <input type="text" id="txtVersionReq" runat="server" tabindex="12" maxlength="100"
                                    readonly="readonly" value="V001Dummy" /></dd>
                            <dt>Version Autorizador:</dt>
                            <dd>
                                <input type="text" id="txtVersionAturizador" runat="server" tabindex="13" maxlength="100"
                                    readonly="readonly" value="VA001Dummy" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Panel ID="pnlConsultarReq" runat="server">
                        <dl>
                            <dt>Utility:</dt>
                            <dd>
                                <input type="text" id="txtUtilityCons" runat="server" tabindex="19" maxlength="100"
                                    readonly="readonly" value="UT001Dummy" /></dd>
                            <dt>Codigo_Cliente(CIP):</dt>
                            <dd>
                                <input type="text" id="txtCodigoClienteCons" runat="server" tabindex="21" maxlength="14"
                                    title="CIP" /></dd>
                            <dt>Cod_Barra:</dt>
                            <dd>
                                <input type="text" id="txtCodBarraCons" runat="server" tabindex="20" maxlength="14"
                                    readonly="readonly" /></dd>
                        </dl>
                        <asp:Button ID="btnProcesarConsulta" runat="server" Text="Consultar" CssClass="botonentrada" />
                    </asp:Panel>
                    <asp:Panel ID="pnlPagarReq" runat="server" Visible="false">
                        <fieldset>
                            <legend>Check Request</legend>
                            <dl>
                                <dt>Account ID:</dt>
                                <dd>
                                    <input type="text" id="txtAccountID" runat="server" tabindex="1" value="ACCID001Dummy"
                                        maxlength="100" readonly="readonly" /></dd>
                                <dt>Amount:</dt>
                                <dd>
                                    <input type="text" id="txtAmount" runat="server" tabindex="2" value="25010" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Bank Branch:</dt>
                                <dd>
                                    <input type="text" id="txtBankBranch" runat="server" tabindex="3" value="BB001Dummy"
                                        maxlength="100" readonly="readonly" /></dd>
                                <dt>Bank ID:</dt>
                                <dd>
                                    <input type="text" id="txtBankID" runat="server" tabindex="4" value="BKI001Dummy"
                                        maxlength="100" readonly="readonly" /></dd>
                                <dt>Bank Square:</dt>
                                <dd>
                                    <input type="text" id="txtBankSquare" runat="server" tabindex="5" value="BKS001Dummy"
                                        maxlength="100" readonly="readonly" /></dd>
                                <dt>Check Number:</dt>
                                <dd>
                                    <input type="text" id="txtCheckNumber" runat="server" tabindex="6" maxlength="100"
                                        value="CKN001Dummy" title="Monto" readonly="readonly" /></dd>
                                <dt>Expiration</dt>
                                <dd>
                                    <input type="text" id="txtExpiration" runat="server" tabindex="7" maxlength="100"
                                        value="01/01/2020" readonly="readonly" /></dd>
                            </dl>
                        </fieldset>
                        <dl>
                            <dt>Utility:</dt>
                            <dd>
                                <input type="text" id="txtUtilityCanc" runat="server" tabindex="19" maxlength="100"
                                    readonly="readonly" value="UT001Dummy" /></dd>
                            <dt>Bar Code (incluye el CIP):</dt>
                            <dd>
                                <input type="text" id="txtBarCodeReq" runat="server" tabindex="20" maxlength="60"
                                    title="Codigo de Barras devuelto en la operacion de Consulta" value="" /></dd>
                            <dt>Medio de Pago:</dt>
                            <dd>
                                <input type="text" id="txtMedioPago" runat="server" tabindex="21" maxlength="1" value="0"
                                    title="Medio de Pago: Efectivo=0, Cheque=1" /></dd>
                            <dt>Credit Card:</dt>
                            <dd>
                                <input type="text" id="txtCreditCard" runat="server" tabindex="22" maxlength="100"
                                    value="CRC001Dummy" title="Texto reservado WU" /></dd>
                            <dt>Amount:</dt>
                            <dd>
                                <input type="text" id="txtAmountCanc" runat="server" tabindex="23" maxlength="10"
                                    value="" title="Importe formato EEEEEEDD , Ejm: 10.51 = 1051" /></dd>
                        </dl>
                        <asp:Button ID="btnProcesarPago" runat="server" Text="Pagar" CssClass="botonentrada" />
                    </asp:Panel>
                    <asp:Panel ID="pnlAnularReq" runat="server" Visible="false">
                        <dl>
                            <dt>Terminal Original:</dt>
                            <dd>
                                <input type="text" id="txtTerminalOriginal" runat="server" tabindex="19" maxlength="50"
                                    title="Codigo del Terminal donde se realizo el pago" value="TROR001Dummy" /></dd>
                            <dt>Cajero Original:</dt>
                            <dd>
                                <input type="text" id="txtCajeroOriginal" runat="server" tabindex="20" maxlength="50"
                                    value="068880" title="Codigo del Cajero que realizo el pago" /></dd>
                            <dt>Fecha Hora Original(dd/MM/yyyy hh:mm):</dt>
                            <dd>
                                <input type="text" id="txtFechaHoraOriginal" runat="server" tabindex="21" value=""
                                    title="Fecha y Hora en la que se realizo el pago formato(dd/MM/yyyy hh:ss)" /></dd>
                            <dt>Nro. de Secuencia Original:</dt>
                            <dd>
                                <input type="text" id="txtNroSecuenciaOriginal" runat="server" tabindex="22" maxlength="100"
                                    value="" title="Numero de Secuencia del Pago" /></dd>
                            <dt>Tipo de Reversa:</dt>
                            <dd>
                                <input type="text" id="txtTipoReversa" runat="server" tabindex="23" maxlength="1"
                                    title="R=Reversa Manual,A=Reversa Automatica" value="R" /></dd>
                            <dt>Utility:</dt>
                            <dd>
                                <input type="text" id="txtUtilityAnul" runat="server" tabindex="24" maxlength="100"
                                    value="77520175" title="Codigo de la Entidad asignado por WU" /></dd>
                            <dt>Amount:</dt>
                            <dd>
                                <input type="text" id="txtAmountAnul" runat="server" tabindex="25" maxlength="10"
                                    value="" title="importen en formato EEEEEEEEDD" /></dd>
                        </dl>
                        <asp:Button ID="btnProcesarAnulacion" runat="server" Text="Anular" CssClass="botonentrada" />
                    </asp:Panel>
                </fieldset>
                <fieldset class="salida">
                    <legend>Respuesta a Consulta</legend>
                    <fieldset>
                        <legend>Header Response</legend>
                        <dl>
                            <dt>Algoritmo:</dt>
                            <dd>
                                <input type="text" id="txtAlgoritmoRes" runat="server" tabindex="1" value="" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Cajero:</dt>
                            <dd>
                                <input type="text" id="txtcajeroRes" runat="server" tabindex="2" value="" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Codigo Error:</dt>
                            <dd>
                                <input type="text" id="txtCodError" runat="server" tabindex="3" value="" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Codigo Severidad:</dt>
                            <dd>
                                <input type="text" id="txtCodSeveridad" runat="server" tabindex="3" value="" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Fecha Hora(dd/MM/yyyy hh:mm):</dt>
                            <dd>
                                <input type="text" id="txtFechaHoraRes" runat="server" tabindex="4" value="" readonly="readonly" /></dd>
                            <dt>Id Mensaje:</dt>
                            <dd>
                                <input type="text" id="txtIdMensajeRes" runat="server" tabindex="5" value="" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Marca:</dt>
                            <dd>
                                <input type="text" id="txtMarcaRes" runat="server" tabindex="6" maxlength="100" value=""
                                    readonly="readonly" /></dd>
                            <dt>Numero de Secuencia</dt>
                            <dd>
                                <input type="text" id="txtNroSecuenciaRes" runat="server" tabindex="7" maxlength="100"
                                    value="" readonly="readonly" /></dd>
                            <dt>Terminal:</dt>
                            <dd>
                                <input type="text" id="txtTerminalRes" runat="server" tabindex="8" maxlength="100"
                                    readonly="readonly" value="" /></dd>
                            <dt>Version:</dt>
                            <dd>
                                <input type="text" id="txtVersionRes" runat="server" tabindex="9" maxlength="100"
                                    readonly="readonly" value="" /></dd>
                        </dl>
                    </fieldset>
                    <asp:Panel ID="pnlConsultarRes" runat="server">
                        <dl>
                            <dt>Cob. Cliente Nomb:</dt>
                            <dd>
                                <input type="text" id="txtCobClienteNomb" runat="server" tabindex="19" maxlength="100"
                                    readonly="readonly" /></dd>
                            <dt>Count:</dt>
                            <dd>
                                <input type="text" id="txtCount" runat="server" tabindex="20" maxlength="100" readonly="readonly" /></dd>
                            <dt>Selección con Prioridad:</dt>
                            <dd>
                                <input type="text" id="txtSeleccionConPrioridad" runat="server" tabindex="21" maxlength="100"
                                    readonly="readonly" /></dd>
                        </dl>
                        <fieldset>
                            <legend>fields(0) (primer detalle)</legend>
                            <dl>
                                <dt>Cob. Codigo de Barra:</dt>
                                <dd>
                                    <input type="text" id="txtCobCodBarra" runat="server" tabindex="19" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Cob. Prior gpo:</dt>
                                <dd>
                                    <input type="text" id="txtCobPriorGpo" runat="server" tabindex="20" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Cob. Prior nro:</dt>
                                <dd>
                                    <input type="text" id="txtCobPriorNro" runat="server" tabindex="21" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Cob. Texto Fe:</dt>
                                <dd>
                                    <input type="text" id="txtCobTextoFe" runat="server" tabindex="22" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Cob. Estado:</dt>
                                <dd>
                                    <input type="text" id="txtCobEstado" runat="server" tabindex="23" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Cob. Cobro tipo:</dt>
                                <dd>
                                    <input type="text" id="txtCobCobroTipo" runat="server" tabindex="24" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Cob. Comp imp:</dt>
                                <dd>
                                    <input type="text" id="txtCobCompImp" runat="server" tabindex="25" maxlength="100"
                                        readonly="readonly" /></dd>
                                <dt>Numero de Orden:</dt>
                                <dd>
                                    <input type="text" id="txtNumeroDeOrden" runat="server" tabindex="26" maxlength="100"
                                        readonly="readonly" /></dd>
                            </dl>
                        </fieldset>
                    </asp:Panel>
                    <asp:Panel ID="pnlPagarRes" runat="server" Visible="false">
                        <dl>
                            <dt>Msg:</dt>
                            <dd>
                                <input type="text" id="txtMsg" runat="server" tabindex="24" maxlength="120" readonly="readonly" /></dd>
                        </dl>
                    </asp:Panel>
                    <asp:Panel ID="pnlAnularRes" runat="server" Visible="false">
                        <dl>
                            <dt>Estado: (no se usa)</dt>
                            <dd>
                                <input type="text" id="txtEstado" runat="server" tabindex="24" maxlength="100" readonly="readonly" /></dd>
                        </dl>
                        <dl>
                            <dt>Operador: (no se usa)</dt>
                            <dd>
                                <input type="text" id="txtOperador" runat="server" tabindex="24" maxlength="100"
                                    readonly="readonly" /></dd>
                        </dl>
                        <dl>
                            <dt>Ticket: (no se usa)</dt>
                            <dd>
                                <input type="text" id="txtTicket" runat="server" tabindex="24" maxlength="100" readonly="readonly" /></dd>
                        </dl>
                    </asp:Panel>
                </fieldset>
            </div>
        </div>
        <center>
            <asp:TextBox ID="txtException" runat="server" TextMode="MultiLine" Visible="false" ></asp:TextBox>
        </center>
    </asp:Panel>
    </form>
</body>
</html>
