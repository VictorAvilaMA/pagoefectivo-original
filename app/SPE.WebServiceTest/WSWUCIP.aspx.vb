﻿
Imports System.Configuration.ConfigurationManager
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Net
Imports ProxyWSWU

Public Class WSWUCIP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            EstablecerValoresPredeterminados()
        End If
    End Sub

    Private Sub EstablecerValoresPredeterminados()
        txtFechaHoraReq.Value = DateTime.Now.ToString("dd/MM/yyyy hh:mm")
        Dim timeSpan As TimeSpan = New TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second)
        txtNroSecuenciaReq.Value = timeSpan.TotalSeconds
    End Sub

    Private Sub OcultarPaneles()
        pnlConsultarReq.Visible = False
        pnlConsultarRes.Visible = False
        pnlPagarReq.Visible = False
        pnlPagarRes.Visible = False
        pnlAnularReq.Visible = False
        pnlAnularRes.Visible = False
    End Sub
    Private Sub LimpiarHeaderResponse()
        txtAlgoritmoRes.Value = String.Empty
        txtcajeroRes.Value = String.Empty
        txtCodError.Value = String.Empty
        txtCodSeveridad.Value = String.Empty
        txtFechaHoraRes.Value = String.Empty
        txtIdMensajeRes.Value = String.Empty
        txtMarcaRes.Value = String.Empty
        txtNroSecuenciaRes.Value = String.Empty
        txtTerminalRes.Value = String.Empty
        txtVersionRes.Value = String.Empty
    End Sub
    Protected Sub ddlTipoOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTipoOperacion.SelectedIndexChanged
        OcultarPaneles()
        LimpiarHeaderResponse()
        Select Case ddlTipoOperacion.SelectedValue
            Case "C"
                pnlConsultarReq.Visible = True
                pnlConsultarRes.Visible = True
                txtCodTrx.Value = "06650"
                txtIdMensajeReq.Value = "D"
            Case "D"
                pnlPagarReq.Visible = True
                pnlPagarRes.Visible = True
                txtCodTrx.Value = "068880"
                txtIdMensajeReq.Value = "D"
                txtBarCodeReq.Value = IIf(String.IsNullOrWhiteSpace(txtCobCodBarra.Value), String.Empty, txtCobCodBarra.Value)
                txtAmountCanc.Value = IIf(String.IsNullOrWhiteSpace(txtCobCompImp.Value), String.Empty, txtCobCompImp.Value)
            Case "R"
                pnlAnularReq.Visible = True
                pnlAnularRes.Visible = True
                txtCodTrx.Value = "068880"
                txtIdMensajeReq.Value = "R"
                txtTerminalOriginal.Value = IIf(String.IsNullOrWhiteSpace(txtTerminalReq.Value), txtTerminalOriginal.Value, txtTerminalReq.Value)
                txtCajeroOriginal.Value = IIf(String.IsNullOrWhiteSpace(txtCajeroReq.Value), txtCajeroOriginal.Value, txtCajeroReq.Value)
                txtFechaHoraOriginal.Value = IIf(String.IsNullOrWhiteSpace(txtFechaHoraReq.Value), txtFechaHoraOriginal.Value, txtFechaHoraReq.Value)
                txtNroSecuenciaOriginal.Value = IIf(String.IsNullOrWhiteSpace(txtNroSecuenciaReq.Value), txtNroSecuenciaOriginal.Value, txtNroSecuenciaReq.Value)
                txtAmountAnul.Value = IIf(String.IsNullOrWhiteSpace(txtAmountCanc.Value), txtAmountAnul.Value, txtAmountCanc.Value)
        End Select
        EstablecerValoresPredeterminados()
    End Sub
    Protected Sub btnProcesarPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarPago.Click
        Try
            Using Proxy As New ProxyWSWU.Service
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf OnValidationCallback)

                Dim check As New Check
                Dim response As DirectaResp = Proxy.directa(obtenerHeaderReq(), check, txtUtilityCanc.Value, txtBarCodeReq.Value, txtMedioPago.Value.ToString(), txtCreditCard.Value, txtAmountCanc.Value)
                asignarHeaderRes(response.header)
                txtMsg.Value = response.msg
            End Using
        Catch ex As Exception
            txtException.Visible = True
            txtException.Text = ex.ToString()
        End Try
    End Sub


    Protected Sub btnProcesarAnulacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarAnulacion.Click
        Try
            Using Proxy As New ProxyWSWU.Service
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf OnValidationCallback)
                Dim response As ReversaResp = Proxy.reversa(obtenerHeaderReq(), txtTerminalOriginal.Value, txtCajeroOriginal.Value, txtFechaHoraOriginal.Value, _
                                        txtNroSecuenciaOriginal.Value, txtTipoReversa.Value, txtUtilityAnul.Value, txtAmountAnul.Value)
                asignarHeaderRes(response.header)
                txtEstado.Value = response.estado
                txtOperador.Value = response.operador
                txtTicket.Value = response.ticket
            End Using
        Catch ex As Exception
            txtException.Visible = True
            txtException.Text = ex.ToString()
        End Try
    End Sub
    Protected Function obtenerHeaderReq() As HeaderReq
        Dim header As New HeaderReq
        header.algoritmo = txtAlgoritmoReq.Value
        header.cajero = txtCajeroReq.Value
        header.codTrx = txtCodTrx.Value
        header.fechaHora = IIf(txtFechaHoraReq.Value = "", DateTime.MinValue, Convert.ToDateTime(txtFechaHoraReq.Value))
        header.fechaHora = DateTime.Now
        header.idMensaje = txtIdMensajeReq.Value
        header.marca = txtMarcaReq.Value
        header.nroSecuencia = txtNroSecuenciaReq.Value
        header.plataforma = txtPlataforma.Value
        header.puesto = txtPuesto.Value
        header.supervisor = txtSupervisor.Value
        header.terminal = txtTerminalReq.Value
        header.version = txtVersionReq.Value
        header.versionAutorizador = txtVersionAturizador.Value
        Return header
    End Function
    Protected Sub asignarHeaderRes(ByVal header As HeaderResp)
        txtAlgoritmoRes.Value = header.algoritmo.ToString()
        txtcajeroRes.Value = header.cajero.ToString()
        txtCodError.Value = header.codError.ToString()
        txtCodSeveridad.Value = header.codSeveridad.ToString()
        txtFechaHoraRes.Value = IIf(header.fechaHora.HasValue, header.fechaHora.Value.ToString("dd/MM/yyyy hh:mm"), String.Empty)
        txtIdMensajeRes.Value = header.idMensaje.ToString()
        txtMarcaRes.Value = header.marca.ToString()
        txtNroSecuenciaRes.Value = header.nroSecuencia.ToString()
        txtTerminalRes.Value = header.terminal.ToString()
        txtVersionRes.Value = header.version.ToString()
    End Sub
    Protected Sub btnValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidar.Click
        pnlValidar.Visible = Not (AppSettings("ValidarTestWSWUCIP") <> "" And txtCodigoSeguridad.Text = AppSettings("ValidarTestWSWUCIP"))
        pnlOperaciones.Visible = (AppSettings("ValidarTestWSWUCIP") <> "" And txtCodigoSeguridad.Text = AppSettings("ValidarTestWSWUCIP"))
    End Sub


    Function OnValidationCallback(ByVal sender As Object, ByVal cert As X509Certificate, ByVal chain As X509Chain, ByVal errors As SslPolicyErrors) As Boolean
        Return True
    End Function
    Protected Sub btnProcesarConsulta_Click(sender As Object, e As EventArgs) Handles btnProcesarConsulta.Click
        Try
            Using Proxy As New ProxyWSWU.Service
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf OnValidationCallback)

                Dim response As ConsultaResp = Proxy.consulta(obtenerHeaderReq(), txtUtilityCons.Value, txtCodBarraCons.Value, txtCodigoClienteCons.Value)
                asignarHeaderRes(response.header)
                txtCobClienteNomb.Value = response.cob_cliente_nomb
                txtCount.Value = IIf(response.count.HasValue, response.count.ToString(), String.Empty)
                txtSeleccionConPrioridad.Value = IIf(response.seleccion_con_prioridad.HasValue, response.seleccion_con_prioridad.ToString(), String.Empty)
                If response.fields.Length > 0 Then
                    txtCobCobroTipo.Value = response.fields(0).cob_cobro_tipo.ToString()
                    txtCobCodBarra.Value = response.fields(0).cob_cod_barra.ToString()
                    txtCobCompImp.Value = IIf(response.fields(0).cob_comp_imp.HasValue, response.fields(0).cob_comp_imp.ToString(), String.Empty)
                    txtCobEstado.Value = response.fields(0).cob_estado.ToString()
                    txtCobPriorGpo.Value = response.fields(0).cob_prior_gpo.ToString()
                    txtCobPriorNro.Value = response.fields(0).cob_prior_nro.ToString()
                    txtCobTextoFe.Value = response.fields(0).cob_texto_fe.ToString()
                    txtNumeroDeOrden.Value = IIf(response.fields(0).numero_de_orden.HasValue, response.fields(0).numero_de_orden.ToString(), String.Empty)
                Else
                    txtCobCobroTipo.Value = String.Empty
                    txtCobCodBarra.Value = String.Empty
                    txtCobCompImp.Value = String.Empty
                    txtCobEstado.Value = String.Empty
                    txtCobPriorGpo.Value = String.Empty
                    txtCobPriorNro.Value = String.Empty
                    txtCobTextoFe.Value = String.Empty
                    txtNumeroDeOrden.Value = String.Empty
                End If
            End Using
        Catch ex As Exception
            txtException.Visible = True
            txtException.Text = ex.ToString()
        End Try
    End Sub
End Class