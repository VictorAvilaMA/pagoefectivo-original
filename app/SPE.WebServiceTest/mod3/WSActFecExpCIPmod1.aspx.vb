﻿Imports SPE.Utilitario
Imports System.Configuration.ConfigurationManager
Imports SPE.Api

Public Class WSActFecExpCIPmod1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
        End If
    End Sub

    Protected Sub btnActualizarCIP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizarCIP.Click

        Dim clsEncripta As New ClsLibreriax()
        Dim request As New SPE.Api.Proxys.BEWSActualizaCIPRequestMod1
        Dim rsp As New SPE.Api.Proxys.BEWSActualizaCIPResponseMod1

        request.CodServ = txtCAPI.Text
        request.CIP = txtCIP.Text
        request.FechaExpira = txtFechaExpiracion.Text
        PagoEfectivo.PublicPathContraparte = txtKeyPublicPath.Text
        PagoEfectivo.PrivatePath = txtKeyPrivatePath.Text

        Try
            rsp = PagoEfectivo.ActualizarCIPMod1(request)
            If (rsp IsNot Nothing) Then
                txtEstado.Text = rsp.Estado
                txtMensaje.Text = rsp.Mensaje
            End If
        Catch ex As Exception
            txtMensaje.Text = ex.Message
            Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ActualizarFechaExpiracion"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub

End Class
