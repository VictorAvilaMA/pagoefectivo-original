﻿Imports SPE.Utilitario
Imports System.Configuration.ConfigurationManager
Imports SPE.Api

Public Class WSConsultarCIPMod1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
        End If
    End Sub

    Protected Sub btnEjecutar_Click(sender As Object, e As EventArgs) Handles btnEjecutar.Click

        Dim clsEncripta As New ClsLibreriax()
        Dim request As New SPE.Api.Proxys.BEWSConsultarCIPRequestMod1
        Dim response As New SPE.Api.Proxys.BEWSConsultarCIPResponseMod1

        request.CodServ = txtCAPI.Text
        request.CIPS = txtCIPs.Text.Trim.Split(",")(0).ToString()
        PagoEfectivo.PublicPathContraparte = txtKeyPublicPath.Text
        PagoEfectivo.PrivatePath = txtKeyPrivatePath.Text
        response = PagoEfectivo.ConsultarCIPsMod1(request)


        If response IsNot Nothing Then
            If response.XML IsNot Nothing Then
                'Dim CIPs As New StringBuilder
                'CIPs.Append("<ArrayOfBEWSConsCIP>" + Environment.NewLine)
                'For Each CIP As SPE.Api.Proxys.BEWSConsCIPMod1 In response.CIPs
                '    CIPs.Append("    <BEWSConsCIP>" + Environment.NewLine)
                '    For Each prop As System.Reflection.PropertyInfo In GetType(SPE.Api.Proxys.BEWSConsCIPMod1).GetProperties()
                '        If prop.Name <> "Detalle" Then
                '            CIPs.AppendFormat("        <{0}>{1}</{0}>" + Environment.NewLine, prop.Name, prop.GetValue(CIP, Nothing))
                '        End If
                '    Next
                '    CIPs.Append("        <ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                '    For Each CIPDetalle As SPE.Api.Proxys.BEWSConsCIPDetalleMod1 In CIP.Detalle
                '        CIPs.Append("            <BEWSConsCIPDetalle>" + Environment.NewLine)
                '        For Each prop2 As System.Reflection.PropertyInfo In GetType(SPE.Api.Proxys.BEWSConsCIPDetalleMod1).GetProperties()
                '            CIPs.AppendFormat("                <{0}>{1}<{0}>" + Environment.NewLine, prop2.Name, prop2.GetValue(CIPDetalle, Nothing))
                '        Next
                '        CIPs.Append("            </BEWSConsCIPDetalle>" + Environment.NewLine)
                '    Next
                '    CIPs.Append("        </ArrayOfBEWSConsCIPDetalle>" + Environment.NewLine)
                '    CIPs.Append("    </BEWSConsCIP>" + Environment.NewLine)
                'Next
                'CIPs.Append("</ArrayOfBEWSConsCIP>" + Environment.NewLine)
                txtResultado.Text = response.XML
            Else
                txtResultado.Text = ""
            End If
        Else
            txtResultado.Text = ""
        End If
        txtEstado.Text = response.Estado
        txtMensaje.Text = response.Mensaje
    End Sub

    Protected Sub btnValidacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidacion.Click
        pnlValidacion.Visible = (AppSettings("ValidarTestWSConsultarCIP") <> "" And txtValidacion.Text = AppSettings("ValidarTestWSConsultarCIP"))
    End Sub

End Class