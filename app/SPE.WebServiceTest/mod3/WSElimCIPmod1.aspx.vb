﻿Imports SPE.Utilitario
Imports System.Configuration.ConfigurationManager
Imports SPE.Api

Public Class WSElimCIPmod1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCApi As String = System.Configuration.ConfigurationManager.AppSettings("cAPIGenerarCIPDefault")
            Dim strCClave As String = System.Configuration.ConfigurationManager.AppSettings("cClaveGenerarCIPDefault")
            If (strCApi <> "") Then txtCAPI.Text = strCApi
        End If
    End Sub

    Protected Sub btnEliminarCIP_Click(sender As Object, e As EventArgs) Handles btnEliminarCIP.Click

        Dim clsEncripta As New ClsLibreriax()
        Dim request As New SPE.Api.Proxys.BEWSElimCIPRequestMod1
        Dim response As New SPE.Api.Proxys.BEWSElimCIPResponseMod1

        request.CodServ = txtCAPI.Text
        request.CIP = txtCIP.Text
        PagoEfectivo.PublicPathContraparte = txtKeyPublicPath.Text
        PagoEfectivo.PrivatePath = txtKeyPrivatePath.Text

        response = PagoEfectivo.EliminarCIPMod1(request)

        If (response IsNot Nothing) Then
            txtEstado.Text = response.Estado
            txtMensaje.Text = response.Mensaje
        End If

    End Sub

    Protected Sub btnPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPanel.Click
        Validar()
    End Sub

    Private Sub Validar()
        Dim flag As Boolean = (txtValidar.Text = System.Configuration.ConfigurationManager.AppSettings("ValidarTestWSElimCIP"))
        txtValidar.Visible = flag
        btnPanel.Visible = flag
        pPanel.Visible = flag
    End Sub

End Class