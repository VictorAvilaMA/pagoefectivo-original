﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WSGenerarCIPMod1.aspx.vb" Inherits="SPE.WebServiceTest.WSGenerarCIPMod1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Prueba Solicitud de Pago desde WS</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:TextBox ID="txtValidar" runat="server" Text="" Width="358px" TextMode="Password"></asp:TextBox>
    <asp:Button ID="btnPanel" runat="server" Text="Validar" />
    <asp:Panel ID="pPanel" runat="server" Visible="false">
        <div>
            <br />
            1) Prueba Generar CIP Modalidad 1 desde WS
            <table>
                <tr>
                    <td>
                        Dir KeyPublicSPE.:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtKeyPublicPath" runat="server" Width="304px">C:\\PagoEfectivo2\\Claves\\SPE_PublicKey.1pz</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Dir KeyPrivateEntidad.:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtKeyPrivatePath" runat="server" Width="304px">C:\\PagoEfectivo2\\Claves\\OF2_PrivateKey.1pz</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CAPI o Cod.Serv.:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCAPIoCOD" runat="server" Width="304px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CClave:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtCClave" runat="server" Width="304px" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Xml:
                    </td>
                    <td style="width: 506px">
                        <asp:TextBox ID="txtXml" TextMode="MultiLine" runat="server" Height="520px" Width="771px"><?xml version="1.0" encoding="utf-8" ?>
                        <SolPago>
                          <IdMoneda>1</IdMoneda>
                          <Total>22.00</Total>
                          <MetodosPago>1,2</MetodosPago>
                          <CodServicio>OF2</CodServicio>
                          <Codtransaccion>0000040</Codtransaccion>
                          <EmailComercio>admin@ofertop2.pe</EmailComercio>
                          <FechaAExpirar>31/12/2016 17:00:00</FechaAExpirar>
                          <UsuarioId>001</UsuarioId>
                          <DataAdicional></DataAdicional>
                          <UsuarioNombre>Victor</UsuarioNombre>
                          <UsuarioApellidos>Avila</UsuarioApellidos>
                          <UsuarioLocalidad>LIMA</UsuarioLocalidad>
                          <UsuarioProvincia>LIMA</UsuarioProvincia>
                          <UsuarioPais>PERU</UsuarioPais>
                          <UsuarioAlias>Victor</UsuarioAlias>
                          <UsuarioTipoDoc>1</UsuarioTipoDoc>
                          <UsuarioNumeroDoc>1</UsuarioNumeroDoc>
                          <UsuarioEmail>victor.avila@metricaandina.com</UsuarioEmail>
                          <ConceptoPago>Pago</ConceptoPago>
                          <Detalles>
                            <Detalle>
                              <Cod_Origen>CT</Cod_Origen>
                              <TipoOrigen>TO</TipoOrigen>
                              <ConceptoPago>Transaccion Comision 1</ConceptoPago>
                              <Importe>10.00</Importe>
                              <Campo1></Campo1>
                              <Campo2></Campo2>
                              <Campo3></Campo3>
                            </Detalle>
                            <Detalle>
                              <Cod_Origen>CT</Cod_Origen>
                              <TipoOrigen>TO</TipoOrigen>
                              <ConceptoPago>Transaccion Comision 2</ConceptoPago>
                              <Importe>12.00</Importe>
                              <Campo1></Campo1>
                              <Campo2></Campo2>
                              <Campo3></Campo3>
                            </Detalle>
                          </Detalles>
                          <ParamsURL>
                            <ParamURL>
                              <Nombre>IDCliente</Nombre>
                              <Valor>15474</Valor>
                            </ParamURL>
                            <ParamURL>
                              <Nombre>FechaHoraRegistro</Nombre>
                              <Valor>19/08/2010</Valor>
                            </ParamURL>
                          </ParamsURL>
                          <ParamsEmail>
                            <ParamEmail>
                              <Nombre>[UsuarioNombre]</Nombre>
                              <Valor>Victor</Valor>
                            </ParamEmail>
                            <ParamEmail>
                              <Nombre>[Moneda]</Nombre>
                              <Valor>S/.</Valor>
                            </ParamEmail>
                          </ParamsEmail>
                        </SolPago>
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td style="width: 506px">
                        <asp:Button ID="btnGenerarCIP" runat="server" Text="Solicitar Pago" />
                    </td>
                </tr>
            </table>
            <br />
            <table style="width: 60%;">
                <tr>
                    <td>
                        Estado:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEstado" runat="server" Style="width: 100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Mensaje:
                    </td>
                    <td>
                        <asp:TextBox ID="txtMensaje" runat="server" TextMode="MultiLine" Height="70px" Style="width: 100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        XML:
                    </td>
                    <td>
                        <asp:TextBox ID="txtinformacionCIP" runat="server" TextMode="MultiLine" Height="520px"
                            Width="100%"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
