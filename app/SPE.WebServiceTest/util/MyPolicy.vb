﻿Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Public Class MyPolicy
    Implements ICertificatePolicy

    Public Function CheckValidationResult(ByVal srvPoint As ServicePoint, _
                  ByVal cert As X509Certificate, ByVal request As WebRequest, _
                  ByVal certificateProblem As Integer) _
              As Boolean Implements ICertificatePolicy.CheckValidationResult
        'Return True to force the certificate to be accepted.
        Return True
    End Function
End Class
