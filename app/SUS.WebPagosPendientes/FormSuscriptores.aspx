<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FormSuscriptores.aspx.vb"
    Inherits="FormSuscriptores" Title="Consultar Facturas Pendientes Suscriptor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <script language="javascript" type="text/javascript">
    function Enviar()
    {
    form1.Button1.click();
    }
    </script>

    <title>Estado de Cuenta - Suscriptores</title>
    <style type="text/css">
</style>
    <link rel="stylesheet" type="text/css" href="Style/default.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div  style="width: 795px" >
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="upGrillas" runat="server">
            <ContentTemplate>
                <div id="cuerpo" align="center" >
                    <div id="ENCABEZADO" style="height: 177px">
                        <table align="left" style="width: 300px; height: 1px;">
                            <tr>
                                <td class="Textonegronormal TituloEstadoCuenta" align="middle" style="width: 100%">
                                    <b>ESTADO DE CUENTA SUSCRIPCION</b></td>
                            </tr>
                            <tr>
                                <td class="Textonegronormal" style="width: 420px">
                                    <table cellspacing="1" cellpadding="1" border="0" class="TablaInfoCliente">
                                        <tbody>
                                            <tr>
                                                <td align="left" width="20%" class="TablaInfoClienteTD">
                                                    Codigo Cliente</td>
                                                <td align="left" width="2%">
                                                    :&nbsp;</td>
                                                <td align="left">
                                                    <asp:Label ID="cod_cliente" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Cliente Pago</td>
                                                <td align="left">
                                                    :&nbsp;</td>
                                                <td align="left">
                                                    <asp:Label ID="cliente" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Direcci�n</td>
                                                <td align="left">
                                                    :&nbsp;</td>
                                                <td align="left">
                                                    <asp:Label ID="dir" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 15px">
                                                    Documento</td>
                                                <td align="left" style="height: 15px">
                                                    :&nbsp;</td>
                                                <td align="left" style="height: 15px">
                                                    <asp:Label ID="numdoc" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <asp:TextBox ID="txtIdComercio" runat="server" Visible="False"></asp:TextBox>
                                    <asp:TextBox ID="TextBox6" runat="server" Style="visibility: hidden"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" Text="Button" Style="visibility: hidden" /></td>
                            </tr>
                        </table>
                        <div id="LOGO_SUS">
                            <div align="right">
                                <img src="img/sus.jpg" width="184" height="79" /><br />
                                <br />
                                <table>
                                    <tr>
                                        <td style="width: 105px">
                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#FF8000" Text="Sub - Total"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="moneda" runat="server" Font-Bold="True" ForeColor="#FF8000" Text="S/."></asp:Label>
                                        </td>
                                        <td style="width: 50px">
                                            <div align="left">
                                                <asp:Label ID="MontoTotal" runat="server" Font-Bold="True" ForeColor="#FF8000">0.00</asp:Label>&nbsp;
                                            </div>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="pagar" runat="server" ImageUrl="img/boton-suscriptores.jpg" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="msj" runat="server" Font-Bold="True" ForeColor="#FF8000" Text="Label"
                        Visible="False"></asp:Label>
                    <asp:Panel ID="pGrillas" runat="server" Height="50px" Width="100%">
                        <fieldset style="background-color: gainsboro; height: 24px; width: 100%;">
                            <center>
                                <b>Detalle Estado de Cuenta</b></center>
                        </fieldset>
                        <table>
                            <tr>
                                <td class="Textonegronormal" style="font-size: 9pt;" align="left">
                                    <br />
                                    <br />
                                    <div align="center">
                                        <div align="left" style="height:30px;">
                                            <asp:RadioButton ID="soles" runat="server" Text="Soles S/." AutoPostBack="True" OnCheckedChanged="soles_CheckedChanged" />
                                           
                                        </div>
                                        <div align="center">
                                            <asp:GridView ID="gvResultadoS" CssClass="grilla" runat="server" AutoGenerateColumns="False"
                                                Enabled="False" AutoPostBack="True">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <div style="margin-left: 12px;">
                                                                <asp:CheckBox ID="ch1" runat="server" CausesValidation="True" CssClass="item_grilla"
                                                                    AutoPostBack="True" OnCheckedChanged="ch1_CheckedChanged" />
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chTodos" AutoPostBack="true" runat="server" CausesValidation="True"
                                                                Text="" Width="40px" OnCheckedChanged="chTodos_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle Width="10px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="10px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Tipo Doc." DataField="TipoDoc">
                                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Nro. Documento">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Serie")  + "-"+DataBinder.Eval(Container.DataItem, "Correlativo") %>'></asp:Label>
                                                            <asp:Label ID="lblSerie" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Serie") %>' Visible ="false" ></asp:Label>
                                                    <asp:Label ID="lblCorrelativo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Correlativo") %>' Visible ="false" ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Fec. Vencimiento" DataField="FechaVencimiento" DataFormatString="{0:dd/MM/yyyy}" >
                                                        <ItemStyle Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Monto">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSimboloS" runat="server" Text="S/."></asp:Label>
                                                            <asp:Label ID="lblMonto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Monto") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Saldo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSimboloS2" runat="server" Text="S/."></asp:Label>
                                                            <asp:Label ID="lblSaldo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Saldo") %>'></asp:Label>
                                                            <asp:Label ID="lblNroInterno" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NroInterno") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" Width="100px" />
                                                    </asp:TemplateField>
                                                   
                                                </Columns>
                                                <HeaderStyle CssClass="cabecera" />
                                                <EmptyDataTemplate>
                                                    No se encontraron registros.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                        <br />
                                        <br />                                        
                                        <div align="left" >
                                            <asp:RadioButton ID="dolares" runat="server" Text="D�lares $" AutoPostBack="True"
                                                OnCheckedChanged="dolares_CheckedChanged" />
                                        </div>
                                        <br />
                                        <asp:GridView ID="gvResultadoD" CssClass="grilla" runat="server" AutoGenerateColumns="False"
                                            Enabled="False">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div style="margin-left: 12px;">
                                                            <asp:CheckBox ID="ch1" runat="server" CausesValidation="True" CssClass="item_grilla"
                                                                AutoPostBack="True" OnCheckedChanged="ch1_CheckedChanged" />
                                                        </div>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chTodos" AutoPostBack="true" runat="server" CausesValidation="True"
                                                            Text="" Width="40px" OnCheckedChanged="chTodos_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="10px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10px" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Tipo Doc." DataField="TipoDoc">
                                                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Nro. Documento">
                                                    <ItemTemplate>
                                                    <asp:Label ID="lblSerie" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Serie") %>' Visible ="false" ></asp:Label>
                                                    <asp:Label ID="lblCorrelativo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Correlativo") %>' Visible ="false" ></asp:Label>
                                                        <asp:Label ID="lblNroDocumento" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Serie") + "-"+ DataBinder.Eval(Container.DataItem, "Correlativo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Fec. Vencimiento" DataField="FechaVencimiento" DataFormatString="{0:dd/MM/yyyy}" >
                                                    <ItemStyle Width="120px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Monto">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSimboloD" runat="server" Text="$/."></asp:Label>
                                                        <asp:Label ID="lblMonto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Monto") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Saldo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSimboloD2" runat="server" Text="$/."></asp:Label>
                                                        <asp:Label ID="lblSaldo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Saldo") %>'></asp:Label>
                                                        <asp:Label ID="lblNroInterno" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NroInterno") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" Width="100px" />
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                            <HeaderStyle CssClass="cabecera" />
                                            <EmptyDataTemplate>
                                                No se encontraron registros.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    </td>
                            </tr>
                        </table>
                        <br />
                        <fieldset style="background-color: gainsboro; height: 24px;">
                            <center>
                                <b>Detalle Estado de Cuenta</b></center>
                        </fieldset>
                        <div align="right">
                            <br />
                            <table>
                                <tr>
                                    <td style="width: 105px; height: 33px;">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#FF8000" Text="Sub - Total"></asp:Label>
                                    </td>
                                    <td style="height: 33px">
                                        <asp:Label ID="moneda2" runat="server" Font-Bold="True" ForeColor="#FF8000" Text="S/."></asp:Label>
                                    </td>
                                    <td style="width: 50px; height: 33px;">
                                        <div align="left">
                                            <asp:Label ID="MontoTotal2" runat="server" Font-Bold="True" ForeColor="#FF8000">0.00</asp:Label>
                                        </div>
                                    </td>
                                    <td style="height: 33px">
                                        <asp:ImageButton ID="pagar2" runat="server" ImageUrl="img/boton-suscriptores.jpg"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="pagar" />
                <asp:PostBackTrigger ControlID="pagar2" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
        
    </form>
</body>
</html>