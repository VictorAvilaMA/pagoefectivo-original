
Partial Class GenImgCIPTest
    Inherits System.Web.UI.Page

    Protected Sub btnVerCodigoBarras_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerCodigoBarras.Click
        Dim str As String = String.Format("cip={0}|capi={1}|cclave={2}", txtCIP.Text.TrimEnd(), txtCAPI.Text.TrimEnd, txtCClave.Text.TrimEnd)
        Dim clsEncripta As New clsLibreriax()
        Dim querystring As String = clsEncripta.Encrypt(str, clsEncripta.fEncriptaKey)
        imgCodigoBarra.Src = txtUrlCodigoBarra.Text + "codigo=" + querystring
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtUrlCodigoBarra.Text = System.Configuration.ConfigurationManager.AppSettings("ServerCodigoBarraUrl")
    End Sub
End Class
