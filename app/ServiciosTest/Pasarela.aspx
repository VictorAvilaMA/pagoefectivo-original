<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Pasarela.aspx.vb" Inherits="Pasarela" ValidateRequest="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PASARELA</title>
    <script src="scripts/jsPagoEfectivo.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                    Datos de Prueba para el Request de Pago Efectivo-PASARELA<br />
        <table>
            <tr>
                <td style="width: 3px">
                    Server Request</td>
                <td style="width: 3px">
                    <asp:TextBox ID="txtServer" runat="server" Width="450px">http://localhost:4202/SPE.Web/PAS/PRSolPago.aspx?</asp:TextBox></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 3px">
                    Server Popup</td>
                <td style="width: 3px">
                    <asp:TextBox ID="txtServerPopup" runat="server" Width="450px">http://localhost:4202/SPE.Web/PAS/PRSolPago.aspx?</asp:TextBox></td>
                <td>
                </td>
            </tr>
        </table>
        <br />
        <table style="width: 500px; height: 61px">
            <tr>
                <td style="width: 107px; height: 30px">
                </td>
                <td style="width: 19px; height: 30px">
                </td>
                <td style="width: 2395px; height: 30px">
                </td>
                <td style="width: 227px; height: 30px">
                </td>
                <td style="width: 227px; height: 30px">
                </td>
                <td style="width: 130px; height: 30px">
                    Utiliza Encriptacion</td>
                <td style="width: 183px; height: 30px">
                    <asp:CheckBox ID="chkUtilizaEncriptacion" runat="server" Text="Utilizar Encriptacion" Checked="True" /></td>
                <td style="width: 183px; height: 30px">
                </td>
            </tr>
            <tr>
                <td style="width: 107px; height: 21px;">
                </td>
                <td style="width: 19px; height: 21px;">
                    Parametro</td>
                <td style="width: 2395px; height: 21px;">
                    Valor.............
                </td>
                <td style="width: 227px; height: 21px;">
                    .........</td>
                <td style="width: 227px; height: 21px;">
                </td>
                <td style="width: 130px; height: 21px;">
                    SubParametros</td>
                <td style="width: 183px; height: 21px;">
                    Valor</td>
                <td style="width: 183px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 107px; height: 20px">
                    1-</td>
                <td style="width: 19px; height: 20px">
                    EMerchantID</td>
                <td style="width: 2395px; height: 20px">
                    <asp:TextBox ID="TextBox1" runat="server" Width="80px">NEO</asp:TextBox></td>
                <td style="width: 227px; height: 20px">
                </td>
                <td style="width: 227px; height: 20px">
                </td>
                <td style="width: 130px; height: 20px">
                </td>
                <td style="width: 183px; height: 20px">
                </td>
                <td style="width: 183px; height: 20px">
                </td>
            </tr>
            
            <tr>
                <td style="width: 107px">
                    2-</td>
                <td style="width: 19px">
                    EDatosEnc</td>
                <td style="width: 2395px">
                    <asp:TextBox ID="txtDatosEnc" runat="server" Width="80px">datosEnc</asp:TextBox></td>
                <td style="width: 227px">
                </td>
                <td style="width: 227px">
                </td>
                <td style="width: 130px">
                </td>
                <td style="width: 183px">
                </td>
                <td style="width: 183px">
                </td>
            </tr>
            <tr>
                <td style="width: 107px; height: 21px">
                </td>
                <td style="width: 19px; height: 21px">
                </td>
                <td style="width: 2395px; height: 21px">
                </td>
                <td style="width: 3262px; height: 21px">
                    1-</td>
                <td style="width: 227px; height: 21px">
                    MerchantID</td>
                <td style="width: 130px; height: 21px">
                    <asp:TextBox ID="txtMerchantID" runat="server">empresa</asp:TextBox></td>
                <td style="width: 183px; height: 21px">
                    <asp:TextBox ID="txtMerchantIDValor" runat="server" Width="350px">KOT</asp:TextBox></td>
                <td style="width: 183px; height: 21px">
                    <asp:CheckBox ID="chkMerchantID" runat="server"  /></td>
            </tr>
            <tr>
                <td style="width: 107px; height: 21px">
                </td>
                <td style="width: 19px; height: 21px">
                </td>
                <td style="width: 2395px; height: 21px">
                </td>
                <td style="width: 3262px; height: 21px">
                    1-</td>
                <td style="width: 227px; height: 21px">
                    CClave</td>
                <td style="width: 130px; height: 21px">
                    <asp:TextBox ID="txtCClave" runat="server">egp_CClave</asp:TextBox></td>
                <td style="width: 183px; height: 21px">
                    <asp:TextBox ID="txtCClaveValor" runat="server" Width="350px">882b3114-fe8a-4b66-a3eb-b853001c7708</asp:TextBox></td>
                <td style="width: 183px; height: 21px">
                    <asp:CheckBox ID="chkCClave" runat="server"  Checked="true"  /></td>
            </tr>
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    2-</td>
                <td style="width: 227px">
                    OrdenId</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtOrderID" runat="server">egp_OrderID</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtOrderIDValor" runat="server" Width="350px">0000018</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkOrderID" runat="server" Checked="True" /></td>
            </tr>
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    2-</td>
                <td style="width: 227px">
                    Tipo Tarjeta</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtTipoTarjeta" runat="server">egp_TipoTarjeta</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtTipoTarjetaValor" runat="server" Width="350px">1</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkTipoTarjeta" runat="server" Checked="True" /></td>
            </tr>
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    3-</td>
                <td style="width: 227px">
                    UrlOk</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUrlOk" runat="server">egp_UrlOk</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUrlOkValor" runat="server" Width="350px">http://localhost/UrlsDummy/UrlOk.htm</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUrlOk" runat="server"/></td>
            </tr>
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    4-</td>
                <td style="width: 227px">
                    UrlError</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUrlError" runat="server">egp_UrlError</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUrlErrorValor" runat="server" Width="350px">http://localhost/UrlsDummy/UrlError.htm</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUrlError" runat="server"/></td>
            </tr>
            <tr>
                <td style="width: 107px; height: 16px">
                </td>
                <td style="width: 19px; height: 16px;">
                </td>
                <td style="width: 2395px; height: 16px">
                </td>
                <td style="width: 3262px; height: 16px">
                    5-</td>
                <td style="width: 227px; height: 16px;">
                    MailCom</td>
                <td style="width: 130px; height: 16px;">
                    <asp:TextBox ID="txtMailCom" runat="server">egp_MailCom</asp:TextBox></td>
                <td style="width: 183px; height: 16px;">
                    <asp:TextBox ID="txtMailComValor" runat="server" Width="350px">mail@compa&#241;ia.com</asp:TextBox></td>
                <td style="width: 183px; height: 16px">
                    <asp:CheckBox ID="chkMailCom" runat="server"/></td>
            </tr>
            <tr>
                <td style="width: 107px; height: 2px;">
                </td>
                <td style="width: 19px; height: 2px;">
                </td>
                <td style="width: 2395px; height: 2px;">
                </td>
                <td style="width: 3262px; height: 2px;">
                    6-</td>
                <td style="width: 227px; height: 2px;">
                    OrdenDesc</td>
                <td style="width: 130px; height: 2px;">
                    <asp:TextBox ID="txtOrdenDesc" runat="server">egp_OrderDescription</asp:TextBox></td>
                <td style="width: 183px; height: 2px;">
                    <asp:TextBox ID="txtOrdenDescValor" runat="server" Width="350px">Televisor LG</asp:TextBox></td>
                <td style="width: 183px; height: 2px;">
                    <asp:CheckBox ID="chkOrdenDesc" runat="server"/></td>
            </tr>
            <tr>
                <td style="width: 107px; height: 31px;">
                </td>
                <td style="width: 19px; height: 31px;">
                </td>
                <td style="width: 2395px; height: 31px;">
                </td>
                <td style="width: 3262px; height: 31px;">
                    7-</td>
                <td style="width: 227px; height: 31px;">
                    Moneda</td>
                <td style="width: 130px; height: 31px;">
                    <asp:TextBox ID="txtMoneda" runat="server">egp_Currency</asp:TextBox></td>
                <td style="width: 183px; height: 31px;">
                    <asp:TextBox ID="txtMonedaValor" runat="server" Width="350px">1</asp:TextBox></td>
                <td style="width: 183px; height: 31px;">
                    <asp:CheckBox ID="chkMoneda" runat="server"/></td>
            </tr>
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    8-</td>
                <td style="width: 227px">
                    Monto</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtMonto" runat="server">importe</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtMontoValor" runat="server" Width="350px">00000899</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkMonto" runat="server"   Checked="true"/></td>
            </tr>
            <tr>
                <td style="width: 107px; height: 26px;">
                </td>
                <td style="width: 19px; height: 26px;">
                </td>
                <td style="width: 2395px; height: 26px;">
                </td>
                <td style="width: 3262px; height: 26px;">
                    8-</td>
                <td style="width: 227px; height: 26px;">
                    UsuarioID</td>
                <td style="width: 130px; height: 26px;">
                    <asp:TextBox ID="txtUsuarioID" runat="server">cliente</asp:TextBox></td>
                <td style="width: 183px; height: 26px;">
                    <asp:TextBox ID="txtUsuarioIDValor" runat="server" Width="350px">59</asp:TextBox></td>
                <td style="width: 183px; height: 26px">
                    <asp:CheckBox ID="chkUsuarioID" runat="server"   Checked="true"/></td>
            </tr> 
            <tr>
                <td style="width: 107px; height: 26px;">
                </td>
                <td style="width: 19px; height: 26px;">
                </td>
                <td style="width: 2395px; height: 26px;">
                </td>
                <td style="width: 3262px; height: 26px;">
                    8-</td>
                <td style="width: 227px; height: 26px;">
                    E-mail</td>
                <td style="width: 130px; height: 26px;">
                    <asp:TextBox ID="txtEmail" runat="server">cliente</asp:TextBox></td>
                <td style="width: 183px; height: 26px;">
                    <asp:TextBox ID="txtEmailValor" runat="server" Width="350px">david.silva@ec.pe</asp:TextBox></td>
                <td style="width: 183px; height: 26px">
                    <asp:CheckBox ID="chkEmail" runat="server"   Checked="true"/></td>
            </tr>            
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    9-</td>
                <td style="width: 227px">
                    DataAdicional</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtDataAdicional" runat="server">egp_DataAdicional</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtDataAdicionalValor" runat="server" Width="350px"></asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkDataAdicional" runat="server" /></td>
            </tr>   
            <tr>
                <td style="width: 107px; height: 33px;">
                </td>
                <td style="width: 19px; height: 33px;">
                </td>
                <td style="width: 2395px; height: 33px;">
                </td>
                <td style="width: 3262px; height: 33px;">
                    10-</td>
                <td style="width: 227px; height: 33px;">
                    UsuarioNombre</td>
                <td style="width: 130px; height: 33px;">
                    <asp:TextBox ID="txtUsuarioNombre" runat="server">egp_UsuarioNombre</asp:TextBox></td>
                <td style="width: 183px; height: 33px;">
                    <asp:TextBox ID="txtUsuarioNombreValor" runat="server" Width="350px">JOSE</asp:TextBox></td>
                <td style="width: 183px; height: 33px;">
                    <asp:CheckBox ID="chkUsuarioNombre" runat="server"/></td>
            </tr>   
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    11-</td>
                <td style="width: 227px">
                    UsuarioApellidos</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUsuarioApellidos" runat="server">egp_UsuarioApellidos</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUsuarioApellidosValor" runat="server" Width="350px">PEREZ</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUsuarioApellidos" runat="server"/></td>
            </tr>   
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    12-</td>
                <td style="width: 227px">
                    UsuarioLocalidad</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUsuarioLocalidad" runat="server">egp_UsuarioLocalidad</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUsuarioLocalidadValor" runat="server" Width="350px">AV. LIMA</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUsuarioLocalidad" runat="server"/></td>
            </tr>               
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    12-</td>
                <td style="width: 227px">
                    UsuarioDomicilio</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUsuarioDomicilio" runat="server">egp_UsuarioDomicilio</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUsuarioDomicilioValor" runat="server" Width="350px">AV. LIMA</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUsuarioDomicilio" runat="server"/></td>
            </tr>   
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    13-</td>
                <td style="width: 227px">
                    UsuarioProvincia</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUsuarioProvincia" runat="server">egp_UsuarioProvincia</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUsuarioProvinciaValor" runat="server" Width="350px">LIMA</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUsuarioProvincia" runat="server"/></td>
            </tr>   
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    14-</td>
                <td style="width: 227px">
                    UsuarioPais</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUsuarioPais" runat="server">egp_UsuarioPais</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUsuarioPaisValor" runat="server" Width="350px">PERU</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:CheckBox ID="chkUsuarioPais" runat="server"/></td>
            </tr>  
            <tr>
                <td style="width: 107px">
                </td>
                <td style="width: 19px">
                </td>
                <td style="width: 2395px">
                </td>
                <td style="width: 3262px">
                    14-</td>
                <td style="width: 227px">
                    UsuarioAlias</td>
                <td style="width: 130px">
                    <asp:TextBox ID="txtUsuarioAlias" runat="server">Apodo</asp:TextBox></td>
                <td style="width: 183px">
                    <asp:TextBox ID="txtUsuarioAliasValor" runat="server" Width="350px">Cesar</asp:TextBox></td>
                <td style="width: 183px">
                <asp:CheckBox ID="chkUsuarioAlias" runat="server"   Checked="true"/>
                </td>
            </tr>  
        </table>
    
    </div>
        <br />
        <table style="width: 766px; margin-left:300px">
            <tr>
                <td style="width: 7px">
                    &nbsp;<asp:RadioButton ID="rbtRequest" runat="server" GroupName="Action" AutoPostBack="True" Text="Redirect" /></td>
                <td style="width: 3px">
                </td>
                <td>
                    <asp:Button ID="btnPagoEfectivo" runat="server" Text="Ir a Pago Efectivo" /><asp:Button ID="btnTest"   runat="server" Text="Ir a Test Integracion" /></td>
            </tr>
            <tr>
                <td style="width: 7px">
                    <asp:RadioButton ID="rbPopup" runat="server" GroupName="Action" AutoPostBack="True" Text="Popup" />
                </td>
                <td style="width: 3px">
                </td>
                <td>
                    <input id="btnAbrirPopup" runat="server" type="button" value="Ir a Pago efectivo Popup" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
