﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="QueEsPE.aspx.vb" Inherits="QueEsPE" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <dl>
            <dt><asp:Label ID="lblCAPI" runat="server" AssociatedControlID="txtCAPI">Clave API:</asp:Label></dt>
            <dd><asp:TextBox ID="txtCAPI" runat="server" Width="300px" Text="1870bb1b-fd48-49dc-ba50-26ff286ccf46"></asp:TextBox></dd>
            <dt><asp:Label ID="lblCClave" runat="server" AssociatedControlID="txtCClave">Clave secreta:</asp:Label></dt>
            <dd><asp:TextBox ID="txtCClave" runat="server" Width="300px" Text="2d3b154c-63e3-4a82-8f7a-0780d6d3c32f"></asp:TextBox></dd>
            <dt><asp:Label ID="lblMoneda" runat="server" AssociatedControlID="txtMoneda">Moneda:</asp:Label></dt>
            <dd><asp:TextBox ID="txtMoneda" runat="server" Text="1"></asp:TextBox></dd>
            <dt><asp:Label ID="lblUrlBase" runat="server" AssociatedControlID="txtUrlBase">Url base:</asp:Label></dt>
            <dd><asp:TextBox ID="txtUrlBase" runat="server" Width="300px" Text="http://localhost:4202/SPE.Web/CNT/QueEsPE.aspx"></asp:TextBox></dd>
        </dl>
        <asp:Button ID="btnGenerar" runat="server" Text="Generar" />
        <fieldset id="fsQueEsPE" runat="server" visible="false">
            <legend>¿Qué es PagoEfectivo?</legend>
            <asp:TextBox ID="txtUrl" runat="server" Width="100%"></asp:TextBox>
            <iframe id="ifmQueEsPE" runat="server" height="500px" width="100%"></iframe>
        </fieldset>
    </form>
</body>
</html>
