﻿
Partial Class QueEsPE
    Inherits System.Web.UI.Page

    Protected Sub btnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        Dim codigo As String = String.Format("capi={0}|cclave={1}|moneda={2}", txtCAPI.Text, txtCClave.Text, txtMoneda.Text)
        Dim objclsLibreria As New clsLibreriax
        Dim query As String = objclsLibreria.Encrypt(codigo, objclsLibreria.fEncriptaKey)
        Dim url As String = txtUrlBase.Text & "?codigo=" & query
        txtUrl.Text = url
        ifmQueEsPE.Attributes("src") = url
        fsQueEsPE.Visible = True
    End Sub

End Class
