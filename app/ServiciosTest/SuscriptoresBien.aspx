<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SuscriptoresBien.aspx.vb" Inherits="SuscriptoresBien"  ValidateRequest="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SuscriptoresBien</title>
    
    <script language="javascript" type="text/javascript">
      function Enviar()
      {
       form1.btnGenerarCIP.click();
      }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
     <tr>
        <td>UrlServer:</td>
        <td style="width: 506px">
            <asp:TextBox ID="txtUrlServer" runat="server" Width="304px">6964dc43-1507-47f0-978c-954d7bb41b95</asp:TextBox>
            </td>
     </tr>
          
     <tr>
        <td>Xml:</td>
        <td style="width: 506px">
            <asp:TextBox ID="TextBox6" runat="server" Width="304px"></asp:TextBox>
            <asp:TextBox ID="txtXML" TextMode="MultiLine"  runat="server" Height="520px" Width="771px"><?xml version="1.0" encoding="utf-8" ?>
<OrdenPago>
  <IdMoneda>1</IdMoneda>
  <Total>22.55</Total>
  <MerchantId>KOT</MerchantId>
  <OrdenIdComercio>1000001</OrdenIdComercio>
  <UrlOk>http://localhost/UrlsDummy/UrlOk.htm</UrlOk>
  <UrlError>http://localhost/UrlsDummy/UrlError.htm</UrlError>
  <MailComercio>admin@kotear.pe</MailComercio>
  <UsuarioId>001</UsuarioId>
  <DataAdicional></DataAdicional>
  <UsuarioNombre>Cesar</UsuarioNombre>
  <UsuarioApellidos>Miranda</UsuarioApellidos>
  <UsuarioLocalidad>LIMA</UsuarioLocalidad>
  <UsuarioProvincia>LIMA</UsuarioProvincia>
  <UsuarioPais>PERU</UsuarioPais>
  <UsuarioAlias>cesarmmg</UsuarioAlias>
  <UsuarioEmail>cesar.mirandag@gmail.com</UsuarioEmail>
  <Detalles>
    <Detalle>
      <Cod_Origen>CT</Cod_Origen>
      <TipoOrigen>TO</TipoOrigen>
      <ConceptoPago>Transacci�n Comisi�n 1</ConceptoPago>
      <Importe>10.55</Importe>
      <Campo1></Campo1>      
      <Campo2></Campo2>
      <Campo3></Campo3>
    </Detalle>
	<Detalle>
      <Cod_Origen>CT</Cod_Origen>
      <TipoOrigen>TO</TipoOrigen>
      <ConceptoPago>Transacci�n Comisi�n 2</ConceptoPago>
      <Importe>12.00</Importe>
      <Campo1></Campo1>      
      <Campo2></Campo2>
      <Campo3></Campo3>
    </Detalle>
  </Detalles>
</OrdenPago></asp:TextBox>
        </td>
     </tr>                    
     <tr>
        <td></td>
        <td style="width: 506px">
           <asp:Button ID="btnRefrescarUrlServerPost" runat="server" Text="Refrescar" />
           <br />
           <asp:Button ID="btnGenerarCIP" runat="server" Text="Generar CIP" />
        </td>
     </tr>                         
    </table>
    </div>
    </form>
</body>
</html>
