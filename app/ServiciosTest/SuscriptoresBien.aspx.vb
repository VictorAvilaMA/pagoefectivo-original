
Partial Class SuscriptoresBien
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtUrlServer.Text = System.Configuration.ConfigurationManager.AppSettings("ServerUrlPost")

        btnRefrescarUrlServerPost_Click(sender, e)
        If (ViewState("XML") IsNot Nothing) Then
            txtXML.Text = ViewState("XML")
        End If
    End Sub

    Protected Sub btnGenerarCIP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerarCIP.Click
        If (ViewState("XML") IsNot Nothing) Then
            txtXML.Text = ViewState("XML")
        End If
    End Sub

    Protected Sub btnRefrescarUrlServerPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefrescarUrlServerPost.Click
        ViewState("XML") = txtXML.Text
        txtXML.Text = ""
        btnGenerarCIP.PostBackUrl = txtUrlServer.Text
        Dim objclsLibreria As New clsLibreriax
        TextBox6.Text = objclsLibreria.Encrypt(ViewState("XML"), objclsLibreria.fEncriptaKey)
    End Sub
End Class
