
Partial Class WebUtil
    Inherits System.Web.UI.Page

    Protected Sub btnEncriptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEncriptar.Click
        Dim objclsLibreria As New clsLibreriax
        txtResultado.Text = objclsLibreria.Encrypt(txtString.Text, objclsLibreria.fEncriptaKey)
    End Sub

    Protected Sub btnDesencriptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDesencriptar.Click
        Dim objclsLibreria As New clsLibreriax
        txtResultado.Text = objclsLibreria.Decrypt(txtString.Text, objclsLibreria.fEncriptaKey)
    End Sub
End Class
