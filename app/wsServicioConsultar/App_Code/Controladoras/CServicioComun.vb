Imports Microsoft.VisualBasic
Imports _3Dev.FW.EmsambladoComun
Imports SPE.EmsambladoComun


Public Class CServicioComun
    'Public Function ConsultarOPsConciliadas() As System.Data.DataSet
    '    Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IServicioComun.ConsultarOPsConciliadas()
    'End Function
    Public Function ConsultaNumCorrelativo(ByVal url As String) As Integer
        'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IServicioComun.ActualizarOPAMigradoRecaudacion(pidOrdenes)
        Using Conexions As New ProxyBase(Of IComun)
            Return Conexions.DevolverContrato().ConsultaNumCorrelativo(url)
        End Using
        'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IComun.ConsultaNumCorrelativo(url)
    End Function
End Class
