Imports Microsoft.VisualBasic
Imports SPE.EmsambladoComun

Namespace SPE.Web
    Public Class RemoteServices
        Inherits _3Dev.FW.Web.RemoteServices

        Private _IComun As SPE.EmsambladoComun.IComun

        Public Property IComun() As SPE.EmsambladoComun.IComun
            Get
                Return _IComun
            End Get
            Set(ByVal value As SPE.EmsambladoComun.IComun)
                _IComun = value
            End Set
        End Property

        Public Overrides Sub DoSetupNetworkEnviroment()
            MyBase.DoSetupNetworkEnviroment()
            Me.IComun = CType(Activator.GetObject(GetType(SPE.EmsambladoComun.IComun), Me.ServerUrl + "/" + NombreServiciosConocidos.IComun), SPE.EmsambladoComun.IComun)
        End Sub

    End Class
End Namespace
