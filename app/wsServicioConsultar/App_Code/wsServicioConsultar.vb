Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
    Inherits System.Web.Services.WebService
    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function
    <WebMethod()> _
    Public Function ConsultarNumCorrelativo(ByVal url As String) As Integer
        Dim resultado As Integer
        Try
            resultado = New CServicioComun().ConsultaNumCorrelativo(url) ' SPE.Web.CServicioComun().ActualizarOPAMigradoRecaudacion(pidOrdenes)
            Return resultado
            'Return CType(SPE.Web.RemoteServices.Instance, SPE.Web.RemoteServices).IComun.ConsultaNumCorrelativo(url)
        Catch ex As Exception
            _3Dev.FW.Web.Log.Logger.LogException(ex)
        End Try
        Return resultado
    End Function
End Class
