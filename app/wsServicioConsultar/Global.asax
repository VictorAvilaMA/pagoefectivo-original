<%@ Application Language="VB" %>
<%@ Import Namespace = "_3Dev.FW.Web.Log" %>
<script runat="server">
 Function EstablishConnection() As Boolean
        Return SPE.Web.RemoteServices.Instance.SetupNetworkEnvironment()
    End Function
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
          
        Try
            SPE.Web.RemoteServices.Instance = New SPE.Web.RemoteServices()
        Catch ex As Exception
            ex.Message.ToString()
            Return
        End Try


        If Not EstablishConnection() Then
            Return
        End If
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
                Logger.LogException(Me.Server.GetLastError.GetBaseException)
        Logger.LogTransaction("Error de Transacción")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
</script>